*************** This is Core PHP and Smarty templates based Project *******************

1. Files inside folder 'erp' are the project files. These php files worked as a controller.
2. Template files inside 'erp/templates' worked as View.
3. Php files inside 'lib/includes' worked as Model.
4. Third party services like editors, html2pdf, images croping are placed inside 'lib' folder along with sub folders 
   different folders.
5. Config file, database connection, Session manager,Authentication files inside 'lib' folder.
6. Every module has it's main php file eg account.php and for every action like add, edit, delete there are respective files account-add.php, account-edit.php and account-delete.php. Template files are in 'erp/templates' with names as account.html,account-add.html,account-edit.html 
7. Smarty is a template system. lib/smarty
8. tmp and templates_c are used to store cache files.
9. User uploaded files are found under 'files/'.