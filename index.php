<?php
    
    if ( !@constant("THIS_DOMAIN") ) {
        require_once("./lib/config.php"); 
    }

	page_open(array("sess" => "PT_Session",
					"auth" => "PT_Default_Auth"
				));
	
    include(THIS_PATH ."/header.php");
    
    $page_name = 'index.html';
    if ( !empty($page_name) || !empty($page_id) ) {
        if ( empty($page_id) ) {
            //$page_name = eregi_replace("\.html|\.php|\.asp|\.jsp", "", $page_name);
             $ext=array('.html','.php','.asp','.jsp','.HTML','.PHP','.ASP','.JSP');
             $page_name = str_replace($ext, "", $page_name);

            $condition = " WHERE ". TABLE_SETTINGS_STATICPAGES .".page_name='". $page_name ."' "
                    ." AND ". TABLE_SETTINGS_STATICPAGES .".page_status='1' ";

            $query = "SELECT page_id FROM ". TABLE_SETTINGS_STATICPAGES ." ". $condition ;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
                $page_id = $db->f("page_id");
            }
        }
        
        $condition = " WHERE ". TABLE_SETTINGS_STATICPAGES .".page_id = '". $page_id ."' "
                ." AND ". TABLE_SETTINGS_STATICPAGES .".page_status = '1' "
                ." AND (". TABLE_SETTINGS_STATICPAGES .".page_access = '0' ";
        if ( $logged_in ) {
            $condition .= " OR ". TABLE_SETTINGS_STATICPAGES .".page_access ='2' ";
        }
        else {
            $condition .= " OR ". TABLE_SETTINGS_STATICPAGES .".page_access ='1' ";
        }
        $condition .= ')';
        
        $query = "SELECT * FROM ". TABLE_SETTINGS_STATICPAGES ." ". $condition ;
        if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
            $data = processSQLData($db->result());
            
            if ( $data["show_heading"] ) {
                $variables["PAGE_HEADING"]  = $data["heading"];
            }
            if ( !empty($data["meta_keyword"]) ) {
                $variables["PAGE_KEYWORDS"]   = $data["meta_keyword"];
            }
            if ( !empty($data["meta_desc"]) ) {
                $variables["PAGE_DESC"]   = $data["meta_desc"];
            }
            $variables["PAGE_CONTENT"]  = $data["content"];
        }
        else {
            $variables["PAGE_CONTENT"]  = $s->fetch("noPage.html");
        }
    }
    else {
        $variables["PAGE_CONTENT"]  = $s->fetch("noPage.html");
    }

    // BO: Read a random picture from the gallery.
//    $gallery_img= '';
//    $query      = "SELECT fileName FROM ". TABLE_IMAGES;
//    $db->query($query);
//    if ( $db->nf()>0 ) {
//        $db->next_record();
//        $image_count = $db->nf();
//        if ( $image_count>0 ) {
//            $query .= " LIMIT ". rand(0, ($image_count-1)) .", 1 ";
//            $db->query($query);
//            if ( $db->nf()>0 ) {
//                $db->next_record();
//                $variables['gallery_img']= processSQLData($db->f('fileName'));
//                $variables['gallery']   = DIR_WS_GALLERY;
//                $variables['thumb']     = DIR_WS_GALLERY_THUMB;
//            }
//        }
//    }
    // EO: Read a random picture from the gallery.

    $s->assign("variables", $variables);
    $s->assign("CONTENT", $variables["PAGE_CONTENT"]);
    $s->display('home.html');
?>
