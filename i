<?php
	if ( !@constant("THIS_DOMAIN") ) {
		require_once("./lib/config.php"); 
	}
    
	page_open(array("sess" => "PT_Session",
					"auth" => "PT_Default_Auth"
				));
    
    $page_id    = isset($_GET["page_id"])   ? $_GET["page_id"]  : ( isset($_POST["page_id"])    ? $_POST["page_id"] : '' );
	$site_id    = isset($_GET["site_id"])   ? $_GET["site_id"]  : ( isset($_POST["site_id"])    ? $_POST["site_id"] : SITE_ID );
	
    $infoArr    = explode("/",$_SERVER["PATH_INFO"]);
    $page_name  = $infoArr[(count($infoArr)-1)];


    include(THIS_PATH."/header.php");

	$variables["full_url"] = $_SERVER["PATH_INFO"] ;
	$variables["page_name"] = $page_name ;

    if ( !empty($page_name) || !empty($page_id) ) {
        if ( empty($page_id) ) {
            $page_name = eregi_replace("\.html|\.php|\.asp|\.jsp", "", $page_name);
            
            $condition = " WHERE ". TABLE_SETTINGS_STATICPAGES .".page_name='". $page_name ."' "
                            ." AND ". TABLE_SETTINGS_STATICPAGES .".page_status='1' ";

            $query = "SELECT page_id FROM ". TABLE_SETTINGS_STATICPAGES ." ". $condition ;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
                $page_id = $db->f("page_id");
            }
        }
        
        $condition = " WHERE ". TABLE_SETTINGS_STATICPAGES .".page_id = '". $page_id ."' "
                        ." AND ". TABLE_SETTINGS_STATICPAGES .".page_status = '1' "
                        ." AND (". TABLE_SETTINGS_STATICPAGES .".page_access = '0' ";
        if ( $logged_in ) {
            $condition .= " OR ". TABLE_SETTINGS_STATICPAGES .".page_access ='2' ";
        }
        else {
            $condition .= " OR ". TABLE_SETTINGS_STATICPAGES .".page_access ='1' ";
        }
        $condition .= ')';
        
        $query = "SELECT * FROM ". TABLE_SETTINGS_STATICPAGES ." ". $condition ;
        if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
            $data = processSQLData($db->result());
            
            if ( $data["show_heading"] ) {
                $variables["PAGE_HEADING"]  = $data["heading"];
            }
            if ( !empty($data["meta_keyword"]) ) {
                $variables["PAGE_KEYWORDS"]   = $data["meta_keyword"];
            }
            if ( !empty($data["meta_desc"]) ) {
                $variables["PAGE_DESC"]   = $data["meta_desc"];
            }
            $variables["PAGE_CONTENT"]  = $data["content"];
        }
        else {
            $variables["PAGE_CONTENT"]  = $s->fetch("noPage.html");
        }
    }
    else {
        $variables["PAGE_CONTENT"]  = $s->fetch("noPage.html");
    }
    
    $s->assign("variables",$variables);
    $s->assign("CONTENT", $variables["PAGE_CONTENT"]);
    if ($page_name == "index") {
        $s->display('home.html');
    }
    else {
        $s->display('index.html');
    }
?>