<?php

/**
* Header script common to all files
    SMEERP Snipers aka SMEERP Development Team
    SMEERP Technologies
    website : http://www.pretechno.com
    email   : info@pretechno.com
    Date    :
    Purpose :     
**/

	// Retrieve the Menu List.
	include_once ( DIR_FS_INCLUDES ."/common-functions.inc.php");
    include_once ( DIR_FS_INCLUDES .'/news-events.inc.php' );
    
	if(!isset($_SESSION["lang"])){
		$lang = DEFAULT_LANG ;
		$_SESSION["lang"] = $lang ;
	}
	include(THIS_PATH ."/lib/languages/". $_SESSION["lang"] . ".lang.php");
	$etype     = isset($_GET["etype"]) ? $_GET["etype"] : ( isset($_POST["etype"]) ? $_POST["etype"] : '');
	$db 				= new db_local; // database handle
	//$s 					= new smarty; 	//smarty template handle
	$s = new Smarty();
	$messages 			= new Messager;	// create the object for the Message class.

	/* $s->template_dir 	= THIS_PATH ."/templates/";
	$s->compile_dir 	= THIS_PATH ."/templates/templates_c/";
	$s->config_dir 		= DIR_FS_LIB .'/smarty/configs/';
	$s->cache_dir 		= DIR_FS_LIB .'/smarty/cache/';
	//$s->debugging 	= true;
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true; */
	
	$s->template_dir	= THIS_PATH .'/templates/';
    $s->compile_dir 	= THIS_PATH .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	//$s->allow_php_templates= true;
	$s->force_compile = true;
	//$s->caching = true;
	//$s->cache_lifetime = 100;


	$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'direct';
	/* $variables["stats"] = "<img src='". THIS_DOMAIN ."/stats//docs/scripts/image.php"
										."?client_id=". SITE_ID 
										."&referer=".($referer) ."'"
										." alt='' border='0'>";

	*/
    
    if ( is_object($auth) ) {
        if ( isset($auth->auth["logged_in"]) && $auth->auth["logged_in"] ) {
            $my = $auth->auth;
            $logged_in = true;
        }
        else {
            $logged_in = false;
        }
    }
  
   
    
    /*if ( !isset($page_id) || $page_id == '' ) {
        if ( isset($page_name) ) {
            $page_name = eregi_replace("\.html|\.php|\.asp|\.jsp", "", $page_name);
        }
        else {
            $page_name = '';
        }
        if ( isset($page_name) ) {
            $page_id = getPageId($db, $page_name);
        }
        else {
            $page_id = '';
        }
    }*/
    if(!isset( $page_name)){
        $page_name='';
    }
    if(!isset( $page_id)){
        $page_id='';
    }
    
    //BOF For page banner 2009-07-jul-21 
    $page_details =array();
    $page_condition = $page_banner = '';
    if ( !isset($page_id) || $page_id == '' ) {
        //$page_name = eregi_replace("\.html|\.php|\.asp|\.jsp", "", $page_name);
        $ext=array('.html','.php','.asp','.jsp','.HTML','.PHP','.ASP','.JSP');
        $page_name = str_replace($ext, "", $page_name);

        if ( isset($page_name) && !empty($page_name)) {
            //$page_id = getPageId($db, $page_name);
            
            //Get page details BOF
            $table = TABLE_SETTINGS_STATICPAGES;
            $fields = ' page_id, banner ';
            $page_condition = " WHERE page_name = '". trim($page_name) ."'"." AND page_status = '1' ";
            $page_details = getRecord($table,$fields,$page_condition);              
            $page_id =$page_details['page_id'];
            $page_banner =$page_details['banner'];
            //Get page details EOF
        }
        else {
            $page_id = '';
        }
    }  
    
    if(empty($page_banner)){
         $page_condition = $page_banner = '';
         $table = TABLE_SETTINGS_STATICPAGES;
         $fields = " banner ";
         $page_condition = " WHERE is_default_banner = '1' ";
         $page_default = getRecord($table,$fields,$page_condition);  
         $page_banner =$page_default['banner'];
    }
    //EOF For page banner 2009-07-jul-21 
    
    //BOF Breadcrumbs
	$parent=array();
	$parent =NULL;
	$menu_id="";
	if(!empty($page_id)){	
	$query = "SELECT id FROM ".TABLE_MENU." WHERE page=". $page_id ;
		$db->query($query);
		while( $db->next_record()){
			$menu_id=$db->f("id");        		
		}				
	}		   
   
    getBreadcrumbs($menu_id, $breadcrumbs);
    if(!empty($breadcrumbs)){
		$breadcrumbs = array_reverse($breadcrumbs); 
    }
    else
    {
    	$self=Array();
    	$self=explode("/",$_SERVER['PHP_SELF']);
    	$name_php=$self[count($self)-1];
    	$name = '';
    	$url = '';
        
    	if($name_php=="contact-us.php")
    	{
    		$name="Contact Us";
    		$url=$_SERVER['PHP_SELF'];
	    }
	    
	    if($name_php=="gallery.php")
    	{
    		$name="Photo Gallery";
    		$url=$_SERVER['PHP_SELF'];
	    }
	    
	    if($name_php=="event-detail.php")
    	{
    		$name="News & Events";
    		$url=THIS_DOMAIN."/events_list.php?etype=".$etype;
	    }
	    
	    if($name_php=="events_list.php")
    	{
    		$name="News & Events";
    		$url=$_SERVER['PHP_SELF']."?etype=".$etype;
	    }	
	    
    	$breadcrumbs[]	= array( 
							    "name"	=> $name,
							    "page_url1" => $url
						        );
    }
    //EOF Breadcrumbs 
    
	// Read the Tickers.
    getTickers($db, $ticker['left'], 'LEFT', $page_id);
    getTickers($db, $ticker['right'], 'RIGHT', $page_id);
    getTickers($db, $ticker['top'], 'TOP', $page_id);
    getTickers($db, $ticker['bottom'], 'BOTTOM', $page_id);
    //print_r($ticker['right']);
	/* $do_expiry= '2011-01-07 00:00:00' ;// strtotime("2011-01-07");
	$arr = explode(' ',$do_expiry);
	print_R($arr);
	echo strtotime($arr[0]);
	echo"<br/>";
	echo $check_dt = strtotime(date('Y-m-d'));
	echo date('Y-m-d', $check_dt);
		exit; */
     
	
	// Read All the Menus.
	$menu = array();
    getAllMenus($db, $menu['top'], 'HT', '', '', $logged_in, SITE_ID);
    getAllMenus($db, $menu['bottom'], 'HB', '', '', $logged_in, SITE_ID);
    getAllMenus($db, $menu['left'], 'VL', '', '', $logged_in, SITE_ID);
    getAllMenus($db, $menu['right'], 'VR', '', '', $logged_in, SITE_ID);
    //print_r($menu);
   
    /* BOF Read the news & event */
    // Read the News Eents.
    $eventArr = array();
    $list_pressrelease = array();
    $list_media = array();
    $list_Coverage = array();
    
	$eventTypeArr = NewsEvents::getEventType();
	$eventArr  = array_flip($eventTypeArr);
	
    //$mtime          = mktime()-86400;
    $mtime = date('Y-m-d H:i:s');    
    //$mtime = explode('/', $current_date);
    //$mtime = $mtime[2] .'-'. $mtime[1] .'-'. $mtime[0] .' 00:00:00';
    $limit = 3;
    $condition_query_news='';
    $fields = TABLE_EVENTS.'.*';
    
   	$condition_query_news      = " WHERE status = '".NewsEvents::ACTIVE."'"
   							." AND event_type  = '". NewsEvents::PRESSRELEASE ."'"
                            ." AND events_exp_date >= '". $mtime ."'"
                            ." ORDER BY events_date ASC ";
    NewsEvents::getDetails( $db, $list_pressrelease, $fields, $condition_query_news, 0, $limit);
    
 	$flist_pressrelease = array();
    if(!empty( $list_pressrelease)){
        foreach( $list_pressrelease as $key =>$val){	
           $val['event_type_title']= $eventArr[$val['event_type']];
            $flist_pressrelease[$key]=$val ;
        }    
    }
    
   $condition_query_news='';
   $condition_query_news      = " WHERE status = '".NewsEvents::ACTIVE."'"
   							." AND event_type  = '". NewsEvents::MEDIA ."'"
                            ." AND events_exp_date >= '". $mtime ."'"
                            ." ORDER BY events_date ASC ";
    NewsEvents::getDetails( $db, $list_media, $fields, $condition_query_news, 0, $limit);
   
    $flist_media = array();
    if(!empty( $list_media)){
        foreach( $list_media as $key =>$val){	
           $val['event_type_title']= $eventArr[$val['event_type']];
           $flist_media[$key]=$val ;
        }    
    }
    
    $condition_query_news=''; 
    $condition_query_news      = " WHERE status = '".NewsEvents::ACTIVE."'"
   							." AND event_type  = '". NewsEvents::COVERAGE ."'"
                            ." AND events_exp_date >= '". $mtime ."'"
                            ." ORDER BY events_date ASC ";
    NewsEvents::getDetails( $db, $list_Coverage , $fields, $condition_query_news, 0, $limit);
   
    $flist_Coverage =array();
    if(!empty( $list_Coverage )){
        foreach( $list_Coverage  as $key =>$val){	
           $val['event_type_title']= $eventArr[$val['event_type']];
           $flist_Coverage[$key]=$val ;
        }    
    }
    
    /* EOF Read the news & event */
    
    //$jmenu = array();
    //getJSMenu($jmenu['top'], $menu['top']);
    //getJSMenu($jmenu['bottom'], $menu['bottom']);
    //getJSMenu($jmenu['left'], $menu['left']);
    //getJSMenuHT(&$jmenu['right'], $menu['right']);
    //print_r($jmenu);
    
    
    // Read the News Eents.
    //getNewsEvents($db, $lst_events);

    
    $charset = 'ISO-8859-1';
    
	$variables["header_img"]= sprintf("%'02s", rand(1,2));
    $variables['charset']   = $charset;
    $variables['domain']    = THIS_DOMAIN;
    $variables['title'] 	= TITLE;
    $variables['css'] 		= DIR_WS_CSS;
    $variables['images'] 	= DIR_WS_IMAGES;
    $variables['swf'] 	= DIR_WS_SWF;
    $variables['scripts'] 	= DIR_WS_SCRIPTS;
	
	$variables["PAGE_HEADING"]	= '';
	$variables["PAGE_INTRO"]	= htmlentities('PHP & MySQL based custom web development and customization of our products & solutions and open source projects');
	$variables["PAGE_INTRO_URL"]= str_replace(' ', '_', strtolower($variables["PAGE_INTRO"])) .'.html';
	$variables["PAGE_KEYWORDS"]	= '';
	$variables["PAGE_DESC"]		= '';

	//$variables["PAGE_BANNER"]		= DEFAULT_PAGE_BANNER;
    //$variables["DEFAULT_PAGE_BANNER"]		= DEFAULT_PAGE_BANNER;
    $variables['page_banner']   = $page_banner;
	$variables["PAGE_BANNER_PATH"] 	= DIR_WS_BANNER;
    
    $s->assign('list_pressrelease', $flist_pressrelease);
	$s->assign('list_media', $flist_media);
	$s->assign('list_coverage', $flist_Coverage); 
	$s->assign('variables', $variables);
    $s->assign('breadcrumbs', $breadcrumbs);
	$s->assign('menu', $menu);
    //$s->assign('jmenu', $jmenu);
	$s->assign('ticker', $ticker);
    //$s->assign('lst_events', $lst_events);

    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Pragma: no-cache');
    header('Content-Type: text/html; charset='. $charset);

?>