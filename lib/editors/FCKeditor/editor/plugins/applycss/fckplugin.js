﻿/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2005 Frederico Caldeira Knabben
 * 
 * Licensed under the terms of the GNU Lesser General Public License:
 * 		http://www.opensource.org/licenses/lgpl-license.php
 * 
 * For further information visit:
 * 		http://www.fckeditor.net/
 * 
 * "Support Open Source software. What about a donation today?"
 * 
 * File Name: fckplugin.js
 * 	Plugin to insert "Placeholders" in the editor.
 * 
 * File Authors:
 * 		Frederico Caldeira Knabben (fredck@fckeditor.net)
 */


var oapplycssCombo = new FCKToolbarStyleCombo() ;
oapplycssCombo.FieldWidth = 250 ;
oapplycssCombo.PanelWidth = 300 ;
FCKToolbarItems.RegisterItem( 'Apply_css', oapplycssCombo ) ;

