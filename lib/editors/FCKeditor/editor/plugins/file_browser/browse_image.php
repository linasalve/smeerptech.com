<?php
	include("./dir.class.php");
	
	if(!@constant("THIS_DOMAIN")){
		require_once("../../../../../../lib/config.php"); 
	}
	
	// Configurations.
	//$f_base_path 		= THIS_PATH .'/'. DIR_FS_EDITOR_UPLOADS;
	//$w_base_path 		= THIS_DOMAIN .'/'. DIR_WS_EDITOR_UPLOADS;
	
	$dir_new	= isset($_GET["filedir"]) 	? $_GET["filedir"]	: ( isset($_POST["filedir"]) 	? $_POST["filedir"] : '' );
	$cur_dir	= isset($_GET["cur_dir"]) 	? $_GET["cur_dir"]	: ( isset($_POST["cur_dir"]) 	? $_POST["cur_dir"] : '' );
	$error_code = array();
	$show		= 'list';
	$result 	= NULL;
	$error_code	= NULL;
	
    $objDir 	= new Dir(DIR_FS_EDITOR_UPLOADS, $cur_dir);
	$objDir->setFilter(array('image/bmp', 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/x-png'), 'mime');
    $objDir->setFilter(array('bmp', 'gif', 'jpg','jpeg', 'pjpeg', 'png'), 'ext');
	$objDir->setFilter('1048576', 'max_size');
	$objDir->setFilter('/^[a-zA-Z0-9][a-zA-Z0-9_\.-]{1,30}$/', 'filename');
	$objDir->setFilter('/^[a-zA-Z0-9][a-zA-Z0-9_-]{1,20}$/', 'dirname');

	// Begin the procedure to delete the Files/Directories.
	if ( isset($_POST['delete_selected']) 
			&& isset($_POST['chk_delete']) 
			&& count($_POST['chk_delete'])>0 ) {
		
		$files_to_delete = array();
		
		foreach ($_POST['chk_delete'] as $value) {
			$result = NULL;
			if ( $objDir->exists($value, $result) ) {
				$files_to_delete[] = $result;
			}
		}

		if ( count($files_to_delete) <= 0 ) {
			$error_code[] 	= '4'; // No dir/files found to delete.
		}
		else {
			$show = 'delete_confirm';
		}
	}

	// Delete the Files/Directories.
	if ( isset($_POST['delete_confirm']) 
			&& isset($_POST['chk_confirm']) && $_POST['chk_confirm'] == '1' 
			&& count($_POST['delete_files'])>0 ) {
		// The Delete Operation is confirmed, delete the files and directories.
		
		$files_deleted = true;
		foreach ($_POST['delete_files'] as $value) {
			if ( !$objDir->delete($value, $result) ) {
				$error_code[] 	= $result['code'];
				$files_deleted 	= false;
			}
		}
		if ( $files_deleted ) {
			if ( count($_POST['delete_files'])>1 ) {
				$error_code[] = '8';
			}
			else {
				$error_code[] = '7';
			}
		}
	}

	// Create a new Directory
	if ( isset($_POST['submit_newdir']) 
			&& isset($_POST['new_dirname']) 
			&& !empty($_POST['new_dirname']) ) {

		//if ( preg_match('^[a-zA-Z][a-zA-Z0-9_\.]{1,20}$', $_POST['new_dirname']) ) {
			// The name is a valid name, create the directory.
			$objDir->createDirectory($_POST['new_dirname'], $result);

			foreach ($result as $value) {
				$error_code[] = $value['code'];
			}
//		}
//		else {
//			$error_code[] = '9';
//		}
	}
	

	if ( isset($_POST['submit_upload']) 
			&& isset($_FILES['upload_file']) 
			&& !empty($_FILES['upload_file']) ) {

		if ( !empty($_FILES['upload_file']["tmp_name"]) && $_FILES['upload_file']["size"] != 0 ) {
			// The name is a valid name, create the directory.
                    $objDir->uploadFile($_FILES['upload_file'], $result, '11'); // 11 -> Check MIME & extension of the file.
//			$objDir->uploadFile($_FILES['upload_file'], $result, true);  MIME restrictions in effect.

			foreach ($result as $value) {
				$error_code[] = $value['code'];
			}
		}
		else {
			$error_code[] = '14';
		}
	}

	
	// Read the refreshed Directory.	
	$file_list 	= $objDir->getFileList($cur_dir);
//	$file_list 	= $objDir->getFileList($cur_dir, true);	  MIME restrictions in effect.
	$dir_arr 	= $objDir->getCurPathArray();
	
	$IMG['MAX_SIZE']= array('bytes' => $objDir->getFilter('max_size'), 'display' => ($objDir->getFilter('max_size')/1024).' Kb');
	$IMG['MIME'] 	= $objDir->getFilter('mime');
	
	// Cleanup procedure.
	$result = NULL;
?>

<html>
<head>
<title>Browse Directory/Files</title>
<style  type="text/css">
#browse_image {
	height:100%;
	margin:0;
	overflow:scroll;
	padding:0;
	width:100%;
}
.tablediv {
	/*font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	font-weight:normal;*/
	display:  table;
	width:100%;
	background-color:#FDFDFD;
	border:1px solid  #666666;
	border-spacing:0px;/*cellspacing:poor IE support for  this*/
	border-collapse:separate;
	overflow:scroll;
	margin:0;padding:1;
}
celldiv1 {
display:  table-cell;
padding-left:5px;
width:2%;
}
.celldiv2 {
display:  table-cell;
padding-left:5px;
width:68%;
}
.celldiv3 {
padding-left:5px;
width:5%;
}
.celldiv4 {
padding-left:5px;
width:20%;
}
.celldiv5 {
width:5%;
}
.rowdiv_even  {
	width:auto;
	background-color:#FCFCFC;
	height:20px;
}
.rowdiv_odd  {
	width:auto;
	background-color:#F5F5F5;
	height:20px;
}
.header {
	background-color:#999999;
	color:#000000;
	font-size:12px;
	font-weight:bold;
	height:20px;
}

a.dir {
	color:#000000;
	cursor:pointer;
	display:block;
	font-family:Georgia, "Times New Roman", Times, serif;
	font-size:12px;
	font-weight:bold;
	text-decoration:none;
}
a.file {
	color:#000000;
	cursor:pointer;
	display:block;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	font-weight:normal;
	text-decoration:none;
}
a.dir:hover, a.file:hover {
	color:#0066CC;
	text-decoration:underline;
}

a.action {
	background-color:#EBEBEB;
	border:1px solid #999999;
	color:#000000;
	cursor:pointer;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	font-weight:normal;
	padding:2px 2px 2px 2px;
	text-decoration:none;
}
a.action:hover{
	background-color:#FFFFCC;
}
a.delete {
	background-image:url('./icons/delete-off.gif');
	background-repeat:no-repeat;
	color:transparent;
	cursor:pointer;
	height:16px;
	width:16px;
	text-decoration:none;
}
a.delete:hover {
	background-image:url('./icons/delete-on.gif');
	background-repeat:no-repeat;
}
span.breadcrumb {
	cursor:pointer;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
	font-weight:normal;
	padding:2px 2px 2px 2px;
	text-decoration:none;
	height:20px;
	line-height:20px;
	padding-top:2px;
	padding-bottom:2px;
}
a.breadcrumb {
	color:#0033FF;
	cursor:pointer;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
	font-weight:normal;
	padding:2px 2px 2px 2px;
	text-decoration:none;
}
.aLeft {text-align:left;}
.aCenter {text-align:center;}
.aRight {text-align:right;}
.input_upload {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	font-weight:normal;
	height:15px;
}
.input_button {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	font-weight:normal;
	height:18px;
}
.txt {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	font-weight:normal;
}
</style>
<script type="text/javascript" language="javascript" src="./lang/en.js"></script>
<script type="text/javascript" language="javascript">
	function openDir(filename) {
		document.frmDirWindow.cur_dir.value = filename;
		document.frmDirWindow.method = "POST";
		document.frmDirWindow.submit();
	}
	
	function selectFile(filename) {
		//window.opener.document.getElementById('imageurl').value = filename;
		//window.close();
	window.top.opener.SetUrl( '<?=DIR_WS_EDITOR_UPLOADS?>'+ filename ) ;
	window.top.close() ;
	window.top.opener.focus() ;
	}
	
	function deleteFile(filename) {
		var j_delete = document.getElementsByName('chk_delete[]');

		j_delete[0].value = filename;
		j_delete[0].checked = true;

		document.frmDirWindow.delete_selected.click();
	}
	
</script>
</head>
<body>
<div id="browse_image">
<?php	
	if ( $show == 'list' ) { 
?>
<span class="breadcrumb">/
<a href="#" class="breadcrumb" onClick="javascript:openDir('');">root</a>/
<?php
	foreach ($dir_arr as $key=>$dir) { 
?>
<a href="#" class="breadcrumb" onClick="javascript:openDir('<?=$dir['path']?>');"><?=$dir['dir']?></a>/
<?php } ?>
</span>
<form name="frmDirWindow" action="browse_image.php" method="post" enctype="multipart/form-data">
<table border="0" cellpadding="2" cellspacing="2" class="tablediv">
<tr class="rowdiv header">
	<td class="celldiv1 aCenter"></td>
	<td class="celldiv2 aCenter">&nbsp;</td>
	<td class="celldiv3 aCenter">Type</td>
	<td class="celldiv4 aCenter">Size</td>
	<td class="celldiv5 aCenter">Action</td>
</tr>
<?php if ( count($file_list)>0 ) { ?>
<? foreach ( $file_list as $key=>$file ) { ?>
	<tr class="<?=($key%2==0)? 'rowdiv_even': 'rowdiv_odd'?> ">	
		<td class="celldiv1"><input name="chk_delete[]" type="checkbox" value="<?='/'. $file['name']?>" /></td>
		<td class="celldiv2">
<?		if($file['type'] == 'd') { ?>
			<a name='' class="dir" title="Click to Open"
					onClick="javascript:openDir('<?=$cur_dir .'/'. $file['name']?>');"><?=$file['name']?></a>	
<?		} else { ?>
			<a name="#" class="file" title="Click to Select" 
					onClick="javascript:selectFile('<?=$cur_dir .'/'. $file['name']?>');"><?=$file['name']?></a>
<?		} ?>	
		</td>		
		<td class="celldiv3 txt"><?=$file['type']?></td>
		<td class="celldiv4 aRight txt"><?=($file['size']!='')? $file['size'] .' bytes' : '';?></td>
		<td class="celldiv5 aCenter">
			<a name="#" class="delete"
					onClick="javascript:deleteFile('<?='/'. $file['name']?>');">&nbsp;&nbsp;&nbsp;&nbsp;</a>
		</td>
</tr>
<? } ?>
<tr>
	<td colspan="5" class="txt">
		<input type="submit" name="delete_selected" value="Delete Selected" class="input_button" />
	</td>
</tr>
<tr>
	<td colspan="5" class="txt">
		Create new Directory 
		<input type="text" name="new_dirname" value="" maxlength="20" class="input_button" />
		<input type="submit" name="submit_newdir" value="Create" class="input_button" />
		valid characters (a-z, 0-9, _ ) starting with a letter.
	</td>
</tr>
<tr>
	<td colspan="5" class="txt">
		<span class="input_upload">Upload file to server
			<input type="hidden" name="cur_dir" value="<?=$cur_dir?>" />
			<input type="hidden" name="MAX_FILE_SIZE" value="<?=$IMG['MAX_SIZE']['bytes']?>" />
			<input type="file" name="upload_file" value="" class="input_button" />
			<input type="submit" name="submit_upload" value="Upload" class="input_button" />
			(file size limit <?=$IMG['MAX_SIZE']['display']?>) 
		</span>
	</td>
</tr>
<?php } 
	else {
?>
<tr>
	<td colspan="5" class="txt">The Directory is empty.</td>
</tr>
<tr>
	<td colspan="5" class="txt">
		Create new Directory 
		<input type="text" name="new_dirname" value="" maxlength="20" class="input_button" />
		<input type="submit" name="submit_newdir" value="Create" class="input_button" />
		valid characters (a-z, 0-9, _ ) starting with a letter.
	</td>
</tr>
<tr>
	<td colspan="5" class="txt">
		<span class="input_upload">Upload file to server
			<input type="hidden" name="cur_dir" value="<?=$cur_dir?>" />
			<input type="hidden" name="MAX_FILE_SIZE" value="<?=$IMG['MAX_SIZE']['bytes']?>" />
			<input type="file" name="upload_file" value="" class="input_button" />
			<input type="submit" name="submit_upload" value="Upload" class="input_button" />
			(file size limit <?=$IMG['MAX_SIZE']['display']?>) 
		</span>
	</td>
</tr>
<?php } ?>
</table>
</form>
<?php } ?>

<?php if ( $show == 'delete_confirm' ) { ?>
<form name="frmDeleteConfirm" action="browse_image.php" method="post">
<table border="0" cellpadding="2" cellspacing="2">
<tr>
	<td class="txt">Please confirm the deletion of the following files/directories</td>
</tr>
<tr>
	<td style="padding-left:5px;" class="txt">
		<ul>
<?php 
	foreach ( $files_to_delete as $file_name ) {
		echo ('<li>'. $file_name['filename'] .' ('. $file_name['type'] .')</li>');
		echo ('<input type="hidden" name="delete_files[]" value="'. $file_name['filename'] .'" />');
	}
?>
		</ul>
	</td>
</tr>
<tr>
	<td class="txt">
		<input type="checkbox" name="chk_confirm" value="1" />
		yes, delete the Directories, Sub-Directories and the files permanently.
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>

<tr>
	<td>
		<input type="submit" name="<?=$show?>" value="Confirm" class="editor_button" />
		<input type="submit" name="btn_cancel" value="Cancel" class="editor_button" />
		<input type="hidden" name="cur_dir" value="<?=$cur_dir?>" />
	</td>
</tr>
</table>
<form>
<?php } 
if ( count($error_code)>0 ) {
?>
<script language="javascript" type="text/javascript">
	eval ('var errors_last = Array(<?php echo( '"'. implode('","', $error_code) .'"');?>);');
	var msg = '';
	for ( i=0; i<errors_last.length; i++ ) {
		msg += error[errors_last[i]] +"\n";
	}
	alert(msg);
</script>
<?php } ?>
</div>
</body>
</html>