<?php
class Dir {
	private $base_path;
	private $cur_dir;
	private $filter;


	function Dir($base_path, $cur_dir='') {
		$this->base_path 	= $base_path;
		$this->cur_dir 		= $cur_dir;
		$this->filter		= array (
									'mime' 		=> array('image/bmp', 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/x-png'),
									'ext' 		=> array('bmp', 'gif', 'jpg','jpeg', 'pjpeg', 'png'),
									'filename' 	=> '/^[a-zA-Z][a-zA-Z0-9_\.]{1,30}$/',
									'dirname' 	=> '/^[a-zA-Z][a-zA-Z0-9_]{1,20}$/',
									'max_size'	=> '1048576', // always in bytes
								);
	}


	function getFileList($dirname, $check_bit=false, $mime=false, $ext=false) {
		if (!$mime) {
			$mime = $this->filter['mime']; // default MIME types of the files that are allowed.
		}
		elseif ( !is_array($mime) ) {
			$mime = explode(',', $mime);
		}
		if (!$ext) {
			$ext = $this->filter['ext']; // default Extentions of the files that are allowed.
		}
		elseif ( !is_array($ext) ) {
			$ext = explode(',', $ext);
		}
		
		$dir_list	= array();
		$files 		= array();
		$file		= NULL;

		// Clear the File Statistics Cache.
		clearstatcache();
						
		if ( is_dir( $this->base_path . $dirname) ) {
			$this->cur_dir = $dirname;
		}
		$hwnd_dir 		= @opendir($this->base_path . $this->cur_dir);
		if ( $hwnd_dir ) {
			while ( false !== ($file = readdir($hwnd_dir)) ) {
				
				if ( is_dir($this->base_path . $dirname.'/'.$file) && $file != '.' && $file != '..') {
					$dir_list[] = array(	'name' => $file,
											'size' => '',
											'type' => 'd'
										);
				}
				elseif ( is_file($this->base_path . $dirname.'/'.$file) ) {

					if ( $this->checkFilter($this->base_path . $dirname.'/', $file, $check_bit, $mime, $ext) ) {
						$files[] = array(	'name' => $file,
											'size' => filesize($this->base_path . $dirname.'/'.$file),
											'type' => (strrpos($file, '.')) ? substr($file, strrpos($file, '.')+1) : ''
										);
					}
				}
			}	
			// Close the Directory Handle.
			closedir($hwnd_dir);
			sort($dir_list);
			sort($files);
	
			$files = array_merge($dir_list, $files);
		}
		
		return ($files);
	}
	
	
	function exists($filename, &$return_value='', $check_bit=false, $mime=false, $ext=false) {
		$result = false;
		
		if (!$mime) {
			$mime = $this->filter['mime']; // default MIME types of the files that are allowed.
		}
		elseif ( !is_array($mime) ) {
			$mime = explode(',', $mime);
		}
		if (!$ext) {
			$ext = $this->filter['ext']; // default Extensions of the files that are allowed.
		}
		elseif ( !is_array($ext) ) {
			$ext = explode(',', $ext);
		}

		// Clear the File Statistics Cache.
		clearstatcache();
		
		// Parent Directory exists or not.
		if ( is_dir( $this->base_path . $this->cur_dir) ) {
			if ( is_dir($this->base_path . $this->cur_dir . $filename) ) {
				// The filename is a Directory.
				$result 		= true;
				$return_value	= array('filename' 	=> $filename,
										'path' 		=> $this->cur_dir,
										'type' 		=> 'dir'
										);
			}
			elseif ( is_file($this->base_path . $this->cur_dir . $filename) ) {
				// The filename is a File.
				// Check if the file gets through the filter or not.
				if ( $this->checkFilter($this->base_path.$this->cur_dir, $filename, $check_bit, $mime, $ext) ) {
					$result = true;
					$return_value	= array('filename' 	=> $filename,
											'path' 		=> $this->cur_dir,
											'type' 		=> 'file'
											);
				}
				else {
					// The file is not allowed to be listed, generate the error.
					$return_value	= array('code'=> '1',
											'desc'=> 'The File is Forbidden.'
											);
				}
			}
			else {
				// There is no Directory or a File with the name filename.
				$return_value	= array('code'=> '2',
										'desc'=> 'File not found.'
										);
			}
		}
		
		return ($result);
	}
	
	
	function delete($filename, &$return_value='') {
		$result = false;
		$path 	= $this->base_path . $this->cur_dir;
		
		// Strip all the relative paths.
		$filename = str_replace('../', '/', $filename);
		$filename = str_replace('/..', '/', $filename);
		$filename = str_replace('./', '/', $filename);
		$filename = str_replace('/.', '/', $filename);
		
		// Clear the File Statistics Cache.
		clearstatcache();
		
		// Parent Directory exists or not.
		if ( is_dir($path) ) {
			$old_umask = umask(0);
			@chmod($path . $filename, 0777);
			if ( is_writable($path . $filename) ) {

				if ( is_dir($path . $filename) ) {
					// The filename is a Directory.
					$hwnd_dir = @opendir($path . $filename);
					if ( $hwnd_dir ) {
						while ( false !== ($file = readdir($hwnd_dir)) ) {
							if ( $file != "." && $file != ".." ) {
								$this->delete($filename .'/'. $file, $return_value);
							}
						}
						closedir($hwnd_dir);
					}
					if ( ! @rmdir($path . $filename) ) {
						$return_value	= array('code'=> '5',
												'desc'=> 'Internal server error.'
												);
					}
					else {
						$result = true;
					}
				}
				elseif ( is_file($this->base_path . $this->cur_dir . $filename) ) {
					// The filename is a File.
					if ( ! @unlink($this->base_path . $this->cur_dir . $filename) ) {
						$return_value	= array('code'=> '5',
												'desc'=> 'Internal server error.'
												);
					}
					else {				
						$result = true;
					}
				}
				else {
					// There is no Directory or a File with the name filename.
					$return_value	= array('code'=> '2',
											'desc'=> 'File not found.'
											);
				}
			}
			else {
				$return_value	= array('code'=> '6',
										'desc'=> 'no Permission.'
										);
			}
			umask($old_umask);
		}
		
		return $result;
	}
	
	
	function createDirectory($dirname, &$return_value='') {
		$result = false;
		$path 	= $this->base_path . $this->cur_dir .'/';
		
		// Strip all the relative paths.
		$dirname = str_replace('../', '/', $dirname);
		$dirname = str_replace('/..', '/', $dirname);
		$dirname = str_replace('./', '/', $dirname);
		$dirname = str_replace('/.', '/', $dirname);
		
		// Clear the File Statistics Cache.
		clearstatcache();
		
		// Validations.
		// Check for valid directory name (max 20 characters).
		if ( !preg_match($this->filter['dirname'], $dirname) ) {
			$return_value[]	= array('code'=> '9',
									'desc'=> 'Invalid name.'
									);		
		}
		
		// The Parent Directory exists or not.
		if ( !is_dir($path) ) {
			$return_value[]	= array('code'=> '2',
									'desc'=> 'Directory not found.'
									);	
		}
		else {
			// Parent Directory exists.
			$old_umask = umask(0);
			@chmod($path, 0777);
			if ( !@is_writable($path) ) {
				$return_value[]	= array('code'=> '12',
										'desc'=> 'parent directory not writable.'
										);
			}
			
			// Check for Duplicate Directory name in the Parent Directory.

			if ( is_dir($path . $dirname) ) {
				$return_value[]	= array('code'=> '11',
										'desc'=> 'Duplicate directory name.'
										);
			}
		}
		
		// If no error then proceed to create the Directory.
		if ( !is_array($return_value) || count($return_value)<1 ) {		

			if ( @mkdir($path.$dirname, 0777) ) {
				$return_value[]	= array('code'=> '13',
										'desc'=> 'Directory created.'
										);
				$result = true;
			}
			else {
				$return_value[]	= array('code'=> '10',
										'desc'=> 'Internal error.'
										);
			}
			umask($old_umask);
		}
		
		return $result;
	}
	
	
	function uploadFile($file_obj, &$return_value='', $check_bit=false, $mime=false, $ext=false) {
		$result = false;
		$path 	= $this->base_path . $this->cur_dir .'/';
		
		if (!$mime) {
			$mime = $this->filter['mime']; // default MIME types of the files that are allowed.
		}
		elseif ( !is_array($mime) ) {
			$mime = explode(',', $mime);
		}
		if (!$ext) {
			$ext = $this->filter['ext']; // default Extensions of the files that are allowed.
		}
		elseif ( !is_array($ext) ) {
			$ext = explode(',', $ext);
		}
					
		// Clear the File Statistics Cache.
		clearstatcache();
		
		// Validations.
		// Check for valid file name (max 20 characters).
        if ( !preg_match($this->filter['filename'], $file_obj['name']) ) {echo $this->filter['filename'];
			$return_value[]	= array('code'=> '21',
									'desc'=> 'Invalid name.'
									);
		}

		
		// Check if the file gets through the filter or not.
		if ( ! $this->checkFileFilter( $check_bit, $file_obj['type'], $mime, 
												 substr($file_obj['name'], strrpos($file_obj['name'], '.')+1), $ext) ) {
			$return_value[]	= array('code'=> '20',
									'desc'=> 'Invalid File type.'
									);	
		}
		
		// Check the size of the file.
		if ( $file_obj["size"] > $this->filter['max_size'] ) {
			$return_value[]	= array('code'=> '17',
									'desc'=> 'Max file size.'
									);
		}

		// Check if the Parent Directory exists or not.
		if ( !is_dir($path) ) {
			$return_value[]	= array('code'=> '2',
									'desc'=> 'Directory not found.'
									);	
		}
		else {
			// Parent Directory exists.
			$old_umask = umask(0);
			@chmod($path, 0777);
			if ( !@is_writable($path) ) {
				$return_value[]	= array('code'=> '12',
										'desc'=> 'parent directory not writable.'
										);
			}

			// Check for Duplicate Filename name in the Parent Directory.
			if ( file_exists($path . $file_obj['name']) ) {
				$return_value[]	= array('code'=> '16',
										'desc'=> 'Duplicate filename name.'
										);
			}
		}
		
		// If no error then proceed to create the Directory.
		if ( !is_array($return_value) || count($return_value)<1 ) {		

			if ( @copy($file_obj['tmp_name'], $path.$file_obj['name']) ) {
				@chmod($path.$file_obj['name'], 0777);
				$return_value[]	= array('code'=> '18',
										'desc'=> 'File uploaded.'
										);
				$result = true;
			}
			else {
				$return_value[]	= array('code'=> '19',
										'desc'=> 'Internal error.'
										);
			}
			umask($old_umask);
		}
		
		return $result;
	}
	
	
	function getPathArray($path) {
		$path_array = array();
		
		if ( !is_array($path) ) {
			$path = explode('/', $path);
		}

		foreach ( $path as $key=>$value ) {
			if ( $value != '' ) {
				$index = count($path_array)-1;
				if ( $index < 0 ) {
					$prev_path = '';
				}
				else {
					$prev_path = $path_array[$index]['path'];
				}
				
				
				$path_array[] = array('path' => ( $prev_path .'/'. $value), 'dir' => $value);
			}
		}
		
		return ($path_array);
	}
	
	
	function getCurPathArray() {
		return ($this->getPathArray($this->cur_dir));
	}
	
	
	function getFilter($index='') {
		if ( $index != '' ) {
			return ($this->filter[$index]);
		}
		else {
			return ($this->filter);
		}
	}
	
	function setFilter($filter, $index='') {
		if ( $index != '' ) {
			$this->filter[$index] = $filter;
		}
		else {
			if ( is_array($filter) ) {
				foreach ( $filter as $key=>$value ) {
					$this->filter[$key] = $value;
				}
			}
		}
		return (true);
	}

	
	function checkFilter($path, $filename, $check_bit, $mime_arr, $ext_arr) {
		//echo '<br/>['. $type .']['. $ext .']';print_r($ext_arr);
		switch ( $check_bit ) {
			case ('01'): {
				$ext 	= substr($filename, strrpos($filename, '.')+1);
				if ( !empty($ext) && in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('02'): {
				$ext 	= substr($filename, strrpos($filename, '.')+1);
				if ( !empty($ext) && !in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('10'): {
                if ( function_exists('mime_content_type') ) {
                    $type = mime_content_type($path.$filename);
                }
                else {
                    // BOF :: code for mime content type -> date : 2007 08 Aug 13
                    $finfo = finfo_open(FILEINFO_MIME); // return mime type ala mimetype extension
                    $type = finfo_file($finfo, $path.$filename);
                    finfo_close ($finfo);
                    // EOF :: code for mime content type -> date : 2007 08 Aug 13
                }
                        
				if ( !empty($type) && in_array($type, $mime_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('11'): {
                if ( function_exists('mime_content_type') ) {
                    $type = mime_content_type($path.$filename);
                }
                else {
                    // BOF :: code for mime content type -> date : 2007 08 Aug 13
                    $finfo = finfo_open(FILEINFO_MIME); // return mime type ala mimetype extension
                    $type = finfo_file($finfo, $path.$filename);
                    finfo_close ($finfo);
                    // EOF :: code for mime content type -> date : 2007 08 Aug 13
                }
                
				$ext 	= substr($filename, strrpos($filename, '.')+1);
				if ( !empty($type) && in_array($type, $mime_arr) && !empty($ext) && in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('12'): {
                if ( function_exists('mime_content_type') ) {
                    $type = mime_content_type($path.$filename);
                }
                else {
                    // BOF :: code for mime content type -> date : 2007 08 Aug 13
                    $finfo = finfo_open(FILEINFO_MIME); // return mime type ala mimetype extension
                    $type = finfo_file($finfo, $path.$filename);
                    finfo_close ($finfo);
                    // EOF :: code for mime content type -> date : 2007 08 Aug 13
                }
				$ext 	= substr($filename, strrpos($filename, '.')+1);
				
				if ( !empty($type) && in_array($type, $mime_arr) && !empty($ext) && !in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
			}
			case ('20'): {
                if ( function_exists('mime_content_type') ) {
                    $type = mime_content_type($path.$filename);
                }
                else {
                    // BOF :: code for mime content type -> date : 2007 08 Aug 13
                    $finfo = finfo_open(FILEINFO_MIME); // return mime type ala mimetype extension
                    $type = finfo_file($finfo, $path.$filename);
                    finfo_close ($finfo);
                    // EOF :: code for mime content type -> date : 2007 08 Aug 13
                }
                
				if ( !empty($type) && !in_array($type, $mime_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('21'): {
                if ( function_exists('mime_content_type') ) {
                    $type = mime_content_type($path.$filename);
                }
                else {
                    // BOF :: code for mime content type -> date : 2007 08 Aug 13
                    $finfo = finfo_open(FILEINFO_MIME); // return mime type ala mimetype extension
                    $type = finfo_file($finfo, $path.$filename);
                    finfo_close ($finfo);
                    // EOF :: code for mime content type -> date : 2007 08 Aug 13
                }
                
				$ext 	= substr($filename, strrpos($filename, '.')+1);
				
				if ( !empty($type) && !in_array($type, $mime_arr) && !empty($ext) && in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('22'): {
                if ( function_exists('mime_content_type') ) {
                    $type = mime_content_type($path.$filename);
                }
                else {
                    // BOF :: code for mime content type -> date : 2007 08 Aug 13
                    $finfo = finfo_open(FILEINFO_MIME); // return mime type ala mimetype extension
                    $type = finfo_file($finfo, $path.$filename);
                    finfo_close ($finfo);
                    // EOF :: code for mime content type -> date : 2007 08 Aug 13
                }
				$ext 	= substr($filename, strrpos($filename, '.')+1);

				if ( !empty($type) && !in_array($type, $mime_arr) && !empty($ext) && !in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			default: {
				return true;
			}
		}
	}

	
	function checkFileFilter($check_bit, $mime, $mime_arr, $ext, $ext_arr) {
		
		switch ( $check_bit ) {
			case ('01'): {
				if ( !empty($ext) && in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('02'): {
				if ( !empty($ext) && !in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('10'): {
				if ( !empty($mime) && in_array($mime, $mime_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('11'): {

				if ( !empty($mime) && in_array($mime, $mime_arr) && !empty($ext) && in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('12'): {
				if ( !empty($mime) && in_array($mime, $mime_arr) && !empty($ext) && !in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
			}
			case ('20'): {
				if ( !empty($mime) && !in_array($mime, $mime_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('21'): {
				if ( !empty($mime) && !in_array($mime, $mime_arr) && !empty($ext) && in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			case ('22'): {
				if ( !empty($mime) && !in_array($mime, $mime_arr) && !empty($ext) && !in_array($ext, $ext_arr) ) {
					return (true);
				}
				else {
					return false;
				}
				break;
			}
			default: {
				return true;
			}
		}
	}

}
	
?>