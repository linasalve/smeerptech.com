// Define the command.

var FCKEditor = function( name )
{
	this.Name = name ;
	this.EditMode = FCK.EditMode;
}

FCKEditor.prototype.Execute = function()
{
	switch ( this.Name )
	{		
		case 'PageBreak' :
			alert('in here') ;
			break;
		default :
	}
}

FCKEditor.prototype.GetState = function()
{
	return FCK_TRISTATE_OFF ;
}

// Register the commands.
FCKCommands.RegisterCommand( 'FileBrowser', new FCKEditor( 'PageBreak' ) ) ;

var fckItem = new FCKToolbarButton( 'FileBrowser', 'PageBreak' ) ;
fckItem.IconPath = FCKConfig.PluginsPath + 'file_browser/browse_server.gif';
FCKToolbarItems.RegisterItem( 'FileBrowser', fckItem ) ;
