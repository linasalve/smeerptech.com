// JavaScript Document

var error = Array();

//error[] = '';
error[1] = 'No Permission to access the file/directory.';
error[2] = 'The file/directory is not found.';
error[3] = 'The file/directory is not Writable';
error[4] = 'There is no valid file or directory to be deleted';
error[5] = 'Internal Error: Cannot delete the file/directory';
error[6] = 'You do not have the permission to delete the specified file/directory.';
error[7] = 'The file/directory has been deleted.';
error[8] = 'The files/directories have been deleted.';
error[9] = 'Enter a valid Directory name.';
error[10] = 'Internal Error: Cannot create the new directory';
error[11] = 'A directory with the same name is present.';
error[12] = 'The Parent Directory is not writable.';
error[13] = 'The new Directory has been created.';

error[14] = 'User Error: The File was not uploaded.';
/*error[15] = 'The Name of file is not valid. Please rename the file.';*/
error[15] = 'File upload not successful as -'
            +'\n'+' Either'+'\n'+'  The size of the file you are trying to upload is more than 1 MB (1024 KB)'
            +'[Maximum allowed file size is 1 MB (1024 KB). Please resize your image size with any image editing'
            +' software and then upload again]'
            +'\n'+'OR'+'\n'+'  The file extension is not among the allowed type'
            +'[Allowed files types - bmp, gif, jpe, jpg, jpeg, png]';
error[16] = 'A file with the same name is present.';
/*error[17] = 'The file exceeds the allowed limit of filesize.';*/
error[17] = 'Size of the file you are trying to upload is more than 1 MB (1024 KB).'
            +'\n'+' Maximum allowed file size is 1 MB (1024 KB).'
            +'\n'+'  Please resize your image size with any image editing software and then upload again.';
error[18] = 'The file has been uploaded.';
error[19] = 'Internal Error: The file was not uploaded.';
/*error[20] = 'The file type is not allowed to be uploaded.';*/
error[20] = 'The file extension is not among the allowed type [Allowed files types - bmp, gif, jpe, jpg, jpeg, png]';

error[100] = 'OK';
