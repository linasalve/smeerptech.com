// Define the command.

var FCKEditor = function( name )
{
	this.Name = name ;
	this.EditMode = FCK.EditMode;
}

FCKEditor.prototype.Execute = function()
{
	switch ( this.Name )
	{		
		case 'PageBreak' :
			var oPageBreak = FCK.InsertHtml('#MyPageBreak#') ;
			break;
		default :
	}
}

FCKEditor.prototype.GetState = function()
{
	return FCK_TRISTATE_OFF ;
}

// Register the commands.
FCKCommands.RegisterCommand( 'SMEERPPageBreak', new FCKEditor( 'PageBreak' ) ) ;

var fckItem = new FCKToolbarButton( 'SMEERPPageBreak', 'PageBreak' ) ;
fckItem.IconPath = FCKConfig.PluginsPath + 'smeerpbreak/pagebreak.gif';
FCKToolbarItems.RegisterItem( 'SMEERPPageBreak', fckItem ) ;
