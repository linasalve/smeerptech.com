//AJAX NAVIGATION FUNCTION BOF
function changeRecordList(page,pagename,form1){  
	 
	//var poststr = createQueryString(form1);	
	//var poststr =Url.decode($(form1).serialize()) ;
	st_options = "";
	/* var slist = document.getElementById('sStatus');  
	for(var i = 0; i < slist.options.length; ++i){
		comma = ",";
		if(slist.options[i].selected){
			st_options = st_options + slist[i].value + comma;
		}
	}
	st_options = trim(st_options,","); */
	
	var tslist = document.getElementById('tStatus'); 
	tst_options = "";
	for(var i = 0; i < tslist.options.length; ++i){
		comma = ",";
		if(tslist.options[i].selected){
			tst_options = tst_options + tslist[i].value + comma;
		}
	}
	tst_options = trim(tst_options,",");
	var gslist = document.getElementById('sGrade'); 
	gst_options = "";
	for(var i = 0; i < gslist.options.length; ++i){
		comma = ",";
		if(gslist.options[i].selected){
			gst_options = gst_options + gslist[i].value + comma;
		}
	}
	gst_options = trim(gst_options,","); 
	
	var tlist = document.getElementById('tType'); 
	t_options = "";
	for(var i = 0; i < tlist.options.length; ++i){
		comma = ",";
		if(tlist.options[i].selected){
			t_options = t_options + tlist[i].value + comma;
		}
	}
	t_options = trim(t_options,","); 
	
	var chk_status=chk_t_status=chk_date_from=chk_date_to=chk_assign=chk_grade=chk_type=is_classified='';
	/* if(document.getElementById("chk_status").checked){
		chk_status = 'AND' ;
	} */
	if(document.getElementById("chk_t_status").checked ){
		chk_t_status = 'AND' ;
	}
	if(document.getElementById("chk_date_from").checked){
		chk_date_from = 'AND' ;
	}
	if(document.getElementById("chk_date_to").checked ){
		chk_date_to = 'AND' ;
	}
	if(document.getElementById("chk_assign").checked ){
		chk_assign = 'AND' ;
	}
	if(document.getElementById("chk_grade").checked ){
		chk_grade = 'AND' ;
	}
	if(document.getElementById("chk_type").checked ){
		chk_type = 'AND' ;
	}  
	//Check the permission to view can_view_classified
	var can_view_classified = document.getElementById('can_view_classified').value; 
	if(can_view_classified==1){
		if(document.getElementById("is_classified").checked){
			is_classified = 1 ;
		}
	}else{
		is_classified = 0 ;
	}
	poststr = "sString="+document.getElementById("sString").value+"&sType="+document.getElementById("sType").value+
	"&sOrderBy="+document.getElementById("sOrderBy").value+"&sOrder="+document.getElementById("sOrder").value+
	"&chk_status="+chk_status+"&sStatus="+st_options+"&chk_t_status="+chk_t_status+"&tStatus="+tst_options+"&chk_grade="+chk_grade+"&sGrade="+gst_options+"&chk_type="+chk_type+"&tType="+t_options+"&chk_date_from="+document.getElementById("chk_date_from").value+"&date_from="+document.getElementById("date_from").value+"&date_to="+document.getElementById("date_to").value+"&chk_date_to="+chk_date_to+"&dt_field="+document.getElementById("dt_field").value+"&chk_assign="+chk_assign+"&search_client="+document.getElementById("search_client").value+"&assign="+document.getElementById("assign").value+"&is_classified="+is_classified+"&perform="+document.getElementById("perform").value+"&rpp="+document.getElementById("rpp").value ;
	var uri = pagename+"?ajx=1&x="+page+"&"+poststr ; 
	
	
	 
	 var element =  document.getElementById('progress');
	  if (typeof(element) != 'undefined' && element != null)
	  { 
		// alert('exist');
	  }else{
		 //alert('notexist');
	  }
	$("#progress").show();
	$("#loader").show();
	/* $('#inner_content').load(uri, function() {
		$("#progress").hide();
		$("#loader").hide();
	 }); */
	 
	 $( "#inner_content" ).load( uri, function( response, status, xhr ) {
		$("#progress").hide();
		$("#loader").hide();
		 
		  if ( status == "error" ) {
			var msg = "Sorry but there was an error: ";
			alert( msg + xhr.status + " " + xhr.statusText );
		  }
	});
} 

function loadRecordList(){
	if(http_req_alerts.readyState==4){
        if (http_req_alerts.status == 200){
            var allData = http_req_alerts.responseText;
            if(allData){
			    document.getElementById("record_info").innerHTML = allData; 
            }
        }
    }else{
		document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';         
    }
} 
//AJAX PAGINATION FUNCTION EOF
function clearSearch(){
	document.getElementById("sString").value='' ;
	document.getElementById("sType").value='' ;
	document.getElementById("sOrderBy").value='do_add' ;
	document.getElementById("sOrder").value='DESC' ;
	document.getElementById("date_from").value='' ;
	document.getElementById("date_to").value='' ;	 
}

function searchRecord(pagename,form1){
	changeRecordList(1,pagename,form1);
	document.getElementById("pageno").value=1 ;	
	return false;
}  
function editStatus(surl){
	ajxUrl= surl +"&ajx=1" ;
	var del=reallyDel('Are you sure you want to change the Status?'); 
	if(del==true){ 
		$( "#dialog-form").dialog({
				autoOpen: false,
				height: 700,
				width: 1015,
				resizable: true,
				closeText: 'close',
				title: 'SMEERP - NERVE CENTER',
				modal: true,
				close: function() {
					searchRecord(ajxUrl,frmSearch);
					//changeRecordList(pageno,'prospects.php',frmSearch);
				}
		});
		
		$("#dialog-form").dialog("open");
		var diFrame =  document.getElementById('dialogIframeId');
		if (navigator.appName.indexOf("Microsoft")!=-1){
			diFrameBody = document.frames['dialogIframeId'].document;
		}else{
		    diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
		}
		diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
		$("#dialogIframeId").attr("src",ajxUrl); 
	}
	return false;
}

function changeType(surl){
	ajxUrl= surl +"&ajx=1" ;
	var del=reallyDel('Are you sure you want to change the Type of Ticket?'); 
	if(del==true){ 
		$( "#dialog-form").dialog({
				autoOpen: false,
				height: 700,
				width: 1015,
				resizable: true,
				closeText: 'close',
				title: 'SMEERP - NERVE CENTER',
				modal: true,
				close: function() {
					//searchRecord('client-doc-upload.php',frmSearch);
					searchRecord(ajxUrl,frmSearch);
					//changeRecordList(pageno,'prospects.php',frmSearch);
					//$("#dialog-form" ).dialog( "destroy" );
				}
		});
		
		$("#dialog-form").dialog("open");
		var diFrame =  document.getElementById('dialogIframeId');
		if (navigator.appName.indexOf("Microsoft")!=-1){
			diFrameBody = document.frames['dialogIframeId'].document;
		}else{
		    diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
		}
		diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
		$("#dialogIframeId").attr("src",ajxUrl); 
	}
	return false;
}

function addRecord(surl){
	ajxUrl= surl +"&ajx=1" ;
	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 700,
		width: 1015,
		resizable: true,
		closeText: 'close',
		title: 'SMEERP - NERVE CENTER',
		modal: true,
		close: function() {		
			var pageno = document.getElementById("pageno").value ;
			 
			url = SITE_URL+"/"+ADMIN_PANEL+"/client-doc-upload.php?perform=list&pageno="+pageno ;
			//changeRecordList(pageno,'client-doc-upload.php',frmSearch);
			changeRecordList(pageno,ajxUrl,frmSearch); 
		}
	}); 
    $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;
	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
    $("#dialogIframeId").attr("src",ajxUrl);
    return false; 
}
 
function listRecord(surl){ 
	 
	ajxUrl= surl +"&ajx=1" ;
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';
	$('#inner_content').load(ajxUrl);
	   return false; 
}
/*AJAX NAVIGATION EOF*/ 

function markClientVisibility(ele,alt,divname1,divname2){

    var el = document.getElementsByName(alt);
    if(ele.checked){
        document.getElementById(divname1).style.display ='block';
        document.getElementById(divname2).style.display ='block';
        el[0].checked=true ;
    }else{
        document.getElementById(divname1).style.display ='none';
        document.getElementById(divname2).style.display ='none';
        el[0].checked=false ;
    }
}

function addClientInfo(client_arr) {
    div_client = document.getElementById("client_info");

    client_detail_div = document.createElement('div');
    client_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'client');
        client_id_input.setAttribute('value', client_arr[0]);

        client_det_input = document.createElement('input');
        client_det_input.setAttribute('type', 'text');
        client_det_input.setAttribute('name', 'client_details');
        client_det_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +") ("+ client_arr[4] +")");
        client_det_input.setAttribute('size', '60');
        client_det_input.setAttribute('readOnly', '1');        
        client_det_input.setAttribute('class', 'inputbox_c');

        client_detail_div.appendChild(client_id_input);
        client_detail_div.appendChild(client_det_input);

    div_client.innerHTML = '<span class="txthelp">The Client for whom the support ticket is created.</span>';
    div_client.appendChild(client_detail_div);
    return (true);
}


function addExecutive(executive_arr,div) {

    div_executive = document.getElementById(div);
	
        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', 'remove_'+ executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'assign_members[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];
 
        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'assign_members_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ executive_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeExecutive('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}


/**
 * This function is used to un-relate a Executive from a Customer.
 *
 */
function removeExecutive(id) {
    div_executive = document.getElementById("executive_info"); 
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
} 
 
//Function to update the Banner status using AJAX BOF
/*
var http_req_alerts_banner;
function closeStatus( id,status){

	http_req_alerts_banner = false;	
	document.getElementById("stid").value = id;

	
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    } 
   
    var uri = "get-support-ticket-close-status.php?id="+id+"&status="+status;    
    http_req_alerts_banner.onreadystatechange = loadCloseStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);
}
function loadCloseStatus(){
    id = document.getElementById("stid").value ;
	if(http_req_alerts_banner.readyState==4){

        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
		 
            if(allData){            
            
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataId = dataSplit[1]; 
				 
				highlightid = 'row'+dataId ;
				closeid = 'close'+dataId ;
				tstatusid = 'tstatus'+dataId ;
				 
                document.getElementById("message").innerHTML= dataMsg;  
                document.getElementById(closeid).innerHTML = " ";  
                document.getElementById(tstatusid).innerHTML = "Close";  
				
				if(dataId!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
    }
}*/
//Function to update the Banner status using AJAX BOF