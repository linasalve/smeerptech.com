function toggleFollowup(id) {
	if ( id=='1' ) {	
		document.getElementById(id).className = "visible";
		document.getElementById('submitType').value = "1";
		document.getElementById('2').className = "hidden";
		document.getElementById('3').className = "hidden";
	}
	if ( id=='2' ) {	
		document.getElementById(id).className = "visible";
        document.getElementById('submitType').value = "2";
		document.getElementById('1').className = "hidden";
		document.getElementById('3').className = "hidden";
	}
	if ( id=='3' ) {	
		document.getElementById(id).className = "visible";
        document.getElementById('submitType').value = "3";
		document.getElementById('1').className = "hidden";
		document.getElementById('2').className = "hidden";
	}
}
/**
 * This function is used to select and relate the Clients 
 * task reminder.
 *
 */
function addClientInfo(client_arr) {
    
    div_client = document.getElementById("client_info");

        client_div = document.createElement('div');
        client_div.setAttribute('class', 'row');
        client_div.setAttribute('id', 'remove_'+ client_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'alloted_clients[]');
        client_id_input.setAttribute('value', client_arr[0]);
        
        client_email_input = document.createElement('input');
        client_email_input.setAttribute('type', 'hidden');
        client_email_input.setAttribute('name', 'client_email[]');
        client_email_input.setAttribute('value', client_arr[4]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ client_arr[1] +")&nbsp;"+ client_arr[2] +"&nbsp;"+ client_arr[3] +"&nbsp;"+ client_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'client_details[]');
        //exec_client_input.setAttribute('value', "("+ client_arr[1] +") "+ client_arr[2] +" "+ client_arr[3] +" "+ client_arr[4]);
        exec_client_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ client_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ client_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ client_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeClient('remove_'+ client_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        client_div.appendChild(client_id_input);
        client_div.appendChild(client_email_input);
        //client_div.appendChild(exec_span);
        client_div.appendChild(exec_client_input);
        client_div.appendChild(exec_remove_a);

    div_client.appendChild(client_div);
    
    return (true);
}

/**
 * This function is used to un-relate a Client from a Task Reminder entry
 *
 */
function removeClient(id) {
    div_client = document.getElementById("client_info");

    if ( div_client.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}



/**
 * This function is used to add new elements
 * to the existing HTML elements.
 * This particular function adds a DIV tag to the parent 
 * element identified by 'element'.
 *
 * @param   HTML Element    this can be reference to any HTML element eg. DIV, TABLE, TD, P.
 * 
 *
 */
function addPhoneField(element, p_type, cc) {
    
	id = Math.random();
    // Create the main container Div
    phone_div = document.createElement('div');
    phone_div.setAttribute('class', 'row pt5 wp100');
	phone_div.setAttribute('className', 'row pt5 wp100');
    
        // Create the Phone Type Div.
        // <div class="coloumn pr10 w100">
        p_type_div = document.createElement('div');
        p_type_div.setAttribute('class', 'coloumn pr10 w100');
		p_type_div.setAttribute('className', 'coloumn pr10 w100');
    
            // Create the HIDDEN phone_id field.
            // <input type="hidden" name="phone_id[]" value="" />
            p_id_input = document.createElement('input');
            p_id_input.setAttribute('type', 'hidden');
            p_id_input.setAttribute('name', 'phone_id[]');
            p_id_input.setAttribute('value', '');
            
            // Create the HIDDEN p_is_verified field.
            // <input type="hidden" name="p_is_verified[]" value="0" />
            p_is_verified_input = document.createElement('input');
            p_is_verified_input.setAttribute('type', 'hidden');
            p_is_verified_input.setAttribute('name', 'p_is_verified[]');
            p_is_verified_input.setAttribute('value', '0');
            
            // Create the SELECT p_type field.
            // <select name="p_type[]" class="inputbox w100" >
            p_type_select = document.createElement('select');
            p_type_select.setAttribute('name', 'p_type[]');
            p_type_select.setAttribute('class', 'inputbox w100');
			p_type_select.setAttribute('className', 'inputbox w100');
    
            // Create the OPTIONS.
            // <option value="'+ p_type[0][i] +'">'+ p_type[1][i] +'</option>
            for (i=0;i<p_type[0].length;i++) {
                p_type_select.options[i] = new Option(p_type[1][i], p_type[0][i]);
            }
    
            // Append all these elements to the Phone Type Div.
            p_type_div.appendChild(p_id_input);
            p_type_div.appendChild(p_is_verified_input);
            p_type_div.appendChild(p_type_select);
    
    
        // Create the Country Code Div.
        // <div class="coloumn pr10 w50 center">
        cc_div = document.createElement('div');
        cc_div.setAttribute('class', 'coloumn pr10 w50 center');
		cc_div.setAttribute('className', 'coloumn pr10 w50 center');
        
            // Create the INPUT TEXT cc field.
            // <input type="text" name="cc[]" value="" class="inputbox" maxlength="3" size="4"/>
            /*cc_input = document.createElement('input');
            cc_input.setAttribute('type', 'text');
            cc_input.setAttribute('name', 'cc[]');
            cc_input.setAttribute('value', '');
            cc_input.setAttribute('class', 'inputbox');
			cc_input.setAttribute('className', 'inputbox');
            cc_input.setAttribute('maxlength', '3');
            cc_input.setAttribute('size', '4');
            // Append the INPUT field to the DIV.
            cc_div.appendChild(cc_input);*/
            
            cc_select = document.createElement('select');
            cc_select.setAttribute('name', 'cc[]');
            cc_select.setAttribute('class', 'inputbox w30');
			cc_select.setAttribute('className', 'inputbox w30');
    
            // Create the OPTIONS.
            // <option value="'+ p_type[0][i] +'">'+ p_type[1][i] +'</option>
            for (i=0;i<cc[0].length;i++) {
                cc_select.options[i] = new Option(cc[1][i], cc[0][i]);
                if ( cc[0][i] == '91') {
                    cc_select.options[i].selected = true;
                }
            }
            
            // Append all these elements to the Phone Type Div.
            cc_div.appendChild(cc_select);
           
    
        // Create the Area Code Div.
        // <div class="coloumn pr10 w50 center">
        ac_div = document.createElement('div');
        ac_div.setAttribute('class', 'coloumn pr10 w50 center');
		ac_div.setAttribute('className', 'coloumn pr10 w50 center');
    
            // Create the INPUT TEXT ac field.
            // <input type="text" name="ac[]" value="" class="inputbox" maxlength="5" size="6"/>
            ac_input = document.createElement('input');
            ac_input.setAttribute('type', 'text');
            ac_input.setAttribute('name', 'ac[]');
            ac_input.setAttribute('value', '');
            ac_input.setAttribute('class', 'inputbox');
			ac_input.setAttribute('className', 'inputbox');
            ac_input.setAttribute('maxlength', '5');
            ac_input.setAttribute('size', '6');
            // Append the INPUT field to the DIV.
            ac_div.appendChild(ac_input);
    
    
        // Create the Number Div.
        // <div class="coloumn pr10 w100 center">
        p_number_div = document.createElement('div');
        p_number_div.setAttribute('class', 'coloumn pr10 w100 center');
		p_number_div.setAttribute('className', 'coloumn pr10 w100 center');
        
            // Create the INPUT TEXT p_number field.
            // <input type="text" name="p_number[]" value="" class="inputbox" maxlength="10" size="15"/>
            p_number_input = document.createElement('input');
            p_number_input.setAttribute('type', 'text');
            p_number_input.setAttribute('name', 'p_number[]');
            p_number_input.setAttribute('value', '');
            p_number_input.setAttribute('class', 'inputbox');
			p_number_input.setAttribute('className', 'inputbox');
            p_number_input.setAttribute('maxlength', '10');
            p_number_input.setAttribute('size', '15');
            // Append the INPUT field to the DIV.
            p_number_div.appendChild(p_number_input);
    
    
        // Create the Is Preferred Div.
        // <div class="coloumn pl10 w50">
        p_is_preferred_div = document.createElement('div');
        p_is_preferred_div.setAttribute('class', 'coloumn pl10 w50');
		p_is_preferred_div.setAttribute('className', 'coloumn pl10 w50');
        
            // Create the INPUT HIDDEN p_is_preferred field.
            // <input type="hidden" name="p_is_preferred[]" value="0" />
            p_is_preferred_input_hidden = document.createElement('input');
            p_is_preferred_input_hidden.setAttribute('type', 'hidden');
            p_is_preferred_input_hidden.setAttribute('name', 'p_is_preferred[]');
            p_is_preferred_input_hidden.setAttribute('value', '0');
            // Create the INPUT RADIO p_is_preferred_radio field.
            // <input type="radio" name="p_is_preferred_radio[]" value="0" onclick="javascript: return changeSelection(this, 'p_is_preferred');" />
            p_is_preferred_input_radio = document.createElement('input');
            p_is_preferred_input_radio.setAttribute('type', 'radio');
            p_is_preferred_input_radio.setAttribute('name', 'p_is_preferred_radio[]');
            p_is_preferred_input_radio.setAttribute('value', '0');
            p_is_preferred_input_radio.setAttribute('onclick', "javascript: return changeSelection(this, 'p_is_preferred');");
            // Append the INPUT fields to the DIV.
            p_is_preferred_div.appendChild(p_is_preferred_input_hidden);
            p_is_preferred_div.appendChild(p_is_preferred_input_radio);
    
        // Create the p_remove Div.
        // <div class="coloumn pl10 w50">
        p_remove_div = document.createElement('div');
        p_remove_div.setAttribute('class', 'coloumn pl10');
		p_remove_div.setAttribute('className', 'coloumn pl10');
        p_remove_div.innerHTML = '<span class="txthelp">To remove, leave the number fields empty.</span>';

    
    // Attach all the DIVs to the main container DIV.
    phone_div.appendChild(p_type_div);
    phone_div.appendChild(cc_div);
    phone_div.appendChild(ac_div);
    phone_div.appendChild(p_number_div);
    phone_div.appendChild(p_is_preferred_div);
    phone_div.appendChild(p_remove_div);
    
    element.appendChild(phone_div);
    return (true);
}


function addAddressField(element, a_type, a_country , a_city, a_state) {
    
    id = Math.random();
    
    // Create the main container Div
    address_div = document.createElement('div');
    address_div.setAttribute('id', id);
    address_div.setAttribute('class', 'row pb5 wp100');
	address_div.setAttribute('className', 'row pb5 wp100');

        // Create the 1st coloumn DIV.
        c1_div = document.createElement('div');
        c1_div.setAttribute('class', 'coloumn pt5 wp40');
		c1_div.setAttribute('className', 'coloumn pt5 wp40');
            
            // Create the 1st coloumn 1st Row DIV.
            c1r1_div = document.createElement('div');
            c1r1_div.setAttribute('class', 'row pb5 wp100');
			c1r1_div.setAttribute('className', 'row pb5 wp100');
                // Create the 1st coloumn 1st Row 1st coloumn DIV.
                c1r1c1_div = document.createElement('div');
                c1r1c1_div.setAttribute('class', 'coloumn pr10 w60');
				c1r1c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c1r1c1_div.innerHTML = 'Preferred';
                // Create the 1st coloumn 1st Row 2nd coloumn DIV.
                c1r1c2_div = document.createElement('div');
                c1r1c2_div.setAttribute('class', 'coloumn pr10');
				c1r1c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the INPUT HIDDEN for,
                    // <input type="hidden" name="is_verified[]" value="0"/>
                    a_is_verified_input = document.createElement('input');
                    a_is_verified_input.setAttribute('type', 'hidden');
                    a_is_verified_input.setAttribute('name', 'is_verified[]');
                    a_is_verified_input.setAttribute('value', '0');
                    /*
                    // Create the INPUT RADIO for,
                    // <input type="radio" name="is_preferred[]" value="" />
                    a_is_preferred_input = document.createElement('input');
                    a_is_preferred_input.setAttribute('type', 'radio');
                    a_is_preferred_input.setAttribute('name', 'is_preferred[]');
                    a_is_preferred_input.setAttribute('value', '1');
                    */
                    
                    // Create the INPUT HIDDEN is_preferred field.
                    // <input type="hidden" name="is_preferred[]" value="0" />
                    a_is_preferred_input_hidden = document.createElement('input');
                    a_is_preferred_input_hidden.setAttribute('type', 'hidden');
                    a_is_preferred_input_hidden.setAttribute('name', 'is_preferred[]');
                    a_is_preferred_input_hidden.setAttribute('value', '0');
                    // Create the INPUT RADIO is_preferred_radio field.
                    // <input type="radio" name="is_preferred_radio[]" value="0" onclick="javascript: return changeSelection(this, 'is_preferred');" />
                    a_is_preferred_input_radio = document.createElement('input');
                    a_is_preferred_input_radio.setAttribute('type', 'radio');
                    a_is_preferred_input_radio.setAttribute('name', 'is_preferred_radio[]');
                    a_is_preferred_input_radio.setAttribute('value', '0');
                    a_is_preferred_input_radio.setAttribute('onclick', "javascript: return changeSelection(this, 'is_preferred');");
                    
                    // Create the ANCHOR for
                    // <a href="javascript:void(0);" onclick="javascript:removeAddress(parent, this)">Remove</a>
                    a_remove_anchor = document.createElement('a');
                    a_remove_anchor.setAttribute('class', 'pl25');
					a_remove_anchor.setAttribute('className', 'pl25');
                    a_remove_anchor.setAttribute('href', 'javascript:void(0);');
                    a_remove_anchor.setAttribute('onclick', "javascript:removeElement(document.getElementById('addresses'), document.getElementById('"+ id +"'));");
//a_remove_anchor["onclick"] = new Function("javascript:removeElement(document.getElementById('addresses'), document.getElementById('"+ id +"'));");
                    a_remove_anchor.innerHTML = 'Remove';
                // Append the INPUT to C1 R1 C2 DIV.
                c1r1c2_div.appendChild(a_is_verified_input);
                c1r1c2_div.appendChild(a_is_preferred_input_hidden);
                c1r1c2_div.appendChild(a_is_preferred_input_radio);
                c1r1c2_div.appendChild(a_remove_anchor);
            // Append the Coloumns to the C1 R1 DIV
            c1r1_div.appendChild(c1r1c1_div);
            c1r1_div.appendChild(c1r1c2_div);
            
            // Create the 1st coloumn 2nd Row DIV.
            c1r2_div = document.createElement('div');
            c1r2_div.setAttribute('class', 'row pb2 wp100');
			c1r2_div.setAttribute('className', 'row pb2 wp100');
                // Create the 1st coloumn 2nd Row 1st coloumn DIV.
                c1r2c1_div = document.createElement('div');
                c1r2c1_div.setAttribute('class', 'coloumn pr10 w60');
				c1r2c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c1r2c1_div.innerHTML = 'Type';
                // Create the 1st coloumn 2nd Row 2nd coloumn DIV.
                c1r2c2_div = document.createElement('div');
                c1r2c2_div.setAttribute('class', 'coloumn pr10');
				c1r2c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the SELECT address_type field.
                    // <select name="address_type[]" class="inputbox w100" >
                    a_type_select = document.createElement('select');
                    a_type_select.setAttribute('name', 'address_type[]');
                    a_type_select.setAttribute('class', 'inputbox w100');
					a_type_select.setAttribute('className', 'inputbox w100');
                    // Create the OPTIONS.
                    // <option value="'+ a_type[0][i] +'">'+ a_type[1][i] +'</option>
                    for (i=0;i<a_type[0].length;i++) {
                        a_type_select.options[i] = new Option(a_type[1][i], a_type[0][i]);
                    }
                // Append the SELECT to C1 R2 C2 DIV.
                c1r2c2_div.appendChild(a_type_select);
            // Append the Coloumns to the C1 R2 DIV
            c1r2_div.appendChild(c1r2c1_div);
            c1r2_div.appendChild(c1r2c2_div);
            
            // Create the 1st coloumn 3rd Row DIV.
            c1r3_div = document.createElement('div');
            c1r3_div.setAttribute('class', 'row pb2 wp100');
			c1r3_div.setAttribute('className', 'row pb2 wp100');
                // Create the 1st coloumn 3rd Row 1st coloumn DIV.
                c1r3c1_div = document.createElement('div');
                c1r3c1_div.setAttribute('class', 'coloumn pr10 w60');
				c1r3c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c1r3c1_div.innerHTML = '<span class="star">*</span> Address';
                // Create the 1st coloumn 3rd Row 2nd coloumn DIV.
                c1r3c2_div = document.createElement('div');
                c1r3c2_div.setAttribute('class', 'coloumn pr10');
				c1r3c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the TEXTAREA address_type field.
                    // <textarea name="address[]" class="inputbox" rows="3" cols="20"></textarea>
                    address_textarea = document.createElement('textarea');
                    address_textarea.setAttribute('name', 'address[]');
                    address_textarea.setAttribute('class', 'inputbox');
					address_textarea.setAttribute('className', 'inputbox');
                    address_textarea.setAttribute('rows', '3');
                    address_textarea.setAttribute('cols', '20');
                // Append the TEXTAREA to C1 R3 C2 DIV.
                c1r3c2_div.appendChild(address_textarea);
            // Append the Coloumns to the C1 R3 DIV.
            c1r3_div.appendChild(c1r3c1_div);
            c1r3_div.appendChild(c1r3c2_div);
        
        // Append the Rows to the Coloumn C1.
        c1_div.appendChild(c1r1_div);
        c1_div.appendChild(c1r2_div);
        c1_div.appendChild(c1r3_div);
            
        // Create the 2nd coloumn DIV.
        c2_div = document.createElement('div');
        c2_div.setAttribute('class', 'coloumn pt5 wp60');
		c2_div.setAttribute('className', 'coloumn pt5 wp60');
            //---
             // Create the 2nd coloumn 0th Row DIV.
            c2r0_div = document.createElement('div');
            c2r0_div.setAttribute('class', 'row pt5 wp100');
			c2r0_div.setAttribute('className', 'row pt5 wp100'); 
                
                // Create the 2nd coloumn 0st Row 1st coloumn DIV.
                /*
                c2r0c1_div = document.createElement('div');
                c2r0c1_div.setAttribute('class', 'coloumn pr10 w60');
				c2r0c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c2r0c1_div.innerHTML = '<span class="star">*</span>C/O';
                // Create the 2nd coloumn 1st Row 2nd coloumn DIV.
                c2r0c2_div = document.createElement('div');
                c2r0c2_div.setAttribute('class', 'coloumn pr10');
				c2r0c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the INPUT TEXT for,
                    // <input type="text" name="city[]" value="" class="inputbox" maxlength="100" size="32"/>
                    
                    a_comp_name_input = document.createElement('input');
                    a_comp_name_input.setAttribute('type', 'text');
                    a_comp_name_input.setAttribute('name', 'company_name[]');
                    a_comp_name_input.setAttribute('value', '');
                    a_comp_name_input.setAttribute('class', 'inputbox');
					a_comp_name_input.setAttribute('className', 'inputbox');
                    a_comp_name_input.setAttribute('maxlength', '255');
                    a_comp_name_input.setAttribute('size', '32');
                    // Create the SPAN for,
                    // <span class="txthelp">(100 chars max).</span>
                    a_comp_name_span = document.createElement('span');
                    a_comp_name_span.setAttribute('class', 'txthelp');
					a_comp_name_span.setAttribute('className', 'txthelp');
                    a_comp_name_span.innerHTML = ' (255 chars max).';
                    
                // Append to C2 R1 C2 DIV.
                c2r0c2_div.appendChild(a_comp_name_input);
                c2r0c2_div.appendChild(a_comp_name_span);
            // Append the Coloumns to the C2 R1 DIV.
            c2r0_div.appendChild(c2r0c1_div);
            c2r0_div.appendChild(c2r0c2_div);
            */
            //---
        
        
            // Create the 2nd coloumn 1st Row DIV.
            /* Not in used
            c2r1_div = document.createElement('div');
            c2r1_div.setAttribute('class', 'row pt5 wp100');
			c2r1_div.setAttribute('className', 'row pt5 wp100'); 
                
                // Create the 2nd coloumn 1st Row 1st coloumn DIV.
                c2r1c1_div = document.createElement('div');
                c2r1c1_div.setAttribute('class', 'coloumn pr10 w60');
				c2r1c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c2r1c1_div.innerHTML = '<span class="star">*</span>City';
                // Create the 2nd coloumn 1st Row 2nd coloumn DIV.
                c2r1c2_div = document.createElement('div');
                c2r1c2_div.setAttribute('class', 'coloumn pr10');
				c2r1c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the INPUT TEXT for,
                    // <input type="text" name="city[]" value="" class="inputbox" maxlength="100" size="32"/>
                    a_city_input = document.createElement('input');
                    a_city_input.setAttribute('type', 'text');
                    a_city_input.setAttribute('name', 'city[]');
                    a_city_input.setAttribute('value', '');
                    a_city_input.setAttribute('class', 'inputbox');
					a_city_input.setAttribute('className', 'inputbox');
                    a_city_input.setAttribute('maxlength', '100');
                    a_city_input.setAttribute('size', '32');
                    // Create the SPAN for,
                    // <span class="txthelp">(100 chars max).</span>
                    a_city_span = document.createElement('span');
                    a_city_span.setAttribute('class', 'txthelp');
					a_city_span.setAttribute('className', 'txthelp');
                    a_city_span.innerHTML = ' (100 chars max).';
                // Append to C2 R1 C2 DIV.
                c2r1c2_div.appendChild(a_city_input);
                c2r1c2_div.appendChild(a_city_span);
            // Append the Coloumns to the C2 R1 DIV.
            c2r1_div.appendChild(c2r1c1_div);
            c2r1_div.appendChild(c2r1c2_div);*/
            
            // Create the 2nd coloumn 4th Row DIV.
            c2r1_div = document.createElement('div');
            c2r1_div.setAttribute('class', 'row pt5 wp100');
			c2r1_div.setAttribute('className', 'row pt5 wp100');
                // Create the 2nd coloumn 4th Row 1st coloumn DIV.
                c2r1c1_div = document.createElement('div');
                c2r1c1_div.setAttribute('class', 'coloumn pr10 w60');
				c2r1c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c2r1c1_div.innerHTML = '<span class="star">*</span>City';
                // Create the 2nd coloumn 4th Row 2nd coloumn DIV.
                c2r1c2_div = document.createElement('div');
                c2r1c2_div.setAttribute('class', 'coloumn pr10');
				c2r1c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the SELECT address_type field.
                    // <select name="country[]" class="inputbox" >
                    a_city_select = document.createElement('select');
                    a_city_select.setAttribute('name', 'city[]');
                    a_city_select.setAttribute('class', 'inputbox');
					a_city_select.setAttribute('className', 'inputbox');
                    a_city_select.setAttribute('style', 'width:260px;');
                    // Create the OPTIONS.
                    // <option value="'+ a_country[0][i] +'">'+ a_country[1][i] +'</option>
                    /*a_city_select.options[0] = new Option("Select City");*/
                    for (i=0;i<a_city[0].length;i++) {
                        a_city_select.options[i] = new Option(a_city[1][i], a_city[0][i]);
                        /*if ( a_country[0][i] == '91') {
                            a_country_select.options[i].selected = true;
                        }*/
                    }
                // Append to C2 R4 C2 DIV.
                c2r1c2_div.appendChild(a_city_select);
            // Append the Coloumns to the C2 R4 DIV.
            c2r1_div.appendChild(c2r1c1_div);
            c2r1_div.appendChild(c2r1c2_div);
            
            // Create the 2nd coloumn 4th Row DIV.
            c2r2_div = document.createElement('div');
            c2r2_div.setAttribute('class', 'row pt5 wp100');
			c2r2_div.setAttribute('className', 'row pt5 wp100');
                // Create the 2nd coloumn 4th Row 1st coloumn DIV.
                c2r2c1_div = document.createElement('div');
                c2r2c1_div.setAttribute('class', 'coloumn pr10 w60');
				c2r2c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c2r2c1_div.innerHTML = '<span class="star">*</span>State';
                // Create the 2nd coloumn 4th Row 2nd coloumn DIV.
                c2r2c2_div = document.createElement('div');
                c2r2c2_div.setAttribute('class', 'coloumn pr10');
				c2r2c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the SELECT address_type field.
                    // <select name="country[]" class="inputbox" >
                    a_state_select = document.createElement('select');
                    a_state_select.setAttribute('name', 'state[]');
                    a_state_select.setAttribute('class', 'inputbox');
					a_state_select.setAttribute('className', 'inputbox');
                    a_state_select.setAttribute('style', 'width:260px;');
                    // Create the OPTIONS.
                    // <option value="'+ a_country[0][i] +'">'+ a_country[1][i] +'</option>
                    /*a_state_select.options[0] = new Option("Select State");*/
                    for (i=0;i<a_state[0].length;i++) {
                        a_state_select.options[i] = new Option(a_state[1][i], a_state[0][i]);
                        /*if ( a_country[0][i] == '91') {
                            a_country_select.options[i].selected = true;
                        }*/
                    }
                // Append to C2 R4 C2 DIV.
                c2r2c2_div.appendChild(a_state_select);
            // Append the Coloumns to the C2 R4 DIV.
            c2r2_div.appendChild(c2r2c1_div);
            c2r2_div.appendChild(c2r2c2_div);

            // Create the 2nd coloumn 2nd Row DIV.
            /*c2r2_div = document.createElement('div');
            c2r2_div.setAttribute('class', 'row pt5 wp100');
			c2r2_div.setAttribute('className', 'row pt5 wp100');
                // Create the 2nd coloumn 2nd Row 1st coloumn DIV.
                c2r2c1_div = document.createElement('div');
                c2r2c1_div.setAttribute('class', 'coloumn pr10 w60');
				c2r2c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c2r2c1_div.innerHTML = 'State';
                // Create the 2nd coloumn 2nd Row 2nd coloumn DIV.
                c2r2c2_div = document.createElement('div');
                c2r2c2_div.setAttribute('class', 'coloumn pr10');
				c2r2c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the INPUT TEXT for,
                    // <input type="text" name="state[]" value="" class="inputbox" maxlength="100" size="32"/>
                    a_state_input = document.createElement('input');
                    a_state_input.setAttribute('type', 'text');
                    a_state_input.setAttribute('name', 'state[]');
                    a_state_input.setAttribute('value', '');
                    a_state_input.setAttribute('class', 'inputbox');
					a_state_input.setAttribute('className', 'inputbox');
                    a_state_input.setAttribute('maxlength', '100');
                    a_state_input.setAttribute('size', '32');
                    // Create the SPAN for,
                    // <span class="txthelp">(100 chars max).</span>
                    a_state_span = document.createElement('span');
                    a_state_span.setAttribute('class', 'txthelp');
					a_state_span.setAttribute('className', 'txthelp');
                    a_state_span.innerHTML = ' (100 chars max).';
                // Append to C2 R2 C2 DIV.
                c2r2c2_div.appendChild(a_state_input);
                c2r2c2_div.appendChild(a_state_span);
            // Append the Coloumns to the C2 R2 DIV.
            c2r2_div.appendChild(c2r2c1_div);
            c2r2_div.appendChild(c2r2c2_div);*/
        
            // Create the 2nd coloumn 3rd Row DIV.
            c2r3_div = document.createElement('div');
            c2r3_div.setAttribute('class', 'row pt5 wp100');
			c2r3_div.setAttribute('className', 'row pt5 wp100');
                // Create the 2nd coloumn 3rd Row 1st coloumn DIV.
                c2r3c1_div = document.createElement('div');
                c2r3c1_div.setAttribute('class', 'coloumn pr10 w60');
				c2r3c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c2r3c1_div.innerHTML = 'Pin/Zip';
                // Create the 2nd coloumn 3rd Row 2nd coloumn DIV.
                c2r3c2_div = document.createElement('div');
                c2r3c2_div.setAttribute('class', 'coloumn pr10');
				c2r3c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the INPUT TEXT for,
                    // <input type="text" name="zipcode[]" value="" class="inputbox" maxlength="16" size="32"/>
                    a_pin_input = document.createElement('input');
                    a_pin_input.setAttribute('type', 'text');
                    a_pin_input.setAttribute('name', 'zipcode[]');
                    a_pin_input.setAttribute('value', '');
                    a_pin_input.setAttribute('class', 'inputbox');
					a_pin_input.setAttribute('className', 'inputbox');
                    a_pin_input.setAttribute('maxlength', '16');
                    a_pin_input.setAttribute('size', '32');
                    // Create the SPAN for,
                    // <span class="txthelp">(16 chars max).</span>
                    a_pin_span = document.createElement('span');
                    a_pin_span.setAttribute('class', 'txthelp');
					a_pin_span.setAttribute('className', 'txthelp');
                    a_pin_span.innerHTML = ' (16 chars max).';
                // Append to C2 R3 C2 DIV.
                c2r3c2_div.appendChild(a_pin_input);
                c2r3c2_div.appendChild(a_pin_span);
            // Append the Coloumns to the C2 R3 DIV.
            c2r3_div.appendChild(c2r3c1_div);
            c2r3_div.appendChild(c2r3c2_div);

            // Create the 2nd coloumn 4th Row DIV.
            c2r4_div = document.createElement('div');
            c2r4_div.setAttribute('class', 'row pt5 wp100');
			c2r4_div.setAttribute('className', 'row pt5 wp100');
                // Create the 2nd coloumn 4th Row 1st coloumn DIV.
                c2r4c1_div = document.createElement('div');
                c2r4c1_div.setAttribute('class', 'coloumn pr10 w60');
				c2r4c1_div.setAttribute('className', 'coloumn pr10 w60');
                    c2r4c1_div.innerHTML = '<span class="star">*</span>Country';
                // Create the 2nd coloumn 4th Row 2nd coloumn DIV.
                c2r4c2_div = document.createElement('div');
                c2r4c2_div.setAttribute('class', 'coloumn pr10');
				c2r4c2_div.setAttribute('className', 'coloumn pr10');
                    // Create the SELECT address_type field.
                    // <select name="country[]" class="inputbox" >
                    a_country_select = document.createElement('select');
                    a_country_select.setAttribute('name', 'country[]');
                    a_country_select.setAttribute('class', 'inputbox');
					a_country_select.setAttribute('className', 'inputbox');
                    // Create the OPTIONS.
                    // <option value="'+ a_country[0][i] +'">'+ a_country[1][i] +'</option>
                    for (i=0;i<a_country[0].length;i++) {
                        a_country_select.options[i] = new Option(a_country[1][i], a_country[0][i]);
                        if ( a_country[0][i] == '91') {
                            a_country_select.options[i].selected = true;
                        }
                    }
                // Append to C2 R4 C2 DIV.
                c2r4c2_div.appendChild(a_country_select);
            // Append the Coloumns to the C2 R4 DIV.
            c2r4_div.appendChild(c2r4c1_div);
            c2r4_div.appendChild(c2r4c2_div);
        
        // Append the Rows to the Coloumn C2.
        c2_div.appendChild(c2r0_div);
        c2_div.appendChild(c2r1_div);
        c2_div.appendChild(c2r2_div);
        c2_div.appendChild(c2r3_div);
        c2_div.appendChild(c2r4_div);
    
    // Append the Coloums to the main div.
    address_div.appendChild(c1_div);
    address_div.appendChild(c2_div);
    
    element.appendChild(address_div);
    return (true);
}

function addReminderField(element) {
    
    r_count = parseInt(document.getElementById("reminder_count").value);
    
    // Create the main container Div
    reminder_div = document.createElement('div');
    reminder_div.setAttribute('class', 'row pt5 wp100');
	reminder_div.setAttribute('className', 'row pt5 wp100');
    
        // Create the 1st coloumn DIV.
        c1_div = document.createElement('div');
        c1_div.setAttribute('class', 'coloumn pl10 pt5 w30');
		c1_div.setAttribute('className', 'coloumn pl10 pt5 w30');
            c1_div.innerHTML = "Reminder "+ (r_count+1);
        
        c2_div = document.createElement('div');
        c2_div.setAttribute('class', 'coloumn pl10 w200');
		c2_div.setAttribute('className', 'coloumn pl10 w200');
            // Create the SCRIPT tag
            r_script = document.createElement('script');
            r_script.setAttribute('language', 'javascript');
            r_script.setAttribute('type', 'text/javascript');
// This inner hTML is not working in IE.
			r_script.innerHTML = 'var c_remind_'+ r_count +' = new CalendarPopup();'
                                    + 'c_remind_'+ r_count +'.showNavigationDropdowns();'
                                    + 'c_remind_'+ r_count +'.setYearSelectStartOffset(+1);';
            // Create the INPUT TEXT. 
            r_input = document.createElement('input');
            r_input.setAttribute('type', 'text');
            r_input.setAttribute('name', 'remind_date_'+ r_count);
            r_input.setAttribute('value', '');
            r_input.setAttribute('class', 'inputbox');
			r_input.setAttribute('className', 'inputbox');
            r_input.setAttribute('size', '12');
            r_input.setAttribute('readonly', 'readonly');
            // Create the ANCHOR Tag to clear the date field.
            r_clear_a = document.createElement('a');
            r_clear_a.setAttribute('href', 'javascript:void(0);');
            r_clear_a.setAttribute('name', 'clear'+ r_count);
            r_clear_a.setAttribute('onclick', 'javascript:clearField(document.frmExecutiveEdit.remind_date_'+ r_count +');');
            r_clear_a.setAttribute('readonly', 'readonly');
                // Create the IMG tag.
                r_clear_img = document.createElement('img');
                r_clear_img.setAttribute('src', '../media/images/nc/off.gif');
                r_clear_img.setAttribute('class', 'pl2');
				r_clear_img.setAttribute('className', 'pl2');
                r_clear_img.setAttribute('border', '0');
                r_clear_img.setAttribute('title', 'Clear');
                r_clear_img.setAttribute('alt', 'clear');
            r_clear_a.appendChild(r_clear_img);
            //r_clear_a.innerHTML = 'Clear';
            // Create the ANCHOR Tag to show the calander.
            r_cal_a = document.createElement('a');
            r_cal_a.setAttribute('href', 'javascript:void(0);');
            r_cal_a.setAttribute('name', 'a_remind_date_'+ r_count);
            r_cal_a.setAttribute('id', 'a_remind_date_'+ r_count);
            r_cal_a.setAttribute('onclick', "c_remind_"+ r_count +".select(document.frmExecutiveEdit.remind_date_"+ r_count +", 'a_remind_date_"+ r_count +"','dd/MM/yyyy', 1); return false;");
            r_cal_a.setAttribute('readonly', 'readonly');
                // Create the IMG tag.
                r_cal_img = document.createElement('img');
                r_cal_img.setAttribute('src', '../media/images/nc/calander.gif');
                r_cal_img.setAttribute('class', 'pl2');
				r_cal_img.setAttribute('className', 'pl2');
                r_cal_img.setAttribute('border', '0');
                r_cal_img.setAttribute('title', 'Calander');
                r_cal_img.setAttribute('alt', 'calander');
            r_cal_a.appendChild(r_cal_img);
            //r_cal_a.innerHTML = 'Calander';
            // Create the SPAN for,
            // (dd/mm/yyyy)
            //r_cal_span = document.createElement('span');
            //r_cal_span.setAttribute('class', 'pl10');
			//r_cal_span.setAttribute('className', 'pl10');
            //r_cal_span.innerHTML = ' (dd/mm/yyyy)';
        c2_div.appendChild(r_script);
        c2_div.appendChild(r_input);
        c2_div.appendChild(r_clear_a);
        c2_div.appendChild(r_cal_a);
        //c2_div.appendChild(r_cal_span);
        c2_div.innerHTML = c2_div.innerHTML + ' (dd/mm/yyyy)';
        
        c3_div = document.createElement('div');
        c3_div.setAttribute('class', 'coloumn pl10');
		c3_div.setAttribute('className', 'coloumn pl10');
            // Create the INPUT TEXT.
            r_text_input = document.createElement('input');
            r_text_input.setAttribute('type', 'text');
            r_text_input.setAttribute('name', 'remind_text_'+ r_count);
            r_text_input.setAttribute('value', '');
            r_text_input.setAttribute('class', 'inputbox');
			r_text_input.setAttribute('className', 'inputbox');
            r_text_input.setAttribute('maxlength', '200');
            r_text_input.setAttribute('size', '50');
        c3_div.appendChild(r_text_input);
    
    // Append the Coloums to the main div.
    reminder_div.appendChild(c1_div);
    reminder_div.appendChild(c2_div);
    reminder_div.appendChild(c3_div);
    
    document.getElementById("reminder_count").value = r_count+1;
    element.appendChild(reminder_div);
    return (true);
}


function removeElement(element, to_delete) {

    if ( element.removeChild(to_delete) ) {
    //if (1) {
        return (true);
    }
    else {
        return (false);
    }
    
}


/**
 * This function is used to tackle the problem that arises when using
 * Checkboxes and Radio buttons as arrays in tandem with other entities.
 * The problem is tackled by using a hidden field for representing the
 * actual Checkboxes and Radio buttons, thus the all the hidden fields will
 * be POSTED to the SERVER instead only one value that is selected in case of
 * of Checkboxes and Radio buttons thus maintaining the INDEX associativity 
 * when compared to other entities.
 *
 */
function changeSelection(element, parent_name) {
    pref_hidden_arr = document.getElementsByName(parent_name+"[]");
    pref_radio_arr = document.getElementsByName(parent_name+"_radio[]");
    count = pref_hidden_arr.length;
    for ( i=0; i<count; i++ ) {
        //alert(pref_hidden_arr[i].name +"["+ i +"] = "+ pref_hidden_arr[i].value);
        if ( pref_radio_arr[i] == element ){
            pref_hidden_arr[i].value = 1;
        }
        else {
            pref_hidden_arr[i].value = 0;
        }
        //alert(pref_hidden_arr[i].name +"["+ i +"] = "+ pref_hidden_arr[i].value);
    }

}

function changeChkSelection(element, parent_name) {
    pref_hidden_arr = document.getElementsByName(parent_name+"[]");
    pref_chk_arr = document.getElementsByName(parent_name+"_chk[]");
    count = pref_hidden_arr.length;
    for ( i=0; i<count; i++ ) {
        //alert(pref_hidden_arr[i].name +"["+ i +"] = "+ pref_hidden_arr[i].value);
        if ( pref_chk_arr[i] == element ) {
            if ( pref_chk_arr[i].checked ) {
                pref_hidden_arr[i].value = 1;
            }
            else {
                pref_hidden_arr[i].value = 0;
            }
        }
        //alert(pref_hidden_arr[i].name +"["+ i +"] = "+ pref_hidden_arr[i].value);
    }
}


/**
 * This function is used to select and relate the Executive to a 
 * Customer as the manager.
 *
 */
function addExecutive(executive_arr) {
    
    div_executive = document.getElementById("executive_info");

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', 'remove_'+ executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'team[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'team_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ executive_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeExecutive('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img); 
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a); 
    div_executive.appendChild(exec_div); 
    return (true);
}
/**
 * This function is used to un-relate a Executive from a Customer.
 *
 */
function removeExecutive(id) {
    div_executive = document.getElementById("executive_info"); 
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    } else {
        return (false);
    }
}
function toggleAllowIp(sel1, element1) { 
  element = document.getElementById(element1);
  // if specified then dispay textarea to enter Ip

  if (sel1.value == '2') {
    element.style.display = 'inline';
  }
  else {
    element.value = ''; // input text will be empty
    element.style.display = 'none'; // hide text element
  }

  return (true);
}


function emailBlock(){ 
	if(document.getElementById("check_email").checked){
		document.getElementById("email").value = 'noemail@smeerptech.com'; 
	}else{
		document.getElementById("email").value = ''; 
	}
	
	return (true);
}