//Function to update the Order Followup status using AJAX BOF
var http_req_alerts_banner;
function updateInvFlwStatus( id,status){
 var del=reallyDel('Are you sure you want to Change the Status?');

  if(del==true){
	http_req_alerts_banner = false;	
	document.getElementById("orderid").value = id;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    }
    var uri = "get-orders-status.php?id="+ id+"&status="+status; 
    http_req_alerts_banner.onreadystatechange = loadInvFlwStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);
  }
}

function loadInvFlwStatus(){
 
     id = document.getElementById("orderid").value ;
	 statusid = 'status'+id ;
	 tdsstatusid = 'tdsstatus'+id ;
	 ceostatusid = 'ceostatus'+id ;
	 
	if(http_req_alerts_banner.readyState==4){
   
        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
             
			 
            if(allData){            
           
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataTdsStatus = dataSplit[2];
                var dataCeoStatus = dataSplit[3];
                var dataHighlight = dataSplit[4];	
 				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
                // document.getElementById(tdsstatusid).innerHTML= dataTdsStatus; 
                //document.getElementById(ceostatusid).innerHTML= dataCeoStatus; 
				
				highlightid = 'row'+dataHighlight ;
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
        
        
    }

}
//Function to update the Order Followup status using AJAX BOF

function showInvoice(inv_id, file_type) {	
	if (file_type == 'htmlprint' || file_type == 'html' ||  file_type == 'pdf') {      
        url = "./bill-invoice.php?perform=view_file"	
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Invoice','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}
function showInvoiceProfm(inv_id, file_type) {


   if (file_type == 'htmlprint' || file_type == 'html' ||  file_type == 'pdf') {      
        url = "./bill-invoice-profm.php?perform=view_file"
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Invoice','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}

function showClientDetails(client_id) {
    window.open('./clients.php?perform=view&user_id='+client_id,'clientDetails','left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1');
    return (true);
}