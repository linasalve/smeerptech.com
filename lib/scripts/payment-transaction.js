function printRecords(){
alert("print");
}
function excelRecords(){
alert("excel");
}
function wordRecords(){
alert("word");
}
function pdfRecords(){
alert("pdf");
}


function addExecutiveInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);
    executive_detail_div = document.createElement('div');
 
	if (navigator.appName=="Netscape") {
        executive_detail_div.setAttribute('class', 'row');
    }
    if (navigator.appName.indexOf("Microsoft")!=-1) {
        executive_detail_div.setAttribute('className', 'row');
    }
    
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'executive_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'executive_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
		
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeExecutiveInfo('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  

    return (true);
}

function removeExecutiveInfo(id,divname) {
    div_executive = document.getElementById(divname);

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}
function addPartyInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);
 

    executive_detail_div = document.createElement('div');
    executive_detail_div.setAttribute('class', 'row');
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'vendor_bank_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'vendor_bank_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" - "+executive_arr[5] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        		if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removePartyInfo('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  
    getBillAgainstVendorAcc(executive_arr[0]);
    return (true);
}

function removePartyInfo(id,divname) {
    div_executive = document.getElementById(divname);
   
    if ( div_executive.removeChild(document.getElementById(id)) ) {       
        return (true);
    }
    else {
        return (false);
    }
}

function addClientInfo(executive_arr) {
    div_executive = document.getElementById('add_clients_info');
 

    executive_detail_div = document.createElement('div');
    executive_detail_div.setAttribute('class', 'row');
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'client_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'client_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" - "+executive_arr[5] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        		if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeClientInfo('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  
    
    return (true);
}

function removeClientInfo(id) {
    div_executive = document.getElementById('add_clients_info');
    
    if ( div_executive.removeChild(document.getElementById(id)) ) {         
        return (true);
    }
    else {
        return (false);
    }
}


//Function to Mark Completed using AJAX BOF
var http_req_alerts_cmp;
function updateStatus(id){

	http_req_alerts_cmp = false;	
	document.getElementById("transactionid").value = id;
	trn_tpy = document.getElementById("transaction_type"+id).value ;
	gl_type = document.getElementById("gl_type"+id).value ;
 
	
	tr_dt = document.getElementById("do_transaction"+id).value ;
	//vch_dt = document.getElementById("do_voucher"+id).value ;
	status = document.getElementById("status"+id).value ;
	db_bank_id = document.getElementById("db_bank_id"+id).value ;
	cr_bank_id = document.getElementById("cr_bank_id"+id).value ;
	
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_cmp = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_cmp = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_cmp = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_cmp) {
        return false;
    } 
    /*  
	if(tr_dt=='' || vch_dt==''){
		alert('Select transaction dt and voucher dt');
		return false;
	} */
    if(tr_dt==''){
		alert('Select transaction dt');
		return false;
	}
	if(document.getElementById("status"+id).selectedIndex==0){
		alert('Select Proper Status');
		return false;
	}
	
	var slist = document.getElementById('status'+id); 
	s_options = "";
	for(var i = 0; i < slist.options.length; ++i){
		comma = ",";
		if(slist.options[i].selected){
			s_options = s_options + slist[i].value + comma;
		}
	}
	s_options = trim(s_options,",");
	if(gl_type==0){
		if( (trn_tpy==2 || trn_tpy==3) && db_bank_id==''){
			alert('Select debit bank');
			return false;
		}
		if( (trn_tpy==1 || trn_tpy==3) && cr_bank_id==''){
			alert('Select credit bank');
			return false;
		}
	} 
	
/* var uri = "payment-transaction-mark-completed.php?btnGo=1&transaction_id="+ id+"&status="+status+"&do_transaction="+tr_dt+"&do_voucher="+vch_dt+"&credit_pay_bank_id="+cr_bank_id+"&debit_pay_bank_id="+db_bank_id+"&gl_type="+gl_type;  */   
var uri = "payment-transaction-mark-completed.php?btnGo=1&transaction_id="+id+"&status="+s_options+"&do_transaction="+tr_dt+"&credit_pay_bank_id="+cr_bank_id+"&debit_pay_bank_id="+db_bank_id+"&gl_type="+gl_type; 

		http_req_alerts_cmp.onreadystatechange = loadStatus;
		http_req_alerts_cmp.open('GET', uri, true);
		http_req_alerts_cmp.send(null);
	
	

}
function loadStatus(){
    id = document.getElementById("transactionid").value ;
	statusdivid = 'status_div'+id ;
	trdtdivid = 'trdt_div'+id ;
	vchdtdivid = 'vch_div'+id ;
	bankdivid = 'bank_div'+id ;
	godivid = 'go_div'+id ;
	editdivid = 'edit_div'+id ;
	dramtdivid = 'dramt_div'+id ;
	cramtdivid = 'cramt_div'+id ;
	 
	if(http_req_alerts_cmp.readyState==4){
   
        if (http_req_alerts_cmp.status == 200){
            var allData = http_req_alerts_cmp.responseText;
            
            if(allData){
			
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];                
				var dataStatus = dataSplit[1];		 
                var dataTrDt = dataSplit[2]; 
                var dataVchDt = dataSplit[3]; 
                var dataGo = dataSplit[4]; 
                var dataEdit = dataSplit[5]; 
                var dataDrAmt = dataSplit[6]; 
                var dataCrAmt = dataSplit[7];
				
                document.getElementById('message').innerHTML= dataMsg; 
                document.getElementById(statusdivid).innerHTML= dataStatus; 
                document.getElementById(trdtdivid).innerHTML = dataTrDt; 
                document.getElementById(vchdtdivid).innerHTML = dataVchDt; 
                document.getElementById(godivid).innerHTML = dataGo; 
                document.getElementById(editdivid).innerHTML = dataEdit; 
                document.getElementById(dramtdivid).innerHTML = dataDrAmt; 
                document.getElementById(cramtdivid).innerHTML = dataCrAmt; 
                document.getElementById(bankdivid).innerHTML = ''; 
				
				/* 
				highlightid = 'row'+dataHighlight ;				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");					
				}   
				*/   
            }
        }
    }else{
		document.getElementById("message").innerHTML= ""; 
    }
}
//Function to Mark Completed using AJAX BOF

//Function to update the Banner status using AJAX BOF
var http_req_alerts_link;
function updateLinkStatus( id,status){

	http_req_alerts_link = false;	
	document.getElementById("transactionid").value = id;
	
	
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_link = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_link = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_link = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_link) {
        return false;
    } 
   
    var uri = "get-transaction-linkstatus.php?id="+ id+"&status="+status;    

    http_req_alerts_link.onreadystatechange = loadLinkStatus;
    http_req_alerts_link.open('GET', uri, true);
    http_req_alerts_link.send(null);

}
function loadLinkStatus(){
    id = document.getElementById("transactionid").value ;
	statusid = 'status'+id ;
	 
	if(http_req_alerts_link.readyState==4){
   
        if (http_req_alerts_link.status == 200){
            var allData = http_req_alerts_link.responseText;			
            if(allData){  
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataHighlight = dataSplit[2];
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
				highlightid = 'row'+dataHighlight ;
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
				}     
            }
        }
    }else{
		document.getElementById("message").innerHTML= ""; 
    }
}
//Function to update the Link status using AJAX BOF

//Function to update the Banner status using AJAX BOF
var http_req_alerts_link;
function updateLinkBankStatus( id,status){
 var del=reallyDel('Are you sure you want to change the Bank Stmt Status?');
 if(del==true){
	http_req_alerts_link = false;	
	document.getElementById("transactionid").value = id; 
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_link = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_link = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_link = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_link) {
        return false;
    }  
    var uri = "get-transaction-link-bank-stmt-status.php?id="+ id+"&status="+status;     
    http_req_alerts_link.onreadystatechange = loadLinkBankStatus;
    http_req_alerts_link.open('GET', uri, true);
    http_req_alerts_link.send(null); 
 }
}
function loadLinkBankStatus(){
    //id = document.getElementById("transactionid").value ;
	
	if(http_req_alerts_link.readyState==4){
   
        if (http_req_alerts_link.status == 200){
            var allData = http_req_alerts_link.responseText;			
            if(allData){  
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1]; 
                var id = dataSplit[2]; 
				statusid = 'stmt_status'+id ; 
				highlightid='row'+id ; 
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
				 
				if(id!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
				}     
            }
        }
    }else{
		document.getElementById("message").innerHTML= ""; 
    }
}
//Function to update the Link status using AJAX BOF

//Function to send the message using AJAX BOF
var http_req_alerts_msg;
function getMessage( trans_id,trans_no,client_id,type){

	http_req_alerts_msg = false;	
	document.getElementById("trans_id").value = trans_id;
	//msg_by = document.getElementById("msg_by"+trans_id).value; 
	   
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_msg = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_msg = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_msg = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_msg) {
        return false;
    } 
   
    var uri = "ajx-send-transaction-msg.php?trans_id="+ trans_id+"&trans_no="+ trans_no +"&client_id="+client_id+"&type="+type;    

    http_req_alerts_msg.onreadystatechange = sendMessage;
    http_req_alerts_msg.open('GET', uri, true);
    http_req_alerts_msg.send(null);

}
function sendMessage(){
     trans_id = document.getElementById("trans_id").value ;
	 rowid = 'row'+trans_id ;
	if(http_req_alerts_msg.readyState==4){

        if (http_req_alerts_msg.status == 200){
            var allData = http_req_alerts_msg.responseText;	   			
            if(allData){                        
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg =dataSplit[0];
                var dataStatus =dataSplit[1];
                document.getElementById("message").innerHTML= dataMsg; 
               // document.getElementById(rowid).innerHTML= dataStatus; 
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= "Loading..."; 
        
        
    }

}
//Function to send the message using AJAX BOF
 

/**
* To toggle pdc in payment in/out module
**/

function togglePDC(obj) {
   if(obj.value == '1'){
        document.getElementById('check').className="show";
    }else{
        document.getElementById('check').className="hide";
    }  
}

/**
* To toggle payment mode in payment in/out module
**/

function togglePaymentMode(obj) {
   
   if(obj.value == '2'){
        document.getElementById('debit').className="show";
        document.getElementById('credit').className="hide";
    }else{
        document.getElementById('debit').className="hide";
        document.getElementById('credit').className="show";
    }
}
/**Update the field on change type BOF**/
function updateFields(type){

    if(type == '2'){
        document.getElementById('debit').className="show";
        document.getElementById('credit').className="hide";
		document.getElementById('credit_pay_bank_id').selectedIndex=0;
		document.getElementById('credit_pay_amt').value=0;
        //document.getElementById('bill').className="show";
       // document.getElementById('invoice').className="hide";           
        //document.getElementById('bill_no').className="hide";                
       // document.frmAdd.is_bill_receive[1].checked="true";       
    }
    if(type == '1'){
        document.getElementById('debit').className="hide";
        document.getElementById('credit').className="show";
		
		document.getElementById('debit_pay_bank_id').selectedIndex=0;
		document.getElementById('gl_type').selectedIndex=0;
		document.getElementById('debit_pay_amt').value=0;
        //document.getElementById('invoice').className="show";
       // document.getElementById('bill').className="hide";        
        //document.getElementById('bill_no').className="hide";
       // document.frmAdd.is_bill_receive[1].checked="true";       
    }
    if(type == '3'){
        document.getElementById('debit').className="show";
        document.getElementById('credit').className="show";
        //document.getElementById('bill').className="show";
        //document.getElementById('invoice').className="show";
    }
    
}
/**Update the field on change type EOF**/
function updateFieldsN(){
 var e = document.getElementById('transaction_type');
 var type =  e.options[e.selectedIndex].value;

 var e1 = document.getElementById('tran_purpose');
 var purpose =  e1.options[e1.selectedIndex].value;
   

	if(type == '1'){ //In
        document.getElementById('to_out').className="hide";
        document.getElementById('from_out').className="hide"; 
		document.getElementById('from_in').className="show";
        document.getElementById('to_in').className="show";
		
		document.getElementById('debit_pay_bank_id').selectedIndex=0;
		if(purpose == '1'){ //salary
			document.getElementById('client_fi').className="hide";
			document.getElementById('staff_fi').className="hide";
			document.getElementById('ibank_fi').className="show";
			document.getElementById('client_id_fi').value='';
			document.getElementById('client_details_fi').value='';
			document.getElementById('executive_id_fi').value='';
			document.getElementById('executive_details_fi').value='';
		}
		if(purpose == '2'){ //bank charges
			document.getElementById('client_fi').className="hide";
			document.getElementById('staff_fi').className="hide";
			document.getElementById('ibank_fi').className="hide";
			document.getElementById('client_id_fi').value='';
			document.getElementById('client_details_fi').value='';
			document.getElementById('executive_id_fi').value='';
			document.getElementById('executive_details_fi').value='';
			document.getElementById('ref_bank_id_fi').selectedIndex=0;
		}
		if(purpose == '3'){ //sale/purchase
			document.getElementById('ibank_fi').className="hide";
			document.getElementById('staff_fi').className="hide";
			document.getElementById('client_fi').className="show";
			document.getElementById('executive_id_fi').value='';
			document.getElementById('executive_details_fi').value='';
			document.getElementById('ref_bank_id_fi').selectedIndex=0;
		} 
		if(purpose == '4'){ //betw bank accounts
			document.getElementById('client_fi').className="hide";
			document.getElementById('staff_fi').className="hide";
			document.getElementById('ibank_fi').className="show";
			document.getElementById('client_id_fi').value='';
			document.getElementById('client_details_fi').value='';
			document.getElementById('executive_id_fi').value='';
			document.getElementById('executive_details_fi').value='';			
		}  		
		
    }
	if(type == '2'){ //out
        
		document.getElementById('from_in').className="hide";
        document.getElementById('to_in').className="hide";
		document.getElementById('to_out').className="show";
        document.getElementById('from_out').className="show"; 
		document.getElementById('credit_pay_bank_id').selectedIndex=0;
		if(purpose == ''){
			document.getElementById('client_to').className="show";
			document.getElementById('ibank_to').className="show";
			document.getElementById('staff_to').className="show";
		}
		if(purpose == '1'){//salary
			document.getElementById('client_to').className="hide";
			document.getElementById('ibank_to').className="show";
			document.getElementById('staff_to').className="show";
			
			document.getElementById('client_id_to').value='';
			document.getElementById('client_details_to').value='';
			 
		}
		if(purpose == '2'){ //bank charges
			document.getElementById('ibank_to').className="hide";
			document.getElementById('staff_to').className="hide";
			document.getElementById('client_to').className="show";
			document.getElementById('executive_id_to').value='';
			document.getElementById('executive_details_to').value='';
			document.getElementById('ref_bank_id_to').selectedIndex=0;
			
		}
		if(purpose == '3'){ //sale/purchase
			document.getElementById('ibank_to').className="hide";
			document.getElementById('staff_to').className="hide";
			document.getElementById('client_to').className="show";
			document.getElementById('ref_bank_id_to').selectedIndex=0;
			document.getElementById('executive_id_to').value='';
			document.getElementById('executive_details_to').value='';
			
		}
		if(purpose == '4'){ //betw bank accounts
			document.getElementById('client_to').className="hide";
			document.getElementById('staff_to').className="hide";
			document.getElementById('ibank_to').className="show";
			document.getElementById('executive_id_to').value='';
			document.getElementById('executive_details_to').value='';
			document.getElementById('client_id_to').value='';
			document.getElementById('client_details_to').value='';
		} 
    }
	if(type == '3'){ //internal
		document.getElementById('to_out').className="hide";
        document.getElementById('from_in').className="hide"; 
		document.getElementById('from_out').className="show"; 
		document.getElementById('to_in').className="show";
		document.getElementById('tran_purpose').selectedIndex=4;
		document.getElementById('executive_id_to').value='';
		document.getElementById('executive_details_to').value='';
		document.getElementById('client_id_to').value='';
		document.getElementById('client_details_to').value='';
		document.getElementById('ref_bank_id_to').selectedIndex=0;
		document.getElementById('ref_bank_id_fi').selectedIndex=0;
		document.getElementById('client_id_fi').value='';
		document.getElementById('client_details_fi').value='';
		document.getElementById('executive_id_fi').value='';
		document.getElementById('executive_details_fi').value='';
    }	 
	updateBankList();
	updateMode();
}

var http_req_alerts_bank;
function updateBankList(){ 
  /* var company_id = document.getElementById('company_id').value ; 
  var transaction_type = document.getElementById('transaction_type').value ;  */
  var e1 = document.getElementById('company_id');
  var company_id =  e1.options[e1.selectedIndex].value;
  
  var e2 = document.getElementById('transaction_type');
  var transaction_type =  e2.options[e2.selectedIndex].value;
  
	
	http_req_alerts_bank = false;
	   
	if (window.XMLHttpRequest){ // Mozilla, Safari,...
		http_req_alerts_bank = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts_bank = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts_bank = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts_bank) {
		return false;
	} 
	var uri = "get-bank-list.php?company_id="+company_id+"&transaction_type="+ transaction_type ;    
	 
	http_req_alerts_bank.onreadystatechange = loadBankList;
	http_req_alerts_bank.open('GET', uri, true);
	http_req_alerts_bank.send(null);	
}
function loadBankList(){
    var company_id = document.getElementById('company_id').value ; 
    var transaction_type = document.getElementById('transaction_type').value ; 
	 
	if(transaction_type == 1){// IN
		//
		var crdBank = document.getElementById("credit_pay_bank_id");
		if(http_req_alerts_bank.readyState==4){
   
			if (http_req_alerts_bank.status == 200){
				var allThoseTypes = http_req_alerts_bank.responseText;
			 
				if(allThoseTypes){	
					crdBank.options.length=0;
					crdBank.options[0] = new Option("Select Bank", "0");
					crdBank.style.background ="#ffffff";
					var typeSplit =new Array();
					typeSplit = allThoseTypes.split(",");
				 
					for (var i=0;i<typeSplit.length;i++){
						var tmpStr =typeSplit[i];
						if(tmpStr){
							var tmpArray = new Array();
							tmpArray = tmpStr.split("|");
							var tmpName = tmpArray[0];
							var tmpVal = tmpArray[1];
						   
							if(tmpName){
								crdBank.options[i+1] = new Option(tmpName, tmpVal);                           
							}
						}
					}           
				}
			}
		}else{        
			document.getElementById('credit_pay_bank_id').options.length=0;
			/* crdBank.options[0] = new Option("Loading...", "");
			crdBank.style.background = "#c3c3c3"; */
		}
		
	}
	
	if(transaction_type == 2){// OUT
		
		//debit_pay_bank_id
		var dbBank = document.getElementById("debit_pay_bank_id");
		if(http_req_alerts_bank.readyState==4){
   
			if (http_req_alerts_bank.status == 200){
				var allThoseTypes = http_req_alerts_bank.responseText;
			 
				if(allThoseTypes){	
					dbBank.options.length=0;
					dbBank.options[0] = new Option("Select Bank", "0");
					dbBank.style.background ="#ffffff";
					var typeSplit =new Array();
					typeSplit = allThoseTypes.split(",");
					
					for (var i=0;i<typeSplit.length;i++){
						var tmpStr =typeSplit[i];
						if(tmpStr){
							var tmpArray = new Array();
							tmpArray = tmpStr.split("|");
							var tmpName = tmpArray[0];
							var tmpVal = tmpArray[1];
						   
							if(tmpName){
								dbBank.options[i+1] = new Option(tmpName, tmpVal);                           
							}
						}
					}           
				}
			}
		}else{   
			document.getElementById('debit_pay_bank_id').options.length=0;
			/* dbBank.options[0] = new Option("Loading...", "");
			dbBank.style.background = "#c3c3c3"; */
		}
	}
	
	if(transaction_type == 3){ // INTERNAL
		
		var crdBank = document.getElementById("credit_pay_bank_id");
		var dbBank = document.getElementById("debit_pay_bank_id");
		if(http_req_alerts_bank.readyState==4){
   
			if (http_req_alerts_bank.status == 200){
				var allThoseTypes = http_req_alerts_bank.responseText;
			 
				if(allThoseTypes){	
					crdBank.options.length=0;
					crdBank.options[0] = new Option("Select Bank", "0");
					crdBank.style.background ="#ffffff";
					dbBank.options.length=0;
					dbBank.options[0] = new Option("Select Bank", "0");
					dbBank.style.background ="#ffffff";
					var typeSplit =new Array();
					typeSplit = allThoseTypes.split(",");
					for (var i=0;i<typeSplit.length;i++){
						var tmpStr =typeSplit[i];
						if(tmpStr){
							var tmpArray = new Array();
							tmpArray = tmpStr.split("|");
							var tmpName = tmpArray[0];
							var tmpVal = tmpArray[1];
						   
							if(tmpName){
								crdBank.options[i+1] = new Option(tmpName, tmpVal);
								dbBank.options[i+1] = new Option(tmpName, tmpVal); 								
							}
						}
					}           
				}
			}
		}else{        
			/* crdBank.options[0] = new Option("Loading...", "");
			crdBank.style.background = "#c3c3c3";
			dbBank.options[0] = new Option("Loading...", "");
			dbBank.style.background = "#c3c3c3"; */
		}		
	}       
}


http_req_alerts_ch = false;
function updateMode(){
	 
	var ee = document.getElementById('mode');
	var mode =  ee.options[ee.selectedIndex].value;
	var e1 = document.getElementById('transaction_type');
	var transaction_type =  e1.options[e1.selectedIndex].value;
    if(mode==1 && (transaction_type==2 || transaction_type==3) ){ // if cheque selected + OUT + INTERNAL
 
		document.getElementById('mode_chq').className="show";
		document.getElementById('pay_cheque_no').readOnly=1;
		

		var debit_pay_bank_id =credit_pay_bank_id=0;
		if(transaction_type==2 || transaction_type==3){
			var e2 = document.getElementById('debit_pay_bank_id');
			debit_pay_bank_id =  e2.options[e2.selectedIndex].value;
		}
		if(transaction_type==1 || transaction_type==3){
			var e3 = document.getElementById('credit_pay_bank_id');
			credit_pay_bank_id =  e3.options[e3.selectedIndex].value;
		}
		http_req_alerts_ch = false;
		
		if (window.XMLHttpRequest){ // Mozilla, Safari,...
			http_req_alerts_ch = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_ch = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_ch = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_ch) {
			return false;
		} 
		var uri = "get-cheque-nos.php?credit_pay_bank_id="+credit_pay_bank_id+"&debit_pay_bank_id="+debit_pay_bank_id+"&transaction_type="+ transaction_type ;    
		 
		http_req_alerts_ch.onreadystatechange = loadChequeList;
		http_req_alerts_ch.open('GET', uri, true);
		http_req_alerts_ch.send(null);	
  }else{
	document.getElementById('mode_chq').className="hide";
    document.getElementById('cheque_no').options.length=0;
    document.getElementById('cheque_no_id').value='';
    document.getElementById('pay_cheque_no').value='';
    document.getElementById('pay_cheque_no').readonly=0;
  }
  
}

function loadChequeList(){

	var cheque_no = document.getElementById("cheque_no");
	if(http_req_alerts_ch.readyState==4){

		if (http_req_alerts_ch.status == 200){
			var allThoseTypes = http_req_alerts_ch.responseText;
		 
			if(allThoseTypes){	
				cheque_no.options.length=0;
				cheque_no.options[0] = new Option("Select Cheque", "0");
				cheque_no.style.background ="#ffffff";
				var typeSplit =new Array();
				typeSplit = allThoseTypes.split(",");
			 
				for (var i=0;i<typeSplit.length;i++){
					var tmpStr =typeSplit[i];
					if(tmpStr){
						var tmpArray = new Array();
						tmpArray = tmpStr.split("|");
						var tmpName = tmpArray[0];
						var tmpVal = tmpArray[0]+"#"+tmpArray[1];
					   
						if(tmpName){
							cheque_no.options[i+1] = new Option(tmpName, tmpVal);                           
						}
					}
				}           
			}
		}
	}else{        
		cheque_no.options[0] = new Option("Loading...", "");
		cheque_no.style.background = "#c3c3c3";
	}

}
function loadCheque(val){

 if(val!=''){
	var arr =new Array();
	arr = val.split("#");
	document.getElementById('pay_cheque_no').value=arr[0];
	document.getElementById('cheque_no_id').value=arr[1];

  }
}
http_req_alerts_chc = false;
function clearChequeTran(url){
	var del=reallyDel('Are you sure you want to clear cheque no?');
	if(del==true){
		window.location.href= url;
		return true;		
	}
	return false;
}
 
function toggleCompany(){
	 
	var inhouse = document.getElementById('inhouse');
	var global = document.getElementById('global');

	 
	if(inhouse.checked==true){
		 document.getElementById('company').className="show";
		 
	}
	if(global.checked==true){
		document.getElementById('company').className="hide";
		document.getElementById('company_id').selectedIndex=0;
	}
}
/**
* To Load account head depending on transaction type in payment in/out module
**/
//new added
var http_req_alerts;
var http_req_alerts1;

function updateAccountHead(type) {

    if(type == '2'){
        document.getElementById('debit').className="show";
        document.getElementById('credit').className="hide";
        document.getElementById('bill').className="show";
        document.getElementById('invoice').className="hide";           
        document.getElementById('bill_no').className="hide";                
        document.frmAdd.is_bill_receive[1].checked="true";       
    }
    if(type == '1'){
        document.getElementById('debit').className="hide";
        document.getElementById('credit').className="show";
        document.getElementById('invoice').className="show";
        document.getElementById('bill').className="hide";        
        document.getElementById('bill_no').className="hide";
        document.frmAdd.is_bill_receive[1].checked="true";       
    }
    if(type == '3'){
        document.getElementById('debit').className="show";
        document.getElementById('credit').className="show";
        document.getElementById('bill').className="show";
    }
    
    var cboType = document.getElementById("accounthead");
   
    cboType.options.length = 0;
    if(!type){
        cboType.options[0] = new Option("", "");	
        return null;
    }
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""

    var uri = "get-account-head.php?type=" + type;    

    http_req_alerts.onreadystatechange = loadAccountHead;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

/**
* To Load account head depending on transaction type in payment in/out module
**/


function loadAccountHead(){
    var cboType = document.getElementById("accounthead");
   
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
          
            if(allThoseTypes){			
                
                cboType.options[0] = new Option("Select Account Head", "0");
                cboType.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");
            
                 for (var i=0;i<typeSplit.length;i++){
                    var tmpStr =typeSplit[i];
                    if(tmpStr){
                        var tmpArray = new Array();
                        tmpArray = tmpStr.split("|");
                        var tmpName = tmpArray[0];
                        var tmpVal = tmpArray[1];
                       
                        if(tmpName){
                            cboType.options[i+1] = new Option(tmpName, tmpVal);                           
                        }
                    }
                }           
            }
        }
    }else{        
        cboType.options[0] = new Option("Loading...", "");
          cboType.style.background = "#c3c3c3";
    }
}

/**
* To Toggle invoice in payment in/out module
**/


function toggleInvoice(obj) {
  	if(obj.value == '1'){
        document.getElementById('invoice_no').className="show";
    }else{
        document.getElementById('invoice_no').className="hide";
    }  
}

/**
* To Toggle bill receive in payment in/out module
**/


function toggleBill(obj) {
  	if(obj.value == '1'){
        document.getElementById('bill_no').className="show";
    }else{
        document.getElementById('bill_no').className="hide";
    }  
}


function updateCheque(mode) {
	
    if(mode == '1'){
        document.getElementById('pay_chq_no').className="show";
        document.getElementById('pay_chq_dt').className="show";
        document.getElementById('pay_chq_bank').className="show";
        document.getElementById('pay_chq_branch').className="show";      
        //document.getElementById('invoice_no').className="hide";       
        //document.getElementById('bill_no').className="hide";                
    }else{
        document.getElementById('pay_chq_no').className="hide";
        document.getElementById('pay_chq_dt').className="hide";
        document.getElementById('pay_chq_bank').className="hide";
        document.getElementById('pay_chq_branch').className="hide";      
    }
}

function updateCreditAmt() {
    var transType=document.getElementById('type').value;
    if(transType== '3'){
        var x=document.getElementById("debit_pay_amt").value;
        document.getElementById("credit_pay_amt").value=x;
        document.getElementById("credit_pay_amt").readOnly="true";
    }
}

function copyAmount(){   
    updateTotal();
   /* pay_recamt = document.frmAdd.pay_received_amt.value;
    document.frmAdd.credit_pay_amt.value = pay_recamt;
    document.frmAdd.debit_pay_amt.value = pay_recamt;
     */  

}
function updateTotal() {
    total = document.frmAdd.pay_received_amt.value ;
    exchange_rate = document.frmAdd.exchange_rate.value;    
    document.frmAdd.pay_received_amt_inr.value = parseFloat(total) * parseFloat(exchange_rate);
	//document.frmAdd.credit_pay_amt.value = parseFloat(total) * parseFloat(exchange_rate);
    //document.frmAdd.debit_pay_amt.value = parseFloat(total) * parseFloat(exchange_rate);
       
}
// @param trans_id - id 
// @param type ie cheque, deposite slip
function showTransaction(trans_id,file) {
    if (trans_id != '') {          
        url = "./payment-transaction.php?perform=download_file"
        url = url + "&trans_id="+trans_id+'&file='+ file;        
        window.open(url,'Transaction','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
	
}


function showTransactionPending(trans_id,file) {
    if (trans_id != '') {          
        url = "./payment-transaction-pending.php?perform=download_file"
        url = url + "&trans_id="+trans_id+'&file='+ file;        
        window.open(url,'Transaction','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
	
}


function showBill(bill_id,file) {
    if (bill_id != '') {          
        url = "./payment-party-bills.php?perform=download_file"
        url = url + "&bill_id="+bill_id+'&file='+ file;        
        window.open(url,'Download Bill','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
	
}
var http_req_alerts_clr;
function clearAmount(bill_id){
	var del=reallyDel('Are you sure you want to clear amount?');

	if(del==true){
		document.getElementById("cbill_id").value = bill_id;
		http_req_alerts_clr = false;	
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_req_alerts_clr = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_clr = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_clr = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_clr) {
			return false;
		} 
			
		var uri = "payment-party-bills-clear-amt.php?bill_id="+ bill_id ;    

		http_req_alerts_clr.onreadystatechange = loadClearAmt;
		http_req_alerts_clr.open('GET', uri, true);
		http_req_alerts_clr.send(null);
	}
}


function loadClearAmt(){
	id = document.getElementById("cbill_id").value ;
	paiddivid = 'paid_div'+id ;
	baldivid = 'bal_div'+id ;
	if(http_req_alerts_clr.readyState==4){
   
        if (http_req_alerts_clr.status == 200){
            var allData = http_req_alerts_clr.responseText;
            
            if(allData){
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataPaid = dataSplit[1];
                var dataBal = dataSplit[2]; 
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(paiddivid).innerHTML= dataPaid; 
                document.getElementById(baldivid).innerHTML = dataBal; 				  
            }
        }
    }else{
		document.getElementById("message").innerHTML= ""; 
    }
}