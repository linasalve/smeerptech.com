//Function to update the Order Followup status using AJAX BOF
var http_req_alerts_banner;
function updateInvFlwStatus( id,status){
 var del=reallyDel('Are you sure you want to Change the Status?');

  if(del==true){
	http_req_alerts_banner = false;	
	document.getElementById("orderid").value = id;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    }
    var uri = "get-orders-status.php?id="+ id+"&status="+status; 
    http_req_alerts_banner.onreadystatechange = loadInvFlwStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);
  }
}

function loadInvFlwStatus(){
 
     id = document.getElementById("orderid").value ;
	 statusid = 'status'+id ;
	 tdsstatusid = 'tdsstatus'+id ;
	 ceostatusid = 'ceostatus'+id ;
	 
	if(http_req_alerts_banner.readyState==4){
   
        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;			 
            if(allData){  
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataTdsStatus = dataSplit[2];
                var dataCeoStatus = dataSplit[3];
                var dataHighlight = dataSplit[4];	
				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
                document.getElementById(tdsstatusid).innerHTML= dataTdsStatus; 
                document.getElementById(ceostatusid).innerHTML= dataCeoStatus; 
				highlightid = 'row'+dataHighlight ;				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
				}     
            }
        }
    }else{
		document.getElementById("message").innerHTML= ""; 
    }

}
//Function to update the Order Followup status using AJAX BOF
//Function to update the Order TDS status using AJAX BOF
var http_req_alerts_tds;
function updateTdsStatus( id,tdsstatus){
	http_req_alerts_tds = false;	
	document.getElementById("orderid").value = id;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_tds = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_tds = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_tds = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_tds) {
        return false;
    }
    var uri = "get-orders-tds-status.php?id="+ id+"&tdsstatus="+tdsstatus; 
    http_req_alerts_tds.onreadystatechange = loadTdsStatus;
    http_req_alerts_tds.open('GET', uri, true);
    http_req_alerts_tds.send(null);
}

function loadTdsStatus(){  
    id = document.getElementById("orderid").value ;
	tdsstatusid = 'tdsmarkstatus'+id ; 
	if(http_req_alerts_tds.readyState==4){ 
        if (http_req_alerts_tds.status == 200){
            var allData = http_req_alerts_tds.responseText; 
            if(allData){      
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataHighlight = dataSplit[2];	 
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(tdsstatusid).innerHTML= dataStatus;  
				highlightid = 'row'+dataHighlight ; 
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight"); 
				}     
            }
        }
    }else{       
		document.getElementById("message").innerHTML= "";  
    }
}
//Function to update the Order TDS status using AJAX BOF */

var http_req_alerts_npa;
function updateNPAStatus( id,status){
 var del=reallyDel('Are you sure you want to Change the NPA-Status?');

  if(del==true){
	http_req_alerts_npa = false;	
	document.getElementById("orderid").value = id;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_npa = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_npa = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_npa = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_npa) {
        return false;
    }
    var uri = "get-orders-npa-status.php?id="+ id+"&status="+status; 
    http_req_alerts_npa.onreadystatechange = loadNPAStatus;
    http_req_alerts_npa.open('GET', uri, true);
    http_req_alerts_npa.send(null);
  }
}

function loadNPAStatus(){
     id = document.getElementById("orderid").value ;
	 statusid = 'npastatus'+id ;
	if(http_req_alerts_npa.readyState==4){
   
        if (http_req_alerts_npa.status == 200){
            var allData = http_req_alerts_npa.responseText;			 
            if(allData){  
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataTdsStatus = dataSplit[2];
                var dataCeoStatus = dataSplit[3];
                var dataHighlight = dataSplit[4];	
 				document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
				highlightid = 'row'+dataHighlight ;
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{
		document.getElementById("message").innerHTML= ""; 
    }
}


//Function to update the Order Followup status using AJAX BOF
var http_req_alerts_pro;
function updateProStatus( id,status){
 var del=reallyDel('Are you sure you want to Change the Production-Status?'); 
  if(del==true){
	http_req_alerts_pro = false;	
	document.getElementById("orderid").value = id;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_pro = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_pro = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_pro = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_pro) {
        return false;
    }
    var uri = "get-orders-production-status.php?id="+ id+"&status="+status; 
    http_req_alerts_pro.onreadystatechange = loadProStatus;
    http_req_alerts_pro.open('GET', uri, true);
    http_req_alerts_pro.send(null);
  }
} 

function loadProStatus(){
    id = document.getElementById("orderid").value ;
	statusid = 'prostatus'+id ;  
	if(http_req_alerts_pro.readyState==4){
   
        if (http_req_alerts_pro.status == 200){
            var allData = http_req_alerts_pro.responseText;			 
            if(allData){  
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
               // var dataTdsStatus = dataSplit[2];
                //var dataCeoStatus = dataSplit[3];
                var dataHighlight = dataSplit[2];	
 				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
                
				
				highlightid = 'row'+dataHighlight ;
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{   
		document.getElementById("message").innerHTML= "";  
    } 
}

//Function to update the Order Followup status using AJAX BOF
var http_req_alerts_daily;
function updateDailyStatus( id,status){
var del=reallyDel('Are you sure you want to Change the Daily Update Status?'); 
	if(del==true){
		http_req_alerts_pro = false;	
		document.getElementById("orderid").value = id;
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_req_alerts_pro = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_pro = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_pro = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_pro) {
			return false;
		}
		var uri = "get-orders-dailyupdate-status.php?id="+ id+"&status="+status; 
		http_req_alerts_pro.onreadystatechange = loadDailyStatus; 
		http_req_alerts_pro.open('GET', uri, true);
		http_req_alerts_pro.send(null);
	}
}

function loadDailyStatus(){
    id = document.getElementById("orderid").value ;
	statusid = 'dailystatus'+id ;  
	if(http_req_alerts_pro.readyState==4){
   
        if (http_req_alerts_pro.status == 200){
            var allData = http_req_alerts_pro.responseText;			 
            if(allData){  
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
               // var dataTdsStatus = dataSplit[2];
                //var dataCeoStatus = dataSplit[3];
                var dataHighlight = dataSplit[2];	
 				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
                
				
				highlightid = 'row'+dataHighlight ;
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
        
        
    }

}






//Function to update the Order Followup status using AJAX BOF
var http_req_alerts;
function updateFYr(company_id) {  
    var cboType = document.getElementById("accounthead");
   
    cboType.options.length = 0;
    if(!type){
        cboType.options[0] = new Option("", "");	
        return null;
    }
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""

    var uri = "get-financial-yrs.php?company_id=" + company_id; 
    http_req_alerts.onreadystatechange = loadFYr;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

/**
* To Load account head depending on transaction type in payment in/out module
**/
function loadFYr(){
    var cboType = document.getElementById("financialYr");
    if(http_req_alerts.readyState==4){   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
            if(allThoseTypes){
                cboType.options[0] = new Option("Select financial Yr", "0");
                cboType.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");            
                for (var i=0;i<typeSplit.length;i++){
                    var tmpStr =typeSplit[i];
                    if(tmpStr){
                        var tmpArray = new Array();
                        tmpArray = tmpStr.split("|");
                        var tmpName = tmpArray[0];
                        var tmpVal = tmpArray[1];
                       
                        if(tmpName){
                            cboType.options[i+1] = new Option(tmpName, tmpVal);                           
                        }
                    }
                }           
            }
        }
    }else{        
        cboType.options[0] = new Option("Loading...", "");
        cboType.style.background = "#c3c3c3";
    }
}

function showInvoice(inv_id, file_type) {	
	if (file_type == 'htmlprint' || file_type == 'html' ||  file_type == 'pdf') {      
        url = "./bill-invoice.php?perform=view_file"	
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Invoice','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}
function showInvoiceProfm(inv_id, file_type) {
   if (file_type == 'htmlprint' || file_type == 'html' ||  file_type == 'pdf') {      
        url = "./bill-invoice-profm.php?perform=view_file"
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Invoice','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}
function addClientSuInfo(executive_arr,divname,fieldname){
    div_executive = document.getElementById(divname); 

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', ''+fieldname+'[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

       
        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', ''+fieldname+'_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
       // exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove'+ executive_arr[1]);
        exec_remove_a.setAttribute('onclick', "javascript:removeClientSu('"+ executive_arr[0] +"','"+divname+"');");
            // Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}

function removeClientSu(id,divname) {
 
    div_executive = document.getElementById(divname);
 
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
   
}

function addClientInfo(client_arr) {
    div_client = document.getElementById("client_info");

    client_detail_div = document.createElement('div');
    client_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'client');
        client_id_input.setAttribute('value', client_arr[0]);

        client_det_input = document.createElement('input');
        client_det_input.setAttribute('type', 'text');
        client_det_input.setAttribute('name', 'client_details');
        //client_det_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +") ("+ client_arr[4] +")");
        client_det_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +")");
        client_det_input.setAttribute('size', '60');
        //client_det_input.setAttribute('readonly', 'readonly');
        client_det_input.setAttribute('class', 'inputbox_c');

        client_detail_div.appendChild(client_id_input);
        client_detail_div.appendChild(client_det_input);

    div_client.innerHTML = '<span class="txthelp">The Client for whom the Order is created.</span>';
    div_client.appendChild(client_detail_div);
    return (true);
}


function addExecutive(executive_arr) {
    div_executive = document.getElementById("executive_info");

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'team[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'team_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove'+ executive_arr[1]);
        exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
            // Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}

function removeExecutive(id) {
 
    div_executive = document.getElementById("executive_info");
  
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
   
}

function showClientDetails(client_id) {
    window.open('./clients.php?perform=view&user_id='+client_id,'clientDetails','left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1');
    return (true);
}
function showExecutiveDetails(executive_id) {
    window.open('./user.php?perform=view&user_id='+executive_id,'executiveDetails','left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1');
    return (true);
}
function showFileAttachment(file_name) {
    
    url = "./bill-pre-order.php?perform=view_attachment"
    url = url + "&file_name="+ file_name;
    //url = url + "&type="+ file_type;
    window.open(url,'File','left=280,top=200,width=350,height=300,toolbar=0,resizable=1,scrollbars=1')
}
function showOrderAttachment(file_name) {
   
    url = "./bill-order.php?perform=view_attachment"
    url = url + "&file_name="+file_name;
    window.open(url,'File','left=280,top=200,width=350,height=300,toolbar=0,resizable=1,scrollbars=1')
}
/* added for particulars calculation bof */





function showQuotation(q_id, file_type) {
  
    if(file_type == 'html' || file_type =='pdf'){
        url = "./sale-quotation.php?perform=view_file"
        url = url + "&q_id="+ q_id;
        url = url + "&type="+ file_type;
        window.open(url,'Quotation','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}

function clearField(field){

  field.value='';

}
function markComplete(str) {
    
    
	if ( !str ) {
		str = "Do you really want to mark this Order as completed? \nNote: It will mark Today as Completion date Of this Order.";
	}
	if ( confirm(str) ) {
		return true;
	}
	return false;
}


function addBifurcations(element, s_service) {
   counter = document.getElementById('scount').value;
   counter = parseInt(counter) + 1;
   company_div = document.createElement('div');
   company_div.setAttribute('id', counter);
   
    // Create the main container Div
    company_table = '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tbl_form" style="margin:0px;padding:0px;">';
    company_table += '<tr>';
    company_table += '<td  style="padding-top:10px;"><a href="javascript:void(0);" class="search-head-txt" onclick="javascript:removeBifurcations('+counter+');" >Remove</a></td>';
    company_table += '</tr>';
    company_table += '<tr>';
    company_table += '<td>';
    company_table += '<table cellpadding="0" cellspacing="0" border="0" width="100%" align="left" style="margin:0px;padding:0px;">';
    company_table += '<tr>';
    company_table += '<td>';
    company_table += '<select name="service[]" class="select-box" style="width:185px;" ><option value="">Select service</option>';
    for (i=0;i<s_service.length;i++) {
        company_table += '<option value="'+s_service[0][i]+'">'+ s_service[1][i]+'</option>';
    }
    company_table += '</select></td>';
    company_table += '<td>Price : <input type="text" name="price[]" value="" class="inputbox" maxlength="255" size="15"></td>';
    company_table += '<td valign="top">Details : </td><td><textarea name="details[]" class="inputbox" cols="20" rows="5"></textarea>';
    company_table += '<input type="hidden" name="count[]" value="'+counter+'" class="inputbox" maxlength="255" size="15"></td>';   
    company_table += '</tr>';   
    company_table += '</table></td></tr>';
    company_table += '</table>';   
    company_div.innerHTML = company_table;
    element.appendChild(company_div);   
    document.getElementById('scount').value = parseInt(document.getElementById('scount').value) + 1;
    
    return (true);
}

function removeBifurcations(id) {

    div_bifur = document.getElementById("bifurcations");
    if ( div_bifur.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}
function showOrderSRS(order_id) {
	param = 'left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1';
    window.open('./project-task.php?perform=srs&or_id='+order_id, 'orderSRS', param);
    return (true);
}


function clearAutoReferred(){
 document.getElementById('ord_referred_by_name').value='';
 document.getElementById('ord_referred_by').value='';
 document.getElementById('ord_referred_amount').value=''; 


}
function clearAutoDiscount(){
 document.getElementById('ord_discount_to_name').value='';
 document.getElementById('ord_discount_to').value='';
 document.getElementById('ord_discount_amt').value='';
 document.getElementById('ord_discount_against_no').value='';
 document.getElementById('ord_discount_against_id').value='';


}

function addOrderReferred(ord_arr) {
    div_ord = document.getElementById("ord_refr");
 
  document.getElementById('ord_discount_amt').value=ord_arr[2]  ;
  document.getElementById('ord_discount_against_no').value=ord_arr[1];
  document.getElementById('ord_discount_against_id').value=ord_arr[0];

	
    /* ord_detail_div = document.createElement('div');
    ord_detail_div.setAttribute('class', 'row');
        // Create the Hidden User ID field.
       
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'client');
        client_id_input.setAttribute('value', ord_arr[0]);

        client_det_input = document.createElement('input');
        client_det_input.setAttribute('type', 'text');
        client_det_input.setAttribute('name', 'client_details');
        client_det_input.setAttribute('value', ord_arr[2] +" "+ ord_arr[3] +" ("+ ord_arr[1] +")");
        client_det_input.setAttribute('size', '60');
        client_det_input.setAttribute('class', 'inputbox_c');

        ord_detail_div.appendChild(client_id_input);
        ord_detail_div.appendChild(client_det_input);

    div_ord.innerHTML = '<span class="txthelp">The Client for whom the Order is created.</span>';
    div_ord.appendChild(ord_detail_div); */
    return (true);
}