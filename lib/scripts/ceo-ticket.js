//AJAX NAVIGATION FUNCTION BOF

function changeRecordList(page,pagename,form1){ 
	 
	http_req_alerts = false;			
	if (window.XMLHttpRequest) { //Mozilla, Safari,...
		http_req_alerts = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts) {
		return false;
	}

	//var poststr = createQueryString(form1);	
	//var poststr =Url.decode($(form1).serialize()) ;
	
	var slist = document.getElementById('sStatus'); 
	st_options = "";
	for(var i = 0; i < slist.options.length; ++i){
		comma = ",";
		if(slist.options[i].selected){
			st_options = st_options + slist[i].value + comma;
		}
	}
	st_options = trim(st_options,",");
	
	var tslist = document.getElementById('tStatus'); 
	tst_options = "";
	for(var i = 0; i < tslist.options.length; ++i){
		comma = ",";
		if(tslist.options[i].selected){
			tst_options = tst_options + tslist[i].value + comma;
		}
	}
	tst_options = trim(tst_options,",");
	 
	var chk_status=chk_t_status=chk_date_from=chk_date_to='';
	if(document.getElementById("chk_status").checked){
		chk_status = 'AND' ;
	}
	if(document.getElementById("chk_t_status").checked ){
		chk_t_status = 'AND' ;
	}
	if(document.getElementById("chk_date_from").checked){
		chk_date_from = 'AND' ;
	}
	if(document.getElementById("chk_date_to").checked ){
		chk_date_to = 'AND' ;
	}
	
	 
	 
	poststr = "sString="+document.getElementById("sString").value+"&sType="+document.getElementById("sType").value+
	"&sOrderBy="+document.getElementById("sOrderBy").value+"&sOrder="+document.getElementById("sOrder").value+"&search_client="+document.getElementById("search_client").value+
	"&chk_status="+chk_status+"&sStatus="+st_options+"&chk_t_status="+chk_t_status+"&tStatus="+tst_options+
"&chk_date_from="+document.getElementById("chk_date_from").value+
	"&date_from="+document.getElementById("date_from").value+"&date_to="+document.getElementById("date_to").value+"&chk_date_to="+chk_date_to+"&dt_field="+document.getElementById("dt_field").value+"&perform="+document.getElementById("perform").value+"&rpp="+document.getElementById("rpp").value ;
	 
	 
	var uri = pagename+"?ajx=1&x="+page+"&"+poststr ; 
	 
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';	
	$('#inner_content').load(uri);
	
 
} 

function loadRecordList(){
	if(http_req_alerts.readyState==4){
        if (http_req_alerts.status == 200){
            var allData = http_req_alerts.responseText;
            if(allData){
			    document.getElementById("record_info").innerHTML = allData; 
            }
        }
    }else{
		document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';         
    }
} 
//AJAX PAGINATION FUNCTION EOF
function clearSearch(){
	document.getElementById("sString").value='' ;
	document.getElementById("sType").value='' ;
	document.getElementById("sOrderBy").value='do_add' ;
	document.getElementById("sOrder").value='DESC' ;
	document.getElementById("date_from").value='' ;
	document.getElementById("date_to").value='' ;	 
}

function searchRecord(pagename,form1){
	changeRecordList(1,pagename,form1);
	document.getElementById("pageno").value=1 ;	
	return false;
}  
function editStatus(surl){
	ajxUrl= surl +"&ajx=1" ;
	var del=reallyDel('Are you sure you want to change the Status?');

	if(del==true){
	
		
		$( "#dialog-form" ).dialog({
				autoOpen: false,
				height: 300,
				width: 500,
				resizable: true,
				closeText: 'close',
				title: 'SMEERP - NERVE CENTER',
				modal: true,
				close: function() {
					
					//searchRecord('ceo-ticket.php',frmSearch);
					searchRecord(ajxUrl,frmSearch);
					//changeRecordList(pageno,'prospects.php',frmSearch);
					//$( "#dialog-form" ).dialog( "destroy" );
				}
		});
		
		$("#dialog-form").dialog("open");
		var diFrame =  document.getElementById('dialogIframeId');
		if (navigator.appName.indexOf("Microsoft")!=-1) {
			diFrameBody = document.frames['dialogIframeId'].document;
		}else{
		  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
		}
		diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
		$("#dialogIframeId").attr("src",ajxUrl);
		
	}
	return false;
}

function addRecord(surl){

	 ajxUrl= surl +"&ajx=1" ;
	$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 900,
			resizable: true,
			closeText: 'close',
			title: 'SMEERP - NERVE CENTER',
			modal: true,
			close: function() {
			
				var pageno = document.getElementById("pageno").value ;
				//url = SITE_URL+"/"+ADMIN_PANEL+"/ceo-ticket.php?perform=list&pageno="+pageno ;				
				//changeRecordList(pageno,'ceo-ticket.php',frmSearch);
				changeRecordList(pageno,ajxUrl,frmSearch);
				//$("#dialog-form" ).dialog("destroy");
			}
	});
   
 
    $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;
	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
    $("#dialogIframeId").attr("src",ajxUrl);
    return false; 
}
 
function listRecord(surl){
	
	ajxUrl= surl +"&ajx=1" ;
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';
	$('#inner_content').load(ajxUrl);
	   return false; 
}
/*AJAX NAVIGATION EOF*/

//Function to update the Banner status using AJAX BOF
var http_req_alerts_banner;
function updateStAllStatus( id,status){
	var del=reallyDel('Are you sure you want to Change the Status?');

  if(del==true){
	http_req_alerts_banner = false;	
	document.getElementById("ticketid").value = id;

	
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    } 
   
    var uri = "get-support-ticket-all-status.php?ticket_id="+id+"&status="+status;    
	
    http_req_alerts_banner.onreadystatechange = loadStAllStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);
  }
}
function loadStAllStatus(){
    id = document.getElementById("ticketid").value ;
	statusid = 'status'+id ;
	cstatusid = 'cstatus'+id ;
	 
	if(http_req_alerts_banner.readyState==4){

        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
		 
            if(allData){            
            
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataCStatus = dataSplit[2];
                var dataEdit = dataSplit[3];
				var dataHighlight = dataSplit[4];
				highlightid = 'row'+dataHighlight ;
			 
				editid = 'edit'+dataHighlight ;
			
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus;                 
                document.getElementById(cstatusid).innerHTML= dataCStatus;                 
                document.getElementById(editid).innerHTML= dataEdit; 
                
                
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
        
        
    }

}
//Function to update the Banner status using AJAX BOF

function markClientVisibility(ele,alt,divname1,divname2){

    var el = document.getElementsByName(alt);
    if(ele.checked){
        document.getElementById(divname1).style.display ='block';
        document.getElementById(divname2).style.display ='block';
        el[0].checked=true ;
    }else{
        document.getElementById(divname1).style.display ='none';
        document.getElementById(divname2).style.display ='none';
        el[0].checked=false ;
    }
}

function addClientInfo(client_arr) {
    div_client = document.getElementById("client_info");

    client_detail_div = document.createElement('div');
    client_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'client');
        client_id_input.setAttribute('value', client_arr[0]);

        client_det_input = document.createElement('input');
        client_det_input.setAttribute('type', 'text');
        client_det_input.setAttribute('name', 'client_details');
        client_det_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +") ("+ client_arr[4] +")");
        client_det_input.setAttribute('size', '60');
        client_det_input.setAttribute('readOnly', '1');        
        client_det_input.setAttribute('class', 'inputbox_c');

        client_detail_div.appendChild(client_id_input);
        client_detail_div.appendChild(client_det_input);

    div_client.innerHTML = '<span class="txthelp">The Client for whom the support ticket is created.</span>';
    div_client.appendChild(client_detail_div);
    return (true);
}


function addExecutive(executive_arr,div) {

    div_executive = document.getElementById(div);
	
        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', 'remove_'+ executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'assign_members[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];
 
        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'assign_members_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ executive_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeExecutive('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}


/**
 * This function is used to un-relate a Executive from a Customer.
 *
 */
function removeExecutive(id) {
    div_executive = document.getElementById("executive_info");

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}
var http_req_alerts1;
function updateTemplate(template_id){

    http_req_alerts1 = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts1 = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts1 = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts1 = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    if (!http_req_alerts1) {
        return false;
    } 
    var uri = "get-st-template-details.php?template_id=" + template_id;    
    http_req_alerts1.onreadystatechange = loadTemplate;
    http_req_alerts1.open('GET', uri, true);
    http_req_alerts1.send(null);

}
function loadTemplate(){
	
	isTitleLd = document.getElementById("isTitleLd").value ;
    if(http_req_alerts1.readyState==4){
        if (http_req_alerts1.status == 200){
            var allThoseTypes = http_req_alerts1.responseText;
			dataSplit = allThoseTypes.split("~");
			templateFileStr = '';
			ticket_text = dataSplit[0];
			ticket_subject = dataSplit[1];
			template_id = dataSplit[2];
			template_file_1 = dataSplit[3];
			template_file_2 = dataSplit[4];
			template_file_3 = dataSplit[5];
            document.getElementById("ticket_text").value =  ticket_text.replace(/^\s+|\s+$/g,"");
			document.getElementById("template_file_1").value =template_file_1 ;
			document.getElementById("template_file_2").value =template_file_2 ;
			document.getElementById("template_file_3").value =template_file_3 ;
			
			if(template_file_1!=''){				
				templateFileStr = templateFileStr + "<a onclick=\"javascript:downloadStTplFile('"+template_id+"','"+template_file_1+"')\" href=\"javascript:void(0);\">Attachment1 </a>";
			}
			if(template_file_2!=''){				
				templateFileStr = templateFileStr + "<a onclick=\"javascript:downloadStTplFile('"+template_id+"','"+template_file_2+"')\" href=\"javascript:void(0);\">Attachment2 </a>";
			}
			if(template_file_3!=''){				
				templateFileStr = templateFileStr + "<a onclick=\"javascript:downloadStTplFile('"+template_id+"','"+template_file_3+"')\" href=\"javascript:void(0);\">Attachment3 </a>";
			} 
			 
			 
			document.getElementById("templateFile").innerHTML = templateFileStr ;
			
			if(isTitleLd==1){			
				document.getElementById("ticket_subject").value =  ticket_subject;
			}
        }
    }
}

//

var http_req_alerts_ct;
function updateTemplateCategory(id,type_txt){
	
	 
	http_req_alerts_ct = false;		
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_req_alerts_ct = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts_ct = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts_ct = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts_ct) {
		return false;
	} 
   
	var uri = "get-st-templates.php?category_id="+ id+"&type_txt="+type_txt    ; 

	http_req_alerts_ct.onreadystatechange = loadTemplateCategory;
	http_req_alerts_ct.open('GET', uri, true);
	http_req_alerts_ct.send(null);
 
}

function loadTemplateCategory(){   	 
	
    var template_id = document.getElementById('template_id');
	var isTitleLd = document.getElementById("isTitleLd").value ;
	
	if(http_req_alerts_ct.readyState==4){  

        if (http_req_alerts_ct.status == 200){
            var optionData = http_req_alerts_ct.responseText;			 
			 
			if(optionData!=''){
				template_id.options.length=0;
				template_id.options[0] = new Option("Select template", " ");
				template_id.style.background ="#ffffff";
				var optSplit =new Array();				 
				optSplit = optionData.split("~");
			
				for (var i=0;i<optSplit.length;i++){
					var tmpStr =optSplit[i];
					if(tmpStr){
						var tmpArray = new Array();
						tmpArray = tmpStr.split("|");
						var tmpName = tmpArray[0];
						var tmpVal = tmpArray[1];
						if(tmpName){
							template_id.options[i+1] = new Option(tmpName, tmpVal);    
							template_id.options[i+1].title= tmpName;
						}
					}
				}       
            }else{	
				template_id.options.length=0;
				template_id.options[0] = new Option("Select template", " ");
			}
			document.getElementById("templateFile").innerHTML = "";
			document.getElementById("ticket_text").value = '';
			
			if(isTitleLd==1){
				document.getElementById("ticket_subject").value = '';
			}
        }
    }else{     
       
        //template_id.options[0] = new Option("Loading...", "");
        //template_id.style.background = "#c3c3c3";
    }

}
//AJX Function to load the sub services EOF

//


var http_req_alerts;

function showReplyTo(ticket_id) {
	
    document.getElementById("replyto").innerHTML="";
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    if (!http_req_alerts) {
        return false;
    } 
    var uri = "get-cst-eml-details.php?ticket_id=" + ticket_id;    
    http_req_alerts.onreadystatechange = loadReplyTo;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}


function loadReplyTo(){
   
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            document.getElementById('replyto').innerHTML = " ";
            
            var allThoseTypes = http_req_alerts.responseText;
            
            if(allThoseTypes){			
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split("|");
                
                var to_1 = typeSplit['0'];
                var cc_1 =typeSplit['1'];
                var bcc_1 =typeSplit['2'];
                var to =typeSplit['3'];
                var cc =typeSplit['4'];
                var bcc =typeSplit['5'];
            }
            
            if(to){
                document.getElementById('show_to').className = "visible";
                document.getElementById("to_1").value=to_1;
                document.getElementById("to_email_1").value=to_1;
                document.getElementById("to").value=to;
            }
            if(cc){
                document.getElementById('show_cc').className = "visible";
                document.getElementById("cc_1").value=cc_1;
                document.getElementById("cc_email_1").value=cc_1;
                document.getElementById("cc").value=cc;
            }
            if(bcc){
                document.getElementById('show_bcc').className = "visible";
                document.getElementById("bcc_1").value=bcc_1;
                document.getElementById("bcc_email_1").value=bcc_1;
                document.getElementById("bcc").value=bcc;
            }
            //document.getElementById("replyto").innerHTML="To : "+to+"<br/> Cc : "+cc+"<br/> Bcc : "+bcc;
        }
    }else{ 
            document.getElementById("replyto").innerHTML="<img src='../media/images/nc/loading.gif' border=0/><br>Loading....Please Wait.";
    }
}
 
function showStAllFile(id,file){
	if (id != '') {      
        url = "./support-ticket-all.php?perform=download_file" ;
        url = url + "&id="+id+'&file='+ file;        
        window.open(url,'Download file','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}

function showStTplFile(id,file) {

   if (id != '') {      
        url = "./support-ticket-template.php?perform=download_file" ;
        url = url + "&id="+id+'&file='+ file;        
        window.open(url,'Download file','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}
function downloadStTplFile(id,file) {

   if (id != '') {      
        url = "./ceo-ticket.php?perform=download_file";
        url = url +"&id="+id+"&file="+file;        
        window.open(url,'Download file','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1');
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}
function showServiceFile(id,file) {

   if (id != '') {      
        url = "./services.php?perform=download_file" ;
        url = url + "&id="+id+'&file='+ file;        
        window.open(url,'Download file','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}

function showInvoice(inv_id, file_type) {


   if (file_type == 'htmlprint' || file_type == 'html' ||  file_type == 'pdf') {      
        url = "./bill-invoice.php?perform=view_file"
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Invoice','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}

function showInvoiceProfm(inv_id, file_type) {

   if (file_type == 'htmlprint' || file_type == 'html' ||  file_type == 'pdf') {      
        url = "./bill-invoice-profm.php?perform=view_file"
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Invoice','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}