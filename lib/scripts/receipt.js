/**
 * This function is used to add new elements
 * to the existing HTML elements.
 * This particular function adds a DIV tag to the parent 
 * element identified by 'element'.
 *
 * @param   HTML Element    this can be reference to any HTML element eg. DIV, TABLE, TD, P.
 * 
 *
 */
function addParticularField(element) {

    // Create the main container Div
    // <div class="row">
    div_main = document.createElement('div');
    div_main.setAttribute('class', 'row');
    
        // <div class="coloumn vb">
        div_col1 = document.createElement('div');
        div_col1.setAttribute('class', 'coloumn vb');
            // Create the TEXTAREA field.
            text_parti = document.createElement('textarea');
            text_parti.setAttribute('name', 'particulars[]');
            text_parti.setAttribute('class', 'inputbox');
            text_parti.setAttribute('cols', '50');
            text_parti.setAttribute('rows', '1');
            text_parti.setAttribute('style', 'height:17px;');
            text_parti.setAttribute('onkeypress', 'javascript: return increaseHeight(this, event);');
        div_col1.appendChild(text_parti);
        
        // <div class="coloumn vb">
        div_col2 = document.createElement('div');
        div_col2.setAttribute('class', 'coloumn pl5 vb');
            // Create the Amount field.
            // <input type="text" name="p_amount[]" value="" class="inputbox" size="10"/>
            input_amount = document.createElement('input');
            input_amount.setAttribute('type', 'text');
            input_amount.setAttribute('name', 'p_amount[]');
            input_amount.setAttribute('value', '0');
            input_amount.setAttribute('onblur', 'javascript: return updateTotal();');
            input_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
            input_amount.setAttribute('class', 'inputbox');
            input_amount.setAttribute('size', '7');
            
            input_pid = document.createElement('input');
            input_pid.setAttribute('type', 'hidden');
            input_pid.setAttribute('name', 'p_id[]');
            input_pid.setAttribute('value', '');
        div_col2.appendChild(input_pid);
        div_col2.appendChild(input_amount);
    
    div_main.appendChild(div_col1);
    div_main.appendChild(div_col2);
    
    element.appendChild(div_main);
    return (true);
}

function hwndAddParticularField(element, e) {
    var e = e? e : window.event;
    if(!e) 
    return;
    
    var key = 0;
    if (e.keycode) {
        key = e.keycode;
    } // for moz/fb, if keycode==0 use 'which'
    else if (typeof(e.which)!= 'undefined') { 
        key = e.which; 
    } 
    
    if ( key==13 ) {
        addParticularField(document.getElementById('particular_details'));
        return (false);
    }
    return (true);
}


function increaseHeight(element, e) {
    var e = e? e : window.event;
    if(!e) 
    return;
    
    var key = 0;
    if (e.keycode) {
        key = e.keycode;
    } // for moz/fb, if keycode==0 use 'which'
    else if (typeof(e.which)!= 'undefined') { 
        key = e.which; 
    } 
    
    if ( key==13 ) {
        height = element.style.height;
        height = parseInt(height.substr(0, (height.length-2)));
        element.style.height = (height + 20) + 'px';
    }
    return (true);
}


function updateTotal() {
    /*
    p_amount  = document.getElementsByName('p_amount[]');
    dst_amount  = document.getElementsByName('d_amount[]');
    stot_amount  = document.getElementsByName('stot_amount[]');
    tax1_value  = document.getElementsByName('tax1_value[]');
	tax1_pvalue  = document.getElementsByName('tax1_pvalue[]');
    tot_amount  = document.getElementsByName('tot_amount[]');
    
    var total   = 0.0;

    for ( i=0; i< p_amount.length; i++ ) {
    
        if ( parseFloat(p_amount[i].value) ) {
            //total = total + parseFloat(lst_amount[i].value) ;
        }
        if(dst_amount[i].value != 0){
            stot_amount[i].value =  parseFloat(p_amount[i].value) - parseFloat(dst_amount[i].value) ;
        }else{
            stot_amount[i].value =  parseFloat(p_amount[i].value) ;
        }
       
       
        //tot_amount[i].value = adjustAmount(parseFloat(stot_amount[i].value),tax1_value[i].value);
		tot_amount[i].value = addPercentAmount(parseFloat(stot_amount[i].value),tax1_pvalue[i].value);
        total = parseFloat(total) + parseFloat(tot_amount[i].value) ;

	}
   
    temp = document.frmInvoiceAdd.octroi.value;
    total = adjustAmount(total, temp) ;
 
    document.frmInvoiceAdd.amount.value = total;
    */
    
    amount = document.frmInvoiceAdd.amount.value;
    exchange_rate = document.frmInvoiceAdd.exchange_rate.value;
    
    document.frmInvoiceAdd.amount_inr.value = parseFloat(amount) * parseFloat(exchange_rate);
	
}
//new added for discount
function subtractAmount(amount, adjustment) {
    index = adjustment.indexOf('%');
    if ( index > 0 ) {
        adjustment = parseFloat(adjustment.substr(0, index));
        adjustment = parseFloat(adjustment/100);
        adjustment = parseFloat(amount * adjustment);
        amount = parseFloat(amount) - parseFloat(adjustment);
    }
    else {
        if ( adjustment = parseFloat(adjustment) ) {
            amount = parseFloat(amount) - parseFloat(adjustment);
        }
    }
    //amount = parseFloat(parseInt((amount+0.005)*100.0)/100.0);
    amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
    return (amount);
}
function adjustAmount(amount, adjustment) {
    index = adjustment.indexOf('%');
    if ( index > 0 ) {
        adjustment = parseFloat(adjustment.substr(0, index));
        adjustment = parseFloat(adjustment/100);
       
        adjustment = parseFloat(amount * adjustment);
        //adjustment = parseFloat(amount) * parseFloat(adjustment);
        amount = parseFloat(amount) + parseFloat(adjustment);
    }
    else {
        if ( adjustment = parseFloat(adjustment) ) {
            amount = parseFloat(amount) + parseFloat(adjustment);
        }
    }
   // amount = parseFloat(parseInt((amount+0.005)*100.0)/100.0);
    amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
    
    return (amount);
}
//new function added 2009-04-02 in replace of adjustAmount
function addPercentAmount(amount, taxrate){
	amount = parseFloat(amount) ; 
	taxrate = parseFloat(taxrate) ; 
	adjustment = parseFloat(amount * taxrate);
    //adjustment = parseFloat(amount) * parseFloat(adjustment);
    amount = parseFloat(amount) + parseFloat(adjustment);
	 amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
    
    return (amount);
}

function showInvoice(inv_id, file_type) {


   if ( file_type == 'html' ||  file_type == 'pdf') {      
        url = "./bill-invoice.php?perform=view_file"
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Invoice','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}

function showReceipt(rcpt_id, file_type) {
    if ( file_type == 'html' ||  file_type == 'pdf') {    
        url = "./bill-receipt.php?perform=view_file"
        url = url + "&rcpt_id="+ rcpt_id;
        url = url + "&type="+ file_type;
        window.open(url,'Invoice','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
    
        alert("Invalid file type "+ file_type);
        return (false);
    }
}
function showClientDetails(client_id) {
    window.open('./clients.php?perform=view&user_id='+client_id,'clientDetails','left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1');
    return (true);
}

//ajax code for on service select display textarea/ price BOF
var http_req_alerts;


function updateParticulars(serviceIdStr) {
    //var serviceIdStr = document.getElementById("serviceIdStr").value;
    
   
   
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    }
 
    var uri = "getservices.php?serviceIdStr=" + serviceIdStr;
    
    http_req_alerts.onreadystatechange = loadParticulars;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

function loadParticulars(){
    var partcularVal =new Array();
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            document.getElementById("loading").innerHTML='';
            
          // var contentStr ;
            //contentStr =  http_req_alerts.responseText;            
             //   alert(document.getElementById("particularsub").innerHTML);
           //document.getElementById("particularsub").innerHTML =  contentStr;            
           document.getElementById("particulars_s").innerHTML +=  http_req_alerts.responseText;     

            maxval= document.frmInvoiceAdd.service_id.length;
            
            
          /*
          for(var i=0; i<maxval ; i++){
                if(document.frmInvoiceAdd.service_id[i].checked){
                   
                  document.frmInvoiceAdd.particulars[i].value = partcularVal[i] ;
                }
                
           }*/
            //document.getElementById("particulars_s").innerHTML = document.getElementById("particularsub").innerHTML;            
             
            
        }
    }else{
        
        document.getElementById("loading").innerHTML = "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""
                    +" alt=\"Loading\" />......please wait"; 
            
    }
}


/*
 * Function to add code to set values of selected product ids separated by ','  
 * as into  hidden input field and code delete product id if user has unchecked the selected/checked product.
 */
function collectServiceIds(chkElement)
{

    

	try 
    {
        if (chkElement.checked == true)
        {
            if ( document.getElementById('serviceIdStr') && document.getElementById('serviceIdStr').value) 
            {	
                if ( document.getElementById('serviceIdStr').value == "" ) 
                {
                    document.getElementById('serviceIdStr').value = chkElement.value;
                }
                else 
                {
                    document.getElementById('serviceIdStr').value = document.getElementById('serviceIdStr').value +"," + chkElement.value;
                }
            }
            else 
            {
                document.getElementById('serviceIdStr').value = chkElement.value;
            }
        }
        else
        {
            var compProdIds = document.getElementById('serviceIdStr').value.split(',');
            document.getElementById('serviceIdStr').value = '';
            for (i=0; i<compProdIds.length; i++)
            {
                if (compProdIds[i] == chkElement.value)
                {
                }
                else
                {
                    if(document.getElementById('serviceIdStr').value == "")
                    {
                        document.getElementById('serviceIdStr').value = compProdIds[i];                    
                    }
                    else
                    {
                        document.getElementById('serviceIdStr').value = document.getElementById('serviceIdStr').value +"," + compProdIds[i];
                    }
                }
            }
        }
	}
	catch(e) {
		document.getElementById('serviceIdStr').value = chkElement.value;
	}
    updateParticulars();

    
	//return (true);
}

function popUpSubServices(chkElement){
    if (chkElement.checked == true)
    {
        parent_id= chkElement.value;
        //updateParticulars(parent_id); this is for ajax
        
        //code to create the fields on fly for for selected parent service id bof
        
        stitleid = "servTitle"+parent_id ;
        stitle = document.getElementById(stitleid).value ;
       
        spunchlineid = "servPunch"+parent_id ;
        spunchline = document.getElementById(spunchlineid).value ;
        
        taxnameid = "servTaxname"+parent_id ;
        taxname = document.getElementById(taxnameid).value ;
        
        
        taxvalueid = "servTaxvalue"+parent_id ;
        taxvalue = document.getElementById(taxvalueid).value ;
        
        taxpvalueid = "servTaxpvalue"+parent_id ;
        taxpvalue = document.getElementById(taxpvalueid).value ;
        
        currency_id =  document.getElementById('currency_id').value;
        
        addParticularDetails(document.getElementById('particularsub'),parent_id,stitle,spunchline,taxname,taxvalue,taxpvalue);        
        
        //code to create the fields on fly for for selected parent service id eof
        
        window.open('./services.php?perform=subservices&parent_id='+parent_id+'&currency_id='+currency_id,'Services','left=20,top=20,width=750,height=400,toolbar=0,resizable=1,scrollbars=1');
    }else{
        parent_id= chkElement.value;
        //document.getElementById(parent_id).innerHTML ='';
        //document.getElementById('particularsub1').RemoveChild(div_main);
        
        div_particular =  document.getElementById("particularsub") ;
        div_particular.removeChild(document.getElementById(parent_id)) ; 
         
        
    }

}
//push value + sub services form popup window into parent window
function addServiceInfo(particular,priceVal,serviceid) {

    document.getElementById("part"+serviceid).value = particular;
    document.getElementById("pamt"+serviceid).value = priceVal;
    return (true);
}


// create particular  fields on  fly bof
function addParticularDetails(element,parentServiceId,title,punchline,taxname,taxvalue,taxpvalue) {    
   
   
    div_main = document.createElement('div');
    div_main.setAttribute('id', parentServiceId);
        div_row = document.createElement('div');
        div_row.setAttribute('class', 'row');
            div_col = document.createElement('div');
            div_col.setAttribute('class', 'coloumn vb');
            
                // Create the TEXTAREA field.
                item = document.createElement('input');
                item.setAttribute('type', 'text');
                item.setAttribute('name', 'itemname[]');
                item.setAttribute('value', title+' '+punchline);
                item.setAttribute('class', 'inputbox');
                item.setAttribute('disabled', '1');
                item.setAttribute('cols', '28');
                item.setAttribute('rows', '4');
                item.setAttribute('id', 'item'+parentServiceId);
                item.setAttribute('style', 'height:20px;width:350px');
                
            div_col.appendChild(item);
            div_row.appendChild(div_col);
            
        
        
        
        div_row1 = document.createElement('div');
        div_row1.setAttribute('class', 'row');
            div_colt1 = document.createElement('div');
            div_col_txt1 = document.createTextNode('Particulars');
            
            div_col1 = document.createElement('div');
            div_col1.setAttribute('class', 'coloumn vb');
                // Create the TEXTAREA field.
                text_parti = document.createElement('textarea');
                text_parti.setAttribute('name', 'particulars[]');
                text_parti.setAttribute('class', 'inputbox');
                text_parti.setAttribute('cols', '28');
                text_parti.setAttribute('rows', '4');
                text_parti.setAttribute('id', 'part'+parentServiceId);
                text_parti.setAttribute('style', 'height:45px;');
                text_parti.setAttribute('onkeypress', 'javascript: return increaseHeight(this, event);');
            div_colt1.appendChild(div_col_txt1);
            div_col1.appendChild(div_colt1);
            div_col1.appendChild(text_parti);
         
          
           
            div_colt2 = document.createElement('div');
            div_col_txt2 = document.createTextNode('Price');
            
            div_col2 = document.createElement('div');
            div_col2.setAttribute('class', 'coloumn pl5 vb');
                // Create the Amount field.
                // <input type="text" name="p_amount[]" value="" class="inputbox" size="10"/>
                input_amount = document.createElement('input');
                input_amount.setAttribute('type', 'text');
                input_amount.setAttribute('name', 'p_amount[]');
                input_amount.setAttribute('value', '0');
                input_amount.setAttribute('id', 'pamt'+parentServiceId);
                input_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_amount.setAttribute('class', 'inputbox');
                input_amount.setAttribute('size', '5');
                div_colt2.appendChild(div_col_txt2);
                div_col2.appendChild(div_colt2);
                
                input_pid = document.createElement('input');
                input_pid.setAttribute('type', 'hidden');
                input_pid.setAttribute('name', 'p_id[]');
                input_pid.setAttribute('value', '');
                
                input_s_id = document.createElement('input');
                input_s_id.setAttribute('type', 'hidden');
                input_s_id.setAttribute('name', 's_id[]');
                input_s_id.setAttribute('value', parentServiceId);
                
                
                input_ss_title = document.createElement('input');
                input_ss_title.setAttribute('type', 'hidden');
                input_ss_title.setAttribute('name', 'ss_title[]');
                input_ss_title.setAttribute('value', title);
                
                input_ss_punch_line = document.createElement('input');
                input_ss_punch_line.setAttribute('type', 'hidden');
                input_ss_punch_line.setAttribute('name', 'ss_punch_line[]');
                input_ss_punch_line.setAttribute('value', punchline);
                
                
                input_tax1_name = document.createElement('input');
                input_tax1_name.setAttribute('type', 'hidden');
                input_tax1_name.setAttribute('name', 'tax1_name[]');
                input_tax1_name.setAttribute('value', taxname);
                
                input_tax1_pvalue = document.createElement('input');
                input_tax1_pvalue.setAttribute('type', 'hidden');
                input_tax1_pvalue.setAttribute('name', 'tax1_pvalue[]');
                input_tax1_pvalue.setAttribute('value', taxpvalue);
            
          
            
            div_col2.appendChild(input_tax1_name);
            div_col2.appendChild(input_tax1_pvalue);
            div_col2.appendChild(input_ss_punch_line);
            div_col2.appendChild(input_ss_title);
            div_col2.appendChild(input_s_id);
            div_col2.appendChild(input_pid);
            div_col2.appendChild(input_amount);
            
            
            
        
            div_col3 = document.createElement('div');
            div_col3.setAttribute('class', 'coloumn pl5 vb');
            div_colt3 = document.createElement('div');
            div_col_txt3 = document.createTextNode('Discount');
                // Create the TEXTAREA field.
                input_d_amount = document.createElement('input');
                input_d_amount.setAttribute('type', 'text');
                input_d_amount.setAttribute('name', 'd_amount[]');
                input_d_amount.setAttribute('value', '0');
                input_d_amount.setAttribute('id', 'damt'+parentServiceId);
                input_d_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_d_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_d_amount.setAttribute('class', 'inputbox');
                input_d_amount.setAttribute('size', '5');
            div_colt3.appendChild(div_col_txt3);
            div_col3.appendChild(div_colt3);
            div_col3.appendChild(input_d_amount);
            
            div_col4 = document.createElement('div');
            div_col4.setAttribute('class', 'coloumn pl5 vb');
            div_colt4 = document.createElement('div');
            div_col_txt4 = document.createTextNode('SubTot');
                // Create the TEXTAREA field.
                input_stot_amount = document.createElement('input');
                input_stot_amount.setAttribute('type', 'text');
                input_stot_amount.setAttribute('name', 'stot_amount[]');
                input_stot_amount.setAttribute('value', '0');
                input_stot_amount.setAttribute('id', 'stotamt'+parentServiceId);
                input_stot_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_stot_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_stot_amount.setAttribute('class', 'inputbox');
                input_stot_amount.setAttribute('size', '5');
            div_colt4.appendChild(div_col_txt4);
            div_col4.appendChild(div_colt4);
            div_col4.appendChild(input_stot_amount);
            
            div_col5 = document.createElement('div');
            div_col5.setAttribute('class', 'coloumn pl5 vb');
            div_colt5 = document.createElement('div');
            if(taxname!=''){
                 div_col_txt5 = document.createTextNode(taxname);
            }else{
                div_col_txt5 = document.createTextNode('Tax');
            }
                // Create the TEXTAREA field.
                input_tax1_value = document.createElement('input');
                input_tax1_value.setAttribute('type', 'text');
                input_tax1_value.setAttribute('name', 'tax1_value[]');
                input_tax1_value.setAttribute('value', taxvalue);
                input_tax1_value.setAttribute('readonly', '1');
                input_tax1_value.setAttribute('id', 'txval'+parentServiceId);
                input_tax1_value.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tax1_value.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tax1_value.setAttribute('class', 'inputbox');
                input_tax1_value.setAttribute('size', '5');
            div_colt5.appendChild(div_col_txt5);
            div_col5.appendChild(div_colt5);
            div_col5.appendChild(input_tax1_value);
           
            div_col6 = document.createElement('div');
            div_col6.setAttribute('class', 'coloumn pl5 vb');
            
            div_colt6 = document.createElement('div');
            div_col_txt6 = document.createTextNode('Tot');
                // Create the TEXTAREA field.
                input_tot_amount = document.createElement('input');
                input_tot_amount.setAttribute('type', 'text');
                input_tot_amount.setAttribute('name', 'tot_amount[]');
                input_tot_amount.setAttribute('value', '0');
                input_tot_amount.setAttribute('readonly', '1');
                input_tot_amount.setAttribute('id', 'totamt'+parentServiceId);
                input_tot_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tot_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tot_amount.setAttribute('class', 'inputbox');
                input_tot_amount.setAttribute('size', '5');
            div_colt6.appendChild(div_col_txt6);
            div_col6.appendChild(div_colt6);
            div_col6.appendChild(input_tot_amount);
            
            div_col7 = document.createElement('div');
            div_col7.setAttribute('class', 'coloumn pl5 vb');   
            div_colt7 = document.createElement('div');
            div_col_txt7= document.createTextNode('Disc.Type');            
                // Create the TEXTAREA field.
                select_discount_type = document.createElement('select');
                select_discount_type.options[0] = new Option('','');
                select_discount_type.options[1] = new Option('OneTime','1');
                select_discount_type.options[2] = new Option('Recurring','2');
                select_discount_type.setAttribute('name', 'discount_type[]');
                select_discount_type.setAttribute('id', 'totamt'+parentServiceId);
                select_discount_type.setAttribute('class', 'inputbox w100');
            div_colt7.appendChild(div_col_txt7);
            div_col7.appendChild(div_colt7);
            div_col7.appendChild(select_discount_type);
    
    div_row2 = document.createElement('div');
    div_row2.setAttribute('class', 'row');
            div_col21 = document.createElement('div');
            div_col21.setAttribute('class', 'coloumn pl5 vb');
            
            div_colt21 = document.createElement('div');
            div_col_txt21= document.createTextNode('Purchase price');     
            
            input_pp_amount = document.createElement('input');
                input_pp_amount.setAttribute('type', 'text');
                input_pp_amount.setAttribute('name', 'pp_amount[]');
                input_pp_amount.setAttribute('value', '0');
                input_pp_amount.setAttribute('id', 'ppamt'+parentServiceId);
                input_pp_amount.setAttribute('class', 'inputbox');
                input_pp_amount.setAttribute('size', '5');
            div_colt21.appendChild(div_col_txt21);
            div_col21.appendChild(div_colt21);
            div_col21.appendChild(input_pp_amount);
            
  
            div_col22 = document.createElement('div');
            div_col22.setAttribute('class', 'coloumn pl5 vb');
            div_colt22 = document.createElement('div');
            div_col_txt22= document.createTextNode('Vendor');   
            
            select_vendor = document.createElement('select');
                select_vendor.options[0] = new Option(' ','');
                vendorNameList  = document.getElementsByName('vendorNameList[]');   
                vendorIdList  = document.getElementsByName('vendorIdList[]');   
                select_vendor.options[0] = new Option('vendor','');
                for ( i=0; i< vendorNameList.length; i++ ) {
                     var vendornm = vendorNameList[i].value;
                     var vendorid = vendorIdList[i].value;
                     select_vendor.options[i] = new Option(vendornm,vendorid);
                
                
                }
              
                select_vendor.setAttribute('name', 'vendor[]');
                select_vendor.setAttribute('id', 'vendor'+parentServiceId);
                select_vendor.setAttribute('class', 'inputbox w150');
            div_colt22.appendChild(div_col_txt22);
            div_col22.appendChild(div_colt22);
            div_col22.appendChild(select_vendor);
            
            div_col23 = document.createElement('div');
            div_col23.setAttribute('class', 'coloumn vb');
            
            div_colt23 = document.createElement('div');
            div_col_txt23= document.createTextNode('Purchase Particulars');   
                // Create the TEXTAREA field.
                purchase_particulars = document.createElement('textarea');
                purchase_particulars.setAttribute('name', 'purchase_particulars[]');
                purchase_particulars.setAttribute('class', 'inputbox');
                purchase_particulars.setAttribute('cols', '28');
                purchase_particulars.setAttribute('rows', '4');
                purchase_particulars.setAttribute('id', 'pur_part'+parentServiceId);
                purchase_particulars.setAttribute('style', 'height:45px;');
                purchase_particulars.setAttribute('onkeypress', 'javascript: return increaseHeight(this, event);');
            div_colt23.appendChild(div_col_txt23);
            div_col23.appendChild(div_colt23);
            div_col23.appendChild(purchase_particulars);
    
    
    
    
     
      div_row1.appendChild(div_col1);
      div_row1.appendChild(div_col2);
      div_row1.appendChild(div_col3);
      div_row1.appendChild(div_col4);
      div_row1.appendChild(div_col5);
      div_row1.appendChild(div_col6);
      div_row1.appendChild(div_col7);
      
      div_row2.appendChild(div_col21);
      div_row2.appendChild(div_col22);
      div_row2.appendChild(div_col23);
    
    div_main.appendChild(div_row);
    div_main.appendChild(div_row1);
    div_main.appendChild(div_row2);
    element.appendChild(div_main);
    return (true);
}

function updateExchangeRate(){

   document.getElementById('exchange_rate').value=1;
   serviceNo = document.getElementsByName('service_id[]').length;
    div_particular =  document.getElementById("particularsub") ;
    
    for(var i = 0; i<serviceNo ;i++){
        if(document.frmInvoiceAdd.service_id[i].checked){
            document.frmInvoiceAdd.service_id[i].checked = false  ;
            parent_id = document.frmInvoiceAdd.service_id[i].value;
            div_particular.removeChild(document.getElementById(parent_id)) ;         
        }
       
    }
    
   /* 
  
  alert(document.getElementByName('service_id[]').length);

    document.getElementById("particularsub").innerHTML =''; ;
*/
  
}
// create particular  fields on  fly bof


function updateCheque(mode) {
	
    if(mode == '1'){
        document.getElementById('pay_chq_no').className="show";
        document.getElementById('pay_chq_dt').className="show";
        document.getElementById('pay_chq_bank').className="show";
        document.getElementById('pay_chq_branch').className="show";
         
    }else{
        document.getElementById('pay_chq_no').className="hide";
        document.getElementById('pay_chq_dt').className="hide";
        document.getElementById('pay_chq_bank').className="hide";
        document.getElementById('pay_chq_branch').className="hide";        
           
    }
}

/*Add multiple vouchers bof*/

function addVoucher(voucher_arr) {

 div_order = document.getElementById("voucher_info");

    order_detail_div = document.createElement('div');
    order_detail_div.setAttribute('className', 'row');

        
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'voucher');
        exec_id_input.setAttribute('value', voucher_arr[0]);
		
		exec_id_input0 = document.createElement('input');
        exec_id_input0.setAttribute('type', 'hidden');
        exec_id_input0.setAttribute('name', 'voucher_no');
        exec_id_input0.setAttribute('value', voucher_arr[1]);
		
		exec_id_input1 = document.createElement('input');
        exec_id_input1.setAttribute('type', 'hidden');
        exec_id_input1.setAttribute('name', 'voucher_amount');
        exec_id_input1.setAttribute('value', voucher_arr[4]);
		
		exec_id_input3 = document.createElement('input');
        exec_id_input3.setAttribute('type', 'hidden');
        exec_id_input3.setAttribute('name', 'voucher_total_amount');
        exec_id_input3.setAttribute('value', voucher_arr[13]);
		
		exec_id_input4 = document.createElement('input');
        exec_id_input4.setAttribute('type', 'hidden');
        exec_id_input4.setAttribute('name', 'voucher_company_id');
        exec_id_input4.setAttribute('value', voucher_arr[14]);
		
		exec_id_input5 = document.createElement('input');
        exec_id_input5.setAttribute('type', 'hidden');
        exec_id_input5.setAttribute('name', 'do_voucher');
        exec_id_input5.setAttribute('value', voucher_arr[15]);
		
		exec_id_input6 = document.createElement('input');
        exec_id_input6.setAttribute('type', 'hidden');
        exec_id_input6.setAttribute('name', 'do_transaction');
        exec_id_input6.setAttribute('value', voucher_arr[16]);
		
		exec_id_input2 = document.createElement('input');
        exec_id_input2.setAttribute('type', 'hidden');
        exec_id_input2.setAttribute('name', 'vcurrency_id');
        exec_id_input2.setAttribute('value', voucher_arr[12]);
		
         exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'voucher_details');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', voucher_arr[2] +" "+ voucher_arr[3] +" ("+ voucher_arr[1] +" - Amt "+ voucher_arr[4] +" "+ voucher_arr[11] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');
		
      

		order_detail_div.appendChild(exec_id_input);
        order_detail_div.appendChild(exec_id_input0);
        order_detail_div.appendChild(exec_id_input1);
        order_detail_div.appendChild(exec_id_input2);
        order_detail_div.appendChild(exec_id_input3);
        order_detail_div.appendChild(exec_id_input4);
        order_detail_div.appendChild(exec_id_input5);
        order_detail_div.appendChild(exec_id_input6);
        order_detail_div.appendChild(exec_client_input);

    div_order.innerHTML = '<span class="txthelp">Select the Voucher.</span>';
    div_order.appendChild(order_detail_div);
	
	exchange_rate = document.getElementById("exchange_rate").value =  voucher_arr[5];	 
	var total = voucher_arr[4];    
	
	document.getElementById("amount").value =total;
    document.getElementById("amount_inr").value = parseFloat(total) * parseFloat(exchange_rate);
	document.getElementById("p_mode").value =   voucher_arr[6] ;
	document.getElementById("mode_id").value =   voucher_arr[6] ;	
	document.getElementById("do_r").value =   voucher_arr[15] ;
	//document.getElementById("financialYr").value =   voucher_arr[15] ;
	//document.getElementById("financialYr_id").value =   voucher_arr[15] ;
	
	 
	document.getElementById("pay_cheque_no").value=voucher_arr[7];		
	document.getElementById("do_pay_received").value=voucher_arr[8];
	document.getElementById("pay_bank_company").value=voucher_arr[9];
	document.getElementById("pay_bank_company_1").value=voucher_arr[9];
	document.getElementById("pay_branch").value=voucher_arr[10];
	document.getElementById("pay_chq_no").className='show';
	document.getElementById("pay_chq_dt").className='show';
	document.getElementById("pay_chq_bank").className='show';
	document.getElementById("pay_chq_branch").className='show';
		
	  
    return (true);
 

}

function removeVoucher(id) {
 
    div_executive = document.getElementById("voucher_info");
	document.getElementById("exchange_rate").value=1;
	
	exchange_rate = document.getElementById("exchange_rate").value ;
    total = document.getElementById("amount").value;    
   

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
   
}
/*Add multiple vouchers eof*/