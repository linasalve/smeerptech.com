

/**
* To Load api depending on gateway in SMS purchase Module
**/


//new added
var http_req_alerts;
var http_req_alerts1;

function updateApi(gateway) {
	
    var cboModule = document.getElementById("api_id");
   
    cboModule.options.length = 0;
    if(!gateway){
        cboModule.options[0] = new Option("--SELECT--", "-2");	
        cboModule.options[1] = new Option("None", "-1");	
        return null;
    }
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""
    
    var uri = "get-api.php?gateway="+gateway;    
   
    http_req_alerts.onreadystatechange = loadApi;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

/**
* To Load api depending on gateway in SMS purchase Module
**/

function loadApi(){
    var cboModule = document.getElementById("api_id");
   
  
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
        
            if(allThoseTypes){			
                
                cboModule.options[0] = new Option("--SELECT--", "-1");
                //cboModule.options[1] = new Option("None", "-1");
                cboModule.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");
            
              for (var i=0;i<typeSplit.length;i++){
            
                var tmpStr =typeSplit[i];
                if(tmpStr){
                    var tmpArray = new Array();
                    tmpArray = tmpStr.split("|");
                    var tmpName =tmpArray[0];
                    var tmpVal = tmpArray[1];
                   
                    if(tmpName!=''){
                        cboModule.options[i+1] = new Option(tmpName, tmpVal);   
                                             
                    }
                }
              }           
            }
        }
    }else{        
          cboModule.options[0] = new Option("Loading...", "");
          cboModule.style.background = "#c3c3c3";
  }
}
