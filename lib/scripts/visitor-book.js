//Function to update the Order TDS status using AJAX BOF
var http_req_alerts_tds;
function updateVStatus( id,status){
  var del = reallyDel('Are you sure you want to change the status?');

  if(del==true){	
	http_req_alerts_tds = false;	
	document.getElementById("vid").value = id;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_tds = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_tds = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_tds = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_tds) {
        return false;
    }
    var uri = "ajx-visitor-status.php?id="+id+"&status="+status; 
	
    http_req_alerts_tds.onreadystatechange = loadVStatus;
    http_req_alerts_tds.open('GET', uri, true);
    http_req_alerts_tds.send(null);
 }
}

function loadVStatus(){
 
     id = document.getElementById("vid").value ;
	 statusid = 'status'+id ;
	 
	if(http_req_alerts_tds.readyState==4){
   
        if (http_req_alerts_tds.status == 200){
            var allData = http_req_alerts_tds.responseText;
            
			 
            if(allData){            
           
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataHighlight = dataSplit[2];	
 				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
				
				highlightid = 'row'+dataHighlight ;
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
        
        
    }

}
//Function to update the Order TDS status using AJAX BOF */


var http_req_alertsp ;
 
function updatePurpose(type) { 
    
    http_req_alertsp = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alertsp = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alertsp = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alertsp = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alertsp) {
        return false;
    } 
     
    var uri = "ajx-get-vbook-purpose.php?pid=" + type;    
 
    http_req_alertsp.onreadystatechange = loadPurpose;
    http_req_alertsp.open('GET', uri, true);
    http_req_alertsp.send(null);
}

 


function loadPurpose(){
    
    if(http_req_alertsp.readyState==4){
   
        if (http_req_alertsp.status == 200){
            var allThoseTypes = http_req_alertsp.responseText;
            document.getElementById("purpose").value = allThoseTypes;
            
        }
    }
}
