//Function to update the Banner status using AJAX BOF
var http_req_alerts_banner;
function updateStatus( id,status){

	http_req_alerts_banner = false;	
	document.getElementById("nwsmid").value = id;
 
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    } 
   
    var uri = "newsletters-bounce-status-ajx.php?id="+id+"&status="+status;    
	
    http_req_alerts_banner.onreadystatechange = loadStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);
}
function loadStatus(){
     id = document.getElementById("nwsmid").value ;
	 ebounceid = 'ebounce'+id ;
	 bounceid = 'bounce'+id ;
	 
	 
	if(http_req_alerts_banner.readyState==4){

        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
		 
            if(allData){            
            
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataeStatus = dataSplit[1];
                var dataStatus = dataSplit[2];
               
				
				var dataHighlight = dataSplit[3];
				highlightid = 'row'+dataHighlight ;
				 
				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(ebounceid).innerHTML= dataeStatus; 
                document.getElementById(bounceid).innerHTML= dataStatus; 
                
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        document.getElementById("message").innerHTML= ""; 
    }
}


 
var http_req_alerts1;
function markTempIdsStatus(type){
	http_req_alerts1 = false;		 
 
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts1 = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts1 = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts1 = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts1) {
        return false;
    } 
   
    var uri = "newsletters-temp-status-ajx.php?type="+type;    
	
    http_req_alerts1.onreadystatechange = loadTempIdsStatus;
    http_req_alerts1.open('GET', uri, true);
    http_req_alerts1.send(null);
}
function loadTempIdsStatus(){	 
	 
	if(http_req_alerts1.readyState==4){
        if (http_req_alerts1.status == 200){
            var allData = http_req_alerts1.responseText;
		 
            if(allData){
				document.getElementById("message").innerHTML= allData; 
            }
        }
    }else{                
		document.getElementById("message").innerHTML= ""; 
    }
}

function downloadNwsFile(id,file) {

   if (id != '') {      
        url = "./newsletters.php?perform=download_file";
        url = url + "&id="+id+'&file='+ file;        
        window.open(url,"Download file","left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1");
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}

var http_req_alerts ;
function updateSmtp(client) {	
    var cboModule = document.getElementById("newsletters_smtp_id");   
    cboModule.options.length = 0;   
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts) {
        return false;
    }     
    var uri = "get-newsletters-smtp.php?client="+client;  
    http_req_alerts.onreadystatechange = loadSmtpList;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

/**
* To Load smtp details depending on client
**/
function loadSmtpList(){
    var cboModule = document.getElementById("newsletters_smtp_id");
   if(http_req_alerts.readyState==4){   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
            if(allThoseTypes){			                
                cboModule.options[0] = new Option("--SELECT--", "");                
                cboModule.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");
               
			    for (var i=0;i<typeSplit.length;i++){            
					var tmpStr =typeSplit[i];
					if(tmpStr){
						var tmpArray = new Array();
						tmpArray = tmpStr.split("|");
						var tmpName =tmpArray[0];
						var tmpVal = tmpArray[1];
					   	if(tmpName!=''){
							cboModule.options[i+1] = new Option(tmpName, tmpVal);   												 
						}
					}
				}           
            }
        }
    }else{        
          cboModule.options[0] = new Option("Loading...", "");
          cboModule.style.background = "#c3c3c3";
    }
}

var http_req_alerts2 ;
function updateSmtpDetails(smtp_id){
   
    http_req_alerts2 = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts2 = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts2 = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts2 = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts2) {
        return false;
    }     
    var uri = "get-newsletters-smtp-details.php?smtp_id="+smtp_id;  
    http_req_alerts2.onreadystatechange = loadSmtpDetails;
    http_req_alerts2.open('GET', uri, true);
    http_req_alerts2.send(null);

}

function loadSmtpDetails(){
	
   if(http_req_alerts2.readyState==4){   
        if (http_req_alerts2.status == 200){
            var allThoseTypes = http_req_alerts2.responseText;
			 
			alert(allThoseTypes);
			
            if(allThoseTypes){			              
                var tmpArray = new Array();
                tmpArray = allThoseTypes.split("|");
                var sendmethod = trim(tmpArray[0]);				
				var sockethost = tmpArray[1];
				var smtpauth = tmpArray[2];
				var smtpauthuser = tmpArray[3];
				var smtpauthpass = tmpArray[4];
				var smtpport = tmpArray[5];
				var from_name = tmpArray[6];
				var from_email = tmpArray[7];
				var replyto_name = tmpArray[8];
				var replyto_email = tmpArray[9];
				var domain_name = tmpArray[10];
				var domain_mailgun_key = tmpArray[11];  
				
				
				if( sendmethod == 'smtp' ){	
					
					document.getElementById("sendmethod_smtp").checked = true ;
				}
				if(sendmethod == 'sendmail'){
					document.getElementById("sendmethod_sendmail").checked = true ;
				}
				if(sendmethod == 'mail'){					 
					document.getElementById("sendmethod_mail").checked = true ;
				}
				if(sendmethod == 'qmail'){					 
					document.getElementById("sendmethod_qmail").checked = true ;
				}
				if(smtpauth == 'TRUE'){
					document.getElementById('smtpauth_true').checked = 'checked' ;
				}
				if(smtpauth == 'FALSE'){
					document.getElementById('smtpauth_false').checked = 'checked' ;
				}
				
				document.getElementById('sockethost').value = sockethost ; 
				document.getElementById('smtpauthuser').value = smtpauthuser ; 
				document.getElementById('smtpauthpass').value = smtpauthpass ; 
				document.getElementById('smtpport').value = smtpport ; 
				document.getElementById('from_name').value = from_name ; 
				document.getElementById('from_email').value = from_email ; 
				document.getElementById('replyto_name').value = replyto_name ; 
				document.getElementById('replyto_email').value = replyto_email ; 
				document.getElementById('domain_name').value = domain_name ; 
				document.getElementById('domain_mailgun_key').value = domain_mailgun_key ; 
				
            }
        }
    }else{        
        
    }

}