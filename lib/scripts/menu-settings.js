function reallyDel() {
	if(!confirm("Are you sure you want to delete?")) {
		return false;
	}
}

function changePage(page) {
	var q_string = "";

	q_string = "?&perform=list";
	
	// Retrieve the Filters that are applied.
	q_string += "&site_id="+ document.frmSearch.site_id.value;
	q_string += "&status="+ document.frmSearch.status.value;
	q_string += "&rpp="+ document.frmSearch.rpp.value;

	if ( page != "" && page > 0 ) {
		q_string += "&x="+ page;
	}
	else {
		q_string += "&x=1";
	}	
	window.location.href = './menu-settings.php'+ q_string;
	return false;
}

function setMenuPage() {
	var page = document.frmMenuAdd;
	if ( page.type.value == "S" ) {
		document.getElementById('static').style.display		= 'block';
		document.getElementById('dynamic').style.display	= 'none';
		document.getElementById('external').style.display	= 'none';
	}
	else if ( page.type.value == "D" ) {
		document.getElementById('static').style.display		= 'none';
		document.getElementById('dynamic').style.display	= 'block';
		document.getElementById('external').style.display	= 'none';
	}
	else if ( page.type.value == "E" ) {
		document.getElementById('static').style.display		= 'none';
		document.getElementById('dynamic').style.display	= 'none';
		document.getElementById('external').style.display	= 'block';
	}
	return true;
}

function onPlacementChange(placement) {
	result = fillParentMenu(placement);
	return result;
}

function fillParentMenu(placement) {
	return true;	
}

function onWebsiteChange(site_id, form) {
	// The Website selection has been changed.
	// Reload all the values.
	var q_string = "";
	
	q_string += '?';
	q_string += createQueryString(form);
	
	URL = 'menu-settings.php'+ q_string;
	window.location = URL;
	return true;
}

function executeFilter(form) {
	return (onWebsiteChange('', form));
}