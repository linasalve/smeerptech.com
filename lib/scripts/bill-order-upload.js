function addOrderInfo(order_arr) {
    div_order = document.getElementById("add_order_info");

    order_detail_div = document.createElement('div');
    order_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        order_id_input = document.createElement('input');
        order_id_input.setAttribute('type', 'hidden');
        order_id_input.setAttribute('name', 'order_id');
        order_id_input.setAttribute('value', order_arr[0]);

        order_det_input = document.createElement('input');
        order_det_input.setAttribute('type', 'text');
        order_det_input.setAttribute('name', 'order_details');
        order_det_input.setAttribute('value', order_arr[1]);
        order_det_input.setAttribute('size', '60');
        //order_det_input.setAttribute('readonly', 'readonly');
        order_det_input.setAttribute('class', 'inputbox_c');

        order_detail_div.appendChild(order_id_input);
        order_detail_div.appendChild(order_det_input);

    div_order.innerHTML = '<span class="txthelp">Select the Order.</span>';
    div_order.appendChild(order_detail_div);
    return (true);
}