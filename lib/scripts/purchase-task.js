  
 //AJAX PAGINATION FUNCTION BOF
function changeRecordList(page,pagename,form1){ 
	http_req_alerts = false;			
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_req_alerts = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts) {
		return false;
	}

	//var poststr = createQueryString(form1);	
	//var poststr =Url.decode($(form1).serialize()) ;
	
	var slist = document.getElementById('sStatus'); 
	st_options = "";
	for(var i = 0; i < slist.options.length; ++i){
		comma = ",";
		if(slist.options[i].selected){
			st_options = st_options + slist[i].value + comma;
		}
	}
	st_options = trim(st_options,",");
	
	var or_id =document.getElementById('or_id').value; 
	var actual_time =document.getElementById('actual_time').value; 
	var show =document.getElementById('show').value; 
	 
	
	poststr = "or_id="+or_id+"&actual_time="+actual_time+"&show="+show+"&sString="+
	document.getElementById("sString").value+"&sType="+document.getElementById("sType").value+
	"&sOrderBy="+document.getElementById("sOrderBy").value+"&sOrder="+document.getElementById("sOrder").value+
	"&chk_status="+document.getElementById("chk_status").value+"&sStatus="+st_options+"&date_from="+document.getElementById("date_from").value+"&date_to="+document.getElementById("date_to").value+
	"&perform="+document.getElementById("perform").value+"&rpp="+document.getElementById("rpp").value ;
	 
	var uri = pagename+"?ajx=1&x="+page+"&"+poststr ; 
	
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';	
	$('#inner_content').load(uri);
	
 
} 

function loadRecordList(){
	if(http_req_alerts.readyState==4){
        if (http_req_alerts.status == 200){
            var allData = http_req_alerts.responseText;
            if(allData){
			    document.getElementById("record_info").innerHTML = allData; 
            }
        }
    }else{
		document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';         
    }
} 
//AJAX PAGINATION FUNCTION EOF
function clearSearch(){
	document.getElementById("sString").value='' ;
	document.getElementById("sType").value='' ;
	document.getElementById("sOrderBy").value='do_add' ;
	document.getElementById("sOrder").value='DESC' ;
	document.getElementById("date_from").value='' ;
	document.getElementById("date_to").value='' ;
	
	 
}

function searchRecord(pagename,form1){
	changeRecordList(1,pagename,form1);
	document.getElementById("pageno").value=1 ;	
	return false;
}


function addRecord(surl){

	
	$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 900,
			resizable: true,
			closeText: 'close',
			title: 'SMEERP - NERVE CENTER',
			modal: true,
			close: function() {
			
				var pageno = document.getElementById("pageno").value ;

				url = SITE_URL+"/"+ADMIN_PANEL+"/purchase-task.php?perform=list&pageno="+pageno ;
				//searchRecord('prospects.php',frmSearch);
				changeRecordList(pageno,'purchase-task.php',frmSearch);
				//$( "#dialog-form" ).dialog( "destroy" );
			}
	});
    ajxUrl= surl +"&ajx=1" ;
 
    $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;
	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
    $("#dialogIframeId").attr("src",ajxUrl);
    return false; 
}
 
function editRecord(surl){
   
   $( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 600,
		width: 900,
		resizable: true,
		closeText: 'close',
		title: 'SMEERP - NERVE CENTER',
		modal: true,
		close: function() {
			
			var pageno = document.getElementById("pageno").value ;
			url = SITE_URL+"/"+ADMIN_PANEL+"/purchase-task.php?perform=list&pageno="+pageno ;
			changeRecordList(pageno,'purchase-task.php',frmSearch);
			//$( "#dialog-form" ).dialog( "destroy" );
		}
	});
   
   ajxUrl= surl +"&ajx=1" ;	 
   $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;

	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
  return false; 
}

function viewRecord(surl){

	
	$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 900,
			resizable: true,
			closeText: 'close',
			title: 'SMEERP - NERVE CENTER',
			modal: true,
			close: function(){
			
				/* var pageno = document.getElementById("pageno").value ;
				url = SITE_URL+"/"+ADMIN_PANEL+"/purchase-task.php?perform=list&pageno="+pageno ;
				changeRecordList(pageno,'purchase-task.php',frmSearch); */
				//$( "#dialog-form" ).dialog( "destroy" );
			}
	});
    ajxUrl= surl +"&ajx=1" ;
 
    $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;

	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
  
   return false; 
	
} 
function listRecord(surl){
	
	ajxUrl= surl +"&ajx=1" ;
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';
	$('#inner_content').load(ajxUrl);
	   return false; 
}
/*AJAX NAVIGATION EOF*/



var http_req_alerts_sms;
function updateClientMobile(client_id){

    http_req_alerts_sms = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_sms = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_sms = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_sms = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    if (!http_req_alerts_sms) {
        return false;
    } 
    var uri = "get-client-mobile-details.php?client_id=" + client_id;    
    http_req_alerts_sms.onreadystatechange = loadMobileDetails;
    http_req_alerts_sms.open('GET', uri, true);
    http_req_alerts_sms.send(null);

}
function loadMobileDetails(){
    if(http_req_alerts_sms.readyState==4){
        if (http_req_alerts_sms.status == 200){
            var allThoseTypes = http_req_alerts_sms.responseText;
             mobiledetails=  allThoseTypes.replace(/^\s+|\s+$/g,"");
             var dataSplit =new Array();
             dataSplit = mobiledetails.split("|");
             document.getElementById("sms").innerHTML = dataSplit[0];
             document.getElementById("client_mobile_exist").value =  dataSplit[1];
             
        }
    }
}

var http_req_alerts1;
function updateTemplate(template_id){

    http_req_alerts1 = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts1 = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts1 = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts1 = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    if (!http_req_alerts1) {
        return false;
    } 
    var uri = "get-pt-template-details.php?template_id=" + template_id;    
    http_req_alerts1.onreadystatechange = loadTemplate;
    http_req_alerts1.open('GET', uri, true);
    http_req_alerts1.send(null);

}
function loadTemplate(){
    if(http_req_alerts1.readyState==4){
        if (http_req_alerts1.status == 200){
            var allThoseTypes = http_req_alerts1.responseText;
            document.getElementById("details").value =  allThoseTypes.replace(/^\s+|\s+$/g,"");
        }
    }
}

function markClientVisibility(ele,alt){

    var el = document.getElementsByName(alt);
    if(ele.checked){
       
        el[0].checked=true ;
    }else{
        el[0].checked=false ;
    }
}

/**
* To Load account head depending on transaction type in payment in/out module
**/


//new added
var http_req_alerts;
var http_req_alerts1;

function updateSubModule(module) {
	
    var cboModule = document.getElementById("sub_module_id");
   
    cboModule.options.length = 0;
    if(!module){
        cboModule.options[0] = new Option("--SELECT--", "-2");	
        cboModule.options[1] = new Option("None", "-1");
        return null;
    }
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""

    var uri = "get-sub-module.php?module="+module;    
   
    http_req_alerts.onreadystatechange = loadSubModule;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

/**
* To Load account head depending on transaction type in payment in/out module
**/


function loadSubModule(){
    var cboModule = document.getElementById("sub_module_id");
   
  
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
        
            if(allThoseTypes){			
                
                cboModule.options[0] = new Option("--SELECT--", "-2");
                cboModule.options[1] = new Option("None", "-1");
                cboModule.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");
            
              for (var i=0;i<typeSplit.length;i++){
            
                var tmpStr =typeSplit[i];
                if(tmpStr){
                    var tmpArray = new Array();
                    tmpArray = tmpStr.split("|");
                    var tmpName =tmpArray[0];
                    var tmpVal = tmpArray[1];
                   
                    if(tmpName!=''){
                        cboModule.options[i+2] = new Option(tmpName, tmpVal);   
                                             
                    }
                }
              }           
            }
        }
    }else{        
          cboModule.options[0] = new Option("Loading...", "");
          cboModule.style.background = "#c3c3c3";
  }
}

function showExecutiveDetails(executive_id) {
    window.open('./user.php?perform=view&user_id='+executive_id,'executiveDetails','left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1');
    return (true);
}

function showOrderDetails(order_id) {
	param = 'left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1';
    window.open('./bill-order.php?perform=view&or_id='+order_id,'orderDetails', param);
    return (true);
}


function addExecutive(executive_arr) {
    div_executive = document.getElementById("executive_info");

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'team[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'team_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove'+ executive_arr[1]);
        exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
            // Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}

function removeExecutive(id) {
 
    div_executive = document.getElementById("executive_info");
  
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
   
}


//Functions from snipper 
function OpenPopUpList(args,or_id)
{
  var w_left = Math.ceil((screen.width-480)/2);
  var win=window.open("attachment.php?prjTaskAdd=1&first="+args+"&or_id="+or_id, "UploadFiles", "top=10,left="+w_left+",width=600,height=300,status=no,toolbar=no,menubar=no,location=no,scrollbars=yes");
  win.focus();
}
function confirm_delete(args)
{
    if(confirm("Do You Really Want To remove?"))
    {
       var page; 
       page = document.frmAdd;       
       page.action = "purchase-task.php?fileid="+args;
       page.submit();
       
    } 
    else{
        return false;
    }
}

function submitBug(){
       page = document.frmAdd; 
       orid = document.frmAdd.or_id.value ;      
       id = document.frmAdd.id.value ;      
       actual_time = document.frmAdd.actual_time.value ;      
       page.action = "project-task-bug.php?perform=add&"+"or_id="+orid+"&task_id="+id+"&actual_time="+actual_time;
       page.submit();
}

function viewBugList(){
       page = document.frmAdd; 
       orid = document.frmAdd.or_id.value ;      
       id = document.frmAdd.id.value ;   
       actual_time = document.frmAdd.actual_time.value ;         
       page.action = "project-task-bug.php?perform=list&"+"or_id="+orid+"&task_id="+id+"&actual_time="+actual_time;
       page.submit();
}

function showOrderSRS(order_id) {
	param = 'left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1';
	
    window.open('purchase-task.php?perform=srs&or_id='+order_id, 'orderSRS', param);
    //return (true);
}

function updateTask(type) {
	
    var cboType = document.getElementById("task_id");
      
    cboType.options.length = 0;
    if(!type){
        cboType.options[0] = new Option("", "");	
        return null;
    }
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    if (!http_req_alerts) {
        return false;
    } 
    var uri = "get-purchase-task.php?order_id=" + type;    
  
    http_req_alerts.onreadystatechange = loadTask;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}


function loadTask(){
    
    var cboType = document.getElementById("task_id");
   
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
           
            if(allThoseTypes){			
               
                cboType.options[0] = new Option("Select Task", "0");
                cboType.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");
            
             for (var i=0;i<typeSplit.length;i++){
                var tmpStr =typeSplit[i];
                if(tmpStr){
                    var tmpArray = new Array();
                    tmpArray = tmpStr.split("|");
                    var tmpName = tmpArray[0];
                    var tmpVal = tmpArray[1];
                   
                    if(tmpName){
                        cboType.options[i+1] = new Option(tmpName, tmpVal);                           
                    }
                }
             }           
            }
        }
    }else{        
        cboType.options[0] = new Option("Loading...", "");
          cboType.style.background = "#c3c3c3";
    }
}

function toggleFollowup(id) {
	if ( id=='1' ) {	
		document.getElementById(id).className = "visible";
		//document.getElementById('submitType').value = "1";
		document.getElementById('2').className = "hidden";
		document.getElementById('3').className = "hidden";
	}
	if ( id=='2' ) {	
		document.getElementById(id).className = "visible";
        //document.getElementById('submitType').value = "2";
		document.getElementById('1').className = "hidden";
		document.getElementById('3').className = "hidden";
        client_id = document.getElementById('client_id').value;
        //updateClientMobile(client_id);
	}
	if ( id=='3' ) {	
		document.getElementById(id).className = "visible";
        //document.getElementById('submitType').value = "3";
		document.getElementById('1').className = "hidden";
		document.getElementById('2').className = "hidden";
	}
}


	