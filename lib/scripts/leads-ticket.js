function markClientVisibility(ele,alt,divname1,divname2){

    var el = document.getElementsByName(alt);
    if(ele.checked){
        document.getElementById(divname1).style.display ='block';
        document.getElementById(divname2).style.display ='block';
        el[0].checked=true ;
    }else{
        document.getElementById(divname1).style.display ='none';
        document.getElementById(divname2).style.display ='none';
        el[0].checked=false ;
    }
}

function addClientInfo(client_arr) {
    div_client = document.getElementById("client_info");

    client_detail_div = document.createElement('div');
    client_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'client');
        client_id_input.setAttribute('value', client_arr[0]);

        client_det_input = document.createElement('input');
        client_det_input.setAttribute('type', 'text');
        client_det_input.setAttribute('name', 'client_details');
        client_det_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +") ("+ client_arr[4] +")");
        client_det_input.setAttribute('size', '60');
        client_det_input.setAttribute('readOnly', '1');        
        client_det_input.setAttribute('class', 'inputbox_c');

        client_detail_div.appendChild(client_id_input);
        client_detail_div.appendChild(client_det_input);

    div_client.innerHTML = '<span class="txthelp">The Client for whom the support ticket is created.</span>';
    div_client.appendChild(client_detail_div);
    return (true);
}


function addExecutive(executive_arr) {
    
    div_executive = document.getElementById("executive_info");

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', 'remove_'+ executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'assign_members[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'assign_members_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ executive_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeExecutive('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}


/**
 * This function is used to un-relate a Executive from a Customer.
 *
 */
function removeExecutive(id) {
    div_executive = document.getElementById("executive_info");

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}
var http_req_alerts1;
function updateTemplate(template_id){

    http_req_alerts1 = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts1 = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts1 = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts1 = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    if (!http_req_alerts1) {
        return false;
    } 
	var uri = "get-st-template-details.php?template_id=" + template_id;    
    http_req_alerts1.onreadystatechange = loadTemplate;
    http_req_alerts1.open('GET', uri, true);
    http_req_alerts1.send(null);

}
function loadTemplate(){
	isTitleLd = document.getElementById("isTitleLd").value ;
    if(http_req_alerts1.readyState==4){
        if (http_req_alerts1.status == 200){
            var allThoseTypes = http_req_alerts1.responseText;
			dataSplit = allThoseTypes.split("~");
			templateFileStr='';
			ticket_text = dataSplit[0];
			ticket_subject = dataSplit[1];
			template_id = dataSplit[2];
			template_file_1 = dataSplit[3];
			template_file_2 = dataSplit[4];
			template_file_3 = dataSplit[5];
            document.getElementById("ticket_text").value =  ticket_text.replace(/^\s+|\s+$/g,"");
			document.getElementById("template_file_1").value =template_file_1 ;
			document.getElementById("template_file_2").value =template_file_2 ;
			document.getElementById("template_file_3").value =template_file_3 ;
			
			if(template_file_1!=''){
				
				templateFileStr = templateFileStr + "<a onclick=\"javascript:downloadLstTplFile('"+template_id+"', '"+template_file_1+"')\" href=\"javascript:void(0);\">Attachment1 </a>";
			}
			if(template_file_2!=''){
				
				templateFileStr = templateFileStr + "<a onclick=\"javascript:downloadLstTplFile('"+template_id+"','"+template_file_2+"')\" href=\"javascript:void(0);\">Attachment2 </a>";
			}
			if(template_file_3!=''){
				
				templateFileStr = templateFileStr + "<a onclick=\"javascript:downloadLstTplFile('"+template_id+"', '"+template_file_3+"')\" href=\"javascript:void(0);\">Attachment3 </a>";
			} 
			 
			 
			document.getElementById("templateFile").innerHTML = templateFileStr ;
			 
			if(isTitleLd==1){
				document.getElementById("ticket_subject").value =  ticket_subject;
			}
        }
    }
}


var http_req_alerts_ct;
function updateTemplateCategory(id){
	
	 
	http_req_alerts_ct = false;		
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_req_alerts_ct = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts_ct = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts_ct = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts_ct) {
		return false;
	} 
   
	var uri = "get-st-templates.php?category_id="+ id    ;    

	http_req_alerts_ct.onreadystatechange = loadTemplateCategory;
	http_req_alerts_ct.open('GET', uri, true);
	http_req_alerts_ct.send(null);
 
}

function loadTemplateCategory(){   	 
	
    var template_id = document.getElementById('template_id');
	var isTitleLd = document.getElementById("isTitleLd").value ;
	
	if(http_req_alerts_ct.readyState==4){  

        if (http_req_alerts_ct.status == 200){
            var optionData = http_req_alerts_ct.responseText;			 
			 
			if(optionData!=''){
				template_id.options.length=0;
				template_id.options[0] = new Option("Select template", " ");
				template_id.style.background ="#ffffff";
				var optSplit =new Array();				 
				optSplit = optionData.split("~");
			
				for (var i=0;i<optSplit.length;i++){
					var tmpStr =optSplit[i];
					if(tmpStr){
						var tmpArray = new Array();
						tmpArray = tmpStr.split("|");
						var tmpName = tmpArray[0];
						var tmpVal = tmpArray[1];
						if(tmpName){
							template_id.options[i+1] = new Option(tmpName, tmpVal);    
							template_id.options[i+1].title= tmpName;
						}
					}
				}       
            }else{	
				template_id.options.length=0;
				template_id.options[0] = new Option("Select template", " ");
			}
			document.getElementById("templateFile").innerHTML = "";
			document.getElementById("ticket_text").value =  '';
			if(isTitleLd==1){
				document.getElementById("ticket_subject").value = '';
			}
        }
    }else{ 
        //template_id.options[0] = new Option("Loading...", "");
        //template_id.style.background = "#c3c3c3";
    }
}

var http_req_alerts;

function showReplyTo(ticket_id) {
	
    document.getElementById("replyto").innerHTML="";
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    if (!http_req_alerts) {
        return false;
    } 
    var uri = "get-st-eml-details.php?ticket_id=" + ticket_id;    
    http_req_alerts.onreadystatechange = loadReplyTo;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}


function loadReplyTo(){
   
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            document.getElementById('replyto').innerHTML = " ";
            
            var allThoseTypes = http_req_alerts.responseText;
            
            if(allThoseTypes){			
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split("|");
                
                var to_1 = typeSplit['0'];
                var cc_1 =typeSplit['1'];
                var bcc_1 =typeSplit['2'];
                var to =typeSplit['3'];
                var cc =typeSplit['4'];
                var bcc =typeSplit['5'];
            }
            
            if(to){
                document.getElementById('show_to').className = "visible";
                document.getElementById("to_1").value=to_1;
                document.getElementById("to_email_1").value=to_1;
                document.getElementById("to").value=to;
            }
            if(cc){
                document.getElementById('show_cc').className = "visible";
                document.getElementById("cc_1").value=cc_1;
                document.getElementById("cc_email_1").value=cc_1;
                document.getElementById("cc").value=cc;
            }
            if(bcc){
                document.getElementById('show_bcc').className = "visible";
                document.getElementById("bcc_1").value=bcc_1;
                document.getElementById("bcc_email_1").value=bcc_1;
                document.getElementById("bcc").value=bcc;
            }
            //document.getElementById("replyto").innerHTML="To : "+to+"<br/> Cc : "+cc+"<br/> Bcc : "+bcc;
        }
    }else{ 
            document.getElementById("replyto").innerHTML="<img src='../media/images/nc/loading.gif' border=0/><br>Loading....Please Wait.";
    }
}

function showLstTplFile(id,file) {

   if (id != '') {      
         url = "./support-ticket-template.php?perform=download_file" ;
        url = url + "&id="+id+'&file='+ file;     
        window.open(url,'Download file','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}
function downloadLstTplFile(id,file) {

   if (id != '') {      
        url = "./support-ticket-template.php?perform=download_file" ;
        url = url + "&id="+id+'&file='+ file;            
        window.open(url,'Download file','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}

function showQuotation(inv_id, file_type) {

    if (file_type == 'htmlprint' || file_type == 'html' ||  file_type == 'pdf') {      
        url = "./leads-quotation.php?perform=view_file"
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Proposal','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}

function showServiceFile(id,file) {

   if (id != '') {      
        url = "./services.php?perform=download_file" ;
        url = url + "&id="+id+'&file='+ file;        
        window.open(url,'Download file','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}