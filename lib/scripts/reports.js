function addOrderInfo(order_arr) {
    
    div_order = document.getElementById("add_order_info");

    order_detail_div = document.createElement('div');
    order_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        order_id_input = document.createElement('input');
        order_id_input.setAttribute('type', 'hidden');
        order_id_input.setAttribute('name', 'project_id');
        order_id_input.setAttribute('value', order_arr[0]);

        order_det_input = document.createElement('input');
        order_det_input.setAttribute('type', 'text');
        order_det_input.setAttribute('name', 'order_details');
        order_det_input.setAttribute('value', order_arr[1]);
        order_det_input.setAttribute('size', '60');
        //order_det_input.setAttribute('readonly', 'readonly');
        order_det_input.setAttribute('class', 'inputbox_c');

        order_detail_div.appendChild(order_id_input);
        order_detail_div.appendChild(order_det_input);

    div_order.innerHTML = '<span class="txthelp">Select the Project.</span>';
    div_order.appendChild(order_detail_div);
    return (true);
}

function addExecutiveInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);

    executive_detail_div = document.createElement('div');
    executive_detail_div.setAttribute('class', 'row');
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'executive');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'executive_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        executive_det_input.setAttribute('class', 'inputbox_c');
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeExecutiveInfo('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  

    return (true);
}
function removeExecutiveInfo(id,divname) {
    div_executive = document.getElementById(divname);

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}
function loadReport(casetxt,loadid){
	var executive = document.getElementById('executive').value;
	var date_from = document.getElementById('date_from').value;
	var date_to = document.getElementById('date_to').value;
	var ajxUrl = "get-sales-report.php?casetxt="+casetxt+"&executive="+executive+"&fdt="+date_from+"&tdt="+date_to ;
	
	$('#'+loadid).load(ajxUrl); 
}