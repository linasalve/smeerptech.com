  



function updateTotal() {
 
 
    p_amount  = document.getElementsByName("p_amount[]");   
    s_type  = document.getElementsByName("s_type[]");
    s_quantity  = document.getElementsByName("s_quantity[]");
    s_amount  = document.getElementsByName("s_amount[]");
    dst_amount  = document.getElementsByName("d_amount[]");
    stot_amount  = document.getElementsByName("stot_amount[]");
    tax1_value  = document.getElementsByName("tax1_value[]");
	tax1_pvalue  = document.getElementsByName("tax1_pvalue[]");
    tax1_amount  = document.getElementsByName("tax1_amount[]");
	
	tax1_sub1_value  = document.getElementsByName("tax1_sub1_value[]");
	tax1_sub1_pvalue  = document.getElementsByName("tax1_sub1_pvalue[]");
    tax1_sub1_amount  = document.getElementsByName("tax1_sub1_amount[]");
	tax1_sub2_value  = document.getElementsByName("tax1_sub2_value[]");
	tax1_sub2_pvalue  = document.getElementsByName("tax1_sub2_pvalue[]");
    tax1_sub2_amount  = document.getElementsByName("tax1_sub2_amount[]");
	
    tot_amount  = document.getElementsByName("tot_amount[]");
    
    var total   = 0.0;
   
    for ( i=0; i< p_amount.length; i++ ) {
        taxamt=stot=0;
        if( parseFloat(p_amount[i].value) ) {
            //total = total + parseFloat(lst_amount[i].value) ;
        }
        s_amount[i].value = p_amount[i].value ; 
        if( s_quantity[i].value > 0 ){
            s_amount[i].value =  parseFloat(p_amount[i].value) * parseFloat(s_quantity[i].value) ;
        }
       
        if(dst_amount[i].value != 0){            
           // stot_amount[i].value =  parseFloat(s_amount[i].value) - parseFloat(dst_amount[i].value) ;            
            stot =  parseFloat(s_amount[i].value) - parseFloat(dst_amount[i].value) ;            
        }else{
            //stot_amount[i].value =  parseFloat(s_amount[i].value) ;
            stot =  parseFloat(s_amount[i].value) ;
            
        } 
	
        stot_amount[i].value = stot.toFixed(2);
		 
		var tax1pval=0 ;
		if(tax1_pvalue[i].value>0){
			 tax1pval=parseFloat(tax1_pvalue[i].value);
		}
        taxamt= tax1pval * parseFloat(stot_amount[i].value)  ;
		
        tax1_amount[i].value =taxamt.toFixed(2);
		
		var tax1sub1pval=0 ;
		if(tax1_sub1_pvalue[i].value>0){
			 tax1sub1pval=parseFloat(tax1_sub1_pvalue[i].value);
		}
		taxs1amt= tax1sub1pval * parseFloat(tax1_amount[i].value)  ;
		var tax1sub2pval=0 ;
		if(tax1_sub2_pvalue[i].value>0){
			 tax1sub2pval=parseFloat(tax1_sub2_pvalue[i].value);
		}
		taxs2amt= tax1sub2pval * parseFloat(tax1_amount[i].value)  ;
		
		tax1_sub1_amount[i].value =taxs1amt.toFixed(2);
		tax1_sub2_amount[i].value =taxs2amt.toFixed(2);
		
		//tax1_sub1_amount[i].value = addPercentAmount(parseFloat(tax1_amount[i].value),tax1_sub1_pvalue[i].value);
		//tax1_sub2_amount[i].value = addPercentAmount(parseFloat(tax1_amount[i].value),tax1_sub2_pvalue[i].value);
		
		
		tot_amount[i].value = addPercentAmount(parseFloat(stot_amount[i].value),tax1_pvalue[i].value);
		tot_amount[i].value = parseFloat(tot_amount[i].value ) + parseFloat(tax1_sub1_amount[i].value) + parseFloat(tax1_sub2_amount[i].value) ;
				
        total = parseFloat(total) +parseFloat(tot_amount[i].value)   ;

	}
    total = total.toFixed(2);
    document.frmOrderAdd.amount.value = total;
    
    //exchange_rate = document.frmOrderAdd.exchange_rate.value;
    //document.frmOrderAdd.amount_inr.value = parseFloat(total) * parseFloat(exchange_rate);
	
}
//new added for discount
function subtractAmount(amount, adjustment) {
    index = adjustment.indexOf('%');
    if ( index > 0 ) {
        adjustment = parseFloat(adjustment.substr(0, index));
        adjustment = parseFloat(adjustment/100);
        adjustment = parseFloat(amount * adjustment);
        amount = parseFloat(amount) - parseFloat(adjustment);
    }
    else {
        if ( adjustment = parseFloat(adjustment) ) {
            amount = parseFloat(amount) - parseFloat(adjustment);
        }
    }
    //amount = parseFloat(parseInt((amount+0.005)*100.0)/100.0);
    amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
     amount = amount.toFixed(2);
    return (amount);
}
function adjustAmount(amount, adjustment) {
    index = adjustment.indexOf('%');
    if ( index > 0 ) {
        adjustment = parseFloat(adjustment.substr(0, index));
        adjustment = parseFloat(adjustment/100);
       
        adjustment = parseFloat(amount * adjustment);
        //adjustment = parseFloat(amount) * parseFloat(adjustment);
        amount = parseFloat(amount) + parseFloat(adjustment);
    }
    else {
        if ( adjustment = parseFloat(adjustment) ) {
            amount = parseFloat(amount) + parseFloat(adjustment);
        }
    }
   // amount = parseFloat(parseInt((amount+0.005)*100.0)/100.0);
    amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
    amount = amount.toFixed(2);
    return (amount);
}
//new function added 2009-04-02 in replace of adjustAmount
function addPercentAmount(amount, taxrate){
	amount = parseFloat(amount) ; 
	taxrate = parseFloat(taxrate) ; 
	adjustment = parseFloat(amount * taxrate);
    //adjustment = parseFloat(amount) * parseFloat(adjustment);
    amount = parseFloat(amount) + parseFloat(adjustment);
	 amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
    
    return (amount);
}


function popUpSubServices(chkElement){
   

    
    if (chkElement.checked == true)
    {
        parent_id= chkElement.value;
        //updateParticulars(parent_id); this is for ajax
       
        //code to create the fields on fly for for selected parent service id bof
       
        stitleid = "servTitle"+parent_id ;
        stitle = document.getElementById(stitleid).value ; 
       
        spunchlineid = "servPunch"+parent_id ;
        spunchline = document.getElementById(spunchlineid).value ;
        
        ssdivid = "servSsdivid"+parent_id ;
        ssdiv = document.getElementById(ssdivid).value ;
       
        
        taxidid = "servTaxid"+parent_id ;
        taxid = document.getElementById(taxidid).value ;
       
        taxnameid = "servTaxname"+parent_id ;
        taxname = document.getElementById(taxnameid).value ;
              
        taxvalueid = "servTaxvalue"+parent_id ;
        taxvalue = document.getElementById(taxvalueid).value ;
        
        taxpvalueid = "servTaxpvalue"+parent_id ;
        taxpvalue = document.getElementById(taxpvalueid).value ;
		 
		
		tax1sub1idid = "servTax1Sub1id"+parent_id ;
        tax1sub1id = document.getElementById(tax1sub1idid).value ; 
 
        tax1sub1nameid = "servTax1Sub1name"+parent_id ;
        tax1sub1name = document.getElementById(tax1sub1nameid).value ;              
        tax1sub1valueid = "servTax1Sub1value"+parent_id ;
        tax1sub1value = document.getElementById(tax1sub1valueid).value ;        
        tax1sub1pvalueid = "servTax1Sub1pvalue"+parent_id ;
        tax1sub1pvalue = document.getElementById(tax1sub1pvalueid).value ;
		
		tax1sub2idid = "servTax1Sub2id"+parent_id ;
        tax1sub2id = document.getElementById(tax1sub2idid).value ;       
        tax1sub2nameid = "servTax1Sub2name"+parent_id ;
        tax1sub2name = document.getElementById(tax1sub2nameid).value ;              
        tax1sub2valueid = "servTax1Sub2value"+parent_id ;
        tax1sub2value = document.getElementById(tax1sub2valueid).value ;        
        tax1sub2pvalueid = "servTax1Sub2pvalue"+parent_id ;
        tax1sub2pvalue = document.getElementById(tax1sub2pvalueid).value ;
		
        
        isrenewable = "servIsrenw"+parent_id ;
        renewableVal = document.getElementById(isrenewable).value ;
        
        currency_id =  document.getElementById("currency_id").value;
        
        if(navigator.appName!='Microsoft Internet Explorer'){
addParticularDetails(document.getElementById("particularsub"),parent_id,ssdiv,stitle,spunchline,taxid,taxname,taxvalue,taxpvalue,			tax1sub1id,tax1sub1name,tax1sub1value,tax1sub1pvalue,tax1sub2id,tax1sub2name,tax1sub2value,tax1sub2pvalue,
			renewableVal);        
            //code to create the fields on fly for for selected parent service id eof
            window.open('./services.php?perform=subservices&parent_id='+parent_id+'&currency_id='+currency_id,'Services','left=20,top=20,width=750,height=400,toolbar=0,resizable=1,scrollbars=1');
        }else{
        
            addParticularDetails(document.getElementById("particularsub"),parent_id,ssdiv,stitle,spunchline,taxid,taxname,taxvalue,taxpvalue,
			tax1sub1id,tax1sub1name,tax1sub1value,tax1sub1pvalue,tax1sub2id,tax1sub2name,tax1sub2value,tax1sub2pvalue,
			renewableVal);        
            //code to create the fields on fly for for selected parent service id eof
            window.open('./services.php?perform=subservices&parent_id='+parent_id+'&currency_id='+currency_id,'Services','left=20,top=20,width=750,height=400,toolbar=0,resizable=1,scrollbars=1');

        
        }
        
    }else{
        parent_id= chkElement.value;
        //document.getElementById(parent_id).innerHTML ='';
        //document.getElementById('particularsub1').RemoveChild(div_main);        
        div_particular =  document.getElementById("particularsub") ;
        div_particular.removeChild(document.getElementById(parent_id)) ;         
    }
    
    updateTotal();
}
//push value + sub services form popup window into parent window
function addServiceInfo(particular,priceVal,serviceid,subservice_id) {

    document.getElementById("part"+serviceid).value = particular;   
    document.getElementById("pamt"+serviceid).value = priceVal;
    document.getElementById("sub_s_id"+serviceid).value = subservice_id;
    updateTotal();
    return (true);
}

/* added in 2010-10-oct-26 */
function addParticularDetails(element,parentServiceId,ssdiv,title,punchline,taxid,taxname,taxvalue,taxpvalue,
tax1sub1id,tax1sub1name,tax1sub1value,tax1sub1pvalue,tax1sub2id,tax1sub2name,tax1sub2value,tax1sub2pvalue,
renewableVal) {  
 
    div_main = document.createElement('div');
    div_main.setAttribute('width', '100%');
    div_main.setAttribute('id', parentServiceId);
        div_row = document.createElement('div');
        div_row.setAttribute('class', 'row 650px');
        div_row.setAttribute('className', 'row 650px');
            div_col = document.createElement('div');
            div_col.setAttribute('class', 'coloumn  w100');            
            div_col.setAttribute('className', 'coloumn w100');   
                // Create the TEXTAREA field.
                var item = document.createElement('input');
                item.setAttribute('type', 'text');
                item.setAttribute('name', "itemname[]");
                item.setAttribute('value', title+' '+punchline);
                item.setAttribute('class', 'inputbox');
                item.setAttribute('disabled', '1');
                item.setAttribute('cols', '28');
                item.setAttribute('rows', '4');
                item.setAttribute('id', "item"+parentServiceId);
                item.setAttribute('style', 'height:20px;width:350px');
                
            div_col.appendChild(item);
            div_row.appendChild(div_col);
            
        
        
        
        div_row1 = document.createElement('div');
        //div_row1.setAttribute('class', 'row 650px');
        //div_row1.setAttribute('className', 'row 650px');
		div_row1.setAttribute('class', 'row');
        div_row1.setAttribute('className', 'row');
        div_row1.setAttribute('width', '100%');
      
            div_colt1 = document.createElement('div');
            div_col_txt1 = document.createTextNode('Particulars');
            
            div_col1 = document.createElement('div');
            div_col1.setAttribute('class', 'coloumn w250');
		    div_col1.setAttribute('className', 'coloumn w250');
            
                // Create the TEXTAREA field.
                var text_parti = document.createElement("textarea");
                text_parti.setAttribute('name', "particulars[]");
                text_parti.setAttribute('class', "inputbox");
                text_parti.setAttribute('cols', '25');
                text_parti.setAttribute('rows', '4');
                text_parti.setAttribute('id', "part"+parentServiceId);
                text_parti.setAttribute('style', "height:90px;");
                text_parti.setAttribute('onkeypress', 'javascript: return increaseHeight(this, event);');
            div_colt1.appendChild(div_col_txt1);
            div_col1.appendChild(div_colt1);
            div_col1.appendChild(text_parti);
         
            // 2nd column of 1st row BOF
            div_col2 = document.createElement('div');            
           // div_col2.setAttribute('class', 'coloumn w400');
		   // div_col2.setAttribute('className', 'coloumn w400');
			div_col2.setAttribute('class', 'coloumn');
		    div_col2.setAttribute('className', 'coloumn');
            
                div_cr1 = document.createElement('div');
                div_cr1.setAttribute('class', 'row');
                div_cr1.setAttribute('className', 'row');
                
                
                        div_cr1_c1 = document.createElement('div');
                        div_cr1_c1.setAttribute('class', 'coloumn w110');
                        div_cr1_c1.setAttribute('className', 'coloumn w110');
                        
                        div_cr1_c1t1 = document.createElement('div');
                        div_cr1_c1_txt1 = document.createTextNode('Price');
                        // Create the Amount field.
                        // <input type="text" name="p_amount[]" value="" class="inputbox" size="10"/>
                        var input_amount = document.createElement('input');
                        input_amount.setAttribute('type', 'text');
                        input_amount.setAttribute('name', 'p_amount[]');
                        input_amount.setAttribute('value', '0');
                        input_amount.setAttribute('id', 'pamt'+parentServiceId);
                        input_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                        input_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                        input_amount.setAttribute('class', 'inputbox w100');
                        input_amount.setAttribute('size', '5');
                        div_cr1_c1t1.appendChild(div_cr1_c1_txt1);
                        div_cr1_c1.appendChild(div_cr1_c1t1);
                        
                        
                        
                        var input_sub_s_id = document.createElement('input');
                        input_sub_s_id.setAttribute('type', 'hidden');
                        input_sub_s_id.setAttribute('name', 'sub_s_id[]');
                        input_sub_s_id.setAttribute('id', 'sub_s_id'+parentServiceId);
                        input_sub_s_id.setAttribute('value', '');
                        
                        var input_pid = document.createElement('input');
                        input_pid.setAttribute('type', 'hidden');
                        input_pid.setAttribute('name', 'p_id[]');
                        input_pid.setAttribute('value', '');
                        
                        var input_s_id = document.createElement('input');
                        input_s_id.setAttribute('type', 'hidden');
                        input_s_id.setAttribute('name', 's_id[]');
                        input_s_id.setAttribute('value', parentServiceId);
                        
                        var input_ss_div_id = document.createElement('input');
                        input_ss_div_id.setAttribute('type', 'hidden');
                        input_ss_div_id.setAttribute('name', 'ss_div_id[]');
                        input_ss_div_id.setAttribute('value', ssdiv);
                        
                        var input_ss_title = document.createElement('input');
                        input_ss_title.setAttribute('type', 'hidden');
                        input_ss_title.setAttribute('name', 'ss_title[]');
                        input_ss_title.setAttribute('value', title);
                        
                        var input_ss_punch_line = document.createElement('input');
                        input_ss_punch_line.setAttribute('type', 'hidden');
                        input_ss_punch_line.setAttribute('name', 'ss_punch_line[]');
                        input_ss_punch_line.setAttribute('value', punchline);
                        
                        var input_tax1_id = document.createElement('input');
                        input_tax1_id.setAttribute('type', 'hidden');
                        input_tax1_id.setAttribute('name', 'tax1_id[]');
                        input_tax1_id.setAttribute('value', taxid);
                        
                        var input_tax1_name = document.createElement('input');
                        input_tax1_name.setAttribute('type', 'hidden');
                        input_tax1_name.setAttribute('name', 'tax1_name[]');
                        input_tax1_name.setAttribute('value', taxname);
                        
                        var input_tax1_pvalue = document.createElement('input');
                        input_tax1_pvalue.setAttribute('type', 'hidden');
                        input_tax1_pvalue.setAttribute('name', 'tax1_pvalue[]');
                        input_tax1_pvalue.setAttribute('value', taxpvalue);
                    
						//added 2010-10-oct-26
						var input_tax1_sub1_id = document.createElement('input');
                        input_tax1_sub1_id.setAttribute('type', 'hidden');
                        input_tax1_sub1_id.setAttribute('name', 'tax1_sub1_id[]');
                        input_tax1_sub1_id.setAttribute('value', tax1sub1id);
                        
                        var input_tax1_sub1_name = document.createElement('input');
                        input_tax1_sub1_name.setAttribute('type', 'hidden');
                        input_tax1_sub1_name.setAttribute('name', 'tax1_sub1_name[]');
                        input_tax1_sub1_name.setAttribute('value', tax1sub1name);
                        
                        var input_tax1_sub1_pvalue = document.createElement('input');
                        input_tax1_sub1_pvalue.setAttribute('type', 'hidden');
                        input_tax1_sub1_pvalue.setAttribute('name', 'tax1_sub1_pvalue[]');
                        input_tax1_sub1_pvalue.setAttribute('value', tax1sub1pvalue);
						
						
						var input_tax1_sub2_id = document.createElement('input');
                        input_tax1_sub2_id.setAttribute('type', 'hidden');
                        input_tax1_sub2_id.setAttribute('name', 'tax1_sub2_id[]');
                        input_tax1_sub2_id.setAttribute('value', tax1sub2id);
                        
                        var input_tax1_sub2_name = document.createElement('input');
                        input_tax1_sub2_name.setAttribute('type', 'hidden');
                        input_tax1_sub2_name.setAttribute('name', 'tax1_sub2_name[]');
                        input_tax1_sub2_name.setAttribute('value', tax1sub2name);
                        
                        var input_tax1_sub2_pvalue = document.createElement('input');
                        input_tax1_sub2_pvalue.setAttribute('type', 'hidden');
                        input_tax1_sub2_pvalue.setAttribute('name', 'tax1_sub2_pvalue[]');
                        input_tax1_sub2_pvalue.setAttribute('value', tax1sub2pvalue);
                    
					
                        var input_is_rnwl = document.createElement('input');
                        input_is_rnwl.setAttribute('type', 'hidden');
                        input_is_rnwl.setAttribute('name', 'is_renewable[]');
                        input_is_rnwl.setAttribute('value', renewableVal);
                
              
              
               
                div_cr1_c1.appendChild(input_tax1_name);
                div_cr1_c1.appendChild(input_tax1_id);
                div_cr1_c1.appendChild(input_tax1_pvalue);
				
				div_cr1_c1.appendChild(input_tax1_sub1_name);
                div_cr1_c1.appendChild(input_tax1_sub1_id);
                div_cr1_c1.appendChild(input_tax1_sub1_pvalue);
				div_cr1_c1.appendChild(input_tax1_sub2_name);
                div_cr1_c1.appendChild(input_tax1_sub2_id);
                div_cr1_c1.appendChild(input_tax1_sub2_pvalue);
				
				div_cr1_c1.appendChild(input_is_rnwl);
                div_cr1_c1.appendChild(input_ss_punch_line);
                div_cr1_c1.appendChild(input_ss_title);
                div_cr1_c1.appendChild(input_s_id);
                div_cr1_c1.appendChild(input_ss_div_id);
                div_cr1_c1.appendChild(input_pid);
              
                div_cr1_c1.appendChild(input_sub_s_id);               
                div_cr1_c1.appendChild(input_amount);
            
            
            div_cr1_c2 = document.createElement('div');
            div_cr1_c2.setAttribute('class', 'coloumn w100');
		    div_cr1_c2.setAttribute('className', 'coloumn w100');    
            div_cr1_c2t1 = document.createElement('div');
            div_cr1_c2_txt1 = document.createTextNode('Type');            
           
                var select_s_type = document.createElement('select');                
                select_s_type.options[0] = new Option('','');
                select_s_type.options[1] = new Option('Qty','Qty');
                select_s_type.options[2] = new Option('Nos','Nos');
                select_s_type.options[3] = new Option('Yrs','Yrs');
                select_s_type.options[4] = new Option('Mth','Mth');
              
                select_s_type.setAttribute('name', 's_type[]');
                select_s_type.setAttribute('id', 's_type'+parentServiceId);
                select_s_type.setAttribute('class', 'inputbox w80');
        
            div_cr1_c2t1.appendChild(div_cr1_c2_txt1);
            div_cr1_c2.appendChild(div_cr1_c2t1);
            div_cr1_c2.appendChild(select_s_type);    
                
            
            div_cr1_c3 = document.createElement('div');
            div_cr1_c3.setAttribute('class', 'coloumn w80');
		    div_cr1_c3.setAttribute('className', 'coloumn w80');    
            div_cr1_c3t1 = document.createElement('div');
            div_cr1_c3_txt1 = document.createTextNode('No');    
          
                var select_s_quantity = document.createElement('select');                
                select_s_quantity.options[0] = new Option('','');
                select_s_quantity.options[1] = new Option('1','1');
                select_s_quantity.options[2] = new Option('2','2');
                select_s_quantity.options[3] = new Option('3','3');
                select_s_quantity.options[4] = new Option('4','4');
                select_s_quantity.options[5] = new Option('5','5');
                select_s_quantity.setAttribute('name', 's_quantity[]');
                select_s_quantity.setAttribute('id', 's_quantity'+parentServiceId);
                select_s_quantity.setAttribute('onchange', 'javascript: return updateTotal();');
                select_s_quantity.setAttribute('class', 'inputbox w60');
                
            div_cr1_c3t1.appendChild(div_cr1_c3_txt1);
            div_cr1_c3.appendChild(div_cr1_c3t1);
            div_cr1_c3.appendChild(select_s_quantity);    
            
            
            div_cr1_c4 = document.createElement('div');
            div_cr1_c4.setAttribute('class', 'coloumn w110');
		    div_cr1_c4.setAttribute('className', 'coloumn w110');    
            div_cr1_c4t1 = document.createElement('div');
            div_cr1_c4_txt1 = document.createTextNode('Cal Amt');   
            
                var input_s_amount = document.createElement('input');
                input_s_amount.setAttribute('type', 'text');
                input_s_amount.setAttribute('name', 's_amount[]');
                input_s_amount.setAttribute('value', '0');
                input_s_amount.setAttribute('id', 'samt'+parentServiceId);
                input_s_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_s_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_s_amount.setAttribute('class', 'inputbox w100');
                input_s_amount.setAttribute('size', '5');
            div_cr1_c4t1.appendChild(div_cr1_c4_txt1);
            div_cr1_c4.appendChild(div_cr1_c4t1);
            div_cr1_c4.appendChild(input_s_amount);
    
        div_cr1.appendChild(div_cr1_c1);
        div_cr1.appendChild(div_cr1_c2);
        div_cr1.appendChild(div_cr1_c3);
        div_cr1.appendChild(div_cr1_c4);
        div_col2.appendChild(div_cr1);
            
        div_cr2 = document.createElement('div');
        div_cr2.setAttribute('class', 'row');   
        div_cr2.setAttribute('className', 'row');   
           
                div_cr2_c1 = document.createElement('div');
                div_cr2_c1.setAttribute('class', 'coloumn w110');
                div_cr2_c1.setAttribute('className', 'coloumn w110');                        
                div_cr2_c1t1 = document.createElement('div');
                div_cr2_c1_txt1 = document.createTextNode('Discount');
                // Create the TEXTAREA field.
                var input_d_amount = document.createElement('input');
                input_d_amount.setAttribute('type', 'text');
                input_d_amount.setAttribute('name', 'd_amount[]');
                input_d_amount.setAttribute('value', '0');
                input_d_amount.setAttribute('id', 'damt'+parentServiceId);
                input_d_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_d_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_d_amount.setAttribute('class', 'inputbox w100');
                input_d_amount.setAttribute('size', '5');
            div_cr2_c1t1.appendChild(div_cr2_c1_txt1);
            div_cr2_c1.appendChild(div_cr2_c1t1);
            div_cr2_c1.appendChild(input_d_amount);
            
                div_cr2_c2 = document.createElement('div');
                div_cr2_c2.setAttribute('class', 'coloumn w110');
                div_cr2_c2.setAttribute('className', 'coloumn w110');                        
                div_cr2_c2t1 = document.createElement('div');
                div_cr2_c2_txt1 = document.createTextNode('SubTot');
         
                // Create the TEXTAREA field.
                var input_stot_amount = document.createElement('input');
                input_stot_amount.setAttribute('type', 'text');
                input_stot_amount.setAttribute('name', 'stot_amount[]');
                input_stot_amount.setAttribute('value', '0');
                input_stot_amount.setAttribute('id', 'stotamt'+parentServiceId);
                input_stot_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_stot_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_stot_amount.setAttribute('class', 'inputbox w100');
                input_stot_amount.setAttribute('size', '5');
            div_cr2_c2t1.appendChild(div_cr2_c2_txt1);
            div_cr2_c2.appendChild(div_cr2_c2t1);
            div_cr2_c2.appendChild(input_stot_amount);
            
            div_cr2.appendChild(div_cr2_c1);
            div_cr2.appendChild(div_cr2_c2);
            div_col2.appendChild(div_cr2);
            
            
            div_cr3 = document.createElement('div');
            div_cr3.setAttribute('class', 'row');  
            div_cr3.setAttribute('width', '100%');  
            
                div_cr3_c1 = document.createElement('div');
                div_cr3_c1.setAttribute('class', 'coloumn w110');
                div_cr3_c1.setAttribute('className', 'coloumn w110');                        
                div_cr3_c1t1 = document.createElement('div');
                if(taxname!=''){
                    div_cr3_c1_txt1 = document.createTextNode(taxname);
                }else{
                    div_cr3_c1_txt1 = document.createTextNode('Tax');
                }
           
                // Create the TEXTAREA field.
                var input_tax1_value = document.createElement('input');
                input_tax1_value.setAttribute('type', 'text');
                input_tax1_value.setAttribute('name', 'tax1_value[]');
                input_tax1_value.setAttribute('value', taxvalue);
                input_tax1_value.setAttribute('readonly', '1');
                input_tax1_value.setAttribute('id', 'txval'+parentServiceId);
                input_tax1_value.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tax1_value.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tax1_value.setAttribute('class', 'inputbox w100');
                input_tax1_value.setAttribute('size', '5');          
            
                div_cr3_c1t1.appendChild(div_cr3_c1_txt1);
                div_cr3_c1.appendChild(div_cr3_c1t1);
                div_cr3_c1.appendChild(input_tax1_value);
               

                // tax amt
                div_cr3_c2 = document.createElement('div');
                div_cr3_c2.setAttribute('class', 'coloumn w110');
                div_cr3_c2.setAttribute('className', 'coloumn w110');                        
                div_cr3_c2t1 = document.createElement('div');
                div_cr3_c2_txt1 = document.createTextNode('Tax Amt');    
                
                // Create the TEXTfield.
                var input_tax_amount = document.createElement('input');
                input_tax_amount.setAttribute('type', 'text');
                input_tax_amount.setAttribute('name', 'tax1_amount[]');
                input_tax_amount.setAttribute('value', '0');
                input_tax_amount.setAttribute('readonly', '1');
                input_tax_amount.setAttribute('id', 'taxamt'+parentServiceId);
                input_tax_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tax_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tax_amount.setAttribute('class', 'inputbox w100');
                input_tax_amount.setAttribute('size', '5');                
                
				
                div_cr3_c2t1.appendChild(div_cr3_c2_txt1);
                div_cr3_c2.appendChild(div_cr3_c2t1);
                div_cr3_c2.appendChild(input_tax_amount);
                
				 
				//sub1 tax
				div_cr3_c3 = document.createElement('div');
                div_cr3_c3.setAttribute('class', 'coloumn w110');
                div_cr3_c3.setAttribute('className', 'coloumn w110');                        
                div_cr3_c3t1 = document.createElement('div');
                if(tax1sub1name!=''){
                    div_cr3_c3_txt1 = document.createTextNode(tax1sub1name);
                }else{
                    div_cr3_c3_txt1 = document.createTextNode('Tax');
                }
           
                // Create the TEXTAREA field.
                var input_tax1_sub1_value = document.createElement('input');
                input_tax1_sub1_value.setAttribute('type', 'text');
                input_tax1_sub1_value.setAttribute('name', 'tax1_sub1_value[]');
                input_tax1_sub1_value.setAttribute('value', tax1sub1value);
                input_tax1_sub1_value.setAttribute('readonly', '1');
                input_tax1_sub1_value.setAttribute('id', 'txs1val'+parentServiceId);
                input_tax1_sub1_value.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tax1_sub1_value.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tax1_sub1_value.setAttribute('class', 'inputbox w100');
                input_tax1_sub1_value.setAttribute('size', '5');     
                
				div_cr3_c3t1.appendChild(div_cr3_c3_txt1);
                div_cr3_c3.appendChild(div_cr3_c3t1);
                div_cr3_c3.appendChild(input_tax1_sub1_value);
                
				
				 
				//sub1 tax amt 
			 	
				div_cr3_c4 = document.createElement('div');
                div_cr3_c4.setAttribute('class', 'coloumn w110');
                div_cr3_c4.setAttribute('className', 'coloumn w110');                        
                div_cr3_c4t1 = document.createElement('div');
                div_cr3_c4_txt1 = document.createTextNode('Sub Tax Amt');    
                
                // Create the TEXTfield.
                var input_tax1_sub1_amount = document.createElement('input');
                input_tax1_sub1_amount.setAttribute('type', 'text');
                input_tax1_sub1_amount.setAttribute('name', 'tax1_sub1_amount[]');
                input_tax1_sub1_amount.setAttribute('value', '0');
                input_tax1_sub1_amount.setAttribute('readonly', '1');
                input_tax1_sub1_amount.setAttribute('id', 'tax1sub1amt'+parentServiceId);
                input_tax1_sub1_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tax1_sub1_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tax1_sub1_amount.setAttribute('class', 'inputbox w100');
                input_tax1_sub1_amount.setAttribute('size', '5');     
				
				div_cr3_c4t1.appendChild(div_cr3_c4_txt1);
                div_cr3_c4.appendChild(div_cr3_c4t1);
                div_cr3_c4.appendChild(input_tax1_sub1_amount);
                div_cr3_c2.appendChild(div_cr3_c4);
		 
				//sub2 bof
				div_cr3_c5 = document.createElement('div');
                div_cr3_c5.setAttribute('class', 'coloumn w110');
                div_cr3_c5.setAttribute('className', 'coloumn w110');                        
                div_cr3_c5t1 = document.createElement('div');
                if(tax1sub2name!=''){
                    div_cr3_c5_txt1 = document.createTextNode(tax1sub2name);
                }else{
                    div_cr3_c5_txt1 = document.createTextNode('Tax');
                }
           
                // Create the TEXTAREA field.
                var input_tax1_sub2_value = document.createElement('input');
                input_tax1_sub2_value.setAttribute('type', 'text');
                input_tax1_sub2_value.setAttribute('name', 'tax1_sub2_value[]');
                input_tax1_sub2_value.setAttribute('value', tax1sub2value);
                input_tax1_sub2_value.setAttribute('readonly', '1');
                input_tax1_sub2_value.setAttribute('id', 'txs1val'+parentServiceId);
                input_tax1_sub2_value.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tax1_sub2_value.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tax1_sub2_value.setAttribute('class', 'inputbox w100');
                input_tax1_sub2_value.setAttribute('size', '5');     
                
				div_cr3_c5t1.appendChild(div_cr3_c5_txt1);
                div_cr3_c5.appendChild(div_cr3_c5t1);
                div_cr3_c5.appendChild(input_tax1_sub2_value);
                
				
				 
				//sub1 tax amt 
			 	
				div_cr3_c6 = document.createElement('div');
                div_cr3_c6.setAttribute('class', 'coloumn w110');
                div_cr3_c6.setAttribute('className', 'coloumn w110');                        
                div_cr3_c6t1 = document.createElement('div');
                div_cr3_c6_txt1 = document.createTextNode('Sub Tax Amt');    
                
                // Create the TEXTfield.
                var input_tax1_sub2_amount = document.createElement('input');
                input_tax1_sub2_amount.setAttribute('type', 'text');
                input_tax1_sub2_amount.setAttribute('name', 'tax1_sub2_amount[]');
                input_tax1_sub2_amount.setAttribute('value', '0');
                input_tax1_sub2_amount.setAttribute('readonly', '1');
                input_tax1_sub2_amount.setAttribute('id', 'tax1sub1amt'+parentServiceId);
                input_tax1_sub2_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tax1_sub2_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tax1_sub2_amount.setAttribute('class', 'inputbox w100');
                input_tax1_sub2_amount.setAttribute('size', '5');     
				
				div_cr3_c6t1.appendChild(div_cr3_c6_txt1);
                div_cr3_c6.appendChild(div_cr3_c6t1);
                div_cr3_c6.appendChild(input_tax1_sub2_amount);
                div_cr3_c2.appendChild(div_cr3_c6);
				//sub2 eof
			
			
            
                div_cr3_c7 = document.createElement('div');
                div_cr3_c7.setAttribute('class', 'coloumn');
                div_cr3_c7.setAttribute('className', 'coloumn');                        
                div_cr3_c7t1 = document.createElement('div');
                div_cr3_c7_txt1 = document.createTextNode('Tot');    
                
                // Create the TEXTAREA field.
                var input_tot_amount = document.createElement('input');
                input_tot_amount.setAttribute('type', 'text');
                input_tot_amount.setAttribute('name', 'tot_amount[]');
                input_tot_amount.setAttribute('value', '0');
                input_tot_amount.setAttribute('readonly', '1');
                input_tot_amount.setAttribute('id', 'totamt'+parentServiceId);
                input_tot_amount.setAttribute('onblur', 'javascript: return updateTotal();');
                input_tot_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
                input_tot_amount.setAttribute('class', 'inputbox w100');
                input_tot_amount.setAttribute('size', '5');                
                
                div_cr3_c7t1.appendChild(div_cr3_c7_txt1);
                div_cr3_c7.appendChild(div_cr3_c7t1);
                div_cr3_c7.appendChild(input_tot_amount);
                
          
                div_cr3_c8 = document.createElement('div');
                div_cr3_c8.setAttribute('class', 'coloumn w110');
                div_cr3_c8.setAttribute('className', 'coloumn w110');                        
                div_cr3_c8t1 = document.createElement('div');
                div_cr3_c8_txt1 = document.createTextNode('Disc.Type');                    
               
                // Create the TEXTAREA field.
                var select_discount_type = document.createElement('select');
                select_discount_type.options[0] = new Option('','');
                select_discount_type.options[1] = new Option('OneTime','OneTime');
                select_discount_type.options[2] = new Option('Recurring','Recurring');
                select_discount_type.setAttribute('name', 'discount_type[]');
                select_discount_type.setAttribute('id', 'totamt'+parentServiceId);
                select_discount_type.setAttribute('class', 'inputbox w100');
                
                div_cr3_c8t1.appendChild(div_cr3_c8_txt1);
                div_cr3_c8.appendChild(div_cr3_c8t1);
                div_cr3_c8.appendChild(select_discount_type);
            
            div_cr3.appendChild(div_cr3_c1);
            div_cr3.appendChild(div_cr3_c2);
            div_cr3.appendChild(div_cr3_c3);
            div_cr3.appendChild(div_cr3_c4);
            div_cr3.appendChild(div_cr3_c5);
            div_cr3.appendChild(div_cr3_c6);
            div_cr3.appendChild(div_cr3_c7);
            div_cr3.appendChild(div_cr3_c8);
            div_col2.appendChild(div_cr3);
            // 2nd column of 1st row EOF
        
    
        div_row2 = document.createElement('div');        
        div_row2.setAttribute('class', 'row  wp500');
        div_row2.setAttribute('className', 'row  wp500');
        
            div_col21 = document.createElement('div');            
            div_col21.setAttribute('class', 'coloumn w110');
		    div_col21.setAttribute('className', 'coloumn w110');
                   
            
            div_colt21 = document.createElement('div');
            div_col_txt21= document.createTextNode('Purchase price');   
            var input_pp_amount = document.createElement('input');
                input_pp_amount.setAttribute('type', 'text');
                input_pp_amount.setAttribute('name', "pp_amount[]");
                input_pp_amount.setAttribute('value', '0');
                input_pp_amount.setAttribute('id', 'ppamt'+parentServiceId);
                input_pp_amount.setAttribute('class', 'inputbox w100');
                input_pp_amount.setAttribute('size', '5');
            div_colt21.appendChild(div_col_txt21);
            div_col21.appendChild(div_colt21);
            div_col21.appendChild(input_pp_amount);
            
  
            div_col22 = document.createElement('div');
            div_col22.setAttribute('class', 'coloumn w160');
		    div_col22.setAttribute('className', 'coloumn w160');            
            div_colt22 = document.createElement('div');
            div_col_txt22= document.createTextNode('Vendor');               
            var select_vendor = document.createElement('select');
               
                vendorNameList  = document.getElementsByName("vendorNameList[]");   
                vendorIdList  = document.getElementsByName("vendorIdList[]");   
                select_vendor.options[0] = new Option('vendor','');
                var j;
                for ( i=0; i< vendorNameList.length; i++ ) {                     
                    
                     var vendornm = vendorNameList[i].value;
                     var vendorid = vendorIdList[i].value;
                     select_vendor.options[i] = new Option(vendornm,vendorid);
                }              
                select_vendor.setAttribute('name', "vendor[]");
                select_vendor.setAttribute('id', 'vendor'+parentServiceId);
                select_vendor.setAttribute('class', 'inputbox w150');
                
            div_colt22.appendChild(div_col_txt22);
            div_col22.appendChild(div_colt22);
            div_col22.appendChild(select_vendor);
            
            div_col23 = document.createElement('div');
            div_col23.setAttribute('class', 'coloumn w200');
		    div_col23.setAttribute('className', 'coloumn w200');            
            div_colt23 = document.createElement('div');
            div_col_txt23= document.createTextNode('Purchase Particulars');   
                // Create the TEXTAREA field.
                var purchase_particulars = document.createElement('textarea');
                purchase_particulars.setAttribute('name', "purchase_particulars[]");
                purchase_particulars.setAttribute('class', 'inputbox');
                purchase_particulars.setAttribute('cols', '25');
                purchase_particulars.setAttribute('rows', '4');
                purchase_particulars.setAttribute('id', 'pur_part'+parentServiceId);
                purchase_particulars.setAttribute('style', 'height:90px;');
                purchase_particulars.setAttribute('onkeypress', 'javascript: return increaseHeight(this, event);');
            div_colt23.appendChild(div_col_txt23);
            div_col23.appendChild(div_colt23);
            div_col23.appendChild(purchase_particulars);    
    
     
      div_row1.appendChild(div_col1);
      div_row1.appendChild(div_col2);
      
      //div_row1.appendChild(div_col3);
      //div_row1.appendChild(div_col4);
      //div_row1.appendChild(div_col5);
      //div_row1.appendChild(div_col6);
      //div_row1.appendChild(div_col7);
      //div_row1.appendChild(div_col8);
      //div_row1.appendChild(div_col9);
      //div_row1.appendChild(div_col10);
      
      div_row2.appendChild(div_col21);
      div_row2.appendChild(div_col22);
      div_row2.appendChild(div_col23);
    
    div_main.appendChild(div_row);
    div_main.appendChild(div_row1);
    div_main.appendChild(div_row2);
    element.appendChild(div_main);
    
   
    return (true);
}




function addParticularDetailsIE(element,ssdivid,parentServiceId,title,punchline,taxid,taxname,taxvalue,taxpvalue,
tax1sub1id,tax1sub1name,tax1sub1value,tax1sub1pvalue,tax1sub2id,tax1sub2name,tax1sub2value,tax1sub2pvalue,
renewableVal) {    

    div_main = document.createElement('div');
    div_main.setAttribute('id', parentServiceId);
        div_row = document.createElement('div');
        div_row.setAttribute('class', 'row  wp100');
        div_row.setAttribute('className', 'row  wp100');
            div_col = document.createElement('div');
            div_col.setAttribute('class', 'coloumn  w100');            
            div_col.setAttribute('className', 'coloumn w100');   
                // Create the TEXTAREA field.
                var item = document.createElement('<input name="itemname[]">');
                item.setAttribute('type', 'text');               
                item.setAttribute('value', title+' '+punchline);
                item.setAttribute('class', 'inputbox');
                item.setAttribute('disabled', '1');
                item.setAttribute('cols', '28');
                item.setAttribute('rows', '4');
                item.setAttribute('id', "item"+parentServiceId);
                item.setAttribute('style', 'height:20px;width:350px');
                
            div_col.appendChild(item);
            div_row.appendChild(div_col);
            
        
        
        
        div_row1 = document.createElement('div');
        div_row1.setAttribute('class', 'row');
            div_colt1 = document.createElement('div');
            div_col_txt1 = document.createTextNode('Particulars');
            
            div_col1 = document.createElement('div');
            div_col1.setAttribute('class', 'coloumn w250');
		    div_col1.setAttribute('className', 'coloumn w250');
            
                // Create the TEXTAREA field.
                var text_parti = document.createElement('<textarea name="particulars[]">');
                text_parti.setAttribute('class', "inputbox");
                text_parti.setAttribute('cols', '28');
                text_parti.setAttribute('rows', '4');
                text_parti.setAttribute('id', "part"+parentServiceId);
                text_parti.setAttribute('style', "height:45px;");
                text_parti.setAttribute('onKeyPress', 'javascript: return increaseHeight(this, event);');
            div_colt1.appendChild(div_col_txt1);
            div_col1.appendChild(div_colt1);
            div_col1.appendChild(text_parti);
         
          
           
            div_colt2 = document.createElement('div');
            div_col_txt2 = document.createTextNode('Price');
            
            div_col2 = document.createElement('div');
            
            div_col2.setAttribute('class', 'coloumn w20');
		    div_col2.setAttribute('className', 'coloumn w20');
                // Create the Amount field.
                // <input type="text" name="p_amount[]" value="" class="inputbox" size="10"/>
                var input_amount = document.createElement('<input name="p_amount[]">');
                input_amount.setAttribute('type', "text");
                
                input_amount.setAttribute('value', '0');
                input_amount.setAttribute('id', "pamt"+parentServiceId);
                input_amount.setAttribute('onBlur', "javascript: return updateTotal();");
                input_amount.setAttribute('onKeyPress', "javascript:return hwndAddParticularField(this, event);");
                input_amount.setAttribute('class', "inputbox");
                input_amount.setAttribute('size', '5');
                div_colt2.appendChild(div_col_txt2);
                div_col2.appendChild(div_colt2);
                
               
                
                
                
                var input_sub_s_id = document.createElement('input');
                input_sub_s_id.setAttribute('type', 'hidden');
                input_sub_s_id.setAttribute('name', "sub_s_id[]");
                input_sub_s_id.setAttribute('id', 'sub_s_id'+parentServiceId);
                input_sub_s_id.setAttribute('value', '');
                
                var input_pid = document.createElement('input');
                input_pid.setAttribute('type', 'hidden');
                input_pid.setAttribute('name', "p_id[]");
                input_pid.setAttribute('value', '');
                
                var input_s_id = document.createElement('input');
                input_s_id.setAttribute('type', 'hidden');
                input_s_id.setAttribute('name', "s_id[]");
                input_s_id.setAttribute('value', parentServiceId);
                
                
                var input_ss_title = document.createElement('input');
                input_ss_title.setAttribute('type', 'hidden');
                input_ss_title.setAttribute('name', "ss_title[]");
                input_ss_title.setAttribute('value', title);
                
                var input_ss_punch_line = document.createElement('input');
                input_ss_punch_line.setAttribute('type', 'hidden');
                input_ss_punch_line.setAttribute('name', "ss_punch_line[]");
                input_ss_punch_line.setAttribute('value', punchline);
                
                
                var input_tax1_name = document.createElement('input');
                input_tax1_name.setAttribute('type', 'hidden');
                input_tax1_name.setAttribute('name', "tax1_name[]");
                input_tax1_name.setAttribute('value', taxname);
                
                var input_tax1_pvalue = document.createElement('input');
                input_tax1_pvalue.setAttribute('type', 'hidden');
                input_tax1_pvalue.setAttribute('name', "tax1_pvalue[]");
                input_tax1_pvalue.setAttribute('value', taxpvalue);
            
                 var input_is_rnwl = document.createElement('input');
                input_is_rnwl.setAttribute('type', 'hidden');
                input_is_rnwl.setAttribute('name', "is_renewable[]");
                input_is_rnwl.setAttribute('value', renewableVal);
                
            div_col2.appendChild(input_is_rnwl);
            div_col2.appendChild(input_tax1_name);
            div_col2.appendChild(input_tax1_pvalue);            
            div_col2.appendChild(input_ss_punch_line);
            div_col2.appendChild(input_ss_title);
            div_col2.appendChild(input_s_id);
            div_col2.appendChild(input_pid);
            div_col2.appendChild(input_sub_s_id);
            div_col2.appendChild(input_amount);
            
            div_col3 = document.createElement('div');
            div_col3.setAttribute('class', 'coloumn w20');
		    div_col3.setAttribute('className', 'coloumn w20');
            div_colt3 = document.createElement('div');   
            div_col_txt3 = document.createTextNode('Type');            
            
                var select_s_type = document.createElement('select');                
                select_s_type.options[0] = new Option('','');
     			select_s_type.options[1] = new Option('Qty','Qty');
                select_s_type.options[2] = new Option('Nos','Nos');
                select_s_type.options[3] = new Option('Yrs','Yrs');
                select_s_type.options[4] = new Option('Mth','Mth');
                select_s_type.setAttribute('name', "s_type[]");
                select_s_type.setAttribute('id', 's_type'+parentServiceId);
                select_s_type.setAttribute('class', 'inputbox w60');
            div_colt3.appendChild(div_col_txt3);
            div_col3.appendChild(div_colt3);
            div_col3.appendChild(select_s_type);    
                
            
            div_col4 = document.createElement('div');
            div_col4.setAttribute('class', 'coloumn w20');
		    div_col4.setAttribute('className', 'coloumn w20');
            div_colt4 = document.createElement('div');   
            div_col_txt4 = document.createTextNode('No');    
            
                var select_s_quantity = document.createElement('select');                
                select_s_quantity.options[0] = new Option('','');
                select_s_quantity.options[1] = new Option('1','1');
                select_s_quantity.options[2] = new Option('2','2');
                select_s_quantity.options[3] = new Option('3','3');
                select_s_quantity.options[4] = new Option('4','4');
                select_s_quantity.options[5] = new Option('5','5');
                select_s_quantity.setAttribute('name', "s_quantity[]");
                select_s_quantity.setAttribute('id', 's_quantity'+parentServiceId);
                select_s_quantity.setAttribute('onChange', 'javascript: return updateTotal();');
                select_s_quantity.setAttribute('class', 'inputbox w50');
                
            div_colt4.appendChild(div_col_txt4);
            div_col4.appendChild(div_colt4);
            div_col4.appendChild(select_s_quantity);    
            
            div_col5 = document.createElement('div');
            div_col5.setAttribute('class', 'coloumn w20');
		    div_col5.setAttribute('className', 'coloumn w20');
            div_colt5 = document.createElement('div');   
            div_col_txt5 = document.createTextNode('Cal Amt');
                // Create the TEXTAREA field.
                var input_s_amount = document.createElement('input');
                input_s_amount.setAttribute('type', 'text');
                input_s_amount.setAttribute('name', "s_amount[]");
                input_s_amount.setAttribute('value', '0');
                input_s_amount.setAttribute('id', 'samt'+parentServiceId);
                input_s_amount.setAttribute('onBlur', 'javascript: return updateTotal();');
                input_s_amount.setAttribute('onKeyPress', 'javascript:return hwndAddParticularField(this, event);');
                input_s_amount.setAttribute('class', 'inputbox');
                input_s_amount.setAttribute('size', '5');
            div_colt5.appendChild(div_col_txt5);
            div_col5.appendChild(div_colt5);
            div_col5.appendChild(input_s_amount);
            
            
            
            
        
            div_col6 = document.createElement('div');
            div_col6.setAttribute('class', 'coloumn w20');
		    div_col6.setAttribute('className', 'coloumn w20');
            div_colt6 = document.createElement('div');   
            div_col_txt6 = document.createTextNode('Discount');
                // Create the TEXTAREA field.
                var input_d_amount = document.createElement('input');
                input_d_amount.setAttribute('type', 'text');
                input_d_amount.setAttribute('name', "d_amount[]");
                input_d_amount.setAttribute('value', '0');
                input_d_amount.setAttribute('id', 'damt'+parentServiceId);
                input_d_amount.setAttribute('onBlur', 'javascript: return updateTotal();');
                input_d_amount.setAttribute('onKeyPress', 'javascript:return hwndAddParticularField(this, event);');
                input_d_amount.setAttribute('class', 'inputbox');
                input_d_amount.setAttribute('size', '5');
            div_colt6.appendChild(div_col_txt6);
            div_col6.appendChild(div_colt6);
            div_col6.appendChild(input_d_amount);
            
            div_col7 = document.createElement('div');            
            div_col7.setAttribute('class', 'coloumn w20');
		    div_col7.setAttribute('className', 'coloumn w20');
            div_colt7 = document.createElement('div');
            div_col_txt7 = document.createTextNode('SubTot');
                // Create the TEXTAREA field.
                var input_stot_amount = document.createElement('input');
                input_stot_amount.setAttribute('type', 'text');
                input_stot_amount.setAttribute('name', "stot_amount[]");
                input_stot_amount.setAttribute('value', '0');
                input_stot_amount.setAttribute('id', 'stotamt'+parentServiceId);
                input_stot_amount.setAttribute('onBlur', 'javascript: return updateTotal();');
                input_stot_amount.setAttribute('onKeyPress', 'javascript:return hwndAddParticularField(this, event);');
                input_stot_amount.setAttribute('class', 'inputbox');
                input_stot_amount.setAttribute('size', '5');
            div_colt7.appendChild(div_col_txt7);
            div_col7.appendChild(div_colt7);
            div_col7.appendChild(input_stot_amount);
            
            div_col8 = document.createElement('div');
            div_col8.setAttribute('class', 'coloumn w20');
		    div_col8.setAttribute('className', 'coloumn w20');
            div_colt8 = document.createElement('div');
    
            if(taxname!=''){
                 div_col_txt8 = document.createTextNode(taxname);
            }else{
                div_col_txt8 = document.createTextNode('Tax');
            }
                // Create the TEXTAREA field.
                var input_tax1_value = document.createElement('input');
                input_tax1_value.setAttribute('type', 'text');
                input_tax1_value.setAttribute('name', "tax1_value[]");
                input_tax1_value.setAttribute('value', taxvalue);
                input_tax1_value.setAttribute('readonly', '1');
                input_tax1_value.setAttribute('id', 'txval'+parentServiceId);
                input_tax1_value.setAttribute('onBlur', 'javascript: return updateTotal();');
                input_tax1_value.setAttribute('onKeyPress', 'javascript:return hwndAddParticularField(this, event);');
                input_tax1_value.setAttribute('class', 'inputbox');
                input_tax1_value.setAttribute('size', '5');
            div_colt8.appendChild(div_col_txt8);
            div_col8.appendChild(div_colt8);
            div_col8.appendChild(input_tax1_value);
           
            div_col9 = document.createElement('div');
            div_col9.setAttribute('class', 'coloumn w20');
		    div_col9.setAttribute('className', 'coloumn w20');
            div_colt9 = document.createElement('div');
            div_col_txt9= document.createTextNode('Tot');
                // Create the TEXTAREA field.
                var input_tot_amount = document.createElement('input');
                input_tot_amount.setAttribute('type', 'text');
                input_tot_amount.setAttribute('name', "tot_amount[]");
                input_tot_amount.setAttribute('value', '0');
                input_tot_amount.setAttribute('readonly', '1');
                input_tot_amount.setAttribute('id', 'totamt'+parentServiceId);
                input_tot_amount.setAttribute('onBlur', 'javascript: return updateTotal();');
                input_tot_amount.setAttribute('onKeyPress', 'javascript:return hwndAddParticularField(this, event);');
                input_tot_amount.setAttribute('class', 'inputbox');
                input_tot_amount.setAttribute('size', '5');
            div_colt9.appendChild(div_col_txt9);
            div_col9.appendChild(div_colt9);
            div_col9.appendChild(input_tot_amount);
            
            div_col10 = document.createElement('div');
            div_col10.setAttribute('class', 'coloumn w20');
		    div_col10.setAttribute('className', 'coloumn w20');
            div_colt10 = document.createElement('div');
            div_col_txt10= document.createTextNode('Disc.Type');            
                // Create the TEXTAREA field.
                var select_discount_type = document.createElement('select');
                select_discount_type.options[0] = new Option('','');
                select_discount_type.options[1] = new Option('OneTime','1');
                select_discount_type.options[2] = new Option('Recurring','2');
                select_discount_type.setAttribute('name', "discount_type[]");
                select_discount_type.setAttribute('id', 'totamt'+parentServiceId);
                select_discount_type.setAttribute('class', 'inputbox w100');
            div_colt10.appendChild(div_col_txt10);
            div_col10.appendChild(div_colt10);
            div_col10.appendChild(select_discount_type);
    
        div_row2 = document.createElement('div');        
        div_row2.setAttribute('class', 'row  wp100');
        div_row2.setAttribute('className', 'row  wp100');
        
            div_col21 = document.createElement('div');            
            div_col21.setAttribute('class', 'coloumn w20');
		    div_col21.setAttribute('className', 'coloumn w20');
                   
            
            div_colt21 = document.createElement('div');
            div_col_txt21= document.createTextNode('Purchase price');   
            var input_pp_amount = document.createElement('input');
                input_pp_amount.setAttribute('type', 'text');
                input_pp_amount.setAttribute('name', "pp_amount[]");
                input_pp_amount.setAttribute('value', '0');
                input_pp_amount.setAttribute('id', 'ppamt'+parentServiceId);
                input_pp_amount.setAttribute('class', 'inputbox');
                input_pp_amount.setAttribute('size', '5');
            div_colt21.appendChild(div_col_txt21);
            div_col21.appendChild(div_colt21);
            div_col21.appendChild(input_pp_amount);
            
  
            div_col22 = document.createElement('div');
            div_col22.setAttribute('class', 'coloumn w20');
		    div_col22.setAttribute('className', 'coloumn w20');            
            div_colt22 = document.createElement('div');
            div_col_txt22= document.createTextNode('Vendor');               
            var select_vendor = document.createElement('select');
               
                vendorNameList  = document.getElementsByName("vendorNameList[]");   
                vendorIdList  = document.getElementsByName("vendorIdList[]");   
                select_vendor.options[0] = new Option('vendor','');
                var j;
                for ( i=0; i< vendorNameList.length; i++ ) {                     
                    
                     var vendornm = vendorNameList[i].value;
                     var vendorid = vendorIdList[i].value;
                     select_vendor.options[i] = new Option(vendornm,vendorid);
                }              
                select_vendor.setAttribute('name', "vendor[]");
                select_vendor.setAttribute('id', 'vendor'+parentServiceId);
                select_vendor.setAttribute('class', 'inputbox w150');
                
            div_colt22.appendChild(div_col_txt22);
            div_col22.appendChild(div_colt22);
            div_col22.appendChild(select_vendor);
            
            div_col23 = document.createElement('div');
            div_col23.setAttribute('class', 'coloumn w20');
		    div_col23.setAttribute('className', 'coloumn w20');            
            div_colt23 = document.createElement('div');
            div_col_txt23= document.createTextNode('Purchase Particulars');   
                // Create the TEXTAREA field.
                var purchase_particulars = document.createElement('textarea');
                purchase_particulars.setAttribute('name', "purchase_particulars[]");
                purchase_particulars.setAttribute('class', 'inputbox');
                purchase_particulars.setAttribute('cols', '28');
                purchase_particulars.setAttribute('rows', '4');
                purchase_particulars.setAttribute('id', 'pur_part'+parentServiceId);
                purchase_particulars.setAttribute('style', 'height:45px;');
                purchase_particulars.setAttribute('onKeyPress', 'javascript: return increaseHeight(this, event);');
            div_colt23.appendChild(div_col_txt23);
            div_col23.appendChild(div_colt23);
            div_col23.appendChild(purchase_particulars);    
    
     
      div_row1.appendChild(div_col1);
      div_row1.appendChild(div_col2);
      div_row1.appendChild(div_col3);
      div_row1.appendChild(div_col4);
      div_row1.appendChild(div_col5);
      div_row1.appendChild(div_col6);
      div_row1.appendChild(div_col7);
      div_row1.appendChild(div_col8);
      div_row1.appendChild(div_col9);
      div_row1.appendChild(div_col10);
      
      div_row2.appendChild(div_col21);
      div_row2.appendChild(div_col22);
      div_row2.appendChild(div_col23);
    
    div_main.appendChild(div_row);
    div_main.appendChild(div_row1);
    div_main.appendChild(div_row2);
    element.appendChild(div_main);
	
    return (true);
}
 
function updateExchangeRate(){

   document.getElementById('exchange_rate').value=1;
   serviceNo = document.getElementsByName('service_id[]').length;
    div_particular =  document.getElementById("particularsub") ;
    
    for(var i = 0; i<serviceNo ;i++){
        if(document.frmOrderAdd.service_id[i].checked){
            document.frmOrderAdd.service_id[i].checked = false  ;
            parent_id = document.frmOrderAdd.service_id[i].value;
            div_particular.removeChild(document.getElementById(parent_id)) ;         
        }
    }
}

//2010-10-oct-27
function updateQuantity(){

    p_amount  = document.getElementsByName('p_amount[]');
    s_type  = document.getElementsByName('s_type[]');
    s_quantity  = document.getElementsByName('s_quantity[]');
    s_amount  = document.getElementsByName('s_amount[]');
    dst_amount  = document.getElementsByName('d_amount[]');
    stot_amount  = document.getElementsByName('stot_amount[]');
    tax1_value  = document.getElementsByName('tax1_value[]');
	tax1_pvalue  = document.getElementsByName('tax1_pvalue[]'); 
	
    tot_amount  = document.getElementsByName('tot_amount[]');
    
    tax1_sub1_value  = document.getElementsByName('tax1_sub1_value[]');
	tax1_sub1_pvalue  = document.getElementsByName('tax1_sub1_pvalue[]');
    tot_sub1_amount  = document.getElementsByName('tot_sub1_amount[]');
	tax1_sub2_value  = document.getElementsByName('tax1_sub2_value[]');
	tax1_sub2_pvalue  = document.getElementsByName('tax1_sub2_pvalue[]');
    tot_sub2_amount  = document.getElementsByName('tot_sub2_amount[]');
    
    var total   = 0.0;

    for ( i=0; i< p_amount.length; i++ ) {
    
        if ( parseFloat(p_amount[i].value) ) {
            //total = total + parseFloat(lst_amount[i].value) ;
        }
        if(dst_amount[i].value != 0){
            stot_amount[i].value =  parseFloat(p_amount[i].value) - parseFloat(dst_amount[i].value) ;
        }else{
            stot_amount[i].value =  parseFloat(p_amount[i].value) ;
        }
       
        if(parseFloat(s_quantity[i].value) > 0){
            // s_amount[i].value =parseFloat(p_amount[i].value);
        }
       
        taxamt= parseFloat(tax1_pvalue[i].value) * parseFloat(stot_amount[i].value)  ;
        tax1_amount[i].value =taxamt.toFixed(2);
		
		taxs1amt= parseFloat(tax1_sub1_pvalue[i].value) * parseFloat(tax1_amount[i].value)  ;
		taxs2amt= parseFloat(tax1_sub2_pvalue[i].value) * parseFloat(tax1_amount[i].value)  ;
		
		tax1_sub1_amount[i].value =taxs1amt.toFixed(2);
		tax1_sub2_amount[i].value =taxs2amt.toFixed(2);
		
		tot_amount[i].value = addPercentAmount(parseFloat(stot_amount[i].value),tax1_pvalue[i].value);
		tot_amount[i].value = parseFloat(tot_amount[i].value ) + parseFloat(tax1_sub1_amount[i].value) + parseFloat(tax1_sub2_amount[i].value) ;
        total = parseFloat(total) + parseFloat(tot_amount[i].value) ;

	}
	 total = total.toFixed(2);
    document.frmOrderAdd.amount.value = total;
}

//create particular  fields on  fly bof
 
 
