

/**
* To Load api depending on gateway in SMS purchase Module
**/


//new added
var http_req_alerts;
var http_req_alerts1;

function updateAccount(client_id) {
	
    var cboModule = document.getElementById("client_acc_id");
   
    cboModule.options.length = 0;
    if(!client_id){
        cboModule.options[0] = new Option("--SELECT--", "-2");	
        cboModule.options[1] = new Option("None", "-1");	
        return null;
    }
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""
    
    var uri = "get-sms-account.php?client_id="+client_id;    
    
    http_req_alerts.onreadystatechange = loadAccount;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

/**
* To Load api depending on gateway in SMS purchase Module
**/

function loadAccount(){
    var cboModule = document.getElementById("client_acc_id");
   
  
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
        
            if(allThoseTypes){			
                
                cboModule.options[0] = new Option("--SELECT--", "-1");
                //cboModule.options[1] = new Option("None", "-1");
                cboModule.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");
            
              for (var i=0;i<typeSplit.length;i++){
            
                var tmpStr =typeSplit[i];
                if(tmpStr){
                    var tmpArray = new Array();
                    tmpArray = tmpStr.split("|");
                    var tmpName =tmpArray[0];
                    var tmpVal = tmpArray[1];
                   
                    if(tmpName!=''){
                        cboModule.options[i+1] = new Option(tmpName, tmpVal);   
                                             
                    }
                }
              }           
            }
        }
    }else{        
          cboModule.options[0] = new Option("Loading...", "");
          cboModule.style.background = "#c3c3c3";
  }
}


function dispalyDetails(client_acc_id) {
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""
    
    var uri = "get-account-details.php?client_acc_id="+client_acc_id;    
    
    http_req_alerts.onreadystatechange = loadAccountDetails;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}
function loadAccountDetails(){

    var bal = document.getElementById("bal");
    var balance_sms = document.getElementById("balance_sms");
   
  
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
      
            if(allThoseTypes){			
                
               bal.innerHTML= allThoseTypes;
               balance_sms.value = allThoseTypes;
            }
        }
    }else{        
         bal.innerHTML = '';
          
  }
}