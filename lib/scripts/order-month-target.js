
//AJAX SUBMIT FORM BOF
  
 //AJAX PAGINATION FUNCTION BOF
function changeRecordList(page,pagename,form1){ 
	http_req_alerts = false;			
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_req_alerts = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts) {
		return false;
	}

	//var poststr = createQueryString(form1);	
	//var poststr =Url.decode($(form1).serialize()) ;
	 
	poststr = "sString="+document.getElementById("sString").value+"&sType="+document.getElementById("sType").value+
	"&sOrderBy="+document.getElementById("sOrderBy").value+"&sOrder="+document.getElementById("sOrder").value+"&date_from="+
	document.getElementById("date_from").value+"&date_to="+document.getElementById("date_to").value+
	"&perform="+document.getElementById("perform").value+"&rpp="+document.getElementById("rpp").value ;
	
	var uri = pagename+"?ajx=1&x="+page+"&"+poststr ; 
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';
	
	$('#inner_content').load(uri);
	
 
} 

function loadRecordList(){
	if(http_req_alerts.readyState==4){
        if (http_req_alerts.status == 200){
            var allData = http_req_alerts.responseText;
            if(allData){
			    document.getElementById("record_info").innerHTML = allData; 
            }
        }
    }else{
		document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';         
    }
} 
//AJAX PAGINATION FUNCTION EOF
function clearSearch(){
	document.getElementById("sString").value='' ;
	document.getElementById("sType").value='' ;
	document.getElementById("sOrderBy").value='do_add' ;
	document.getElementById("sOrder").value='DESC' ;
	document.getElementById("date_from").value='' ;
	document.getElementById("date_to").value='' ;
	
	 
}

function searchRecord(pagename,form1){
	changeRecordList(1,pagename,form1);
	document.getElementById("pageno").value=1 ;	
	return false;
}
function deleteRecord(surl){
 
	var del=reallyDel('Are you sure you want to delete this Record?');

	if(del==true){
	
		
		$( "#dialog-form" ).dialog({
				autoOpen: false,
				height: 600,
				width: 900,
				resizable: true,
				closeText: 'close',
				title: 'SMEERP - NERVE CENTER',
				modal: true,
				close: function() {
					
					searchRecord('order-month-target.php',frmSearch);
					//changeRecordList(pageno,'order-month-target.php',frmSearch);
					//$( "#dialog-form" ).dialog( "destroy" );
				}
		});
		ajxUrl= surl +"&ajx=1" ;
		$("#dialog-form").dialog("open");
		var iFrame =  document.getElementById('dialogIframeId');
		iFrameBody = iFrame.contentDocument.getElementsByTagName('body')[0];
		iFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
		$("#dialogIframeId").attr("src",ajxUrl);
		
		
		
	}
	return false;
}
function addRecord(surl){

	
	$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 900,
			resizable: true,
			closeText: 'close',
			title: 'SMEERP - NERVE CENTER',
			modal: true,
			close: function() {
			
				var pageno = document.getElementById("pageno").value ;
				url = SITE_URL+"/"+ADMIN_PANEL+"/order-month-target.php?perform=list&pageno="+pageno ;
				//searchRecord('order-month-target.php',frmSearch);
				changeRecordList(pageno,'order-month-target.php',frmSearch);
				//$( "#dialog-form" ).dialog( "destroy" );
			}
	});
   ajxUrl= surl +"&ajx=1" ;
   $("#dialog-form").dialog("open");
   var iFrame =  document.getElementById('dialogIframeId');
   iFrameBody = iFrame.contentDocument.getElementsByTagName('body')[0];
   iFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
  
   return false; 
	
} 
function editRecord(surl){
   
   $( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 600,
		width: 900,
		resizable: true,
		closeText: 'close',
		title: 'SMEERP - NERVE CENTER',
		modal: true,
		close: function() {
			
			var pageno = document.getElementById("pageno").value ;
			url = SITE_URL+"/"+ADMIN_PANEL+"/order-month-target.php?perform=list&pageno="+pageno ;
			//searchRecord('order-month-target.php',frmSearch);
			changeRecordList(pageno,'order-month-target.php',frmSearch);
			//$( "#dialog-form" ).dialog( "destroy" );
		}
	});
   
   ajxUrl= surl +"&ajx=1" ;	 
   $("#dialog-form").dialog("open");
   var iFrame =  document.getElementById('dialogIframeId');
   iFrameBody = iFrame.contentDocument.getElementsByTagName('body')[0];
   iFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
  return false; 
}

function listRecord(surl){
	
	ajxUrl= surl +"&ajx=1" ;
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';
	$('#inner_content').load(ajxUrl);
	   return false; 
}

function eTimer(url) {
	var t=setTimeout("window.location.href='"+url+"'",1000);
	$('#loading').fadeOut('slow');
}
	
//AJAX SUBMIT FORM EOF

/**
 * This function is used to add new elements
 * to the existing HTML elements.
 * This particular function adds a DIV tag to the parent 
 * element identified by 'element'.
 *
 * @param   HTML Element    this can be reference to any HTML element eg. DIV, TABLE, TD, P.
 * 
 *
 */

   