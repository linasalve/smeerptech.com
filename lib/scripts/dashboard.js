function showExecutiveDetails(executive_id) {
	param = 'left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1';
    window.open('./user.php?perform=view&user_id='+executive_id,'executiveDetails', param);
    return (true);
}

function showOrderDetails(order_id) {
	param = 'left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1';
    window.open('./bill-order.php?perform=view&or_id='+order_id,'orderDetails', param);
    return (true);
}


function showOrderSRS(order_id) {
	param = 'left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1';
    window.open('./project-task.php?perform=srs&or_id='+order_id, 'orderSRS', param);
    return (true);
}


function showClientDetails(client_id) {
	param = 'left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1';
    window.open('./clients.php?perform=view&user_id='+client_id, 'clientDetails', param);
    return (true);
}

function changePageWorkOrder(page) {
    var q_string = "?";

    if ( page != "" && page > 0 ) {
        q_string += "&xwo="+ page;
    }
    else {
        q_string += "&xwo=1";
    }
    window.location.href = q_string;
    return false;
}