//Function to update the Banner status using AJAX BOF
var http_req_alerts_banner;
function updateStatus( id,status){

	http_req_alerts_banner = false;	
	document.getElementById("saleleadenqid").value = id;
	
	
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    } 
   
    var uri = "get-sale-lead-enquiry-details.php?id="+ id+"&status="+status;    

    http_req_alerts_banner.onreadystatechange = loadStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);

}
function loadStatus(){
     id = document.getElementById("saleleadenqid").value ;
	 rowid = 'row'+id ;
	if(http_req_alerts_banner.readyState==4){
   
        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
            
			
            if(allData){            
            
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg =dataSplit[0];
                var dataStatus =dataSplit[1];
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(rowid).innerHTML= dataStatus; 
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
        
        
    }

}
//Function to update the Banner status using AJAX BOF


function clearProductFields(element1,element2) {
   
    element1.value = '';
    element2.value = '';
    return true;
   
}