// Variables decalaration used for the Top Menu
var	top_menu = Array();
var tm_div;
var tm_div_childs;
var tm_items;
var under_hover;	// Contains the Menu Id which is currently selected.
var hwndHideMenu; 	// Timer instance.

function initializeTopMenu() {
	tm_div			= document.getElementById('top-menu');
	tm_div_childs 	= tm_div.childNodes;
	tm_items;

	for (i=0; i<tm_div_childs.length; i++) {

		if ( tm_div_childs[i].nodeName.toLowerCase() == 'li' ) {
			temp 		= top_menu;
			count 		= temp.length+1;
			top_menu 	= Array(count);
			
			for ( x=0; x<count; x++ ) {
				top_menu[x] = temp[x];
			}
			index 				= count-1;
			top_menu[index]		= Array(2);
			top_menu[index][0] 	= i;
			top_menu[index][1] 	= Array();
			tm_items 		= tm_div_childs[i].childNodes;

			for (j=0; j<tm_items.length; j++) {
				if ( tm_items[j].nodeName.toLowerCase() == 'span' ) {
					top_menu[index][0] 	= tm_items[j].getAttribute('id');
				}
				else if ( tm_items[j].nodeName.toLowerCase() == 'ul' ) {
					top_menu[index][1] 	= tm_items[j].getAttribute('id');
				}
			}
		}
	}
}


function hover(menu_id) {
	if ( menu_id != under_hover ) {
		// Show the Hover image
		under_hover = menu_id;
		setBackImage(menu_id, './../media/images/top-menu/hover.jpg');
		clearTimeout(hwndHideMenu);
		hideMenu();
		
		if (document.getElementById('sub-'+ menu_id)) {
			document.getElementById('sub-'+ menu_id).style.display = 'block';
		}
	}
}

function hoverOut(menu_id) {
	// Hide the Hover image
	under_hover = '';
	unsetBackImage(menu_id);
	hwndHideMenu = setTimeout('hideMenu()', 100);
}

function hideMenu() {
	for (i=0; i<top_menu.length; i++) {
		if ( top_menu[i][1] != '' ) {
			document.getElementById(top_menu[i][1]).style.display = 'none';
		}
	}
}