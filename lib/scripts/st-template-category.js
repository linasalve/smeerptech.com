//AJAX PAGINATION FUNCTION BOF
var http_req_alerts;
function changeRecordList(page,pagename,form1){ 
	http_req_alerts = false;			
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_req_alerts = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts) {
		return false;
	}

	var slist = document.getElementById('sStatus'); 
	st_options = "";
	for(var i = 0; i < slist.options.length; ++i){
		comma = ",";
		if(slist.options[i].selected){
			st_options = st_options + slist[i].value + comma;
		}
	}
	st_options = trim(st_options,",");
	 
	var chk_date_from=chk_date_to=chk_status='';
	if(document.getElementById("chk_date_from").checked){
		chk_date_from = 'AND' ;
	}
	if(document.getElementById("chk_date_to").checked ){
		chk_date_to = 'AND' ;
	}
	if(document.getElementById("chk_status").checked ){
		chk_status = 'AND' ;
	}

	
	poststr = "sString="+document.getElementById("sString").value+"&sType="+document.getElementById("sType").value+
	"&sOrderBy="+document.getElementById("sOrderBy").value+"&sOrder="+document.getElementById("sOrder").value+	
	"&chk_status="+chk_status+"&sStatus="+st_options+"&chk_date_from="+chk_date_from+"&date_from="+document.getElementById("date_from").value+"&date_to="+document.getElementById("date_to").value+"&chk_date_to="+chk_date_to+"&dt_field="+document.getElementById("dt_field").value+"&perform="+document.getElementById("perform").value+"&rpp="+document.getElementById("rpp").value 
	
	var uri = pagename+"?ajx=1&x="+page+"&"+poststr ; 
	
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';	
	$('#inner_content').load(uri);
	
 
} 

function loadRecordList(){
	if(http_req_alerts.readyState==4){
        if (http_req_alerts.status == 200){
            var allData = http_req_alerts.responseText;
            if(allData){
			    document.getElementById("record_info").innerHTML = allData; 
            }
        }
    }else{
		document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';         
    }
} 
//AJAX PAGINATION FUNCTION EOF
function clearSearch(){
	document.getElementById("sString").value='' ;
	document.getElementById("sType").value='' ;
	document.getElementById("sOrderBy").value='do_add' ;
	document.getElementById("sOrder").value='DESC' ;
	document.getElementById("date_from").value='' ;
	document.getElementById("date_to").value='' ;
}

function searchRecord(pagename,form1){
	
	changeRecordList(1,pagename,form1);
	document.getElementById("pageno").value=1 ;	
	return false;
}
function deleteRecord(surl){
 
	var del=reallyDel('Are you sure you want to delete this Record?');

	if(del==true){
	
		
		$( "#dialog-form" ).dialog({
				autoOpen: false,
				height: 600,
				width: 900,
				resizable: true,
				closeText: 'close',
				title: 'SMEERP - NERVE CENTER',
				modal: true,
				close: function() {
					
					searchRecord('st-template-category.php',frmSearch);
					//changeRecordList(pageno,'prospects.php',frmSearch);
					//$( "#dialog-form" ).dialog( "destroy" );
				}
		});
		ajxUrl= surl +"&ajx=1" ;
		$("#dialog-form").dialog("open");	
		var diFrame =  document.getElementById('dialogIframeId');
		if (navigator.appName.indexOf("Microsoft")!=-1) {
			diFrameBody = document.frames['dialogIframeId'].document;

		}else{
		  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
		}
		diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
		
		$("#dialogIframeId").attr("src",ajxUrl);
		
		
		
	}
	return false;
} 
function editStatus(surl){
 
	var del=reallyDel('Are you sure you want to change the Status?');

	if(del==true){
	
		
		$( "#dialog-form" ).dialog({
				autoOpen: false,
				height: 600,
				width: 900,
				resizable: true,
				closeText: 'close',
				title: 'SMEERP - NERVE CENTER',
				modal: true,
				close: function() {
					
					searchRecord('st-template-category.php',frmSearch);
					//changeRecordList(pageno,'prospects.php',frmSearch);
					//$( "#dialog-form" ).dialog( "destroy" );
				}
		});
		ajxUrl= surl +"&ajx=1" ;
		$("#dialog-form").dialog("open");
		var diFrame =  document.getElementById('dialogIframeId');
		if (navigator.appName.indexOf("Microsoft")!=-1) {
			diFrameBody = document.frames['dialogIframeId'].document;
		}else{
		  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
		}
		diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
		$("#dialogIframeId").attr("src",ajxUrl);
		
	}
	return false;
}

function addRecord(surl){

	
	$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 900,
			resizable: true,
			closeText: 'close',
			title: 'SMEERP - NERVE CENTER',
			modal: true,
			close: function() {
			
				var pageno = document.getElementById("pageno").value ;
				url = SITE_URL+"/"+ADMIN_PANEL+"/st-template-category.php?perform=list&pageno="+pageno ;
				//searchRecord('prospects.php',frmSearch);
				changeRecordList(pageno,'st-template-category.php',frmSearch);
				//$( "#dialog-form" ).dialog( "destroy" );
			}
	});
    ajxUrl= surl +"&ajx=1" ;
 
    $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;
	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
    $("#dialogIframeId").attr("src",ajxUrl);
    return false; 
}
 
function editRecord(surl){
   
   $( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 600,
		width: 900,
		resizable: true,
		closeText: 'close',
		title: 'SMEERP - NERVE CENTER',
		modal: true,
		close: function() {
			
			var pageno = document.getElementById("pageno").value ;
			url = SITE_URL+"/"+ADMIN_PANEL+"/st-template-category.php?perform=list&pageno="+pageno ;
			changeRecordList(pageno,'st-template-category.php',frmSearch);
			//$( "#dialog-form" ).dialog( "destroy" );
		}
	});
   
   ajxUrl= surl +"&ajx=1" ;	 
   $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;

	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
  return false; 
}

function viewRecord(surl){

	
	$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 900,
			resizable: true,
			closeText: 'close',
			title: 'SMEERP - NERVE CENTER',
			modal: true,
			close: function(){
			
				/* var pageno = document.getElementById("pageno").value ;
				url = SITE_URL+"/"+ADMIN_PANEL+"/st-template-category.php?perform=list&pageno="+pageno ;
				changeRecordList(pageno,'st-template-category.php',frmSearch); */
				//$( "#dialog-form" ).dialog( "destroy" );
			}
	});
    ajxUrl= surl +"&ajx=1" ;
 
    $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;

	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
  
   return false; 
	
} 
function listRecord(surl){
	
	ajxUrl= surl +"&ajx=1" ;
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';
	$('#inner_content').load(ajxUrl);
	   return false; 
}
/*AJAX NAVIGATION EOF*/ 
 
  