$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 800,
			resizable: true,
			closeText: 'close',
			title: 'Add New Record',
			modal: true,
			close: function() {
				url = SITE_URL+"/"+ADMIN_PANEL+"/vendors-jobwork.php?perform=list" ;
				var pageno = document.getElementById("pageno").value ;
				changeRecordList(pageno,'vendors-jobwork.php',frmSearch);
				//allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});
	});
//AJAX SUBMIT FORM BOF
function validateFormAdd(form1){	
	var returnVal= true;	
	/* if(document.frm.title.value=='') {	 
		var str = getValidateStr("Please enter the Job title") ;		
		document.getElementById('err_title_id').innerHTML = str;
		returnVal= false;
	}else{		
		document.getElementById('err_title_id').innerHTML ='';
	}
	if(document.frm.vendors_id.value=='') {	 
		var str = getValidateStr("Please select Vendor") ;		
		document.getElementById('err_vendors_id').innerHTML = str;
		returnVal= false;
	}else{		
		document.getElementById('err_vendors_id').innerHTML ='';
	} */
	if(returnVal==true){		
		var poststr = createQueryString(form1);
	/* 	document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';  */
		makePOSTRequest('vendors-jobwork.php?perform=add&ajx=1', poststr,1);		
		return false;
	}
	return false;	
}
function validateFormEdit(form1){	
	var returnVal= true;	
	/* if(document.frm.title.value=='') {	 
		var str = getValidateStr("Please enter the Job title") ;		
		document.getElementById('err_title_id').innerHTML = str;
		returnVal= false;
	}else{		
		document.getElementById('err_title_id').innerHTML ='';
	}
	if(!isset(document.frm.vendors_id.value) || document.frm.vendors_id.value=='') {	 
		var str = getValidateStr("Please select Vendor") ;		
		document.getElementById('err_vendors_id').innerHTML = str;
		returnVal= false;
	}else{		
		document.getElementById('err_vendors_id').innerHTML ='';
	} */
	
	if(returnVal==true){		
		var poststr = createQueryString(form1);
		/* document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';  */
		makePOSTRequest('vendors-jobwork.php?perform=edit&ajx=1', poststr,1);		
		return false;
	}
	return false;	
}
var http_request_pt = false;
function makePOSTRequest(url, parameters,is_div_dialog) {	
      http_request_pt = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request_pt = new XMLHttpRequest();
         if (http_request_pt.overrideMimeType) {
         	// set type accordingly to anticipated content type
            //http_request_pt.overrideMimeType('text/xml');
            http_request_pt.overrideMimeType('text/html');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request_pt = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request_pt = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request_pt) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }
	  if(is_div_dialog==1){
		http_request_pt.onreadystatechange = showDialogInfo ;		
	  }else{
		http_request_pt.onreadystatechange = showRecordInfo;
	  }
      http_request_pt.open('POST', url, true);
      http_request_pt.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      http_request_pt.setRequestHeader("Content-length", parameters.length);
      http_request_pt.setRequestHeader("Connection", "close");
      http_request_pt.send(parameters);
}
function showDialogInfo() {
    if (http_request_pt.readyState == 4) {
         if (http_request_pt.status == 200) {            
            allData = http_request_pt.responseText;			
			if(allData){		
				document.getElementById("dialog-form").innerHTML = allData; 
			}			
         } else {
            alert('There was a problem with the request.');
         }
    }
 }
function showRecordInfo() {
    if (http_request_pt.readyState == 4) {
         if (http_request_pt.status == 200) {            
            allData = http_request_pt.responseText;			
			if(allData){		
				document.getElementById("record_info").innerHTML = allData; 
				
			}			
         } else {
            alert('There was a problem with the request.');
         }
    }
 }
var http_req_alerts ; 
function makeGETRequest(url,is_div_dialog){
	http_req_alerts = false;			
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_req_alerts = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts) {
		return false;
	}  
	if(is_div_dialog==1){
		http_req_alerts.onreadystatechange = loadRecordDialog;
	}else{
		http_req_alerts.onreadystatechange = loadRecordList;
	}
	//http_req_alerts.onreadystatechange = loadRecordList;
	http_req_alerts.open('GET', url, true);
	http_req_alerts.send(null);
}

function loadRecordDialog(){
	if(http_req_alerts.readyState==4){
        if (http_req_alerts.status == 200){
            var allData = http_req_alerts.responseText;
            if(allData){				
                $("#dialog-form").dialog("open");
				document.getElementById("dialog-form").innerHTML = allData; 
            }
        }
    }else{
		document.getElementById("dialog-form").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';         
    }
} 

 //AJAX PAGINATION FUNCTION BOF
function changeRecordList(page,pagename,form1){     
 
	http_req_alerts = false;			
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_req_alerts = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts) {
		return false;
	}
	var poststr = createQueryString(form1);	
    var uri = pagename+"?ajx=1&x="+page+poststr ; 
	document.getElementById("pageno").value=page;
	http_req_alerts.onreadystatechange = loadRecordList;
	http_req_alerts.open('GET', uri, true);
	http_req_alerts.send(null);
} 

function loadRecordList(){
	if(http_req_alerts.readyState==4){
        if (http_req_alerts.status == 200){
            var allData = http_req_alerts.responseText;
            if(allData){
			    document.getElementById("record_info").innerHTML= allData; 
            }
        }
    }else{
		document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';         
    }
} 
//AJAX PAGINATION FUNCTION EOF
function clearSearch(){
	document.frmSearch.sString.value='' ; 
	document.frmSearch.sType.value='-1' ; 
	document.frmSearch.sOrderBy.value='do_add' ; 
	document.frmSearch.sOrder.value='DESC' ; 
	document.frmSearch.date_from.value='' ; 
	document.frmSearch.date_to.value='' ; 
}

function searchRecord(pagename,form1){
	changeRecordList(1,pagename,form1);
	return false;
}
function deleteRecord(surl){
	ajxUrl= surl +"&ajx=1" ;
    $("#record_info").html('<div style="padding-top:30px">Loading...</div>');	
	makePOSTRequest(ajxUrl, '');
	//$('#record_info').load(ajxUrl);	
}
function addRecord(surl){
	
	ajxUrl= surl +"&ajx=1" ;	 
	//$("#record_info").html('<div style="padding-top:30px">Loading...</div>'); 
	makeGETRequest(ajxUrl,1);  //is_div_dialog=1
	//$('#record_info').load(ajxUrl);
} 
function editRecord(surl){
	ajxUrl= surl +"&ajx=1" ;	
/* 	$("#record_info").html('<div style="padding-top:30px">Loading...</div>'); */
	makeGETRequest(ajxUrl,1);	
	//$('#record_info').load(ajxUrl);
	//$("#record_info").html('<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>'); 
}

function listRecord(surl){
	ajxUrl= surl +"&ajx=1" ;
	$("#record_info").html('<div style="padding-top:30px">Loading...</div>'); 
	makeGETRequest(ajxUrl);
	//$('#record_info').load(ajxUrl);
}
function eTimer(url) {
	var t=setTimeout("window.location.href='"+url+"'",1000);
	$('#loading').fadeOut('slow');
}
	
//AJAX SUBMIT FORM EOF
 

/**
 * This function is used to select and relate the Executive to a 
 * Customer as the manager.
 *
 */
function addExecutive(executive_arr) {
    div_executive = document.getElementById("executive_info");

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', 'remove_'+ executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'team[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'team_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ executive_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeExecutive('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}


/**
 * This function is used to un-relate a Executive from a Customer.
 *
 */
function removeExecutive(id) {
    div_executive = document.getElementById("executive_info");

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}


function addPartyInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);
 

    executive_detail_div = document.createElement('div');
    executive_detail_div.setAttribute('class', 'row');
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'vendors_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'vendors_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" - "+executive_arr[5] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        		if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removePartyInfo('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  
    getBillAgainst(executive_arr[0]);
    return (true);
}

function removePartyInfo(id,divname) {
    div_executive = document.getElementById(divname);
    
    if ( div_executive.removeChild(document.getElementById(id)) ) {
       
        return (true);
    }
    else {
        return (false);
    }
}

