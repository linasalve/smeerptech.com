

/**
* To Load api depending on gateway in SMS purchase Module**/


//new added
var http_req_alerts;
var http_req_alerts0;
var http_req_alerts1;

function updateApi(gateway) {
	
    var cboModule = document.getElementById("api_id");
   
    cboModule.options.length = 0;
   
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    /*var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""
    */
    var uri = "get-api.php?gateway="+gateway;    
   
    http_req_alerts.onreadystatechange = loadApi;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

/**
* To Load api depending on gateway in SMS purchase Module
**/

function loadApi(){
    var cboModule = document.getElementById("api_id");
   
  
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
        
            if(allThoseTypes){			
                
                cboModule.options[0] = new Option("--SELECT--", "");
                
                cboModule.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");
            
              for (var i=0;i<typeSplit.length;i++){
            
                var tmpStr =typeSplit[i];
                if(tmpStr){
                    var tmpArray = new Array();
                    tmpArray = tmpStr.split("|");
                    var tmpName =tmpArray[0];
                    var tmpVal = tmpArray[1];
                   
                    if(tmpName!=''){
                        cboModule.options[i+1] = new Option(tmpName, tmpVal);   
                                             
                    }
                }
              }           
            }
        }
    }else{        
          cboModule.options[0] = new Option("Loading...", "");
          cboModule.style.background = "#c3c3c3";
  }
}


function dispalyDetails(api_id) {
	
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""
    
    var uri = "get-api-details.php?api_id="+api_id;    
   
    http_req_alerts.onreadystatechange = loadApiDetails;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}
function loadApiDetails(){
    var scrub = document.getElementById("scrub");
    var scrubname = document.getElementById("scrubname");
   
  
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
      
            if(allThoseTypes){			
                
               scrub.innerHTML= allThoseTypes;
               scrubname.value = allThoseTypes;
            }
        }
    }else{        
         scrub.innerHTML = '';
          
  }
}

function updateSender(api_id) {
	
    var cboModule = document.getElementById("sender_id");
   
    cboModule.options.length = 0;
   
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
   
    var uri = "get-sender.php?api_id="+api_id;    
   
    http_req_alerts.onreadystatechange = loadSender;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
}

/**
* To Load api depending on gateway in SMS purchase Module
**/

function loadSender(){
    var cboModule = document.getElementById("sender_id"); 
  
    if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseTypes = http_req_alerts.responseText;
        
            if(allThoseTypes){		 
                cboModule.options[0] = new Option("--SELECT--", ""); 
                cboModule.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseTypes.split(",");
            
              for (var i=0;i<typeSplit.length;i++){
            
                var tmpStr =typeSplit[i];
                if(tmpStr){
                    var tmpArray = new Array();
                    tmpArray = tmpStr.split("|");
                    var tmpName =tmpArray[0];
                    var tmpVal = tmpArray[1];
                   
                    if(tmpName!=''){
                        cboModule.options[i+1] = new Option(tmpName, tmpVal);   
                                             
                    }
                }
              }           
            }
        }
    }else{        
          cboModule.options[0] = new Option("Loading...", "");
          cboModule.style.background = "#c3c3c3";
  }
}




function addClientInfo(client_arr) {
    div_client = document.getElementById("client_info");

    client_detail_div = document.createElement('div');
    client_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'client_id');
        client_id_input.setAttribute('value', client_arr[0]);

        client_det_input = document.createElement('input');
        client_det_input.setAttribute('type', 'text');
        client_det_input.setAttribute('name', 'client_details');
        client_det_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +") ("+ client_arr[4] +")");
        client_det_input.setAttribute('size', '60');
        //client_det_input.setAttribute('readonly', 'readonly');
        client_det_input.setAttribute('class', 'inputbox_c');

        client_detail_div.appendChild(client_id_input);
        client_detail_div.appendChild(client_det_input);

    div_client.innerHTML = '<span class="txthelp">The Client for whom the Order is created.</span>';
    div_client.appendChild(client_detail_div);
    return (true);
}
