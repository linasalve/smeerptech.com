//POST DATA USING AJAX BOF
function getPageContent(obj,id) { 
	
	var type_method = document.getElementsByName('type'+id);
	var ischecked_type = false;
	var type_val='';
	for ( var i = 0; i < type_method.length; i++) {
		if(type_method[i].checked) {
			ischecked_type= true;
			type_val  = type_method[i].value;
		}
	}
      var poststr = "email=" + encodeURI( document.getElementById("email"+id).value )+"&id="+id+"&type="+type_val ;
	
	 makePOSTRequest('repository-sendmail.php', poststr);
}

var http_request = false;
   function makePOSTRequest(url, parameters) {
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
         if (http_request.overrideMimeType) {
         	// set type accordingly to anticipated content type
            //http_request.overrideMimeType('text/xml');
            http_request.overrideMimeType('text/html');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }
      
      http_request.onreadystatechange = alertContents;
      http_request.open('POST', url, true);
      http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      http_request.setRequestHeader("Content-length", parameters.length);
      http_request.setRequestHeader("Connection", "close");
      http_request.send(parameters);
   }

   function alertContents() {
   
      if (http_request.readyState == 4) {
         if (http_request.status == 200) {            
            allData = http_request.responseText;
			if(allData){            
            
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg =dataSplit[0];
                var dataId =dataSplit[1];					
                document.getElementById("message").innerHTML= dataMsg; 	
				rowid = 'row'+dataId ;				
				if(dataId!=0){
					document.getElementById(rowid).setAttribute("class","highlight");
				    document.getElementById(rowid).setAttribute("className", "highlight");
					
				}                
            }			
			
            //document.getElementById('myspan').innerHTML = result;            
         } else {
            alert('There was a problem with the request.');
         }
      }
   }
   
   


//POST DATA USING AJAX EOF
 
 
 
 
function showRepository(rps_id,file) {

   if (rps_id != '') {      
        url = "./repository.php?perform=download_file"
        url = url + "&rps_id="+ rps_id+'&file='+ file;        
        openSmallWindow(url,750,600);
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
	 
	
}


//Function to update the Banner status using AJAX BOF
var http_req_alerts_del;
function deleteRecord( str,id){
	if ( !str ) {
		str = "Do you really want to DELETE?";
	}
	if ( confirm(str) ) {
		http_req_alerts_del = false;	
		document.getElementById("repository_id").value = id;
		
		
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_req_alerts_del = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_del = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_del = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_del) {
			return false;
		} 
	   
		var uri = "repository-delete.php?rps_id="+ id;    

		http_req_alerts_del.onreadystatechange = actionDelete;
		http_req_alerts_del.open('GET', uri, true);
		http_req_alerts_del.send(null);
	
	}
	return false;

}

function actionDelete(){
    id = document.getElementById("repository_id").value ;
	rowid = 'row'+id ;
	if(http_req_alerts_del.readyState==4){
   
        if (http_req_alerts_del.status == 200){
            var allData = http_req_alerts_del.responseText;           
			
            if(allData){            
            
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg =dataSplit[0];
                var dataStatus =dataSplit[1];					
                document.getElementById("message").innerHTML= dataMsg; 			
				if(dataStatus==1){
					document.getElementById(rowid).setAttribute("class","hidden");
				    document.getElementById(rowid).setAttribute("className", "hidden");
					
				}
                
            }
        }
    }else{
		document.getElementById("message").innerHTML= ""; 
    }

}
//Function to update the Banner status using AJAX BOF