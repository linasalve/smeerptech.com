/**
 * Variable to store XMLHTTP object.
 */
var objXmlHttp 	= null;
var lastCall 	= new Date();

/**
 * Function to check whether the XMLHTTP object is created or not.
 * 
 * @return	boolean	true if object is created.
 * 					false if object is not created.
 */
function isCreated() {
	
	if ( !objXmlHttp ) {
		if ( (objXmlHttp == false) || (objXmlHttp == null)) {
			return false;
		}
	}
	return true;
}

/**
 * Function to create the XMLHTTP object to handle the AJAX functionality.
 *
 * @return 	XMLHTTP 	object on success.
 * 			boolean 	false on failure.
 */
function createObject() { 

	if ( isCreated() ) {
		objXmlHttp = null;
	}

	if ( !isCreated() ) {
		try {
			// For Opera 8.0+, Mozilla, Firefox, Safari
			objXmlHttp = new XMLHttpRequest();
		}
		catch(e) {
			// For Internet Explorers
			try {
				// Internet Explorer 5.5+
				objXmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try {
					// Internet Explorer upto 5.5
					objXmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch(e) {
					// property is not supported.
					objXmlHttp = false;
				}
			}
		}
	}
}

/**
 * Function to call the server using the GET method.
 *
 * @param	string	url	to which the call will be made.
 * @param	string	name of the function that will handle the response from the server.
 *
 * @return	boolean	true if the call is successful to the server.
 					false if the call is un-successful to the server.
 */
function requestGET(url, handler) {
	
	createObject();
	if ( !isCreated() ) {
		return false;
	}
	
	// Hack to make a new call everytime.
	if ( url.indexOf("?") >= 0 ) {
		url += '&rnd='+ Math.random();
	}
	else {
		url += '?&rnd='+ Math.random();
	}

	objXmlHttp.onreadystatechange = handler; 
	objXmlHttp.open( "GET", url, true );
	objXmlHttp.send( null );
	return true;
}

/**
 * Function to call the server using the POST method.
 * It is to be noted that AJAX cannot handle file uploads
 * by iteself, to upload a file to the server a server 
 * script has to be used.
 *
 * @param	string	url	to which the call will be made.
 * @param	string	name of the function that will handle the response from the server.
 *
 * @return	boolean	true if the call is successful to the server.
 					false if the call is un-successful to the server.
 */
function requestPOST(url, parameters, handler) {
	createObject();
	if ( !isCreated() ) {
		return false;
	}

	// Hack to make a new call everytime.
	if ( url.indexOf("?") >= 0 ) {
		url += '&rnd='+ Math.random();
	}
	else {
		url += '?&rnd='+ Math.random();
	}

	objXmlHttp.onreadystatechange = handler; 
	objXmlHttp.open( "POST", url, true );
	objXmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	objXmlHttp.setRequestHeader("Content-length", parameters.length);
	objXmlHttp.setRequestHeader("Connection", "close");
	objXmlHttp.send(parameters);
	return true;
}

function registerScript(html) {
	var head = document.getElementsByTagName('head').item(0);
	var old  = document.getElementById('lastLoadedScript');
	if (old) head.removeChild(old);

	// Extract the script 
	var reg = new RegExp("<script.*?>(.+?)</script>");
	var m = reg.exec(html);
	if (m) {
		script = document.createElement('script');
		script.text = m[1];
		script.type = 'text/javascript';
		script.defer = true;
		script.id = 'lastLoadedScript';
		void(head.appendChild(script));
	}
}

function getLoadingHTML() {
	var html = 	'<table align="center" cellpadding="5" cellspacing="5" border="0">'
				+'<tr>'
				+'	<td align="center" valign="middle">'
				+'		<table align="center" cellpadding="0" cellspacing="5" class="loading">'
				+'		<tr>'
				+'			<td valign="middle">'
				+'				<img src="./../media/images/nc/loading_circle.gif" alt="Loading" border="0"/>'
				+'			</td>'
				+'			<td class="text_style">'
				+'				Please Wait<br/>'
				+'				Processing...'
				+'			</td>'
				+'		</tr>'
				+'		</table>'
				+'	</td>'
				+'</tr>'
				+'</table>'
				+'<br/>';

return html;

}