
function Marquee(ticker_id, text_id) {
    this.m_left     = 1000;
    this.m_width    = 0;
    this.m_timer    = '';
    this.m_object   = document.getElementById(ticker_id);
    this.m_text     = document.getElementById(text_id);
}

Marquee.prototype.start = function()  {
    this.run();
}

Marquee.prototype.stop = function() {
    clearTimeout(this.m_timer);
}

Marquee.prototype.run = function() {
    this.m_timer = setTimeout("marquee.run()", 20);
    this.m_left--;
    this.m_object.style.left = this.m_left;
    if (this.m_left <= (-1 * this.m_object.clientWidth)) {
        this.m_left = 1000;
    }

    this.m_width++;
    this.m_object.style.width = this.m_width;
    if ( this.m_width >= (800+this.m_text.innerHTML.length) ) {
        this.m_width = 0;
    }
}
