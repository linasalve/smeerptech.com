//Function to update the Order Followup status using AJAX BOF
var http_req_alerts_banner;
function updateInvFlwStatus( id,status){
	http_req_alerts_banner = false;	
	document.getElementById("orderid").value = id;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    }
    var uri = "get-leads-orders-status.php?id="+ id+"&status="+status; 
    http_req_alerts_banner.onreadystatechange = loadInvFlwStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);
}

function loadInvFlwStatus(){
 
     id = document.getElementById("orderid").value ;
	 statusid = 'status'+id ;
	 
	if(http_req_alerts_banner.readyState==4){
   
        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
            
			 
            if(allData){            
           
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataHighlight = dataSplit[2];	
 				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
				
				highlightid = 'row'+dataHighlight ;
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
        
        
    }

}
//Function to update the Order Followup status using AJAX BOF

/**
 * This function is used to add new elements
 * to the existing HTML elements.
 * This particular function adds a DIV tag to the parent 
 * element identified by 'element'.
 *
 * @param   HTML Element    this can be reference to any HTML element eg. DIV, TABLE, TD, P.
 * 
 *
 */
function addParticularField(element) {

    // Create the main container Div
    // <div class="row">
    div_main = document.createElement('div');
    div_main.setAttribute('class', 'row');
    
        // <div class="coloumn vb">
        div_col1 = document.createElement('div');
        div_col1.setAttribute('class', 'coloumn vb');
            // Create the TEXTAREA field.
            text_parti = document.createElement('textarea');
            text_parti.setAttribute('name', 'particulars[]');
            text_parti.setAttribute('class', 'inputbox');
            text_parti.setAttribute('cols', '50');
            text_parti.setAttribute('rows', '1');
            text_parti.setAttribute('style', 'height:17px;');
            text_parti.setAttribute('onkeypress', 'javascript: return increaseHeight(this, event);');
        div_col1.appendChild(text_parti);
        
        // <div class="coloumn vb">
        div_col2 = document.createElement('div');
        div_col2.setAttribute('class', 'coloumn pl5 vb');
            // Create the Amount field.
            // <input type="text" name="p_amount[]" value="" class="inputbox" size="10"/>
            input_amount = document.createElement('input');
            input_amount.setAttribute('type', 'text');
            input_amount.setAttribute('name', 'p_amount[]');
            input_amount.setAttribute('value', '0');
            input_amount.setAttribute('onblur', 'javascript: return updateTotal();');
            input_amount.setAttribute('onkeypress', 'javascript:return hwndAddParticularField(this, event);');
            input_amount.setAttribute('class', 'inputbox');
            input_amount.setAttribute('size', '7');
            
            input_pid = document.createElement('input');
            input_pid.setAttribute('type', 'hidden');
            input_pid.setAttribute('name', 'p_id[]');
            input_pid.setAttribute('value', '');
        div_col2.appendChild(input_pid);
        div_col2.appendChild(input_amount);
    
    div_main.appendChild(div_col1);
    div_main.appendChild(div_col2);
    
    element.appendChild(div_main);
    return (true);
}

function hwndAddParticularField(element, e) {
    var e = e? e : window.event;
    if(!e) 
    return;
    
    var key = 0;
    if (e.keycode) {
        key = e.keycode;
    } // for moz/fb, if keycode==0 use 'which'
    else if (typeof(e.which)!= 'undefined') { 
        key = e.which; 
    } 
    
    if ( key==13 ) {
        addParticularField(document.getElementById('particular_details'));
        return (false);
    }
    return (true);
}


function increaseHeight(element, e) {
    var e = e? e : window.event;
    if(!e) 
    return;
    
    var key = 0;
    if (e.keycode) {
        key = e.keycode;
    } // for moz/fb, if keycode==0 use 'which'
    else if (typeof(e.which)!= 'undefined') { 
        key = e.which; 
    } 
    
    if ( key==13 ) {
        height = element.style.height;
        height = parseInt(height.substr(0, (height.length-2)));
        element.style.height = (height + 20) + 'px';
    }
    return (true);
}


function updateTotal() {
    /*
    p_amount  = document.getElementsByName('p_amount[]');
    dst_amount  = document.getElementsByName('d_amount[]');
    stot_amount  = document.getElementsByName('stot_amount[]');
    tax1_value  = document.getElementsByName('tax1_value[]');
	tax1_pvalue  = document.getElementsByName('tax1_pvalue[]');
    tot_amount  = document.getElementsByName('tot_amount[]');
    
    var total   = 0.0;

    for ( i=0; i< p_amount.length; i++ ) {
    
        if ( parseFloat(p_amount[i].value) ) {
            //total = total + parseFloat(lst_amount[i].value) ;
        }
        if(dst_amount[i].value != 0){
            stot_amount[i].value =  parseFloat(p_amount[i].value) - parseFloat(dst_amount[i].value) ;
        }else{
            stot_amount[i].value =  parseFloat(p_amount[i].value) ;
        }
       
       
        //tot_amount[i].value = adjustAmount(parseFloat(stot_amount[i].value),tax1_value[i].value);
		tot_amount[i].value = addPercentAmount(parseFloat(stot_amount[i].value),tax1_pvalue[i].value);
        total = parseFloat(total) + parseFloat(tot_amount[i].value) ;

	}
   
    temp = document.frmInvoiceAdd.octroi.value;
    total = adjustAmount(total, temp) ;
 
    document.frmInvoiceAdd.amount.value = total;
    */
    
    total = document.frmInvoiceAdd.amount.value ;
    exchange_rate = document.frmInvoiceAdd.exchange_rate.value;
    
    document.frmInvoiceAdd.amount_inr.value = parseFloat(total) * parseFloat(exchange_rate);
	
}
//new added for discount
function subtractAmount(amount, adjustment) {
    index = adjustment.indexOf('%');
    if ( index > 0 ) {
        adjustment = parseFloat(adjustment.substr(0, index));
        adjustment = parseFloat(adjustment/100);
        adjustment = parseFloat(amount * adjustment);
        amount = parseFloat(amount) - parseFloat(adjustment);
    }
    else {
        if ( adjustment = parseFloat(adjustment) ) {
            amount = parseFloat(amount) - parseFloat(adjustment);
        }
    }
    //amount = parseFloat(parseInt((amount+0.005)*100.0)/100.0);
    amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
    return (amount);
}
function adjustAmount(amount, adjustment) {
    index = adjustment.indexOf('%');
    if ( index > 0 ) {
        adjustment = parseFloat(adjustment.substr(0, index));
        adjustment = parseFloat(adjustment/100);
       
        adjustment = parseFloat(amount * adjustment);
        //adjustment = parseFloat(amount) * parseFloat(adjustment);
        amount = parseFloat(amount) + parseFloat(adjustment);
    }
    else {
        if ( adjustment = parseFloat(adjustment) ) {
            amount = parseFloat(amount) + parseFloat(adjustment);
        }
    }
   // amount = parseFloat(parseInt((amount+0.005)*100.0)/100.0);
    amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
    
    return (amount);
}
//new function added 2009-04-02 in replace of adjustAmount
function addPercentAmount(amount, taxrate){
	amount = parseFloat(amount) ; 
	taxrate = parseFloat(taxrate) ; 
	adjustment = parseFloat(amount * taxrate);
    //adjustment = parseFloat(amount) * parseFloat(adjustment);
    amount = parseFloat(amount) + parseFloat(adjustment);
	amount = parseFloat(parseInt((amount+0.00)*100.0)/100.0);
    
    return (amount);
}

function showQuotation(inv_id, file_type) {

    if (file_type == 'htmlprint' || file_type == 'html' ||  file_type == 'pdf') {      
        url = "./leads-quotation.php?perform=view_file"
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        window.open(url,'Proposal','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}

 function showQuotationAfile(inv_id, file_type,filename){
 
    if (file_type == 'attach_file1' || file_type == 'attach_file2' ||  file_type == 'attach_file3' || file_type == 'attach_file4' ||  file_type == 'attach_file5') {      
        url = "./leads-quotation.php?perform=view_file"
        url = url + "&inv_id="+ inv_id;
        url = url + "&type="+ file_type;
        url = url + "&filename="+ filename;
        window.open(url,'Quotation','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
}
function showClientDetails(client_id) {
    window.open('./sale-lead.php?perform=view&user_id='+client_id,'clientDetails','left=20,top=20,width=600,height=500,toolbar=0,resizable=1,scrollbars=1');
    return (true);
}


