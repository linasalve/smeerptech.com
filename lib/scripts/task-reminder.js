function showTaskCommentFile(cid,file){
	if (cid!= '') {      
		url = "./task-reminder.php?perform=download_file";
        url = url + "&cid="+cid+'&file='+ file;   
        openSmallWindow(url,750,600);
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }

}

function showTaskFile(id,file) {

   if (id != '') {      
		url = "./task-reminder.php?perform=download_file";
        url = url + "&id="+id+'&file='+ file;   
       openSmallWindow(url,750,600);
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}
//Function to update the Banner status using AJAX BOF
var http_req_alerts_banner;
function updateStatus( id,status){
 
	http_req_alerts_banner = false;	
	document.getElementById("taskid").value = id;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    } 
   
    var uri = "get-task-reminder-status.php?id="+id+"&status="+status;    
	
    http_req_alerts_banner.onreadystatechange = loadStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);
 
 
}
function loadStatus(){
     id = document.getElementById("taskid").value ;
	 statusid = 'status'+id ;
	 
	if(http_req_alerts_banner.readyState==4){

        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
		 
            if(allData){            
            
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataDoc = dataSplit[2];               
                var dataEdit = dataSplit[3];
                var dataComment = dataSplit[4];
                var dataDelete = dataSplit[5];
				 var dataHighlight = dataSplit[6];
				highlightid = 'row'+dataHighlight ;
				docid = 'doc'+dataHighlight ;
				editid = 'edit'+dataHighlight ;
				commentid = 'comment'+dataHighlight ;
				deleteid = 'delete'+dataHighlight ;
				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
                document.getElementById(docid).innerHTML= dataDoc; 
                //document.getElementById(editid).innerHTML= dataEdit; 
                document.getElementById(commentid).innerHTML= dataComment; 
                document.getElementById(deleteid).innerHTML= dataDelete; 
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
        
        
    }

}
//Function to update the Banner status using AJAX BOF

function addExecutiveInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);

    executive_detail_div = document.createElement('div');
    executive_detail_div.setAttribute('class', 'row');
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'executive');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'executive_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        executive_det_input.setAttribute('class', 'inputbox_c');
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeExecutiveInfo('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  

    return (true);
}
function removeExecutiveInfo(id,divname) {
    div_executive = document.getElementById(divname);

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}
/**
 * This function is used to select and relate the Leads
 * task reminder.
 *
 */
function addExecutive(executive_arr) {
    
    div_executive = document.getElementById("executive_info");

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', 'remove_'+ executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'allotted_to[]');
        exec_id_input.setAttribute('value', executive_arr[0]);
        
        exec_email_input = document.createElement('input');
        exec_email_input.setAttribute('type', 'hidden');
        exec_email_input.setAttribute('name', 'exec_email[]');
        exec_email_input.setAttribute('value', executive_arr[4]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'team_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ executive_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeExecutive('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_email_input);
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}


/**
 * This function is used to select and relate the Clients 
 * task reminder.
 *
 */
function addClientInfo(client_arr) {
    
    div_client = document.getElementById("client_info");

        client_div = document.createElement('div');
        client_div.setAttribute('class', 'row');
        client_div.setAttribute('id', 'remove_'+ client_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'allotted_to_client[]');
        client_id_input.setAttribute('value', client_arr[0]);
        
        client_email_input = document.createElement('input');
        client_email_input.setAttribute('type', 'hidden');
        client_email_input.setAttribute('name', 'client_email[]');
        client_email_input.setAttribute('value', client_arr[4]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ client_arr[1] +")&nbsp;"+ client_arr[2] +"&nbsp;"+ client_arr[3] +"&nbsp;"+ client_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'client_details[]');
        //exec_client_input.setAttribute('value', "("+ client_arr[1] +") "+ client_arr[2] +" "+ client_arr[3] +" "+ client_arr[4]);
        exec_client_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ client_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ client_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ client_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeClient('remove_'+ client_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        client_div.appendChild(client_id_input);
        client_div.appendChild(client_email_input);
        //client_div.appendChild(exec_span);
        client_div.appendChild(exec_client_input);
        client_div.appendChild(exec_remove_a);

    div_client.appendChild(client_div);
    
    return (true);
}


/**
 * This function is used to select and relate the Leads
 * task reminder.
 *
 */
function addLeadInfo(lead_arr) {
    
    div_lead = document.getElementById("lead_info");

        lead_div = document.createElement('div');
        lead_div.setAttribute('class', 'row');
        lead_div.setAttribute('id', 'remove_'+ lead_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        lead_id_input = document.createElement('input');
        lead_id_input.setAttribute('type', 'hidden');
        lead_id_input.setAttribute('name', 'allotted_to_lead[]');
        lead_id_input.setAttribute('value', lead_arr[0]);

        lead_email_input = document.createElement('input');
        lead_email_input.setAttribute('type', 'hidden');
        lead_email_input.setAttribute('name', 'lead_email[]');
        lead_email_input.setAttribute('value', lead_arr[3]);
        
        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ lead_arr[1] +")&nbsp;"+ lead_arr[2] +"&nbsp;"+ lead_arr[3] +"&nbsp;"+ lead_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'lead_details[]');
        //exec_client_input.setAttribute('value', "("+ lead_arr[1] +") "+ lead_arr[2] +" "+ lead_arr[3] +" "+ lead_arr[4]);
        exec_client_input.setAttribute('value', lead_arr[1] +" "+ lead_arr[2] +" ("+ lead_arr[3] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Lead.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ lead_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeLead('"+ lead_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeLead('"+ lead_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeLead('remove_'+ lead_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        lead_div.appendChild(lead_email_input);
        lead_div.appendChild(lead_id_input);
        //lead_div.appendChild(exec_span);
        lead_div.appendChild(exec_client_input);
        lead_div.appendChild(exec_remove_a);

    div_lead.appendChild(lead_div);
    
    return (true);
}


/**
 * This function is used to select and relate the external user
 * task reminder.
 *
 */
function addExternalUserInfo(external_user_arr) {
    
    div_external_user = document.getElementById("external_user_info");

        external_user_div = document.createElement('div');
        external_user_div.setAttribute('class', 'row');
        external_user_div.setAttribute('id', 'remove_'+ external_user_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        external_user_id_input = document.createElement('input');
        external_user_id_input.setAttribute('type', 'hidden');
        external_user_id_input.setAttribute('name', 'allotted_to_addr[]');
        external_user_id_input.setAttribute('value', external_user_arr[0]);
        
        external_user_email_input = document.createElement('input');
        external_user_email_input.setAttribute('type', 'hidden');
        external_user_email_input.setAttribute('name', 'addr_email[]');
        external_user_email_input.setAttribute('value', external_user_arr[3]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ external_user_arr[1] +")&nbsp;"+ external_user_arr[2] +"&nbsp;"+ external_user_arr[3] +"&nbsp;"+ external_user_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'addr_details[]');
        //exec_client_input.setAttribute('value', "("+ external_user_arr[1] +") "+ external_user_arr[2] +" "+ external_user_arr[3] +" "+ external_user_arr[4]);
        exec_client_input.setAttribute('value', external_user_arr[1] +" ("+ external_user_arr[2] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Lead.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ external_user_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeLead('"+ external_user_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExternalUser('"+ external_user_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeExternalUser('remove_'+ external_user_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        external_user_div.appendChild(external_user_id_input);
        external_user_div.appendChild(external_user_email_input);
        //external_user_div.appendChild(exec_span);
        external_user_div.appendChild(exec_client_input);
        external_user_div.appendChild(exec_remove_a);

    div_external_user.appendChild(external_user_div);
    
    return (true);
}

function addExecutiveOnBehalf(executive_arr,divname) {
    
    div_executive = document.getElementById(divname);

    executive_detail_div = document.createElement('div');
    executive_detail_div.setAttribute('class', 'row');
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'on_behalf_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'on_behalf_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        executive_det_input.setAttribute('class', 'inputbox_c');
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeOnBehalfOf('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive on behalf of whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  

    return (true);
}

/**
 * This function is used to un-relate a Executive from a Task Reminder entry
 *
 */
function removeOnBehalfOf(id) {
    div_executive = document.getElementById("add_on_behalf_info");

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}


/**
 * This function is used to un-relate a Executive from a Task Reminder entry
 *
 */
function removeExecutive(id) {
    div_executive = document.getElementById("executive_info");

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}


/**
 * This function is used to un-relate a Client from a Task Reminder entry
 *
 */
function removeClient(id) {
    div_client = document.getElementById("client_info");

    if ( div_client.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}

/**
 * This function is used to un-relate a Lead from a Task Reminder entry
 *
 */
function removeLead(id) {
    div_lead= document.getElementById("lead_info");

    if ( div_lead.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}

/**
 * This function is used to un-relate a External Users from a Task Reminder entry
 *
 */
function removeExternalUser(id) {
    div_external_user = document.getElementById("external_user_info");

    if ( div_external_user.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}

/**
 * This function is used to un-relate a External Users from a Task Reminder entry
 *
 */
function addOrderInfo(order_arr) {
    
    div_order = document.getElementById("add_order_info");

    order_detail_div = document.createElement('div');
    order_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        order_id_input = document.createElement('input');
        order_id_input.setAttribute('type', 'hidden');
        order_id_input.setAttribute('name', 'order_id');
        order_id_input.setAttribute('value', order_arr[0]);

        order_det_input = document.createElement('input');
        order_det_input.setAttribute('type', 'text');
        order_det_input.setAttribute('name', 'order_details');
        order_det_input.setAttribute('value', order_arr[1]);
        order_det_input.setAttribute('size', '60');
        //order_det_input.setAttribute('readonly', 'readonly');
        order_det_input.setAttribute('class', 'inputbox_c');

        order_detail_div.appendChild(order_id_input);
        order_detail_div.appendChild(order_det_input);

    div_order.innerHTML = '<span class="txthelp">Select the Order.</span>';
    div_order.appendChild(order_detail_div);
    return (true);
}

/**
 * This function is used to un-relate a External Users from a Task Reminder entry
 *
 */

function getActivity(type) {
	
    /*var cboType = document.getElementById("activity_id");
    
    cboType.options.length = 0;
    if(!type){
        cboType.options[0] = new Option("", "");	
        return null;
    }*/
    
    document.getElementById("txtActivity").innerHTML="";
    
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    if (!http_req_alerts) {
        return false;
    } 
    var uri = "get-activity.php?type=" + type;    
    http_req_alerts.onreadystatechange = loadActivity;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
} 
function loadActivity(){
   
   if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            document.getElementById("txtActivity").innerHTML=http_req_alerts.responseText;
        }
    }else{ 
            document.getElementById("txtActivity").innerHTML="<img src='../media/images/nc/loading.gif' border=0/><br>Loading....Please Wait.";
  }
}

function validateTaskAdd(){
	//var e = document.getElementById('type');
	//var type1 =  e.options[e.selectedIndex].value;
	
	
	if(document.frmAdd.allotted_to.value==""){
		alert("Please select Associates");
		document.frmAdd.team_details.focus();
		return false;
	}
	  
	if(document.frmAdd.task.value==""){
		alert("Enter Title");
		document.frmAdd.task.focus();
		return false;
	}
	if(document.frmAdd.type.value==""){
		alert("Select Type");
		document.frmAdd.type.focus();
		return false;
	}
	if(document.frmAdd.comment.value==""){
		alert("Enter Details");
		document.frmAdd.comment.focus();
		return false;
	}
	if(document.frmAdd.do_r.value==""){
		alert("Select date of deadline");
		document.frmAdd.do_r.focus();
		return false;
	}
	return true;
}


var http_req_alerts_a;
function acknowledgeTask( id,task_cid){
  var del=reallyDel(' Are you sure you want to acknowledge this task? \n By acknowledging the task you are confirming that you had read the task/comment ');
	if(del==true){
		http_req_alerts_a = false;	
		document.getElementById("taskid").value = id;
		document.getElementById("task_cid").value = task_cid;
		
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_req_alerts_a = new XMLHttpRequest();
		}else if (window.ActiveXObject){ // IE
			try {
				http_req_alerts_a = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_a = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_a) {
			return false;
		} 
	   
		var uri = "get-task-reminder-status.php?id="+id+"&task_cid="+task_cid+"&action=acklge"; 
		
		http_req_alerts_a.onreadystatechange = loadTaskAcknowledge;
		http_req_alerts_a.open('GET', uri, true);
		http_req_alerts_a.send(null);
	} 
}

function loadTaskAcknowledge(){
     id = document.getElementById("taskid").value ; 
	 statusid = 'acknowledge'+id ; 
	if(http_req_alerts_a.readyState==4){

        if (http_req_alerts_a.status == 200){
            var allData = http_req_alerts_a.responseText;
			
            if(allData){    
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0]; 
				var dataHighlight = dataSplit[1]; 
				highlightid = 'row'+dataHighlight ; 
                document.getElementById("message").innerHTML = dataMsg; 
               // document.getElementById(statusid).innerHTML = dataStatus;  
				 
				var highlight = document.getElementById(highlightid);
				 highlight.setAttribute("class", "highlight");
				 highlight.setAttribute("className", "highlight");
            }
        }
    }else{  
		document.getElementById("message").innerHTML= "";  
    }

}

 

var http_req_alerts_a;
function acknowledgeTaskComment( id,task_cid){
  var del=reallyDel(' Are you sure you want to acknowledge this task? \n By acknowledging the task you are confirming that you had read the task/comment ');
	if(del==true){
		http_req_alerts_a = false;	
		document.getElementById("taskid").value = id;
		document.getElementById("task_cid").value = task_cid;
		
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_req_alerts_a = new XMLHttpRequest();
		}else if (window.ActiveXObject){ // IE
			try {
				http_req_alerts_a = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_a = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_a) {
			return false;
		} 
	   
		var uri = "get-task-reminder-status.php?id="+id+"&task_cid="+task_cid+"&action=acklge_c"; 
		
		http_req_alerts_a.onreadystatechange = loadTaskCommentAcknowledge;
		http_req_alerts_a.open('GET', uri, true);
		http_req_alerts_a.send(null);
	} 
}

function loadTaskCommentAcknowledge(){
    id = document.getElementById("task_cid").value ; 
	 
	if(http_req_alerts_a.readyState==4){

        if (http_req_alerts_a.status == 200){
            var allData = http_req_alerts_a.responseText;
			 
            if(allData){    
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0]; 
				var dataHighlight = dataSplit[1]; 
				highlightid = 'row'+dataHighlight ; 
                document.getElementById("message").innerHTML = dataMsg; 
                 
				var highlight = document.getElementById(highlightid);
				 highlight.setAttribute("class", "highlight");
				 highlight.setAttribute("className", "highlight");
            }
        }
    }else{  
		document.getElementById("message").innerHTML= "";  
    }

}