
function calculatePercent() {

  /*   promised_payment  = document.getElementById("promised_payment");  	
    per_amount   	 = document.getElementById("30_per_appr_pay");
	if(promised_payment.value==''){
		promised_payment.value=0;
	}
	per_amount.value = percentAmount(parseFloat(promised_payment.value),'0.3');
	
	earned_payment  = document.getElementById("earned_payment");  
    peroff_earned_pay   	 = document.getElementById("30_perof_earned_pay");    
	if(earned_payment.value==''){
		earned_payment.value=0;
	} 
    peroff_earned_pay_value = percentAmount(parseFloat(earned_payment.value),'0.3');
	peroff_earned_pay.value=peroff_earned_pay_value.toFixed(2)
	
	per_pay_amount  = document.getElementById("earned_30_per_appr_pay"); 
	appraisal_score =  document.getElementById("30_appraisal_score"); 
	if(appraisal_score.value==''){
		appraisal_score.value=0;
	}
	var appraisal_score_val = (appraisal_score.value/100)  ;
	
	per_pay_amount.value = percentAmount(parseFloat(peroff_earned_pay.value),appraisal_score_val);
	updateTotal(); */
	 
}

function percentAmount(amount, taxrate){
	amount = parseFloat(amount) ; 
	taxrate = parseFloat(taxrate) ; 	
    pamount = parseFloat(amount) * taxrate;
	pamount = parseFloat(pamount);
	  
    return (pamount);
}

function updateTotal(){
	promised_payment  = document.getElementById("promised_payment");  	
	earned_payment  = document.getElementById("earned_payment");  	
	working_days  = document.getElementById("working_days");  	
	present_days  = document.getElementById("present_days");  	
	worked_hrs  = document.getElementById("worked_hrs");  	
	incentive_amount  = document.getElementById("incentive_amount");  	
	earned_per_appr_pay = document.getElementById("earned_30_per_appr_pay");  	
	total_earned_payment  = document.getElementById("total_earned_payment");  	
	deduction_amount  = document.getElementById("deduction_amount");  	
	per_day  = document.getElementById("per_day");  
	per_hour  = document.getElementById("per_hour");  
	per_hour_type  = document.getElementById("per_hour_type");  
	per_day_type  = document.getElementById("per_day_type");  
	per_amount   	 = document.getElementById("30_per_appr_pay");
	if(promised_payment.value==''){
		promised_payment.value=0;
	}
 
	var per_day_amt = per_hour_amt = payable_salary_value = hr_amt = min_amt= 0 ;
	if(earned_payment.value==''){
		earned_payment.value=0;
	}
	
	if(incentive_amount.value==''){
		incentive_amount.value=0;
	} 
	if(earned_per_appr_pay.value==''){
		earned_per_appr_pay.value=0;
	}
	if(working_days.value==''){
		working_days.value=0;
	}
	if(present_days.value==''){
		present_days.value=0;
	}
	if(deduction_amount.value==''){
		deduction_amount.value=0;
	}
	if(per_day.value==''){
		per_day.value = 0;
	}
	if(per_hour.value==''){
		per_hour.value=0;
	}
	if(worked_hrs.value==''){
		worked_hrs.value=0.0;
	}
	
	per_day_amt = parseFloat(promised_payment.value)/parseFloat(working_days.value)  ;
	per_amount.value = percentAmount(parseFloat(promised_payment.value),'0.3');
	earned_payment  = document.getElementById("earned_payment");  
    perof_earned_pay   	 = document.getElementById("30_perof_earned_pay");    
	 
	var workedHrs = parseFloat(worked_hrs.value) ; 
	workedHrs = workedHrs.toFixed(2);
	worked_hrs.value =workedHrs ;
	var hrsArray = new Array();
    var hrs = min=0;
	if(workedHrs.indexOf(".")!='-1'){
		hrsArray = workedHrs.split(".");
		hrs = hrsArray[0];
		min = hrsArray[1];
	}else{
		hrs =workedHrs ;
	}
	 
	hr_amt = parseFloat(hrs * per_hour.value ) ;
	min_amt = ( parseFloat(per_hour.value)/60 ) * min   ; 
	
	if(document.getElementById("per_day_type").checked==true){
		per_day.value = per_day_amt.toFixed(2);
		earned_payment_value = parseFloat(per_day.value) * parseFloat(present_days.value) ; 
		per_hour.value = 0;
	}
	
	if(document.getElementById("per_hour_type").checked==true){
		per_hour_value = parseFloat(per_hour.value);
		per_hour.value = per_hour_value.toFixed(2);
		earned_payment_value = parseFloat(hr_amt) + parseFloat(min_amt) ; 
		earned_payment_value = parseFloat(earned_payment_value)
		per_day.value = 0;
	}
		
	earned_per_appr_pay  = document.getElementById("earned_30_per_appr_pay"); 
	appraisal_score =  document.getElementById("30_appraisal_score"); 
	if(appraisal_score.value==''){
		appraisal_score.value=0;
	}
	var appraisal_score_val = (appraisal_score.value/100)  ;
	earned_per_appr_pay.value = percentAmount(parseFloat(perof_earned_pay.value),appraisal_score_val);
	
	earned_payment.value = earned_payment_value.toFixed(2);
	perof_earned_pay_value = percentAmount(parseFloat(earned_payment.value),'0.3');
	perof_earned_pay.value=perof_earned_pay_value.toFixed(2)
	
	actual_payable_salary_tot = parseFloat(earned_payment.value)  + parseFloat(incentive_amount.value) + 		parseFloat(earned_per_appr_pay.value) - parseFloat(deduction_amount.value) ;
 
	total_earned_payment.value = parseFloat(actual_payable_salary_tot);
	
}

function toggleCT(obj) {
   if(obj.value == '1'){
        document.getElementById('per_hour_div').className="show";
    }else{
        document.getElementById('per_hour_div').className="hide";
    }
	updateTotal()	;
}