  /*
 * Facebook Style Add Friends Script
 *  http://www.amitpatil.me/
 *
 * @version
 * 1.0 (May 20 2010)
 * 
 * @copyright
 * Copyright (C) 2010-2011 
 *
 * @Auther
 * Amit Patil (www.amitpatil.me)
 * Maharashtra (India) 
 *
 * @license
 * This file is part of Facebook Style Add Friends.
 * 
 * Facebook Style Add Friends Script is freeware script. you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Facebook Style Add Friends Script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this script.  If not, see <http://www.gnu.org/copyleft/lesser.html>.
 */
$().ready(function(){
	var ajaxResponse = "#ajax_response";
	var ids = new Array();
	
if (typeof inputBox != "undefined") {
	// initialization's 
	$("<div id='ajax_response'></div>").insertAfter(inputBox);
	$(mainHolder).addClass("fb_holder");
	$("#selected_ids").val("");
	$(mainHolder).val("").focus();
	
	if(ajaxFilePath == "")
		alert("Please set value for 'ajaxFilePath'");
	// on focus of textbox show friends list
	$(inputBox).keyup(function(event){
		var p = $(mainHolder);
		var offset = p.offset();

		// create ajax request and get all the friend names starting with name XXX
		var keyword = $(inputBox).val();
		
		// remove select-friend class
		$(mainHolder).find(".selected-friend").removeClass("selected-friend");
		$(mainHolder).find(".selected-friend").find("#close").css("color","#8F8F8F");

		if(keyword.length>2)
		 {
			 if(event.keyCode != 40 && event.keyCode != 38 && event.keyCode != 13)
			 {
				 $(ajaxResponse).css("left",parseInt(offset.left));
				 $(ajaxResponse).css("top",parseInt(offset.top + $(mainHolder).height()));
				 $(ajaxResponse).css("opacity","0.9");
				 $(ajaxResponse).css("width",parseInt($(mainHolder).width()));
				 
				 if(ajaxFilePath != "")
				 {
					 $.ajax({
					   type: "POST",
					   url: ajaxFilePath,
					   data: ajaxDataVar+"="+keyword,
					   success: function(rep){	 
						if(rep != 0)
						  $(ajaxResponse).html(rep).css("display","block");
						else
						  $(".list").css("display","none");
					   },
					   error: function (xhr, ajaxOptions, thrownError) {
							alert(xhr.status);
							alert(thrownError);
					   }					   
					 });
				 }
			 }
			 else
			 {
				$("li .selected").removeClass("selected");
				switch (event.keyCode)
				{
				 case 40:
				 {
					  found = 0;
					  $(".list li").each(function(){
						 if($(this).attr("class") == "selected")
							found = 1;
					  });
					  if(found == 1)
					  {
						var sel = $("li[class='selected']");
						// check if his is a last element in the list
						// if so then add selected class to the first element in the list
						if(sel.next().text() == "")					
							$(".list li:first").addClass("selected");
						else
							sel.next().addClass("selected");
						// remove class selected from previous item
						sel.removeClass("selected");
					  }
					  else
						$(".list li:first").addClass("selected");
					 }
				 break;
				 case 38:
				 {
					  found = 0;
					  $(".list li").each(function(){
						 if($(this).attr("class") == "selected")
							found = 1;
					  });
					  if(found == 1)
					  {
						var sel = $("li[class='selected']");
						// check if his is a last element in the list
						// if so then add selected class to the first element in the list
						if(sel.prev().text() == "")					
							$(".list li:last").addClass("selected");
						else
							sel.prev().addClass("selected");
						// remove class selected from previous item
						sel.removeClass("selected");
					  }
					  else
						$(".list li:last").addClass("selected");
				 }
				 break;
				 case 13:
					$(ajaxResponse).css("display","none");
				 	var id = $("li[class='selected']").find("a").attr("id");
					$().addFriend($("li[class='selected']").text(),id);
				 break;
				}
			 }
		 }
		else
			$(ajaxResponse).fadeOut("slow");
	});
	// on click of list item mark that friend as selected

	$("#close").live("click",function(){
		var found = "";
		// remove selected friend
		$(this).parent().css("display","none");
		// get id of selected item
		var index = $(this).parent(".added").attr("id");
		// find items index in ids array
		for(i=0;i<ids.length;i++){
			if(ids[i] == index){
				found = i;
				continue;
			}
		}
		// remove selected index
		if(index != " " || index != "undefined")
			ids.splice(parseInt(found),1);
		// print updated ids
		$("#selected_ids").val(ids);
	});
	$(inputBox).focus(function(){
		// remove class
		$(mainHolder).find(".selected-friend").removeClass("selected-friend");
		$(mainHolder).find("#close").css("color","#8F8F8F");
	});
	$(".list li").live("mouseover",function () {
		  $("li[class='selected']").removeClass("selected");
		  $(this).addClass("selected");
	});
	$(".list li").live("mouseout",function () {
		  $("li .selected").removeClass("selected");
		  $(this).removeClass("selected");
	});
	$(".list li").live("click",function () {
		var text = $(this).text();
		var id = $(this).find("a").attr("id");
		// mark friend as selected and add to selected ist
		$(this).addFriend(text,id);
	});
	$(mainHolder).click(function(){
		$(inputBox).focus();
	});

	$(".added").live("mouseover",function(){
		$(this).addClass("added-hover");
	});
	$(".added").live("mouseout",function(){
		$(this).removeClass("added-hover");
		$(this).addClass("added");
	});
	$(".added").live("click",function(){
		$(mainHolder).find(".selected-friend").removeClass("selected-friend");
		$(this).addClass("selected-friend");
		$(this).find("#close").css("color","white");
	});
	jQuery.fn.addFriend = function(text,id) {
		if(text)
		{
			if($(mainHolder).find("div").attr("class") != "added")
				$("<div class='added' id='"+id+"'>"+text+"<span id='close'>x</span></div>").prependTo($(mainHolder));
			else
				$("<div class='added' id='"+id+"'>"+text+"<span id='close'>x</span></div>").insertAfter($(inputBox).prev());
			// hide list
			$(".list").css("display","none");
			// clear textbox 
			$(inputBox).val("").focus();
		}
		// insert selected id to array
		ids.push(id);
		// print selected ids in textbox
		$("#selected_ids").val(ids);
	}
}
});