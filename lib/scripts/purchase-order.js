 

/*AJAX NAVIGATION BOF*/

  
//AJAX PAGINATION FUNCTION BOF
function changeRecordList(page,pagename,form1){ 
	http_req_alerts = false;			
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_req_alerts = new XMLHttpRequest();
	}else if (window.ActiveXObject) { // IE
		try {
			http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e) {
			try {
				http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}    
	if (!http_req_alerts) {
		return false;
	}

	//var poststr = createQueryString(form1);	
	//var poststr =Url.decode($(form1).serialize()) ;
	
	var slist = document.getElementById('sStatus'); 
	st_options = "";
	for(var i = 0; i < slist.options.length; ++i){
		comma = ",";
		if(slist.options[i].selected){
			st_options = st_options + slist[i].value + comma;
		}
	}
	st_options = trim(st_options,",");
	 
	 
	
	poststr = "sString="+document.getElementById("sString").value+"&sType="+document.getElementById("sType").value+
	"&sOrderBy="+document.getElementById("sOrderBy").value+"&sOrder="+document.getElementById("sOrder").value+
	"&chk_status="+document.getElementById("chk_status").value+"&sStatus="+st_options+"&date_from="+document.getElementById("date_from").value+"&date_to="+document.getElementById("date_to").value+
	"&perform="+document.getElementById("perform").value+"&rpp="+document.getElementById("rpp").value ;
	
	var uri = pagename+"?ajx=1&x="+page+"&"+poststr ; 
		
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';	
	$('#inner_content').load(uri);
	
 
} 

function loadRecordList(){
	if(http_req_alerts.readyState==4){
        if (http_req_alerts.status == 200){
            var allData = http_req_alerts.responseText;
            if(allData){
			    document.getElementById("record_info").innerHTML = allData; 
            }
        }
    }else{
		document.getElementById("record_info").innerHTML= '<div style="padding-top:30px"><img src="'+ADMIN_PANEL_IMG+'/loading.gif" /></div>';         
    }
} 
//AJAX PAGINATION FUNCTION EOF
function clearSearch(){
	document.getElementById("sString").value='' ;
	document.getElementById("sType").value='' ;
	document.getElementById("sOrderBy").value='do_add' ;
	document.getElementById("sOrder").value='DESC' ;
	document.getElementById("date_from").value='' ;
	document.getElementById("date_to").value='' ;
	
	 
}

function searchRecord(pagename,form1){
	changeRecordList(1,pagename,form1);
	document.getElementById("pageno").value=1 ;	
	return false;
}
function deleteRecord(surl){
 
	var del=reallyDel('Are you sure you want to delete this Record?');

	if(del==true){
	
		
		$( "#dialog-form" ).dialog({
				autoOpen: false,
				height: 600,
				width: 900,
				resizable: true,
				closeText: 'close',
				title: 'SMEERP - NERVE CENTER',
				modal: true,
				close: function() {
					
					searchRecord('purchase-order.php',frmSearch);
					//changeRecordList(pageno,'prospects.php',frmSearch);
					//$( "#dialog-form" ).dialog( "destroy" );
				}
		});
		ajxUrl= surl +"&ajx=1" ;
		$("#dialog-form").dialog("open");	
		var diFrame =  document.getElementById('dialogIframeId');
		if (navigator.appName.indexOf("Microsoft")!=-1) {
			diFrameBody = document.frames['dialogIframeId'].document;

		}else{
		  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
		}
		diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
		
		$("#dialogIframeId").attr("src",ajxUrl);
		
		
		
	}
	return false;
} 
function editStatus(surl){
 
	var del=reallyDel('Are you sure you want to change the Status?');

	if(del==true){
	
		
		$( "#dialog-form" ).dialog({
				autoOpen: false,
				height: 600,
				width: 900,
				resizable: true,
				closeText: 'close',
				title: 'SMEERP - NERVE CENTER',
				modal: true,
				close: function() {
					
					searchRecord('purchase-order.php',frmSearch);
					//changeRecordList(pageno,'prospects.php',frmSearch);
					//$( "#dialog-form" ).dialog( "destroy" );
				}
		});
		ajxUrl= surl +"&ajx=1" ;
		$("#dialog-form").dialog("open");
		var diFrame =  document.getElementById('dialogIframeId');
		if (navigator.appName.indexOf("Microsoft")!=-1) {
			diFrameBody = document.frames['dialogIframeId'].document;
		}else{
		  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
		}
		diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
		$("#dialogIframeId").attr("src",ajxUrl);
		
	}
	return false;
}

function createPoPdf(surl){

	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 600,
		width: 900,
		resizable: true,
		closeText: 'close',
		title: 'SMEERP - NERVE CENTER',
		modal: true,
		close: function() {
			
			var pageno = document.getElementById("pageno").value ;
			url = SITE_URL+"/"+ADMIN_PANEL+"/purchase-order.php?perform=list&pageno="+pageno ;
			changeRecordList(pageno,'purchase-order.php',frmSearch);
			//$( "#dialog-form" ).dialog("destroy");
		}
	});
   
   ajxUrl= surl +"&ajx=1" ;	 
   $("#dialog-form").dialog("open");
   var diFrame =  document.getElementById('dialogIframeId');
   if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;

   }else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
   }
   diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
   return false;
}
function addRecord(surl){

	
	$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 900,
			resizable: true,
			closeText: 'close',
			title: 'SMEERP - NERVE CENTER',
			modal: true,
			close: function() {
			
				var pageno = document.getElementById("pageno").value ;
				url = SITE_URL+"/"+ADMIN_PANEL+"/purchase-order.php?perform=list&pageno="+pageno ;
				//searchRecord('prospects.php',frmSearch);
				changeRecordList(pageno,'purchase-order.php',frmSearch);
				//$( "#dialog-form" ).dialog( "destroy" );
			}
	});
    ajxUrl= surl +"&ajx=1" ;
 
    $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;
	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
    $("#dialogIframeId").attr("src",ajxUrl);
    return false; 
}
 
function editRecord(surl){
   
   $( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 600,
		width: 900,
		resizable: true,
		closeText: 'close',
		title: 'SMEERP - NERVE CENTER',
		modal: true,
		close: function() {
			
			var pageno = document.getElementById("pageno").value ;
			url = SITE_URL+"/"+ADMIN_PANEL+"/purchase-order.php?perform=list&pageno="+pageno ;
			changeRecordList(pageno,'purchase-order.php',frmSearch);
			//$( "#dialog-form" ).dialog( "destroy" );
		}
	});
   
   ajxUrl= surl +"&ajx=1" ;	 
   $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;

	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
  return false; 
}

function viewRecord(surl){

	
	$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 600,
			width: 900,
			resizable: true,
			closeText: 'close',
			title: 'SMEERP - NERVE CENTER',
			modal: true,
			close: function(){
			
				/* var pageno = document.getElementById("pageno").value ;
				url = SITE_URL+"/"+ADMIN_PANEL+"/purchase-order.php?perform=list&pageno="+pageno ;
				changeRecordList(pageno,'purchase-order.php',frmSearch); */
				//$( "#dialog-form" ).dialog( "destroy" );
			}
	});
    ajxUrl= surl +"&ajx=1" ;
 
    $("#dialog-form").dialog("open");
    var diFrame =  document.getElementById('dialogIframeId');
	if (navigator.appName.indexOf("Microsoft")!=-1) {
		diFrameBody = document.frames['dialogIframeId'].document;

	}else{
	  diFrameBody = diFrame.contentDocument.getElementsByTagName('body')[0];
	}
	diFrameBody.innerHTML =  '<div style="padding-top:30px">Loading...</div>';
   $("#dialogIframeId").attr("src",ajxUrl);
  
   return false; 
	
} 
function listRecord(surl){
	
	ajxUrl= surl +"&ajx=1" ;
	$('#inner_content').innerHTML= '<div style="padding-top:30px">Loading...</div>';
	$('#inner_content').load(ajxUrl);
	   return false; 
}
/*AJAX NAVIGATION EOF*/

function addClientSuInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'clients_su[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

       
        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'clients_su_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
       // exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove'+ executive_arr[1]);
        exec_remove_a.setAttribute('onclick', "javascript:removeClientSu('"+ executive_arr[0] +"');");
            // Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}

function removeClientSu(id) {
 
    div_executive = document.getElementById("add_clients_su_info");
 
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
   
}

function addClientInfo(client_arr) {
    div_client = document.getElementById("client_info");

    client_detail_div = document.createElement('div');
    client_detail_div.setAttribute('class', 'row');

        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'client');
        client_id_input.setAttribute('value', client_arr[0]);

        client_det_input = document.createElement('input');
        client_det_input.setAttribute('type', 'text');
        client_det_input.setAttribute('name', 'client_details');
        //client_det_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +") ("+ client_arr[4] +")");
        client_det_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +")");
        client_det_input.setAttribute('size', '60');
        //client_det_input.setAttribute('readonly', 'readonly');
        client_det_input.setAttribute('class', 'inputbox_c');

        client_detail_div.appendChild(client_id_input);
        client_detail_div.appendChild(client_det_input);

    div_client.innerHTML = '<span class="txthelp">The Client for whom the Order is created.</span>';
    div_client.appendChild(client_detail_div);
    return (true);
}


function addExecutive(executive_arr) {
    div_executive = document.getElementById("executive_info");

        exec_div = document.createElement('div');
        exec_div.setAttribute('class', 'row');
        exec_div.setAttribute('id', executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'team[]');
        exec_id_input.setAttribute('value', executive_arr[0]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'team_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readonly', 'readonly');
        exec_client_input.setAttribute('class', 'inputbox_c');

        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove'+ executive_arr[1]);
        exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
            // Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}

function removeExecutive(id) {
 
    div_executive = document.getElementById("executive_info");
  
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
   
}

//33333
function addExecutiveInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);

    executive_detail_div = document.createElement('div');
 
	if (navigator.appName=="Netscape") {
        executive_detail_div.setAttribute('class', 'row');
    }
    if (navigator.appName.indexOf("Microsoft")!=-1) {
        executive_detail_div.setAttribute('className', 'row');
    }
    
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'executive_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'executive_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
		
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeExecutiveInfo('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  

    return (true);
}

function removeExecutiveInfo(id,divname) {
    div_executive = document.getElementById(divname);

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}


function addTeamInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);

    executive_detail_div = document.createElement('div');
 
	if (navigator.appName=="Netscape") {
        executive_detail_div.setAttribute('class', 'row');
    }
    if (navigator.appName.indexOf("Microsoft")!=-1) {
        executive_detail_div.setAttribute('className', 'row');
    }
    
    executive_detail_div.setAttribute('id', 'removet_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'team_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'team_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
		
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'removet_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeTeamInfo('removet_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  

    return (true);
}
function removeTeamInfo(id,divname) {
    div_executive = document.getElementById(divname);

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}
function addPartyInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);
 

    executive_detail_div = document.createElement('div');
    executive_detail_div.setAttribute('class', 'row');
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'vendor_bank_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'vendor_bank_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" - "+executive_arr[5] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        		if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removePartyInfo('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  
    getBillAgainstVendorAcc(executive_arr[0]);
    return (true);
}

function removePartyInfo(id,divname) {
    div_executive = document.getElementById(divname);
    var bill_against_id = document.getElementById("bill_against_id");
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        bill_against_id.options.length = 0;
        bill_against_id.options[0] = new Option("Select Bill Against", "");
        return (true);
    }
    else {
        return (false);
    }
}

function addClientInfo(executive_arr) {
    div_executive = document.getElementById('add_clients_info');
 

    executive_detail_div = document.createElement('div');
    executive_detail_div.setAttribute('class', 'row');
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'party_id');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'party_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" - "+executive_arr[5] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        		if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeClientInfo('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            exec_remove_img.setAttribute('class', 'pl2');
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  
    getBillAgainst(executive_arr[0]);
    return (true);
}

function removeClientInfo(id) {
    div_executive = document.getElementById('add_clients_info');
    var bill_against_id = document.getElementById("bill_against_id");
    if ( div_executive.removeChild(document.getElementById(id)) ) {
        bill_against_id.options.length = 0;
        bill_against_id.options[0] = new Option("Select Bill Against", "");
        return (true);
    }
    else {
        return (false);
    }
}

var http_req_alerts;

function getBillAgainst(vendor_id){
    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""

    var uri = "get-bills-against.php?vendor_id=" + vendor_id;    

    http_req_alerts.onreadystatechange = loadBillAgainst;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
    
}

function loadBillAgainst(){
    var bill_against_id = document.getElementById("bill_against_id");
   
    if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseBills = http_req_alerts.responseText;
          
            if(allThoseBills){
                
                bill_against_id.options[0] = new Option("Select Bill Against", " ");
                bill_against_id.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseBills.split(",");
            
                for (var i=0;i<typeSplit.length;i++){
                    var tmpStr =typeSplit[i];
                    if(tmpStr){
                        var tmpArray = new Array();
                        tmpArray = tmpStr.split("|");
                        var tmpName = tmpArray[0];
                        var tmpVal = tmpArray[1];
                       
                        if(tmpName){
                            bill_against_id.options[i+1] = new Option(tmpName, tmpVal);                           
                        }
                    }
                }           
            }
        }
    }else{     
       
        bill_against_id.options[0] = new Option("Loading...", "");
        bill_against_id.style.background = "#c3c3c3";
    }

}

function getBillAgainstVendorAcc(vendor_id){

    http_req_alerts = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }
    
    if (!http_req_alerts) {
        return false;
    } 
    //var uri = "{/literal}{$variables.nc}{literal}get-account-head.php?type=" + type;   
                    "<img"
                    +" src=\"{/literal}{$variables.images}{literal}/loading_circle.gif\""

    var uri = "get-bills-against.php?vendor_bank_id=" + vendor_id;    

    http_req_alerts.onreadystatechange = loadBillAgainstVendorAcc;
    http_req_alerts.open('GET', uri, true);
    http_req_alerts.send(null);
    
}

function loadBillAgainstVendorAcc(){
    var bill_against_id = document.getElementById("bill_against_id");
   
    if(http_req_alerts.readyState==4){
   
        if (http_req_alerts.status == 200){
            var allThoseBills = http_req_alerts.responseText;
          
            if(allThoseBills){
                
                bill_against_id.options[0] = new Option("Select Bill Against", " ");
                bill_against_id.style.background ="#ffffff";
                var typeSplit =new Array();
                typeSplit = allThoseBills.split(",");
            
                for (var i=0;i<typeSplit.length;i++){
                    var tmpStr =typeSplit[i];
                    if(tmpStr){
                        var tmpArray = new Array();
                        tmpArray = tmpStr.split("|");
                        var tmpName = tmpArray[0];
                        var tmpVal = tmpArray[1];
                       
                        if(tmpName){
                            bill_against_id.options[i+1] = new Option(tmpName, tmpVal);                           
                        }
                    }
                }           
            }
        }
    }else{     
       
        bill_against_id.options[0] = new Option("Loading...", "");
        bill_against_id.style.background = "#c3c3c3";
    }

}

//Function to update the Banner status using AJAX BOF
// added in  2010-10-oct-26 
var http_req_alerts_banner;
function updateSubTax( tax_str,idstr){

	var tax_id = id='';
	if(tax_str!=''){
	  var dataSplit =new Array();
	  dataSplit = tax_str.split("#");
	  tax_id =dataSplit[1];
	}
	if(idstr!=''){
	  var dataSplit1 = new Array();
	  dataSplit1 = idstr.split("_");
	  id = dataSplit1[1];
	}

	if(tax_id!=''){
		http_req_alerts_banner = false;		
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_req_alerts_banner = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_banner) {
			return false;
		} 
	   
		var uri = "get-bills-subtax-details.php?tax_id="+ tax_id+"&id="+id ;    

		http_req_alerts_banner.onreadystatechange = loadSubTax;
		http_req_alerts_banner.open('GET', uri, true);
		http_req_alerts_banner.send(null);
	}else{
		subtaxstr = "subtax_"+id+"" ;
		document.getElementById(subtaxstr).innerHTML='';
	}

}
function loadSubTax(){    
	var subtaxstr ;
	subtaxstr = "subtax_"+id+"" ;
 
	if(http_req_alerts_banner.readyState==4){   
        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
			
            if(allData){               
                document.getElementById(subtaxstr).innerHTML= allData; 
				updateTotal();
            }
        }
    }else{       
        document.getElementById(subtaxstr).innerHTML= ""; 
    }
}
//Function to update the Banner status using AJAX BOF
//AJX Function to load the sub services BOF
var http_req_alerts_subs;
function updateSubServices(service_str,idstr){
 	
	var service_id = id='';
	if(service_str!=''){
	  service_id =service_str;
	}
	if(idstr!=''){
	  var dataSplit1 = new Array();
	  dataSplit1 = idstr.split("_");
	  id = dataSplit1[1];
	}
	
	currency_id = document.getElementById('currency_id').value ;
	if(service_id!=''){
	
		http_req_alerts_subs = false;		
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_req_alerts_subs = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_subs = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_subs = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if(!http_req_alerts_subs){
			return false;
		} 
	   
		var uri = "get-subservices.php?perform=load-sub-service&service_id="+ service_id+"&id="+id+"&currency_id="+currency_id ;    

		http_req_alerts_subs.onreadystatechange = loadSubServices;
		http_req_alerts_subs.open('GET', uri, true);
		http_req_alerts_subs.send(null);
	} 
}

function loadSubServices(){    

	var sub_service_id ;
	sub_service_id = "subsid_"+id+"" ;
	punchlinestr_id = "punchlinestr_"+id+"" ;
	punchline_id = "punchline_"+id+"" ;
	isrenewable_id = "isrenewable_"+id+"" ;
	ssdiv_id = "ssdivid_"+id+"" ;
	sstitle_id = "sstitle_"+id+"" ;
	particulars_id = "particulars_"+id  ;
	pamount_id = "pamount_"+id  ;
	damount_id = "damount_"+id  ;
	
    var sub_s_id = document.getElementById(sub_service_id);
	
	if(http_req_alerts_subs.readyState==4){  

        if (http_req_alerts_subs.status == 200){
            var allData = http_req_alerts_subs.responseText;
			
			var dtSplit =new Array();
			dtSplit = allData.split("~");
			optionData = dtSplit[0];
			serviceData = dtSplit[1];
			
			serviceArr = serviceData.split("#");
			punchline = serviceArr[0];
			service_renewable = serviceArr[1];
			ss_div_id = serviceArr[2];
			ss_title = serviceArr[3];
			document.getElementById(punchline_id).value= punchline;			 
			service_renewable_str="   ; <b>Service</b>  Non-Renewable ";
			
			if(service_renewable==1){
				service_renewable_str=" ; <b> Service </b>  Renewable ";
			}
			
			if(punchline==''){
				punchline='No Punchline';
			}
			document.getElementById(punchlinestr_id).innerHTML="<b>Punchline:</b>"+punchline+service_renewable_str;
			document.getElementById(isrenewable_id).value=service_renewable;
			document.getElementById(ssdiv_id).value=ss_div_id;
			document.getElementById(sstitle_id).value=ss_title;
			document.getElementById(particulars_id).value='';
			document.getElementById(pamount_id).value=0;
			document.getElementById(damount_id).value=0;
			updateTotal();
			 
			if(optionData!=''){
				sub_s_id.options.length=0;
				sub_s_id.options[0] = new Option("Select SubService", " ");
				sub_s_id.style.background ="#ffffff";
				var optSplit =new Array();
				optSplit = optionData.split("^");
			
				for (var i=0;i<optSplit.length;i++){
					var tmpStr =optSplit[i];
					if(tmpStr){
						var tmpArray = new Array();
						tmpArray = tmpStr.split("|");
						var tmpName = tmpArray[0];
						var tmpVal = tmpArray[1];
						if(tmpName){
							sub_s_id.options[i+1] = new Option(tmpName, tmpVal);    
							sub_s_id.options[i+1].title= tmpName;
						}
					}
				}       
            }else{			
				sub_s_id.options[0] = new Option("Select SubService", " ");
				
			}
        }
    }else{     
       
        sub_s_id.options[0] = new Option("Loading...", "");
        sub_s_id.style.background = "#c3c3c3";
    }

}
//AJX Function to load the sub services EOF

//AJX Function to load the particulars BOF
var http_req_alerts_part;
function updateParticulars(idstr){
	var subsid = '';
	if(idstr!=''){
	  var dataSplit1 = new Array();
	  dataSplit1 = idstr.split("_");
	  id = dataSplit1[1];	  
	}
	currency_id = document.getElementById('currency_id').value ;
	subsid = "subsid_"+ id ;
	subsidstr = "subsidstr_"+ id ;
	var subservice_str ='';
	var subservicelist = document.getElementById(subsid);	
	var len =subservicelist.options.length;
	 
	for(var i = 0; i < subservicelist.options.length; ++i){		
		if(subservicelist.options[i].selected){			 
			subservice_str = subservice_str+","+subservicelist.options[i].value ; 
		}
	}
	 
	document.getElementById(subsidstr).value=subservice_str;
	if(subservice_str!=''){		
		http_req_alerts_part = false;		
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_req_alerts_part = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_part = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_part = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_part) {
			return false;
		} 	   
		var uri = "get-subservices.php?perform=load-particulars&subservice_str="+subservice_str+"&id="+id+"&currency_id="+currency_id ;     
	 
		http_req_alerts_part.onreadystatechange = loadParticulars;
		http_req_alerts_part.open('GET', uri, true);
		http_req_alerts_part.send(null);		
	}
}

function loadParticulars(){
	var particulars_id ;
	var pamount_id ;
	
	particulars_id = "particulars_"+id  ;
	pamount_id = "pamount_"+id  ;
	
    
	if(http_req_alerts_part.readyState==4){
        if (http_req_alerts_part.status == 200){
            var allData = http_req_alerts_part.responseText; 
			
			var infoSplit =new Array();
			infoSplit = allData.split("#");
			document.getElementById(particulars_id).value=infoSplit[0];
			document.getElementById(pamount_id).value=infoSplit[1];
			updateTotal();
		}
	}
	
}
//AJX Function to load the particulars EOF
function updateTotal() {
	if(document.getElementById('exchange_rate').value!='' && document.getElementById('exchange_rate').value<=0){
		document.getElementById('exchange_rate').value=1;
	}
	
	exchange_rate  = document.getElementById('exchange_rate').value ; 
	particulars  = document.getElementsByName("particulars[]");   
	s_quantity  = document.getElementsByName("s_quantity[]");   
	p_amount  = document.getElementsByName("p_amount[]");   
	s_amount  = document.getElementsByName("s_amount[]");   
	d_amount  = document.getElementsByName("d_amount[]");   
	stot_amount  = document.getElementsByName("stot_amount[]");   
	tax1_value  = document.getElementsByName("tax1_value[]");   
	tax1_amount  = document.getElementsByName("tax1_amount[]");
	tax1_sub1_value  = document.getElementsByName("tax1_sub1_value[]");   
	tax1_sub1_amount  = document.getElementsByName("tax1_sub1_amount[]"); 
	tax1_sub2_value  = document.getElementsByName("tax1_sub2_value[]");   
	tax1_sub2_amount  = document.getElementsByName("tax1_sub2_amount[]");	 
	tot_amount  = document.getElementsByName("tot_amount[]");   
	
	stotal_amount=bill_amount=0;
	for ( i=0; i< particulars.length; i++ ){
		
        taxamt=stot=0;		 
		if(p_amount[i].value>0){
			p_amount1 = p_amount[i].value= parseFloat(p_amount[i].value).toFixed(2);
		}else{
			p_amount1=0;
		}
		if(s_quantity[i].value>0){
			s_quantity1 =s_quantity[i].value;
		}else{
			s_quantity[i].value=s_quantity1=1;
		}
		 
		s_amount1 = s_amount[i].value = parseFloat(s_quantity1) * parseFloat(p_amount1) ;
		
		if(d_amount[i].value>0){
			d_amount1 =d_amount[i].value;
		}else{
			d_amount1=0;
		}
		stot_amount1= stot_amount[i].value  = parseFloat(s_amount1) - parseFloat(d_amount1) ;
		
		if(tax1_value[i].value!=''){
			tax1_value1 =tax1_value[i].value;
		}else{
			tax1_value1='0%';
		}
		tax1_amount1 = tax1_amount[i].value = calculatePercentAmount(stot_amount1,tax1_value1) ;

		if(tax1_sub1_value[i].value!=''){
			tax1_sub1_value1 =tax1_sub1_value[i].value;
		}else{
			tax1_sub1_value1='0%';
		}
		 
		tax1_sub1_amount[i].value = tax1_sub1_amount1  = calculatePercentAmount(tax1_amount1,tax1_sub1_value1) ;
		
		if(tax1_sub1_amount1 < 0 || tax1_sub1_amount1 ==''){
			tax1_sub1_amount1=0;
		}		
		if(tax1_sub2_value[i].value!=''){
			tax1_sub2_value1 =tax1_sub2_value[i].value;
		}else{
			tax1_sub2_value1='0%';
		}		 
		tax1_sub2_amount1 = tax1_sub2_amount[i].value = calculatePercentAmount(tax1_amount1,tax1_sub2_value1) ;
		
		if(tax1_sub2_amount1 < 0 || tax1_sub2_amount1 ==''){
			tax1_sub2_amount1=0;
		}
		
        tot_amount1=  parseFloat(stot_amount1) + parseFloat(tax1_amount1) + parseFloat(tax1_sub1_amount1) +  
		parseFloat(tax1_sub2_amount1);
		tot_amount[i].value  = tot_amount1.toFixed(2);
	    bill_amount = parseFloat(bill_amount) + parseFloat(tot_amount[i].value );
		stotal_amount = parseFloat(stotal_amount) + parseFloat(stot_amount1);
	}
	
	bill_amount =  bill_amount.toFixed(2);
	stotal_amount =  stotal_amount.toFixed(2);
	var round_off_op = document.getElementById("round_off_op").value ;
	var round_off = document.getElementById("round_off").value ;
	if(bill_amount!='' && round_off_op!='' && round_off>=0){
		if(round_off_op=='+'){
			bill_amount =  parseFloat(bill_amount)  + parseFloat(round_off) ;
		}
		if(round_off_op=='-'){
			bill_amount =  parseFloat(bill_amount)  - parseFloat(round_off) ;
		}
	}	
	document.getElementById("bill_amount").value= bill_amount;
	document.getElementById("stotal_amount").value= stotal_amount;
}
 

function calculatePercentAmount(amount, percent) {
    index = percent.indexOf('%');
    if ( index > 0 ) {
		percent = parseFloat(percent.substr(0, index));
		percent = parseFloat(percent/100);
        amount = parseFloat(amount * percent);
    } 
  
    amount = parseFloat(amount).toFixed(2);
    return (amount);
}

function showBill(bill_id,file){
    if (bill_id != '') {          
        url = "./purchase-order.php?perform=download_file"
        url = url + "&bill_id="+bill_id+'&file='+ file;        
        window.open(url,'Download Bill','left=20,top=20,width=750,height=600,toolbar=0,resizable=1,scrollbars=1')
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
	
}