http_req_alerts_d = false;
function updateDomain(client_id){ 
	var client = document.getElementById(client_id).value; 
    if(client!=''){  
		if (window.XMLHttpRequest){ // Mozilla, Safari,...
			http_req_alerts_d = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_d = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_d = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_d) {
			return false;
		} 
		var uri = "get-client-domains.php?client="+client ;    		
		http_req_alerts_d.onreadystatechange = loadDomainList;
		http_req_alerts_d.open('GET', uri, true);
		http_req_alerts_d.send(null);	
	}else{
		alert('Please Select Client');
		return false;
	}
  
}

function loadDomainList(){ 
	var domain = document.getElementById('domain_id');
	if(http_req_alerts_d.readyState==4){
		if (http_req_alerts_d.status == 200){
			var allThoseTypes = http_req_alerts_d.responseText;
			
			if(allThoseTypes){	
				domain.options.length=0;
				domain.options[0] = new Option("Select Domain", "0");
				domain.style.background ="#ffffff";
				var typeSplit =new Array();
				typeSplit = allThoseTypes.split(","); 
				for (var i=0;i<typeSplit.length;i++){
					var tmpStr =typeSplit[i];
					if(tmpStr){
						var tmpArray = new Array();
						tmpArray = tmpStr.split("|");
						var tmpName = tmpArray[0];
						var tmpVal =  tmpArray[1]; 
						if(tmpName){
							domain.options[i+1] = new Option(tmpName, tmpVal);                           
						}
					}
				}           
			}
		}
	}else{        
		domain.options[0] = new Option("Loading...", "");
		domain.style.background = "#c3c3c3";
	}
}


http_req_alerts_d1 = false;
function updateDomainSearch(client_id){ 
	var client = document.getElementById(client_id).value; 
    if(client!=''){  
		if (window.XMLHttpRequest){ // Mozilla, Safari,...
			http_req_alerts_d1 = new XMLHttpRequest();
		}else if (window.ActiveXObject) { // IE
			try {
				http_req_alerts_d1 = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e) {
				try {
					http_req_alerts_d1 = new ActiveXObject("Microsoft.XMLHTTP");
				}catch (e) {}
			}
		}    
		if (!http_req_alerts_d1) {
			return false;
		} 
		var uri = "get-client-domains.php?client="+client ;   
		
		http_req_alerts_d1.onreadystatechange = loadDomainListSearch;
		http_req_alerts_d1.open('GET', uri, true);
		http_req_alerts_d1.send(null);	
	}else{
		alert('Please Select Client');
		return false;
	}
  
}

function loadDomainListSearch(){ 
	var domain = document.getElementById('sdomain_id');
	if(http_req_alerts_d1.readyState==4){
		if (http_req_alerts_d1.status == 200){
			var allThoseTypes = http_req_alerts_d1.responseText; 
			if(allThoseTypes){	
				domain.options.length=0;
				domain.options[0] = new Option("Select Domain", "0");
				domain.style.background = "#ffffff";
				var typeSplit =new Array();
				typeSplit = allThoseTypes.split(","); 
				for (var i=0;i<typeSplit.length;i++){
					var tmpStr =typeSplit[i];
					if(tmpStr){
						var tmpArray = new Array();
						tmpArray = tmpStr.split("|");
						var tmpName = tmpArray[0];
						var tmpVal =  tmpArray[1]; 
						if(tmpName){
							domain.options[i+1] = new Option(tmpName, tmpVal);                           
						}
					}
				}           
			}
		}
	}else{        
		domain.options[0] = new Option("Loading...", "");
		domain.style.background = "#c3c3c3";
	}
}

function updateToDt(value1){

	var fr_dt = document.getElementById('fr_dt').value;
	var period_in_months = document.getElementById('period_in_months').value;
    var set_start = fr_dt.split('/');  
	var day = set_start[0]; 
 
	var month = (set_start[1] - 1);  // January is 0 so August (8th month) is 7
	var year = set_start[2];
	var myDate = new Date(year, month, day); 
	var newmonth = (month + parseInt(period_in_months));  // Must convert term to integer
	 
	var newdate = myDate.setMonth(newmonth);
	var newdate1 = new Date(newdate);
	day = newdate1.getDate();
	month = newdate1.getMonth() + 1;
	year = newdate1.getFullYear();
	alert("ToDate - "+day+'/'+month+'/'+year);
}