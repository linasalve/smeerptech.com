function showTaskCommentFile(cid,file){
	if (cid!= '') {      
		url = "./work-report.php?perform=download_file";
        url = url + "&cid="+cid+'&file='+ file;   
        openSmallWindow(url,750,600);
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }

}

function showTaskFile(id,file) {

   if (id != '') {      
		url = "./work-report.php?perform=download_file";
        url = url + "&id="+id+'&file='+ file;   
       openSmallWindow(url,750,600);
    }else{
        alert("Invalid file type "+ file_type);
        return (false);
    }
		
}
//Function to update the Banner status using AJAX BOF
var http_req_alerts_banner;
function updateStatus( id,status){

	http_req_alerts_banner = false;	
	document.getElementById("taskid").value = id;

	
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_req_alerts_banner = new XMLHttpRequest();
    }else if (window.ActiveXObject) { // IE
        try {
            http_req_alerts_banner = new ActiveXObject("Msxml2.XMLHTTP");
        }catch (e) {
            try {
                http_req_alerts_banner = new ActiveXObject("Microsoft.XMLHTTP");
            }catch (e) {}
        }
    }    
    if (!http_req_alerts_banner) {
        return false;
    } 
   
    var uri = "get-work-report-status.php?id="+id+"&status="+status;    
	
    http_req_alerts_banner.onreadystatechange = loadStatus;
    http_req_alerts_banner.open('GET', uri, true);
    http_req_alerts_banner.send(null);
}
function loadStatus(){
     id = document.getElementById("taskid").value ;
	 statusid = 'status'+id ;
	 
	if(http_req_alerts_banner.readyState==4){

        if (http_req_alerts_banner.status == 200){
            var allData = http_req_alerts_banner.responseText;
		 
            if(allData){            
            
                var dataSplit =new Array();
                dataSplit = allData.split("|");
                var dataMsg = dataSplit[0];
                var dataStatus = dataSplit[1];
                var dataDoc = dataSplit[2];               
                var dataEdit = dataSplit[3];
                var dataComment = dataSplit[4];
                var dataDelete = dataSplit[5];
				 var dataHighlight = dataSplit[6];
				highlightid = 'row'+dataHighlight ;
				docid = 'doc'+dataHighlight ;
				editid = 'edit'+dataHighlight ;
				commentid = 'comment'+dataHighlight ;
				deleteid = 'delete'+dataHighlight ;
				
                document.getElementById("message").innerHTML= dataMsg; 
                document.getElementById(statusid).innerHTML= dataStatus; 
                document.getElementById(docid).innerHTML= dataDoc; 
                document.getElementById(editid).innerHTML= dataEdit; 
                document.getElementById(commentid).innerHTML= dataComment; 
                document.getElementById(deleteid).innerHTML= dataDelete; 
				
				if(dataHighlight!=0){
					document.getElementById(highlightid).setAttribute("class","highlight");
				    document.getElementById(highlightid).setAttribute("className", "highlight");
					
				}     
            }
        }
    }else{        
        
		document.getElementById("message").innerHTML= ""; 
        
        
    }

}
//Function to update the Banner status using AJAX BOF


function addExecutiveInfo(executive_arr,divname) {
    div_executive = document.getElementById(divname);

    executive_detail_div = document.createElement('div');
    
    if (navigator.appName=="Netscape") {
        executive_detail_div.setAttribute('class', 'row');
    }
    if (navigator.appName.indexOf("Microsoft")!=-1) {
        executive_detail_div.setAttribute('className', 'row');
    }
    
    executive_detail_div.setAttribute('id', 'remove_'+ executive_arr[0]);
        // Create the Hidden User ID field.
        // <input type="hidden" name="client" value="{$_ALL_POST.client}" />
        executive_id_input = document.createElement('input');
        executive_id_input.setAttribute('type', 'hidden');
        executive_id_input.setAttribute('name', 'executive');
        executive_id_input.setAttribute('value', executive_arr[0]);

        executive_det_input = document.createElement('input');
        executive_det_input.setAttribute('type', 'text');
        executive_det_input.setAttribute('name', 'executive_details');
        executive_det_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +") ("+ executive_arr[4] +")");
        executive_det_input.setAttribute('size', '60');
        executive_det_input.setAttribute('readOnly', '1');
        if (navigator.appName=="Netscape") {
            executive_det_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            executive_det_input.setAttribute('className', 'inputbox_c');
        }
        executive_detail_div.appendChild(executive_id_input);
        executive_detail_div.appendChild(executive_det_input);
      
       // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
      	exec_remove_a.onclick = function(){
			removeExecutiveInfo('remove_'+ executive_arr[0],divname);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');            
            
            if (navigator.appName=="Netscape") {
                exec_remove_img.setAttribute('class', 'pl2');
            }
            if (navigator.appName.indexOf("Microsoft")!=-1) {
               exec_remove_img.setAttribute('className', 'pl2');
            }
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);        
        executive_detail_div.appendChild(exec_remove_a);
        
    div_executive.innerHTML = '<span class="txthelp">The executive for whom the Task is alloted.</span>';
    div_executive.appendChild(executive_detail_div);
  

    return (true);
}
function removeExecutiveInfo(id,divname) {
    div_executive = document.getElementById(divname);

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}

/**
 * This function is used to select and relate the Leads
 * task reminder.
 *
 */
function addExecutive(executive_arr) {
    
    div_executive = document.getElementById("executive_info");

        exec_div = document.createElement('div');
        
        if (navigator.appName=="Netscape") {
            exec_div.setAttribute('class', 'row');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            exec_div.setAttribute('className', 'row');
        }
        exec_div.setAttribute('id', 'remove_'+ executive_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        exec_id_input = document.createElement('input');
        exec_id_input.setAttribute('type', 'hidden');
        exec_id_input.setAttribute('name', 'allotted_to[]');
        exec_id_input.setAttribute('value', executive_arr[0]);
        
        exec_email_input = document.createElement('input');
        exec_email_input.setAttribute('type', 'hidden');
        exec_email_input.setAttribute('name', 'exec_email[]');
        exec_email_input.setAttribute('value', executive_arr[4]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ executive_arr[1] +")&nbsp;"+ executive_arr[2] +"&nbsp;"+ executive_arr[3] +"&nbsp;"+ executive_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'team_details[]');
        //exec_client_input.setAttribute('value', "("+ executive_arr[1] +") "+ executive_arr[2] +" "+ executive_arr[3] +" "+ executive_arr[4]);
        exec_client_input.setAttribute('value', executive_arr[2] +" "+ executive_arr[3] +" ("+ executive_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readOnly', '1');
        
        if (navigator.appName=="Netscape") {
            exec_client_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            exec_client_input.setAttribute('className', 'inputbox_c');
        }
        
        
        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ executive_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ executive_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ executive_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeExecutive('remove_'+ executive_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
            
            if (navigator.appName=="Netscape") {
               exec_remove_img.setAttribute('class', 'pl2');
            }
            if (navigator.appName.indexOf("Microsoft")!=-1) {
                exec_remove_img.setAttribute('className', 'pl2');
            }
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
            exec_remove_a.appendChild(exec_remove_img);

        
        exec_div.appendChild(exec_email_input);
        exec_div.appendChild(exec_id_input);
        //exec_div.appendChild(exec_span);
        exec_div.appendChild(exec_client_input);
        exec_div.appendChild(exec_remove_a);

    div_executive.appendChild(exec_div);
    
    return (true);
}


/**
 * This function is used to select and relate the Clients 
 * task reminder.
 *
 */
function addClientInfo(client_arr) {
    
    div_client = document.getElementById("client_info");

        client_div = document.createElement('div');       
        if (navigator.appName=="Netscape") {
            client_div.setAttribute('class', 'row');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            client_div.setAttribute('className', 'row');
        }
        
        client_div.setAttribute('id', 'remove_'+ client_arr[0]);

        // Create the Hidden User ID field.
        // <input type="hidden" name="team[]" value="{$_ALL_POST.client}" />
        client_id_input = document.createElement('input');
        client_id_input.setAttribute('type', 'hidden');
        client_id_input.setAttribute('name', 'allotted_to_client[]');
        client_id_input.setAttribute('value', client_arr[0]);
        
        client_email_input = document.createElement('input');
        client_email_input.setAttribute('type', 'hidden');
        client_email_input.setAttribute('name', 'client_email[]');
        client_email_input.setAttribute('value', client_arr[4]);

        //exec_span = document.createElement('span');
        //exec_span.setAttribute('class', '');
        //exec_span.innerHTML = "("+ client_arr[1] +")&nbsp;"+ client_arr[2] +"&nbsp;"+ client_arr[3] +"&nbsp;"+ client_arr[4];

        exec_client_input = document.createElement('input');
        exec_client_input.setAttribute('type', 'text');
        exec_client_input.setAttribute('name', 'client_details[]');
        //exec_client_input.setAttribute('value', "("+ client_arr[1] +") "+ client_arr[2] +" "+ client_arr[3] +" "+ client_arr[4]);
        exec_client_input.setAttribute('value', client_arr[2] +" "+ client_arr[3] +" ("+ client_arr[1] +")");
        exec_client_input.setAttribute('size', '40');
        exec_client_input.setAttribute('readOnly', '1');        
        if (navigator.appName=="Netscape") {
           exec_client_input.setAttribute('class', 'inputbox_c');
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
           exec_client_input.setAttribute('className', 'inputbox_c');
        }
        // ANCHOR tag to remove the Executive.
        exec_remove_a = document.createElement('a');
        exec_remove_a.setAttribute('href', 'javascript:void(0);');
        exec_remove_a.setAttribute('name', 'remove_'+ client_arr[0]);
        //exec_remove_a.setAttribute('onclick', "javascript:removeExecutive('"+ client_arr[0] +"');");
		//exec_remove_a.onclick = "javascript:removeExecutive('"+ client_arr[0] +"');";
		exec_remove_a.onclick = function(){
			removeClient('remove_'+ client_arr[0]);							
		}
			// Create the IMG tag.
            exec_remove_img = document.createElement('img');
            exec_remove_img.setAttribute('src', '../media/images/nc/off.gif');
           
            if (navigator.appName=="Netscape") {
                exec_remove_img.setAttribute('class', 'pl2');
            }
            if (navigator.appName.indexOf("Microsoft")!=-1) {
                exec_remove_img.setAttribute('className', 'pl2');
            }
            exec_remove_img.setAttribute('border', '0');
            exec_remove_img.setAttribute('title', 'Remove');
            exec_remove_img.setAttribute('alt', 'Remove');
        exec_remove_a.appendChild(exec_remove_img);

        
        client_div.appendChild(client_id_input);
        client_div.appendChild(client_email_input);
        //client_div.appendChild(exec_span);
        client_div.appendChild(exec_client_input);
        client_div.appendChild(exec_remove_a);

    div_client.appendChild(client_div);
    
    return (true);
}

    
/**
 * This function is used to un-relate a Executive from a Task Reminder entry
 *
 */
function removeOnBehalfOf(id) {
    div_executive = document.getElementById("add_on_behalf_info");

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}


/**
 * This function is used to un-relate a Executive from a Task Reminder entry
 *
 */
function removeExecutive(id) {
    div_executive = document.getElementById("executive_info");

    if ( div_executive.removeChild(document.getElementById(id)) ) {
        return (true);
    }
    else {
        return (false);
    }
}

 