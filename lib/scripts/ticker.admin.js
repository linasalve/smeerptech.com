function onSiteChange() {
	// Change the Static page list in the Select option.
	obj_form 	= document.frmTickerAdd;
	lst_site_id = obj_form.elements['site_id[]'];
	count 		= lst_site_id.length;
	site_id_str = '';
	
	for ( i=0; i<count; i++ ) {
		if ( lst_site_id.options[i].selected == true ) {
			site_id_str += lst_site_id.options[i].value +',';
		}
	}
	site_id_str = site_id_str.substr(0, (site_id_str.length-1) );
	
	return (getPageList(site_id_str, fillPageList));
}



/** 
 * This function is used to initiate the retrival of static pages 
 * from the server.
 */
function getPageList(site_id_str, handler) {
	var q_string = "";

	q_string = "perform=getSitePages";
	q_string += "&site_id="+site_id_str;



	if ( !requestPOST("ajax.php", q_string, handler) ) {
		alert("cannot connect to the server");
	}		
	return false;
}

function fillPageList() {
	if ( objXmlHttp.readyState == 4 
		||
		objXmlHttp.readyState == "complete" ) 
	{
		if ( objXmlHttp.status == 200 ) {
			//window.document.getElementById("content_main").innerHTML = objXmlHttp.responseText;
			eval("response = " + objXmlHttp.responseText + ";");
			fillSelectOptions('visible_on_page[]', response);
		}
		else {
			//window.document.getElementById("content_main").innerHTML = "Cannot connect to the Server.";
			alert("Cannot connect to the Server.");
		}
		registerScript(objXmlHttp.responseText);
//		window.document.getElementById("window_loading").className = "hidden";
//		window.document.getElementById("window_inactive").className = "hidden";
//		showSelect();
		objXmlHttp = null;
	}
	else {
//		hideSelect();
//		window.document.getElementById("window_inactive").className = "visible";
//		window.document.getElementById("window_loading").className = "visible";
//		window.document.getElementById("window_loading").innerHTML = getLoadingHTML();
		
		//window.document.getElementById("content_main").innerHTML = getLoadingHTML();
	}
}



function onLinkTypeChange(option) {
	if ( option == 'P' ) {
		document.getElementById('link_ecraft').style.display = 'none';
		document.getElementById('link_dynamic').style.display = 'none';
		document.getElementById('link_external').style.display = 'none';
	}
	else if ( option == 'S' ) {
		document.getElementById('link_ecraft').style.display = 'block';
		document.getElementById('link_dynamic').style.display = 'none';
		document.getElementById('link_external').style.display = 'none';
	}
	else if ( option == 'D' ) {
		document.getElementById('link_ecraft').style.display = 'none';
		document.getElementById('link_dynamic').style.display = 'block';
		document.getElementById('link_external').style.display = 'none';
	}
	else if ( option == 'E' ) {
		document.getElementById('link_ecraft').style.display = 'none';
		document.getElementById('link_dynamic').style.display = 'none';
		document.getElementById('link_external').style.display = 'block';
	}

	return (true);
}


function onLinkSiteChange() {
	// Change the Static page list in the Select option.
	obj_form 	= document.frmTickerAdd;
	lst_site_id = obj_form.elements['link_site'];
	count 		= lst_site_id.length;
	site_id_str = '';
	
	for ( i=0; i<count; i++ ) {
		if ( lst_site_id.options[i].selected == true ) {
			site_id_str += lst_site_id.options[i].value +',';
		}
	}
	site_id_str = site_id_str.substr(0, (site_id_str.length-1) );
	
	return (getLinkPageList(site_id_str, fillLinkPageList));
}



/** 
 * This function is used to initiate the retrival of static pages 
 * from the server.
 */
function getLinkPageList(site_id_str, handler) {
	var q_string = "";

	q_string = "perform=getLinkPageList";
	q_string += "&site_id="+site_id_str;



	if ( !requestPOST("ajax.php", q_string, handler) ) {
		alert("cannot connect to the server");
	}		
	return false;
}


function fillLinkPageList() {
	if ( objXmlHttp.readyState == 4 
		||
		objXmlHttp.readyState == "complete" ) 
	{
		if ( objXmlHttp.status == 200 ) {
			//window.document.getElementById("content_main").innerHTML = objXmlHttp.responseText;
			eval("response = " + objXmlHttp.responseText + ";");
			fillSelectOptions('link_page', response);
		}
		else {
			//window.document.getElementById("content_main").innerHTML = "Cannot connect to the Server.";
			alert("Cannot connect to the Server.");
		}
		registerScript(objXmlHttp.responseText);
//		window.document.getElementById("window_loading").className = "hidden";
//		window.document.getElementById("window_inactive").className = "hidden";
//		showSelect();
		objXmlHttp = null;
	}
	else {
//		hideSelect();
//		window.document.getElementById("window_inactive").className = "visible";
//		window.document.getElementById("window_loading").className = "visible";
//		window.document.getElementById("window_loading").innerHTML = getLoadingHTML();
		
		//window.document.getElementById("content_main").innerHTML = getLoadingHTML();
	}
}



/** 
 * This function is used to fill the Select with the new values 
 * that have been passed as an array in the function call.
 *
 */
function fillSelectOptions(select_name, options_arr) {
	obj_form 	= document.frmTickerAdd;
	lst_select	= obj_form.elements[select_name];

	for (i = lst_select.options.length-1; i>=0; i--) {
		lst_select.options[i]=null ;
	}
	
	// Fill the Page list with new content.
	count = options_arr.length;
	for ( i=0; i<count; i++ ) {
		lst_select.options[i] =  new Option(options_arr[i][1], options_arr[i][0]);
	}
	
	return (true);
}