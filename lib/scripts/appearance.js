function setBackImage(element, imageURL) {
	document.getElementById(element).style.backgroundImage = 'url('+ imageURL +')';
	//document.getElementById(element).style.backgroundPosition = '0px 0px';
	//document.getElementById(element).style.color = '#CF0021';
}
function unsetBackImage(element) {
	document.getElementById(element).style.backgroundImage = 'none';
	//document.getElementById(element).style.color = '#000000';
}

function setClass(element, classname) {
	document.getElementById(element).style.className = classname;
}


function optionHighlight(div,classname) {	
	var cell = document.getElementById(div);
	toggleClass(cell,classname);
}

function optionDefault(div,classname) {
	var cell = document.getElementById(div);
	toggleClass(cell,classname);
}

function isClass(target,className) {
	if (!target.className) {
		return false;
	}
	if (isType(className,STRING)) {
		return target.className.indexOf(className) > -1;
		
	} else if (isType(className,OBJECT)) {
		for (var i = 0; i < className.length; i++) {
			if (target.className.indexOf(className[i]) > -1) {
				return true;
			}
		}
	}
	
	return false;
}

function addClass(target,k) {
	var cn = target.className;
	if (cn && cn.indexOf(k) > -1) {
		return;
	}
	if (cn && cn.length > 0) {
		k = SPACE + k;
		cn += k;
	} else {
		cn = k;
	}
	target.className = cn;
}

function removeClass(target,k) {
	var cn = target.className;
	var index;
	
	if (!cn) {
		return;
	}
	cn = trim(cn);
	if ((index = cn.indexOf(k)) > -1) {
		cn = cn.substring(0,index)+cn.substring(index+k.length);
	}
	
	target.className = cn;
}

function toggleClass(target,k) {
	if (isClass(target,k)) {
		removeClass(target,k);
	} else {
		addClass(target,k);
	}	
}

function getURL(URL) {
	window.location (URL);
}

function cleanSlate(id) {
	var slate;
	slate = document.getElementById(id);
	slate.value = "";
	slate.style.color = "#292929";
}