<?php
/**
* English (en) translation file.
* This also serves as the base translation file from which to derive
*  all other translations.


////////////////////////////////
/* Do not modify this section */
////////////////////////////////
global $strings;			  //
global $email;				  //
global $dates;				  //
global $charset;			  //
global $letters;			  //
global $days_full;			  //
global $days_abbr;			  //
global $days_two;			  //
global $days_letter;		  //
global $months_full;		  //
global $months_abbr;		  //
global $days_letter;		  //
/******************************/

// Charset for this language
// 'iso-8859-1' will work for most languages
$charset = 'iso-8859-1';

/***
  DAY NAMES
  All of these arrays MUST start with Sunday as the first element
   and go through the seven day week, ending on Saturday
***/
// The full day name
$days_full = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
// The three letter abbreviation
$days_abbr = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
// The two letter abbreviation
$days_two  = array('Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa');
// The one letter abbreviation
$days_letter = array('S', 'M', 'T', 'W', 'T', 'F', 'S');

/***
  MONTH NAMES
  All of these arrays MUST start with January as the first element
   and go through the twelve months of the year, ending on December
***/
// The full month name
$months_full = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
// The three letter month name
$months_abbr = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

// All letters of the alphabet starting with A and ending with Z
$letters = array ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

/***
  DATE FORMATTING
  All of the date formatting must use the PHP strftime() syntax
  You can include any text/HTML formatting in the translation
***/
// General date formatting used for all date display unless otherwise noted
$dates['general_date'] = '%d/%m/%Y';
// General datetime formatting used for all datetime display unless otherwise noted
// The hour:minute:second will always follow this format
$dates['general_datetime'] = '%d/%m/%Y @';
// Date in the reservation notification popup and email
$dates['res_check'] = '%A %d/%m/%Y';
// Date on the scheduler that appears above the resource links
$dates['schedule_daily'] = '%A,<br />%d/%m/%Y';
// Date on top-right of each page
$dates['header'] = '%A, %B %d, %Y';
// Jump box format on bottom of the schedule page
// This must only include %m %d %Y in the proper order,
//  other specifiers will be ignored and will corrupt the jump box
$dates['jumpbox'] = '%d %m %Y';

/***
  STRING TRANSLATIONS
  All of these strings should be translated from the English value (right side of the equals sign) to the new language.
  - Please keep the keys (between the [] brackets) as they are.  The keys will not always be the same as the value.
  - Please keep the sprintf formatting (%s) placeholders where they are unless you are sure it needs to be moved.
  - Please keep the HTML and punctuation as-is unless you know that you want to change it.
***/
$strings['hours'] = 'hours';
$strings['minutes'] = 'minutes';
// The common abbreviation to hint that a user should enter the month as 2 digits
$strings['mm'] = 'mm';
// The common abbreviation to hint that a user should enter the day as 2 digits
$strings['dd'] = 'dd';
// The common abbreviation to hint that a user should enter the year as 4 digits
$strings['yyyy'] = 'yyyy';
$strings['am'] = 'am';
$strings['pm'] = 'pm';

$strings['Administrator'] = 'Administrator';
$strings['Welcome Back'] = 'Welcome Back, %s';

$strings['Log Out'] = 'Log Out';
$strings['My Control Panel'] = 'My Control Panel';
$strings['Help'] = 'Help';



$strings['Default'] = 'Default';
$strings['Reset'] = 'Reset';
$strings['Edit'] = 'Edit';
$strings['Delete'] = 'Delete';
$strings['Cancel'] = 'Cancel';
$strings['View'] = 'View';
$strings['Modify'] = 'Modify';
$strings['Save'] = 'Save';
$strings['Proceed'] = 'Proceed' ; // Added new
$strings['Back'] = 'Back';
$strings['Next'] = 'Next';
$strings['Close Window'] = 'Close Window';
$strings['Paybtn'] = 'Pay for It'; // Added new
$strings['Credit Card Number'] ='Credit Card Number';// Added new
$strings['Expiration Month'] ='Expiration Month'; // Added new
$strings['Expiration Year'] = 'Expiration Year'; // Added new
$strings['Card Code'] = 'Card Code'; // Added new
$strings['Pay'] = 'Pay for Reservation'; //Added new
$strings['Transaction cannot be processed.'] = 'Transaction cannot be processed.'; //Added New
$strings['Amount'] = 'Amount'; // Added new
$strings['Search'] = 'Search';
$strings['Clear'] = 'Clear';


$strings['Hidden'] = 'Hidden';
$strings['Show Summary'] = 'Show Summary';

$strings['No'] = 'No';
$strings['Yes'] = 'Yes';
$strings['Name'] = 'Name';
$strings['First Name'] = 'First Name';
$strings['Last Name'] = 'Last Name';
$strings['Resource Name'] = 'Resource Name';
$strings['Property'] = 'Property' ;
$strings['Value'] = 'Value' ;
$strings['Email'] = 'Email';
$strings['Institution'] = 'Company';	//	$strings['Institution'] = 'Institution';
$strings['Phone'] = 'Phone';
$strings['Password'] = 'Password';
$strings['Permissions'] = 'Permissions';
$strings['View information about'] = 'View information about %s %s';
$strings['Send email to'] = 'Send email to %s %s';
$strings['Reset password for'] = 'Reset password for %s %s';
$strings['Edit permissions for'] = 'Edit permissions for %s %s';
$strings['Edit credits for'] = 'Edit credits for %s %s' ;
$strings['Position'] = 'Position';
$strings['Password (6 char min)'] = 'Password (6 char min)';
$strings['Re-Enter Password'] = 'Re-Enter Password';

$strings['Search Users'] = 'Search Users';
$strings['Location'] = 'Location';
$strings['Phone'] = 'Phone';
$strings['Notes'] = 'Notes';
$strings['Status'] = 'Status';
$strings['All Users'] = 'All Users';
$strings['Add New User'] = 'Add New User';

$strings['Edit data for'] = 'Edit data for %s';
$strings['Active'] = 'Active';
$strings['Inactive'] = 'Inactive';
$strings['Auto-assign permission'] = 'Auto-assign permission';
$strings['Allowed'] = 'Allowed';
$strings['Notify user'] = 'Notify user';

$strings['Date'] = 'Date';
$strings['User'] = 'User';
$strings['Email Users'] = 'Email Users';
$strings['Subject'] = 'Subject';
$strings['Message'] = 'Message';
$strings['Please select users'] = 'Please select users';
$strings['Send Email'] = 'Send Email';
$strings['problem sending email'] = 'Sorry, there was a problem sending your email. Please try again later.';
$strings['The email sent successfully.'] = 'The email sent successfully.';
$strings['do not refresh page'] = 'Please <u>do not</u> refresh this page. Doing so will send the email again.';
$strings['Return to email management'] = 'Return to email management';

$strings['Please select which tables and fields to export'] = 'Please select which tables and fields to export:';
$strings['all fields'] = '- all fields -';
$strings['HTML'] = 'HTML';
$strings['Plain text'] = 'Plain text';
$strings['XML'] = 'XML';
$strings['CSV'] = 'CSV';
$strings['Export Data'] = 'Export Data';

$strings['Reset Password for'] = 'Reset Password for %s';
$strings['Please edit your profile'] = 'Please edit your profile';
$strings['Please register'] = 'Please register';
$strings['Email address (this will be your login)'] = 'Email address (this will be your login)';
$strings['Email address (this will be user login)'] = 'Email address (this will be user login)';
$strings['Keep me logged in'] = 'Keep me logged in <br />(requires cookies)';
$strings['Edit Profile'] = 'Edit Profile';
$strings['Register'] = 'Register';
$strings['Please Log In'] = 'Please Log In';
$strings['Email address'] = 'Email address';
$strings['Password'] = 'Password';
$strings['First time user'] = 'First time user?';
$strings['Click here to register'] = 'Click here to register';
$strings['Register'] = 'Register';
$strings['Log In'] = 'Log In';

$strings['I Forgot My Password'] = 'I Forgot My Password';
$strings['Retreive lost password'] = 'Retreive lost password';
$strings['Get online help'] = 'Get online help';
$strings['Language'] = 'Language';
$strings['(Default)'] = '(Default)';

$strings['My Announcements'] = 'My Announcements';
$strings['My Reservations'] = 'My Reservations';
$strings['My Permissions'] = 'My Permissions';
$strings['My Quick Links'] = 'My Quick Links';
$strings['Announcements as of'] = 'Announcements as of %s';
$strings['There are no announcements.'] = 'There are no announcements.';

$strings['Created'] = 'Created';
$strings['Last Modified'] = 'Last Modified';
$strings['Change My Profile Information/Password'] = 'Change My Profile Information/Password';
$strings['Manage My Email Preferences'] = 'Manage My Email Preferences';
$strings['Mass Email Users'] = 'Mass Email Users';
$strings['Export Database Content'] = 'Export Database Content';
$strings['View System Stats'] = 'View System Stats';
$strings['Email Administrator'] = 'Email Administrator';


$strings['Per page'] = 'Per page:';
$strings['Page'] = 'Page:';

$strings['You are not logged in!'] = 'You are not logged in!';

/***
  EMAIL MESSAGES
  Please translate these email messages into your language.  You should keep the sprintf (%s) placeholders
   in their current position unless you know you need to move them.
  All email messages should be surrounded by double quotes "
  Each email message will be described below.
***/

$email_text['greet'] = "Dear %s, \r\n" ;

// Email message that a user gets after they register
$email_text['register_member_notify_sub'] = "Successful registration with smeerp family %s" ;
$email_text['register_member_notify'] =  "You have successfully registered with the following information:\r\n\r\n"
									//	. "Username   : %s \r\n"
										. "Username   : %s \r\n"
                                        . "Password   : %s \r\n\r\n"
										. "First Name : %s \r\n"
										. "Last Name  : %s \r\n"
										. "Company    : %s \r\n"
										. "Position   : %s \r\n\r\n "
									//	. "Site Name  : %s \r\n\r\n"
										. "Please save this information. You will be needing it to log on to your account at :\r\n"
										. "%s \r\n\r\n"
										. "You can view your complete details at the following link :\r\n"
										. "%s \r\n\r\n"	;
				
				
// Email message that a admin gets after a user has registered
$email_text['register_member_notify_admin_sub'] = "New member registered with smeerp family %s" ;
$email_text['register_member_notify_admin'] = "A new member has registered with the following information:\r\n"
											. "Username   : %s \r\n"
											//. "Email      : %s \r\n"
											. "First Name : %s \r\n"
											. "Last Name  : %s \r\n"
											. "Company    : %s \r\n"
											. "Position   : %s \r\n"
											. "Site Name  : %s \r\n\r\n"
											. "Click on the following link to view the details :\r\n"
											. "%s \r\n\r\n";				
				
				

// Email message that a user gets after they have been registered by Admin
$email_text['register_by_admin_sub'] = "Invitation to join smeerp family%s" ;


$email_text["welcome_to_smeerp"] = "Welcome to SMEERP Family!\r\n"
										."SMEERP is a IT firm based in Jhena, India dealing in Web technologies and "
										."Network security & solutions.\r\n\r\n"
										."We have four divisions - \r\n"
										."\thttp://www.pretechno.com\r\n"
										."\tWeb development using LAMP (Linux, Apache, MySQL, PHP), SUN - J2EE, EJB, JSP,\r\n"
										."\tMicrosoft - ASP, ASP.NET, .NET framework, MS SQL etc.\r\n\r\n"
                                        ."\tDesktop development using Visual C++, Visual Basic, MS SQL, Oracle etc.\r\n\r\n"
                                        ."\tEnterprise development using LAMP - PHP, Linux, MySQL, Apache, and Visual C++,\r\n"
                                        ."\tVisual Basic, MS SQL, Oracle etc. Soon we are coming up with ERP and CRM solutions.\r\n\r\n"
										."\tNetwork Security & Solutions - Linux Server Setup, Linux Server Admin, Windows Server\r\n"
                                        ."\tSetup, Windows Server Admin, Internet Gateway Setup, Internet Security, Mail Server,\r\n"
                                        ."\tNetwork Solutions.\r\n\r\n"
										."\thttp://www.just247.com\r\n"
										."\tWeb hosting, Domain name registration, SSL, Mailing solutions, Linux Dedicated Server,\r\n"
                                        ."\tWindows Dedicated Server, Linux Hosting, Windows Hosting etc. Comparing the price, quality\r\n"
                                        ."\tand service, we are the best around. We have industry leading, world class service & support.\r\n"
										."\tworld class service support.\r\n\r\n"
										."\thttp://www.buywebart.com\r\n"
										."\tWeb designing, Flash animation, Presentation, Banner Advt. Creative and artistic templates.\r\n\r\n" ;

$email_text['register_by_admin'] = "You have partially registered with the following information:\r\n"
							//	. "Username   : %s \r\n"
								. "First Name : %s \r\n"
								. "Last Name  : %s \r\n"
							//	. "Site Name  : %s \r\n\r\n"
								. "This information was added by %s.\r\n"
								. "To complete your registration click on the following link to open in the same window \r\n"
								. " or copy paste the link into a new browser window.\r\n"
								. "%s \r\n\r\n"
								. "If you do not wish to register please click on the following link to remove your information from our database.\r\n"
								. "%s \r\n\r\n" ;


//	Email the admin when a user requests to delete the entry
// Email message that a user gets after they have been registered by Admin
$email_text['unregister_sub'] = "Request to remove entry of %s at MySMEERP" ;
$email_text['unregister'] =   "A request has been made to remove the entry with the following information:\r\n"
							. "Username   : %s \r\n"
							. "First Name : %s \r\n"
							. "Last Name  : %s \r\n"
							. "Site Name  : %s \r\n"
							. "Date Added : %s \r\n\r\n"
							. "This information was added by %s.\r\n"
							. "To view the entry click on the following link :\r\n"
							. "%s \r\n\r\n" ;


// Email message that a admin gets after a user has subscribed
$email_text['subscribe_member_notify_admin_sub'] = "New member Subscribed to %s" ;
$email_text['subscribe_member_notify_admin'] = "A new member has subscribed with the following information:\r\n"
											. "Username   : %s \r\n"
											. "Email      : %s \r\n"
											. "First Name : %s \r\n"
											. "Last Name  : %s \r\n"
											. "Permission : %s \r\n"
											. "Site Name  : %s \r\n\r\n"
											. "Click on the following link to view the details :\r\n"
											. " %s \r\n\r\n";


// Email message which is sent when the invoice is expiring in a few days
$email_text['invoice_expiry_sub'] = "The package purchase against Invoice no. %s is expiring on %s"  ;
$email_text['invoice_expiry'] = "The package purchased against invoice with the following information is expiring in few days:\r\n"
											. "Invoice no    : %s \r\n"
											. "Creation Date : %s \r\n"
											. "Expiry Date   : %s \r\n"
											. "First Name    : %s \r\n"
											. "Last Name     : %s \r\n"
											. "E-mail        : %s \r\n"
											. "Particulars   : \r\n"
											. "---------------------------------------------------------\r\n"
											. "%s\r\n"
											. "---------------------------------------------------------\r\n\r\n"									
											. "Please renew the invoice to continue receiving the services.\r\n\r\n" ;






$email_text['general_footer'] = "\nBest Regards,\n"
								."%s\n"
								."SMEERP Technologies\n"
								."India Office +91 (0) 98 2307 8819\n"
								."             +91 (0) 98 2307 8899\n"
								."             +91 (0712) 5646453\n" ;



$email_text['do_not_reply'] = "[TO REPLY THE SUPPORT TICKET PLEASE LOGIN TO YOUR MYSMEERP MEMBER PANEL]\r\n" ;

//	Email message that a user gets after creation of a new ticket.
$email_text['support_ticket_new_sub'] = "New Support Ticket no. %s created in your MySMEERP account" ;
$email_text['support_ticket_new'] = "A new support ticket has been created with the following information:\r\n\r\n"
									. "Ticket No. : %s \r\n"
									. "Owner      : %s \r\n"
									. "Creator    : %s \r\n"
									. "Date/Time  : %s \r\n\r\n"
									//. "Priority   : %s \r\n"
									//. "Department : %s \r\n"
									//. "Status     : %s \r\n\r\n"
									. "Subject    : %s \r\n\r\n"
									//. "Comments    : \r\n"
                                   // . "-------------------------------------------------------------------------------\r\n"
                                    . "%s\r\n\r\n" 
                                    //. "-------------------------------------------------------------------------------\r\n\r\n"                                    
									. "Attachment [Login to MySMEERP to view] : %s \r\n\r\n" ;
									
//	Email message that a user gets after a ticket is closed.
$email_text['support_ticket_close_sub'] = "Closed Support Ticket no. %s in your MySMEERP account" ;
$email_text['support_ticket_close'] = "The support ticket No. %s has been closed :\r\n\r\n"
										. "Reason : There was no response against this Ticket for last %s days. \r\n"
									 	. "However you can open this ticket anytime.\r\n"
										. "To re-open the ticket simply change its status to open\r\n"
										. "To view the ticket click on the following link:\r\n" ;									
									
$email_text['support_ticket_details'] = "To view the details click on the following link :\r\n"
										. "%s \r\n\r\n" ;
																				
$email_text['support_ticket_footer'] = "As always, it is our pleasure to assist you in any way possible!\r\n" 
									."\nBest Regards,\n"
									."SMEERP Snipers\n"
									."SMEERP e-Technologies Pvt. Ltd.\n"
									//."India Office +91 (0) 98 2307 8819\n"
									//."             +91 (0) 98 2307 8899\n"
									//."             +91 (0712) 5646453\n"
									//."-------------------------------------------------------------------------------\n"
									//."Because this update contains security sensitive information, we ask that you please log on "
									//."to http://my.pretechno.com to view the update. Should you have any questions or comments, "
									//."please feel free to add them to your ticket via http://my.pretechno.com. For security"
                                    //."purposes, comments may only be added to your support tickets via the MySMEERP Portal. "
                                    //."Emailed replies to this notification message will not be received by SMEERP staff..\n\n"
									//."As always, it is our pleasure to assist in any way possible!\n\n"
									//."If you have any further questions or concerns, please do not hesitate to contact us by"
									//."creating the support ticket via http://my.pretechno.com.";
									;

//	Email message that a user gets after creation of a new thread to a ticket.
$email_text['support_ticket_thread_sub'] = "Updated Support Ticket no. %s  in your MySMEERP account" ;
/*
$email_text['support_ticket_thread'] = "The support ticket has been updated with the following information:\r\n"
										. "Thread No.     : %s \r\n"
										. "Owner          : %s \r\n"
										. "Creator        : %s \r\n\r\n"
										. "Date/Time      : %s \r\n"
										. "Response Time  : %s \r\n\r\n"
										. "Priority       : %s \r\n"
										. "Department     : %s \r\n"
										. "Status         : %s \r\n\r\n"
										. "Subject        : %s \r\n"
										. "Text           : \r\n\t\t %s \r\n\r\n"
										. "Attachment     : %s \r\n\r\n\r\n" ;

*/

/* Modified on 02 June 2006 Replaced with the later one
$email_text['support_ticket_thread'] = "Your created support ticket has been updated:\r\n\r\n"
										. "Ticket No.         : %s \r\n"
										. "Created By         : %s \r\n"
										. "Department         : %s \r\n"
										. "Priority           : %s \r\n"
										. "Status             : %s \r\n"
										. "Subject            : %s \r\n\r\n"
										. "Date Created       : %s \r\n" 
										. "Date Last modified : %s \r\n\r\n" ;
										. "If you find your issue as resolved then please UPDATE the STATUS of the ticket as CLOSED \r\n"
										. "from the status drop down box on the View Support Ticket page.\r\n"
										. "You can re-open this TICKET by changing the STATUS to OPEN from the status drop down box \r\n"
										. "on the View Support Ticket page.\r\n\r\n" ;
*/

$email_text['support_ticket_thread'] = "Your created support ticket has been updated:\r\n\r\n"
										. "Ticket No.         : %s \r\n"
										. "Created By         : %s \r\n"
										//. "Department         : %s \r\n"
										//. "Priority           : %s \r\n"
										//. "Status             : %s \r\n"
										. "Date Created       : %s \r\n" 
										//. "Date               : %s \r\n\r\n" ;
										. "Subject            : %s \r\n";
$email_text['support_ticket_msg'] 	= " ";// "If you find your issue as resolved then please UPDATE the STATUS of the ticket as CLOSED \r\n"
										//. "from the status drop down box on the View Support Ticket page.\r\n"
										//. "You can re-open this TICKET by changing the STATUS to OPEN from the status drop down box \r\n"
										//. "on the View Support Ticket page.\r\n\r\n" ;


/* Modified on 02 June 2006 Replaced with the later one
$email_text['support_ticket_thread_childs'] =    "Ticket No.    : %s\r\n" 
												."Date          : %s\r\n"
												."Written By    : %s\r\n"
												."Response Time : %s\r\n"
												."Comments      : \r\n"
												. "-------------------------------------------------------------------------------\r\n"
												. "%s\r\n" 
												. "-------------------------------------------------------------------------------\r\n\r\n" 
												."Attachment      : %s \r\n\r\n" ;
*/
$email_text['support_ticket_thread_childs'] =    "Response Time      : %s\r\n"
												//."Comment         : \r\n"
												."\r\n"
												//."-------------------------------------------------------------------------------\r\n"
												."%s\r\n\r\n"
												."Attachment [Login to MySMEERP to view] : %s \r\n";
												//."-------------------------------------------------------------------------------\r\n\r\n"  ;

$email_text['previous_threads'] = "\r\nPrevious threads  (Oldest First)\r\n" ;
$email_text['seperator_line'] = "_____________________________________________________________________\r\n" ;



// Email that the user gets when the administrator changes their password
$email_text['password_reset'] = "Your %s password has been reset by the administrator.\r\n\r\n"
			. "Your temporary password is:\r\n\r\n %s\r\n\r\n"
			. "Please use this temporary password (copy and paste to be sure it is correct) to log into %s at %s"
			. " and immediately change it using the 'Change My Profile Information/Password' link in the My Quick Links table.\r\n\r\n"
			. "Please contact %s with any questions.";

// Email that the user gets when they change their lost password using the 'Password Reset' form
$email_text['new_password_sub'] = "MySMEERP: Password changed for your MySMEERP account" ;
$email_text['new_password'] = "A request for the new password was made using this email address.\r\n"
								. "Your new password for your account is:%s\r\n\r\n"
								. "Please Log In at %s "
								. "with this new password "
								. "(copy and paste it to ensure it is correct) "
								. "and promptly change your password by clicking the "
								. "My Info/Change Password "
								. "link in My Control Panel.\r\n\r\n"
								. "Please direct any questions to %s.";


$email_text['new_order_sub'] = "New Order (No. %s) is created in your MySMEERP account" ;
$email_text['new_order'] = "With reference to the enquiry/discussion/request, a new order has been placed with us having following details\r\n\r\n"
							. "Order No.    : %s \r\n" 
							. "Date         : %s \r\n"
							. "Member       : %s \r\n"
							. "Particulars  : \r\n"
							. "---------------------------------------------------------\r\n"
							. "%s\r\n"
							. "---------------------------------------------------------\r\n\r\n" 
                            . "The order will be processed soon, if you have confirmed the order via phone/email/support ticket. \r\n"
                            . "If you have not confirmed the processing of order then we will wait for approx 21 days as per \r\n"
                            . "SMEERP Technologies policy before cancelling it.\r\n\r\n" ;
$email_text['order_detail'] = "To view the details click on the following link:\r\n %s \r\n\r\n" ;


$email_text['new_transaction_sub'] = "New %s Note (No. %s) created : SMEERP Technologies"  ;
$email_text['new_transaction'] = "A new %s Note has been created in your account \r\n\r\n"
							. "Transact No. : %s \r\n" 
							. "Date         : %s \r\n"
							. "Amount       : %s \r\n\r\n"
							. "Title        : %s \r\n"
							. "Particulars  : \r\n"
							. "---------------------------------------------------------\r\n"
							. "%s\r\n"
							. "---------------------------------------------------------\r\n\r\n" ;
$email_text['transaction_detail'] = "To view the details click on the following link:\r\n %s \r\n\r\n" ;




$email_text['ref_sub'] = "Referral Reward : SMEERP Technologies" ;
$email_text['ref'] = "\tWith due thanks, we inform you that a sum of Rs. %s is pending to be credited with \r\n"
						."SMEERP to your account as a Referral Reward for referring Mr. %s %s \r\n"
						."to SMEERP Technologies.\r\n\r\n"
						."As you become eligible to utilize this Referral Reward only after completion of order \r\n"
						."and payment procedure by your friend hence you may suggest him to expedite the process.\r\n\r\n"                        
                        ."Details of Referral Reward scheme ::: \r\n"
						."Reap the SMEERP Rewards!\r\n"
						."Earn each time you refer a friend to SMEERP.\r\n"
						."SMEERP is pleased to inform about newly launched Customer Referral Program.\r\n"
						."Under this program when you refer anybody to SMEERP and his / her order gets confirmed \r\n"
						."with payment, the then referral reward for the particular package gets credited with SMEERP \r\n"
						."to your account. You can adjust accumulated credit reward through any future deal of yours with \r\n"
						."SMEERP excepting domain name registration charge.\r\n\r\n"
                        ."Terms and Conditions: \r\n"
						."Only one promotion valid at a time. Offer is not valid with any other discounts. Credits \r\n"
						."will be issued to all accounts in good standing only. SMEERP Referral program is in \r\n"
                        ."effect from 16th March 2005.\r\n" ;

$email_text['new_invoice_sub'] = "New %s no. %s created in your MySMEERP account" ;

$email_text['new_invoice1'] ='<div class="moz-text-html" lang="x-western">'
							.'    <table align="center" border="0" cellpadding="1" cellspacing="0" width="600">'
							.'    <tr>'
							.'      	<td valign="middle" width="400">'
							.'	  		<font class="header-font" face="Arial"><b>Invoice</b></font>'
							.'		</td>'
							.'      	<td align="right" width="200">&nbsp;</td>'
							.'    </tr>'
							.'  	</table>'
							.'	<table align="center" border="0" cellpadding="1" cellspacing="0" width="600">'
							.'    <tr valign="top">'
							.'      	<td valign="top" width="100%">'
							.'      		<table bordercolorlight="#FEF4BF" bordercolordark="#FFE130" border="0" '
							.'						bordercolor="#84c321" cellpadding="0" cellspacing="0" width="100%">'
							.'        	<tr>'
							.'          		<td class="boxborder-yellow" align="left" height="1" bordercolor="#FFFFFF" bgcolor="#FFE130"></td>'
							.'        	</tr>'
							.'        	<tr>'
							.'          		<td class="small-font" bgcolor="#ffffe7" width="100%" align="center">'
							.'          			<table class="table-tradelist-color" border="0" cellpadding="2" cellspacing="1" width="100%">'
							.'            		<tr>'
							.'              			<td rowspan="5" width="47%" bgcolor="#FFFFFF" align="left">'
							.'							<font face="Arial">' ;


$email_text['new_invoice2'] ='								<b>%s</b><br/>' ;

$email_text['new_invoice3'] ='								<br/>'
							.'								<font class="small-font" style="font-size: 8pt">';


$email_text['new_invoice4'] ='									%s <br />' 
							.'									%s <br />'
							.'									%s %s <br />'
							.'									%s <br />'
							.'									%s <br />' ;

$email_text['new_invoice5'] ='								</font>'
							.'							</font>'
							.'						</td>'
							.'						<td width="22%" align="right" bgcolor="#FFFFFF">&nbsp;'
							.'						<!--	<font class="small-font" face="Arial" style="font-size: 8pt">Username</font>-->'
							.'						</td>'
							.'						<td width="31%" align="right" bgcolor="#FFFFFF">&nbsp;'
							.'						<!--	<font face="Arial" style="font-size: 8pt">{$invoice.username}</font>-->'
							.'						</td>'
							.'					</tr>'
							.'					<tr>'
							.'						<td width="22%" align="right" bgcolor="#FFFFFF">'
							.'							<font class="small-font" face="Arial" style="font-size: 8pt">Invoice Number</font>'
							.'						</td>'
							.'						<td width="31%" align="right" bgcolor="#FFFFFF">'
							.'							<font class="small-font" face="Arial" style="font-size: 8pt">' ;


$email_text['new_invoice6'] ='%s';


$email_text['new_invoice7'] ='							</font></td>'
							.'					</tr>'
							.'					<tr>'
							.'						<td width="22%" align="right" bgcolor="#FFFFFF">'
							.'							<font class="small-font" face="Arial" style="font-size: 8pt">Invoice Date</font>'
							.'						</td>'
							.'						<td width="31%" align="right" bgcolor="#FFFFFF">' 
							.'							<font class="small-font" face="Arial" style="font-size: 8pt">' ;
$email_text['new_invoice8'] =	'%s' ;

$email_text['new_invoice9'] ='							</font>'
							.'						</td>'
							.'					</tr>'
							.'					<tr>'
							.'						<td width="22%" align="right" bgcolor="#FFFFFF">'
							.'							<font class="small-font" face="Arial" style="font-size: 8pt">Package / Item / Service against this Invoice has expiry date of </font>'
							.'						</td>'
							.'						<td width="31%" align="right" bgcolor="#FFFFFF">'
							.'							<font class="small-font" face="Arial" style="font-size: 8pt">';
							
$email_text['new_invoice10'] ='							%s';

$email_text['new_invoice11'] ='							</font>'
							.'						</td>'
							.'					</tr>					'
							.'					<tr>'
							.'						<td width="22%" align="right" bgcolor="#FFFFFF">'
							.'							<font class="small-font" face="Arial" style="font-size: 8pt">Order No.</font>'
							.'						</td>'
							.'						<td width="31%" align="right" bgcolor="#FFFFFF">'
							.'							<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
$email_text['new_invoice12'] ='								<b>%s</b>' ;

$email_text['new_invoice13'] ='							</font>'
							.'						</td>'
							.'					</tr>'
							.'					</table>'
							.'				</td>'
							.'			</tr>'
							.'			<tr>'
							.'				<td class="boxborder-yellow" align="center" height="1" bgcolor="#FFE130"></td>'
							.'			</tr>'
							.'			</table>'
							.'		</td>'
							.'	</tr>'
							.'	</table>'
							.'	<br/>'	
							.'  	<table align="center" border="0" cellpadding="1" cellspacing="0" width="600">'
							.'    <tr class="boxborder-yellow">'
							.'      	<td valign="top">'
							.'			<table border="1" cellpadding="3" cellspacing="0" width="100%" '
							.'					style="border-collapse:collapse;border-style:solid;padding-left:4;'
							.'							padding-right:4;padding-top:1;padding-bottom:1" '
							.'					bordercolor="#FFE130">'
							.'			<tr class="table-alt-color">'
							.'				<td class="big-maroon-font" height="23" bgcolor="#FEF4BF">'
							.'					<b><font face="Arial" size="2" color="#800000">'
							.'						Transaction Details for Invoice '
							.'					</font></b>'
							.'				</td>'
							.'			</tr>'
							.'			</table>'
							.'      	</td>'
							.'	</tr>'
							.'  	</table>'  	
							.'	<table align="center" border="0" cellpadding="2" cellspacing="1" width="600">'
							.'    <tr class="table-color">'
							.'      	<td colspan="1" bgcolor="#FFFFE7" height="25" width="75%">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
							
$email_text['new_invoice14'] ='			<b>Opening Balance on %s</b>' ;

$email_text['new_invoice15'] ='			</font>'
							.'		</td>'
							.'      	<td align="right" width="25%" bgcolor="#FFFFE7" height="25" style="padding-right:5px;">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;


$email_text['new_invoice16'] ='				<b>%s&nbsp;%s</b>' ;


$email_text['new_invoice17'] ='			</font>'
							.'		</td>'
							.'    </tr>'
							.'    <tr class="table-alt-color">'
							.'      	<td width="75%" bgcolor="#FEF4BF" height="25">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt"><b>Particulars</b></font>'
							.'		</td>'
							.'      	<td align="right" width="25%" bgcolor="#FEF4BF" height="25" style="padding-right:5px;">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
$email_text['new_invoice18'] ='<b>Amount&nbsp;%s</b></font>' ;

$email_text['new_invoice19'] ='	 	</td>' 
							.'    </tr>'
							.'    <tr class="table-color">'
							.'      	<td width="75%" bgcolor="#FFFFE7" height="50">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">'
							.'				<pre>' ;


$email_text['new_invoice20'] ='%s' ;

$email_text['new_invoice21'] ='</pre>'
							.'			</font>'
							.'		</td>'
							.'      	<td align="right" width="25%" bgcolor="#FFFFE7" height="14">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;

$email_text['new_invoice22'] = '%s' ;

$email_text['new_invoice23'] ='</font> '
							.'		</td>'
							.'    </tr>'
							.'	<tr class="table-alt-color">'
							.'      	<td width="75%" bgcolor="#FEF4BF" height="19">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
							
$email_text['new_invoice24'] ='In words : <b>%s&nbsp;%s</b>' ;


$email_text['new_invoice25'] ='			</font>'
							.'		</td>'
							.'      	<td align="right" width="25%" bgcolor="#FEF4BF" height="14">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt"></font>'
							.'		</td>'
							.'    </tr>'
							.'	<tr class="table-alt-color">'
							.'      	<td width="75%" bgcolor="#FEF4BF" height="19">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt"><b>Total Invoice Amount</b></font>'
							.'		</td>'
							.'      	<td align="right" width="25%" bgcolor="#FEF4BF" height="19">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
							
$email_text['new_invoice26'] ='				<b>%s&nbsp;%s</b>' ;

$email_text['new_invoice27'] ='			</font>'
							.'	 	</td>'
							.'    </tr>'
							.'    <tr class="table-color">'
							.'      	<td colspan="4" class="boxborder-yellow" height="5" bgcolor="#FFE130" bordercolor="#FFE130"></td>'
							.'    </tr>'
							.'    <tr class="table-color">'
							.'      	<td width="75%" height="19" bgcolor="#FFFFE7">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;


$email_text['new_invoice28'] ='				<b>Closing Account Balance on %s</b>' ;


$email_text['new_invoice29'] ='			</font>'
							.'		</td>'
							.'      	<td align="right" width="25%" height="19" bgcolor="#FFFFE7">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;

$email_text['new_invoice30'] ='				<b>%s&nbsp;%s</b>' ;

$email_text['new_invoice31'] ='			</font>'
							.'		</td>'
							.'    </tr>'
							.'	<tr class="table-alt-color">'
							.'      	<td width="75%" bgcolor="#FEF4BF" height="19">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
$email_text['new_invoice32'] ='				<b>Pending Invoice Amount on %s</b> ' ;

$email_text['new_invoice33'] ='			</font>'
							.'		</td>'
							.'      	<td align="right" width="25%" bgcolor="#FEF4BF" height="19">'
							.'      		<font class="small-font" face="Arial" style="font-size: 8pt"><b>' ;


$email_text['new_invoice34'] ='%s&nbsp;%s' ;


$email_text['new_invoice35'] ='</b></font>'
							.'	 	</td>'
							.'    </tr>		'
							.'  	</table>'
							.'<br/>'
							.'	<table align="center" border="0" cellpadding="1" cellspacing="0" width="600" height="18">'
							.'    <tr class="boxborder-yellow">'
							.'      	<td valign="top" height="16">'
							.'      		<table border="1" cellpadding="3" cellspacing="0" width="600" bordercolor="#FFE130" '
							.'												style="border-collapse: collapse" height="17">'
							.'        	<tr class="table-alt-color">'
							.'          		<td class="big-maroon-font" height="11" bgcolor="#FEF4BF" width="592">'
							.'          			<font face="Arial" size="2" color="#800000"><b>Payment Options</b></font>'
							.'				</td>'
							.'        	</tr>'
							.'      		</table>'
							.'      	</td>'
							.'    </tr>'
							.'  	</table>	'
							.'  	<table class="table-alt-color" align="center" border="0" cellpadding="0" cellspacing="0" width="600">'
							.'    <tr class="table-alt-color">'
							.'     	<td>'
							.'      	<div align="center">'
							.'        <center>'
							.'      		<table border="1" cellpadding="5" cellspacing="0" width="600" '
							.'							bordercolor="#FFE130" style="border-collapse: collapse">'
							.'        	<tr class="table-color">'
							.'          		<td align="left" width="15%" bgcolor="#FFFFE7">'
							.'          		<ul>'
							.'            		<li><font face="Arial" style="font-size: 8pt">'
							.'						<b>Online Bank Transfer</b> - ICICI</font>'
							.'					</li>'
							.'            		<li><font face="Arial" style="font-size: 8pt">'
							.'						<b>Cheque</b> or <b>Demand Draft</b> - Drawn in favour '
							.'							of \'<b>SMEERP Technologies</b>\'</font>'
							.'					</li>'
							.'					<li><font face="Arial" style="font-size: 8pt">'
							.'						Send your payment to: '
							.'            			Billing Department, SMEERP Technologies, 512, A Wing, Lokmat Bhawan. Jhena '
							.'            			400012</font>'
							.'					</li>'
							.'          		</ul>'
							.'				</td>'
							.'			</tr>'
							.'      		</table>'
							.'		</center>'
							.'      	</div>'
							.' 		</td>'
							.'	</tr>'
							.'  	</table>'
							.'<br/>'
							.'  	<table align="center" border="0" cellpadding="1" cellspacing="0" width="600">'
							.'    <tr class="boxborder-yellow">'
							.'      	<td valign="top">'
							.'      		<table border="1" cellpadding="3" cellspacing="0" width="100%" '
							.'						bordercolor="#FFE130" style="border-collapse: collapse">'
							.'        	<tr class="table-alt-color">'
							.'          		<td class="big-maroon-font" height="23" bgcolor="#FEF4BF">'
							.'          			<font face="Arial" size="2" color="#800000"><b>Terms &amp; Conditions</b></font>'
							.'				</td>'
							.'        	</tr>'
							.'      		</table>'
							.'      	</td>'
							.'    </tr>'
							.'  	</table>  '
							.'  	<table class="table-alt-color" align="center" border="0" cellpadding="0" cellspacing="0" width="600">'
							.'    <tr class="table-alt-color">'
							.'      	<td>'
							.'      	<div align="center">'
							.'        <center>'
							.'      		<table border="1" cellpadding="5" cellspacing="0" '
							.'					width="600" bordercolor="#FFE130" style="border-collapse: collapse">'
							.'        	<tr class="table-color">'
							.'          		<td align="left" width="15%" bgcolor="#FFFFE7">'
							.'				<font class="small-font">'
							.'          		<ul>'
							.'            		<li>'
							.'						<font face="Arial" style="font-size: 8pt">'
							.'							In case of late payment, interest will be charged '
							.'							@ 10% per week.'
							.'						</font> '
							.'					</li>'
							.'           			<li>'
							.'						<font face="Arial" style="font-size: 8pt">'
							.'							Every Cheque returned will attract a penalty of '
							.'							Rs.500/-.'
							.'						</font>'
							.'					</li>'
							.'           			<li>'
							.'						<font face="Arial" style="font-size: 8pt">'
							.'							Goods once sold will not be taken back or exchanged.'
							.'						</font>'
							.'					</li>'
							.'           			<li>'
							.'						<font face="Arial" style="font-size: 8pt">'
							.'							Our risk &amp; responsibility ceases on delivery of the goods.'
							.'						</font>'
							.'					</li>'
							.'            		<li>'
							.'						<font face="Arial" style="font-size: 8pt">'
							.'							Email us at <u><font color="#0000FF">billing@pretechno.com</font></u> '
							.'							or call us on weekdays between 10.00 am to 7.00 pm Jhena : 0712 5646453 '
							.'						</font>'
							.'					</li>'
							.'            		<li>'
							.'						<font face="Arial" style="font-size: 8pt">'
							.'							Subject to Jhena Jurisdiction.'
							.'						</font>'
							.'					</li>'
							.'          		</ul>'
							.'          		</font>'
							.'				</td>'
							.'        	</tr>'
							.'      		</table>'
							.'		</center>'
							.'      	</div>'
							.' 		</td>'
							.'    </tr>'
							.'  	</table>  '
							.'  	<table align="center" border="0" cellpadding="5" cellspacing="1" width="600" height="52">'
							.'    <tr>'
								.'      	<td class="small-font" align="center" height="22">'
							.'      		<font face="Arial" style="font-size: 8pt">'
							.'			This is a computer generated invoice and does not require any signature.<br>'
							.'      		SMEERP Technologies, </font> <font class="small-font">'
							.'          	<font face="Arial" style="font-size: 8pt">512, A Wing, Lokmat Bhawan. Jhena '
							.'            400012</font></font><font face="Arial" style="font-size: 8pt">. '
							.'			</font>'
							.'		</td>'
							.'	</tr>'
							.'    <tr class="table-color">'
							.'      	<td class="boxborder-yellow" height="9" bgcolor="#FFE130"></td>'
							.'    </tr>'
							.'  	</table>'
							.'  	<p><font face="Arial"><br/>&nbsp;</font>'
							.'</div>' ;







$email_text['new_receipt1'] ='<div class="moz-text-html" lang="x-western">'
								.' <table align="center" border="0" cellpadding="1" cellspacing="0" width="600">'
								.'<tr>'
									.'<td valign="middle" width="400">'
										.'<font class="header-font" face="Arial"><b>Receipt</b></font>'
									.'</td>'
									.'<td align="right" width="200">&nbsp;</td>'
								.'</tr>'
								.'</table>'
								.'<table align="center" border="0" cellpadding="1" cellspacing="0" width="600">'
								.'<tr valign="top">'
									.'<td valign="top" width="100%">'
										.'<table bordercolorlight="#FEF4BF" bordercolordark="#FFE130" border="0" '
												.'bordercolor="#84c321" cellpadding="0" cellspacing="0" width="100%">'
										.'<tr>'
											.'<td class="boxborder-yellow" align="left" height="1" bordercolor="#FFFFFF" bgcolor="#FFE130"></td>'
										.'</tr>'
										.'<tr>'
											.'<td class="small-font" bgcolor="#ffffe7" width="100%" align="center">'
												.'<table class="table-tradelist-color" border="0" cellpadding="2" cellspacing="1" width="100%">'
												.'<tr>'
													.'<td rowspan="4" width="47%" bgcolor="#FFFFFF" align="left">'
														.'<font face="Arial">'
															.'<b>' ;
$email_text['new_receipt2'] = '%s' ;

$email_text['new_receipt3'] =								'</b><br/>'
															.'<br/>'
														.'<font class="small-font" style="font-size: 8pt">' ;
														
$email_text['new_receipt4'] =								'%s <br/>'
															.'%s <br/>'
															.'%s %s <br>'
															.'%s <br/>'
															.'%s ' ;
															
$email_text['new_receipt5'] =								'</font>'
														.'</font>'
													.'</td>'
													.'<td width="22%" align="right" bgcolor="#FFFFFF">&nbsp;'
														.'<!--	<font class="small-font" face="Arial" style="font-size: 8pt">Username</font>-->'
													.'</td>'
													.'<td width="31%" align="right" bgcolor="#FFFFFF">&nbsp;'
														.'<!--<font face="Arial" style="font-size: 8pt">{$receipt.username}</font>-->'
													.'</td>'
												.'</tr>'
												.'<tr>'
													.'<td width="22%" align="right" bgcolor="#FFFFFF">'
														.'<font class="small-font" face="Arial" style="font-size: 8pt">Receipt Number</font>'
													.'</td>'
													.'<td width="31%" align="right" bgcolor="#FFFFFF">'
														.'<font class="small-font" face="Arial" style="font-size: 8pt"><b>' ;
							
							
$email_text['new_receipt6'] ='%s' ;

$email_text['new_receipt7'] = 							'</b></font>'
													.'</td>'
							.'</tr>'
							.'<tr>'
							.'<td width="22%" align="right" bgcolor="#FFFFFF">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">Invoce Number</font>'
							.'</td>'
							.'<td width="31%" align="right" bgcolor="#FFFFFF">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt"><b>' ;


$email_text['new_receipt8'] = '%s' ;

$email_text['new_receipt9'] = 							'</b></font>'
							.'</td>'
							.'</tr>'
							.'<tr>'
							.'<td width="22%" align="right" bgcolor="#FFFFFF">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">Receipt Date</font>'
							.'</td>'
							.'<td width="31%" align="right" bgcolor="#FFFFFF">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt"><b>' ;
							
$email_text['new_receipt10'] = '%s' ;


$email_text['new_receipt11'] = 							'</b></font>'
							.'</td>'
							.'</tr>'
							.'<!--'
							.'<tr>'
							.'<td width="22%" align="right" bgcolor="#FFFFFF">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">Current Account Balance</font>'
							.'</td>'
							.'<td width="31%" align="right" bgcolor="#FFFFFF">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' 
							.'<b>{$receipt.currency}&nbsp;{$receipt.receipt_closing_balance}</b>'
							.'</font>'
							.'</td>'
							.'</tr>'
							.'-->'
							.'</table>'
							.'</td>'
							.'</tr>'
							.'<tr>'
							.'<td class="boxborder-yellow" align="center" height="1" bgcolor="#FFE130"></td>'
							.'</tr>'
							.'</table>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'	<br/>'
							.'<table align="center" border="0" cellpadding="1" cellspacing="0" width="600">'
							.'<tr class="boxborder-yellow">'
							.'<td valign="top">'
							.'<table border="1" cellpadding="3" cellspacing="0" width="100%" '
							.'style="border-collapse:collapse;border-style:solid;padding-left:4;'
							.'padding-right:4;padding-top:1;padding-bottom:1" '
							.'bordercolor="#FFE130">'
							.'<tr class="table-alt-color">'
							.'<td class="big-maroon-font" height="23" bgcolor="#FEF4BF">'
							.'<b><font face="Arial" size="2" color="#800000">'
							.'Transaction Details for Receipt '
							.'</font></b>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'<table align="center" border="0" cellpadding="2" cellspacing="1" width="600">'
							.'<tr class="table-color">'
							.'<td width="75%" colspan="1" bgcolor="#FFFFE7" height="25">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">'
							.'<b>Opening Invoice Balance on ' ;
							
$email_text['new_receipt12'] = '%s' ;

$email_text['new_receipt13'] =  		'</b>'
							.'</font>'
							.'</td>'
							.'<td align="right" width="25%" bgcolor="#FFFFE7" height="25" style="padding-right:5px;">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
							
$email_text['new_receipt14'] = '<b>%s&nbsp;%s</b>' ;


$email_text['new_receipt15'] = '</font>'
							.'</td>'
							.'</tr>'
							.'<tr class="table-alt-color">'
							.'<td width="75%" bgcolor="#FEF4BF" height="25">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt"><b>Particulars</b></font>'
							.'</td>'
							.'<td align="right" width="25%" bgcolor="#FEF4BF" height="25" style="padding-right:5px;">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
$email_text['new_receipt16'] = '<b>Amount&nbsp;%s</b></font>' ;

$email_text['new_receipt17'] =			'</td>'
							.'</tr>'
							.'<tr class="table-color">'
							.'<td width="75%" bgcolor="#FFFFE7" height="50">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt"><pre>' ;
							
$email_text['new_receipt18'] = '%s' ; 

$email_text['new_receipt19'] = 				'</pre></font>'
							.'</td>'
							.'<td align="right" width="25%" bgcolor="#FFFFE7" height="14">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' ;


$email_text['new_receipt20'] = '%s' ;


$email_text['new_receipt21'] = 				'</font>'
							.'</td>'
							.'</tr>'
							.'<tr class="table-color">'
							.'<td width="75%" bgcolor="#FFFFE7" height="50">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' ;


$email_text['new_receipt22'] = 'Mode Of Payment : <b>%s</b>' ;

$email_text['new_receipt23'] =					'<br />Details :<br />'
							.'<pre><span style="padding-left:20px;font-weight:bold;">' ;
							
$email_text['new_receipt24'] = '%s' ;

$email_text['new_receipt25'] =				'</span></pre>'
							.'</font>'
							.'</td>'
							.'<td align="right" width="25%" bgcolor="#FFFFE7" height="14">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt"></font>'
							.'</td>'
							.'</tr>'
							.'<tr class="table-alt-color">'
							.'<td width="75%" bgcolor="#FEF4BF" height="19">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
							
$email_text['new_receipt26'] = 'In words : <b>%s&nbsp;%s</b></font>' ;


$email_text['new_receipt27'] =		'</td>'
							.'<td align="right" width="25%" bgcolor="#FEF4BF" height="14">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt"></font>'
							.'</td>'
							.'</tr>'
							.'<tr class="table-alt-color">'
							.'<td width="75%" bgcolor="#FEF4BF" height="19">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt"><b>Total</b></font>'
							.'</td>'
							.'<td align="right" width="25%" bgcolor="#FEF4BF" height="19">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
							
$email_text['new_receipt28'] = '<b>%s&nbsp;%s</b>' ;


$email_text['new_receipt29'] =			'</font>'
							.'</td>'
							.'</tr>'
							.'<tr class="table-color">'
							.'<td colspan="4" class="boxborder-yellow" height="5" bgcolor="#FFE130" bordercolor="#FFE130"></td>'
							.'</tr>'
							.'<tr class="table-color">'
							.'<td width="75%" height="19" bgcolor="#FFFFE7">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' ;
							
							
$email_text['new_receipt30'] = '<b>Closing Invoice Balance on %s</b>' ;


$email_text['new_receipt31'] = 				'</font>'
							.'</td>'
							.'<td align="right" width="25%" height="19" bgcolor="#FFFFE7">'
							.'<font class="small-font" face="Arial" style="font-size: 8pt">' ;

$email_text['new_receipt32'] = '%s&nbsp;%s' ;


$email_text['new_receipt33'] = '</b>'
							.'</font>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'<br/>'
							.'<table align="center" border="0" cellpadding="1" cellspacing="0" width="600" height="18">'
							.'<tr class="boxborder-yellow">'
							.'<td valign="top" height="16">'
							.'<table border="1" cellpadding="3" cellspacing="0" width="600" bordercolor="#FFE130" '
							.'style="border-collapse: collapse" height="17">'
							.'<tr class="table-alt-color">'
							.'<td class="big-maroon-font" height="11" bgcolor="#FEF4BF" width="592">'
							.'<font face="Arial" size="2" color="#800000"><b>Payment Options</b></font>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'<table class="table-alt-color" align="center" border="0" cellpadding="0" cellspacing="0" width="600">'
							.'<tr class="table-alt-color">'
							.'<td>'
							.'<div align="center">'
							.'<center>'
							.'<table border="1" cellpadding="5" cellspacing="0" width="600" '
							.'bordercolor="#FFE130" style="border-collapse: collapse">'
							.'<tr class="table-color">'
							.'<td align="left" width="15%" bgcolor="#FFFFE7">'
							.'<ul>'
							.'<li><font face="Arial" style="font-size: 8pt">'
							.'<b>Online Bank Transfer</b> - ICICI</font>'
							.'</li>'
							.'<li><font face="Arial" style="font-size: 8pt">'
							.'<b>Cheque</b> or <b>Demand Draft</b> - Drawn in favour '
							.'of \'<b>SMEERP Technologies</b>\'</font>'
							.'</li>'
							.'<li><font face="Arial" style="font-size: 8pt">'
							.'Send your payment to: '
							.'Billing Department, SMEERP Technologies, 512, A Wing, Lokmat Bhawan. Jhena '
							.'400012</font>'
							.'</li>'
							.'</ul>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'</center>'
							.'</div>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'<br/>'
							.'<table align="center" border="0" cellpadding="1" cellspacing="0" width="600">'
							.'<tr class="boxborder-yellow">'
							.'<td valign="top">'
							.'<table border="1" cellpadding="3" cellspacing="0" width="100%" '
							.'bordercolor="#FFE130" style="border-collapse: collapse">'
							.'<tr class="table-alt-color">'
							.'<td class="big-maroon-font" height="23" bgcolor="#FEF4BF">'
							.'<font face="Arial" size="2" color="#800000"><b>Terms &amp; Conditions</b></font>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'</td>'
							.'</tr>'
							.'</table>  '
							.'<table class="table-alt-color" align="center" border="0" cellpadding="0" cellspacing="0" width="600">'
							.'<tr class="table-alt-color">'
							.'<td>'
							.'<div align="center">'
							.'<center>'
							.'<table border="1" cellpadding="5" cellspacing="0" '
							.'width="600" bordercolor="#FFE130" style="border-collapse: collapse">'
							.'<tr class="table-color">'
							.'<td align="left" width="15%" bgcolor="#FFFFE7">'
							.'<font class="small-font">'
							.'<ul>'
							.'<li>'
							.'<font face="Arial" style="font-size: 8pt">'
							.'In case of late payment, interest will be charged '
							.'@ 10% per week.'
							.'</font> '
							.'</li>'
							.'<li>'
							.'<font face="Arial" style="font-size: 8pt">'
							.'Every Cheque returned will attract a penalty of '
							.'Rs.500/-.'
							.'</font>'
							.'</li>'
							.'<li>'
							.'<font face="Arial" style="font-size: 8pt">'
							.'Goods once sold will not be taken back or exchanged.'
							.'</font>'
							.'</li>'
							.'<li>'
							.'<font face="Arial" style="font-size: 8pt">'
							.'Our risk &amp; responsibility ceases on delivery of the goods.'
							.'</font>'
							.'</li>'
							.'<li>'
							.'<font face="Arial" style="font-size: 8pt">'
							.'Email us at <u><font color="#0000FF">billing@pretechno.com</font></u> '
							.'or call us on weekdays between 10.00 am to 7.00 pm Jhena : 0712 5646453 '
							.'</font>'
							.'</li>'
							.'<li>'
							.'<font face="Arial" style="font-size: 8pt">'
							.'Subject to Jhena Jurisdiction.'
							.'</font>'
							.'</li>'
							.'</ul>'
							.'</font>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'</center>'
							.'</div>'
							.'</td>'
							.'</tr>'
							.'</table>'
							.'<table align="center" border="0" cellpadding="5" cellspacing="1" width="600" height="52">'
							.'<tr>'
							.'<td class="small-font" align="center" height="22">'
							.'<font face="Arial" style="font-size: 8pt">'
							.'This is a computer generated invoice and does not require any signature.<br>'
							.'SMEERP Technologies, </font> <font class="small-font">'
							.'<font face="Arial" style="font-size: 8pt">512, A Wing, Lokmat Bhawan. Jhena '
							.'400012</font></font><font face="Arial" style="font-size: 8pt">. '
							.'</font>'
							.'</td>'
							.'</tr>'
							.'<tr class="table-color">'
							.'<td class="boxborder-yellow" height="9" bgcolor="#FFE130"></td>'
							.'</tr>'
							.'</table>'
							.'<p><font face="Arial"><br/>&nbsp;</font>'
							.'</div>' ;




?>