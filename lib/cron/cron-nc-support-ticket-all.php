<?php
  
    if (!defined( "THIS_DOMAIN")) {
        //include('/var/www/html/leena/smeerptech.com/smeerptech.com/lib/config.php');
		include ('/home/httpd/vhosts/smeerptech.com/httpdocs/lib/config.php');	
	}
  
   
	$db 		= new db_local; // database handle
	$s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class.
	
	//putenv("TZ=GMT"); // done in config.php

	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true;
    
    
    
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
 

    $db_new 	= new db_local; // database handle
   	$temp_query = " SELECT ". TABLE_TICKET_ALL_BATCHES .".id, "
				.TABLE_TICKET_ALL_BATCHES .".ticket_id,"
				.TABLE_TICKET_ALL_BATCHES .".ticket_no,"
				.TABLE_TICKET_ALL_BATCHES .".ticket_subject,"
				.TABLE_TICKET_ALL_BATCHES .".ticket_text,"
				.TABLE_TICKET_ALL_BATCHES .".ticket_attachment,"
				.TABLE_TICKET_ALL_BATCHES .".mail_client,"
				.TABLE_TICKET_ALL_BATCHES .".mail_vendor,"
				.TABLE_TICKET_ALL_BATCHES .".template_id,"
				.TABLE_TICKET_ALL_BATCHES .".template_file_1,"
				.TABLE_TICKET_ALL_BATCHES .".template_file_2,"
				.TABLE_TICKET_ALL_BATCHES .".template_file_3,"
				.TABLE_TICKET_ALL_BATCHES .".mail_to_all_su,"
				.TABLE_TICKET_ALL_BATCHES .".email,"
				.TABLE_TICKET_ALL_BATCHES .".ticket_date,"
				.TABLE_TICKET_ALL_BATCHES .".total_batches,"
				.TABLE_TICKET_ALL_BATCHES .".pending_batches,"
				.TABLE_TICKET_ALL_BATCHES .".total_members,"
				.TABLE_TICKET_ALL_BATCHES .".per_batch "
				." FROM ". TABLE_TICKET_ALL_BATCHES
		." WHERE ". TABLE_TICKET_ALL_BATCHES .".pending_batches !=0 AND "
		.TABLE_TICKET_ALL_BATCHES.".status='".SupportTicket::STALL_BATCH_ACTIVE."'"
		." ORDER BY do_e ASC "." LIMIT 0, 1";
	$db->query($temp_query); 
	$data = array();
	if( $db->nf()>0 ){
		$db->next_record();
		$data = processSqlData( $db->Record );
		//print_R($data);
		$batch_no =0;
	}
	
	if(!empty($data)){
		$batch_no = ($data['total_batches']- $data['pending_batches'])+1 ;
		$batch_id = $data['id'];
		$pending_batches = $data['pending_batches'];
		$from=0;
		if($batch_no==1){			 
			$from=0;
		}else{			 	
			//$from = ($data['total_batches']- $data['pending_batches'] ) * $data['per_batch'] + 1;
			$from = ( $data['total_batches'] - $data['pending_batches'] ) * $data['per_batch'] ;
		}
		$condition ='';
		$attachfilename = $data['ticket_attachment'];
		$ticket_subject = $data['ticket_subject'];
		$ticket_text = $data['ticket_text'];
		$ticket_id = $data['ticket_id'];
		$ticket_no = $data['ticket_no'];
		$mail_to_all_su = $data['mail_to_all_su'];
		$mail_client = $data['mail_client'];		 
		$mail_vendor = $data['mail_vendor'];		 
		$per_batch = $data['per_batch'];
		$email = $data['email'];
	
		
		if(!empty($email)){ 
			/***************************************
			* Send the notification to the Client
			* of the new Ticket being created
			****************************************/
			$data['ticket_no']    =   $ticket_no ;                       
			$data['mticket_no']   =   $ticket_no ;
			$data['replied_by']   =   '' ;
			$data['subject']    =     $ticket_subject ;
			$data['text']    =   $ticket_text ;
			$data['attachment']   =   $attachfilename ;
			$data['counter']   =   $batch_no ;
			
			$file_name='';
			if(!empty($attachfilename)){
				$file_name[] = DIR_FS_ST_FILES ."/". $attachfilename;
			}
			if(!empty($data['template_file_1'])){
				$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
			}
			if(!empty($data['template_file_2'])){
				$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
			}
			if(!empty($data['template_file_3'])){
				$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
			}
			
			//Send a copy to check the mail for clients		
		
			$emailArr=explode(",",$data['email']);
			if(!empty($emailArr)){
				if ( getParsedEmail($db_new, $s, 'EMAIL_STALL_MEMBER', $data, $email) ) { 
					foreach($emailArr as $key=>$val){
						$sendStatus='';
						$to_email ='';
						$to     = '';
						$cc_to_sender=$cc=$bcc='';
						$to[]   = array('name' =>$val, 'email' => $val);
						$to_email = $val;
						
						$from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']); 						  $sendStatus = SendMail($to,$from, $reply_to, $email["subject"], $email["body"], 
						$email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
						
						$pos = strpos($sendStatus,'Email Sent');                   
						if($pos == false){
							$status = 1;
							$email_sent.=$val.",";
						}else{
							$status = 0;
							$email_not_sent.=$val."," ;
						}	
						
					}
					
					$query1 = "INSERT INTO 
					  " . TABLE_TICKET_ALL_CLIENTS ." SET "
					  . TABLE_TICKET_ALL_CLIENTS .".ticket_no      = '". $ticket_no ."', "
					  . TABLE_TICKET_ALL_CLIENTS .".ticket_id      = '". $ticket_id."', "
					  . TABLE_TICKET_ALL_CLIENTS .".email_sent	   = '".trim($email_sent,",") ."', "
					  . TABLE_TICKET_ALL_CLIENTS .".email_not_sent = '".trim($email_not_sent,",")."', "
					  . TABLE_TICKET_ALL_CLIENTS .".batch_no       = '".$batch_no."', " 
					  . TABLE_TICKET_ALL_CLIENTS .".status         = '".$status."', " 
					  . TABLE_TICKET_ALL_CLIENTS .".do_sent        = '".date('Y-m-d H:i:s')."' "; 
					  $db_new->query($query1);
				}
			}
			
		}else{	
			 
			if($mail_to_all_su ==1 && $mail_client ==1 && $mail_vendor ==1){
						
			}elseif($mail_to_all_su==1 && $mail_client==1 && $mail_vendor ==0){					
					$condition = " AND ".TABLE_CLIENTS.".member_type LIKE '%".Clients::MEMBER_CLIENTS."%' ";
			}elseif($mail_to_all_su==1 && $mail_client==0 && $mail_vendor ==1){
					$condition = " AND ".TABLE_CLIENTS.".member_type LIKE '%".Clients::MEMBER_VENDORS."%' ";
			}elseif($mail_to_all_su ==0 && $mail_client ==1 && $mail_vendor ==1){					 
					$condition = " AND ".TABLE_CLIENTS.".parent_id ='' ";					 
			}elseif($mail_to_all_su ==0 && $mail_client ==1 && $mail_vendor ==0){
					$condition = " AND ".TABLE_CLIENTS.".parent_id ='' AND 
					".TABLE_CLIENTS.".member_type LIKE '%".Clients::MEMBER_CLIENTS."%' " ;
			}elseif($mail_to_all_su ==0 && $mail_client ==0 && $mail_vendor ==1){
					$condition = " AND ".TABLE_CLIENTS.".parent_id ='' AND 
					".TABLE_CLIENTS.".member_type LIKE '%".Clients::MEMBER_VENDORS."%' " ;
			}elseif($mail_to_all_su ==0 && $mail_client ==1 && $mail_vendor ==1){
					$condition = " AND ".TABLE_CLIENTS.".parent_id =''" ;
			}    
		
			$sql=  "SELECT ".TABLE_CLIENTS.".user_id,".TABLE_CLIENTS.".parent_id, "
					.TABLE_CLIENTS.".f_name,"				
					.TABLE_CLIENTS.".l_name,"				
					.TABLE_CLIENTS.".number,"				
					.TABLE_CLIENTS.".email,"				
					.TABLE_CLIENTS.".email_1,"				
					.TABLE_CLIENTS.".email_2"				
					." FROM ".TABLE_CLIENTS." WHERE 
					".TABLE_CLIENTS.".status='".Clients::ACTIVE."' AND ".TABLE_CLIENTS.".check_email='0' ".$condition;
			 $sql .= " LIMIT ".$from.", ".$per_batch;
			 $db->query( $sql ); 
			
			if ( $db->nf() > 0 ) {
				while( $db->next_record() ) {
					$details = processSqlData( $db->Record ); 
					$client_name = $details['f_name']." ".$details['l_name'];
				
					$to_user_id = $details['user_id'];
					$client_email = $details['email'];
					$client_email_1 = $details['email_1'];
					$client_email_2 = $details['email_2'];
					$parent_id = $details['parent_id'];
					$is_subuser = !empty($parent_id) ? '1': '0';
					$ticket_owner_uid = !empty($parent_id) ? $parent_id : $to_user_id;
						/***************************************
						* Send the notification to the Client
						* of the new Ticket being created
						****************************************/
						$data['ticket_no']    =   $ticket_no ;                       
						$data['mticket_no']   =   $ticket_no ;
						$data['replied_by']   =   '' ;
						$data['subject']    =     $ticket_subject ;
						$data['text']    =   $ticket_text ;
						$data['attachment']   =   $attachfilename ;
						$data['counter']   =   $batch_no ;
						
						$file_name='';
						if(!empty($attachfilename)){
							$file_name[] = DIR_FS_ST_FILES ."/". $attachfilename;
						}
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
						
						 //Send a copy to check the mail for clients
						 $sendStatus='';
						 $to_email ='';
						if ( getParsedEmail($db_new, $s, 'EMAIL_STALL_MEMBER', $data, $email) ) { 
							if(!empty($client_email)){
								$to     = '';
								$cc_to_sender=$cc=$bcc='';
								$to[]   = array('name' => $client_email, 
								'email' => $client_email);
														 
								$to_email = $client_email;
								$from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
								/* echo $email["body"]; */
								
								$sendStatus= SendMail($to,$from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
								$cc_to_sender,$cc,$bcc,$file_name);
							}						
							if(!empty($client_email_1)){
								$to     = '';
								$cc_to_sender=$cc=$bcc='';
								$to[]   = array('name' => $client_email_1, 'email' => $client_email_1);
								$to_email = $to_email.",".$client_email_1;
								$from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
								/* echo $email["body"]; */
								
								$sendStatus= SendMail($to,$from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
								$cc_to_sender,$cc,$bcc,$file_name);
							}	
							if(!empty($client_email_2)){
								$to     = '';
								$cc_to_sender=$cc=$bcc='';
								$to[]   = array('name' => $client_email_2,'email' => $client_email_2);
								$to_email = $to_email.",".$client_email_2;
								$from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
								
								$sendStatus= SendMail($to,$from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
								$cc_to_sender,$cc,$bcc,$file_name);
							}	
						}
						
						$pos    =   strpos($sendStatus,'Email Sent');                   
						if($pos == false){
							$status=1;
						}else{
							$status=0;
						}		
						 
						$query1 = "INSERT INTO 
						  " . TABLE_TICKET_ALL_CLIENTS ." SET "
						  . TABLE_TICKET_ALL_CLIENTS .".ticket_no         = '". $ticket_no ."', "
						  . TABLE_TICKET_ALL_CLIENTS .".ticket_id        = '". $ticket_id."', "
						  . TABLE_TICKET_ALL_CLIENTS .".ticket_owner_uid    = '". $ticket_owner_uid ."', "
						  . TABLE_TICKET_ALL_CLIENTS .".is_subuser       = '". $is_subuser ."', "
						  . TABLE_TICKET_ALL_CLIENTS .".to_name = '". $client_name ."', "
						  . TABLE_TICKET_ALL_CLIENTS .".to_email       = '".$to_email."', "
						  . TABLE_TICKET_ALL_CLIENTS .".to_user_id    = '".$to_user_id."', "
						  . TABLE_TICKET_ALL_CLIENTS .".batch_no    = '".$batch_no."', " 
						  . TABLE_TICKET_ALL_CLIENTS .".status     = '".$status."', " 
						  . TABLE_TICKET_ALL_CLIENTS .".do_sent    = '".date('Y-m-d H:i:s')."' "; 
						  
						  $db_new->query($query1);
						  
				}
			}
		}
		//Update the pending batches of current batch
		$pending = $pending_batches - 1;
		$sql = " UPDATE ".TABLE_TICKET_ALL_BATCHES." SET 
			".TABLE_TICKET_ALL_BATCHES.".pending_batches= ".$pending." 
			WHERE ".TABLE_TICKET_ALL_BATCHES.".id='".$batch_id."'";
		 $db->query($sql);
		 if($pending<=0){
			$sqlUpdate = " UPDATE ".TABLE_TICKET_ALL." SET 
			".TABLE_TICKET_ALL.".status='".SupportTicket::STALL_COMPLETED."'
			WHERE ".TABLE_TICKET_ALL.".ticket_id = ".$ticket_id ;
			$db->query($sqlUpdate);
		 }else{
			$sqlUpdate = " UPDATE ".TABLE_TICKET_ALL." SET 
			".TABLE_TICKET_ALL.".status='".SupportTicket::STALL_INPROCESS."'
			WHERE ".TABLE_TICKET_ALL.".ticket_id = ".$ticket_id ;
			$db->query($sqlUpdate);
		 }
		
    }
    $sql="DELETE FROM ".TABLE_TICKET_ALL_BATCHES." WHERE ".TABLE_TICKET_ALL_BATCHES.".pending_batches=0" ;
    $db->query($sql);
	

ob_end_flush();
$db->close();
$db_new->close();
$db = null;
$db_new = null;
$s = null;
$messages=null; 
$datetime =null; 
?>
