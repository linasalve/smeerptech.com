<?php  
    if (!defined( "THIS_DOMAIN")) {
        //include('/var/www/html/leena/smeerptech.com/smeerptech.com/lib/config.php');
		include ('/home/httpd/vhosts/smeerptech.com/httpdocs/lib/config.php');	
	}

    $db 		= new db_local; // database handle
	$s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class.
	
	//putenv("TZ=GMT"); // done in config.php

	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true;
    
    //include_once ( DIR_FS_INCLUDES .'/vendors-bank.inc.php'); 
	include_once ( DIR_FS_INCLUDES .'/clients.inc.php');    
    include_once ( DIR_FS_INCLUDES .'/bank-ticket.inc.php');        
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
    include_once ( DIR_FS_INCLUDES ."/html2text.inc.php" );
    include_once (DIR_FS_INCLUDES .'/imap.inc.php');
    
    $completeMailList= $emailArray = $idArray = $diffEmails =array();
  
    //$mailBox = imap_open("{192.168.1.68:143/novalidate-cert}INBOX", "test@smeerptech.com", "malviya")
    //$mailBox = imap_open("{192.168.100.40:143/novalidate-cert}INBOX", "accounts@smeerptech.com", "DFKJ#@%3487dfhj")
	$mailBox = imap_open("{imap.emailsrvr.com:143/novalidate-cert}INBOX", "accountsimap@smeerptech.com", "hongkong3")
    or die("can't connect: " . imap_last_error());
    $check = imap_mailboxmsginfo($mailBox);
    $totalNumOfMsg = $check->Nmsgs;
   
    $numOfMess = $totalNumOfMsg ;
    for($msgno=1; $msgno<=$numOfMess;$msgno++){
        $emailList = array();
        $header = imap_headerinfo($mailBox, $msgno, 80, 80);
       
        //if($header->Unseen =='U'){        
            $msgBody = retrieve_message($mailBox, $msgno);  
            
			 
            //$email = $msgBody['from_email'] ;
			$email = strtolower(trim($msgBody['from_email'])) ;
            $clientDetails = BankTicket::getStClient($db, $email);
            $clientDetails = $clientDetails['0'];          
            //get ticket no bof
            /*
			$pos3 = strpos( $msgBody['subject'],"#") +1;
            $pos4 = strpos( $msgBody['subject'],"::");
            $len  = $pos4 - $pos3;
            $mTicketNo = substr( $msgBody['subject'],$pos3,$len); */
			
			
			$pos3 = strpos( $msgBody['subject'],"#") +1;
            $pos4 = strpos($msgBody['subject'], '#]',$pos3);
            $len  = $pos4 - $pos3;
            $mTicketNo = substr( $msgBody['subject'],$pos3,$len); 
			/******** 
			AS Clients are sending mails to support email id with SALES ticket subject 
			thats why, creating the threads on ticket nos which is a sales ticket no.
			*********/
			$needle = "[ACNT#" ;
			if (strpos($msgBody['subject'],$needle)){
				  
			}else{
				if($mTicketNo >0){
					$mTicketNo =0;
				}
			}
			 
            $mainTicketId =0;
            if($mTicketNo >0){
                $ticketDetails = BankTicket::getTicketDetails($mTicketNo);
                $mainTicketId  = $ticketDetails['ticket_id'];                
                //If ticket status = CLOSED then create new ticket
                if(!empty($ticketDetails['ticket_status']) && $ticketDetails['ticket_status'] == BankTicket::CLOSED ){
                     $mainTicketId  ='';
                }
            }
            //get ticket no eof
            //Check the from email if aryan sir's email the thread will be attach in the given task no BOF
            if( ($email=='aryan@smeerptech.com' || $email=='lina.salve28@gmail.com' || $email=='aryan@smeerptech.com') && $mTicketNo >0 && !empty($ticketDetails) ){
                /***********************************
				SUBJECT => pt[ACNT#3232#] - subject
				************************************/
				//$pos1 = strpos( $msgBody['subject'],":") +1;
                $pos2 = strpos( $msgBody['subject'],"[");
                $pos1 = $pos2 - 2; //pos before 2 characters of #
				$len  = $pos2 - $pos1;
                $statusName = strtolower(trim(substr( $msgBody['subject'],$pos1,$len)));
				$statusName = trim( $statusName);
                $sendmailclient = 0;
                $ticket_status = BankTicket::PENDINGWITHSMEERP;
                
                if($statusName=='pt'){
                   $ticket_status = BankTicket::PENDINGWITHSMEERP;
                   $sendmailclient = 0;
                }
                if($statusName=='pp'){
                   $ticket_status = BankTicket::PENDINGWITHSMEERP;
                   $sendmailclient = 1;
                }
                if($statusName=='pc'){
                   $ticket_status = BankTicket::PENDINGWITHCLIENTS;
                   $sendmailclient = 1;
                }
                
                $my['uid']    = 'e68adc58f2062f58802e4cdcfec0af2d';
                $my['f_name'] ='Aryan';
                $my['l_name'] ='Salve';
                $clientDetails['mainClient']='';
                $data['ticket_subject'] = $msgBody['subject'];
                $data['ticket_text']   =   $msgBody['body'];
                
                $ticket_no  =  BankTicket::getNewNumber($db);               
                $ticket_id = $ticketDetails['ticket_id'];
                $data['ticket_owner_uid'] = $ticketDetails['ticket_owner_uid'];
                $data['ticket_owner'] = $ticketDetails['ticket_owner'];
                $data['ticket_creator_uid'] = $ticketDetails['ticket_creator_uid'];
                 
                $query = "SELECT "	. TABLE_VENDORS_BANK_TICKETS .".ticket_date, ".TABLE_VENDORS_BANK_TICKETS.".status"
								." FROM ". TABLE_VENDORS_BANK_TICKETS 
								." WHERE ". TABLE_VENDORS_BANK_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
								." AND (". TABLE_VENDORS_BANK_TICKETS .".ticket_child = '". $ticket_id ."' " 
											." OR "
											. TABLE_VENDORS_BANK_TICKETS .".ticket_id = '". $ticket_id ."' " 
										.") "
								." ORDER BY ". TABLE_VENDORS_BANK_TICKETS .".ticket_date DESC LIMIT 0,1"; 
				
				$db->query($query) ;
				if($db->nf() > 0){
					$db->next_record() ;
					$last_time = $db->f("ticket_date") ;
					$ticket_response_time = time() - $last_time ;
					$status = $db->f("status") ;
				}
				else{
					$ticket_response_time = 0 ;
				}
				$ticket_date 	= time() ;
				$ticket_replied = '0' ;
                                  
                 
                 $query = "INSERT INTO ". TABLE_VENDORS_BANK_TICKETS ." SET "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_subject       = '". $data['ticket_subject'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_status     = '".$ticket_status."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_child      = '".$ticket_id."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_replied    = '".$ticket_replied."', "
							. TABLE_VENDORS_BANK_TICKETS .".from_admin_panel  = '".BankTicket::ADMIN_PANEL."', "
							. TABLE_VENDORS_BANK_TICKETS .".last_comment_from = '".BankTicket::ADMIN_COMMENT."', "
							. TABLE_VENDORS_BANK_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_VENDORS_BANK_TICKETS .".do_e              = '".date('Y-m-d H:i:s', time())."', "
							. TABLE_VENDORS_BANK_TICKETS .".do_comment        = '".date('Y-m-d H:i:s', time())."', "
						   . TABLE_VENDORS_BANK_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
               $variables['hid']= $db->query($query) ;
                
                
                $query_update = "UPDATE ". TABLE_VENDORS_BANK_TICKETS 
								." SET ". TABLE_VENDORS_BANK_TICKETS .".ticket_status = '".$ticket_status."'"                               
                                    .",".TABLE_VENDORS_BANK_TICKETS.".do_comment = '".date('Y-m-d H:i:s', time())."'"
                                    .",".TABLE_VENDORS_BANK_TICKETS.".last_comment_from = '".BankTicket::ADMIN_COMMENT."'"
                                    .",".TABLE_VENDORS_BANK_TICKETS.".last_comment = '".$data['ticket_text']."'"
                                    .",".TABLE_VENDORS_BANK_TICKETS.".last_comment_by = '".$my['uid'] ."'"
                                    .",".TABLE_VENDORS_BANK_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
                                    ." WHERE ". TABLE_VENDORS_BANK_TICKETS .".ticket_id = '". $ticket_id ."' " ;
                $db->query($query_update) ;
                
                $data['replied_by']   =   $my['f_name']." ". $my['l_name'] ;
                $data['subject']    =  $ticketDetails['ticket_subject'] ;
                $data['text']    =   $data['ticket_text'] ;                  
               //$data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $ticket_id;
                $data['ticket_no']    =   $ticket_no ;
                $data['mticket_no']   =   $mTicketNo ;
                
                if($sendmailclient ==1){
                 
                    Clients::getList($db, $clientDetails, 'number, f_name, l_name, email,email_1,email_2,email_3,email_4,
                        additional_email,title as ctitle,billing_name,mobile1,mobile2', " WHERE user_id = '".$data['ticket_owner_uid']."'");
                    $clientDetails=$clientDetails['0']; 
                   
                    if(!empty($clientDetails)){
                        
                        $data['client_name']    =   $clientDetails['f_name']." ".$clientDetails['l_name'];
                        $data['ctitle']    =   $clientDetails['ctitle'];
                        $email = NULL;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_MEMBER', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $clientDetails['email'],'email' => $clientDetails['email']);
                            $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            $mail_send_to_su .= ",".$clientDetails['email'];
                            
							/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                $mail_send_to_su .= ",".$clientDetails['email'];
                            } */
                            //Send mail on alternate emails ids bof
                            if(!empty($clientDetails['email_1'])){                                       
                                //$to = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $clientDetails['email_1'], 'email' =>
                                $clientDetails['email_1']);                                   
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                $mail_send_to_su  .= ",".$clientDetails['email_1'];
                                /*  
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
                            }
                            if(!empty($clientDetails['email_2'])){                                       
                                //$to = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $clientDetails['email_2'], 'email' =>
                                $clientDetails['email_2']);                                   
                                $from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);
                                $mail_send_to_su  .= ",".$clientDetails['email_2'];
                               /*  SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
                            }
                            if(!empty($clientDetails['email_3'])){                                       
                                //$to = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $clientDetails['email_3'], 
								'email' => $clientDetails['email_3']);                                   
                                $from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);
                                $mail_send_to_su  .= ",".$clientDetails['email_3'];
                                /* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
                            }
                            if(!empty($clientDetails['email_4'])){                                       
                                //$to = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $userDetails['email_4'], 'email' =>
                                $userDetails['email_4']);                                   
                                $from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);
                                $mail_send_to_su  .= ",".$clientDetails['email_4'];
                                /*  
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
                            }
                        }

                    }
                    
                    if($ticketDetails['last_comment_from']==BankTicket::CLIENT_COMMENT){
                        if($data['ticket_owner_uid'] != $data['ticket_creator_uid'] ){
                             Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', " WHERE parent_id = '".$data['ticket_owner_uid']."' AND user_id IN('".$data['ticket_creator_uid']."') AND status='".BankTicket::ACTIVE."' ORDER BY f_name");                        
                            if(!empty($subUserDetails)){
                                
                                foreach ($subUserDetails  as $key=>$value){
                                    $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                    $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                    $email = NULL;
                                    $cc_to_sender= $cc = $bcc = Null;
                                    if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_MEMBER', $data, $email) ) {
                                        //$to = '';
                                        $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                        '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
										/* 
										if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                        $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                        } */
                                        if(!empty($subUserDetails[$key]['email_1'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_1'], 
											'email' => $subUserDetails[$key]['email_1']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                           
                                            
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                            } */
                                        } 
                                        if(!empty($subUserDetails[$key]['email_2'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_2'], 
											'email' => $subUserDetails[$key]['email_2']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                            } */
                                        }
                                        if(!empty($subUserDetails[$key]['email_3'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_3'], 
											'email' => $subUserDetails[$key]['email_3']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                            } */
                                        }
										
                                        if(!empty($subUserDetails[$key]['email_4'])){
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_4'], 
											'email' => $subUserDetails[$key]['email_4']);
                                            $from = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                            
											$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                
                                            } */
                                        }
                                        
                                    }                      
                                }
                            }
                        }
                    }
					
                    if(!empty($mail_send_to_su)){
						$bcc[]   = array('name' => '', 
							'email' => $smeerp_client_email);
					    SendMail($to, $from, $reply_to, $email["subject"], $email["body"],$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
					
                        //Set the list subuser's email-id to whom the mails are sent bof                    
                        $query_update = "UPDATE ". TABLE_VENDORS_BANK_TICKETS 
                        ." SET ". TABLE_VENDORS_BANK_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."' 
						 WHERE ". TABLE_VENDORS_BANK_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
                        $db->query($query_update) ;
                    }
                }
                imap_delete($mailBox, $msgno);
            }
            //Check the from email if aryan sir's email the thread will be attach in the given task no EOF
           
            if(!empty($clientDetails['mainClient'])){
             $clientname=$clientDetails['mainClient']['0']['f_name']." ".$clientDetails['mainClient']['0']['l_name'];
                $billing_name =  $clientDetails['mainClient']['0']['billing_name'] ;
                $data['main_client_name'] = $clientname;
                $data['billing_name'] = $billing_name;
                
                if(!empty($clientDetails['subClient'])){
                    $ticket_owner_uid = $clientDetails['mainClient']['0']['user_id'];
                    $ticket_owner = $clientDetails['mainClient']['0']['f_name']." ".$clientDetails['mainClient']['0']['l_name'];
                    $ticket_creator_uid = $clientDetails['subClient']['0']['user_id'];
                    $ticket_creator = $clientDetails['subClient']['0']['f_name']." ".$clientDetails['subClient']['0']['l_name'];
                }else{
                    $ticket_owner_uid = $ticket_creator_uid = $clientDetails['mainClient']['0']['user_id'];
                    $ticket_owner = $ticket_creator = $clientDetails['mainClient']['0']['f_name']." ".$clientDetails['mainClient']['0']['l_name'];
                }               
                $ticket_no = BankTicket::getNewNumber($db);
                $ticket_subject = $msgBody['subject'];
                $ticket_text  =   $msgBody['body'];
                $headers_imap  =  $msgBody['headers_imap'];
                $email_subject =  $msgBody['subject'];
                $email_body =     $msgBody['body'];
                $to_email =       $msgBody['to_email'];
                $to_email_1 =     $msgBody['to_email_1'];
                $cc_email =       $msgBody['cc_email'];
                $cc_email_1 =     $msgBody['cc_email_1'];
                $bcc_email =      $msgBody['bcc_email'];
                $bcc_email_1 =    $msgBody['bcc_email_1'];
                $fromaddress =    $msgBody['from_email'];
                $udate =          $msgBody['udate'];
                
                //$email_subject  =  "[ACNT#".$ticket_no."#] -".$msgBody['subject'];          
                $data = array();
                $data['isimap'] = '1';
                //$data['mysmeerplink'] = DIR_WS_MP."/support-ticket-request.php?perform=add";
                
                if(!empty($mainTicketId) && $mainTicketId >0){
                    $ticket_subject =$email_subject = $ticketDetails['ticket_subject'];
                    //Calculate responce time BOF
                    $query1 = "SELECT "	. TABLE_VENDORS_BANK_TICKETS .".ticket_date "
                    ." FROM ". TABLE_VENDORS_BANK_TICKETS 
                    ." WHERE ". TABLE_VENDORS_BANK_TICKETS .".ticket_owner_uid = '". $ticket_owner_uid ."' " 
                    ." AND ("
                                . TABLE_VENDORS_BANK_TICKETS .".ticket_child = '". $mainTicketId ."' " 
                                ." OR "
                                . TABLE_VENDORS_BANK_TICKETS .".ticket_id = '". $mainTicketId ."' " 
                            .") "
                    ." ORDER BY ". TABLE_VENDORS_BANK_TICKETS .".ticket_date DESC LIMIT 0,1";	
                    $db->query($query1) ;
                    if($db->nf() > 0){
                        $db->next_record() ;
                        $last_time = $db->f("ticket_date") ;
                        $ticket_response_time = time() - $last_time ;                      
                    }
                    else{
                        $ticket_response_time = 0 ;
                    }
                    //Calculate responce time EOF      
                    
                    $data['showautomsg'] = '0';
                    $data['mticket_no'] = $mTicketNo;
                    //post comment
                   $query = "INSERT INTO "	. TABLE_VENDORS_BANK_TICKETS ." SET "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_no         = '". $ticket_no ."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_owner_uid  = '". $ticket_owner_uid ."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_owner      = '". processUserData($ticket_owner )."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_creator_uid= '". $ticket_creator_uid ."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_creator    = '". processUserData($ticket_creator) ."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_subject    = '". processUserData($ticket_subject)."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_text       = '". processUserData($ticket_text) ."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_department = '', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_priority   = '', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_status     = '".BankTicket::PENDINGWITHSMEERP."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_child      = '".$mainTicketId."', "
					//. TABLE_VENDORS_BANK_TICKETS .".ticket_attachment = '". $attachfilename ."', "
					//. TABLE_VENDORS_BANK_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_replied       = '0', "
					. TABLE_VENDORS_BANK_TICKETS .".domain_name          = '', "
					. TABLE_VENDORS_BANK_TICKETS .".is_private           = '0', "
					. TABLE_VENDORS_BANK_TICKETS .".using_imap           = '1', "
					. TABLE_VENDORS_BANK_TICKETS .".headers_imap       = '". processUserData($headers_imap )."', "
					. TABLE_VENDORS_BANK_TICKETS .".to_email           = '". processUserData($to_email )."', "
					. TABLE_VENDORS_BANK_TICKETS .".cc_email           = '". processUserData($cc_email)."', "
					. TABLE_VENDORS_BANK_TICKETS .".bcc_email          = '". processUserData($bcc_email)."', "
					. TABLE_VENDORS_BANK_TICKETS .".to_email_1         = '".processUserData($to_email_1)."', "
					. TABLE_VENDORS_BANK_TICKETS .".cc_email_1         = '".processUserData($cc_email_1)."', "
					. TABLE_VENDORS_BANK_TICKETS .".bcc_email_1        = '".processUserData($bcc_email_1)."', "
					. TABLE_VENDORS_BANK_TICKETS .".from_email        = '".processUserData($fromaddress)."', "
					. TABLE_VENDORS_BANK_TICKETS .".last_comment_from    = '".BankTicket::CLIENT_COMMENT."', " 
					. TABLE_VENDORS_BANK_TICKETS .".status           = '". BankTicket::DEACTIVE ."', "
					. TABLE_VENDORS_BANK_TICKETS .".ip    = '', "
					. TABLE_VENDORS_BANK_TICKETS .".do_e    = '".date('Y-m-d H:i:s', time())."', "
					//. TABLE_VENDORS_BANK_TICKETS .".do_comment    = '".date('Y-m-d H:i:s',time())."', "
					. TABLE_VENDORS_BANK_TICKETS .".ticket_date       = '". $udate ."' " ;
                }else{
                       
                        $data['showautomsg'] = '1';
                        $data['mticket_no'] = $ticket_no;
                        //post main ticket
                        $query = "INSERT INTO "	. TABLE_VENDORS_BANK_TICKETS ." SET "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_no         = '". $ticket_no ."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_owner_uid  = '". $ticket_owner_uid ."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_owner      = '". processUserData($ticket_owner) ."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_creator_uid= '". $ticket_creator_uid ."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_creator   = '". processUserData($ticket_creator) ."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_subject    = '". processUserData($ticket_subject)."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_mail_subject    = '". processUserData($ticket_subject)."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_text       = '". processUserData($ticket_text )."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_department = '', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_priority   = '', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_status     = '".BankTicket::PENDINGWITHSMEERP."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_child      = '0', "
						//. TABLE_VENDORS_BANK_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						//. TABLE_VENDORS_BANK_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_response_time = '0', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_replied        = '0', "
						. TABLE_VENDORS_BANK_TICKETS .".domain_name           = '', "
						. TABLE_VENDORS_BANK_TICKETS .".is_private           = '0', "
						. TABLE_VENDORS_BANK_TICKETS .".using_imap           = '1', "
						. TABLE_VENDORS_BANK_TICKETS .".headers_imap       = '". processUserData($headers_imap )."', "
						. TABLE_VENDORS_BANK_TICKETS .".to_email           = '". processUserData($to_email )."', "
						. TABLE_VENDORS_BANK_TICKETS .".cc_email           = '". processUserData($cc_email)."', "
						. TABLE_VENDORS_BANK_TICKETS .".bcc_email          = '". processUserData($bcc_email)."', "
						. TABLE_VENDORS_BANK_TICKETS .".to_email_1         = '".processUserData($to_email_1)."', "
						. TABLE_VENDORS_BANK_TICKETS .".cc_email_1         = '".processUserData($cc_email_1)."', "
						. TABLE_VENDORS_BANK_TICKETS .".bcc_email_1        = '".processUserData($bcc_email_1)."', "
						. TABLE_VENDORS_BANK_TICKETS .".from_email        = '".processUserData($fromaddress)."', "
						. TABLE_VENDORS_BANK_TICKETS .".last_comment_from    = '".BankTicket::CLIENT_COMMENT."', "
						. TABLE_VENDORS_BANK_TICKETS.".last_comment = '".processUserData($ticket_text )."', "
						. TABLE_VENDORS_BANK_TICKETS .".status           = '". BankTicket::DEACTIVE ."', "
						. TABLE_VENDORS_BANK_TICKETS .".ip    = '', "
						. TABLE_VENDORS_BANK_TICKETS .".do_e    = '".date('Y-m-d H:i:s', time())."', "					 
						. TABLE_VENDORS_BANK_TICKETS .".do_comment  = '".date('Y-m-d H:i:s',$udate)."', "
						. TABLE_VENDORS_BANK_TICKETS .".ticket_date = '".$udate ."' " ;
                    }
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        
                        $variables['hid'] = $db->last_inserted_id() ;
                        
                        if(!empty($mainTicketId) && $mainTicketId >0){
                            $data['ticket_id'] = $mainTicketId;
                            //Mark pending towards client bof
                            $query_update = "UPDATE ". TABLE_VENDORS_BANK_TICKETS 
								." SET ". TABLE_VENDORS_BANK_TICKETS .".ticket_status = '".BankTicket::PENDINGWITHSMEERP."'"
								.",".TABLE_VENDORS_BANK_TICKETS.".do_comment= '".date('Y-m-d H:i:s', $udate)."'"
								.",".TABLE_VENDORS_BANK_TICKETS.".last_comment_from= '".BankTicket::CLIENT_COMMENT."'"
								.",".TABLE_VENDORS_BANK_TICKETS.".last_comment = '".processUserData($ticket_text )."'"
								." WHERE ". TABLE_VENDORS_BANK_TICKETS .".ticket_id = '". $mainTicketId ."' " ;
                            $db->query($query_update) ;
                            //Mark pending towards client eof                            
                        }else{
                            $data['ticket_id'] =  $variables['hid'];
                        }
                        //BOF ATTACHMENT BOF
                        $filestore = DIR_FS_BT_FILES."/" ;
                        $i=100;
                        $fileNameStr ='';
                        if(!empty($msgBody['attachments'])){
                            if(!empty($msgBody['attachments'])){
                                foreach($msgBody['attachments'] as $key=>$val){
                                    $filedata["extension"]='';
                                    $filedata = pathinfo($key);                   
                                    $ext = $filedata["extension"] ; 
                                    if(!empty($ext)){
                                        $filename = $ticket_no."-".$variables['hid']."-".$i.".".$ext ;
                                        $fileNameStr .=$filename."," ;
                                        $fp=fopen($filestore.$filename,"w+");
                                        fwrite($fp,$val);
                                         @chmod($filestore.$filename,0777);
                                        fclose($fp);
                                        $i++;
                                    }
                                }                             
                                $fileNameStr = trim($fileNameStr,",") ;
                            }
                            $sql="UPDATE ".TABLE_VENDORS_BANK_TICKETS." SET ".TABLE_VENDORS_BANK_TICKETS.".attachment_imap='".$fileNameStr."' WHERE ticket_id=".$variables['hid'];
                            $db->query($sql) ;
                        }
                        //EOF ATTACHMENT EOF
                        // **************************************************************************
                        //* Send the notification to the ticket owner and the Administrators
                        //* of the new Ticket being created
                        //************************************************************************** 
                        
                        $data['ticket_no']    =   $ticket_no ;                       
                        $data['status']       = "";                
                        $data['replied_by']   =   $ticket_owner;
                        $data['subject']      =   $ticket_subject;
                        $data['text']         =   $ticket_text;
                        //$data['attachment']   =   $attachfilename ;
                        //$data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $data['ticket_id'];
                        /*
                        // Send Email to the ticket owner BOF  
                        if(!empty($clientDetails['mainClient'])){
                            $userDetails=$clientDetails['mainClient']['0'];
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    = $userDetails['title'];
                            $email = NULL;
                           
                            if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' => $userDetails['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                //echo $email["body"];
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }  
                        }
                        // Send Email to the ticket owner EOF             
                        // Send Email to the Client sub users involved BOF              
                        if(!empty($clientDetails['subClient'])){
                            $userDetails=$clientDetails['subClient']['0'];
                            $data['replied_by']   =    $userDetails['f_name']." ".$userDetails['l_name'] ;
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    = $userDetails['title'];
                            $email = NULL;
                            
                            if ( getParsedEmail($db, $s, 'EMAIL_BKT_MEMBER', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' => $userDetails['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                //echo $email["body"];
                                SendMail($to, $from, $reply_to, $email_subject, $email["body"], $email["isHTML"]);
                            }
                                       
                        }*/
                        // Send Email to the sub users involved EOF
                        
                         // Send Email to the admin BOF
						/*
                        $email = NULL;
                        $data['link']   = DIR_WS_NC .'/bank-ticket.php?perform=view&ticket_id='.$data['ticket_id'];                       
                        if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_ADMIN', $data, $email) ) {
                            $to = $bcc = '';
                            $to[]   = array('name' => $bank_purchase_name , 'email' => $bank_purchase_email);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            //echo $email["body"];
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }      
						*/
                        // Send Email to the admin EOF   
						//Mark delete to the emails after create the ST
						imap_delete($mailBox, $msgno);
                    }                
                
               
            }else{
                //if sender is not found in client list not found         
                // Send Email to the admin BOF
                /*
                $email = NULL;
                $data['link']   = DIR_WS_NC .'/bank-ticket.php?perform=view&ticket_id='.$data['ticket_id'];
                $data['from_email']=$msgBody['from_email'] ;
                $data['email_subject']=$msgBody['subject'] ;
                $data['email_body']=$msgBody['body'] ;          
                if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_ST_ADMIN_IMAP_NOT_MEMBER', $data, $email) ) {
                    $to = '';
                    $to[]   = array('name' => $support_name , 'email' => $support_email);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                    
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                }    */                    
                // Send Email to the admin EOF   
            }
           
        //}
        
}
imap_expunge($mailBox);
//imap_close which will silently expunge the mailbox before closing, removing all messages marked for deletion.
imap_close($mailBox);
imap_gc($mailBox, IMAP_GC_ELT);
$completeMailList= $emailArray = $idArray = $diffEmails =$emailList =$data=$msgBody=null;
$numOfMess = $totalNumOfMsg =$email =  $clientDetails = $pos3 = $pos4 = $len  = $mTicketNo =$mainTicketId = $ticketDetails= null;
$pos2 = $pos1 = $len  = $statusName= $sendmailclient=$ticket_status=$clientname=$billing_name= $ticket_id=$ticketDetails=$data=$last_time =$ticket_response_time =$status =$ticket_date = $query = $query_update =$sendmailclient =  $from=$reply_to=$mail_send_to_su = null;
$to = $cc_to_sender=$cc=$bcc=$smeerp_client_email=null;
$subUserDetails = $main_client_name =   $ticket_owner_uid=$ticket_owner =$ticket_creator_uid=$ticket_creator =$ticket_no =  $ticket_subject =  $ticket_text  =  $headers_imap  = $email_subject =  $email_body =$to_email = $to_email_1 =$cc_email = $cc_email_1 = $bcc_email = $bcc_email_1 =  $fromaddress = $udate = null;  
$filestore =$i= $fileNameStr =$msgBody =$userDetails= null;

ob_end_flush();
$db->close();
$db = null;
$s = null;
$messages=null; 
$datetime =null;
?>