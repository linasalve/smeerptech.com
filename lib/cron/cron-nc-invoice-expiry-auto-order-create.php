<?php  
    if (!defined( "THIS_DOMAIN")) {
       //include('/var/www/html/leena/smeerptech.com/smeerptech.com/lib/config.php');
		include ('/home/httpd/vhosts/smeerptech.com/httpdocs/lib/config.php');	
	}
	$db 		= new db_local; // database handle
	$s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class.
	
	//putenv("TZ=GMT"); // done in config.php

	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true;
    
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');        
    include_once ( DIR_FS_INCLUDES .'/bill-invoice-proforma.inc.php');        
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');        
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');  
   
   
  
    $db1 = new db_local; // database handle    
    //$date = date('Y-m-d', strtotime("+89 days"));
    $date = date('Y-m-d', strtotime("+119 days"));
  
    $sql ="SELECT DISTINCT(".TABLE_INV_REMINDER_CRON.".pinv_id),"	
		.TABLE_INV_REMINDER_CRON.".client_id,".TABLE_INV_REMINDER_CRON.".pinv_no,"
		.TABLE_INV_REMINDER_CRON.".do_expiry,".TABLE_BILL_INV_PROFORMA.".or_no 
		FROM ".TABLE_INV_REMINDER_CRON." LEFT JOIN
		".TABLE_BILL_INV_PROFORMA." ON ".TABLE_INV_REMINDER_CRON.".pinv_id = ".TABLE_BILL_INV_PROFORMA.".id
        WHERE date_format(".TABLE_INV_REMINDER_CRON.".do_expiry,'%Y-%m-%d') <= '".$date."' AND 
		 ".TABLE_BILL_INV_PROFORMA.".new_ref_ord_id='0' ";
 
	
		
	/* 
	******** If you want to create missed orders due to late invoice creation uncomment me & 
	Comment $sql (line 33-39)****
	********After done comment me again and keep $sql in used*****
	
	$date = date('Y-m-d', strtotime("+89 days"));
	$sql ="SELECT DISTINCT(".TABLE_INV_REMINDER.".pinv_id),"	
	.TABLE_INV_REMINDER.".client_id,".TABLE_INV_REMINDER.".pinv_no,"
	.TABLE_INV_REMINDER.".do_expiry,".TABLE_BILL_INV_PROFORMA.".or_no 
	FROM ".TABLE_INV_REMINDER." LEFT JOIN ".TABLE_BILL_INV_PROFORMA." 
	ON ".TABLE_INV_REMINDER.".pinv_id = ".TABLE_BILL_INV_PROFORMA.".id
	WHERE date_format(".TABLE_INV_REMINDER.".do_expiry,'%Y-%m-%d') <= '".$date."' AND 
	 ".TABLE_BILL_INV_PROFORMA.".new_ref_ord_id='0' "; */
 
   $db->query($sql) ;
   $sr = 0;
   $db1 = new db_local; // database handle
	 
 if ( $db->nf() > 0 ) {  
    while ( $db->next_record()){            
        $client_id = $db->f('client_id') ;
        $inv_id = $db->f('pinv_id') ;
        $inv_no = $db->f('pinv_no') ;
        $or_no = $db->f('or_no') ;
		
		$invDetails=null;
		if(!empty($client_id)){
			$invDetails['client']=$client_id;
			$invDetails['id']=$inv_id;
			$invDetails['number']=$inv_no;
			$invDetails['or_no']=$or_no;
		}
		
        $temp_p=$temp=null;
        if(!empty($invDetails)){
			echo "<br/>";
			
			$condition1 	= " WHERE user_id = '".$invDetails['client']."' ";
			$clientDetails  = null;
			Clients::getList($db1, $clientDetails, 'f_name, l_name, billing_name ', $condition1);					 
			if(!empty($clientDetails)){
				$clientDetails = $clientDetails[0];
				$data['client']['f_name'] = $clientDetails['f_name'];
				$data['client']['l_name'] = $clientDetails['l_name'];
				
			}
			
			//get ord details bof 
			$ord_details_list=$ord_details=array();
			$fields= TABLE_BILL_ORDERS.".* ";
			$condition3 = " WHERE ".TABLE_BILL_ORDERS.".number = '". $invDetails['or_no']."'";
			Order::getList($db1, $ord_details_list, $fields, $condition3);
			if(!empty($ord_details_list)){
				$data=$ord_details = $ord_details_list[0];
			}
			
			//get ord details eof 	
			$temp = array();
			$condition3 = " WHERE ".TABLE_BILL_ORD_P.".ord_no = '".$invDetails['or_no']."'";
			Order::getParticulars($db1, $temp, '*', $condition3);
			
			//If blank then current finansial yr based no generate
		    $data['financialYr']=''; 
		    //$order_number = getOrderCounterNumber($db1, $data['financialYr']);
			$order_number='';
		    if($ord_details['do_o']!='0000-00-00 00:00:00'){				 
				$ord_details['do_o'] = date('Y-m-d h:i:s',strtotime(date("Y-m-d",strtotime($ord_details['do_o'])) . " +1 year"));
				
				$dordArr = explode(' ', $ord_details['do_o']);
				$dordArr1 = explode('-', $dordArr[0]);
				$data['do_o'] = mktime(0, 0, 0, $dordArr1[1], $dordArr1[2], $dordArr1[0]);
				 
				$detailOrdNo = getOrderNumber($data['do_o']); 				
				if(!empty($detailOrdNo)){
					 $data['number'] = $order_number= $detailOrdNo['number'];
					$data['ord_counter'] = $detailOrdNo['ord_counter'];
				}
		    }
			
		    if($ord_details['do_d']!='0000-00-00 00:00:00'){
				$data['do_d']=$ord_details['do_d'] = date('Y-m-d h:i:s',strtotime(date("Y-m-d", strtotime($ord_details['do_d'])) . " +1 year"));
		    }
		    if($ord_details['do_e']!='0000-00-00 00:00:00'){
				$data['do_e'] = $ord_details['do_e'] = date('Y-m-d h:i:s',strtotime(date("Y-m-d", strtotime($ord_details['do_e'])) . " +1 year"));
				
		    }
		    if($ord_details['do_fe']!='0000-00-00 00:00:00'){
				$data['do_fe']=$ord_details['do_fe'] = date('Y-m-d h:i:s',strtotime(date("Y-m-d", strtotime($ord_details['do_fe'])) . " +1 year"));
		    }
		    if($ord_details['st_date']!='0000-00-00 00:00:00'){
				$data['st_date']=$ord_details['st_date'] = date('Y-m-d h:i:s',strtotime(date("Y-m-d", strtotime($ord_details['st_date'])) . " +1 year"));
		    }
			
		    if($ord_details['es_ed_date']!='0000-00-00 00:00:00'){
				$data['es_ed_date']=$ord_details['es_ed_date'] = date('Y-m-d h:i:s',strtotime(date("Y-m-d", strtotime($ord_details['es_ed_date'])) . " +1 year"));
		    }
		    $my['user_id'] = 'dec32cb82fe9bd2ffbec266ab14fdac6'; // provide id of auto generated user
			$my['f_name']  = 'Auto'	  ;
		    $my['l_name']  = 'System' ;
			$team = ',e68adc58f2062f58802e4cdcfec0af2d,'; //default Aryan Sir user id
			$data['added_by'] = $my['f_name']." ".$my['l_name'] ;
			
			//Assign Support team bof
			$sql="SELECT DISTINCT(user_id), f_name,l_name FROM ".TABLE_USER." WHERE ".TABLE_USER.".groups 
				LIKE '%,". User::TP_OSTP.",%' AND ".TABLE_USER.".status='".User::ACTIVE."' ORDER BY RAND() LIMIT 0,3 " ;
			$db1->query($sql);
			$wp_support_team = $wp_support_team_name = $wp_sql = '';
			if ( $db1->nf()>0 ){
				while ( $db1->next_record() ){ 
					$wp_support_team .= $db1->f('user_id').",";
					$wp_support_team_name .= $db1->f('f_name')." ".$db1->f('l_name').",";
				}
				$wp_support_team = trim($wp_support_team,',');
				$wp_support_team = ",".$wp_support_team."," ;
				$wp_support_team_name = trim($wp_support_team_name,',');
				$wp_sql = ",". TABLE_BILL_ORDERS .".wp_support_team  = '".$wp_support_team ."'
						  ,".TABLE_BILL_ORDERS.".wp_support_team_name  = '".$wp_support_team_name ."'" ;
			}
			//Assign Support team eof	  
		    echo  $query	= " INSERT INTO ".TABLE_BILL_ORDERS
			." SET ". TABLE_BILL_ORDERS .".number           = '". $order_number."'"
			.",". TABLE_BILL_ORDERS .".ord_counter  = '". processUserData($data['ord_counter'])."'"
			.",". TABLE_BILL_ORDERS .".company_id       = '". processUserData($ord_details['company_id']) ."'"
			.",". TABLE_BILL_ORDERS .".company_name     = '". processUserData($ord_details['company_name']) ."'"
			.",". TABLE_BILL_ORDERS .".company_prefix   = '". processUserData($ord_details['company_prefix']) ."'"
			.",". TABLE_BILL_ORDERS .".po_id       		= '". processUserData($ord_details['po_id']) ."'"
			.",". TABLE_BILL_ORDERS .".access_level     = '". processUserData($ord_details['access_level']) ."'"
			.",". TABLE_BILL_ORDERS .".client           = '". processUserData($ord_details['client']) ."'"
			.",". TABLE_BILL_ORDERS .".created_by       = '". $my['user_id'] ."'"
			.$wp_sql
			.",". TABLE_BILL_ORDERS .".clients_su       = '". processUserData($ord_details['clients_su'])."'"            
			.",". TABLE_BILL_ORDERS .".team             = '". processUserData($team) ."'"
			.",". TABLE_BILL_ORDERS .".service_id       = '". processUserData($ord_details['service_id']) ."'"
			.",". TABLE_BILL_ORDERS .".currency_id      = '". processUserData($ord_details['currency_id']) ."'"
			.",". TABLE_BILL_ORDERS .".currency_abbr    = '". processUserData($ord_details['currency_abbr']) ."'"
			.",".TABLE_BILL_ORDERS .".currency_name    = '".  processUserData($ord_details['currency_name']) ."'"
			.",". TABLE_BILL_ORDERS .".currency_symbol  = '". processUserData($ord_details['currency_symbol']) ."'"
			.",". TABLE_BILL_ORDERS .".currency_country = '". processUserData($ord_details['currency_country']) ."'"
			.",". TABLE_BILL_ORDERS .".amount           = '". processUserData($ord_details['amount']) ."'"
			.",". TABLE_BILL_ORDERS .".stotal_amount    = '". processUserData($ord_details['stotal_amount']) ."'"
			.",". TABLE_BILL_ORDERS .".round_off     	= '".$ord_details['round_off']."'"
			.",". TABLE_BILL_ORDERS .".round_off_op     = '".$ord_details['round_off_op']."'"
			.",". TABLE_BILL_ORDERS .".old_particulars	= '". processUserData( $ord_details['old_particulars']) ."'"
			.",". TABLE_BILL_ORDERS .".details			= '". processUserData( $ord_details['details']) ."'"
			.",". TABLE_BILL_ORDERS .".do_o             = '". $ord_details['do_o'] ."'"
			.",". TABLE_BILL_ORDERS .".do_c             = '". date('Y-m-d H:i:s') ."'"
			.",". TABLE_BILL_ORDERS .".do_d             = '". $ord_details['do_d'] ."'"
			.",". TABLE_BILL_ORDERS .".do_e             = '". $ord_details['do_e']."'" 
			.",". TABLE_BILL_ORDERS .".do_fe            = '". $ord_details['do_fe']."'"
			.",". TABLE_BILL_ORDERS .".show_period      = '". $ord_details['show_period']."'"
			.",". TABLE_BILL_ORDERS .".order_title	    = '". processUserData( $ord_details['order_title']) ."'"
			.",". TABLE_BILL_ORDERS .".tax_ids	    	= '". processUserData( $ord_details['tax_ids']) ."'"
			.",". TABLE_BILL_ORDERS .".project_manager  = '". processUserData($ord_details['project_manager'])."'"
			.",". TABLE_BILL_ORDERS .".order_domain	    = '". processUserData($ord_details['order_domain']) ."'"
			.",". TABLE_BILL_ORDERS .".existing_client	= '". processUserData($ord_details['existing_client']) ."'"
			.",". TABLE_BILL_ORDERS .".client_s_client	= '". processUserData($ord_details['client_s_client'] )."'"
			.",". TABLE_BILL_ORDERS .".order_type	    = '". processUserData($ord_details['order_type']) ."'"
			.",". TABLE_BILL_ORDERS .".order_closed_by	= '". processUserData($ord_details['order_closed_by'] )."'"
			.",". TABLE_BILL_ORDERS .".source_to        = '". processUserData($ord_details['source_to'] )."'"
			.",". TABLE_BILL_ORDERS .".source_from      = '". processUserData($ord_details['source_from']) ."'"
			.",". TABLE_BILL_ORDERS .".po_filename      = '". processUserData($ord_details['po_filename']) ."'"
			.",". TABLE_BILL_ORDERS .".st_date          = '". $ord_details['st_date']."'"
			.",". TABLE_BILL_ORDERS .".es_ed_date       = '". $ord_details['es_ed_date']."'"
			.",". TABLE_BILL_ORDERS .".es_hrs           = '". $ord_details['es_hrs']."'"
			.",". TABLE_BILL_ORDERS .".delivery_at      = '". $ord_details['delivery_at']."'"
			.",". TABLE_BILL_ORDERS .".is_renewable     = '". $ord_details['is_renewable'] ."'"
			.",". TABLE_BILL_ORDERS .".is_renewal_ord   = '". $ord_details['is_renewal_ord'] ."'"
			.",". TABLE_BILL_ORDERS .".status           = '". Order::ACTIVE ."'" 
			.",". TABLE_BILL_ORDERS .".order_against_id = '". $ord_details['id'] ."'"
			.",". TABLE_BILL_ORDERS .".order_against_no = '". $ord_details['number'] ."'"
			.",". TABLE_BILL_ORDERS .".invoice_profm_against_id = '". $invDetails['id'] ."'"
			.",". TABLE_BILL_ORDERS .".invoice_profm_against_no = '". $invDetails['number'] ."'"
			.",". TABLE_BILL_ORDERS .".cron_created     = '1' "
			.",". TABLE_BILL_ORDERS .".ip               = '".$_SERVER['REMOTE_ADDR']."'" ;
 
			 echo "<br/>";
			$db1->query($query);
			$variables['hid'] = $db1->last_inserted_id();
			
			//updateOrderCounterNumber($db1, $data['financialYr']);
			$data['query_p']=$particulars='';
			if(!empty($temp)){

				echo $data['query_p'] = 'INSERT INTO '. TABLE_BILL_ORD_P .' (ord_no, particulars, p_amount, ss_div_id,
				s_id, sub_s_id, s_type, s_quantity, s_amount, ss_title, ss_punch_line,tax1_id,tax1_number, 
				tax1_name,tax1_value, tax1_pvalue,tax1_amount,tax1_sub1_id, tax1_sub1_number,tax1_sub1_name, 
				tax1_sub1_value,tax1_sub1_pvalue,tax1_sub1_amount,tax1_sub2_id,tax1_sub2_number, tax1_sub2_name, 
				tax1_sub2_value,tax1_sub2_pvalue,tax1_sub2_amount,d_amount, stot_amount, tot_amount,discount_type, 
				pp_amount,vendor,vendor_name, purchase_particulars, is_renewable, do_e, do_fe) VALUES ';
				
				$particulars .="<table cellpadding='0' cellspacing='0' border='1' width='100%'>" ;
				foreach ( $temp as $pKey => $parti ) {                                  
					$temp_p[]= array(                                       
								'particulars'=>nl2br($temp[$pKey]['particulars']),
								'ss_punch_line' =>nl2br($temp[$pKey]['ss_punch_line'])
								);	
					$do_e='';
					$do_fe='';
					if($temp[$pKey]['do_e'] !='0000-00-00 00:00:00'){
						$do_e = date('Y-m-d h:i:s',strtotime(date("Y-m-d", strtotime($temp[$pKey]['do_e'])). " +1 year"));
					}
					if($temp[$pKey]['do_fe'] !='0000-00-00 00:00:00'){
						$do_fe = date('Y-m-d h:i:s',strtotime(date("Y-m-d", strtotime($temp[$pKey]['do_fe'])). " +1 year"));
					}
					
					$data['query_p'] .= "('". $order_number ."', 
							   '". processUserData(trim($temp[$pKey]['particulars'],',')) ."', 
							   '". $temp[$pKey]['p_amount'] ."',
							   '". $temp[$pKey]['ss_div_id']  ."',
							   '". $temp[$pKey]['s_id']."',
							   '". $temp[$pKey]['sub_s_id']."',
                               '". processUserData( $temp[$pKey]['s_type']) ."', 
							   '". processUserData($temp[$pKey]['s_quantity']) ."',
							   '". processUserData($temp[$pKey]['s_amount']) ."',
                               '". processUserData($temp[$pKey]['ss_title']) ."', 
							   '". processUserData($temp[$pKey]['ss_punch_line']) ."',
                               '". processUserData($temp[$pKey]['tax1_id']) ."',
							   '". processUserData($temp[$pKey]['tax1_number']) ."',  
                               '". processUserData($temp[$pKey]['tax1_name']) ."', 
							   '". processUserData($temp[$pKey]['tax1_value']) ."',
							   '". processUserData($temp[$pKey]['tax1_pvalue']) ."',
							   '". processUserData($temp[$pKey]['tax1_amount']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub1_id']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub1_number']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub1_name']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub1_value']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub1_pvalue']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub1_amount']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub2_id']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub2_number']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub2_name']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub2_value']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub2_pvalue']) ."',
							   '". processUserData($temp[$pKey]['tax1_sub2_amount']) ."',
                               '". processUserData($temp[$pKey]['d_amount']) ."', 
							   '". processUserData($temp[$pKey]['stot_amount']) ."',
                               '". processUserData($temp[$pKey]['tot_amount']) ."', 
                               '". processUserData($temp[$pKey]['discount_type']) ."', 
							   '". processUserData($temp[$pKey]['pp_amount']) ."',
                               '". processUserData($temp[$pKey]['vendor']) ."', 
							   '". processUserData($temp[$pKey]['vendor_name'] ) ."',
							   '". processUserData($temp[$pKey]['purchase_particulars']) ."',
                               '". processUserData($temp[$pKey]['is_renewable']) ."', 
							   '". $do_e ."','".$do_fe."'
                                        )," ;
										
							$ss_title =  $temp[$pKey]['ss_title'] ;
							$ss_punch_line =  $temp[$pKey]['ss_punch_line'] ;
							$p_amount =  $temp[$pKey]['p_amount'] ;
							$s_type =  $temp[$pKey]['s_type'] ;
							$s_quantity =  $temp[$pKey]['s_quantity'] ;
							$samount =  $temp[$pKey]['samount'] ;
							$discount_type =  $temp[$pKey]['discount_type'] ; 
							$stot_amount =  $temp[$pKey]['stot_amount'] ; 
							$tax1_name =  $temp[$pKey]['tax1_name'] ; 
							$tax1_value =  $temp[$pKey]['tax1_value'] ; 
							$tax1_amount =  $temp[$pKey]['tax1_amount'] ; 
							$tax1_sub1_name =  $temp[$pKey]['tax1_sub1_name'] ; 
							$tax1_sub1_value =  $temp[$pKey]['tax1_sub1_value'] ; 
							$tax1_sub1_amount =  $temp[$pKey]['tax1_sub1_amount'] ; 
							$tax1_sub2_name =  $temp[$pKey]['tax1_sub2_name'] ; 
							$tax1_sub2_value =  $temp[$pKey]['tax1_sub2_value'] ; 
							$tax1_sub2_amount =  $temp[$pKey]['tax1_sub2_amount'] ; 
							$d_amount =  $temp[$pKey]['d_amount'] ; 
							$stot_amount =  $temp[$pKey]['stot_amount'] ; 
							$tot_amount =  $temp[$pKey]['tot_amount'] ; 
							$discount_type =  $temp[$pKey]['discount_type'] ; 
							$particulars .="
							<tr>
								<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'><b>".$sr."</b> Punchline : ".$temp[$pKey]['ss_punch_line'] ." - Service type  : ".$service_type  ."</td/>
							</tr>
							<tr>
								<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Services : ".$ss_title ."</td/>
							</tr>
							<tr>
								<td> 
									<table border='0'>
										<tr>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Price</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Type</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Qty</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Amt</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Discount</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>DiscType</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Sub Tot</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Taxname</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tax%</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tax Amt</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tot Amt</td>
										</tr>
										<tr>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$p_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$s_type."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$s_quantity."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$samount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$d_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$discount_type."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$stot_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_name."<br/>".$tax1_sub1_name."<br/>".$tax1_sub2_name."
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_value."<br/>".$tax1_sub1_value."<br/>".$tax1_sub2_value."
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_amount."<br/>".$tax1_sub1_amount."<br/>".$tax1_sub2_amount."
											
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tot_amount."</td>
										</tr>
									</table>
								</td/>
							</tr>
							" ;
					
				}
				$particulars .= "</table>";
				if(!empty($data['query_p'])){
					$data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
					if ( !$db1->query($data['query_p']) ) {
						$messages->setErrorMessage("The Particulars were not Saved.");
					}
				}
			}
			//Update old order and inv
			echo $sql_update = "UPDATE ".TABLE_BILL_ORDERS." SET 
				".TABLE_BILL_ORDERS.".new_ref_ord_id=".$variables['hid'].",
				".TABLE_BILL_ORDERS.".new_ref_ord_no='".$order_number."' WHERE id=".$ord_details['id']  ;
			$db1->query($sql_update);
			
			echo $sql_update = "UPDATE ".TABLE_BILL_INV_PROFORMA." SET 
				".TABLE_BILL_INV_PROFORMA.".new_ref_ord_id=".$variables['hid'].",
				".TABLE_BILL_INV_PROFORMA.".new_ref_ord_no='".$order_number."' WHERE id=".$invDetails['id']  ;
			$db1->query($sql_update); 
		
			$data['order_title'] =$ord_details['order_title'];
			$data['order_domain'] =$ord_details['order_domain'];
			$data['number'] =$order_number;
			$data['af_name'] =$my['f_name'];
			$data['al_name'] = $my['l_name'] ;
			$data['client_name'] = $clientDetails['f_name']." ".$clientDetails['l_name']." 
				(".$clientDetails['billing_name']." )" ;
			$data['particulars']=$particulars;
			$data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
			$email = NULL;
			$cc_to_sender= $cc = $bcc = Null;
			if ( getParsedEmail($db1, $s, 'ORDER_CREATE_TO_ADMIN', $data, $email) ) {
				$to     = '';
				echo $email["body"];
				$to[]   = array('name' => $bill_ord_name , 'email' => $bill_ord_email);                           
				$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
				SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
			   
			}
			
        }
    }
    
  }
  
$date = $body=$list	= $fields =$condition_query =$exeCreted= $exeCreatedname= $fields1 =$condition2 = $exeCreatedname=$temp = $pKey = $parti=$subject =$to =$isHtml=$admin_email = $from   = $reply_to =null;

$ss_title =  $ss_punch_line =  $p_amount =  $s_type =  $s_quantity =   $samount = $discount_type = $stot_amount = $tax1_name = $tax1_value = $tax1_amount = $tax1_sub1_name = $tax1_sub1_value = $tax1_sub1_amount = $tax1_sub2_name = $tax1_sub2_value = $tax1_sub2_amount = $d_amount = $tot_amount = $particulars =$sql_update= null;

ob_end_flush();
$db1->close();
$db->close();
$db1 = null;
$db = null;
$s = null;
$messages=null; 
$datetime =null;
?>
