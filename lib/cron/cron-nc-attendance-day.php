<?php  
    if (!defined( "THIS_DOMAIN")) {
      // include('/var/www/html/leena/smeerptech.com/smeerptech.com/lib/config.php');
      include ('/home/httpd/vhosts/smeerptech.com/httpdocs/lib/config.php');	
	}   
	$db 		= new db_local; // database handle
	/* $s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class.
	
	//putenv("TZ=GMT"); // done in config.php
	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true; */
	
    include_once ( DIR_FS_INCLUDES .'/hr-attendance.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    include_once(DIR_FS_CLASS."/class.datetime.php");
    $db1 		= new db_local;
    $datetime 	= new UsrDateTime ;
    $sql =" SELECT ".TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".user_id FROM ".TABLE_USER." WHERE 
        ".TABLE_USER.".do_resign = '0000-00-00 00:00:00' AND ".TABLE_USER.".status='".User::ACTIVE."'";    
    $db->query($sql) ;
    if($db->nf() > 0){
        $total = $db->nf() ;
    }
    else{
        $total = 0 ;
    }
    $body=  "<style type=\"text/css\" rel=\"stylesheet\">
     			.rowodd{background-color:#e3e3e3;}
     			.roweven{background-color:#f0f0f0;}
     		  </style>
     			<table cellpadding='1' cellspacing='1' border='0' align='center'>
     				<tr>
						<td style=\"font-family: arial,sans-serif; font-size: 12px; line-height: 20px; color: rgb(17, 17, 17); padding-top: 7px;\">
							<p><big><strong>Greetings!!</strong></big></p>
						</td>
					</tr>
                    <tr><td colspan=\"2\" style=\"font-family: arial,sans-serif; font-size: 13px; line-height: 20px; font-weight: bold; padding-top: 25px; padding-bottom:10px; color: rgb(204, 0, 0);\"><big>Nerve Center: Attendance : on ".date("d M, Y")."</big></td></tr>
                    <tr>
                        <td colspan=\"2\" style=\" padding-bottom:20px;\"> 
                            <table cellpadding='5' cellspacing='0' border='0' width='100%' style=\"border-collapse:collapse; border:1px solid #d8d8d8;\">
                                <tr>
                                    <td style=\"font-family: arial,sans-serif; font-size: 12px; line-height: 18px; font-weight: bold; border-collapse:collapse; border:1px solid #d8d8d8; width:3% \"><b>Sr</b></td>
                                    <td style=\"font-family: arial,sans-serif; font-size: 12px; line-height: 18px; font-weight: bold; border-collapse:collapse; border:1px solid #d8d8d8; width:12% \"><b>Name</b></td>
                                    <td style=\"font-family: arial,sans-serif; font-size: 12px; line-height: 18px; font-weight: bold; border-collapse:collapse; border:1px solid #d8d8d8; width:12% \"><b>Time in</b></td>
                                    <td style=\"font-family: arial,sans-serif; font-size: 12px; line-height: 18px; font-weight: bold; border-collapse:collapse; border:1px solid #d8d8d8; width:10% \"><b>Time Out</b></td>
                                    <td style=\"font-family: arial,sans-serif; font-size: 12px; line-height: 18px; font-weight: bold; border-collapse:collapse; border:1px solid #d8d8d8; width:10% \"><b>Status</b></td>
                                </tr>" ;
    
    $chdate =date("Y-m-d");
    $db->query($sql) ;
    $key=0;
   
    if($total>0){
        while($db->next_record())  {      
            $name = $db->f('f_name')." ".$db->f('l_name');
            $user_id = $db->f('user_id');
            $details=array();
            $presentCount=0;        
            $sql1 = " SELECT pstatus, MIN(".TABLE_HR_ATTENDANCE.".att_time_in) as att_time_in, MAX(".TABLE_HR_ATTENDANCE.".att_time_out) as att_time_out FROM ".TABLE_HR_ATTENDANCE." 
                WHERE ".TABLE_HR_ATTENDANCE.".date='".$chdate."' AND
               ".TABLE_HR_ATTENDANCE.".uid='".$user_id."' 
               GROUP BY ".TABLE_HR_ATTENDANCE.".uid,".TABLE_HR_ATTENDANCE.".date
               ORDER BY ".TABLE_HR_ATTENDANCE.".att_time_in, ".TABLE_HR_ATTENDANCE.".uid ASC LIMIT 0,1";                                       
             $db1->query($sql1) ;
             $time_in ='';
             $time_out='';
             $bgcolor_in = '';
             $bgcolor_out = '';
             $cstatus='';
             $pstatus='-';
                while($db1->next_record())  {
                     $cstatus =  $db1->f('pstatus');
                     $att_time_in =  $db1->f('att_time_in');
                     $att_time_out =  $db1->f('att_time_out');
                     $bgcolor_in = '';
                     $time_in_c = '';
                     $time_out_c = '';                         
                     if($cstatus==HrAttendance::PRESENT ){                           
                            $pstatus = 'Present';
                            $presentCount++;
                            if($att_time_in !='0000-00-00 00:00:00'){
                               $time_in = synchronizeTime($datetime->dateToUnixTime($att_time_in), $local_setting["time_offset"] );
                            }
                            if($att_time_out!='0000-00-00 00:00:00'){
                                $time_out = synchronizeTime($datetime->dateToUnixTime($att_time_out), $local_setting["time_offset"] );
                            }                           
                            if($time_in !='0000-00-00 00:00:00' && !empty($time_in)){
                                $time_in_c = (int) date("Hi",$time_in);                               
                                if($time_in_c >IN_TIME_CHECK){
                                    $bgcolor_in=IN_COLOR;
                                }else{
                                    $bgcolor_in='';
                                }
                            }
                            if($time_out!='0000-00-00 00:00:00' && !empty($time_out)){
                                /*$time_out_c = date("Hi",$time_out);
                                if($time_out_c > OUT_TIME_CHECK){
                                    $bgcolor_out= OUT_COLOR;
                                }else{
                                    $bgcolor_out='';
                                }
                                */
                                $bgcolor_out='';
                            }
                            $time_in =date("H:i",$time_in);
                            $time_out =date("H:i",$time_out);
                      }elseif($cstatus==HrAttendance::PAIDLEAVE){
                           $pstatus = 'Paid Leave';
                           $time_in ='';
                           $time_out='';
                      }elseif($cstatus==HrAttendance::NONPAIDLEAVE){
                            $pstatus = 'NonPaid Leave';
                            $time_in ='';
                            $time_out='';                            
                      }
                      
                      
                      
                }
                $sr = $key+1;                 
                if($sr%2==0){
                  $class = 'rowodd';
                }else{
                  $class = 'roweven';
                }
                $key++;
                $body.="<tr class=\"$class\">
                           <td style=\"font-family: Verdana; border-collapse:collapse; border:1px solid #d8d8d8;\"><font size=\"2\">".$sr."</font></td>
                           <td style=\"font-family: Verdana; border-collapse:collapse; border:1px solid #d8d8d8;\"><font size=\"2\">".$name."</font></td>
                           <td style=\"font-family: Verdana; border-collapse:collapse; border:1px solid #d8d8d8;\" bgcolor=".$bgcolor_in."><font size=\"2\">".$time_in."</font></td>
                           <td style=\"font-family: Verdana; border-collapse:collapse; border:1px solid #d8d8d8;\" bgcolor=".$bgcolor_out."><font size=\"2\">".$time_out."</td>
                           <td style=\"font-family: Verdana; border-collapse:collapse; border:1px solid #d8d8d8;\"><font size=\"2\">".$pstatus ."</font></td>
                        </tr>";
        }   
       
    } 

    $body.="     </table>                            
                    </td>
                  </tr>
                  <tr>
                  	<td valign=\"top\"><img src=\"".DIR_WS_IMAGES_NC."/cron-erp-logo.jpg\" alt=\"\" /></td>
                  	<td valign=\"top\" align=\"right\"><img src=\"".DIR_WS_IMAGES_NC."/cron-smeerp-logo.jpg\" alt=\"\" /></td>
                  </tr>
                </table>";
    $subject = "Nerve Center: Attendance : on ".date("d M, Y");
    $to = '';
    $isHtml= true;
    $admin_email = 'cron-ceo@smeerptech.com';
    $to[]   = array('name' => $admin_name , 'email' => $admin_email);                          
    $from   = $reply_to = array('name' => $admin_name, 'email' => $admin_email);
    SendMail($to, $from, $reply_to, $subject, $body, $isHtml);  
	
	
	$subject = $admin_name = $to =	$isHtml = $admin_email = $from=$body = $exeCreted = $fields1 = $condition2 = $chdate=$executive=$condition1 = $reply_to = null;
$time_in = $time_out=$bgcolor_in =      $bgcolor_out =  $cstatus=null;
	$details =  $presentCount=null;
	
ob_end_flush();
$db1->close();
$db->close();
$db1 = null;
$db = null;
$s = null;
$messages=null; 
$datetime =null;	
?>