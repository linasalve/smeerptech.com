<?php  
    if (!defined( "THIS_DOMAIN")) {
       //include('/var/www/html/leena/smeerptech.com/smeerptech.com/lib/config.php');
		include ('/home/httpd/vhosts/smeerptech.com/httpdocs/lib/config.php');	
	}
	$db 		= new db_local; // database handle
	$s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class.
	
	//putenv("TZ=GMT"); // done in config.php

	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true;
    
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php'); 
	include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');     	
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');  
	include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');	
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');        
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
  
  //start from date 06 Aug 2010
  $date  = date("Y-m-d");
  $db1 = new db_local; // database handle
  $db2 = new db_local; // database handle    
  $sql = " SELECT * FROM ".TABLE_INV_REMINDER_CRON." 
         WHERE date_format(".TABLE_INV_REMINDER_CRON.".send_reminder_dt,'%Y-%m-%d') = '".$date."' ";
  $db->query($sql) ;
  $sr=0;
  
 if ( $db->nf() > 0 ){
  
    while ($db->next_record()) {   
		$data=array();
        $client_id = $db->f('client_id') ;
        $pinv_id = $db->f('pinv_id') ;
        $pinv_no = $db->f('pinv_no') ;
        $reminder_id = $db->f('id') ;
        $days_before_expiry = $db->f('days_before_expiry') ;		  
        $do_expiry = $db->f('do_expiry') ;	
		
		//
		$temp = explode(' ',$do_expiry);
		$temp1 =explode('-',$temp[0]);		
		$dd=$temp1[2] - 60 ;
		$mm=$temp1[1];
		$yy=$temp1[0];
		$sendDt = mktime(0,0,0,$mm,$dd,$yy);                            
		$last_dt = date('d M Y',$sendDt);
		
		//check inv id in Support ticket bof
		$ticket_id=0;
		$mticket_no='';
		$sql2 = " SELECT ticket_id ,ticket_no FROM ".TABLE_ST_TICKETS." WHERE 
		".TABLE_ST_TICKETS.".invoice_profm_id = '".$pinv_id."' AND ticket_child=0 LIMIT 0,1 ";
		if ( $db1->query($sql2) ) {
			$ticket_id=0;
			if ( $db1->nf() > 0 ) {
				while ($db1->next_record()) {
				   $ticket_id = $db1->f('ticket_id');
				   $mticket_no = $db1->f('ticket_no');
				}
			}
			 
		}
		// check inv id in Support ticket eof
		//Get new ord id & inv details bof
		  $new_ref_ord_id = 0;
		  $condition2 = " WHERE ".TABLE_BILL_INV_PROFORMA.".id = '". $pinv_id ."' ";		  
          $invDetails      = null;
		  $field = TABLE_BILL_INV_PROFORMA.".number,".TABLE_BILL_INV_PROFORMA.".or_no,
		  ".TABLE_BILL_INV_PROFORMA.".amount,".TABLE_BILL_INV_PROFORMA.".company_name," ;
		  $field .= TABLE_BILL_INV_PROFORMA.".new_ref_ord_id,".TABLE_BILL_INV_PROFORMA.".new_ref_ord_no,";
		  $field .= TABLE_BILL_INV_PROFORMA.".do_e,".TABLE_BILL_INV_PROFORMA.".do_i";
          InvoiceProforma::getList($db1, $invDetails, $field, $condition2);
          if(!empty($invDetails)){
            $invDetails = $invDetails[0];
			$new_ref_ord_id = $invDetails['new_ref_ord_id'];
          }
          $temp_p=$temp=null;
          if(!empty($invDetails['or_no'])){
			$condition3 = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $invDetails['or_no'] ."'";
			Order::getParticulars($db1, $temp, 'particulars,ss_punch_line', $condition3);
			if(!empty($temp)){                    
				foreach ( $temp as $pKey => $parti ) {                                  
					$temp_p[] =array(                                       
								'particulars'=>nl2br($temp[$pKey]['particulars']),
								'ss_punch_line' =>nl2br($temp[$pKey]['ss_punch_line'])
								);
				}        
			}
          }
		  
        $data['particulars'] = $temp_p ;        
        //Get inv details eof  
		  
        //Get client details bof
		$condition1 = " WHERE user_id = '". $client_id ."' ";
		$clientDetails      =$subUserDetails= null;
		Clients::getList($db1, $clientDetails, 'f_name,l_name,user_id,title, email,email_1,email_2,billing_name,
		authorization_id ', $condition1);
		$isValid=0;      
		
		if(!empty($clientDetails)){
			$clientDetails = $clientDetails[0];
			$authorization_id = trim($clientDetails['authorization_id'],",");
			$authorizationArr = explode(",",$authorization_id);
			$isValid = in_array(Clients::ACCOUNTS,$authorizationArr);				
			$suCondition = " WHERE parent_id = '".$client_id."' AND status='".Clients::ACTIVE."' 
			AND authorization_id  LIKE '%,".Clients::ACCOUNTS.",%' ORDER BY f_name"; 
			Clients::getList($db1, $subUserDetails, 'number,user_id, f_name, l_name,title as ctitle,
				email,email_1,email_2, email_3,email_4, mobile1, mobile2', $suCondition);
		}
        
		
							
        //Get client details eof
		 $ticket_no  =  SupportTicket::getNewNumber($db1);
		 if(empty($mticket_no)){
			$mticket_no = $ticket_no;
		 }
               
        $data["fname"] = $clientDetails["f_name"];
        $data["lname"] = $clientDetails["l_name"];
		$data['ticket_owner'] = $data["fname"]." ".$data["lname"] ;
        $data["title"] = $clientDetails["title"];
        $data["email"] = $clientDetails["email"];
        $data["email_1"] = $clientDetails["email_1"];
        $data["email_2"] = $clientDetails["email_2"];
		$data["billing_name"]	 = $clientDetails["billing_name"];
        $data["invoice_no"]	 = $invDetails["number"];
		$data["company_name"]	 = $invDetails["company_name"];
		
        $data["mticket_no"]	 = $mticket_no;
		$data["last_dt"] = $last_dt ;
        $temp=$temp1=$dtArr=array();
       
        $temp = explode(" ",$invDetails['do_e']) ;
        $temp1=$temp[0];
        $dtArr =explode("-",$temp1) ;
        $dd =$dtArr[2];
        $mm =$dtArr[1];
        $yy=$dtArr[0];
        $do_e = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
        $data["expiry_date"] = $do_e;
        
        $temp = explode(" ",$invDetails['do_i']) ;
        $temp1=$temp[0];
        $dtArr =explode("-",$temp1) ;
        $dd =$dtArr[2];
        $mm =$dtArr[1];
        $yy=$dtArr[0];
        $do_i = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
        $data["do_invoice"] = $do_i;
        
        $cc_to_sender=$cc=$bcc=null ;        
        $file_name=$mail_send_to_su='';
		$my['user_id'] = 'dec32cb82fe9bd2ffbec266ab14fdac6'; // provide id of auto generated user
		$my['f_name']  = 'Auto'	  ;
		$my['l_name']  = 'System' ;
		
		
        //Send mail to client
        if ( getParsedEmail($db1, $s, 'INVOICE_EXPIRY_REMINDER_CLIENT', $data, $email) ) {
			 $data['ticket_text']    = $email["body"] ;
			
			$data['ticket_subject'] = $email["subject"];
			
			//Create Support ticket bof
			$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
			$data['hrs']=1;
			$data['min']=0;
			$hrs1 = (int) ($data['hrs'] * 6);
			$min1 = (int) ($data['min'] * 6);     
			$ticket_date 	= time() ;
			$ticket_replied = '0' ;
			
			if($ticket_id >0 ){
				$query = "SELECT ". TABLE_ST_TICKETS .".ticket_date, ".TABLE_ST_TICKETS.".status"
								." FROM ". TABLE_ST_TICKETS 
								." WHERE ". TABLE_ST_TICKETS .".ticket_owner_uid = '". $client_id ."' " 
								." AND (". TABLE_ST_TICKETS .".ticket_child = '". $ticket_id ."' " 
											." OR "
											. TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " 
										.") "
								." ORDER BY ". TABLE_ST_TICKETS .".ticket_date DESC LIMIT 0,1";				
						
				$db2->query($query) ;
				if($db2->nf() > 0){
					$db2->next_record() ;
					$last_time = $db2->f("ticket_date") ;
					$ticket_response_time = time() - $last_time ;
				}else{
					$ticket_response_time = 0 ;
				}
				
				$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
					. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
					. TABLE_ST_TICKETS .".invoice_profm_id  = '". $pinv_id ."', "
					. TABLE_ST_TICKETS .".invoice_profm_no  = '". $pinv_no ."', "
					. TABLE_ST_TICKETS .".flw_ord_id        = '". $new_ref_ord_id ."', "
					. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $client_id ."', "
					. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
					. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['user_id'] ."', "
					. TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
					. TABLE_ST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
					. TABLE_ST_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
					. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status."', "
					. TABLE_ST_TICKETS .".ticket_child      = '".$ticket_id."', "
					. TABLE_ST_TICKETS .".ticket_attachment = '', "
					. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
					. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
					. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
					. TABLE_ST_TICKETS .".min = '". $data['min']."', "
					. TABLE_ST_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
					. TABLE_ST_TICKETS .".ticket_replied    = '".$ticket_replied."', "
					. TABLE_ST_TICKETS .".from_admin_panel  = '".SupportTicket::ADMIN_PANEL."', "					
					. TABLE_ST_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
					. TABLE_ST_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
					. TABLE_ST_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "				
					. TABLE_ST_TICKETS .".ticket_date        = '". $ticket_date ."' " ;		
				
				 $db2->query($query);
			
				$variables['hid'] = $db2->last_inserted_id() ;	
				
			}else{
				$ticket_response_time = 0 ;
				$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
                        . TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
                        . TABLE_ST_TICKETS .".invoice_profm_id  = '". $pinv_id ."', "
						. TABLE_ST_TICKETS .".invoice_profm_no  = '". $pinv_no ."', "
						. TABLE_ST_TICKETS .".flw_ord_id        = '". $new_ref_ord_id ."', "
                        . TABLE_ST_TICKETS .".ticket_owner_uid  = '". $client_id ."', "
                        . TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
                        . TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['user_id'] ."', "
                        . TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
                        . TABLE_ST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
                        . TABLE_ST_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
                        . TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
                        . TABLE_ST_TICKETS .".ticket_child      = '0', "						
						. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_ST_TICKETS .".min = '". $data['min']."', "                       
                        . TABLE_ST_TICKETS .".ticket_response_time = '0', "
                        . TABLE_ST_TICKETS .".ticket_replied        = '0', "
                        . TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
                       // . TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
                       // . TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
                       // . TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
                        . TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
                        . TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ;
					
					$db2->query($query);
					$variables['hid'] = $db2->last_inserted_id() ;	
				   //Mark pending towards client bof
                    $query_update = "UPDATE ". TABLE_ST_TICKETS 
						." SET ". TABLE_ST_TICKETS .".ticket_status = '".$ticket_status."'"                     
						.",".TABLE_ST_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
						.",".TABLE_ST_TICKETS.".last_comment_from = '".SupportTicket::ADMIN_COMMENT."'"
						.",".TABLE_ST_TICKETS.".last_comment = '".$data['ticket_text']."'"
						.",".TABLE_ST_TICKETS.".last_comment_by = '".$my['user_id'] ."'"
						.",".TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
						." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
					$db2->query($query_update) ;
                    //Mark pending towards client eof
			}
			
			
			//Create Support ticket eof
			
			
            if(($isValid==0 && empty($subUserDetails)) || $isValid==1  ){
				$to     = array();
				$mail_send_to_su .= ",".$data["email"] ;
				$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
				$to[]   = array('name' => $data['email'], 'email' => $data['email']);
				/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
				$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
				*/			   
				//Send mail on email_1
				if(!empty($data["email_1"])){
					//$to     = '';
					$mail_send_to_su .= ",".$data["email_1"] ;
					$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
					$to[]   = array('name' => $data['email_1'], 'email' => $data['email_1']);
					/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
					$cc_to_sender,$cc,$bcc,$file_name); */
				}
				//Send mail on email_2
				if(!empty($data["email_2"])){
					//$to     = '';
					$mail_send_to_su .= ",".$data["email_2"] ;
					$to[]   = array('name' => $data['email_2'], 'email' => $data['email_2']);
					/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
					$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
				} 
			}
			if(!empty($subUserDetails)){
				foreach($subUserDetails as $key=>$val){
				    $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
					$data['ctitle']    =   $subUserDetails[$key]['ctitle'];
					$data['email']    =   $subUserDetails[$key]['email'];
					$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
					$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
					//$to     = array();
					$to[]   = array('name' =>$data['email'], 'email' => $data['email']);
					/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
					$cc_to_sender,$cc,$bcc,$file_name); */
					
					if(!empty($subUserDetails[$key]['email_1'])){
						$data['email_1']    =   $subUserDetails[$key]['email_1'];
						$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						//$to     = '';
						$to[]   = array('name' => $data['email_1']  , 'email' => $data['email_1']);
						/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
						$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
					}
					if(!empty($subUserDetails[$key]['email_2'])){
						$data['email_2']    =   $subUserDetails[$key]['email_2'];
						$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						//$to     = '';
						$to[]   = array('name' => $data['email_2']  , 'email' => $data['email_2']);
						/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
						$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
					}
				}
			}
			if(!empty($mail_send_to_su)){
				
				$query_update = "UPDATE ". TABLE_ST_TICKETS 
                                ." SET ". TABLE_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."'
                                WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
                $db2->query($query_update) ;
				
				
			}
			//update ticket 			
			//Send one copy to test smeerp client bof
			if(!empty($to)){
				$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
				$bcc[]   = array('name' => $data['fname'] .' '. $data['lname'], 'email' => $smeerp_client_email);
				$bcc[]   = array('name' => $data['fname'] .' '. $data['lname'], 'email' => 'team@smeerptech.com');
				SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
			}
			//Send one copy to test smeerp client eof
			 
			
        }
		
		
		
		
         
       /*  if ( getParsedEmail($db2, $s, 'INVOICE_EXPIRY_REMINDER_ADMIN', $data, $email) ) {
			
            $to     = '';
            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
            $to[]   = array('name' => $bill_inv_name , 'email' => $bill_inv_email);
            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
        } */
        //Delete record from reminder cron table
       
        $sql = " DELETE FROM ".TABLE_INV_REMINDER_CRON." WHERE id = ".$reminder_id ;
        $db2->query($sql) ;
        
        $sql = " UPDATE ".TABLE_INV_REMINDER." SET status='1' WHERE pinv_id = ".$pinv_id." AND days_before_expiry = ".$days_before_expiry ;
        $db2->query($sql) ;
        //Update record from reminder table
       
    }
    
  }
  
$date = $body=$list	= $fields =$condition_query =$exeCreted= $exeCreatedname= $fields1 =$condition2 = $exeCreatedname=$temp = $pKey = $parti=$subject =$to =$isHtml=$admin_email = $from   = $reply_to =null;

$client_id = $pinv_id = $pinv_no = $reminder_id = $days_before_expiry=$do_expiry = $temp = $temp1 =  $dd=$mm=$yy=  $sendDt =  $last_dt=$ticket_id=   $mticket_no= $new_ref_ord_id = $condition2 =$invDetails =$field = $temp_p=$clientDetails =$subUserDetails=   $dtArr =$do_i =$ticket_status = $ticket_date 	=$ticket_replied =$last_time =$ticket_response_time = $ticket_no =  $pinv_id =    $pinv_no = $new_ref_ord_id=        $client_id =     $mail_send_to_su=null;





ob_end_flush();
$db1->close();
$db2->close();
$db->close();
$db1 = null;
$db2 = null;
$db = null;
$s = null;
$messages=null; 
$datetime =null;
?>
