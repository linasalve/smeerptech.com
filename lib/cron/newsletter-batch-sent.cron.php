<?php
	ob_start();
	if(!@constant("THIS_DOMAIN")){
		require_once("/home/httpd/vhosts/smeerptech.com/httpdocs/lib/config.php");
		//require_once("/var/www/html/111/leena/smeerptech.com/smeerptech.com/lib/config.php");
	} 

	//include(DIR_FS_NC."/header.php");
	$db 		= new db_local; // database handle
	$s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class. 
	
	//putenv("TZ=GMT"); // done in config.php

	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true;
	
   
    include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    
	$current_time	= isset($_GET["upto"]) ? $_GET["upto"]	: (isset($_POST["upto"]) ? $_POST["upto"] : '');
	$from_time		= isset($_GET["from"]) ? $_GET["from"]	: (isset($_POST["from"]) ? $_POST["from"] : '');
	
	$bcc = "";
	$cc  = "";
	$to  = "";
	
	// Set the Time Zone to IST
	//putenv($TIME_ZONE['tz']);	
    if ( empty($from_time) ) {
		//$from_time = '2000-01-01 00:00:00';
		//$compare_time	= date("Y-m-d H:i:s", mktime(date("H"),date("i")+30,date("s"),date("m"),date("d"),date("Y")));
        
        //7 days before from current date 
        $from_time 	= date("Y-m-d H:i:s", mktime(date("H"),date("i"),date("s"),date("m"),date("d")-7,date("Y")));
	}
	if ( empty($current_time) ) {
		$current_time 	= date("Y-m-d H:i:s", mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")));
	}
	
    echo ("<br/>between $from_time and $current_time");
    
	 $temp_query = " SELECT ". TABLE_NEWSLETTER_TEMP .".temp_news_id"
						.", ". TABLE_NEWSLETTER_TEMP .".n_id"
						.", ". TABLE_NEWSLETTER_TEMP .".smtp_id"
						.", ". TABLE_NEWSLETTER_TEMP .".subject"
						.", ". TABLE_NEWSLETTER_TEMP .".message"
						.", ". TABLE_NEWSLETTER_TEMP .".sendAs"
						.", ". TABLE_NEWSLETTER_TEMP .".batches_remaining"
				." FROM ". TABLE_NEWSLETTER_TEMP
				." WHERE ". TABLE_NEWSLETTER_TEMP .".batches_remaining != ''"
				." AND ". TABLE_NEWSLETTER_TEMP .".send_on_date BETWEEN '". $from_time ."' AND '". $current_time ."'"
				." AND ". TABLE_NEWSLETTER_TEMP .".failed = '0'"
				." ORDER BY send_on_date ASC"
				." LIMIT 0, 1";
//echo '<br/>'. $temp_query;

	$db->query($temp_query);
	
	if ( $db->nf()>0 ) {
		$db->next_record();
		$data = processSqlData( $db->Record );

		$temp_newsletter = array(
									"temp_news_id"		=> $data["temp_news_id"],
									"n_id"			=> $data["n_id"],
									"smtp_id"			=> $data["smtp_id"],
									"subject"			=> $data["subject"],
									"message"			=> $data["message"],
									"sendAs"			=> $data["sendAs"],
									"batches_remaining"	=> $data["batches_remaining"]
								);
        $newsletter_id = $temp_newsletter['n_id'];
		// Read the SMTP account to send email.
		$query = "SELECT * FROM ". TABLE_NEWSLETTER_SMTP
					." WHERE ". TABLE_NEWSLETTER_SMTP .".smtp_id = '". $temp_newsletter["smtp_id"] ."' ";
		$db->query( $query );
		if ( $db->nf() > 0 ) {
			while( $db->next_record() ) {
				$data = processSqlData( $db->Record );

				$socketfromname = $data["smtp_name_display"];
				$socketfrom 	= $data["smtp_email_display"];
				$smtpauthuser	= $data["smtp_email"];			
				$smtpauthpass	= $data["smtp_password"];
				$sockethost		= $data["smtp_host"];
				$smtpport		= $data["smtp_port"];
				$smtp_id		= $data["smtp_id"]; 
				$smtpauth		= true;
			}
		}			
	    
		// Read the batch emails
		$batch_array = explode(",", $temp_newsletter["batches_remaining"]);
		//print_r($batch_array);
		
		$query = " SELECT "	. TABLE_NEWSLETTER_TEMP_EMAILS .".temp_email_id,"
							. TABLE_NEWSLETTER_TEMP_EMAILS .".to,"
							. TABLE_NEWSLETTER_TEMP_EMAILS .".cc,"
							. TABLE_NEWSLETTER_TEMP_EMAILS .".bcc"
					. " FROM " . TABLE_NEWSLETTER_TEMP_EMAILS
					. " WHERE ". TABLE_NEWSLETTER_TEMP_EMAILS .".temp_email_id = '". $batch_array[0] ."'";
		
		$db->query($query);
		
		if ( $db->next_record() ) {
			$data 							= processSqlData( $db->Record );	
			$temp_newsletter["to"] 			= $data["to"];
			$temp_newsletter["cc"] 			= $data["cc"];
			$temp_newsletter["bcc"] 		= $data["bcc"];
			
			$temp_newsletter["from"]["email"]= $socketfrom;
			$temp_newsletter["from"]["name"]= $socketfromname;
			$temp_newsletter["reply_to"] 	= $temp_newsletter["from"];
			
			if ( !empty($temp_newsletter["to"]) ) {
				$to = "" ;
				$email_arr = explode(",", $temp_newsletter["to"]);
	
				foreach ($email_arr as $key=>$item) {
					$name 	= trim(substr($item, 0, strrpos($item, "<")) ) ;
					$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;
	
					if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/" , $email)) {
						$error["email_to_invalid"] = 1 ;
					}
					else {
						$to[] = array (	"email" => $email,
										"name" => $name
									);
					}
				}
			}
	
			if ( !empty($temp_newsletter["cc"]) ) {
				$cc = "" ;
				$email_arr = explode(",", $temp_newsletter["cc"]);
	
				foreach ($email_arr as $key=>$item) {
					$name 	= trim(substr($item, 0, strrpos($item, "<")) ) ;
					$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;
	
					if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/" , $email)) {
						$error["email_cc_invalid"] = 1 ;
					}
					else {
						$cc[] = array (	"email" => $email,
										"name" => $name
									);
					}
				}
			}
			if ( !empty($temp_newsletter["bcc"]) ) {
				$bcc = "" ;
				$email_arr = explode(",", $temp_newsletter["bcc"]);
	
				foreach ($email_arr as $key=>$item) {
					$name 	= trim(substr($item, 0, strrpos($item, "<")) ) ;
					$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;
	
					if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/" , $email)) {
						$error["email_bcc_invalid"] = 1 ;
					}
					else {
						$bcc[] = array (	"email" => $email,
											"name" 	=> $name											
										);
					}
				}
			}
			
			// Send the Mail.
			$attach 	= NULL;
			$from		= $temp_newsletter["from"];
			$reply_to	= $temp_newsletter["reply_to"];
			$subject	= $temp_newsletter["subject"];
			$message	= $temp_newsletter["message"];
			$sendAs		= $temp_newsletter["sendAs"];	
			$sendCopy	= false;
            
            
             //Code to send email to each email id BOF
             if(!empty($to)){
                foreach($to as $key=>$sendto){
                   
                    $sendcc=null;
                    $sendbcc =null;
                    $sendStatus = SendMail($sendto, $from, $reply_to, $subject, $message, $sendAs, false, $sendcc, $sendbcc, $attach);
                    $pos    =   strpos($sendStatus,'Email Sent');
                    
                    if($pos == false){
                        //mail not sent 
                        $email= $sendto['name']." <".$sendto['email'].">";
                        $query = "INSERT INTO " . TABLE_NEWSLETTER_SENT_EMAIL . " SET "
                                    . TABLE_NEWSLETTER_SENT_EMAIL .".n_id = '"	. $newsletter_id ."', "
                                    . TABLE_NEWSLETTER_SENT_EMAIL .".email = '"	. $email ."', "
                                    . TABLE_NEWSLETTER_SENT_EMAIL .".status = '0', "                                                 
                                    . TABLE_NEWSLETTER_SENT_EMAIL .".do_sent = '"	.date("Y-m-d") ."' " ;
                        $db->query($query);
                    }else{
                        //mail sent successufully
                        $email= $sendto['name']." <".$sendto['email'].">";
                        $query = "INSERT INTO " . TABLE_NEWSLETTER_SENT_EMAIL . " SET "
                                    . TABLE_NEWSLETTER_SENT_EMAIL .".n_id = '". $newsletter_id ."', "
                                    . TABLE_NEWSLETTER_SENT_EMAIL .".email = '". $email ."', "
                                    . TABLE_NEWSLETTER_SENT_EMAIL .".status = '1',"                                                 
                                    . TABLE_NEWSLETTER_SENT_EMAIL .".do_sent = '"	.date("Y-m-d") ."' " ;
                        $db->query($query);
                        
                    }
                 
                }
                
                $query = "UPDATE " . TABLE_NEWSLETTER ." SET "
                                       . TABLE_NEWSLETTER .".sent_date = '".date("Y-m-d")."' " 
                             ." WHERE ". TABLE_NEWSLETTER .".n_id = '". $newsletter_id ."' " ;
                $db->query($query);
                
                
                $query = " DELETE FROM ". TABLE_NEWSLETTER_TEMP_EMAILS 
							. " WHERE ". TABLE_NEWSLETTER_TEMP_EMAILS .".temp_email_id = '". $batch_array[0] ."'";
				$db->query( $query );
				
				// Remove the entry of the sent email from the database.
				unset($batch_array[0]);
				$batch_array = implode(",", $batch_array);
				$query = "UPDATE ". TABLE_NEWSLETTER_TEMP 
					." SET ". TABLE_NEWSLETTER_TEMP .".batches_remaining = '". $batch_array ."'"
					." WHERE ". TABLE_NEWSLETTER_TEMP .".temp_news_id = '". $temp_newsletter["temp_news_id"] ."'";
             }
				$db->query( $query );
             //Code to send email to each email id EOF    
            
			/*	
			$sentmsg = SendMail($to, $from, $reply_to, $subject, $message, $sendAs, false, $cc, $bcc, $attach);
			if ( $sentmsg == 'Email Sent. ' ) 
			{	
				$query = " DELETE FROM ". TABLE_NEWSLETTER_TEMP_EMAILS 
							. " WHERE ". TABLE_NEWSLETTER_TEMP_EMAILS .".temp_email_id = '". $batch_array[0] ."'";
				$db->query( $query );
				
				// Remove the entry of the sent email from the database.
				unset($batch_array[0]);
				$batch_array = implode(",", $batch_array);
				$query = "UPDATE ". TABLE_NEWSLETTER_TEMP 
								." SET ". TABLE_NEWSLETTER_TEMP .".batches_remaining = '". $batch_array ."'"
							." WHERE ". TABLE_NEWSLETTER_TEMP .".temp_news_id = '". $temp_newsletter["temp_news_id"] ."'";
				$db->query( $query );
				//echo $query;exit;				
			}
			elseif($sentmsg == "Error: The following From address failed: ".$temp_newsletter["from"]["email"]){
			//else{							
				$query = "UPDATE ". TABLE_NEWSLETTER_TEMP 
							." SET ". TABLE_NEWSLETTER_TEMP .".failed = '1'"
							." WHERE ". TABLE_NEWSLETTER_TEMP .".temp_news_id = '". $temp_newsletter["temp_news_id"] ."'";
				$db->query( $query );
			}
			else{
				// Remove the entry from the database if the receipent emails fails.
				unset($batch_array[0]);
				$batch_array = implode(",", $batch_array);
				$query = "UPDATE ". TABLE_NEWSLETTER_TEMP 
								." SET ". TABLE_NEWSLETTER_TEMP .".batches_remaining = '". $batch_array ."'"
							." WHERE ". TABLE_NEWSLETTER_TEMP .".temp_news_id = '". $temp_newsletter["temp_news_id"] ."'";
				$db->query( $query );
			}
            */
		}
	}
	
	// All the mails have been sent, empty the tables.
	$query = "DELETE FROM ". TABLE_NEWSLETTER_TEMP
				." WHERE ". TABLE_NEWSLETTER_TEMP .".batches_remaining = ''";
	$db->query( $query );
	
	$query = "SELECT count(*) as records FROM ". TABLE_NEWSLETTER_TEMP
				." WHERE ". TABLE_NEWSLETTER_TEMP .".batches_remaining != ''";
	$db->query($query);
	$db->next_record();
	$records = $db->f('records');
	if($records < 1 ){
		// All the mails have been sent, empty the tables.
		$query = "TRUNCATE TABLE ". TABLE_NEWSLETTER_TEMP;
		$db->query( $query );
		$query = "TRUNCATE TABLE ". TABLE_NEWSLETTER_TEMP_EMAILS;
		$db->query( $query );
	}
	
	
$subject = $to = $isHtml = $query =$admin_email=$from=$body =$message=$exeCreted=$fields1=$condition2=$executive=$condition1=$reply_to =  $batch_array = $exeCreatedname = $statusArr = $priorityArr = $temp_newsletter = null;

ob_end_flush();
$db->close();
$db = null;
$s = null;
$messages=null; 
$datetime =null;

?>									
