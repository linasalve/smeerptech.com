<?php  
    if (!defined( "THIS_DOMAIN")) {
        //include('/var/www/html/leena/smeerptech.com/smeerptech.com/lib/config.php');
		/* include ('/home/httpd/vhosts/smeerptech.com/httpdocs/lib/config.php');	 */

		include ('/var/www/vhosts/smeerp.erp.in/httpdocs/lib/config.php');	
	}

    $db 		= new db_local; // database handle
	$s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class.
	
	//$bgselect = "SET OPTION SQL_BIG_SELECTS=1" ;
	//$db->query($bgselect); 
	//putenv("TZ=GMT"); // done in config.php	
	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true;
    
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');     
    include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');        
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
	include_once (DIR_FS_INCLUDES .'/payment-party-bills.inc.php');
    include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
    include_once ( DIR_FS_INCLUDES ."/html2text.inc.php" );
    include_once (DIR_FS_INCLUDES .'/imap.inc.php');
    
    $completeMailList= $emailArray = $idArray = $diffEmails = array();
  
 
	$mailBox = imap_open("{imap.emails.com:123/novalidate-cert}INBOX", "supportimap@smeerptech.com", "DK34#dfL34#dfKS#%&34#df34dn")
    or die("can't connect: " . imap_last_error());
    $check = imap_mailboxmsginfo($mailBox);
    $totalNumOfMsg = $check->Nmsgs;
   
    $numOfMess = $totalNumOfMsg ;
    for($msgno=1; $msgno<=$numOfMess;$msgno++){
        $emailList = array();
        $header = imap_headerinfo($mailBox, $msgno, 80, 80);
       
        //if($header->Unseen =='U'){        
            $msgBody = retrieve_message($mailBox, $msgno);  
			//print_R($msgBody);
            /*
            $strString1 = $msgBody['fromaddress'] ;
            $pos1 = strpos($strString1,"<") +1;
            $pos2 = strpos($strString1,">")  ; 
            $pos3  = $pos2 - $pos1;
            $email = substr($strString1,$pos1,$pos3);
            */
			$mainTicketId =$mTicketNo = 0;
            //$email = trim($msgBody['from_email']) ;
			$email = strtolower(trim($msgBody['from_email'])) ;
            $clientDetails = SupportTicket::getStClient($db, $email);
            $clientDetails = $clientDetails['0'];          
            //get ticket no bof
            /*
			$pos3 = strpos( $msgBody['subject'],"#") +1;
            $pos4 = strpos( $msgBody['subject'],"::");
            $len  = $pos4 - $pos3;
            $mTicketNo = substr( $msgBody['subject'],$pos3,$len);  */
			
			$pos3 = strpos( $msgBody['subject'],"#") +1;
            $pos4 = strpos($msgBody['subject'], '#]',$pos3);
            $len  = $pos4 - $pos3;
            $mTicketNo = substr( $msgBody['subject'],$pos3,$len); 
			/******** 
			AS Clients are sending mails to support email id with SALES ticket subject 
			thats why, creating the threads on ticket nos which is a sales ticket no.
			*********/
			$needle = "[SPPT#" ;
			if (strpos($msgBody['subject'],$needle)){
				  
			}else{
				if($mTicketNo >0){
					$mTicketNo =0;
				}
			}
			
            
			if( $email=='support@smeerptech.com' || $email=='support@smeerptech.com' || $email=='aryan@smeerptech.com' || $email=='supportadmin@smeerptech.com' || $email=='aryan@smeerptech.com' || $email=='aryan@smeerptech.com' || $email=='aryan@smeerptech.com' || $email=='admin@smeerptech.com' || $email=='admin@smeerptech.com' || $email=='lina.salve28@gmail.com'){
			
				if($mTicketNo >0){
					$ticketDetails = SupportTicket::getTicketDetails($mTicketNo);
					$mainTicketId  = $ticketDetails['ticket_id'];     
					$cemail		=	$ticketDetails['from_email'];  	
					//If ticket status = CLOSED then create new ticket
					if(!empty($ticketDetails['ticket_status']) && 
						$ticketDetails['ticket_status'] == SupportTicket::CLOSED ){
						 $mainTicketId  ='';
					}
					$clientDetails = SupportTicket::getStClient($db, $cemail);
					$clientDetails = $clientDetails['0']; 
				}
				
			}else{
			
				if($mTicketNo >0 && !empty($clientDetails['mainClient']) ){
					 
					$ticketDetails = SupportTicket::getTicketDetails($mTicketNo);
					$mainTicketId  = $ticketDetails['ticket_id'];                
					//If ticket status = CLOSED then create new ticket
					if(!empty($ticketDetails['ticket_status']) && 
						$ticketDetails['ticket_status'] == SupportTicket::CLOSED ){
						 $mainTicketId  ='';
					}
				}
			}
            //get ticket no eof
			
		
            //Check the from email if aryan sir's email the thread will be attach in the given task no BOF
			//$email=='support@smeerptech.com'
            if( ($email=='support@smeerptech.com' || $email=='support@smeerptech.com' || $email=='aryan@smeerptech.com' || $email=='aryan@smeerptech.com' || $email=='supportadmin@smeerptech.com' || $email=='supportadmin@smeerptech.com' || $email=='aryan@smeerptech.com' || $email=='admin@smeerptech.com' || $email=='admin@smeerptech.com' || $email=='lina.salve28@gmail.com') && $mTicketNo >0 && !empty($ticketDetails) ){
			    /***********************************
					SUBJECT => pt[#3232#] - subject
				************************************/
			    
				//disply random name BOF 
				$data['display_name']=$data['display_user_id']=$data['display_designation']='';
				//if($mail_to_all_su==1 || $mail_to_additional_email==1 || $mail_client==1){
					$randomUser = getRandomAssociate();
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
				//}
				//disply random name EOF
                 
				//$pos1 = strpos( $msgBody['subject'],":") +1;
                $pos2 = strpos( $msgBody['subject'],"[");
                $pos1 = $pos2 - 2; // pos before 2 characters of #
				$len  = $pos2 - $pos1;
                $statusName = strtolower(trim(substr( $msgBody['subject'],$pos1,$len)));
				$statusName = trim( $statusName);
                $sendmailclient = 0;
                $ticket_status = SupportTicket::PENDINGWITHSMEERP;
                
                if($statusName=='pt'){
                   $ticket_status = SupportTicket::PENDINGWITHSMEERP;
                   $sendmailclient = 0;
                }
                if($statusName=='pp'){
                   $ticket_status = SupportTicket::PENDINGWITHSMEERP;
                   $sendmailclient = 1;
                }
                if($statusName=='pc'){
                   $ticket_status = SupportTicket::PENDINGWITHCLIENTS;
                   $sendmailclient = 1;
                }
                
                $my['uid']    = 'e68adc58f2062f58802e4cdcfec0af2d';
                $my['f_name'] ='Aryan';
                $my['l_name'] ='Salve';
				                
                $data['ticket_subject'] = $msgBody['subject'];
                $data['text']= $data['ticket_text']   =   $msgBody['body'];
                $data['text_html']= $data['ticket_text_html']   =   $msgBody['body_html'];
				
                $clientname   =  $clientDetails['mainClient']['0']['f_name']." ".$clientDetails['mainClient']['0']['l_name'];
                $billing_name =  $clientDetails['mainClient']['0']['billing_name'] ;
                $data['main_client_name'] = $clientname;
                $data['billing_name'] = $billing_name;
				$clientDetails['mainClient']='';
				
                $ticket_no  =  SupportTicket::getNewNumber($db);               
                $ticket_id = $ticketDetails['ticket_id'];
				$inv_id =  $ticketDetails['inv_id'];
				$pinv_id =  $ticketDetails['pinv_id'];
				$flw_ord_id = $ticketDetails['flw_ord_id'];
                $data['ticket_owner_uid'] = $ticketDetails['ticket_owner_uid'];
                $data['ticket_owner'] = $ticketDetails['ticket_owner'];
                $data['ticket_creator_uid'] = $ticketDetails['ticket_creator_uid'];
                 
                $query = "SELECT "	. TABLE_ST_TICKETS .".ticket_date, ".TABLE_ST_TICKETS.".status"
						." FROM ". TABLE_ST_TICKETS ." WHERE ". TABLE_ST_TICKETS .".ticket_owner_uid 
						= '". $data['ticket_owner_uid'] ."' " 
						." AND (". TABLE_ST_TICKETS .".ticket_child = '". $ticket_id ."' " 
						." OR "	. TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " 
						.") "." ORDER BY ". TABLE_ST_TICKETS .".ticket_date DESC LIMIT 0,1"; 
				
				$db->query($query) ;
				if($db->nf() > 0){
					$db->next_record() ;
					$last_time = $db->f("ticket_date") ;
					$ticket_response_time = time() - $last_time ;
					$status = $db->f("status") ;
				}
				else{
					$ticket_response_time = 0 ;
				}
				$ticket_date 	= time() ;
				$ticket_replied = '0' ;
                $query = "INSERT INTO ". TABLE_ST_TICKETS ." SET "
						. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
						. TABLE_ST_TICKETS .".ticket_owner_uid  = '".processUserData($data['ticket_owner_uid']) ."', "
						. TABLE_ST_TICKETS .".ticket_owner      = '".processUserData( $data['ticket_owner']) ."', "
						. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
						. TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_ST_TICKETS .".invoice_id        = '". $inv_id ."', "
						. TABLE_ST_TICKETS .".invoice_profm_id  = '". $pinv_id ."', "
						. TABLE_ST_TICKETS .".flw_ord_id        = '". $flw_ord_id ."', "
						. TABLE_ST_TICKETS .".ticket_text_html = '". processUserData($data['ticket_text_html']) ."', "
						. TABLE_ST_TICKETS .".ticket_text       = '". processUserData($data['ticket_text']) ."', "
						. TABLE_ST_TICKETS .".ticket_subject    = '". processUserData($data['ticket_subject']) ."', "
						. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status."', "
						. TABLE_ST_TICKETS .".ticket_child      = '".$ticket_id."', "
						. TABLE_ST_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
						. TABLE_ST_TICKETS .".ticket_replied    = '".$ticket_replied."', "
						. TABLE_ST_TICKETS .".from_admin_panel  = '".SupportTicket::ADMIN_PANEL."', "
						. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
						. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
						. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
						. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
						. TABLE_ST_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_ST_TICKETS .".do_e              = '".date('Y-m-d H:i:s', time())."', "
						. TABLE_ST_TICKETS .".do_comment        = '".date('Y-m-d H:i:s', $ticket_date)."', "
						. TABLE_ST_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
						$db->query($query) ;
						$variables['hid'] =  $db->last_inserted_id() ; 
						$query_update = "UPDATE ". TABLE_ST_TICKETS 
							." SET ". TABLE_ST_TICKETS .".ticket_status = '".$ticket_status."'"                           
							.",".TABLE_ST_TICKETS.".do_comment = '".date('Y-m-d H:i:s', $ticket_date)."'"
							.",".TABLE_ST_TICKETS.".last_comment_from = '".SupportTicket::ADMIN_COMMENT."'"
							.",".TABLE_ST_TICKETS.".last_comment = '".processUserData($data['ticket_text'])."'"
							.",".TABLE_ST_TICKETS.".last_comment_by = '".$my['uid'] ."'"
							.",".TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
							." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
                
				$db->query($query_update) ; 
                $data['replied_by']   =   $my['f_name']." ". $my['l_name'] ;
                $data['subject']    =  $ticketDetails['ticket_subject'] ;
                $data['text']    =   $data['ticket_text'] ;                  
                $data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $ticket_id;
                $data['ticket_no']    =   $ticket_no ;
                $data['mticket_no']   =   $mTicketNo ;
                
                if($sendmailclient ==1){
                 
                     Clients::getList($db, $clientDetails, 'number, f_name, l_name, email,email_1,email_2,email_3,email_4,
                        additional_email,title as ctitle,billing_name,mobile1,mobile2', " WHERE
						user_id = '".$data['ticket_owner_uid']."'");
                    $clientDetails=$clientDetails['0']; 
                   
                    if(!empty($clientDetails)){
                        
                        $data['client_name']    =   $clientDetails['f_name']." ".$clientDetails['l_name'];
                        $data['ctitle']    =   $clientDetails['ctitle'];
                        $email = NULL;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $clientDetails['email'],
                            'email' => $clientDetails['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            if(!empty($clientDetails['email'])){ 
							
								/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
								$mail_send_to_su .= ",".$clientDetails['email'];
							}  
                            
                           
                            //Send mail on alternate emails ids bof
                            if(!empty($clientDetails['email_1'])){                                       
                                //$to = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' =>  $clientDetails['email_1'], 'email' =>
                                $clientDetails['email_1']);                                   
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                $mail_send_to_su  .= ",".$clientDetails['email_1'];
                               /*  SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
                            }
                            if(!empty($clientDetails['email_2'])){                                       
                                //$to = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $clientDetails['email_2'], 'email' =>
                                $clientDetails['email_2']);                                   
                                $from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);
                                $mail_send_to_su  .= ",".$clientDetails['email_2'];
                               /*  SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
                            }
                            if(!empty($clientDetails['email_3'])){                                       
                                //$to = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $clientDetails['email_3'], 'email' =>
                                $clientDetails['email_3']);                                   
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                $mail_send_to_su  .= ",".$clientDetails['email_3'];
                               /*  SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
                            }
                            if(!empty($clientDetails['email_4'])){                                       
                                //$to = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $userDetails['email_4'], 'email' =>
                                $userDetails['email_4']);                                   
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                $mail_send_to_su  .= ",".$clientDetails['email_4'];
                                /* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
                            }
                        }
						//Send a copy to check the mail for clients
						
                    }
                    
                    //if($ticketDetails['last_comment_from']==SupportTicket::CLIENT_COMMENT){
                        if($data['ticket_owner_uid'] != $data['ticket_creator_uid'] ){
                             Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND user_id 
								IN('".$data['ticket_creator_uid']."')
                                AND status='".Clients::ACTIVE."' ORDER BY f_name");                        
                            if(!empty($subUserDetails)){
                                
                                foreach ($subUserDetails  as $key=>$value){
                                    $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                    $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                    $email = NULL;
                                    $cc_to_sender= $cc = $bcc = Null;
                                    if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                                        $to = '';
                                        $to[]   = array('name' => $subUserDetails[$key]['email'], 
										'email' => $subUserDetails[$key]['email']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        /*  
										if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                        $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                        } */
                                        if(!empty($subUserDetails[$key]['email_1'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_1'], 
											'email' => $subUserDetails[$key]['email_1']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                            
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                
                                            } */
                                        } 
                                        if(!empty($subUserDetails[$key]['email_2'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_2'], 
											'email' => $subUserDetails[$key]['email_2']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                            } */
                                        }
                                        if(!empty($subUserDetails[$key]['email_3'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_3'], 
											'email' => $subUserDetails[$key]['email_3']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                
                                            } */
                                        }
                                        if(!empty($subUserDetails[$key]['email_4'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_4'], 'email' => $subUserDetails[$key]['email_4']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                             $mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                               
                                            } */
                                        }
                                        
                                    }                      
                                }
                            }
                        }
                    //}
					
					if(!empty($to)){
						//$to     = '';
						$cc_to_sender=$cc=$bcc='';
						$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
						'email' => $smeerp_client_email);
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
					}    
                    if(!empty($mail_send_to_su)){
                        //Set the list subuser's email-id to whom the mails are sent bof                    
                        $query_update = "UPDATE ". TABLE_ST_TICKETS 
                        ." SET ". TABLE_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."' WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
                        $db->query($query_update) ;
                    }
                }
				// Send Email to the admin BOF
				/* $email = NULL;
				$data['main_client_name']   = $main_client_name;
				$data['billing_name']   = $billing_name;
				$data['link']   = DIR_WS_NC .'/support-ticket.php?perform=view&ticket_id='.$ticket_id;   
				$data['quicklink']   = DIR_WS_SHORTCUT .'/support-ticket.php?perform=view&ticket_id='. $ticket_id;                              
				 if ( getParsedEmail($db, $s, 'EMAIL_ST_ADMIN_IMAP', $data, $email) ) {
					$to = '';
					$bcc='';
					$to[]   = array('name' => $support_name , 'email' => $support_email);
					$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
					//echo $email["body"];
					SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
				}       */
				                   
				// Send Email to the admin EOF 
                imap_delete($mailBox, $msgno);
            }
            //Check the from email if aryan sir's email the thread will be attach in the given task no EOF
           
           
		   
            if(!empty($clientDetails['mainClient'])){
                $clientname   =  $clientDetails['mainClient']['0']['f_name']." ".$clientDetails['mainClient']['0']['l_name'];
                $billing_name =  $clientDetails['mainClient']['0']['billing_name'] ;
                $data['main_client_name'] = $clientname;
                $data['billing_name'] = $billing_name;
                
                if(!empty($clientDetails['subClient'])){
                    $ticket_owner_uid = $clientDetails['mainClient']['0']['user_id'];
                    $ticket_owner = $clientDetails['mainClient']['0']['f_name']." ".$clientDetails['mainClient']['0']['l_name'];
                    $ticket_creator_uid = $clientDetails['subClient']['0']['user_id'];
                    $ticket_creator = $clientDetails['subClient']['0']['f_name']." ".$clientDetails['subClient']['0']['l_name'];
                }else{
                    $ticket_owner_uid = $ticket_creator_uid = $clientDetails['mainClient']['0']['user_id'];
                    $ticket_owner = $ticket_creator = $clientDetails['mainClient']['0']['f_name']." ".$clientDetails['mainClient']['0']['l_name'];
                }               
                $ticket_no =SupportTicket::getNewNumber($db);
                $ticket_subject = $msgBody['subject'];
                $ticket_text  =   $msgBody['body'];
                $ticket_text_html  =   $msgBody['body_html'];			
				
				
                $headers_imap  =  $msgBody['headers_imap'];
                $email_subject =  $msgBody['subject'];
                $email_body =     $msgBody['body'];
                $to_email =       $msgBody['to_email'];
                $to_email_1 =     $msgBody['to_email_1'];
                $cc_email =       $msgBody['cc_email'];
                $cc_email_1 =     $msgBody['cc_email_1'];
                $bcc_email =      $msgBody['bcc_email'];
                $bcc_email_1 =    $msgBody['bcc_email_1'];
                $fromaddress =    $msgBody['from_email'];
                $udate =          $msgBody['udate'];
                
				//CRON EBILL UPLOAD BOF
				if(trim(strtolower($email))==TATAINDICOM_CBEMAIL || 
					trim(strtolower($email))==VODAFONE_CBEMAIL || 
					trim(strtolower($email))==MSEB_CBEMAIL || 
					trim(strtolower($email))==IDEA_CBEMAIL ){
					$extra1  = array('db' 		=> &$db,
									'messages'  => $messages);
									
					$noArr= array(
						'tata'=>$tataIndicomCron,
						'vodafone'=>$vodafoneCron,
						'idea'=>$ideaCron,
						'bsnl'=>$bsnlCron,
						'mseb'=>$msebCron
					);
					PaymentPartyBills::addCronBill($msgBody,$clientDetails,$noArr,$extra1);
				}
				//CRON EBILL UPLOAD EOF
				if( $ticket_owner_uid=='5684eb8a6c15da7d6eace243a76b7613' || $ticket_owner_uid=='fc54e399d7e4806dd4a2c7edd1f995c0'){
					$tckt_status = SupportTicket::PENDINGWITHCLIENTS ;
				}else{
					$tckt_status = SupportTicket::PENDINGWITHSMEERP;
				}
				
                //$email_subject  =  "[ SMEERP SUPPORT #".$ticket_no."# ] : ".$msgBody['subject'];          
                $data = array();
                $data['isimap'] = '1';
                $data['mysmeerplink'] = DIR_WS_MP."/support-ticket-request.php?perform=add";
                
                if(!empty($mainTicketId) && $mainTicketId >0){
                    $ticket_subject = $email_subject = $ticketDetails['ticket_subject'];
					$inv_id =  $ticketDetails['inv_id'];
					$pinv_id =  $ticketDetails['pinv_id'];
					$flw_ord_id = $ticketDetails['flw_ord_id'];
                    //Calculate responce time BOF
                    $query1 = "SELECT "	. TABLE_ST_TICKETS .".ticket_date "
                    ." FROM ". TABLE_ST_TICKETS 
                    ." WHERE ". TABLE_ST_TICKETS .".ticket_owner_uid = '". $ticket_owner_uid ."' " 
                    ." AND ("
                                . TABLE_ST_TICKETS .".ticket_child = '". $mainTicketId ."' " 
                                ." OR "
                                . TABLE_ST_TICKETS .".ticket_id = '". $mainTicketId ."' " 
                           .") "
                    ." ORDER BY ". TABLE_ST_TICKETS .".ticket_date DESC LIMIT 0,1";	
                    $db->query($query1) ;
                    if($db->nf() > 0){
                        $db->next_record() ;
                        $last_time = $db->f("ticket_date") ;
                        $ticket_response_time = time() - $last_time ;                      
                    }else{
                        $ticket_response_time = 0 ;
                    }
                    //Calculate responce time EOF      
                    
                    $data['showautomsg'] = '0';
                    $data['mticket_no'] = $mTicketNo;
                    //post comment 
                    $query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
                        . TABLE_ST_TICKETS .".ticket_no         	= '". $ticket_no ."', "
                        . TABLE_ST_TICKETS .".ticket_owner_uid 		= '". $ticket_owner_uid ."', "
                        . TABLE_ST_TICKETS .".ticket_owner      	= '". processUserData($ticket_owner )."', "
                        . TABLE_ST_TICKETS .".ticket_creator_uid	= '". $ticket_creator_uid ."', "
                        . TABLE_ST_TICKETS .".ticket_creator    	= '". processUserData($ticket_creator) ."', "
                        . TABLE_ST_TICKETS .".ticket_subject    	= '". processUserData($ticket_subject)."', "
                        . TABLE_ST_TICKETS .".ticket_text       	= '". processUserData($ticket_text) ."', "
                        . TABLE_ST_TICKETS .".ticket_text_html       = '". processUserData($ticket_text_html) ."', "
						. TABLE_ST_TICKETS .".invoice_id        = '". $inv_id ."', "
						. TABLE_ST_TICKETS .".invoice_profm_id  = '". $pinv_id ."', "
						. TABLE_ST_TICKETS .".flw_ord_id        = '". $flw_ord_id ."', "
                        . TABLE_ST_TICKETS .".ticket_department = '', "
                        . TABLE_ST_TICKETS .".ticket_priority   = '', "
                        . TABLE_ST_TICKETS .".ticket_status     = '".$tckt_status."', "
                        . TABLE_ST_TICKETS .".ticket_child      = '".$mainTicketId."', "
						//. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						//. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
						. TABLE_ST_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
						. TABLE_ST_TICKETS .".ticket_replied       = '0', "
						. TABLE_ST_TICKETS .".domain_name          = '', "
						. TABLE_ST_TICKETS .".is_private           = '0', "
						. TABLE_ST_TICKETS .".using_imap           = '1', "
						. TABLE_ST_TICKETS .".headers_imap       = '". processUserData($headers_imap )."', "
						. TABLE_ST_TICKETS .".to_email           = '". processUserData($to_email )."', "
						. TABLE_ST_TICKETS .".cc_email           = '". processUserData($cc_email)."', "
						. TABLE_ST_TICKETS .".bcc_email          = '". processUserData($bcc_email)."', "
						. TABLE_ST_TICKETS .".to_email_1         = '".processUserData($to_email_1)."', "
						. TABLE_ST_TICKETS .".cc_email_1         = '".processUserData($cc_email_1)."', "
						. TABLE_ST_TICKETS .".bcc_email_1        = '".processUserData($bcc_email_1)."', "
						. TABLE_ST_TICKETS .".from_email        = '".processUserData($fromaddress)."', "
						. TABLE_ST_TICKETS .".last_comment_from    = '".SupportTicket::CLIENT_COMMENT."', "
						. TABLE_ST_TICKETS .".status           = '". SupportTicket::DEACTIVE ."', "
						. TABLE_ST_TICKETS .".ip    = '', "
						. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s', time())."', "
						. TABLE_ST_TICKETS .".ticket_date       = '". $udate ."' " ;
                }else{
                       
                        $data['showautomsg'] = '1';
                        $data['mticket_no'] = $ticket_no;
                        //post main ticket
                        $query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
								. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
								. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $ticket_owner_uid ."', "
								. TABLE_ST_TICKETS .".ticket_owner      = '". processUserData($ticket_owner) ."', "
								. TABLE_ST_TICKETS .".ticket_creator_uid= '". $ticket_creator_uid ."', "
								. TABLE_ST_TICKETS .".ticket_creator     = '". processUserData($ticket_creator) ."', "
								. TABLE_ST_TICKETS .".ticket_subject     = '". processUserData($ticket_subject)."', "
								. TABLE_ST_TICKETS .".ticket_mail_subject    = '". processUserData($ticket_subject)."', "
								. TABLE_ST_TICKETS .".ticket_text       = '". processUserData($ticket_text )."', "
								. TABLE_ST_TICKETS .".ticket_text_html       = '". processUserData($ticket_text_html) ."', "
								. TABLE_ST_TICKETS .".ticket_department = '', "
								. TABLE_ST_TICKETS .".ticket_priority   = '', "
								. TABLE_ST_TICKETS .".ticket_status     = '".$tckt_status."', "
								. TABLE_ST_TICKETS .".ticket_child      = '0', "
								//. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
								//. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
								. TABLE_ST_TICKETS .".ticket_response_time = '0', "
								. TABLE_ST_TICKETS .".ticket_replied        = '0', "
								. TABLE_ST_TICKETS .".domain_name           = '', "
								. TABLE_ST_TICKETS .".is_private           = '0', "
								. TABLE_ST_TICKETS .".using_imap           = '1', "
								. TABLE_ST_TICKETS .".headers_imap       = '". processUserData($headers_imap )."', "
								. TABLE_ST_TICKETS .".to_email           = '". processUserData($to_email )."', "
								. TABLE_ST_TICKETS .".cc_email           = '". processUserData($cc_email)."', "
								. TABLE_ST_TICKETS .".bcc_email          = '". processUserData($bcc_email)."', "
								. TABLE_ST_TICKETS .".to_email_1         = '".processUserData($to_email_1)."', "
								. TABLE_ST_TICKETS .".cc_email_1         = '".processUserData($cc_email_1)."', "
								. TABLE_ST_TICKETS .".bcc_email_1        = '".processUserData($bcc_email_1)."', "
								. TABLE_ST_TICKETS .".from_email         = '".processUserData($fromaddress)."', "
                                . TABLE_ST_TICKETS .".last_comment_from  = '".SupportTicket::CLIENT_COMMENT."', "
                                . TABLE_ST_TICKETS .".last_comment    	 = '".processUserData($ticket_text )."', "
                                . TABLE_ST_TICKETS .".last_comment_by    = '".$ticket_creator_uid."', "
                                . TABLE_ST_TICKETS .".last_comment_by_name    = '".$ticket_creator ."', " 
                                . TABLE_ST_TICKETS .".status           = '". SupportTicket::DEACTIVE ."', "
                                . TABLE_ST_TICKETS .".ip    = '', "	
								. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
								. TABLE_ST_TICKETS .".do_comment    = '".date('Y-m-d H:i:s',$udate)."', "
                                . TABLE_ST_TICKETS .".ticket_date       = '".$udate ."' " ;
                    }
				 
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        
                        $variables['hid'] = $db->last_inserted_id() ; 
                        if(!empty($mainTicketId) && $mainTicketId >0){
                             $data['ticket_id'] = $mainTicketId;
                            //Mark pending towards client bof
                            $query_update = "UPDATE ". TABLE_ST_TICKETS 
                            ." SET ". TABLE_ST_TICKETS .".ticket_status = '".SupportTicket::PENDINGWITHSMEERP."'"                            
							.",".TABLE_ST_TICKETS.".do_comment= '".date('Y-m-d H:i:s',$udate)."'"
                            .",".TABLE_ST_TICKETS.".last_comment_from= '".SupportTicket::CLIENT_COMMENT."'"
                            .",".TABLE_ST_TICKETS.".last_comment = '".processUserData($ticket_text )."'"
							.",".TABLE_ST_TICKETS.".last_comment_by = '".$ticket_creator_uid ."'"
							.",".TABLE_ST_TICKETS.".last_comment_by_name = '".$ticket_creator ."'"
                            ." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $mainTicketId ."' " ;
                            $db->query($query_update) ;							
                            //Mark pending towards client eof  
                        }else{
                            $data['ticket_id'] =  $variables['hid'];
                        }
                        //BOF ATTACHMENT BOF
                        $filestore = DIR_FS_ST_FILES."/" ;
                        $i=100;
                        $fileNameStr ='';
                        if(!empty($msgBody['attachments'])){
                            if(!empty($msgBody['attachments'])){
                                foreach($msgBody['attachments'] as $key=>$val){
                                    $filedata["extension"]='';
                                    $filedata = pathinfo($key);                   
                                    $ext = $filedata["extension"] ; 
                                    if(!empty($ext)){
                                        $filename = $ticket_no."-".$variables['hid']."-".$i.".".$ext ;
                                        $fileNameStr .=$filename."," ;
                                        $fp=fopen($filestore.$filename,"w+");
                                        fwrite($fp,$val);
                                         @chmod($filestore.$filename,0777);
                                        fclose($fp);
                                        $i++;
                                    }
                                }                             
                                $fileNameStr = trim($fileNameStr,",") ;
                            }
                            $sql="UPDATE ".TABLE_ST_TICKETS." SET ".TABLE_ST_TICKETS.".attachment_imap='".$fileNameStr."' WHERE ticket_id=".$variables['hid'];
                            $db->query($sql) ;
                        }
                        //EOF ATTACHMENT EOF
                        
                        
                        
                        // **************************************************************************
                        //* Send the notification to the ticket owner and the Administrators
                        //* of the new Ticket being created
                        //************************************************************************** 
                        
                        $data['ticket_no']    =   $ticket_no ;                       
                        $data['status']       = "";                
                        $data['replied_by']   =   $ticket_owner;
                        $data['subject']      =   $ticket_subject;
                        $data['text']         =   $ticket_text;
                        //$data['attachment']   =   $attachfilename ;
                        $data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $data['ticket_id'];
                       
                        // Send Email to the ticket owner BOF  
                        
                        if(!empty($clientDetails['mainClient'])){
                            $userDetails=$clientDetails['mainClient']['0'];
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    = $userDetails['title'];
							$main_client_name   =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $billing_name    =   $userDetails['billing_name'];
                            $email = NULL;
                           /*
                            if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' => $userDetails['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                //echo $email["body"];
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            } */ 
                           
                        }
                        // Send Email to the ticket owner EOF             
                        // Send Email to the Client sub users involved BOF   
                        /*                        
                        if(!empty($clientDetails['subClient'])){
                            $userDetails=$clientDetails['subClient']['0'];
                            $data['replied_by']   =    $userDetails['f_name']." ".$userDetails['l_name'] ;
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    = $userDetails['title'];
                            $email = NULL;
                            
                            if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' => $userDetails['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                //echo $email["body"];
                                SendMail($to, $from, $reply_to, $email_subject, $email["body"], $email["isHTML"]);
                            }          
                        } */
                        // Send Email to the sub users involved EOF
                        
                         // Send Email to the admin BOF
                        $email = NULL;
						$data['main_client_name']   = $main_client_name;
                        $data['billing_name']   = $billing_name;
                        $data['link']   = DIR_WS_NC .'/support-ticket.php?perform=view&ticket_id='.$data['ticket_id'];   
                        $data['quicklink']   = DIR_WS_SHORTCUT .'/support-ticket.php?perform=view&ticket_id='. $data['ticket_id'];                              
                        // ADMIN COPY
						/* if ( getParsedEmail($db, $s, 'EMAIL_ST_ADMIN_IMAP', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $support_name , 'email' => $support_email);
                           
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            //echo $email["body"];
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }    */  
						    
                        // Send Email to the admin EOF   
						
						//Mark delete to the emails after create the ST
						imap_delete($mailBox, $msgno);
                    }                
                
               
            }else{
                //if sender is not found in client list not found         
                // Send Email to the admin BOF
                /* 30-3-2010
                $email = NULL;
                $data['link']   = DIR_WS_NC .'/support-ticket.php?perform=view&ticket_id='.$data['ticket_id'];
                $data['from_email']=$msgBody['from_email'] ;
                $data['email_subject']=$msgBody['subject'] ;
                $data['email_body']=$msgBody['body'] ;                
                if ( getParsedEmail($db, $s, 'EMAIL_ST_ADMIN_IMAP_NOT_MEMBER', $data, $email) ) {
                    $to = '';
                    $to[]   = array('name' => $support_name , 'email' => $support_email);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                    
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                }              
                */                
                // Send Email to the admin EOF   
            }
           
        //} 
        
}

imap_expunge($mailBox);


//imap_close which will silently expunge the mailbox before closing, removing all messages marked for deletion.
imap_close($mailBox);
imap_gc($mailBox, IMAP_GC_ELT);


$completeMailList= $emailArray = $idArray = $diffEmails =$emailList =$data=$msgBody=null;
$numOfMess = $totalNumOfMsg =$email =  $clientDetails = $pos3 = $pos4 = $len  = $mTicketNo =$mainTicketId = $ticketDetails= null;
$pos2 = $pos1 = $len  = $statusName= $sendmailclient=$ticket_status=$clientname=$billing_name= $ticket_id=$ticketDetails=$data=$last_time =$ticket_response_time =$status =$ticket_date = $query = $query_update =$sendmailclient =  $from=$reply_to=$mail_send_to_su = null;
$to = $cc_to_sender=$cc=$bcc=$smeerp_client_email=null;
$subUserDetails = $main_client_name =   $ticket_owner_uid=$ticket_owner =$ticket_creator_uid=$ticket_creator =$ticket_no =  $ticket_subject =  $ticket_text  =  $headers_imap  = $email_subject =  $email_body =$to_email = $to_email_1 =$cc_email = $cc_email_1 = $bcc_email = $bcc_email_1 =  $fromaddress = $udate = null;  
$filestore =$i= $fileNameStr =$msgBody =$userDetails= null;

ob_end_flush();
$db->close();
$db = null;
$s = null;
$messages=null; 
$datetime =null;
?>