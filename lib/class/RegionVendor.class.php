<?php
    class RegionVendor{
        private $region;
        private $area;
        private $city;
        
        private $type_list;
        private $address_of;
        private $table;
        
        private $lst_region;
        private $lst_area;
        private $lst_city;
        
        private $db;
        private $error = array();
        
        
        function __construct($region='', $area='', $city='') {
            $this->db       = new db_local();
            $this->region   = $region;
            $this->area     = $area;
            $this->city     = $city;
            
            $this->type_list = array(
                                    array('title' => 'Permanent', 'value' => 'p'),
                                    array('title' => 'Mailing', 'value' => 'm'),
                                    array('title' => 'Home', 'value' => 'h'),
                                    array('title' => 'Work', 'value' => 'w'),
                                    array('title' => 'Other', 'value' => 'o')
                                    );
            
            $this->refresh();
        }
        
        function __destruct() {
            $this->region       = NULL;
            $this->area         = NULL;
            $this->city         = NULL;
            $this->lst_region   = NULL;
            $this->lst_area     = NULL;
            $this->lst_city     = NULL;
            $this->db           = NULL;
            $this->error        = NULL;
        }
        
        /**
         *   Function to get all the Records.
         */
        function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }
            
            $query .= " FROM ". TABLE_VENDORS_ADDRESS;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
                }
                
                return ( $db->nf() );
            }
            else {
                return false;
            }
        }

        
        function get($id='') {
            $list = NULL;
            if ( empty($id) ) {
                $condition = " WHERE table_name = '". $this->table ."'"
                        ." AND address_of = '". $this->address_of ."'"
                        ." ORDER BY address_type ASC";
                if ( $this->getList($this->db, $list, 'id,address,company_name,city,state,country,zipcode,address_type,is_preferred,is_verified', $condition)>0 ) {
                    foreach ( $list as $key=>$address ) {
                        $list[$key]['title'] = $this->getTypeTitle($address['address_type']);
                        $list[$key]['c_title'] = $this->getCountryName($address['country']);
                        $list[$key]['city_title'] = $this->getCityName($address['city']);
                        $list[$key]['state_title'] = $this->getStateName($address['state']);
                    }
                }
            }
            else {
                $condition = " WHERE table_name = '". $this->table ."'"
                                ." AND address_of = '". $this->address_of ."'"
                                ." AND id = '". $id ."'";
                if ( $this->getList($this->db, $list, 'id,address,company_name,city,state,country,zipcode,address_type,is_preferred,is_verified', $condition)>0 ) {
                    $list = $list[0];
                    $list['title']  = $this->getTypeTitle($list['address_type']);
                    $list['c_title']= $this->getCountryName($list['country']);
                    $list['city_title']= $this->getCityName($list['city']);
                    $list['state_title']= $this->getStateName($list['state']);
                }
                else {
                    $list = false;
                }
            }
            return ($list);
        }
        
        function getTypeList() {
            return ($this->type_list);
        }

        function getTypeTitle($type) {
            $type_index = array();
            array_key_search('value', $type, $this->type_list, $type_index);
            if ( count($type_index)<=0 ) {
                return ('');
            }
            else {
                return ($this->type_list[$type_index[0]]['title']);
            }
        }
        
        
        function clearErrors() {
            $this->error = array();
            return (true);
        }
        
        
        function delete(&$db, $id, $address_of='', $table='') {
            $query = "DELETE FROM ". TABLE_VENDORS_ADDRESS
                        ." WHERE id = '$id'"
                        ." AND is_preferred != '1'";
            if ( $address_of != '' && $table != '') {
                $query .= " AND table_name = '$table' "
                        ." AND address_of = '$address_of'";
            }

            if ( $db->query($query) && $db->affected_rows()>0 ) {
                return (true);
            }
            else {
                $this->clearErrors();
                $this->setError(11);
                return (false);
            }
        }
        
        
        function update($id, $table, $address) {
            if ( $address['is_preferred'] == '1' ) {
                $query = "UPDATE ". TABLE_VENDORS_ADDRESS
                        ." SET is_preferred = '0' "
                        ." WHERE is_preferred = '1' "
                        ." AND table_name = '". $table ."'"
                        ." AND address_of = '". $id ."'";
                $this->db->query($query);
            }
            
            
            if ( empty($address['id']) ) {
                $query = "INSERT INTO ". TABLE_VENDORS_ADDRESS
                        ." SET table_name = '". $table ."'"
                            .", address_of = '".    $id ."'"
                            .", company_name = '".   $address['company_name'] ."'"
                            .", address = '".       $address['address'] ."'"
                            .", city = '".          $address['city'] ."'"
                            .", state = '".         $address['state'] ."'"
                            .", country = '".       $address['country'] ."'"
                            .", zipcode = '".       $address['zipcode'] ."'"
                            .", address_type = '".  $address['address_type'] ."'"
                            .", is_preferred = '".  $address['is_preferred'] ."'"
                            .", is_verified = '".   $address['is_verified'] ."'";
            }
            else {
                $query = "UPDATE ". TABLE_VENDORS_ADDRESS
                            ." SET table_name = '". $table ."'"
                                .", address_of = '".    $id ."'"
                                 .", company_name = '".   $address['company_name'] ."'"
                                .", address = '".       $address['address'] ."'"                                
                                .", city = '".          $address['city'] ."'"
                                .", state = '".         $address['state'] ."'"
                                .", country = '".       $address['country'] ."'"
                                .", zipcode = '".       $address['zipcode'] ."'"
                                .", address_type = '".  $address['address_type'] ."'"
                                .", is_preferred = '".  $address['is_preferred'] ."'"
                                .", is_verified = '".   $address['is_verified'] ."'"
                            ." WHERE id = '". $address['id'] ."'";
            }
            if ( $this->db->query($query) ) {
                return (true);
            }
            else {
                $this->clearErrors();
                $this->setError(12);
                return (false);
            }
        }
        
        function refresh($region='', $area='', $city='') {
            if ( $region != '' ) {
                $region = $this->region;
            }
//            if ( $area != '' ) {
//                $area = $this->area;
//            }
//            if ( $city != '' ) {
//                $city = $this->city;
//            }
            $this->refreshCountryList();
            $this->refreshStateList();
            $this->refreshCityList();
        }
        
        
        function refreshCountryList($region='') {
            if ( $region == '' ) {
                $region = $this->region;
            }
            
            $query = 'SELECT * FROM '. TABLE_COUNTRIES
                        ." WHERE status = '1'"
                        .' ORDER BY title ASC';
            if ( $this->db->query($query) && ($this->db->nf() > 0) ) {
                while ( $this->db->next_record() ) {
                    $is_selected = 0;
                    if ( ($region == $this->db->f('code')) || ($region == $this->db->f('title')) ) {
                        $is_selected = 1;
                    }
                    $this->lst_region[$this->db->f('code')] = array(
                                                            'id'            => $this->db->f('id'),
                                                            'code'          => $this->db->f('code'),
                                                            'title'         => $this->db->f('title'),
                                                            'status'        => $this->db->f('status'),
                                                            'is_selected'   => $is_selected
                                                        );
                
                }
            }
        }
        
        function refreshStateList($region='') {
            
            if ( $region == '' ) {
                $region = $this->region;
            }
            
                $area = $this->area;
            
            $query = 'SELECT * FROM '. TABLE_LIST_STATE
                        ." WHERE status = '1'"
                        .' ORDER BY name ASC';
            if ( $this->db->query($query) && ($this->db->nf() > 0) ) {
                while ( $this->db->next_record() ) {
                    $is_selected = 0;
                    /*
                    if ( ($region == $this->db->f('id')) || ($region == $this->db->f('name')) ) {
                        $is_selected = 1;
                    }
                    */
                    if ( ($area == $this->db->f('id')) || ($area == $this->db->f('name')) ) {
                        $is_selected = 1;
                    }
                    
                    $this->lst_area[$this->db->f('id')] = array(
                                                            'id'            => $this->db->f('id'),
                                                            'name'         => $this->db->f('name'),
                                                            'country_code'  => $this->db->f('country_code'),
                                                            'country_id'  => $this->db->f('country_id'),
                                                            'status'        => $this->db->f('status'),
                                                            'is_selected'   => $is_selected
                                                        );
                }
            }
        }
        
        function refreshCityList($region='') {
            
            if ( $region == '' ) {
                $region = $this->region;
            }
           
            $city = $this->city;
           
            $query = 'SELECT * FROM '. TABLE_LIST_CITY
                        ." WHERE status = '1'"
                        .' ORDER BY name ASC';
            if ( $this->db->query($query) && ($this->db->nf() > 0) ) {
                 $is_selected = 0;
                 if ( ($city == "-1") || ($city == "Other") ) {
                        $is_selected = 1;
                 }
                 $this->lst_city["-1"] = array(
                                                            'id'           => "-1",
                                                            'name'         => "Other",
                                                            'state_id'     => "-1",
                                                            'country_code' => "-1",
                                                            'country_id'   => "-1",
                                                            'status'       => "1",
                                                            'is_selected'  => $is_selected
                                                        );
                while ( $this->db->next_record() ) {
                    $is_selected = 0;
                    /*
                    if ( ($region == $this->db->f('id')) || ($region == $this->db->f('name')) ) {
                        $is_selected = 1;
                    }*/
                    if ( ($city == $this->db->f('id')) || ($city == $this->db->f('name')) ) {
                        $is_selected = 1;
                    }
                    $this->lst_city[$this->db->f('id')] = array(
                                                            'id'           => $this->db->f('id'),
                                                            'name'         => $this->db->f('name'),
                                                            'state_id'     => $this->db->f('state_id'),
                                                            'country_code' => $this->db->f('country_code'),
                                                            'country_id'   => $this->db->f('country_id'),
                                                            'status'       => $this->db->f('status'),
                                                            'is_selected'  => $is_selected
                                                        );
                }
            }
        }
        
        function getCountryList() {
            return ($this->lst_region);
        }
        
        function getCountry($c_code) {
            return ($this->lst_region[$c_code]);
        }
        function getCountryName($c_code) {
            if(isset($this->lst_region[$c_code]['title'])){
                return ($this->lst_region[$c_code]['title']);
            }
        }
        
        function getCityName($c_code) {
            if(isset($this->lst_city[$c_code]['name'])){
                return ($this->lst_city[$c_code]['name']);
            }
        }
        
        function getStateName($c_code) {
            if(isset($this->lst_area[$c_code]['name'])){
                return ($this->lst_area[$c_code]['name']);
            }
        }
        
        function getStateList() {
            return ($this->lst_area);
        }
        
        function getCityList() {
            return ($this->lst_city);
        }
        
        function getError($code='') {
            if ( !empty($code) ) {
                return ($this->error[$code]);
            }
            else {
                return ($this->error);
            }
            
        }
        
        function setAddressOf($table='', $address_of='') {
            $this->table        = $table;
            $this->address_of   = $address_of;
        }
        
        function setRegion($region) {
            $this->lst_region = $region;
            return (true);
        }
        
        function setArea($area) {
            $this->area = $area;
            return (true);
        }
        
        function setCity($city) {
            $this->city = $city;
            return (true);
        }
        
        function setError($error) {
            switch ( $error ) {
                case (0): {
                    //$this->error[0] = array('code' => 0, 'description' => "The C/O is invalid.");
                    break;
                }
            
                case (1): {
                    $this->error[1] = array('code' => 1, 'description' => "The Address Type is invalid.");
                    break;
                }
                case (2): {
                    $this->error[2] = array('code' => 2, 'description' => "The Address field is empty.");
                    break;
                }
                case (3): {
                    $this->error[3] = array('code' => 3, 'description' => "The City field is empty.");
                    break;
                }
                case (4): {
                    $this->error[4] = array('code' => 4, 'description' => "The name of City is more than allowed 100 characters.");
                    break;
                }
                case (5): {
                    $this->error[5] = array('code' => 5, 'description' => "The State field is empty.");
                    break;
                }
                case (6): {
                    $this->error[6] = array('code' => 6, 'description' => "The name of State is more than allowed 100 characters.");
                    break;
                }
                case (7): {
                    $this->error[7] = array('code' => 7, 'description' => "The Country field is empty.");
                    break;
                }
                case (8): {
                    $this->error[8] = array('code' => 8, 'description' => "The name of Country is more than allowed 50 characters.");
                    break;
                }
                case (9): {
                    $this->error[9] = array('code' => 9, 'description' => "The Zip/Pin code field is empty.");
                    break;
                }
                case (10): {
                    $this->error[10] = array('code' => 10, 'description' => "The Zip/Pin code is more than allowed 16 characters.");
                    break;
                }
                case (11): {
                    $this->error[11] = array('code' => 11, 'description' => "The Address was not updated.");
                    break;
                }
                case (12): {
                    $this->error[12] = array('code' => 12, 'description' => "The Address was not deleted.");
                    break;
                }
            }
        }
        
        
        function validate($type,$company_name, $address, $city, $area, $region, $zip) {
            $this->clearErrors();
            /*
            if ( !isset($company_name) || empty($company_name) ) {
                $this->setError(0);
            }
            */
            $type_index = array();
            array_key_search('value', $type, $this->type_list, $type_index);
            if ( count($type_index)<=0 ) {
                $this->setError(1);
            }
                        
            if ( !isset($address) || empty($address) ) {
                $this->setError(2);
            }
            
            if ( !isset($city) || empty($city) ) {
                $this->setError(3);
            }
            elseif ( strlen($city)>100 ) {
                $this->setError(4);
            }
            /*
            if ( !isset($area) || empty($area) ) {
                $this->setError(6);
            }
            elseif ( strlen($area)>100 ) {
                $this->setError(7);
            }
            */
            if ( !isset($region) || empty($region) ) {
                $this->setError(5);
            }
            elseif ( strlen($region)>50 ) {
                $this->setError(6);
            }
            /*
            if ( !isset($zip) || empty($zip) ) {
                $this->setError(9);
            }
            elseif ( strlen($zip)>16 ) {
                $this->setError(10);
            }
            */
            if ( (count($this->error)) > 0 ) {
                return (false);
            }
            else {
                return (true);
            }
        }
    }

?>