<?php
	
	class Validation {	
		
		/*
			Currency accpets
				 100,000,000.00
				 10,000,000.00
				 1,000,000.00
				 1000
		*/
		function isValidCurrency( $value ) {
			$value	= trim( $value );
			if( $value	== "" )
			{
				return (false);
			}
			//$pattern	= "/^(\d{1,3},?(\d{3},?)*)?(\.\d{2})?$/";
			$pattern	= "/^\d+(\.\d{2})?$/";
			if( preg_match( $pattern, $value ) ) {
				return ( true );
			}
			else{
				return( false);
			}
		}
		
		//Username atleast 7 characters long (it accepts a-zA-Z0-9 and _ only)
		function isValidUsername( $value ) {
			$value = trim( $value );
			if( preg_match( "/^[a-zA-Z]+\w{4,}[0-9a-zA-Z]+$/", $value )) {
				return (true);
			}
			else{
				return (false);
			}
		}
		//Phone No atleast 15 characters long (it accepts 0nly +,- and[0-9] @format : +91-712-2345678 )
		function isValidPhone( $value ) {
			$value = trim( $value );
			if( preg_match("/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/",  $value))
			{
				return (true);
			}
			else{
				return (false);
			}
		}
		//Username atleast 6 characters long (it accepts a-zA-Z0-9 only)
		function isValidPassword( $value ) {
			$value = trim( $value );
			if( preg_match( "/^[a-zA-Z0-9]{6,15}$/", $value )) {
				return (true);
			}
			else{
				return (false);
			}
		}
		
		function isEmpty( $value ) {		
			$value = trim( $value );
			if ( empty($value) || $value == "" ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		// Checks whether the value is alphabets
		function isAlphabets( $value ) {
			$value	= trim( $value );
			$value	= str_replace(" ","",$value);
			
			if( empty($value) || $value == "" ){
				return (false);
			}
			else {
				if( !ctype_alpha( $value ) ){
					return( false );
				}
				else{
					return (true);
				}
			}
		}
		
//		// Checks whether the value is correct full name or not
//		function isValidFullName( $value ) {
//			$value	= trim( $value );
//			$value	= str_replace(" ","",$value);
//			
//			if( preg_match( "/^[a-z]+$/", $value )) {
//				return (true);
//			}
//			else{
//				return (false);
//			}
//		}
		
		// Checks whether the value is numeric
		function isNumeric( $value ) {
			$value	= trim( $value );
			if( empty($value) || $value == "" ){
				return (false);
			}
			else {
				if( !ctype_digit( $value ) ){
					return( false );
				}
				else{
					return (true);
				}
			}
		}
		
		// Checks whether the value is alphanumeric
		function isAlphanumeric( $value ) {
			$value	= trim( $value );
			$value	= str_replace(" ","",$value);
			$value	= str_replace(".","",$value);
			
			if( empty($value) || $value == "" ){
				return (false);
			}
			else {
				if( !ctype_alnum( $value ) ){
					return( false );
				}
				else{
					return (true);
				}
			}
		}
		
		// Checks the validity for date passed
		// mindiff is in years
		function isValidAge( $value, $mindiff=0 )
		{
			$today_date	= mktime(0,0,0, date("m"), date("d"), date("Y"));
			$date_diff	= (int)floor((($today_date - $value)/84600)/365);
			if( $date_diff < $mindiff )	{
				return (false);
			}
			else{
				return (true);
			}
		}
		
		// Compares two strings
		function isEqual( $value1, $value2 )
		{
			$value1	= trim( $value1 );
			$value2	= trim( $value2 );
			if( strcmp( $value1 , $value2 ) != 0  ){
				return( false );
			}
			else{
				return (true);
			}
		}
		/******************************************************
		* Match the floating point numbers upto double 
		* smeerp. 
		* When $strict = false 
		* VALID   : 23, 23.06, 43., 34.9, .3, 0.43, 0.0
		* INVALID : 33.43534, .345, .
		*
		* When $strict = false & $smeerp = 3
		* VALID   : 23, 23.006,  34.999, .999, 0.343 
		* INVALID : 33.43534, .34, 43., .3, 0.43, 0.0 .
		*
		* @param $float_value 
		* 	The value that is to be checked.
		* @param $smeerp
		*	The number of decimal placed allowed.
		* @param $strict
		* 	If set to true then the number of digits after 
		*	decimal points should be $smeerp else the 
		*	number can be between 0 to $smeerp
		* @return boolean
		*	If the floating point is valid then return value
		* 	is true else false.
		******************************************************/
		function isValidFloat( $float_value, $smeerp = 2, $strict = false) {
			if ( $strict ) {
				$pattern = "/^\s*[$]?\s*\d+(\.\d{". $smeerp ."})?\s*$/";
			}
			else {
				$pattern = "/^\s*[$]?\s*\d+(\.\d{0, ". $smeerp ."})?\s*$/";
			}
			
			if (preg_match($pattern, $float_value)) {
				return (true);
			}
			else {
				return (false);
			}
		}
		

		function isValidInt( $int_value, $smeerp = 2, $strict = false) {
			$pattern = "/^[0-9]*$/";			
			if (preg_match($pattern, $int_value)) {
				return (true);
			}
			else {
				return (false);
			}
		}

		/******************************************************
		* Check the validity of the email
		* VALID   : pradeep.agnihotri@pretechno.com, 
		*			pradeep_agnihotri@rent.pretechno.com
		*			pradeepagnihotri@properties.pretechno.co.in
		*
		* INVALID : pradeep.agnihotri#pretechno.com
		*			pradeep.agnihotri@pretechno
		*
		* @param $email
		* 	The email address that is to be checked for
		* 	validity.
		* @return boolean
		*	If the email address is valid then return value
		* 	is true else false.
		******************************************************/
		function isValidEmail( $email ) {
			$pattern = "/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/";
			
			if (preg_match($pattern , $email)) {
				return (true);
			}
			else {
				return (false);
			}
		}
	}
?>