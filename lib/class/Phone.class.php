<?php
    class Phone {
    // Attributes of a Phone Number are as follows;
    // cc - Country Code
    // ac - Area Code
    // value - Number
    // Type - h = Home
    //      - w = Work
    //      - m = Mobile
    //      - f = Fax
    //      - p = Public Phone
        private $cc_list;
        private $ac_list;
        private $type_list;

        private $phone_of;
        private $table;


        function __construct($table='', $phone_of='') {
            $this->table    = $table;
            $this->phone_of = $phone_of;

            $this->type_list = array(
                                    array('title' => 'Home', 'value' => 'h'),
                                    array('title' => 'Work', 'value' => 'w'),
                                    array('title' => 'Mobile', 'value' => 'm'),
                                    array('title' => 'Fax', 'value' => 'f'),
                                    array('title' => 'Public Phone', 'value' => 'p'),
                                    array('title' => 'Other', 'value' => 'o')
                                    );
        }
        
        
        /**
        *   Function to get all the Pre Orders.
        *
        *   @param Object of database
        *   @param Array of User role
        *   @param required fields
        *   @param condition
        *   return array of User roles
        *   otherwise return NULL
        */      
        function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }
            
            $query .= " FROM ". TABLE_PHONE;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
                }
                
                return ( $db->nf() );
            }
            else {
                return false;
            }
        }

        
        function get(&$db) {
            $list = NULL;
            $condition = " WHERE table_name = '". $this->table ."'"
                            ." AND phone_of = '". $this->phone_of ."'"
                            ." ORDER BY p_type ASC";
            if ( $this->getList($db, $list, 'id,cc,ac,p_number,p_type,p_is_preferred,p_is_verified', $condition)>0 ) {
                foreach ( $list as $key=>$phone ) {
                    $list[$key]['title'] = $this->getTypeTitle($phone['p_type']);
                }
            }
            return ($list);
        }
        
        function getTypeList() {
            return ($this->type_list);
        }

        function getTypeTitle($type) {
            $type_index = array();
            array_key_search('value', $type, $this->type_list, $type_index);
            if ( count($type_index)<=0 ) {
                return ('');
            }
            else {
                return ($this->type_list[$type_index[0]]['title']);
            }
        }
        
        
        function delete(&$db, $id, $phone_of='', $table='') {
            $query = "DELETE FROM ". TABLE_PHONE
                        ." WHERE id = '$id'"
                        ." AND p_is_preferred != '1'";
            if ( $phone_of != '' && $table != '') {
                $query .= " AND table_name = '$table' "
                        ." AND phone_of = '$phone_of'";
            }
            
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                return (true);
            }
            else {
                $this->clearErrors();
                $this->setError(13);
                return (false);
            }
        }

        function update(&$db, $id, $table, $phone_data) {
            if ( $phone_data['p_is_preferred'] == '1' ) {
                $query = "UPDATE ". TABLE_PHONE
                        ." SET p_is_preferred = '0' "
                        ." WHERE p_is_preferred = '1' "
                        ." AND table_name = '". $table ."'"
                        ." AND phone_of = '". $id ."'";
                $db->query($query);
            }
            
            
            if ( empty($phone_data['id']) ) {
                $query = "INSERT INTO ". TABLE_PHONE
                        ." SET table_name = '".     $table ."'"
                            .", phone_of = '".      $id ."'"
                            .", cc = '".            $phone_data['cc'] ."'"
                            .", ac = '".            $phone_data['ac'] ."'"
                            .", p_number = '".      $phone_data['p_number'] ."'"
                            .", p_type = '".        $phone_data['p_type'] ."'"
                            .", p_is_preferred = '".$phone_data['p_is_preferred'] ."'"
                            .", p_is_verified = '". $phone_data['p_is_verified'] ."'";
            }
            else {
                $query = "UPDATE ". TABLE_PHONE
                        ." SET table_name = '". $table ."'"
                            .", phone_of = '".      $id ."'"
                            .", cc = '".            $phone_data['cc'] ."'"
                            .", ac = '".            $phone_data['ac'] ."'"
                            .", p_number = '".      $phone_data['p_number'] ."'"
                            .", p_type = '".        $phone_data['p_type'] ."'"
                            .", p_is_preferred = '".$phone_data['p_is_preferred'] ."'"
                            .", p_is_verified = '". $phone_data['p_is_verified'] ."'"
                        ." WHERE id = '". $phone_data['id'] ."'";
            }
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                $this->clearErrors();
                $this->setError(11);
                return (false);
            }
        }


        function clearErrors() {
            $this->error = array();
            return (true);
        }


        function getError($code='') {
            if ( !empty($code) ) {
                return ($this->error[$code]);
            }
            else {
                return ($this->error);
            }
        }


        function setError($error) {
            switch ( $error ) {
                case (1): {
                    $this->error[1] = array('code' => 1, 'description' => 'The Type of the Phone is not specified.');
                    break;
                }
                case (2): {
                    $this->error[2] = array('code' => 2, 'description' => 'The Type of the Phone is invalid.');
                    break;
                }
                case (3): {
                    $this->error[3] = array('code' => 3, 'description' => 'The Country Code is not specified.');
                    break;
                }
                case (4): {
                    $this->error[4] = array('code' => 4, 'description' => 'The length of Country Code is more than allowed 4 characters.');
                    break;
                }
                case (5): {
                    $this->error[5] = array('code' => 5, 'description' => 'The Country Code is invalid.');
                    break;
                }
                case (6): {
                    $this->error[6] = array('code' => 6, 'description' => 'The Area Code is not specified.');
                    break;
                }
                case (7): {
                    $this->error[7] = array('code' => 7, 'description' => 'The length of Area Code is more than allowed 7 characters.');
                    break;
                }
                case (8): {
                    $this->error[8] = array('code' => 8, 'description' => 'The Area Code is invalid.');
                    break;
                }
                case (9): {
                    $this->error[9] = array('code' => 9, 'description' => 'The Number is not specified.');
                    break;
                }
                case (10): {
                    $this->error[10] = array('code' => 10, 'description' => 'The length of Number is more than allowed 10 characters.');
                    break;
                }
                case (11): {
                    $this->error[11] = array('code' => 11, 'description' => 'The Number is invalid.');
                    break;
                }
                case (12): {
                    $this->error[12] = array('code' => 12, 'description' => 'The Number is not valid Indian Mobile Number.');
                    break;
                }
                case (13): {
                    $this->error[13] = array('code' => 13, 'description' => 'The Phone was not deleted.');
                    break;
                }
            }
        }
        
        
        function setPhoneOf($table='', $phone_of='') {
            $this->table    = $table;
            $this->phone_of = $phone_of;
        }
        
        function validate($type, $cc, $ac, $number) {
            $this->clearErrors();
            
            if ( empty($type) ) {
                $this->setError(1);
            }
            else {
                $type_index = array();
                array_key_search('value', $type, $this->type_list, $type_index);
                if ( count($type_index)<=0 ) {
                    $this->setError(2);
                }
            }
            
            if ( !isset($cc) || empty($cc) ) {
                $this->setError(3);
            }
            elseif ( strlen($cc)>4 ) {
                $this->setError(4);
            }
//            else {
//                // Check for validity of the Country Code.
//            }
            
            if ( (!isset($ac) || $ac == '') && ($type != 'm') ) {
                $this->setError(6);
            }
            elseif ( strlen($ac)>7 ) {
                $this->setError(7);
            }
//            else {
//                // Check for validity of the Area Code.
//            }
           
            if ( !isset($number) || empty($number) ) {
                $this->setError(9);
            }
            elseif ( strlen($number)>10 ) {
                $this->setError(10);
            }
            else {
                // Check for validity of the Number.
                // At the moment only Indian Mobile Numbers are validated,
                // others are assumed to be correct.
                /* if ( $type == 'm' && $cc == '91') {
                    if ( !preg_match('/^9+[0-9]{9}$/', $number) ) {
                        $this->setError(12);
                    }
                } */
            }
             
            if ( (count($this->error)) > 0 ) {
                return (false);
            }
            else {
                return (true);
            }
        }
        
        
    }
 ?>

