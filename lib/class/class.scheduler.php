<?Php
	
	class Scheduler
	{
		var $sendsmstime;
			
		function __construct()
		{
			 $this->sendsmstime = date("Y-m-d H:i:00");
		}

		function getMessageslist(&$list)
		{
			$db = new db_local;
		
			$sql = "SELECT * FROM ".TABLE_SMS_SCHEDULER." WHERE s_datenext < '".$this->sendsmstime."'";

			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
					$res[] = $db->Record;

				$list = $res;
				return true;
			}
		}

		function resetScheduler($list)
		{
			$db = new db_local;

			for($j=0;$j<count($list);$j++)
			{
				$nextdt = $this->getDate($list[$j]["s_repeat"],$list[$j]["s_scheduledate"],$list[$j]["s_interval"],$list[$j]["s_intervaltype"]);

				//reduce the interval			
				if($list[$j]["s_repeat"] == "B")
				{
		            if(preg_match("/,/",$list[$j]["s_interval"]))
						$intv = split(",",$list[$j]["s_interval"]);
					else
						$intv[] = $list[$j]["s_interval"];

					$a = "";	
					for($i=1;$i<count($intv);$i++)
					{
						if($a != "")
							$a .= ",";
						$a .= $intv[$i];
					}
				}
				
				//with this dt,a value run the update query to update the s_datenext,s_interval field in the database;
				$sql = "UPDATE ".TABLE_SMS_SCHEDULER." SET s_datenext  = '".$nextdt."' ";

				if($list[$j]["s_repeat"] == "B")
					$sql .= ",s_interval = '".$a."'"; 

				$sql .= " WHERE s_id='".$list[$j]["s_id"]."'";
				if($db->query($sql))
				{
					$sql = "DELETE FROM ".TABLE_SMS_SCHEDULER." WHERE s_datenext < '".$this->sendsmstime."'";
					$db->query($sql);	
				}
			}
		}

		function getDate($repeat,$scheduledate,$interval,$intervaltype)
		{
			$dt = preg_split("/-/",$scheduledate);//split the date
			$sp	= preg_split("/\s/",$dt[2]);//split with space after the day 
			$tm = preg_split("/:/",$sp[1]);//split the time

			if(preg_match("/,/",$interval))
				$int1 = split(",",$interval);
			else
			{
				$int1 = array();	
				$int1[] = $interval;
			}
			
			$int = $int1[0];

			if($repeat == "B")
			{
				if($intervaltype == "day")
					$schedt = mktime($tm[0],$tm[1],0,date($dt[1]),date($sp[0])-$int,date($dt[0]));				
				else if($intervaltype == "month")
					$schedt = mktime($tm[0],$tm[1],0,date($dt[1])-$int,date($sp[0]),date($dt[0]));				
				else if($intervaltype == "year")
					$schedt = mktime($tm[0],$tm[1],0,date($dt[1]),date($sp[0]),date($dt[0])-$int);				
				else if($intervaltype == "hour")
					$schedt = mktime($tm[0]-$int,$tm[1],0,date($dt[1]),date($sp[0]),date($dt[0]));				
				else if($intervaltype == "min")
					$schedt = mktime($tm[0],$tm[1]-$int,0,date($dt[1]),date($sp[0]),date($dt[0]));				
			}
			elseif($repeat == "E")
			{
				if($intervaltype == "day")
					$schedt = mktime($tm[0],$tm[1],0,date($dt[1]),date($sp[0])+$int,date($dt[0]));				
				else if($intervaltype == "month")
					$schedt = mktime($tm[0],$tm[1],0,date($dt[1])+$int,date($sp[0]),date($dt[0]));				
				else if($intervaltype == "year")
					$schedt = mktime($tm[0],$tm[1],0,date($dt[1]),date($sp[0]),date($dt[0])+$int);				
				else if($intervaltype == "hour")
					$schedt = mktime($tm[0]+$int,$tm[1],0,date($dt[1]),date($sp[0]),date($dt[0]));				
				else if($intervaltype == "min")
					$schedt = mktime($tm[0],$tm[1]+$int,0,date($dt[1]),date($sp[0]),date($dt[0]));
			}

			return date("Y-m-d H:i:00",$schedt);
		}
	}	
?>

