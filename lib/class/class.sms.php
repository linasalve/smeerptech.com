<?php
    class sms
    {
		var $percision_username;
		var $percision_password;
        var $username;
        var $password;
        var $posturl;
    
        var $phoneno = array();
        var $errorphone;
        var $message = '';
        var $db;
        var $balance;
        var $clientid;
		var $userid; 
		var $daysleft;   
		var $expirydt;
		var $displayname;

		//api variables
	    var $host;
	    var $service_url;
		var $msgresponse;
		var $guid;

		function setAttribute($user,$pass,$post_url)
		{
            $this->username 	= $user;
            $this->password 	= $pass;
            $this->posturl  	= $post_url; 
			$this->host			= 'api.myvaluefirst.com';
			$this->service_url  = '/psms/servlet/psms.Eservice2';

			$db = new db_local;
			$this->db = $db;
		}

        /*Creates an array of phone numbers*/    
        /*Seperates valid phone number and invalid phone number*/
        function addRecipient($arrphone)
        {
            $validflg = 0;

            //check for valid phone number    
            for($i=0;$i<count($arrphone);$i++)
            {
                if(is_numeric($arrphone[$i]) && strlen($arrphone[$i]) == 12 && substr($arrphone[$i],0,2) == 91 && substr($arrphone[$i],2,1) == 9)
                {
                    if($validphone != '')
                        $validphone .= ',';
                    $validphone .= $arrphone[$i];
                    $validflg = 1;
                }
                else
                {
                    if($invalidphone != '')
                        $invalidphone .= ',';
                    $invalidphone .= $arrphone[$i];
                }    
            }
    
            if(preg_match("/,/",$validphone))
                $phone = explode(",",$validphone);
            else
                $phone[] = $validphone;

            if($validflg == 1)
                $this->phoneno = $phone;
            
            $this->errorphone = $invalidphone;

            return $invalidphone;
        }
    
		function setScheduler($getval)
		{
            $this->setMessage($getval["mess"]);
            
            $errmesg = $this->checkValidity();
            if($errmesg == '0')    
            {
                //get the array of phone number to send sms
				$arrphone = $_GET["arrphone"];
                $errph = $this->addRecipient($arrphone);

                //count number of valid phone numbers to send sms
                $countphone = count($this->phoneno);
                if($countphone > 0)                                     
                {
                    //check for balance before fetching data to database
	                if($countphone > $this->balance)
					{
						$errmsg["code"] = 2;
						$errmsg["numbal"] = $this->balance;
		                $response = '$errmsg["code"] = 2;';
		                $response .= '$errmsg["numbal"] = "'.$this->balance.'";';
                        return $response;//"Only ".$this->balance." messages can be send";
					}

					$scheduledt = $getval["yr"]."-".$getval["mnth"]."-".$getval["day"]." ".$getval["hr"].":".$getval["min"];
					$dt = strtotime($scheduledt);

					$expdt = strtotime($this->expirydt);
					if($getval["interval"] != "")
					{
						if($getval["repeat"] == "E")
						{
							if($getval["interval_type"] == "day")
								$schedt = mktime($getval["hr"], $getval["min"], 0, date($getval["mnth"]), date($getval["day"])+$getval["interval"], date($getval["yr"]));
							else if($getval["interval_type"] == "month")
								$schedt = mktime($getval["hr"], $getval["min"], 0, date($getval["mnth"])+$getval["interval"], date($getval["day"]), date($getval["yr"]));
							else if($getval["interval_type"] == "year")
								$schedt = mktime($getval["hr"], $getval["min"], 0, date($getval["mnth"]), date($getval["day"]), date($getval["yr"])+$getval["interval"]);
							else if($getval["interval_type"] == "hour")
								$schedt = mktime(($getval["hr"]+$getval["interval"]), $getval["min"], 0, date($getval["mnth"]), date($getval["day"]), date($getval["yr"]));
							else if($getval["interval_type"] == "min")
								$schedt = mktime($getval["hr"], ($getval["min"]+$getval["interval"]), 0, date($getval["mnth"]), date($getval["day"]), date($getval["yr"]));

							if($expdt < $schedt)
							{
								
								$errmsg["code"] = 11;
		                        //$arr[0] = "You cannot schedule you scheduler on this date because your account is expiring before this date. So kindly re-set your date.";
		                        return '$errmsg["code"] = 11;';
							}
						}
						elseif($getval["repeat"] == "B")
						{
							$schedt = $this->getDate($getval);
							$intv = split(",",$getval["interval"]);
							$a = "";	
							for($i=1;$i<count($intv);$i++)
							{
								if($a != "")
									$a .= ",";
								$a .= $intv[$i];
							}	

							$getval["interval"] = $a;
						}
						
						if(time() > $schedt)	
						{
							$errmsg["code"] = 13;
	                        //$arr[0] = "You cannot schedule you scheduler on this date because your first interval is going before the current date. So kindly re-set your date.";
	                        return '$errmsg["code"] = 13;';
						}
	
					}
					else					
						$schedt = strtotime($scheduledt);

                    //display invalid phone number with error message to user
                    if(strlen($errph) > 0)
                    {
						$errmsg["code"] = 3;
						$errmsg["phonenum"] = $this->errorphone;
		                $response = '$errmsg["code"] = 3;';
		                $response .= '$errmsg["phonenum"] = "'.$this->errorphone.'";';
/*                            $arr[0] = "Invalid phone numbers. Cannot send messages to these number.";
                        $arr[1] = $this->errorphone;*/
                        return $response;
                    }
                    else
                    {
	                    foreach($this->phoneno as $key=>$value)
	                    {
							$sql = "INSERT INTO  ".TABLE_SMS_SCHEDULER." (s_userid,s_scheduledate,s_repeat,s_interval,s_message,
									s_mobileno,s_username,s_password,s_posturl,s_datenext,s_intervaltype) VALUES ('".$this->userid."',
									'".$scheduledt."','".$getval["repeat"]."','".$getval["interval"]."','".$this->message."',
									'".$value."','".$this->username."','".$this->password."','".$this->posturl."',
									'".date("Y-m-d H:m:s",$schedt)."','".$getval["interval_type"]."')";
							$this->db->query($sql);
							
	                    }
                    	//reset the phone number array and message text variable
	                    $this->reset();
						$errmsg["code"] = 1;
                        //$arr[0] = "Request completed successfully.";
                        return '$errmsg["code"] = 1;';
                    }
                }
                else
                {
                    $this->reset();    
					$errmsg["code"] = 5;
                    return '$errmsg["code"] = 5;';//"Invalid phone number.";
                }
            }
            else
            {
                $this->reset();
                return $errmesg;//error message return by the checkvalidity function
            }
		}

		function getDate($getval)
		{
			$int = split(",",$getval["interval"]);
			rsort($int);
			
			$interval = $int[0];

			if($getval["interval_type"] == "day")
				$schedt = mktime($getval["hr"], $getval["min"], 0, date($getval["mnth"]), date($getval["day"])-$interval, date($getval["yr"]));
			else if($getval["interval_type"] == "month")
				$schedt = mktime($getval["hr"], $getval["min"], 0, date($getval["mnth"])-$interval, date($getval["day"]), date($getval["yr"]));
			else if($getval["interval_type"] == "year")
				$schedt = mktime($getval["hr"], $getval["min"], 0, date($getval["mnth"]), date($getval["day"]), date($getval["yr"])-$interval);
			else if($getval["interval_type"] == "hour")
				$schedt = mktime(($getval["hr"]-$interval), $getval["min"], 0, date($getval["mnth"]), date($getval["day"]), date($getval["yr"]));
			else if($getval["interval_type"] == "min")
				$schedt = mktime($getval["hr"], ($getval["min"]-$interval), 0, date($getval["mnth"]), date($getval["day"]), date($getval["yr"]));

			return $schedt;
		}

        /*set the user entered message with the class file variable*/
        function setMessage($msg)
        {
            $this->message = $msg;
        }
    
        /*reset the phone number array and the text message variable*/    
        function reset()
        {
            $this->phoneno = NULL;
            $this->message = NULL;
        }
    
        /*fetches the sms data into database and send sms*/
        function sendSms($arrphone,$message)
        {
            $this->setMessage($message);
            
            $errmesg = $this->checkValidity();
            if($errmesg == '0')    
            {
                //get the array of phone number to send sms
                $errph = $this->addRecipient($arrphone);

                //count number of valid phone numbers to send sms
                $countphone = count($this->phoneno);
                if($countphone > 0)                                     
                {
                    //check for balance before fetching data to database
	                if($countphone > $this->balance)
					{
						$errmsg["code"] = 2;
						$errmsg["numbal"] = $this->balance;
		                $response = '$errmsg["code"] = 2;';
		                $response .= '$errmsg["numbal"] = "'.$this->balance.'";';
                        return $response;//"Only ".$this->balance." messages can be send";
					}

					if($this->daysleft < 0)
					{
						$errmsg["code"] = 10;
						$errmsg["daysleft"] = $this->daysleft;
		                $response = '$errmsg["code"] = 10;';
		                $response .= '$errmsg["daysleft"] = "'.$this->daysleft.'";';
	                    return $response;//"Account expired";						
					}
					
					//get the username and password of the smeerp
					$this->smeerpDetails();

                    $sql = "INSERT INTO ".TABLE_SMS_HISTORY." (sh_userid,sh_message,sh_count,sh_date) VALUES('".$this->userid."',
                            '".$this->message."','".$countphone."','".date("Y-m-d")."')";
                    if($this->db->query($sql))
                    {
						$sendflg = 0;
                        $id = mysql_insert_id(); 
                        foreach($this->phoneno as $key=>$value)
                        {
                            $query = "INSERT INTO ".TABLE_SMS_HISTORY_PHONE." (shp_shid,shp_phoneno,shp_status) VALUES('".$id."',
                                      '".$value."','0')";
                            $this->db->query($query);
							$childid = mysql_insert_id();
							//send sms through the api
                            $pushsms = $this->pushSms($this->percision_username,$this->percision_password,$this->displayname,$value,$this->message);
                            if($pushsms)        
                            {
								//flag is set to 1 because sms is relayed
								$sendflg = 1;

								//update table with message id
                                $sql = "UPDATE ".TABLE_SMS_HISTORY
                                                ." SET sh_messageid='". $this->guid ."'"
                                                .", response = '". $this->msgresponse ."' "
                                            ." WHERE sh_id='".$id."'";                                
								$this->db->query($sql);

								//update table with status
                                $sql = "UPDATE ".TABLE_SMS_HISTORY_PHONE." SET shp_status='1' WHERE shp_phoneno='".$value."' AND 
                                        shp_status='0' AND shp_shid='".$id."'";
                                $this->db->query($sql);

								//update table by reducing the balance sms								
                                $sql = "UPDATE ".TABLE_SMS_CLIENT_ACC." SET ca_balance = (ca_balance -1),
                                        ca_consumedsms = (ca_consumedsms + 1) WHERE 
                                        ca_id='".$this->clientid."'";
                                $this->db->query($sql);
	
								//update table by incrementing the number of consumed sms	
                                $sql = "UPDATE ".TABLE_SMS_MASTER_ACC." SET ma_consumedsms = (ma_consumedsms + 1)";
                                $this->db->query($sql);
                            }
							else
							{
								$sql = "DELETE FROM ".TABLE_SMS_HISTORY_PHONE." WHERE shp_id='".$childid."'";	
								$this->db->query($sql);
							}
                        }

						//no messages are send then delete the record from the master table		
						if($sendflg == 0)	
						{
							$sql = "DELETE FROM ".TABLE_SMS_HISTORY." WHERE sh_id='".$id."'";	
							$this->db->query($sql);
							$errmsg["code"] = 14;
                            //$arr[0] = "Cannot send msg. Error in relaying messages.";
                            return '$errmsg["code"] = 14;';
						}

                        //reset the phone number array and message text variable
                        $this->reset();
                        //display invalid phone number with error message to user
                        if(strlen($errph) > 0)
                        {
							$errmsg["code"] = 3;
							$errmsg["phonenum"] = $this->errorphone;
			                $response = '$errmsg["code"] = 3;';
			                $response .= '$errmsg["phonenum"] = "'.$this->errorphone.'";';
/*                            $arr[0] = "Invalid phone numbers. Cannot send messages to these number.";
                            $arr[1] = $this->errorphone;*/
                            return $response;
                        }
                        else
                        {
							$errmsg["code"] = 1;
                            //$arr[0] = "Message send successfully.";
                            return '$errmsg["code"] = 1;';
                        }
                    }
                    else 
                    {
                        $this->reset();    
						$errmsg["code"] = 4;
                        return '$errmsg["code"] = 4;';//"Cannot send messages.";
                    }
                }
                else
                {
                    $this->reset();    
					$errmsg["code"] = 5;
                    return '$errmsg["code"] = 5;';//"Invalid phone number.";
                }
            }
            else
            {
                $this->reset();
                return $errmesg;//error message return by the checkvalidity function
            }
        }
    
		function smeerpDetails()
		{
			$sql = "SELECT ma_username,ma_password,ma_posturl FROM ".TABLE_SMS_MASTER_ACC;
			$this->db->query($sql);
			if($this->db->nf() > 0)
			{
				$this->db->next_record();
				$this->percision_username = $this->db->f("ma_username");
				$this->percision_password = $this->db->f("ma_password");
			}	
		}

        /*checks the validity of the client account*/
        function checkValidity()
        {
            $res = $this->getClientDetais();
            if($res != -1)
            {
                $this->balance     = $res[0]["ca_balance"];
				$this->userid      = $res[0]["ca_userid"];
				$this->daysleft    = $res[0]["daysleft"];
				$this->expirydt    = $res[0]["ca_expirydate"];
				$this->displayname = $res[0]["ca_displayname"];

                if($res[0]["ca_balance"] <= 0)
				{
					$errmsg["code"] = 6;	
                    return '$errmsg["code"] = 6;';//"Cannot send SMS. Balance not available";
				}
                elseif($res[0]["ca_status"] == '0')
				{
					$errmsg["code"] = 7;
                    return '$errmsg["code"] = 7;';//"Invalid account";
				}
                else
                {
                    $this->clientid = $res[0]['ca_id'];    
                    return 0;
                }
            }
            else
            {
                $this->reset();
				$errmsg["code"] = 8;
                return '$errmsg["code"] = 8;'; //"Account does not exist.";
            }
        }
    
        /*send sms*/
        function pushSms($VF_USERNAME,$VF_PASSWORD,$DISPLAY_NAME,$receiver,$message)
        {
			$this->msgresponse = 'HTTP/1.1 200 OK
				Date: Tue, 31 Jul 2007 12:45:32 GMT
				Server: Apache/2.2.3 (Unix) DAV/2 mod_jk/1.2.19 PHP/5.2.1
				X-Powered-By: Servlet 2.4; JBoss-4.0.5.GA (build: CVSTag=Branch_4_0 date=200611200314)/Tomcat-5.5
				Content-Length: 164
				Connection: close
				Content-Type: text/plain;charset=ISO-8859-1

				<?xml version="1.0" encoding="ISO-8859-1"?><MESSAGEACK><GUID GUID="c48a8769-f20b-4b05-a876-934d7d5cb98d" 					SUBMITDATE="2007-7-31 18:15:32" ID="1"></GUID></MESSAGEACK>';
			$this->guid = "c48a8769-f20b-4b05-a876-934d7d5cb98d";
			return true;
/*	        $data = '<?xml version="1.0" encoding="ISO-8859-1"?>'
	                    .'<!DOCTYPE MESSAGE SYSTEM "http://127.0.0.1/psms/dtd/messagev12.dtd" >'
	                    .'<MESSAGE VER="1.2">'
	                    .'<USER USERNAME="'. $VF_USERNAME .'" PASSWORD="'. $VF_PASSWORD .'"/>'
	                    .'<SMS UDH="0" CODING="1" TEXT="'. $message .'" PROPERTY="0" ID="1">'
	                    .'<ADDRESS FROM="'. $DISPLAY_NAME. '" TO="'. $receiver .'" SEQ="1" TAG="some clientside random data" />'
	                    .'</SMS>'
	                    .'</MESSAGE>';

			$data 	= 'data='. ($data);
			$action = 'action=send';
			$this->msgresponse = $this->httpConnection($this->host, $this->service_url, "post", $data.'&'.$action);
			if($this->msgresponse != -1)
			{
				$this->guid = $this->parseXMLResponse($this->msgresponse);
				if($this->guid != -1)
		            return true;
				else
					return false;
			}
			else
				return false;*/
        }
    
        /*reterive the client details*/
        function getClientDetais()
        {
            $sql = "SELECT ca_id,ca_userid,ca_username,ca_password,ca_posturl,ca_allotedsms,ca_consumedsms,ca_balance,
                    ca_status,ca_expirydate,DATEDIFF(ca_expirydate,CURRENT_DATE) as daysleft,ca_displayname FROM ".TABLE_SMS_CLIENT_ACC." WHERE ca_username='".$this->username."' AND 
                    ca_password='".$this->password."'";

            $this->db->query($sql);
            if($this->db->nf() > 0)
            {
                while($this->db->next_record())
                    $res[] = $this->db->Record;

                return $res;
            }
            else
                return -1;
        }

		function subscriberBal()
		{
            $sql = "SELECT ca_balance,ca_expirydate,DATEDIFF(ca_expirydate,CURRENT_DATE) as daysleft FROM ".TABLE_SMS_CLIENT_ACC." WHERE ca_username='".$this->username."' AND ca_password='".$this->password."'";
//return $sql;
            $this->db->query($sql);
            if($this->db->nf() > 0)
            {
	            $this->db->next_record();
                $res["bal"]      = $this->db->f("ca_balance");
				$res["expdt"]    = $this->db->f("ca_expirydate");
				$res["daysleft"] = $this->db->f("daysleft");

				$arr["bal"]	     = $res["bal"];
				$arr["expdt"]    = $res["expdt"];
				$arr["daysleft"] = $res["daysleft"];

				$response  = '$arr["bal"]="'.$res["bal"].'";';
				$response .= '$arr["expdt"]="'.$res["expdt"].'";';				
				$response .= '$arr["daysleft"]="'.$res["daysleft"].'";';

				return $response;
            }
		}

		function parseXMLResponse($response)
		{
			//for error codes
			preg_match_all('|<ERROR SEQ="(.*) CODE="(.*)" />|U', $response, $res);
			if($res[2][0] != "")
				return -1;

			//for sms send without error codes
			preg_match_all('|GUID="(.*)"[\s]*SUBMITDATE|U', $response, $res);
			return $res[1][0];
		}

// ------------------------- SMS API--------------------
		function httpConnection($host, $service_url, $method, $data) {
			$response = '';
			$fp = fsockopen("$host", "80", $errno, $errstr, $timeout = 30);

			if (!$fp){
				//error tell us
				return -1;
				//return ("$errstr ($errno)\n");
			}
			else {
				//send the server request
				fputs($fp, "POST $service_url HTTP/1.1\r\n");
				fputs($fp, "Host: $host\r\n");
				fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
				fputs($fp, "Content-length: ".strlen($data)."\r\n");
				fputs($fp, "Connection: close\r\n\r\n");
				fputs($fp, $data . "\r\n\r\n");
		
				//loop through the response from the server
				while(!feof($fp)) {
					//fgets($fp, 4096);
	                $response .= fgets($fp, 4096);
				}
				//close fp - we are done with it
				fclose($fp);
			}
			return ($response);
		}
        
        
        function getMessageStatus($guid) 
		{
            $data   = '<?xml version="1.0" encoding="ISO-8859-1"?>'
                    .'<!DOCTYPE Messaging SYSTEM "http://127.0.0.1/psms/dtd/requeststatus.dtd" >'
                    .'<STATUSREQUEST>'
                    .'<USER USERNAME="'. $this->username .'" PASSWORD="'. $this->password .'"/>'
                    .'<GUID GUID="'. $guid .'">'
                    .'<STATUS SEQ="1" />'
                    .'<STATUS SEQ="2" />'
                    .'</GUID>'
                    .'<GUID GUID="'. $guid .'">'
                    .'</GUID>'
                    .'</STATUSREQUEST>';

            $data   = 'data='. ($data);
            $action = 'action=status';
        
            $response = $this->httpConnection($this->host, $this->service_url, "post", $data.'&'.$action);
            
            return ($response);
        }

		function getCreditBalance()
		{
	        $data = '<?xml version="1.0" encoding="ISO-8859-1"?>'
	                    .'<!DOCTYPE REQUESTCREDIT SYSTEM "http://127.0.0.1/psms/dtd/requestcredit.dtd">'
	                    .'<REQUESTCREDIT USERNAME="'. $this->username .'" PASSWORD="'. $this->password .'">'
	                    .'</REQUESTCREDIT>';
	        
			$data 	= 'data='. ($data);
			$action = 'action=Credits';
			$response = $this->httpConnection($this->host, $this->service_url, "post", $data.'&'.$action);

			return ($response);
		}
    }
?>
