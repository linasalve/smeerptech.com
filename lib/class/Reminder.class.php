<?php
/**
 * Requirements : 	PHP5, MySql 4.0+, Scheduler on Windows Server / CRON on *nix servers
 *					Messages Class
 * 
 */

if ( defined("__REMINDER_CLASS__")) return;
define("__REMINDER_CLASS__", 1);

class Reminder {
	// Define the properties of the Class
	private $id; // Variable to hold the unique ID of the reminder entry.
	private $type;
	private $title;
	private $text;
	private $url;
	private $to_email;
	private $date_next;
	private $repeat_every;
	private $repeat_for;
	private $repeat_type;
	private $repeat_upto;
	private $earlier_date_allowed;
	
	private $reminder_from	= array('name' => 'Reminders', 'email' => 'reminders@localhost.com');

	private $reminder_type = array(	'rem_inv_exp' 	=> 'Invoice Expiry',
									'rem_inv_bal' 	=> 'Invoice Balance', 
									'rem_bday' 		=> 'Birthday Reminder',
									'rem_evt' 		=> 'Event Reminder',
									'rem_met' 		=> 'Meeting Reminder',
									'rem_oth' 		=> 'Other Reminder',
								);
								
	private $interval_type = array(	'year' 		=> 'Year',
									'month' 	=> 'Month', 
									'day' 		=> 'Day',
									'hour' 		=> 'Hour',
									'min' 		=> 'Minute',
									'sec' 		=> 'Second',
								);
	
	private $db;
	private $table;
	private $message; // object of Message class
	private $timestamp; // UNIX (bigint) | MYSQL (YYYY-MM-DD HH:MM:SS)
	
	
	function __construct( $db, $table, &$message, $timestamp='UNIX' ) {
		$this->db 					= $db;
		$this->table 				= $table;
		$this->message 				= $message;
		$this->timestamp 			= $timestamp;
		$this->earlier_date_allowed = false;
	}

	function __destruct() {}
	
	
	/**
	 * Fucntion to retrieve the list of the entries from the database 
	 * table, depending on the conditions set.
	 *
	**/
	public function getList(&$list, $required_fields="", $condition="", $from="", $rpp="") {
		$query = "SELECT DISTINCT ";
		
		if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
			$query .= implode(",", $required_fields);
		}
		elseif ( !empty($required_fields) ) {
			$query .= " ". $required_fields;
		}
		else {
			$query .= " * ";
		}
		
		$query .= " FROM ". $this->table;
		
		$query .= " ". $condition;

		if ( (!empty($from) || $from == 0 ) && ($from >= 0) && (!empty($rpp) || $rpp == 0 ) && ($rpp > 0) ) {
			$query .= " LIMIT ". $from .", ". $rpp;
		}

		if ( $this->db->query($query) ) {
			if ( $this->db->affected_rows() > 0 ) {
				while ($this->db->next_record()) {
					$list[] = clearData($this->db->Record);
				}
			}
			return ( $this->db->nf() );
		}
		else {
			return (-1);
		}

	}


	/**
	 * Fucntion to retrieve the mentioned fields for the entry 
	 * identified by the unique ID.
	 *
	**/
	public function get(&$details, $id, $required_fields="", $extra_condition="") {
		$query = "SELECT ";
		
		if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
			$query .= implode(",", $required_fields);
		}
		elseif ( !empty($required_fields) ) {
			$query .= " ". $required_fields;
		}
		else {
			$query .= " * ";
		}
		$query .= " FROM ". $this->table;
		
		$query .= " WHERE id = '". $id ."' ". $extra_condition;

		if ( $this->db->query($query) ) {
			if ( $this->db->affected_rows() > 0 ) {
				while ($this->db->next_record()) {
					$details = clearData($this->db->Record);
				}
			}
			return ( true );
		}
		else {
			return (-1);
		}

	}


	/**
	 * This function inserts one row of reminder data in to the Reminder table.
	 *
	 * @return	mixed	int		on success the ID of the last inserted row is returned.
	 *					boolean	on failure false is retuned.
	 *
	**/
	 public function add($id='', $type='', $title='', $text='', $url='', $to_email='', $date_next='0', 
					$repeat_every='0', $repeat_for='0', $repeat_type='', $repeat_upto='0', $is_html=1,$client_id,$inv_no) {
		
		$result	= false;
		
		// Check for empty fields.

		if ( empty($type) ) {
			$this->message->setErrorMessage('The <b>Type</b> of the Reminder cannot be empty.');
		}
		if ( empty($title) ) {
			$this->message->setErrorMessage('The <b>Title</b> of the Reminder cannot be empty.');
		}
		if ( empty($text) ) {
			$this->message->setErrorMessage('The <b>Description</b> of the Reminder cannot be empty.');
		}
		if ( empty($to_email) ) {
			$this->message->setErrorMessage('The <b>To</b> field of the Reminder cannot be empty.');
		}
		else {
			// Validate the email addresses.
			$to_str		= $to_email;
			$to_email	= '';
			$email_arr 	= explode(",", $to_str);
            
			$error = "" ;
			foreach ($email_arr as $key=>$item) {
				$name 	= trim(substr($item, 0, strrpos($item, "<")-1) ) ;
				$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;

				if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/" , $email)) {
					$this->message->setErrorMessage('The <b>To</b> field contains invalid emails.');
					break;
				}
				// Format the Receiver emails properly.
				$to_email .= $name .' <'. $email .'>,';
			}
			$to_email	= substr($to_email, 0, strlen($to_email)-1);
			$email_arr	= NULL;
			$error		= NULL;
			$to_str		= NULL;
		}
		if ( empty($date_next) ) {
			$this->message->setErrorMessage('The <b>Date</b> of the Reminder cannot be empty.');
		}
		elseif ( !$this->getEarlierDateAllowed() && $date_next <= time() ) {
			$this->message->setErrorMessage('The Reminder cannot be set for an earlier date.');
		}
		if ( !empty($repeat_every) && !array_key_exists($repeat_type, $this->getIntervalType()) ) {
			$this->message->setErrorMessage('The interval type is not selected.');
		}
		if ( !empty($repeat_for) && !array_key_exists($repeat_type, $this->getIntervalType()) ) {
			$this->message->setErrorMessage('The interval type is not selected.');
		}
		if ( !empty($repeat_for) && empty($repeat_upto) ) {
			$this->message->setErrorMessage('The expiry date of the Reminder is not set.');
		}
		elseif ( !$this->getEarlierDateAllowed() && !empty($repeat_for) && !empty($repeat_upto) && $repeat_upto <= time() ) {
			$this->message->setErrorMessage('The Reminder cannot be set for an earlier date.');
		}
		
		
		if ( $this->message->getErrorMessageCount() <= 0 ) {
			// Insert the data into the database table.
			 $query = "INSERT INTO ". $this->table
						." SET "
							." id = '". 			$id ."'"
							.", type = '". 			$type ."'"
							.", title = '". 		$title ."'"
							.", text = '". 			$text ."'"
							.", url = '". 			$url ."'"
							.", to_email = '". 		$to_email ."'"
							.", date_next = '". 	$date_next ."'"
							.", repeat_every = '". 	$repeat_every ."'"
							.", repeat_for = '". 	$repeat_for ."'"
							.", repeat_type = '". 	$repeat_type ."'"
							.", repeat_upto = '". 	$repeat_upto ."'"
							.", is_html = '". 		$is_html ."'"
							.", client_id = '". 	$client_id ."'"
							.", inv_no = '". 		$inv_no ."'";
			if ( $this->db->query($query) && $this->db->affected_rows()>0 ) {
				$this->message->setOkMessage("Reminder Added successfully.");
				$result = $this->db->last_inserted_id();
                //exit;
			}
			else {
				$this->message->setErrorMessage('Reminder was not set. Please try again later.');
			}
		}
		
		return ($result);
	}


	/**
	 * This function updates one specific row identified by the unique ID 
	 * of the entry in the Reminder table.
	 *
	 * @return	mixed	int		on success the ID of the last inserted row is returned.
	 *					boolean	on failure false is retuned.
	 *
	**/
	 public function update($id='', $type='', $title='', $text='', $url='', $to_email='', $date_next='0', 
							$repeat_every='0', $repeat_for='0', $repeat_type='', $repeat_upto='0', $is_html=1) {
		
		$result	= false;
		
		// Check if the entry is present in the database table or not?
		if ( $this->db->query("SELECT id FROM ". $this->table ." WHERE id = '$id'" ) && $this->db->nf()<=0 ) {
			// There is no entry in the database table with the specified ID.
			$this->message->setErrorMessage('Reminder not found: It may have been elapsed or deleted.');
		}
		
		// Check for empty fields.
		
		if ( empty($type) ) {
			$this->message->setErrorMessage('The <b>Type</b> of the Reminder cannot be empty.');
		}
		
		if ( empty($title) ) {
			$this->message->setErrorMessage('The <b>Title</b> of the Reminder cannot be empty.');
		}
		
		if ( empty($text) ) {
			$this->message->setErrorMessage('The <b>Description</b> of the Reminder cannot be empty.');
		}
		
		if ( empty($to_email) ) {
			$this->message->setErrorMessage('The <b>To</b> field of the Reminder cannot be empty.');
		}
		else {
			// Validate the email addresses.
			$to_str		= $to_email;
			$to_email	= '';
			$email_arr 	= explode(",", $to_str);
			$error = "" ;
			foreach ($email_arr as $key=>$item) {
				$name 	= trim(substr($item, 0, strrpos($item, "<")-1) ) ;
				$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;

				if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/" , $email)) {
					$this->message->setErrorMessage('The <b>To</b> field contains invalid emails.');
					break;
				}
				// Format the Receiver emails properly.
				$to_email .= $name .' <'. $email .'>,';
			}
			$to_email	= substr($to_email, 0, strlen($to_email)-1);
			$email_arr	= NULL;
			$error		= NULL;
			$to_str		= NULL;
		}
		
		if ( empty($date_next) ) {
			$this->message->setErrorMessage('The <b>Date</b> of the Reminder cannot be empty.');
		}
		elseif ( $date_next <= time() && !$this->getEarlierDateAllowed() ) {
			$this->message->setErrorMessage('The Reminder cannot be set for an earlier date.');
		}
		
		if ( !empty($repeat_every) && !array_key_exists($repeat_type, $this->getIntervalType()) ) {
			$this->message->setErrorMessage('The interval type is not selected.');
		}
		
		if ( !empty($repeat_for) && !array_key_exists($repeat_type, $this->getIntervalType()) ) {
			$this->message->setErrorMessage('The interval type is not selected.');
		}
		
		if ( !empty($repeat_for) && empty($repeat_upto) ) {
			$this->message->setErrorMessage('The expiry date of the Reminder is not set.');
		}
		elseif ( !empty($repeat_for) && !empty($repeat_upto) && $repeat_upto <= time() ) {
			$this->message->setErrorMessage('The Reminder cannot be set for an earlier date.');
		}
		
		
		if ( $this->message->getErrorMessageCount() <= 0 ) {
			// Insert the data into the database table.
			$query = "UPDATE ". $this->table
						." SET "
							." id = '". 			$id ."'"
							.", type = '". 			$type ."'"
							.", title = '". 		$title ."'"
							.", text = '". 			$text ."'"
							.", url = '". 			$url ."'"
							.", to_email = '". 		$to_email ."'"
							.", date_next = '". 	$date_next ."'"
							.", repeat_every = '". 	$repeat_every ."'"
							.", repeat_for = '". 	$repeat_for ."'"
							.", repeat_type = '". 	$repeat_type ."'"
							.", repeat_upto = '". 	$repeat_upto ."'"
							.", is_html = '". 		$is_html ."'"
						." WHERE id = '". $id ."' ";
			//echo "<br/>". $query;
			if ( $this->db->query($query) && $this->db->affected_rows()>0 ) {
				$this->message->setOkMessage("Reminder Updated successfully.");
				$result = $this->db->last_inserted_id();
			}
			else {
				$this->message->setErrorMessage("Reminder was not Updated. Please try again later.");
			}
		}
		
		return ($result);
	}


	/**
	 * This function deletes the entry from the database table 
	 * corresponding to the unique ID specified.
	 *
	 * @return	boolean	true		on successfully deleting the entry.
	 *					false		on failure if the entry is not deleted.
	 *
	**/
	public function delete($id) {
		// Check if the entry is present in the database table or not?
		if ( $this->db->query("SELECT id FROM ". $this->table ." WHERE id = '$id'" ) && $this->db->nf()<=0 ) {
			// There is no entry in the database table with the specified ID.
			$this->message->setErrorMessage('Reminder not found: It may have already been elapsed or deleted.');
		}
		
		if ( $this->message->getErrorMessageCount() <= 0 ) {
			if ( $this->db->query("DELETE FROM ". $this->table ." WHERE id = '$id'" ) ) {
				$this->message->setOkMessage("Reminder has been deleted successfully.");
			}
			else {
				$this->message->setErrorMessage("Reminder was not Deleted. Please try again later.");
			}
		}
	}
	
	
	/**
	 * This function sends all the reminders whose date value in 
	 * date_next is less than the date value specified in the call.
	 *
	 * @param	mixed	The date (in DATETIME/TIMESTAMP format) for which the elapsed reminders are to be sent.
	 *
	 * @return	int		the number of reminders that have been sent.
	 *
	**/
	public function send($date_from, $date_to) {

		$list = array();
		$sent = array();
		$this->getList($list, '', " WHERE date_next BETWEEN '". $date_from ."' AND '". $date_to ."'");

		foreach ( $list as $index=>$remind ) {
			
			// Prepare the receipients email array.
			$email_arr	= array();
			$temp 		= explode(",", $remind['to_email']);
			foreach ($temp as $key=>$item) {
				$name 	= trim(substr($item, 0, strrpos($item, "<")-1) ) ;
				$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;
				$email_arr[] = array('name' => $name, 'email' => $email);
			}
						
			// Send the mail.
			$result = Sendmail(	$email_arr, $this->reminder_from, $this->reminder_from, 
								$this->getEmailSubject($remind), $this->getEmailBody($remind), false);
			$sent[]	= $remind['id'];
			
			// Adjust the date of the reminder if it is a recurring reminder.
			if ( empty($remind['repeat_every']) && empty($remind['repeat_for']) && empty($remind['repeat_type']) ) {
				// The reminder is not to be repeated.
				// Nothing to be done.
			}
			elseif ( empty($remind['repeat_for']) /*&& empty($remind['repeat_upto'])*/ && !empty($remind['repeat_every']) && !empty($remind['repeat_type']) ) {
				// The reminder is to be repeated on a fixed interval recursively.
				// Hence, calculate the next date when the reminder is to be sent.
								
				// Set the time when the next reminder will be sent.
				if ( $this->getTimestamp() == 'MYSQL' ) {
					$data['date_next']	= date("Y-m-d H:i:s", strtotime("+". $remind['repeat_every'] . $remind['repeat_type'] 
																		." ". $remind['date_next']));
				}
				elseif ( $this->getTimestamp() == 'UNIX' ) {
					$data['date_next']	= strtotime("+". $remind['repeat_every'] . $remind['repeat_type'], $remind['date_next']);
				}
				
				$this->update(	$remind['id'], $remind['type'], $remind['title'], $remind['text'], $remind['url'], 
								$remind['to_email'], $data['date_next'], $remind['repeat_every'], 
								$remind['repeat_for'], $remind['repeat_type'], $remind['repeat_upto']);

			}
			elseif ( empty($remind['repeat_every']) && !empty($remind['repeat_for']) && !empty($remind['repeat_type']) && !empty($remind['repeat_upto']) ) {
				// The reminder is to be repeated on fixed interval before the expiry date. 
				
				$data['repeat_for']	 	= explode(',', $remind['repeat_for']);
				$repeat_for_arr			= array();
				
				foreach ($data['repeat_for'] as $key=>$item) {
					$item = str_replace(' ', '', $item);
					if ( !empty($item) ) {
						$repeat_for_arr[]	= trim($item);
					}
				}
				rsort($repeat_for_arr, SORT_NUMERIC);	// Sort the interval in descending order.

				// Set the time when the first reminder will be sent.
				if ( $this->getTimestamp() == 'MYSQL' ) {
					$data['date_next']	= date("Y-m-d H:i:s", strtotime("-". $repeat_for_arr[0] . $remind['repeat_type'] 
																		." ". $remind['repeat_upto']));
				}
				elseif ( $this->getTimestamp() == 'UNIX' ) {
					$data['date_next']	= strtotime("-". $repeat_for_arr[0] . $remind['repeat_type'], $remind['repeat_upto']);
				}
				// Remove the element for which the date has been set.
				$data['repeat_for']		= implode(',', array_slice($repeat_for_arr, 1));
				
				$this->update(	$remind['id'], $remind['type'], $remind['title'], $remind['text'], $remind['url'], 
								$remind['to_email'], $data['date_next'], $remind['repeat_every'], 
								$data['repeat_for'], $remind['repeat_type'], $remind['repeat_upto']);
			}
		}
		return ($sent);
	}
	
	
	public function getReminderType() {
		return $this->reminder_type;
	}
	
	public function getIntervalType() {
		return $this->interval_type;
	}
	public function getTimestamp() {
		return $this->timestamp;
	}
	public function getReminderFrom() {
		return $this->reminder_from;
	}
	public function getEarlierDateAllowed() {
		return $this->earlier_date_allowed;
	}
	
	public function getEmailSubject($data) {
		$subject = NULL;
		$subject = $data['title'];
		
		return ($subject);
	}
	
	public function getEmailBody($data) {
		$body = NULL;	
		$body = $data['text'] ."\n\n". $data['url'];
		
		return ($body);
	}
	
	public function setEarlierDateAllowed($flag=false) {
		$this->earlier_date_allowed = $flag;
	}
	
	public function setReminderFrom($email='', $name='') {
		$this->reminder_from['email'] 	= $email;
		$this->reminder_from['name'] 	= $name;
	}
	
	function convertTimestamp( $time_src, &$time_dest, $stamp='UNIX' ) {
		if ( $stamp == 'UNIX' ) {
			$time_dest = strtotime($time_src);
			return ($time_dest);
		}
		elseif ( $stamp == 'MYSQL' ) {
			$time_dest = date("Y-m-d H:i:s", $time_src);
			return ($time_dest);
		}
	}
}

?>