<?
class Messager {
	
	var	$ok_message;
	var	$ok_messages;
	var	$error_message;
	var	$error_messages;
	
	function initMessages() {
        $this->ok_message = '';
        $this->error_message = '';
        $this->ok_messages = array();
        $this->error_messages = array();
    }

    function resetMessages() {
        $this->initMessages();
    }
    
	function setOkMessage($msg) {
        $this->ok_message .= $msg.'<br>';
        $this->ok_messages[] = $msg;
	}

	function setErrorMessage($msg) {
        $this->error_message .= $msg.'<br>';
        $this->error_messages[] = $msg;
	}
	
	function getOkMessage() {
	   return $this->ok_message;
    }

	function getErrorMessage() {
	   return $this->error_message;
    }
	
	function getOkMessages() {
        return $this->ok_messages;
    }

	function getErrorMessages() {
        return $this->error_messages;
    }
    
	function getErrorMessageCount() {
        return count($this->error_messages);
    }
	
	function getOkMessageCount() {
        return count($this->ok_messages);
    }
}
?>
