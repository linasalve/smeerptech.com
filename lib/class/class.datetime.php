<?php
	class UsrDateTime
	{
		var $usrtime;
		var $usrdate;
		var $month;
		var $day;
		var $year;
		var $strtotime;
		
		function unixTimeToDate($t)
		{
			$this->usrtime = $t;
			$this->usrdate = date("Y-m-d h:i:s",$this->usrtime);
			//$this->usrdate = strftime("%Y-%m-%d %H:%M:%S",$this->usrtime);
			
			return $this->usrdate;
		}
		function month($t)
		{
			$this->usrtime = $t;
			//return $this->month = strftime("%m",$this->usrtime);
			return $this->month = date("m",$this->usrtime);
		}
		function day($t)
		{
			$this->usrtime = $t;
			//return $this->day = strftime("%d",$this->usrtime);
			return $this->day = date("d",$this->usrtime);
		}
		function year($t)
		{
			$this->usrtime = $t;
			//return $this->year = strftime("%Y",$this->usrtime);
			return $this->year = date("Y",$this->usrtime);
		}
		function unixTimetoTime($t)
		{
			$this->usrtime = $t;
			//$getTime = strftime("%T",$this->usrtime);
			$getTime = date("h:i:s",$this->usrtime);
			return $getTime;
		}
		function dateToUnixTime($d)
		{
			$this->usrdate = $d;
			$this->usrtime = strtotime($this->usrdate);
			return $this->usrtime;
		}
	}
?>