<?php
    class ReminderProspects {
        private $reminder_of;
        private $table;


        function __construct($table='', $reminder_of='') {
            $this->table        = $table;
            $this->reminder_of  = $reminder_of;
        }
        
        
        /**
        *   Function to get all the Reminders.
        *
        *   @param Object       of database
        *   @param Array        of Reminders
        *   @param required     fields
        *   @param condition
        *   @return array       count of Records
        *                       otherwise boolean false.
        */      
        function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }

            $query .= " FROM ". TABLE_PROSPECTS_REMINDERS;

            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
                }
                
                return ( $db->nf() );
            }
            else {
                return false;
            }
        }


        function get(&$db) {
            $list = NULL;
            $condition = " WHERE table_name = '". $this->table ."'"
                    ." AND reminder_for = '". $this->reminder_of ."'"
                    ." ORDER BY id ASC";
            $this->getList($db, $list, 'id, do_reminder, description', $condition);
            
            return ($list);
        }
        

        function delete(&$db, $id, $reminder_of='', $table='') {
            $query = "DELETE FROM ". TABLE_PROSPECTS_REMINDERS
                        ." WHERE id = '$id'";
            if ( $reminder_of != '' && $table != '') {
                $query .= " AND table_name = '$table' "
                        ." AND reminder_for = '$reminder_of'";
            }
            
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                return (true);
            }
            else {
                $this->clearErrors();
                $this->setError(5);
                return (false);
            }
        }
        

        function update(&$db, $id, $table, $reminder_data) {
            if ( empty($reminder_data['id']) ) {
                $query = "INSERT INTO ". TABLE_PROSPECTS_REMINDERS
                        ." SET table_name = '".     $table ."'"
                            .", reminder_for = '".  $id ."'"
                            .", do_reminder = '".   $reminder_data['do_reminder'] ."'"
                            .", description = '".   $reminder_data['description'] ."'";
            }
            else {
                $query = "UPDATE ". TABLE_PROSPECTS_REMINDERS
                        ." SET table_name = '".     $table ."'"
                            .", reminder_for = '".  $id ."'"
                            .", do_reminder = '".   $reminder_data['do_reminder'] ."'"
                            .", description = '".   $reminder_data['description'] ."'"
                        ." WHERE id = '". $reminder_data['id'] ."'";
            }
            
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                $this->clearErrors();
                $this->setError(6);
                return (false);
            }
        }


        function clearErrors() {
            $this->error = array();
            return (true);
        }


        function getError($code='') {
            if ( !empty($code) ) {
                return ($this->error[$code]);
            }
            else {
                return ($this->error);
            }
        }


        function setError($error) {
            switch ( $error ) {
                case (1): {
                    $this->error[1] = array('code' => 1, 'description' => 'The Date for the Reminder is empty.');
                    break;
                }
                case (2): {
                    $this->error[2] = array('code' => 2, 'description' => 'The Date should be in format YYYY-MM-DD HH:MM:SS.');
                    break;
                }
                case (3): {
                    $this->error[3] = array('code' => 3, 'description' => 'The Description cannot be empty.');
                    break;
                }
                case (4): {
                    $this->error[4] = array('code' => 4, 'description' => 'The length of Description is more than allowed 200 characters.');
                    break;
                }
                case (5): {
                    $this->error[5] = array('code' => 5, 'description' => 'The Reminder was not deleted.');
                    break;
                }
                case (6): {
                    $this->error[6] = array('code' => 6, 'description' => 'The Reminder was not updated.');
                    break;
                }
            }
        }
        
        
        function setReminderOf($table='', $reminder_of='') {
            $this->table        = $table;
            $this->reminder_of  = $reminder_of;
        }
        
        function validate($date, $text) {
            $this->clearErrors();
            
            if ( !isset($date) || empty($date) ) {
                $this->setError(1);
            }
            else {
                $pattern = '/^[0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}$/';
    
                if ( !preg_match($pattern, $date) ) {
                    $this->setError(2);
                }
            }
            
            if ( !isset($text) || empty($text) ) {
                $this->setError(3);
            }
            elseif ( strlen($text)>200 ) {
                $this->setError(4);
            }
             
            if ( (count($this->error)) > 0 ) {
                return (false);
            }
            else {
                return (true);
            }
        }
        
        
    }
 ?>

