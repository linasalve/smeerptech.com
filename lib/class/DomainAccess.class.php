<?php
    class DomainAccess {
    // Attributes of a Phone Number are as follows;
    // cc - Country Code
    // ac - Area Code
    // value - Number
    // Type - h = Home
    //      - w = Work
    //      - m = Mobile
    //      - f = Fax
    //      - p = Public Phone
        private $client_id;
		
        function __construct($client_id=''){
            $this->client_id    = $client_id;            
        }
		
        /**
        *   Function to get all the Pre Orders.
        *
        *   @param Object of database
        *   @param Array of User role
        *   @param required fields
        *   @param condition
        *   return array of User roles
        *   otherwise return NULL
        */      
        function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }
            
            $query .= " FROM ". TABLE_CLIENTS_DOMAIN_ACCESS;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
                }                
                return ( $db->nf() );
            }
            else {
                return false;
            }
        }
		
        function get(&$db) {
            $list = NULL;
            $condition = " WHERE clients_id = '". $this->client_id ."'" 
                            ." ORDER BY domain_id ASC";
            if ( $this->getList($db, $list, 'id as cda_id,domain_id,clients_id,excluded_emails', $condition)>0 ) {
                
            }
            return ($list);
        } 
        
        function delete(&$db, $id ) {
            $query = "DELETE FROM ". TABLE_CLIENTS_DOMAIN_ACCESS
                        ." WHERE id = '$id'";
            
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                return (true);
            }
            else {
                $this->clearErrors();
                $this->setError(13);
                return (false);
            }
        }

        function update(&$db, $id, $d_data) {
		print_R($d_data);
            if ( empty($d_data['cda_id']) ) {
               echo $query = "INSERT INTO ". TABLE_CLIENTS_DOMAIN_ACCESS
                        ." SET domain_id = '".     $d_data['domain_id'] ."'"
						.", clients_id = '".      $id ."'"                             
						.", excluded_emails = '".$d_data['excluded_emails'] ."'"
						.", do_e 			= '".date('Y-m-d h:i:s')."'"
						.", ip = '".$_SERVER['REMOTE_ADDR'] ."'";
            }
            else {
              echo  $query = "UPDATE ". TABLE_CLIENTS_DOMAIN_ACCESS
                        ." SET domain_id 	= '".$d_data['domain_id'] ."'"
						.", clients_id 		= '".$id."'"                             
						.", excluded_emails = '".$d_data['excluded_emails'] ."'"
						.", do_e 			= '".date('Y-m-d h:i:s')."'"
						.", ip 				= '".$_SERVER['REMOTE_ADDR']."'"
                        ." WHERE id  		= '". $d_data['cda_id'] ."'";
            }
            if ( $db->query($query) ) {
                return (true);
            } else {
                $this->clearErrors();
                $this->setError(11);
                return (false);
            }
        }


        function clearErrors() {
            $this->error = array();
            return (true);
        }


        function getError($code='') {
            if ( !empty($code) ) {
                return ($this->error[$code]);
            }
            else {
                return ($this->error);
            }
        }


        function setError($error) {
            switch ( $error ) {
                case (1): {
                    $this->error[1] = array('code' => 1, 'description' => 'The Type of the Phone is not specified.');
                    break;
                }
                case (2): {
                    $this->error[2] = array('code' => 2, 'description' => 'The Type of the Phone is invalid.');
                    break;
                }
                case (3): {
                    $this->error[3] = array('code' => 3, 'description' => 'The Country Code is not specified.');
                    break;
                }
                case (4): {
                    $this->error[4] = array('code' => 4, 'description' => 'The length of Country Code is more than allowed 4 characters.');
                    break;
                }
                case (5): {
                    $this->error[5] = array('code' => 5, 'description' => 'The Country Code is invalid.');
                    break;
                }
                case (6): {
                    $this->error[6] = array('code' => 6, 'description' => 'The Area Code is not specified.');
                    break;
                }
                case (7): {
                    $this->error[7] = array('code' => 7, 'description' => 'The length of Area Code is more than allowed 7 characters.');
                    break;
                }
                case (8): {
                    $this->error[8] = array('code' => 8, 'description' => 'The Area Code is invalid.');
                    break;
                }
                case (9): {
                    $this->error[9] = array('code' => 9, 'description' => 'The Number is not specified.');
                    break;
                }
                case (10): {
                    $this->error[10] = array('code' => 10, 'description' => 'The length of Number is more than allowed 10 characters.');
                    break;
                }
                case (11): {
                    $this->error[11] = array('code' => 11, 'description' => 'The Number is invalid.');
                    break;
                }
                case (12): {
                    $this->error[12] = array('code' => 12, 'description' => 'The Number is not valid Indian Mobile Number.');
                    break;
                }
                case (13): {
                    $this->error[13] = array('code' => 13, 'description' => 'The Phone was not deleted.');
                    break;
                }
            }
        }
        
        
        function setDomainAccess($client_id='') {
            $this->client_id    = $client_id; 
        }
        
         
        
        
    }
 ?>

