<?php

	require_once (DIR_FS_ADDONS .'/turing/hn_captcha.class.x1.php');
    

	$CAPTCHA_INIT = array(
							'tempfolder'     		=> DIR_FS_ADDONS .'/turing/_tmp/', 
							'tempfolderpath'     	=> DIR_WS_ADDONS .'/turing/_tmp/', 
							'TTF_folder'     		=> DIR_FS_ADDONS .'/turing/ttf/',
							'TTF_RANGE'      		=> array('trebuc.ttf','comic.ttf','tahoma.ttf','times.ttf','arial.ttf'),
							'chars'         		=> ((TURING_CODE_LENGTH != '')? 6 : TURING_CODE_LENGTH),
							'minsize'        		=> 15,
							'maxsize'        		=> 20,
							'maxrotation'    		=> 35,
							'noise'          		=> TRUE,
							'noisefactor'			=> 16,
							'nb_noise'				=> 10,
							'websafecolors'  		=> FALSE,
							'refreshlink'    		=> TRUE,
							'lang'           		=> 'en',
							'maxtry'         		=> 3,
							'badguys_url'    		=> '/',
							'secretstring'   		=> 'jkgf874ejdgf7jh890',
							'secretposition' 		=> 24,
							'debug'          		=> FALSE,
							'counter_filename'		=> '',
							'prefix'				=> 'hn_captcha_',
							'collect_garbage_after'	=> 20,
							'maxlifetime'			=> 60
						);
                        
    
	$turing =& new hn_captcha_X1($CAPTCHA_INIT);
    //$turing1 =& new hn_captcha_X1($CAPTCHA_INIT);
	
?>