<?php
	class NewsletterSmtp {
		
		const DEACTIVE = 0;
		const ACTIVE  = 1;      

        function getStatus() {
			$status = array(
							'ACTIVE'   => NewsletterSmtp::ACTIVE,
                            'DEACTIVE'    => NewsletterSmtp::DEACTIVE                          
						);
			return ($status);
		}      
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_NEWSLETTER_SMTP;
			 $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_NEWSLETTER_SMTP .".client "; 
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_NEWSLETTER_SMTP;
		    $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_NEWSLETTER_SMTP .".client ";   
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (NewsletterSmtp::getList($db, $details, 'smtp_id', " WHERE smtp_id = '$id'")) > 0 ) {
               $details = $details[0];
                
				$query = "DELETE FROM ". TABLE_NEWSLETTER_SMTP
							." WHERE smtp_id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage("The Record has been deleted.");                      
				}
				else {
					$messages->setErrorMessage("The Record was not deleted.");
				}
              
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			if ( !isset($data['client']) || empty($data['client']) ) 
                $messages->setErrorMessage('Please select Client');
			
            if ( !isset($data['domain_name']) || empty($data['domain_name']) ) 
                $messages->setErrorMessage('Please enter domain name');
			
			if ( !isset($data['domain_mailgun_key']) || empty($data['domain_mailgun_key']) ) 
                $messages->setErrorMessage('Please eneter domain MKey');

			
			if ( !isset($data['sockethost']) || empty($data['sockethost']) ) 
                $messages->setErrorMessage('Socket host cannot be empty.');
			
			if ( !isset($data['smtpauthuser']) || empty($data['smtpauthuser']) ) 
                $messages->setErrorMessage('Smtp auth user cannot be empty.');
			if ( !isset($data['smtpauthpass']) || empty($data['smtpauthpass']) ) 
                $messages->setErrorMessage('Smtp auth password cannot be empty.');
			if ( !isset($data['smtpport']) || empty($data['smtpport']) ) 
                $messages->setErrorMessage('Smtp port cannot be empty.');
			
			if ( !isset($data['from_name']) || empty($data['from_name']) ) 
                $messages->setErrorMessage('From name cannot be empty.');
			
			if ( !isset($data['from_email']) || empty($data['from_email']) ) 
                $messages->setErrorMessage('From_email cannot be empty.');
			
			if ( !isset($data['replyto_name']) || empty($data['replyto_name']) ) 
                $messages->setErrorMessage('Reply to name cannot be empty.');
			
			if ( !isset($data['replyto_email']) || empty($data['replyto_email']) ) 
                $messages->setErrorMessage('Reply to email cannot be empty.');
             			
            
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
        function validateUpdate(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			if ( !isset($data['client']) || empty($data['client']) ) 
                $messages->setErrorMessage('Please select Client');
			
            if ( !isset($data['domain_name']) || empty($data['domain_name']) ) 
                $messages->setErrorMessage('Please enter domain name');
			
			if ( !isset($data['domain_mailgun_key']) || empty($data['domain_mailgun_key']) ) 
                $messages->setErrorMessage('Please eneter domain MKey');

			
            if ( !isset($data['sockethost']) || empty($data['sockethost']) ) 
                $messages->setErrorMessage('Socket host cannot be empty.');
			
			if ( !isset($data['smtpauthuser']) || empty($data['smtpauthuser']) ) 
                $messages->setErrorMessage('Smtp auth user cannot be empty.');
			if ( !isset($data['smtpauthpass']) || empty($data['smtpauthpass']) ) 
                $messages->setErrorMessage('Smtp auth password cannot be empty.');
			if ( !isset($data['smtpport']) || empty($data['smtpport']) ) 
                $messages->setErrorMessage('Smtp port cannot be empty.');
			
			if ( !isset($data['from_name']) || empty($data['from_name']) ) 
                $messages->setErrorMessage('From name cannot be empty.');
			
			if ( !isset($data['from_email']) || empty($data['from_email']) ) 
                $messages->setErrorMessage('From_email cannot be empty.');
			
			if ( !isset($data['replyto_name']) || empty($data['replyto_name']) ) 
                $messages->setErrorMessage('Reply to name cannot be empty.');
			
			if ( !isset($data['replyto_email']) || empty($data['replyto_email']) ) 
                $messages->setErrorMessage('Reply to email cannot be empty.');
             
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            $query = " UPDATE ". TABLE_NEWSLETTER_SMTP
                    ." SET ". TABLE_NEWSLETTER_SMTP .".status = '".NewsletterSmtp::ACTIVE."'"
                    ." WHERE smtp_id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The SMTP Account has been activated.");                      
            }
            else {
                $messages->setErrorMessage("The SMTP Account has not been activated.");
            }
       
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            $query = " UPDATE ". TABLE_NEWSLETTER_SMTP
                    ." SET ". TABLE_NEWSLETTER_SMTP .".status = '".NewsletterSmtp::DEACTIVE."'"
                    ." WHERE smtp_id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The SMTP Account has been deactivated.");                      
            }
            else {
                $messages->setErrorMessage("The SMTP Account has not been deactivated.");
            }
                
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
		
    }
?>
