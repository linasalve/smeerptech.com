<?php
	class ProspectsOrder {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const SUSPENDED = 2;
        const DELETED = 3;
		const COMPLETED = 4;
		const PROCESSED = 5;
        
		const WALKIN = 1;
		const TARGETED =2;
        
        const NONRENEWABLE =0;
        const RENEWABLE =1;
		
		const FOLLOWUPPENDING = 0;
		const FOLLOWUPACTIVE  = 1;
		
		const NONELSTATUS = 0;
		const HOTLSTATUS  = 1;
		const WARMLSTATUS = 2;
		const COLDLSTATUS = 3;
		const NEGATIVELSTATUS = 4;
		const WINLSTATUS = 5;
		const LOOSELSTATUS = 6;
        
		function getStatus() {
			$status = array(
							'ACTIVE'    => ProspectsOrder::ACTIVE,
							'SUSPENDED' => ProspectsOrder::SUSPENDED,
							'CANCELLED' => ProspectsOrder::BLOCKED,
                            'DELETED'   => ProspectsOrder::DELETED
                            //'COMPLETED' => ProspectsOrder::COMPLETED,
							//'PROCESSED' => ProspectsOrder::PROCESSED
						);
			return ($status);
		}
		function getLStatus() {
			$lstatus = array('HOT' => ProspectsOrder::HOTLSTATUS,
							'WARM'    => ProspectsOrder::WARMLSTATUS,
							'COLD'   => ProspectsOrder::COLDLSTATUS,
                            'NEGATIVE'   => ProspectsOrder::NEGATIVELSTATUS,
                            'WIN'   => ProspectsOrder::WINLSTATUS,
                            'LOOSE'   => ProspectsOrder::LOOSELSTATUS 
                           // 'NONE' => ProspectsOrder::NONELSTATUS
						);
			return ($lstatus);
		}
        function getFollowupStatus() {
			$status = array(
						'ACTIVE'    => ProspectsOrder::FOLLOWUPACTIVE,
						'PENDING' => ProspectsOrder::FOLLOWUPPENDING						
						);
			return ($status);
		}
        function getTypes() {
			$status = array('WALKIN' => ProspectsOrder::WALKIN,
							'TARGETED' => ProspectsOrder::TARGETED
						);
			return ($status);
		}
		 
		function getDiscountType(){
		
			$typeList= array('OneTime'=>'OneTime',
							 'Recurring'=>'Recurring' 		 
			);
			return $typeList;
		}
		//New added eof
		function getSTypes(){
		
			$typeList= array('Qty'=>'Qty',
							 'Nos'=>'Nos',							 
							 'Yrs'=>'Yrs',							 
							 'Mth'=>'Mth',							 
							 'Hrs'=>'Hrs'							 
			);
			return $typeList;
		}
        function getNos(){
			$noList=array();
			for($i=1;$i<=11;$i++){
				$noList[]=$i;
			}
			return $noList;
		}
		function getRoundOffOp(){
			$arr= array('+','-');
			return $arr;
		}
		function getRoundOff(){
			$roundOffList=array();
			
			for($i=0.00;$i< 1; $i = $i+0.01){
				$roundOffList[]=$i;
			}
			return $roundOffList;
		}
		//New added bof
        /*
        function getFinancialYear() {
			$financialyear = array( '0' => '2000-2001',
							        '1' => '2001-2002',
                                    '2' => '2002-2003',
                                    '3' => '2004-2005',
                                    '4' => '2006-2007',
                                    '5' => '2007-2008',
                                    '6' => '2008-2009',
						);
			return ($financialyear);
		}
        */
		
		function validateCommentDailyAdd(&$posted_data,$extra='') {
            foreach ($extra as $key=>$value) {
				$$key = $value;
			}   
            if( empty($posted_data["or_id"])) {
				$messages->setErrorMessage("Please Select Project / Order .");
			}
             	
			/* if(!empty($posted_data['est_completion_dt'])){
				$est_arr = explode("/",$posted_data['est_completion_dt']);
				$est_completion_dt1 = $est_arr[2].$est_arr[1].$est_arr[0] ;
				$currentDt = date('Ymd');
				if($est_completion_dt1 < $currentDt){
					$messages->setErrorMessage("Select Valid Estimated Completion date as of Today.");
				} 
			}else{
				$messages->setErrorMessage("Please Select Estimated Completion date as of Today.");
			} */
			 
			if(empty($posted_data["details"]) && empty($posted_data["internal_comment"])){
				$messages->setErrorMessage("Please enter Followup Sent to Prospect or Internal Work Status");
			}	
			 
            if(empty($posted_data["hrs"]) && empty($posted_data["min"])){ 
                $messages->setErrorMessage("Please select Time sheet Hrs.");
            }
           
            $fileSize = 0;
            if(!empty($files['attached_file'])){
                if(!empty( $files['attached_file']["name"])){                
                    if ( !in_array($files['attached_file']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                }
                 
            }
			if(!empty($files['attached_file1'])){
                if(!empty( $files['attached_file1']["name"])){                
                    if ( !in_array($files['attached_file1']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file1']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file1']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
            if(!empty($files['attached_file2'])){
                if(!empty( $files['attached_file2']["name"])){                
                    if ( !in_array($files['attached_file2']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file2']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file2']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
			if(!empty($files['attached_file3'])){
                if(!empty( $files['attached_file3']["name"])){                
                    if ( !in_array($files['attached_file3']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file3']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file3']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
			if(!empty($files['attached_file4'])){
                if(!empty( $files['attached_file4']["name"])){                
                    if ( !in_array($files['attached_file4']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file4']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file4']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
			if(!empty($files['attached_file5'])){
                if(!empty( $files['attached_file5']["name"])){                
                    if ( !in_array($files['attached_file5']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file5']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file5']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
			if(!empty($files['attached_file6'])){
                if(!empty( $files['attached_file6']["name"])){                
                    if ( !in_array($files['attached_file6']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file6']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file6']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
			if(!empty($files['attached_file7'])){
                if(!empty( $files['attached_file7']["name"])){                
                    if ( !in_array($files['attached_file7']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file7']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file7']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
			if(!empty($files['attached_file8'])){
                if(!empty( $files['attached_file8']["name"])){                
                    if ( !in_array($files['attached_file8']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file8']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file8']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
			if(!empty($files['attached_file9'])){
                if(!empty( $files['attached_file9']["name"])){                
                    if ( !in_array($files['attached_file9']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file9']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    } 
                    $fileSize += $files['attached_file9']["size"] ; 
                    /* if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    } */
                } 
            }
			
			
            if($fileSize > $posted_data['max_file_size']){
                 $messages->setErrorMessage('Size of all files is greater than 10Mb');
            }
            /*
            if(!empty($files['attached_file']["name"])){
                if(empty($posted_data['attached_desc'])){
                     $messages->setErrorMessage('File description Should not be Empty as you are uploading the file.');
                }
            }*/
           /*  if(!empty($posted_data['attached_desc'])){
                if(empty($files['attached_file']["name"])){
                        $messages->setErrorMessage('Please upload file as File Description is not empty.');
                }
            }
            
            if(isset($posted_data['mail_client']) &&  !isset($posted_data['is_visibleto_client'])){
                 $messages->setErrorMessage("As you are sending Notification To client, So Visible to Client Option should be selected.");
            }
            
            if(!isset($posted_data['mail_client']) &&  isset($posted_data['is_visibleto_client'])){
                 $messages->setErrorMessage("As Task will be visible to Client, So Send Notification to Client.");
            } */
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
		
		function getCloseDtList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PROSPECTS_ORD_CLOSE_DT;
			
			  $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PROSPECTS_ORDERS;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
          
            $query .= " FROM ". TABLE_PROSPECTS_ORDERS." ";
            $query .= " INNER JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_PROSPECTS_ORDERS .".created_by ";
            $query .= " INNER JOIN ". TABLE_PROSPECTS
                        ." ON ". TABLE_PROSPECTS .".user_id = ". TABLE_PROSPECTS_ORDERS .".client ";  
                      
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	   function getDashboard( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
             $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PROSPECTS_ORDERS;
            $query .= " LEFT JOIN ". TABLE_PROSPECTS
                        ." ON ". TABLE_PROSPECTS .".user_id = ". TABLE_PROSPECTS_ORDERS .".client ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
					
					if ( strpos($required_fields, 'team') > 0 ) {
						include_once ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$count = count($list);
						for ( $i=0; $i<$count; $i++ ) {
							// Read the Team Member Details.
							$list[$i]['team'] = "'". implode("','", (explode(',', $list[$i]['team']))) ."'";
							$list[$i]['team_members']= '';
                            
							User::getList($db, $list[$i]['team_members'], 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $list[$i]['team'] .")");  
							 
						}
					}
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
	   
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( !in_array($status_new, ProspectsOrder::getStatus()) ) {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
            else {
                $order = NULL;
                if ( (ProspectsOrder::getList($db, $order, 'id, access_level, status', " WHERE id = '$id'")) > 0 ) {
                    $order = $order[0];
                    if ( $order['status'] != ProspectsOrder::COMPLETED ) {
                        $markComplete='';
                        //if ( $order['access_level'] < $access_level ) {
                            if($status_new==ProspectsOrder::COMPLETED){
                                 $markComplete = ", ac_ed_date = '".date('Y-m-d H:i:s')."' ";
                            }
                            $query = "UPDATE ". TABLE_PROSPECTS_ORDERS
                                        ." SET status = '$status_new' ".$markComplete
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        /*}
                        else {
                            $messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
                        }*/
                    }
                    else {
                        $messages->setErrorMessage("Status cannot be changed: Order has been Approved.");
                    }
                }
                else {
                    $messages->setErrorMessage("The Order was not found.");
                }
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PROSPECTS_ORD_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $order = NULL;
            if ( (ProspectsOrder::getList($db, $order, TABLE_PROSPECTS_ORDERS.".id,".
				TABLE_PROSPECTS_ORDERS.".number,".TABLE_PROSPECTS_ORDERS.".access_level,".TABLE_PROSPECTS_ORDERS.".status", 
				" WHERE ".TABLE_PROSPECTS_ORDERS.".id = '$id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != ProspectsOrder::COMPLETED ) {
                    if ( $order['status'] == ProspectsOrder::BLOCKED ) {
                        if ( $order['access_level'] < $access_level ) {
                        
                            if((ProspectsQuotation::getList($db, $invoice, TABLE_PROSPECTS_QUOTATION.'id', " WHERE ".TABLE_PROSPECTS_QUOTATION.".or_no = '".$order['number']."' AND ".TABLE_PROSPECTS_QUOTATION.".status !='".ProspectsQuotation::DELETED."'") ) > 0){
                             
                                $messages->setErrorMessage("Please delete Quotations of Prospect ".$order['number']."  ");
                             
                            }else{                            
                                /*
                                    $query = "DELETE FROM ". TABLE_PROSPECTS_ORDERS
                                            ." WHERE id = '$id'";
                                */
                                
                                $query = "UPDATE ". TABLE_PROSPECTS_ORDERS
                                            ." SET status ='".ProspectsOrder::DELETED."' WHERE id = '$id'";
                                            
                                if ( $db->query($query) && $db->affected_rows()>0 ) {
                                
                                    $messages->setOkMessage("The Lead Order has been deleted.");
                                    
                                }
                                else {
                                    $messages->setErrorMessage("The Lead Order was not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Lead Order with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("To Delete Lead Order it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Lead cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Lead was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
           // print_r($data);
            
            $files = $_FILES;
           
            // Validate the Pre Order.
            /* comment on 2009-04-10 
            $list = NULL;
            if ( Order::getList($db, $list, 'id', " WHERE po_id = '". $data['po_id'] ."'")>0 ) {
                $messages->setErrorMessage("The Pre Order has been processed already.");
            }
            else {
                if ( isset($data['po_id']) && !empty($data['po_id']) ) {
                    $query = "SELECT access_level, status FROM ". TABLE_BILL_PO
                            ." WHERE id = '". $data['po_id'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage("The Pre Order was not found in the database.");
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            if ( $db->f('status') != ACTIVE ) {
                                $messages->setErrorMessage("The Pre Order is not yet approved.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                        }
                    }
                }
            }
            */
            
            // As date of Order is compulsary
            if(empty($data['do_o'])){
                 $messages->setErrorMessage("Select the date of Order.");
            }
			// Format the Date of order, if provided. order number will be generated on the basis od order date
            if ( isset($data['do_o']) && !empty($data['do_o']) ) {
                $data['do_o'] = explode('/', $data['do_o']);
                //$data['number'] = "PT". $data['do_o'][2] ."-OR-". $data['do_o'][1]. $data['do_o'][0].date("hi-s") ;
                $data['do_o'] = mktime(0, 0, 0, $data['do_o'][1], $data['do_o'][0], $data['do_o'][2]);
            }            
            if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
			$data['number']=$data['ord_counter'] ='';
            if(!empty($data['do_o'])){				
				$detailOrdNo = getProspectLeadNumber($data['do_o']); 				
				if(!empty($detailOrdNo)){
					$data['number'] = $detailOrdNo['number'];
					$data['ord_counter'] = $detailOrdNo['ord_counter'];
				}
			}
			if(empty($data['number']) ){
                $messages->setErrorMessage("Order Number was not generated.");
            }          
            
            // Check for the duplicate order Number.
            $list = NULL;
            if(!empty($data['number'])){
                if ( ProspectsOrder::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                    $messages->setErrorMessage("An Order with the same Number already exists.<br/>Please try again.");
                }
            }
            $list = NULL;
            $data['po_filename'] = '';
            if((!empty($files['po_filename']) && (!empty($files['po_filename']['name'])))){
                
                $filename = $files['po_filename']['name'];
                $data['po_filename'] = $filename;
                $type = $files['po_filename']['type'];
                $size = $files['po_filename']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Purchase Order file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of purchase order file is greater than 1Mb');
                }
            }
            
            $data['filename_1'] = '';
            if(!empty($data['filecaption_1']) && empty($files['filename_1']['name'])){
                    $messages->setErrorMessage("Please select work order file1.");
            }
            if((!empty($files['filename_1']) && (!empty($files['filename_1']['name'])))){
                if(empty($data['filecaption_1'])){
                    $messages->setErrorMessage("Caption for file 1 should be provided.");
                }
                $filename = $files['filename_1']['name'];
                $data['filename_1'] = $filename;
                $type = $files['filename_1']['type'];
                $size = $files['filename_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 1 is greater than 1Mb');
                }
            }
            
            $data['filename_2'] = '';
            if(!empty($data['filecaption_2']) && empty($files['filename_2']['name'])){
                    $messages->setErrorMessage("Please select work order file2.");
            }
            if((!empty($files['filename_2']) && (!empty($files['filename_2']['name'])))){
                if(empty($data['filecaption_2'])){
                    $messages->setErrorMessage("Caption for file 2 should be provided.");
                }
                $filename = $files['filename_2']['name'];
                $data['filename_2'] = $filename;
                $type = $files['filename_2']['type'];
                $size = $files['filename_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 2 is greater than 1Mb');
                }
            }
            
            $data['filename_3'] = '';
            if(!empty($data['filecaption_3']) && empty($files['filename_3']['name'])){
                    $messages->setErrorMessage("Please select work order file3.");
            }
            if((!empty($files['filename_3']) && (!empty($files['filename_3']['name'])))){
                if(empty($data['filecaption_3'])){
                    $messages->setErrorMessage("Caption for file 3 should be provided.");
                }
                $filename = $files['filename_3']['name'];
                $data['filename_3'] = $filename;
                $type = $files['filename_3']['type'];
                $size = $files['filename_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 3 is greater than 1Mb');
                }
            }
            
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                    
                   if(!empty($val["name"]) ){
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of all files is greater than 2Mb');
            }
            
            // As date of delivery is compulsary
            /*
			if(empty($data['do_d'])){
                 $messages->setErrorMessage("Select the date of Delivery.");
            }
			*/
            // Format the Date of Delivery, if provided.
            if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
            if ( isset($data['do_e']) && !empty($data['do_e']) ) {
                $data['do_e'] = explode('/', $data['do_e']);
                $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
            }
            
            if ( isset($data['do_fe']) && !empty($data['do_fe']) ) {
                $data['do_fe'] = explode('/', $data['do_fe']);
                $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
            }
            
            /* comment on 2009-04-10
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creator's status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }
            */
            
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Prospect for whom the Order is being Created.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_PROSPECTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Prospect was not found.");
                }
                else {
                    $db->next_record();
                   
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Prospect is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                    }
                }
            }

            // Validate the Executives Team.
            if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
            }
            else {
                /*
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result()); 
                    }                                               
                    $team_count = count($data['team']);
                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }                   
                }
                */
            }
            if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                $messages->setErrorMessage("Order Title should be provided.");
			}
            if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                $messages->setErrorMessage("Order Type should be provided.");
			}
			if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                $messages->setErrorMessage("Lead Owner should be provided.");
            }
            
            /*
            if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                $messages->setErrorMessage("Order Particulars should be provided.");
			}
           
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
             */
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}
            
           
            $list = NULL;
            // validate particulars bof
            
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                //temp. commented 
              // $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }
            $data['query_p']='';
            $is_part = false;
            $isrenewable = false;
           
            if(isset($data['particulars'])){
                  $data['query_p'] = 'INSERT INTO '. TABLE_PROSPECTS_ORD_P .' ( ord_no, particulars, p_amount, 
				  ss_div_id,s_id, sub_s_id, 
				  s_type, s_quantity, s_amount, ss_title, ss_punch_line,
				  tax1_id,tax1_number, tax1_name,tax1_value, tax1_pvalue,tax1_amount,
				  tax1_sub1_id,tax1_sub1_number, tax1_sub1_name,tax1_sub1_value, tax1_sub1_pvalue,tax1_sub1_amount,
				  tax1_sub2_id,tax1_sub2_number, tax1_sub2_name,tax1_sub2_value, tax1_sub2_pvalue,tax1_sub2_amount,
				  d_amount, stot_amount, tot_amount, discount_type, pp_amount, vendor,vendor_name, purchase_particulars, 
				  is_renewable, do_e, do_fe ) VALUES ';
				$data['alltax_ids']= array();
				
                foreach ( $data['particulars'] as $key=>$particular ) {
					$data['sub_sid_list'][$key]= $data['subsid'][$key]=$ss_sub_id_details=array();
					
					if(!empty($data['s_id'][$key])){
						$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = ".$data['s_id'][$key]." 
						AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
						$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = ".$data['currency_id']." 
						AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;

						$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
						TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
						$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." 
						".$condition_query_sid ; 
						 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

						if ( $db->query($query) ) {
							if ( $db->nf() > 0 ) {
								while ($db->next_record()) {
									$ss_subtitle =  trim($db->f("ss_title")).",";
									$ss_subprice =  $db->f("ss_price") ;
									$ss_subid =  $db->f("ss_id") ;
									$ss_sub_id_details[] = array(
										'ss_title'=>$ss_subtitle,
										'ss_price'=>$ss_subprice,
										'ss_id'=>$ss_subid	
									);
								}
							}
						} 
				   }
				   $data['sub_sid_list'][$key]=$ss_sub_id_details;
				   $sub_s_id_str='';
				   $sub_s_id_str =trim($data['subsidstr'][$key],",");
				   if(!empty($sub_s_id_str)){
						$data['subsid'][$key] = explode(",",$sub_s_id_str);
						$sub_s_id_str=",".$sub_s_id_str.",";
				   }
				   
					$tax1_id_str = $data['tax1_id'][$key] ;
					$tax1_sub1_id_str = $data['tax1_sub1_id'][$key] ;
					$tax1_sub2_id_str = $data['tax1_sub2_id'][$key] ;
					
					if(!empty($tax1_id_str)){
						$arr = explode("#",$tax1_id_str);
						$data['tax1_name_str'][$key] = $arr[0] ;
						$data['tax1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_name_str'][$key] = 0 ;		
						$data['tax1_id_check'][$key] = 0 ;		
					}
					
					if(!empty($data['tax1_value'][$key])){
						$tax1_val = $data['tax1_value'][$key] ;
						$percent =   substr($tax1_val,0,-1);
						$data['tax1_pvalue'][$key] = (float) ($percent/100);
					}else{
						$data['tax1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub1_value'][$key])){
						$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
						$percent_sub1 =   substr($tax1_sub1_val,0,-1);
						$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
					}else{
						$data['tax1_sub1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub2_value'][$key])){
						$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
						$percent_sub2 =   substr($tax1_sub2_val,0,-1);
						$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
					}else{
						$data['tax1_sub2_pvalue'][$key] =0;
					}
					$data['tax_check_ids'][] = $data['tax1_id_check'][$key] ;
					$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					$data['tax1_sid_opt'][$key]=array();
					$data['tax1_sid_opt_count'][$key]=0;
					if($data['tax1_id_check'][$key]>0){
						//tax1_sub_id Options of taxes 
						//$data['tax1_sid_opt'][$key] = array('0'=>2);
						$tax_opt = array();
						$tax_id = $data['tax1_id_check'][$key];
						$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." ORDER BY tax_name ASC ";
						ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
						if(!empty($tax_opt)){
							$data['tax1_sid_opt'][$key] = $tax_opt ;  
							$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
						}
					}
					if(!empty($tax1_sub1_id_str)){
						$arr = explode("#",$tax1_sub1_id_str);
						$data['tax1_sub1_name_str'][$key] = $arr[0] ;
						$data['tax1_sub1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub1_name_str'][$key] = 0 ;		
						$data['tax1_sub1_id_check'][$key] = 0 ;		
					}
					if(!empty($tax1_sub2_id_str)){
						$arr = explode("#",$tax1_sub2_id_str);
						$data['tax1_sub2_name_str'][$key] = $arr[0] ;
						$data['tax1_sub2_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub2_name_str'][$key] = 0 ;		
						$data['tax1_sub2_id_check'][$key] = 0 ;		
					}
					
					if(!empty($data['tax1_id_check'][$key])){
						$data['alltax_ids'][]=$data['tax1_id_check'][$key] ;	
					}		
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub1_id_check'][$key] ;
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub2_id_check'][$key] ;
					}
					 
					
					if(empty($data['s_id'][$key])){
						$messages->setErrorMessage('Select the services.');
					}
                    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                     
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
                                
                                /*  
							    if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
                                    $messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
                                   
                                }elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){
                                
                                    $messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');
                                    
                                }else{ 
								*/
								
									/*
                                    if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || empty($data['do_fe']) ) ){
                                       $messages->setErrorMessage('Service '. $data['ss_title'][$key] .' is Renewal Service. 
										Please specify Service From Date &&  Renewal Date');
                                      
                                    }else{
									*/
                                       $is_part = true;
                                      
                                       $do_e  = "";
                                       $do_fe = "";  ;
                                        
										if($data['is_renewable'][$key] =='1'){
                                             // $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                                             // $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                                              $isrenewable = true;
                                        } 
                                       //Get vendor name BOF                                     
                                        $data['vendor_name'][$key]='';
                                        if(!empty($data['vendor'][$key])){
                                            $vendorSql ="SELECT billing_name FROM ".TABLE_VENDORS." WHERE 
											id='".$data['vendor'][$key]."'" ;
                                            $db->query( $vendorSql );
                                            if( $db->nf() > 0 )
                                            {
                                                while($db->next_record()){			
                                                    $data['vendor_name'][$key] = $db->f('billing_name')  ;
                                                }
                                            }
                                        }
                                       //Get vendor name EOF
                                       
									  
                                       
                                    $data['query_p'] .= "('". $data['number'] ."', 
									'". processUserData(trim($data['particulars'][$key],',')) ."', 
									'". $data['p_amount'][$key] ."','". $data['ss_div_id'][$key] ."',
									'". $data['s_id'][$key] ."','".$sub_s_id_str."',
									'". processUserData($data['s_type'][$key]) ."', 
									'". processUserData($data['s_quantity'][$key]) ."',
									'". processUserData($data['s_amount'][$key]) ."',
									'". processUserData($data['ss_title'][$key]) ."', 
									'". processUserData($data['ss_punch_line'][$key]) ."',
									'". processUserData($data['tax1_id_check'][$key]) ."',
									'". processUserData($data['tax1_number'][$key]) ."',  
									'". processUserData($data['tax1_name_str'][$key]) ."',
									'". processUserData($data['tax1_value'][$key]) ."',
									'". processUserData($data['tax1_pvalue'][$key]) ."',
							'".processUserData($data['tax1_amount'][$key])."',                                       
									'". processUserData($data['tax1_sub1_id_check'][$key]) ."',
									'". processUserData($data['tax1_sub1_number'][$key]) ."',  
									'". processUserData($data['tax1_sub1_name_str'][$key]) ."', 
									'". processUserData($data['tax1_sub1_value'][$key]) ."',
									'". processUserData($data['tax1_sub1_pvalue'][$key]) ."',
									'". processUserData($data['tax1_sub1_amount'][$key]) ."',					
									'". processUserData($data['tax1_sub2_id_check'][$key]) ."',
									'". processUserData($data['tax1_sub2_number'][$key]) ."',  
									'". processUserData($data['tax1_sub2_name_str'][$key]) ."', 
									'". processUserData($data['tax1_sub2_value'][$key]) ."',
									'". processUserData($data['tax1_sub2_pvalue'][$key]) ."',
									'". processUserData($data['tax1_sub2_amount'][$key]) ."',
									'". processUserData($data['d_amount'][$key]) ."', 
									'". processUserData($data['stot_amount'][$key]) ."',
									'". processUserData($data['tot_amount'][$key]) ."', 
									'". processUserData($data['discount_type'][$key]) ."', 
									'". processUserData($data['pp_amount'][$key]) ."',
									'". processUserData($data['vendor'][$key]) ."', 
									'". processUserData($data['vendor_name'][$key]) ."',
									'". processUserData($data['purchase_particulars'][$key]) ."',
									'". processUserData($data['is_renewable'][$key]) ."', '". $do_e ."','".$do_fe."'
                                        )," ;
									
								   //}
                                //}
                            }
                        }
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
           
			$data['taxpval_check'] = array_unique($data['taxpval_check']);
			$countPVal = count($data['taxpval_check']);
			if( $countPVal > 1 ){
				$messages->setErrorMessage('All Particulars of TAXES/VATS should be of same %');
			}
			$data['alltax_ids']=array_unique($data['alltax_ids']);
            //To check that order should be of only one tax
			$data['tax_check_ids']= array_unique(array_flip($data['tax_check_ids']));
			$countTax = count($data['tax_check_ids']);
			if( $countTax > 1 ){
				$messages->setErrorMessage('Particulars with Different Taxes are not allowed. All Particulars should be of same TAXES/VATS 	or without any TAXES/VATS');
			}           
            $data['isrenewable']=$isrenewable;
            if(!$isrenewable && (!empty($data['do_e']) || !empty($data['do_fe']) )){            
                 $messages->setErrorMessage('You have not selected any Renewable Services So Service From Date and Renewal Date should be blank.  ');
            }
            if ( !$is_part ) {
               // $messages->setErrorMessage('The Particular for the Order cannot be empty.');
            }
            else {
                if(!empty($data['query_p'])){
                    $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
                }
            }

            if ( ( !isset($data['amount']) || empty($data['amount']) ) && isset($data['particulars']) ) {
                //temp. commented 
               $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if(!empty($data['amount'])){
                    if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                        $messages->setErrorMessage("The Total Amount is not valid.");
                    }
                    else {
                        $data['balance'] = $data['amount'];
                    }
                }
            }
            // Validate particulars eof
            
            // Check for start date
            if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
            
           
            
            //Check Estimated hrs should not be characters
            if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
        
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $files = $_FILES;
            // First check if the ProspectsOrder exists or not?
            $list = NULL;
            if ( ProspectsOrder::getList($db, $list, 'id, created_by', " WHERE id = '". $data['or_id'] ."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                $list = NULL;                
               
                $data['po_filename'] = '';
                $data['filename_1'] = '';
                $data['filename_2'] = '';
                $data['filename_3'] = '';  
            /*
			if((!empty($files['po_filename']) && (!empty($files['po_filename']['name'])))){           
				$filename = $files['po_filename']['name'];
				$data['po_filename'] = $filename;
				$type = $files['po_filename']['type'];
				$size = $files['po_filename']['size'];
				$max_size = ($data['max_file_size'] * 1024);
				if ( !in_array($type, $data["allowed_file_types"] ) ) {
					 $messages->setErrorMessage("$type type Of Purchase Order file is not allowed");
				}
				if($size > $max_size){
					$messages->setErrorMessage('Size of purchase order file is greater than 1Mb');
				}
			}
			
			if((!empty($files['filename_1']) && (!empty($files['filename_1']['name'])))){    
				if(empty($data['filecaption_1'])){
					$messages->setErrorMessage("Caption for file 1 should be provided.");
				}
				$filename = $files['filename_1']['name'];
				$data['filename_1'] = $filename;
				$type = $files['filename_1']['type'];
				$size = $files['filename_1']['size'];
				$max_size = ($data['max_file_size'] * 1024);
				if ( !in_array($type, $data["allowed_file_types"] ) ) {
					 $messages->setErrorMessage("$type type Of Work Order file 1 is not allowed");
				}
				if($size > $max_size){
					$messages->setErrorMessage('Size of Work order file 1 is greater than 1Mb');
				}
			}elseif(!empty($data['old_filename_1'])){
				if(empty($data['filecaption_1']) && !isset($data['delete_fl_1'])){
					$messages->setErrorMessage("Caption for file 1 should be provided.");
				}                  
			}
			
		   
			
			if((!empty($files['filename_2']) && (!empty($files['filename_2']['name'])))){
				
				if(empty($data['filecaption_2'])){
					$messages->setErrorMessage("Caption for file 2 should be provided.");
				}
				
				$filename = $files['filename_2']['name'];
				$data['filename_2'] = $filename;
				$type = $files['filename_2']['type'];
				$size = $files['filename_2']['size'];
				$max_size = ($data['max_file_size'] * 1024);
				if ( !in_array($type, $data["allowed_file_types"] ) ) {
					 $messages->setErrorMessage("$type type Of Work Order file 2 is not allowed");
				}
				if($size > $max_size){
					$messages->setErrorMessage('Size of Work order file 2 is greater than 1Mb');
				}
			}elseif(!empty($data['old_filename_2'])){                
				if(empty($data['filecaption_2']) && !isset($data['delete_fl_2'])){
					$messages->setErrorMessage("Caption for file 2 should be provided.");
				}
			}
			
			
			
			if((!empty($files['filename_3']) && (!empty($files['filename_3']['name'])))){
				if(!empty($data['old_filename_3'])){
					if(empty($data['filecaption_3'])){
						$messages->setErrorMessage("Caption for file 3 should be provided.");
					}
				}
				$filename = $files['filename_3']['name'];
				$data['filename_3'] = $filename;
				$type = $files['filename_3']['type'];
				$size = $files['filename_3']['size'];
				$max_size = ($data['max_file_size'] * 1024);
				if ( !in_array($type, $data["allowed_file_types"] ) ) {
					 $messages->setErrorMessage("$type type Of Work Order file 3 is not allowed");
				}
				if($size > $max_size){
					$messages->setErrorMessage('Size of Work order file 3 is greater than 1Mb');
				}
			}elseif(!empty($data['old_filename_3'])){
				if(empty($data['filecaption_3']) && !isset($data['delete_fl_3'])){
					$messages->setErrorMessage("Caption for file 3 should be provided.");
				}
			}
		
			$fileSize = 0;
			if(!empty($data['files'])){
				foreach($data['files'] as $key => $val){
					
				   if(!empty($val["name"]) ){
						if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
							$msg = $val["type"]." is not allowed ";
							$messages->setErrorMessage($msg);
						}
						$fileSize += $val["size"] ;
					}
				}
			}
			if($fileSize > $data['max_mfile_size']){
				 $messages->setErrorMessage('Size of all files is greater than 2Mb');
			}
			
			*/
               
                if ( !isset($data['client']) || empty($data['client']) ) {
                    $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
                }
                else {
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_PROSPECTS
                                ." WHERE user_id = '". $data['client'] ."'";
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The selected Client was not found.");
                    }
                    else {
                        $db->next_record();
                        //print_r($db->result());
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Prospect is not Active.");
                        }
                        else {
                            $data['client'] = processSQLData($db->result());
                        }
                    }
                }
				
                // Validate the Executives Team.
                if ( !isset($data['team']) || empty($data['team']) ) {
                    $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
                }
                 
                if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                    $messages->setErrorMessage("Order Title should be provided.");
                }
                //As order form differentiated as per the permissions 
                
                    if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                        $messages->setErrorMessage("Order Type should be provided.");
                    }
                    if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                        $messages->setErrorMessage("Lead Owner should be provided.");
                    }
                 
               
                
               /*  if ( !isset($data['access_level']) ) {
                    $messages->setErrorMessage("Select the Access Level.");
                }
                elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                    $messages->setErrorMessage("Selected Access Level not found.");
                } */
                
                
                
                // Format the Date of Delivery, if provided.
                if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                    $data['do_d'] = explode('/', $data['do_d']);
                    $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
                }
                
                //As order form differentiated as per the permissions 
                
                    if ( isset($data['do_e']) && !empty($data['do_e']) ) {
                        $data['do_e'] = explode('/', $data['do_e']);
                        $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                    }
                    
                    if ( isset($data['do_fe']) && !empty($data['do_fe']) ) {
                        $data['do_fe'] = explode('/', $data['do_fe']);
                        $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                    }
                 
                
                // Check for the duplicate Order Number.
                $list = NULL;
                $condition_query = " WHERE number = '". $data['number'] ."' AND id != '". $data['or_id'] ."'";
                if ( ProspectsOrder::getList($db, $list, 'id', $condition_query)>0 ) {
                    $messages->setErrorMessage("An Order with the same Number already exists.");
                }
                $list = NULL;
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
			//As order form differentiated as per the permissions 
			if( $data['is_inv_cr']!=1){
                if ( !isset($data['s_id']) || empty($data['s_id']) ) {
                   //temp.comment $messages->setErrorMessage('Please select atleast one service.');
                }
                else {
                    foreach ( $data['s_id'] as $service ) {
                        $index = array();
                        if ( !arraySearchR($service, $lst_service) ) {
                            $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                        }
                    }
                }
            
          
				$is_part = false;
				$isrenewable = false;
				if(isset($data['particulars'])){
					foreach ( $data['particulars'] as $key=>$particular ) {
						$data['sub_sid_list'][$key]= $data['subsid'][$key]=$ss_sub_id_details=array();
										   
						if(!empty($data['s_id'][$key])){
							$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = ".$data['s_id'][$key]." 
							AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
							$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = ".$data['currency_id']." 
							AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;

							$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
							TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
							$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." 
							".$condition_query_sid ; 
							$query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

							if ( $db->query($query) ) {
								if ( $db->nf() > 0 ) {
									while ($db->next_record()) {
										$ss_subtitle =  trim($db->f("ss_title")).",";
										$ss_subprice =  $db->f("ss_price") ;
										$ss_subid =  $db->f("ss_id") ;
										$ss_sub_id_details[] = array(
											'ss_title'=>$ss_subtitle,
											'ss_price'=>$ss_subprice,
											'ss_id'=>$ss_subid	
										);
									}
								}
							} 
					   }
					   $data['sub_sid_list'][$key]=$ss_sub_id_details;
					   $sub_s_id_str='';
					   $sub_s_id_str =trim($data['subsidstr'][$key],",");
					   if(!empty($sub_s_id_str)){
							$data['subsid'][$key] = explode(",",$sub_s_id_str);
							$sub_s_id_str=",".$sub_s_id_str.",";
					   }
					   
						$tax1_id_str = $data['tax1_id'][$key] ;
						$tax1_sub1_id_str = $data['tax1_sub1_id'][$key] ;
						$tax1_sub2_id_str = $data['tax1_sub2_id'][$key] ;
						 
						if(!empty($tax1_id_str)){
							$arr = explode("#",$tax1_id_str);
							$data['tax1_name_str'][$key] = $arr[0] ;
							$data['tax1_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_name_str'][$key] = 0 ;		
							$data['tax1_id_check'][$key] = 0 ;		
						}
					
						if(!empty($data['tax1_value'][$key])){
							$tax1_val = $data['tax1_value'][$key] ;
							$percent =   substr($tax1_val,0,-1);
							$data['tax1_pvalue'][$key] = (float) ($percent/100);
						}else{
							$data['tax1_pvalue'][$key] =0;
						}
						if(!empty($data['tax1_sub1_value'][$key])){
							$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
							$percent_sub1 =   substr($tax1_sub1_val,0,-1);
							$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
						}else{
							$data['tax1_sub1_pvalue'][$key] =0;
						}
						if(!empty($data['tax1_sub2_value'][$key])){
							$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
							$percent_sub2 =   substr($tax1_sub2_val,0,-1);
							$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
						}else{
							$data['tax1_sub2_pvalue'][$key] =0;
						}
						
						$data['tax1_sid_opt'][$key]=array();
						$data['tax1_sid_opt_count'][$key]=0;
						if($data['tax1_id_check'][$key]>0){
							//tax1_sub_id Options of taxes 
							//$data['tax1_sid_opt'][$key] = array('0'=>2);
							$tax_opt = array();
							$tax_id = $data['tax1_id_check'][$key];
							$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." ORDER BY tax_name ASC ";
							ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
							if(!empty($tax_opt)){
								$data['tax1_sid_opt'][$key] = $tax_opt ;  
								$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
							}
						}
						if(!empty($tax1_sub1_id_str)){
							$arr = explode("#",$tax1_sub1_id_str);
							$data['tax1_sub1_name_str'][$key] = $arr[0] ;
							$data['tax1_sub1_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_sub1_name_str'][$key] = 0 ;		
							$data['tax1_sub1_id_check'][$key] = 0 ;		
						}
						if(!empty($tax1_sub2_id_str)){
							$arr = explode("#",$tax1_sub2_id_str);
							$data['tax1_sub2_name_str'][$key] = $arr[0] ;
							$data['tax1_sub2_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_sub2_name_str'][$key] = 0 ;		
							$data['tax1_sub2_id_check'][$key] = 0 ;		
						}
						
						if(!empty($data['tax1_id_check'][$key])){
							$data['alltax_ids'][]=$data['tax1_id_check'][$key] ;	
						}		
						if(!empty($data['tax1_sub1_id_check'][$key])){					
							$data['alltax_ids'][]=$data['tax1_sub1_id_check'][$key] ;
						}
						if(!empty($data['tax1_sub2_id_check'][$key])){					
							$data['alltax_ids'][]=$data['tax1_sub2_id_check'][$key] ;
						}
						$data['tax_check_ids'][] = $data['tax1_id_check'][$key] ;
						$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;  
					
						if(empty($data['s_id'][$key])){
							$messages->setErrorMessage('Select the services.');
						}
						if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key]) && empty($data['p_id'][$key])){
							 $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
						}
						if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
							if ( empty($data['particulars'][$key]) ) {
								$messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
							}
							if ( empty($data['p_amount'][$key]) ) {
								$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
							}
							else {
								if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
									$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
								}
								else {
									/*
									if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
										$messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
									   
									}elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){                                
										$messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');                                
									}else{*/
										/*
										if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || 
										empty($data['do_fe']) ) ){
											$messages->setErrorMessage('Service '. $data['ss_title'][$key] .' 
											is Renewal Service. Please specify Service From Date &&  Renewal Date');

										}else{*/
											$is_part = true;
											$do_e  = "";
											$do_fe = "";  
											if($data['is_renewable'][$key] =='1'){
												  //$do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
												  //$do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
												  $isrenewable = true;
											}
											
										//}
									//}
									
								}
							}
						}
					}
				}
				$data['taxpval_check'] = array_unique($data['taxpval_check']);
				$countPVal = count($data['taxpval_check']);
				if( $countPVal > 1 ){
					$messages->setErrorMessage('All Particulars of TAXES/VATS should be of same %');
				}
				$data['isrenewable']=$isrenewable;
				if(!$isrenewable && (!empty($data['do_e']) || !empty($data['do_fe']) )){            
					 $messages->setErrorMessage('You are not select any Renewable Services So Service From Date and Renewal Date should be blank.  ');
				}
				/* 
				$data['taxpval_check'] = array_unique($data['taxpval_check']);
				$countPVal = count($data['taxpval_check']);
				if( $countPVal > 1 ){
					$messages->setErrorMessage('All Particulars of TAXES/VATS should be of same %');
				}
				$data['alltax_ids']=array_unique($data['alltax_ids']);
				//To check that order should be of only one tax
				$data['tax_check_ids']= array_unique(array_flip($data['tax_check_ids']));
				$countTax = count($data['tax_check_ids']);
				if( $countTax > 1 ){
					$messages->setErrorMessage('Particulars with Different Taxes are not allowed. All Particulars should be of
					same TAXES/VATS or without any TAXES/VATS');
				}
				*/
				if ( !$is_part ) {
				   //temp.comment $messages->setErrorMessage('Please check the Services entries For the Order.');
				}
				else {
					//$data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
				}

				if ( (!isset($data['amount']) || empty($data['amount'])) && isset($data['particulars']) ) {
					$messages->setErrorMessage("The Amount is not specified.");
				}
				else {
					if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
						$messages->setErrorMessage("The Total Amount is not valid.");
					}
					else {
						$data['balance'] = $data['amount'];
					}
				}
			}
            // Check for start date
            if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
            //Check Estimated hrs should not be characters
            if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function validateBifurcationAdd(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            if(isset($data['service'])){
                foreach ( $data['service'] as $key=>$service ) {
                    if(empty($data['service'][$key])){
                         $messages->setErrorMessage('The Service field at Position '. ($key+1) .' cannot be empty.');
                    }
                    
                    if ( empty($data['price'][$key]) ) {
                        $messages->setErrorMessage('The Price of Service '. ($key+1) .' cannot be empty.');
                    }
                    else {
                        if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['price'][$key]) ) {
                            $messages->setErrorMessage('The Price of Service '. ($key+1) .' is not valid.');
                        }
                        
                    }
                    
                }
            }else{            
                 $messages->setErrorMessage('Select Services.');
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        function getDetailsBifurcation( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
           
            $query .= " FROM ". TABLE_ORD_BIFURCATION;
          
               
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
        function deleteBifurcation($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $query = "DELETE FROM ". TABLE_ORD_BIFURCATION."  WHERE id = '$id'";
                                            
            if ( $db->query($query) ) {            
                $messages->setOkMessage("The Order Bifurcation has been deleted.");
            }
        }
        /**
         * This function is called after a new Order has been created
         * against a Pre Order. This function is called only once for 
         * each Order.
         *
         */
        function setPreOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_BILL_PO
                        ." SET status = '$status'"
                        ." WHERE id = '$id'";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		
		/**
		 * This function is used to retrieve the Members of the ProspectsOrder.
		 * 
		 */
		function getTeamMembers($id, &$team_members, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$order = NULL;
			if ( ProspectsOrder::getList($db, $order, 'team', " WHERE id = '$id'" ) > 0 ) {
				include_once ( DIR_FS_INCLUDES .'/user.inc.php');

				$order = $order[0];
				$order['team'] = "'". implode("','", (explode(',', $order['team']))) ."'";
                $order['team_members']= '';
                if ( User::getList($db, $team_members, 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $order['team'] .")") > 0 ) {
					return (true);
				}
			}
			return (false);
		}

        
		
		function getDeliveryDate(&$db, $order_id) {
			$query = "SELECT do_d FROM ". TABLE_PROSPECTS_ORDERS
						." WHERE id = '". $order_id ."'";
			$db->query($query);
			if ( $db->nf()>0 && $db->next_record() ) {
				return ($db->f('do_d'));
			}
			return ('');
		}
        
        //Function required for displaying the order_id + order_title from  work_timeline
        function getOrderList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                 $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PROSPECTS_ORDERS;
            $query .= " LEFT JOIN ". TABLE_PROSPECTS." ON ". TABLE_PROSPECTS .".user_id = ". TABLE_PROSPECTS_ORDERS .".client ";
            //$query .= " LEFT JOIN ". TABLE_WORK_TL." ON ". TABLE_WORK_TL .".order_id = ". TABLE_PROSPECTS_ORDERS .".id ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
					
					if ( strpos($required_fields, 'team') > 0 ) {
						include_once ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$count = count($list);
						for ( $i=0; $i<$count; $i++ ) {
							// Read the Team Member Details.
							$list[$i]['team'] = "'". implode("','", (explode(',', $list[$i]['team']))) ."'";
							$list[$i]['team_members']= '';
							User::getList($db, $list[$i]['team_members'], 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $list[$i]['team'] .")");
							
							// Read the latest Time Line update.
							include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
							$list[$i]['work'] = NULL;
							//WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, order_title, to_id, by_id, do_assign, comments, status');
							WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, to_id, by_id, do_assign, comments, status');
							$list[$i]['work'] 		= $list[$i]['work'][0];
							//$list[$i]['order_title']= $list[$i]['work']['order_title'];
							
							$list[$i]['comments'] 	= $list[$i]['work']['comments'];
							User::getList($db, $list[$i]['work']['by'], 'user_id, number, f_name, l_name, email', "WHERE user_id = '". $list[$i]['work']['by_id'] ."'");
							$list[$i]['work']['by'] = $list[$i]['work']['by'][0];
						}
					}
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
		
        
        
        /**
		 * This function is used to retrieve the details of the order.
		 *
		 */
		function getOrder($ord_id, &$order, &$extra) {
           // include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
			
			foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
            $condition_query = " WHERE (". TABLE_PROSPECTS_ORDERS .".id = '". $ord_id ."' "
                                ." OR ". TABLE_PROSPECTS_ORDERS .".number = '". $ord_id ."')";
            $fields = TABLE_PROSPECTS_ORDERS .'.*'
                        .','. TABLE_PROSPECTS .'.user_id AS c_user_id'
                        .','. TABLE_PROSPECTS .'.number AS c_number'
                        .','. TABLE_PROSPECTS .'.f_name AS c_f_name'
                        .','. TABLE_PROSPECTS .'.l_name AS c_l_name'
                        .','. TABLE_PROSPECTS .'.email AS c_email'
                        .','. TABLE_PROSPECTS .'.status AS c_status';
						
			if ( ProspectsOrder::getDetails($db, $order, $fields, $condition_query) > 0 ) {
                $order = $order['0'];

				// Set up the Client Details field.
				$order['client_details']= $order['c_f_name'] .' '. $order['c_l_name']
												.' ('. $order['c_number'] .')'
												.' ('. $order['c_email'] .')';
				
				// Set up the dates.
//				$invoice['do_i']  = explode(' ', $invoice['do_i']);
//				$temp               = explode('-', $invoice['do_i'][0]);
//				$invoice['do_i']  = NULL;
//				$invoice['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
//				
//				$invoice['do_d']  = explode(' ', $invoice['do_d']);
//				$temp               = explode('-', $invoice['do_d'][0]);
//				$invoice['do_d']  = NULL;
//				$invoice['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
//				
//				$invoice['do_e']  = explode(' ', $invoice['do_e']);
//				$temp               = explode('-', $invoice['do_e'][0]);
//				$invoice['do_e']  = NULL;
//				$invoice['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				
				// Read the Particulars.
				$temp = NULL;
				$temp_p = NULL;
				$condition_query = "WHERE ord_no = '". $order['number'] ."'";
				ProspectsOrder::getParticulars($db, $temp, '*', $condition_query);
                if(!empty($temp)){
                    foreach ( $temp as $pKey => $parti ) {
                      
                        $temp_p[]=array(
                                        'p_id'=>$temp[$pKey]['id'],
                                        'particulars'=>$temp[$pKey]['particulars'],
                                        'p_amount' =>$temp[$pKey]['p_amount'] ,                                   
                                        's_id' =>$temp[$pKey]['s_id'] ,                                   
                                        'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                        'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                        'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                        'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                        'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                        'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                        'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                        'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                        'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                        );
                    }
                }
                $order['particular_details']=$temp_p ;
				$condition_query = '';
				
				
				
				
            }
            else {
                $messages->setErrorMessage("The Order was not found or you do not have the Permission to access this Order.");
            }
								
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
    }
    
?>
