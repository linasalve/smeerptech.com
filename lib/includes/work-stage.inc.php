<?php 

	class WorkStage {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => WorkStage::BLOCKED,
							'ACTIVE'  => WorkStage::ACTIVE,
							'PENDING' => WorkStage::PENDING,
							'DELETED' => WorkStage::DELETED
						);
			return ($status);
		}
		
        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_WORK_STATGES;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Order Status.
         *
         * @param    string      unique ID of the Order Status whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, WorkStage::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $order_status = NULL;
                if ( (WorkStage::getList($db, $order_status, 'id', " WHERE id = '$id'")) > 0 ) {
                    $order_status = $order_status[0];
					$query = "UPDATE ". TABLE_WORK_STATGES
								." SET status = '$status_new' "
								." WHERE id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Order Status was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Order Status information from the database.
        *
        * @param   string      unique ID of the Order Status whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $order_status = NULL;
            if ( (WorkStage::getList($db, $order_status, 'id, status', " WHERE id = '$id'")) > 0 ) 
			{
                $order_status = $order_status[0];
				// Check if the Order Status has been assigned to any Order Timeline?
				$query = "SELECT id FROM ". TABLE_BILL_ORDERS_TL
							." WHERE status = '". $id ."'";
				if ( $db->query($query) ) {
					if ( $db->nf()>0 ) {
						$messages->setErrorMessage("The Order Status cannot be deleted, it has been assigned to a Order Timeline.");
					}
					else {
						if ( $order_status['status'] == WorkStage::BLOCKED || $order_status['status'] == WorkStage::DELETED) 
						{
							$query = "DELETE FROM ". TABLE_WORK_STATGES
										." WHERE id = '". $id ."'";
							if ( !$db->query($query) || $db->affected_rows()<=0 ) {
								$messages->setErrorMessage("The Order Status is not deleted.");
							}
							else {
								$messages->setOkMessage("The Order Status has been deleted.");
							}
						}
						else {
							$messages->setErrorMessage("Cannot delete Order Status.");
						}
					}
				}
				else {
					$messages->setErrorMessage("Your request cannot be processed right now. Please try again later.");
				}
			}
			else
				$messages->setErrorMessage("Order Status not found.");
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Order Status.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			//validate the service title 
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage('Title should be provided.');
			}
			elseif ( strlen($data['title'])>100 ) {
				$messages->setErrorMessage('The Title can only be 100 characters long.');
			}
			else {
				// Check for duplicate Status title.
				$order_status = NULL;
				if ( (WorkStage::getList($db, $order_status, 'title', " WHERE title = '". $data["title"] ."'")) > 0 ) {
					$messages->setErrorMessage('Service title name already exists. Please change the title.');
				}
			}
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating the Order Status.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the service title 
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage('Title should be provided.');
			}
			elseif ( strlen($data['title'])>100 ) {
				$messages->setErrorMessage('The Title can only be 100 characters long.');
			}
			else {
				// Check for duplicate Status title.
				$order_status = NULL;
				$condition_query = " WHERE title = '". $data["title"] ."'"
									." AND id != '". $data['id'] ."'";
				if ( (WorkStage::getList($db, $order_status, 'title', $condition_query)) > 0 ) {
					$messages->setErrorMessage('The Title already exists. Please enter some other title.');
				}
			}

            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
		
		
		/**
		 * This function is used to retrieve the list of the Work Stage.
		 * for using.
		 *
		 */
		function getWorkStages(&$db, &$status) {
			WorkStage::getList($db, $status, 'id, title', " WHERE status='1' ORDER BY status_order ");
			return ($status);
		}
		
		/**
		 * This function is used to retrieve the title of the Work Stage.
		 *
		 */
		function getWorkStageTitle(&$db, $id) {
			$work = '';
			if ( WorkStage::getList($db, $work, 'title', " WHERE status='1' AND id='$id' ") ) {
				$work = $work[0]['title'];
			}
			return ($work);
		}
		
    }
?>