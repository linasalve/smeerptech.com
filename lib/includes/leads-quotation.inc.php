<?php
	class LeadsQuotation {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
        const DELETED = 3;
		const COMPLETED = 4;

		
		const PENDING_INV_R   = 0;
		const COMPLETED_INV_R = 1;
		const CANCELLED_INV_R = 2;
		
		function getStatus() {
			$status = array(
							'APPROVED'    => LeadsQuotation::ACTIVE,
							'PENDING'   => LeadsQuotation::PENDING,
							'BLOCKED' => LeadsQuotation::BLOCKED,
                            'DELETED'   => LeadsQuotation::DELETED,
                           //'COMPLETED' => LeadsQuotation::COMPLETED
						);
			return ($status);
		}
		function getReminderStatus() {
			$status = array('PENDING' => LeadsQuotation::PENDING_INV_R,							 
							'COMPLETED'   => LeadsQuotation::COMPLETED_INV_R,
                            'CANCELLED'   => LeadsQuotation::CANCELLED_INV_R
						);
			return ($status);
		}
        /*
         array of days before which reminder of expiry has been sent
         ie 60 days before expiry,
            30 days before expiry
        */        
        function expirySetDays() {
			$arr = array(120=>120,
                         90=>90
						);
			return ($arr);
		}
        
        function getStatusTitle() {
            $status = array(LeadsQuotation::BLOCKED    => 'Bad Debt',
                            LeadsQuotation::ACTIVE     => 'Paid Fully',
                            LeadsQuotation::PENDING    => 'Payment Pending',
                            LeadsQuotation::DELETED    => 'Deleted',
                           // LeadsQuotation::COMPLETED  => 'Renewed',
                            LeadsQuotation::COMPLETED  => 'Completed',
                            'CANCELLED'         => 'Bad Debt',
                            'ACTIVE'            => 'Active',
                           //'ACTIVE'            => 'Paid Fully',
                            'PENDING'           => 'Payment Pending',
                            'DELETED'           => 'Deleted',
                            //'PROCESSED'         => 'Renewed',
                            //'PROCESSED'         => 'Completed',
                           );
            return ($status);
        }
        
		/**
		 *	Function to get all the LeadsQuotations.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			//$query .= " FROM ". TABLE_LEADS_QUOTATION;
			$query .= " FROM ". TABLE_LEADS_QUOTATION ;
            $query .= " LEFT JOIN ". TABLE_LEADS_ORDERS
                        ." ON ". TABLE_LEADS_ORDERS .".number = ". TABLE_LEADS_QUOTATION .".or_no ";
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_LEADS_QUOTATION .".created_by ";
            $query .= " LEFT JOIN ". TABLE_SALE_LEADS
                        ." ON ". TABLE_SALE_LEADS .".lead_id = ". TABLE_LEADS_QUOTATION .".client ";            
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            /* comment 2009-03-mar-21 BOF
            $query .= " FROM ". TABLE_LEADS_QUOTATION;
            $query .= " LEFT JOIN ". TABLE_SALE_LEADS
                        ." ON ". TABLE_SALE_LEADS .".user_id = ". TABLE_LEADS_QUOTATION .".client ";
            comment 2009-03-mar-21 EOF
            */
            
            $query .= " FROM ". TABLE_LEADS_QUOTATION ;
            $query .= " LEFT JOIN ". TABLE_LEADS_ORDERS
                        ." ON ". TABLE_LEADS_ORDERS .".number = ". TABLE_LEADS_QUOTATION .".or_no ";
            //$query .= " LEFT JOIN ". TABLE_BILL_ORD_P
                 //       ." ON ". TABLE_BILL_ORD_P .".ord_no = ". TABLE_LEADS_QUOTATION .".or_no ";
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_LEADS_QUOTATION .".created_by ";
            $query .= " LEFT JOIN ". TABLE_SALE_LEADS
                        ." ON ". TABLE_SALE_LEADS .".lead_id = ". TABLE_LEADS_QUOTATION .".client ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
               
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( in_array($status_new, LeadsQuotation::getStatus()) ) {
                $order = NULL;
                if ( (LeadsQuotation::getList($db, $invoice, TABLE_LEADS_QUOTATION.'.id,'.TABLE_LEADS_QUOTATION.'.access_level,'.TABLE_LEADS_QUOTATION.'.created_by,'.TABLE_LEADS_QUOTATION.'.status', " WHERE ".TABLE_LEADS_QUOTATION.".id = '$id'")) > 0 ) {
                    $invoice = $invoice[0];
					
					/* // Check Access Level for the invoice.
					if ( ($invoice['created_by'] == $my['user_id']) && ($invoice['access_level'] >= $access_level) ) {
						$messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
					}
					elseif ( ($invoice['created_by'] != $my['user_id']) && ($invoice['access_level'] >= $access_level_ot) ) {
						$messages->setErrorMessage("Cannot change Status of Invoice created by others, with current Access Level.");
					}
					else { */
                    	if ( $invoice['status'] != LeadsQuotation::COMPLETED ) {
                            $query = "UPDATE ". TABLE_LEADS_QUOTATION
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("Status not updated. New and Old status is same.");
                        }
                    //}
                }
                else {
                    $messages->setErrorMessage("The Proposal was not found.");
                }
            }
            else {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $order = NULL;
            if ( (LeadsQuotation::getList($db, $order, TABLE_LEADS_QUOTATION.'.id,'.TABLE_LEADS_QUOTATION.'.or_no,'.TABLE_LEADS_QUOTATION.'.number, '.TABLE_LEADS_QUOTATION.'.access_level,'.TABLE_LEADS_QUOTATION.'.status', " WHERE ".TABLE_LEADS_QUOTATION.".id = '$id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != LeadsQuotation::COMPLETED ) {
                    if ( $order['status'] == LeadsQuotation::BLOCKED ) {
                        //if ( $order['access_level'] < $access_level ) {
                             /* check receipt created or not if receipt created then dont delete it 
                             ie Proposal will be deleted only after deleting the receipts
                            */
                            $query = " UPDATE ". TABLE_LEADS_QUOTATION
                                            ." SET status ='".LeadsQuotation::DELETED."' WHERE id = '$id'";
							$db->query($query);
							
                            $messages->setOkMessage("The Proposal has been deleted.");
                        /* }
                        else {
                            $messages->setErrorMessage("Cannot Delete Proposal with the current Access Level.");
                        } */
                    }
                    else {
                        $messages->setErrorMessage("To Delete Proposal it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Proposal cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Quotation was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        /**
		 * Function to validate the input from the User while sending the invoice via email.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
	    function validateSendInvoice(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$data['mkex_fname']   = '';
			$data['mkex_lname']   = '';
			$data['mkex_designation'] ='';
			$data['mkex_mobile1']   ='';					
			$data['mkex_email']   ='';	
			
			if(!empty($data['flw_ord_id'])){
				$table= TABLE_LEADS_ORDERS ;
				$fields1 = TABLE_LEADS_ORDERS.".order_closed_by "; 
				$condition2 = " WHERE ".TABLE_LEADS_ORDERS.".id ='".$data['flw_ord_id']."' LIMIT 0,1" ;
				$userArr = getRecord($table,$fields1,$condition2);
				$table= $fields1 = 	$condition2='';
				 
				
				if(!empty($userArr['order_closed_by'])){
			
					$table = TABLE_USER;
					$condition2 = " WHERE ".TABLE_USER.".user_id = '".$userArr['order_closed_by']."'
					AND ".TABLE_USER.".user_id != '".SALES_MEMBER_USER_ID."' 
					AND ".TABLE_USER.".department ='".ID_MARKETING."'" ;
					
					$fields1 =  TABLE_USER .".f_name, ".TABLE_USER.".l_name,".TABLE_USER.".department,"
					.TABLE_USER.".marketing_contact, "
					.TABLE_USER.".desig, "
					.TABLE_USER.".email, "
					.TABLE_USER.".user_id " ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					
					if(!empty($detailsArr)){
						
						$data['ord_user_id']   	= $detailsArr['user_id'];
						$data['department']   	= $detailsArr['department'];
						$data['mkex_fname']   	= $detailsArr['f_name'];
						$data['mkex_lname']   	= $detailsArr['l_name'];
						$data['display_designation']   = $detailsArr['desig'];
						$data['mkex_email']   = $detailsArr['email'];
						$data['display_mobile1'] =$data['marketing_contact']  = $detailsArr['marketing_contact'];
						$data['display_user_id']   = $detailsArr['user_id'];
						$data['display_name'] = $data['mkex_fname']." ".$data['mkex_lname'];
						//This is the marketing person identity
						$data['tck_owner_member_id'] = $detailsArr['user_id'];
						$data['tck_owner_member_name']  = $detailsArr['f_name']." ".$detailsArr['l_name'];
						$data['tck_owner_member_email'] = $detailsArr['email'];
						$data['marketing_email'] = $detailsArr['email'];
						$data['marketing_contact'] = $detailsArr['marketing_contact'];
						$data['marketing'] = 1;
					 
						//disply random name EOF
						 
					}else{
						$messages->setErrorMessage('Selected Lead is not closed by Marketing Person.');
					}
				}else{
					$messages->setErrorMessage('Selected Lead is not closed by anyone<br/> Please update it.');
				}
			}else{				
				$messages->setErrorMessage('Lead No is not found.');
			}
			
            if( !isset($data['to']) && empty($data['to']) && !isset($data['mail_client']) 
				&& !isset($data['mail_to_all_su'])  && !isset($data['mail_to_su'])){
				
                $messages->setErrorMessage('Please select member to send the proposal or add valid email id.');
            }
			
            if(!empty($data['to'])){
				if(!isValidEmail($data['to'])){
					$messages->setErrorMessage('To Email Address is not valid.');
				}
            }
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$files = $_FILES;
            // Validate the Order.
            $list = NULL;
            if ( LeadsQuotation::getList($db, $list, TABLE_LEADS_QUOTATION.'.id', " WHERE ".TABLE_LEADS_QUOTATION.".or_no = '". $data['or_no'] ."' AND ".TABLE_LEADS_QUOTATION.".status !='".LeadsQuotation::DELETED."'")>0 ) {
                $messages->setErrorMessage("The Lead Order has been processed already.");
            }
            else {
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_LEADS_ORDERS
                            ." WHERE number = '". $data['or_no'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage("The Lead Order was not found in the database.");
                    }
                    else {
                        $db->next_record();
                        if ( $access_level >= $db->f('access_level') ) {
                            if ( ($db->f('status') != ACTIVE) && ($db->f('status') != PROCESSED) && ($db->f('status') != COMPLETED) ) {
                                $messages->setErrorMessage("The Lead Order is not yet approved.");
                            }
                            $data['client'] = $db->f('client');
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Proposal for this LeadOrder.");
                        }
                    }
                }
            }
			 if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
             /* if(empty($data['financialYr'])){
                 $messages->setErrorMessage("Select Financial Yr.");
            }
           if(!empty($data['financialYr']) && !empty($data['company_id']) ){
            
                $_ALL_POST['number'] =$data['number']= getOldCounterNumber($db,'QUOT',$data['company_id'],$data['financialYr']); 
                //$_ALL_POST['number'] =$data['number']= getCounterNumber($db,'ORD',$data['company_id']); 
                if(empty($data['number']) ){
                    $messages->setErrorMessage("Quotation Number was not generated.");
                }
            }else{
                $messages->setErrorMessage(" Financial Year and Company Name are not Found.");
            } */
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creator's status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }

            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Prospect for whom the Proposal is being Created.");
            }
            else {
                $query = "SELECT lead_id as user_id, number, f_name, l_name, email,org,billing_name, status FROM ". TABLE_SALE_LEADS." WHERE lead_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Prospect was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Prospect is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
			if (  empty($data['quotation_subject']) ) {
                $messages->setErrorMessage("Please enter the Proposal Subject.");
            }
            
            // Check the billing name. 
           if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage("Billing Name is not specified in the Selected Proposal.<br/> Please update the Billing Name in Proposal's Profile");
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                $region         = new RegionLead();
                $region->setAddressOf(TABLE_SALE_LEADS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
            // Format the Date of Invoice.
            if ( !isset($data['do_i']) || empty($data['do_i']) ) {
                $messages->setErrorMessage("The Date of Proposal is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_i']) ) {
                $messages->setErrorMessage("Format of the Date of Proposal is invalid.");
            }
            else {
                $data['do_i'] = explode('/', $data['do_i']);
                //generate invoice number on the basis of invoice date 
                // $data['number'] = "PT". $data['do_i'][2]."-INV-".$data['do_i'][1]. $data['do_i'][0].date("hi-s") ;
                $data['do_i'] = mktime(0, 0, 0, $data['do_i'][1], $data['do_i'][0], $data['do_i'][2]);
            }
            $data['number']=$data['inv_counter'] ='';
			if(!empty($data['company_id']) && !empty($data['do_i'])){				
				$detailInvNo = getQuotationNumber($data['do_i'],$data['company_id']); 				
				if(!empty($detailInvNo)){
					$data['number'] = $detailInvNo['number'];
					$data['inv_counter'] = $detailInvNo['inv_counter'];
				}
			}
             // Format the Date of expiry from date, if provided.
            if ( !empty($data['do_fe']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_fe']) ) {
                    $messages->setErrorMessage("Format of the Service From Date is invalid.");
                }
                else {
                    $data['do_fe'] = explode('/', $data['do_fe']);
                    $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                }
                
            }else{
                $data['do_fe'] = '';
            }      
             
             // Format the Date of Invoice Expiry, if provided.
            if ( !empty($data['do_e']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_e']) ) {
                    $messages->setErrorMessage("Format of the Date Renewal is invalid.");
                }
                else {
                    $data['do_e'] = explode('/', $data['do_e']);
                    $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                }                
            }else{
                 $data['do_e'] = '';
        
            }
            // Format the Invoice Payment Due Date.			 
            if ( !isset($data['do_d']) || empty($data['do_d']) ) {
                $messages->setErrorMessage("The Due date for Payment of Proposal is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_d']) ) {
                $messages->setErrorMessage("Format of the Due date for Payment of Proposal is invalid.");
            }
            else {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
           
            
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Proposal Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
           
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Proposal Amount in INR is not valid.");
                }
            }
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{  
               
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
			$data['attach_file1'] = '';  
            if((!empty($files['attach_file1']) && (!empty($files['attach_file1']['name'])))){		
                $filename = $files['attach_file1']['name'];
                $data['attach_file1'] = $filename;
                $type = $files['attach_file1']['type'];
                $size = $files['attach_file1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file is greater than 1Mb');
                }
            }			
			$data['attach_file2'] = '';            
            if((!empty($files['attach_file2']) && (!empty($files['attach_file2']['name'])))){		
                $filename = $files['attach_file2']['name'];
                $data['attach_file2'] = $filename;
                $type = $files['attach_file2']['type'];
                $size = $files['attach_file2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file is greater than 1Mb');
                }
            }			
			$data['attach_file3'] = '';            
            if((!empty($files['attach_file3']) && (!empty($files['attach_file3']['name'])))){		
                $filename = $files['attach_file3']['name'];
                $data['attach_file3'] = $filename;
                $type = $files['attach_file3']['type'];
                $size = $files['attach_file3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                    $messages->setErrorMessage("$type type Of file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file is greater than 1Mb');
                }
            }			
			$data['attach_file4'] = '';            
            if((!empty($files['attach_file4']) && (!empty($files['attach_file4']['name'])))){		
                $filename = $files['attach_file4']['name'];
                $data['attach_file4'] = $filename;
                $type = $files['attach_file4']['type'];
                $size = $files['attach_file4']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                    $messages->setErrorMessage("$type type Of file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file is greater than 1Mb');
                }
            }
			$data['attach_file5'] = '';            
            if((!empty($files['attach_file5']) && (!empty($files['attach_file5']['name'])))){		
                $filename = $files['attach_file5']['name'];
                $data['attach_file5'] = $filename;
                $type = $files['attach_file5']['type'];
                $size = $files['attach_file5']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                    $messages->setErrorMessage("$type type Of file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file is greater than 1Mb');
                }
            }
           /*  if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            } */
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}
            if(empty($data['number']) ){
                $messages->setErrorMessage("An Proposal Number was not generated.");
            }
            if( empty($data['inv_counter']) ){
                $messages->setErrorMessage("An Proposal Counter was not generated.");
            } 
            // Check for the duplicate Invoice Number.
            $list = NULL;
            if ( LeadsQuotation::getList($db, $list, TABLE_LEADS_QUOTATION.'.id', " WHERE ".TABLE_LEADS_QUOTATION.".number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Proposal with the same Number already exists.<br/>Please try again.");
            }
            $list = NULL;
            
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // Validate the Order.
            
            $list = NULL;
            if ( LeadsQuotation::getList($db, $list, TABLE_LEADS_QUOTATION.'.id,'.TABLE_LEADS_QUOTATION.'.created_by',
			" WHERE ".TABLE_LEADS_QUOTATION.".or_no = '". $data['or_no'] ."' AND 
			".TABLE_LEADS_QUOTATION.".status !='".LeadsQuotation::DELETED."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_LEADS_ORDERS
                                ." WHERE number = '". $data['or_no'] ."' AND status !='".LeadsOrder::DELETED."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage('The Lead Order was not found in the database.');
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            /* As complete ma
                            if ( $db->f('status') != COMPLETED ) {
                                $messages->setErrorMessage('The Order has not been marked as completed.');
                            }*/
                            $data['client'] = $db->f('client');
                        }
                        else {
                            $messages->setErrorMessage('You donot have the Right to update Proposal for this Order.');
                        }
                    }
                }
            }
            else {
                $messages->setErrorMessage('The Proposal for the Lead Order has not been created yet.');
            }
            
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage('Creator details cannot be retrieved, Please try again.');
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage('The Creator was not found.');
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage('The Creators status is not Active.');
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }

            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage('Select the Prospect for whom the Proposal is being Created.');
            }
            else {
                $query = "SELECT lead_id as user_id, number, f_name, l_name, email, billing_name,status FROM ". TABLE_SALE_LEADS
                            ." WHERE lead_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Prospect was not found.");
                } else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Prospect is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            if (  empty($data['quotation_subject']) ) {
                $messages->setErrorMessage("Please enter the Proposal Subject.");
            }
            // Check the billing name.
           
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage("Billing Name is not specified in the Selected Prospect.<br/> Please update the Billing Name in Prospect's Profile");
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                $region         = new RegionLead();
                $region->setAddressOf(TABLE_SALE_LEADS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
            // Format the Date of Proposal.
			 
            if ( !isset($data['do_i']) || empty($data['do_i']) ) {
                $messages->setErrorMessage("The Date of Proposal is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_i']) ) {
                $messages->setErrorMessage("Format of the Date of Proposal is invalid.");
            }
            else {
                $data['do_i'] = explode('/', $data['do_i']);
                $data['do_i'] = mktime(0, 0, 0, $data['do_i'][1], $data['do_i'][0], $data['do_i'][2]);
            } 
            
           
                                     
            // Format the Date of expiry from date, if provided.
            if ( !empty($data['do_fe']) ) {
            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_fe']) ) {
                    $messages->setErrorMessage("Format of the Service From Date is invalid.");
                }
                else {
                    $data['do_fe'] = explode('/', $data['do_fe']);
                    $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                }
                
           }else{
                $data['do_fe'] = '';
           }
                    
             // Format the Date of Invoice Expiry, if provided.
            if ( !empty($data['do_e']) ) {
            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_e']) ) {
                    $messages->setErrorMessage("Format of the Date Renewal is invalid.");
                }
                else {
                    $data['do_e'] = explode('/', $data['do_e']);
                    $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                }
                
            }else{
                 $data['do_e'] = '';
        
            }           
            // Format the Proposal Payment Due Date.
            if ( !isset($data['do_d']) || empty($data['do_d']) ) {
                $messages->setErrorMessage("The Due date for Payment of Proposal is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_d']) ) {
                $messages->setErrorMessage("Format of the Due date for Payment of Proposal is invalid.");
            }
            else {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
            // Validate Services.
            /*
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }*/
            /*
            $data['query_p'] = 'INSERT INTO '. TABLE_BILL_INV_P .' (inv_no, particulars, p_amount) VALUES ';
            $is_part = false;
            foreach ( $data['particulars'] as $key=>$particular ) {
                if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                    if ( empty($data['particulars'][$key]) ) {
                        $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( empty($data['p_amount'][$key]) ) {
                        $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                    }
                    else {
                        if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                        }
                        else {
                            $is_part = true;
                            $data['query_p'] .= "('". $data['number'] ."', '". $data['particulars'][$key] ."', '". $data['p_amount'][$key] ."'),";
                        }
                    }
                }
            }
            if ( !$is_part ) {
                $messages->setErrorMessage('The Particular for the Invoice cannot be empty.');
            }
            else {
                $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
            }
            */
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Proposal Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Proposal Amount in INR is not valid.");
                }
            }
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{            
                
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
            
            
           /*  
		    if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            } */
           
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                $messages->setErrorMessage("The Status is not valid.");
            }
            

            
            // Check for the duplicate Invoice Number.
            /*
            $list = NULL;
            if ( Invoice::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Invoice with the same Number already exists.");
            }
            $list = NULL;
            */
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for 
         * each Invoice.
         *
         */
        function setOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_LEADS_ORDERS
					." SET status = '$status'"
					." WHERE ( id = '$id' OR number = '$id' )";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for
         * each Invoice so that the service provided is updated in the
         * Clients Account.
         *
         */
        function UpdateClientService(&$db, $client_id, $services) {
            $return     = false;
            $c_service  = 'NULL';
            
            $query = "SELECT service_id FROM ". TABLE_SALE_LEADS." WHERE user_id = '". $client_id ."'";
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
                $c_service = processSQLData($db->f('service_id'));
                if ( empty($c_service) ) {
                    $c_service = array();
                }
                else {
                    $c_service = explode(',', $c_service);
                }
                
                $c_service = arrayCombine($c_service, $services);
                $c_service = implode(',', $c_service);
                $query = "UPDATE ". TABLE_SALE_LEADS
                            ." SET service_id = '$c_service'"
                            ." WHERE user_id = '$client_id'";
                if ( $db->query($query) ) {
                    $return = true;
                }
            }
            return ($return);
        }
        
        /**
         * This function is used to create the File in PDF or HTML format for the Invoice.
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createFile($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("leads-quotation-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_QUOTATIONS_HTML, 0777);
                if ( is_writable(DIR_FS_QUOTATIONS_HTML) ) {
                    $file_name = DIR_FS_QUOTATIONS_HTML ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Proposal was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Proposal is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
                   /*
                   $pdf_content = Invoice::createPdfContent($data);
                   $invoiceArr['content'] = $pdf_content ;
                   $invoiceArr['header'] = '' ;
                   $invoiceArr['footer'] = '' ;
                   createInvoicePDF($data["number"],$invoiceArr);
                   $file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";
				   */
            }elseif($type == 'HTMLPRINT'){
                $s->assign('data', $data);
                $file_data = $s->fetch("leads-quotation-tpl-print.html");
                
                // Create a new file and save to disk.
               /*  $old_umask = umask(0);
                @chmod(DIR_FS_QUOTATIONS_FILES_HTMLPRINT, 0777);
                if ( is_writable(DIR_FS_QUOTATIONS_HTMLPRINT_FILES) ) {
                    $file_name = DIR_FS_QUOTATIONS_HTMLPRINT_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The quotation was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }else {
                    $messages->setErrorMessage("The Directory to store Invoice is not writable.");
                }
                umask($old_umask); */
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
       
        
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_LEADS_ORD_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }                
                return ( $total );
            }
            else {
                return false;
            }
    }

function createPdfFile($data,$filedestination) {        
$variables['images'] = DIR_WS_IMAGES_NC ;
$css=  "<style type=\"text/css\" rel=\"stylesheet\">
    body {margin-right:auto;
            margin-left:auto;
        }
        #content {
        margin:0;
        width: 100%;
        text-align:center;
        }
        #content-main {
        width: 731px;
        text-align:center;
        margin-left:auto;
        margin-right:auto;
        }
        .heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:18px;
            color:#000000;
            text-decoration:none;
            font-weight:bold;
        }
        .sub-heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#686868;
            line-height:15px;
            text-decoration:none;
            font-weight:regular;
        }
        .invoice-border{ 
             background:url(".$variables['images']."/invoice-repeater-new.jpg);
             background-repeat:repeat-y;
             width:253px;
         }	
        .invoice-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:14px;
            color:#000000;
            text-decoration:none;
            font-weight:bold;
        }
        
        .content-heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:10px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:bold;
        }
        .address-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:10px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
		.address-text-small{
			font-family:Arial, Verdana, 'San serif';
            font-size:9px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
		
		}
        .green-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#176617;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .white-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .design-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#797979;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .custom-content{
            font-family:arial,verdana,sans-serif;
            font-size:11px;
            color:#646464;
        }
        .content{
            font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
            lignt-height:10px;
        }
		.contentsub{
            font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
            lignt-height:10px;
			font-weight:bold;
        }
		.contenthead{
            font-family:arial,verdana,sans-serif;
            font-size:13px;
            color:#4f4f4f;
            lignt-height:11px;
        }
        .content-small{
            font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#4f4f4f;
        }
		.content-small-term{
            font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#726e6e;
        }
		.content-small-7{
        	font-family:arial,verdana,sans-serif;
            font-size:7px;
            color:#4f4f4f;
	    }
		.content-small-10{
        	font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
	    }
        .spacer{background:url(".$variables['images']."/spacer.gif)repeat;}
        .bg1{background-color:#898989;}
        .bg2{background-color:#CCCCCC;}
        .bg3{background-color:#f4f4f4;}
        .bg4{background-color:#ba1a1c;}
        .bg5{background-color:#956565;}
        .bg6{background-color:#BEBE74;}
        .bg7{background-color:#48b548;}
        .bg8{background-color:#000000;}
        .bg9{background-color:#ca2825;}
        .bdb1{border-bottom:1px solid #000000;}
        .b {font-weight:bold;}
        .ac {text-align:center;}
        .al {text-align:left;}
        .ar {text-align:right;}
        .vt {vertical-align:top;}
        .vm {vertical-align:middle;}
        .vb {vertical-align:bottom;}
        .fs08 {font-size:0.8em;}
        .fs11 {font-size:1.1em;}
        .fst11{font-size:1.1em; font-family:'Arial Narrow',Verdana,Arial,Sans-Serif;}
        .fs15 {font-size:1.5em;}
        .lh15 {line-height:1.5em;}
        .lh10 {line-height:1em;}                                    
        .pl2{padding-left:2px;}
        .pl5{padding-left:5px;}
        .pl8{padding-left:8px;}                                    
        .pl10{padding-left:10px;}
        .pl11{padding-left:11px;}
        .pl15{padding-left:15px;}
        .pl20{padding-left:20px;}
        .pl60{padding-left:60px;}
        .pl42{padding-left:42px;}                                    
        .pr2{padding-right:2px;}
        .pr5{padding-right:5px;}
        .pr10{padding-right:10px;}                                    
        .pb2{padding-bottom:2px;}
        .pb3{padding-bottom:3px;}
        .pb4{padding-bottom:4px;}
        .pb5{padding-bottom:5px;}        
        .pb10{padding-bottom:10px;}
		.pb13{padding-bottom:13px;} 
        .pb20{padding-bottom:20px;}
        .pb25{padding-bottom:25px;}
        .pb30{padding-bottom:30px;}
        .pb40{padding-bottom:40px;}
        .pb50{padding-bottom:50px;}
        .pb100{padding-bottom:100px;}
        
        .pt1{padding-top:1px;}        
        .pt2{padding-top:2px;}
        .pt3{padding-top:3px;}
        .pt4{padding-top:4px;}
        .pt5{padding-top:5px;}
        .pt6{padding-top:6px;}
        .pt8{padding-top:8px;}
        .pt10{padding-top:10px;}
        .pt15{padding-top:15px;}
        .pt20{padding-top:20px;}
        .pt25{padding-top:25px;}
        .pt30{padding-top:30px;}
        .pt32{padding-top:32px;}
        .pt45{padding-top:45px;}                                    
        .b1{border:collapse;border:1px solid #000000;}
        .wp100{width:100%;}
        .wp90{width:90%;}
        .wp70{width:70%;}
        .wp65{width:65%;}
        .wp60{width:60%;}
        .wp55{width:55%;}
        .wp50{width:50%;}
        .wp45{width:45%;}
        .wp40{width:40%;}
        .wp35{width:35%;}
        .wp30{width:30%;}
        .wp28{width:28%;}
        .wp25{width:25%;}
        .wp20{width:20%;}
        .wp15{width:15%;}
        .wp13{width:13%;}
        .wp12{width:12%;}
        .wp10{width:10%;}
        .wp9{width:9%;}
        .wp5{width:5%;}
        .wp6{width:6%;}
        .wp4{width:4%;}                                    
        .wpx7{width:7px;}
        .wpx9{width:9px;}
        .wpx10{width:10px;}
        .wpx20{width:20px;}
        .wpx712{width:712px;}
        .wpx729{width:729px;}
        .wpx731{width:731px;}
        .wpx330{width:330px;}
        .wpx337{width:337px;}
        .wpx348{width:348px;}
       
        .wpx711{width:711px;}
        .wpx707{width:707px;}
        .wpx9{width:9px;}
        .wpx713{width:713px;}
        .wpx347{width:347px;}
        .wpx253{width:253px;}
        .wpx335{width:335px;}                                    
        .hp30{
            height:27px;
            border-top:1px solid #dddddd;
        }
        .hp22{height:22px;}
        .hp20{height:20px;}
        .hp4{height:4px;}
        .hp9{height:9px;}
        .hp8{height:8px;}
        .hp17{height:19px;}
        .borlr{
            border-left:1px solid #dddddd;
            border-right:1px solid #dddddd;
        }                                    
        .borl{
            border-left:1px solid #dddddd;
        }                                    
        .borr{
            border-right:1px solid #dddddd;
        }
        .borb{
            border-bottom:1px solid #dddddd;
        }                                    
        div.row {
            clear:both; 
            float:left;
        }
        div.coloumn {
            float:left;
        }
        div.coloumn-right {
            float:right;
        }                                    
        .seccolor{background-color:#999999;}
        .bg2{background-color:#f4f4f4;}                                    
        div.clear{
            clear:both;
            background-color:#f4f4f4;
        }
        .header-text{font-size:11px; font-family:arial; font-weight:normal;}
		.top-border{
			border:#999999 solid 1px;
		}
		.contentheading{
			font-family: Arial, Verdana,'San serif';
			font-size:22px;		 
			color:#cf1f25;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 10px 0px 10px 0px;
		}
		.contentsmallheading{
			font-family: Arial, Verdana,'San serif';
			font-size:16px;
			color:#cf1f25;			 
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.contentsmallheadingtext{
			font-family: Arial, Verdana,'San serif';
			font-size:14px;
			color:#2c2c2c;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.content-service{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 9px 0px 9px 0px;
		}
		.content-font{
			font-family: Arial, Verdana,'San serif';
			font-size:11px;
			color:#3c3c3c;
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
		}
		.whytop-border
		{
			border:#999999 solid 1px;
			text-align:left;
		}

		.whycontentheading
		{
			font-family: Arial, Verdana,'San serif';
			font-size:22px;
			color:#cf1f25;	
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 5px 0px 10px 0px;
		}

		.whycontentsmallheading
		{
			font-family: Arial, Verdana,'San serif';
			font-size:16px;
			color:#cf1f25;		 
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.whycontentsmallheadingtext
		{
			font-family: Arial, Verdana,'San serif';
			font-size:14px;
			color:#2c2c2c;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		
		.whycontent
		{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:normal;    
			text-decoration:none;
			text-align:left;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 15px 0px 15px 15px;
		}

		.whycontent-font
		{
			font-family: Arial, Verdana,'San serif';
			font-size:11px;
			color:#3c3c3c;
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
			
		}

		.whycontentbottom
		{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:normal;    
			text-decoration:none;
			text-align:left;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 9px 0px 9px 9px;
		}

		.whycontentbottomheading
		{
			font-family: Arial, Verdana,'San serif';
			font-size:15px;
			color:#cf1f25;
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 10px 0px 10px 0px;
		}
		
.service-heading-text{font-family:Arial;font-size:20px;line-height:25px;color:#fff;font-weight:bold}
.service-sub-heading-text{font-family:Arial;font-size:14px;line-height:20px;color:#fff;font-weight:bold}
.service-content-heading{font-family:Arial;font-size:25px;line-height:30px;color:#C4001F;font-weight:bold}
.service-content-sub-heading{font-family:Arial;font-size:28px;line-height:35px;color:#C4001F;font-weight:bold}
.service-content-text{font-family:Arial;font-size:15px;line-height:20px;color:#505050;font-weight:regular}

</style> ";
       
$header_content ="<div id=\"content\">
<div id=\"content-main\"> ";
$body="
<div class=\"row wp100 pt5\" style=\"height:985px\">
	<div class=\"row ac pb10 pt25\">
		<div class=\"row wpx731 bg9 pb3 pt3\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"60px\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl10 pt3\" width=\"393px\" >
					<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
				</td>
				<td class=\"address-text ar pr10\">
				</td>
			</tr>
		</table>
		</div>
		<div class=\"row wp731 pt5 pb5 bg8 \">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl10\" width=\"325px\">
					<span class=\"white-text\"><b>Motivated to Serve&nbsp;&nbsp;Determined to Excel</b><sup>TM</sup></span> 
					 
				</td>
				<td class=\"address-text ar pr10\" width=\"406px\">
					<span class=\"white-text\"><b>An ISO 9001:2008 Certified Company</b></span> 
				</td>
			</tr>
			</table>
		</div> 
	</div>
<div class=\"row wpx731 pb5 pt32\">
<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
    <tr>
        <td width=\"478px\" class=\"pl42 al\" valign=\"top\">";
if (!empty($data['old_billing_address'])){
$body.="<div class=\"heading pb2\">".$data['billing_name']."</div>
        <div class=\"sub-heading\">".nl2br($data['old_billing_address'])."</div>";
}else{
/*
$body.="<div class=\"heading pb2 al\">".$data['billing_name']."</div>
<div class=\"sub-heading al\">".nl2br($data['b_addr'])."</div>      
<div class=\"sub-heading al\">";
    if (!empty($data['b_addr_city'])){
        $body.=$data['b_addr_city'];
    }
    if (!empty($data['b_addr_zip'])){
        $body.="&nbsp;". $data['b_addr_zip'];
    }
    if (!empty($data['b_addr_state'])){
        $body.="&nbsp;". $data['b_addr_state'];
    }
    if (!empty($data['b_addr_country'])){
        $body.="<br/>". $data['b_addr_country'];
    }
$body.="</div> "  ;
*/
}
$body.="</td>
<td width=\"253px\" valign=\top\" class=\"ar\">
<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"ar\">
    <tr>
        <td class=\"b al invoice-text pl5 pb10\">".$data['invoice_title']."</td>
    </tr>
    <tr>
        <td><img src=\"".$variables['images']."/invoice-top-corner-new.jpg\"  width=\"253px\" height=\"9\"  border=\"0\" /></td>
    </tr>
    <tr>
        <td class=\"invoice-border \">             
            ";
if (!empty($data['do_i'])){
$temp=$temp1=$dtArr=null;
$dd  =$mm=$yy='';
$temp = explode(" ",$data['do_i']) ;
$temp1=$temp[0];
$dtArr =explode("-",$temp1) ;
$dd =$dtArr[2];
$mm =$dtArr[1];
$yy=$dtArr[0];
//$do_i = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
$do_i = date("d M Y",$data['do_i']);

$body.="<div class=\"row wpx253\">
<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\"> Date</span></div>
<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_i."</span></div>
</div>";
}
$body.="<div class=\"row wpx253\">
            <div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Proposal No.</span></div>
            <div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['number']."</span></div>
        </div>";
if (!empty($data['do_d'])){
$do_d = date("d M Y",$data['do_d']);
$body.="<div class=\"row wpx253\">
<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Proposal Valid till</span></div>
<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_d."</span></div>
</div>";
}

$body.="</td>
        </tr>
        <tr>
            <td>
			<img src=\"".$variables['images']."/invoice-bottom-corner-new.jpg\"  width=\"253px\" height=\"9\"  
			border=\"0\" /></td>
        </tr>
    </table>
</td>
</tr>
</table>
</div>";
/*
$body.="<div class=\"row wpx731\">
    <div class=\"green-text ac b\">
        Go Green initiative! Every 3000 sheets of paper cost us a tree. Log on to MySMEERP to opt for 'Soft Copy Only'.
    </div>
</div>";
*/
$body.="
	<div class=\"row wp100\" style=\"height:660px\">
		<div class=\"row pb15 pl42 wp100\">
			<div class=\"content b pb2 al pt45\">To,</div><br/>
			<div class=\"contenthead b pb15 al\">".$data['billing_name']."</div>
			<div class=\"contentsub b al pt3\">".nl2br($data['b_addr'])."</div>      
			<div class=\"contentsub b al pt3\">";
				if (!empty($data['b_addr_city'])){
					$body.=$data['b_addr_city'];
				}
				if (!empty($data['b_addr_zip'])){
					$body.="&nbsp;". $data['b_addr_zip'];
				}
				if (!empty($data['b_addr_state'])){
					$body.="&nbsp;". $data['b_addr_state'];
				}
				if (!empty($data['b_addr_country'])){
					$body.="<br/>". $data['b_addr_country'];
				}
			$body.="
			</div>
		</div>";
$body.="<div class=\"row wp100 al pb5 pt45\">
			<span class=\"content pl42 b\">Subject &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proposal for ".$data['quotation_subject']." </span>
		</div>
		<div class=\"row wp100 pb5 al pl42 pt25\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"wp100\">
			<tr>
				<td>
					<div class=\"content pl b\">Dear Sir/Madam, </div>
				</td>
			</tr>
			<tr>
				<td>
				<div class=\"content pl pt20 pr2\">
					With reference to the discussion regarding your requirements, herewith please find our proposal 
					for your consideration.  					
				</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class=\"content pl pt10 pr2\">					 
					Should you however have any further technical / commercial query, kindly feel free to contact. 
					It would be our pleasure to be <br/>of service to you. 
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class=\"content pl pt10 pr2\">					 
					Finally, I must <b>THANK !</b> for the very kind courtesy extended to us to discuss on above mentioned 
					subject.   
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class=\"row content pl pt10 pr2\">				 
					Thanking you in anticipation and looking forward to a healthy business association.
					</div>
					<div class=\"row content pl pt25\">					 
					With very profound regards.
					</div>
					<div class=\"row content pl pt20\">					 
					Yours Sincerely,
					</div>
					<div class=\"row contenthead b pl pt15\">					 
					".$data['mkex_fname']." ".$data['mkex_lname']."
					</div> 
					<div class=\"row contentsub b pl pt3\">".$data['mkex_designation']."&nbsp;</div>";
					if(!empty($data['mkex_mobile1'])){
						$body.="<div class=\"row content pl pt3\">Mobile +91 - ".$data['mkex_mobile1']."</div>";
					}
					if(!empty($data['mkex_email'])){
						$body.="<div class=\"row content pl pt3\">Email - ".$data['mkex_email']."</div>";
						//<div class=\"row invoice-text b pl\" style=\"height:180px\"></div>
					}
				$body.=" 
				</td>
			</tr>	
			</table>
		</div>	
	</div>		
		<div class=\"row wpx731 bg9 pb3 pt3\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
				<tr>
					<td class=\"address-text al pl10\" width=\"393px\">
						<span class=\"white-text b\">".$data['company_name']."</span> 
					</td>
					<td class=\"address-text ar pr10\">
						<span class=\"white-text b\">www.smeerptech.com &nbsp; sales@smeerptech.com</span> 
					</td>
				</tr>
			</table>
		</div>
		<div class=\"row wp731 pt5 pb5 bg8 \">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl10\" width=\"325px\">
					<span class=\"white-text\"><b>Worldwide Network :</b></span> 
					<span class=\"address-text-small\">INDIA, USA, UK, UAE, OMAN, AUSTRALIA <br/>
					MEXICO, CANADA, EUROPE, JAPAN, SINGAPORE, MALAYSIA</span> 
				</td>
				<td class=\"address-text ar pr10\" width=\"406px\">
					<span class=\"white-text\"><b>Corporate office:</b></span> 
					<span class=\"address-text-small\">
					17, MG street | Jhena 440 025 Maharashtra | India <br/></span> 
					<b>Dedicated Sales:</b> <span class=\"address-text-small\">
					98230-99998, 98230-88884, 98230-88885, 98230-99996,98230-88889</span> 
				</td>
			</tr>
			</table>
		</div>
	</div>";
		
$body.="<div class=\"row wp100 pt5\" style=\"height:985px\">
<div class=\"row ac pt32 pb10 \"> 
	<div class=\"row wpx731 bg9 pb3 pt3\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"70px\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl10 pt3\" width=\"393px\" >
					<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
				</td>
				<td class=\"address-text ar pr10\">
					
				</td>
			</tr>
		</table>
	</div>
	<div class=\"row wp731 pt5 pb5 bg8 \">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl10\" width=\"325px\">
				<span class=\"white-text\"><b>Motivated to Serve&nbsp;&nbsp;Determined to Excel</b><sup>TM</sup></span> 
				 
			</td>
			<td class=\"address-text ar pr10\" width=\"406px\">
				<span class=\"white-text\"><b>An ISO 9001:2008 Certified Company</b></span> 
			</td>
		</tr>
		</table>
	</div>
</div>
<div class=\"row pt32\">
<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" width=\"731px\" >
<tr>
    <td width=\"".$data['wd1']."px\" class=\"bg1 vt\">
	<img src=\"".$variables['images']."/first-left-corner.jpg\" width=\"10px\" 
     border=\"0\" /></td>
    <td class=\"bg1 al pl5 \" width=\"".$data['wdinvp']."px\" ><span class=\"content-heading b\"> Particulars</span></td>
    <td class=\"bg1 ar\" width=\"".$data['wdpri']."px\"><span class=\"content-heading b\">Price</span></td>";
    if($data['show_nos'] ==1){
        $body.="<td class=\"bg1 ar\" width=\"".$data['wdnos']."px\"><span class=\"content-heading b\">No/Yr</span></td>";
    }
    $body.="<td class=\"bg1 ar\" width=\"".$data['wdamt']."px\"><span class=\"content-heading b\">Amt.</span></td>";
    if($data['show_discount'] ==1){
     $body.= "<td class=\"bg1 ar\" width=\"".$data['wddisc']."px\"><span class=\"content-heading b\">Disc.</span></td>
     <td class=\"bg1 ar\" width=\"".$data['wdst']."px\"><span class=\"content-heading b\">SubTot</span></td>";
    }
   	$body.= "<td width=\"".$data['wd2']."px\" class=\"bg1 vt\"><img src=\"".$variables['images']."/first-right-corner.jpg\" width=\"9px\" border=\"0\" /></td>
</tr>";    

$k=1;           
foreach($data['particulars'] as $key=>$particular){

    if (!empty($particular['ss_punch_line'])){
        $body.= "<tr>
            <td class=\"bg3 borl\" height=\"1px\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" 
			height=\"1px\"  border=\"0\" /></td>
            <td class=\"bg3 vt al content pt8 b pb5 pl5\" height=\"10px\" valign=\"top\">";
            $body.=$k.". ".nl2br($particular['ss_punch_line'])."</td>";
            $body.="<td class=\"bg3 al content pt8 b pb5 pl5\" colspan=\"".$data['colsSpanNo1']."\" height=\"10px\" >&nbsp;</td>";
            $body.="<td class=\"bg3 borr\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" 
			height=\"1px\" border=\"0\"/></td>
         </tr>";
    }
    $body.="<tr>
        <td class=\"pb5 bg3 borl\" height=\"1px\" ><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" 
		height=\"1px\" border=\"0\"/></td>
        <td class=\"bg3 vt pb5 content al pl10 pl5\" height=\"10px\">".nl2br($particular['particulars'])."</td>
        <td class=\"bg3 vt pb5 content ar\" height=\"10px\" >".$particular['p_amount']."</td>";
        if($data['show_nos'] ==1){ 
         $body.="<td class=\"bg3 vt pb5 content ar\" height=\"10px\">
		 ".$particular['s_quantity']." ".$particular['s_type']."</td>" ;
        }
        $body.="<td class=\"bg3 vt pb5 content ar\" height=\"10px\">".$particular['s_amount']."</td>";
        if($data['show_discount'] ==1){
			$body.="<td class=\"bg3 vt pb5 content ar\" height=\"10px\">".$particular['d_amount']."</td>";
			$body.="<td class=\"bg3 vt pb5 content ar\" height=\"10px\">".$particular['stot_amount']."</td>";
        }  		
        $body.="<td class=\"pb5 bg3 borr\" height=\"1px\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" 
		 height=\"1px\" border=\"0\" /></td>
    </tr>";
    $k++;	
}
 $body.="<tr>
            <td width=\"10px\" height=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-left-corner.jpg\"  width=\"10px\" height=\"9px\"  border=\"0\" /></td>";
            $body.="<td class=\"bg3 borb\" colspan=\"".$data['colsSpanNo2']."\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>";
            $body.="<td width=\"9px\" height=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-right-corner.jpg\" width=\"9px\" height=\"9px\"  border=\"0\" /></td>
        </tr>
</table>
</div>";
if($data['dont_show_total']==0){
$body.="<div class=\"row wp100 pt10\">
			<table align=\"right\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">	";
			if(!empty($data['tax1_name'])){			
				$body.="<tr>				
				 <td align=\"right\"><span class=\"content ar b\">Total :</span></td>
			     <td align=\"right\" width=\"100px\">
				 <span class=\"content pr10 b\">&nbsp;".$data['sub_total_amount']."</span></td>
				</tr>
				<tr>		
					<td align=\"right\"><span class=\"content ar\">".$data['tax1_name']." @ ".$data['tax1_value']." :</span></td>
					<td align=\"right\" width=\"100px\">
					<span class=\"content pr10 \">&nbsp;".$data['tax1_total_amount']."</span></td>
				</tr>";
				if(!empty($data['tax1_sub1_name'])){
					$body.="<tr>				
					<td align=\"right\"><span class=\"content ar\">".$data['tax1_sub1_name']." @ ".$data['tax1_sub1_value']." :</span></td>
					<td align=\"right\" width=\"100px\">
					<span class=\"content pr10\">&nbsp;".$data['tax1_sub1_total_amount']."</span></td>
					</tr>" ;
				}
				if (!empty($data['tax1_sub2_name'])){
					$body.="<tr>				
						<td align=\"right\"><span class=\"content ar \">".$data['tax1_sub2_name']." @ ".$data['tax1_sub2_value']." :</span></td>
						<td align=\"right\" width=\"100px\">
						<span class=\"content pr10\">&nbsp;".$data['tax1_sub2_total_amount']."</span></td>
					</tr>" ;
				}
			}
			if (!empty($data['round_off_op']) && !empty($data['round_off']) ){
					$body.="<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar \">Round off :</span></td>
						<td align=\"right\" width=\"100px\"><span class=\"content pr10\">"
						.$data['round_off_op']."&nbsp;".$data['round_off']."</span></td>
						</tr>" ;
			}
			$body.="<tr>				
					<td align=\"right\"><span class=\"content ar b\">Grand Total :</span></td>";
					if ($data['do_i_chk'] <= $data['do_rs_symbol']){
						$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\">".$data['currency_symbol']." ".$data['amount']."</span></td>";
					}else{
						$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\"><img src=\"".$variables['images']."/".$data['currency_symbol'].".jpg\" border=\"0\" /> ".$data['amount']."</span></td>";		 
					}
			$body.="</tr>";
		   $body.="<tr>
			        <td align=\"right\" colspan=\"2\">
						<span class=\"content pr10 b\">" ;
						if(!empty($data['currency_name'])) {
							$body.=$data['currency_name'] ;
						} 
            $body.=   " ".$data['amount_words']." only 
						</span>
					</td>
			    </tr>";
			$body.="
				
			</table>
		</div>";
}
if(!empty($data['other_details']) ){

$body.="<div class=\"row pt32\">
<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" width=\"731px\">
<tr>
    <td width=\"10px\" class=\"bg1 vt\">
		<img src=\"".$variables['images']."/first-left-corner.jpg\" width=\"10px\" border=\"0\" />
	</td>
    <td width=\"712px\" class=\"bg1 al pl5\"><span class=\"content-heading b\">Payment, Schedule & other terms</span></td>
    <td width=\"9px\" class=\"bg1 vt\"><img src=\"".$variables['images']."/first-right-corner.jpg\" width=\"9px\" 
	border=\"0\" /></td>
</tr>   
<tr>
	<td class=\"bg3 borl\" height=\"1px\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" 
	height=\"1px\"  border=\"0\" /></td>
	<td width=\"712px\" class=\"bg3 vt al content-small-10 pt8 pb5 pl5\" height=\"10px\" valign=\"top\">";
	$body.= nl2br($data['other_details'])."</td>";
	$body.="<td class=\"bg3 borr\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" 
	height=\"1px\" border=\"0\"/></td>
 </tr> 
 <tr>
     <td width=\"10px\" height=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-left-corner.jpg\"  width=\"10px\" height=\"9px\"  border=\"0\" /></td>";
    $body.="<td width=\"712px\" class=\"bg3 borb\"\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>";
    $body.="<td width=\"9px\" height=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-right-corner.jpg\" width=\"9px\" height=\"9px\"  border=\"0\"/></td>
        </tr>
</table>
</div>";

}

$body.="</div> 
<div class=\"row wpx731 bg9 pb3 pt3\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl10\" width=\"393px\">
				<span class=\"white-text b\">".$data['company_name']."
			</td>
			<td class=\"address-text ar pr10\">
				<span class=\"white-text b\">www.smeerptech.com &nbsp; sales@smeerptech.com</span> 
			</td>
		</tr>
	</table>
</div>
<div class=\"row wp731 pt5 pb5 bg8 \">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
	<tr>
		<td class=\"address-text al pl10\" width=\"325px\">
			<span class=\"white-text\"><b>Worldwide Network :</b></span> 
			<span class=\"address-text-small\">INDIA, USA, UK, UAE, OMAN, AUSTRALIA <br/>
			MEXICO, CANADA, EUROPE, JAPAN, SINGAPORE, MALAYSIA</span> 
		</td>
		<td class=\"address-text ar pr10\" width=\"406px\">
			<span class=\"white-text\"><b>Corporate office:</b></span> 
			<span class=\"address-text-small\">
			17, MG street | Jhena 440 025 Maharashtra | India <br/></span> 
			<b>Dedicated Sales:</b> <span class=\"address-text-small\">
			98230-99998, 98230-88884, 98230-88885, 98230-99996,98230-88889</span> 
		</td>
	</tr>
	</table>
</div>"; 
/*
if( !empty($data['delivery_at'])){
	$body.="<div class=\"row wp100 vt\">		
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"left\" width=\"100%\">				
					<tr>
						<td align=\"left\">
							<span class=\"content al b\">Delivery At :</span>
							<span class=\"content al\">".$data['delivery_at']."</span>
						</td>
					</tr>		
				</table>
			</div> ";
}
$body.="<div class=\"row wp100 vt\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
    <tr>
		<td align=\"left\" >" ;
			$body.="<table align=\"left\" border=\"0\">
				<tr>					 
					<td class=\"content-small-10 b\">PAN No :&nbsp;</td>
					<td class=\"content-small-10\">".$data['pan_no']."</td>";
				if ($data['do_i_chk'] >= $data['do_tax'] && $data['do_tax']!=0  && !empty($data['tax1_name']) ){	
					if(!empty($data['vat_no'])){
						$body.="<td class=\"content-small-10 b\">&nbsp;&nbsp;&nbsp;VAT No :&nbsp;</td>
						<td class=\"content-small-10\">".$data['vat_no']."</td>";
					}
					if(!empty($data['cst_no'])){
						$body.="<td class=\"content-small-10 b\">&nbsp;&nbsp;&nbsp;CST No :&nbsp;</td>
						<td class=\"content-small-10\">".$data['cst_no']."</td>" ;
					}
				}
				$body.="</tr>
				</table>";
	
$body.="</td>";	
$body.= "</tr>     
</table>
</div> ";
if (!empty($data['tax1_declaration'])){
$body.="<div class=\"row wp100 vt\">		
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"left\" width=\"100%\">	
				<tr>
			        <td align=\"left\">
						<span class=\"content-small-7\">".$data['tax1_declaration']."</span>							
					</td>
			    </tr>	
			</table>
		</div> ";
}
*/

/*
header image bof
$body.="<div class=\"row wp100 pt32\">
<div class=\"row al pb10\">
    <div><img src=\"".$variables['images']."/quotation-header.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\"></div>
</div>
</div>
header image eof
";
<div class=\"row pt32 wp100\">
<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
    <tr>
        <td width=\"10px\"><img src=\"".$variables['images']."/sec-gray-corner.jpg\"  width=\"10px\" height=\"22\" 
		border=\"0\" /></td>
        <td class=\"bg1 wpx711 al content-heading\"><span class=\"pl2\">Terms and Conditions</span></td>
        <td width=\"10px\"><img src=\"".$variables['images']."/sec-right-corner.jpg\"  width=\"10px\" 
		height=\"22\"  border=\"0\" /></td>
     </tr> 
</table>  
</div>
*/
$terms_condition = "<div class=\"row wp100 pt30\"> 
<div class=\"row al pb10\">
    <div class=\"row wpx731 bg9 pb3 pt3\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"70px\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl10 pt3\" width=\"393px\">
					<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
				</td>
				<td class=\"address-text ar pr10\">
					
				</td>
			</tr>
		</table>
	</div>
	<div class=\"row wp731 pt5 pb5 bg8\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl10\" width=\"325px\">
				<span class=\"white-text\"><b>Motivated to Serve&nbsp;&nbsp;Determined to Excel</b><sup>TM</sup></span> 
				 
			</td>
			<td class=\"address-text ar pr10\" width=\"406px\">
				<span class=\"white-text\"><b>An ISO 9001:2008 Certified Company</b></span> 
			</td>
		</tr>
		</table>
	</div>
</div>
</div>";
$terms_condition.="<div class=\"row wp100 al pt1 pb1\" style=\"height:836px\">
<table cellspacing=\"0\" cellpadding=\"0px\" border=\"0\" width=\"100%\">
    <tr>
		<td width=\"10px\"><img src=\"".$variables['images']."/sec-gray-corner.jpg\" width=\"10px\" height=\"15px\" 
		border=\"0\" /></td>
		<td class=\"bg1 wpx711 al content-heading\"><span class=\"pl2\">Terms and Conditions</span></td>
		<td width=\"10px\"><img src=\"".$variables['images']."/sec-right-corner.jpg\" width=\"10px\" 
		height=\"15px\" border=\"0\" /></td>
	</tr>	 
	<tr>
        <td class=\"al pr10 pl10 pt3\" colspan=\"3\" valign=\"top\" width=\"100%\" >
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" style=\"padding-left:0px;padding-right:0px\">
			<tr>
				<td width=\"50%\" valign=\"top\">
					<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:5px\">
					<tr>
						<td class=\"pt2 content-small-term pl pr5\">
						<div class=\"row\">
							<div class=\"coloumn\">By making payment against this invoice/proposal, it is agreed between SMEERP E-Technologies Private Limited ('PETPL') and the Customer as follows -</div>
						</div>
					   </td>
					</tr> 	
					<tr>
						<td class=\"content-small b pl pt2 pr5\"> A. Definitions</td>
					</tr>
					<tr>
						<td class=\"pt2 content-small-term pl10 pr5\">
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Extra Efforts' or 'Additional Services' means any efforts or services which are not mentioned in the proposal.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Charges' means all fees, charges/tariffs, costs and rates chargeable by PETPL from time to time for providing the Customer with the Service, for Software Products, for Goods and Additional Services and other Government levies.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Customer or Client' shall mean any person, partnership firm or private limited company or limited company or such other organisation authorized by PETPL to use the services or to whom PETPL sell goods or services.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Government' shall mean the Government of India and/or the State Government of Maharashtra or such other local authority, as the case may be.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Services' shall mean all/any services made available by PETPL or via its associates or channel partners.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Software Products' of PETPL are like myCraft<sup>TM</sup>, myGallery, Nerve Center<sup>TM</sup> etc. and are products/solutions developed by PETPL and licenses for which are sold by PETPL or via its associates or channel partners.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Goods' shall mean all the Goods/Material/Equipment sold by PETPL or via its associates or channel partners.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'MySMEERP' is the online Customer Support Portal for support, billing, communication etc.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Renewal/Expiry Date' means the renewal/expiry date of Software Product/Equipment/Goods/Service as mentioned in Invoice.
							</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">'Support Email' means support@smeerptech.com
							</div>
						</div>												
					    </td>
					</tr>	
					<tr>
						<td class=\"content-small b pl pt2 pr5\"> B. Provision of Services</td>
					</tr>
					<tr>
						<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">PETPL agrees to provide the Services or Goods to the Customer subject to the terms and conditions of this agreement.
							</div>
						</td>
					</tr>
					<tr>
						<td class=\"content-small b pl pt2 pr5\"> C. Obligations of PETPL</td>
					</tr>
					<tr>
						<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;1.</div>
								<div class=\"coloumn \">PETPL shall use reasonable efforts to make the Services available to the Customer at all times.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;2.</div>
								<div class=\"coloumn \">The availability and quality of the Services may be affected by the factors outside PETPL's control such as physical obstructions, geographic, weather conditions and other causes of interference or faults.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;3.</div>
								<div class=\"coloumn \">Services may be suspended in whole or in part at any time, without notice, if Network fails or requires maintenance. PETPL will make all reasonable efforts to minimise the frequency and duration of such events. The Customer will remain liable for all charges during the period of suspension unless PETPL in its discretion decides otherwise.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;4.</div>
								<div class=\"coloumn \"> PETPL has the sole right and discretion to vary or increase the Charges at any time on reasonable notice to the Customer.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;5.</div>
								<div class=\"coloumn \">PETPL reserves the right to apply a monthly/quarterly/half-yearly/yearly financial limit and such other conditions for Charges incurred by the Customer and to demand interim advance payment, suspend or disconnect access to the Services if such limits are exceeded.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;6.</div>
								<div class=\"coloumn \">PETPL has the right to check the credentials of the Customer including the Customer's financial standing and to use the services of any person or agency for such purpose.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class=\"content-small b pl pt2 pr5\"> D. Obligations of the Customer</div></td>
					</tr>
					<tr>
						<td class=\"pt2 content-small-term pl10 pr5\">
						<div class=\"row\">
							<div class=\"coloumn \"> The Customer hereby expressly agrees:</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;1.</div>
							<div class=\"coloumn \"> To make payments for the Services on the following basis</div>
						</div>
						<div class=\"row pl15\">(a)	payment will be due when PETPL raises the billing statement on the Customer.</div>
						<div class=\"row pl15\">(b)	payment will be made on or before the due date mentioned in the invoice or as per schedule mentioned in the proposal, failing which PETPL shall be entitled to charge interest @18% per annum or INR 500 whichever is higher and/or late fees on all outstanding Charges from due date till date of payment &amp; shall be entitled to discontinue Services or delivery of Goods/Software Products, without notice, in its sole discretion. Cheque return will attract a penalty of Rs. 1000 separately.</div>
						<div class=\"row pl15\">(c)	in case of delinquent payment or non-payment, neither the domain name ownership cannot be claimed by Customer nor can be transferred to any other service provider.</div>
						<div class=\"row pl15\">(d)	the Customer will pay all the costs of collection and legal expenses, with interest should it become necessary to refer the matter to a collection agency or to legal recourse to enforce payment.</div>
						<div class=\"row pl15\">(e)	PETPL shall be entitled to apply payments/deposits/advances made by the Customer towards any Charges outstanding including for any other PETPL service or goods availed by the customer.</div>
						<div class=\"row pl15\">(f)	payments will be made in cash, credit card or A/c Payee cheque or demand draft or internet banking or any other instrument drawn on any bank in Jhena and payable at Jhena.</div>
						<div class=\"row pl15\">(g)	the Customer shall be liable for all Charges for the Services provided to the Customer whether or not the Services have been used by the Customer.</div>
						<div class=\"row pl15\">(h)	in the event of any dispute regarding the Charges, the Customer agrees to pay PETPL Charges billed pending resolution of such dispute.</div>
						<div class=\"row pl15\">(i)	the Customer shall be liable to pay for the Services provided even if he/she does not receive the bills. It will be the Customer's responsibility to make enquiries in case of non-receipt of bills.</div>
						<div class=\"row pl15\">(j)	Charges payable by the Customer are exclusive of taxes, duties or levies payable, unless expressly stated to the contrary in the billing statement.</div>
						<div class=\"row pl15\">(k)	any advance/security deposit paid by the Customer shall be adjusted against any dues payable by the Customer to PETPL and balance if any will be refunded by PETPL within 60 days from the deactivation of the Services.</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;2.</div>
							<div class=\"coloumn \">To make advance payment for Charges including service charges if billed to customer by PETPL.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;3.</div>
							<div class=\"coloumn \">To not use or cause or allow others to use the Services for any improper, immoral or unlawful purpose including in any manner (for e.g. Spamming, Bulk Emailing, Transmission of infected content etc.) which may jeopardise or impair the operation of the Network and/or the Services.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;4.</div>
							<div class=\"coloumn \">To comply with instructions issued by Government or PETPL, concerning Customer's access to and use of Services.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;5.</div>
							<div class=\"coloumn \">To furnish correct and complete information and documents as required by PETPL from time to time. The Services agreed to be provided by PETPL shall always be subject to verification of the Customer's credentials and documents and if at any time, any information and/or documents furnished by the Customer is/are found incorrect or incomplete or suspicious, PETPL shall be entitled to suspend/terminate the Service forthwith without any further notice.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;6.</div>
							<div class=\"coloumn \">That PETPL may suspend the Service in whole or in part at any time without prior notice and without assigning any reason thereto. PETPL reserves the right to Charge for re-activation.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;7.</div>
							<div class=\"coloumn \">Customer is liable (a) for the Charges during the period of suspension and thereafter (b) to pay the overages as applicable and (c) Charges towards Extra Efforts</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;8.</div>
							<div class=\"coloumn \">To comply with the applicable laws, rules and regulation regarding the use of the Services and procurement of the Equipment including but not limited to relevant tax laws and import control regulations.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;9.</div>
							<div class=\"coloumn \">To pay to PETPL such amount as PETPL may require as security for the due performance of the Customer's obligations under these terms and conditions. PETPL may set off these amounts against any cost, damages or expenses which PETPL may suffer or incur as a result of the Customer's failure to perform any of these obligations. Security deposit amount shall not carry any interest.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">10.</div>
							<div class=\"coloumn \">To inform PETPL, in writing, of any changes in billing name or billing address or email id or cellular number. Any written communication, billing statement or notice from PETPL to the Customer will be deemed as served within 48 hours of posting by ordinary mail or email.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">11.</div>
							<div class=\"coloumn \">To notify PETPL immediately in case of any complaints with regards to the Services via email to Support Email</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">12.</div>
							<div class=\"coloumn \">Not to assign any right or interest under this agreement without PETPL's prior written consent.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">13.</div>
							<div class=\"coloumn \">To be bound at all times by any modifications and or variations made to these terms and conditions.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">14.</div>
							<div class=\"coloumn \">Customer is not entitled to assign/transfer/resell/lease/rent or create any charge/lien on the Service of any nature whatsoever or Software Product owned by PETPL, without prior permission of PETPL. Any transfer affected in contravention of the express terms contained herein, shall not absolve the Customer of his/her primary duty towards PETPL for usages charges levied against the Customer.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">15.</div>
							<div class=\"coloumn \">To timely provide PETPL with the certificates etc. towards any deductions as per the law.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">17.</div>
							<div class=\"coloumn \">Security and Backup of the data to which Customer is provided access with is the liability of Customer.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">18.</div>
							<div class=\"coloumn \">Customer must verify the credentials of the visiting PETPL's personnel.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">19.</div>
							<div class=\"coloumn \">Neither PETPL's personnel will install any illegal/pirated software/text/images for Customer nor any Illegal/pirated software/text/images are covered under maintenance service support.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">21.</div>
							<div class=\"coloumn \">Work will commence once PETPL receives (a) Work/Purchase order referring proposal, scope of work and payment schedule (b) Duly signed and stamped copy of proposal (c) Duly filled, signed and stamped Customer Agreement form.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">22.</div>
							<div class=\"coloumn \">Customer must designate only one of his/her personnel to coordinate and to approve the work/delivery. Customer is not allowed to change designated personnel till completion of work. In case of factors outside control, change of personnel is allowed if agreeable to both the parties.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">23.</div>
							<div class=\"coloumn \">Domain and Web hosting account with data/email is deleted after seven days of Expiry Date. Late renewal during these seven days attracts late payment Charges too.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">24.</div>
							<div class=\"coloumn \">PETPL will remind Customer about the renewal dates via SMS/Email automated notification. Customer is responsible to remember renewal dates and make renewal payments before 60 days of Expiry Date.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">25.</div>
							<div class=\"coloumn \">Customer must provide information/references for his/her choice for design, images in JPEG format, content in editable MS Word format and Logo in editable CDR or PSD format via email and before commencement of work. Our designers will use all the images provided as-is. Besides basic cropping, PETPL is unable to perform advanced manipulation. Logo design, Content writing or typing or proof-reading or extra design samples is separate job work needing Extra Efforts and is separately chargeable. Static page means an A4 size page containing text in Arial font size 12.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">26.</div>
							<div class=\"coloumn \">Customer is liable to pay any statutory duties or levies or taxes as applicable by the law. </div>
						</div>						
						</td>
					</tr>					
					</table>
				</td>
				<td width=\"50%\" valign=\"top\">
					<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:2px;padding-right:2px\">
						<tr>
						<td class=\"content-small-term pl10 pr5\">
						<div class=\"row\">
							<div class=\"coloumn pr5\">27.</div>
							<div class=\"coloumn \">Once the order is confirmed Customer must sign up with MySMEERP.</div>
						</div>						
						<div class=\"row\">
							<div class=\"coloumn pr5\">28.</div>
							<div class=\"coloumn \">PETPL will not be responsible for any delay in schedule caused due to delay in providing the content to PETPL or non-availability of any of Customer official or any other delay on Customer part. Customer will not hold back any of PETPL's payment for delays on part of Customer. Content received after the completion of work OR in excess as per proposal will be termed as extra and separately chargeable.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">29.</div>
							<div class=\"coloumn \">Transfer of domain name from his/her existing service provider to PETPL is the sole responsibility of Customer.</div>
						</div>
						<div class=\"row\">
							<div class=\"coloumn pr5\">30.</div>
							<div class=\"coloumn \">Customer must use MySMEERP or email to Support Email for service and support.</div>
						</div>						
						</td>
						</tr>
						<tr>
							<td class=\"content-small b pl5 pt2 pr5\"> E. Software Product License to use:</td>
						</tr>
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;1.</div>
								<div class=\"coloumn \">If billed for, Customer gets the license to use for the PETPL's Software Products. PETPL's Software Products are provided on license to use basis only on PETPL's web servers without ftp & hosting control panel access. The license to use of Software Products is non-transferable to any other Domain name or other person/party and is subject to yearly renewal against annual maintenance support charges as specified in the proposal.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;2.</div>
								<div class=\"coloumn \">Under all/any circumstances, programming code of Software Products is the sole property of PETPL and under no circumstances Customer can claim any rights towards the ownership of product or code. License of the product is only for using it i.e. License to use.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;3.</div>
								<div class=\"coloumn \">Credits to Software Product and/or PETPL are compulsorily mentioned on the Customer website. Credit to PETPL is compulsorily mentioned on the designed and/or printed material of the Customer.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;4.</div>
								<div class=\"coloumn \">Any customization done in the Software Product doesn't make the Customer the owner of product. Under all/any circumstances the Software Product remains the property of PETPL with full rights over the entire product and customizations done.</div>
							</div>
							</td>
						</tr>
						<tr>
							<td class=\"content-small b pl5 pt2 pr5\"> F. Validity</td>
						</tr>
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;1.</div>
								<div class=\"coloumn \">Both parties agree that, this agreement has been duly authorised and executed and is valid and binding and is enforceable in law in accordance with its terms.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;2.</div>
								<div class=\"coloumn \">The validity construction and performance of this agreement shall be governed by and interpreted in accordance with the laws of the Republic of India.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;3.</div>
								<div class=\"coloumn \">The courts in Jhena shall have the jurisdiction.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;4.</div>
								<div class=\"coloumn \">Should any provision of this agreement be or become ineffective or be held to be invalid, this shall not affect the validity of the remaining provisions. Any invalid provision in this agreement shall be replaced, interpreted or supplemented as the case may be in such a manner that the intended economic purpose of the agreement will be achieved.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;5.</div>
								<div class=\"coloumn \">This agreement is complete and exclusive statement of agreement between parties and it supersedes all understanding or prior agreement, whether oral or written and all representations or other communications between parties.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;6.</div>
								<div class=\"coloumn \">These terms & conditions are subject to applicable Indian laws, rules and regulation framed thereunder and any statutory modifications or re-enactment for time being in force and any other Government regulations issued from time to time.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;7.</div>
								<div class=\"coloumn \">Prices/Availability/Specifications are subject to change without prior notice.</div>
							</div>
							</td>
						</tr>	
						<tr>
							<td class=\"content-small b pl5 pt2 pr5\">G. Support, Goods Return, Warranties, Disclaimer of warranties</td>
						</tr>
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;1.</div>
							<div class=\"coloumn \">PETPL sends Customer the information related to terms/conditions/services/offers/warnings/support/advisories etc. via email/sms/telephone. It is Customer's responsibility to do the needful accordingly.</div>
							</div>
							</td>
						</tr>	
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">
							<div class=\"coloumn pr5\">&nbsp;2.</div>
							<div class=\"coloumn \">Annual Maintenance Support Charge for renewal of License to use of Software Product is compulsory as specified in the proposal and includes services (a) Maintaining Software Product in working condition (b) FREE feasible patches and updates (c) Customer support via email, telephone and MySMEERP</div>
							</div>
							</td>
						</tr>						
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
								<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;3.</div>
								<div class=\"coloumn \">Goods, Software Products, Services once sold will not be taken back or exchanged and payment will not be refunded under any circumstances. Our risk & responsibility ceases on delivery of the goods. After sales support services is valid only if applicable to purchased item and explicitly mentioned in the invoice.</div>
								</div>
							</td>
						</tr>	
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
								<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;4.</div>
								<div class=\"coloumn \">PETPL makes no representation or warranty other than those set forth in this agreement. PETPL expressly disclaims all other warranties express or implied, including, but not limited to any implied warranty or merchantability or fitness for a particular purpose.</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
								<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;5.</div>
								<div class=\"coloumn \">Any goods or equipment re-sold by PETPL will carry the warranty, if any, of the manufacturer. Service to such goods or equipment, if not explicitly mentioned in invoice, if any, will be provided by authorized service centre of the manufacturer. PETPL is not liable towards service support or replacement under warranty, if any, of any goods or equipment manufactured by third party and that PETPL is just reselling.</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class=\"content-small b pl5 pt2 pr5\"> H. Disclaimer of liability</td>
						</tr>
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;1.</div>
								<div class=\"coloumn \">PETPL shall not be liable to the Customer for any loss or damage whatsoever or howsoever caused arising directly or indirectly in connection with this agreement, the Services or Products or Equipment or Goods, their use, application or otherwise except to the extent to which it is unlawful to exclude such liability.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;2.</div>
								<div class=\"coloumn \">Malfunction/Limitation in working of any of PETPL's Software Product due to any change in third party Software/Hardware on which PETPL's Software Product is dependent for functioning is not covered under maintenance support and PETPL cannot be held responsible for it. Though PETPL shall use reasonable efforts for resolution of issue if feasible. However Customer will be liable towards Charges for these Extra Efforts.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;3.</div>
								<div class=\"coloumn \">Under any circumstances, PETPL will not be responsible for any kind of loss / damage of data / equipment etc. while / due to working PETPL's service personnel at Customer's site. All the maintenance and service related work executed is at the risk of Customer. Customer has rights to ask PETPL's service personnel to work under Customer's authorized personnel's vigilance, seek information and take decision regarding maintenance / service work.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;4.</div>
								<div class=\"coloumn \">Notwithstanding the generality of (a) above, PETPL expressly excludes liability for consequential loss, damage or for loss of profit, business revenue, goodwill or anticipated savings.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;5.</div>
								<div class=\"coloumn \">PETPL at its discretion, may send to the Customer various information on his/her cellular number through SMS or on his/her email id through Email or on his/her billing address through mail or otherwise, as an Additional Service. In case the Customer does not wish to receive such information he/she may notify PETPL via email at Support Email</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;6.</div>
								<div class=\"coloumn \">In the event that any exclusion contained in this agreement shall be held to be invalid for any reason, and PETPL becomes liable for the loss or damage that it may otherwise not have been liable, such liability shall be limited to the cost of the Services actually paid for by the Customer to PETPL during the relevant period.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;7.</div>
								<div class=\"coloumn \">Customer agrees to indemnify and keep PETPL harmless and defend PETPL at its own expense from and against all claims arising as a result of breach of this agreement and from all taxes, duties or levies.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;8.</div>
								<div class=\"coloumn \">PETPL advises Customer to make cash payments only after verifying the credentials (ID card etc.) of the visiting personnel of PETPL. PETPL encourages cheque, demand draft or internet banking transfer to avoid any untoward incident. PETPL will not be liable for any such loss incurring due to handing over of cash payment to unauthorized personnel.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;9.</div>
								<div class=\"coloumn \">Customer agrees that any request/communication received from Customer's and his/her authorized personnel email ids or mobiles numbers on PETPL's Support Email via email or SMS shall be deemed to be valid request/communication from the Customer. Nothing herein shall apply with respect to the notice to be given by the Customer section J or any other provisions of this agreement.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">10.</div>
								<div class=\"coloumn \">Customer must regularly update and verify credentials of his / her authorized personnel registered with MySMEERP and in case of any issue must inform PETPL immediately and confirm the same in writing to avoid loss/damage. Liability of any loss due to misuse/mistake/mishandling/loss of confidential information by/due to Customer or his/her authorized personnel remains with Customer and PETPL will not be held responsible under any circumstances.</div>
							</div>							
							</td>
						</tr>
						<tr>
							<td class=\"content-small b pl5 pt2 pr5\">I. Termination</td>
						</tr>
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;1.</div>
								<div class=\"coloumn \">	Either party shall have the right to terminate the agreement by giving 60 days prior notice in writing.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;2.</div>
								<div class=\"coloumn \">Notwithstanding anything contained herein, PETPL shall be entitled to terminate this agreement and the Services if (a)	the Government either suspends or terminates the License or Permission or the Services temporarily or otherwise (b)	at any time the Customer fails to satisfy the requisite credit checks or provides fraudulent information to PETPL. (c)	the Customer fails to pay its subscription or the Charges due (d)	the Customer is in breach of any other terms of the agreement and the Customer does not remedy the breach within seven (7) days of the day of receipt of a written / email notice from PETPL specifying the breach.</div>
								</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;3.</div>
								<div class=\"coloumn \">The agreement may also be terminated at the option of either party, on the happening of the following events (a) if either party is declared insolvent, bankrupt or is liquidated or dissolved (b)	if a trustee or receiver is appointed to take over the assets of either party (c)	if the Government requires any of this agreement to be revised in such a way as to cause significant adverse consequences to either party</div>
								</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;4.</div>
								<div class=\"coloumn \">Termination of this agreement under the preceding provisions shall be without prejudice to and in addition to any right or remedy available to the terminating party under any applicable law or statute.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;5.</div>
								<div class=\"coloumn \">In the event of termination of the agreement for any reason whatsoever, PETPL shall be entitled to recover all outstanding Charges and dues from the Customer.</div>
							</div>
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;6.</div>
								<div class=\"coloumn \">If the agreement is terminated for reasons of fraudulent information provided or misuse or unlawful use by the Customer, the security deposit shall be forfeited.</div>
							</div>				
							</td>
						</tr>
						<tr>
							<td class=\"content-small b pl5 pt2 pr5\"> J. Miscellaneous</td>
						</tr>
						<tr>
							<td class=\"pt2 content-small-term pl10 pr5\">
							<div class=\"row\">
								<div class=\"coloumn pr5\">&nbsp;1.</div>
								<div class=\"coloumn \">All notices required to be given to PETPL pursuant to this agreement shall be in writing and shall be directed by registered post to the Registered office at SMEERP E-Technologies Private Limited, Plot no. 17, MG street, Jhena 440 025. Tel. 0712-2284514  www.smeerptech.com support@smeerptech.com</div>
							</div>				
							</td>
						</tr>
					</table>
				</td>
			</tr>			
			";		 
		$terms_condition.="</table>
		</td>
	</tr>" ;
$terms_condition.="";
$terms_condition.="</table>   
</div>
<div class=\"row wpx731 bg9 pb3 pt1\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl10\" width=\"393px\">
				<span class=\"white-text b\">".$data['company_name']."
			</td>
			<td class=\"address-text ar pr10\">
				<span class=\"white-text b\">www.smeerptech.com &nbsp; sales@smeerptech.com</span> 
			</td>
		</tr>
	</table>
</div>
<div class=\"row wp731 pt5 pb3 bg8\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
	<tr>
		<td class=\"address-text al pl10\" width=\"325px\">
			<span class=\"white-text\"><b>Worldwide Network :</b></span> 
			<span class=\"address-text-small\">INDIA, USA, UK, UAE, OMAN, AUSTRALIA <br/>
			MEXICO, CANADA, EUROPE, JAPAN, SINGAPORE, MALAYSIA</span> 
		</td>
		<td class=\"address-text ar pr10\" width=\"406px\">
			<span class=\"white-text\"><b>Corporate office:</b></span> 
			<span class=\"address-text-small\">
			17, MG street | Jhena 440 025 Maharashtra | India <br/></span> 
			<b>Dedicated Sales:</b> <span class=\"address-text-small\">
			98230-99998, 98230-88884, 98230-88885, 98230-99996,98230-88889</span> 
		</td>
	</tr>
	</table>
</div>";

//BOF Proposal gallery

//Other Services BOF
$body_services_header ="
<div class=\"row wp100 pt20\"> 
	<div class=\"row al pb10\">
		<div class=\"row wpx731 bg9 pb3 pt3\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"70px\" width=\"731px\">
				<tr>
					<td class=\"address-text pt3 al pl10\" width=\"393px\">
						<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
					</td>
					<td class=\"address-text ar pr10\">
						
					</td>
				</tr>
			</table>
		</div>
		<div class=\"row wp731 pt5 pb5 bg8\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl10\" width=\"325px\">
					<span class=\"white-text\"><b>Motivated to Serve&nbsp;&nbsp;Determined to Excel</b><sup>TM</sup></span> 
					 
				</td>
				<td class=\"address-text ar pr10\" width=\"406px\">
					<span class=\"white-text\"><b>An ISO 9001:2008 Certified Company</b></span> 
				</td>
			</tr>
			</table>
		</div>
	</div>
</div>";

/*
$body_other_service.="<div class=\"row wp100 al pt1 pb1\" style=\"height:843px\">
<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
	<tr><td colspan=\"5\" class=\"contentheading\">List of Services at SMEERP</td></tr>
	<tr><td colspan=\"5\" class=\"contentsmallheading\">( SMEERP 360 - Multiple Services Under-One-Roof )</td></tr>
	<tr><td colspan=\"5\" class=\"contentsmallheadingtext\">\"I insist you to try our services to experience the difference.<br /> And I guarantee your benefit.\" - Aryan Salve, Director</td></tr>								
	<tr height=\"80px\">
		<td class=\"content-service top-border\">Ready-to-use & Custom <br />Softwares<br /><span class=\"content-font\">(Online / Offline / SAAS)<br />(Inventory, Billing, Sales, CRM, ERP etc.)</span></td>								
		<td width=\"10px\">&nbsp;</td>
		<td width=\"33%\" class=\"content-service top-border\">Domain Name<br />Registration <br /><span class=\"content-font\">(.com / .net /.org / .in  etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>									
		<td  width=\"33%\" class=\"content-service top-border\">Website<br />Design & Development<br /><span class=\"content-font\">(Starts from just Rs. 2900/-)</span></td>
	</tr>
	<tr>
		<td height=\"5px\"></td>
	</tr>
	<tr height=\"65px\">
		<td  width=\"33%\" class=\"content-service top-border\">Web Hosting<br /><span class=\"content-font\">(Starts from just Rs. 900/-)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Web Servers <br /><span class=\"content-font\"> (Dedicated / Colocation / Cloud) <br />(Managed / Non Managed)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Email Services <br /><span class=\"content-font\">India's No.1 Mail Service <br />(Premium / Enterprise / Economy)</span></td>
	</tr>
	<tr>
		<td height=\"5px\"></td>
	</tr>
	<tr height=\"80px\">
		<td class=\"content-service top-border\">Designing <br /><span class=\"content-font\">(Website, Brochure, Visiting card, <br /> Letterhead, Hoarding, Signboards,<br />Banners, Exhibition Event Collaterals etc.)</span></td>								
		<td width=\"10px\">&nbsp;</td>									
		<td class=\"content-service top-border\">Printing <br /><span class=\"content-font\">(Offset, Screen, UV, Flex, Digital etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Photography<br /><span class=\"content-font\">(Professional & Strategic<br />Photography Services)</span></td>
	</tr>
	<tr>
		<td height=\"5px\"></td>
	</tr>
	<tr height=\"80px\">
		<td class=\"content-service top-border\">Multimedia Solutions<br /><span class=\"content-font\">(Ringtones, 3D Animation, Flash etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Mobile Applications<br /><span class=\"content-font\">(Mobile based software applications)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Bulk SMS & <br />SMS based Solutions<br /><span class=\"content-font\">(Bulk SMS, SMS based solutions,<br />Short Codes, etc.)</span></td>									
	</tr>								
	<tr>
		<td height=\"5px\"></td>
	</tr>
	<tr height=\"80px\">
		<td class=\"content-service top-border\">Security<br />Gadgets & Equipments<br /><span class=\"content-font\">(CCTV, Cameras, DVR, Vehicle Tracking, <br />Electronic Home/Vehicle Security, etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">IT Hardware Sales <br /><span class=\"content-font\">(Computers, Laptops, Printers, <br /> Firewall and all other IT Equipments)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">IT Infrastructure<br />Maintenance <br /><span class=\"content-font\">(Maintenance of Computers<br />Hardware, Servers & Networking etc.)</span></td>
	</tr>
	<tr>
		<td height=\"5px\"></td>
	</tr>
	<tr height=\"80px\">
		<td class=\"content-service top-border\">Manpower Services<br /><span class=\"content-font\">(Search, Selection,<br />Staffing, Consulting etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Consumer<br />Products & Equipments<br /><span class=\"content-font\">(Computers, Electronics,<br />Health & Fitness, etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Industrial<br />Products & Equipments<br /><span class=\"content-font\">(Lathe M/c, Drill M/c, CNC, etc.)</span></td>
	</tr>
	<tr>
		<td height=\"5px\"></td>
	</tr>
	<tr height=\"70px\">
		<td class=\"content-service top-border\">Digital Signatures<br />Certificates<br /><span class=\"content-font\">(Income Tax Filing, ROC,<br />Online Auction etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Trademark,<br />Copyright & Patent<br /><span class=\"content-font\">(Strategic Consultancy Services)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">ISO Certification<br /><span class=\"content-font\">(Strategic Consultancy Services)</span></td>
	</tr>
	<tr>
		<td height=\"5px\"></td>
	</tr>
	<tr height=\"80px\">
		<td class=\"content-service top-border\">Consulting<br />Information Technology<br /><span class=\"content-font\">(Software, IT Infrastructure,<br />Planning, Implementation etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Consulting<br />Marketing & Sales<br /><span class=\"content-font\">(Analysis, Planning, Execution etc.)</span></td>
		<td width=\"10px\">&nbsp;</td>
		<td class=\"content-service top-border\">Consulting<br />Finance<br /><span class=\"content-font\">(Home finance, Project finance etc.)</span></td>
	</tr>								
</table>
</div>
<div class=\"row wpx731 bg9 pb3 pt3\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl10\" width=\"393px\">
				<span class=\"white-text b\">".$data['company_name']."</span>
			</td>
			<td class=\"address-text ar pr10\">
				<span class=\"white-text b\">www.smeerptech.com &nbsp; sales@smeerptech.com</span> 
			</td>
		</tr>
	</table>
</div>
<div class=\"row wp731 pt5 pb5 bg8\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
	<tr>
		<td class=\"address-text al pl10\" width=\"325px\">
			<span class=\"white-text\"><b>Worldwide Network :</b></span> 
			<span class=\"address-text-small\">INDIA, USA, UK, UAE, OMAN, AUSTRALIA <br/>
			MEXICO, CANADA, EUROPE, JAPAN, SINGAPORE, MALAYSIA</span> 
		</td>
		<td class=\"address-text ar pr10\" width=\"406px\">
			<span class=\"white-text\"><b>Corporate office:</b></span> 
			<span class=\"address-text-small\">
			17, MG street | Jhena 440 025 Maharashtra | India <br/></span> 
			<b>Dedicated Sales:</b> <span class=\"address-text-small\">
			98230-99998, 98230-88884, 98230-88885, 98230-99996,98230-88889</span> 
		</td>
	</tr>
	</table>
</div>
";
*/
//Other Services EOF 

$body_services ="<div class=\"row wp100 al pt1 pb1\" style=\"height:830px\">
<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
	<tr>
		 <td bgcolor=\"#CF1F25\" style=\"padding-top:8px;padding-bottom:8px;\">
			<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"100%\">
				<tr>
					<td align=\"center\" class=\"service-heading-text\">Did you know? Hiring us guarantees you advantages like</td>
				</tr>
				<tr>
					<td align=\"center\" class=\"service-sub-heading-text\">Savings on new purchases.&nbsp;&nbsp;&nbsp;Savings on wrong purchases. &nbsp;&nbsp;&nbsp;Increased productivity. <br/>&nbsp;&nbsp;&nbsp;Value for money.&nbsp;&nbsp;&nbsp;Peace of Mind.</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td style=\"padding:15px 0px 0px 0px;\">
			<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"100%\">
				<tr>
					<td align=\"center\" colspan=\"2\" class=\"contentheading\">Our Services</td>
				</tr>
				<tr>
					<td width=\"340px\" align=\"center\" style=\"padding:15px 0px 0px 30px\" valign=\"top\">
						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"90%\">
							<tr>
								<td class=\"service-content-text\">
									<ul>
										<li>Website Development</li>
										<li>Software Development</li>
										<li>Domain & Web Hosting</li>
										<li>Email Solutions </li>
										<li>Cloud & Dedicated Servers</li>
										<li>Search Engine Ranking</li>
										<li>Internet Promotion</li>
										<li>Bulk SMS & Mobile Solutions</li>
										<li>Online Payment Solutions</li>
										<li>Multimedia Animation Solutions</li>
										<li>IT Infrastructure Uptime & Security</li>
										<li>Designing & Printing</li>
										<li>Photography & Video-shooting</li>		 
									</ul>
								</td>
							</tr>
						</table>
					</td>
					<td  width=\"340px\" align=\"center\" valign=\"top\" style=\"padding:20px 0px 0px 0px\">
						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"90%\">
							<tr>
								<td class=\"service-content-text\">
									<ul>
										<li>Digital Signatures</li>
										<li>Trademark, Copyright & patent</li>
										<li>ISO Certification</li>
										<li>Computers & Accessories</li>
										<li>Security Gadgets & Equipment</li>
										<li>Consumer Products & Equipment</li>
										<li>Industrial Products & Equipment</li>
										<li>Marketing & Sales Consulting</li>
										<li>Manpower, Training & Placement</li>
										<li>Architectural Services</li>
										<li>Real Estate Solutions</li>
										<li>Project & Financial consulting</li>
										<li>Information Technology Consulting</li>
									</ul>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"100%\">
				<tr>
					<td align=\"center\" colspan=\"2\" class=\"contentheading\">Why SMEERP</td>
				</tr>
				<tr>
					<td style=\"padding:15px 0px 0px 30px\" width=\"340px\" align=\"center\" valign=\"top\">
						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"90%\">
							<tr>
								<td class=\"service-content-text\">
									<ul>
										<li>ISO 9001:2008 Certified</li>
										<li>Presence in multiple cities</li>
										<li>Thousands of clients globally</li>
										<li>Experience of thousands of projects</li>
										<li>Multiple services under-one-roof</li>
									</ul>
								</td>
							</tr>
						</table>
					</td>
					<td  width=\"340px\" style=\"padding:20px 0px 0px 0px\" align=\"center\" valign=\"top\" >
						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"90%\">
							<tr>
								<td class=\"service-content-text\">
									<ul>
										<li>Options for products, services & pricing</li>
										<li>Professional yet friendly approach</li>
										<li>Qualified & Experienced team</li>
										<li>Well-equipped infrastructure</li>
										<li>Quality & Value for money</li>
									</ul>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan=\"2\" align=\"center\" valign=\"top\" class=\"service-content-text\">
						<ul>
							<li>Company managed by world's best SME - CRM - Nerve Center<sup>TM</sup></li>
						</ul>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td> 
			<img src=\"".$variables['images']."/proposal-gallery.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
		</td>
	</tr>	
</table>
</div>
<div class=\"row wpx731 bg9 pb3 pt3\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl10\" width=\"393px\">
				<span class=\"white-text b\">".$data['company_name']."</span>
			</td>
			<td class=\"address-text ar pr10\">
				<span class=\"white-text b\">www.smeerptech.com &nbsp; sales@smeerptech.com</span> 
			</td>
		</tr>
	</table>
</div>
<div class=\"row wp731 pt5 pb5 bg8\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
	<tr>
		<td class=\"address-text al pl10\" width=\"325px\">
			<span class=\"white-text\"><b>Worldwide Network :</b></span> 
			<span class=\"address-text-small\">INDIA, USA, UK, UAE, OMAN, AUSTRALIA <br/>
			MEXICO, CANADA, EUROPE, JAPAN, SINGAPORE, MALAYSIA</span> 
		</td>
		<td class=\"address-text ar pr10\" width=\"406px\">
			<span class=\"white-text\"><b>Corporate office:</b></span> 
			<span class=\"address-text-small\">
			17, MG street | Jhena 440 025 Maharashtra | India <br/></span> 
			<b>Dedicated Sales:</b> <span class=\"address-text-small\">
			98230-99998, 98230-88884, 98230-88885, 98230-99996,98230-88889</span> 
		</td>
	</tr>
	</table>
</div>
";

//BOF Proposal gallery
/* 
$body_why ="<div class=\"row wp100 pt32\">
<div class=\"row al pb20\">
   <div class=\"row wpx731 bg9 pb3 pt3\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"70px\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl10 pt3\" width=\"393px\">
					<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
				</td>
				<td class=\"address-text ar pr10\">
					
				</td>
			</tr>
		</table>
	</div>
	<div class=\"row wp731 pt5 pb5 bg8 \">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl10\" width=\"325px\">
				<span class=\"white-text\"><b>Motivated to Serve&nbsp;&nbsp;Determined to Excel</b><sup>TM</sup></span> 
				 
			</td>
			<td class=\"address-text ar pr10\" width=\"406px\">
				<span class=\"white-text\"><b>An ISO 9001:2008 Certified Company</b></span> 
			</td>
		</tr>
		</table>
	</div>
</div>
</div>
<div class=\"row ac pb20\">
	<div class=\"ac wpx731\"> 
		<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"  bgcolor=\"#ffffff\">
		<tr>
			<td>
				<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"100%\">
					<tr>
						<td valign=\"bottom\">
							<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
								<tr><td colspan=\"3\" class=\"whycontentheading\">Why SMEERP ?</td></tr>
								<tr height=\"60px\">
									<td width=\"50%\" class=\"whycontent whytop-border\">-- Experience of more than 10,000 projects.<br /><br />-- More than 46 software products.<br /><br />-- More than 5000 clients across 23 countries.<br /><br />-- ISO 9001:2008 Certified Company.<br /><br />-- Processes managed by World's Best CRM & Project Management System \"Nerve Center<sup>TM</sup>\".</td>
									<td width=\"10px\">&nbsp;</td>
									<td  width=\"50%\" class=\"whycontent whytop-border\">-- On-time and Quality services.<br /><br />-- 360 degree experience & expertise in IT.<br /><br />-- Mutually beneficial relationship policy.<br /><br />-- Professional yet friendly approach.<br /><br />-- Under-one-roof IT Services & Business Solutions. </td>
								</tr>
							</table>
						</td>							
					</tr>					
				</table>
			</td>			
		</tr>
		<tr>
			<td>
				<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"100%\">
					<tr>
						<td valign=\"bottom\">
							<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
							<tr>
								<td colspan=\"2\" class=\"whycontentbottomheading\">And we precisely know that it is our job & responsibility to;</td>
							</tr>
							<tr height=\"60px\">
								<td width=\"50%\" class=\"whycontentbottom whytop-border\">-- serve you with quality services On-time.<br /><br />-- take care of your work and schedules.<br /></td>
								<td width=\"50%\" class=\"whycontentbottom whytop-border\">-- keep you informed about progress of work.<br /><br />-- ensure that work is completed as promised.<br /></td>
							</tr>
							<tr>
								<td colspan=\"2\" class=\"whycontentbottomheading\">\"I insist you to try our services to experience the difference.<br /> And I guarantee your benefit.\" - Aryan Salve, Director
								</td>
							</tr>			
							</table>											
						</td>				
					</tr>					
				</table>
			</td>
		</tr>
		<tr> 
			<td class=\"pt3\">
				<img src=\"".$variables['images']."/quotation-gallery.jpg\" 
				alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
			</td>
		</tr>
		</table>	 
	</div>
</div> 
<div class=\"row wpx731 bg9 pb3 pt3\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl10\" width=\"393px\">
				<span class=\"white-text b\">".$data['company_name']."</span>
			</td>
			<td class=\"address-text ar pr10\">
				<span class=\"white-text b\">www.smeerptech.com &nbsp; sales@smeerptech.com</span> 
			</td>
		</tr>
	</table>
</div>
<div class=\"row wp731 pt5 pb5 bg8 \">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
	<tr>
		<td class=\"address-text al pl10\" width=\"325px\">
			<span class=\"white-text\"><b>Worldwide Network :</b></span> 
			<span class=\"address-text-small\">INDIA, USA, UK, UAE, OMAN, AUSTRALIA <br/>
			MEXICO, CANADA, EUROPE, JAPAN, SINGAPORE, MALAYSIA</span> 
		</td>
		<td class=\"address-text ar pr10\" width=\"406px\">
			<span class=\"white-text\"><b>Corporate office:</b></span> 
			<span class=\"address-text-small\">
			17, MG street | Jhena 440 025 Maharashtra | India <br/></span> 
			<b>Dedicated Sales:</b> <span class=\"address-text-small\">
			98230-99998, 98230-88884, 98230-88885, 98230-99996,98230-88889</span> 
		</td>
	</tr>
	</table>
</div>
"; */

//EOF Proposal gallery
$footer_content ="
</div>
</div>";
    
	//echo $content = $css.$header_content.$body.$body_other_service.$body_why.$terms_condition.$footer_content ;
    //$content = $css.$header_content.$body.$terms_condition.$body_services_header.$body_services.$footer_content ;
    $content = $css.$header_content.$body.$terms_condition.$footer_content ;
	$invoiceArr['content'] = $content ;
    $invoiceArr['header'] = "<div class=\"row al pb5 pt10 wp100 content-small\">
		<div class=\"ac\">Classified Document - Only respective client and SMEERP Personnel's are authorized to view or edit 
		or store</div>
		</div>";
	$invoiceArr['header']='';
    $invoiceArr['footer'] = "" ;
    createQuotationPDF2($content,$filedestination);
    //createQuotationPDF($data["number"],$invoiceArr);
}

        function getBifurcation( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }
            
            $query .= " FROM ". TABLE_BILL_INV_B;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
                }
                
                return ( $db->nf() );
            }
            else {
                return false;
            }
        }

    }
    
 
 
function createQuotationPDF( $q_no, $data ) {
		require_once(DIR_FS_ADDONS .'/html2pdf/config.quotation.inc.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
		parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
	  
		global 	$g_config;
		
		class ReportData extends Fetcher {
			var $content;
			var $url;
		
			function get_data($data) {
				return new FetchedDataURL($this->content, array(), "","");
			}
		
			function ReportData($content) {
				$this->content = $content;
			}
			
			function get_base_url() {
				return "";
			}
		}
		
		$contents 	= '';
		$contents = $data['content'];      
		
		// Configuration Settings.
		$g_config = array(
							'process_mode' 	=> 'single',
							'mode'		    => '',
							'pixels'		=> '750',
							'scalepoints'	=> '1',
							'renderimages'	=> '1',
							'renderlinks'	=> '1',
							'renderfields'	=> '1',
							'media'			=> 'A4',
							'cssmedia'		=> 'screen',
							//'leftmargin'	=> '30',
							'leftmargin'	=> '5',
							//'rightmargin'	=> '15',
							'rightmargin'	=> '5',
							'topmargin'		=> '10',
							'bottommargin'	=> '10',
							'encoding'		=> 'iso-8859-1',
							//'encoding'		=> 'UTF-8',
							'headerhtml'	=> $data['header'],
							'boydhtml'		=> $contents,
							'footerhtml'	=> $data['footer'],
							'watermarkhtml'	=> '',
							'pslevel'		=> '3',
							'method'		=> 'fpdf',
							'pdfversion'	=> '1.3',
							'output'		=> '2',
							'convert'		=> 'Convert File'
						);
		
		$media = Media::predefined('A4');
		$media->set_landscape(false);
		/*
		$media->set_margins(array(	'left' => 10,
									'right' => 10,
									'top' => 15,
									'bottom' => 35)
							);
		$media->set_pixels(700);
		*/
		 $media->set_margins(array(	'left' => 5,
									'right' => 5,
									'top' => 5,
									'bottom' => 15)
							);
		$media->set_pixels(740);
		
		$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
		$g_pt_scale = $g_px_scale * 1.43;
		
		$pipeline 						= PipelineFactory::create_default_pipeline("","");
		$pipeline->pre_tree_filters[] 	= new PreTreeFilterHeaderFooter( $g_config['headerhtml'], $g_config['footerhtml']);
		$pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
		if ($g_config['renderfields']) {
			$pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
		}

		// Changed so that script will try to fetch files via HTTP in this case) and to provide absolute paths to the image files.
		// http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
		//		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
		$pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
		//$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
		$pipeline->destination 			= new DestinationFile($q_no);
		 
		$pipeline->process($q_no, $media);
		$filename = DIR_FS_QUOTATIONS_PDF .'/'. $q_no .'.pdf'; 
		@chmod($filename, 0777);
		$contents = NULL;
		return true;
}
    
?>
