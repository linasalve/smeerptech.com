<?php
	class Reports {
		
	
		/**
		 *	Function to get all the month.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */
        
        function getMonth() {
			$month = array( '1' => 'January',
							'2' => 'February',
							'3' => 'March',
							'4' => 'April',
							'5' => 'May',
							'6' => 'June',
							'7' => 'July',
							'8' => 'August',
							'9' => 'September',
							'10' => 'October',
							'11' => 'November',
							'12' => 'December'
						);
			return ($month);
		} 

        function getYear(){
            $yearStart =2008;
            $yearEnd  =   date('Y') ;
            for($j=$yearStart; $j<=$yearEnd  ;$j++){
                $lst_year[$j] = $j;
            }  
            return ($lst_year);
        }  
        
        /**
		 * Function to validate the input from the User while Adding.
          used in -> monthly attendance report
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateMonthAttAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
           
            if ( !isset($data['month']) || empty($data['month']) ) {
                $messages->setErrorMessage("Select month.");
            }
            
            if ( !isset($data['year']) || empty($data['year']) ) {
                $messages->setErrorMessage("Select year.");
            }
            
          
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 
        
        function validateExecutiveMonthAttAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
            if ( !isset($data['member_id']) || empty($data['member_id']) ) {
                $messages->setErrorMessage("Select Executive.");
            }
            
            if ( !isset($data['date_from']) || empty($data['date_from']) ) {
                $messages->setErrorMessage("Select From date.");
            }
            if ( !isset($data['date_to']) || empty($data['date_to']) ) {
                $messages->setErrorMessage("Select To date.");
            }
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $data['date_from'] = explode('/', $data['date_from']);
                $data['date_from'] = mktime(0, 0, 0, $data['date_from'][1], $data['date_from'][0], $data['date_from'][2]);
                
                $data['date_to'] = explode('/', $data['date_to']);
                $data['date_to'] = mktime(0, 0, 0, $data['date_to'][1], $data['date_to'][0], $data['date_to'][2]);
            
                if($data['date_from']  > $data['date_to'] ){
                     $messages->setErrorMessage("From date should not be greater than To date.");
                }
            }
           
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}  
		
    }
?>
