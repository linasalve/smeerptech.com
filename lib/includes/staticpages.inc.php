<?php 

	class Staticpages {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => Staticpages::BLOCKED,
							'ACTIVE'  => Staticpages::ACTIVE,
							'PENDING' => Staticpages::PENDING,
							'DELETED' => Staticpages::DELETED
						);
			return ($status);
		}
		
        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SETTINGS_STATICPAGES;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Staticpages::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Staticpages::getList($db, $user, 'page_id', " WHERE page_id = '$id'")) > 0 ) {
                    $user = $user[0];
                    if ( $user['access_level'] < $access_level ) {
                        $query = "UPDATE ". TABLE_SETTINGS_STATICPAGES
                                    ." SET page_status = '$status_new' "
                                    ." WHERE page_id = '$id'";
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            $messages->setOkMessage("The Status has been updated.");
                        }
                        else {
                            $messages->setErrorMessage("The Status was not updated.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of page with the current Access Level.");
                    }
                }
                else {
                    $messages->setErrorMessage("The Page was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $user = NULL;
            if ( (Staticpages::getList($db, $user, 'page_id', " WHERE page_id = '$id'")) > 0 ) 
			{
                $user = $user[0];
                if ( $user['page_status'] == Staticpages::BLOCKED || $user['page_status'] == Staticpages::DELETED) 
				{
					$query = "DELETE FROM ". TABLE_SETTINGS_STATICPAGES
								." WHERE page_id = '". $id ."'";
					if ( !$db->query($query) || $db->affected_rows()<=0 ) 
						$messages->setErrorMessage("The Page is not deleted.");
					else
						$messages->setOkMessage("The Page has been deleted.");
				}
				else
					$messages->setErrorMessage("Cannot delete Page.");
			}
			else
				$messages->setErrorMessage("Page not found.");        
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
 
            if ( !isset($data['page_name']) || empty($data['page_name']) ) {
                $messages->setErrorMessage('Page Name should be provided.');
			}
            
            if ( !isset($data['page_heading']) || empty($data['page_heading']) ) {
                $messages->setErrorMessage('Page Heading should be provided.');
            }
            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
 
            if ( !isset($data['page_name']) || empty($data['page_name']) ) {
                $messages->setErrorMessage('Page Name should be provided.');
			}
            
            if ( !isset($data['heading']) || empty($data['heading']) ) {
                $messages->setErrorMessage('Page Heading should be provided.');
            }


            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
        
        
        function getRoles(&$db, $level, $role_ids='') {
            include_once ( DIR_FS_INCLUDES .'/user-roles.inc.php');
            
            $role_list = NULL;
            if ( !empty($role_ids) ) {
                if ( is_array($role_ids) ) {
                    $role_ids = ' AND id IN ('. ('\''. implode('\',', $role_ids) .'\'') .')';
                }
                else {
                    $role_ids = ' AND id IN ('. ('\''. str_replace(',', '\',\'', $role_ids) .'\'') .')';
                }
            }
            else {
                $role_ids = '';
            }
            
            $condition  = " WHERE access_level < '". $level ."'"
                            . $role_ids
                            ." AND status = '". UserRoles::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            
            if ( (UserRoles::getList($db, $role_list, 'id, title', $condition)) <= 0 ) {
                $role_list[] = array('id' => '', 'title' => 'No Role Available' );
            }
            
            return ($role_list);
        }
        
        
        function getAccessLevel(&$db, $level=0, $al_list='') {
            include_once ( DIR_FS_INCLUDES .'/settings-access-level.inc.php');

            $role_list = NULL;
            if ( !empty($al_list) ) {
                if ( is_array($al_list) ) {
                    $al_list = ' AND access_level IN ('. ('\''. implode('\',', $al_list) .'\'') .')';
                }
                else {
                    $al_list = ' AND access_level IN ('. ('\''. str_replace(',', '\',\'', $al_list) .'\'') .')';
                }
            }
            else {
                $al_list = '';
            }
            
            $access     = NULL;
            $condition  = " WHERE access_level < '". $level ."'"
                            . $al_list
                            ." AND status = '". AccessLevel::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            if ( (AccessLevel::getList($db, $access, 'title, access_level', $condition)) <= 0 ) {
                $access[] = array('title' => 'None', 'access_level' => '0' );
            }
            
            return ($access);
        }
    }
?>