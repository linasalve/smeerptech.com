<?php
	class AccessLevel{
		
        const BLOCKED = BLOCK;
        const ACTIVE  = ACTIVE;
        const PENDING = PENDING;
        const DELETED = DELETED;

		function getStatus() {
			$status = array('BLOCKED' =>AccessLevel::BLOCKED,
							'ACTIVE'  =>AccessLevel::ACTIVE,
							'PENDING' =>AccessLevel::PENDING,
							'DELETED' =>AccessLevel::DELETED
						);
			return ($status);
		}
		
		/**
		*	Function to get all Email Templates.
		*
		*	@param Object of database
		*	@param Array of User role
		* 	@param required fields
		* 	@param condition
		*	return array of User roles
		*	otherwise return NULL
		*/		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
            $query .= " FROM ". TABLE_SETTINGS_ACC_LVL;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}

       
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			// Check if status is valid or not.
			if ( !in_array($status_new, AccessLevel::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
				 $query = "UPDATE ". TABLE_SETTINGS_ACC_LVL
								." SET status = '$status_new' "
							." WHERE id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage("The Status has been changed.");
				}
				else {
					$messages->setErrorMessage("The Status was not changed.");
				}
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
      }
	
        //used to delete records.
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $list = NULL;
            if ( AccessLevel::getList( $db, $list, 'id, status', " WHERE id = '$id'") > 0 ) {
                // Check if the User Role is blocked or not.
                $list = $list[0];
                if ( $list['status'] != AccessLevel::BLOCKED &&  $list['status'] != AccessLevel::DELETED ) {
                    $messages->setErrorMessage("To Delete an Access Level its Status should be changed to 'Blocked'.");
                }
                
                if ( $messages->getErrorMessageCount() <= 0 ) {
                    // Delete the User Role
                        $query = "DELETE FROM ". TABLE_SETTINGS_ACC_LVL ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("Access Level has been deleted.");
                    }
                    else {
                        $messages->setErrorMessage("Internal Error: The Access Level was not deleted.");
                    }
                }
            }
            else {
                $messages->setErrorMessage("The Access Level was not found, it may have been deleted or not created yet.");
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		 
        
        /**
		 * Function to validate the input from the User while Adding a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd($data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            if ( !isset($data['title']) || !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_\s]{1,32}$/', $data['title']) ) {
                $messages->setErrorMessage("The Title is not valid.");
                $messages->setErrorMessage("Title should contain (a-z, A-z, 0-9, _, space) and begin with an alphabet.");
                $messages->setErrorMessage("The length of the title should be between 1 to 32 characters");
            }
            else {
                $condition = " WHERE title = '". $data['title'] ."'";
                if ( AccessLevel::getList( $db, $list, 'id', $condition) > 0 ) {
                    $messages->setErrorMessage("Access Level with the same Title already exists.");
                }
            }

            if ( !isset($data['access_level']) || !preg_match('/^[\d]+$/', $data['access_level']) || ($data['access_level'] > 65535) ) {
                $messages->setErrorMessage("The Access Level is not valid.");
                $messages->setErrorMessage("Access Level should be a number only.");
                $messages->setErrorMessage("The maximum value for Access Level is 65535.");
            }

            if ( $data['status'] != AccessLevel::PENDING && $data['status'] != AccessLevel::ACTIVE 
                 && $data['status'] != AccessLevel::BLOCKED && $data['status'] != AccessLevel::DELETED ) {
                $messages->setErrorMessage("The Status is not valid.");
            }

			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate($id, $data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            if ( !isset($data['title']) || !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_\s]{1,32}$/', $data['title']) ) {
                $messages->setErrorMessage("The Title is not valid.");
                $messages->setErrorMessage("Title should contain (a-z, A-z, 0-9, _, space) and begin with an alphabet.");
                $messages->setErrorMessage("The length of the title should be between 1 to 32 characters");
            }
            else {
                $condition = " WHERE title = '". $data['title'] ."'"
                                ." AND id != '". $id ."'";
                if ( AccessLevel::getList( $db, $list, 'id', $condition) > 0 ) {
                    $messages->setErrorMessage("Access Level with the same Title already exists.");
                }
            }

            if ( !isset($data['access_level']) || !preg_match('/^[\d]+$/', $data['access_level']) || ($data['access_level'] > 65535) ) {
                $messages->setErrorMessage("The Access Level is not valid.");
                $messages->setErrorMessage("Access Level should be a number only.");
                $messages->setErrorMessage("The maximum value for Access Level is 65535.");
            }

            if ( $data['status'] != AccessLevel::PENDING && $data['status'] != AccessLevel::ACTIVE 
                    && $data['status'] != AccessLevel::BLOCKED && $data['status'] != AccessLevel::DELETED ) {
                $messages->setErrorMessage("The Status is not valid.");
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
    }
?>
