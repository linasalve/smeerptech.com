<?php 
class TicketReminder {		 
		//Status for Ticket Reminder BOF
		const RPENDING = 0;
		const RCOMPLETED = 1;
		const RCANCELLED = 2; 
		//Status for Ticket Reminder EOF
		
		
        const COMPLETED = 1;
		const PENDING = 0;
		
		const ACTIVE = 1;
		const DEACTIVE = 0;
        
        const CLIENT_COMMENT = 0;
		const ADMIN_COMMENT = 1;
        
        const CLIENT_PANEL = 0;
		const ADMIN_PANEL = 1;
        
        const PENDINGWITHCLIENTS = 0;      
        const PENDINGWITHSMEERP = 1;        
        const PENDINGWITHSMEERPCOZCLIENT = 2;        
        const CLOSED = 3; 
		
        const UNSATISFACTORY = 1;
        const AVERAGE = 2;
        const GOOD= 3;
		const FANTASTIC= 4;
		
		const STALL_PENDING = 0;
		const STALL_COMPLETED = 1;
		const STALL_INPROCESS = 2;
		const STALL_CANCELLED = 3;
		
		const STALL_BATCH_ACTIVE = 1;
		const STALL_BATCH_CANCELLED = 0;
		
		const TYP_NONE = 0;
		const TYP_PROJECT = 1;
		const TYP_REMINDER = 2;
		const TYP_INFO = 3;
		const TYP_SSUPPORT = 4;
		
		function getStatus() {
			$status = array('Active' => TicketReminder::ACTIVE,
							'De-active'  => TicketReminder::DEACTIVE
						);
			return ($status);
		}
		function getType() {
			$type = array(	
							'Support' => 	TicketReminder::TYP_NONE,
							'Senior Support' => 	TicketReminder::TYP_SSUPPORT,
							'Project' => 	TicketReminder::TYP_PROJECT,
							'Billing'  => TicketReminder::TYP_REMINDER,
							'Info'  => TicketReminder::TYP_INFO
						);
			return ($type);
		}
		
        function getStAllStatus(){
			$status = array('PENDING' => TicketReminder::STALL_PENDING,
							'COMPLETED'  => TicketReminder::STALL_COMPLETED,
							'INPROCESS'  => TicketReminder::STALL_INPROCESS,
							'CANCELLED'  => TicketReminder::STALL_CANCELLED,
						);
			return ($status);
		}
        function getTicketStatus() {
			$status = array('OPEN & PENDING WITH CLIENT' => TicketReminder::PENDINGWITHCLIENTS,
							'OPEN & PENDING WITH SMEERP'  => TicketReminder::PENDINGWITHSMEERP,
							'OPEN & PENDING WITH SMEERP BCOZ OF CLIENT'  => TicketReminder::PENDINGWITHSMEERPCOZCLIENT,
							'CLOSED'  => TicketReminder::CLOSED
						);
			return ($status);
		}
        
        function getTicketStatusList(){
			$status = array('OPEN & PENDING WITH CLIENT' => TicketReminder::PENDINGWITHCLIENTS,
							'OPEN & PENDING WITH SMEERP'  => TicketReminder::PENDINGWITHSMEERP,
                            'OPEN & PENDING WITH SMEERP BCOZ OF CLIENT'  => TicketReminder::PENDINGWITHSMEERPCOZCLIENT
						);
			return ($status);
		}		
        function getRTicketStatus(){
			$status = array('PENDING' 	 => TicketReminder::RPENDING,
                            'COMPLETED'  => TicketReminder::RCOMPLETED,
                            'CANCELLED'  => TicketReminder::RCANCELLED
						);
			return ($status);
		}
        function getRating(){
           	$rating = array('UNSATISFACTORY' => TicketReminder::UNSATISFACTORY,
							'AVERAGE'  => TicketReminder::AVERAGE,
							'GOOD'  => TicketReminder::GOOD,
							'FANTASTIC'  => TicketReminder::FANTASTIC
						);
            return ($rating);
		}
        
        function getCronComments() {
			$status = array(0  => 'Sorry for your inconvinience',
                            1  => 'As we require some more time to do the task please give us some time. We will revert you in a short time.',
							2  => 'Sorry for your inconvinience',
							3  => 'Sorry for your inconvinience',
							4  => 'Sorry for your inconvinience',
							5  => 'Sorry for your inconvinience'
						);
			return ($status);
		}
        /*function getStatus(){			
            
           	$status = array('OPEN' => TicketReminder::OPEN,
							'CLOSED'  => TicketReminder::CLOSED
						);
            return ($status);
		}*/
		
        function getDomains(&$db, &$domains, $ticket_owner_id) {
        
            $query= "SELECT DISTINCT (order_domain) FROM ". TABLE_BILL_ORDERS 
					." WHERE ". TABLE_BILL_ORDERS .".client='".$ticket_owner_id."' AND ".TABLE_BILL_ORDERS.".order_domain!='' ";
                       
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$domains[]= $db->f('order_domain');
						//$domains[]= processSqlData($db->result());
					}
				}	                
				return ( $db->nf() );
			}
			else {
				return false;
			}
			
		}
        
        /*function getDepartments( &$db, &$department){
            
            $query= "SELECT * FROM ". TABLE_SETTINGS_DEPARTMENT 
					." WHERE ". TABLE_SETTINGS_DEPARTMENT .".status='1' AND show_in_st='1' ";
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$department[]= processSqlData($db->result());
					}
				}	                
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        
        function getPriority( &$db, &$priority){			
            
            $query= "SELECT * FROM ". TABLE_ST_PRIORITY 
					." WHERE ". TABLE_ST_PRIORITY .".priority_status='1' " 
					." ORDER BY ". TABLE_ST_PRIORITY .".priority_order ASC ";
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$priority[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        */
        
		function getStAllList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TICKET_ALL;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
		
		function getStAllClientsList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TICKET_ALL_CLIENTS;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				 return ( $total );
			}
			else {
				return false;
			}	
		}
		
		
		/**
		 *	Function to get all the support tickets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		
        function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TICKET_REMINDER; 
			$query .= " ". $condition;  
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			} 
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total = $db->f('count');
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
		
        function tStatus($id,$ticket_status, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
            $query = " UPDATE ". TABLE_TICKET_REMINDER
                    ." SET ". TABLE_TICKET_REMINDER .".ticket_status = '".$ticket_status."'"
                    ." WHERE ticket_id = '". $id ."'";
            if ( $db->query($query)  ) {
                $messages->setOkMessage("The ticket status changed Successfully.");                      
            }
            else {
                $messages->setErrorMessage("The ticket status not changed.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            } else {
                return (false);
            }
            
        }
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        /*function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Services::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Services::getList($db, $user, 'ss_id', " WHERE ss_id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_SETTINGS_SERVICES
								." SET ss_status = '$status_new' "
								." WHERE ss_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Service was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }*/
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        /*function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (Services::getList($db, $user, 'ss_id', " WHERE ss_id = '$id'")) > 0 ) 
			{
                $user = $user[0];
                if ( $user['status'] == Services::BLOCKED || $user['status'] == Services::DELETED) 
				{
					 $query = "DELETE FROM ". TABLE_SETTINGS_SERVICES
								." WHERE ss_id = '". $id ."'";
                    
					if ( !$db->query($query) || $db->affected_rows()<=0 ){
                        
						$messages->setErrorMessage("The Service is not deleted.");
					}else{
	                    $sql ="DELETE FROM ".TABLE_SETTINGS_SERVICES_PRICE." WHERE service_id='".$id."'";
                        $db->query($sql) ;
                        
                        $messages->setOkMessage("The Service has been deleted.");
                    }
				}
				else
					$messages->setErrorMessage("Cannot delete service.");
			}
			else
				$messages->setErrorMessage("Service not found.");
		}*/
        function getNewNumber(&$db){
            
            $ticket_no= 3000;
            $query      = "SELECT MAX(ticket_no) AS ticket_no FROM ". TABLE_TICKET_REMINDER;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("ticket_no") > 0){
                $ticket_no = $db->f("ticket_no");
            }
			$ticket_no++;
            return $ticket_no;
        }
		function getStAllNewNumber(&$db){
            
            $ticket_no= 3000;
            $query      = "SELECT MAX(ticket_no) AS ticket_no FROM ". TABLE_TICKET_ALL;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("ticket_no") > 0){
                $ticket_no = $db->f("ticket_no");
            }
			$ticket_no++;
            return $ticket_no;
        }
        
        function getTicketId($ticketNo){
            $db 		= new db_local; // database handle
            $ticket_id=0;
            $query      = "SELECT ticket_id FROM ". TABLE_TICKET_REMINDER." WHERE ticket_no ='".$ticketNo."'";
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("ticket_id") > 0){
                $ticket_id = $db->f("ticket_id");
            }
			
            return $ticket_id;
        }
        function getTicketDetails($ticketNo){
            $db 		= new db_local; // database handle
            $ticket_id=0;
            $ticket_status='';
            $ticketdetails=array();
            $ticketdetails['ticket_id']=  '';
			$ticketdetails['flw_ord_id']=  '';
            $ticketdetails['inv_id']=  '';
            $ticketdetails['pinv_id']=  '';
            $ticketdetails['ticket_status']='';
            $ticketdetails['ticket_number']='';
            $ticketdetails['ticket_subject']='';
            $ticketdetails['ticket_owner_uid']='';
            $ticketdetails['ticket_owner']='';
            $ticketdetails['to_email']='';
            $ticketdetails['cc_email']='';
            $ticketdetails['bcc_email']='';
            $ticketdetails['ticket_creator_uid']='';            
            $ticketdetails['from_admin_panel']='';            
            $ticketdetails['last_comment_from']='';            
             $ticketdetails['from_email']='';    
            
            $query  = "SELECT ticket_id, ticket_status,ticket_subject ,ticket_owner_uid,ticket_owner,ticket_creator_uid,to_email,cc_email,bcc_email,from_email,
            from_admin_panel,last_comment_from, flw_ord_id,invoice_id,invoice_profm_id
                           FROM ". TABLE_TICKET_REMINDER." WHERE ticket_no ='".$ticketNo."'";
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("ticket_id") > 0){
                $ticket_id = $db->f("ticket_id");
                $ticket_status = $db->f("ticket_status");
				$flw_ord_id = $db->f("flw_ord_id");
                $invoice_id = $db->f("invoice_id");
                $invoice_profm_id = $db->f("invoice_profm_id");
                $ticket_subject = $db->f("ticket_subject");
                $ticket_owner_uid = $db->f("ticket_owner_uid");
                $ticket_creator_uid = $db->f("ticket_creator_uid");
                $ticket_owner = $db->f("ticket_owner");
                $to_email = $db->f("to_email");
                $cc_email = $db->f("cc_email");
                $bcc_email = $db->f("bcc_email");
				$from_email = $db->f("from_email");
                $from_admin_panel = $db->f("from_admin_panel");
                $last_comment_from = $db->f("last_comment_from");
                
                $ticketdetails['ticket_id']=  $ticket_id ;
				$ticketdetails['flw_ord_id']=  $flw_ord_id ;
                $ticketdetails['inv_id']=  $invoice_id ;
                $ticketdetails['pinv_id']=  $invoice_profm_id ;
                $ticketdetails['ticket_status']=  $ticket_status ;
                $ticketdetails['ticket_number']=  $ticketNo ;
                $ticketdetails['ticket_subject']=  $ticket_subject ;
                $ticketdetails['ticket_owner']=  $ticket_owner ;
                $ticketdetails['ticket_owner_uid']=  $ticket_owner_uid ;
                $ticketdetails['to_email']=  $to_email ;
                $ticketdetails['cc_email']=  $cc_email ;
                $ticketdetails['bcc_email']=  $bcc_email ;
				$ticketdetails['from_email']=  $from_email ;
                $ticketdetails['ticket_creator_uid']=  $ticket_creator_uid ;
                $ticketdetails['from_admin_panel']=  $from_admin_panel ;
                $ticketdetails['last_comment_from']=  $last_comment_from ;
            }
			
            return $ticketdetails;
        }
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            $files = $_FILES;
            if ( !isset($data['ces_reminder_dt']) || empty($data['ces_reminder_dt']) ) 
                $messages->setErrorMessage('Reminder date should not be empty.');
				
			if ( !isset($data['ticket_subject']) || empty($data['ticket_subject']) ) 
                $messages->setErrorMessage('Subject cannot be empty.');
            
            if ( !isset($data['ticket_text']) || empty($data['ticket_text']) ) 
                $messages->setErrorMessage('Comment cannot be empty.');
            
		    if(empty($data["hrs"]) && empty($data["min"])){
            
                $messages->setErrorMessage("Please select Estimated Hrs.");
            }
            /*if ( !isset($data['domain_name']) || empty($data['domain_name']) ) 
                $messages->setErrorMessage('Domain name cannot be empty.');*/
            
            $data['ticket_attachment'] = '';
            
            if((!empty($files['ticket_attachment']) && (!empty($files['ticket_attachment']['name'])))){
                $filename = $files['ticket_attachment']['name'];
                $data['ticket_attachment'] = $filename;
                $type = $files['ticket_attachment']['type'];
                 $size = $files['ticket_attachment']['size'];
                //$max_size = ($data['max_file_size'] * 1024);
                $max_size = ($data['max_file_size'] * 1);
				
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than '.$data['max_file_size_mb']."MB");
                }
            }
            /*$fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                    
                   if(!empty($val["name"]) ){
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of file is greater than 2Mb');
            }*/
            
            /*if ( $_FILES["ticket_attachment"]["name"] != "") {
                if ( $_FILES["ticket_attachment"]["error"] == "0") {
                    if($_FILES["ticket_attachment"]["size"] > (MAX_FILE_SIZE*1024)){
                        $error["size_limit"] = 1 ;
                    }
                    
                    if ( !in_array($_FILES["ticket_attachment"]["type"], $allowed_file_types) ) {
                        $error["invalid_attach"] = 1 ;
                    }
                    
                    $ticket_attachment  = $ticket_no ."_". $_FILES["ticket_attachment"]["name"] ;
                    $ticket_attachment_path = FILE_SAVE_DOMAIN."/attachments" ;
                
                    if ( empty($error) && $error["invalid_attach"] == 0){
                        if (!copy($_FILES['ticket_attachment']['tmp_name'], FILE_SAVE_PATH."/attachments/".$ticket_attachment)){
                            $error["cannot_attach"] = 1 ;
                            $ticket_attachment = "" ;
                        }
                    }
                }
                else {
                    $error["cannot_upload"] = 1 ;
                }
            }*/
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		function validateStAllAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            $files = $_FILES;
            if(!empty($data['test_email'])){
				if(!isValidEmail($data['test_email'])){
					$messages->setErrorMessage('<b>'.$data['test_email'].'</b> Test email Address is not valid.');
				}
			}
			if ( !isset($data['ticket_subject']) || empty($data['ticket_subject']) ) 
                $messages->setErrorMessage('Subject cannot be empty.');
            
            if ( !isset($data['ticket_text']) || empty($data['ticket_text']) ) 
                $messages->setErrorMessage('Comment cannot be empty.');
            
			 
				
		    if(!isset($data['mail_vendor']) && !isset($data['mail_client']) && !isset($data['mail_to_all_su']) 
				&& empty($data['email'])){
				
                $messages->setErrorMessage('Select the checkbox to whom you want to send the mail.');
			}
				
		    if( ( isset($data['mail_vendor']) || isset($data['mail_client']) || isset($data['mail_to_all_su']) ) 
			&& !empty($data['email']) ){
				$messages->setErrorMessage("Please select Either Checkbox or Enter Comma Seperated emails.");
			}
			if(!empty($data['email'])){
				$emailArr=explode(",",$data['email']);
				if(!empty($emailArr)){
					foreach($emailArr as $key=>$val){
						if(!isValidEmail($val)){
							$messages->setErrorMessage('<b>'.$val.'</b> Email Address is not valid.<br/>Please check the comma seperated emails');
						}
					}
				}
			}
            
            $data['ticket_attachment'] = '';
            
            if((!empty($files['ticket_attachment']) && (!empty($files['ticket_attachment']['name'])))){
                $filename = $files['ticket_attachment']['name'];
                $data['ticket_attachment'] = $filename;
                $type = $files['ticket_attachment']['type'];
                 $size = $files['ticket_attachment']['size'];
                //$max_size = ($data['max_file_size'] * 1024);
                $max_size = ($data['max_file_size'] * 1);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
      
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		
		function validateStAllUpdate(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            $files = $_FILES;
            if(!empty($data['test_email'])){
				if(!isValidEmail($data['test_email'])){
					$messages->setErrorMessage('<b>'.$data['test_email'].'</b> Test email Address is not valid.');
				}
			}
			if ( !isset($data['ticket_subject']) || empty($data['ticket_subject']) ) 
                $messages->setErrorMessage('Subject cannot be empty.');
            
            if ( !isset($data['ticket_text']) || empty($data['ticket_text']) ) 
                $messages->setErrorMessage('Comment cannot be empty.');
            
			if(!isset($data['mail_vendor']) && !isset($data['mail_client']) && !isset($data['mail_to_all_su']) 
				&& empty($data['email'])){
				
                $messages->setErrorMessage('Select the checkbox to whom you want to send the mail.');
			}
				
		    if( ( isset($data['mail_vendor']) || isset($data['mail_client']) || isset($data['mail_to_all_su']) ) 
			&& !empty($data['email']) ){
				$messages->setErrorMessage("Please select Either Checkbox or Enter Comma Seperated emails.");
			}
			if(!empty($data['email'])){
				$emailArr=explode(",",$data['email']);
				if(!empty($emailArr)){
					foreach($emailArr as $key=>$val){
						if(!isValidEmail($val)){
							$messages->setErrorMessage('<b>'.$val.'</b> Email Address is not valid.<br/>Please check the comma seperated emails');
						}
					}
				}
			}
           
            
            $data['ticket_attachment'] = '';
            
            if((!empty($files['ticket_attachment']) && (!empty($files['ticket_attachment']['name'])))){
                $filename = $files['ticket_attachment']['name'];
                $data['ticket_attachment'] = $filename;
                $type = $files['ticket_attachment']['type'];
                 $size = $files['ticket_attachment']['size'];
                //$max_size = ($data['max_file_size'] * 1024);
                $max_size = ($data['max_file_size'] * 1);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
      
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        /*function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the service title 
            if ( !isset($data['ss_title']) || empty($data['ss_title']) ) 
                $messages->setErrorMessage('Service title should be provided.');
            
            if ( !isset($data['domain_name']) || empty($data['domain_name']) ) 
                $messages->setErrorMessage('Domain name cannot be empty.');
            
            //check for same service title name
			$service_title = $data["ss_title"];
			$sid = $data["ss_id"];
            
            if ( (Services::getList($db, $service, 'ss_title', " WHERE ss_title = '$service_title' && ss_id !='$sid'")) > 0 )
				$messages->setErrorMessage('Service title name already exists. Please change the title.');
            
            //code to check price values BOf 
            
            if(!empty($data['ss_parent_id'])){            
               for ( $i=0; $i < count($data['service_price']); $i++ ) {                        
                    if ( empty($data['service_price'][$i]) ) {
                        $messages->setErrorMessage('The Price field at Position '. ($i+1) .' cannot be empty.');
                    }                   
                    else {
                        if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['service_price'][$i]) ) {
                            $messages->setErrorMessage('The Price field at Position '. ($i+1) .' is not valid.');
                        }
                    }
               }        
            }   
            
            //code to check price values eOf       
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
        function getParent(&$db,&$services,$id='0')
		{
			$sql = "SELECT ".TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title FROM ".TABLE_SETTINGS_SERVICES." WHERE ss_parent_id='".$id."'";
			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
					$services[] = processSqlData($db->Record);
                    //$title = processSqlData($db->f('ss_title')) ;
                   // $ss_id = $db->f('ss_id') ;
					//$services[$ss_id] = $title ;

				return ( $db->nf() );
			}
			else 
				return false;			
		}
        
        function getPriceList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp=''){
            $query = "SELECT DISTINCT ";
			
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) ";
			}
			
			$query .= " FROM ". TABLE_SETTINGS_SERVICES_PRICE;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
					}
				}
				
				return ( $db->nf() );
			}
			else {
				return false;
			}	
        
        }*/
        
		function validateMemberSelect(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			if ( !isset($data['client']) || empty($data['client']) ) 
                $messages->setErrorMessage('Please choose the Client.');
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
        function validateAssign(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the members
            /*  if ( !isset($data['assign_members']) || empty($data['assign_members']) ) 
                $messages->setErrorMessage('Plz select the members.'); */
            
			if ( !isset($data['order_id']) || empty($data['order_id']) ) 
                $messages->setErrorMessage('Please select the order.');
			
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
		function changeType($id,$type, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL; 
            $query = " UPDATE ". TABLE_TICKET_REMINDER ." SET ". TABLE_TICKET_REMINDER .".ticket_type = '".$type."'"." WHERE ".TABLE_TICKET_REMINDER.".ticket_id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("Type of support ticket has been changed.");                      
            }
            else {
                $messages->setErrorMessage("Type of support ticket not changed. Try Again.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
            $query = " UPDATE ". TABLE_TICKET_REMINDER
                    ." SET ". TABLE_TICKET_REMINDER .".status = '".TicketReminder::ACTIVE."'"
                    ." WHERE ticket_id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The support ticket has been activated.");                      
            }
            else {
                $messages->setErrorMessage("The support ticket has not been activated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
               	
            $query = " UPDATE ". TABLE_TICKET_REMINDER
                    ." SET ". TABLE_TICKET_REMINDER .".status = '".TicketReminder::DEACTIVE."'"
                    ." WHERE ticket_id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The support ticket has been deactivated.");                      
            }
            else {
                $messages->setErrorMessage("The support ticket has not been deactivated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function getStClient($db, $email){
        
            $fields = TABLE_CLIENTS .'.user_id'
                        .','. TABLE_CLIENTS .'.number'
                        .','. TABLE_CLIENTS .'.f_name'
                        .','. TABLE_CLIENTS .'.l_name'
                        .','. TABLE_CLIENTS .'.title'
                        .','. TABLE_CLIENTS .'.billing_name'
                        .','. TABLE_CLIENTS .'.parent_id'
                        .','. TABLE_CLIENTS .'.email'
                        .','. TABLE_CLIENTS .'.email_1'
                        .','. TABLE_CLIENTS .'.email_2'
                        .','. TABLE_CLIENTS .'.email_3'
                        .','. TABLE_CLIENTS .'.email_4'
                        .','. TABLE_CLIENTS .'.org';
                        
            $condition_query = " WHERE LOWER(email)='".$email."' OR LOWER(email_1)='".$email."' OR LOWER(email_2) = '".$email."' OR LOWER(email_3) = '".$email."' OR LOWER(email_4) = '".$email."' LIMIT 0, 1 ";        
            $list	= NULL;
            Clients::getList( $db, $list, $fields, $condition_query);
            $parent_id = $list['0']['parent_id'];
            
            $mainClient	= NULL;
            $subClient	= NULL;
            
            if(!empty($parent_id)){
                $mainClient1=null;
                
                $condition_query1 = " WHERE user_id='".$parent_id."' ";        
                Clients::getList( $db, $mainClient1, $fields, $condition_query1);
                $mainClient = $mainClient1;
                $subClient = $list;
            }else{
                $mainClient =$list;
            }
           
            $clientDetails[]=array(
                                    "mainClient"=>$mainClient,
                                    "subClient"=>$subClient
                                );
            return($clientDetails);
        
        }  
    }
?>