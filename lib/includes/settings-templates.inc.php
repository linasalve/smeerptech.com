<?php
	class EmailTemplates{
		
        const BLOCKED = BLOCK;
        const ACTIVE  = ACTIVE;
        const PENDING = PENDING;
        const DELETED = DELETED;

		function getStatus() {
			$status = array('BLOCKED' =>EmailTemplates::BLOCKED,
							'ACTIVE'  =>EmailTemplates::ACTIVE,
							'PENDING' =>EmailTemplates::PENDING,
							'DELETED' =>EmailTemplates::DELETED
						);
			return ($status);
		}
		
		/**
		*	Function to get all Email Templates.
		*
		*	@param Object of database
		*	@param Array of User role
		* 	@param required fields
		* 	@param condition
		*	return array of User roles
		*	otherwise return NULL
		*/		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
            $query .= " FROM ". TABLE_SETTINGS_EMAIL_TPL;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}

       
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			// Check if status is valid or not.
			if ( !in_array($status_new, EmailTemplates::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
				 $query = "UPDATE ". TABLE_SETTINGS_EMAIL_TPL
								." SET status = '$status_new' "
							." WHERE id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage("The Status has been changed.");
				}
				else {
					$messages->setErrorMessage("The Status was not changed.");
				}
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
      }
	
        //used to delete records.
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $list = NULL;
            if ( EmailTemplates::getList( $db, $list, 'id, status', " WHERE id = '$id'") > 0 ) {
                // Check if the User Role is blocked or not.
                $list = $list[0];
                if ( $list['status'] != EmailTemplates::BLOCKED &&  $list['status'] != EmailTemplates::DELETED ) {
                    $messages->setErrorMessage("To Delete a Email Template its Status should be changed to 'Blocked'.");
                }
                
                if ( $messages->getErrorMessageCount() <= 0 ) {
                    // Delete the User Role
                        $query = "DELETE FROM ". TABLE_SETTINGS_EMAIL_TPL ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("Email Template has been deleted.");
                    }
                    else {
                        $messages->setErrorMessage("Internal Error: The Email Template was not deleted.");
                    }
                }
            }
            else {
                $messages->setErrorMessage("The Email Template was not found, it may have been deleted or not created yet.");
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		 
        
        /**
		 * Function to validate the input from the User while Adding a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd($data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            if ( !isset($data['name']) || !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_]{1,40}$/', $data['name']) ) {
                $messages->setErrorMessage("The Template Name is not valid.");
                $messages->setErrorMessage("Name should contain (a-z, A-z, 0-9, _) and being with an alphabet.");
                $messages->setErrorMessage("The length of the name should be between 3 to 50 characters");
            }
            else {
                $condition = " WHERE name = '". $data['name'] ."'";
                if ( EmailTemplates::getList( $db, $list, 'id', $condition) > 0 ) {
                    $messages->setErrorMessage("A Template with the same Name already exists.");
                }
            }

            if ( !isset($data['description']) || empty($data['description']) ) {
                $messages->setErrorMessage("Enter the Description for the Template.");
            }
            
            if ( $data['is_html'] != '0' && $data['is_html'] != '1' ) {
                $messages->setErrorMessage("Select the mode by which to Send email as.");
            }
            
            if ( !isset($data['from_name']) || empty($data['from_name']) ) {
                $messages->setErrorMessage("Enter the Name that will appear in the From field.");
            }
            if ( !isset($data['from_email']) || empty($data['from_email']) ) {
                $messages->setErrorMessage("Enter the Email that will appear in the From field.");
            }
            elseif ( !isValidEmail($data['from_email']) ) {
                $messages->setErrorMessage("The email specified in From Email is invalid.");
            }
            
            if ( !isset($data['subject']) || empty($data['subject']) ) {
                $messages->setErrorMessage("Enter the Subject for the email template.");
            }
            
            if ( !isset($data['text']) || empty($data['text']) || (strlen($data['text'])<10) ) {
                $messages->setErrorMessage("Enter the Body Text for the email template.");
            }
            
            if ( $data['status'] != EmailTemplates::PENDING && $data['status'] != EmailTemplates::ACTIVE 
                 && $data['status'] != EmailTemplates::BLOCKED && $data['status'] != EmailTemplates::DELETED ) {
				$messages->setErrorMessage("The Status of the Email Template is not valid.");
			}

			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate($id, $data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            if ( !isset($data['name']) || !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_]{1,40}$/', $data['name']) ) {
                $messages->setErrorMessage("The Template Name is not valid.");
                $messages->setErrorMessage("Name should contain (a-z, A-z, 0-9, _) and being with an alphabet.");
                $messages->setErrorMessage("The length of the name should be between 3 to 50 characters");
            }
            else {
                $condition = " WHERE name = '". $data['name'] ."'"
                                ." AND id != '". $id ."'";
                if ( EmailTemplates::getList( $db, $list, 'id', $condition) > 0 ) {
                    $messages->setErrorMessage("A Template with the same Name already exists.");
                }
            }

            if ( !isset($data['description']) || empty($data['description']) ) {
                $messages->setErrorMessage("Enter the Description for the Template.");
            }
            
            if ( $data['is_html'] != '0' && $data['is_html'] != '1' ) {
                $messages->setErrorMessage("Select the mode by which to Send email as.");
            }
            
            if ( !isset($data['from_name']) || empty($data['from_name']) ) {
                $messages->setErrorMessage("Enter the Name that will appear in the From field.");
            }
            if ( !isset($data['from_email']) || empty($data['from_email']) ) {
                $messages->setErrorMessage("Enter the Email that will appear in the From field.");
            }
            elseif ( !isValidEmail($data['from_email']) ) {
                $messages->setErrorMessage("The email specified in From Email is invalid.");
            }
            
            if ( !isset($data['subject']) || empty($data['subject']) ) {
                $messages->setErrorMessage("Enter the Subject for the email template.");
            }
            
            if ( !isset($data['text']) || empty($data['text']) || (strlen($data['text'])<10) ) {
                $messages->setErrorMessage("Enter the Body Text for the email template.");
            }
            
            if ( $data['status'] != EmailTemplates::PENDING && $data['status'] != EmailTemplates::ACTIVE 
                    && $data['status'] != EmailTemplates::BLOCKED && $data['status'] != EmailTemplates::DELETED ) {
                $messages->setErrorMessage("The Status of the Email Template is not valid.");
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
    }
?>
