<?php 

	class HrAttendance {
		
		
		const PRESENT  = 1;
		const ABSENT = 2;
		const PAIDLEAVE = 3;
		const NONPAIDLEAVE = 4;
		const HALFDAY = 5;
		
        const NOTMARKEDATT  = 0;
		const MARKEDATT = 1;
		const MARKOUT = 2;
        
        function getStatus() {
            /*
             0			User has not marked his attendence for today .
             1			User has marked his attendence for today .
             2			User has marked his attendence for today and 
                                            also entered leaving time i.e. not present 
                                            in Office.
           */
			$status = array(
							'NOTMARKEDATT'   => HrAttendance::NOTMARKEDATT,
                            'MARKEDATT'     => HrAttendance::MARKEDATT,
                            'MARKOUT'    => HrAttendance::MARKOUT                          
						);
			return ($status);
		}
        
        function getPresenty() {
         
            $pstatus = array(
							'PRESENT'   => HrAttendance::PRESENT,
                            'ABSENT'     => HrAttendance::ABSENT,
                            'HALF DAY'     => HrAttendance::HALFDAY,
                            'PAIDLEAVE'    => HrAttendance::PAIDLEAVE,
                            'NONPAIDLEAVE'   => HrAttendance::NONPAIDLEAVE
						);
			return ($pstatus);
         
        }
		 
        function getHrs(){
            $hrStart =0;
            $hrEnd  =  23 ;
            $lst_hr=array();
            for($j=$hrStart; $j<=$hrEnd  ;$j++){
            
                $lst_hr[$j] =strlen($j)==1 ? '0'.$j :$j;
            }  
            return ($lst_hr);
        }  
        function getMin(){
            $minStart =0;
            $minEnd  =  59;
            $lst_min=array();
            for($j=$minStart; $j<=$minEnd  ;$j++){
                $lst_min[$j] = strlen($j)==1 ? '0'.$j:$j;
            }  
            return ($lst_min);
        }  
       
		function getSec(){
            $secStart =0;
            $secEnd  =  59;
            $lst_sec=array();
            for($j=$secStart; $j<=$secEnd  ;$j++){
                $lst_sec[$j] =strlen($j)==1 ? '0'.$j:$j;
            }  
            return ($lst_sec);
        }  
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_HR_ATTENDANCE;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $module = NULL;
            if ( (HrAttendance::getList($db, $module, 'att_id', " WHERE att_id = '$id'")) > 0 ) 
			{
                $module = $module[0];
					
                    $query = "DELETE FROM ". TABLE_HR_ATTENDANCE
								." WHERE att_id = '". $id ."'";
                    
					if ( $db->query($query) || $db->affected_rows()>=0 ){
                        $messages->setOkMessage("The Record has been deleted.");
                    }else{
                        $messages->setOkMessage("The Record was not deleted.");
                    }
			}
			else
				$messages->setErrorMessage("Record not found.");
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
            if(!empty($data['date'])){
                  $data['date'] = explode('/', $data['date']);
                  $data["cmbMonth"]=$data['date'][1];
                  $data["cmbDay"]=$data['date'][0];
                  $data["cmbYear"]=$data['date'][2];
                  $dateCheck = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"];
            
            }
            /*
			if(isset($data['executive']) && !empty($data['executive'])){
                  
                  
                  $query = "SELECT DATE_FORMAT(date,'%d/%m/%Y') as date,att_time_out FROM ". TABLE_HR_ATTENDANCE 
                                           ." WHERE ".TABLE_HR_ATTENDANCE.".uid = '".$data['executive']."' 
                                           AND
                                           ".TABLE_HR_ATTENDANCE.".date < '".$dateCheck."'
                                           AND ".TABLE_HR_ATTENDANCE.".att_time_out='0000-00-00 00:00:00' AND ".TABLE_HR_ATTENDANCE.".pstatus= '".HrAttendance::PRESENT."'";
                  $db->query($query);
                  $db->next_record();
                  $timeOut = $db->f("att_time_out");
                  $attDate = $db->f("date");
                  
                  if($timeOut == "0000-00-00 00:00:00"){
                     $messages->setErrorMessage("Sorry,Time out for ".$attDate." has not been entered.");
                  } 
			}*/
            
			//validate the 
            if ( !isset($data['executive']) || empty($data['executive']) ) {
                $messages->setErrorMessage('Select Executive.');
            }
            //validate the Particulars
            if ( !isset($data['date']) || empty($data['date']) ) {
                $messages->setErrorMessage('Select Date.');
            }
            $alreadyMark=0;
            if(!empty($data['executive']) && !empty($data['date'])){
                
                $query = "SELECT  att_id FROM ". TABLE_HR_ATTENDANCE 
                                           ." WHERE ".TABLE_HR_ATTENDANCE.".uid = '".$data['executive']."' 
                                           AND
                                           ".TABLE_HR_ATTENDANCE.".date = '".$dateCheck."'";
                $db->query($query);
                if ( $db->query($query) ) {
				    if ( $db->nf() > 0 ) {
                        $alreadyMark=1;
                        $messages->setErrorMessage('Attendance Already marked for Selected Associate on Selected date.');
                    }
                }
            }
            if ( isset($data['date']) && !empty($data['date']) ) {
                $currDate = date('Ymd');
                /*
                echo 
                echo "<br/>";
                $data['date'] = explode('/', $data['date']);
                print_R();
                echo $data["cmbMonth"]=$data['date'][1];
                echo $data["cmbDay"]=$data['date'][0];
                echo $data["cmbYear"]=$data['date'][2];
                */
                $data['date'] = mktime(0, 0, 0, $data['date'][1], $data['date'][0], $data['date'][2]);                
                $compDate = $data["cmbYear"].$data["cmbMonth"].$data["cmbDay"] ;
               
                if($compDate > $currDate){
                    $messages->setErrorMessage('Selected date should not be greater than Current date.');                
                }
            }
            
           
            $data["fromdate"] = '';
            $data["todate"] = '';
            
            if($data["pstatus"]==HrAttendance::PRESENT){
            
                if(isset($data["cmbMonth"]) && isset( $data["cmbDay"]) && isset($data["cmbYear"]) ){
                    /* 
					$fromdate = mktime($data["cmbtimeinHour"],$data["cmbtimeinMin"],$data["cmbtimeinSec"],$data["cmbMonth"],$data["cmbDay"],
					$data["cmbYear"]);	 */
					$data["fromdate"]=$fromdate = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"]." ".$data["cmbtimeinHour"].":".$data["cmbtimeinMin"].":".$data["cmbtimeinSec"] ;
					
					$frmtime=$data["cmbtimeinHour"].$data["cmbtimeinMin"].$data["cmbtimeinSec"] ;
                    if($data["cmbtimeinHour"]=='00'){
                         $messages->setErrorMessage('Please select In time.');                
                    }else{
                      $data["fromdate"] = $fromdate;            
                    }
                    
                  /* 
				    $todate   = mktime($data["cmbtimeoutHour"],$data["cmbtimeoutMin"],$data["cmbtimeoutSec"],$data["cmbMonth"],$data["cmbDay"],
					$data["cmbYear"]); */
					$data["todate"]=$todate = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"]." ".$data["cmbtimeoutHour"].":".$data["cmbtimeoutMin"].":".$data["cmbtimeoutSec"] ;
					
					$tdt = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"]." ".$data["cmbtimeoutHour"].":".$data["cmbtimeoutMin"].":".$data["cmbtimeoutSec"] ;
					$totime = $data["cmbtimeoutHour"].$data["cmbtimeoutMin"].$data["cmbtimeoutSec"] ;
                    if($data["cmbtimeoutHour"]=='00'){
                         $messages->setErrorMessage('Please select Out time.');                
                    }else{
                       $data["todate"] = $todate;     
                    } 
                    
                    
                }
               
                if(!empty($data["fromdate"]) && !empty($data["todate"])){
                    if($totime <=$frmtime){
                        $messages->setErrorMessage("Sorry,Time out should be greater than Time in");
                    }
                }
                
            }
           
           
            if($data["pstatus"]!=HrAttendance::PRESENT){
                $data["fromdate"] = '';
                $data["todate"] = '';
                
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			if(!empty($data['date'])){
                  $data['date'] = explode('/', $data['date']);
                  $data["cmbMonth"]=$data['date'][1];
                  $data["cmbDay"]=$data['date'][0];
                  $data["cmbYear"]=$data['date'][2];
                  $dateCheck = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"];
            
            }
            /*
			if(isset($data['executive']) && !empty($data['executive'])){
                  
                  
                  $query = "SELECT DATE_FORMAT(date,'%d/%m/%Y') as date,att_time_out FROM ". TABLE_HR_ATTENDANCE 
                                           ." WHERE ".TABLE_HR_ATTENDANCE.".uid = '".$data['executive']."' 
                                           AND
                                           ".TABLE_HR_ATTENDANCE.".date < '".$dateCheck."'
                                           AND ".TABLE_HR_ATTENDANCE.".att_time_out='0000-00-00 00:00:00' AND ".TABLE_HR_ATTENDANCE.".pstatus= '".HrAttendance::PRESENT."'";
                  $db->query($query);
                  $db->next_record();
                  $timeOut = $db->f("att_time_out");
                  $attDate = $db->f("date");
                  
                  if($timeOut == "0000-00-00 00:00:00"){
                     $messages->setErrorMessage("Sorry,Time out for ".$attDate." has not been entered.");
                  } 
			}*/
            
			//validate the 
            if ( !isset($data['executive']) || empty($data['executive']) ) {
                $messages->setErrorMessage('Select Executive.');
            }
            //validate the Particulars
            if ( !isset($data['date']) || empty($data['date']) ) {
                $messages->setErrorMessage('Select Date.');
            }
            
            if ( isset($data['date']) && !empty($data['date']) ) {
                $currDate = date('Ymd');
                /*
                echo 
                echo "<br/>";
                $data['date'] = explode('/', $data['date']);
                print_R();
                echo $data["cmbMonth"]=$data['date'][1];
                echo $data["cmbDay"]=$data['date'][0];
                echo $data["cmbYear"]=$data['date'][2];
                */
                $data['date'] = mktime(0, 0, 0, $data['date'][1], $data['date'][0], $data['date'][2]);                
                $compDate = $data["cmbYear"].$data["cmbMonth"].$data["cmbDay"] ;
				
               
                if($compDate > $currDate){
                    $messages->setErrorMessage('Selected date should not be greater than Current date.');                
                }
            }
            
           
            $data["fromdate"] = '';
            $data["todate"] = '';
            
            if($data["pstatus"]==HrAttendance::PRESENT){
            
                if(isset($data["cmbMonth"]) && isset( $data["cmbDay"]) && isset($data["cmbYear"]) ){
                    /* 
					$fromdate = mktime($data["cmbtimeinHour"],$data["cmbtimeinMin"],$data["cmbtimeinSec"],$data["cmbMonth"],$data["cmbDay"],
					$data["cmbYear"]);	 */
					$data["fromdate"]=$fromdate = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"]." ".$data["cmbtimeinHour"].":".$data["cmbtimeinMin"].":".$data["cmbtimeinSec"] ;
					
					$frmtime=$data["cmbtimeinHour"].$data["cmbtimeinMin"].$data["cmbtimeinSec"] ;
                    if($data["cmbtimeinHour"]=='00'){
                         $messages->setErrorMessage('Please select In time.');                
                    }else{
                      $data["fromdate"] = $fromdate;            
                    }
                    
                  /* 
				    $todate   = mktime($data["cmbtimeoutHour"],$data["cmbtimeoutMin"],$data["cmbtimeoutSec"],$data["cmbMonth"],$data["cmbDay"],
					$data["cmbYear"]); */
					$data["todate"]=$todate = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"]." ".$data["cmbtimeoutHour"].":".$data["cmbtimeoutMin"].":".$data["cmbtimeoutSec"] ;
					
					$tdt = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"]." ".$data["cmbtimeoutHour"].":".$data["cmbtimeoutMin"].":".$data["cmbtimeoutSec"] ;
					$totime = $data["cmbtimeoutHour"].$data["cmbtimeoutMin"].$data["cmbtimeoutSec"] ;
                    if($data["cmbtimeoutHour"]=='00'){
                         $messages->setErrorMessage('Please select Out time.');                
                    }else{
                       $data["todate"] = $todate;     
                    } 
                    
                    
                }
               
                if(!empty($data["fromdate"]) && !empty($data["todate"])){
                    if($totime <=$frmtime){
                        $messages->setErrorMessage("Sorry,Time out should be greater than Time in");
                    }
                }
                
            }
               
            if($data["pstatus"]!= HrAttendance::PRESENT){
                $data["fromdate"] = '';
                $data["todate"] = '';
                
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
       
        
        
       
        
    }
?>