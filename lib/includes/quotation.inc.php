<?php
	class Quotation {
    
		const PENDING  = 0;
		const ACTIVE  = 1;		
        const REJECTED = 2;
		const APPROVED = 3;
		const COMPLETED = 4;
        
		function getStatus() {
			$status = array('PENDING' => Quotation::PENDING,
                            'ACTIVE'   => Quotation::ACTIVE,
                            'REJECTED' => Quotation::REJECTED,
							'APPROVED' => Quotation::APPROVED,
							'COMPLETED' => Quotation::COMPLETED
						);
			return ($status);
		}
        
        function getValidity() { 
			$vFrom =6 ;
            $vTo =15 ;
            for($i=$vFrom ; $i<$vTo ;$i++){
                $validity[$i] = $i;
            }
			return ($validity);
		}
        
        function getValidityType() {
			$validityType = array('Days' => 'Days',
							'Months'   => 'Months'
						);
            return ($validityType);
		}
        
        
		/**
		 *	Function to get all the PreQuotes.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			//$query .= " FROM ". TABLE_SALE_QUOTATION;
            $query .= " FROM ". TABLE_SALE_QUOTATION;
            $query .= " LEFT JOIN ". TABLE_SALE_LEADS
                        ." ON ". TABLE_SALE_LEADS .".lead_id = ". TABLE_SALE_QUOTATION .".lead_id ";
            $query .= " LEFT JOIN ". TABLE_SETTINGS_QUOT_SUBJECT
                        ." ON ". TABLE_SETTINGS_QUOT_SUBJECT .".id = ". TABLE_SALE_QUOTATION .".subject ";
                        
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        function allRejectedQuotation(&$db,$parentQid, &$list){
                
                $condition = " WHERE ".TABLE_SALE_QUOTATION.".id ='$parentQid' ";
                
				$fields= TABLE_SALE_QUOTATION.".id,".TABLE_SALE_QUOTATION.".number,".TABLE_SALE_QUOTATION.".status,".TABLE_SALE_QUOTATION.".parent_q_id ";
				Quotation::getList($db, $qlist, $fields, $condition);
                
                if(!empty($qlist)){
                    $qlist = $qlist[0];
                    $id = $qlist['id'];
                    $number = $qlist['number'];
                    $parent_q_id = $qlist['parent_q_id'];
                    $list[$id] = $number ;
                  
                    if($parent_q_id !=0 ){                    
                        Quotation::allRejectedQuotation($db,$parent_q_id, $list);                        
                    }
                }
               
               
        
        }
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
           
            $query .= " FROM ". TABLE_SALE_QUOTATION;
            $query .= " LEFT JOIN ". TABLE_SALE_LEADS
                        ." ON ". TABLE_SALE_LEADS .".lead_id = ". TABLE_SALE_QUOTATION .".lead_id ";
            $query .= " LEFT JOIN ". TABLE_SETTINGS_QUOT_SUBJECT
                        ." ON ". TABLE_SETTINGS_QUOT_SUBJECT .".id = ". TABLE_SALE_QUOTATION .".subject ";
                        
           /* $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_SALE_QUOTATION .".client ";
                        
            $query .= " LEFT JOIN ". TABLE_BILL_INV
                        ." ON ". TABLE_BILL_INV .".or_no = ". TABLE_SALE_QUOTATION .".number ";*/
                        
             $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       function getLevelQuote(&$db, &$catg_list, $group_id=0, $level=0 ) {
       
             $temp = NULL;
            
			//if ( !empty($placement) ) {
				$condition = "WHERE ".TABLE_SALE_QUOTATION.".parent_q_id ='$group_id' ";
                $condition .= " ORDER BY id";
			
                
                 $fields = TABLE_SALE_QUOTATION .'.id'
                    .','. TABLE_SALE_QUOTATION .'.number'
					//.','. TABLE_SALE_QUOTATION .'.subject'
                    .','. TABLE_SALE_QUOTATION .'.access_level'
                    .','. TABLE_SALE_QUOTATION .'.parent_q_id'
                    .','. TABLE_SALE_QUOTATION .'.do_e'
                    .','. TABLE_SALE_QUOTATION .'.status'
                    .','. TABLE_SETTINGS_QUOT_SUBJECT .'.title as subject'
                    .','. TABLE_SALE_LEADS .'.lead_id' 
                    .','. TABLE_SALE_LEADS .'.lead_number' 
                    .','. TABLE_SALE_LEADS .'.f_name' 
                    .','. TABLE_SALE_LEADS .'.m_name' 
                    .','. TABLE_SALE_LEADS .'.l_name' 
                    .','. TABLE_SALE_LEADS .'.org' ;
                    
				Quotation::getList($db, $catg_list, $fields, $condition);
                if (is_array($catg_list))
                {
                    foreach ($catg_list as $id => $featVal)
                    {
                        $featId = $featVal['id'];
                        $catgList[$featId] = $featVal;
                    }
                    $catg_list = $catgList;
                }

				if ( is_array($catg_list) && count($catg_list)>0 ) 
                {
					foreach ($catg_list as $key=>$catg) 
                    {
						$temp = NULL;
						if ( $catg['parent_q_id'] == 0 ) 
                        {	
							$level = 0 ;
						}
						Quotation::getLevelQuote($db, $temp, $catg['id'], ($level+1));

                        $tempList = NULL;
                        if (is_array($temp))
                        {
                            foreach ($temp as $id => $tempVal)
                            {
                                $subId = $tempVal['id'];
                                $tempList[$subId] = $tempVal;
                            }
                        }
  
						$catg_list[$key]['level'] = $level;
						$catg_list[$key]['sub_menu'] = $tempList;
						
                        
                        //$catg_list[$key]['url_title'] = str_replace(' ', '-', strtolower($catg_list[$key]['title'])) .'.html';
                        //$catg_list[$key]['url_display'] = SOCCER_FILE."/category/".$catg_list[$key]['id']."/".str_replace(' ', '-', strtolower($catg_list[$key]['title']))."/".$catg_list[$key]['url_title'];
					}
				}
		
			if ( is_array($catg_list) && count($catg_list)>0 ) 
            {
				return true;
			}
			else 
            {
				return false;
			}
       
       
       }
       
       function createSublevel(&$catg_list,&$sideMenu,$cat_id,$parentArr,$uptolevel,$variables){
            $db = new db_local;       
            
        if(!empty($catg_list)){ 
            foreach($catg_list as $key=> $value){            
                $lead_id = $value['lead_id'];
                $number = $value['number'];
                $subject = $value['subject'];
                $status = $value['status'];
                $parent_id = $value['parent_q_id'];
                $category_id = $value['id'];
                $level = $value['level']; 
                $sub_menu = $value['sub_menu'];
                /*
                $title = $value['title'];
                $parent_id = $value['parent_id'];
                $status = $value['status'];
                $category_id = $value['id'];
                $sub_menu = $value['sub_menu'];
                $level = $value['level'];
                $url_str=$category_id."/".str_replace(' ', '-', strtolower($title))."/".$value['url_title'] ;
                */
                
                if($uptolevel!=''){
                    if($level <= $uptolevel){
                    
                        if($parent_id==0){
                          
                            $statusStr ='';
                            
                            $link = $variables['nc']."/sale-quotation.php?perform=view&q_id=".$value['id'] ;
                            
                            $statusStr = "&nbsp;<a href='javascript:void(0);'
                                onclick=\"window.open('".$link."','mywin','left=20,top=20,width=600,height=400,toolbar=0,resizable=1,scrollbars=1')\";' class='action' title=''>
                                <img src='".$variables['nc_images']."/view-on.gif' border='0' title='View' name='View' alt='View' /></a>" ;
                                            
                            
                            if($status == $variables['active'] || $status == $variables['rejected'] || $status == $variables['completed']){
                            
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/active-off.gif border='0' title='Active' name='Active' alt='Active'/>" ;
                            
                            }else{
                                $statusStr .="&nbsp;
                                    <a href=".$variables['nc']."/sale-quotation.php?perform=change_status&status=".$variables['active']."&q_id=".$value['id']."
                                    class='action' title='Active'><img src=".$variables['nc_images']."/active-off.gif border='0' title='Active' name='Active' alt='Active'/></a>" ;
                            
                            }
                                                      
                           
                            if($status == $variables['active'] || $status == $variables['rejected'] || $status == $variables['completed']){
                            
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/block-off.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/>" ;
                            
                            }else{
                                $statusStr .="&nbsp;
                                    <a href=".$variables['nc']."/sale-quotation.php?perform=change_status&status=".$variables['rejected']."&q_id=".$value['id']."
                                    class='action' title='Rejected'><img src=".$variables['nc_images']."/block-on.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/></a>" ;
                            
                            }
                            $createcopy=false; 
                            
                            //Check status 
                            if(!empty($sub_menu)){                                 
                                 $createcopy =    false;   
                            }else{                            
                                $table ='';
                                if( $status==$variables['rejected']){
                                  $createcopy =    true;   
                                }
                            }
                            
                            if($createcopy){
                                $statusStr .="&nbsp;
                                 <a href=".$variables['nc']."/sale-quotation.php?perform=copy&q_id=".$value['id']."
                                    class='action' title='Click to Create Copy of Quotation'><img src=".$variables['nc_images']."/completed-on.gif border='0' title='Click to Create Copy of Quotation' name='copy_quot' alt='Click to Create Copy of Quotation'/></a>" ;
                            
                            }else{
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/completed-off.gif border='0' title='completed' name='completed' alt='completed'/>" ;
                            }
                            
                            $iSts = '';
                            if($status==$variables['rejected'] ){
                               $iSts ="&nbsp;<img src=".$variables['nc_images']."/block-on.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/>" ;
                            
                            }elseif($status==$variables['active']){
                            
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/active-on.gif border='0' title='Active' name='Active' alt='Active'/>" ;
                            
                            }elseif($status==$variables['pending']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/pending-on.gif border='0' title='Pending' name='Pending' alt='Pending'/>" ;
                            
                            }elseif($status==$variables['deleted']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/delete-on.gif border='0' title='Deleted' name='Deleted' alt='Deleted'/>" ;
                            
                            }elseif($status==$variables['completed']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/completed-on.gif border='0' title='Completed' name='Completed' alt='Completed'/>" ;
                            }
                            
                            
                            $sideMenu.= "<tr class=normaltext><td>".$iSts."&nbsp;".str_repeat("->",$level).$number."</td>
                             <td>&nbsp;</td>
                             <td>".$subject."</td>
                             <td>".$statusStr."</td>
                            </tr>"; 
                        }elseif( $parent_id > 0 ){                           
                            
                             $statusStr ='';
                            
                            $link = $variables['nc']."/sale-quotation.php?perform=view&q_id=".$value['id'] ;
                            
                            $statusStr = "&nbsp;<a href='javascript:void(0);'
                                onclick=\"window.open('".$link."','mywin','left=20,top=20,width=600,height=400,toolbar=0,resizable=1,scrollbars=1')\";' class='action' title=''>
                                <img src='".$variables['nc_images']."/view-on.gif' border='0' title='View' name='View' alt='View' /></a>" ;
                                            
                            
                            if($status == $variables['active'] || $status == $variables['rejected'] || $status == $variables['completed']){
                            
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/active-off.gif border='0' title='Active' name='Active' alt='Active'/>" ;
                            
                            }else{
                                $statusStr .="&nbsp;
                                    <a href=".$variables['nc']."/sale-quotation.php?perform=change_status&status=".$variables['active']."&q_id=".$value['id']."
                                    class='action' title='Active'><img src=".$variables['nc_images']."/active-off.gif border='0' title='Active' name='Active' alt='Active'/></a>" ;
                            
                            }
                                                      
                           
                            if($status == $variables['active'] || $status == $variables['rejected'] || $status == $variables['completed']){
                            
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/block-off.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/>" ;
                            
                            }else{
                                $statusStr .="&nbsp;
                                    <a href=".$variables['nc']."/sale-quotation.php?perform=change_status&status=".$variables['rejected']."&q_id=".$value['id']."
                                    class='action' title='Rejected'><img src=".$variables['nc_images']."/block-on.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/></a>" ;
                            
                            }
                            $createcopy=false; 
                            
                            //Check status 
                            if(!empty($sub_menu)){                                 
                                 $createcopy =    false;   
                            }else{                            
                                $table ='';
                                if( $status==$variables['rejected']){
                                  $createcopy =    true;   
                                }
                            }
                            
                            if($createcopy){
                                $statusStr .="&nbsp;
                                 <a href=".$variables['nc']."/sale-quotation.php?perform=copy&q_id=".$value['id']."
                                    class='action' title='Click to Create Copy of Quotation'><img src=".$variables['nc_images']."/copy-on.gif border='0' title='Click to Create Copy of Quotation' name='copy_quot' alt='Click to Create Copy of Quotation'/></a>" ;
                            
                            }else{
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/copy-off.gif border='0' title='completed' name='completed' alt='completed'/>" ;
                            }
                            //write code
                           /*if($variables['can_view_file']){*/
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:showQuotation('".$value['id']."', 'pdf')\" 
                                    class='action' title='View Quotation'><img src=".$variables['nc_images']."/pdf.jpg border='0' title='View PDF' name='view_pdf' alt='View PDF'/></a>" ;
                                
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:showQuotation('".$value['id']."', 'html')\" 
                                    class='action' title='View Quotation'><img src=".$variables['nc_images']."/html.gif border='0' title='View HTML' name='view_html' alt='View HTML'/></a>" ;

                           /*}*/
                            
                            
                            $iSts = '';
                            if($status==$variables['rejected'] ){
                               $iSts ="&nbsp;<img src=".$variables['nc_images']."/block-on.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/>" ;
                            
                            }elseif($status==$variables['active']){
                            
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/active-on.gif border='0' title='Active' name='Active' alt='Active'/>" ;
                            
                            }elseif($status==$variables['pending']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/pending-on.gif border='0' title='Pending' name='Pending' alt='Pending'/>" ;
                            
                            }elseif($status==$variables['deleted']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/delete-on.gif border='0' title='Deleted' name='Deleted' alt='Deleted'/>" ;
                            
                            }elseif($status==$variables['completed']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/completed-on.gif border='0' title='Completed' name='Completed' alt='Completed'/>" ;
                            }
                            
                            
                            $sideMenu.= "<tr class=normaltext><td>".$iSts."&nbsp;".str_repeat("->",$level).$number."</td>
                             <td>&nbsp;</td>
                             <td>".$subject."</td>
                             <td>".$statusStr."</td>
                            </tr>"; 
                        }
                        
                    }
                }else{
                       if($parent_id==0){                        
                           $statusStr ='';
                            
                            $link = $variables['nc']."/sale-quotation.php?perform=view&q_id=".$value['id'] ;
                            
                            $statusStr = "&nbsp;<a href='javascript:void(0);'
                                onclick=\"window.open('".$link."','mywin','left=20,top=20,width=600,height=400,toolbar=0,resizable=1,scrollbars=1')\";' class='action' title=''>
                                <img src='".$variables['nc_images']."/view-on.gif' border='0' title='View' name='View' alt='View' /></a>" ;
                                            
                            
                            if($status == $variables['active'] || $status == $variables['rejected'] || $status == $variables['completed']){
                            
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/active-off.gif border='0' title='Active' name='Active' alt='Active'/>" ;
                            
                            }else{
                                $statusStr .="&nbsp;
                                    <a href=".$variables['nc']."/sale-quotation.php?perform=change_status&status=".$variables['active']."&q_id=".$value['id']."
                                    class='action' title='Active'><img src=".$variables['nc_images']."/active-on.gif border='0' title='Active' name='Active' alt='Active'/></a>" ;
                            
                            }
                                                      
                           
                            if($status == $variables['active'] || $status == $variables['rejected'] || $status == $variables['completed']){
                            
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/block-off.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/>" ;
                            
                            }else{
                                $statusStr .="&nbsp;
                                    <a href=".$variables['nc']."/sale-quotation.php?perform=change_status&status=".$variables['rejected']."&q_id=".$value['id']."
                                    class='action' title='Rejected'><img src=".$variables['nc_images']."/block-on.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/></a>" ;
                            
                            }
                            $createcopy=false; 
                            
                            //Check status 
                            if(!empty($sub_menu)){                                 
                                 $createcopy =    false;   
                            }else{                            
                                $table ='';
                                if( $status==$variables['rejected']){
                                  $createcopy =    true;   
                                }
                            }
                            
                            if($createcopy){
                                $statusStr .="&nbsp;
                                 <a href=".$variables['nc']."/sale-quotation.php?perform=copy&q_id=".$value['id']."
                                    class='action' title='Click to Create Copy of Quotation'><img src=".$variables['nc_images']."/copy-on.gif border='0' title='Click to Create Copy of Quotation' name='copy_quot' alt='Click to Create Copy of Quotation'/></a>" ;
                            
                            }else{
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/copy-off.gif border='0' title='Create Copy of Quotation' name='Create Copy of Quotation' alt='Create Copy of Quotation'/>" ;
                            }
                            
                            //write code
                            if($variables['can_view_file']){
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:showQuotation('".$value['id']."', 'pdf')\" 
                                    class='action' title='View Quotation'><img src=".$variables['nc_images']."/pdf.jpg border='0' title='View PDF' name='view_pdf' alt='View PDF'/></a>" ;
                                    
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:showQuotation('".$value['id']."', 'html')\" 
                                    class='action' title='View Quotation'><img src=".$variables['nc_images']."/html.gif border='0' title='View HTML' name='view_html' alt='View HTML'/></a>" ;                                    
                            }
                            if($variables['can_send_file']){
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:sendQuotation('".$value['id']."', 'pdf')\" 
                                    class='action' title='Send Quotation'><img src=".$variables['nc_images']."/pdf.jpg border='0' title='Send PDF' name='send_pdf' alt='Send PDF'/></a>" ;                                        
                                    
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:sendQuotation('".$value['id']."', 'html')\" 
                                    class='action' title='Send Quotation'><img src=".$variables['nc_images']."/html.gif border='0' title='Send HTML' name='send_html' alt='Send HTML'/></a>" ;                                        
                            }
                            
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:sendQuotation('".$value['id']."', 'html')\" 
                                    class='action' title='Click to Send Invoice' name='send_invoice' alt='Click to Send Invoice'><img src=".$variables['nc_images']."/sendmail.gif border='0' alt='Click to Send Invoice'/></a>" ; 
                            
                            $iSts = '';
                            if($status==$variables['rejected'] ){
                               $iSts ="&nbsp;<img src=".$variables['nc_images']."/block-on.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/>" ;
                            
                            }elseif($status==$variables['active']){
                            
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/active-on.gif border='0' title='Active' name='Active' alt='Active'/>" ;
                            
                            }elseif($status==$variables['pending']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/pending-on.gif border='0' title='Pending' name='Pending' alt='Pending'/>" ;
                            
                            }elseif($status==$variables['deleted']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/delete-on.gif border='0' title='Deleted' name='Deleted' alt='Deleted'/>" ;
                            
                            }elseif($status==$variables['completed']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/completed-on.gif border='0' title='Completed' name='Completed' alt='Completed'/>" ;
                            }
                            
                            
                            $sideMenu.= "<tr class=normaltext><td>".$iSts."&nbsp;".str_repeat("->",$level).$number."</td>
                             <td>&nbsp;</td>
                             <td>".$subject."</td>
                             <td>".$statusStr."</td>
                            </tr>"; 
                        }elseif( $parent_id > 0 ){ 
                            $statusStr ='';
                            
                            $link = $variables['nc']."/sale-quotation.php?perform=view&q_id=".$value['id'] ;
                            
                            $statusStr = "&nbsp;<a href='javascript:void(0);'
                                onclick=\"window.open('".$link."','mywin','left=20,top=20,width=600,height=400,toolbar=0,resizable=1,scrollbars=1')\";' class='action' title=''>
                                <img src='".$variables['nc_images']."/view-on.gif' border='0' title='View' name='View' alt='View' /></a>" ;
                                            
                            
                            if($status == $variables['active'] || $status == $variables['rejected'] || $status == $variables['completed']){
                            
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/active-off.gif border='0' title='Active' name='Active' alt='Active'/>" ;
                            
                            }else{
                                $statusStr .="&nbsp;
                                    <a href=".$variables['nc']."/sale-quotation.php?perform=change_status&status=".$variables['active']."&q_id=".$value['id']."
                                    class='action' title='Active'><img src=".$variables['nc_images']."/active-on.gif border='0' title='Active' name='Active' alt='Active'/></a>" ;
                            
                            }
                                                      
                           
                            if($status == $variables['active'] || $status == $variables['rejected'] || $status == $variables['completed']){
                            
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/block-off.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/>" ;
                            
                            }else{
                                $statusStr .="&nbsp;
                                    <a href=".$variables['nc']."/sale-quotation.php?perform=change_status&status=".$variables['rejected']."&q_id=".$value['id']."
                                    class='action' title='Rejected'><img src=".$variables['nc_images']."/block-on.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/></a>" ;
                            
                            }
                            $createcopy=false; 
                            
                            //Check status 
                            if(!empty($sub_menu)){                                 
                                 $createcopy =    false;   
                            }else{                            
                                $table ='';
                                if( $status==$variables['rejected']){
                                  $createcopy =    true;   
                                }
                            }
                            
                            
                            if($createcopy){
                                $statusStr .="&nbsp;
                                 <a href=".$variables['nc']."/sale-quotation.php?perform=copy&q_id=".$value['id']."
                                    class='action' title='Click to Create Copy of Quotation'><img src=".$variables['nc_images']."/copy-on.gif border='0' title='Click to Create Copy of Quotation' name='copy_quot' alt='Click to Create Copy of Quotation'/></a>" ;
                            
                            }else{
                                $statusStr .="&nbsp;<img src=".$variables['nc_images']."/copy-off.gif border='0' title='Create Copy of Quotation' name='copy_quot' alt='Create Copy of Quotation'/>" ;
                            }
                            
                            //write code
                            if($variables['can_view_file']){
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:showQuotation('".$value['id']."', 'pdf')\" 
                                    class='action' title='View Quotation'><img src=".$variables['nc_images']."/pdf.jpg border='0' title='View PDF' name='view_pdf' alt='View PDF'/></a>" ;
                                    
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:showQuotation('".$value['id']."', 'html')\" 
                                    class='action' title='View Quotation'><img src=".$variables['nc_images']."/html.gif border='0' title='View HTML' name='view_html' alt='View HTML'/></a>" ;                                    
                            }
                            if($variables['can_send_file']){
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:sendQuotation('".$value['id']."', 'pdf')\" 
                                    class='action' title='Send Quotation'><img src=".$variables['nc_images']."/pdf.jpg border='0' title='Send PDF' name='send_pdf' alt='Send PDF'/></a>" ;                                        
                                    
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:sendQuotation('".$value['id']."', 'html')\" 
                                    class='action' title='Send Quotation'><img src=".$variables['nc_images']."/html.gif border='0' title='Send HTML' name='send_html' alt='Send HTML'/></a>" ;                                        
                            }
                            
                                $statusStr .="&nbsp;
                                 <a href='javascript:void(0);' onclick=\"javascript:sendQuotation('".$value['id']."', 'html')\" 
                                    class='action' title='Click to Send Invoice' name='send_invoice' alt='Click to Send Invoice'><img src=".$variables['nc_images']."/sendmail.gif border='0' alt='Click to Send Invoice'/></a>" ;                                                                        
                            
                            
                            
                            $iSts = '';
                            if($status==$variables['rejected'] ){
                               $iSts ="&nbsp;<img src=".$variables['nc_images']."/block-on.gif border='0' title='Rejected' name='Rejected' alt='Rejected'/>" ;
                            
                            }elseif($status==$variables['active']){
                            
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/active-on.gif border='0' title='Active' name='Active' alt='Active'/>" ;
                            
                            }elseif($status==$variables['pending']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/pending-on.gif border='0' title='Pending' name='Pending' alt='Pending'/>" ;
                            
                            }elseif($status==$variables['deleted']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/delete-on.gif border='0' title='Deleted' name='Deleted' alt='Deleted'/>" ;
                            
                            }elseif($status==$variables['completed']){
                              $iSts ="&nbsp;<img src=".$variables['nc_images']."/completed-on.gif border='0' title='Completed' name='Completed' alt='Completed'/>" ;
                            }
                            
                            
                            $sideMenu.= "<tr class=normaltext><td>".$iSts."&nbsp;".str_repeat("->",$level).$number."</td>
                             <td>&nbsp;</td>
                             <td>".$subject."</td>
                             <td>".$statusStr."</td>
                            </tr>"; 
                        }
                
                }
                if(!empty($sub_menu)){
                        Quotation::createSublevel($sub_menu,$sideMenu,$category_id,$parentArr,$uptolevel,$variables);
                }
               
            }
        }
    }
	   
        /*function getDashboard( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }
            
            $query .= " FROM ". TABLE_SALE_QUOTATION;
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_SALE_QUOTATION .".client ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
					
					if ( strpos($required_fields, 'team') > 0 ) {
						include_once ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$count = count($list);
						for ( $i=0; $i<$count; $i++ ) {
							// Read the Team Member Details.
							$list[$i]['team'] = "'". implode("','", (explode(',', $list[$i]['team']))) ."'";
							$list[$i]['team_members']= '';
                            
							User::getList($db, $list[$i]['team_members'], 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $list[$i]['team'] .")");
							
							// Read the latest Time Line update.
							include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
							$list[$i]['work'] = NULL;
							//WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, order_title, to_id, by_id, do_assign, comments, status');
							$fields = TABLE_WORK_TL.".id ,".TABLE_WORK_TL.".to_id, ".TABLE_WORK_TL.".by_id, ".TABLE_WORK_TL.".do_assign, ".TABLE_WORK_TL.".comments,".TABLE_WORK_TL.".status";
                            WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], $fields);
							$list[$i]['work'] 		= $list[$i]['work'][0];
							//$list[$i]['order_title']= $list[$i]['work']['order_title'];
							$list[$i]['comments'] 	= $list[$i]['work']['comments'];
                            
							User::getList($db, $list[$i]['work']['by'], 'user_id, number, f_name, l_name, email', "WHERE user_id = '". $list[$i]['work']['by_id'] ."'");
							$list[$i]['work']['by'] = $list[$i]['work']['by'][0];
						}
					}
                }
                
                return ( $db->nf() );
            }
            else {
                return false;
            }   
        }*/
	   
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( !in_array($status_new, Quotation::getStatus()) ) {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
            else {
                $order = NULL;
                $fields = TABLE_SALE_QUOTATION.".id, ".TABLE_SALE_QUOTATION.".lead_id, ".TABLE_SALE_QUOTATION.".access_level,".TABLE_SALE_QUOTATION.".status" ;
                if ( (Quotation::getList($db, $order, $fields, " WHERE ".TABLE_SALE_QUOTATION.".id = '$id' OR ".TABLE_SALE_QUOTATION.".number='$id'")) > 0 ) {
                    $order = $order[0];
                        
                        //if ( $order['access_level'] < $access_level ) {
                            $query = "UPDATE ". TABLE_SALE_QUOTATION
                                       ." SET status = '$status_new' "
                                       ." WHERE id = '".$order['id']."'";
                           if ( $db->query($query) && $db->affected_rows()>0 ) {
                               $messages->setOkMessage("The Status has been updated.");
                           }
                          else {
                               $messages->setErrorMessage("The Status was not updated.");
                           }
                       /*}
                       else {
                           $messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
                       }*/
                    
                    /*
                    if ( $order['status'] != Quotation::ACTIVE ) {
                        if ( $order['access_level'] < $access_level ) {
                             $query = "UPDATE ". TABLE_SALE_QUOTATION
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("Status cannot be changed: Quotation has been Approved.");
                    }
                    */
                    
                }
                else {
                    $messages->setErrorMessage("The  Quote was not found.");
                }
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        
        //This function is used to perform action on status.
		function markStatusAcHead($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( !in_array($status_new, Quotation::getStatus()) ) {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
            else {
                $order = NULL;
                $query = "UPDATE ". TABLE_SALE_QUOTATION
                                        ." SET is_apr_ac_head  	 = '$status_new', "
                                        ."  ac_head_uid = '".$my['uid']."' "
                                        ." WHERE id = '$id'"; 
                if ( $db->query($query) && $db->affected_rows()>0 ) {
                    $messages->setOkMessage("The Status has been updated.");
                }
                else {
                    $messages->setErrorMessage("The Status was not updated.");
                }
                /*
                if ( (Quotation::getList($db, $order, 'id, access_level, status', " WHERE id = '$id'")) > 0 ) {
                    $order = $order[0];
                    if ( $order['status'] != Quotation::APPROVED ) {
                        if ( $order['access_level'] < $access_level ) {
                            $query = "UPDATE ". TABLE_SALE_QUOTATION
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("Status cannot be changed: Order has been Approved.");
                    }
                }
                else {
                    $messages->setErrorMessage("The  Quote was not found.");
                }
                */
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        
        
        
        
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                 $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SALE_QUOTATION_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
    
        /*function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $order = NULL;
            if ( (PreQuote::getList($db, $order, 'id, number,access_level, status', " WHERE id = '$id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != PreQuote::COMPLETED ) {
                    if ( $order['status'] == PreQuote::BLOCKED ) {
                        if ( $order['access_level'] < $access_level ) {
                        
                            if((Invoice::getList($db, $invoice, 'id', " WHERE or_no = '".$order['number']."'") ) > 0){
                             
                                $messages->setErrorMessage("Please delete Invoice of order ".$order['number']."  ");
                             
                            }else{
                            
                                $query = "DELETE FROM ". TABLE_SALE_QUOTATION
                                            ." WHERE id = '$id'";
                                if ( $db->query($query) && $db->affected_rows()>0 ) {
                                
                                    //Delete worktimeline for selected order bof
                                    $query = "DELETE FROM ". TABLE_WORK_TL
                                            ." WHERE order_no = '".$order['number']."' ";
                                    $db->query($query);
                                    //Delete worktimeline for selected order eof
                                    
                                
                                    $messages->setOkMessage("The Order has been deleted.");
                                    
                                    
                                    $query = "DELETE FROM ". TABLE_BILL_ORD_P
                                            ." WHERE ord_no = '".$order['number']."' ";
                                    $db->query($query);
                                    
                                    
                                }
                                else {
                                    $messages->setErrorMessage("The Order was not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Order with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("To Delete Order it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Order cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }*/
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {

			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
            if ( empty($data['lead_id']) ) {
                $messages->setErrorMessage('Select the Lead / Client Against whome You are creating the Quotation.');
            }
            
            if ( empty($data['company_id']) ) {
                $messages->setErrorMessage('Select the Company.');
            }
          
            // Check for the duplicate Quotation Number.
            
            $data['number'] = getCounterNumber($db,'QT',$data['company_id']);
            
            $list = NULL;
            $fields = TABLE_SALE_QUOTATION.".id";
            $conditionQt = " WHERE ".TABLE_SALE_QUOTATION.".number = '". $data['number'] ."'" ;
            if ( Quotation::getList($db, $list, $fields, $conditionQt)>0 ) {
                $messages->setErrorMessage(" Quotation with the same Number already exists.<br/>Please try again.");
                //$messages->setErrorMessage("An Order with the same Number already exists.");
            }
            $list = NULL;
            // validate particulars bof
            
            if ( empty($data['subject']) ) {
                $messages->setErrorMessage('Subject Should not be empty.');
            }
            if ( empty($data['lead_id']) ) {
                $messages->setErrorMessage('Please Choose lead.');
            }
            if ( empty($data['validity_no']) ) {
                $messages->setErrorMessage('Validity Should not be empty.');
            }
            
            if ( !isset($data['terms']) && empty($data['terms'])  ) {
                $messages->setErrorMessage('Select the term.');
            }
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }
            
            $data['query_p'] = 'INSERT INTO '. TABLE_SALE_QUOTATION_P .' (q_no, particulars, p_amount, s_id,sub_s_id, s_type, s_quantity, s_amount, ss_title, ss_punch_line, tax1_name,tax1_value, tax1_pvalue,d_amount,stot_amount,tot_amount,discount_type,is_renewable ) VALUES ';
            $is_part = false;
            if(isset($data['particulars'])){
                foreach ( $data['particulars'] as $key=>$particular ) {
                    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
                                if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
                                    $messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
                                   
                                }elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){
                                
                                    $messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');
                                }else{
                                    $is_part = true;
                                        $data['query_p'] .= "('". $data['number'] ."', '". processUserData(trim($data['particulars'][$key],",")) ."', '". $data['p_amount'][$key] ."','". $data['s_id'][$key] ."','".$data['sub_s_id'][$key] ."',
                                        '". processUserData($data['s_type'][$key]) ."', '". processUserData($data['s_quantity'][$key]) ."','". processUserData($data['s_amount'][$key]) ."',
                                        '". processUserData($data['ss_title'][$key]) ."', '". processUserData($data['ss_punch_line'][$key]) ."',
                                        '". processUserData($data['tax1_name'][$key]) ."', '". processUserData($data['tax1_value'][$key]) ."','". processUserData($data['tax1_pvalue'][$key]) ."',
                                        '". processUserData($data['d_amount'][$key]) ."', '". processUserData($data['stot_amount'][$key]) ."',
                                        '". processUserData($data['tot_amount'][$key]) ."',
                                        '". processUserData($data['discount_type'][$key]) ."',   
                                        '". processUserData($data['is_renewable'][$key]) ."'                                     
                                        )," ;
                                   
                                   
                                   /*
                                    if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || empty($data['do_fe']) ) ){
                                        $messages->setErrorMessage('Service '. $data['ss_title'][$key] .' is Renewal Service. Please specify Service From Date &&  Renewal Date');

                                    }else{
                                            $is_part = true;
                                            $data['query_p'] .= "('". $data['number'] ."', '". processUserData(trim($data['particulars'][$key],",")) ."', '". $data['p_amount'][$key] ."','". $data['s_id'][$key] ."','".$data['sub_s_id'][$key] ."',
                                            '". processUserData($data['s_type'][$key]) ."', '". processUserData($data['s_quantity'][$key]) ."','". processUserData($data['s_amount'][$key]) ."',
                                            '". processUserData($data['ss_title'][$key]) ."', '". processUserData($data['ss_punch_line'][$key]) ."',
                                            '". processUserData($data['tax1_name'][$key]) ."', '". processUserData($data['tax1_value'][$key]) ."','". processUserData($data['tax1_pvalue'][$key]) ."',
                                            '". processUserData($data['d_amount'][$key]) ."', '". processUserData($data['stot_amount'][$key]) ."',
                                            '". processUserData($data['tot_amount'][$key]) ."',
                                            '". processUserData($data['discount_type'][$key]) ."'                            
                                            )," ;
                                    }*/
                                }
                            }
                        }
                    }
                }    
            }
            
            if ( !$is_part ) {
                $messages->setErrorMessage('The Particular for the Quotation cannot be empty.');
            }
            else {
                $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
            }

            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Total Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                $messages->setErrorMessage("Amount in words is not specified.");
            }else{
                if(is_numeric($data['amount_words'])){  
                     $messages->setErrorMessage("Amount in words Should be Characters  only.");
                    
                }
            
            }
            // Validate particulars eof
            
            // Check for start date
            /*if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }*/
            // Check for estimated End date
            /*if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }*/
            //Validate dates ie start date > est end date            
            /*if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }*/
            
            //Check Estimated hrs should not be characters
            /*if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }*/
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // First check if the Order exists or not?
            $list = NULL;
            if ( PreQuote::getList($db, $list, 'id, created_by', " WHERE id = '". $data['pq_id'] ."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                $list = NULL;
                
                // Validate the Pre Order.
                /*
                $list = NULL;
                if ( Order::getList($db, $list, 'id', " WHERE po_id = '". $data['po_id'] ."'")<=0 ) {
                    $messages->setErrorMessage("The Pre Order has not been processed.");
                }
                else {
                    if ( isset($data['po_id']) && !empty($data['po_id']) ) {
                        $query = "SELECT access_level, status FROM ". TABLE_BILL_PO
                                ." WHERE id = '". $data['po_id'] ."'";
                        if ( $db->query($query) && $db->nf() <= 0 ) {
                            $messages->setErrorMessage("The Pre Order was not found in the database.");
                        }
                        else {
                            $db->next_record();
                            if ( $access_level > $db->f('access_level') ) {
                                if ( $db->f('status') != COMPLETED ) {
                                    $messages->setErrorMessage("There is anomaly in the Status of the Pre Order.");
                                }
                            }
                            else {
                                $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                            }
                        }
                    }
                }*/
                
                if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                    $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
                }
                else {
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The Creator was not found.");
                    }
                    else {
                        $db->next_record();
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Creators' status is not Active.");
                        }
                        else {
                            $data['creator'] = processSQLData($db->result());
                        }
                    }
                }
    
                if ( !isset($data['client']) || empty($data['client']) ) {
                    $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
                }
                else {
                    $query = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                                ." WHERE user_id = '". $data['client'] ."'";
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The selected Client was not found.");
                    }
                    else {
                        $db->next_record();
                        //print_r($db->result());
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Client is not Active.");
                        }
                        else {
                            $data['client'] = processSQLData($db->result());
                        }
                    }
                }
    
                // Validate the Executives Team.
                if ( !isset($data['team']) || empty($data['team']) ) {
                    $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
                }
                else {
                    $team = "'". implode("','", $data['team']) ."'";
                    
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                                ." WHERE user_id IN (". $team .") ";
                    if ( $db->query($query) ) {
                        $team_db = NULL;
                        while ($db->next_record()) {
                            $team_db[] = processSqlData($db->result());
                        }
                        $team_count = count($data['team']);
    
                        for ( $i=0; $i<$team_count; $i++ ) {
                            $index = array();
                            if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                            }
                            else {
                                if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                    $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                                }
                                else {
                                    $data['team_members'][$i] = $team_db[$index[0]];
                                }
                            }
                        }
                        // Uncomment the following if speed is preferred against a detailed error report as above.
                        //if ( $db->nf() < $team_count ) {
                        //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                        //}
                    }
                }
                if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                $messages->setErrorMessage("Order Title should be provided.");
                }
                if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                    $messages->setErrorMessage("Order Type should be provided.");
                }
                if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                    $messages->setErrorMessage("Order Closed By should be provided.");
                }
               /*
                if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                    $messages->setErrorMessage("Order Particulars should be provided.");
                }*/
                
                if ( !isset($data['access_level']) ) {
                    $messages->setErrorMessage("Select the Access Level.");
                }
                elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                    $messages->setErrorMessage("Selected Access Level not found.");
                }
                
                if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                        && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                    $messages->setErrorMessage("The Status is not valid.");
                }
                
                // Format the Date of Delivery, if provided.
                if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                    $data['do_d'] = explode('/', $data['do_d']);
                    $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
                }
                
                // Check for the duplicate Order Number.
                $list = NULL;
                $condition_query = " WHERE number = '". $data['number'] ."' AND id != '". $data['or_id'] ."'";
                if ( PreQuote::getList($db, $list, 'id', $condition_query)>0 ) {
                    $messages->setErrorMessage("An Order with the same Number already exists.");
                }
                $list = NULL;
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
            
             if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }
            
            //$data['query_p'] = 'INSERT INTO '. TABLE_BILL_ORD_P .' (ord_no, particulars, p_amount, s_id,sub_s_id, ss_title, ss_punch_line, tax1_name,tax1_value, tax1_pvalue,d_amount,stot_amount,tot_amount,discount_type,pp_amount,vendor,purchase_particulars ) VALUES ';
            $is_part = false;
            if(isset($data['particulars'])){
                foreach ( $data['particulars'] as $key=>$particular ) {
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
        
                                $is_part = true;
                                /*
                                $data['query_p'] .= "('". $data['number'] ."', '". processUserData($data['particulars'][$key]) ."', '". $data['p_amount'][$key] ."','". $data['s_id'][$key] ."','".$data['sub_s_id'][$key] ."',
                                '". processUserData($data['ss_title'][$key]) ."', '". processUserData($data['ss_punch_line'][$key]) ."',
                                '". processUserData($data['tax1_name'][$key]) ."', '". processUserData($data['tax1_value'][$key]) ."','". processUserData($data['tax1_pvalue'][$key]) ."',
                                '". processUserData($data['d_amount'][$key]) ."', '". processUserData($data['stot_amount'][$key]) ."',
                                '". processUserData($data['tot_amount'][$key]) ."',
                                '". processUserData($data['discount_type'][$key]) ."', '". processUserData($data['pp_amount'][$key]) ."',
                                '". processUserData($data['vendor'][$key]) ."', '". processUserData($data['purchase_particulars'][$key]) ."'                           
                                )," ;
                                */
                            }
                        }
                    }
                }
            }
            if ( !$is_part ) {
                $messages->setErrorMessage('The Particular for the Order cannot be empty.');
            }
            else {
                $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
            }

            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Total Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            
            
            
            
            
             // Check for start date
            if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
            //Check Estimated hrs should not be characters
            if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        /**
         * This function is used to create the File in PDF or HTML format for the Invoice.
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createFile($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("sale-quotation-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_QUOTATIONS_HTML, 0777);
                if ( is_writable(DIR_FS_QUOTATIONS_HTML) ) {
                    $file_name = DIR_FS_QUOTATIONS_HTML ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Quotation was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Quotation is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
               
            
            
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
        /**
         * This function is called after a new Order has been created
         * against a Pre Order. This function is called only once for 
         * each Order.
         *
         */
        function setPreOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_BILL_PO
                        ." SET status = '$status'"
                        ." WHERE id = '$id'";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		
		/**
		 * This function is used to retrieve the Members of the Order.
		 * 
		 */
		function getTeamMembers($id, &$team_members, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$order = NULL;
			if ( PreQuote::getList($db, $order, 'team', " WHERE id = '$id'" ) > 0 ) {
				include_once ( DIR_FS_INCLUDES .'/user.inc.php');

				$order = $order[0];
				$order['team'] = "'". implode("','", (explode(',', $order['team']))) ."'";
                $order['team_members']= '';
                if ( User::getList($db, $team_members, 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $order['team'] .")") > 0 ) {
					return (true);
				}
			}
			return (false);
		}

        
		
		function getDeliveryDate(&$db, $order_id) {
			$query = "SELECT do_d FROM ". TABLE_SALE_QUOTATION
						." WHERE id = '". $order_id ."'";
			$db->query($query);
			if ( $db->nf()>0 && $db->next_record() ) {
				return ($db->f('do_d'));
			}
			return ('');
		}
        
        //Function required for displaying the order_id + order_title from  work_timeline
        /*
        function getOrderList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }
            
            $query .= " FROM ". TABLE_SALE_QUOTATION;
            $query .= " LEFT JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_SALE_QUOTATION .".client ";
            $query .= " LEFT JOIN ". TABLE_WORK_TL." ON ". TABLE_WORK_TL .".order_id = ". TABLE_SALE_QUOTATION .".id ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
					
					if ( strpos($required_fields, 'team') > 0 ) {
						include_once ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$count = count($list);
						for ( $i=0; $i<$count; $i++ ) {
							// Read the Team Member Details.
							$list[$i]['team'] = "'". implode("','", (explode(',', $list[$i]['team']))) ."'";
							$list[$i]['team_members']= '';
							User::getList($db, $list[$i]['team_members'], 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $list[$i]['team'] .")");
							
							// Read the latest Time Line update.
							include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
							$list[$i]['work'] = NULL;
							//WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, order_title, to_id, by_id, do_assign, comments, status');
							WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, to_id, by_id, do_assign, comments, status');
							$list[$i]['work'] 		= $list[$i]['work'][0];
							//$list[$i]['order_title']= $list[$i]['work']['order_title'];
							
							$list[$i]['comments'] 	= $list[$i]['work']['comments'];
							User::getList($db, $list[$i]['work']['by'], 'user_id, number, f_name, l_name, email', "WHERE user_id = '". $list[$i]['work']['by_id'] ."'");
							$list[$i]['work']['by'] = $list[$i]['work']['by'][0];
						}
					}
                }
                
                return ( $db->nf() );
            }
            else {
                return false;
            }   
        }*/
        
        function getAccessLevel(&$db, $level=0, $al_list='') {
            include_once ( DIR_FS_INCLUDES .'/settings-access-level.inc.php');

            $role_list = NULL;
            if ( !empty($al_list) ) {
                if ( is_array($al_list) ) {
                    $al_list = ' AND access_level IN ('. ('\''. implode('\',', $al_list) .'\'') .')';
                }
                else {
                    $al_list = ' AND access_level IN ('. ('\''. str_replace(',', '\',\'', $al_list) .'\'') .')';
                }
            }
            else {
                $al_list = '';
            }
            
            $access     = NULL;
            $condition  = " WHERE access_level < '". $level ."'"
                            . $al_list
                            ." AND status = '". AccessLevel::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            if ( (AccessLevel::getList($db, $access, 'title, access_level', $condition)) <= 0 ) {
                $access[] = array('title' => 'None', 'access_level' => '0' );			
            }
            
            return ($access);
        }
		
    }
    
    
    function createQuotationPDF( $q_no, $data ) {
                require_once(DIR_FS_ADDONS .'/html2pdf/config.inc.php');
                require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
                require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
                parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
              
                global 	$g_config;
                
                class ReportData extends Fetcher {
                    var $content;
                    var $url;
                
                    function get_data($data) {
                        return new FetchedDataURL($this->content, array(), "","");
                    }
                
                    function ReportData($content) {
                        $this->content = $content;
                    }
                    
                    function get_base_url() {
                        return "";
                    }
                }
                
                $contents 	= '';
                
        
                // Prepare the Quotation content.
                
        //		$contents .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        //		$contents .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        //		$contents .= '<head>';
        //		$contents .= '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
        //		$contents .= '<title>'. $q_no .'</title>';
                
                // CSS used in this Quotation
                /* comment for some time 2009-04-22 
                $contents .= '<style media="all" type="text/css">';
                $contents .= '.clear{clear:both;}';
                $contents .= '.left{text-align:left;}';
                $contents .= '.pageBreak{page-break-after:always;}';
                $contents .= '#quotation_header {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';		
                $contents .= '#quotation_header_subscript {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';
                $contents .= '#quotation_header_subscript div.right {padding-right:10px;text-align:right;}';
                $contents .= '#quotation_header_subscript div.left {padding-left:10px;text-align:left;}';
                $contents .= '#quotation_header_subscript .bold {font-weight:bold;}';
                $contents .= '#quotation_sign {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';
                $contents .= '#quotation_sign .smeerp{color:#CC0000;font-family:Arial, Helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:bold;}';
                $contents .= '#quotation_sign .name {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:13px;font-style:normal;font-weight:bold;}';
                $contents .= '#quotation_sign .designation {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';
                $contents .= '#quotation_sign .punchLine {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:bold;}';
                $contents .= '#quotation_sign .subScript {color:#6600CC;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:bold;}';
                $contents .= '.bold {font-weight:bold;}';
                $contents .= '#quotation_footer {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';
                $contents .= '#quotation_footer hr {height:1px;width:90%}';
                $contents .= '#quotation_footer .bold {font-weight:bold;}';		
                $contents .= '#quotation_terms {text-align:left;}';
                $contents .= '#quotation_terms div.header{color:#BF2004;font-family:Arial, Helvetica, sans-serif;font-size:18px;font-weight:bold;text-align:center;}';
                $contents .= '#quotation_terms div.headerSub{color:#453CF2;font-family:Arial, Helvetica, sans-serif;font-size:14px;font-weight:bold;text-align:left;}';
                //$contents .= '@import "'. DIR_WS_CSS .'/quotation_doc.css";';
                $contents .= '</style>';
                
        //		$contents .= '</head>';
        //		$contents .= '<body>';
                
                $contents .= '<div style="text-align:center;"><div style="width:650px;text-align:left;">';
                $contents .= '<table width="100%">';
                        
                // Body.
                $contents .= '<tr><td>';
                $contents .= $data['headerSub'];
                $contents .= '<div class="clear left"></div>';
                $contents .= '</td></tr>';
                
                // Content
                $contents .= '<tr><td>';
                $contents .= $data['body'];
                $contents .= '<div class="clear left"></div>';
                $contents .= '</td></tr>';
                // Signature
                $contents .= '<tr><td>';
                $contents .= $data['sign'];
                $contents .= '<div class="clear left"></div>';
                $contents .= '</td></tr>';
                
                $contents .= '</table>';
                //$contents .= '<!--NewPage-->';
                $contents .= '<pagebreak/>';
                */
                /*$contents .= '<?page-break>';*/
                /*comment for some time 2009-04-apr 
                $contents .= '<table width="100%">';
                
                // Terms & Conditions
                $contents .= '<tr><td>';
                $contents .= $data['tc'];
                $contents .= '</td></tr>';
            
        
                $contents .= '</table>';
                $contents .= '</div></div>';
                
                */
                
                $contents .= $data['content'];
                
        //		$contents .= '</body>';
        //		$contents .= '</html>';
        
                $style	='';
                $style .= '<style media="all" type="text/css">';
                $style .= '.clear{clear:both;}';
                $style .= '.pageBreak{page-break-after:always;}';
                $style .= '#quotation_header {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';
                $style .= '#quotation_header_subscript {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';
                $style .= '#quotation_header_subscript div.right {padding-right:10px;text-align:right;}';
                $style .= '#quotation_header_subscript div.left {padding-left:10px;text-align:left;}';
                $style .= '#quotation_header_subscript .bold {font-weight:bold;}';
                $style .= '#quotation_sign {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';
                $style .= '.smeerp{color:#CC0000;font-family:Arial, Helvetica, sans-serif;font-size:20px;font-style:italic;font-weight:bold;}';
                $style .= '.name {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:13px;font-style:normal;font-weight:bold;}';
                $style .= '.designation {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:italic;font-weight:bold;}';
                $style .= '.punchLine {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:italic;font-weight:bold;padding-left:80px;}';
                $style .= '.subScript {color:#6600CC;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:bold;}';
                $style .= '.bold {font-weight:bold;}';
                $style .= '#quotation_footer {color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-style:normal;font-weight:normal;}';
                $style .= '#quotation_footer hr {height:1px;width:90%}';
                $style .= '#quotation_footer .bold {font-weight:bold;}';
                $style .= '#quotation_terms {text-align:left;}';
                $style .= '#quotation_terms div.header{color:#BF2004;font-family:Arial, Helvetica, sans-serif;font-size:18px;font-weight:bold;text-align:center;}';
                $style .= '#quotation_terms div.headerSub{color:#453CF2;font-family:Arial, Helvetica, sans-serif;font-size:14px;font-weight:bold;text-align:left;}';
                //$style .= '@import "'. DIR_WS_CSS .'/quotation_doc.css";';
                $style .= '</style>';
                
                // Configuration Settings.
                $g_config = array(
                                    'process_mode' 	=> 'single',
                                    'pixels'		=> '750',
                                    'scalepoints'	=> '1',
                                    'renderimages'	=> '1',
                                    'renderlinks'	=> '1',
                                    'renderfields'	=> '1',
                                    'media'			=> 'A4',
                                    'cssmedia'		=> 'screen',
                                    'leftmargin'	=> '30',
                                    'rightmargin'	=> '15',
                                    'topmargin'		=> '5',
                                    'bottommargin'	=> '15',
                                    'encoding'		=> 'iso-8859-1',
                                    //'encoding'		=> 'UTF-8',
                                    'headerhtml'	=> $data['header'],
                                    'boydhtml'		=> $contents,
                                    'footerhtml'	=> $data['footer'],
                                    'watermarkhtml'	=> '',
                                    'pslevel'		=> '3',
                                    'method'		=> 'fpdf',
                                    'pdfversion'	=> '1.3',
                                    'output'		=> '2',
                                    'convert'		=> 'Convert File'
                                );
                
                $media = Media::predefined('A4');
                $media->set_landscape(false);
                $media->set_margins(array(	'left' => 10,
                                            'right' => 10,
                                            'top' => 15,
                                            'bottom' => 35)
                                    );
                $media->set_pixels(700);
                 
                $g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
                $g_pt_scale = $g_px_scale * 1.43;
                
                $pipeline 						= PipelineFactory::create_default_pipeline("","");
                $pipeline->pre_tree_filters[] 	= new PreTreeFilterHeaderFooter( $g_config['headerhtml'], $g_config['footerhtml']);
                $pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
                if ($g_config['renderfields']) {
                    $pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
                }
                
        
        // Changed so that script will try to fetch files via HTTP in this case) and to provide absolute paths to the image files.
        // http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
        //		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
                $pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
                //$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
                $pipeline->destination 			= new DestinationFile($q_no);
                 
                $pipeline->process($q_no, $media);
                
               // $filename = DIR_FS_QUOTATIONS .'/pdf/'. $q_no .'.pdf';
                $filename = DIR_FS_QUOTATIONS_PDF .'/'. $q_no .'.pdf';
                @chmod($filename, 0777);
                $contents = NULL;
                return true;
            }
    //Functions 
    function generateQuotationPDF( $q_no, $data ) {
                require_once(DIR_FS_ADDONS .'/html2pdf/config.inc.php');
                require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
                require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
                parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
              
                global 	$g_config;
                
                class ReportData extends Fetcher {
                    var $content;
                    var $url;
                
                    function get_data($data) {
                        return new FetchedDataURL($this->content, array(), "","");
                    }
                
                    function ReportData($content) {
                        $this->content = $content;
                    }
                    
                    function get_base_url() {
                        return "";
                    }
                }
                
                $contents 	= '';
                
       
                // Prepare the Quotation content.
                
        //		$contents .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        //		$contents .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        //		$contents .= '<head>';
        //		$contents .= '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
        //		$contents .= '<title>'. $q_no .'</title>';
                
                // CSS used in this Quotation
                // comment for some time 2009-04-22 
                $contents .= '<style media="all" type="text/css">
                        body {
                            font-family:verdana,arial,sans-serif;
                            font-size:10px;
                            margin-right:auto;
                            margin-left:auto;
                        }
                        #content {
                        margin:0;
                        width: 100%;
                        text-align:center;
                        }
                        #content-main {           
                            padding:5px 5px 5px 5px;
                            width: 710px;
                            text-align:center;
                            margin-left:auto;
                            margin-right:auto;
                        }';
                 $contents .= '.spacer{background:url('.$data['images'].'/spacer.gif) repeat;}
                        .bg1{background-color:#121211;}
                        .bg2{background-color:#CCCCCC;}
                        .bg3{background-color:#F34838;}
                        .bg4{background-color:#675675;}
                        .bg5{background-color:#956565;}
                        .bg6{background-color:#BEBE74;}
                        .bg7{background-color:#f7f7f7;}
                         
                        .bdb1{border-bottom:1px solid #000000;}
                        .bdt1{border-top:1px solid #000000;}
                        .bdt2{border-top:2px solid #ffffff;}
                        
                        .bdl1{border-left:2px solid #ffffff;}
                        
                        .b {font-weight:bold;}
                        
                        .fr1{color:#da1900;}
                        .fr2{color:#c3031c;}
                        
                        .al {text-align:left;}
                        .ac {text-align:center;}
                        .ar {text-align:right;}
                        
                        .vt {vertical-align:top;}
                        .vm {vertical-align:middle;}
                        .vb {vertical-align:bottom;}
                        
                        .fs08 {font-size:0.8em;}
                        .fs11 {font-size:1.1em;}
                        .fs15 {font-size:1.5em;}
                        
                        .lh15 {line-height:1.5em;}
                        .lh10 {line-height:1em;}
                        
                        .pl2{padding-left:2px;}
                        .pl5{padding-left:5px;}
                        .pl10{padding-left:10px;}
                        .pl20{padding-left:20px;}
                        .pl40{padding-left:40px;}
                        .pl60{padding-left:60px;}
                        
                        .pr2{padding-right:2px;}
                        .pr5{padding-right:5px;}
                        .pr10{padding-right:10px;}
                        .pr20{padding-right:20px;}
                        .pr40{padding-right:40px;}
                        .pr50{padding-right:50px;}
                        
                        .pb2{padding-bottom:2px;}
                        .pb5{padding-bottom:5px;}
                        .pb10{padding-bottom:10px;}
                        .pb20{padding-bottom:20px;}
                        
                        .pt2{padding-top:2px;}
                        .pt5{padding-top:5px;}
                        .pt10{padding-top:10px;}
                        .pt30{padding-top:30px;}
                        
                        .wp100{width:100%;}
                        .wp80{width:80%;}
                        .wp70{width:70%;}
                        .wp65{width:65%;}
                        .wp60{width:60%;}
                        .wp50{width:50%;}
                        .wp40{width:40%;}
                        .wp35{width:35%;}
                        .wp30{width:30%;}
                        .wp15{width:15%;}
                        .wp20{width:20%;}
                        .wp10{width:10%;}
                        .wp5{width:5%;}
                        div.row {
                            clear:both;
                            float:left;
                        }
                        div.coloumn {
                            float:left;
                        }
                        #quotation_header {color:#000000;font-family:Verdana, Helvetica, sans-serif;font-size:11px;font-style:normal;font-weight:normal;text-align:center;}
                 </style>';
                
       	$contents .= '<div id="content">
                        <div id="content-main">
                            <div class="row al al10">
                                <div id="quotation_header">
                                    Classified Document - Only respective client and SMEERP Personnel\'s are authorized to view or edit or store           
                                </div>            
                            </div>       
                            <div class="row pb20 pt30">
                                <div class="coloumn al pl10"><img src='.$data['images'].'/smeerp-quot-logo.jpg alt="SMEERP Technologies Pvt. Ltd." width="220" height="25" title="SMEERP Technologies Pvt. Ltd."border="0" /></div>        
                                <div style="width:30px;">&nbsp;</div>
                                <div class="coloumn ar pr10"><img src='.$data['images'].'/motivatedserve.jpg alt="Motivated To Serve Determined To Excel" border="0" /></div>
                            </div>
                            <div class="row wp100 pb5 ar ">
                                   <span class="pt5 pr10 pl10 lh10"><span class="b">Date: </span>'.$data['do_e'].'
                            </div>
                            <div class="row wp100 pb5 ar ">
                                   <span class="pt5 pr10 pl10 lh10 b"><span class="b">Quot. No: </span>'.$data['number'].'
                            </div>
                            <div class="row wp100 pb5 ar ">
                                   <span class="pt5 pr10 pl10 lh10 b"><span class="b">Quot.Validity: </span>'.$data['validity'].'
                            </div>        
                            <div class="row wp100 pt10 pb10">
                                 <div class="coloumn al wp60">
                                        <br/><span class="pl10 lh10">To,</span><br>
                                        <span class="b pl20 lh15">
                                            '.$data['b_address']['company_name'].'
                                        </span>
                                        <br/><span class="pl20 lh10">'.nl2br($data['b_address']['address']).'</span>
                                        <br/><span class="pl20 lh10">'.$data['b_address']['city_title'].' '.$data['b_address']['zipcode'].' '.$data['b_address']['state_title'].' '.$data['b_address']['c_title'].'</span>
                                 </div>
                            </div>        
                            <div class="row wp100 pt10 pb10">   
                                <div class="row al pl10 wp80">
                                    <div class="coloumn pr10 wp10 b">Subject :</div>
                                    <div class="coloumn al wp70">'.$data['subject_title'].'</div>
                                </div>           
                            </div>
                            <div class="row wp100 pt10 pb10">   
                                <div class="row al pl10 wp60">
                                    <div class="coloumn pr10 wp60 b">Dear Sir,</div>                  
                                </div>           
                            </div>
                            
                            <div class="row wp100 pt10 pb10">
                                <div class="row al pl20 wp100">
                                       With reference to the discussion regarding your web services requirements, herewith please find our <br/>quotation for
                                    your consideration.
                                       
                                </div>
                               <div class="row al pl20 wp100">              
                                    Should you however have any further technical / commercial query, kindly feel free to contact. It would be our <br/>pleasure to be of service to you.
                                    
                                </div>
                                <div class="row al pl20 wp100">              
                                    Finally, I must <b>THANK! </b>for the very kind courtesy extended to us to discuss on above mentioned subject.             
                                </div>
                                <div class="row al pl20 wp100">            
                                    Thanking you in anticipation and looking forward to a healthy business association.                     
                                </div>
                                
                                <div class="row al pl20 pt10 wp100">                   
                                    With very profound regards.   
                                          
                                </div>
                                <div class="row al wp100">
                                    <span class="pl20 pt10">    
                                    Yours Sincerely,
                                    </span>                     
                                </div>
                                <div class="row al pl20 wp100 ">               
                                   <b>'.$data['user']['f_name']. ' '.$data['user']['l_name'].'</b> <br/>
                                    <b>'.$data['user']['desig'].'</b><br/>
                                    '.$data['user']['contact'].'             
                                </div>
                                <div class="clear"></div>
                                <div class="row wp100 pt10 pb5 ac">
                                <span class="b fs15 lh15">Quotation Particulars</span>
                                </div>
                                <div class="row wp100 pb5 pt5 bg2 lh10">
                                    <div class="coloumn al wp30"><span class="pl5 fs11 b">Quot. Particulars</span></div>
                                    <div class="coloumn al wp10"><span class="pl5 fs11 b">Price</span></div>
                                    <div class="coloumn al wp10"><span class="pl5 fs11 b">Discount</span></div>
                                    <div class="coloumn al wp10"><span class="pl5 fs11 b">SubTot</span></div>
                                    <div class="coloumn al wp10"><span class="pl5 fs11 b">Tax</span></div>
                                    <div class="coloumn al wp10"><span class="pl5 fs11 b">Tax%</span></div>
                                    <div class="coloumn ar wp10"><span class="pr5 fs11 b">Tot.Amount</span></div>
                                </div>
                                <div class="row spacer pt5 wp100"></div>';
                               foreach($data['particulars'] as $pkey =>$particular){
                                    if (!empty($particular['ss_punch_line'])){
                                        $contents .= '<div class="row wp100 pb5 pl5 pr5 lh15 al">
                                                        <div class="row pt2 wp100">'.($pkey+1).
                                                            '&nbsp;<span class="fs10"><b>'.$particular['ss_punch_line'].'</b></span>
                                                        </div>
                                                    </div>' ;      
                                    }
                                    $contents .='<div class="row pb5 wp100">
                                        <div class="coloumn al wp30"><span class="pl5">'.$particular['particulars'].'</span></div>
                                        <div class="coloumn al wp10"><span class="pr5">'.$particular['p_amount'].'</span></div>
                                        <div class="coloumn al wp10"><span class="pr5">'.$particular['d_amount'].'</span></div>
                                        <div class="coloumn al wp10"><span class="pr5">'.$particular['stot_amount'].'</span></div>';
                                        if($data['tax1_pvalue'][$pkey] != 0){
                                            $contents .='<div class="coloumn al wp10"><span class="pr5">'.$particular['tax1_name'].'</span></div>
                                            <div class="coloumn al wp10"><span class="pr5">'.$particular['tax1_value'].'</span></div>';
                                        }else{
                                            $contents .='<div class="coloumn al wp10"><span class="pr5">--</span></div>
                                            <div class="coloumn al wp10"><span class="pr5">--</span></div>';
                                        }
                                        $contents .='<div class="coloumn ar wp10"><span class="pr5">'.$particular['tot_amount'].'</span></div>
                                    </div>';   
                                }
                                $contents .='<div class="row pt5 wp100">
                                    <hr class="bdb1"/>
                                </div>                                
                                <div class="row wp100 pt10 pb10">
                                    <div class="coloumn al wp60">
                                        &nbsp;
                                    </div>
                                    <div class="coloumn ar wp40 lh15">
                                       
                                        <div class="row wp100">
                                            <div class="coloumn al wp40"><span class="b">Grand Total</span></div>
                                            <div class="coloumn al wp60"><span>'.$data['amount'].'</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row wp100 pb10 pt5 lh10 ar">
                                    <span class="b fs11 pr10">Amount in words: '.$data['currency'].' '.$data['amount_words'].'</span>
                                </div>            
                                <div class="row wp100 pb5 pt5 bg2 lh10 al">
                                    <span class="b fs11 pl5">Terms and Conditions</span>
                                </div>  ';          
                                foreach($data['terms'] as $key1=>$item){
                                     $contents .='<div class="row wp100 pb5 pt5 lh10 al">
                                        <span class="b fr1 fs11 pl5">'.($key1).' '.
                                            $item['title'].'</span>
                                        </div>
                                        <div class="row wp100 pb5 pl5 pr5 lh15 al">
                                            <div class="row pt2 wp100"><span class="fs10">
                                            '.$item['condition_content'].'
                                            </span></div>
                                        </div>';
                                
                                }
                                $contents .='
                                <div class="row wp100 pb5 pt5 bg2 lh10">
                                    <div class="coloumn wp50 al">
                                        <div class="row wp100">
                                            <span class="b pl5">Questions? Contact us with a support ticket on</span>
                                        </div>
                                        <div class="row wp100">
                                            <span class="b pl5">www.my.smeerptech.com or call us at the numbers to the right</span>
                                        </div>
                                    </div>
                                    <div class="coloumn wp50 ar">
                                        <div class="wp100 ar">
                                            <span class="b" style="padding-right:115px;">India : 0091-712-6546453, 2284514 </span>
                                        </div>
                                        <div class="row ar wp100">
                                            <span class="b al pr5">USA (Toll Free) : 1-837-621-1000 | www.smeerptech.com</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row wp100 pb5 pt5 lh10">
                                    <div class="coloumn wp40 al"><span class="b fs11 pl5">SMEERP Technologies</span></div>
                                    <div class="coloumn wp60 ar"><span class="b fs11 pr5">17, MG street, Jhena 440 025 Maharashtra India</span></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>';       
               echo $contents;
             
                // Configuration Settings.
                $g_config = array(
                                    'process_mode' 	=> 'single',
                                    'pixels'		=> '750',
                                    'scalepoints'	=> '1',
                                    'renderimages'	=> '1',
                                    'renderlinks'	=> '1',
                                    'renderfields'	=> '1',
                                    'media'			=> 'A4',
                                    'cssmedia'		=> 'screen',
                                    'leftmargin'	=> '30',
                                    'rightmargin'	=> '15',
                                    'topmargin'		=> '5',
                                    'bottommargin'	=> '15',
                                    'encoding'		=> 'iso-8859-1',
                                    //'encoding'		=> 'UTF-8',
                                    'headerhtml'	=> $data['header'],
                                    'boydhtml'		=> $contents,
                                    'footerhtml'	=> $data['footer'],
                                    'watermarkhtml'	=> '',
                                    'pslevel'		=> '3',
                                    'method'		=> 'fpdf',
                                    'pdfversion'	=> '1.3',
                                    'output'		=> '2',
                                    'convert'		=> 'Convert File'
                                );
                
                $media = Media::predefined('A4');
                $media->set_landscape(false);
                $media->set_margins(array(	'left' => 10,
                                            'right' => 10,
                                            'top' => 15,
                                            'bottom' => 35)
                                    );
                $media->set_pixels(700);
                 
                $g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
                $g_pt_scale = $g_px_scale * 1.43;
                
                $pipeline 						= PipelineFactory::create_default_pipeline("","");
                $pipeline->pre_tree_filters[] 	= new PreTreeFilterHeaderFooter( $g_config['headerhtml'], $g_config['footerhtml']);
                $pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
                if ($g_config['renderfields']) {
                    $pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
                }
                
        
            // Changed so that script will try to fetch files via HTTP in this case) and to provide absolute paths to the image files.
             // http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
             //		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
                $pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
                //$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
                $pipeline->destination 			= new DestinationFile($q_no);
                 
                $pipeline->process($q_no, $media);
                
               // $filename = DIR_FS_QUOTATIONS .'/pdf/'. $q_no .'.pdf';
                $filename = DIR_FS_QUOTATIONS_PDF .'/'. $q_no .'.pdf';
                @chmod($filename, 0777);
                $contents = NULL;
                return true;
        }
           
    //Other functions to create the quotation 
    /*
	function getQuotationDocHeader(&$s, &$db) {
		$contents 	= '';
		$filename 	= DIR_FS_QUOTATIONS .'/quotation-header.html';
		
		$handle 	= fopen($filename,"rb"); 
		$contents 	= fread ($handle, filesize ($filename)); 
		fclose ($handle);
		
		return $contents;
	}
	
	function getQuotationDocHeaderSub(&$s, &$db, $data,$extra) {
		
        foreach($data as $key=>$value) {
			$s->assign($key, $value);
		}
        $s->assign('data', $data);
        
        $contents = $s->fetch("quotation-header-subscript.html");
        return $contents;
	}*/
    
    function getQuotationContent(&$s, &$db, $data,$extra) {
		/*        
        foreach ($extra as $key=>$value) {
            $key = $value;
        }
        $s->assign('data', $data);
        $contents = $s->fetch("sale-quotation-tpl.html");
        */
        $filename = DIR_FS_QUOTATIONS_HTML.'/'.$data['q_no'].".html";
        $handle = fopen($filename, "r");
        $contents = fread($handle, filesize($filename));
        fclose($handle);

        return $contents;
	}
    
	/*
	function getQuotationDocBody(&$s, &$db, $q_id='') {
		$contents 	= '';
		
		$query = "SELECT pq_particulars FROM ". TABLE_PRE_QUOTE_HISTORY
					." WHERE pq_id = '". $q_id ."'"
					." ORDER BY date DESC LIMIT 0, 1";
		if ( $db->query($query) &&  $db->nf()>0 ) {
			$db->next_record();
			$contents = $db->f('pq_particulars');
		}
		
		return $contents;
	}
	
	function getQuotationDocSign(&$s, &$db, $sign_id='') {
		$contents 	= '';
		$filename 	= DIR_FS_QUOTATIONS .'/quotation-signature.html';
		
		$handle 	= fopen($filename,"rb"); 
		$contents 	= fread ($handle, filesize ($filename)); 
		fclose ($handle);
		
		return $contents;
	}
	
	function getQuotationDocFooter(&$s, &$db) {
		$contents 	= '';
		$filename 	= DIR_FS_QUOTATIONS .'/quotation-footer.html';
		
		$handle 	= fopen($filename,"rb"); 
		$contents 	= fread ($handle, filesize ($filename)); 
		fclose ($handle);
		
		return $contents;
	}
   */
	function getQuotationTC(&$s, &$db, $q_id='') {
		$contents 	= '';
		$terms 		= '';
		
		$query = "SELECT terms FROM ". TABLE_SALE_QUOTATION
					." WHERE q_id = '". $q_id ."'"
					." ORDER BY date DESC LIMIT 0, 1";
		if ( $db->query($query) &&  $db->nf()>0 ) {
			$db->next_record();
			$terms = $db->f('terms');
		}
		
		$query = "SELECT title, condition_content FROM ". TABLE_TERMS_CONDITION
					." WHERE (id IN (". $terms .") AND status = '1') "
					." OR status = '3' "
					." ORDER BY status DESC ";
		if ( $db->query($query) &&  $db->nf()>0 ) {
			$contents .= '<div id="quotation_terms">';
			$contents .= '<div class="header">Terms & Conditions</div>';
			while ( $db->next_record() ) {
				$terms = clearData($db->Record);
				$contents .= '<div class="headerSub">'. $terms['title'] .'</div>';
				$contents .= '<div>'. $terms['condition_content'] .'</div>';
			}
			$contents .= '</div></div>';
		}
        
		return $contents;
	}
        
        
    // create quotation in PDF EOF
        
?>
