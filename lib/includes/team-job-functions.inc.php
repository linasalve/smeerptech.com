<?php 

	class TeamJobFunctions {
		
		const DEACTIVE = 0;
		const ACTIVE  = 1;
		 
		
	
        
      
	

        
        //const ADDUSER = 5;

	function getStatus() {
		$status = array('DEACTIVE' => TeamJobFunctions::DEACTIVE,
				'ACTIVE'  => TeamJobFunctions::ACTIVE
			 
				);
		return ($status);
	}
		
	 
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				  $query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TJ_FUNCTIONS;
			
            $query .= " ". $condition;
	  
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
       

        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, TeamJobFunctions::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (TeamJobFunctions::getList($db, $user, 'id', " WHERE id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_TJ_FUNCTIONS
								." SET status = '$status_new' "
								." WHERE id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) {
						$messages->setOkMessage("The Status has been updated.");
					}
					else {
						$messages->setErrorMessage("The Status was not updated.");
					}
                     
                }
                else {
                    $messages->setErrorMessage("The Client was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
             if ( (TeamJobFunctions::getList($db, $details, 'id ', " WHERE id = '$id'")) > 0 ) {
                $details = $details[0];
				$query = "DELETE FROM ". TABLE_TJ_FUNCTIONS_P
							." WHERE tjf_id = '$id'";
				
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$query = " DELETE FROM ". TABLE_TJ_FUNCTIONS ." WHERE id = '$id'" ;
							
					$db->query($query);		
					$messages->setOkMessage("The Record has been deleted.");                      
				}
				else {
					$messages->setErrorMessage("The Record was not deleted.");
				}
                
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
         
        
        
         
		 
		 
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
            
			if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage("Job Profile can not be empty.");
            }
            if(isset($data['jtitle'])){
				 
				foreach ( $data['jtitle'] as $key=>$particular ) {
					if(empty($data['jtitle'][$key]) && empty($data['jdescription'][$key])){
                         $messages->setErrorMessage('The job functions and description '. ($key+1) .' cannot be empty.');
                    }
				}
			
			}else{
				 $messages->setErrorMessage("Enter Job functions.");
			}
            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            
			if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage("Job Profile can not be empty.");
            }
            if(isset($data['jtitle'])){
				 
				foreach ( $data['jtitle'] as $key=>$particular ) {
					if(empty($data['jtitle'][$key]) && empty($data['jdescription'][$key])){
                         $messages->setErrorMessage('The job functions and description '. ($key+1) .'
						 cannot be empty.');
                    }
				}
			
			}else{
				 $messages->setErrorMessage("Enter Job Functions.");
			}


            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
        
        
        /**
         * This function is used to retrieve the next account number in series.
         *
         * @param   object  database class object
         *
         * @return  int     the next number in series
         *
         */
                 
         
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_TJ_FUNCTIONS_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
    }
?>