<?php
	class Outward {
		
		       
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_OUT_WARD;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_OUT_WARD;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (Outward::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               $details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_OUT_WARD
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                /*}
                else {
                    $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                }*/
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
           
            if ( !isset($data['to']) || empty($data['to']) ) {
                $messages->setErrorMessage("To can not be empty.");
            }
            if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                $messages->setErrorMessage("Particulars can not be empty.");
            }
            
            if(!empty($data['content']) ){
                if(!is_numeric($data['content'])){
                    $messages->setErrorMessage("Content should be numeric.");
                }
            }
            if ( !isset($data['mode']) || empty($data['mode']) ) {
                $messages->setErrorMessage("Mode can not be empty.");
            }
            if ( !isset($data['for']) || empty($data['for']) ) {
                $messages->setErrorMessage("For can not be empty.");
            }
            
            if ( !isset($data['date']) || empty($data['date']) ) {
                $messages->setErrorMessage("Date can not be empty.");
            }
            if ( !isset($data['tpf']) || empty($data['tpf']) ) {
                $messages->setErrorMessage("Please choose To process further or not.");
            }
            // Check for start date
            if(!empty($data['date'])){
                $dateArr= explode("/",$data['date']);
                $data['date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
                
                $currDate=date('Y-m-d');
                if($data['date'] > $currDate){
                     $messages->setErrorMessage("Please select proper date.");
                }
            }
             
           
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		//For communication log
        function validateCommentAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( !isset($data['to_id']) || empty($data['to_id']) ) {
                $messages->setErrorMessage("Select Executive.");
            }
            if ( !isset($data['comment']) || empty($data['comment']) ) {
                $messages->setErrorMessage("Task can not be empty.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}

        }
         //Used in communication log
	    function getDetailsCommLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_OUT_WARD_LOG;
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_OUT_WARD_LOG .".by_id  ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
               return ( $total );
            }
            else {
                return false;
            }   
        }
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            if ( !isset($data['to']) || empty($data['to']) ) {
                $messages->setErrorMessage("To can not be empty.");
            }
            if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                $messages->setErrorMessage("Particulars can not be empty.");
            }
            
            if(!empty($data['content']) ){
                if(!is_numeric($data['content'])){
                    $messages->setErrorMessage("Content should be numeric.");
                }
            }
            if ( !isset($data['mode']) || empty($data['mode']) ) {
                $messages->setErrorMessage("Mode can not be empty.");
            }
            if ( !isset($data['for']) || empty($data['for']) ) {
                $messages->setErrorMessage("For can not be empty.");
            }
            
            if ( !isset($data['date']) || empty($data['date']) ) {
                $messages->setErrorMessage("Date can not be empty.");
            }
            if ( !isset($data['tpf']) || empty($data['tpf']) ) {
                $messages->setErrorMessage("Please choose To process further or not.");
            }
            // Check for start date
            if(!empty($data['date'])){
                $dateArr= explode("/",$data['date']);
                $data['date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
                
                $currDate=date('Y-m-d');
                if($data['date'] > $currDate){
                     $messages->setErrorMessage("Please select proper date.");
                }
            }
                       
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
		
    }
?>
