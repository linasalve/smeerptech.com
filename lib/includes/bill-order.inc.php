<?php
	class Order {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
        const DELETED = 3;
		const COMPLETED = 4;
		const PROCESSED = 5;
        
		const WALKIN = 1;
		const TARGETED =2;
        
        const NONRENEWABLE =0;
        const RENEWABLE =1;
		
		const RNONE = 0;
		const RDEACTIVE = 1;
		const RACTIVE  = 2;
		const RUSED = 3;
		 
		const FOLLOWUPPENDING = 0;
		const FOLLOWUPACTIVE  = 1;
		const FOLLOWUPTDS  = 2;
		const FOLLOWUPCEO  = 3;
        const TDSFLWPENDING = 0;
		const TDSFLWACTIVE = 1;
		const NPADEACTIVE = 0;
		const NPAACTIVE = 1;
		//In Production Status 
		const PROACTIVE = 1;
		const PRODEACTIVE = 0;
		
		
		function getRestrictionsFiles($type='') {
			// 1024 * 1024 = 1048576 = 1MB
			$restrictions = array(
									'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), 
									'MIME'      => array('text/csv','text/comma-separated-values','application/vnd.ms-excel', 'text/plain','text/x-csv','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
									'EXT'       => array('csv', 'txt','xlsx')
									 );
			
			if ( empty($type) ) {
				return ($restrictions);
			}
			else {
				return($restrictions[$type]);
			}
		}
		
		function getTypeArray(){
			$order_of = array(
				'1'=>'Programming',
				'2'=>'Designing',
				'3'=>'Printing',
				'4'=>'Domain',
				'5'=>'Hosting',
				'6'=>'Email',
				'7'=>'Digital Signature',
				'8'=>'myCraft - New Order',
				'9'=>'myCraft Annual Support',
				'10'=>'myGallery - New Order',
				'11'=>'myGallery Annual Support',
				'12'=>'eTask - New Order',
				'13'=>'eTask Annual Support',
				'14'=>'eAlumni - New Order',
				'15'=>'eAlumni Annual Support',
				'16'=>'ISO Certification',
				'17'=>'Digital Marketing',
				'18'=>'Photo Video Shoot',
				'19'=>'MSSQL Database',
				'20'=>'MySQL Database',
				'21'=>'Other Services'
			);
			 
			return  $order_of;		
		}
		
		//Referral Status
		function getRStatus(){
			$status = array('ACTIVE'    => Order::RACTIVE,
							'DEACTIVE'   => Order::RDEACTIVE,
							'USED' => Order::RUSED
						);
			return ($status);
		
		}
		
		function getStatus() {
			$status = array('ACTIVE'    => Order::ACTIVE,
							'PENDING'   => Order::PENDING,
							'COMPLETED' => Order::COMPLETED,
							'PROCESSED' => Order::PROCESSED,
                            'DELETED'   => Order::DELETED,
							'CANCELLED' => Order::BLOCKED	
						);
			return ($status);
		}
        function getFollowupStatus() {
			$status = array(
						'ACTIVE'    => Order::FOLLOWUPACTIVE,
						'PENDING' => Order::FOLLOWUPPENDING						
						);
			return ($status);
		}
        function getTypes() {
			$status = array('WALKIN' => Order::WALKIN,
							'TARGETED' => Order::TARGETED
						);
			return ($status);
		}
		function getDiscountType(){		
			$typeList= array('OneTime'=>'OneTime',
							 'Recurring'=>'Recurring' 		 
			);
			return $typeList;
		}
		//New added eof
		function getSTypes(){
		
			$typeList= array('Qty'=>'Qty',
							 'Nos'=>'Nos',							 
							 'Yrs'=>'Yrs',							 
							 'Mth'=>'Mth',
							 'Hrs'=>'Hrs'							 							 
			);
			return $typeList;
		}
        function getNos(){
			$noList=array();
			for($i=1;$i<=11;$i++){
				$noList[]=$i;
			}
			return $noList;
		}
		function getRoundOffOp(){
			$arr= array('+','-');
			return $arr;
		}
		 function getRoundOff(){
			$roundOffList=array();
			
			for($i=0.00;$i< 1; $i = $i+0.01){
				$roundOffList[]=$i;
			}
			return $roundOffList;
		}
        /*
        function getFinancialYear() {
			$financialyear = array( '0' => '2000-2001',
							        '1' => '2001-2002',
                                    '2' => '2002-2003',
                                    '3' => '2004-2005',
                                    '4' => '2006-2007',
                                    '5' => '2007-2008',
                                    '6' => '2008-2009',
						);
			return ($financialyear);
		}
        */
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			//$query = "SELECT DISTINCT ";
			$query = "SELECT ";
			$count = $total = 0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			} 
			$query .= " FROM ". TABLE_BILL_ORDERS;
			/* $query .= " FROM ". TABLE_BILL_ORDERS." LEFT JOIN ".TABLE_BILL_ORD_P." ON ".TABLE_BILL_ORDERS.".number = ".TABLE_BILL_ORD_P.".ord_no"; */
			$query .= " ". $condition; 
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
			//echo $query;
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(DISTINCT(".TABLE_BILL_ORDERS.".id)) as count";
				$count = 1;
            }
            /*
            $query .= " FROM ". TABLE_BILL_ORDERS;
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client ";
            */
            $query .= " FROM ". TABLE_BILL_ORDERS." ";
			$query .=" LEFT JOIN ".TABLE_BILL_ORD_P." ON ".TABLE_BILL_ORDERS.".number = ".TABLE_BILL_ORD_P.".ord_no";
			$query .= " INNER JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_ORDERS .".created_by ";
            $query .= " INNER JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client ";  
            /*$query .= " LEFT JOIN ". TABLE_BILL_INV
                        ." ON ". TABLE_BILL_INV .".or_no = ". TABLE_BILL_ORDERS .".number ";
            */           
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	   function getDashboard( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
               $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_ORDERS;
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
					
					if ( strpos($required_fields, 'team') > 0 ) {
						include_once ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$count = count($list);
						for ( $i=0; $i<$count; $i++ ) {
							// Read the Team Member Details.
							$list[$i]['team'] = "'". implode("','", (explode(',', $list[$i]['team']))) ."'";
							$list[$i]['team_members']= '';
                            
							User::getList($db, $list[$i]['team_members'], 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $list[$i]['team'] .")");
							
							// Read the latest Time Line update.
							include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
							$list[$i]['work'] = NULL;
							//WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, order_title, to_id, by_id, do_assign, comments, status');
							$fields = TABLE_WORK_TL.".id ,".TABLE_WORK_TL.".to_id, ".TABLE_WORK_TL.".by_id, ".TABLE_WORK_TL.".do_assign, ".TABLE_WORK_TL.".comments,".TABLE_WORK_TL.".status";
                            WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], $fields);
							$list[$i]['work'] 		= $list[$i]['work'][0];
							//$list[$i]['order_title']= $list[$i]['work']['order_title'];
							$list[$i]['comments'] 	= $list[$i]['work']['comments'];
                            
							User::getList($db, $list[$i]['work']['by'], 'user_id, number, f_name, l_name, email', "WHERE user_id = '". $list[$i]['work']['by_id'] ."'");
							$list[$i]['work']['by'] = $list[$i]['work']['by'][0];
						}
					}
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
	   
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( !in_array($status_new, Order::getStatus()) ) {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
            else {
                $order = NULL;
                if ( (Order::getList($db, $order, 'id, access_level, status', " WHERE id = '$id'")) > 0 ) {
                    $order = $order[0];
                    if ( $order['status'] != Order::COMPLETED ) {
                        $markComplete='';
                        if ( $order['access_level'] < $access_level ) {
                            if($status_new==Order::COMPLETED){
                                 $markComplete = ", ac_ed_date = '".date('Y-m-d H:i:s')."' ";
                            }
                            $query = "UPDATE ". TABLE_BILL_ORDERS
                                        ." SET status = '$status_new' ".$markComplete
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("Status cannot be changed: Order has been Approved.");
                    }
                }
                else {
                    $messages->setErrorMessage("The Order was not found.");
                }
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count ";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_ORD_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $order = NULL;
            if ( (Order::getList($db, $order, TABLE_BILL_ORDERS.".id,".
				TABLE_BILL_ORDERS.".number,".TABLE_BILL_ORDERS.".access_level,".TABLE_BILL_ORDERS.".status", 
				" WHERE ".TABLE_BILL_ORDERS.".id = '$id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != Order::COMPLETED ) {
                    if ( $order['status'] == Order::BLOCKED ) {
                        if ( $order['access_level'] < $access_level ) {
                        
                            if((Invoice::getList($db, $invoice, TABLE_BILL_INV.'id', " WHERE ".TABLE_BILL_INV.".or_no = '".$order['number']."' AND ".TABLE_BILL_INV.".status !='".Invoice::DELETED."'") ) > 0){
                             
                                $messages->setErrorMessage("Please delete Invoice of order ".$order['number']."  ");
                             
                            }else{                            
                                /*
                                    $query = "DELETE FROM ". TABLE_BILL_ORDERS
                                            ." WHERE id = '$id'";
                                */
                                
                                $query = "UPDATE ". TABLE_BILL_ORDERS
                                            ." SET status ='".Order::DELETED."' WHERE id = '$id'";
                                            
                                if ( $db->query($query) && $db->affected_rows()>0 ) {
                                
                                    //Delete worktimeline for selected order bof
                                    /*
                                    $query = "DELETE FROM ". TABLE_WORK_TL
                                            ." WHERE order_no = '".$order['number']."' ";
                                    $db->query($query);
                                    */
                                    //Delete worktimeline for selected order eof
                                    
                                
                                    $messages->setOkMessage("The Order has been deleted.");
                                    
                                    /*
                                    $query = "DELETE FROM ". TABLE_BILL_ORD_P
                                            ." WHERE ord_no = '".$order['number']."' ";
                                    $db->query($query);
                                    */
                                    
                                }
                                else {
                                    $messages->setErrorMessage("The Order was not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Order with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("To Delete Order it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Order cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
    
		function validateUpload($data, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			if ( empty($file['file_csv']['error']) ) {
				if ( !empty($file['file_csv']['tmp_name']) ) {
					if ( $file['file_csv']['size'] == 0 ) {
						$messages->setErrorMessage('The Uploaded file is empty.');
					}
					if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
						$messages->setErrorMessage('The File type is not allowed to be uploaded.');
					}
					if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
						$messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
					}
				}
				else {
					$messages->setErrorMessage('Server Error: File was not uploaded.');
				}
			}
			else {
				$messages->setErrorMessage('Unexpected Error: File was not uploaded.');
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
           // print_r($data);
            
            $files = $_FILES;
           
            // Validate the Pre Order.
            /* comment on 2009-04-10 
            $list = NULL;
            if ( Order::getList($db, $list, 'id', " WHERE po_id = '". $data['po_id'] ."'")>0 ) {
                $messages->setErrorMessage("The Pre Order has been processed already.");
            }
            else {
                if ( isset($data['po_id']) && !empty($data['po_id']) ) {
                    $query = "SELECT access_level, status FROM ". TABLE_BILL_PO
                            ." WHERE id = '". $data['po_id'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage("The Pre Order was not found in the database.");
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            if ( $db->f('status') != ACTIVE ) {
                                $messages->setErrorMessage("The Pre Order is not yet approved.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                        }
                    }
                }
            }
            */
            
            // As date of Order is compulsary
            if(empty($data['do_o'])){
                 $messages->setErrorMessage("Select the date of Order.");
            }            
            if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            if(empty($data['financialYr'])){
                 $messages->setErrorMessage("Select Financial Yr.");
            }
            if(!empty($data['financialYr'])  ){
                //$_ALL_POST['number'] =$data['number']= getOldCounterNumber($db,'ORD',$data['company_id'],$data['financialYr']); 
                $_ALL_POST['number'] = $data['number'] = getOrderCounterNumber($db, $data['financialYr']);	
				//$_ALL_POST['number'] =$data['number']= getCounterNumber($db,'ORD',$data['company_id']); 
                if(empty($data['number']) ){
                    $messages->setErrorMessage("Order Number was not generated.");
                }
            }
          
            // Check for the duplicate order Number.
            $list = NULL;
            if(!empty($data['number'])){
                if ( ORDER::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                    $messages->setErrorMessage("An Order with the same Number already exists.<br/>Please try again.");
                }
            }
            $list = NULL;
            $data['po_filename'] = '';
            
            if((!empty($files['po_filename']) && (!empty($files['po_filename']['name'])))){
                
                $filename = $files['po_filename']['name'];
                $data['po_filename'] = $filename;
                $type = $files['po_filename']['type'];
                $size = $files['po_filename']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Purchase Order file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of purchase order file is greater than 1Mb');
                }
            }
            
            $data['filename_1'] = '';
            if(!empty($data['filecaption_1']) && empty($files['filename_1']['name'])){
                    $messages->setErrorMessage("Please select work order file1.");
            }
            if((!empty($files['filename_1']) && (!empty($files['filename_1']['name'])))){
                if(empty($data['filecaption_1'])){
                    $messages->setErrorMessage("Caption for file 1 should be provided.");
                }
                $filename = $files['filename_1']['name'];
                $data['filename_1'] = $filename;
                $type = $files['filename_1']['type'];
                $size = $files['filename_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 1 is greater than 1Mb');
                }
            }
            
            $data['filename_2'] = '';
            if(!empty($data['filecaption_2']) && empty($files['filename_2']['name'])){
                    $messages->setErrorMessage("Please select work order file2.");
            }
            if((!empty($files['filename_2']) && (!empty($files['filename_2']['name'])))){
                if(empty($data['filecaption_2'])){
                    $messages->setErrorMessage("Caption for file 2 should be provided.");
                }
                $filename = $files['filename_2']['name'];
                $data['filename_2'] = $filename;
                $type = $files['filename_2']['type'];
                $size = $files['filename_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 2 is greater than 1Mb');
                }
            }
            
            $data['filename_3'] = '';
            if(!empty($data['filecaption_3']) && empty($files['filename_3']['name'])){
                    $messages->setErrorMessage("Please select work order file3.");
            }
            if((!empty($files['filename_3']) && (!empty($files['filename_3']['name'])))){
                if(empty($data['filecaption_3'])){
                    $messages->setErrorMessage("Caption for file 3 should be provided.");
                }
                $filename = $files['filename_3']['name'];
                $data['filename_3'] = $filename;
                $type = $files['filename_3']['type'];
                $size = $files['filename_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 3 is greater than 1Mb');
                }
            }
            
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                    
                   if(!empty($val["name"]) ){
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of all files is greater than 2Mb');
            }
            
            
            // Format the Date of order, if provided. order number will be generated on the basis od order date
            if ( isset($data['do_o']) && !empty($data['do_o']) ) {
                $data['do_o'] = explode('/', $data['do_o']);
                //$data['number'] = "PT". $data['do_o'][2] ."-OR-". $data['do_o'][1]. $data['do_o'][0].date("hi-s") ;
                $data['do_o'] = mktime(0, 0, 0, $data['do_o'][1], $data['do_o'][0], $data['do_o'][2]);
            }
            
            
            // As date of delivery is compulsary
           /*  if(empty($data['do_d'])){
                 $messages->setErrorMessage("Select the date of Delivery.");
            } */
            // Format the Date of Delivery, if provided.
            if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
            if ( isset($data['do_e']) && !empty($data['do_e']) ) {
                $data['do_e'] = explode('/', $data['do_e']);
                $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
            }
            
            if ( isset($data['do_fe']) && !empty($data['do_fe']) ) {
                $data['do_fe'] = explode('/', $data['do_fe']);
                $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
            }
            
            /* comment on 2009-04-10
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creator's status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }
            */
            
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                    }
                }
            }

            // Validate the Executives Team.
            if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
            }
            else {
                /*
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result()); 
                    }                                               
                    $team_count = count($data['team']);
                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }                   
                }
                */
            }
			/* if(empty($data['order_of'])){
				$messages->setErrorMessage("Select Order Of.");
			} */
            if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                $messages->setErrorMessage("Order Title should be provided.");
			}
            if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                $messages->setErrorMessage("Order Type should be provided.");
			}
            if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                $messages->setErrorMessage("Order Closed By should be provided.");
			}
            
            /*
            if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                $messages->setErrorMessage("Order Particulars should be provided.");
			}
            */
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}
            
           
            $list = NULL;
            // validate particulars bof
            
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                //temp. commented 
              // $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }
            $data['query_p']='';
            $is_part = false;
            $isrenewable = false;
           
            if(isset($data['particulars'])){
                  $data['query_p'] = 'INSERT INTO '. TABLE_BILL_ORD_P .' (ord_no, particulars, p_amount, ss_div_id,s_id, sub_s_id, s_type, s_quantity, s_amount, ss_title, ss_punch_line,tax1_id,tax1_number, tax1_name,tax1_value, tax1_pvalue,tax1_amount,
tax1_sub1_id,tax1_sub1_number, tax1_sub1_name,tax1_sub1_value, tax1_sub1_pvalue,tax1_sub1_amount,
tax1_sub2_id,tax1_sub2_number, tax1_sub2_name,tax1_sub2_value, tax1_sub2_pvalue,tax1_sub2_amount,
            d_amount, stot_amount, tot_amount, discount_type, pp_amount, vendor,vendor_name, purchase_particulars, is_renewable, do_e, do_fe) VALUES ';
				$data['alltax_ids']= array();
                foreach ( $data['particulars'] as $key=>$particular ) {
					$data['tax_check_ids'][] = $data['tax1_id'][$key] ;
					$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					
					$data['alltax_ids'][]= $data['tax1_id'][$key] ;					
					$data['alltax_ids'][]= $data['tax1_sub1_id'][$key] ;					
					$data['alltax_ids'][]= $data['tax1_sub2_id'][$key] ;	
					
                    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                     
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
                                
                                if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
                                    $messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
                                   
                                }elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){
                                
                                    $messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');
                                    
                                }else{
                                  
                                    if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || empty($data['do_fe']) ) ){
                                        $messages->setErrorMessage('Service '. $data['ss_title'][$key] .' 
										is Renewal Service. Please specify Service From Date &&  Renewal Date');
                                      
                                    }else{
                                       $is_part = true;
                                      
                                       $do_e  = "";
                                       $do_fe = "";  ;
                                        if($data['is_renewable'][$key] =='1'){
                                              $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                                              $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                                              $isrenewable = true;
                                       }
                                       //Get vendor name BOF                                     
                                        $data['vendor_name'][$key]='';
                                        if(!empty($data['vendor'][$key])){
                                            $vendorSql ="SELECT billing_name FROM ".TABLE_VENDORS." 
											WHERE id='".$data['vendor'][$key]."'" ;
                                            $db->query( $vendorSql );
                                            if( $db->nf() > 0 )
                                            {
                                                while($db->next_record()){			
                                                    $data['vendor_name'][$key] = $db->f('billing_name')  ;
                                                }
                                            }
                                        }
                                       //Get vendor name EOF
                                       //Get tax number BOF
                                        $data['tax1_number'][$key]='';
                                        if(!empty($data['tax1_id'][$key])){
                                            $vendorSql ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." 
											WHERE id='".$data['tax1_id'][$key]."'" ;
                                            $db->query( $vendorSql );
                                            if( $db->nf() > 0 )
                                            {
                                                while($db->next_record()){			
                                                    $data['tax1_number'][$key] = $db->f('tax_number')  ;
                                                }
                                            }
                                        }
                                       //Get tax number EOF
									   
									   //Get subtax1 number 
                                        $data['tax1_sub1_number'][$key]='';
                                        if(!empty($data['tax1_sub1_id'][$key])){
                                            $vendorSql ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." 
											WHERE id='".$data['tax1_sub1_id'][$key]."'" ;
                                            $db->query( $vendorSql );
                                            if( $db->nf() > 0 )
                                            {
                                                while($db->next_record()){			
                                                    $data['tax1_sub1_number'][$key] = $db->f('tax_number')  ;
                                                }
                                            }
                                        }
										//Get subtax2 number 
                                        $data['tax1_sub2_number'][$key]='';
                                        if(!empty($data['tax1_sub2_id'][$key])){
                                            $vendorSql ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." 
											WHERE id='".$data['tax1_sub2_id'][$key]."'" ;
                                            $db->query( $vendorSql );
                                            if( $db->nf() > 0 ){
                                                while($db->next_record()){			
                                                    $data['tax1_sub2_number'][$key] = $db->f('tax_number')  ;
                                                }
                                            }
                                        }
                                       
                                       
                    $data['query_p'] .= "('". $data['number'] ."', '". processUserData(trim($data['particulars'][$key],',')) ."', 
					'". $data['p_amount'][$key] ."','". $data['ss_div_id'][$key] ."',
					'". $data['s_id'][$key] ."','".",".trim($data['sub_s_id'][$key],",")."," ."',
                    '". processUserData($data['s_type'][$key]) ."', 
					'". processUserData($data['s_quantity'][$key]) ."','". processUserData($data['s_amount'][$key]) ."',
                    '". processUserData($data['ss_title'][$key]) ."', '". processUserData($data['ss_punch_line'][$key]) ."',
                    '". processUserData($data['tax1_id'][$key]) ."','". processUserData($data['tax1_number'][$key]) ."',  
                    '". processUserData($data['tax1_name'][$key]) ."', 
					'". processUserData($data['tax1_value'][$key]) ."','". processUserData($data['tax1_pvalue'][$key]) ."','"
					. processUserData($data['tax1_amount'][$key]) ."',  
					'". processUserData($data['tax1_sub1_id'][$key]) ."',
					'". processUserData($data['tax1_sub1_number'][$key]) ."',  
                    '". processUserData($data['tax1_sub1_name'][$key]) ."', 
					'". processUserData($data['tax1_sub1_value'][$key]) ."',
					'". processUserData($data['tax1_sub1_pvalue'][$key]) ."',
					'". processUserData($data['tax1_sub1_amount'][$key]) ."',					
					'". processUserData($data['tax1_sub2_id'][$key]) ."',
					'". processUserData($data['tax1_sub2_number'][$key]) ."',  
                    '". processUserData($data['tax1_sub2_name'][$key]) ."', 
					'". processUserData($data['tax1_sub2_value'][$key]) ."',
					'". processUserData($data['tax1_sub2_pvalue'][$key]) ."',
					'". processUserData($data['tax1_sub2_amount'][$key]) ."',
					'". processUserData($data['d_amount'][$key]) ."', 
					'". processUserData($data['stot_amount'][$key]) ."',
                    '". processUserData($data['tot_amount'][$key]) ."', 
                    '". processUserData($data['discount_type'][$key]) ."', 
					'". processUserData($data['pp_amount'][$key]) ."',
                    '". processUserData($data['vendor'][$key]) ."', 
					'". processUserData($data['vendor_name'][$key]) ."',
					'". processUserData($data['purchase_particulars'][$key]) ."',
                    '". processUserData($data['is_renewable'][$key]) ."', '". $do_e ."','".$do_fe."'
                                        )," ;
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
			
			$data['taxpval_check'] = array_unique($data['taxpval_check']);
			//$data['taxpval_check'] = remove_array_empty_values($data['taxpval_check'],true);
			$countPVal = count($taxpval_check);
			if( $countPVal > 1 ){
				$messages->setErrorMessage('All Particulars of TAXES/VATS should be of same %');
			}
           
			
			$data['alltax_ids']=array_unique($data['alltax_ids']);
			//To check that order should be of only one tax
			$data['tax_check_ids']= array_unique(array_flip($data['tax_check_ids']));
			//$data['tax_check_ids'] = remove_array_empty_values($data['tax_check_ids'],true);		 
			$countTax = count($tax_check_ids);
			if( $countTax > 1 ){
				$messages->setErrorMessage('Particulars with Different Taxes are not allowed. All Particulars should be of 
				same TAXES/VATS or without any TAXES/VATS');
			}
           
            $data['isrenewable']=$isrenewable;
            if(!$isrenewable && (!empty($data['do_e']) || !empty($data['do_fe']) )){            
                 $messages->setErrorMessage('You have not selected any Renewable Services So Service From Date and 
				 Renewal Date should be blank.  ');
            }
            if ( !$is_part ) {
               // $messages->setErrorMessage('The Particular for the Order cannot be empty.');
            }
            else {
                if(!empty($data['query_p'])){
                    $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
                }
            }

            if ( ( !isset($data['amount']) || empty($data['amount']) ) && isset($data['particulars']) ) {
                //temp. commented 
               $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if(!empty($data['amount'])){
                    if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                        $messages->setErrorMessage("The Total Amount is not valid.");
                    }
                    else {
                        $data['balance'] = $data['amount'];
                    }
                }
            }
            // Validate particulars eof
            //exit;
            // Check for start date
            if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated end date.");
			}
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
            
           
            
            //Check Estimated hrs should not be characters
            if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
        function validateAddN(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
           // print_r($data);
            
            $files = $_FILES;
           
            // Validate the Pre Order.
            /* comment on 2009-04-10 
            $list = NULL;
            if ( Order::getList($db, $list, 'id', " WHERE po_id = '". $data['po_id'] ."'")>0 ) {
                $messages->setErrorMessage("The Pre Order has been processed already.");
            }
            else {
                if ( isset($data['po_id']) && !empty($data['po_id']) ) {
                    $query = "SELECT access_level, status FROM ". TABLE_BILL_PO
                            ." WHERE id = '". $data['po_id'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage("The Pre Order was not found in the database.");
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            if ( $db->f('status') != ACTIVE ) {
                                $messages->setErrorMessage("The Pre Order is not yet approved.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                        }
                    }
                }
            }
            */
           
            // As date of Order is compulsary
            if(empty($data['do_o'])){
                 $messages->setErrorMessage("Select the date of Order.");
            }    
			// Format the Date of order, if provided. order number will be generated on the basis od order date
            if ( isset($data['do_o']) && !empty($data['do_o']) ) {
                $data['do_o'] = explode('/', $data['do_o']);
                //$data['number'] = "PT". $data['do_o'][2] ."-OR-". $data['do_o'][1]. $data['do_o'][0].date("hi-s") ;
                $data['do_o'] = mktime(0, 0, 0, $data['do_o'][1], $data['do_o'][0], $data['do_o'][2]);
            }			
            if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            $data['number']=$data['ord_counter'] ='';
            if(!empty($data['do_o'])){				
				$detailOrdNo = getOrderNumber($data['do_o']); 				
				if(!empty($detailOrdNo)){
					$data['number'] = $detailOrdNo['number'];
					$data['ord_counter'] = $detailOrdNo['ord_counter'];
				}
			}			
            if(empty($data['number']) ){
                $messages->setErrorMessage("Order Number was not generated.");
            }
			
			if(!empty($data['ord_referred_by_name']) || !empty($data['ord_referred_by_team_name'])){
				if(empty($data['ord_referred_amount'])){
					$messages->setErrorMessage("Enter Referred Order Amount.");
				}
				if(empty($data['ord_referred_by'])){
					$messages->setErrorMessage("Select Referred By Name Properly.");
				}	
				if(empty($data['ord_referred_by_team'])){
					$messages->setErrorMessage("Select Referred By Team Name Properly.");
				}				
			}else{
				if(($data['ord_referred_amount']>0) || !empty($data['ord_referred_by'])){
					$messages->setErrorMessage("Enter Referred Order all details.");
				}
			}			
			if(($data['ord_referred_amount']>0) && empty($data['ord_referred_by']) ){
				$messages->setErrorMessage("Select Referred By Name Properly.");
			}
			if(($data['ord_referred_amount']>0) && empty($data['ord_referred_by_team']) ){
				$messages->setErrorMessage("Select Referred By Team Name Properly.");
			}			
			if(!empty($data['ord_discount_to_name']) || !empty($data['ord_discount_to'])){
				if(empty($data['ord_discount_to'])){
					$messages->setErrorMessage("Select Discount To Name Properly.");
				}
				if(empty($data['ord_discount_amt']) || empty($data['ord_discount_against_no']) || empty($data['ord_discount_against_id'])){
					$messages->setErrorMessage("Select Referral Order to give the discount.");
				}
				 
			}
            // Check for the duplicate order Number.
            $list = NULL;
            if(!empty($data['number'])){
                if ( Order::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                    $messages->setErrorMessage("An Order with the same Number already exists.<br/>Please try again.");
                }
            }
            $list = NULL;
            $data['po_filename'] = '';
            
            if((!empty($files['po_filename']) && (!empty($files['po_filename']['name'])))){
                
                $filename = $files['po_filename']['name'];
                $data['po_filename'] = $filename;
                $type = $files['po_filename']['type'];
                $size = $files['po_filename']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Purchase Order file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of purchase order file is greater than 1Mb');
                }
            }
            
            $data['filename_1'] = '';
            if(!empty($data['filecaption_1']) && empty($files['filename_1']['name'])){
                    $messages->setErrorMessage("Please select work order file1.");
            }
            if((!empty($files['filename_1']) && (!empty($files['filename_1']['name'])))){
                if(empty($data['filecaption_1'])){
                    $messages->setErrorMessage("Caption for file 1 should be provided.");
                }
                $filename = $files['filename_1']['name'];
                $data['filename_1'] = $filename;
                $type = $files['filename_1']['type'];
                $size = $files['filename_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 1 is greater than 1Mb');
                }
            }
            
            $data['filename_2'] = '';
            if(!empty($data['filecaption_2']) && empty($files['filename_2']['name'])){
                    $messages->setErrorMessage("Please select work order file2.");
            }
            if((!empty($files['filename_2']) && (!empty($files['filename_2']['name'])))){
                if(empty($data['filecaption_2'])){
                    $messages->setErrorMessage("Caption for file 2 should be provided.");
                }
                $filename = $files['filename_2']['name'];
                $data['filename_2'] = $filename;
                $type = $files['filename_2']['type'];
                $size = $files['filename_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 2 is greater than 1Mb');
                }
            }
            
            $data['filename_3'] = '';
            if(!empty($data['filecaption_3']) && empty($files['filename_3']['name'])){
                    $messages->setErrorMessage("Please select work order file3.");
            }
            if((!empty($files['filename_3']) && (!empty($files['filename_3']['name'])))){
                if(empty($data['filecaption_3'])){
                    $messages->setErrorMessage("Caption for file 3 should be provided.");
                }
                $filename = $files['filename_3']['name'];
                $data['filename_3'] = $filename;
                $type = $files['filename_3']['type'];
                $size = $files['filename_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 3 is greater than 1Mb');
                }
            }
            
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                    
                   if(!empty($val["name"]) ){
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of all files is greater than 2Mb');
            }            
            // As date of delivery is compulsary
            /* if(empty($data['do_d'])){
                 $messages->setErrorMessage("Select the date of Delivery.");
            } */
            // Format the Date of Delivery, if provided.
            if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
			
            if ( isset($data['do_e']) && !empty($data['do_e']) ) {
                $data['do_e'] = explode('/', $data['do_e']);
                $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
            }
            
            if ( isset($data['do_fe']) && !empty($data['do_fe']) ) {
                $data['do_fe'] = explode('/', $data['do_fe']);
                $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
            }
            
			
			
            /* comment on 2009-04-10
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creator's status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }
            */
            
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                    }
                }
            }

            // Validate the Executives Team.
            if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
            }
            else {
                /*
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result()); 
                    }                                               
                    $team_count = count($data['team']);
                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }                   
                }
                */
            }
			/* if(empty($data['order_of'])){
				$messages->setErrorMessage("Select Order Of.");
			} */
            if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                $messages->setErrorMessage("Order Title should be provided.");
			}
			/* 
			if ( !isset($data['order_title_mail']) || empty($data['order_title_mail']) ) {
                $messages->setErrorMessage("Order Title for Client Mail should be provided.");
			} */
            if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                $messages->setErrorMessage("Order Type should be provided.");
			}
            if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                $messages->setErrorMessage("Order Closed By should be provided.");
			}
            
            /*
            if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                $messages->setErrorMessage("Order Particulars should be provided.");
			}
            */
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}
            
           
            $list = NULL;
            // validate particulars bof
            
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                //temp. commented 
              // $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }
            $data['query_p']='';
            $is_part = false;
            $isrenewable = false;
           
            if(isset($data['particulars'])){
                  $data['query_p'] = 'INSERT INTO '. TABLE_BILL_ORD_P .' (ord_no, particulars, p_amount, ss_div_id,s_id, sub_s_id, s_type, s_quantity, s_amount, ss_title, ss_punch_line,tax1_id,tax1_number, tax1_name,tax1_value, tax1_pvalue,tax1_amount,
tax1_sub1_id,tax1_sub1_number, tax1_sub1_name,tax1_sub1_value, tax1_sub1_pvalue,tax1_sub1_amount,
tax1_sub2_id,tax1_sub2_number, tax1_sub2_name,tax1_sub2_value, tax1_sub2_pvalue,tax1_sub2_amount,
            d_amount, stot_amount, tot_amount, discount_type, pp_amount, vendor,vendor_name, purchase_particulars, is_renewable, do_e, do_fe) VALUES ';
				$data['alltax_ids']= array();
                foreach ( $data['particulars'] as $key=>$particular ) {
				
					$data['sub_sid_list'][$key]= $data['subsid'][$key]=$ss_sub_id_details=array();					
					if(!empty($data['s_id'][$key])){
						$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = ".$data['s_id'][$key]." 
						AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
						$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = ".$data['currency_id']." 
						AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;

						$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
						TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
						$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." 
						".$condition_query_sid ; 
						 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

						if ( $db->query($query) ) {
							if ( $db->nf() > 0 ) {
								while ($db->next_record()) {
									$ss_subtitle =  trim($db->f("ss_title")).",";
									$ss_subprice =  $db->f("ss_price") ;
									$ss_subid =  $db->f("ss_id") ;
									$ss_sub_id_details[] = array(
										'ss_title'=>$ss_subtitle,
										'ss_price'=>$ss_subprice,
										'ss_id'=>$ss_subid	
									);
								}
							}
						} 
				    }
				    $data['sub_sid_list'][$key]=$ss_sub_id_details;
				   
				    $sub_s_id_str='';
				    $sub_s_id_str =trim($data['subsidstr'][$key],",");
				    if(!empty($sub_s_id_str)){
						$data['subsid'][$key] = explode(",",$sub_s_id_str);
						$sub_s_id_str=",".$sub_s_id_str.",";
				    }
				   
					$tax1_id_str = $data['tax1_id'][$key] ;
					$tax1_sub1_id_str = $data['tax1_sub1_id'][$key] ;
					$tax1_sub2_id_str = $data['tax1_sub2_id'][$key] ;
					 
					if(!empty($tax1_id_str)){
						$arr = explode("#",$tax1_id_str);
						$data['tax1_name_str'][$key] = $arr[0] ;
						$data['tax1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_name_str'][$key] = 0 ;		
						$data['tax1_id_check'][$key] = 0 ;		
					}					
					if(!empty($data['tax1_value'][$key])){
						$tax1_val = $data['tax1_value'][$key] ;
						$percent =   substr($tax1_val,0,-1);
						$data['tax1_pvalue'][$key] = (float) ($percent/100);
					}else{
						$data['tax1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub1_value'][$key])){
						$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
						$percent_sub1 =   substr($tax1_sub1_val,0,-1);
						$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
					}else{
						$data['tax1_sub1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub2_value'][$key])){
						$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
						$percent_sub2 =   substr($tax1_sub2_val,0,-1);
						$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
					}else{
						$data['tax1_sub2_pvalue'][$key] =0;
					}
					
				    $data['tax_check_ids'][] = $data['tax1_id_check'][$key] ;
					$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					
					//$data['tax_check_ids'][] = $data['tax1_id'][$key] ;
					//$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					$data['tax1_sid_opt'][$key]=array();
					$data['tax1_sid_opt_count'][$key]=0;
					
					if($data['tax1_id_check'][$key]>0){
						//tax1_sub_id Options of taxes 
						//$data['tax1_sid_opt'][$key] = array('0'=>2);
						$tax_opt = array();
						$tax_id = $data['tax1_id_check'][$key];
						$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." ORDER BY tax_name ASC ";
						ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
						if(!empty($tax_opt)){
							$data['tax1_sid_opt'][$key] = $tax_opt ;  
							$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
						}
					}
					if(!empty($tax1_sub1_id_str)){
						$arr = explode("#",$tax1_sub1_id_str);
						$data['tax1_sub1_name_str'][$key] = $arr[0] ;
						$data['tax1_sub1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub1_name_str'][$key] = 0 ;		
						$data['tax1_sub1_id_check'][$key] = 0 ;		
					}
					if(!empty($tax1_sub2_id_str)){
						$arr = explode("#",$tax1_sub2_id_str);
						$data['tax1_sub2_name_str'][$key] = $arr[0] ;
						$data['tax1_sub2_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub2_name_str'][$key] = 0 ;		
						$data['tax1_sub2_id_check'][$key] = 0 ;		
					}
					
					if(!empty($data['tax1_id_check'][$key])){
						$data['alltax_ids'][]=$data['tax1_id_check'][$key] ;	
					}		
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub1_id_check'][$key] ;
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub2_id_check'][$key] ;
					}
					//tax validation bof
					if( !empty($data['tax1_id_check'][$key] )){
						if(empty( $data['tax1_value'][$key])){
							$messages->setErrorMessage('Select Tax% at Position '. ($key+1));
						}							 
						/* if(empty( $data['tax1_amount'][$key]) || $data['tax1_amount'][$key]<=0){
							$messages->setErrorMessage('Tax amount is not entered at Position '. ($key+1));
						}  */
					}
					if( !empty($data['tax1_value'][$key] )){
						if(empty( $data['tax1_id_check'][$key])){
							$messages->setErrorMessage('Select Tax at Position '. ($key+1));
						}
						/* if(empty( $data['tax1_amount'][$key])){
							$messages->setErrorMessage('Tax amount is not entered at Position '. ($key+1));
						} */
					}
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						if(empty( $data['tax1_sub1_value'][$key])){
							$messages->setErrorMessage('Select Sub1Tax% at Position '. ($key+1));
						}
						/* if(empty( $data['tax1_sub1_amount'][$key])){
							$messages->setErrorMessage('Sub1Tax amount is not entered at Position '. ($key+1));
						} */
					}
					if( !empty($data['tax1_sub1_value'][$key] )){
						if(empty( $data['tax1_sub1_id_check'][$key])){
							$messages->setErrorMessage('Select Sub1Tax at Position '. ($key+1));
						}
						/* if(empty( $data['tax1_sub1_amount'][$key])){
							$messages->setErrorMessage('Sub1Tax amount is not entered at Position '. ($key+1));
						} */
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						if(empty( $data['tax1_sub2_value'][$key])){
							$messages->setErrorMessage('Select Sub2Tax% at Position '. ($key+1));
						}
						/* if(empty( $data['tax1_sub2_amount'][$key])){
							$messages->setErrorMessage('Sub2Tax amount is not entered at Position '. ($key+1));
						} */
					}
					if( !empty($data['tax1_sub2_value'][$key] )){
						if(empty( $data['tax1_sub2_id_check'][$key])){
							$messages->setErrorMessage('Select Sub2Tax at Position '. ($key+1));
						}
						/* if(empty( $data['tax1_sub2_amount'][$key])){
							$messages->setErrorMessage('Sub2Tax amount is not entered at Position '. ($key+1));
						} */
					}
					//tax validation eof 
					
					if(empty($data['s_id'][$key])){
						$messages->setErrorMessage('Select the services.');
					}
                    if(empty($data['particulars'][$key]) && !is_numeric($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
					 	
					
                    if(empty($data['particulars'][$key]) && !is_numeric($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                
                     
						//if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
						
						if ( !is_numeric($data['p_amount'][$key]) ) {
							$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' 
							is not valid.');
						
						}else{
							if(($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || 	empty($data['do_fe']) ) ){
								$messages->setErrorMessage('Service '. $data['ss_title'][$key] .' 
								is Renewal Service. Please specify Service From Date &&  Renewal Date');
								 $isrenewable = true;
							}else {
							
								$is_part = true;
								$do_e  = "";
								$do_fe = ""; 
								  
								if($data['is_renewable'][$key] =='1'){
									  $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
									  $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
									   
									  $isrenewable = true;
								} 
							   //Get vendor name BOF                                     
								$data['vendor_name'][$key]='';
								if(!empty($data['vendor'][$key])){
									$vendorSql ="SELECT billing_name FROM ".TABLE_VENDORS." 
									WHERE id='".$data['vendor'][$key]."'" ;
									$db->query( $vendorSql );
									if( $db->nf() > 0 )
									{
										while($db->next_record()){			
											$data['vendor_name'][$key] = $db->f('billing_name')  ;
										}
									}
								}
							   //Get vendor name EOF
						   
								$data['query_p'] .= "('". $data['number'] ."',
								'". processUserData(trim($data['particulars'][$key],',')) ."', 
								'". $data['p_amount'][$key] ."','". $data['ss_div_id'][$key] ."',
								'". $data['s_id'][$key] ."','".$sub_s_id_str."',
								'". processUserData($data['s_type'][$key]) ."', 
								'". processUserData($data['s_quantity'][$key]) ."',
								'". processUserData($data['s_amount'][$key]) ."',
								'". processUserData($data['ss_title'][$key]) ."', 
								'". processUserData($data['ss_punch_line'][$key]) ."',
								'". processUserData($data['tax1_id_check'][$key]) ."',
								'". processUserData($data['tax1_number'][$key]) ."',  
								'". processUserData($data['tax1_name_str'][$key]) ."', 
								'". processUserData($data['tax1_value'][$key]) ."',
								'". processUserData($data['tax1_pvalue'][$key]) ."',
								'". processUserData($data['tax1_amount'][$key]) ."',  
								'". processUserData($data['tax1_sub1_id_check'][$key]) ."',
								'". processUserData($data['tax1_sub1_number'][$key]) ."',  
								'". processUserData($data['tax1_sub1_name_str'][$key]) ."', 
								'". processUserData($data['tax1_sub1_value'][$key]) ."',
								'". processUserData($data['tax1_sub1_pvalue'][$key]) ."',
								'". processUserData($data['tax1_sub1_amount'][$key]) ."',					
								'". processUserData($data['tax1_sub2_id_check'][$key]) ."',
								'". processUserData($data['tax1_sub2_number'][$key]) ."',  
								'". processUserData($data['tax1_sub2_name_str'][$key]) ."', 
								'". processUserData($data['tax1_sub2_value'][$key]) ."',
								'". processUserData($data['tax1_sub2_pvalue'][$key]) ."',
								'". processUserData($data['tax1_sub2_amount'][$key]) ."',
								'". processUserData($data['d_amount'][$key]) ."', 
								'". processUserData($data['stot_amount'][$key]) ."',
								'". processUserData($data['tot_amount'][$key]) ."', 
								'". processUserData($data['discount_type'][$key]) ."', 
								'". processUserData($data['pp_amount'][$key]) ."',
								'". processUserData($data['vendor'][$key]) ."', 
								'". processUserData($data['vendor_name'][$key]) ."',
								'". processUserData($data['purchase_particulars'][$key]) ."',
								'". processUserData($data['is_renewable'][$key]) ."', '". $do_e ."','".$do_fe."'
								)," ;
							
						   
							}
							
                        }
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
			
			$data['taxpval_check'] = array_unique($data['taxpval_check']);
			$countPVal = count($data['taxpval_check']);
			if( $countPVal > 1 ){
				$messages->setErrorMessage('All Particulars of TAXES/VATS should be of same %');
			}
           
			
			$data['alltax_ids']=array_unique($data['alltax_ids']);
            //To check that order should be of only one tax
			$data['tax_check_ids']= array_unique(array_flip($data['tax_check_ids']));
			$countTax = count($data['tax_check_ids']);
			if( $countTax > 1 ){
				$messages->setErrorMessage('Particulars with Different Taxes are not allowed. All Particulars should be of same TAXES/VATS
				or without any TAXES/VATS');
			}
			
			$data['isrenewable']=$isrenewable;
           /*  if(!$isrenewable && (!empty($data['do_e']) || !empty($data['do_fe']) )){            
                 $messages->setErrorMessage('You have not selected any Renewable Services So Service From Date and Renewal Date should be blank.  ');
            } */
			
            /*  $data['isrenewable'] = $isrenewable;
			if($isrenewable==true){
				//Select from and to date
				if(isset($data['sure_no_renewal']) && $data['sure_no_renewal']==1){
					$messages->setErrorMessage('Please uncheck <br/>
					I am sure, there is no possible renewal date for order. As this is a renewal order.');				  }
			}else{
				
				if( isset($data['sure_no_renewal']) && $data['sure_no_renewal']==1 ){
				
					if( (!empty($data['do_e']) || !empty($data['do_fe']))){
				
						$messages->setErrorMessage('You have not selected any Renewable Services and you agree 
						that there is no possible renewal date for order. So Service From Date and Renewal Date 
						should be blank.');
					}
				}else{
					if(empty($data['do_fe']) || empty($data['do_e'])){
						$messages->setErrorMessage("Please Select Service From Date and Service To Date");
					}
				}
			} */
			
            
			
			
			
			
            if( !$is_part ){
                // $messages->setErrorMessage('The Particular for the Order cannot be empty.');
            }else{
                if(!empty($data['query_p'])){
                    $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
                }
            }

            if( ( !isset($data['amount']) || empty($data['amount']) ) && isset($data['particulars']) ){
               //temp. commented 
               $messages->setErrorMessage("The Amount is not specified.");
            }else{
                if(!empty($data['amount'])){
                    if( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount'])){
                        $messages->setErrorMessage("The Total Amount is not valid.");
                    }else{
                        $data['balance'] = $data['amount'];
                    }
                }
            }
            // Validate particulars eof
            
            // Check for start date
            if(!empty($data['st_date'])){
                $dateArr = explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated start date.");
			}
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated end date.");
			}
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
			
            //Check Estimated hrs should not be characters
            if( isset($data['es_hrs']) && !empty($data['es_hrs'])){
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
            
           
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			} else {
				return (false);
			}
		}
		
        function validateAddOld(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
                       
            $files = $_FILES;
            // As date of Order is compulsary
            
            if(empty($data['do_o'])){
                 $messages->setErrorMessage("Select the date of Order.");
            }
            if(empty($data['do_c'])){
                 $messages->setErrorMessage("Select the date of Create.");
            }
            if(empty($data['number'])){
                 
                 $messages->setErrorMessage("Enter the old order number.");
                 
            }else{
            
                $query = "SELECT id FROM ". TABLE_BILL_ORDERS
                            ." WHERE number = '". $data['number'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() > 0) ) {
                    $messages->setErrorMessage("This Order number is already exist.");
                }
            
            }
            
            if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }else{
                // $_ALL_POST['number'] =$data['number']= getCounterNumber($db,'ORD',$data['company_id']); 
            }
            
            /*
            $data['po_filename'] = '';
            
            if((!empty($files['po_filename']) && (!empty($files['po_filename']['name'])))){
                
                $filename = $files['po_filename']['name'];
                $data['po_filename'] = $filename;
                $type = $files['po_filename']['type'];
                $size = $files['po_filename']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Purchase Order file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of purchase order file is greater than 1Mb');
                }
            }
            
            $data['filename_1'] = '';
            if(!empty($data['filecaption_1']) && empty($files['filename_1']['name'])){
                    $messages->setErrorMessage("Please select work order file1.");
            }
            if((!empty($files['filename_1']) && (!empty($files['filename_1']['name'])))){
                if(empty($data['filecaption_1'])){
                    $messages->setErrorMessage("Caption for file 1 should be provided.");
                }
                $filename = $files['filename_1']['name'];
                $data['filename_1'] = $filename;
                $type = $files['filename_1']['type'];
                $size = $files['filename_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 1 is greater than 1Mb');
                }
            }
            
            $data['filename_2'] = '';
            if(!empty($data['filecaption_2']) && empty($files['filename_2']['name'])){
                    $messages->setErrorMessage("Please select work order file2.");
            }
            if((!empty($files['filename_2']) && (!empty($files['filename_2']['name'])))){
                if(empty($data['filecaption_2'])){
                    $messages->setErrorMessage("Caption for file 2 should be provided.");
                }
                $filename = $files['filename_2']['name'];
                $data['filename_2'] = $filename;
                $type = $files['filename_2']['type'];
                $size = $files['filename_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 2 is greater than 1Mb');
                }
            }
            
            $data['filename_3'] = '';
            if(!empty($data['filecaption_3']) && empty($files['filename_3']['name'])){
                    $messages->setErrorMessage("Please select work order file3.");
            }
            if((!empty($files['filename_3']) && (!empty($files['filename_3']['name'])))){
                if(empty($data['filecaption_3'])){
                    $messages->setErrorMessage("Caption for file 3 should be provided.");
                }
                $filename = $files['filename_3']['name'];
                $data['filename_3'] = $filename;
                $type = $files['filename_3']['type'];
                $size = $files['filename_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Work Order file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of Work order file 3 is greater than 1Mb');
                }
            }
            
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                    
                   if(!empty($val["name"]) ){
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of all files is greater than 2Mb');
            }
            */
            
            // Format the Date of order, if provided. order number will be generated on the basis od order date
            if ( isset($data['do_o']) && !empty($data['do_o']) ) {
                $data['do_o'] = explode('/', $data['do_o']);
                //$data['number'] = "PT". $data['do_o'][2] ."-OR-". $data['do_o'][1]. $data['do_o'][0].date("hi-s") ;
                $data['do_o'] = mktime(0, 0, 0, $data['do_o'][1], $data['do_o'][0], $data['do_o'][2]);
            }
            
            if ( isset($data['do_c']) && !empty($data['do_c']) ) {
                $data['do_c'] = explode('/', $data['do_c']);
                //$data['number'] = "PT". $data['do_o'][2] ."-OR-". $data['do_o'][1]. $data['do_o'][0].date("hi-s") ;
                $data['do_c'] = mktime(0, 0, 0, $data['do_c'][1], $data['do_c'][0], $data['do_c'][2]);
            }
            // As date of delivery is compulsary
           
            /* if(empty($data['do_d'])){
                 $messages->setErrorMessage("Select the date of Delivery.");
            } */
            // Format the Date of Delivery, if provided.
            if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
			if(isset($data['sure_no_renewal'])){
				if( !empty($data['do_fe']) || !empty($data['do_e']) ){
			
					$messages->setErrorMessage("As you agree, that there is no possible renewal date for this order. So kindly blank the date of Service From Date and Service To Date ");
				}
			}else{
				if(empty($data['do_fe']) || empty($data['do_e'])){
					$messages->setErrorMessage("Please Select Service From Date and Service To Date ");
				}
			}
            if ( isset($data['do_e']) && !empty($data['do_e']) ) {
                $data['do_e'] = explode('/', $data['do_e']);
                $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
            }
            
            if ( isset($data['do_fe']) && !empty($data['do_fe']) ) {
                $data['do_fe'] = explode('/', $data['do_fe']);
                $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
            }
            
            /* comment on 2009-04-10
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creator's status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }
            */
            
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                    }
                }
            }

            // Validate the Executives Team.
            if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
            }
            else {
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result());
                    }
                    $team_count = count($data['team']);

                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }
                    // Uncomment the following if speed is preferred against a detailed error report as above.
                    //if ( $db->nf() < $team_count ) {
                    //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                    //}
                }
            }
            if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                $messages->setErrorMessage("Order Title should be provided.");
			}
            if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                $messages->setErrorMessage("Order Type should be provided.");
			}
            if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                $messages->setErrorMessage("Order Closed By should be provided.");
			}
            
            /*
            if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                $messages->setErrorMessage("Order Particulars should be provided.");
			}
            */
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}
            
            // Check for the duplicate Order Number.
            $list = NULL;
            if ( Order::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Order with the same Number already exists.<br/>Please try again.");
                //$messages->setErrorMessage("An Order with the same Number already exists.");
            }
            $list = NULL;
            // validate particulars bof
            
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }
            
            $data['query_p'] = 'INSERT INTO '. TABLE_BILL_ORD_P .' (ord_no, particulars, p_amount,ss_div_id, s_id, sub_s_id, s_type, s_quantity, s_amount, ss_title, ss_punch_line,tax1_id, tax1_name,tax1_value, tax1_pvalue,
            d_amount, stot_amount, tot_amount, discount_type, pp_amount, vendor, purchase_particulars, is_renewable, do_e, do_fe) VALUES ';
            $is_part = false;
            $isrenewable = false;
           
            if(isset($data['particulars'])){
                foreach ( $data['particulars'] as $key=>$particular ) {
					$data['tax_check_ids'][] = $data['tax1_id'][$key] ; 
                    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                     
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
                                
                                if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
                                    $messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
                                }elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){
                                    $messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');
                                }else{
                                  
                                    if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || empty($data['do_fe']) ) ){
                                        $messages->setErrorMessage('Service '. $data['ss_title'][$key] .' is Renewal Service. Please specify Service From Date &&  Renewal Date');
                                      
                                    }else{
                                       $is_part = true;
                                      
                                       $do_e  = "";
                                       $do_fe = "";  ;
                                       if($data['is_renewable'][$key] =='1'){
                                              $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                                              $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                                              $isrenewable = true;
                                       }
                                       $data['query_p'] .= "('". $data['number'] ."', '". processUserData($data['particulars'][$key]) ."', '". $data['p_amount'][$key] ."','". $data['ss_div_id'][$key] ."','". $data['s_id'][$key] ."','".$data['sub_s_id'][$key] ."', '". processUserData($data['s_type'][$key]) ."', '". processUserData($data['s_quantity'][$key]) ."','". processUserData($data['s_amount'][$key]) ."',
                                        '". processUserData($data['ss_title'][$key]) ."', '". processUserData($data['ss_punch_line'][$key]) ."',
                                        '". processUserData($data['tax1_id'][$key]) ."',
                                        '". processUserData($data['tax1_name'][$key]) ."', '". processUserData($data['tax1_value'][$key]) ."','". processUserData($data['tax1_pvalue'][$key]) ."',
                                        '". processUserData($data['d_amount'][$key]) ."', '". processUserData($data['stot_amount'][$key]) ."',
                                        '". processUserData($data['tot_amount'][$key]) ."', 
                                        '". processUserData($data['discount_type'][$key]) ."', '". processUserData($data['pp_amount'][$key]) ."',
                                        '". processUserData($data['vendor'][$key]) ."', '". processUserData($data['purchase_particulars'][$key]) ."',
                                        '". processUserData($data['is_renewable'][$key]) ."', '". $do_e ."','".$do_fe."'
                                        )," ;
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                $messages->setErrorMessage('No any Particulars available.');
            }
            
			//To check that order should be of only one tax
			$data['tax_check_ids']= array_unique(array_flip($data['tax_check_ids']));
			$countTax = count($data['tax_check_ids']);
			if( $countTax > 1 ){
				$messages->setErrorMessage('Particulars with more than 1 Taxes are not allowed. All Particulars should be of same TAXES/VATS
				or without any TAX');
			}
            
            $data['isrenewable']=$isrenewable;
            if(!$isrenewable && (!empty($data['do_e']) || !empty($data['do_fe']) )){            
                 $messages->setErrorMessage('You have not selected any Renewable Services So Service From Date and Renewal Date should be blank.  ');
            }
            if ( !$is_part ) {
               // $messages->setErrorMessage('The Particular for the Order cannot be empty.');
            }
            else {
                $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
            }
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Total Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            // Validate particulars eof
            
            // Check for start date
            if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated start date.");
			}
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated end date.");
			}
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
            
           
            
            //Check Estimated hrs should not be characters
            if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			 
            $files = $_FILES;
            // First check if the Order exists or not?
            $list = NULL;
            if ( Order::getList($db, $list, 'id, created_by', " WHERE id = '". $data['or_id'] ."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                $list = NULL;                
                // Validate the Pre Order.
                /*
                $list = NULL;
                if ( Order::getList($db, $list, 'id', " WHERE po_id = '". $data['po_id'] ."'")<=0 ) {
                    $messages->setErrorMessage("The Pre Order has not been processed.");
                }
                else {
                    if ( isset($data['po_id']) && !empty($data['po_id']) ) {
                        $query = "SELECT access_level, status FROM ". TABLE_BILL_PO
                                ." WHERE id = '". $data['po_id'] ."'";
                        if ( $db->query($query) && $db->nf() <= 0 ) {
                            $messages->setErrorMessage("The Pre Order was not found in the database.");
                        }
                        else {
                            $db->next_record();
                            if ( $access_level > $db->f('access_level') ) {
                                if ( $db->f('status') != COMPLETED ) {
                                    $messages->setErrorMessage("There is anomaly in the Status of the Pre Order.");
                                }
                            }
                            else {
                                $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                            }
                        }
                    }
                }*/
                $data['po_filename'] = '';
                $data['filename_1'] = '';
                $data['filename_2'] = '';
                $data['filename_3'] = '';
                
				if($data['access_extra_details']){
					if((!empty($files['po_filename']) && (!empty($files['po_filename']['name'])))){           
						$filename = $files['po_filename']['name'];
						$data['po_filename'] = $filename;
						$type = $files['po_filename']['type'];
						$size = $files['po_filename']['size'];
						$max_size = ($data['max_file_size'] * 1024);
						if ( !in_array($type, $data["allowed_file_types"] ) ) {
							 $messages->setErrorMessage("$type type Of Purchase Order file is not allowed");
						}
						if($size > $max_size){
							$messages->setErrorMessage('Size of purchase order file is greater than 1Mb');
						}
					}
					
				  
					
					if((!empty($files['filename_1']) && (!empty($files['filename_1']['name'])))){    
						if(empty($data['filecaption_1'])){
							$messages->setErrorMessage("Caption for file 1 should be provided.");
						}
						$filename = $files['filename_1']['name'];
						$data['filename_1'] = $filename;
						$type = $files['filename_1']['type'];
						$size = $files['filename_1']['size'];
						$max_size = ($data['max_file_size'] * 1024);
						if ( !in_array($type, $data["allowed_file_types"] ) ) {
							 $messages->setErrorMessage("$type type Of Work Order file 1 is not allowed");
						}
						if($size > $max_size){
							$messages->setErrorMessage('Size of Work order file 1 is greater than 1Mb');
						}
					}elseif(!empty($data['old_filename_1'])){
						if(empty($data['filecaption_1']) && !isset($data['delete_fl_1'])){
							$messages->setErrorMessage("Caption for file 1 should be provided.");
						}                  
					}
					
				   
					
					if((!empty($files['filename_2']) && (!empty($files['filename_2']['name'])))){
						
						if(empty($data['filecaption_2'])){
							$messages->setErrorMessage("Caption for file 2 should be provided.");
						}
						
						$filename = $files['filename_2']['name'];
						$data['filename_2'] = $filename;
						$type = $files['filename_2']['type'];
						$size = $files['filename_2']['size'];
						$max_size = ($data['max_file_size'] * 1024);
						if ( !in_array($type, $data["allowed_file_types"] ) ) {
							 $messages->setErrorMessage("$type type Of Work Order file 2 is not allowed");
						}
						if($size > $max_size){
							$messages->setErrorMessage('Size of Work order file 2 is greater than 1Mb');
						}
					}elseif(!empty($data['old_filename_2'])){                
						if(empty($data['filecaption_2']) && !isset($data['delete_fl_2'])){
							$messages->setErrorMessage("Caption for file 2 should be provided.");
						}
					}
					
					
					
					if((!empty($files['filename_3']) && (!empty($files['filename_3']['name'])))){
						if(!empty($data['old_filename_3'])){
							if(empty($data['filecaption_3'])){
								$messages->setErrorMessage("Caption for file 3 should be provided.");
							}
						}
						$filename = $files['filename_3']['name'];
						$data['filename_3'] = $filename;
						$type = $files['filename_3']['type'];
						$size = $files['filename_3']['size'];
						$max_size = ($data['max_file_size'] * 1024);
						if ( !in_array($type, $data["allowed_file_types"] ) ) {
							 $messages->setErrorMessage("$type type Of Work Order file 3 is not allowed");
						}
						if($size > $max_size){
							$messages->setErrorMessage('Size of Work order file 3 is greater than 1Mb');
						}
					}elseif(!empty($data['old_filename_3'])){
						if(empty($data['filecaption_3']) && !isset($data['delete_fl_3'])){
							$messages->setErrorMessage("Caption for file 3 should be provided.");
						}
					}
				
					$fileSize = 0;
					if(!empty($data['files'])){
						foreach($data['files'] as $key => $val){
							
						   if(!empty($val["name"]) ){
								if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
									$msg = $val["type"]." is not allowed ";
									$messages->setErrorMessage($msg);
								}
								$fileSize += $val["size"] ;
							}
						}
					}
					if($fileSize > $data['max_mfile_size']){
						 $messages->setErrorMessage('Size of all files is greater than 2Mb');
					}
					
				}
                /*
                if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                    $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
                }
                else {
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The Creator was not found.");
                    }
                    else {
                        $db->next_record();
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Creators' status is not Active.");
                        }
                        else {
                            $data['creator'] = processSQLData($db->result());
                        }
                    }
                }
                */
                if ( !isset($data['client']) || empty($data['client']) ) {
                    $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
                }
                else {
                    $query = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                                ." WHERE user_id = '". $data['client'] ."'";
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The selected Client was not found.");
                    }
                    else {
                        $db->next_record();
                        //print_r($db->result());
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Client is not Active.");
                        }
                        else {
                            $data['client'] = processSQLData($db->result());
                        }
                    }
                }
    
                // Validate the Executives Team.
                if ( !isset($data['team']) || empty($data['team']) ) {
                    $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
                }
                else {
                    /*
                    $team = "'". implode("','", $data['team']) ."'";                    
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                                ." WHERE user_id IN (". $team .") ";
                    if ( $db->query($query) ) {
                        $team_db = NULL;
                        while ($db->next_record()) {
                            $team_db[] = processSqlData($db->result());
                        }
                        $team_count = count($data['team']);
    
                        for ( $i=0; $i<$team_count; $i++ ) {
                            $index = array();
                            if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                            }
                            else {
                                if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                    $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                                }
                                else {
                                    $data['team_members'][$i] = $team_db[$index[0]];
                                }
                            }
                        }
                        // Uncomment the following if speed is preferred against a detailed error report as above.
                        //if ( $db->nf() < $team_count ) {
                        //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                        //}
                    }
                    */
                }
				/* if(empty($data['order_of'])){
					$messages->setErrorMessage("Select Order Of.");
				} */
                if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                    $messages->setErrorMessage("Order Title should be provided.");
                }
                //As order form differentiated as per the permissions 
                if($data['access_extra_details']){
                    if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                        $messages->setErrorMessage("Order Type should be provided.");
                    }
                    if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                        $messages->setErrorMessage("Order Closed By should be provided.");
                    }
                }
               
                
                if ( !isset($data['access_level']) ) {
                    $messages->setErrorMessage("Select the Access Level.");
                }
                elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                    $messages->setErrorMessage("Selected Access Level not found.");
                }
                
                
                 // Format the Date of order, if provided. order number will be generated on the basis od order date
				if ( isset($data['do_o']) && !empty($data['do_o']) ) {
					
					$data['do_o'] = explode('/', $data['do_o']);
					//$data['number'] = "PT". $data['do_o'][2] ."-OR-". $data['do_o'][1]. $data['do_o'][0].date("hi-s") ;
					$data['do_o'] = mktime(0, 0, 0, $data['do_o'][1], $data['do_o'][0], $data['do_o'][2]);
				}
				
                // Format the Date of Delivery, if provided.
                if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                    $data['do_d'] = explode('/', $data['do_d']);
                    $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
                }
                
                //As order form differentiated as per the permissions 
                if($data['access_extra_details']){
                    if ( isset($data['do_e']) && !empty($data['do_e']) ) {
                        $data['do_e'] = explode('/', $data['do_e']);
                        $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                    }
                    
                    if ( isset($data['do_fe']) && !empty($data['do_fe']) ) {
                        $data['do_fe'] = explode('/', $data['do_fe']);
                        $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                    }
                }
                
                // Check for the duplicate Order Number.
                $list = NULL;
                $condition_query = " WHERE number = '". $data['number'] ."' AND id != '". $data['or_id'] ."'";
                if ( Order::getList($db, $list, 'id', $condition_query)>0 ) {
                    $messages->setErrorMessage("An Order with the same Number already exists.");
                }
                $list = NULL;
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
        //As order form differentiated as per the permissions 
        if($data['access_extra_details']){
                if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                   //temp.comment $messages->setErrorMessage('Please select atleast one service.');
                }
                else {
                    foreach ( $data['service_id'] as $service ) {
                        $index = array();
                        if ( !arraySearchR($service, $lst_service) ) {
                            $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                        }
                    }
                }
            
            //$data['query_p'] = 'INSERT INTO '. TABLE_BILL_ORD_P .' (ord_no, particulars, p_amount, s_id,sub_s_id, ss_title, ss_punch_line, 
			//tax1_name,tax1_value, tax1_pvalue,d_amount,stot_amount,tot_amount,discount_type,pp_amount,vendor,purchase_particulars ) VALUES ';
				$is_part = false;
				$isrenewable = false;
				if(isset($data['particulars'])){
					foreach ( $data['particulars'] as $key=>$particular ) {
						$data['tax_check_ids'][] = $data['tax1_id'][$key] ; 
						$data['alltax_ids'][]=$data['tax1_id'][$key] ;					
						$data['alltax_ids'][]=$data['tax1_sub1_id'][$key] ;					
						$data['alltax_ids'][]=$data['tax1_sub2_id'][$key] ;	
						$data['taxpval_check'][]=$data['taxpval_check'][$key] ;	
					
						if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key]) && empty($data['p_id'][$key])){
							 $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
						}
						if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
							if ( empty($data['particulars'][$key]) ) {
								$messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
							}
							if ( empty($data['p_amount'][$key]) ) {
								$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
							}
							else {
								if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
									$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
								}
								else {
									if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
										$messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
									   
									}elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){                                
										$messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');                                
									}else{
										if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || empty($data['do_fe']) ) ){
											$messages->setErrorMessage('Service '. $data['ss_title'][$key] .' is Renewal Service. Please 
											specify Service From Date &&  Renewal Date');

										}else{
											$is_part = true;
											$do_e  = "";
											$do_fe = "";  
											if($data['is_renewable'][$key] =='1'){
												$do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
												$do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
												$isrenewable = true;
											}
										}
									}
									
								}
							}
						}
					}
				}
				$data['taxpval_check'] = array_unique($data['taxpval_check']);
				$countPVal = count($data['taxpval_check']);
				if( $countPVal > 1 ){
					$messages->setErrorMessage('All Particulars of TAXES/VATS should be of same %');
				}
				//To check that order should be of only one tax
				$data['alltax_ids']=array_unique($data['alltax_ids']);
				$data['tax_check_ids']= array_unique(array_flip($data['tax_check_ids']));
				$countTax = count($data['tax_check_ids']);
				if( $countTax > 1 ){
					$messages->setErrorMessage('Particulars with more than 1 Taxes are not allowed. All Particulars should be of same TAXES/VATS
					or without any TAX');
				}
			
				$data['isrenewable']=$isrenewable;
				/* if(!$isrenewable && (!empty($data['do_e']) || !empty($data['do_fe']) )){            
					 $messages->setErrorMessage('You are not select any Renewable Services So Service From Date and Renewal Date should be blank.  ');
				} */
				if($isrenewable && ( empty($data['do_e']) || empty($data['do_fe']))){
					$messages->setErrorMessage('As you selected Renewable Services So Service From Date and Renewal Date should not be blank.');
				}
				 
				if ( !$is_part ) {
				   //temp.comment $messages->setErrorMessage('Please check the Services entries For the Order.');
				}
				else {
					//$data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
				}

				if ( (!isset($data['amount']) || empty($data['amount'])) && isset($data['particulars']) ) {
					$messages->setErrorMessage("The Amount is not specified.");
				}
				else {
					if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
						$messages->setErrorMessage("The Total Amount is not valid.");
					}
					else {
						$data['balance'] = $data['amount'];
					}
				}
			}
            
            
            
            
             // Check for start date
            if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated start date.");
			}
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated end date.");
			}
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
            //Check Estimated hrs should not be characters
            if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdateN(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			 
            $files = $_FILES;
            // First check if the Order exists or not?
            $list = NULL;
            if ( Order::getList($db, $list, 'id, created_by', " WHERE id = '". $data['or_id'] ."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                $list = NULL;                
                
                $data['po_filename'] = '';
                $data['filename_1'] = '';
                $data['filename_2'] = '';
                $data['filename_3'] = '';
                
				if($data['access_extra_details']){
				
					if((!empty($files['po_filename']) && (!empty($files['po_filename']['name'])))){           
						$filename = $files['po_filename']['name'];
						$data['po_filename'] = $filename;
						$type = $files['po_filename']['type'];
						$size = $files['po_filename']['size'];
						$max_size = ($data['max_file_size'] * 1024);
						if ( !in_array($type, $data["allowed_file_types"] ) ) {
							 $messages->setErrorMessage("$type type Of Purchase Order file is not allowed");
						}
						if($size > $max_size){
							$messages->setErrorMessage('Size of purchase order file is greater than 1Mb');
						}
					}
					
				  
					
					if((!empty($files['filename_1']) && (!empty($files['filename_1']['name'])))){    
						if(empty($data['filecaption_1'])){
							$messages->setErrorMessage("Caption for file 1 should be provided.");
						}
						$filename = $files['filename_1']['name'];
						$data['filename_1'] = $filename;
						$type = $files['filename_1']['type'];
						$size = $files['filename_1']['size'];
						$max_size = ($data['max_file_size'] * 1024);
						if ( !in_array($type, $data["allowed_file_types"] ) ) {
							 $messages->setErrorMessage("$type type Of Work Order file 1 is not allowed");
						}
						if($size > $max_size){
							$messages->setErrorMessage('Size of Work order file 1 is greater than 1Mb');
						}
					}elseif(!empty($data['old_filename_1'])){
						if(empty($data['filecaption_1']) && !isset($data['delete_fl_1'])){
							$messages->setErrorMessage("Caption for file 1 should be provided.");
						}                  
					}
					
				   
					
					if((!empty($files['filename_2']) && (!empty($files['filename_2']['name'])))){
						
						if(empty($data['filecaption_2'])){
							$messages->setErrorMessage("Caption for file 2 should be provided.");
						}
						
						$filename = $files['filename_2']['name'];
						$data['filename_2'] = $filename;
						$type = $files['filename_2']['type'];
						$size = $files['filename_2']['size'];
						$max_size = ($data['max_file_size'] * 1024);
						if ( !in_array($type, $data["allowed_file_types"] ) ) {
							 $messages->setErrorMessage("$type type Of Work Order file 2 is not allowed");
						}
						if($size > $max_size){
							$messages->setErrorMessage('Size of Work order file 2 is greater than 1Mb');
						}
					}elseif(!empty($data['old_filename_2'])){                
						if(empty($data['filecaption_2']) && !isset($data['delete_fl_2'])){
							$messages->setErrorMessage("Caption for file 2 should be provided.");
						}
					}
					
					
					
					if((!empty($files['filename_3']) && (!empty($files['filename_3']['name'])))){
						if(!empty($data['old_filename_3'])){
							if(empty($data['filecaption_3'])){
								$messages->setErrorMessage("Caption for file 3 should be provided.");
							}
						}
						$filename = $files['filename_3']['name'];
						$data['filename_3'] = $filename;
						$type = $files['filename_3']['type'];
						$size = $files['filename_3']['size'];
						$max_size = ($data['max_file_size'] * 1024);
						if ( !in_array($type, $data["allowed_file_types"] ) ) {
							 $messages->setErrorMessage("$type type Of Work Order file 3 is not allowed");
						}
						if($size > $max_size){
							$messages->setErrorMessage('Size of Work order file 3 is greater than 1Mb');
						}
					}elseif(!empty($data['old_filename_3'])){
						if(empty($data['filecaption_3']) && !isset($data['delete_fl_3'])){
							$messages->setErrorMessage("Caption for file 3 should be provided.");
						}
					}
				
					$fileSize = 0;
					if(!empty($data['files'])){
						foreach($data['files'] as $key => $val){
							
						   if(!empty($val["name"]) ){
								if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
									$msg = $val["type"]." is not allowed ";
									$messages->setErrorMessage($msg);
								}
								$fileSize += $val["size"] ;
							}
						}
					}
					if($fileSize > $data['max_mfile_size']){
						 $messages->setErrorMessage('Size of all files is greater than 2Mb');
					}
					
				}
                /*
                if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                    $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
                }
                else {
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The Creator was not found.");
                    }
                    else {
                        $db->next_record();
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Creators' status is not Active.");
                        }
                        else {
                            $data['creator'] = processSQLData($db->result());
                        }
                    }
                }
                */
                if ( !isset($data['client']) || empty($data['client']) ) {
                    $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
                }
                else {
                    $query = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                                ." WHERE user_id = '". $data['client'] ."'";
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The selected Client was not found.");
                    }
                    else {
                        $db->next_record();
                        //print_r($db->result());
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Client is not Active.");
                        }
                        else {
                            $data['client'] = processSQLData($db->result());
                        }
                    }
                }
    
                // Validate the Executives Team.
                if ( !isset($data['team']) || empty($data['team']) ) {
                    $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
                }
                else {
                    /*
                    $team = "'". implode("','", $data['team']) ."'";                    
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                                ." WHERE user_id IN (". $team .") ";
                    if ( $db->query($query) ) {
                        $team_db = NULL;
                        while ($db->next_record()) {
                            $team_db[] = processSqlData($db->result());
                        }
                        $team_count = count($data['team']);
    
                        for ( $i=0; $i<$team_count; $i++ ) {
                            $index = array();
                            if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                            }
                            else {
                                if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                    $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                                }
                                else {
                                    $data['team_members'][$i] = $team_db[$index[0]];
                                }
                            }
                        }
                        // Uncomment the following if speed is preferred against a detailed error report as above.
                        //if ( $db->nf() < $team_count ) {
                        //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                        //}
                    }
                    */
                }
				/* if(empty($data['order_of'])){
					$messages->setErrorMessage("Select Order Of.");
				} */
                if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                    $messages->setErrorMessage("Order Title should be provided.");
                }
				
				if(!empty($data['ord_referred_by_name']) || !empty($data['ord_referred_by_team_name'])){
					if(empty($data['ord_referred_amount'])){
						$messages->setErrorMessage("Enter Referred Order Amount.");
					}
					if(empty($data['ord_referred_by'])){
						$messages->setErrorMessage("Select Referred By Name Properly.");
					}	
					if(empty($data['ord_referred_by_team'])){
						$messages->setErrorMessage("Select Referred By Team Name Properly.");
					}				
				}else{
					if(($data['ord_referred_amount']>0) || !empty($data['ord_referred_by'])){
						$messages->setErrorMessage("Enter Referred Order all details.");
					}
				}			
				if(($data['ord_referred_amount']>0) && empty($data['ord_referred_by']) ){
					$messages->setErrorMessage("Select Referred By Name Properly.");
				}
				if(($data['ord_referred_amount']>0) && empty($data['ord_referred_by_team']) ){
					$messages->setErrorMessage("Select Referred By Team Name Properly.");
				}			
				if(!empty($data['ord_discount_to_name']) || !empty($data['ord_discount_to'])){
					if(empty($data['ord_discount_to'])){
						$messages->setErrorMessage("Select Discount To Name Properly.");
					}
					if(empty($data['ord_discount_amt']) || empty($data['ord_discount_against_no']) || empty($data['ord_discount_against_id'])){
						$messages->setErrorMessage("Select Referral Order to give the discount.");
					}
				}
				/* 
				if ( !isset($data['order_title_mail']) || empty($data['order_title_mail']) ) {
					$messages->setErrorMessage("Order Title for Client Mail should be provided.");
				} 
				*/
                //As order form differentiated as per the permissions 
                if($data['access_extra_details']){
                    if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                        $messages->setErrorMessage("Order Type should be provided.");
                    }
                    if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                        $messages->setErrorMessage("Order Closed By should be provided.");
                    }
                }
               
                
                if ( !isset($data['access_level']) ) {
                    $messages->setErrorMessage("Select the Access Level.");
                }
                elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                    $messages->setErrorMessage("Selected Access Level not found.");
                }
                
                
                 // Format the Date of order, if provided. order number will be generated on the basis od order date
				if ( isset($data['do_o']) && !empty($data['do_o']) ) {
					
					$data['do_o'] = explode('/', $data['do_o']);
					//$data['number'] = "PT". $data['do_o'][2] ."-OR-". $data['do_o'][1]. $data['do_o'][0].date("hi-s") ;
					$data['do_o'] = mktime(0, 0, 0, $data['do_o'][1], $data['do_o'][0], $data['do_o'][2]);
				}
				
                // Format the Date of Delivery, if provided.
                if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                    $data['do_d'] = explode('/', $data['do_d']);
                    $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
                }
                
                //As order form differentiated as per the permissions 
                if($data['access_extra_details']){
                    if ( isset($data['do_e']) && !empty($data['do_e']) ) {
                        $data['do_e'] = explode('/', $data['do_e']);
                        $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                    }
                    
                    if ( isset($data['do_fe']) && !empty($data['do_fe']) ) {
                        $data['do_fe'] = explode('/', $data['do_fe']);
                        $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                    }
                }
                
                // Check for the duplicate Order Number.
                $list = NULL;
                $condition_query = " WHERE number = '". $data['number'] ."' AND id != '". $data['or_id'] ."'";
                if ( Order::getList($db, $list, 'id', $condition_query)>0 ) {
                    $messages->setErrorMessage("An Order with the same Number already exists.");
                }
                $list = NULL;
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
        //As order form differentiated as per the permissions 
        if($data['access_extra_details'] && $data['is_inv_cr']!=1){
		
			/* if(isset($data['sure_no_renewal'])){
				if( !empty($data['do_fe']) || !empty($data['do_e']) ){
			
					$messages->setErrorMessage("As you agree, that there is no possible renewal date for this order. So kindly blank the date of Service From Date and Service To Date ");
				}
			}else{
				if(empty($data['do_fe']) || empty($data['do_e'])){
					$messages->setErrorMessage("Please Select Service From Date and Service To Date ");
				}
			}
			 */
			
			if ( !isset($data['service_id']) || empty($data['service_id']) ) {
			   //temp.comment $messages->setErrorMessage('Please select atleast one service.');
			}
			else {
				foreach ( $data['service_id'] as $service ) {
					$index = array();
					if ( !arraySearchR($service, $lst_service) ) {
						$messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
					}
				}
			}
            
            //$data['query_p'] = 'INSERT INTO '. TABLE_BILL_ORD_P .' (ord_no, particulars, p_amount, s_id,sub_s_id, ss_title, ss_punch_line, 
			//tax1_name,tax1_value, tax1_pvalue,d_amount,stot_amount,tot_amount,discount_type,pp_amount,vendor,purchase_particulars ) VALUES ';
				$is_part = false;
				$isrenewable = false;
				if(isset($data['particulars'])){
					foreach ( $data['particulars'] as $key=>$particular ){
						$data['sub_sid_list'][$key]= $data['subsid'][$key]=$ss_sub_id_details=array();
						if(!empty($data['s_id'][$key])){
							$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = ".$data['s_id'][$key]." 
							AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
							$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = ".$data['currency_id']." 
							AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;

							$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
							TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
							$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." 
							".$condition_query_sid ; 
							$query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

							if ( $db->query($query) ) {
								if ( $db->nf() > 0 ) {
									while ($db->next_record()) {
										$ss_subtitle =  trim($db->f("ss_title")).",";
										$ss_subprice =  $db->f("ss_price") ;
										$ss_subid =  $db->f("ss_id") ;
										$ss_sub_id_details[] = array(
											'ss_title'=>$ss_subtitle,
											'ss_price'=>$ss_subprice,
											'ss_id'=>$ss_subid	
										);
									}
								}
							} 
					    }
						$data['sub_sid_list'][$key]=$ss_sub_id_details;
					    $sub_s_id_str='';
					    $sub_s_id_str =trim($data['subsidstr'][$key],",");
					    if(!empty($sub_s_id_str)){
							$data['subsid'][$key] = explode(",",$sub_s_id_str);
							$sub_s_id_str=",".$sub_s_id_str.",";
					    }
						$tax1_id_str = $data['tax1_id'][$key] ;
						$tax1_sub1_id_str = $data['tax1_sub1_id'][$key] ;
						$tax1_sub2_id_str = $data['tax1_sub2_id'][$key] ;
						 
						if(!empty($tax1_id_str)){
							$arr = explode("#",$tax1_id_str);
							$data['tax1_name_str'][$key] = $arr[0] ;
							$data['tax1_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_name_str'][$key] = 0 ;		
							$data['tax1_id_check'][$key] = 0 ;		
						}
						
						if(!empty($data['tax1_value'][$key])){
							$tax1_val = $data['tax1_value'][$key] ;
							$percent =   substr($tax1_val,0,-1);
							$data['tax1_pvalue'][$key] = (float) ($percent/100);
						}else{
							$data['tax1_pvalue'][$key] =0;
						}
						if(!empty($data['tax1_sub1_value'][$key])){
							$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
							$percent_sub1 =   substr($tax1_sub1_val,0,-1);
							$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
						}else{
							$data['tax1_sub1_pvalue'][$key] =0;
						}
						if(!empty($data['tax1_sub2_value'][$key])){
							$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
							$percent_sub2 =   substr($tax1_sub2_val,0,-1);
							$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
						}else{
							$data['tax1_sub2_pvalue'][$key] =0;
						}
						
						$data['tax1_sid_opt'][$key]=array();
						$data['tax1_sid_opt_count'][$key]=0;
						if($data['tax1_id_check'][$key]>0){
							//tax1_sub_id Options of taxes 
							//$data['tax1_sid_opt'][$key] = array('0'=>2);
							$tax_opt = array();
							$tax_id = $data['tax1_id_check'][$key];
							$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." ORDER BY tax_name ASC ";
							ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
							if(!empty($tax_opt)){
								$data['tax1_sid_opt'][$key] = $tax_opt ;  
								$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
							}
						}
						if(!empty($tax1_sub1_id_str)){
							$arr = explode("#",$tax1_sub1_id_str);
							$data['tax1_sub1_name_str'][$key] = $arr[0] ;
							$data['tax1_sub1_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_sub1_name_str'][$key] = 0 ;		
							$data['tax1_sub1_id_check'][$key] = 0 ;		
						}
						if(!empty($tax1_sub2_id_str)){
							$arr = explode("#",$tax1_sub2_id_str);
							$data['tax1_sub2_name_str'][$key] = $arr[0] ;
							$data['tax1_sub2_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_sub2_name_str'][$key] = 0 ;		
							$data['tax1_sub2_id_check'][$key] = 0 ;		
						}
						
						if(!empty($data['tax1_id_check'][$key])){
							$data['alltax_ids'][]=$data['tax1_id_check'][$key] ;	
						}		
						if(!empty($data['tax1_sub1_id_check'][$key])){					
							$data['alltax_ids'][]=$data['tax1_sub1_id_check'][$key] ;
						}
						if(!empty($data['tax1_sub2_id_check'][$key])){					
							$data['alltax_ids'][]=$data['tax1_sub2_id_check'][$key] ;
						}
					    $data['tax_check_ids'][] = $data['tax1_id_check'][$key] ;
						$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
						/*
						$data['tax_check_ids'][] = $data['tax1_id'][$key] ; 
						$data['alltax_ids'][]=$data['tax1_id'][$key] ;					
						$data['alltax_ids'][]=$data['tax1_sub1_id'][$key] ;					
						$data['alltax_ids'][]=$data['tax1_sub2_id'][$key] ;	
						$data['taxpval_check'][]=$data['taxpval_check'][$key] ;	
						
						if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key]) && empty($data['p_id'][$key])){
							 $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
						}
						if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
							if ( empty($data['particulars'][$key]) ) {
								$messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
							}
							if ( empty($data['p_amount'][$key]) ) {
								$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
							}
							else {
								if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
									$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
								}
								else {
									if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
										$messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
									   
									}elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){                                
										$messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');                                
									}else{
										if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || empty($data['do_fe']) ) ){
											$messages->setErrorMessage('Service '. $data['ss_title'][$key] .' is Renewal Service. Please 
											specify Service From Date &&  Renewal Date');

										}else{
											$is_part = true;
											$do_e  = "";
											$do_fe = "";  
											if($data['is_renewable'][$key] =='1'){
												$do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
												$do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
												$isrenewable = true;
											}
										}
									}
									
								}
							}
						}*/
						
						//tax validation bof
						if( !empty($data['tax1_id_check'][$key] )){
							if(empty( $data['tax1_value'][$key])){
								$messages->setErrorMessage('Select Tax% at Position '. ($key+1));
							}							 
							/* if(empty( $data['tax1_amount'][$key]) || $data['tax1_amount'][$key]<=0){
								$messages->setErrorMessage('Tax amount is not entered at Position '. ($key+1));
							}  */
						}
						if( !empty($data['tax1_value'][$key] )){
							if(empty( $data['tax1_id_check'][$key])){
								$messages->setErrorMessage('Select Tax at Position '. ($key+1));
							}
							/* if(empty( $data['tax1_amount'][$key])){
								$messages->setErrorMessage('Tax amount is not entered at Position '. ($key+1));
							} */
						}
						if(!empty($data['tax1_sub1_id_check'][$key])){					
							if(empty( $data['tax1_sub1_value'][$key])){
								//$messages->setErrorMessage('Select Sub1Tax% at Position '. ($key+1));
							}
							/* if(empty( $data['tax1_sub1_amount'][$key])){
								$messages->setErrorMessage('Sub1Tax amount is not entered at Position '. ($key+1));
							} */
						}
						if( !empty($data['tax1_sub1_value'][$key] )){
							if(empty( $data['tax1_sub1_id_check'][$key])){
								$messages->setErrorMessage('Select Sub1Tax at Position '. ($key+1));
							}
							/* if(empty( $data['tax1_sub1_amount'][$key])){
								$messages->setErrorMessage('Sub1Tax amount is not entered at Position '. ($key+1));
							} */
						}
						if(!empty($data['tax1_sub2_id_check'][$key])){					
							if(empty( $data['tax1_sub2_value'][$key])){
								//$messages->setErrorMessage('Select Sub2Tax% at Position '. ($key+1));
							}
							/* if(empty( $data['tax1_sub2_amount'][$key])){
								$messages->setErrorMessage('Sub2Tax amount is not entered at Position '. ($key+1));
							} */
						}
						if( !empty($data['tax1_sub2_value'][$key] )){
							if(empty( $data['tax1_sub2_id_check'][$key])){
								$messages->setErrorMessage('Select Sub2Tax at Position '. ($key+1));
							}
							/* if(empty( $data['tax1_sub2_amount'][$key])){
								$messages->setErrorMessage('Sub2Tax amount is not entered at Position '. ($key+1));
							} */
						}
						//tax validation eof
						if(empty($data['s_id'][$key])){
							$messages->setErrorMessage('Select the services.');
						}
						if(empty($data['particulars'][$key]) && !is_numeric($data['p_amount'][$key]) && empty($data['p_id'][$key])){
							 $messages->setErrorMessage('The Particular field and amount at Position 
							 '. ($key+1) .'cannot be empty.');
						}
						if ( !empty($data['particulars'][$key]) || is_numeric($data['p_amount'][$key]) ) {
							if ( empty($data['particulars'][$key]) ) {
								$messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
							}
							if ( !is_numeric($data['p_amount'][$key]) ) {
								$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
							}
							else {
								/* 
								if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
									$messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
								}else { 
								*/
									
									if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || 
										empty($data['do_fe']) ) ){
										$messages->setErrorMessage('Service '. $data['ss_title'][$key] .' 
										is Renewal Service. Please specify Service From Date &&  Renewal Date');

									}else{
										$is_part = true;
										$do_e  = "";
										$do_fe = "";  
										if($data['is_renewable'][$key] =='1'){
										  $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
										  $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
										  $isrenewable = true;
										}
										
									}
									
									
								//} 
							}
						}
					}
				}
				
					
				$data['taxpval_check'] = array_unique($data['taxpval_check']);
				$countPVal = count($data['taxpval_check']);
				if( $countPVal > 1 ){
					$messages->setErrorMessage('All Particulars of TAXES/VATS should be of same %');
				}
				//To check that order should be of only one tax
				$data['alltax_ids']=array_unique($data['alltax_ids']);
				$data['tax_check_ids']= array_unique(array_flip($data['tax_check_ids']));
				$countTax = count($data['tax_check_ids']);
				if( $countTax > 1 ){
					$messages->setErrorMessage('Particulars with more than 1 Taxes are not allowed. All Particulars should be of same TAXES/VATS or without any TAX');
				}
			
				  
				/* 
				$data['isrenewable']=$isrenewable;
				if($isrenewable==true){
					//Select from and to date
					if(isset($data['sure_no_renewal']) && $data['sure_no_renewal']==1){
						$messages->setErrorMessage('Please uncheck <br/>
						I am sure, there is no possible renewal date for order. As this is a renewal order.');				  	  }
				}else{
					
					if( isset($data['sure_no_renewal']) && $data['sure_no_renewal']==1 ){
					
						if( (!empty($data['do_e']) || !empty($data['do_fe']))){
					
							$messages->setErrorMessage('You have not selected any Renewable Services and you agree 
							that there is no possible renewal date for order. So Service From Date and Renewal Date 
							should be blank.');
						}
					}else{
						if(empty($data['do_fe']) || empty($data['do_e'])){
							$messages->setErrorMessage("Please Select Service From Date and Service To Date");
						}
					}
				}
				*/
				$data['isrenewable'] = $isrenewable ;
				/* 
				if(!$isrenewable && (!empty($data['do_e']) || !empty($data['do_fe']) )){            
					$messages->setErrorMessage('You have not selected any Renewable Services So Service From Date and Renewal Date should be blank.');
				} 
				*/
				if($isrenewable && ( empty($data['do_e']) || empty($data['do_fe']))){
					$messages->setErrorMessage('As you selected Renewable Services So Service From Date and Renewal Date should not be blank.');
				}
				 
				if ( !$is_part ) {
				   //temp.comment $messages->setErrorMessage('Please check the Services entries For the Order.');
				} else {
					//$data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
				}

				if ( (!isset($data['amount']) || empty($data['amount'])) && isset($data['particulars']) ) {
					$messages->setErrorMessage("The Amount is not specified.");
				}
				else {
					if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
						$messages->setErrorMessage("The Total Amount is not valid.");
					}
					else {
						$data['balance'] = $data['amount'];
					}
				}
			}
            
            
            
            
             // Check for start date
            if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated start date.");
			}
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }else{
				$messages->setErrorMessage("Select estimated end date.");
			}
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
            //Check Estimated hrs should not be characters
            if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
			
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		
        function validateBifurcationAdd(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            if(isset($data['service'])){
                foreach ( $data['service'] as $key=>$service ) {
                    if(empty($data['service'][$key])){
                         $messages->setErrorMessage('The Service field at Position '. ($key+1) .' cannot be empty.');
                    }
                    
                    if ( empty($data['price'][$key]) ) {
                        $messages->setErrorMessage('The Price of Service '. ($key+1) .' cannot be empty.');
                    }
                    else {
                        if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['price'][$key]) ) {
                            $messages->setErrorMessage('The Price of Service '. ($key+1) .' is not valid.');
                        }
                        
                    }
                    
                }
            }else{            
                 $messages->setErrorMessage('Select Services.');
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        function getDetailsBifurcation( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
           
            $query .= " FROM ". TABLE_ORD_BIFURCATION;
          
               
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
        function deleteBifurcation($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $query = "DELETE FROM ". TABLE_ORD_BIFURCATION."  WHERE id = '$id'";
                                            
            if ( $db->query($query) ) {            
                $messages->setOkMessage("The Order Bifurcation has been deleted.");
            }
        }
        /**
         * This function is called after a new Order has been created
         * against a Pre Order. This function is called only once for 
         * each Order.
         *
         */
        function setPreOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_BILL_PO
                        ." SET status = '$status'"
                        ." WHERE id = '$id'";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		
		/**
		 * This function is used to retrieve the Members of the Order.
		 * 
		 */
		function getTeamMembers($id, &$team_members, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$order = NULL;
			if ( Order::getList($db, $order, 'team', " WHERE id = '$id'" ) > 0 ) {
				include_once ( DIR_FS_INCLUDES .'/user.inc.php');

				$order = $order[0];
				$order['team'] = "'". implode("','", (explode(',', $order['team']))) ."'";
                $order['team_members']= '';
                if ( User::getList($db, $team_members, 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $order['team'] .")") > 0 ) {
					return (true);
				}
			}
			return (false);
		}

        
		
		function getDeliveryDate(&$db, $order_id) {
			$query = "SELECT do_d FROM ". TABLE_BILL_ORDERS
						." WHERE id = '". $order_id ."'";
			$db->query($query);
			if ( $db->nf()>0 && $db->next_record() ) {
				return ($db->f('do_d'));
			}
			return ('');
		}
        
        //Function required for displaying the order_id + order_title from  work_timeline
        function getOrderList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
               $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_ORDERS;
            $query .= " LEFT JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client ";
            $query .= " LEFT JOIN ". TABLE_WORK_TL." ON ". TABLE_WORK_TL .".order_id = ". TABLE_BILL_ORDERS .".id ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
					
					if ( strpos($required_fields, 'team') > 0 ) {
						include_once ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$count = count($list);
						for ( $i=0; $i<$count; $i++ ) {
							// Read the Team Member Details.
							$list[$i]['team'] = "'". implode("','", (explode(',', $list[$i]['team']))) ."'";
							$list[$i]['team_members']= '';
							User::getList($db, $list[$i]['team_members'], 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $list[$i]['team'] .")");
							
							// Read the latest Time Line update.
							include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
							$list[$i]['work'] = NULL;
							//WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, order_title, to_id, by_id, do_assign, comments, status');
							WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, to_id, by_id, do_assign, comments, status');
							$list[$i]['work'] 		= $list[$i]['work'][0];
							//$list[$i]['order_title']= $list[$i]['work']['order_title'];
							
							$list[$i]['comments'] 	= $list[$i]['work']['comments'];
							User::getList($db, $list[$i]['work']['by'], 'user_id, number, f_name, l_name, email', "WHERE user_id = '". $list[$i]['work']['by_id'] ."'");
							$list[$i]['work']['by'] = $list[$i]['work']['by'][0];
						}
					}
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
		
        
        
        /**
		 * This function is used to retrieve the details of the order.
		 *
		 */
		function getOrder($ord_id, &$order, &$extra) {
           // include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
			
			foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
            $condition_query = " WHERE (". TABLE_BILL_ORDERS .".id = '". $ord_id ."' "
                                ." OR ". TABLE_BILL_ORDERS .".number = '". $ord_id ."')";
            $fields = TABLE_BILL_ORDERS .'.*'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.status AS c_status';
						
			if ( Order::getDetails($db, $order, $fields, $condition_query) > 0 ) {
                $order = $order['0'];

				// Set up the Client Details field.
				$order['client_details']= $order['c_f_name'] .' '. $order['c_l_name']
												.' ('. $order['c_number'] .')'
												.' ('. $order['c_email'] .')';
				
				// Set up the dates.
//				$invoice['do_i']  = explode(' ', $invoice['do_i']);
//				$temp               = explode('-', $invoice['do_i'][0]);
//				$invoice['do_i']  = NULL;
//				$invoice['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
//				
//				$invoice['do_d']  = explode(' ', $invoice['do_d']);
//				$temp               = explode('-', $invoice['do_d'][0]);
//				$invoice['do_d']  = NULL;
//				$invoice['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
//				
//				$invoice['do_e']  = explode(' ', $invoice['do_e']);
//				$temp               = explode('-', $invoice['do_e'][0]);
//				$invoice['do_e']  = NULL;
//				$invoice['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				
				// Read the Particulars.
				$temp = NULL;
				$temp_p = NULL;
				$condition_query = "WHERE ord_no = '". $order['number'] ."'";
				Order::getParticulars($db, $temp, '*', $condition_query);
                if(!empty($temp)){
                    foreach ( $temp as $pKey => $parti ) {
                      
                        $temp_p[]=array(
                                        'p_id'=>$temp[$pKey]['id'],
                                        'particulars'=>$temp[$pKey]['particulars'],
                                        'p_amount' =>$temp[$pKey]['p_amount'] ,                                   
                                        's_id' =>$temp[$pKey]['s_id'] ,                                   
                                        'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                        'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                        'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                        'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                        'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                        'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                        'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                        'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                        'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                        );
                    }
                }
                $order['particular_details']=$temp_p ;
				$condition_query = '';
				
				
				
				
            }
            else {
                $messages->setErrorMessage("The Order was not found or you do not have the Permission to access this Order.");
            }
								
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
	  	  
	function createPdfDmFile($data) {        
		$variables['images'] = DIR_WS_IMAGES_NC ;
		$body=  "<style type=\"text/css\" rel=\"stylesheet\">     
				body {
					
					margin-right:auto;
					margin-left:auto;
				}
				
				#content {
				margin:0;
				width: 100%;
				text-align:center;
				}
				#content-main {
				width: 731px;
				text-align:center;
				margin-left:auto;
				margin-right:auto;
				}
				
				.heading{
					font-family:Arial, Verdana, 'San serif';
					font-size:18px;
					color:#000000;
					text-decoration:none;
					font-weight:bold;
				}    
				.sub-heading{
					font-family:Arial, Verdana, 'San serif';
					font-size:11px;
					color:#686868;
					line-height:15px;
					text-decoration:none;
					font-weight:regular;
				}
				.invoice-border{ 
					 background:url(".$variables['images']."/invoice-repeater-new.jpg);
					 background-repeat:repeat-y;
					 width:253px;
				}
				.invoice-text{
					font-family:Arial, Verdana, 'San serif';
					font-size:12px;
					color:#000000;
					text-decoration:none;
					font-weight:bold;
				}
				
				.content-heading{
					font-family:Arial, Verdana, 'San serif';
					font-size:12px;
					color:#FFFFFF;
					text-decoration:none;
					font-weight:bold;
				}
				.address-text{
					font-family:Arial, Verdana, 'San serif';
					font-size:10px;
					color:#FFFFFF;
					text-decoration:none;
					font-weight:regular;
					lignt-height:13px;
				}
				.address-text-small{
					font-family:Arial, Verdana, 'San serif';
					font-size:9px;
					color:#FFFFFF;
					text-decoration:none;
					font-weight:regular;
					lignt-height:13px;
				
				}
				.green-text{
					font-family:Arial, Verdana, 'San serif';
					font-size:11px;
					color:#176617;
					text-decoration:none;
					font-weight:regular;
					lignt-height:13px;
				}
				.white-text{
					font-family:Arial, Verdana, 'San serif';
					font-size:11px;
					color:#FFFFFF;
					text-decoration:none;
					font-weight:regular;
					lignt-height:13px;
				}
				.white-text-12{
					font-family:Arial, Verdana, 'San serif';
					font-size:12px;
					color:#FFFFFF;
					text-decoration:none;
					font-weight:regular;
					lignt-height:13px;
				}
				.design-text{
					font-family:Arial, Verdana, 'San serif';
					font-size:11px;
					color:#797979;
					text-decoration:none;
					font-weight:regular;
					lignt-height:13px;
				}
				.custom-content{
					font-family:arial,verdana,sans-serif;
					font-size:11px;
					color:#646464;
				}	
				
				.content{
					font-family:arial,verdana,sans-serif;
					font-size:11px;
					color:#4f4f4f;
					lignt-height:10px;
				}	
				.contentsub{
					font-family:arial,verdana,sans-serif;
					font-size:10px;
					color:#4f4f4f;
					lignt-height:10px;
					font-weight:bold;
				}
				.contenthead{
					font-family:arial,verdana,sans-serif;
					font-size:13px;
					color:#4f4f4f;
					lignt-height:11px;
				}		
				.content-small{
					font-family:arial,verdana,sans-serif;
					font-size:6px;
					color:#4f4f4f;
				}	
				.content-small-term{
					font-family:arial,verdana,sans-serif;
					font-size:6px;
					color:#726e6e;
				}
				.content-small-7{
					font-family:arial,verdana,sans-serif;
					font-size:7px;
					color:#4f4f4f;
				}
				.content-small-8{
					font-family:arial,verdana,sans-serif;
					font-size:8px;
					color:#4f4f4f;
				}
				.content-small-10{
					font-family:arial,verdana,sans-serif;
					font-size:10px;
					color:#4f4f4f;
				}
				<!-- 9px-->
				
				.spacer{background:url(".$variables['images']."/spacer.gif) repeat;}
				.bg1{background-color:#898989;}
				.bg2{background-color:#CCCCCC;}
				.bg3{background-color:#f4f4f4;}
				.bg4{background-color:#ba1a1c;}
				.bg5{background-color:#956565;}
				.bg6{background-color:#BEBE74;}
				.bg7{background-color:#48b548;}
				.bg8{background-color:#000000;}
				.bg9{background-color:#bf1f21;}
				
				.bdb1{border-bottom:1px solid #000000;}
				
				.b {font-weight:bold;}
				
				.al {text-align:left;}
				.ac {text-align:center;}
				.ar {text-align:right;}
				
				.vt {vertical-align:top;}
				.vm {vertical-align:middle;}
				.vb {vertical-align:bottom;}
				
				.fs08 {font-size:0.8em;}
				.fs11 {font-size:1.1em;}
				.fst11{font-size:1.1em; font-family:'Arial Narrow',Verdana,Arial,Sans-Serif;}
				.fs15 {font-size:1.5em;}
				
				.lh15 {line-height:1.5em;}
				.lh10 {line-height:1em;}
				
				.pl2{padding-left:2px;}
				.pl5{padding-left:5px;}
				.pl8{padding-left:8px;}
				
				.pl10{padding-left:10px;}
				.pl11{padding-left:11px;}
				.pl15{padding-left:15px;}
				.pl20{padding-left:20px;}
				.pl60{padding-left:60px;}
				.pl42{padding-left:42px;}
				
				.pr2{padding-right:2px;}
				.pr5{padding-right:5px;}
				.pr10{padding-right:10px;}
			  
				
				.pb2{padding-bottom:2px;}
				.pb3{padding-bottom:3px;}
				.pb4{padding-bottom:4px;}
				.pb5{padding-bottom:5px;}         
				.pb10{padding-bottom:10px;}
				.pb13{padding-bottom:13px;}
				.pb20{padding-bottom:20px;}
				.pb25{padding-bottom:25px;}
				.pb30{padding-bottom:30px;}
				.pb40{padding-bottom:40px;}
				.pb50{padding-bottom:50px;}
				.pb100{padding-bottom:100px;}
				
				.pt1{padding-top:1px;}
				.pt2{padding-top:2px;}
				.pt3{padding-top:3px;}
				.pt4{padding-top:4px;}
				.pt5{padding-top:5px;}
				.pt6{padding-top:6px;}
				.pt8{padding-top:8px;}
				.pt10{padding-top:10px;}
				.pt15{padding-top:15px;}
				.pt20{padding-top:20px;}
				.pt25{padding-top:25px;}		
				.pt30{padding-top:30px;}       
				.pt40{padding-top:40px;}       
				.pt32{padding-top:32px;}
				.pt45{padding-top:45px;}
				
				.b1{border:collapse;border:1px solid #000000;}
				.wp100{width:100%;}
				.wp90{width:90%;}
				.wp70{width:70%;}
				.wp65{width:65%;}
				.wp60{width:60%;}
				.wp55{width:55%;}
				.wp50{width:50%;}
				.wp45{width:45%;}
				.wp40{width:40%;}
				.wp35{width:35%;}
				.wp30{width:30%;}
				.wp28{width:28%;}
				.wp25{width:25%;}
				.wp20{width:20%;}
				.wp15{width:15%;}
				.wp13{width:13%;}
				.wp12{width:12%;}
				.wp10{width:10%;}
				.wp9{width:9%;}
				.wp5{width:5%;}
				.wp6{width:6%;}
				.wp4{width:4%;}        
			   
				.wpx7{width:7px;}
				.wpx9{width:9px;}
				.wpx10{width:10px;}
				.wpx20{width:15px;}
				.wpx707{width:707px;}		 
				.wpx711{width:711px;}
				.wpx712{width:712px;}
				.wpx729{width:729px;}
				.wpx731{width:731px;}
				.wpx330{width:345px;}
				.wpx337{width:352px;}
				.wpx348{width:348px;}
				
				
			   
				.wpx711{width:711px;}
				.wpx707{width:707px;}
				.wpx9{width:9px;}
				.wpx713{width:713px;}
				.wpx347{width:347px;}
				.wpx253{width:253px;}
				.wpx335{width:335px;}
				
				.hp30{
					height:27px;
					border-top:1px solid #dddddd;
				}
				.hp22{height:22px;}
				.hp20{height:20px;}
				.hp4{height:4px;}
				.hp9{height:9px;}
				.hp8{height:8px;}
				.hp17{height:19px;}
				
				
				.borlr{
					border-left:1px solid #dddddd;
					border-right:1px solid #dddddd;
				}
				
				.borl{
					border-left:1px solid #dddddd;
				}
				
				.borr{
					border-right:1px solid #dddddd;
				}
				
				.borb{
					border-bottom:1px solid #dddddd;
				}
				
				div.row {
					clear:both;
					float:left;
				}
				div.coloumn {
					float:left;
				}
				div.coloumn-right {
					float:right;
				}        
				.seccolor{background-color:#999999;}
				.bg2{background-color:#f4f4f4;}		
				div.clear{
					clear:both;
					background-color:#f4f4f4;
				}
				.header-text{font-size:11px; font-family:arial; font-weight:bold;}
		</style>        
		<div id=\"content\">
		<div id=\"content-main\">
		<div class=\"row wp100 al\" style=\"height:800px\">
			<div class=\"row al pt45\">
				<div><img src=\"".$variables['images']."/smeerp-inv-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\"></div>
			</div>
			<div class=\"row wpx731 al pb10 pt1\">
				<div class=\"coloumn al content wp65\">
					Motivated to Serve&nbsp;Determined to Excel <sup>TM</sup>
				</div>
				<div class=\"coloumn-right invoice-text ar\">";			
					if( $data['do_i_chk'] >=$data['do_iso'] ){
						$body.=" An ISO 9001:2008 CERTIFIED COMPANY ";
					}  
		$body.="</div>
			</div>
			<div class=\"row wpx731 pb5 pt30\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
				<tr>
					<td width=\"478px\" class=\"pl42 al\" valign=\"top\">";
			if (!empty($data['old_billing_address'])){
			$body.="<div class=\"heading pb2\">".$data['billing_name']."</div>
					 <div class=\"sub-heading\">".nl2br($data['old_billing_address'])."</div>";
			}else{
			$body.="<div class=\"heading pb2 al\">".$data['billing_name']."</div>
			<div class=\"sub-heading al\">".nl2br($data['b_addr'])."</div>      
			<div class=\"sub-heading al\">";
				if (!empty($data['b_addr_city']))
				{
					$body.=$data['b_addr_city'];
				}
				if (!empty($data['b_addr_zip']))
				{
					$body.="&nbsp;". $data['b_addr_zip'];
				}
				if (!empty($data['b_addr_state']))
				{
					$body.="&nbsp;". $data['b_addr_state'];
				}
				if (!empty($data['b_addr_country']))
				{
					$body.="<br/>". $data['b_addr_country'];
				}    
			$body.="</div> "  ;
			}
			$body.="</td>
			<td width=\"253px\" valign=\top\" class=\"ar\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"ar\">
				<tr>
					<td class=\"b al invoice-text pl5 pb3\">".$data['invoice_title']."</td>
				</tr>
				<tr>
					<td><img src=\"".$variables['images']."/invoice-top-corner-new.jpg\" width=\"253px\" height=\"9\"  border=\"0\" /></td>
				</tr>
				<tr>
					<td class=\"invoice-border\">
						<div class=\"row wpx253\">
						<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">Relationship No.</span></div>
						<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$data['client']['number']."</span></div>
						</div>";
						
						$body.="<div class=\"row wpx253\">
			<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\"> Order No.</span></div>
			<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$data['or_no']."</span></div>
			</div>";
						$body.="<div class=\"row wpx253\">
						<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">".$data['profm']." DM No.</span></div>
						<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$data['number']."</span></div>
						</div>";
						
			if (!empty($data['do_i']))
			{
			$temp=$temp1=$dtArr=null;
			$dd  =$mm=$yy='';
			$temp = explode(" ",$data['do_i']) ;
			$temp1=$temp[0];
			$dtArr =explode("-",$temp1) ;
			$dd =$dtArr[2];
			$mm =$dtArr[1];
			$yy=$dtArr[0];
			//$do_i = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
			$do_i = date("d M Y",$data['do_i']);

			$body.="<div class=\"row wpx253\">
			<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">".$data['profm']." DM Date</span></div>
			<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$do_i."</span></div>
			</div>";
			}
			if (!empty($data['currency_abbr']))
			{
			/* $body.="<div class=\"row wpx253\">
			<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">".$data['profm']." Invoice Currency</span></div>
			<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$data['currency_abbr']."</span></div>
			</div>"; */
			}

			
			 
			

			if (!empty($data['do_d']))
			{
			/* $do_d = date("d M Y",$data['do_d']);
			$body.="<div class=\"row wpx253\">
			<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">Payment Due Date</span></div>
			<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$do_d."</span></div>
			</div>"; */
			}

			if (!empty($data['do_fe']) && $data['show_period_chk'] ==1){
				/* 
				$temp=$temp1=$dtArr=null;
				$dd  =$mm=$yy='';
				$temp = explode(" ",$data['do_fe']) ;
				$temp1=$temp[0];
				$dtArr =explode("-",$temp1) ;
				$dd =$dtArr[2];
				$mm =$dtArr[1];
				$yy=$dtArr[0];
				$do_fe = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
				$body.="<div class=\"row wpx253\">
				<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\"> Service Start Date</span></div>
				<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$do_fe."</span></div>
				</div>";  */
			}
			if(!empty($data['do_e'] ) && $data['show_period_chk'] ==1){
				/* 
				$temp=$temp1=$dtArr=null;
				$dd  =$mm=$yy='';
				$temp = explode(" ",$data['do_e']) ;
				$temp1=$temp[0];
				$dtArr =explode("-",$temp1) ;
				$dd =$dtArr[2];
				$mm =$dtArr[1];
				$yy=$dtArr[0];
				$do_e = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));			 
				$body.="<div class=\"row wpx253\">
				<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">Renewal/Expiry Date</span></div>
				<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$do_e."</span></div>
				</div>"; 
				*/
			}
			$body.="</td>
					</tr>
					<tr>
						<td><img src=\"".$variables['images']."/invoice-bottom-corner-new.jpg\"  width=\"253px\" height=\"9\"  border=\"0\" /></td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
		</div> ";
			
		//Main div particulars area bof
		$body.="
		<div class=\"row wp100 pt2\" style=\"height:450px\">
			<div class=\"row wp100 pt5\">
			<table cellspacing=\"0\" class=\"bor\" cellpadding=\"0\" border=\"0px\" width=\"731px\" >
			<tr>
				<td width=\"".$data['wd1']."px\" class=\"bg1\" height=\"30px\"><img src=\"".$variables['images']."/first-left-corner.jpg\" width=\"10px\" 
				height=\"30px\" border=\"0\" /></td>
				<td class=\"bg1 al pl5\" width=\"".$data['wdinvp']."px\" height=\"30px\"><span class=\"content-heading b\">Particulars</span></td>
				<td class=\"bg1 ar\" width=\"".$data['wdpri']."px\" height=\"30px\"><span class=\"content-heading b\">Rate</span></td>";
				if($data['show_nos'] ==1){
					$body.="<td class=\"bg1 ar\" width=\"".$data['wdnos']."px\" height=\"30px\"><span class=\"content-heading b\">Unit</span></td>";
				}
				$body.="<td class=\"bg1 ar\" width=\"".$data['wdamt']."px\" height=\"30px\"><span class=\"content-heading b\">Amt.</span></td>";
				if($data['show_discount'] ==1){
				 $body.= "<td class=\"bg1 ar\" width=\"".$data['wddisc']."px\" height=\"30px\"><span class=\"content-heading b\">Disc.</span></td>
				 <td class=\"bg1 ar\" width=\"".$data['wdst']."px\" height=\"30px\"><span class=\"content-heading b\">SubTot</span></td>";

				}
				$body.= "<td width=\"".$data['wd2']."px\" class=\"bg1\" height=\"30px\"><img src=\"".$variables['images']."/first-right-corner.jpg\" width=\"9px\" height=\"30px\" border=\"0\" /></td>
			</tr>";    
			$k=1;           
			foreach($data['particulars'] as $key=>$particular)
			{
				if(!empty($particular['ss_punch_line'])){
					$body.= "<tr>
						<td class=\"bg3 borl\" height=\"30px\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" height=\"1px\" border=\"0\" /></td>
						<td class=\"bg3 vt al content pt8 b pb5 pl5\" height=\"30px\">";
						$body.=$k.". ".nl2br($particular['ss_punch_line'])."</td>";
						$body.="<td class=\"bg3 al content pt8 b pb5 pl5\" height=\"30px\" colspan=\"".$data['colsSpanNo1']."\">&nbsp;</td>";
						$body.="<td class=\"bg3 borr\" height=\"30px\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" height=\"1px\"  border=\"0\" /></td>
					 </tr>";
				}
				$body.="<tr>
					<td class=\"pb5 bg3 borl\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>
					<td class=\"bg3 pb5 content vt al pl10 pl5\" height=\"30px\">".nl2br($particular['particulars'])."</td>
					<td class=\"bg3 pb5 content vt ar\" height=\"30px\">".$particular['p_amount']."</td>";
					if($data['show_nos'] ==1){ 
					 $body.="<td class=\"bg3 pb5 vt content ar\" height=\"30px\">".$particular['s_quantity']." ".$particular['s_type']."</td>" ;
					}
					$body.="<td class=\"bg3 pb5 vt content ar\" height=\"30px\">".$particular['s_amount']."</td>";
					if($data['show_discount'] ==1){
					 $body.="<td class=\"bg3 pb5 vt content ar\" height=\"30px\">".$particular['d_amount']."</td>";
					 $body.="<td class=\"bg3 pb5 vt content ar\" height=\"30px\">".$particular['stot_amount']."</td>";
					}  		
					$body.=" <td class=\"pb5 bg3 borr\" height=\"30px\">
						<img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" height=\"1px\"  border=\"0\" /></td>
				</tr>";
				$k++;	
			}
			$body.="<tr>
						<td width=\"10px\" class=\"bg3 vb\"><img src=\"".$variables['images']."/first-bottom-left-corner.jpg\"  width=\"10px\" height=\"9px\"  border=\"0\" /></td>";
						$body.="<td class=\"bg3 borb vb\" colspan=\"".$data['colsSpanNo2']."\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>";
						$body.="<td width=\"9px\" class=\"bg3 vb\"><img src=\"".$variables['images']."/first-bottom-right-corner.jpg\"  width=\"9px\" height=\"9px\"  border=\"0\" /></td>
					</tr>
			</table>
			</div> ";

			$body.="<div class=\"row wp100 pt5 pb5\">
						<table align=\"right\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">	";
						if(!empty($data['tax1_name'])){			
							$body.="<tr>				
							 <td align=\"right\" width=\"500px\"><span class=\"content ar b\">Total :</span></td>
							 <td align=\"right\" width=\"100px\">
							 <span class=\"content pr10 b\">&nbsp;".$data['sub_total_amount']."</span></td>
							</tr>
							<tr>				
								<td align=\"right\" width=\"500px\"><span class=\"content ar\">".$data['tax1_name']." @ ".$data['tax1_value']." :</span></td>
								<td align=\"right\" width=\"100px\">
								<span class=\"content pr10 \">&nbsp;".$data['tax1_total_amount']."</span></td>
							</tr>";
							if(!empty($data['tax1_sub1_name'])){
								$body.="<tr>				
								<td align=\"right\" width=\"500px\"><span class=\"content ar\">".$data['tax1_sub1_name']." @ ".$data['tax1_sub1_value']." :</span></td>
								<td align=\"right\" width=\"100px\">
								<span class=\"content pr10\">&nbsp;".$data['tax1_sub1_total_amount']."</span></td>
								</tr>" ;
							}
							if (!empty($data['tax1_sub2_name'])){
								$body.="<tr>				
									<td align=\"right\" width=\"500px\"><span class=\"content ar \">".$data['tax1_sub2_name']." @ ".$data['tax1_sub2_value']." :</span></td>
									<td align=\"right\" width=\"100px\">
									<span class=\"content pr10\">&nbsp;".$data['tax1_sub2_total_amount']."</span></td>
								</tr>" ;
							}
						}
						if (!empty($data['round_off_op']) && !empty($data['round_off']) ){
							$body.="<tr>				
								<td align=\"right\" width=\"500px\"><span class=\"content ar \">Round off :</span></td>
								<td align=\"right\" width=\"100px\"><span class=\"content pr10\">"
								.$data['round_off_op']."&nbsp;".$data['round_off']."</span></td>
								</tr>" ;
						}
						$body.="<tr>				
								<td align=\"right\" width=\"500px\"><span class=\"content ar b\">Grand Total :</span></td>";
								if ($data['do_i_chk'] <= $data['do_rs_symbol']){
									$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\">".$data['currency_symbol']." ".$data['amount']."</span></td>";
								}else{
									$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\"><img src=\"".$variables['images']."/".$data['currency_symbol'].".jpg\" border=\"0\" /> ".$data['amount']."</span></td>";		 
								}
						$body.="</tr>";
					   $body.="<tr>
								<td align=\"right\" colspan=\"2\">
									<span class=\"content pr10 b\">" ;
									if(!empty($data['currency_name'])) {
										$body.=$data['currency_name'] ;
									} 
						$body.= " ".$data['amount_words']." only 
									</span>
								</td>
							</tr>";
						$body.="</table>
					</div>";
			$body.="</div> 	";
			//Main div particulars area eof
			 
			 
			$body.="
			<div class=\"row wp100 pt2 pl10\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
				<tr>
					<td align=\"left\" >" ;
						$body.="
							<table align=\"left\" border=\"0\">
							<tr>					 
								<td class=\"content-small-10 b\">PAN No : </td>
								<td class=\"content-small-10\">".$data['pan_no']."</td>";
							if ($data['do_i_chk'] >= $data['do_tax'] && $data['do_tax']!=0 && !empty($data['tax1_name']) ){	
								if(!empty($data['vat_no'])){
									$body.="<td class=\"content-small-10 b\">&nbsp;VAT No : </td>
									<td class=\"content-small-10\">".$data['vat_no']."</td>";
								}
								if(!empty($data['cst_no'])){
									$body.="<td class=\"content-small-10 b\">&nbsp;CST No : </td>
									<td class=\"content-small-10\">".$data['cst_no']."</td>" ;
								}
							}
							if ($data['do_i_chk'] >= $data['do_st'] && $data['do_st']!=0 && !empty($data['tax1_name'])){	
								if(!empty($data['service_tax_regn'])){
									$body.="<td class=\"content-small-10 b\">&nbsp;Service Tax No : </td>
									<td class=\"content-small-10\">".$data['service_tax_regn']."</td>";
								}
							}
							if( !empty($data['delivery_at'])){
								$body.="<td class=\"content-small-10 b\">&nbsp;Delivery At : </td>
									<td class=\"content-small-10\">".$data['delivery_at']."</td>";
							}
							$body.="</tr>
							</table>";
			$body.="</td>";	
			$body.= "</tr>     
				</table>
			</div> ";		
			if (!empty($data['tax1_declaration'])){
			$body.="<div class=\"row wpx731 pt5 pb5\">		
						<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" width=\"731px\">	
							<tr>
								<td align=\"left\" class=\"pl10 pr10\">
									<span class=\"content-small-7\">".$data['tax1_declaration']."</span>							
								</td>
							</tr>	
						</table>
					</div> ";
			}
			
			 
		
		$body.="<div class=\"row wpx731 bg9 pb3 pt3\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
					<tr>
						<td class=\"address-text al pl20\" width=\"393px\">
							<span class=\"white-text b\">".$data['company_name']."
						</td>
						<td class=\"address-text ar pr10\">
							<span class=\"white-text-12 b\">www.GroupSMEERP.com</span> 
						</td>
					</tr>
				</table>
			</div>
			<div class=\"row wp731 pt5 pb5 bg8 \">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
				<tr>
					<td class=\"address-text al pl20\" width=\"265px\">
						<span class=\"white-text\"><b>Questions?</b></span> Contact us with a support ticket <br/>or call us at the numbers to the right.
					</td>
					<td class=\"address-text ar pr10\" width=\"466px\">
						Plot No 17, MG street | Jhena 440 025 Maharashtra | India<br/>
						<b>INDIA</b> : +91 9823066661, 9823066662  |   <b>USA & CANADA</b> : 1-837-621-1000
					</td>
				</tr>
			</table>
			</div>
			<div class=\"row wpx731 pt6\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
					<tr>
						<td height=\"10px\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"right\">
							<tr>
								<td class=\"design-text vt ar\">Designed by</td>
								<td class=\"pl5 vt pt2\"><img src=\"".$variables['images']."/invoice-studio-new.jpg\" border=\"0\" /></td>
							</tr>
							</table>						
						</td>
					</tr>
					<tr><td><hr/></td></tr>					
					<tr>
						<td class=\"vt pt15\" width=\"85%\">
							<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
								<tr>
									<td class=\"content pl2\" colspan=\"2\">For or On-behalf of,</td>
								</tr>
								<tr>
									<td class=\"content b pl2\" colspan=\"2\">".$data['billing_name']."</td>
								</tr>
								<tr>
									<td class=\"content pl2 pt20\" colspan=\"2\">
									I agree to all terms & conditions mentioned in the invoice and confirm that goods
or services as stated above are received by me in good working condition.
									</td>
								</tr>
								<tr>
									<td class=\"content pl2 pt40\">
									(Signature)
									</td>
									<td class=\"content pl2 pt40\">
									(Stamp)
									</td>
								</tr>
								<tr>
									<td class=\"content pl2 b\" colspan=\"2\">
									Name:<br/>
									Date:<br/>
									Time:
									</td>
								</tr> 
							</table>						
						</td>						
					</tr>
					<tr><td><hr/></td></tr>
				</table>
			</div>
		</div>
		" ;

		//Terms 2 BOF
		
		//Terms 2 EOF
		$body.="</div>
		</div>";
			$invoiceArr['content'] = $body ; 
			
			$invoiceArr['header'] = '' ;
			$invoiceArr['footer'] = '' ;
			createDmPDF($data["number"],$invoiceArr);
	  }
	
    } 
	
	function createDmPDF( $q_no, $data ){
		require_once(DIR_FS_ADDONS .'/html2pdf/config.orddm.inc.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
		parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
	  
		global 	$g_config;
		
		class ReportData extends Fetcher {
			var $content;
			var $url;
		
			function get_data($data) {
				return new FetchedDataURL($this->content, array(), "","");
			}
		
			function ReportData($content) {
				$this->content = $content;
			}
			
			function get_base_url() {
				return "";
			}
		}
		
		$contents 	= '';
		$contents = $data['content'];      
		
		// Configuration Settings.
		$g_config = array(
							'process_mode' 	=> 'single',
							'mode'		    => '',
							'pixels'		=> '750',
							'scalepoints'	=> '1',
							'renderimages'	=> '1',
							'renderlinks'	=> '1',
							'renderfields'	=> '1',
							'media'			=> 'A4',
							'cssmedia'		=> 'screen',
							//'leftmargin'	=> '30',
							'leftmargin'	=> '5',
							//'rightmargin'	=> '15',
							'rightmargin'	=> '5',
							'topmargin'		=> '5',
							'bottommargin'	=> '5',
							'encoding'		=> 'iso-8859-1',
							//'encoding'	=> 'UTF-8',
							'headerhtml'	=> $data['header'],
							'boydhtml'		=> $contents,
							'footerhtml'	=> $data['footer'],
							'watermarkhtml'	=> '',
							'pslevel'		=> '3',
							'method'		=> 'fpdf',
							'pdfversion'	=> '1.3',
							'output'		=> '2',
							'convert'		=> 'Convert File'
						);
		
		$media = Media::predefined('A4');
		$media->set_landscape(false);
		/*
		$media->set_margins(array(	'left' => 10,
									'right' => 10,
									'top' => 15,
									'bottom' => 35)
							);
		$media->set_pixels(700);
		*/
		 $media->set_margins(array(	'left' => 5,
									'right' => 5,
									'top' => 5,
									'bottom' => 5)
							);
		$media->set_pixels(740);
		
		$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
		$g_pt_scale = $g_px_scale * 1.43;
		
		$pipeline 					= PipelineFactory::create_default_pipeline("","");
		$pipeline->pre_tree_filters[] = new PreTreeFilterHeaderFooter( $g_config['headerhtml'], 
		$g_config['footerhtml']);
		$pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
		if ($g_config['renderfields']) {
			$pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
		}

		// Changed so that script will try to fetch files via HTTP in this case) and to provide absolute 
		//paths to the image files.
		// http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
		//		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
		$pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
		//$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
		$pipeline->destination 			= new DestinationFile($q_no);
		 
		$pipeline->process($q_no, $media);
		$filename = DIR_FS_ORDDM_PDF_FILES .'/'. $q_no .'.pdf'; 
		@chmod($filename, 0777);
		$contents = NULL;
		return true;
}

?>
