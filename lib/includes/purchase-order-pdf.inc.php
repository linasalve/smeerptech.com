<?php
	class PurchaseOrderPdf {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
        const DELETED = 3;
		const COMPLETED = 4;

		
		const PENDING_INV_R   = 0;
		const COMPLETED_INV_R = 1;
		const CANCELLED_INV_R = 2;
		
		function getStatus() {
			$status = array(
							'COMPLETED' => PurchaseOrderPdf::COMPLETED,
							'ACTIVE'    => PurchaseOrderPdf::ACTIVE,
							'PENDING'   => PurchaseOrderPdf::PENDING,
                            'BLOCKED' => PurchaseOrderPdf::BLOCKED,
							'DELETED'   => PurchaseOrderPdf::DELETED
                           
						);
			return ($status);
		}		
		function getReminderStatus() {
			$status = array('PENDING' => PurchaseOrderPdf::PENDING_INV_R,							 
							'COMPLETED'   => PurchaseOrderPdf::COMPLETED_INV_R,
                            'CANCELLED'   => PurchaseOrderPdf::CANCELLED_INV_R
						);
			return ($status);
		}
		
        /*
         array of days before which reminder of expiry has been sent
         ie 60 days before expiry,
            30 days before expiry purchase-order-pdf.inc
        */        
        function expirySetDays() {
			/*  $arr = array(120=>120,
                         90=>90
						); 
			*/
			$arr = array(90=>90,
						 60=>60
						);
			return ($arr);
		}
        
        function getStatusTitle() {
            $status = array(PurchaseOrderPdf::BLOCKED    => 'Bad Debt',
                            PurchaseOrderPdf::ACTIVE     => 'Paid Fully',
                            PurchaseOrderPdf::PENDING    => 'Payment Pending',
                            PurchaseOrderPdf::DELETED    => 'Deleted',
                           // PurchaseOrderPdf::COMPLETED  => 'Renewed',
                            PurchaseOrderPdf::COMPLETED  => 'Completed',
                            'CANCELLED'         => 'Bad Debt',
                            'ACTIVE'            => 'Active',
                           //'ACTIVE'            => 'Paid Fully',
                            'PENDING'           => 'Payment Pending',
                            'DELETED'           => 'Deleted',
                            //'PROCESSED'         => 'Renewed',
                            //'PROCESSED'         => 'Completed',
                           );
            return ($status);
        }
        
		/**
		 *	Function to get all the InvoiceProformas.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
		 
			$query .= " FROM ". TABLE_PURCHASE_ORDER_PDF ;
            $query .= " LEFT JOIN ". TABLE_PURCHASE_ORDER
                        ." ON ". TABLE_PURCHASE_ORDER .".id = ". TABLE_PURCHASE_ORDER_PDF .".or_id ";
			$query .= " LEFT JOIN ". TABLE_PURCHASE_ORDER_P
                        ." ON ". TABLE_PURCHASE_ORDER_P.".bill_id = ".TABLE_PURCHASE_ORDER_PDF .".or_id " ;			
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_PURCHASE_ORDER_PDF .".created_by ";
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_PURCHASE_ORDER_PDF .".client ";            
			$query .= " ". $condition;
	
			if( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
           
            
            $query .= " FROM ". TABLE_PURCHASE_ORDER_PDF ;
            $query .= " LEFT JOIN ". TABLE_PURCHASE_ORDER
                        ." ON ". TABLE_PURCHASE_ORDER .".number = ". TABLE_PURCHASE_ORDER_PDF .".or_no ";
            /* $query .= " LEFT JOIN ". TABLE_PURCHASE_ORDER_P
                       ." ON ". TABLE_PURCHASE_ORDER_P .".ord_no = ". TABLE_PURCHASE_ORDER_PDF .".or_no "; */
			$query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_PURCHASE_ORDER_PDF .".created_by ";
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_PURCHASE_ORDER_PDF .".client ";
            
			$query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
               
               return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( in_array($status_new, PurchaseOrderPdf::getStatus()) ) {
                $order = NULL;
                if ( (PurchaseOrderPdf::getList($db, $invoice, TABLE_PURCHASE_ORDER_PDF.'.id,'.TABLE_PURCHASE_ORDER_PDF.'.access_level,'.TABLE_PURCHASE_ORDER_PDF.'.created_by,'.TABLE_PURCHASE_ORDER_PDF.'.status', " WHERE ".TABLE_PURCHASE_ORDER_PDF.".id = '$id'")) > 0 ) {
                    $invoice = $invoice[0];
					
					// Check Access Level for the invoice.
					/*
					if ( ($invoice['created_by'] == $my['user_id']) && ($invoice['access_level'] >= $access_level) ) {
						$messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
					}elseif( ($invoice['created_by'] != $my['user_id']) && ($invoice['access_level'] >= $access_level_ot) ) {
						$messages->setErrorMessage("Cannot change Status of Invoice created by others, with current Access Level.");
					} else { */
                    	if ( $invoice['status'] != PurchaseOrderPdf::COMPLETED ) {
                            $query = "UPDATE ". TABLE_PURCHASE_ORDER_PDF
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }else{
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        } else {
                            $messages->setErrorMessage("Status not updated.");
                        }
                    //}
                }
                else {
                    $messages->setErrorMessage("The Purchase order PDF was not found.");
                }
            }
            else {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $order = NULL;
            if ( (PurchaseOrderPdf::getList($db, $order, TABLE_PURCHASE_ORDER_PDF.'.id,'.TABLE_PURCHASE_ORDER_PDF.'.or_no,'.TABLE_PURCHASE_ORDER_PDF.'.number, '.TABLE_PURCHASE_ORDER_PDF.'.access_level,'.TABLE_PURCHASE_ORDER_PDF.'.status', " WHERE ".TABLE_PURCHASE_ORDER_PDF.".id = '$id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != PurchaseOrderPdf::COMPLETED ) {
                    if ( $order['status'] == PurchaseOrderPdf::BLOCKED ) {
                        if ( $order['access_level'] < $access_level ) {
                            /* check receipt created or not if receipt created then dont delete it 
                             ie invoice will be deleted only after deleting the receipts
                            */                                
							$query = " UPDATE ". TABLE_PURCHASE_ORDER_PDF
										." SET status ='".PurchaseOrderPdf::DELETED."' WHERE id = '$id'";
							 
							 
							if ( $db->query($query) && $db->affected_rows()>0 ) {
							 
							
								$messages->setOkMessage("The Purchase Order PDF has been deleted.");
								
								 
							} else {
								$messages->setErrorMessage("The Purchase Order PDF was not deleted.");
							}
                               
                            
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Purchase Order PDF with the current 
							Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("To Delete Purchase Order PDF it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Purchase Order PDF cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Purchase Order PDF was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        /**
		 * Function to validate the input from the User while sending the invoice via email.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
	    function validateSendInvoice(&$data, $extra='') {
             foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( !isset($data['to']) || empty($data['to']) ) {
                $messages->setErrorMessage('To should be provided.');
            }
            elseif ( !isValidEmail($data['to']) ) {
                $messages->setErrorMessage('To Email Address is not valid.');
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Validate the Order.
            $list = NULL;
            if ( PurchaseOrderPdf::getList($db, $list, TABLE_PURCHASE_ORDER_PDF.'.id', " WHERE ".TABLE_PURCHASE_ORDER_PDF.".or_id = '". $data['or_id'] ."' AND ".TABLE_PURCHASE_ORDER_PDF.".status !='".PurchaseOrderPdf::DELETED."'")>0 ) {
                $messages->setErrorMessage("The Purchase Order has been processed already.");
            } else {
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT party_id as  client, access_level, status FROM ". TABLE_PURCHASE_ORDER
                            ." WHERE number = '". $data['or_no'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage("The PO was not found in the database.");
                    }else {
                        $db->next_record();
                        if( ($db->f('status') != ACTIVE)  ) {
							$messages->setErrorMessage("The PO is not yet approved.");
						}
						$data['client'] = $db->f('client');
                    }
                }
            }
			if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            
            
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Client for whom the PO is being Created.");
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email,org,billing_name, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            
            // Check the billing name. 
           if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
            // Format the Date of Invoice.
            if ( !isset($data['do_i']) || empty($data['do_i']) ) {
                $messages->setErrorMessage("The Date of PO is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_i']) ) {
                $messages->setErrorMessage("Format of the Date of PO is invalid.");
            }
            else {
                $data['do_i'] = explode('/', $data['do_i']);
				
                //generate invoice number on the basis of invoice date 
                // $data['number'] = "PT". $data['do_i'][2]."-INV-".$data['do_i'][1]. $data['do_i'][0].date("hi-s") ;
                $data['do_i'] = mktime(0, 0, 0, $data['do_i'][1], $data['do_i'][0], $data['do_i'][2]);
            }
            $data['number']=$data['inv_counter'] ='';
			if(!empty($data['company_id']) && !empty($data['do_i'])){				
				$detailInvNo = getPoPdfNumber($data['do_i']); 				
				if(!empty($detailInvNo)){
					$data['number'] = $detailInvNo['number'];
					$data['inv_counter'] = $detailInvNo['inv_counter'];
				}
			}
			
             // Format the Date of expiry from date, if provided.
            /* if ( !empty($data['do_fe']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_fe']) ) {
                    $messages->setErrorMessage("Format of the Service From Date is invalid.");
                }
                else {
                    $data['do_fe'] = explode('/', $data['do_fe']);
                    $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                }
                
            }else{
                $data['do_fe'] = '';
            }      
             
             // Format the Date of Invoice Expiry, if provided.
            if ( !empty($data['do_e']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_e']) ) {
                    $messages->setErrorMessage("Format of the Date Renewal is invalid.");
                }
                else {
                    $data['do_e'] = explode('/', $data['do_e']);
                    $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                }                
            }else{
                 $data['do_e'] = '';
        
            } 
            // Format the Invoice Payment Due Date.
            if ( !isset($data['do_d']) || empty($data['do_d']) ) {
                $messages->setErrorMessage("The Due date for Payment of Proforma Invoice is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_d']) ) {
                $messages->setErrorMessage("Format of the Due date for Payment of Proforma Invoice is invalid.");
            }
            else {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
            */
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Proforma Invoice Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
           
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Proforma Invoice Amount in INR is not valid.");
                }
            }
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{  
               
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}
			 
			if( empty($data['number']) ){
                $messages->setErrorMessage("An PO PDF Number was not generated.");
            } 
			if( empty($data['inv_counter']) ){
                $messages->setErrorMessage("An PO PDF Counter was not generated.");
            } 
            
            // Check for the duplicate PO PDF Number.
            $list = NULL;
            if ( PurchaseOrderPdf::getList($db, $list, TABLE_PURCHASE_ORDER_PDF.'.id', " WHERE ".TABLE_PURCHASE_ORDER_PDF.".number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An PO PDF with the same Number already exists.<br/>Please try again.");
            }
            $list = NULL;
            
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // Validate the Order.
            
            $list = NULL;
            if ( PurchaseOrderPdf::getList($db, $list, TABLE_PURCHASE_ORDER_PDF.'.id,'.TABLE_PURCHASE_ORDER_PDF.'.created_by', " WHERE ".TABLE_PURCHASE_ORDER_PDF.".or_no = '". $data['or_no'] ."' AND ".TABLE_PURCHASE_ORDER_PDF.".status !='".PurchaseOrderPdf::DELETED."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_PURCHASE_ORDER
                                ." WHERE number = '". $data['or_no'] ."' AND status !='".Order::DELETED."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage('The Order was not found in the database.');
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            /* As complete ma
                            if ( $db->f('status') != COMPLETED ) {
                                $messages->setErrorMessage('The Order has not been marked as completed.');
                            }*/
                            $data['client'] = $db->f('client');
                        }
                        else {
                            $messages->setErrorMessage('You donot have the Right to update Proforma Invoice for this Order.');
                        }
                    }
                }
            }
            else {
                $messages->setErrorMessage('The Proforma Invoice for the Order has not been created yet.');
            }
            
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage('Creator details cannot be retrieved, Please try again.');
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage('The Creator was not found.');
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage('The Creators\' status is not Active.');
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }

            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage('Select the Client for whom the Proforma Invoice is being Created.');
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, billing_name,status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            
            // Check the billing name.
           
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
            // Format the Date of Invoice.
            if ( !isset($data['do_i']) || empty($data['do_i']) ) {
                $messages->setErrorMessage("The Date of Proforma Invoice is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_i']) ) {
                $messages->setErrorMessage("Format of the Date of Proforma Invoice is invalid.");
            }
            else {
                $data['do_i'] = explode('/', $data['do_i']);
                $data['do_i'] = mktime(0, 0, 0, $data['do_i'][1], $data['do_i'][0], $data['do_i'][2]);
            }
            
           
                                     
            // Format the Date of expiry from date, if provided.
            if ( !empty($data['do_fe']) ) {
            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_fe']) ) {
                    $messages->setErrorMessage("Format of the Service From Date is invalid.");
                }
                else {
                    $data['do_fe'] = explode('/', $data['do_fe']);
                    $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                }
                
           }else{
                $data['do_fe'] = '';
           }
                    
             // Format the Date of Invoice Expiry, if provided.
            if ( !empty($data['do_e']) ) {
            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_e']) ) {
                    $messages->setErrorMessage("Format of the Date Renewal is invalid.");
                }
                else {
                    $data['do_e'] = explode('/', $data['do_e']);
                    $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                }
                
            }else{
                 $data['do_e'] = '';
        
            }
            // Format the Invoice Payment Due Date.
            if ( !isset($data['do_d']) || empty($data['do_d']) ) {
                $messages->setErrorMessage("The Due date for Payment of Proforma Invoice is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_d']) ) {
                $messages->setErrorMessage("Format of the Due date for Payment of Proforma Invoice is invalid.");
            }
            else {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Proforma Invoice Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Proforma Invoice Amount in INR is not valid.");
                }
            }
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{            
                
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
            
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
           
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                $messages->setErrorMessage("The Status is not valid.");
            }
            

            
            // Check for the duplicate Invoice Number.
            /*
            $list = NULL;
            if ( PurchaseOrderPdf::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Invoice with the same Number already exists.");
            }
            $list = NULL;
            */
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for 
         * each Invoice.
         *
         */
        function setOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_PURCHASE_ORDER
                        ." SET status = '$status'"
                        ." WHERE ( id = '$id' OR number = '$id' )";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for
         * each Invoice so that the service provided is updated in the
         * Members Account.
         *
         */
        function UpdateClientService(&$db, $client_id, $services) {
            $return     = false;
            $c_service  = 'NULL';
            
            $query = "SELECT service_id FROM ". TABLE_CLIENTS
                        ." WHERE user_id = '". $client_id ."'";
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
                $c_service = processSQLData($db->f('service_id'));
                if ( empty($c_service) ) {
                    $c_service = array();
                }
                else {
                    $c_service = explode(',', $c_service);
                }
                
                $c_service = arrayCombine($c_service, $services);
                $c_service = implode(',', $c_service);
                $query = "UPDATE ". TABLE_CLIENTS
                            ." SET service_id = '$c_service'"
                            ." WHERE user_id = '$client_id'";
                if ( $db->query($query) ) {
                    $return = true;
                }
            }
            return ($return);
        }
        
        /**
         * This function is used to create the File in PDF or HTML format for the Invoice.
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createFile($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("purchase-order-pdf-tpl.html");
                //$file_data = $s->fetch("bill-invoice-tpl-sjm.html");
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_INVPROFM_FILES_HTML, 0777);
                if ( is_writable(DIR_FS_INVPROFM_HTML_FILES) ) {
                    $file_name = DIR_FS_INVPROFM_HTML_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Invoice was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                } else {
                    $messages->setErrorMessage("The Directory to store Invoice is not writable.");
                }
                umask($old_umask);
            }elseif ( $type == 'PDF' ) {
                   /*
                   $pdf_content = Invoice::createPdfContent($data);
                   $invoiceArr['content'] = $pdf_content ;
                   $invoiceArr['header'] = '' ;
                   $invoiceArr['footer'] = '' ;
                   createInvoicePDF($data["number"],$invoiceArr);
                   $file_name = DIR_FS_INVPROFM_PDF_FILES ."/". $data["number"] .".pdf";*/
            } 
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
          
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;

            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
               $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PURCHASE_ORDER_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }                
                return ( $total );
            }
            else {
                return false;
            }
    }
	 
function createPdfFile($data) {        
$variables['images'] = DIR_WS_IMAGES_NC ;
$body=  "<style type=\"text/css\" rel=\"stylesheet\">     
        body {            
			margin-right:auto;
			margin-left:auto;
        }
        
        #content {
        margin:0;
        width: 100%;
        text-align:center;
        }
        #content-main {
        width: 731px;
        text-align:center;
        margin-left:auto;
		margin-right:auto;
        }        
        .heading{
        	font-family:Arial, Verdana, 'San serif';
			font-size:18px;
			color:#000000;
			text-decoration:none;
			font-weight:bold;
	    }    
	    .sub-heading{
        	font-family:Arial, Verdana, 'San serif';
			font-size:11px;
			color:#686868;
			line-height:15px;
			text-decoration:none;
			font-weight:regular;
	    }
	    .invoice-border{ 
			 background:url(".$variables['images']."/invoice-repeater-new.jpg);
    		 background-repeat:repeat-y;
    		 width:253px;
	    }
	    .invoice-text{
        	font-family:Arial, Verdana, 'San serif';
			font-size:12px;
			color:#000000;
			text-decoration:none;
			font-weight:bold;
	    }
	    
	    .content-heading{
        	font-family:Arial, Verdana, 'San serif';
			font-size:12px;
			color:#FFFFFF;
			text-decoration:none;
			font-weight:bold;
	    }
	    .address-text{
        	font-family:Arial, Verdana, 'San serif';
			font-size:10px;
			color:#FFFFFF;
			text-decoration:none;
			font-weight:regular;
			lignt-height:13px;
	    }
		.address-text-small{
			font-family:Arial, Verdana, 'San serif';
            font-size:9px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
		}
	    .green-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#176617;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .white-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
		.white-text-12{
            font-family:Arial, Verdana, 'San serif';
            font-size:12px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .design-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#797979;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
	    .custom-content{
        	font-family:arial,verdana,sans-serif;
            font-size:11px;
            color:#646464;
	    }		    
	    .content{
        	font-family:arial,verdana,sans-serif;
            font-size:11px;
            color:#4f4f4f;
            lignt-height:10px;
	    }	
		.contentsub{
            font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
            lignt-height:10px;
			font-weight:bold;
        }
		.contenthead{
            font-family:arial,verdana,sans-serif;
            font-size:13px;
            color:#4f4f4f;
            lignt-height:11px;
        }		
	    .content-small{
        	font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#4f4f4f;
	    }	
		.content-small-term{
            font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#726e6e;
        }
		.content-small-7{
        	font-family:arial,verdana,sans-serif;
            font-size:7px;
            color:#4f4f4f;
	    }
		.content-small-8{
        	font-family:arial,verdana,sans-serif;
            font-size:8px;
            color:#4f4f4f;
	    }
		.content-small-10{
        	font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
	    }
	    .spacer{background:url(".$variables['images']."/spacer.gif) repeat;}
        .bg1{background-color:#898989;}
        .bg2{background-color:#CCCCCC;}
        .bg3{background-color:#f4f4f4;}
        .bg4{background-color:#ba1a1c;}
        .bg5{background-color:#956565;}
        .bg6{background-color:#BEBE74;}
        .bg7{background-color:#48b548;}
        .bg8{background-color:#000000;}
        .bg9{background-color:#bf1f21;}        
        .bdb1{border-bottom:1px solid #000000;}        
        .b {font-weight:bold;}        
        .al {text-align:left;}
        .ac {text-align:center;}
        .ar {text-align:right;}        
        .vt {vertical-align:top;}
        .vm {vertical-align:middle;}
        .vb {vertical-align:bottom;}        
        .fs08 {font-size:0.8em;}
        .fs11 {font-size:1.1em;}
        .fst11{font-size:1.1em; font-family:'Arial Narrow',Verdana,Arial,Sans-Serif;}
        .fs15 {font-size:1.5em;}        
        .lh15 {line-height:1.5em;}
        .lh10 {line-height:1em;}        
        .pl2{padding-left:2px;}
        .pl5{padding-left:5px;}
        .pl8{padding-left:8px;}
        
        .pl10{padding-left:10px;}
        .pl11{padding-left:11px;}
        .pl15{padding-left:15px;}
        .pl20{padding-left:20px;}
        .pl60{padding-left:60px;}
        .pl42{padding-left:42px;}
        
        .pr2{padding-right:2px;}
        .pr5{padding-right:5px;}
        .pr10{padding-right:10px;}
      
        
        .pb2{padding-bottom:2px;}
		.pb3{padding-bottom:3px;}
		.pb4{padding-bottom:4px;}
        .pb5{padding-bottom:5px;}         
        .pb10{padding-bottom:10px;}
        .pb13{padding-bottom:13px;}
        .pb20{padding-bottom:20px;}
        .pb25{padding-bottom:25px;}
        .pb30{padding-bottom:30px;}
        .pb40{padding-bottom:40px;}
        .pb50{padding-bottom:50px;}
        .pb100{padding-bottom:100px;}
		
        .pt1{padding-top:1px;}
        .pt2{padding-top:2px;}
        .pt3{padding-top:3px;}
        .pt4{padding-top:4px;}
        .pt5{padding-top:5px;}
        .pt6{padding-top:6px;}
        .pt8{padding-top:8px;}
        .pt10{padding-top:10px;}
        .pt15{padding-top:15px;}
         .pt20{padding-top:20px;}
        .pt25{padding-top:25px;}		
        .pt30{padding-top:30px;}       
        .pt32{padding-top:32px;}
        .pt45{padding-top:45px;}
        
        .b1{border:collapse;border:1px solid #000000;}
        .wp100{width:100%;}
        .wp90{width:90%;}
        .wp70{width:70%;}
        .wp65{width:65%;}
        .wp60{width:60%;}
        .wp55{width:55%;}
        .wp50{width:50%;}
        .wp45{width:45%;}
        .wp40{width:40%;}
        .wp35{width:35%;}
        .wp30{width:30%;}
        .wp28{width:28%;}
        .wp25{width:25%;}
        .wp20{width:20%;}
        .wp15{width:15%;}
        .wp13{width:13%;}
        .wp12{width:12%;}
        .wp10{width:10%;}
        .wp9{width:9%;}
        .wp5{width:5%;}
        .wp6{width:6%;}
        .wp4{width:4%;}        
       
	    .wpx7{width:7px;}
	    .wpx9{width:9px;}
        .wpx10{width:10px;}
		.wpx20{width:15px;}
		.wpx707{width:707px;}		 
		.wpx711{width:711px;}
        .wpx712{width:712px;}
        .wpx729{width:729px;}
        .wpx731{width:731px;}
        .wpx330{width:345px;}
        .wpx337{width:352px;}
        .wpx348{width:348px;}
              
		.wpx711{width:711px;}
        .wpx707{width:707px;}
        .wpx9{width:9px;}
		.wpx713{width:713px;}
		.wpx347{width:347px;}
		.wpx253{width:253px;}
        .wpx335{width:335px;}
        
        .hp30{
        	height:27px;
        	border-top:1px solid #dddddd;
        }
        .hp22{height:22px;}
		.hp20{height:20px;}
        .hp4{height:4px;}
        .hp9{height:9px;}
        .hp8{height:8px;}
        .hp17{height:19px;}
        
        .borlr{
        	border-left:1px solid #dddddd;
        	border-right:1px solid #dddddd;
	    }
	    
	    .borl{
        	border-left:1px solid #dddddd;
	    }
	    
	    .borr{
        	border-right:1px solid #dddddd;
	    }
	    
	    .borb{
        	border-bottom:1px solid #dddddd;
	    }
	    
        div.row {
            clear:both;
            float:left;
        }
        div.coloumn {
            float:left;
        }
        div.coloumn-right {
            float:right;
        }        
        .seccolor{background-color:#999999;}
		.bg2{background-color:#f4f4f4;}		
		div.clear{
			clear:both;
			background-color:#f4f4f4;
		}
        .header-text{font-size:11px; font-family:arial; font-weight:bold;}
</style>        
<div id=\"content\">
<div id=\"content-main\">
<div class=\"row wp100 al\" style=\"height:800px\">
	<div class=\"row al pt45\">
		<div><img src=\"".$variables['images']."/smeerp-inv-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\"></div>
	</div>
	<div class=\"row wpx731 al pb10 pt1\">
		<div class=\"coloumn al content wp65\">
			Motivated to Serve&nbsp;Determined to Excel <sup>TM</sup>
		</div>
		<div class=\"coloumn-right invoice-text ar\">";			
			if( $data['do_i_chk'] >= $data['do_iso'] ){
				$body.=" An ISO 9001:2008 CERTIFIED COMPANY ";
			}  
$body.="</div>
	</div>
	<div class=\"row wpx731 pb5 pt30\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
		<tr>
			<td width=\"478px\" class=\"pl42 al\" valign=\"top\">";
	if (!empty($data['old_billing_address'])){
	$body.="<div class=\"heading pb2\">".$data['billing_name']."</div>
			 <div class=\"sub-heading\">".nl2br($data['old_billing_address'])."</div>";
	}else{
	$body.="<div class=\"heading pb2 al\">".$data['billing_name']."</div>
	<div class=\"sub-heading al\">".nl2br($data['b_addr'])."</div>      
	<div class=\"sub-heading al\">";
		if (!empty($data['b_addr_city']))
		{
			$body.=$data['b_addr_city'];
		}
		if (!empty($data['b_addr_zip']))
		{
			$body.="&nbsp;". $data['b_addr_zip'];
		}
		if (!empty($data['b_addr_state']))
		{
			$body.="&nbsp;". $data['b_addr_state'];
		}
		if (!empty($data['b_addr_country']))
		{
			$body.="<br/>". $data['b_addr_country'];
		}    
	$body.="</div> "  ;
	}
	$body.="</td>
	<td width=\"253px\" valign=\top\" class=\"ar\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"ar\">
		<tr>
			<td class=\"b al invoice-text pl5 pb3\">".$data['invoice_title']."</td>
		</tr>
		<tr>
			<td><img src=\"".$variables['images']."/invoice-top-corner-new.jpg\"  width=\"253px\" height=\"9\"  border=\"0\" /></td>
		</tr>
		<tr>
			<td class=\"invoice-border\">
			<div class=\"row wpx253\">
				<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Relationship No.</span></div>
				<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['client']['number']."</span></div>
			</div>
			<div class=\"row wpx253\">
				<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">PO No.</span></div>
				<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['number']."</span></div>
			</div>";
	if (!empty($data['do_i']))
	{
	$temp=$temp1=$dtArr=null;
	$dd  =$mm=$yy='';
	$temp = explode(" ",$data['do_i']) ;
	$temp1=$temp[0];
	$dtArr =explode("-",$temp1) ;
	$dd =$dtArr[2];
	$mm =$dtArr[1];
	$yy=$dtArr[0];
	//$do_i = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
	$do_i = date("d M Y",$data['do_i']);

	$body.="
		<div class=\"row wpx253\">
			<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">PO Date</span></div>
			<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_i."</span></div>
		</div>";
	}
	/* if (!empty($data['currency_abbr']))
	{
	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Invoice Currency</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['currency_abbr']."</span></div>
	</div>";
	} */

	/*
	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Order No.</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['or_no']."</span></div>
	</div>";
	*/

	if (!empty($data['do_d']))
	{
	/*
	$temp=$temp1=$dtArr=null;
	$dd  =$mm=$yy='';
	$temp = explode(" ",$data['do_d']) ;
	$temp1=$temp[0];
	$dtArr =explode("-",$temp1) ;
	$dd =$dtArr[2];
	$mm =$dtArr[1];
	$yy=$dtArr[0];*/

	/* $do_d = date("d M Y",$data['do_d']);
	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Payment Due Date</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_d."</span></div>
	</div>"; */
	}

	if (!empty($data['do_fe'])){
	/* $temp=$temp1=$dtArr=null;
	$dd  =$mm=$yy='';
	$temp = explode(" ",$data['do_fe']) ;
	$temp1=$temp[0];
	$dtArr =explode("-",$temp1) ;
	$dd =$dtArr[2];
	$mm =$dtArr[1];
	$yy=$dtArr[0];
	$do_fe = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Service Start Date</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_fe."</span></div>
	</div>"; */
	}
	if (!empty($data['do_e'] ))
	{
	/* $temp=$temp1=$dtArr=null;
	$dd  =$mm=$yy='';
	$temp = explode(" ",$data['do_e']) ;
	$temp1=$temp[0];
	$dtArr =explode("-",$temp1) ;
	$dd =$dtArr[2];
	$mm =$dtArr[1];
	$yy=$dtArr[0];
	$do_e = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));

	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Renewal Date</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_e."</span></div>
	</div>"; */
	}
		$body.="</td>
				</tr>
				<tr>
					<td><img src=\"".$variables['images']."/invoice-bottom-corner-new.jpg\"  width=\"253px\" height=\"9\"  border=\"0\" /></td>
				</tr>
			</table>
		</td>
		</tr>
		</table>
		</div> ";
	$body.="
		<div class=\"row wp100 pt2\" style=\"height:720px\">
			<div class=\"row wp100 pt5\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
			<tr>
			<td width=\"".$data['wd1']."px\" class=\"bg1\" height=\"30px\"><img src=\"".$variables['images']."/first-left-corner.jpg\" width=\"10px\" height=\"30px\" border=\"0\" /></td>
			<td class=\"bg1 al pl5\" width=\"".$data['wdinvp']."px\" height=\"30px\"><span class=\"content-heading b\">Particulars</span></td>
			<td class=\"bg1 ar\" width=\"".$data['wdpri']."px\" height=\"30px\"><span class=\"content-heading b\">Rate</span></td>";
		if($data['show_nos'] ==1){
			$body.="<td class=\"bg1 ar\" width=\"".$data['wdnos']."px\" height=\"30px\"><span class=\"content-heading b\">Unit</span></td>";
		}
		$body.="<td class=\"bg1 ar\" width=\"".$data['wdamt']."px\" height=\"30px\"><span class=\"content-heading b\">Amt.</span></td>";
		if($data['show_discount'] ==1){
			$body.= "<td class=\"bg1 ar\" width=\"".$data['wddisc']."px\" height=\"30px\"><span class=\"content-heading b\">Disc.</span></td>
		 <td class=\"bg1 ar\" width=\"".$data['wdst']."px\" height=\"30px\"><span class=\"content-heading b\">SubTot</span></td>";
		}
			$body.= "<td width=\"".$data['wd2']."px\" class=\"bg1\" height=\"30px\"><img src=\"".$variables['images']."/first-right-corner.jpg\"  width=\"9px\" height=\"30px\" border=\"0\" /></td>
	</tr>";    
	$k=1;           
	foreach($data['particulars'] as $key=>$particular)
	{
		if (!empty($particular['ss_punch_line'])){
			$body.= "<tr>
				<td class=\"bg3 borl\" height=\"30px\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\"  border=\"0\" /></td>
				<td class=\"bg3 al content pt8 b pb5 pl5\" height=\"30px\">";
				$body.=$k.". ".nl2br($particular['ss_punch_line'])."</td>";
				$body.="<td class=\"bg3 al content pt8 b pb5 pl5\" height=\"30px\" colspan=\"".$data['colsSpanNo1']."\">&nbsp;</td>";
				$body.="<td class=\"bg3 borr\" height=\"30px\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" height=\"1px\"  border=\"0\" /></td>
			 </tr>";
		}
		$body.="<tr>
			<td class=\"pb5 bg3 borl\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>
			<td class=\"bg3 pb5 content vt  al pl10 pl5\" height=\"30px\">".nl2br($particular['particulars'])."</td>
			<td class=\"bg3 pb5 content vt  ar\" height=\"30px\">".$particular['p_amount']."</td>";
			if($data['show_nos'] ==1){ 
			 $body.="<td class=\"bg3 pb5 vt content ar\" height=\"30px\">".$particular['s_quantity']." ".$particular['s_type']."</td>" ;
			}
			$body.="<td class=\"bg3 pb5 vt content ar\" height=\"30px\">".$particular['s_amount']."</td>";
			if($data['show_discount'] ==1){
			 $body.="<td class=\"bg3 pb5 vt content ar\" height=\"30px\">".$particular['d_amount']."</td>";
			 $body.="<td class=\"bg3 pb5 vt content ar\" height=\"30px\">".$particular['stot_amount']."</td>";
			}  		
			$body.="<td class=\"pb5 bg3 borr\" height=\"30px\">
				<img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" height=\"1px\"  border=\"0\" /></td>
			</tr>";
			$k++;	
	}
		$body.="<tr>
				<td width=\"10px\" class=\"bg3 vb\"><img src=\"".$variables['images']."/first-bottom-left-corner.jpg\"  width=\"10px\" height=\"9px\"  border=\"0\" /></td>";
				$body.="<td class=\"bg3 borb vb\" colspan=\"".$data['colsSpanNo2']."\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>";
				$body.="<td width=\"9px\" class=\"bg3 vb\"><img src=\"".$variables['images']."/first-bottom-right-corner.jpg\"  width=\"9px\" 
				height=\"9px\"  border=\"0\" /></td>
			</tr>
			</table>
			</div> ";

	$body.="
			<div class=\"row wp100 pt5 pb5\">
				<table align=\"right\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">	";
				if(!empty($data['tax1_name'])){			
					$body.="<tr>				
					 <td align=\"right\" width=\"500px\"><span class=\"content ar b\">Total :</span></td>
					 <td align=\"right\" width=\"100px\">
					 <span class=\"content pr10 b\">&nbsp;".$data['sub_total_amount']."</span></td>
					</tr>
					<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar\">".$data['tax1_name']." @ ".$data['tax1_value']." :</span></td>
						<td align=\"right\" width=\"100px\">
						<span class=\"content pr10 \">&nbsp;".$data['tax1_total_amount']."</span></td>
					</tr>";
					if(!empty($data['tax1_sub1_name'])){
						$body.="<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar\">".$data['tax1_sub1_name']." @ ".$data['tax1_sub1_value']." :</span></td>
						<td align=\"right\" width=\"100px\">
						<span class=\"content pr10\">&nbsp;".$data['tax1_sub1_total_amount']."</span></td>
						</tr>" ;
					}
					if (!empty($data['tax1_sub2_name'])){
						$body.="<tr>				
							<td align=\"right\" width=\"500px\"><span class=\"content ar \">".$data['tax1_sub2_name']." @ ".$data['tax1_sub2_value']." :</span></td>
							<td align=\"right\" width=\"100px\">
							<span class=\"content pr10\">&nbsp;".$data['tax1_sub2_total_amount']."</span></td>
						</tr>" ;
					}
				}
				if (!empty($data['round_off_op']) && !empty($data['round_off']) ){
					$body.="<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar \">Round off :</span></td>
						<td align=\"right\" width=\"100px\"><span class=\"content pr10\">"
						.$data['round_off_op']."&nbsp;".$data['round_off']."</span></td>
						</tr>" ;
				}
				$body.="<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar b\">Grand Total :</span></td>";
						if ($data['do_i_chk'] <= $data['do_rs_symbol']){
							$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\">".$data['currency_symbol']." ".$data['amount']."</span></td>";
						}else{
							$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\"><img src=\"".$variables['images']."/".$data['currency_symbol'].".jpg\" border=\"0\" /> ".$data['amount']."</span></td>";		 
						}
				$body.="</tr>";
			   $body.="<tr>
						<td align=\"right\" colspan=\"2\">
							<span class=\"content pr10 b\">" ;
							if(!empty($data['currency_name'])) {
								$body.=$data['currency_name'] ;
							} 
				$body.= " ".$data['amount_words']." only 
							</span>
						</td>
					</tr>";
				$body.="</table>
			</div>";
			
			if(!empty($data['po_terms_condition'])){
				$body.="
				<div class=\"row pt32\">
					<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" width=\"731px\">
					<tr>
						<td width=\"10px\" class=\"bg1 vt\">
							<img src=\"".$variables['images']."/first-left-corner.jpg\" width=\"10px\" border=\"0\" />
						</td>
						<td width=\"712px\" class=\"bg1 al pl5\"><span class=\"content-heading b\">Terms and Condition</span></td>
						<td width=\"9px\" class=\"bg1 vt\"><img src=\"".$variables['images']."/first-right-corner.jpg\" width=\"9px\" 
						border=\"0\" /></td>
					</tr>   
					<tr>
						<td class=\"bg3 borl\" height=\"1px\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" 
					height=\"1px\"  border=\"0\" /></td>
						<td width=\"712px\" class=\"bg3 vt al content-small-10 pt8 pb5 pl5\" height=\"10px\" valign=\"top\">";
					$body.= nl2br($data['po_terms_condition'])."</td>";
					$body.="<td class=\"bg3 borr\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" 
					height=\"1px\" border=\"0\"/></td>
					</tr> 
					<tr>
						<td width=\"10px\" height=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-left-corner.jpg\"  width=\"10px\" height=\"9px\"  border=\"0\" /></td>";
					$body.="<td width=\"712px\" class=\"bg3 borb\"\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>";
					$body.="<td width=\"9px\" height=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-right-corner.jpg\" width=\"9px\" height=\"9px\"  border=\"0\"/></td>
					</tr>
					</table>
				</div>";
			}	
	$body.="</div>";
	//EOF Div of particulars 
	
	
	$body.="
		<div class=\"row wp100 pt2 pl10\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
		<tr>
			<td align=\"left\" >" ;
			$body.="<table align=\"left\" border=\"0\">
				<tr>					 
					<td class=\"content-small-10 b\">PAN No : </td>
					<td class=\"content-small-10\">".$data['pan_no']."</td>";
				if ($data['do_i_chk'] >= $data['do_tax'] && $data['do_tax']!=0 && !empty($data['tax1_name']) ){	
					if(!empty($data['vat_no'])){
						$body.="<td class=\"content-small-10 b\">&nbsp;VAT No : </td>
						<td class=\"content-small-10\">".$data['vat_no']."</td>";
					}
					if(!empty($data['cst_no'])){
						$body.="<td class=\"content-small-10 b\">&nbsp;CST No : </td>
						<td class=\"content-small-10\">".$data['cst_no']."</td>" ;
					}
				}
				if ($data['do_i_chk'] >= $data['do_st'] && $data['do_st']!=0 && !empty($data['tax1_name'])){	
					if(!empty($data['service_tax_regn'])){
						$body.="<td class=\"content-small-10 b\">&nbsp;Service Tax No : </td>
						<td class=\"content-small-10\">".$data['service_tax_regn']."</td>";
					}
				}
				if( !empty($data['delivery_at'])){
					$body.="<td class=\"content-small-10 b\">&nbsp;Delivery At : </td>
						<td class=\"content-small-10\">".$data['delivery_at']."</td>";
				}
				$body.="</tr>
				</table>";
		$body.="</td>";	
		$body.= "</tr>     
		</table>
		</div> ";
	
	if (!empty($data['tax1_declaration'])){
	$body.="
		<div class=\"row wpx731 pt5 pb5\">		
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" width=\"731px\">	
				<tr>
					<td align=\"left\" class=\"pl10 pr10\">
						<span class=\"content-small-7\">".$data['tax1_declaration']."</span>							
					</td>
				</tr>	
			</table>
		</div> ";
	}
	/* if (!empty($data['po_terms_condition'])){
	$body.="
		<div class=\"row wpx731 pt5 pb5\">		
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" width=\"731px\">	
				<tr>
					<td align=\"left\" class=\"pl10 pr10\">
						<span class=\"content-small-7\">".$data['po_terms_condition']."</span>							
					</td>
				</tr>	
			</table>
		</div> ";
	} */
	
	
	
	//If inv date is greater than equal to date implement of tm - ecraft NC
	 
$body.="<div class=\"row wpx731 bg9 pb3 pt3\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl20\" width=\"393px\">
					<span class=\"white-text b\">".$data['company_name']."
				</td>
				<td class=\"address-text ar pr10\">
					<span class=\"white-text-12 b\">www.GroupSMEERP.com</span> 
				</td>
			</tr>
		</table>
		</div>
		<div class=\"row wp731 pt5 pb5 bg8 \">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl20\" width=\"265px\">
				<span class=\"white-text\"><b>Questions?</b></span> Contact us with a support ticket <br/>or call us at the numbers to the right.
			</td>
			<td class=\"address-text ar pr10\" width=\"466px\">
				Plot No 17, MG street | Jhena 440 025 Maharashtra | India<br/>
				<b>INDIA</b> : +91 9823099996  |   <b>USA & CANADA</b> : 1-837-621-1000
			</td>
		</tr>
		</table>
		</div>
		<div class=\"row wpx731 pt6\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"right\">
				<tr>
					<td class=\"design-text vt ar\">Designed by</td>
					<td class=\"pl5 vt pt2\"><img src=\"".$variables['images']."/invoice-studio-new.jpg\" border=\"0\" /></td>
				</tr>
			</table>
		</div>
</div>
" ;
$body.="</div>
</div>";
    $invoiceArr['content'] = $body ; 
	 
    $invoiceArr['header'] = '' ;
    $invoiceArr['footer'] = '' ;
    createPoPdf($data["number"],$invoiceArr);
}



        function getBifurcation( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;

            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_INV_PROFORMA_B;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }

    }
    
    
function createPoPdf( $q_no, $data ) {
                require_once(DIR_FS_ADDONS .'/html2pdf/config.popdf.inc.php');
                require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
                require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
                parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
              
                global 	$g_config;
                
                class ReportData extends Fetcher {
                    var $content;
                    var $url;
                
                    function get_data($data) {
                        return new FetchedDataURL($this->content, array(), "","");
                    }
                
                    function ReportData($content) {
                        $this->content = $content;
                    }
                    
                    function get_base_url() {
                        return "";
                    }
                }
                
                $contents 	= '';
                $contents = $data['content'];      
                
                // Configuration Settings.
                $g_config = array(
                                    'process_mode' 	=> 'single',
                                    'mode'		    => '',
                                    'pixels'		=> '750',
                                    'scalepoints'	=> '1',
                                    'renderimages'	=> '1',
                                    'renderlinks'	=> '1',
                                    'renderfields'	=> '1',
                                    'media'			=> 'A4',
                                    'cssmedia'		=> 'screen',
                                    //'leftmargin'	=> '30',
                                    'leftmargin'	=> '5',
                                    //'rightmargin'	=> '15',
                                    'rightmargin'	=> '5',
                                    'topmargin'		=> '5',
                                    'bottommargin'	=> '5',
                                    'encoding'		=> 'iso-8859-1',
                                    //'encoding'		=> 'UTF-8',
                                    'headerhtml'	=> $data['header'],
                                    'boydhtml'		=> $contents,
                                    'footerhtml'	=> $data['footer'],
                                    'watermarkhtml'	=> '',
                                    'pslevel'		=> '3',
                                    'method'		=> 'fpdf',
                                    'pdfversion'	=> '1.3',
                                    'output'		=> '2',
                                    'convert'		=> 'Convert File'
                                );
                
                $media = Media::predefined('A4');
                $media->set_landscape(false);
                /*
                $media->set_margins(array(	'left' => 10,
                                            'right' => 10,
                                            'top' => 15,
                                            'bottom' => 35)
                                    );
                $media->set_pixels(700);
                */
                 $media->set_margins(array(	'left' => 5,
                                            'right' => 5,
                                            'top' => 5,
                                            'bottom' => 5)
                                    );
                $media->set_pixels(740);
                
                $g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
                $g_pt_scale = $g_px_scale * 1.43;
                
                $pipeline 					= PipelineFactory::create_default_pipeline("","");
                $pipeline->pre_tree_filters[] = new PreTreeFilterHeaderFooter( $g_config['headerhtml'], 
				$g_config['footerhtml']);
                $pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
                if ($g_config['renderfields']) {
                    $pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
                }
        
                // Changed so that script will try to fetch files via HTTP in this case) and to provide absolute 
				//paths to the image files.
                // http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
                //		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
                $pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
                //$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
                $pipeline->destination 			= new DestinationFile($q_no);
                 
                $pipeline->process($q_no, $media);
                $filename = DIR_FS_PO_FILES.'/'. $q_no .'.pdf'; 
                @chmod($filename, 0775);
                $contents = NULL;
                return true;
}
            
            

			

    
?>
