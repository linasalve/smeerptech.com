<?php 

class NewslettersMembers {
		const EMAILBOUNCE = 1;
		const UNSUBSCRIBED = 1;
		
    function getRestrictions($type='') {
		// 1024 * 1024 = 1048576 = 1MB
        $restrictions = array(
                            'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), 
                            'MIME'      => array('text/csv','application/vnd.ms-excel', 'text/plain','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
                            'EXT'       => array('csv', 'txt','xlsx')
                             );
        
        if ( empty($type) ) {
            return ($restrictions);
        }
        else {
            return($restrictions[$type]);
        }
    }
    function validateAdd($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
		if(empty($data['email'])){
			$messages->setErrorMessage('Enter valid Email address.');
		}
		if ( !isValidEmail($data['email']) ){
			$messages->setErrorMessage('Email Address is not valid.');
		}else{
			$table = TABLE_NEWSLETTERS_MEMBERS;
			
			$condition1 = " WHERE email='".$data['email']."' AND  nws_category_id ='".$data['nws_category_id']."' " ;
			if ( NewslettersMembers::duplicateFieldValue($db,'email', $table, $condition1 )) {
				$messages->setErrorMessage('Email '.$data['email']." is already exist in database");
			}
		}
		if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        } else {
            return (false);
        }
    }
	
	function validateUpdate($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
		
		if(empty($data['email'])){
			$messages->setErrorMessage('Enter valid Email address.');
		}
		if ( !isValidEmail($data['email']) ){
			$messages->setErrorMessage('Email Address is not valid.');
		}else{
			$table = TABLE_NEWSLETTERS_MEMBERS;
			
			$condition_query1=" WHERE ".TABLE_NEWSLETTERS_MEMBERS.".email='".$data['email']."' AND  nws_category_id ='".$data['nws_category_id']."'
			AND ".TABLE_NEWSLETTERS_MEMBERS.".id!=".$data['id'] ;
			if ( NewslettersMembers::getList( $db, $list, 'id', $condition_query1)>1 ) {
				$messages->setErrorMessage('Email '.$data['email']." is already exist in database");
			}
		}
		if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        } else {
            return (false);
        }
    }
    function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
		$query = "SELECT DISTINCT ";
		$count = $total=0;
		if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
			$query .= implode(",", $required_fields);
		}
		elseif ( !empty($required_fields) ) {
			$query .= " ". $required_fields;
		}
		else {
			$query .= " COUNT(*) as count";
			$count = 1;
		} 
		$query .= " FROM ". TABLE_NEWSLETTERS_MEMBERS; 
		$query .= " ". $condition;
  
		if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
			$query .= " LIMIT ". $from .", ". $rpp;
		}
	
		if ( $db->query($query) ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$list[] = processSqlData($db->result());
					if($count==1){
						$total =$db->f("count") ;							
					}else{
						$total =$db->nf();
					}
				}
			}
			
			return ( $total );
		}
		else {
			return false;
		}	
	}
	function getCategory(&$db, &$list, $required_fields='', $condition='', $from='', $rpp=''){
		$query = "SELECT DISTINCT ";
		$count = $total=0;
		if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
			$query .= implode(",", $required_fields);
		}
		elseif ( !empty($required_fields) ) {
			$query .= " ". $required_fields;
		}
		else {
			$query .= " COUNT(*) as count";
			$count = 1;
		} 
		$query .= " FROM ". TABLE_NEWSLETTERS_MEMBERS_CATEGORY; 
		$query .= " ". $condition;
  
		if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
			$query .= " LIMIT ". $from .", ". $rpp;
		}
	
		if ( $db->query($query) ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$list[] = processSqlData($db->result());
					if($count==1){
						$total =$db->f("count") ;							
					}else{
						$total =$db->nf();
					}
				}
			}
			
			return ( $total );
		}
		else {
			return false;
		}	
	
	}
    function getTables(&$db) {
        $count  = 0;
        $tables = array();
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[] = $temp[$i]['table_name'];
        }
        $temp   = NULL;
        return($tables);
    }
    
    
    function getTablesData(&$db, &$tables) {
        $count  = 0;
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[$i] = array('table_name'=> $temp[$i]['table_name'],
                                'fields'    => $db->metadata($temp[$i]['table_name'])
                               );
        }
        $temp   = NULL;
        return($count);
    }
    
    
    function validateUpload($data, $extra) {
		foreach ($extra as $key=>$value) {
            $$key = $value;
        }
		 
		if(empty($data['nws_category_id'])){
			$messages->setErrorMessage('Select Category.');
		}
        if ( empty($file['file_csv']['error']) ) {
            if ( !empty($file['file_csv']['tmp_name']) ) {
                if ( $file['file_csv']['size'] == 0 ) {
                    $messages->setErrorMessage('The Uploaded file is empty.');
                }
                if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
                    $messages->setErrorMessage('The File type is not allowed to be uploaded.');
                }
                if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
                    $messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
                }
            }
            else {
                $messages->setErrorMessage('Server Error: File was not uploaded.');
            }
        }
        else {
            $messages->setErrorMessage('Unexpected Error: File was not uploaded.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    
    
    
    function duplicateFieldValue(&$db, $table, $field_name, $field_condition) {
        $query = "SELECT $field_name FROM $table ".$field_condition;
        
        if ( $db->query($query) && $db->nf()>0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
	
	function askDataEntry(&$db1, $data,$extra){
		foreach ($extra as $key=>$value) {
            $$key = $value;
        }
		if(!empty($data['email'])){
			//check already exist email in d/b
			$email = trim($data['email']);
			$sql = "SELECT user_id FROM ".TABLE_ASK_CUSTOMER." WHERE username='".$email."'" ;
			$db1->query($sql);
			$count =  $db1->nf() ;
			if ( $count == 0 ) {
				
				$user_id = md5($email);
				$data["password"] = md5('smeerp102');
				
				$query="INSERT INTO ".TABLE_ASK_CUSTOMER
				." SET ".TABLE_ASK_CUSTOMER .".user_id = '".$user_id ."'" 
				.",". TABLE_ASK_CUSTOMER .".f_name = '".processUserData($data['f_name'])."'"  
				.",". TABLE_ASK_CUSTOMER .".l_name = '".processUserData($data['l_name'])."'"  
				.",". TABLE_ASK_CUSTOMER .".username = '".processUserData($data['username'])."'"  
				.",". TABLE_ASK_CUSTOMER .".email = '".processUserData($data['email'])."'"  
				.",". TABLE_ASK_CUSTOMER .".mobile_1 = '".processUserData($data['mobile_1'])."'"  
				.",". TABLE_ASK_CUSTOMER .".mobile_2 = '".processUserData($data['mobile_2'])."'"  
				.",". TABLE_ASK_CUSTOMER .".city = '".processUserData($data['city'])."'"  
				.",". TABLE_ASK_CUSTOMER .".state = '".processUserData($data['state'])."'"  
				.",". TABLE_ASK_CUSTOMER .".country = '".processUserData($data['country'])."'"  
				.",". TABLE_ASK_CUSTOMER .".address = '".processUserData($data['address'])."'"  
				.",". TABLE_ASK_CUSTOMER .".phone = '".processUserData($data['phone'])."'"  
				.",". TABLE_ASK_CUSTOMER .".do_add = '".date('Y-m-d H:i:s')."'"  
				.",". TABLE_ASK_CUSTOMER .".added_by_script = '1'"  
				.",". TABLE_ASK_CUSTOMER .".site_id = '".SITE_ID."'"  ;
				
				$db1->query($query);
				$added_ask++;
			}
			else {
				 $duplicate_ask++;
			}
		}else{
			$empty_ask++;
		}
	
	}
	
	function taskDataEntry(&$db1, $data,$extra){
		foreach ($extra as $key=>$value) {
            $$key = $value;
        }
		if(!empty($data['email'])){
			//check already exist email in d/b
			$email = trim($data['email']);
			$sql = "SELECT uid FROM ".TABLE_TASK_MEMBERS." WHERE username = '".$email."'" ;
			$db1->query($sql);
			$count =  $db1->nf() ;
			if ( $count == 0 ) { 
				$user_id = md5($email);
				$data["password"] = md5('smeerp102'); 
				
				/*
				$query="INSERT INTO ".TABLE_TASK_MEMBERS
				." SET ".TABLE_TASK_MEMBERS .".user_id = '".$user_id ."'" 
				.",". TABLE_TASK_MEMBERS .".f_name = '".processUserData($data['f_name'])."'"  
				.",". TABLE_TASK_MEMBERS .".l_name = '".processUserData($data['l_name'])."'"  
				.",". TABLE_TASK_MEMBERS .".username = '".processUserData($data['username'])."'"  
				.",". TABLE_TASK_MEMBERS .".email = '".processUserData($data['email'])."'"  
				.",". TABLE_TASK_MEMBERS .".mobile_1 = '".processUserData($data['mobile_1'])."'"  
				.",". TABLE_TASK_MEMBERS .".mobile_2 = '".processUserData($data['mobile_2'])."'"  
				.",". TABLE_TASK_MEMBERS .".city = '".processUserData($data['city'])."'"  
				.",". TABLE_TASK_MEMBERS .".state = '".processUserData($data['state'])."'"  
				.",". TABLE_TASK_MEMBERS .".country = '".processUserData($data['country'])."'"  
				.",". TABLE_TASK_MEMBERS .".address = '".processUserData($data['address'])."'"  
				.",". TABLE_TASK_MEMBERS .".phone = '".processUserData($data['phone'])."'"  
				.",". TABLE_TASK_MEMBERS .".do_add = '".date('Y-m-d H:i:s')."'" 
				.",". TABLE_TASK_MEMBERS .".site_id = '".SITE_ID."'"  ;
				*/
				$query 	= "INSERT INTO "
								. TABLE_TASK_MEMBERS
								." SET "
								." uid				= '". $user_id ."', "
								." username			= '". processUserData($data['email']) ."', "
								." password			= '". $data["password"] ."', "
								." firstname		= '". processUserData($data['f_name'])."', "
								." lastname			= '".processUserData($data['l_name']) ."', "
								." email			= '". processUserData($data['email']) ."', "							    ." perms			= 'USER', "
								." status			= '1',"
								." date_of_open		= '". mktime(0,0,0) ."'";
				
				
				$db1->query($query);
				$added_task++;
			}
			else {
				 $duplicate_task++;
			}
		}else{
			$empty_task++;
		}
	}
	
	function buzzDataEntry(&$db1, $data,$extra){
		foreach ($extra as $key=>$value) {
            $$key = $value;
        }
		if(!empty($data['email'])){
			//check already exist email in d/b
			$email = trim($data['email']);
			$sql = "SELECT user_id FROM ".TABLE_TASKASK_CUSTOMER." WHERE username='".$email."'" ;
			$db1->query($sql);
			$count =  $db1->nf() ;
			if ( $count == 0 ) { 
				$user_id = md5($email);
				$query="INSERT INTO ".TABLE_TASK_CUSTOMER
				." SET ".TABLE_TASK_CUSTOMER .".user_id = '".$user_id ."'" 
				.",". TABLE_TASK_CUSTOMER .".f_name = '".processUserData($data['f_name'])."'"  
				.",". TABLE_TASK_CUSTOMER .".l_name = '".processUserData($data['l_name'])."'"  
				.",". TABLE_TASK_CUSTOMER .".username = '".processUserData($data['username'])."'"  
				.",". TABLE_TASK_CUSTOMER .".email = '".processUserData($data['email'])."'"  
				.",". TABLE_TASK_CUSTOMER .".mobile_1 = '".processUserData($data['mobile_1'])."'"  
				.",". TABLE_TASK_CUSTOMER .".mobile_2 = '".processUserData($data['mobile_2'])."'"  
				.",". TABLE_TASK_CUSTOMER .".city = '".processUserData($data['city'])."'"  
				.",". TABLE_TASK_CUSTOMER .".state = '".processUserData($data['state'])."'"  
				.",". TABLE_TASK_CUSTOMER .".country = '".processUserData($data['country'])."'"  
				.",". TABLE_TASK_CUSTOMER .".address = '".processUserData($data['address'])."'"  
				.",". TABLE_TASK_CUSTOMER .".phone = '".processUserData($data['phone'])."'"  ;
				
				$db1->query($query);
				$added_ask++;
			}
			else {
				 $duplicate_ask++;
			}
		}else{
			$empty_ask++;
		}
	
	}
}
?>