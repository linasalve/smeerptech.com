<?php
	class Taskreminder {
		
		const COMPLETED = 1;
		const PENDING = 2;
        const HIGH = 1;
        const LOW = 2;
		const MEDIUM = 3;
     
		const ALLOTED_BY_ME = 1;
        const SHOW_ALL = 2;
        
        const AM = 1;
        const PM = 2;
        
        const SALES = 1;
        const ACCOUNTING = 2;
        //const TEAM = 3 ;	//no data 
        const SUPPORT = 4;  
        const PROGRAMMING = 5;  
        const ADMIN = 6;  
        //const THILEEBAN = 7;  
        const PROGRESSIVE = 8;    
        //const CEO = 9;    
        //const EHD = 10; 
		//const BILLING = 11;
		const PURCHASE = 12;
		const DESIGNING = 13;
		
		
        const CEOTASK = 1; 
		
		const TASK = 1;
        const QUERY = 2;
        const REMINDER = 3;

		function getStatus(){
			$status = array(
							'PENDING'   => Taskreminder::PENDING,
                            'COMPLETED'    => Taskreminder::COMPLETED
						);
			return ($status);
		}      
        
        function getPriority() {
			$priority = array(
							'HIGH'    => Taskreminder::HIGH,
							'LOW'       => Taskreminder::LOW,                            
							'MEDIUM'    => Taskreminder::MEDIUM                            
						);
			return ($priority);
		} 
		
		function getTType() {
			$type = array(  'Task'    => Taskreminder::TASK,
							'Query'      => Taskreminder::QUERY,
							'Reminder'      => Taskreminder::REMINDER
						);
			return ($type);
		}
		
        function getTypes() {
			$type = array(  'SUPPORT'    => Taskreminder::SUPPORT,
							'SALES'      => Taskreminder::SALES,
							'ACCOUTING'	 => Taskreminder::ACCOUNTING,
							//'TEAM'  	 => Taskreminder::TEAM, 
							'PROGRAMMING' => Taskreminder::PROGRAMMING,
							'ADMIN'   	 => Taskreminder::ADMIN,
							//'BILLING'    => Taskreminder::BILLING,
							'PURCHASE'   => Taskreminder::PURCHASE,
							'DESIGNING'   => Taskreminder::DESIGNING,
							//'THILEEBAN'  => Taskreminder::THILEEBAN,
							//'PROGRESSIVE'    => Taskreminder::PROGRESSIVE,
							//'CEO'    => Taskreminder::CEO,
							//'EMAIL-HOSTING-DOMAIN'    => Taskreminder::EHD
							//'CEOTASK'    => Taskreminder::CEOTASK'
						); 
			return ($type);
		}
        function getTimeType() {
			$time_type = array(
							'AM'    => Taskreminder::AM,
							'PM'       => Taskreminder::PM                            
						);
			return ($time_type);
		}        
        function getNewNumber(&$db){
            
            $task_no= 3000;
            $query      = "SELECT MAX(task_no) AS task_no FROM ". TABLE_TASK_REMINDER;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("task_no") > 0){
                $task_no = $db->f("task_no");
            }
			$task_no++;
            return $task_no;
        }
		
        function getTaskDetails($taskNo){
            $db 		= new db_local; // database handle
            $task_id=0;
            $task_status='';
            $taskdetails=array();
            $taskdetails['task_id']=  '';
            $taskdetails['task_status']='';
            $taskdetails['ticket_priority']='';
            $taskdetails['task_number']='';
            $taskdetails['task_subject']='';
            $taskdetails['task_do_r']='';
            $taskdetails['task']='';
            
            $query      = "SELECT id, status,priority,comment,do_r FROM ". TABLE_TASK_REMINDER." WHERE task_no ='".$taskNo."'";
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("id") > 0){
                $task_id = $db->f("id");
                $task_status = $db->f("status");
                $task_priority = $db->f("priority");
                $task_subject = $db->f("comment");
                $task_do_r = $db->f("do_r");
                $taskdetails['task_id']=  $task_id ;
                $taskdetails['task_status']=  $task_status ;
                $taskdetails['task_priority']=  $task_priority ;
                $taskdetails['task_number']=  $taskNo ;
                $taskdetails['task_do_r']=  $task_do_r ;
                $taskdetails['task_subject']=  $task_subject ;
                $taskdetails['task']=  $task_subject ;
            }
			
            return $ticketdetails;
        }
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total = 0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TASK_REMINDER;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_TASK_REMINDER;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                 return ( $total );
            }
            else {
                return false;
            }   
        }
       
	    //Used in communication log
	    function getDetailsCommLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_TASK_REMINDER_LOG;
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_TASK_REMINDER_LOG .".by_id  ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }

        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (Taskreminder::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               $details = $details[0];
                //echo $details['access_level']."<br>";
                //echo $access_level;
                
                /*if ( $details['access_level'] < $access_level ) {*/
                    $query = "DELETE FROM ". TABLE_TASK_REMINDER
                                ." WHERE id = '$id'";
                                
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                /*}
                else {
                    $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                }*/
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }   
        }
    
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}  
			$files = $_FILES;
            
 		    if ( !isset($data['task']) || empty($data['task']) ) {
                $messages->setErrorMessage("Task Title can not be empty.");
            } 
			 if ( !isset($data['type']) || empty($data['type']) ) {
                $messages->setErrorMessage("Select Task type");
            }  
            if ( !isset($data['comment']) || empty($data['comment']) ) {
                $messages->setErrorMessage("Task can not be empty.");
            }
            $data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){
                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file is greater than '.$data['max_file_size_name']);
                }
            }
            if (  empty($data['allotted_to']) && empty($data['allotted_to_client']) && 
			empty($data['allotted_to_vendor']) && empty($data['allotted_to_lead']) && empty($data['allotted_to_addr']) && empty($data['groups'])) {
                $messages->setErrorMessage("Select Executives OR Clients Or Vendor Or Leads Or External users.");
            }
            if(!empty($data['groups']) && !empty($data['allotted_to'])){
				$messages->setErrorMessage("Select Executives OR Group.");
			}
            if ( !isset($data['do_r']) || empty($data['do_r']) ) {
                $messages->setErrorMessage("Select Date of Deadline.");
            }
			if(!empty($data['groups'])){
				$groupsStr='';
				$groups1=implode(",",$data['groups']);
				$groupsStr = ",".$groups1.",";
				
				
				foreach($data['groups'] as $key=>$val){
				
					$group_like_q .= TABLE_USER.".groups LIKE '%,".trim($val).",%' OR " ;
				}
				
				$group_like_q = substr( $group_like_q, 0, strlen( $group_like_q ) - 3 );
				
				$sql= " SELECT f_name,l_name,user_id FROM ".TABLE_USER." WHERE ".$group_like_q." LIMIT 0,1";
				$db->query($sql);
				if ( $db->nf()>0 ) {
				
				}else{
					$messages->setErrorMessage("No members are found in selected Groups.");
				
				}
			}
			
            
            if ( !isset($data['do_r']) || empty($data['do_r']) ) {
                $messages->setErrorMessage("Select Date of Deadline.");
            }
            // Check for start date
            if(!empty($data['do_r'])){
                $dateArr= explode("/",$data['do_r']);
                $data['do_r'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            if ( !empty($data['do_deadline']) ) {
                $dateArr= explode("/",$data['do_deadline']);
                $data['do_deadline'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
          
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		function validateCommentAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$files = $_FILES;
            /* if ( !isset($data['to_id']) || empty($data['to_id']) ) {
                $messages->setErrorMessage("Select Executive.");
            }*/
            if ( !isset($data['comment']) || empty($data['comment']) ) {
                $messages->setErrorMessage("Task Comment can not be empty.");
            }
            $data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){
                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file is greater than '.$data['max_file_size_name']);
                }
            }
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}

        }
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            } 
			$files = $_FILES;			
             
			if ( !isset($data['task']) || empty($data['task']) ) {
                $messages->setErrorMessage("Task Title can not be empty.");
            } 
			 
            if ( !isset($data['comment']) || empty($data['comment']) ) {
                $messages->setErrorMessage("Task can not be empty.");
            }
			
			$data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){
                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file is greater than '.$data['max_file_size_name']);
                }
            }
			
           if (  empty($data['allotted_to']) && empty($data['allotted_to_client']) && empty($data['allotted_to_lead']) && empty($data['allotted_to_addr']) && empty($data['groups'])) {
                $messages->setErrorMessage("Select Executives OR Clients Or Leads Or External users.");
            }
			if(!empty($data['groups']) && !empty($data['allotted_to'])){
				$messages->setErrorMessage("Select Executives OR Group.");
			}
             if ( !isset($data['do_r']) || empty($data['do_r']) ) {
                $messages->setErrorMessage("Select Date of Deadline.");
            }
			if(!empty($data['groups'])){
				$groupsStr='';
				$groups1 = implode(",",$data['groups']);
				$groupsStr = ",".$groups1.",";
							
				foreach($data['groups'] as $key=>$val){
				
					$group_like_q .= TABLE_USER.".groups LIKE '%,".trim($val).",%' OR " ;
				}
				$group_like_q = substr( $group_like_q, 0, strlen( $group_like_q ) - 3 );
				
				$sql= " SELECT f_name,l_name,user_id FROM ".TABLE_USER." WHERE ".$group_like_q." LIMIT 0,1";
				$db->query($sql);
				if ( $db->nf()>0 ) {
				
				}else{
					$messages->setErrorMessage("No members are found in selected Groups.");
				
				}
			}
            if ( !isset($data['do_r']) || empty($data['do_r']) ) {
                $messages->setErrorMessage("Select Date of Deadline.");
            }
          
            // Check for start date
            if(!empty($data['do_r'])){
                $dateArr= explode("/",$data['do_r']);
                $data['do_r'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
                 
                /*$currDate=date('Y-m-d');
                if($data['do_r'] < $currDate){
                     $messages->setErrorMessage("Please select proper date.");
                }*/
            }
             
            if ( !empty($data['do_deadline']) ) {
                $dateArr= explode("/",$data['do_deadline']);
                $data['do_deadline'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }              
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function getHistoryList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TASK_REMINDER_HISTORY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				return ( $total );
			}
			else {
				return false;
			}	
		}
		
    }
?>
