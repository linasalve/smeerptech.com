<?php


	function ajaxResponse($data) {
		$str = '';
		$str .= '[';
		if ( is_array($data) ) {
			$count 	= count($data);
			$i		= 0;
			
			foreach ( $data as $key=>$value ) {
				$i += 1;
				
				if ( is_array($value) ) {
					
					$str .= ajaxResponse($value);
					
				}
				else {
					$str .= "'". $value ."'";
				}
				if ( $i<$count ) {
					$str .= ',';
				}
			}
		}
		else {
			$str = $data;
		}
		$str .= ']';
		
		return ($str);
	}
?>