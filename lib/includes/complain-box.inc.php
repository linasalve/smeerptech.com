<?php
	class Complainbox {
		
		 const COMPLETED = 1;
		const PENDING = 2;
        const HIGH = 1;
        const LOW = 2;
		const MEDIUM = 3;
     
		

		function getStatus() {
			$status = array(
							'PENDING'   => Complainbox::PENDING,
                            'COMPLETED'    => Complainbox::COMPLETED
						);
			return ($status);
		}      
        function getPriority() {
			$priority = array(
							'HIGH'    => Complainbox::HIGH,
							'LOW'       => Complainbox::LOW,                            
							'MEDIUM'    => Complainbox::MEDIUM                            
						);
			return ($priority);
		}         
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_COMPLAIN_BOX ;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_COMPLAIN_BOX;
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_COMPLAIN_BOX .".complain_of  ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (Complainbox::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               $details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_COMPLAIN_BOX
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                /*}
                else {
                    $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                }*/
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
           
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage("Title can not be empty.");
            }
            if ( !isset($data['complain']) || empty($data['complain']) ) {
                $messages->setErrorMessage("Complain can not be empty.");
            }
            
            if(empty($data['complain_of']) ){                
                $messages->setErrorMessage("Select Executive.");
            }
           
            
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage("Title can not be empty.");
            }
            if ( !isset($data['complain']) || empty($data['complain']) ) {
                $messages->setErrorMessage("Complain can not be empty.");
            }
            
            if(empty($data['complain_of']) ){                
                $messages->setErrorMessage("Select Executive.");
            }
           
                       
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
		
    }
?>
