<?php
	class PreQuickOrder{
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
        const DELETED = 3;
		const COMPLETED = 4;

		function getStatus() {
			$status = array('ACTIVE'    => PreQuickOrder::ACTIVE,
							'PENDING'   => PreQuickOrder::PENDING,
							'CANCELLED' => PreQuickOrder::BLOCKED,
                            'DELETED'   => PreQuickOrder::DELETED,
                            'PROCESSED' => PreQuickOrder::COMPLETED
						);
			return ($status);
		}
        
		/**
		*	Function to get all the Pre Orders.
		*
		*	@param Object of database
		*	@param Array of User role
		* 	@param required fields
		* 	@param condition
		*	return array of User roles
		*	otherwise return NULL
		*/		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_BILL_PQO;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->Record);
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}

        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_PQO;
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_PQO .".created_by ";
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->Record);
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   //This function is used to perform action on status.		
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not. 
            if ( !in_array($status_new, PreQuickOrder::getStatus()) ) {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
            else {
                 $query = "UPDATE ". TABLE_BILL_PQO
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                if ( $db->query($query)) {
                    $messages->setOkMessage("The Status has been updated.");
                }
                else {
                    $messages->setErrorMessage("The Status was not updated.");
                } 
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $pre_order = NULL;
            $query = "DELETE FROM ". TABLE_BILL_PQO
                                        ." WHERE id = '$id'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Pre Quick Order has been deleted.");
            }
            else {
                $messages->setErrorMessage("The Pre Quick Order was not deleted.");
            }
            /*
            if ( (PreQuickOrder::getList($db, $pre_order, 'id, access_level, status', " WHERE id = '$id'")) > 0 ) {
                $pre_order = $pre_order[0];
                if ( $pre_order['status'] != PreQuickOrder::COMPLETED ) {
                    if ( $pre_order['status'] == PreQuickOrder::BLOCKED ) {
                        if ( $pre_order['access_level'] < $access_level ) {
                            $query = "DELETE FROM ". TABLE_BILL_PQO
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Pre Order has been deleted.");
                            }
                            else {
                                $messages->setErrorMessage("The Pre Order was not deleted.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Pre Order with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("To Delete Pre Order it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Pre Order cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Pre Order was not found.");
            }
            */
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
    

		 /**
		 * Function to validate the input from the User while Adding a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd($data, &$messages, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Validate the Quotation.
            /*
            if ( isset($data['q_no']) && !empty($data['q_no']) ) {
                $query = "SELECT id, status FROM ". TABLE_BILL_QUOTE
                        ." WHERE number = '". $data['q_no'] ."'";
                if ( $db->query($query) && $db->nf() <= 0 ) {
                    $messages->setErrorMessage("The Quotation was not found in the database.");
                }
                else {
                    if ( $db->next_record() && $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Quotation is not yet approved.");
                    }
                }
			}

            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, status FROM ". TABLE_USER
                        ." WHERE user_id = '". $data['created_by'] ."'"
                        ." OR username = '". $data['created_by'] ."'"
                        ." OR number = '". $data['created_by'] ."'" ;
                if ( $db->query($query) && $db->nf() <= 0 ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    if ( $db->next_record() && $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creators' status is not Active.");
                    }
                }
            }
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                   if(!empty($val["name"])){
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_file_size']){
                 $messages->setErrorMessage('Size of all files is greater than 2Mb');
            }
            
            if ( !isset($data['order_details']) || empty($data['order_details']) ) {
                $messages->setErrorMessage("Order Details should be provided.");
			}
            
            if ( !isset($data['client_details']) || empty($data['client_details']) ) {
                $messages->setErrorMessage("Client Details should be provided.");
            }
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status of the User Right is not valid.");
			}
            */
            if ( !isset($data['order_details']) || empty($data['order_details']) ) {
                $messages->setErrorMessage("Order Details should be provided.");
			}
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate($data, &$messages, $extra='',$urights_id='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            // Validate the Quotation.
            /*
            if ( isset($data['q_no']) && !empty($data['q_no']) ) {
                $query = "SELECT id, status FROM ". TABLE_BILL_QUOTE
                            ." WHERE number = '". $data['q_no'] ."'";
                if ( $db->query($query) && $db->nf() <= 0 ) {
                    $messages->setErrorMessage("The Quotation was not found in the database.");
                }
                else {
                    if ( $db->next_record() && $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Quotation is not yet approved.");
                    }
                }
            }

            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( $db->query($query) && $db->nf() <= 0 ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    if ( $db->next_record() && $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creators' status is not Active.");
                    }
                }
            }
            
            if ( !isset($data['order_details']) || empty($data['order_details']) ) {
                $messages->setErrorMessage("Order Details should be provided.");
            }
            
            if ( !isset($data['client_details']) || empty($data['client_details']) ) {
                $messages->setErrorMessage("Client Details should be provided.");
            }
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                $messages->setErrorMessage("The Status of the User Right is not valid.");
            }*/
            
            if ( !isset($data['order_details']) || empty($data['order_details']) ) {
                $messages->setErrorMessage("Order Details should be provided.");
			}

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
}
?>
