<?php
	class PreQuote {
		
		const REJECTED = 0;
		const ACTIVE  = 1;
		const APPROVED = 2;
        
		function getStatus() {
			$status = array('REJECTED' => PreQuote::REJECTED,
							'ACTIVE'   => PreQuote::ACTIVE,
							'APPROVED' => PreQuote::APPROVED
						);
			return ($status);
		}
        
        /*function getTypes() {
			$status = array('WALKIN' => PreQuote::WALKIN,
							'TARGETED'    => PreQuote::TARGETED
						);
			return ($status);
		}*/
        
		/**
		 *	Function to get all the PreQuotes.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SALE_PRE_QUOTE;			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            /*
            $query .= " FROM ". TABLE_BILL_PreQuoteS;
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_SALE_PRE_QUOTE .".client ";
            */
            $query .= " FROM ". TABLE_SALE_PRE_QUOTE;
            /*$query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_SALE_PRE_QUOTE .".created_by ";
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_SALE_PRE_QUOTE .".client ";
                        
            $query .= " LEFT JOIN ". TABLE_BILL_INV
                        ." ON ". TABLE_BILL_INV .".or_no = ". TABLE_SALE_PRE_QUOTE .".number ";*/
                        
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	   function getDashboard( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SALE_PRE_QUOTE;
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_SALE_PRE_QUOTE .".client ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
					
					if ( strpos($required_fields, 'team') > 0 ) {
						include_once ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$count = count($list);
						for ( $i=0; $i<$count; $i++ ) {
							// Read the Team Member Details.
							$list[$i]['team'] = "'". implode("','", (explode(',', $list[$i]['team']))) ."'";
							$list[$i]['team_members']= '';
                            
							User::getList($db, $list[$i]['team_members'], 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $list[$i]['team'] .")");
							
							// Read the latest Time Line update.
							include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
							$list[$i]['work'] = NULL;
							//WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, order_title, to_id, by_id, do_assign, comments, status');
							$fields = TABLE_WORK_TL.".id ,".TABLE_WORK_TL.".to_id, ".TABLE_WORK_TL.".by_id, ".TABLE_WORK_TL.".do_assign, ".TABLE_WORK_TL.".comments,".TABLE_WORK_TL.".status";
                            WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], $fields);
							$list[$i]['work'] 		= $list[$i]['work'][0];
							//$list[$i]['order_title']= $list[$i]['work']['order_title'];
							$list[$i]['comments'] 	= $list[$i]['work']['comments'];
                            
							User::getList($db, $list[$i]['work']['by'], 'user_id, number, f_name, l_name, email', "WHERE user_id = '". $list[$i]['work']['by_id'] ."'");
							$list[$i]['work']['by'] = $list[$i]['work']['by'][0];
						}
					}
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
	   
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( !in_array($status_new, PreQuote::getStatus()) ) {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
            else {
                $order = NULL;
                if ( (PreQuote::getList($db, $order, 'id, access_level, status', " WHERE id = '$id'")) > 0 ) {
                    $order = $order[0];
                    if ( $order['status'] != PreQuote::APPROVED ) {
                        if ( $order['access_level'] < $access_level ) {
                            $query = "UPDATE ". TABLE_SALE_PRE_QUOTE
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("Status cannot be changed: Order has been Approved.");
                    }
                }
                else {
                    $messages->setErrorMessage("The Pre Quote was not found.");
                }
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_ORD_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
    
        /*function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $order = NULL;
            if ( (PreQuote::getList($db, $order, 'id, number,access_level, status', " WHERE id = '$id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != PreQuote::COMPLETED ) {
                    if ( $order['status'] == PreQuote::BLOCKED ) {
                        if ( $order['access_level'] < $access_level ) {
                        
                            if((Invoice::getList($db, $invoice, 'id', " WHERE or_no = '".$order['number']."'") ) > 0){
                             
                                $messages->setErrorMessage("Please delete Invoice of order ".$order['number']."  ");
                             
                            }else{
                            
                                $query = "DELETE FROM ". TABLE_SALE_PRE_QUOTE
                                            ." WHERE id = '$id'";
                                if ( $db->query($query) && $db->affected_rows()>0 ) {
                                
                                    //Delete worktimeline for selected order bof
                                    $query = "DELETE FROM ". TABLE_WORK_TL
                                            ." WHERE order_no = '".$order['number']."' ";
                                    $db->query($query);
                                    //Delete worktimeline for selected order eof
                                    
                                
                                    $messages->setOkMessage("The Order has been deleted.");
                                    
                                    
                                    $query = "DELETE FROM ". TABLE_BILL_ORD_P
                                            ." WHERE ord_no = '".$order['number']."' ";
                                    $db->query($query);
                                    
                                    
                                }
                                else {
                                    $messages->setErrorMessage("The Order was not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Order with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("To Delete Order it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Order cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }*/
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {

			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Validate the Pre Order.
            /* comment on 2009-04-10 
            $list = NULL;
            if ( Order::getList($db, $list, 'id', " WHERE po_id = '". $data['po_id'] ."'")>0 ) {
                $messages->setErrorMessage("The Pre Order has been processed already.");
            }
            else {
                if ( isset($data['po_id']) && !empty($data['po_id']) ) {
                    $query = "SELECT access_level, status FROM ". TABLE_BILL_PO
                            ." WHERE id = '". $data['po_id'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage("The Pre Order was not found in the database.");
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            if ( $db->f('status') != ACTIVE ) {
                                $messages->setErrorMessage("The Pre Order is not yet approved.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                        }
                    }
                }
            }
            */
            // As date of Order is compulsary
            /*if(empty($data['do_o'])){
                 $messages->setErrorMessage("Select the date of Order.");
            }*/
            
            // Format the Date of order, if provided. order number will be generated on the basis od order date
            /*if ( isset($data['do_o']) && !empty($data['do_o']) ) {
                $data['do_o'] = explode('/', $data['do_o']);
                //$data['number'] = "PT". $data['do_o'][2] ."-OR-". $data['do_o'][1]. $data['do_o'][0].date("hi-s") ;
                $data['do_o'] = mktime(0, 0, 0, $data['do_o'][1], $data['do_o'][0], $data['do_o'][2]);
            }*/
            
            
            // As date of delivery is compulsary
            /*if(empty($data['do_d'])){
                 $messages->setErrorMessage("Select the date of Delivery.");
            }*/
            // Format the Date of Delivery, if provided.
            /*if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }*/
            
            
            /* comment on 2009-04-10
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creator's status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }
            */
            
            /*if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                    }
                }
            }*/

            // Validate the Executives Team.
            /*if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
            }
            else {
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result());
                    }
                    $team_count = count($data['team']);

                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }
                    // Uncomment the following if speed is preferred against a detailed error report as above.
                    //if ( $db->nf() < $team_count ) {
                    //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                    //}
                }
            }
            if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                $messages->setErrorMessage("Order Title should be provided.");
			}
            if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                $messages->setErrorMessage("Order Type should be provided.");
			}
            if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                $messages->setErrorMessage("Order Closed By should be provided.");
			}*/
            
            /*
            if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                $messages->setErrorMessage("Order Particulars should be provided.");
			}
            */
            
            /*if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }*/
            
            /*if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}*/
           
            
            
            
            // Check for the duplicate Order Number.
            $list = NULL;
            if ( PreQuote::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("A Pre Quote with the same Number already exists.<br/>Please try again.");
                //$messages->setErrorMessage("An Order with the same Number already exists.");
            }
            $list = NULL;
            // validate particulars bof
            
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }
            
            $data['query_p'] = 'INSERT INTO '. TABLE_SALE_PRE_QUOTE_P .' (pq_no, particulars, p_amount, s_id,sub_s_id, ss_title, ss_punch_line, tax1_name,tax1_value, tax1_pvalue,d_amount,stot_amount,tot_amount,discount_type,pp_amount,vendor,purchase_particulars ) VALUES ';
            $is_part = false;
            if(isset($data['particulars'])){
                foreach ( $data['particulars'] as $key=>$particular ) {
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
        
                                $is_part = true;
                                $data['query_p'] .= "('". $data['number'] ."', '". processUserData($data['particulars'][$key]) ."', '". $data['p_amount'][$key] ."','". $data['s_id'][$key] ."','".$data['sub_s_id'][$key] ."',
                                '". processUserData($data['ss_title'][$key]) ."', '". processUserData($data['ss_punch_line'][$key]) ."',
                                '". processUserData($data['tax1_name'][$key]) ."', '". processUserData($data['tax1_value'][$key]) ."','". processUserData($data['tax1_pvalue'][$key]) ."',
                                '". processUserData($data['d_amount'][$key]) ."', '". processUserData($data['stot_amount'][$key]) ."',
                                '". processUserData($data['tot_amount'][$key]) ."',
                                '". processUserData($data['discount_type'][$key]) ."', '". processUserData($data['pp_amount'][$key]) ."',
                                '". processUserData($data['vendor'][$key]) ."', '". processUserData($data['purchase_particulars'][$key]) ."'                           
                                )," ;
                            }
                        }
                    }
                }
            }
            if ( !$is_part ) {
                $messages->setErrorMessage('The Particular for the Pre Quote cannot be empty.');
            }
            else {
                $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
            }

            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Total Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            // Validate particulars eof
            
            // Check for start date
            /*if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }*/
            // Check for estimated End date
            /*if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }*/
            //Validate dates ie start date > est end date            
            /*if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }*/
            
            //Check Estimated hrs should not be characters
            /*if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }*/
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // First check if the Order exists or not?
            $list = NULL;
            if ( PreQuote::getList($db, $list, 'id, created_by', " WHERE id = '". $data['pq_id'] ."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                $list = NULL;
                
                // Validate the Pre Order.
                /*
                $list = NULL;
                if ( Order::getList($db, $list, 'id', " WHERE po_id = '". $data['po_id'] ."'")<=0 ) {
                    $messages->setErrorMessage("The Pre Order has not been processed.");
                }
                else {
                    if ( isset($data['po_id']) && !empty($data['po_id']) ) {
                        $query = "SELECT access_level, status FROM ". TABLE_BILL_PO
                                ." WHERE id = '". $data['po_id'] ."'";
                        if ( $db->query($query) && $db->nf() <= 0 ) {
                            $messages->setErrorMessage("The Pre Order was not found in the database.");
                        }
                        else {
                            $db->next_record();
                            if ( $access_level > $db->f('access_level') ) {
                                if ( $db->f('status') != COMPLETED ) {
                                    $messages->setErrorMessage("There is anomaly in the Status of the Pre Order.");
                                }
                            }
                            else {
                                $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                            }
                        }
                    }
                }*/
                
                if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                    $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
                }
                else {
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The Creator was not found.");
                    }
                    else {
                        $db->next_record();
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Creators' status is not Active.");
                        }
                        else {
                            $data['creator'] = processSQLData($db->result());
                        }
                    }
                }
    
                if ( !isset($data['client']) || empty($data['client']) ) {
                    $messages->setErrorMessage("Select the Client for whom the Order is being Created.");
                }
                else {
                    $query = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                                ." WHERE user_id = '". $data['client'] ."'";
                    if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                        $messages->setErrorMessage("The selected Client was not found.");
                    }
                    else {
                        $db->next_record();
                        //print_r($db->result());
                        if ( $db->f('status') != ACTIVE ) {
                            $messages->setErrorMessage("The Client is not Active.");
                        }
                        else {
                            $data['client'] = processSQLData($db->result());
                        }
                    }
                }
    
                // Validate the Executives Team.
                if ( !isset($data['team']) || empty($data['team']) ) {
                    $messages->setErrorMessage("Please associate atleast one Executive with the Order.");
                }
                else {
                    $team = "'". implode("','", $data['team']) ."'";
                    
                    $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                                ." WHERE user_id IN (". $team .") ";
                    if ( $db->query($query) ) {
                        $team_db = NULL;
                        while ($db->next_record()) {
                            $team_db[] = processSqlData($db->result());
                        }
                        $team_count = count($data['team']);
    
                        for ( $i=0; $i<$team_count; $i++ ) {
                            $index = array();
                            if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                            }
                            else {
                                if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                    $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                                }
                                else {
                                    $data['team_members'][$i] = $team_db[$index[0]];
                                }
                            }
                        }
                        // Uncomment the following if speed is preferred against a detailed error report as above.
                        //if ( $db->nf() < $team_count ) {
                        //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                        //}
                    }
                }
                if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                $messages->setErrorMessage("Order Title should be provided.");
                }
                if ( !isset($data['order_type']) || empty($data['order_type']) ) {
                    $messages->setErrorMessage("Order Type should be provided.");
                }
                if ( !isset($data['order_closed_by']) || empty($data['order_closed_by']) ) {
                    $messages->setErrorMessage("Order Closed By should be provided.");
                }
               /*
                if ( !isset($data['particulars']) || empty($data['particulars']) ) {
                    $messages->setErrorMessage("Order Particulars should be provided.");
                }*/
                
                if ( !isset($data['access_level']) ) {
                    $messages->setErrorMessage("Select the Access Level.");
                }
                elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                    $messages->setErrorMessage("Selected Access Level not found.");
                }
                
                if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                        && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                    $messages->setErrorMessage("The Status is not valid.");
                }
                
                // Format the Date of Delivery, if provided.
                if ( isset($data['do_d']) && !empty($data['do_d']) ) {
                    $data['do_d'] = explode('/', $data['do_d']);
                    $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
                }
                
                // Check for the duplicate Order Number.
                $list = NULL;
                $condition_query = " WHERE number = '". $data['number'] ."' AND id != '". $data['or_id'] ."'";
                if ( PreQuote::getList($db, $list, 'id', $condition_query)>0 ) {
                    $messages->setErrorMessage("An Order with the same Number already exists.");
                }
                $list = NULL;
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
            
             if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }
            
            //$data['query_p'] = 'INSERT INTO '. TABLE_BILL_ORD_P .' (ord_no, particulars, p_amount, s_id,sub_s_id, ss_title, ss_punch_line, tax1_name,tax1_value, tax1_pvalue,d_amount,stot_amount,tot_amount,discount_type,pp_amount,vendor,purchase_particulars ) VALUES ';
            $is_part = false;
            if(isset($data['particulars'])){
                foreach ( $data['particulars'] as $key=>$particular ) {
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
        
                                $is_part = true;
                                /*
                                $data['query_p'] .= "('". $data['number'] ."', '". processUserData($data['particulars'][$key]) ."', '". $data['p_amount'][$key] ."','". $data['s_id'][$key] ."','".$data['sub_s_id'][$key] ."',
                                '". processUserData($data['ss_title'][$key]) ."', '". processUserData($data['ss_punch_line'][$key]) ."',
                                '". processUserData($data['tax1_name'][$key]) ."', '". processUserData($data['tax1_value'][$key]) ."','". processUserData($data['tax1_pvalue'][$key]) ."',
                                '". processUserData($data['d_amount'][$key]) ."', '". processUserData($data['stot_amount'][$key]) ."',
                                '". processUserData($data['tot_amount'][$key]) ."',
                                '". processUserData($data['discount_type'][$key]) ."', '". processUserData($data['pp_amount'][$key]) ."',
                                '". processUserData($data['vendor'][$key]) ."', '". processUserData($data['purchase_particulars'][$key]) ."'                           
                                )," ;
                                */
                            }
                        }
                    }
                }
            }
            if ( !$is_part ) {
                $messages->setErrorMessage('The Particular for the Order cannot be empty.');
            }
            else {
                $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
            }

            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Total Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            
            
            
            
            
             // Check for start date
            if(!empty($data['st_date'])){
                $dateArr= explode("/",$data['st_date']);
                $data['st_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            // Check for estimated End date
            if(!empty($data['es_ed_date'])){
                $dateArr= explode("/",$data['es_ed_date']);
                $data['es_ed_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            }
            //Validate dates ie start date > est end date            
            if(!empty($data['st_date']) && !empty($data['es_ed_date']) ){
                if($data['es_ed_date'] < $data['st_date']){
                    $messages->setErrorMessage("Estimated end date should be less than start date.");
                }
            }
            //Check Estimated hrs should not be characters
            if ( isset($data['es_hrs']) && !empty($data['es_hrs']) ) {
                if(!is_numeric($data['es_hrs'])){
                    $messages->setErrorMessage("Estimated hours should be numeric.");
                }
            }
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Order has been created
         * against a Pre Order. This function is called only once for 
         * each Order.
         *
         */
        function setPreOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_BILL_PO
                        ." SET status = '$status'"
                        ." WHERE id = '$id'";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		
		/**
		 * This function is used to retrieve the Members of the Order.
		 * 
		 */
		function getTeamMembers($id, &$team_members, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$order = NULL;
			if ( PreQuote::getList($db, $order, 'team', " WHERE id = '$id'" ) > 0 ) {
				include_once ( DIR_FS_INCLUDES .'/user.inc.php');

				$order = $order[0];
				$order['team'] = "'". implode("','", (explode(',', $order['team']))) ."'";
                $order['team_members']= '';
                if ( User::getList($db, $team_members, 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $order['team'] .")") > 0 ) {
					return (true);
				}
			}
			return (false);
		}

        
		
		function getDeliveryDate(&$db, $order_id) {
			$query = "SELECT do_d FROM ". TABLE_SALE_PRE_QUOTE
						." WHERE id = '". $order_id ."'";
			$db->query($query);
			if ( $db->nf()>0 && $db->next_record() ) {
				return ($db->f('do_d'));
			}
			return ('');
		}
        
        //Function required for displaying the order_id + order_title from  work_timeline
        function getOrderList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SALE_PRE_QUOTE;
            $query .= " LEFT JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_SALE_PRE_QUOTE .".client ";
            $query .= " LEFT JOIN ". TABLE_WORK_TL." ON ". TABLE_WORK_TL .".order_id = ". TABLE_SALE_PRE_QUOTE .".id ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
					
					if ( strpos($required_fields, 'team') > 0 ) {
						include_once ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$count = count($list);
						for ( $i=0; $i<$count; $i++ ) {
							// Read the Team Member Details.
							$list[$i]['team'] = "'". implode("','", (explode(',', $list[$i]['team']))) ."'";
							$list[$i]['team_members']= '';
							User::getList($db, $list[$i]['team_members'], 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $list[$i]['team'] .")");
							
							// Read the latest Time Line update.
							include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
							$list[$i]['work'] = NULL;
							//WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, order_title, to_id, by_id, do_assign, comments, status');
							WorkTimeline::getLastComment( $db, $list[$i]['work'], $list[$i]['id'], 'id, to_id, by_id, do_assign, comments, status');
							$list[$i]['work'] 		= $list[$i]['work'][0];
							//$list[$i]['order_title']= $list[$i]['work']['order_title'];
							
							$list[$i]['comments'] 	= $list[$i]['work']['comments'];
							User::getList($db, $list[$i]['work']['by'], 'user_id, number, f_name, l_name, email', "WHERE user_id = '". $list[$i]['work']['by_id'] ."'");
							$list[$i]['work']['by'] = $list[$i]['work']['by'][0];
						}
					}
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
        
        function getAccessLevel(&$db, $level=0, $al_list='') {
            include_once ( DIR_FS_INCLUDES .'/settings-access-level.inc.php');

            $role_list = NULL;
            if ( !empty($al_list) ) {
                if ( is_array($al_list) ) {
                    $al_list = ' AND access_level IN ('. ('\''. implode('\',', $al_list) .'\'') .')';
                }
                else {
                    $al_list = ' AND access_level IN ('. ('\''. str_replace(',', '\',\'', $al_list) .'\'') .')';
                }
            }
            else {
                $al_list = '';
            }
            
            $access     = NULL;
            $condition  = " WHERE access_level < '". $level ."'"
                            . $al_list
                            ." AND status = '". AccessLevel::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            if ( (AccessLevel::getList($db, $access, 'title, access_level', $condition)) <= 0 ) {
                $access[] = array('title' => 'None', 'access_level' => '0' );			
            }
            
            return ($access);
        }
		
    }
?>
