<?php 

	class Categories {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => Categories::BLOCKED,
							'ACTIVE'  => Categories::ACTIVE,
							'PENDING' => Categories::PENDING,
							'DELETED' => Categories::DELETED
						);
			return ($status);
		}
		
        
		// This function creates the menu combobox 
		function getComboMenus($id=0,$level = 0) {
			global $output;
			
			
			$db_new 	= new DB_Local;
			 $query 		=" SELECT * FROM  "
								.TABLE_SETTINGS_CATEGORY
						." WHERE " .TABLE_SETTINGS_CATEGORY.".sc_parent_id='$id'"
						." AND ".TABLE_SETTINGS_CATEGORY.".sc_status='1' "
						." ORDER BY sc_cat_id";
			
			$db_new->query($query);
	
			if($db_new->nf() ==0) return;
			if ($db_new->nf() > 0 ) {
				
				while($db_new->next_record()) {

					$output[$db_new->f('sc_cat_id')] = str_repeat('&nbsp;', $level * 2).'&#187; '.$db_new->f('sc_title');
					Categories::getComboMenus($db_new->f('sc_cat_id'), $level+1);
				}
			}
			
			return $output;
		}

		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SETTINGS_CATEGORY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       


		function getDisplayList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='') {
			global $lang;
			
			if ( empty($language) ) {
				$language = $lang;
			}
			
			if ( $required_fields == '' ) {
				$required_fields = 	TABLE_SETTINGS_CATEGORY .".sc_cat_id "
									.",". TABLE_SETTINGS_CATEGORY .".sc_parent_id "
									.",". TABLE_SETTINGS_CATEGORY .".sc_title "
									.",". TABLE_SETTINGS_CATEGORY .".sc_status ";
			}		
			if ( !isset($condition) || $condition == "" ) {
				$condition	= " ORDER BY sc_cat_id ASC ";
			}	
			
			$list	= NULL;
			Categories::getList( $db, $list, $required_fields, $condition, $from, $rpp );
			
			foreach ( $list as $key=>$menu ) {
				$list[$key]['sc_parent_name'] 		= Categories::getCategoryTitle($menu['sc_parent_id']);
			}

			return ( count($list) );
		}

		function getCategoryTitle($id) {
			
			$menu   = NULL;
			$db_new = NULL;
			$db_new 	= new DB_Local;
			Categories::getList($db_new, $menu, 'sc_title', " WHERE sc_cat_id = '$id' ");
		
			return ($menu[0]['sc_title']);	
		}
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Categories::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Categories::getList($db, $user, 'sc_cat_id', " WHERE sc_cat_id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_SETTINGS_CATEGORY
								." SET sc_status = '$status_new' "
								." WHERE sc_cat_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Category was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
		// this function is used to delete records.
		function delete($id, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			$child_cat = Categories::getChildId($id);
			if ( is_array($child_cat) && count($child_cat)>0 ) {
				$messages->setErrorMessage('The Category contains Sub-Category(s).');
			}
			else {

				$query = "DELETE FROM ". TABLE_SETTINGS_CATEGORY
							." WHERE sc_cat_id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage('The Category is deleted .');
				}
				else {
					$messages->setErrorMessage('Problem deleting categoy. Try later.');
				}
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
			
		}
        
		function getChildId($catid)
		{
			global $childid;
			
			$db= new db_local();
			
			$query	= "SELECT * FROM  ".TABLE_SETTINGS_CATEGORY ." 
							WHERE sc_parent_id='$catid'";
	        $db->query( $query );

	        //if($db->nf() ==0) return;
			if ($db->nf() > 0 ){ 
				while( $db->next_record() )
				{
					$childid[] = $db->f("sc_cat_id") ;
					Categories::getChildId( $db->f("sc_cat_id") ) ;
				}
				
			return $childid;
		 	}
		}
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$category = NULL;
			//validate the category title 
            if ( !isset($data['categorytitle']) || empty($data['categorytitle']) ) 
                $messages->setErrorMessage('Category title should be provided.');
			
			//check for same service title name
			$category_title = $data["categorytitle"];
            if ( (Categories::getList($db, $category, 'sc_title', "WHERE sc_title = '$category_title'")) > 0 )
				$messages->setErrorMessage('Category title name already exists. Please change the title.');
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the service title 
            if ( !isset($data['sc_title']) || empty($data['sc_title']) ) 
                $messages->setErrorMessage('Category title should be provided.');

            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
    }
?>