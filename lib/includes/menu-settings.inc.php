<?php
	class MenuSetting {
		
		const 	BLOCKED 	= 0;
		const 	ACTIVE 		= 1;
		const 	PENDING 	= 2;
		//const 	DELETED 	= 3;

		public static 	$placements = array( 
									'title' => array(	'HT' => 'Horizontal Top',
														'HB' => 'Horizontal Bottom',
														'VL' => 'Vertical Left',
														'VR' => 'Vertical Right'
													),
									'value' =>	array(	'HT' => 'HT',
														'HB' => 'HB',
														'VL' => 'VL',
														'VR' => 'VR'
													)
									);
								
		public static 	$type = array( 	'S' => 'myCraft Link',
										'D' => 'Dynamic Link',
										'E' => 'External Link',
                                        'N' => 'Not Linked'
									);
        public static   $access = array('0' => 'All',
                                        '1' => 'Public Only',
                                        '2' => 'Logged In Members Only'
                                     );


		function getStatus() {
			$status = array('BLOCKED' 	=> MenuSetting::BLOCKED,
							'ACTIVE'    => MenuSetting::ACTIVE,
							'PENDING'   => MenuSetting::PENDING,
							//'DELETED'   => MenuSetting::DELETED
						);
//print_r($status);
			return ($status);
		}
		
		function getDisplayStatus() {
			$status = array(MenuSetting::BLOCKED=> 'Blocked',
							MenuSetting::ACTIVE => 'Active',
							MenuSetting::PENDING=> 'Pending',
							//MenuSetting::DELETED=> 'Deleted',
							'BLOCKED'  			=> 'Blocked',
							'ACTIVE'  			=> 'Active',
							'PENDING'  			=> 'Pending',
							//'DELETED'  			=> 'Deleted'
						);
			return ($status);
		}

		function getType() {
			return MenuSetting::$type;
		}
        
        function getAccess() {
            return MenuSetting::$access;
        }

		function getPlacements($type='value') {
			
			if ( $type == '' || $type == 'value' ) {
				return (MenuSetting::$placements['value']);	
			}
			elseif ( $type == 'title' ) {
				return (MenuSetting::$placements['title']);	
			}
			elseif ( $type == 'both' ) {
				return (MenuSetting::$placements);	
			}
			
			return false;
		}
		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_MENU;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
			//echo $query;
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = $db->result();
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				return ( $total );
			}
			else {
				return false;
			}	
		}

		
		/*
		 *  Function to retrieve the list of Menus customised to display the list.
		 */
		function getDisplayList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='') {
			global $lang;
			
			if ( empty($language) ) {
				$language = $lang;
			}
			
			if ( $required_fields == '' ) {
				$required_fields = 	TABLE_MENU .".id "
									.",". TABLE_MENU .".group_id "
									.",". TABLE_MENU .".title "
									.",". TABLE_MENU .".page "
									.",". TABLE_MENU .".menu_order "
									.",". TABLE_MENU .".placement "
									.",". TABLE_MENU .".status "
									.",". TABLE_MENU .".type ";
			}		
			if ( !isset($condition) || $condition == "" ) {
				$condition	= " ORDER BY group_id ASC, menu_order ASC, title ASC ";
			}	
			
			$list	= NULL;
            if ( MenuSetting::getList( $db, $list, $required_fields, $condition, $from, $rpp ) ) {
                
                foreach ( $list as $key=>$menu ) {
                    if ( $list[$key]['type'] == 'S' ) {
                        $list[$key]['page_name'] = getPageTitle($db, $menu['page']);
                    }
                    else {
                        $list[$key]['page_name'] = $menu['page'];
                    }

                    $list[$key]['placement_name'] 	= MenuSetting::$placements['title'][$menu['placement']];
                    $list[$key]['type_name'] 		= MenuSetting::$type[$menu['type']];
                    $list[$key]['group_name'] 		= MenuSetting::getMenuTitle($db, $menu['group_id']);
    			}
            }

			return ( count($list) );
		}

		/*
		 * Function to validate the input from the User while Adding Menus.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 */
		function validateAdd($data, &$messages, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			if ( !isset($data["title"]) || empty($data["title"]) ) {
				$messages->setErrorMessage('Enter the Title of the Menu.');
			}
			if ( !isset($data["placement"]) || !array_key_exists($data['placement'], $menu_placement) ) {
				$messages->setErrorMessage('Select a valid Placement for the Menu.');
			}
			
			$index_arr = array();
			array_key_search('id', $data["group_id"], $menu_existing, $index_arr);
			if ( isset($data["group_id"]) && !empty($data["group_id"]) && count($index_arr)<=0 ) {
				$messages->setErrorMessage('The Parent Menu does not exists.');
			}
			
			if ( !isset($data["type"]) || empty($data["type"]) ) {
				//$messages->setErrorMessage('Select a valid option to Link this menu.');
			}
			else {
				// The Type of the Link has been selected, check if the value is provided or not.
				if ( $data["type"] == "S" ) {
					if ( empty($data["page_s"]) ){
						//$messages->setErrorMessage('Select the myCraft Page to link this Menu.');
					}
					else {
						// Check if the ID of the Static Page exists in the Static Pages or not.
						$query = 'SELECT page_id FROM '. TABLE_SETTINGS_STATICPAGES
									.' WHERE page_id = \''. $data["page_s"] .'\'';
						if ( !$db->query($query) || $db->nf()<1 ) {
							$messages->setErrorMessage('The myCraft Page you have selected does not exists.');
						}
					}
				}
				elseif ( $data["type"] == "D" && empty($data["page_d"]) ) {
					//$messages->setErrorMessage('Enter the name of the Dynamic Page to Link this menu.');
				}
				elseif ( $data["type"] == "E" && empty($data["page_e"]) ) {
					//$messages->setErrorMessage('Enter the URL of the External Page to Link this menu.');
				}
			}
			
			if ( !empty($data["order"]) ) {
				if ( !is_numeric($data["order"]) ) {
					$messages->setErrorMessage('The Order can only be a Numeric value.');
				}
			}

			if ( !isset($data["status"]) || !in_array($data['status'], $menu_status['key']) ) {
				$messages->setErrorMessage('Select a valid Status for the Menu.');
			}

			if ( $messages->getErrorMessageCount() <= 0 ) {
				// Check for duplicate name under the same Parent menu.
				$condition = ' WHERE group_id = \''. $data["group_id"] .'\''
								.' AND placement = \''. $data["placement"] .'\''
								.' AND title = \''. $data["title"] .'\'';
				$duplicate = NULL;
				MenuSetting::getList($db, $duplicate, 'id', $condition);
				if ( count($duplicate)>0 ) {
					$messages->setErrorMessage('A Menu with the same Title is already present under the Parent Menu.');
				}
				$duplicate = NULL;
			}
						
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		
		// this function is used for update validation
		function validateUpdate($data, &$messages, $extra,$m_id='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			if ( !isset($data["title"]) || empty($data["title"]) ) {
				$messages->setErrorMessage('Enter the Title of the Menu.');
			}
			if ( !isset($data["placement"]) || !array_key_exists($data['placement'], $menu_placement) ) {
				$messages->setErrorMessage('Select a valid Placement for the Menu.');
			}
			
			$index_arr = array();
			array_key_search('id', $data["group_id"], $menu_existing, $index_arr);
			if ( isset($data["group_id"]) && !empty($data["group_id"]) && count($index_arr)<=0 ) {
				$messages->setErrorMessage('The Parent Menu does not exists.');
			}
			
			if ( !isset($data["type"]) || empty($data["type"]) ) {
				//$messages->setErrorMessage('Select a valid option to Link this menu.');
			}
			else {
				// The Type of the Link has been selected, check if the value is provided or not.
				if ( $data["type"] == "S" ) {
					if ( empty($data["page_s"]) ){
						//$messages->setErrorMessage('Select the myCraft Page to link this Menu.');
					}
					else {
						// Check if the ID of the Static Page exists in the Static Pages or not.
						$query = 'SELECT page_id FROM '. TABLE_SETTINGS_STATICPAGES
									.' WHERE page_id = \''. $data["page_s"] .'\'';
						if ( !$db->query($query) || $db->nf()<1 ) {
							$messages->setErrorMessage('The myCraft Page you have selected does not exists.');
						}
					}
				}
				elseif ( $data["type"] == "D" && empty($data["page_d"]) ) {
					//$messages->setErrorMessage('Enter the name of the Dynamic Page to Link this menu.');
				}
				elseif ( $data["type"] == "E" && empty($data["page_e"]) ) {
					//$messages->setErrorMessage('Enter the URL of the External Page to Link this menu.');
				}
			}
			
			if ( !empty($data["order"]) ) {
				if ( !is_numeric($data["order"]) ) {
					$messages->setErrorMessage('The Order can only be a Numeric value.');
				}
			}

			if ( !isset($data["status"]) || !in_array($data['status'], $menu_status['key']) ) {
				$messages->setErrorMessage('Select a valid Status for the Menu.');
			}
			
			if ( isset($data["group_id"]) && $data["group_id"] != "0" && $data["group_id"] == $data["m_id"] ){
				$messages->setErrorMessage('The Menu cannot be added below itself.');
			}
			else {
				$child_menu = MenuSetting::getChildId($data["m_id"]);
				if ( is_array($child_menu) && in_array($data["group_id"], $child_menu) ) {
					$messages->setErrorMessage('The Menu cannot be added to its child.');
				}
			}


			if ( $messages->getErrorMessageCount() <= 0 ) {
				// Check for duplicate name under the same Parent menu.
				$condition = ' WHERE group_id = \''. $data["group_id"] .'\''
								.' AND placement = \''. $data["placement"] .'\''
								.' AND title = \''. $data["title"] .'\''
								.' AND id != \''. $data['m_id'] .'\'';
				$duplicate = NULL;
				MenuSetting::getList($db, $duplicate, 'id', $condition);
				if ( count($duplicate)>0 ) {
					$messages->setErrorMessage('A Menu with the same Title is already present under the Parent Menu.');
				}
				$duplicate = NULL;
			}
						
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		// this function is used to delete records.
		function delete($id, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			$child_menu = MenuSetting::getChildId($id);
			if ( is_array($child_menu) && count($child_menu)>0 ) {
				$messages->setErrorMessage('The Menu contains Sub-Menu(s).');
			}
			else {

				$query = "DELETE FROM ". TABLE_MENU
							." WHERE id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage('The Menu is deleted.');
				}
				else {
					$messages->setErrorMessage('The Menu is not deleted.');
				}
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
			
		}
		
		// Used to update status.
		function updateStatus($id, $status_new, $extra) {
				foreach ($extra as $key=>$value) {
					$$key = $value;
				}
				// Check if status is valid or not.
				if ( !in_array($status_new, MenuSetting::getStatus()) ) {
					$messages->setErrorMessage('The specified Status is not valid.');
				}
				else {
					$query = "UPDATE ". TABLE_MENU
									." SET status = '$status_new' "
								." WHERE id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) {
						$messages->setOkMessage('Menu Status is updated.');
					}
					else {
						$messages->setErrorMessage('Menu Status is not updated.');
					}
				}
				
				if ( $messages->getErrorMessageCount() <= 0 ) {
					return (true);
				}
				else {
					return (false);
				}
				
			}

        // this function is used to clear page attached to menu.
        function clear($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            $query = " UPDATE ". TABLE_MENU
                    ." SET page = '' "
                    .","." type = '' "
                    ." WHERE id = '$id'";
           if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage('The Page attached to menu is Cleared.');
            }
            else {
                $messages->setErrorMessage('The Page attached to menu is not Cleared.');
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
			
		// This function creates the menu combobox 
		function getComboMenus($menuid=0,$level = 0) {
			global $output;
			global $lang;
			
			$db_new 	= new DB_Local;
			$query 		=" SELECT * FROM  "
								.TABLE_MENU
						." WHERE " .TABLE_MENU.".group_id='$menuid'"
						." AND ".TABLE_MENU.".status='1' "
						." ORDER BY date";
			
			$db_new->query($query);
	
			if($db_new->nf() ==0) return;
			if ($db_new->nf() > 0 ) {
				while($db_new->next_record()) {
					$output[$db_new->f('id')] = str_repeat('&nbsp;', $level * 4).'&#187; '.MenuSetting::getMenuTitle($db_new->f('id'))." (".$db_new->f('placement').")";
					MenuSetting::getComboMenus($db_new->f('id'), $level+1);
				}
			}
			return $output;
		}
		
		
		function getMenuTitle(&$db, $id) {
			$menu = NULL;
			MenuSetting::getList($db, $menu, 'title', " WHERE id = '$id' ");
		
			return ($menu[0]['title']);	
		}


//		function getPageName($id) {
//			$db		= new DB_Local;
//			if ( !isset($condition) || $condition == "" ) {
//						$condition	= " WHERE ". TABLE_SETTINGS_STATICPAGES .".id = '$id' ";
//			}	
//			$list = NULL;
//			$query = "SELECT name FROM ". TABLE_SETTINGS_STATICPAGES;
//			$query .= " ". $condition;
//	
//			if ( $db->query($query) ) {
//				if ( $db->nf() > 0 ) {
//					while ($db->next_record()) {
//						$list	= processSqlData( $db->f('name') );
//					}
//				}
//				else{
//					$list = '-';
//				}
//				return ( $list );
//			}
//			else {
//				return false;
//			}	
//		}


		// Get all the child Id for a menu
		function getChildId($menuid)
		{
			global $childid;
			
			$db_new_id 		= new DB_Local;
			
			$query	= "SELECT * FROM  ". TABLE_MENU." 
							WHERE 
								group_id='$menuid'";
			$db_new_id->query($query);
	
			//if($db->nf() ==0) return;
			
			while( $db_new_id->next_record() )
			{
				$childid[] = $db_new_id->f("id") ;
				MenuSetting::getChildId( $db_new_id->f("id") ) ;
			}
			return $childid;
		}
	
	
// 		function getAllMenus(&$db, &$menu_list, $placement='', $menu_id=0) {
// 			$temp = NULL;
// 			
// 			if ( !empty($placement) ) {
// 				$condition = "WHERE placement = '$placement'"
// 								." AND group_id ='$menu_id'"
// 								." ORDER BY menu_order, title";
// 
// 				MenuSetting::getList($db, $temp, '*', $condition);
// 				if ( is_array($temp) && count($temp)>0 ) {
// 					foreach ($temp as $key=>$menu) {
// 						MenuSetting::getAllMenus($db, $temp[$key]['sub_menu'], $placement, $menu['id']);
// 						$menu_list = $temp;
// 					}
// 				}
// 			}
// 			else {
// 				// The Placement is not defined, read the placements.
// 				$placements_arr = MenuSetting::getPlacements();
// 				
// 				// Fetch all the Menus for these fetched Placements.
// 				foreach ( $placements_arr as $key=>$placements ) {
// 					$temp 		= NULL;
// 					$condition 	= "WHERE placement = '$placements'"
// 									." AND group_id ='$menu_id'"
// 									." ORDER BY menu_order, title";
// 
// 					MenuSetting::getList($db, $temp, '*', $condition);
// 					
// 					if ( is_array($temp) && count($temp)>0 ) {
// 						$menu_list[$placements] = $temp;
// 						
// 						foreach ($menu_list[$placements] as $key=>$menu) {
// 							// Check for the Sub-Menus.
// 							MenuSetting::getAllMenus($db, $menu_list[$placements][$key]['sub_menu'], $placements, $menu['id']);
// 						}
// 					}
// 				}
// 			}
// 			
// 			if ( is_array($menu_list) && count($menu_list)>0 ) {
// 				return true;
// 			}
// 			else {
// 				return false;
// 			}
// 
// 		}

		
		
		function getMenus(&$db, &$menu_list, $placement='', $group_id=0, $level=0, $is_logged='', $site_id='') {
			$temp = NULL;
            
			if ( !empty($placement) ) {
				$condition = "WHERE placement = '$placement'"
								." AND status = '1'"
								." AND group_id ='$group_id'";
                if ( $is_logged === true ) {
                    $condition .= " AND (access = '0' OR access = '2' )";
                }
                elseif ( $is_logged === false ) {
                    $condition .= " AND (access = '0' OR access = '1' )";
                }
                $condition .= " ORDER BY menu_order, title";
				
				MenuSetting::getList($db, $menu_list, '*', $condition);

				if ( is_array($menu_list) && count($menu_list)>0 ) {
					foreach ($menu_list as $key=>$menu) {
						$temp = NULL;
						if ( $menu['group_id'] == 0 ) {	
							$level = 0 ;
						}
						MenuSetting::getMenus($db, $temp, $placement, $menu['id'], ($level+1), $is_logged, $site_id);

						if ( $menu_list[$key]['type'] == 'S' ) {
							//$menu_list[$key]['url'] = sprintf(URL_S, getPageName($db, $menu_list[$key]['page']));
							$menu_list[$key]['url'] = sprintf(URL_S, 
																str_replace(' ', '-', $menu_list[$key]['title']),
																getPageName($db, $menu_list[$key]['page'])
															);
							// attaching the html extension
							$menu_list[$key]['url']     .= ".html";
                            $menu_list[$key]['target']  = "_self";
						}
						elseif ( $menu_list[$key]['type'] == 'D' ){
                            $menu_list[$key]['url']     = sprintf(URL_D, $menu_list[$key]['page']);
                            $menu_list[$key]['target']  = "_self";
						}
						elseif ( $menu_list[$key]['type'] == 'E' ){
                            $menu_list[$key]['url']     = sprintf(URL_E, $menu_list[$key]['page'] );
                            $menu_list[$key]['target']  = "_blank";
						}
						else {
                            $menu_list[$key]['url']     = '#';
                            $menu_list[$key]['target']  = "_self";
						}
						$menu_list[$key]['level'] = $level;
						$menu_list[$key]['sub_menu'] = $temp;
					}
				}
			}
			else {
				// The Placement is not defined, read the placements.
				$placements_arr = MenuSetting::getPlacements();
				
				// Fetch all the Menus for these fetched Placements.
				foreach ( $placements_arr as $key=>$placements ) {
					$temp 		= NULL;
					$condition 	= "WHERE placement = '$placements'"
									." AND status = '1'"
									." AND group_id ='$group_id'"
									." ORDER BY menu_order ASC, title ASC";

					MenuSetting::getList($db, $temp, '*', $condition);
					
					if ( is_array($temp) && count($temp)>0 ) {
						$menu_list[$placements] = $temp;
						
						foreach ($menu_list[$placements] as $key=>$menu) {
							// Check for the Sub-Menus.
							if ( $menu['group_id'] == 0 ) {	
								$level = 0 ;
							}
							$menu_list[$placements][$key]['level'] = $level;
							MenuSetting::getMenus($db, $menu_list[$placements][$key]['sub_menu'], $placements, $menu['id'], ($level+1), $is_logged);
						}
					}
				}
			}
			
			if ( is_array($menu_list) && count($menu_list)>0 ) {
				return true;
			}
			else {
				return false;
			}
		}
	}

?>