<?php
	class Invoice {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
        const DELETED = 3;
		const COMPLETED = 4;

		
		const PENDING_INV_R   = 0;
		const COMPLETED_INV_R = 1;
		const CANCELLED_INV_R = 2;
		
		function getStatus() {
			$status = array('BLOCKED' => Invoice::BLOCKED,
							//'ACTIVE'    => Invoice::ACTIVE,
							'PENDING'   => Invoice::PENDING,
                            'DELETED'   => Invoice::DELETED,
                           'COMPLETED' => Invoice::COMPLETED
						);
			return ($status);
		}
		function getReminderStatus() {
			$status = array('PENDING' => Invoice::PENDING_INV_R,							 
							'COMPLETED'   => Invoice::COMPLETED_INV_R,
                            'CANCELLED'   => Invoice::CANCELLED_INV_R
						);
			return ($status);
		}
        /*
         array of days before which reminder of expiry has been sent
         ie 60 days before expiry,
            30 days before expiry
        */        
        function expirySetDays() {
			/* $arr = array(120=>120,
                         90=>90
						); */
			$arr = array(90=>90,
						 60=>60
						);
			return ($arr);
		}
		
        function getCompany( &$db, &$company){
			$query="SELECT ".TABLE_SETTINGS_COMPANY.".* FROM ".TABLE_SETTINGS_COMPANY ;
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$company[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        function getStatusTitle() {
            $status = array(Invoice::BLOCKED    => 'Bad Debt',
                            Invoice::ACTIVE     => 'Paid Fully',
                            Invoice::PENDING    => 'Payment Pending',
                            Invoice::DELETED    => 'Deleted',
                           // Invoice::COMPLETED  => 'Renewed',
                            Invoice::COMPLETED  => 'Completed',
                            'CANCELLED'         => 'Bad Debt',
                            'ACTIVE'            => 'Active',
                           //'ACTIVE'            => 'Paid Fully',
                            'PENDING'           => 'Payment Pending',
                            'DELETED'           => 'Deleted',
                            //'PROCESSED'         => 'Renewed',
                            //'PROCESSED'         => 'Completed',
                           );
            return ($status);
        }
        
		/**
		 *	Function to get all the Invoices.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			//$query .= " FROM ". TABLE_BILL_INV;
			$query .= " FROM ". TABLE_BILL_INV ;
            $query .= " LEFT JOIN ". TABLE_BILL_ORDERS
                        ." ON ". TABLE_BILL_ORDERS .".number = ". TABLE_BILL_INV .".or_no ";
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_INV .".created_by ";
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_INV .".client ";            
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		 
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ; 
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
             include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
			$query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                //$query .= " COUNT(*) as count";
				$query .= " COUNT(DISTINCT(".TABLE_BILL_INV.".id)) as count";
				$count = 1;
            }
            /* comment 2009-03-mar-21 BOF
            $query .= " FROM ". TABLE_BILL_INV;
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_INV .".client ";
            comment 2009-03-mar-21 EOF
            */
            
           /*  $query .= " FROM ". TABLE_BILL_INV ;
            $query .= " LEFT JOIN ". TABLE_BILL_ORDERS
                        ." ON ". TABLE_BILL_ORDERS .".number = ". TABLE_BILL_INV .".or_no ";
            //$query .= " LEFT JOIN ". TABLE_BILL_ORD_P
                 //       ." ON ". TABLE_BILL_ORD_P .".ord_no = ". TABLE_BILL_INV .".or_no ";
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_INV .".created_by ";
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_INV .".client "; */
             
			
			$query .= " FROM ". TABLE_BILL_INV ;
            $query .= " LEFT JOIN ". TABLE_BILL_ORDERS
                        ." ON ". TABLE_BILL_ORDERS .".number = ". TABLE_BILL_INV .".or_no ";
			$query .= " LEFT JOIN ". TABLE_BILL_INV_PROFORMA
                        ." ON ( ".TABLE_BILL_INV .".inv_profm_no = ". TABLE_BILL_INV_PROFORMA .".number AND
						".TABLE_BILL_INV_PROFORMA.".status = '".InvoiceProforma::COMPLETED."' ) "; 
            $query .= " LEFT JOIN ". TABLE_BILL_ORD_P
                        ." ON ". TABLE_BILL_ORD_P .".ord_no = ". TABLE_BILL_INV .".or_no ";  
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_INV .".created_by ";
            $query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_INV .".client ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
		 
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;
							
						}else{
							$total =$db->nf();
						}
                    }
                }
               
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( in_array($status_new, Invoice::getStatus()) ) {
                $order = NULL;
                if ( (Invoice::getList($db, $invoice, TABLE_BILL_INV.'.id,'.TABLE_BILL_INV.'.access_level,'.TABLE_BILL_INV.'.created_by,'.TABLE_BILL_INV.'.status', " WHERE ".TABLE_BILL_INV.".id = '$id'")) > 0 ) {
                    $invoice = $invoice[0];
					
					// Check Access Level for the invoice.
					if ( ($invoice['created_by'] == $my['user_id']) && ($invoice['access_level'] >= $access_level) ) {
						$messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
					}
					elseif ( ($invoice['created_by'] != $my['user_id']) && ($invoice['access_level'] >= $access_level_ot) ) {
						$messages->setErrorMessage("Cannot change Status of Invoice created by others, with current Access Level.");
					}
					else {
                    	if ( $invoice['status'] != $status_new ) {
                            $query = "UPDATE ". TABLE_BILL_INV
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("Status not updated. New and Old status is same.");
                        }
                    }
                }
                else {
                    $messages->setErrorMessage("The Invoice was not found.");
                }
            }
            else {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $order = NULL;
            if ( (Invoice::getList($db, $order, TABLE_BILL_INV.'.id,'.TABLE_BILL_INV.'.or_no,'.TABLE_BILL_INV.'.number, '.TABLE_BILL_INV.'.access_level,'.TABLE_BILL_INV.'.status', " WHERE ".TABLE_BILL_INV.".id = '$id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != Invoice::COMPLETED ) {
                    if ( $order['status'] == Invoice::BLOCKED ) {
                        if ( $order['access_level'] < $access_level ) {
                             /* check receipt created or not if receipt created then dont delete it 
                             ie invoice will be deleted only after deleting the receipts
                            */
                            
                            if((Receipt::getList($db, $receipt, 'id', " WHERE inv_no = '".$order['number']."' AND status !='".Receipt::DELETED."'") ) > 0){
                             
                                $messages->setErrorMessage("Please delete  Receipts of Invoice ".$order['number']." ");
                             
                            }else{                                
                                
                                $query = " UPDATE ". TABLE_BILL_INV
                                            ." SET status ='".Invoice::DELETED."' WHERE id = '$id'";
                                 /*
                                 $query = "DELETE FROM ". TABLE_BILL_INV
                                            ." WHERE id = '$id'";
                                 */
                                 
                                if ( $db->query($query) && $db->affected_rows()>0 ) {
									//Delete invoice from reminder cron table
									$query = "DELETE FROM ". TABLE_INV_REMINDER
                                            ." WHERE inv_id = '$id'";
									$db->query($query);		
									$query = "DELETE FROM ". TABLE_INV_REMINDER_CRON
                                            ." WHERE inv_id = '$id'";
									$db->query($query);
                                    //code to delete the file bof
                                    //$file_name = DIR_FS_INV_FILES ."/". $order["number"] .".html";
                                    //unlink( $file_name );
                                    //code to delete the file eof 
                                
                                    $messages->setOkMessage("The Invoice has been deleted.");
                                    
                                    //Get order details by using order_no
                                    // Order::getList($db, $orderDetails, 'id, number, po_id ', " WHERE number = '".$order['or_no']."'");
                                    /*
                                    $query = "UPDATE ". TABLE_BILL_ORDERS
                                                ." SET status = '". Order::ACTIVE ."'"
                                                ." WHERE number = '". $order['or_no'] ."'";
                                    $db->query($query); 
                                    */
                                    
                                }
                                else {
                                    $messages->setErrorMessage("The Invoice was not deleted.");
                                }
                               
                            }
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Invoice with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("To Delete Invoice it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Invoice cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Invoice was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        /**
		 * Function to validate the input from the User while sending the invoice via email.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
	    function validateSendInvoice(&$data, $extra='') {
             foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( !isset($data['to']) || empty($data['to']) ) {
                $messages->setErrorMessage('To should be provided.');
            }
            elseif ( !isValidEmail($data['to']) ) {
                $messages->setErrorMessage('To Email Address is not valid.');
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Validate the Order.
            $list = NULL;
            if ( Invoice::getList($db, $list, TABLE_BILL_INV.'.id', " WHERE ".TABLE_BILL_INV.".or_no = '". $data['or_no'] ."' AND ".TABLE_BILL_INV.".status !='".Invoice::DELETED."'")>0 ) {
                $messages->setErrorMessage("The Order has been processed already.");
            }
            else {
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_BILL_ORDERS
                            ." WHERE number = '". $data['or_no'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage("The Order was not found in the database.");
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            if ( ($db->f('status') != ACTIVE) && ($db->f('status') != PROCESSED) && ($db->f('status') != COMPLETED) ) {
                                $messages->setErrorMessage("The Order is not yet approved.");
                            }
                            $data['client'] = $db->f('client');
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Invoice for this Order.");
                        }
                    }
                }
            }
			 if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            /*
			if(empty($data['financialYr'])){
                 $messages->setErrorMessage("Select Financial Yr.");
            }
			if(!empty($data['financialYr']) && !empty($data['company_id']) ){
            
                $_ALL_POST['number'] =$data['number']= getOldCounterNumber($db,'INV',$data['company_id'],$data['financialYr']); 
                //$_ALL_POST['number'] =$data['number']= getCounterNumber($db,'ORD',$data['company_id']); 
                if(empty($data['number']) ){
                    $messages->setErrorMessage("Invoice Number was not generated.");
                }
            }else{
                $messages->setErrorMessage(" Financial Year and Company Name are not Found.");
            }*/
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creator's status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }

            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Client for whom the Invoice is being Created.");
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email,org,billing_name, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            
            // Check the billing name. 
           if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
            // Format the Date of Invoice.
            if ( !isset($data['do_i']) || empty($data['do_i']) ) {
                $messages->setErrorMessage("The Date of Invoice is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_i']) ) {
                $messages->setErrorMessage("Format of the Date of Invoice is invalid.");
            }
            else {
                $data['do_i'] = explode('/', $data['do_i']);
				
                //generate invoice number on the basis of invoice date 
                // $data['number'] = "PT". $data['do_i'][2]."-INV-".$data['do_i'][1]. $data['do_i'][0].date("hi-s") ;
                $data['do_i'] = mktime(0, 0, 0, $data['do_i'][1], $data['do_i'][0], $data['do_i'][2]);
            }
            $data['number']=$data['inv_counter'] ='';
			if(!empty($data['company_id']) && !empty($data['do_i'])){				
				$detailInvNo = getInvoiceNumber($data['do_i'],$data['company_id']); 				
				if(!empty($detailInvNo)){
					$data['number'] = $detailInvNo['number'];
					$data['inv_counter'] = $detailInvNo['inv_counter'];
				}
			}
			
             // Format the Date of expiry from date, if provided.
            if ( !empty($data['do_fe']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_fe']) ) {
                    $messages->setErrorMessage("Format of the Service From Date is invalid.");
                }
                else {
                    $data['do_fe'] = explode('/', $data['do_fe']);
                    $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                }
                
            }else{
                $data['do_fe'] = '';
            }      
             
             // Format the Date of Invoice Expiry, if provided.
            if ( !empty($data['do_e']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_e']) ) {
                    $messages->setErrorMessage("Format of the Date Renewal is invalid.");
                }
                else {
                    $data['do_e'] = explode('/', $data['do_e']);
                    $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                }                
            }else{
                 $data['do_e'] = '';
        
            }
            // Format the Invoice Payment Due Date.
            if ( !isset($data['do_d']) || empty($data['do_d']) ) {
                $messages->setErrorMessage("The Due date for Payment of Invoice is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_d']) ) {
                $messages->setErrorMessage("Format of the Due date for Payment of Invoice is invalid.");
            }
            else {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
            
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Invoice Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
           
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Invoice Amount in INR is not valid.");
                }
            }
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{  
               
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}
			 
			if( empty($data['number']) ){
                $messages->setErrorMessage("An Invoice Number was not generated.");
            } 
			if( empty($data['inv_counter']) ){
                $messages->setErrorMessage("An Invoice Counter was not generated.");
            } 
            
            // Check for the duplicate Invoice Number.
            $list = NULL;
            if ( Invoice::getList($db, $list, TABLE_BILL_INV.'.id', " WHERE ".TABLE_BILL_INV.".number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Invoice with the same Number already exists.<br/>Please try again.");
            }
            $list = NULL;
            
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // Validate the Order.
            
            $list = NULL;
            if ( Invoice::getList($db, $list, TABLE_BILL_INV.'.id,'.TABLE_BILL_INV.'.created_by', " WHERE ".TABLE_BILL_INV.".or_no = '". $data['or_no'] ."' AND ".TABLE_BILL_INV.".status !='".Invoice::DELETED."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_BILL_ORDERS
                                ." WHERE number = '". $data['or_no'] ."' AND status !='".Order::DELETED."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage('The Order was not found in the database.');
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            /* As complete ma
                            if ( $db->f('status') != COMPLETED ) {
                                $messages->setErrorMessage('The Order has not been marked as completed.');
                            }*/
                            $data['client'] = $db->f('client');
                        }
                        else {
                            $messages->setErrorMessage('You donot have the Right to update Invoice for this Order.');
                        }
                    }
                }
            }
            else {
                $messages->setErrorMessage('The Invoice for the Order has not been created yet.');
            }
            
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage('Creator details cannot be retrieved, Please try again.');
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage('The Creator was not found.');
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage('The Creators\' status is not Active.');
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }

            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage('Select the Client for whom the Invoice is being Created.');
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, billing_name,status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            
            // Check the billing name.
           
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
            // Format the Date of Invoice.
            if ( !isset($data['do_i']) || empty($data['do_i']) ) {
                $messages->setErrorMessage("The Date of Invoice is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_i']) ) {
                $messages->setErrorMessage("Format of the Date of Invoice is invalid.");
            }
            else {
                $data['do_i'] = explode('/', $data['do_i']);
                $data['do_i'] = mktime(0, 0, 0, $data['do_i'][1], $data['do_i'][0], $data['do_i'][2]);
            }
            
           
                                     
            // Format the Date of expiry from date, if provided.
            if ( !empty($data['do_fe']) ) {
            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_fe']) ) {
                    $messages->setErrorMessage("Format of the Service From Date is invalid.");
                }
                else {
                    $data['do_fe'] = explode('/', $data['do_fe']);
                    $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                }
                
           }else{
                $data['do_fe'] = '';
           }
                    
             // Format the Date of Invoice Expiry, if provided.
            if ( !empty($data['do_e']) ) {
            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_e']) ) {
                    $messages->setErrorMessage("Format of the Date Renewal is invalid.");
                }
                else {
                    $data['do_e'] = explode('/', $data['do_e']);
                    $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                }
                
            }else{
                 $data['do_e'] = '';
        
            }
          
            
               
           
            // Format the Invoice Payment Due Date.
            if ( !isset($data['do_d']) || empty($data['do_d']) ) {
                $messages->setErrorMessage("The Due date for Payment of Invoice is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_d']) ) {
                $messages->setErrorMessage("Format of the Due date for Payment of Invoice is invalid.");
            }
            else {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
            // Validate Services.
            /*
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }*/
            /*
            $data['query_p'] = 'INSERT INTO '. TABLE_BILL_INV_P .' (inv_no, particulars, p_amount) VALUES ';
            $is_part = false;
            foreach ( $data['particulars'] as $key=>$particular ) {
                if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                    if ( empty($data['particulars'][$key]) ) {
                        $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( empty($data['p_amount'][$key]) ) {
                        $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                    }
                    else {
                        if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                        }
                        else {
                            $is_part = true;
                            $data['query_p'] .= "('". $data['number'] ."', '". $data['particulars'][$key] ."', '". $data['p_amount'][$key] ."'),";
                        }
                    }
                }
            }
            if ( !$is_part ) {
                $messages->setErrorMessage('The Particular for the Invoice cannot be empty.');
            }
            else {
                $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
            }
            */
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Invoice Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Invoice Amount in INR is not valid.");
                }
            }
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{            
                
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
            
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
           
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                $messages->setErrorMessage("The Status is not valid.");
            }
            

            
            // Check for the duplicate Invoice Number.
            /*
            $list = NULL;
            if ( Invoice::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Invoice with the same Number already exists.");
            }
            $list = NULL;
            */
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for 
         * each Invoice.
         *
         */
        function setOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_BILL_ORDERS
                        ." SET status = '$status'"
                        ." WHERE ( id = '$id' OR number = '$id' )";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for
         * each Invoice so that the service provided is updated in the
         * Clients Account.
         *
         */
        function UpdateClientService(&$db, $client_id, $services) {
            $return     = false;
            $c_service  = 'NULL';
            
            $query = "SELECT service_id FROM ". TABLE_CLIENTS
                        ." WHERE user_id = '". $client_id ."'";
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
                $c_service = processSQLData($db->f('service_id'));
                if ( empty($c_service) ) {
                    $c_service = array();
                }
                else {
                    $c_service = explode(',', $c_service);
                }
                
                $c_service = arrayCombine($c_service, $services);
                $c_service = implode(',', $c_service);
                $query = "UPDATE ". TABLE_CLIENTS
                            ." SET service_id = '$c_service'"
                            ." WHERE user_id = '$client_id'";
                if ( $db->query($query) ) {
                    $return = true;
                }
            }
            return ($return);
        }
        
        /**
         * This function is used to create the File in PDF or HTML format for the Invoice.
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createFile($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("bill-invoice-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_INV_FILES_HTML, 0777);
                if ( is_writable(DIR_FS_INV_HTML_FILES) ) {
                    $file_name = DIR_FS_INV_HTML_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Invoice was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Invoice is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
                   /*
                   $pdf_content = Invoice::createPdfContent($data);
                   $invoiceArr['content'] = $pdf_content ;
                   $invoiceArr['header'] = '' ;
                   $invoiceArr['footer'] = '' ;
                   createInvoicePDF($data["number"],$invoiceArr);
                   $file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";*/
            }elseif($type == 'HTMLPRINT'){
                $s->assign('data', $data);
                $file_data = $s->fetch("bill-invoice-tpl-print.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_INV_FILES_HTMLPRINT, 0777);
                if ( is_writable(DIR_FS_INV_HTMLPRINT_FILES) ) {
                    $file_name = DIR_FS_INV_HTMLPRINT_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Invoice was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Invoice is not writable.");
                }
                umask($old_umask);
            
            
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
        /** 
         *  This function is used While migrating the old data 2009-04-apr-08
         * This function is used to create the File in PDF or HTML format for the Invoice. 
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createFileOld($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("bill-invoice-old-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_INV_FILES, 0777);
                if ( is_writable(DIR_FS_INV_FILES) ) {
                    $file_name = DIR_FS_INV_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Invoice was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Invoice is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
            
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
        
        /**
         * This function is used to create the File in PDF or HTML format for the Invoice. After editing the 
         * old orders in new formate of particulars. This function is used in script-generate-invoice-tpl.php
         
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createMigrateInvoice($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("bill-migrate-invoice-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_INV_FILES, 0777);
                if ( is_writable(DIR_FS_INV_FILES) ) {
                    $file_name = DIR_FS_INV_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Invoice was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Invoice is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
            
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
        
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }
            
            $query .= " FROM ". TABLE_BILL_ORD_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
                }                
                return ( $db->nf() );
            }
            else {
                return false;
            }
    }
function createPdfFile($data) {        
$variables['images'] = DIR_WS_IMAGES_NC ;
$body=  "<style type=\"text/css\" rel=\"stylesheet\">
    body {margin-right:auto;
            margin-left:auto;
        }
        #content {
        margin:0;
        width: 100%;
        text-align:center;
        }
        #content-main {
        width: 731px;
        text-align:center;
        margin-left:auto;
        margin-right:auto;
        }
        .heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:18px;
            color:#000000;
            text-decoration:none;
            font-weight:bold;
        }
        .sub-heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#686868;
            line-height:15px;
            text-decoration:none;
            font-weight:regular;
        }
        .invoice-border{ 
             background:url(".$variables['images']."/invoice-repeater-new.jpg);
             background-repeat:repeat-y;
             width:253px;
         }	
        .invoice-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:12px;
            color:#000000;
            text-decoration:none;
            font-weight:bold;
        }
        
        .content-heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:12px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:bold;
        }
        .address-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:10px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
		.address-text-small{
			font-family:Arial, Verdana, 'San serif';
            font-size:9px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
		
		}
        .green-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#176617;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .white-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
		.white-text-12{
			font-family:Arial, Verdana, 'San serif';
            font-size:12px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
		}
        .design-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#797979;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .custom-content{
            font-family:arial,verdana,sans-serif;
            font-size:11px;
            color:#646464;
        }
        .content{
            font-family:arial,verdana,sans-serif;
            font-size:11px;
            color:#4f4f4f;
            lignt-height:10px;
        }
		.contentsub{
            font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
            lignt-height:10px;
			font-weight:bold;
        }
		.contenthead{
            font-family:arial,verdana,sans-serif;
            font-size:13px;
            color:#4f4f4f;
            lignt-height:11px;
        }	
        .content-small{
            font-family:arial,verdana,sans-serif;
            font-size:8px;
            color:#4f4f4f;
        }
		.content-small-term{
            font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#726e6e;
        }
		.content-small-7{
        	font-family:arial,verdana,sans-serif;
            font-size:7px;
            color:#4f4f4f;
	    }
		.content-small-8{
        	font-family:arial,verdana,sans-serif;
            font-size:8px;
            color:#4f4f4f;
	    }
		.content-small-10{
        	font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
	    }
        .spacer{background:url(".$variables['images']."/spacer.gif)repeat;}
        .bg1{background-color:#898989;}
        .bg2{background-color:#CCCCCC;}
        .bg3{background-color:#f4f4f4;}
        .bg4{background-color:#ba1a1c;}
        .bg5{background-color:#956565;}
        .bg6{background-color:#BEBE74;}
        .bg7{background-color:#48b548;}
        .bg8{background-color:#000000;}
        .bg9{background-color:#bf1f21;}
        .bdb1{border-bottom:1px solid #000000;}
        .b {font-weight:bold;}
        .ac {text-align:center;}
        .al {text-align:left;}
        .ar {text-align:right;}
        .vt {vertical-align:top;}
        .vm {vertical-align:middle;}
        .vb {vertical-align:bottom;}
        .fs08 {font-size:0.8em;}
        .fs11 {font-size:1.1em;}
        .fst11{font-size:1.1em; font-family:'Arial Narrow',Verdana,Arial,Sans-Serif;}
        .fs15 {font-size:1.5em;}
        .lh15 {line-height:1.5em;}
        .lh10 {line-height:1em;}                                    
        .pl2{padding-left:2px;}
        .pl5{padding-left:5px;}
        .pl8{padding-left:8px;}                                    
        .pl10{padding-left:10px;}
        .pl11{padding-left:11px;}
        .pl15{padding-left:15px;}
        .pl20{padding-left:20px;}
        .pl60{padding-left:60px;}
        .pl42{padding-left:42px;}                                    
        .pr2{padding-right:2px;}
        .pr5{padding-right:5px;}
        .pr10{padding-right:10px;}                                    
        .pb2{padding-bottom:2px;}
        .pb3{padding-bottom:3px;}
        .pb4{padding-bottom:4px;}
        .pb5{padding-bottom:5px;}        
        .pb10{padding-bottom:10px;}
        .pb13{padding-bottom:13px;}  
        .pt1{padding-top:1px;}        
        .pt2{padding-top:2px;}
        .pt3{padding-top:3px;}
        .pt4{padding-top:4px;}
        .pt5{padding-top:5px;}
        .pt6{padding-top:6px;}
        .pt8{padding-top:8px;}
        .pt10{padding-top:10px;}
        .pt15{padding-top:15px;}
		.pt30{padding-top:30px;}       
        .pt32{padding-top:32px;}
        .pt45{padding-top:45px;}                                    
        .b1{border:collapse;border:1px solid #000000;}
        .wp100{width:100%;}
        .wp90{width:90%;}
        .wp70{width:70%;}
        .wp65{width:65%;}
        .wp60{width:60%;}
        .wp55{width:55%;}
        .wp50{width:50%;}
        .wp45{width:45%;}
        .wp40{width:40%;}
        .wp35{width:35%;}
        .wp30{width:30%;}
        .wp28{width:28%;}
        .wp25{width:25%;}
        .wp20{width:20%;}
        .wp15{width:15%;}
        .wp13{width:13%;}
        .wp12{width:12%;}
        .wp10{width:10%;}
        .wp9{width:9%;}
        .wp5{width:5%;}
        .wp6{width:6%;}
        .wp4{width:4%;}                                    
        .wpx7{width:7px;}
        .wpx9{width:9px;}
        .wpx10{width:10px;}
        .wpx20{width:20px;}
        .wpx712{width:712px;}
        .wpx729{width:729px;}
        .wpx731{width:731px;}
        .wpx330{width:330px;}
        .wpx337{width:337px;}
        .wpx348{width:348px;}       
        .wpx711{width:711px;}
        .wpx707{width:707px;}
        .wpx9{width:9px;}
        .wpx713{width:713px;}
        .wpx347{width:347px;}
        .wpx253{width:253px;}
        .wpx335{width:335px;}                                    
        .hp30{
            height:27px;
            border-top:1px solid #dddddd;
        }
        .hp22{height:22px;}
        .hp20{height:20px;}
        .hp4{height:4px;}
        .hp9{height:9px;}
        .hp8{height:8px;}
        .hp17{height:19px;}
        .borlr{
            border-left:1px solid #dddddd;
            border-right:1px solid #dddddd;
        }                                    
        .borl{
            border-left:1px solid #dddddd;
        }                                    
        .borr{
            border-right:1px solid #dddddd;
        }
        .borb{
            border-bottom:1px solid #dddddd;
        }                                    
        div.row {
            clear:both;
            float:left;
        }
        div.coloumn {
            float:left;
        }
        div.coloumn-right {
            float:right;
        }                                    
        .seccolor{background-color:#999999;}
        .bg2{background-color:#f4f4f4;}                                    
        div.clear{
            clear:both;
            background-color:#f4f4f4;
        }
        .header-text{font-size:11px; font-family:arial; font-weight:normal;}
</style>        
<div id=\"content\">
<div id=\"content-main\">
<div class=\"row wp100 al\" style=\"height:1100px\">
	<div class=\"row al pt45\">
		<div><img src=\"".$variables['images']."/smeerp-inv-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\"></div>
	</div>
	<div class=\"row wpx731 al pb10 pt1\">
		<div class=\"coloumn al content wp65\">
			Motivated to Serve&nbsp;Determined to Excel <sup>TM</sup>
		</div>
		<div class=\"coloumn-right invoice-text ar\">";			
			if( $data['do_i_chk'] >=$data['do_iso'] && !empty($data['do_iso']) ){
				$body.=" An ISO 9001:2008 CERTIFIED COMPANY ";
			}  
$body.="</div>
	</div>
	<div class=\"row wpx731 pb5 pt30\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
		<tr>
			<td width=\"478px\" class=\"pl42 al\" valign=\"top\">";
	if (!empty($data['old_billing_address'])){
	//An ISO 9001:2008 CERTIFIED COMPANY
	$body.="<div class=\"heading pb2\">".nl2br($data['billing_name'])."</div>
			 <div class=\"sub-heading\">".nl2br($data['old_billing_address'])."</div>";
	}else{
	$body.="<div class=\"heading pb2 al\">".nl2br($data['billing_name'])."</div>
	<div class=\"sub-heading al\">".nl2br($data['b_addr'])."</div>      
	<div class=\"sub-heading al\">";
		if (!empty($data['b_addr_city']))
		{
			$body.=$data['b_addr_city'];
		}
		if (!empty($data['b_addr_zip']))
		{
			$body.="&nbsp;". $data['b_addr_zip'];
		}
		if (!empty($data['b_addr_state']))
		{
			$body.="&nbsp;". $data['b_addr_state'];
		}
		if (!empty($data['b_addr_country']))
		{
			$body.="<br/>". $data['b_addr_country'];
		}
	$body.="</div> "  ;
		if(!empty($data['client_st_no']) || !empty($data['client_pan_no']) ){
			$body.="<div class=\"sub-heading al\">"; 
			if(!empty($data['client_st_no'])){
				$body.="STax: ".$data['client_st_no']."&nbsp;";
			}
			if(!empty($data['client_pan_no'])){
				$body.="PAN: ".$data['client_pan_no']; 
			}
			$body.="</div> "  ;
		}
		if(!empty($data['client_vat_no']) || !empty($data['client_cst_no']) ){	
			$body.="<div class=\"sub-heading al\">"; 
			if(!empty($data['client_vat_no'])){
				$body.="VAT: ".$data['client_vat_no']."&nbsp;";
			}
			if(!empty($data['client_cst_no'])){
				$body.="CST: ".$data['client_cst_no'];
			}
			$body.="</div> "  ;
		}
	}
	$body.="</td>
	<td width=\"253px\" valign=\top\" class=\"ar\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"ar\">
		<tr>
			<td class=\"b al invoice-text pl5 pb3\">".$data['invoice_title']."</td>
		</tr>
		<tr>
			<td><img src=\"".$variables['images']."/invoice-top-corner-new.jpg\"  width=\"253px\" height=\"9\"  border=\"0\" /></td>
		</tr>
		<tr>
			<td class=\"invoice-border\">
				<div class=\"row wpx253\">
				<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">Relationship No.</span></div>
				<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$data['client']['number']."</span></div>
				</div>
				<div class=\"row wpx253\">
				<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">".$data['profm']." Invoice No.</span></div>
				<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$data['number']."</span></div>
				</div>";
	if (!empty($data['do_i']))
	{
	$temp=$temp1=$dtArr=null;
	$dd  =$mm=$yy='';
	$temp = explode(" ",$data['do_i']) ;
	$temp1=$temp[0];
	$dtArr =explode("-",$temp1) ;
	$dd =$dtArr[2];
	$mm =$dtArr[1];
	$yy=$dtArr[0];
	//$do_i = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
	$do_i = date("d M Y",$data['do_i']);

	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">".$data['profm']." Invoice Date</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$do_i."</span></div>
	</div>";
	}
	if (!empty($data['currency_abbr'])){
	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">".$data['profm']." Invoice Currency</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$data['currency_abbr']."</span></div>
	</div>";
	}

	/*
	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Order No.</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['or_no']."</span></div>
	</div>";
	*/

	if (!empty($data['do_d']))
	{
	/*
	$temp=$temp1=$dtArr=null;
	$dd  =$mm=$yy='';
	$temp = explode(" ",$data['do_d']) ;
	$temp1=$temp[0];
	$dtArr =explode("-",$temp1) ;
	$dd =$dtArr[2];
	$mm =$dtArr[1];
	$yy=$dtArr[0];*/

	$do_d = date("d M Y",$data['do_d']);
	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">Payment Due Date</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$do_d."</span></div>
	</div>";
	}

	if (!empty($data['do_fe']) && $data['show_period_chk'] ==1){
	$temp=$temp1=$dtArr=null;
	$dd  =$mm=$yy='';
	$temp = explode(" ",$data['do_fe']) ;
	$temp1=$temp[0];
	$dtArr =explode("-",$temp1) ;
	$dd =$dtArr[2];
	$mm =$dtArr[1];
	$yy=$dtArr[0];
	$do_fe = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">Service Start Date</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$do_fe."</span></div>
	</div>";
	}
	if (!empty($data['do_e'] ) && $data['show_period_chk'] ==1){
	$temp=$temp1=$dtArr=null;
	$dd  =$mm=$yy='';
	$temp = explode(" ",$data['do_e']) ;
	$temp1=$temp[0];
	$dtArr =explode("-",$temp1) ;
	$dd =$dtArr[2];
	$mm =$dtArr[1];
	$yy=$dtArr[0];
	$do_e = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));

	$body.="<div class=\"row wpx253\">
	<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">Renewal/Expiry Date</span></div>
	<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$do_e."</span></div>
	</div>";
	}
	//Display Proforma Invoice No.Bof
	if(!empty($data['inv_profm_no'])){
		$body.="<div class=\"row wpx253\">
		<div class=\"coloumn al custom-content pt2\"><span class=\"b pl5\">Proforma Invoice No.</span></div>
		<div class=\"coloumn-right custom-content pt2\"><span class=\"pr5\">".$data['inv_profm_no']."</span></div>
		</div>";
	}
	//Display Proforma Invoice No.Eof
	$body.="</td>
			</tr>
			<tr>
				<td><img src=\"".$variables['images']."/invoice-bottom-corner-new.jpg\"  width=\"253px\" height=\"9\"  border=\"0\" /></td>
			</tr>
		</table>
	</td>
	</tr>
	</table>
	</div> 
	<div class=\"row wpx731\">
		<div class=\"green-text ac b\">
			Go Green initiative! Every 3000 sheets of paper cost us a tree. Log on to MySMEERP to opt for 'Soft Copy Only'.
		</div>
	</div>
	<div class=\"row wp100 pt5\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
	<tr>
		<td width=\"".$data['wd1']."px\" class=\"bg1\"><img src=\"".$variables['images']."/first-left-corner.jpg\" width=\"10px\" 
		height=\"30px\" border=\"0\" /></td>
		<td class=\"bg1 al pl5\" width=\"".$data['wdinvp']."px\" ><span class=\"content-heading b\">Particulars</span></td>
		<td class=\"bg1 ar\" width=\"".$data['wdpri']."px\"><span class=\"content-heading b\">Rate</span></td>";
		if($data['show_nos'] ==1){
			$body.="<td class=\"bg1 ar\" width=\"".$data['wdnos']."px\"><span class=\"content-heading b\">Unit</span></td>";
		}
		$body.="<td class=\"bg1 ar\" width=\"".$data['wdamt']."px\"><span class=\"content-heading b\">Amt.</span></td>";
		if($data['show_discount'] ==1){
		 $body.= "<td class=\"bg1 ar\" width=\"".$data['wddisc']."px\"><span class=\"content-heading b\">Disc.</span></td>
		 <td class=\"bg1 ar\" width=\"".$data['wdst']."px\"><span class=\"content-heading b\">SubTot</span></td>";
		}
		$body.= "<td width=\"".$data['wd2']."px\" class=\"bg1\"><img src=\"".$variables['images']."/first-right-corner.jpg\"  width=\"9px\" 
		height=\"30px\" border=\"0\" /></td>
	</tr>";    
	$k=1;           
	foreach($data['particulars'] as $key=>$particular)
	{
		if (!empty($particular['ss_punch_line'])){
			$body.= "<tr>
				<td class=\"bg3 borl\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\"  border=\"0\" /></td>
				<td class=\"bg3 al content pt8 b pb5 pl5\" colspan=\"".$data['colsSpanNo1']."\">";
				$body.=$k.". ".nl2br($particular['ss_punch_line'])."</td>";
				$body.="<td class=\"bg3 al content pt8 b pb5 pl5\" >&nbsp;</td>";
				$body.="<td class=\"bg3 borr\"><img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" height=\"1px\"  border=\"0\" /></td>
			 </tr>";
		}
		$body.="<tr>
			<td class=\"pb5 bg3 borl\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>
			<td class=\"bg3 pb5 content vt  al pl10 pl5\">".nl2br($particular['particulars'])."</td>
			<td class=\"bg3 pb5 content vt  ar\">".$particular['p_amount']."</td>";
			if($data['show_nos'] ==1){ 
			 $body.="<td class=\"bg3 pb5 vt content ar\">".$particular['s_quantity']." ".$particular['s_type']."</td>" ;
			}
			$body.="<td class=\"bg3 pb5 vt content ar\">".$particular['s_amount']."</td>";
			if($data['show_discount'] ==1){
			 $body.="<td class=\"bg3 pb5 vt content ar\">".$particular['d_amount']."</td>";
			 $body.="<td class=\"bg3 pb5 vt content ar\">".$particular['stot_amount']."</td>";
			}  		
			$body.=" <td class=\"pb5 bg3 borr\">
				<img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" height=\"1px\"  border=\"0\" /></td>
		</tr>";
		$k++;	
	}
	 $body.="<tr>
				<td width=\"10px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-left-corner.jpg\"  width=\"10px\" 
				height=\"9px\"  border=\"0\" /></td>";
				$body.="<td class=\"bg3 borb\" colspan=\"".$data['colsSpanNo2']."\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>";
				$body.="<td width=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-right-corner.jpg\"  width=\"9px\" 
				height=\"9px\"  border=\"0\" /></td>
			</tr>
	</table>
	</div> ";

	$body.="<div class=\"row wp100 pt5\">
				<table align=\"right\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">	";
				if(!empty($data['tax1_name'])){			
					$body.="<tr>				
					 <td align=\"right\" width=\"500px\"><span class=\"content ar b\">Total :</span></td>
					 <td align=\"right\" width=\"100px\">
					 <span class=\"content pr10 b\">&nbsp;".$data['sub_total_amount']."</span></td>
					</tr>
					<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar\">".$data['tax1_name']." @ ".$data['tax1_value']." :</span></td>
						<td align=\"right\" width=\"100px\">
						<span class=\"content pr10 \">&nbsp;".$data['tax1_total_amount']."</span></td>
					</tr>";
					if(!empty($data['tax1_sub1_name']) && ($data['tax1_sub1_total_amount'] > 0)){
						$body.="<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar\">".$data['tax1_sub1_name']." @ ".$data['tax1_sub1_value']." :</span></td>
						<td align=\"right\" width=\"100px\">
						<span class=\"content pr10\">&nbsp;".$data['tax1_sub1_total_amount']."</span></td>
						</tr>" ;
					}
					if (!empty($data['tax1_sub2_name']) && ($data['tax1_sub2_total_amount'] > 0)){
						$body.="<tr>				
							<td align=\"right\" width=\"500px\"><span class=\"content ar \">".$data['tax1_sub2_name']." @ ".$data['tax1_sub2_value']." :</span></td>
							<td align=\"right\" width=\"100px\">
							<span class=\"content pr10\">&nbsp;".$data['tax1_sub2_total_amount']."</span></td>
						</tr>" ;
					}
				}
				if (!empty($data['round_off_op']) && !empty($data['round_off']) ){
					$body.="<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar \">Round off :</span></td>
						<td align=\"right\" width=\"100px\"><span class=\"content pr10\">"
						.$data['round_off_op']."&nbsp;".$data['round_off']."</span></td>
						</tr>" ;
				}
				$body.="<tr>				
						<td align=\"right\" width=\"500px\"><span class=\"content ar b\">Grand Total :</span></td>";
						if ($data['do_i_chk'] <= $data['do_rs_symbol']){
							$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\">".$data['currency_symbol']." ".$data['amount']."</span></td>";
						}else{
							$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\"><img src=\"".$variables['images']."/".$data['currency_symbol'].".jpg\" border=\"0\" /> ".$data['amount']."</span></td>";		 
						}
				$body.="</tr>";
			   $body.="<tr>
						<td align=\"right\" colspan=\"2\">
							<span class=\"content pr10 b\">" ;
							if(!empty($data['currency_name'])) {
								$body.=$data['currency_name'] ;
							} 
				$body.=   " ".$data['amount_words']." only 
							</span>
						</td>
					</tr>";
				$body.="</table>
			</div>";
	if( !empty($data['delivery_at'])){
		$body.="<div class=\"row wp100 pt1\">		
					<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"left\" width=\"100%\">				
						<tr>
							<td align=\"left\">
								<span class=\"content al b\">Delivery At :</span>
								<span class=\"content al\">".$data['delivery_at']."</span>
							</td>
						</tr>		
					</table>
				</div> ";
	}
	$body.="<div class=\"row wp100 pt2 pl10\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
		<tr>
			<td align=\"left\" >" ;
				$body.="<table align=\"left\" border=\"0\">
					<tr>					 
						<td class=\"content-small-10 b\">PAN No :&nbsp;</td>
						<td class=\"content-small-10\">".$data['pan_no']."</td>";
					if ($data['do_i_chk'] >= $data['do_tax'] && $data['do_tax']!=0  && !empty($data['tax1_name']) ){	
						if(!empty($data['vat_no'])){
							$body.="<td class=\"content-small-10 b\">&nbsp;&nbsp;&nbsp;VAT No :&nbsp;</td>
							<td class=\"content-small-10\">".$data['vat_no']."</td>";
						}
						if(!empty($data['cst_no'])){
							$body.="<td class=\"content-small-10 b\">&nbsp;&nbsp;&nbsp;CST No :&nbsp;</td>
							<td class=\"content-small-10\">".$data['cst_no']."</td>" ;
						}
					}
					if ($data['do_i_chk'] >= $data['do_st'] && $data['do_st']!=0 && !empty($data['tax1_name'])){	
						if(!empty($data['service_tax_regn'])){
							$body.="<td class=\"content-small-10 b\">&nbsp;&nbsp;&nbsp;Service Tax No :&nbsp;</td>
							<td class=\"content-small-10\">".$data['service_tax_regn']."</td>";
						}
					}
					$body.="</tr>
					</table>";
		
	$body.="</td>";	
	$body.= "</tr>     
		</table>
	</div> ";

	if (!empty($data['tax1_declaration'])){
	$body.="<div class=\"row wp100 pt2\">		
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"left\" width=\"100%\">	
					<tr>
						<td align=\"left\">
							<span class=\"content-small-7\">".$data['tax1_declaration']."</span>							
						</td>
					</tr>	
				</table>
			</div> ";
	}
	$body.="<div class=\"row wp100 pt5\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td width=\"10px\"><img src=\"".$variables['images']."/sec-gray-corner.jpg\"  width=\"10px\" height=\"22\"  border=\"0\" /></td>
			<td class=\"bg1 wpx711 al content-heading\"><span class=\"pl2\">Terms and Conditions</span></td>
			<td width=\"10px\"><img src=\"".$variables['images']."/sec-right-corner.jpg\"  width=\"10px\" height=\"22\"  border=\"0\" /></td>
		 </tr>
	</table>  
	</div>
	<div class=\"row wp100 pt6 pb10\">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td width=\"9px\"><img src=\"".$variables['images']."/third-top-left-corner.jpg\"  width=\"9px\" height=\"8px\" border=\"0\" /></td>
			<td width=\"335px\"class=\"bg2\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" 
			border=\"0\" /></td>
			<td width=\"8px\"><img src=\"".$variables['images']."/third-top-right-corner.jpg\"  width=\"8px\" height=\"8px\" 
			border=\"0\" /></td>
			<td width=\"6px\"></td>
			<td width=\"9px\"><img src=\"".$variables['images']."/third-top-left-corner.jpg\"  width=\"9px\" height=\"8px\" border=\"0\" /></td>
			<td width=\"356px\" class=\"bg2\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" 
			border=\"0\" /></td>
			<td width=\"8px\"><img src=\"".$variables['images']."/third-top-right-corner.jpg\"  width=\"8px\" height=\"8px\" 
			border=\"0\" /></td>
		 </tr>
		 <tr>
			<td class=\"bg2\"></td>
			<td class=\"bg2 al pl2\" valign=\"top\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
					<tr>
						<td class=\"pt2 content-small-8 vt wp4 ar\">1.</td>
						<td class=\"pt2 content-small-8 vt pl2 \">Payment must be done within 7 days from invoice date or as per quotation.
						Every cheque returned will attract a penalty of INR 500/-.</td>
					</tr>
					<tr>
						<td class=\"pt2 content-small-8 vt ar\">2.</td>
						<td class=\"pt2 content-small-8 vt pl2\">Late payment	charges @ 2.5% per month or INR 500, whichever is higher.</td>
					</tr>	
					<tr> 
						<td class=\"pt2 content-small-8 vt ar\">3.</td>
						<td class=\"pt2 content-small-8 vt pl2\"> 
						Neither Goods, Products, Services once sold will not be taken back or
						exchanged nor payment will be refunded. Our risk & responsibility ceases
						on delivery of the goods. After sales support services is valid only if
						applicable to purchased item and explicitly mentioned in this invoice.
						</td>
					</tr>	
					<tr>
						<td class=\"pt2 content-small-8 vt ar\">4.</td>
						<td class=\"pt2 content-small-8 vt pl2\">Product and company credits are mentioned compulsorily.</td>
					</tr> 	
					<tr>
					   <td class=\"pt2 content-small-8 vt ar\">5.</td>
					   <td class=\"pt2 content-small-8 vt pl2\">Renewal payments must be paid before 60 days of renewal date.</td>
					</tr>   
					<tr>
					   <td class=\"pt2 content-small-8 vt ar\">6.</td>
					   <td class=\"pt2 content-small-8 vt pl2\">Payment to be made in favour of <b>'".$data['company_name']."'</b>.</td>
					</tr> 
					<tr>
					   <td class=\"pt2 content-small-8 vt ar\">7.</td>
					   <td class=\"pt2 content-small-8 vt pl2\"> Subject to Jhena Jurisdiction.</td>
					</tr> 
				</table>
			</td>
			<td class=\"bg2\"></td>
			<td></td>
			<td class=\"bg2\"></td>
			<td class=\"bg2 al pl2\" valign=\"top\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
					<tr>
						<td class=\"pt2 content-small-8 vt wp4 ar\">8.</td>
						<td class=\"pt2 content-small-8 vt pl2\">
						If billed for our products like myCraft<sup>TM</sup>, myGallery, Nerve Center<sup>TM</sup> etc. you get
						non-transferable and single domain license to use only on our web servers
						without ftp access and which is subject to renewal every year against maintenance
						support charges. Under all/any circumstances, code of the product is property of
						SMEERP. 
						</td>
					</tr>              
					<tr> 
						<td class=\"pt2 content-small-8 vt ar\">9.</td>
						<td class=\"pt2 content-small-8 vt pl2\">By making payment against this invoice you agree to:</td>
					</tr>
					<tr>
						<td></td>
						<td class=\"pt1 content-small-8 vt\">a) all the terms &amp; conditions mentioned here and overleaf (including those mentioned in the proposal & MySMEERP)</td>
					</tr>	
					<tr>
						<td></td>
						<td class=\"pt1 content-small-8 vt\">	
							b) compulsory make use of MySMEERP portal for service and support
						</td>
					</tr>	
					<tr>
						<td></td>
						<td class=\"pt1 content-small-8 vt\">			
							<div class=\"coloumn\">c) </div>
							<div class=\"coloumn pl2\" style=\"width:300px;\">in case of delinquent payment or non-payment, domain name 
							ownership can't be claimed</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class=\"pt1 content-small-8 vt\">			
							<div class=\"coloumn\">d) </div>
							<div class=\"coloumn pl2\" style=\"width:300px;\">
							remember the renewal dates yourself and make timely payment
							</div>
						</td>
					</tr>
					<tr> 
						<td class=\"pt2 content-small-8 vt ar\">10.</td>
						<td class=\"pt2 content-small-8 vt pl2\">
							Errors and ommissions excepted (E &amp; O.E.)
						</td>
					</tr>	
				</table>
			</td>
			<td class=\"bg2\"></td>
		</tr> 
		<tr>
			<td><img src=\"".$variables['images']."/third-bottom-left-corner.jpg\"  width=\"9px\" height=\"8px\" border=\"0\" /></td>
			<td class=\"bg2\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\"  border=\"0\" /></td>
			<td><img src=\"".$variables['images']."/third-bottom-right-corner.jpg\"  width=\"8px\" height=\"8px\"  border=\"0\" /></td>
			<td></td>
			<td><img src=\"".$variables['images']."/third-bottom-left-corner.jpg\"  width=\"9px\" height=\"8px\" border=\"0\" /></td>
			<td class=\"bg2\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\"  border=\"0\" /></td>
			<td><img src=\"".$variables['images']."/third-bottom-right-corner.jpg\"  width=\"8px\" height=\"8px\"  border=\"0\" /></td>
		</tr>
	</table>   
	</div>
	<div class=\"row wpx731 bg9 pb3 pt3\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl20\" width=\"393px\">
					<span class=\"white-text b\">".$data['company_name']."
				</td>
				<td class=\"address-text ar pr10\">
					<span class=\"white-text-12 b\">www.SMEERPTech.in</span> 
				</td>
			</tr>
		</table>
	</div>
	<div class=\"row wp731 pt5 pb5 bg8 \">
	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td class=\"address-text al pl20\" width=\"265px\">
				<span class=\"white-text\"><b>Questions?</b></span> Contact us with a support ticket <br/>or call us at the numbers to the right.
			</td>
			<td class=\"address-text ar pr10\" width=\"466px\">
				<span class=\"white-text\"><b>SMEERPPlex,</b></span> 17, MG street | Jhena 440 025 Maharashtra | India<br/>
				<b>INDIA</b> : 99290-699991, 99290-699992   |   <b>USA & CANADA</b> : 1-837-621-1000
			</td>
		</tr>
	</table>
	</div>	
</div>
<div class=\"clear\"></div>" ;
//Terms 2 BOF
$body.="<div class=\"row wp100 al pt1 pb5\" style=\"height:843px\">
		<table cellspacing=\"0\" cellpadding=\"0px\" border=\"0\" width=\"100%\">
			<tr>
				<td width=\"10px\"><img src=\"".$variables['images']."/sec-gray-corner.jpg\" width=\"10px\" height=\"15px\" 
				border=\"0\" /></td>
				<td class=\"bg1 wpx711 al content-heading\"><span class=\"pl2\">Terms and Conditions</span></td>
				<td width=\"10px\"><img src=\"".$variables['images']."/sec-right-corner.jpg\" width=\"10px\" 
				height=\"15px\" border=\"0\" /></td>
			</tr>	 
			<tr>
				<td class=\"al pr10 pl10 borlr borb pt3\" colspan=\"3\" valign=\"top\" width=\"100%\" >
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" style=\"padding-left:0px;padding-right:0px\">
					<tr>
						<td width=\"50%\" valign=\"top\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:5px\">
							<tr>
								<td class=\"pt2 content-small-term pl pr5\">
								<div class=\"row\">
									<div class=\"coloumn\">By making payment against this invoice/proposal, it is agreed between ".$data['company_name']." ('".$data['company_quot_prefix']."') and the Customer as follows -</div>
								</div>
							   </td>
							</tr> 	
							<tr>
								<td class=\"content-small b pl pt2 pr5\"> A. Definitions</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term pl10 pr5\">
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Extra Efforts' or 'Additional Services' means any efforts or services which are not mentioned in the proposal.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Charges' means all fees, charges/tariffs, costs and rates chargeable by ".$data['company_quot_prefix']." from time to time for providing the Customer with the Service, for Software Products, for Goods and Additional Services and other Government levies.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Customer or Client' shall mean any person, partnership firm or private limited company or limited company or such other organisation authorized by ".$data['company_quot_prefix']." to use the services or to whom ".$data['company_quot_prefix']." sell goods or services.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Government' shall mean the Government of India and/or the State Government of Maharashtra or such other local authority, as the case may be.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Services' shall mean all/any services made available by ".$data['company_quot_prefix']." or via its associates or channel partners.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Software Products' of ".$data['company_quot_prefix']." are like myCraft<sup>TM</sup>, myGallery, Nerve Center<sup>TM</sup> etc. and are products/solutions developed by ".$data['company_quot_prefix']." and licenses for which are sold by ".$data['company_quot_prefix']." or via its associates or channel partners.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Goods' shall mean all the Goods/Material/Equipment sold by ".$data['company_quot_prefix']." or via its associates or channel partners.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'MySMEERP' is the online Customer Support Portal for support, billing, communication etc.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Renewal/Expiry Date' means the renewal/expiry date of Software Product/Equipment/Goods/Service as mentioned in Invoice.
									</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">'Support Email' means support@smeerptech.com
									</div>
								</div>												
								</td>
							</tr>	
							<tr>
								<td class=\"content-small b pl pt2 pr5\"> B. Provision of Services</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">".$data['company_quot_prefix']." agrees to provide the Services or Goods to the Customer subject to the terms and conditions of this agreement.
									</div>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pl pt2 pr5\"> C. Obligations of ".$data['company_quot_prefix']."</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." shall use reasonable efforts to make the Services available to the Customer at all times.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">The availability and quality of the Services may be affected by the factors outside ".$data['company_quot_prefix']."'s control such as physical obstructions, geographic, weather conditions and other causes of interference or faults.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">Services may be suspended in whole or in part at any time, without notice, if Network fails or requires maintenance. ".$data['company_quot_prefix']." will make all reasonable efforts to minimise the frequency and duration of such events. The Customer will remain liable for all charges during the period of suspension unless ".$data['company_quot_prefix']." in its discretion decides otherwise.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \"> ".$data['company_quot_prefix']." has the sole right and discretion to vary or increase the Charges at any time on reasonable notice to the Customer.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." reserves the right to apply a monthly/quarterly/half-yearly/yearly financial limit and such other conditions for Charges incurred by the Customer and to demand interim advance payment, suspend or disconnect access to the Services if such limits are exceeded.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;6.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." has the right to check the credentials of the Customer including the Customer's financial standing and to use the services of any person or agency for such purpose.</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pl pt2 pr5\"> D. Obligations of the Customer</div></td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term pl10 pr5\">
								<div class=\"row\">
									<div class=\"coloumn \"> The Customer hereby expressly agrees:</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;1.</div>
									<div class=\"coloumn \"> To make payments for the Services on the following basis</div>
								</div>
								<div class=\"row pl15\">(a)	payment will be due when ".$data['company_quot_prefix']." raises the billing statement on the Customer.</div>
								<div class=\"row pl15\">(b)	payment will be made on or before the due date mentioned in the invoice or as per schedule mentioned in the proposal, failing which ".$data['company_quot_prefix']." shall be entitled to charge interest @18% per annum or INR 500 whichever is higher and/or late fees on all outstanding Charges from due date till date of payment &amp; shall be entitled to discontinue Services or delivery of Goods/Software Products, without notice, in its sole discretion. Cheque return will attract a penalty of Rs. 1000 separately.</div>
								<div class=\"row pl15\">(c)	in case of delinquent payment or non-payment, neither the domain name ownership cannot be claimed by Customer nor can be transferred to any other service provider.</div>
								<div class=\"row pl15\">(d)	the Customer will pay all the costs of collection and legal expenses, with interest should it become necessary to refer the matter to a collection agency or to legal recourse to enforce payment.</div>
								<div class=\"row pl15\">(e)	".$data['company_quot_prefix']." shall be entitled to apply payments/deposits/advances made by the Customer towards any Charges outstanding including for any other ".$data['company_quot_prefix']." service or goods availed by the customer.</div>
								<div class=\"row pl15\">(f)	payments will be made in cash, credit card or A/c Payee cheque or demand draft or internet banking or any other instrument drawn on any bank in Jhena and payable at Jhena.</div>
								<div class=\"row pl15\">(g)	the Customer shall be liable for all Charges for the Services provided to the Customer whether or not the Services have been used by the Customer.</div>
								<div class=\"row pl15\">(h)	in the event of any dispute regarding the Charges, the Customer agrees to pay ".$data['company_quot_prefix']." Charges billed pending resolution of such dispute.</div>
								<div class=\"row pl15\">(i)	the Customer shall be liable to pay for the Services provided even if he/she does not receive the bills. It will be the Customer's responsibility to make enquiries in case of non-receipt of bills.</div>
								<div class=\"row pl15\">(j)	Charges payable by the Customer are exclusive of taxes, duties or levies payable, unless expressly stated to the contrary in the billing statement.</div>
								<div class=\"row pl15\">(k)	any advance/security deposit paid by the Customer shall be adjusted against any dues payable by the Customer to ".$data['company_quot_prefix']." and balance if any will be refunded by ".$data['company_quot_prefix']." within 60 days from the deactivation of the Services.</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;2.</div>
									<div class=\"coloumn \">To make advance payment for Charges including service charges if billed to customer by ".$data['company_quot_prefix'].".</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;3.</div>
									<div class=\"coloumn \">To not use or cause or allow others to use the Services for any improper, immoral or unlawful purpose including in any manner (for e.g. Spamming, Bulk Emailing, Transmission of infected content etc.) which may jeopardise or impair the operation of the Network and/or the Services.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;4.</div>
									<div class=\"coloumn \">To comply with instructions issued by Government or ".$data['company_quot_prefix'].", concerning Customer's access to and use of Services.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;5.</div>
									<div class=\"coloumn \">To furnish correct and complete information and documents as required by ".$data['company_quot_prefix']." from time to time. The Services agreed to be provided by ".$data['company_quot_prefix']." shall always be subject to verification of the Customer's credentials and documents and if at any time, any information and/or documents furnished by the Customer is/are found incorrect or incomplete or suspicious, ".$data['company_quot_prefix']." shall be entitled to suspend/terminate the Service forthwith without any further notice.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;6.</div>
									<div class=\"coloumn \">That ".$data['company_quot_prefix']." may suspend the Service in whole or in part at any time without prior notice and without assigning any reason thereto. ".$data['company_quot_prefix']." reserves the right to Charge for re-activation.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;7.</div>
									<div class=\"coloumn \">Customer is liable (a) for the Charges during the period of suspension and thereafter (b) to pay the overages as applicable and (c) Charges towards Extra Efforts</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;8.</div>
									<div class=\"coloumn \">To comply with the applicable laws, rules and regulation regarding the use of the Services and procurement of the Equipment including but not limited to relevant tax laws and import control regulations.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;9.</div>
									<div class=\"coloumn \">To pay to ".$data['company_quot_prefix']." such amount as ".$data['company_quot_prefix']." may require as security for the due performance of the Customer's obligations under these terms and conditions. ".$data['company_quot_prefix']." may set off these amounts against any cost, damages or expenses which ".$data['company_quot_prefix']." may suffer or incur as a result of the Customer's failure to perform any of these obligations. Security deposit amount shall not carry any interest.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">10.</div>
									<div class=\"coloumn \">To inform ".$data['company_quot_prefix'].", in writing, of any changes in billing name or billing address or email id or cellular number. Any written communication, billing statement or notice from ".$data['company_quot_prefix']." to the Customer will be deemed as served within 48 hours of posting by ordinary mail or email.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">11.</div>
									<div class=\"coloumn \">To notify ".$data['company_quot_prefix']." immediately in case of any complaints with regards to the Services via email to Support Email</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">12.</div>
									<div class=\"coloumn \">Not to assign any right or interest under this agreement without ".$data['company_quot_prefix']."'s prior written consent.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">13.</div>
									<div class=\"coloumn \">To be bound at all times by any modifications and or variations made to these terms and conditions.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">14.</div>
									<div class=\"coloumn \">Customer is not entitled to assign/transfer/resell/lease/rent or create any charge/lien on the Service of any nature whatsoever or Software Product owned by ".$data['company_quot_prefix'].", without prior permission of ".$data['company_quot_prefix'].". Any transfer affected in contravention of the express terms contained herein, shall not absolve the Customer of his/her primary duty towards ".$data['company_quot_prefix']." for usages charges levied against the Customer.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">15.</div>
									<div class=\"coloumn \">To timely provide ".$data['company_quot_prefix']." with the certificates etc. towards any deductions as per the law.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">17.</div>
									<div class=\"coloumn \">Security and Backup of the data to which Customer is provided access with is the liability of Customer.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">18.</div>
									<div class=\"coloumn \">Customer must verify the credentials of the visiting ".$data['company_quot_prefix']."'s personnel.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">19.</div>
									<div class=\"coloumn \">Neither ".$data['company_quot_prefix']."'s personnel will install any illegal/pirated software/text/images for Customer nor any Illegal/pirated software/text/images are covered under maintenance service support.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">21.</div>
									<div class=\"coloumn \">Work will commence once ".$data['company_quot_prefix']." receives (a) Work/Purchase order referring proposal, scope of work and payment schedule (b) Duly signed and stamped copy of proposal (c) Duly filled, signed and stamped Customer Agreement form.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">22.</div>
									<div class=\"coloumn \">Customer must designate only one of his/her personnel to coordinate and to approve the work/delivery. Customer is not allowed to change designated personnel till completion of work. In case of factors outside control, change of personnel is allowed if agreeable to both the parties.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">23.</div>
									<div class=\"coloumn \">Domain and Web hosting account with data/email is deleted after seven days of Renewal/Expiry Date. Late renewal during these seven days attracts late payment Charges too.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">24.</div>
									<div class=\"coloumn \">".$data['company_quot_prefix']." will remind Customer about the renewal dates via SMS/Email automated notification. Customer is responsible to remember renewal dates and make renewal payments before 60 days of Renewal/Expiry Date.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">25.</div>
									<div class=\"coloumn \">Customer must provide information/references for his/her choice for design, images in JPEG format, content in editable MS Word format and Logo in editable CDR or PSD format via email and before commencement of work. Our designers will use all the images provided as-is. Besides basic cropping, ".$data['company_quot_prefix']." is unable to perform advanced manipulation. Logo design, Content writing or typing or proof-reading or extra design samples is separate job work needing Extra Efforts and is separately chargeable. Static page means an A4 size page containing text in Arial font size 12.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">26.</div>
									<div class=\"coloumn \">Customer is liable to pay any statutory duties or levies or taxes as applicable by the law. </div>
								</div>						
								</td>
							</tr>					
							</table>
						</td>
						<td width=\"50%\" valign=\"top\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:2px;padding-right:2px\">
								<tr>
								<td class=\"content-small-term pl10 pr5\">
								<div class=\"row\">
									<div class=\"coloumn pr5\">27.</div>
									<div class=\"coloumn \">Once the order is confirmed Customer must sign up with MySMEERP.</div>
								</div>						
								<div class=\"row\">
									<div class=\"coloumn pr5\">28.</div>
									<div class=\"coloumn \">".$data['company_quot_prefix']." will not be responsible for any delay in schedule caused due to delay in providing the content to ".$data['company_quot_prefix']." or non-availability of any of Customer official or any other delay on Customer part. Customer will not hold back any of ".$data['company_quot_prefix']."'s payment for delays on part of Customer. Content received after the completion of work OR in excess as per proposal will be termed as extra and separately chargeable.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">29.</div>
									<div class=\"coloumn \">Transfer of domain name from his/her existing service provider to ".$data['company_quot_prefix']." is the sole responsibility of Customer.</div>
								</div>
								<div class=\"row\">
									<div class=\"coloumn pr5\">30.</div>
									<div class=\"coloumn \">Customer must use MySMEERP or email to Support Email for service and support.</div>
								</div>						
								</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\"> E. Software Product License to use:</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">If billed for, Customer gets the license to use for the ".$data['company_quot_prefix']."'s Software Products. ".$data['company_quot_prefix']."'s Software Products are provided on license to use basis only on ".$data['company_quot_prefix']."'s web servers without ftp & hosting control panel access. The license to use of Software Products is non-transferable to any other Domain name or other person/party and is subject to yearly renewal against annual maintenance support charges as specified in the proposal.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">Under all/any circumstances, programming code of Software Products is the sole property of ".$data['company_quot_prefix']." and under no circumstances Customer can claim any rights towards the ownership of product or code. License of the product is only for using it i.e. License to use.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">Credits to Software Product and/or ".$data['company_quot_prefix']." are compulsorily mentioned on the Customer website. Credit to ".$data['company_quot_prefix']." is compulsorily mentioned on the designed and/or printed material of the Customer.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">Any customization done in the Software Product doesn't make the Customer the owner of product. Under all/any circumstances the Software Product remains the property of ".$data['company_quot_prefix']." with full rights over the entire product and customizations done.</div>
									</div>
									</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\"> F. Validity</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">Both parties agree that, this agreement has been duly authorised and executed and is valid and binding and is enforceable in law in accordance with its terms.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">The validity construction and performance of this agreement shall be governed by and interpreted in accordance with the laws of the Republic of India.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">The courts in Jhena shall have the jurisdiction.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">Should any provision of this agreement be or become ineffective or be held to be invalid, this shall not affect the validity of the remaining provisions. Any invalid provision in this agreement shall be replaced, interpreted or supplemented as the case may be in such a manner that the intended economic purpose of the agreement will be achieved.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">This agreement is complete and exclusive statement of agreement between parties and it supersedes all understanding or prior agreement, whether oral or written and all representations or other communications between parties.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;6.</div>
										<div class=\"coloumn \">These terms & conditions are subject to applicable Indian laws, rules and regulation framed thereunder and any statutory modifications or re-enactment for time being in force and any other Government regulations issued from time to time.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;7.</div>
										<div class=\"coloumn \">Prices/Availability/Specifications are subject to change without prior notice.</div>
									</div>
									</td>
								</tr>	
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\">G. Support, Goods Return, Warranties, Disclaimer of warranties</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;1.</div>
									<div class=\"coloumn \">".$data['company_quot_prefix']." sends Customer the information related to terms/conditions/services/offers/warnings/support/advisories etc. via email/sms/telephone. It is Customer's responsibility to do the needful accordingly.</div>
									</div>
									</td>
								</tr>	
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
									<div class=\"coloumn pr5\">&nbsp;2.</div>
									<div class=\"coloumn \">Annual Maintenance Support Charge for renewal of License to use of Software Product is compulsory as specified in the proposal and includes services (a) Maintaining Software Product in working condition (b) FREE feasible patches and updates (c) Customer support via email, telephone and MySMEERP</div>
									</div>
									</td>
								</tr>						
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
										<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">Goods, Software Products, Services once sold will not be taken back or exchanged and payment will not be refunded under any circumstances. Our risk & responsibility ceases on delivery of the goods. After sales support services is valid only if applicable to purchased item and explicitly mentioned in the invoice.</div>
										</div>
									</td>
								</tr>	
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
										<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." makes no representation or warranty other than those set forth in this agreement. ".$data['company_quot_prefix']." expressly disclaims all other warranties express or implied, including, but not limited to any implied warranty or merchantability or fitness for a particular purpose.</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
										<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">Any goods or equipment re-sold by ".$data['company_quot_prefix']." will carry the warranty, if any, of the manufacturer. Service to such goods or equipment, if not explicitly mentioned in invoice, if any, will be provided by authorized service centre of the manufacturer. ".$data['company_quot_prefix']." is not liable towards service support or replacement under warranty, if any, of any goods or equipment manufactured by third party and that ".$data['company_quot_prefix']." is just reselling.</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\"> H. Disclaimer of liability</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." shall not be liable to the Customer for any loss or damage whatsoever or howsoever caused arising directly or indirectly in connection with this agreement, the Services or Products or Equipment or Goods, their use, application or otherwise except to the extent to which it is unlawful to exclude such liability.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">Malfunction/Limitation in working of any of ".$data['company_quot_prefix']."'s Software Product due to any change in third party Software/Hardware on which ".$data['company_quot_prefix']."'s Software Product is dependent for functioning is not covered under maintenance support and ".$data['company_quot_prefix']." cannot be held responsible for it. Though ".$data['company_quot_prefix']." shall use reasonable efforts for resolution of issue if feasible. However Customer will be liable towards Charges for these Extra Efforts.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">Under any circumstances, ".$data['company_quot_prefix']." will not be responsible for any kind of loss / damage of data / equipment etc. while / due to working ".$data['company_quot_prefix']."'s service personnel at Customer's site. All the maintenance and service related work executed is at the risk of Customer. Customer has rights to ask ".$data['company_quot_prefix']."'s service personnel to work under Customer's authorized personnel's vigilance, seek information and take decision regarding maintenance / service work.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">Notwithstanding the generality of (a) above, ".$data['company_quot_prefix']." expressly excludes liability for consequential loss, damage or for loss of profit, business revenue, goodwill or anticipated savings.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." at its discretion, may send to the Customer various information on his/her cellular number through SMS or on his/her email id through Email or on his/her billing address through mail or otherwise, as an Additional Service. In case the Customer does not wish to receive such information he/she may notify ".$data['company_quot_prefix']." via email at Support Email</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;6.</div>
										<div class=\"coloumn \">In the event that any exclusion contained in this agreement shall be held to be invalid for any reason, and ".$data['company_quot_prefix']." becomes liable for the loss or damage that it may otherwise not have been liable, such liability shall be limited to the cost of the Services actually paid for by the Customer to ".$data['company_quot_prefix']." during the relevant period.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;7.</div>
										<div class=\"coloumn \">Customer agrees to indemnify and keep ".$data['company_quot_prefix']." harmless and defend ".$data['company_quot_prefix']." at its own expense from and against all claims arising as a result of breach of this agreement and from all taxes, duties or levies.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;8.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." advises Customer to make cash payments only after verifying the credentials (ID card etc.) of the visiting personnel of ".$data['company_quot_prefix'].". ".$data['company_quot_prefix']." encourages cheque, demand draft or internet banking transfer to avoid any untoward incident. ".$data['company_quot_prefix']." will not be liable for any such loss incurring due to handing over of cash payment to unauthorized personnel.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;9.</div>
										<div class=\"coloumn \">Customer agrees that any request/communication received from Customer's and his/her authorized personnel email ids or mobiles numbers on ".$data['company_quot_prefix']."'s Support Email via email or SMS shall be deemed to be valid request/communication from the Customer. Nothing herein shall apply with respect to the notice to be given by the Customer section J or any other provisions of this agreement.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">10.</div>
										<div class=\"coloumn \">Customer must regularly update and verify credentials of his / her authorized personnel registered with MySMEERP and in case of any issue must inform ".$data['company_quot_prefix']." immediately and confirm the same in writing to avoid loss/damage. Liability of any loss due to misuse/mistake/mishandling/loss of confidential information by/due to Customer or his/her authorized personnel remains with Customer and ".$data['company_quot_prefix']." will not be held responsible under any circumstances.</div>
									</div>							
									</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\">I. Termination</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">	Either party shall have the right to terminate the agreement by giving 60 days prior notice in writing.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">Notwithstanding anything contained herein, ".$data['company_quot_prefix']." shall be entitled to terminate this agreement and the Services if (a)	the Government either suspends or terminates the License or Permission or the Services temporarily or otherwise (b)	at any time the Customer fails to satisfy the requisite credit checks or provides fraudulent information to ".$data['company_quot_prefix'].". (c)	the Customer fails to pay its subscription or the Charges due (d)	the Customer is in breach of any other terms of the agreement and the Customer does not remedy the breach within seven (7) days of the day of receipt of a written / email notice from ".$data['company_quot_prefix']." specifying the breach.</div>
										</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">The agreement may also be terminated at the option of either party, on the happening of the following events (a) if either party is declared insolvent, bankrupt or is liquidated or dissolved (b)	if a trustee or receiver is appointed to take over the assets of either party (c)	if the Government requires any of this agreement to be revised in such a way as to cause significant adverse consequences to either party</div>
										</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">Termination of this agreement under the preceding provisions shall be without prejudice to and in addition to any right or remedy available to the terminating party under any applicable law or statute.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">In the event of termination of the agreement for any reason whatsoever, ".$data['company_quot_prefix']." shall be entitled to recover all outstanding Charges and dues from the Customer.</div>
									</div>
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;6.</div>
										<div class=\"coloumn \">If the agreement is terminated for reasons of fraudulent information provided or misuse or unlawful use by the Customer, the security deposit shall be forfeited.</div>
									</div>				
									</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\"> J. Miscellaneous</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">All notices required to be given to 
										".$data['company_quot_prefix']." pursuant to this agreement shall be in writing and shall be directed by registered post to the Registered office at ".$data['company_name'].", Plot no. 17, MG street, Jhena 440 025. www.SMEERPTech.in support@smeerptech.com</div>
									</div>				
									</td>
								</tr>
							</table>
						</td>
					</tr>			
					</table>
				</td>
			</tr>
		</table>   
		</div>
		<div class=\"row wpx731 bg9 pb3 pt3\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
				<tr>
					<td class=\"address-text al pl10\" width=\"393px\">
						<span class=\"white-text b\">".$data['company_name']."
					</td>
					<td class=\"address-text ar pr10\">
						<span class=\"white-text b\">www.SMEERPTech.in &nbsp; support@smeerptech.com</span> 
					</td>
				</tr>
			</table>
		</div>
		<div class=\"row wp731 pt5 pb5 bg8\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
			<tr>
				<td class=\"address-text al pl10\" width=\"325px\">
					<span class=\"white-text\"><b>Worldwide Network :</b></span> 
					<span class=\"address-text-small\">INDIA, USA, UK, UAE, OMAN, AUSTRALIA <br/>
					MEXICO, CANADA, EUROPE, JAPAN, SINGAPORE, MALAYSIA</span> 
				</td>
				<td class=\"address-text ar pr10\" width=\"406px\">
					<span class=\"white-text\"><b>Corporate office:</b></span> 
					<span class=\"address-text-small\">
					17, MG street | Jhena 440 025 Maharashtra | India <br/></span> 
					<b>Dedicated Support:</b> <span class=\"address-text-small\">
					99290-699991, 99290-699992</span> 
				</td>
			</tr>
			</table>
		</div>";
//Terms 2 EOF

$body.="
</div>
</div>";
    $invoiceArr['content'] = $body ;
    $invoiceArr['header'] = '' ;
    $invoiceArr['footer'] = '' ;
    createInvoicePDF($data["number"],$invoiceArr);
}

        function getBifurcation( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) ";
            }
            
            $query .= " FROM ". TABLE_BILL_INV_B;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
                }
                
                return ( $db->nf() );
            }
            else {
                return false;
            }
        }

    }
    
    
function createInvoicePDF( $q_no, $data ) {
                require_once(DIR_FS_ADDONS .'/html2pdf/config.inv.inc.php');
                require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
                require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
                parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
              
                global 	$g_config;
                
                class ReportData extends Fetcher {
                    var $content;
                    var $url;
                
                    function get_data($data) {
                        return new FetchedDataURL($this->content, array(), "","");
                    }
                
                    function ReportData($content) {
                        $this->content = $content;
                    }
                    
                    function get_base_url() {
                        return "";
                    }
                }
                
                $contents 	= '';
                $contents = $data['content'];      
                
                // Configuration Settings.
                $g_config = array(
                                    'process_mode' 	=> 'single',
                                    'mode'		    => '',
                                    'pixels'		=> '750',
                                    'scalepoints'	=> '1',
                                    'renderimages'	=> '1',
                                    'renderlinks'	=> '1',
                                    'renderfields'	=> '1',
                                    'media'			=> 'A4',
                                    'cssmedia'		=> 'screen',
                                    //'leftmargin'	=> '30',
                                    'leftmargin'	=> '5',
                                    //'rightmargin'	=> '15',
                                    'rightmargin'	=> '5',
                                    'topmargin'		=> '5',
                                    'bottommargin'	=> '5',
                                    'encoding'		=> 'iso-8859-1',
                                    //'encoding'		=> 'UTF-8',
                                    'headerhtml'	=> $data['header'],
                                    'boydhtml'		=> $contents,
                                    'footerhtml'	=> $data['footer'],
                                    'watermarkhtml'	=> '',
                                    'pslevel'		=> '3',
                                    'method'		=> 'fpdf',
                                    'pdfversion'	=> '1.3',
                                    'output'		=> '2',
                                    'convert'		=> 'Convert File'
                                );
                
                $media = Media::predefined('A4');
                $media->set_landscape(false);
                /*
                $media->set_margins(array(	'left' => 10,
                                            'right' => 10,
                                            'top' => 15,
                                            'bottom' => 35)
                                    );
                $media->set_pixels(700);
                */
                 $media->set_margins(array(	'left' => 5,
                                            'right' => 5,
                                            'top' => 5,
                                            'bottom' => 5)
                                    );
                $media->set_pixels(740);
                
                $g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
                $g_pt_scale = $g_px_scale * 1.43;
                
                $pipeline 						= PipelineFactory::create_default_pipeline("","");
                $pipeline->pre_tree_filters[] 	= new PreTreeFilterHeaderFooter( $g_config['headerhtml'], $g_config['footerhtml']);
                $pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
                if ($g_config['renderfields']) {
                    $pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
                }
        
                // Changed so that script will try to fetch files via HTTP in this case) and to provide absolute paths to the image files.
                // http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
                //		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
                $pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
                //$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
                $pipeline->destination 			= new DestinationFile($q_no);
                 
                $pipeline->process($q_no, $media);
                $filename = DIR_FS_INV_PDF_FILES .'/'. $q_no .'.pdf'; 
                @chmod($filename, 0777);
                $contents = NULL;
                return true;
}
            
            

			

    
?>
