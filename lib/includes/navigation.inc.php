<?php
    
    
    class Navigation {
				
		const ACTIVE  = 1;		
		const DEACTIVE = 0;

		function getStatus() {
			$status = array(
							'ACTIVE'  => Navigation::ACTIVE,							
							'DEACTIVE' => Navigation::DEACTIVE
						);
			return ($status);
		}
		
        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			
			$query = "SELECT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			/*	SELECT t1.title AS lev1, t2.title as lev2
FROM navigation AS t1
LEFT JOIN navigation AS t2 ON t2.parent_id = t1.id
WHERE t1.title = 'Address book';*/

			$query .= " FROM ". TABLE_NAVIGATION." AS mTbl LEFT JOIN ".TABLE_NAVIGATION." AS subTbl ON subTbl.parent_id=mTbl.id ";
			
			 $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
                
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}
			
		}
       
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_NAVIGATION;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Navigation::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Navigation::getDetails($db, $user, 'id', " WHERE id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_NAVIGATION
								." SET status = '$status_new' "
								." WHERE id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Menu was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_NAVIGATION
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function validateSubAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$service = NULL;
            //validate the service division
            if(empty($data['parent_id'])){   
                $messages->setErrorMessage('Parent Menu should be selected.');
            }
			//validate the service title 
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage('Sub Menu title should be provided.');
            }
            if ( !isset($data['page1']) || empty($data['page1']) ) 
                $messages->setErrorMessage('Page name should be provided.');
          
          	if ( !isset($data['selected_page']) || empty($data['selected_page']) ) 
                $messages->setErrorMessage('Selected Page name should be provided.');
                
            if ( !isset($data['sequence']) || empty($data['sequence']) ) 
                $messages->setErrorMessage('Sequence should be provided.');
            
             if ( !isset($data['perm_name']) || empty($data['perm_name']) ) 
                $messages->setErrorMessage('Permission name should be provided.');
			//check for same service title name
			$menu_title = trim($data["title"]);
			$parent_id = trim($data["parent_id"]);
            
            if ( (Navigation::getList($db, $service, 'subTbl.title', " WHERE subTbl.title = '$menu_title' AND subTbl.parent_id = '$parent_id' ")) > 0 )
				$messages->setErrorMessage('Sub Menu already exists. Please change the title.');
                                    
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
        function validateSubUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			$service = NULL;
            //validate the service division
            if(empty($data['parent_id'])){   
                $messages->setErrorMessage('Parent Menu should be selected.');
            }
			//validate the service title 
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage('Sub Menu title should be provided.');
            }
            if ( !isset($data['page1']) || empty($data['page1']) ) 
                $messages->setErrorMessage('Page name should be provided.');
          
          	if ( !isset($data['selected_page']) || empty($data['selected_page']) ) 
                $messages->setErrorMessage('Selected Page name should be provided.');
                
            if ( !isset($data['sequence']) || empty($data['sequence']) ) 
                $messages->setErrorMessage('Sequence should be provided.');
            
             if ( !isset($data['perm_name']) || empty($data['perm_name']) ) 
                $messages->setErrorMessage('Permission name should be provided.');
            //check for same service title name
			/*$service_title = trim($data["ss_title"]);
			$ss_parent_id = trim($data["ss_parent_id"]);
            $sid = $data["ss_id"];
            if ( (Services::getList($db, $service, 'ss_title', " WHERE ss_title = '$service_title' AND ss_parent_id = '$ss_parent_id' AND ss_id !='$sid'")) > 0 ){
				$messages->setErrorMessage('Sub Service already exists. Please change the title.');
            }*/           
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$service = NULL;
            
            //check for same sub menu title name
			$menu_title = trim($data["title"]);
            
            if ( (Navigation::getList($db, $service, 'mTbl.title', "WHERE mTbl.title = '$menu_title'")) > 0 ){
				$messages->setErrorMessage('Service title name already exists. Please change the title.');
            }
            
			//validate the sub menu title 
            if ( !isset($data['title']) || empty($data['title']) ) 
                $messages->setErrorMessage('Menu title should be provided.');
			
            if ( !isset($data['page1']) || empty($data['page1']) ) 
                $messages->setErrorMessage('Page name should be provided.');
          
          	/*
            if ( !isset($data['selected_page']) || empty($data['selected_page']) ) 
                $messages->setErrorMessage('Selected Page name should be provided.');
            */
            
            if ( !isset($data['sequence']) || empty($data['sequence']) ) 
                $messages->setErrorMessage('Sequence should be provided.');
            
            /*
            if ( !isset($data['perm_name']) || empty($data['perm_name']) ) 
                $messages->setErrorMessage('Permission name should be provided.');
			*/
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
			//check for same menu title name
			
            $menu_title = trim($data["title"]);
			$sid = $data["id"];
            if ( (Navigation::getList($db, $service, 'mTbl.title', " WHERE mTbl.title = '$menu_title' && mTbl.id !='$sid'")) > 0 ){
				$messages->setErrorMessage('Menu  title name already exists. Please change the title.');
            }
            
			
			//validate the menu title 
            if ( !isset($data['title']) || empty($data['title']) ) 
                $messages->setErrorMessage('Menu title should be provided.');
			
            if ( !isset($data['page1']) || empty($data['page1']) ) 
                $messages->setErrorMessage('Page name should be provided.');
          
          	/*if ( !isset($data['selected_page']) || empty($data['selected_page']) ) 
                $messages->setErrorMessage('Selected Page name should be provided.');
            */  
            if ( !isset($data['sequence']) || empty($data['sequence']) ) 
                $messages->setErrorMessage('Sequence should be provided.');
            
             /*if ( !isset($data['perm_name']) || empty($data['perm_name']) ) 
                $messages->setErrorMessage('Permission name should be provided.');     
               */       
            
  
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
        function getParent(&$db,&$menu,$id='0')
		{
			$sql = "SELECT ".TABLE_NAVIGATION.".id,".TABLE_NAVIGATION.".title FROM ".TABLE_NAVIGATION." WHERE parent_id='".$id."'";
			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
					$menu[] = processSqlData($db->Record);                  
				return ( $db->nf() );
			}
			else 
				return false;			
		}
        
        //Function to create the menu 
        function getMenu($curr_page, $query_str='',&$perm) {
            $navigation_list=array();
            
            $db 		= new db_local; // database handle
            $db0 		= new db_local; // database handle
			$db1 		= new db_local; // database handle
            $sql= "SELECT * FROM ".TABLE_NAVIGATION." WHERE  parent_id='0' AND status='1' ORDER BY sequence ";
            $db->query($sql);
			if($db->nf() > 0){
				
				while($db->next_record()){
                    $id = $db->f('id');
                    $parent_id = $db->f('parent_id');
                    $title =$db->f('title');
                    $page =$db->f('page');
                    $image =$db->f('image');
                    $ajax =$db->f('ajax');
                    $selected_page =$db->f('selected_page');
                    $perm_name =$db->f('perm_name');
                    $main_perm_name =$db->f('main_perm_name');
                    $main_menu =array();
					
                    if ( $perm->has($perm_name) || $perm_name=='no_perm' || empty($perm_name)){
						$sql2 = "SELECT * FROM ".TABLE_NAVIGATION." WHERE  parent_id='".$id."' AND status='1' ORDER BY sequence ";
						$db0->query($sql2);
						$selectArr=array();
						$sub_menu0=array();
						while($db0->next_record()){
							$id0 = $db0->f('id');
							$parent_id0 = $db0->f('parent_id');
							$title0 =$db0->f('title');
							$page0 = $db0->f('page');
							$image0 =$db0->f('image');
							$ajax0 = $db0->f('ajax');
							$selected_page0 = $db0->f('selected_page');
							$perm_name0 = $db0->f('perm_name');
							$main_perm_name0 = $db0->f('main_perm_name');
							if ( $perm->has($perm_name0) || $perm_name0=='no_perm' || empty($perm_name0)){ 
								$sql2 = "SELECT * FROM ".TABLE_NAVIGATION." WHERE  parent_id = '".$id0."' AND status='1' ORDER BY sequence ";
								$db1->query($sql2);
								$sub_menu1=$selectArr=array();
								while($db1->next_record()){
									
									$id1 =$db1->f('id');
									$parent_id1 = $db1->f('parent_id');
									$title1 =$db1->f('title');
									$page1 =$db1->f('page');
									$image1 =$db1->f('image');
									$selected_page1 =$db1->f('selected_page');
									$perm_name1 =$db1->f('perm_name');
									$ajax1 =$db1->f('ajax');
									$main_perm_name1 =$db1->f('main_perm_name');
									if ( $perm->has($perm_name1) && $perm->has($main_perm_name1)){
									  $selectArr[]=$selected_page1;
									  $sub_menu1[]=array(
													"id"		=> $id1,
													"parent_id" => "mn".$parent_id1,
													"title" 	=>$title1,
													"page"		=> $page1,
													"ajax"		=> $ajax1,
													"image"		=> "",
													"level"		=> "2",
													"is_selected"	=> ($curr_page ==  $selected_page1)? 1:0,
													"menu_sub" 	=>''
												);
									} 
								}  
							}
							if( ( $perm->has($perm_name0) && $perm->has($main_perm_name0) ) || !empty($sub_menu1)){ 
								$sel_page=array();
								if(!empty($selected_page)){
								   $selectArr[]= explode(",",$selected_page);
								}
								
								$page = $page =='#'? '' : $page;
								$sub_menu0[] = array(
									"id"			=> "mn".$id0,
									"parent_id" 	=> $parent_id0 ,
									"title" 		=> $title0 ,
									"page"		=> $page0,
									"ajax"		=> $ajax0,
									"image"		=> "",
									"level"		=> "1",
									"is_selected"	=> (in_array($curr_page,$selectArr))? 1:0,
									"menu_sub" 	=> $sub_menu1
									);
							} 
						} 
                    }
					 
					if(( $perm->has($perm_name) && $perm->has($main_perm_name) ) || !empty($sub_menu0) ){ //|| ($perm_name=='no_perm')
						$sel_page0=array();
						if(!empty($selected_page0)){
						   $selectArr0[]= explode(",",$selected_page0);
						} 
						$page = $page =='#'? '' : $page;
						$main_menu=array(
							"id"			=> "mn".$id,
							"parent_id" 	=> $parent_id ,
							"title" 		=> $title ,
							"page"		=> $page,
							"ajax"		=> $ajax,
							"image"		=> "",
							"level"		=> "0",
							"is_selected"	=> (in_array($curr_page,$selectArr0))? 1:0,
							"menu_sub" 	=> $sub_menu0
							);
							 
					}
					
                    if(!empty($main_menu)){
                            $navigation_list[]=$main_menu;
                    }
                }
			}
            return  $navigation_list;
        }
		
        function getNavigationMenus(&$db, &$menu_list, &$perm, $parent_id=0, $level=0){
			$temp = NULL;
            
			//if ( !empty($placement) ) { 
				$condition = "WHERE status = '1'"." AND parent_id ='$parent_id'"; 
                $condition .= " ORDER BY sequence, title";
				
				Navigation::getDetails($db, $menu_list, '*', $condition);

				if ( is_array($menu_list) && count($menu_list)>0 ) {
					foreach ($menu_list as $key=>$menu) {
						$temp = NULL;
						if ( $menu['parent_id'] == 0 ) {	
							$level = 0 ;
						}
						Navigation::getNavigationMenus($db, $temp, $perm, $menu['id'], ($level+1));
						$id1 =$menu_list[$key]['id'];
						$parent_id1 = $menu_list[$key]['parent_id'];
						$title1 =$menu_list[$key]['title'];
						$page1 =$menu_list[$key]['page'];
						$image1 =$menu_list[$key]['image'];
						$selected_page1 =$menu_list[$key]['selected_page'];
						$perm_name1 =$menu_list[$key]['perm_name'];
						$ajax1 =$menu_list[$key]['ajax'];
						$main_perm_name1 =$menu_list[$key]['main_perm_name'];
						if ( $perm->has($perm_name1) && $perm->has($main_perm_name1)){
						  $selectArr[]=$selected_page1;
						  /* $sub_menu[]=array(
										"id"			=> $id1,
										"parent_id" 	=> "mn".$parent_id1,
										"title" 		=>$title1,
										"page"		=> $page1,
										"ajax"		=> $ajax1,
										"image"		=> "",
										"level"		=> "1",
										"is_selected"	=> ($curr_page ==  $selected_page1)? 1:0,
										"menu_sub" 	=>''
									); */
							$sub_menu=array(
										"id"			=> $id1,
										"parent_id" 	=> "mn".$parent_id1,
										"title" 		=>$title1,
										"page"		=> $page1,
										"ajax"		=> $ajax1,
										"image"		=> "",
										"level"		=> "1",
										"is_selected"	=> ($curr_page ==  $selected_page1)? 1:0,
										"menu_sub" 	=>''
									);
						}  
						$menu_list[$key]['level'] = $level;
						$menu_list[$key]['sub_menu'] = $temp;
					}
				}
			 
			
			if ( is_array($menu_list) && count($menu_list)>0 ) {
				return true;
			}
			else {
				return false;
			}
		}
		
		
		function getComboMenus($menuid=0,$level = 0) {
			global $output;
			//global $lang;  
			$db_new 		= new db_local; // database handle
			$query1 	= "SELECT * FROM  ".TABLE_NAVIGATION." WHERE " .TABLE_NAVIGATION.".parent_id='$menuid' "." AND ".TABLE_NAVIGATION.".status='1' "." ORDER BY sequence";
			
			$db_new->query($query1);
	
			if($db_new->nf() ==0) return;
			if ($db_new->nf() > 0 ) {
				while($db_new->next_record()) {
					$id1 = $db_new->f('id');
					$level2 =  4;
					$output[$id1] =  str_repeat('&nbsp;', $level * 4).'&#187; '.Navigation::getMenuTitle($id1)." ";
					//$output[$id1] = Navigation::getMenuTitle($id1);
					$level1 = $level + 1 ;
					Navigation::getComboMenus($db_new->f('id'),$level1 );
				}
			}
			return $output;
		}
		
		
		function getMenuTitle($id) {
			$db 		= new db_local; // database handle
			$menu = NULL;
			Navigation::getDetails($db, $menu, 'title', " WHERE id = '$id' ");
		
			return ($menu[0]['title']);	
		}
    }
    
    /*eof for menu module*/
/*
if ($variables['address_book'] == '1'){  
			if ($variables['address_book_list'] == '1'){  
				$address_book_sub_menu[] = array(
					"id"			=> "sab1",
					"parent_id" 	=> "ab",
					"title" 		=> "List",
					"page"		=> "address-book.php?perform=list",
					"image"		=> "",
					"level"		=> "1",
					"is_selected"	=> ($curr_page == "address-book.php?perform=list")? 1:0,
					"menu_sub" 	=>''
				);
			}
			if ($variables['address_book_add'] == '1'){  
				$address_book_sub_menu[] = array(
                                        "id"			=> "sab2",
                                        "parent_id" 	=> "ab",
                                        "title" 		=> "Add",
                                        "page"		=> "address-book.php?perform=add",
					"image"		=> "",
                                        "level"		=> "1",
                                        "is_selected"	=> ($curr_page == "address-book.php?perform=add")? 1:0,
                                        "menu_sub" 	=>''
				);
			}
			if(!empty($address_book_sub_menu)){   
				$address_book_arr=array(
					"id"			=> "ab",
					"parent_id" 	=> "0",
					"title" 		=> "Address&nbsp;Book",
					"page"		=> "",
					"image"		=> "",
					"level"		=> "0",
					"is_selected"	=> (in_array($curr_page, array("address-book.php?perform=list","address-book.php?perform=add")))? 1:0,
					"menu_sub" 	=> $address_book_sub_menu
				);
			}	
		}	  
		if(!empty($address_book_arr)){
			$navigation_list[]=$address_book_arr;
		}

*/
?>
