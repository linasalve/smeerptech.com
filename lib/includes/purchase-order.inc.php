<?php
	class PurchaseOrder {
		
		const CANCELLED = 0;
		const ACTIVE  = 1;     
		const COMPLETED  = 2;     
		
        const BILL_STATUS_UNPAID = 0;
		const BILL_STATUS_PAID = 1;
		const BILL_STATUS_PARTIALPAID = 2;
		
        const BILL = 1;
		const BILL_OTHER = 2;
		const BILL_SALARY = 3;
		
		const FOREXLOSS = 1;
        const ADJUSTMENT = 2;
		
		const FORCEACTIVE = 1;
        const FORCEDEACTIVE = 0;
		
		
		
		
		function getFStatus(){
			$fstatus = array(
						PurchaseOrder::FORCEDEACTIVE=>'Force DeActive',
						PurchaseOrder::FORCEACTIVE=>'Force Active'
											
					);
			return ($fstatus);
		}
		
		
		function getType() {
			$type = array(
						PurchaseOrder::BILL=>'Bill',
						PurchaseOrder::BILL_OTHER=>'No Bill',
						
					);
			return ($type);
		}   
		function getStatus() {
			$status = array(
			    'ACTIVE'   => PurchaseOrder::ACTIVE,
                'COMPLETED'    => PurchaseOrder::COMPLETED,
                'CANCELLED'    => PurchaseOrder::CANCELLED,
						);
			return ($status);
		} 
		function getGLType() {
			$type = array(
							PurchaseOrder::FOREXLOSS=>'FOREX LOSS' ,
                           // PurchaseOrder::ADJUSTMENT=> 'AMOUNT ADJUSTMENT'
						);
			return ($type);
		}  		
		function getBillStatus() {
			$bstatus = array(
			    'UNPAID'   => PurchaseOrder::BILL_STATUS_UNPAID,
                'PAID'    => PurchaseOrder::BILL_STATUS_PAID,
                'PARTIALPAID'    => PurchaseOrder::BILL_STATUS_PARTIALPAID
			);
			return ($bstatus);
		}  
		function getSTypes(){
		
			$typeList= array('Qty'=>'Qty',
							 'Nos'=>'Nos',							 
							 'Yrs'=>'Yrs',							 
							 'Mth'=>'Mth',							 
							 'Ltrs'=>'Ltrs'							 
			);
			return $typeList;
		}
        function getNos(){
			$noList=array();
			for($i=1;$i<=11;$i++){
				$noList[]=$i;
			}
			return $noList;
		}
		function getRoundOffOp(){
			$arr= array('+','-');
			return $arr;
		}
		 function getRoundOff(){
			$roundOffList=array();
			
			for($i=0.00;$i< 1; $i = $i+0.01){
				$roundOffList[]=$i;
			}
			return $roundOffList;
		}
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			/* $query .= " FROM ". TABLE_PURCHASE_ORDER
            	    ." LEFT JOIN ". TABLE_VENDORS
                	." ON ". TABLE_PURCHASE_ORDER .".party_id = ". TABLE_VENDORS .".user_id " 
                    ." LEFT JOIN ". TABLE_USER
                		." ON (". TABLE_USER .".user_id = ". TABLE_PURCHASE_ORDER .".executive_id ) "
                    ." LEFT JOIN ". TABLE_PAYMENT_BILLS_AGAINST
                    ." ON ". TABLE_PAYMENT_BILLS_AGAINST .".id = ". TABLE_PURCHASE_ORDER .".bill_against_id "; */
			
            $query .= " FROM ". TABLE_PURCHASE_ORDER
            	     ." LEFT JOIN ". TABLE_CLIENTS
                	." ON ". TABLE_PURCHASE_ORDER .".party_id = ". TABLE_CLIENTS .".user_id " 
					." LEFT JOIN ". TABLE_VENDORS_BANK
                	." ON ". TABLE_PURCHASE_ORDER .".vendor_bank_id = ". TABLE_VENDORS_BANK .".user_id " 
                   ." LEFT JOIN ". TABLE_USER
                	." ON (". TABLE_USER .".user_id = ". TABLE_PURCHASE_ORDER .".executive_id ) "
					." LEFT JOIN ". TABLE_USER." as team_table"
                	." ON (team_table.user_id = ". TABLE_PURCHASE_ORDER .".team_id ) "
                    ." LEFT JOIN ". TABLE_PAYMENT_BILLS_AGAINST
                    ." ON ". TABLE_PAYMENT_BILLS_AGAINST .".id = ". TABLE_PURCHASE_ORDER .".bill_against_id "  ;
            $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        function getNewNumber(&$db){
            $number = 100;
            $query      = "SELECT MAX(number) AS number FROM ". TABLE_PURCHASE_ORDER;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record()){
                 $number1 =  $db->f("number");
                  $number1 = (int) $number1 ;
                if($number1 > 0){
                    $number = $number1 + 1;
                }else{
                    $number++;
                }
                
            }
            return $number;
        }
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PURCHASE_ORDER;
            	  
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
           
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;            
			$query = "DELETE FROM ". TABLE_PURCHASE_ORDER
						." WHERE id = '$id'";
			if ( $db->query($query) && $db->affected_rows()>0 ) {
				$messages->setOkMessage("The Record has been deleted."); 
				$sql="DELETE FROM ". TABLE_PURCHASE_ORDER_P
						." WHERE bill_id = '$id'";;
				$db->query($sql) ;
			}
			else {
				$messages->setErrorMessage("The Record was not deleted.");
			}
		            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
	function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( empty($data['bill_dt']) ) {
                $messages->setErrorMessage("Select PO date.");
            }else {
                $data['bill_dt1'] = explode('/', $data['bill_dt']);                
                $data['bill_dt1'] = mktime(0, 0, 0, $data['bill_dt1'][1], $data['bill_dt1'][0], $data['bill_dt1'][2]);
            }
			if ( empty($data['company_id']) ) {
                $messages->setErrorMessage("Select company.");
            }
			$data['number']=$data['counter'] ='';
			if(!empty($data['company_id']) && !empty($data['bill_dt'])){				
				$detailNo = getPoNumber($data['bill_dt1'],$data['company_id']); 				
				if(!empty($detailNo)){
					$data['number'] = $detailNo['number'];
					$data['counter'] = $detailNo['counter'];
				}
			}
			if(empty($data['number'])){
			
				$messages->setErrorMessage("PO number is not generated.");
			}
			//getPoNumber  
            if ( empty($data['party_id']) && empty($data['vendor_bank_id']) ) {
                $messages->setErrorMessage("Please select client/vendor.");                
            }
			 if( !isset($data['bill_details']) || empty($data['bill_details']) ) {
                $messages->setErrorMessage("PO Remarks can not be empty.");
            }  
			  
			if(isset($data['particulars'])){
                
				$data['alltax_ids']= array();
                foreach ( $data['particulars'] as $key=>$particular ) {
				
					$data['sub_sid_list'][$key]= $data['subsid'][$key]=$ss_sub_id_details=array();					
					if(!empty($data['s_id'][$key])){
						$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id
						= ".$data['s_id'][$key]." 
						AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
						$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = 
						".$data['currency_id']." AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;

						$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
						TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
						$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",
						".TABLE_SETTINGS_SERVICES_PRICE." ".$condition_query_sid ; 
						 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

						if ( $db->query($query) ) {
							if ( $db->nf() > 0 ) {
								while ($db->next_record()) {
									$ss_subtitle =  trim($db->f("ss_title")).",";
									$ss_subprice =  $db->f("ss_price") ;
									$ss_subid =  $db->f("ss_id") ;
									$ss_sub_id_details[] = array(
										'ss_title'=>$ss_subtitle,
										'ss_price'=>$ss_subprice,
										'ss_id'=>$ss_subid	
									);
								}
							}
						} 
				    }
				    $data['sub_sid_list'][$key]=$ss_sub_id_details;				   
				    $sub_s_id_str='';
				    $sub_s_id_str =trim($data['subsidstr'][$key],",");
				    if(!empty($sub_s_id_str)){
						$data['subsid'][$key] = explode(",",$sub_s_id_str);
						$sub_s_id_str=",".$sub_s_id_str.",";
				    }
				   
					$tax1_id_str = $data['tax1_id'][$key] ;
					$tax1_sub1_id_str = $data['tax1_sub1_id'][$key] ;
					$tax1_sub2_id_str = $data['tax1_sub2_id'][$key] ;
					 
					if(!empty($tax1_id_str)){
						$arr = explode("#",$tax1_id_str);
						$data['tax1_name_str'][$key] = $arr[0] ;
						$data['tax1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_name_str'][$key] = 0 ;		
						$data['tax1_id_check'][$key] = 0 ;		
					}					
					if(!empty($data['tax1_value'][$key])){
						$tax1_val = $data['tax1_value'][$key] ;
						$percent =   substr($tax1_val,0,-1);
						$data['tax1_pvalue'][$key] = (float) ($percent/100);
					}else{
						$data['tax1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub1_value'][$key])){
						$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
						$percent_sub1 =   substr($tax1_sub1_val,0,-1);
						$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
					}else{
						$data['tax1_sub1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub2_value'][$key])){
						$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
						$percent_sub2 =   substr($tax1_sub2_val,0,-1);
						$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
					}else{
						$data['tax1_sub2_pvalue'][$key] =0;
					}
					
				    $data['tax_check_ids'][] = $data['tax1_id_check'][$key] ;
					$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					
					//$data['tax_check_ids'][] = $data['tax1_id'][$key] ;
					//$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					$data['tax1_sid_opt'][$key]=array();
					$data['tax1_sid_opt_count'][$key]=0;
					
					if($data['tax1_id_check'][$key]>0){
						//tax1_sub_id Options of taxes 
						//$data['tax1_sid_opt'][$key] = array('0'=>2);
						$tax_opt = array();
						$tax_id = $data['tax1_id_check'][$key];
						$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
						ORDER BY tax_name ASC ";
						ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
						if(!empty($tax_opt)){
							$data['tax1_sid_opt'][$key] = $tax_opt ;  
							$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
						}
					}
					if(!empty($tax1_sub1_id_str)){
						$arr = explode("#",$tax1_sub1_id_str);
						$data['tax1_sub1_name_str'][$key] = $arr[0] ;
						$data['tax1_sub1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub1_name_str'][$key] = 0 ;		
						$data['tax1_sub1_id_check'][$key] = 0 ;		
					}
					if(!empty($tax1_sub2_id_str)){
						$arr = explode("#",$tax1_sub2_id_str);
						$data['tax1_sub2_name_str'][$key] = $arr[0] ;
						$data['tax1_sub2_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub2_name_str'][$key] = 0 ;		
						$data['tax1_sub2_id_check'][$key] = 0 ;		
					}
					
					if(!empty($data['tax1_id_check'][$key])){
						$data['alltax_ids'][]=$data['tax1_id_check'][$key] ;	
					}		
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub1_id_check'][$key] ;
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub2_id_check'][$key] ;
					}
					 
					
					if(empty($data['s_id'][$key])){
						$messages->setErrorMessage('Select the services.');
					}
                    				 	
					
                    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                     
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
                               
                            }
                        }
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
			
			 
			$data['alltax_ids'] = array_unique($data['alltax_ids']);
			
			
           
			if(!empty($data['bill_amount'])){
                if(!is_numeric($data['bill_amount'])){
                    $messages->setErrorMessage("Total Amount should be numeric value.");
                }
            }else{
				$messages->setErrorMessage("Total Amount should not be blank.");
			}
			
           
            /* 
			$data['bill_attachment'] = '';  		
            if((!empty($files['bill_attachment']) && (!empty($files['bill_attachment']['name'])))){
                
				$filename = $files['bill_attachment']['name'];
                $data['bill_attachment'] = $filename;
                $type = $files['bill_attachment']['type'];
                 $size = $files['bill_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 300Kb');
                }
            }  
			*/   
            if ( !isset($data['order_title_mail']) || empty($data['order_title_mail']) ) {
                $messages->setErrorMessage("PO title can not be empty.");
            }  
			if(empty($data['team_details'])){
			   $messages->setErrorMessage("Select Team.");
			}
			if(empty($data['clients_su_details'])){
			   $messages->setErrorMessage("Select client - sub users to whome we want to send the mails.");
			} 
                  
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			} else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            /*  if ( empty($data['bill_dt']) ) {
                $messages->setErrorMessage("Select Purchase order date.");
            } */
            if ( empty($data['party_id']) && empty($data['vendor_bank_id']) ) {
                $messages->setErrorMessage("Please select Member or Account vendors.");                
            }
			           
                   
			if ( !isset($data['bill_details']) || empty($data['bill_details']) ) {
                $messages->setErrorMessage("PO Remarks can not be empty.");
            }  
			
			
			if(isset($data['particulars'])){
                
				$data['alltax_ids']= array();
                foreach ( $data['particulars'] as $key=>$particular ) {
				
					$data['sub_sid_list'][$key]= $data['subsid'][$key]=$ss_sub_id_details=array();					
					if(!empty($data['s_id'][$key])){
						$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id
						= ".$data['s_id'][$key]." 
						AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
						$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = 
						".$data['currency_id']." AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;

						$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
						TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
						$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",
						".TABLE_SETTINGS_SERVICES_PRICE." ".$condition_query_sid ; 
						 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

						if ( $db->query($query) ) {
							if ( $db->nf() > 0 ) {
								while ($db->next_record()) {
									$ss_subtitle =  trim($db->f("ss_title")).",";
									$ss_subprice =  $db->f("ss_price") ;
									$ss_subid =  $db->f("ss_id") ;
									$ss_sub_id_details[] = array(
										'ss_title'=>$ss_subtitle,
										'ss_price'=>$ss_subprice,
										'ss_id'=>$ss_subid	
									);
								}
							}
						} 
				    }
				    $data['sub_sid_list'][$key]=$ss_sub_id_details;				   
				    $sub_s_id_str='';
				    $sub_s_id_str =trim($data['subsidstr'][$key],",");
				    if(!empty($sub_s_id_str)){
						$data['subsid'][$key] = explode(",",$sub_s_id_str);
						$sub_s_id_str=",".$sub_s_id_str.",";
				    }
				   
					$tax1_id_str = $data['tax1_id'][$key] ;
					$tax1_sub1_id_str = $data['tax1_sub1_id'][$key] ;
					$tax1_sub2_id_str = $data['tax1_sub2_id'][$key] ;
					 
					if(!empty($tax1_id_str)){
						$arr = explode("#",$tax1_id_str);
						$data['tax1_name_str'][$key] = $arr[0] ;
						$data['tax1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_name_str'][$key] = 0 ;		
						$data['tax1_id_check'][$key] = 0 ;		
					}					
					if(!empty($data['tax1_value'][$key])){
						$tax1_val = $data['tax1_value'][$key] ;
						$percent =   substr($tax1_val,0,-1);
						$data['tax1_pvalue'][$key] = (float) ($percent/100);
					}else{
						$data['tax1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub1_value'][$key])){
						$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
						$percent_sub1 =   substr($tax1_sub1_val,0,-1);
						$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
					}else{
						$data['tax1_sub1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub2_value'][$key])){
						$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
						$percent_sub2 =   substr($tax1_sub2_val,0,-1);
						$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
					}else{
						$data['tax1_sub2_pvalue'][$key] =0;
					}
					
				    $data['tax_check_ids'][] = $data['tax1_id_check'][$key] ;
					$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					
					//$data['tax_check_ids'][] = $data['tax1_id'][$key] ;
					//$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					$data['tax1_sid_opt'][$key]=array();
					$data['tax1_sid_opt_count'][$key]=0;
					
					if($data['tax1_id_check'][$key]>0){
						//tax1_sub_id Options of taxes 
						//$data['tax1_sid_opt'][$key] = array('0'=>2);
						$tax_opt = array();
						$tax_id = $data['tax1_id_check'][$key];
						$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
						ORDER BY tax_name ASC ";
						ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
						if(!empty($tax_opt)){
							$data['tax1_sid_opt'][$key] = $tax_opt ;  
							$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
						}
					}
					if(!empty($tax1_sub1_id_str)){
						$arr = explode("#",$tax1_sub1_id_str);
						$data['tax1_sub1_name_str'][$key] = $arr[0] ;
						$data['tax1_sub1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub1_name_str'][$key] = 0 ;		
						$data['tax1_sub1_id_check'][$key] = 0 ;		
					}
					if(!empty($tax1_sub2_id_str)){
						$arr = explode("#",$tax1_sub2_id_str);
						$data['tax1_sub2_name_str'][$key] = $arr[0] ;
						$data['tax1_sub2_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub2_name_str'][$key] = 0 ;		
						$data['tax1_sub2_id_check'][$key] = 0 ;		
					}
					
					if(!empty($data['tax1_id_check'][$key])){
						$data['alltax_ids'][]=$data['tax1_id_check'][$key] ;	
					}		
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub1_id_check'][$key] ;
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub2_id_check'][$key] ;
					}
					 
					
					if(empty($data['s_id'][$key])){
						$messages->setErrorMessage('Select the services.');
					}
                    				 	
					
                    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                     
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
                                
                            }
                        }
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
			
			 
			$data['alltax_ids']=array_unique($data['alltax_ids']);
			             
			if ( !isset($data['order_title_mail']) || empty($data['order_title_mail']) ) {
                $messages->setErrorMessage("PO title can not be empty.");
            }  
			if(empty($data['team_details'])){
			   $messages->setErrorMessage("Select Team Members.");
			}
			if(empty($data['clients_su_details'])){
			   $messages->setErrorMessage("Select Vendors to whome we want to send the mails.");
			} 
			if(!empty($data['bill_amount'])){
                if(!is_numeric($data['bill_amount'])){
                    $messages->setErrorMessage("Amount should be numeric value.");
                }
            }else{
				$messages->setErrorMessage("Amount should not be blank.");
			}
    		 
            /* 
			if((!empty($files['bill_attachment']) && (!empty($files['bill_attachment']['name'])))){                
				$filename = $files['bill_attachment']['name'];
              
                $type = $files['bill_attachment']['type'];
                $size = $files['bill_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
				 
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 300Kb');
                }
            }   */
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                 $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PURCHASE_ORDER_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
            /**
         * This function is used to update the status of the Executive.
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, PurchaseOrder::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid Status.");
			}
			else {
                $user = NULL;
                if ( (PurchaseOrder::getDetails($db, $data, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
                    $data = $data[0];
                   // if ( $user['access_level'] < $access_level ) {
                      $query = "UPDATE ". TABLE_PURCHASE_ORDER
                                    ." SET status = '$status_new'"
                                    ." WHERE id = '$id'";
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            $messages->setOkMessage("The Status has been updated.");
                        }
                        else {
                            $messages->setErrorMessage("The Status was not updated.");
                        }
                    /*}
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of Executives with the current Access Level.");
                    }*/
                }
                else {
                    $messages->setErrorMessage("Record was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

		
    }
?>
