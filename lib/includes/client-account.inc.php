<?php 

	class ClientAccount {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => ClientAccount::BLOCKED,
							'ACTIVE'  => ClientAccount::ACTIVE,
							'PENDING' => ClientAccount::PENDING,
							'DELETED' => ClientAccount::DELETED
						);
			return ($status);
		}
		
        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_CLIENT_ACC;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, ClientAccount::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (ClientAccount::getList($db, $user, 'ca_id', " WHERE ca_id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_SMS_CLIENT_ACC
								." SET ca_status = '$status_new' "
								." WHERE ca_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Account was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (ClientAccount::getList($db, $user, 'ss_id', " WHERE ss_id = '$id'")) > 0 ) 
			{
                $user = $user[0];
                if ( $user['status'] == ClientAccount::BLOCKED || $user['status'] == ClientAccount::DELETED) 
				{
					$query = "DELETE FROM ". TABLE_SETTINGS_SERVICES
								." WHERE ss_id = '". $id ."'";
					if ( !$db->query($query) || $db->affected_rows()<=0 ) 
						$messages->setErrorMessage("The Service is not deleted.");
					else
						$messages->setOkMessage("The Service has been deleted.");
				}
				else
					$messages->setErrorMessage("Cannot delete service.");
			}
			else
				$messages->setErrorMessage("Service not found.");
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			if(empty($data['userid']))
				$messages->setErrorMessage('Please enter userid.');        
			if(empty($data['username']))
				$messages->setErrorMessage('Please enter username.');
			if(empty($data['displayname']))
				$messages->setErrorMessage('Please enter displayname.');
			if(!empty($data['displayname']) && preg_match("/\s/",$data['displayname']))
				$messages->setErrorMessage('Spaces are not allowed in display name.');	
			if(empty($data['pass']))
				$messages->setErrorMessage('Please enter password.');
			if(empty($data['allotedsms']))
				$messages->setErrorMessage('Please enter total no of alloted sms.');
			if(empty($data['amount']))
				$messages->setErrorMessage('Please enter amount of sms package.');
			if(empty($data['persmsrate']))
				$messages->setErrorMessage('Please enter per sms rate.');
			if(empty($data['posturl']))
				$messages->setErrorMessage('Please enter post url.');
			if (!empty($data['posturl']) && !preg_match('|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $data['posturl']))
				$messages->setErrorMessage('Invalid url.');
			if(empty($data['expire_date']))
				$messages->setErrorMessage('Please enter the expiry date of the package.');

			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			if(empty($data['ca_username']))
				$messages->setErrorMessage('Please enter username.');
			if(empty($data['ca_displayname']))
				$messages->setErrorMessage('Please enter displayname.');
			if(!empty($data['ca_displayname']) && preg_match("/\s/",$data['ca_displayname']))
				$messages->setErrorMessage('Spaces are not allowed in display name.');	
			if(empty($data['ca_posturl']))
				$messages->setErrorMessage('Please enter post url.');
			if (!empty($data['ca_posturl']) && !preg_match('|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $data['ca_posturl']))
				$messages->setErrorMessage('Invalid url.');

            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}

		function viewList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_CLIENT_ACC_HISTORY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}

		function showUsers(&$userlist)
		{
			$db = new db_local;
			$query = "SELECT ".TABLE_USER .".user_id, concat(".TABLE_USER .".f_name,' ',".TABLE_USER .".l_name) as name,email "
					."FROM ". TABLE_USER ." WHERE ".TABLE_USER.".status='1' AND ".TABLE_USER.".user_id NOT IN (select ca_id FROM ".TABLE_SMS_CLIENT_ACC.") ORDER BY ".TABLE_USER.".f_name";

			$db->query($query);
			while($db->next_record())
			{
				$userlist[] = $db->Record;	
			}
		}
    }
?>