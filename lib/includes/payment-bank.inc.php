<?php
	class Paymentbank {
    
		const GLOBALBANK = 0;
        const INHOUSEBANK = 1;
		
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PAYMENT_BANK;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
             $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PAYMENT_BANK;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
               return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details1 = $details2 =$details3 =NULL;
           
        
            if( (Paymenttransaction::getList($db, $details1, 'id', " WHERE  credit_pay_bank_id = '$id'")==0 ) && (Paymenttransaction::getList($db, $details2, 'id', " WHERE  debit_pay_bank_id = '$id'") ==0  ) && (Receipt::getList($db, $details3, 'id', " WHERE 	pay_bank_company = ".$id."") == 0  ) ) {
            
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_PAYMENT_BANK
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            }
            else {
                $messages->setErrorMessage("Cannot Delete Record As Record may be exist in Payment Transaction or Receipt with Selected Bank.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
            
            
            if ( !isset($data['bank_name']) || empty($data['bank_name']) ) {
                $messages->setErrorMessage("Bank name can not be empty.");
            }else{
            
                $list = NULL;
                if ( Paymentbank::getList($db, $list, 'id', " WHERE bank_name = '". $data['bank_name'] ."' AND type='".$data['type']."'")>0 ) {
                    $messages->setErrorMessage("Bank name already exist.");
                }
            }
            
            if($data['type']== Paymentbank::INHOUSEBANK){
                if ( !isset($data['opening_balance'])  ) {
                    $messages->setErrorMessage("Opening balance can not be empty.");
                }
				if( empty($data['company_id'])  ) {
                    $messages->setErrorMessage("Select company.");
                }
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
                   
            if ( !isset($data['bank_name']) || empty($data['bank_name']) ) {
                $messages->setErrorMessage("Bank name can not be empty.");
            }else{
            
                $list = NULL;
                if ( Paymentbank::getList($db, $list, 'id', " WHERE bank_name = '". $data['bank_name'] ."' AND type='".$data['type']."' AND id !='".$data['id']."'")>0 ) {
                    $messages->setErrorMessage("Bank name already exist.");
                }

            }
            /*
            if ( !isset($data['bank_address']) || empty($data['bank_address']) ) {
                $messages->setErrorMessage("Bank address can not be empty.");
            }*/
           if($data['type']== Paymentbank::INHOUSEBANK){
                if ( !isset($data['opening_balance'])) {
                    $messages->setErrorMessage("Opening balance can not be empty.");
                }
				if( empty($data['company_id'])  ) {
                    $messages->setErrorMessage("Select company.");
                }
            }
                   
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".status = '1'"
                            	 

                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Bank has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Bank has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".status = '0'"
                            	 

                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Bank has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Bank has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    
        
		
    }
?>
