<?php 

	class FireQuery {
		
        const CLOSED = 2;
		/*const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => Services::BLOCKED,
							'ACTIVE'  => Services::ACTIVE,
							'PENDING' => Services::PENDING,
							'DELETED' => Services::DELETED
						);
			return ($status);
		}*/
		
        /*function getDomains(&$db, &$domains, $ticket_owner_id) {
        
            $query= "SELECT DISTINCT (order_domain) FROM ". TABLE_BILL_ORDERS 
					." WHERE ". TABLE_BILL_ORDERS .".client='".$ticket_owner_id."' AND ".TABLE_BILL_ORDERS.".order_domain!='' ";
                       
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$domains[]= $db->f('order_domain');
						//$domains[]= processSqlData($db->result());
					}
				}	                
				return ( $db->nf() );
			}
			else {
				return false;
			}
			
		}*/
        
        function getDepartments( &$db, &$department){
            include_once ( DIR_FS_INCLUDES .'/fq-department.inc.php' );    
            $query= "SELECT * FROM ". TABLE_FQ_DEPARTMENT 
					." WHERE ". TABLE_FQ_DEPARTMENT .".status='".FqDepartment::ACTIVE."' ";
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$department[]= processSqlData($db->result());
					}
				}	                
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        
        function getStatus( &$db, &$status){			
            include_once ( DIR_FS_INCLUDES .'/fq-status.inc.php' );
            $query= "SELECT * FROM ". TABLE_FQ_STATUS 
					." WHERE ". TABLE_FQ_STATUS .".status_status='".FqStatus::ACTIVE."' ";
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$status[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        
        function getPriority( &$db, &$priority){			
            include_once ( DIR_FS_INCLUDES .'/fq-priority.inc.php' );
            $query= "SELECT * FROM ". TABLE_FQ_PRIORITY 
					." WHERE ". TABLE_FQ_PRIORITY .".priority_status='".FqPriority::ACTIVE."' " 
					." ORDER BY ". TABLE_FQ_PRIORITY .".priority_order ASC ";
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$priority[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        
		/**
		 *	Function to get all the support tickets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		
        function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_FQ_TICKETS;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        function getAllotedClientList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_FQ_ALLOTED_CLIENT;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        /*function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Services::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Services::getList($db, $user, 'ss_id', " WHERE ss_id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_SETTINGS_SERVICES
								." SET ss_status = '$status_new' "
								." WHERE ss_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Service was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }*/
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        /*function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (Services::getList($db, $user, 'ss_id', " WHERE ss_id = '$id'")) > 0 ) 
			{
                $user = $user[0];
                if ( $user['status'] == Services::BLOCKED || $user['status'] == Services::DELETED) 
				{
					 $query = "DELETE FROM ". TABLE_SETTINGS_SERVICES
								." WHERE ss_id = '". $id ."'";
                    
					if ( !$db->query($query) || $db->affected_rows()<=0 ){
                        
						$messages->setErrorMessage("The Service is not deleted.");
					}else{
	                    $sql ="DELETE FROM ".TABLE_SETTINGS_SERVICES_PRICE." WHERE service_id='".$id."'";
                        $db->query($sql) ;
                        
                        $messages->setOkMessage("The Service has been deleted.");
                    }
				}
				else
					$messages->setErrorMessage("Cannot delete service.");
			}
			else
				$messages->setErrorMessage("Service not found.");
		}*/
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            $files = $_FILES;
            
			if ( !isset($data['ticket_subject']) || empty($data['ticket_subject']) ) 
                $messages->setErrorMessage('Subject cannot be empty.');
                
            /*if ( !isset($data['domain_name']) || empty($data['domain_name']) ) 
                $messages->setErrorMessage('Domain name cannot be empty.');*/
            
            $data['ticket_attachment'] = '';
            
            if((!empty($files['ticket_attachment']) && (!empty($files['ticket_attachment']['name'])))){
                $filename = $files['ticket_attachment']['name'];
                $data['ticket_attachment'] = $filename;
                $type = $files['ticket_attachment']['type'];
                $size = $files['ticket_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 1Mb');
                }
            }
            
            $current_date = date('Y-m-d');
            $temp = explode('-', $current_date);
            $current_date_str = $temp[0].$temp[1].$temp[2];
            
            $balance_fq  = NULL;
            $condition  = " WHERE client_id='".$data['ticket_owner_uid']."' AND balance_fq > 0 AND do_expiry >= ".$current_date_str;

            if ( (FireQuery::getAllotedClientList($db, $balance_fq, 'id', $condition)) > 0 ) {
                foreach ( $balance_fq as $fq) {
                    //$messages->setErrorMessage('This Client and Account pair is alredy exists. Please select another.');
                }
            }else{
                $messages->setErrorMessage('Either you dont have any fire query balance or they are expired.');
            }
            
            /*$fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                    
                   if(!empty($val["name"]) ){
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of file is greater than 2Mb');
            }*/
            
            /*if ( $_FILES["ticket_attachment"]["name"] != "") {
                if ( $_FILES["ticket_attachment"]["error"] == "0") {
                    if($_FILES["ticket_attachment"]["size"] > (MAX_FILE_SIZE*1024)){
                        $error["size_limit"] = 1 ;
                    }
                    
                    if ( !in_array($_FILES["ticket_attachment"]["type"], $allowed_file_types) ) {
                        $error["invalid_attach"] = 1 ;
                    }
                    
                    $ticket_attachment  = $ticket_no ."_". $_FILES["ticket_attachment"]["name"] ;
                    $ticket_attachment_path = FILE_SAVE_DOMAIN."/attachments" ;
                
                    if ( empty($error) && $error["invalid_attach"] == 0){
                        if (!copy($_FILES['ticket_attachment']['tmp_name'], FILE_SAVE_PATH."/attachments/".$ticket_attachment)){
                            $error["cannot_attach"] = 1 ;
                            $ticket_attachment = "" ;
                        }
                    }
                }
                else {
                    $error["cannot_upload"] = 1 ;
                }
            }*/
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        /*function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the service title 
            if ( !isset($data['ss_title']) || empty($data['ss_title']) ) 
                $messages->setErrorMessage('Service title should be provided.');
            
            if ( !isset($data['domain_name']) || empty($data['domain_name']) ) 
                $messages->setErrorMessage('Domain name cannot be empty.');
            
            //check for same service title name
			$service_title = $data["ss_title"];
			$sid = $data["ss_id"];
            
            if ( (Services::getList($db, $service, 'ss_title', " WHERE ss_title = '$service_title' && ss_id !='$sid'")) > 0 )
				$messages->setErrorMessage('Service title name already exists. Please change the title.');
            
            //code to check price values BOf 
            
            if(!empty($data['ss_parent_id'])){            
               for ( $i=0; $i < count($data['service_price']); $i++ ) {                        
                    if ( empty($data['service_price'][$i]) ) {
                        $messages->setErrorMessage('The Price field at Position '. ($i+1) .' cannot be empty.');
                    }                   
                    else {
                        if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['service_price'][$i]) ) {
                            $messages->setErrorMessage('The Price field at Position '. ($i+1) .' is not valid.');
                        }
                    }
               }        
            }   
            
            //code to check price values eOf       
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
        function getParent(&$db,&$services,$id='0')
		{
			$sql = "SELECT ".TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title FROM ".TABLE_SETTINGS_SERVICES." WHERE ss_parent_id='".$id."'";
			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
					$services[] = processSqlData($db->Record);
                    //$title = processSqlData($db->f('ss_title')) ;
                   // $ss_id = $db->f('ss_id') ;
					//$services[$ss_id] = $title ;

				return ( $db->nf() );
			}
			else 
				return false;			
		}
        
        function getPriceList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp=''){
            $query = "SELECT DISTINCT ";
			
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) ";
			}
			
			$query .= " FROM ". TABLE_SETTINGS_SERVICES_PRICE;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
					}
				}
				
				return ( $db->nf() );
			}
			else {
				return false;
			}	
        
        }*/
        
		function validateMemberSelect(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			if ( !isset($data['ticket_owner_uid']) || empty($data['ticket_owner_uid']) ) 
                $messages->setErrorMessage('Plz select the member.');
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
        function validateAssign(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the members
            if ( !isset($data['assign_members']) || empty($data['assign_members']) ) 
                $messages->setErrorMessage('Plz select the members.');
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
    }
?>