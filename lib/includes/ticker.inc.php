<?php 

	class Ticker {
		
		const BLOCKED 	= 0;
		const ACTIVE  	= 1;
		const PENDING 	= 2;
		const DELETED 	= 3;
		
		const T_STATIC 	= 'S';
		const T_DYNAMIC = 'D';
		const T_EXTERNAL= 'E';
		const T_PLAIN 	= 'P';
		
		const TOP 		= 'HT';
		const RIGHT  	= 'VR';
		const BOTTOM 	= 'HB';
		const LEFT 		= 'VL';

		function getStatus() {
			$status = array('BLOCKED' => Ticker::BLOCKED,
							'ACTIVE'  => Ticker::ACTIVE,
							'PENDING' => Ticker::PENDING,
							'DELETED' => Ticker::DELETED
						);
			return ($status);
		}
		
		/**
		 * This function returns the type of the Ticker linkage.
		 * STATIC 	- if the Ticker is linked to a myCraft Page.
		 * DYNAMIC	- if the Ticker is linked to a Dynamic Page.
		 * EXTERNAL	- if the Ticker is linked to a External Link outside the current domain.
		 * PLAIN	- if the Ticker is not linked to any page. This is simply a text without any linkage.
		 *
		 */
		function getType() {
			$type = array(	Ticker::T_PLAIN 	=> 'Plain Text',
							Ticker::T_STATIC 	=> 'myCraft Pages',
							Ticker::T_DYNAMIC 	=> 'Dynamic Pages',
							Ticker::T_EXTERNAL	=> 'External Pages'
						);
			return ($type);
		}
		
		
		/**
		 * This function returns the type of the Ticker linkage.
		 * TOP 		- if the Ticker is to be displayed on the top position.
		 * BOTTOM	- if the Ticker is to be displayed at the bottom positon.
		 * LEFT		- if the Ticker is to be displayed on the left coloumn.
		 * RIGHT	- if the Ticker is to be displayed on the right coloumn.
		 *
		 */
		function getPlacement() {
			$placement = array(	Ticker::TOP 	=> 'Horizontal Top',
								Ticker::BOTTOM	=> 'Horizontal Bottom',
								Ticker::LEFT 	=> 'Vertical Left',
								Ticker::RIGHT 	=> 'Vertical Right'
						);
			return ($placement);
		}

        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TICKERS;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				 return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Ticker::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Ticker::getList($db, $user, 'id', " WHERE id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_TICKERS
								." SET status = '$status_new' "
								." WHERE id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Ticker was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (Ticker::getList($db, $user, 'id', " WHERE id = '$id'")) > 0 ) 
			{
                $user = $user[0];
                if ( $user['status'] == Ticker::BLOCKED || $user['status'] == Ticker::DELETED) 
				{
					$query = "DELETE FROM ". TABLE_TICKERS
								." WHERE id = '". $id ."'";
					if ( !$db->query($query) || $db->affected_rows()<=0 ) 
						$messages->setErrorMessage("The Ticker is not deleted.");
					else
						$messages->setOkMessage("The Ticker has been deleted.");
				}
				else
					$messages->setErrorMessage("Cannot delete Ticker.");
			}
			else
				$messages->setErrorMessage("Ticker not found.");
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			if ( $data["title"] == "" ){
				$messages->setErrorMessage("Please enter title for the ticker!") ;
			}
			
			/*if ( empty($data["site_id"]) ){
				$messages->setErrorMessage("Please select atleast one  website!") ;
			}*/
			
			if ( $data["text"] == "" ){
				$messages->setErrorMessage("Please enter the text to be shown in the ticker!") ;
			}
			
			if ( $data["activation_date"] == "" ){
				$messages->setErrorMessage("Please enter start date for the ticker!") ;
			}
            
			if( !empty($data['url']) && !preg_match('|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $data['url'])) {
				$messages->setErrorMessage("Please enter correct url") ;
			}
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			if ( $data["title"] == "" ){
				$error["ticker_title_empty"] = "Please enter title for the ticker!" ;
			}
			
			/*if ( empty($data["site_id"]) ){
				$error["site_id_empty"] = "Please select atleast one  website!" ;
			}*/
			
			if ( $data["text"] == "" ){
				$error["ticker_text_empty"] = "Please enter the text to be shown in the ticker!" ;
			}
			
			if ( $data["activation_date"] == "" ){
				$error["ticker_date_empty"] = "Please enter start date for the ticker!" ;
			}
			if( !empty($data['url']) && !preg_match('|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $data['url'])) {
				$messages->setErrorMessage("Please enter correct url") ;
			}

            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}

		function getSites()
		{
			$db = new db_local;

			$query = "SELECT site_id, site_domain, site_status "
								." FROM ". TABLE_SITES 
								." WHERE site_status = '1' ";
				
				$db->query($query);
				while($db->next_record()){
					$data = $db->Record;
					$all_sites[] = array(	"site_id"		=> $data["site_id"],
											"site_domain"	=> $data["site_domain"],
											"site_status"	=> $data["site_status"]
									) ;
				}
			return $all_sites;
		}


		function gedata( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TICKERS;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
			
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = array("id" 		=> $db->f("id"),
									"site_id" 		=> explode(",", $db->f("site_id")),
									"title" 		=> $db->f("title"),
									"text" 			=> $db->f("text"),
									"url" 			=> $db->f("url"),
									"ticker_order" 	=> $db->f("ticker_order"),
									"status" 		=> $db->f("status"),
									"activation_date"=> $db->f("activation_date") ,
									"expiry_date" 	=> $db->f("expiry_date")
								);
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				 return ( $total );
			}
			else {
				return false;
			}	
		}
    }
?>