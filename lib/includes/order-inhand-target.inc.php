<?php 

	class OrdIhTarget {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;
        
      
		    
		
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			 
			$query .= " FROM ". TABLE_ORDIH_TARGET;
				//LEFT JOIN ". TABLE_USER." ON ".TABLE_ORDIH_TARGET.".lead_assign_to= ".TABLE_USER.".user_id"
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
       

         
	
        /**
        * This function is used to remove the quick entry information from the database.
        *
        * @param   string      unique ID of the quick entry whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (OrdIhTarget::getList($db, $user, 'id', " WHERE id = '$id'")) > 0 ) {
               
				if ( $messages->getErrorMessageCount() <= 0 ) {
					// Delete the Client itself.
					$query = "DELETE FROM ". TABLE_ORDIH_TARGET
								." WHERE id = '". $id ."'";
					if ( $db->query($query) && $db->affected_rows()>0 ) {
						$messages->setOkMessage("The Record has been deleted.");
					}
					else {
						$messages->setErrorMessage("The Record was not deleted.");
					}
				}
			
            }
            else {
                $messages->setErrorMessage("The record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
             
           /*  if ( empty($data['do_it']) ) {
              $messages->setErrorMessage('Select Target Date.');		
            }  */
             
			if ( !isset($data['team_id']) || empty($data['team_id']) ) {
                $messages->setErrorMessage('Select Associate.');
			}
           
			if ( !isset($data['target_amt']) || empty($data['target_amt']) ) {
                $messages->setErrorMessage("The Target Amount is not specified.");
            }else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['target_amt']) ) {
                    $messages->setErrorMessage("The Target Amount is not valid.");
                }                
            }
              
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}        
		}

		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			if ( !isset($data['executive_id']) || empty($data['executive_id']) ) {
                $messages->setErrorMessage('Select Associate.');
			}
			if ( empty($data['frm_dt']) ) {
              $messages->setErrorMessage('Select From Date.');		
            } 
            if ( empty($data['to_dt']) ) {
              $messages->setErrorMessage('Select To Date.');		
            } 
           
			if ( !isset($data['target_amount']) || empty($data['target_amount']) ) {
                $messages->setErrorMessage("The Target Amount is not specified.");
            }else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['target_amount']) ) {
                    $messages->setErrorMessage("The Target Amount is not valid.");
                }
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
       
    }
?>