<?php
	class Company {
		
		       
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SETTINGS_COMPANY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SETTINGS_COMPANY;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                return ( $total );
            } else {
                return false;
            }   
        }
    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;

            $query = "DELETE FROM ". TABLE_SETTINGS_COMPANY
                        ." WHERE id = '$id'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Record has been deleted.");                      
            }
            else {
                $messages->setErrorMessage("The Record was not deleted.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}       
           
            if ( !isset($data['name']) || empty($data['name']) ) {
                $messages->setErrorMessage("Name can not be empty.");
            }
             
            if ( !isset($data['prefix']) || empty($data['prefix']) ) {
                $messages->setErrorMessage("Prefix can not be empty.");
            }
            if ( !isset($data['quot_prefix']) || empty($data['quot_prefix']) ) {
                $messages->setErrorMessage("Other Prefix can not be empty.");
            }
            if ( !isset($data['prefix']) || !empty($data['prefix'])) {
                $query = "SELECT * FROM ".TABLE_SETTINGS_COMPANY." WHERE prefix='". $data['prefix'] ."'";
                    if ( $db->query($query) && $db->affected_rows() > 0 ) { 
                        $messages->setErrorMessage("This prefix is already exist.Please specify another.");
                    }
            }
            if ( !empty($data['do_implement_st']) ) {                    
                $data['do_implement_st'] = explode('/', $data['do_implement_st']);
                $data['do_implement_st'] = mktime(0, 0, 0, $data['do_implement_st'][1], $data['do_implement_st'][0], 
				$data['do_implement_st'][2]);                
            }else{
                $data['do_implement_st'] = '';
            }   
			
			if ( !empty($data['do_implement']) ) {                    
                $data['do_implement'] = explode('/', $data['do_implement']);
                $data['do_implement'] = mktime(0, 0, 0, $data['do_implement'][1], $data['do_implement'][0], 
				$data['do_implement'][2]);                
            }else{
                $data['do_implement'] = '';
            }  
			if ( !empty($data['do_iso']) ) {                    
                $data['do_iso'] = explode('/', $data['do_iso']);
                $data['do_iso'] = mktime(0, 0, 0, $data['do_iso'][1], $data['do_iso'][0], 
				$data['do_iso'][2]);                
            }else{
                $data['do_iso'] = '';
            }  
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            
            if ( !isset($data['name']) || empty($data['name']) ) {
                $messages->setErrorMessage("Name can not be empty.");
            }
            
            if ( !isset($data['prefix']) || empty($data['prefix']) ) {
                $messages->setErrorMessage("Prefix can not be empty.");
            }            
            if ( !isset($data['quot_prefix']) || empty($data['quot_prefix']) ) {
                $messages->setErrorMessage("Other Prefix can not be empty.");
            }
            if ( !isset($data['prefix']) || !empty($data['prefix'])) {
                $query = "SELECT * FROM ".TABLE_SETTINGS_COMPANY." WHERE prefix='". $data['prefix'] ."' AND id != '".$data['id']."'";
                    if ( $db->query($query) && $db->affected_rows() > 0 ) { 
                        $messages->setErrorMessage("This prefix is already exist.Please specify another.");
                    }
            }
			if ( !empty($data['do_implement_st']) ) {                    
                $data['do_implement_st'] = explode('/', $data['do_implement_st']);
                $data['do_implement_st'] = mktime(0, 0, 0, $data['do_implement_st'][1], $data['do_implement_st'][0], 
				$data['do_implement_st'][2]);                
            }else{
                $data['do_implement_st'] = '';
            }   
            if ( !empty($data['do_implement']) ) {                    
                $data['do_implement'] = explode('/', $data['do_implement']);
                $data['do_implement'] = mktime(0, 0, 0, $data['do_implement'][1], $data['do_implement'][0], $data['do_implement'][2]);                
            }else{
                $data['do_implement'] = '';
            }    
			if ( !empty($data['do_iso']) ) {                    
                $data['do_iso'] = explode('/', $data['do_iso']);
                $data['do_iso'] = mktime(0, 0, 0, $data['do_iso'][1], $data['do_iso'][0], 
				$data['do_iso'][2]);                
            }else{
                $data['do_iso'] = '';
            }  
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
               	
            $query = " UPDATE ". TABLE_SETTINGS_COMPANY
                    ." SET ". TABLE_SETTINGS_COMPANY .".status = '1'"
                         

                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Company has been activated.");                      
            }
            else {
                $messages->setErrorMessage("The Company has not been activated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            $query = " UPDATE ". TABLE_SETTINGS_COMPANY
                    ." SET ". TABLE_SETTINGS_COMPANY .".status = '0'"
                         

                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Company has been deactivated.");                      
            }
            else {
                $messages->setErrorMessage("The Company has not been deactivated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    
        
		
    }
?>
