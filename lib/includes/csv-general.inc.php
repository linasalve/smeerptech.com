<?php 

class CsvGeneral {
	 
		
    function getRestrictions($type='') {
		// 1024 * 1024 = 1048576 = 1MB
        $restrictions = array(
                            'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), 
                            'MIME'      => array('text/csv','text/comma-separated-values','application/vnd.ms-excel', 'text/plain','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
                            'EXT'       => array('csv', 'txt','xlsx')
                             );
        
        if ( empty($type) ) {
            return ($restrictions);
        }
        else {
            return($restrictions[$type]);
        }
    }
    
      
    
    
    function validateUpload($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }

        if ( empty($file['file_csv']['error']) ) {
            if ( !empty($file['file_csv']['tmp_name']) ) {
                if ( $file['file_csv']['size'] == 0 ) {
                    $messages->setErrorMessage('The Uploaded file is empty.');
                }
                if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
                    $messages->setErrorMessage('The File type is not allowed to be uploaded.');
                }
                if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
                    $messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
                }
            }
            else {
                $messages->setErrorMessage('Server Error: File was not uploaded.');
            }
        }
        else {
		
            $messages->setErrorMessage('Unexpected Error: File was not uploaded.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    
    
    
       
}
?>