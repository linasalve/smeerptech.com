<?php 

	class TermsRegistration {
		const ACTIVE  = 1;
		const DEACTIVE  =0;

		
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         	* @param mixed    string or integer representing the starting record to fetch from.
        	 * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         	*                   integer, the number of records that are fetched.
         	*
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TERMS_REGISTRATION;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				 return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $user = NULL;
            if ( (TermsRegistration::getList($db, $user, 'id', " WHERE id = '$id'")) > 0 ) 
			{
               		$user = $user[0];
					$query = "DELETE FROM ". TABLE_TERMS_REGISTRATION
								." WHERE id = '". $id ."'";
					if ( !$db->query($query) || $db->affected_rows()<=0 ) {
						$messages->setErrorMessage("Terms and condition is not deleted.");
					}else{
						$messages->setOkMessage("Terms and condition has been deleted.");
					}
				
			}
			else{
				$messages->setErrorMessage("Terms and Condition was not found.");        
			}
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
 
            if ( !isset($data['sequence']) || empty($data['sequence']) ) {
                $messages->setErrorMessage('Sequence should be provided.');
			}
            
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage('Title should be provided.');
			}
            if ( !isset($data['condition_content']) || empty($data['condition_content']) ) {
                $messages->setErrorMessage('Body Text should be provided.');
			}
            
            /*if ( !isset($data['page_heading']) || empty($data['page_heading']) ) {
                $messages->setErrorMessage('Page Heading should be provided.');
            }*/
            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( !isset($data['sequence']) || empty($data['sequence']) ) {
                $messages->setErrorMessage('Sequence should be provided.');
			}
            
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage('Title should be provided.');
			}
            if ( !isset($data['condition_content']) || empty($data['condition_content']) ) {
                $messages->setErrorMessage('Body Text should be provided.');
			}
           

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
        
        
        
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
         
                    $query = " UPDATE ". TABLE_TERMS_REGISTRATION
                            ." SET ". TABLE_TERMS_REGISTRATION .".status = '".TermsRegistration::ACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The terms and condition has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The terms and condition has not been activated.");
                    }
                
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
                    echo $query = " UPDATE ". TABLE_TERMS_REGISTRATION
                            ." SET ". TABLE_TERMS_REGISTRATION .".status = '".TermsRegistration::DEACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The terms and condition has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The terms and condition has not been deactivated.");
                    }
                
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    }
?>