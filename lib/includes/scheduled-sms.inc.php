<?php
	class ScheduledSmsList
	{
		/*
		* get the list of corporate users
		*/
		function getList(&$db, &$user_list, $required_fields, $condition="", $from="", $rpp="")
		{
			$query = "SELECT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) 
			{
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) 
			{
				$query .= " ". $required_fields;
			}
			else 
			{
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			$query .= " FROM ". TABLE_SMS_SCHEDULER;
			
			$query .= " ". $condition;
			
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) 
			{
				$query .= " LIMIT ". $from .", ". $rpp;
			}
			//echo $query;
			if ( $db->query($query) ) 
			{
				if ( $db->nf() > 0 ) 
				{
					while ($db->next_record()) 
						$user_list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
				}
				//print_r($hotel_list);
				return ( $total );
			}
			else 
			{
				return false;
			}
		}

        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (ScheduledSmsList::getList($db, $user, 's_id', " WHERE s_id = '$id'")) > 0 ) 
			{
				$query = "DELETE FROM ". TABLE_SMS_SCHEDULER
							." WHERE s_id = '". $id ."'";
				if ( !$db->query($query) || $db->affected_rows()<=0 ) 
					$messages->setErrorMessage("The Message is not deleted.");
				else
					$messages->setOkMessage("The Message has been deleted.");
			}
			else
				$messages->setErrorMessage("Message not found.");
		}
	}
?>
