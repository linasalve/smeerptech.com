<?php
	class OrderBid {
		
		const PENDING = 0;
		const ACTIVE  = 1;
		         
		function getStatus() {
			$status = array( 
							'ACTIVE'    => OrderBid::ACTIVE,
							'PENDING'   => OrderBid::PENDING
						);
			return ($status);
		}
     
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_ORDER_BID;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
             $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
          
            $query .= " FROM ".TABLE_ORDER_BID ." ";
			$query .= " LEFT JOIN ".TABLE_BILL_ORDERS." ON ".TABLE_ORDER_BID.".order_id = ".TABLE_BILL_ORDERS.".id";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	   
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( !in_array($status_new, OrderBid::getStatus()) ) {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
            else {
                $order = NULL;
                if ( (OrderBid::getList($db, $order, 'id, status', " WHERE id = '$id'")) > 0 ) {
                    $order = $order[0];
					$markComplete='';
					if($status_new==OrderBid::ACTIVE){
						 $markComplete = ", bid_approved_dt = '".date('Y-m-d H:i:s')."', bid_approved_by='".$my['user_id']."'
						 bid_approved_by_name='".$my['f_name']." ".$my['l_name']."'";
					}
					$query = "UPDATE ". TABLE_ORDER_BID
								." SET status = '$status_new' ".$markComplete
								." WHERE id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) {
						$messages->setOkMessage("The Status has been updated.");
					}else {
						$messages->setErrorMessage("The Status was not updated.");
					}
                     
                }
                else {
                    $messages->setErrorMessage("The Order was not found.");
                }
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_ORDER_BID_DETAILS;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
    
        
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra=''){
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
             
            $data['query_p']='';
            $is_part = false;
            $isrenewable = false;
           
            if(isset($data['team_member'])){
                $count1= count($data['team_member']) ;
				$count2 = count(array_unique($data['team_member'])) ; 
				if( $count1!=$count2 ){
					 $messages->setErrorMessage('Please check the entries, You selected duplicate team members');
				} 
                foreach ( $data['team_member'] as $key=>$particular ) {
					
                    if(empty($data['team_member'][$key]) ){
                         $messages->setErrorMessage('The Team Member field at Position '. ($key+1) .' cannot be empty.');
                    }
					if(empty($data['hrs'][$key]) ){
                         $messages->setErrorMessage('The Hours field at Position '. ($key+1) .' cannot be empty.');
                    }
					
					/* 
					if(empty($data['do_complete'][$key])){
                         $messages->setErrorMessage('The Date of Completion field at Position '. ($key+1) .' cannot be empty.');
                    }
					*/
                    if ( ( !empty($data['team_member'][$key]) || !empty($data['hrs'][$key]) ) && $count1==$count2) {
                        
						$table = TABLE_USER;
						$condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['team_member'][$key] ."' LIMIT 0,1 " ;
						$fields1 =  TABLE_USER .'.current_salary';
						$exeCreted= getRecord($table,$fields1,$condition2);                
						$currentSalary = $exeCreted['current_salary'] ;				
						 
						$data['per_hour_salary'][$key] = $currentSalary / STD_WORKING_HRS;
						$data['per_hour_salary'][$key] =  number_format($data['per_hour_salary'][$key],2);
						$worked_min = ( $data['hrs'][$key] * 60 )  ;
						$worked_hour = ($worked_min/60) ;
						$worked_hour =  number_format($worked_hour,2);
						$worked_hour_salary = ($data['per_hour_salary'][$key] * $worked_hour);						
						 
						 
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
			
			 
             

            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		 
    }
    
?>
