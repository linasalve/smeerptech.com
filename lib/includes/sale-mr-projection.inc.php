<?php
		
	class SaleMrProjection {
		       
		const ACTIVE  = 1;
		const DEACTIVE = 0;
		 
		function getMonth() {
			$month = array('0' => 'Jan',
							'1' => 'Feb',
							'2' => 'Mar',
							'3' => 'Apr',
							'4' => 'May',
							'5' => 'June',
							'6' => 'July',
							'7' => 'Aug',
							'8' => 'Sep',
							'9' => 'Oct',
							'10' => 'Nov',
							'11' => 'Dec'
						);
			return ($month);
		}    
		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SALE_MR_PROJECTION;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
             $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
               $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SALE_MR_PROJECTION
                        ." LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.'.user_id='.TABLE_SALE_MR_PROJECTION.'.user_id ';
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (SaleMrProjection::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               $details = $details[0];
               if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_SALE_MR_PROJECTION
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                }
                else {
                    $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
            if(!empty($data['month']) && !empty($data['year'])){
                $sale_mr_projection_list  = NULL;
                $condition  = " WHERE month='".$data['month']."' AND year='".$data['year']."'";
    
                if ( (SaleMrProjection::getList($db, $sale_mr_projection_list, 'id', $condition)) > 0 ) {
                    foreach ( $sale_mr_projection_list as $mr) {
                        $messages->setErrorMessage('Projection for this month & year is already specified. Please select another.');
                    }
                }
            }
           
            if ( !isset($data['month']) || empty($data['month']) ) {
                $messages->setErrorMessage("Select Month for which projection is going to set.");
            }
            if ( !isset($data['year']) || empty($data['year']) ) {
                $messages->setErrorMessage("Select Year for which projection is going to set.");
            }
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("Amount can not be empty.");
            }
            
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }     
            
            if(!empty($data['month']) && !empty($data['year'])){
                $sale_mr_projection_list  = NULL;
                $condition  = " WHERE month='".$data['month']."' AND year='".$data['year']."' AND id!='".$data['id']."'";
    
                if ( (SaleMrProjection::getList($db, $sale_mr_projection_list, 'id', $condition)) > 0 ) {
                    foreach ( $sale_mr_projection_list as $mr) {
                        $messages->setErrorMessage('Projection for this month & year is already specified. Please select another.');
                    }
                }
            }
           
            if ( !isset($data['month']) || empty($data['month']) ) {
                $messages->setErrorMessage("Select Month for which projection is going to set.");
            }
            if ( !isset($data['year']) || empty($data['year']) ) {
                $messages->setErrorMessage("Select Year for which projection is going to set.");
            }
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("Amount can not be empty.");
            }
                      
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_SALE_MR_PROJECTION
                            ." SET ".  TABLE_SALE_MR_PROJECTION .".status = '".SaleMrProjection::ACTIVE."'"
                            	 

                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Sale Marketing Projection has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Sale Marketing Projection has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ".  TABLE_SALE_MR_PROJECTION
                            ." SET ".  TABLE_SALE_MR_PROJECTION .".status = '".SaleMrProjection::DEACTIVE."'"
                            	 

                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Sale Marketing Projection has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Sale Marketing Projection has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
		
    }
?>
