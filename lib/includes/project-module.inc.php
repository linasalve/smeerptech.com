<?php 

	class ProjectModule {
		
		
		const ACTIVE  = 1;
		const DEACTIVE = 0;
		
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PROJECT_TASK_MODULE;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $module = NULL;
            if ( (ProjectModule::getList($db, $module, 'id', " WHERE id = '$id'")) > 0 ) 
			{
                $module = $module[0];
					
                    $query = "DELETE FROM ". TABLE_PROJECT_TASK_MODULE
								." WHERE id = '". $id ."'";
                    
					if ( $db->query($query) || $db->affected_rows()>=0 ){
                        $messages->setOkMessage("The Module has been deleted.");
                    }else{
                        $messages->setOkMessage("The Module was not deleted.");
                    }
			}
			else
				$messages->setErrorMessage("Module not found.");
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$service = NULL;
            
			//validate the module title 
            if ( !isset($data['title']) || empty($data['title']) ) 
                $messages->setErrorMessage('Module title should be provided.');
                
            //validate the order
           /*  if ( !isset($data['or_id']) || empty($data['or_id']) ) 
                $messages->setErrorMessage('Order should be provided.');
             */
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the service title 
            if ( !isset($data['title']) || empty($data['title']) ) 
                $messages->setErrorMessage('Module title should be provided.');
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
        function getParent(&$db,&$services,$or_id,$id='0')
		{
			/*
				$sql = "SELECT ".TABLE_PROJECT_TASK_MODULE.".id,".TABLE_PROJECT_TASK_MODULE.".title FROM "
                    .TABLE_PROJECT_TASK_MODULE." WHERE parent_id='".$id."' AND order_id='".$or_id."'"; 
			*/
			$sql = "SELECT ".TABLE_PROJECT_TASK_MODULE.".id,".TABLE_PROJECT_TASK_MODULE.".title FROM "
                    .TABLE_PROJECT_TASK_MODULE." WHERE ".TABLE_PROJECT_TASK_MODULE.".parent_id='".$id."' 
					AND ".TABLE_PROJECT_TASK_MODULE.".id > 871 ";
			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
					$services[] = processSqlData($db->Record);
                    //$title = processSqlData($db->f('ss_title')) ;
                   // $ss_id = $db->f('ss_id') ;
					//$services[$ss_id] = $title ;

				return ( $db->nf() );
			}
			else 
				return false;			
		}
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
            $query = " UPDATE ". TABLE_PROJECT_TASK_MODULE
                    ." SET ". TABLE_PROJECT_TASK_MODULE .".status = '".ProjectModule::ACTIVE."'"
                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The module has been activated.");                      
            }
            else {
                $messages->setErrorMessage("The module has not been activated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
               	
            $query = " UPDATE ". TABLE_PROJECT_TASK_MODULE
                    ." SET ". TABLE_PROJECT_TASK_MODULE .".status = '".ProjectModule::DEACTIVE."'"
                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The module has been deactivated.");                      
            }
            else {
                $messages->setErrorMessage("The module has not been deactivated.");
            }
                
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
    }
?>