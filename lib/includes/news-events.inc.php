<?php
	class NewsEvents {
		
		const DEACTIVE = 0;
		const ACTIVE  = 1;    
		
		const PRESSRELEASE = 1;
		const MEDIA  = 2;    
		const EVENT  = 3;   
		const EXHIBITION  = 4;   
		const COVERAGE   = 5;   
        
        function getStatus() {
			$status = array(
							'ACTIVE'   => NewsEvents::ACTIVE,
                            'DEACTIVE'    => NewsEvents::DEACTIVE
						);
			return ($status);
		}   
        
        function getEventType() {
			$eventtype = array(
							'Press Release'   => NewsEvents::PRESSRELEASE,
                            'Media'    => NewsEvents::MEDIA,
                            'Event'    => NewsEvents::EVENT,
                            'Exhibition'    => NewsEvents::EXHIBITION,
                            'Coverage'    => NewsEvents::COVERAGE
						);
			return ($eventtype);
		}   
		
		
		/**
		 *	Function to get all the News &amp; Events.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_EVENTS;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_EVENTS;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_EVENTS
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
           
            if ( !isset($data['events_date']) || empty($data['events_date']) ) {
                $messages->setErrorMessage("Select the event date.");
            }
            
          
			
            if ( !isset($data['events_exp_date']) || empty($data['events_exp_date']) ) {
                $messages->setErrorMessage("Select the event expiry date.");
            }
            
           
            
            
            if ( $data["hr"] == "" ||  $data["min"] == "" ||  $data["sec"] == "" ) {	
                $messages->setErrorMessage("Select event time.");
            }else{
            	$data['event_time'] = $data['hr'] .':'. $data['min'] .':'. $data['sec'] ;
		    }
            
            if ( !isset($data['events_title']) || empty($data['events_title']) ) {
                $messages->setErrorMessage("Event Title can not be empty.");
            }
            
            if ( !isset($data['events_source']) || empty($data['events_source']) ) {
                $messages->setErrorMessage("Event Venue can not be empty.");
            }
            
            if ( !isset($data['events_introtext']) || empty($data['events_introtext']) ) {
                $messages->setErrorMessage("Intro Text can not be empty.");
            }
            
            if ( !isset($data['event_type']) || empty($data['event_type']) ) {
                $messages->setErrorMessage("Event Type can not be empty.");
            }
            
             if ( !isset($data['events_content']) || empty($data['events_content']) ) {
                $messages->setErrorMessage("Event Content can not be empty.");
            }
               
           
                    
					           
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            
            if ( !isset($data['events_date']) || empty($data['events_date']) ) {
                $messages->setErrorMessage("Select the event date.");
            }
            if ( !isset($data['events_exp_date']) || empty($data['events_exp_date']) ) {
                $messages->setErrorMessage("Select the event expiry date.");
            }           
           
            if ( $data["hr"] == "" ||  $data["min"] == "" ||  $data["sec"] == "" ) {	
                $messages->setErrorMessage("Select event time.");
            }else{
            	$data['event_time'] = $data['hr'] .':'. $data['min'] .':'. $data['sec'] ;
		    }
            
            if ( !isset($data['events_title']) || empty($data['events_title']) ) {
                $messages->setErrorMessage("Event Title can not be empty.");
            }
            
            if ( !isset($data['events_source']) || empty($data['events_source']) ) {
                $messages->setErrorMessage("Event Venue can not be empty.");
            }
            
            if ( !isset($data['events_introtext']) || empty($data['events_introtext']) ) {
                $messages->setErrorMessage("Intro Text can not be empty.");
            }
            
            if ( !isset($data['event_type']) || empty($data['event_type']) ) {
                $messages->setErrorMessage("Event Type can not be empty.");
            }
            
             if ( !isset($data['events_content']) || empty($data['events_content']) ) {
                $messages->setErrorMessage("Event Content can not be empty.");
            }
                   
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_EVENTS
                            ." SET ". TABLE_EVENTS .".status = '".NewsEvents::ACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The News & Events has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The News & Events has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_EVENTS
                            ." SET ". TABLE_EVENTS .".status = '".NewsEvents::DEACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The News & Events has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The News & Events has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    
        
		
    }
?>
