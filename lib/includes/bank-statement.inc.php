<?php 

class BankStatement{

	const DEACTIVE = 0;
	const ACTIVE  = 1;
	
	const NOTLINKED = 0;
	const LINKED  = 1;
	const PARTIALLINKED  = 2;
	
	function getStatus() {
			$status = array('ACTIVE'    => BankStatement::ACTIVE,
							'DEACTIVE'   => BankStatement::DEACTIVE
						);
			return ($status);
		}
	function getLStatus() {
			$status = array('LINKED'    => BankStatement::LINKED,
						'PARTIALLINKED'   => BankStatement::PARTIALLINKED,
						'NOTLINKED'   => BankStatement::NOTLINKED
						);
			return ($status);
		}
    function getRestrictions($type='') {
		// 1024 * 1024 = 1048576 = 1MB
        $restrictions = array(
								'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), 
								'MIME'      => array('text/csv','text/comma-separated-values','text/x-csv','application/vnd.ms-excel', 'text/plain','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
								'EXT'       => array('csv', 'txt','xlsx')
								 );
        
        if ( empty($type) ) {
            return ($restrictions);
        }
        else {
            return($restrictions[$type]);
        }
    }
    
    function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}else{
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_BANK_STATEMENT." LEFT JOIN ".TABLE_PAYMENT_BANK." ON 
				".TABLE_BANK_STATEMENT.".bank_id = ".TABLE_PAYMENT_BANK.".id ";
			
            $query .= " ". $condition;
	  
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
	}
	
	/**
	 * Function to validate the input from the User while Adding.
	 *
	 * @param	array	The array containing the information to be added.
	 *
	 *
	 *
	 */
	function validateAdd(&$data, $extra='') {
		foreach ($extra as $key=>$value) {
			$$key = $value;
		}        
		/* 
		if ( !isset($data['company_id']) || empty($data['company_id']) ) {
			$messages->setErrorMessage("Select Company");
		}   
		*/ 
		if ( !isset($data['bank_id']) || empty($data['bank_id']) ){
			$messages->setErrorMessage("Select Bank");
		}
		if(empty($data['stmt_date'])){
             $messages->setErrorMessage("Select Transaction date"); 
        }
		if(empty($data['value_date'])){
             $messages->setErrorMessage("Select value date"); 
        }
		if(empty($data['month'])){
             $messages->setErrorMessage("Select month"); 
        } 
		if($data['credit']==''){
             $messages->setErrorMessage("Enter credit"); 
        }
		if($data['debit']==''){
             $messages->setErrorMessage("Enter debit"); 
        }
		if($data['balance']==''){
             $messages->setErrorMessage("Enter balance"); 
        }
		if($data['description']==''){
             $messages->setErrorMessage("Enter description"); 
        }		
				   
		if ( $messages->getErrorMessageCount() <= 0 ) {
			return (true);
		}
		else {
			return (false);
		}
	}
	/**
	 * Function to validate the input from the User while Adding.
	 *
	 * @param	array	The array containing the information to be added.
	 *
	 *
	 *
	 */
	function validateUpdate(&$data, $extra='') {
		foreach ($extra as $key=>$value) {
			$$key = $value;
		}        
		/* 
		if ( !isset($data['company_id']) || empty($data['company_id']) ) {
			$messages->setErrorMessage("Select Company");
		}   
		*/ 
		if ( !isset($data['bank_id']) || empty($data['bank_id']) ){
			$messages->setErrorMessage("Select Bank");
		}
		if(empty($data['stmt_date'])){
             $messages->setErrorMessage("Select Transaction date"); 
        }
		if(empty($data['value_date'])){
             $messages->setErrorMessage("Select value date"); 
        }
		if(empty($data['month'])){
             $messages->setErrorMessage("Select month"); 
        } 
		if($data['credit']==''){
             $messages->setErrorMessage("Enter credit"); 
        }
		if($data['debit']==''){
             $messages->setErrorMessage("Enter debit"); 
        }
		if($data['balance']==''){
             $messages->setErrorMessage("Enter balance"); 
        }
		if($data['description']==''){
             $messages->setErrorMessage("Enter description"); 
        }		
				   
		if ( $messages->getErrorMessageCount() <= 0 ) {
			return (true);
		}
		else {
			return (false);
		}
	}
    function getTables(&$db) {
        $count  = 0;
        $tables = array();
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[] = $temp[$i]['table_name'];
        }
        $temp   = NULL;
        return($tables);
    }
    
    
    function getTablesData(&$db, &$tables) {
        $count  = 0;
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[$i] = array('table_name'=> $temp[$i]['table_name'],
                                'fields'    => $db->metadata($temp[$i]['table_name'])
                               );
        }
        $temp   = NULL;
        return($count);
    }
	
    function validateUpload($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }

        if ( empty($file['file_csv']['error']) ) {
            if ( !empty($file['file_csv']['tmp_name']) ) {
                if ( $file['file_csv']['size'] == 0 ) {
                    $messages->setErrorMessage('The Uploaded file is empty.');
                }
                if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
                    $messages->setErrorMessage('The File type is not allowed to be uploaded.');
                }
                if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
                    $messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
                }
            }
            else {
                $messages->setErrorMessage('Server Error: File was not uploaded.');
            }
        }
        else {
            $messages->setErrorMessage('Unexpected Error: File was not uploaded.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    function duplicateFieldValue(&$db, $table, $field_name, $condition) {
        $query = "SELECT ".$field_name." FROM ".$table." ".$condition;
        
        if ( $db->query($query) && $db->nf()>0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
	
	function active($id, $extra){
		foreach ($extra as $key=>$value) {
			$$key = $value;
		}
		$details = NULL;
		$query = " UPDATE ". TABLE_BANK_STATEMENT
				." SET ". TABLE_BANK_STATEMENT .".status = '1'"
				." WHERE id = '". $id ."'";
		if ( $db->query($query) && $db->affected_rows()>0 ) {
			$messages->setOkMessage("The Record has been activated.");                      
		}else {
			$messages->setErrorMessage("The Record has not been activated.");
		}  
		if ( $messages->getErrorMessageCount() <= 0 ) {
			return (true);
		}
		else {
			return (false);
		}
            
    }
        
	function deactive($id, $extra) {
		foreach ($extra as $key=>$value) {
			$$key = $value;
		}
		$details = NULL;
			
		$query = " UPDATE ". TABLE_BANK_STATEMENT
				." SET ". TABLE_BANK_STATEMENT .".status = '0'"
				." WHERE id = '". $id ."'";
		if ( $db->query($query) && $db->affected_rows()>0 ) {
			$messages->setOkMessage("The Record has been deactivated.");                      
		}
		else {
			$messages->setErrorMessage("The Record has not been deactivated.");
		}
			 
		
		if ( $messages->getErrorMessageCount() <= 0 ) {
			return (true);
		}
		else {
			return (false);
		}
		
	}
	 
}
?>