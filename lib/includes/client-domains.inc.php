<?php
	class ClientDomains{
		
		       
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_CLIENT_DOMAINS;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_CLIENT_DOMAINS;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                return ( $total );
            } else {
                return false;
            }   
        }
    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;

            $query = "DELETE FROM ". TABLE_CLIENT_DOMAINS
                        ." WHERE id = '$id'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Record has been deleted.");                      
            }
            else {
                $messages->setErrorMessage("The Record was not deleted.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}       
           
            if ( !isset($data['domain']) || empty($data['domain']) ) {
                $messages->setErrorMessage("Domain name can not be empty.");
            }else{
				//check domain already exist or not  
				$query = "SELECT id, client_details FROM ".TABLE_CLIENT_DOMAINS." WHERE	domain = '".trim($data['domain'])."' LIMIT 0,1"; 
				if ( $db->query($query) ){
					if ( $db->nf() > 0 ){
						$db->next_record();
                        $record = processSqlData($db->result());
						$messages->setErrorMessage('Domain '.$data['domain'].' is already exist with client '.$record['client_details']);
					}
				} 
			}
			
            if( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select client.");
            } 
			
			if( !isset($data['registered_with']) || empty($data['registered_with']) ) {
                $messages->setErrorMessage("Select with whome this domain is registered.");
            } 
			
			 
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
            if ( !isset($data['domain']) || empty($data['domain']) ) {
                $messages->setErrorMessage("Domain name can not be empty.");
            }else{
				$query = "SELECT id, client_details FROM ".TABLE_CLIENT_DOMAINS." WHERE 
					domain = '".trim($data['domain'])."' AND id != '".$data['id']."' LIMIT 0,1"; 
				if ( $db->query($query)){
					if ( $db->nf() > 0 ){
						$db->next_record();
                        $record = processSqlData($db->result());
						$messages->setErrorMessage('Domain '.$data['domain'].' is already exist with client '.$record['client_details']);
					}
				} 
			}
			
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select Client.");
            } 
			if ( !isset($data['registered_with']) || empty($data['registered_with']) ) {
                $messages->setErrorMessage("Select with whome this domain is registered.");
            }
			
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
               	
            $query = " UPDATE ". TABLE_CLIENT_DOMAINS
                    ." SET ". TABLE_CLIENT_DOMAINS .".status = '1'" 
                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Company has been activated.");                      
            }
            else {
                $messages->setErrorMessage("The Company has not been activated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            $query = " UPDATE ". TABLE_CLIENT_DOMAINS
                    ." SET ". TABLE_CLIENT_DOMAINS .".status = '0'"
                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Company has been deactivated.");                      
            }
            else {
                $messages->setErrorMessage("The Company has not been deactivated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        } 
		
    }
?>
