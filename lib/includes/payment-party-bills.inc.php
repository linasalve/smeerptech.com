<?php
	class PaymentPartyBills {
		
		const DEACTIVE = 0;
		const ACTIVE  = 1;     
		
        const BILL_STATUS_UNPAID = 0;
		const BILL_STATUS_PAID = 1;
		const BILL_STATUS_PARTIALPAID = 2;
		
        const BILL = 1;
		const BILL_OTHER = 2;
		const BILL_SALARY = 3;
		
		const FOREXLOSS = 1;
        const ADJUSTMENT = 2;
		
		const FORCEACTIVE = 1;
        const FORCEDEACTIVE = 0;
		
		
		
		function getRestrictions($type='') {
			// 1024 * 1024 = 1048576 = 1MB
			$restrictions = array(
								'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), 
								'MIME'      => array('text/csv','text/comma-separated-values','application/vnd.ms-excel', 'text/plain','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
								'EXT'       => array('csv', 'txt','xlsx')
								 );
			
			if ( empty($type) ) {
				return ($restrictions);
			}
			else {
				return($restrictions[$type]);
			}
		}
		
		function validateUpload($data, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			if ( empty($file['file_csv']['error']) ) {
				if ( !empty($file['file_csv']['tmp_name']) ) {
					if ( $file['file_csv']['size'] == 0 ) {
						$messages->setErrorMessage('The Uploaded file is empty.');
					}
					if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
						$messages->setErrorMessage('The File type is not allowed to be uploaded.');
					}
					if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
						$messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
					}
				}
				else {
					$messages->setErrorMessage('Server Error: File was not uploaded.');
				}
			}
			else {
				$messages->setErrorMessage('Unexpected Error: File was not uploaded.');
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		function getFStatus(){
			$fstatus = array(
						PaymentPartyBills::FORCEDEACTIVE=>'Force Open',
						PaymentPartyBills::FORCEACTIVE=>'Force Closed'
					);
			return ($fstatus);
		}
		
		
		function getType(){
			$type = array(
						PaymentPartyBills::BILL=>'Bill',
						PaymentPartyBills::BILL_OTHER=>'No Bill',
						//PaymentPartyBills::BILL_SALARY=>'Salary'
					);
			return ($type);
		}   
		function getStatus(){
			$status = array(
			    'ACTIVE'   => PaymentPartyBills::ACTIVE,
                'CANCELLED'    => PaymentPartyBills::DEACTIVE
						);
			return ($status);
		} 
		function getGLType(){
			$type = array(
							PaymentPartyBills::FOREXLOSS=>'FOREX LOSS' ,
                           //PaymentPartyBills::ADJUSTMENT=> 'AMOUNT ADJUSTMENT'
						);
			return ($type);
		}  		
		function getBillStatus() {
			$bstatus = array(
			    'UNPAID'   => PaymentPartyBills::BILL_STATUS_UNPAID,
                'PAID'    => PaymentPartyBills::BILL_STATUS_PAID,
                'PARTIALPAID'    => PaymentPartyBills::BILL_STATUS_PARTIALPAID
			);
			return ($bstatus);
		}  
		function getSTypes(){
		
			$typeList= array('Qty'=>'Qty',
							 'Nos'=>'Nos',							 
							 'Yrs'=>'Yrs',							 
							 'Mth'=>'Mth',							 
							 'Ltrs'=>'Ltrs'							 
			);
			return $typeList;
		}
        function getNos(){
			$noList=array();
			for($i=1;$i<=11;$i++){
				$noList[]=$i;
			}
			return $noList;
		}
		function getRoundOffOp(){
			$arr= array('+','-');
			return $arr;
		}
		function getRoundOff(){
			$roundOffList=array();
			
			for($i=0.00;$i< 1; $i = $i+0.01){
				$roundOffList[]=$i;
			}
			return $roundOffList;
		}
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			/* $query .= " FROM ". TABLE_PAYMENT_PARTY_BILLS
            	    ." LEFT JOIN ". TABLE_VENDORS
                	." ON ". TABLE_PAYMENT_PARTY_BILLS .".party_id = ". TABLE_VENDORS .".user_id " 
                    ." LEFT JOIN ". TABLE_USER
                		." ON (". TABLE_USER .".user_id = ". TABLE_PAYMENT_PARTY_BILLS .".executive_id ) "
                    ." LEFT JOIN ". TABLE_PAYMENT_BILLS_AGAINST
                    ." ON ". TABLE_PAYMENT_BILLS_AGAINST .".id = ". TABLE_PAYMENT_PARTY_BILLS .".bill_against_id "; */
            $query .= " FROM ". TABLE_PAYMENT_PARTY_BILLS
            	    ." LEFT JOIN ". TABLE_CLIENTS
                	." ON ". TABLE_PAYMENT_PARTY_BILLS .".party_id = ". TABLE_CLIENTS .".user_id " 
					." LEFT JOIN ". TABLE_VENDORS_BANK
                	." ON ". TABLE_PAYMENT_PARTY_BILLS .".vendor_bank_id = ". TABLE_VENDORS_BANK .".user_id " 
                    ." LEFT JOIN ". TABLE_USER
                	." ON (". TABLE_USER .".user_id = ". TABLE_PAYMENT_PARTY_BILLS .".executive_id ) "
					." LEFT JOIN ". TABLE_USER." as team_table"
                	." ON (team_table.user_id = ". TABLE_PAYMENT_PARTY_BILLS .".team_id ) "
                    ." LEFT JOIN ". TABLE_PAYMENT_BILLS_AGAINST
                    ." ON ". TABLE_PAYMENT_BILLS_AGAINST .".id = ". TABLE_PAYMENT_PARTY_BILLS .".bill_against_id ";
            $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
			
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total = $db->f("count") ;							
						}else{
							$total = $db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        function getNewNumber(&$db){
            $number = 100;
            $query      = "SELECT MAX(number) AS number FROM ". TABLE_PAYMENT_PARTY_BILLS;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record()){
                 $number1 =  $db->f("number");
                  $number1 = (int) $number1 ;
                if($number1 > 0){
                    $number = $number1 + 1;
                }else{
                    $number++;
                }
                
            }
            return $number;
        }
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PAYMENT_PARTY_BILLS;
            	   // ." LEFT JOIN ". TABLE_PAYMENT_PARTY
                	//." ON ". TABLE_PAYMENT_PARTY_BILLS .".party_id = ". TABLE_PAYMENT_PARTY .".id " ;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
           
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;            
			$query = "DELETE FROM ". TABLE_PAYMENT_PARTY_BILLS
						." WHERE id = '$id'";
			if ( $db->query($query) && $db->affected_rows()>0 ) {
				$messages->setOkMessage("The Record has been deleted."); 
				$sql="DELETE FROM ". TABLE_PAYMENT_BILLS_P
						." WHERE bill_id = '$id'";;
				$db->query($sql) ;
			}
			else {
				$messages->setErrorMessage("The Record was not deleted.");
			}
		            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    
		function addCronBill($msgData,$clientDetails,$noArr, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$tataIndicomCron = $noArr['tata'];
			$vodafoneCron = $noArr['vodafone'];
			$ideaCron = $noArr['idea'];
			$bsnlCron = $noArr['bsnl'];
			$msebCron = $noArr['mseb'];
			$temp_p =$billData= NULL;			
			if(trim(strtolower($msgData['from_email']))==TATAINDICOM_CBEMAIL  ){
				
				foreach($tataIndicomCron as $key=>$val){
					
					$find =strpos($msgData['subject'],$val);
					if($find){
					
						$fields = TABLE_PAYMENT_PARTY_BILLS .'.*'  ; 
						$condition_query = " WHERE (". TABLE_PAYMENT_PARTY_BILLS .".id = '". $key ."' )";
						PaymentPartyBills::getDetails($db, $billData1, $fields, $condition_query);
						$billData=$billData1[0];
						$condition_query_p = " WHERE ".TABLE_PAYMENT_BILLS_P.".bill_id = '". $key ."'";
						//PaymentPartyBills::getParticulars($db, $temp_p, '*', $condition_query_p);
						$temp_p=array();
					}
				}
			
			
			}elseif(trim(strtolower($msgData['from_email']))==VODAFONE_CBEMAIL){
				foreach($vodafoneCron as $key=>$val){
					$find =strpos($msgData['subject'],$val);
					if($find){
					
						$fields = TABLE_PAYMENT_PARTY_BILLS .'.*'  ; 
						$condition_query = " WHERE (". TABLE_PAYMENT_PARTY_BILLS .".id = '". $key ."' )";
						PaymentPartyBills::getDetails($db, $billData1, $fields, $condition_query);
						$billData=$billData1[0];
						$condition_query_p = " WHERE ".TABLE_PAYMENT_BILLS_P.".bill_id = '". $key ."'";
						//PaymentPartyBills::getParticulars($db, $temp_p, '*', $condition_query_p);
						$temp_p=array();
					}
				}
				
			}elseif(trim(strtolower($msgData['from_email']))==MSEB_CBEMAIL){
			
				foreach($msebCron as $key=>$val){
					
					$find =strpos($msgData['subject'],$val);
					if($find){
					
						$fields = TABLE_PAYMENT_PARTY_BILLS .'.*'  ; 
						$condition_query = " WHERE (". TABLE_PAYMENT_PARTY_BILLS .".id = '". $key ."' )";
						PaymentPartyBills::getDetails($db, $billData1, $fields, $condition_query);
						$billData=$billData1[0];
						$condition_query_p = " WHERE ".TABLE_PAYMENT_BILLS_P.".bill_id = '". $key ."'";
						//PaymentPartyBills::getParticulars($db, $temp_p, '*', $condition_query_p);
						$temp_p=array();
					}
				}
			}elseif(trim(strtolower($msgData['from_email']))==IDEA_CBEMAIL){
			
				foreach($ideaCron as $key=>$val){
					
					$find =strpos($msgData['subject'],$val);
					if($find){
					
						$fields = TABLE_PAYMENT_PARTY_BILLS .'.*'  ; 
						$condition_query = " WHERE (". TABLE_PAYMENT_PARTY_BILLS .".id = '". $key ."' )";
						PaymentPartyBills::getDetails($db, $billData1, $fields, $condition_query);
						$billData=$billData1[0];
						$condition_query_p = " WHERE ".TABLE_PAYMENT_BILLS_P.".bill_id = '". $key ."'";
						//PaymentPartyBills::getParticulars($db, $temp_p, '*', $condition_query_p);
						$temp_p=array();
					}
				}
			}elseif(trim(strtolower($msgData['from_email']))==BSNL_CBEMAIL){
			
				foreach($bsnlCron as $key=>$val){
					
					$find =strpos($msgData['subject'],$val);
					if($find){
					
						$fields = TABLE_PAYMENT_PARTY_BILLS .'.*'  ; 
						$condition_query = " WHERE (". TABLE_PAYMENT_PARTY_BILLS .".id = '". $key ."' )";
						PaymentPartyBills::getDetails($db, $billData1, $fields, $condition_query);
						$billData=$billData1[0];
						$condition_query_p = " WHERE ".TABLE_PAYMENT_BILLS_P.".bill_id = '". $key ."'";
						//PaymentPartyBills::getParticulars($db, $temp_p, '*', $condition_query_p);
						$temp_p=array();
					}
				}
			}
			
			if(!empty($billData)){
				 
				$contentBody = $msgData['subject']."<br/>".$msgData['body'] ;
				$contentBody = processUserData($contentBody);
				$number = $billData['number'] = PaymentPartyBills::getNewNumber($db);
				$attachfilename='';
				$bill_dt = date('Y-m-d H:i:s',$msgData['udate']) ;
				
				$query	= " INSERT INTO ".TABLE_PAYMENT_PARTY_BILLS
					." SET ".TABLE_PAYMENT_PARTY_BILLS .".number = '". $number ."'"  
					.",". TABLE_PAYMENT_PARTY_BILLS .".service_id  = '". $billData['service_id']."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".gl_type = '". $billData['gl_type'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".gl_type_name = '". $billData['gl_type_name'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".company_id = '". $billData['company_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_id = '". $billData['currency_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_abbr = '". $billData['currency_abbr'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_name = '". $billData['currency_name'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_symbol = '". $billData['currency_symbol'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_country = '". $billData['currency_country'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".exchange_rate = '". $billData['exchange_rate'] ."'"
					//.",". TABLE_PAYMENT_PARTY_BILLS .".tax_ids     = '". $billData['tax_ids'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".party_id = '". $billData['party_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".vendor_bank_id = '". $billData['vendor_bank_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".team_id = '". $billData['team_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_against_id = '". $billData['bill_against_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".executive_id = '". $billData['executive_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_dt = '". $bill_dt ."'"                           
					//.",". TABLE_PAYMENT_PARTY_BILLS .".pay_due_dt = '". $billData['pay_due_dt'] ."'"                    
					.",". TABLE_PAYMENT_PARTY_BILLS .".type = '". $billData['type'] ."'"
					.",".TABLE_PAYMENT_PARTY_BILLS.".bill_type='".$billData['bill_type']."'"                            
				    //.",".TABLE_PAYMENT_PARTY_BILLS .".amount = '". $billData['amount'] ."'"                            
					//.",".TABLE_PAYMENT_PARTY_BILLS .".balance = '". $billData['amount'] ."'"                            
					//.",". TABLE_PAYMENT_PARTY_BILLS .".round_off_op = '". $billData['round_off_op'] ."'"                    .",". TABLE_PAYMENT_PARTY_BILLS .".round_off = '". $data['round_off'] ."'"                		
					//.",". TABLE_PAYMENT_PARTY_BILLS .".bill_amount = '". $billData['bill_amount'] ."'"                    .",". TABLE_PAYMENT_PARTY_BILLS .".bill_amount_adjs = '". $data['bill_amount_adjs'] ."'"	                  .",". TABLE_PAYMENT_PARTY_BILLS .".bill_amount_perbill = '". $data['bill_amount_perbill'] ."'"
					//.",". TABLE_PAYMENT_PARTY_BILLS .".late_amount = '". $billData['late_amount'] ."'"                    .",". TABLE_PAYMENT_PARTY_BILLS .".late_pay_dt = '". $data['late_pay_dt'] ."'"
					//.",". TABLE_PAYMENT_PARTY_BILLS .".after_amount = '". $billData['after_amount'] ."'"                    .",". TABLE_PAYMENT_PARTY_BILLS .".after_pay_dt = '". $data['after_pay_dt'] ."'"
					//.",". TABLE_PAYMENT_PARTY_BILLS .".period_from = '". $billData['period_from'] ."'"
					//.",". TABLE_PAYMENT_PARTY_BILLS .".period_to = '". $billData['period_to'] ."'"
					//.",". TABLE_PAYMENT_PARTY_BILLS .".bill_details = '". $billData['bill_details'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_particulars = '". $billData['bill_particulars'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".access_level = '3'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".team             = '".$billData['team']."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".clients_su       = '".$billData['clients_su'] ."'"  
					.",". TABLE_PAYMENT_PARTY_BILLS .".status = '".PaymentPartyBills::ACTIVE."'"                    
					.",". TABLE_PAYMENT_PARTY_BILLS .".created_by_cron    = '1'"    
					.",". TABLE_PAYMENT_PARTY_BILLS .".headers_imap    = '".$msgData['headers_imap']."'"    
					.",". TABLE_PAYMENT_PARTY_BILLS .".content_imap    = '".$contentBody."'"    
					.",". TABLE_PAYMENT_PARTY_BILLS .".ip  = ''"    
					.",". TABLE_PAYMENT_PARTY_BILLS .".created_by = '".AUTO_USER_ID ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".created_by_name='". AUTO_USER_NAME ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".do_e   = '".date('Y-m-d H:i:s')."'";
				
				$db->query($query);
				$variables['hid'] = $db->last_inserted_id();   
				
				$filestore = DIR_FS_BILLS_FILES."/" ;
				$i=100;
				$fileNameStr ='';
				if(!empty($msgData['attachments'])){
					if(!empty($msgData['attachments'])){
						foreach($msgData['attachments'] as $key1=>$val1){
							$filedata["extension"]='';
							$filedata = pathinfo($key1);                   
							$ext = $filedata["extension"] ; 
							if(!empty($ext)){
								$filename = $number."-".$i.".".$ext ;
								$fileNameStr .=$filename."," ;
								$fp=fopen($filestore.$filename,"w+");
								fwrite($fp,$val1);
								 @chmod($filestore.$filename,0777);
								fclose($fp);
								$i++;
							}
						}                             
						$fileNameStr = trim($fileNameStr,",") ;
					}
					$sql="UPDATE ".TABLE_PAYMENT_PARTY_BILLS." SET ".TABLE_PAYMENT_PARTY_BILLS.".attachment_imap=
					'".$fileNameStr."' WHERE id=".$variables['hid'];
					$db->query($sql) ;
				}
			
				/* if(!empty($temp_p)){
					foreach($temp_p as $keyt=>$valt){
						$query3  = " INSERT INTO ". TABLE_PAYMENT_BILLS_P
						." SET ". TABLE_PAYMENT_BILLS_P .".particulars = 
						'".processUserData(trim($valt['particulars'],','))."'"
						.",". TABLE_PAYMENT_BILLS_P .".bill_id = '".  $variables['hid'] ."'"
						//.",". TABLE_PAYMENT_BILLS_P .".p_amount = '".  $valt['p_amount']  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".ss_div_id = '". $valt['ss_div_id'] ."'"
						.",". TABLE_PAYMENT_BILLS_P .".s_id = '". $valt['s_id']  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".sub_s_id = '". $valt['sub_s_id']  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".s_type = '".   $valt['s_type']  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".s_quantity = '". $valt['s_quantity']  ."'"
						//.",". TABLE_PAYMENT_BILLS_P .".s_amount = '".  $valt['s_amount']  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".ss_title = 
						'". processUserData($valt['ss_title'])  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".ss_punch_line	
						='".  processUserData($valt['ss_punch_line'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_id	
						= '".  processUserData($valt['tax1_id'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_number	
						= '".processUserData($valt['tax1_number'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_name	
						= '".processUserData($valt['tax1_name'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_value	
						= '".processUserData($valt['tax1_value']) ."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_pvalue	
						= '".processUserData($valt['tax1_pvalue'])  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_amount	
						= '".processUserData($valt['tax1_amount'])  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub1_id	
						= '".processUserData($valt['tax1_sub1_id_check'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub1_number
						= '".processUserData($valt['tax1_sub1_number'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub1_name	
						= '".processUserData($valt['tax1_sub1_name_str'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub1_value	
						= '". processUserData($valt['tax1_sub1_value']) ."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub1_pvalue	
						= '". processUserData($valt['tax1_sub1_pvalue'])  ."'"
						//.",". TABLE_PAYMENT_BILLS_P .".tax1_sub1_amount= '". processUserData($valt['tax1_sub1_amount'])  ."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub2_id	
						= '". processUserData($valt['tax1_sub2_id_check'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub2_number	
						= '". processUserData($valt['tax1_sub2_number'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub2_name	
						= '". processUserData($valt['tax1_sub2_name_str'])."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub2_value	
						= '". processUserData($valt['tax1_sub2_value']) ."'"
						.",". TABLE_PAYMENT_BILLS_P .".tax1_sub2_pvalue	
						= '". processUserData($valt['tax1_sub2_pvalue'])  ."'" ;
						//.",". TABLE_PAYMENT_BILLS_P .".tax1_sub2_amount= '". processUserData($valt['tax1_sub2_amount'])  ."'"
						//.",". TABLE_PAYMENT_BILLS_P .".d_amount = '". processUserData($valt['d_amount']) ."'"                                    
						//.",". TABLE_PAYMENT_BILLS_P .".stot_amount = '". processUserData($valt['stot_amount']) ."'"                                    
						//.",". TABLE_PAYMENT_BILLS_P .".tot_amount = '". processUserData($valt['tot_amount'])  ."'"  ;
                        $db->query($query3); 
					
					}
				} */
			}
					 
			
		}
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
	function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( empty($data['bill_dt']) && empty($data['profm_bill_dt'])) {
                $messages->setErrorMessage("Select Bill date or Proforma Bill date.");
            } 
            if ( empty($data['party_id']) && empty($data['vendor_bank_id']) && empty($data['team_id'])) {
                $messages->setErrorMessage("Please select Member or Account vendors or Associate.");                
            }
			
			if ( !empty($data['party_id']) && !empty($data['vendor_bank_id']) && !empty($data['team_id']) ) {
                $messages->setErrorMessage("Please select Member or Account vendors or Associate.");                
            }elseif(!empty($data['party_id']) && !empty($data['vendor_bank_id']) && empty($data['team_id'])){
				$messages->setErrorMessage("Please select Member or Account vendors or Associate.");
			}elseif(!empty($data['party_id']) && empty($data['vendor_bank_id']) && !empty($data['team_id'])){
				$messages->setErrorMessage("Please select Member or Account vendors or Associate.");
			}elseif(empty($data['party_id']) && !empty($data['vendor_bank_id']) && !empty($data['team_id'])){
				$messages->setErrorMessage("Please select Member or Account vendors or Associate.");
			}
			
			$data['pbill_no_chk'] = $data['profm_bill_no'];
			$data['bill_no_chk'] = $data['bill_no'];
			if(!empty($data['gl_type'])){
			
				if(!empty($data['bill_no'])){
					$data['bill_no_chk'] = "FL-".$data['bill_no'] ;
				}
			}
				
			if($data['type']==PaymentPartyBills::BILL){
				if ( (!isset($data['bill_no']) || empty($data['bill_no']) ) && empty($data['profm_bill_no'])) {
					$messages->setErrorMessage("Bill No can not be empty.");
				}else{
					$list = NULL;
					if(!empty($data['bill_no'])){ 
						if(!empty($data['party_id'])){				
						
							if ( PaymentPartyBills::getList($db, $list, TABLE_PAYMENT_PARTY_BILLS.".id ", 
							" WHERE ".TABLE_PAYMENT_PARTY_BILLS.".bill_no = '". trim($data['bill_no_chk']) 
							."' AND ".TABLE_PAYMENT_PARTY_BILLS.".party_id='".$data['party_id']."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".status='".PaymentPartyBills::ACTIVE."'" )>0 ){
								
								$messages->setErrorMessage("Bill no is already exists for the Selected 
								Member.<br/>Please try again.");
								//$messages->setErrorMessage("An Order with the same Number already exists.");
							}  
						}				
						if(!empty($data['vendor_bank_id'])){				
						
							if ( PaymentPartyBills::getList($db, $list, TABLE_PAYMENT_PARTY_BILLS.".id ", 
							" WHERE ".TABLE_PAYMENT_PARTY_BILLS.".bill_no = '". trim($data['bill_no_chk']) 
							."' AND ".TABLE_PAYMENT_PARTY_BILLS.".vendor_bank_id='".$data['vendor_bank_id']."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".status='".PaymentPartyBills::ACTIVE."'"  )>0 ){
								$messages->setErrorMessage("Bill no is already exists for the Selected 
								Account Vendor.
								<br/>Please try again.");
								//$messages->setErrorMessage("An Order with the same Number already exists.");
							}  
						}	
					}		
					if(!empty($data['profm_bill_no'])){ 
						if(!empty($data['party_id'])){				
						
							if ( PaymentPartyBills::getList($db, $list, TABLE_PAYMENT_PARTY_BILLS.".id ", 
							" WHERE ".TABLE_PAYMENT_PARTY_BILLS.".profm_bill_no = '". trim($data['pbill_no_chk']) 
							."' AND ".TABLE_PAYMENT_PARTY_BILLS.".party_id='".$data['party_id']."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".status='".PaymentPartyBills::ACTIVE."'"  )>0 ){
								
								$messages->setErrorMessage("Proforma Bill no is already exists for the Selected 
								Member.<br/>Please try again.");
								//$messages->setErrorMessage("An Order with the same Number already exists.");
							}  
						}				
						if(!empty($data['vendor_bank_id'])){				
						
							if ( PaymentPartyBills::getList($db, $list, TABLE_PAYMENT_PARTY_BILLS.".id ", 
							" WHERE ".TABLE_PAYMENT_PARTY_BILLS.".profm_bill_no = '". trim($data['pbill_no_chk']) 
							."' AND ".TABLE_PAYMENT_PARTY_BILLS.".vendor_bank_id='".$data['vendor_bank_id']."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".status='".PaymentPartyBills::ACTIVE."'"  )>0 ){
								$messages->setErrorMessage("Proforma Bill no is already exists for the Selected 
								Account Vendor.
								<br/>Please try again.");
								//$messages->setErrorMessage("An Order with the same Number already exists.");
							}  
						}	
					}
				} 
			}
			
            
			
			if(isset($data['particulars'])){
                
				$data['alltax_ids']= array();
                foreach ( $data['particulars'] as $key=>$particular ) {
				
					$data['sub_sid_list'][$key]= $data['subsid'][$key]=$ss_sub_id_details=array();					
					if(!empty($data['s_id'][$key])){
						$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id
						= ".$data['s_id'][$key]." 
						AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
						$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = 
						".$data['currency_id']." AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;

						$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
						TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
						$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",
						".TABLE_SETTINGS_SERVICES_PRICE." ".$condition_query_sid ; 
						 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

						if ( $db->query($query) ) {
							if ( $db->nf() > 0 ) {
								while ($db->next_record()) {
									$ss_subtitle =  trim($db->f("ss_title")).",";
									$ss_subprice =  $db->f("ss_price") ;
									$ss_subid =  $db->f("ss_id") ;
									$ss_sub_id_details[] = array(
										'ss_title'=>$ss_subtitle,
										'ss_price'=>$ss_subprice,
										'ss_id'=>$ss_subid	
									);
								}
							}
						} 
				    }
				    $data['sub_sid_list'][$key]=$ss_sub_id_details;				   
				    $sub_s_id_str='';
				    $sub_s_id_str =trim($data['subsidstr'][$key],",");
				    if(!empty($sub_s_id_str)){
						$data['subsid'][$key] = explode(",",$sub_s_id_str);
						$sub_s_id_str=",".$sub_s_id_str.",";
				    }
				   
					$tax1_id_str = $data['tax1_id'][$key] ;
					$tax1_sub1_id_str = $data['tax1_sub1_id'][$key] ;
					$tax1_sub2_id_str = $data['tax1_sub2_id'][$key] ;
					 
					if(!empty($tax1_id_str)){
						$arr = explode("#",$tax1_id_str);
						$data['tax1_name_str'][$key] = $arr[0] ;
						$data['tax1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_name_str'][$key] = 0 ;		
						$data['tax1_id_check'][$key] = 0 ;		
					}					
					if(!empty($data['tax1_value'][$key])){
						$tax1_val = $data['tax1_value'][$key] ;
						$percent =   substr($tax1_val,0,-1);
						$data['tax1_pvalue'][$key] = (float) ($percent/100);
					}else{
						$data['tax1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub1_value'][$key])){
						$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
						$percent_sub1 =   substr($tax1_sub1_val,0,-1);
						$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
					}else{
						$data['tax1_sub1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub2_value'][$key])){
						$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
						$percent_sub2 =   substr($tax1_sub2_val,0,-1);
						$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
					}else{
						$data['tax1_sub2_pvalue'][$key] =0;
					}
					
				    $data['tax_check_ids'][] = $data['tax1_id_check'][$key] ;
					$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					
					//$data['tax_check_ids'][] = $data['tax1_id'][$key] ;
					//$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					$data['tax1_sid_opt'][$key]=array();
					$data['tax1_sid_opt_count'][$key]=0;
					
					if($data['tax1_id_check'][$key]>0){
						//tax1_sub_id Options of taxes 
						//$data['tax1_sid_opt'][$key] = array('0'=>2);
						$tax_opt = array();
						$tax_id = $data['tax1_id_check'][$key];
						$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
						ORDER BY tax_name ASC ";
						ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
						if(!empty($tax_opt)){
							$data['tax1_sid_opt'][$key] = $tax_opt ;  
							$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
						}
					}
					if(!empty($tax1_sub1_id_str)){
						$arr = explode("#",$tax1_sub1_id_str);
						$data['tax1_sub1_name_str'][$key] = $arr[0] ;
						$data['tax1_sub1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub1_name_str'][$key] = 0 ;		
						$data['tax1_sub1_id_check'][$key] = 0 ;		
					}
					if(!empty($tax1_sub2_id_str)){
						$arr = explode("#",$tax1_sub2_id_str);
						$data['tax1_sub2_name_str'][$key] = $arr[0] ;
						$data['tax1_sub2_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub2_name_str'][$key] = 0 ;		
						$data['tax1_sub2_id_check'][$key] = 0 ;		
					}
					
					if(!empty($data['tax1_id_check'][$key])){
						$data['alltax_ids'][]=$data['tax1_id_check'][$key] ;	
					}		
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub1_id_check'][$key] ;
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub2_id_check'][$key] ;
					}
					
					//tax validation bof
					if( !empty($data['tax1_id_check'][$key] )){
						if(empty( $data['tax1_value'][$key])){
							$messages->setErrorMessage('Select Tax% at Position '. ($key+1));
						}							 
						if(empty( $data['tax1_amount'][$key]) || $data['tax1_amount'][$key]<=0){
							$messages->setErrorMessage('Tax amount is not entered at Position '. ($key+1));
						} 
					}
					if( !empty($data['tax1_value'][$key] )){
						if(empty( $data['tax1_id_check'][$key])){
							$messages->setErrorMessage('Select Tax at Position '. ($key+1));
						}
						if(empty( $data['tax1_amount'][$key])){
							$messages->setErrorMessage('Tax amount is not entered at Position '. ($key+1));
						}
					}
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						if(empty( $data['tax1_sub1_value'][$key])){
							$messages->setErrorMessage('Select Sub1Tax% at Position '. ($key+1));
						}
						if(empty( $data['tax1_sub1_amount'][$key])){
							$messages->setErrorMessage('Sub1Tax amount is not entered at Position '. ($key+1));
						}
					}
					if( !empty($data['tax1_sub1_value'][$key] )){
						if(empty( $data['tax1_sub1_id_check'][$key])){
							$messages->setErrorMessage('Select Sub1Tax at Position '. ($key+1));
						}
						if(empty( $data['tax1_sub1_amount'][$key])){
							$messages->setErrorMessage('Sub1Tax amount is not entered at Position '. ($key+1));
						}
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						if(empty( $data['tax1_sub2_value'][$key])){
							$messages->setErrorMessage('Select Sub2Tax% at Position '. ($key+1));
						}
						if(empty( $data['tax1_sub2_amount'][$key])){
							$messages->setErrorMessage('Sub2Tax amount is not entered at Position '. ($key+1));
						}
					}
					if( !empty($data['tax1_sub2_value'][$key] )){
						if(empty( $data['tax1_sub2_id_check'][$key])){
							$messages->setErrorMessage('Select Sub2Tax at Position '. ($key+1));
						}
						if(empty( $data['tax1_sub2_amount'][$key])){
							$messages->setErrorMessage('Sub2Tax amount is not entered at Position '. ($key+1));
						}
					}
					//tax validation eof
					
					if(empty($data['s_id'][$key])){
						$messages->setErrorMessage('Select the services.');
					}
                    				 	
					
                    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                     
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
                                
                                /*if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
                                    $messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
                                   
                                }elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){
                                
                                    $messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');
                                    
                                }else{
                                  
                                    if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || empty($data['do_fe']) ) ){
                                        $messages->setErrorMessage('Service '. $data['ss_title'][$key] .' 
										is Renewal Service. Please specify Service From Date &&  Renewal Date');
                                      
                                    }else{
                                       $is_part = true;
                                      
                                        $do_e  = "";
                                        $do_fe = "";  ;
                                        if($data['is_renewable'][$key] =='1'){
                                              $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                                              $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                                              $isrenewable = true;
                                        }
                                       
                                       
										$data['query_p'] .= "('". $data['number'] ."',
										'". processUserData(trim($data['particulars'][$key],',')) ."', 
										'". $data['p_amount'][$key] ."','". $data['ss_div_id'][$key] ."',
										'". $data['s_id'][$key] ."','".$sub_s_id_str."',
										'". processUserData($data['s_type'][$key]) ."', 
										'". processUserData($data['s_quantity'][$key]) ."',
										'". processUserData($data['s_amount'][$key]) ."',
										'". processUserData($data['ss_title'][$key]) ."', 
										'". processUserData($data['ss_punch_line'][$key]) ."',
										'". processUserData($data['tax1_id_check'][$key]) ."',
										'". processUserData($data['tax1_number'][$key]) ."',  
										'". processUserData($data['tax1_name_str'][$key]) ."', 
										'". processUserData($data['tax1_value'][$key]) ."',
										'". processUserData($data['tax1_pvalue'][$key]) ."',
										'". processUserData($data['tax1_amount'][$key]) ."',  
										'". processUserData($data['tax1_sub1_id_check'][$key]) ."',
										'". processUserData($data['tax1_sub1_number'][$key]) ."',  
										'". processUserData($data['tax1_sub1_name_str'][$key]) ."', 
										'". processUserData($data['tax1_sub1_value'][$key]) ."',
										'". processUserData($data['tax1_sub1_pvalue'][$key]) ."',
										'". processUserData($data['tax1_sub1_amount'][$key]) ."',					
										'". processUserData($data['tax1_sub2_id_check'][$key]) ."',
										'". processUserData($data['tax1_sub2_number'][$key]) ."',  
										'". processUserData($data['tax1_sub2_name_str'][$key]) ."', 
										'". processUserData($data['tax1_sub2_value'][$key]) ."',
										'". processUserData($data['tax1_sub2_pvalue'][$key]) ."',
										'". processUserData($data['tax1_sub2_amount'][$key]) ."',
										'". processUserData($data['d_amount'][$key]) ."', 
										'". processUserData($data['stot_amount'][$key]) ."',
										'". processUserData($data['tot_amount'][$key]) ."', 
										'". processUserData($data['discount_type'][$key]) ."', 
										'". processUserData($data['pp_amount'][$key]) ."',
										'". processUserData($data['vendor'][$key]) ."', 
										'". processUserData($data['vendor_name'][$key]) ."',
										'". processUserData($data['purchase_particulars'][$key]) ."',
										'". processUserData($data['is_renewable'][$key]) ."', '". $do_e ."','"
										.$do_fe."'
										)," ;
                                    }
                                }*/
                            }
                        }
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
			
			 
			$data['alltax_ids']=array_unique($data['alltax_ids']);
			
			
            if ( !isset($data['pay_due_dt']) || empty($data['pay_due_dt']) ) {
                $messages->setErrorMessage("Due Date can not be empty.");
            }
			 
			if(!empty($data['bill_amount'])){
                if(!is_numeric($data['bill_amount'])){
                    $messages->setErrorMessage("Due Date Amount should be numeric value.");
                }
            }else{
				$messages->setErrorMessage("Due Date Amount should not be blank.");
			}
			
			
			if(!is_numeric($data['bill_amount_perbill'])){
                $messages->setErrorMessage("Bill Amount/Current Charges ( As per the Bill) should be numeric value.");
            }
			if(!is_numeric($data['bill_amount_adjs'])){
                $messages->setErrorMessage("Bill Amount/Current Charges Approved by SMEERP <br/>
				should be numeric value.");
            }
            if ( !isset($data['bill_details']) || empty($data['bill_details']) ) {
                $messages->setErrorMessage("Bill Details can not be empty.");
            }  
            $data['bill_attachment'] = '';  		
            if((!empty($files['bill_attachment']) && (!empty($files['bill_attachment']['name'])))){
                
				$filename = $files['bill_attachment']['name'];
                $data['bill_attachment'] = $filename;
                $type = $files['bill_attachment']['type'];
                 $size = $files['bill_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage("Size of attachment file is greater than ".$data['max_file_size']." Kb");
                }
            }    
            
                  
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		function validateUpdateBill(&$data, $extra=''){
			foreach ($extra as $key=>$value) {
                $$key = $value;
            }  
			
			if((!empty($files['bill_attachment']) && (!empty($files['bill_attachment']['name'])))){                
				$filename = $files['bill_attachment']['name'];
              
                $type = $files['bill_attachment']['type'];
                $size = $files['bill_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
				 
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment file is not allowed");
                }
                 if($size > $max_size){
                    $messages->setErrorMessage("Size of attachment file is greater than ".$data['max_file_size']." Kb");
                }
            }else{
				$messages->setErrorMessage('Select file to upload');
			}  
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
			
		}
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            if ( empty($data['bill_dt']) && empty($data['profm_bill_dt'])) {
                $messages->setErrorMessage("Select Bill date or Proforma Bill date.");
            }            
            if ( empty($data['party_id']) && empty($data['vendor_bank_id']) && empty($data['team_id'])) {
                $messages->setErrorMessage("Please select Member or Account vendors or Associate.");                
            }			
			if ( !empty($data['party_id']) && !empty($data['vendor_bank_id']) && !empty($data['team_id']) ) {
                $messages->setErrorMessage("Please select Member or Account vendors or Associate.");                
            }elseif(!empty($data['party_id']) && !empty($data['vendor_bank_id']) && empty($data['team_id'])){
				$messages->setErrorMessage("Please select Member or Account vendors or Associate.");
			}elseif(!empty($data['party_id']) && empty($data['vendor_bank_id']) && !empty($data['team_id'])){
				$messages->setErrorMessage("Please select Member or Account vendors or Associate.");
			}elseif(empty($data['party_id']) && !empty($data['vendor_bank_id']) && !empty($data['team_id'])){
				$messages->setErrorMessage("Please select Member or Account vendors or Associate.");
			}
			$data['bill_no_chk'] = $data['bill_no'];
			$data['pbill_no_chk'] = $data['profm_bill_no'];
			if(!empty($data['gl_type'])){
			
				if(!empty($data['bill_no'])){
					$data['bill_no_chk'] = "FL-".$data['bill_no'] ;
				}
				if(!empty($data['profm_bill_no'])){
					$data['pbill_no_chk'] = "FL-".$data['profm_bill_no'] ;
				}
			}
				
			if($data['type']==PaymentPartyBills::BILL){
				if ( (!isset($data['bill_no']) || empty($data['bill_no']) ) && empty($data['profm_bill_no'])) {
					$messages->setErrorMessage("Bill No can not be empty.");
				}else{
					$list = NULL;
					if(!empty($data['bill_no'])){ 
						if(!empty($data['party_id'])){				
						
							if ( PaymentPartyBills::getList($db, $list, TABLE_PAYMENT_PARTY_BILLS.".id ", 
							" WHERE ".TABLE_PAYMENT_PARTY_BILLS.".bill_no = '". trim($data['bill_no_chk']) 
							."' AND ".TABLE_PAYMENT_PARTY_BILLS.".party_id='".$data['party_id']."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".status='".PaymentPartyBills::ACTIVE."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".id != ".$data['id'] )>0 ){
								
								$messages->setErrorMessage("Bill no is already exists for the Selected 
								Member.<br/>Please try again.");
								//$messages->setErrorMessage("An Order with the same Number already exists.");
							}  
						}				
						if(!empty($data['vendor_bank_id'])){				
						
							if ( PaymentPartyBills::getList($db, $list, TABLE_PAYMENT_PARTY_BILLS.".id ", 
							" WHERE ".TABLE_PAYMENT_PARTY_BILLS.".bill_no = '". trim($data['bill_no_chk']) 
							."' AND ".TABLE_PAYMENT_PARTY_BILLS.".vendor_bank_id='".$data['vendor_bank_id']."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".status='".PaymentPartyBills::ACTIVE."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".id != ".$data['id'] )>0 ){
								$messages->setErrorMessage("Bill no is already exists for the Selected 
								Account Vendor.
								<br/>Please try again.");
								//$messages->setErrorMessage("An Order with the same Number already exists.");
							}  
						}	
					}		
					if(!empty($data['profm_bill_no'])){ 
						if(!empty($data['party_id'])){				
						
							if ( PaymentPartyBills::getList($db, $list, TABLE_PAYMENT_PARTY_BILLS.".id ", 
							" WHERE ".TABLE_PAYMENT_PARTY_BILLS.".profm_bill_no = '". trim($data['pbill_no_chk']) 
							."' AND ".TABLE_PAYMENT_PARTY_BILLS.".party_id='".$data['party_id']."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".status='".PaymentPartyBills::ACTIVE."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".id != ".$data['id'] )>0 ){
								
								$messages->setErrorMessage("Proforma Bill no is already exists for the Selected 
								Member.<br/>Please try again.");
								//$messages->setErrorMessage("An Order with the same Number already exists.");
							}  
						}				
						if(!empty($data['vendor_bank_id'])){				
						
							if ( PaymentPartyBills::getList($db, $list, TABLE_PAYMENT_PARTY_BILLS.".id ", 
							" WHERE ".TABLE_PAYMENT_PARTY_BILLS.".profm_bill_no = '". trim($data['pbill_no_chk']) 
							."' AND ".TABLE_PAYMENT_PARTY_BILLS.".vendor_bank_id='".$data['vendor_bank_id']."' AND 
							".TABLE_PAYMENT_PARTY_BILLS.".status='".PaymentPartyBills::ACTIVE."'  AND 
							".TABLE_PAYMENT_PARTY_BILLS.".id != ".$data['id'] )>0 ){
								$messages->setErrorMessage("Proforma Bill no is already exists for the Selected 
								Account Vendor.
								<br/>Please try again.");
								//$messages->setErrorMessage("An Order with the same Number already exists.");
							}  
						}	
					}
				} 
			}	
            /*if(!is_numeric($data['bill_no'])){					
				 $messages->setErrorMessage("Bill no should be numeric value.");
			}*/
			 
			
            /*
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("Amount cannot be empty.");
            }else{
                if(!is_numeric($data['amount'])){					
                     $messages->setErrorMessage("Amount should be numeric value.");
                }
            }  */
                   
			if ( !isset($data['bill_details']) || empty($data['bill_details']) ) {
                $messages->setErrorMessage("Bill Details can not be empty.");
            }  
			
			/* if(isset($data['particulars'])){
				$data['alltax_ids']=array();
				  foreach ( $data['particulars'] as $key=>$particular ) {
						$data['particulars'][$key] =trim($data['particulars'][$key]);
						$tax1_id_str = $data['tax1_id'][$key] ;
						$tax1_sub1_id_str = isset($data['tax1_sub1_id'][$key]) ? $data['tax1_sub1_id'][$key] : '' ;
						$tax1_sub2_id_str = isset($data['tax1_sub2_id'][$key]) ? $data['tax1_sub2_id'][$key] : '' ;
						$data['p_id'][$key] = isset($data['p_id'][$key]) ? $data['p_id'][$key] : '' ;
						 
						
						if(!empty($tax1_id_str)){
							$arr = explode("#",$tax1_id_str);
							$data['tax1_name_str'][$key] = $arr[0] ;
							$data['tax1_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_name_str'][$key] = 0 ;		
							$data['tax1_id_check'][$key] = 0 ;		
						}
						
						if(!empty($data['tax1_value'][$key])){
							$tax1_val = $data['tax1_value'][$key] ;
							$percent =   substr($tax1_val,0,-1);
							$data['tax1_pvalue'][$key] = (float) ($percent/100);
						}else{
							$data['tax1_pvalue'][$key] =0;
						}
						if(!empty($data['tax1_sub1_value'][$key])){
							$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
							$percent_sub1 =   substr($tax1_sub1_val,0,-1);
							$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
						}else{
							$data['tax1_sub1_pvalue'][$key] =0;
						}
						if(!empty($data['tax1_sub2_value'][$key])){
							$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
							$percent_sub2 =   substr($tax1_sub2_val,0,-1);
							$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
						}else{
							$data['tax1_sub2_pvalue'][$key] =0;
						}
						
						$data['tax1_sid_opt'][$key]=array();
						$data['tax1_sid_opt_count'][$key]=0;
						if($data['tax1_id_check'][$key]>0){
							//tax1_sub_id Options of taxes 
							//$data['tax1_sid_opt'][$key] = array('0'=>2);
							$tax_opt = array();
							$tax_id = $data['tax1_id_check'][$key];
							$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
							ORDER BY tax_name ASC ";
							ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
							if(!empty($tax_opt)){
								$data['tax1_sid_opt'][$key] = $tax_opt ;  
								$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
							}
						}
						if(!empty($tax1_sub1_id_str)){
							$arr = explode("#",$tax1_sub1_id_str);
							$data['tax1_sub1_name_str'][$key] = $arr[0] ;
							$data['tax1_sub1_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_sub1_name_str'][$key] = 0 ;		
							$data['tax1_sub1_id_check'][$key] = 0 ;		
						}
						if(!empty($tax1_sub2_id_str)){
							$arr = explode("#",$tax1_sub2_id_str);
							$data['tax1_sub2_name_str'][$key] = $arr[0] ;
							$data['tax1_sub2_id_check'][$key] = $arr[1];	
						}else{
							$data['tax1_sub2_name_str'][$key] = 0 ;		
							$data['tax1_sub2_id_check'][$key] = 0 ;		
						}
						
						if(!empty($data['tax1_id_check'][$key])){
							$data['alltax_ids'][] = $data['tax1_id_check'][$key] ;	
						}		
						if(!empty($data['tax1_sub1_id_check'][$key])){					
							$data['alltax_ids'][] = $data['tax1_sub1_id_check'][$key] ;
						}
						if(!empty($data['tax1_sub2_id_check'][$key])){					
							$data['alltax_ids'][] = $data['tax1_sub2_id_check'][$key] ;
						}
						
					$counter =$key+1;
					if(!empty($data['tax1_id'][$key]) && empty($data['tax1_value'][$key]) ){
							$messages->setErrorMessage("Select Tax% for particular at position ".$counter);
					}
					if(empty($data['tax1_id'][$key]) && !empty($data['tax1_value'][$key]) ){
							$messages->setErrorMessage("Select Taxname for particular at position ".$counter);
					}
					
					if(!empty($data['tax1_sub1_id'][$key]) && empty($data['tax1_sub1_value'][$key]) ){
							$messages->setErrorMessage("Select Subtax1% for particular at position ".$counter);
					}
					if(empty($data['tax1_sub1_id'][$key]) && !empty($data['tax1_sub1_value'][$key]) ){
							$messages->setErrorMessage("Select Subtaxname1 for particular at position ".$counter);
					}
					if(!empty($data['tax1_sub2_id'][$key]) && empty($data['tax1_sub2_value'][$key]) ){
							$messages->setErrorMessage("Select Subtax2% for particular at position ".$counter);
					}
					if(empty($data['tax1_sub2_id'][$key]) && !empty($data['tax1_sub2_value'][$key]) ){
							$messages->setErrorMessage("Select Subtaxname2 for particular at position ".$counter);
					}
					if(empty($data['particulars'][$key])){
					
						$messages->setErrorMessage("Add Particulars for row ".$counter);
					}
					if(empty($data['p_amount'][$key])){
					
						$messages->setErrorMessage("Add Rate for row ".$counter);
					}
				    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key]) && empty($data['p_id'][$key])){
                         $messages->setErrorMessage('The Particular field and rate at Position '. ($key+1) .' cannot be empty.');
                    }
				}
			
			}else{
				 $messages->setErrorMessage("Please select Particulars of Bills.");
			} */
			
			
			if(isset($data['particulars'])){
                
				$data['alltax_ids']= array();
                foreach ( $data['particulars'] as $key=>$particular ) {
				
					$data['sub_sid_list'][$key]= $data['subsid'][$key]=$ss_sub_id_details=array();					
					if(!empty($data['s_id'][$key])){
						$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id
						= ".$data['s_id'][$key]." 
						AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
						$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = 
						".$data['currency_id']." AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;

						$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
						TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
						$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",
						".TABLE_SETTINGS_SERVICES_PRICE." ".$condition_query_sid ; 
						 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

						if ( $db->query($query) ) {
							if ( $db->nf() > 0 ) {
								while ($db->next_record()) {
									$ss_subtitle =  trim($db->f("ss_title")).",";
									$ss_subprice =  $db->f("ss_price") ;
									$ss_subid =  $db->f("ss_id") ;
									$ss_sub_id_details[] = array(
										'ss_title'=>$ss_subtitle,
										'ss_price'=>$ss_subprice,
										'ss_id'=>$ss_subid	
									);
								}
							}
						} 
				    }
				    $data['sub_sid_list'][$key]=$ss_sub_id_details;				   
				    $sub_s_id_str='';
				    $sub_s_id_str =trim($data['subsidstr'][$key],",");
				    if(!empty($sub_s_id_str)){
						$data['subsid'][$key] = explode(",",$sub_s_id_str);
						$sub_s_id_str=",".$sub_s_id_str.",";
				    }
				   
					$tax1_id_str = $data['tax1_id'][$key] ;
					$tax1_sub1_id_str = $data['tax1_sub1_id'][$key] ;
					$tax1_sub2_id_str = $data['tax1_sub2_id'][$key] ;
					 
					if(!empty($tax1_id_str)){
						$arr = explode("#",$tax1_id_str);
						$data['tax1_name_str'][$key] = $arr[0] ;
						$data['tax1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_name_str'][$key] = 0 ;		
						$data['tax1_id_check'][$key] = 0 ;		
					}					
					if(!empty($data['tax1_value'][$key])){
						$tax1_val = $data['tax1_value'][$key] ;
						$percent =   substr($tax1_val,0,-1);
						$data['tax1_pvalue'][$key] = (float) ($percent/100);
					}else{
						$data['tax1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub1_value'][$key])){
						$tax1_sub1_val = $data['tax1_sub1_value'][$key] ;
						$percent_sub1 =   substr($tax1_sub1_val,0,-1);
						$data['tax1_sub1_pvalue'][$key] = (float) ($percent_sub1/100);
					}else{
						$data['tax1_sub1_pvalue'][$key] =0;
					}
					if(!empty($data['tax1_sub2_value'][$key])){
						$tax1_sub2_val = $data['tax1_sub2_value'][$key] ;
						$percent_sub2 =   substr($tax1_sub2_val,0,-1);
						$data['tax1_sub2_pvalue'][$key] = (float) ($percent_sub2/100);
					}else{
						$data['tax1_sub2_pvalue'][$key] =0;
					}
					
				    $data['tax_check_ids'][] = $data['tax1_id_check'][$key] ;
					$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					
					//$data['tax_check_ids'][] = $data['tax1_id'][$key] ;
					//$data['taxpval_check'][] = $data['tax1_pvalue'][$key] ;
					$data['tax1_sid_opt'][$key]=array();
					$data['tax1_sid_opt_count'][$key]=0;
					
					if($data['tax1_id_check'][$key]>0){
						//tax1_sub_id Options of taxes 
						//$data['tax1_sid_opt'][$key] = array('0'=>2);
						$tax_opt = array();
						$tax_id = $data['tax1_id_check'][$key];
						$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
						ORDER BY tax_name ASC ";
						ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
						if(!empty($tax_opt)){
							$data['tax1_sid_opt'][$key] = $tax_opt ;  
							$data['tax1_sid_opt_count'][$key] = count($tax_opt) ;  
						}
					}
					if(!empty($tax1_sub1_id_str)){
						$arr = explode("#",$tax1_sub1_id_str);
						$data['tax1_sub1_name_str'][$key] = $arr[0] ;
						$data['tax1_sub1_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub1_name_str'][$key] = 0 ;		
						$data['tax1_sub1_id_check'][$key] = 0 ;		
					}
					if(!empty($tax1_sub2_id_str)){
						$arr = explode("#",$tax1_sub2_id_str);
						$data['tax1_sub2_name_str'][$key] = $arr[0] ;
						$data['tax1_sub2_id_check'][$key] = $arr[1];	
					}else{
						$data['tax1_sub2_name_str'][$key] = 0 ;		
						$data['tax1_sub2_id_check'][$key] = 0 ;		
					}
					
					if(!empty($data['tax1_id_check'][$key])){
						$data['alltax_ids'][]=$data['tax1_id_check'][$key] ;	
					}		
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub1_id_check'][$key] ;
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						$data['alltax_ids'][]=$data['tax1_sub2_id_check'][$key] ;
					}
					 
					//tax validation bof
					if( !empty($data['tax1_id_check'][$key] )){
						if(empty( $data['tax1_value'][$key])){
							$messages->setErrorMessage('Select Tax% at Position '. ($key+1));
						}							 
						if(empty( $data['tax1_amount'][$key]) || $data['tax1_amount'][$key]<=0){
							$messages->setErrorMessage('Tax amount is not entered at Position '. ($key+1));
						} 
					}
					if( !empty($data['tax1_value'][$key] )){
						if(empty( $data['tax1_id_check'][$key])){
							$messages->setErrorMessage('Select Tax at Position '. ($key+1));
						}
						if(empty( $data['tax1_amount'][$key])){
							$messages->setErrorMessage('Tax amount is not entered at Position '. ($key+1));
						}
					}
					if(!empty($data['tax1_sub1_id_check'][$key])){					
						if(empty( $data['tax1_sub1_value'][$key])){
							$messages->setErrorMessage('Select Sub1Tax% at Position '. ($key+1));
						}
						if(empty( $data['tax1_sub1_amount'][$key])){
							$messages->setErrorMessage('Sub1Tax amount is not entered at Position '. ($key+1));
						}
					}
					if( !empty($data['tax1_sub1_value'][$key] )){
						if(empty( $data['tax1_sub1_id_check'][$key])){
							$messages->setErrorMessage('Select Sub1Tax at Position '. ($key+1));
						}
						if(empty( $data['tax1_sub1_amount'][$key])){
							$messages->setErrorMessage('Sub1Tax amount is not entered at Position '. ($key+1));
						}
					}
					if(!empty($data['tax1_sub2_id_check'][$key])){					
						if(empty( $data['tax1_sub2_value'][$key])){
							$messages->setErrorMessage('Select Sub2Tax% at Position '. ($key+1));
						}
						if(empty( $data['tax1_sub2_amount'][$key])){
							$messages->setErrorMessage('Sub2Tax amount is not entered at Position '. ($key+1));
						}
					}
					if( !empty($data['tax1_sub2_value'][$key] )){
						if(empty( $data['tax1_sub2_id_check'][$key])){
							$messages->setErrorMessage('Select Sub2Tax at Position '. ($key+1));
						}
						if(empty( $data['tax1_sub2_amount'][$key])){
							$messages->setErrorMessage('Sub2Tax amount is not entered at Position '. ($key+1));
						}
					}
					//tax validation eof
					if(empty($data['s_id'][$key])){
						$messages->setErrorMessage('Select the services.');
					}
                    				 	
					
                    if(empty($data['particulars'][$key]) && empty($data['p_amount'][$key])){
                         $messages->setErrorMessage('The Particular field and amount at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                        if ( empty($data['particulars'][$key]) ) {
                            $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                        }
                        if ( empty($data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                        }
                        else {
                     
                            if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                                $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                            }
                            else {
                                
                                /*if(empty($data['s_type'][$key]) && !empty($data['s_quantity'][$key]) ){
                                    $messages->setErrorMessage('The Type of Particular '. ($key+1) .' is not valid.');
                                   
                                }elseif(!empty($data['s_type'][$key]) && empty($data['s_quantity'][$key])){
                                
                                    $messages->setErrorMessage('Quantity / Year for Particular '. ($key+1) .' is not valid.');
                                    
                                }else{
                                  
                                    if(  ($data['is_renewable'][$key] =='1') && (empty($data['do_e']) || empty($data['do_fe']) ) ){
                                        $messages->setErrorMessage('Service '. $data['ss_title'][$key] .' 
										is Renewal Service. Please specify Service From Date &&  Renewal Date');
                                      
                                    }else{
                                       $is_part = true;
                                      
                                        $do_e  = "";
                                        $do_fe = "";  ;
                                        if($data['is_renewable'][$key] =='1'){
                                              $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                                              $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                                              $isrenewable = true;
                                        }
                                       
                                       
										$data['query_p'] .= "('". $data['number'] ."',
										'". processUserData(trim($data['particulars'][$key],',')) ."', 
										'". $data['p_amount'][$key] ."','". $data['ss_div_id'][$key] ."',
										'". $data['s_id'][$key] ."','".$sub_s_id_str."',
										'". processUserData($data['s_type'][$key]) ."', 
										'". processUserData($data['s_quantity'][$key]) ."',
										'". processUserData($data['s_amount'][$key]) ."',
										'". processUserData($data['ss_title'][$key]) ."', 
										'". processUserData($data['ss_punch_line'][$key]) ."',
										'". processUserData($data['tax1_id_check'][$key]) ."',
										'". processUserData($data['tax1_number'][$key]) ."',  
										'". processUserData($data['tax1_name_str'][$key]) ."', 
										'". processUserData($data['tax1_value'][$key]) ."',
										'". processUserData($data['tax1_pvalue'][$key]) ."',
										'". processUserData($data['tax1_amount'][$key]) ."',  
										'". processUserData($data['tax1_sub1_id_check'][$key]) ."',
										'". processUserData($data['tax1_sub1_number'][$key]) ."',  
										'". processUserData($data['tax1_sub1_name_str'][$key]) ."', 
										'". processUserData($data['tax1_sub1_value'][$key]) ."',
										'". processUserData($data['tax1_sub1_pvalue'][$key]) ."',
										'". processUserData($data['tax1_sub1_amount'][$key]) ."',					
										'". processUserData($data['tax1_sub2_id_check'][$key]) ."',
										'". processUserData($data['tax1_sub2_number'][$key]) ."',  
										'". processUserData($data['tax1_sub2_name_str'][$key]) ."', 
										'". processUserData($data['tax1_sub2_value'][$key]) ."',
										'". processUserData($data['tax1_sub2_pvalue'][$key]) ."',
										'". processUserData($data['tax1_sub2_amount'][$key]) ."',
										'". processUserData($data['d_amount'][$key]) ."', 
										'". processUserData($data['stot_amount'][$key]) ."',
										'". processUserData($data['tot_amount'][$key]) ."', 
										'". processUserData($data['discount_type'][$key]) ."', 
										'". processUserData($data['pp_amount'][$key]) ."',
										'". processUserData($data['vendor'][$key]) ."', 
										'". processUserData($data['vendor_name'][$key]) ."',
										'". processUserData($data['purchase_particulars'][$key]) ."',
										'". processUserData($data['is_renewable'][$key]) ."', '". $do_e ."','"
										.$do_fe."'
										)," ;
                                    }
                                }*/
                            }
                        }
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
			
			 
			$data['alltax_ids']=array_unique($data['alltax_ids']);
			             
			if ( !isset($data['pay_due_dt']) || empty($data['pay_due_dt']) ) {
                $messages->setErrorMessage("Due Date can not be empty.");
            }
			 
			if(!empty($data['bill_amount'])){
                if(!is_numeric($data['bill_amount'])){
                    $messages->setErrorMessage("Due Date Amount should be numeric value.");
                }
            }else{
				$messages->setErrorMessage("Due Date Amount should not be blank.");
			}
    		if(!is_numeric($data['bill_amount_perbill'])){
                $messages->setErrorMessage("Bill Amount/Current Charges (As per the Bill) should be numeric value.");
            }
			
			if(!is_numeric($data['bill_amount_adjs'])){
                $messages->setErrorMessage("Bill Amount/Current Charges Approved by SMEERP <br/>
				should be numeric value.");
            }
            if((!empty($files['bill_attachment']) && (!empty($files['bill_attachment']['name'])))){                
				$filename = $files['bill_attachment']['name'];
              
                $type = $files['bill_attachment']['type'];
                $size = $files['bill_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
				 
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 300Kb');
                }
            }  
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PAYMENT_BILLS_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
            /**
         * This function is used to update the status of the Executive.
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, PaymentPartyBills::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid Status.");
			}
			else {
                $user = NULL;
                if ( (PaymentPartyBills::getDetails($db, $data, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
                    $data = $data[0];
                   // if ( $user['access_level'] < $access_level ) {
                      $query = "UPDATE ". TABLE_PAYMENT_PARTY_BILLS
                                    ." SET status = '$status_new'"
                                    ." WHERE id = '$id'";
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            $messages->setOkMessage("The Status has been updated.");
                        }
                        else {
                            $messages->setErrorMessage("The Status was not updated.");
                        }
                    /*}
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of Executives with the current Access Level.");
                    }*/
                }
                else {
                    $messages->setErrorMessage("Record was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

		
    }
?>
