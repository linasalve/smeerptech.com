<?php
	class Repository {
		
		const PENDING = 0;
		const COMPLETED  = 1;     
		const CANCELLED  = 2;     
		
        
		    
		const EMAILDOC  = 1; 
		const PRINTDOC  = 2; 
		
		const PET  = 1;   
		const PT  = 2;   
		const JCM  = 3;   
		const PJM  = 4;   
		const SJM  = 5;   
		const MJM  = 6;   
		const MALVIYANAGAR = 7; 
		const SWAMICOLONY  = 8; 
		const CHAKRADHARNGR= 9; 
		const DHAWALSARI= 10; 
		const GORWHA= 11; 
		
		
		
		
        function getStatus() {
			$status = array(
							'PENDING'   => Repository::PENDING,
                            'COMPLETED'    => Repository::COMPLETED,
                            'CANCELLED'    => Repository::CANCELLED
						);
			return ($status);
		}   
        function getType() {
			$status = array(
						Repository::PRINTDOC=> 'PRINT',
                        Repository::EMAILDOC=>'EMAIL' 
						);
			return ($status);
		}   
        function getDocOf() {
			$docOf = array(
							Repository::PET=>'PET',
                            Repository::PT=>'PT',
                            Repository::JCM=>'JCM',
                            Repository::PJM=>'PJM',
							Repository::SJM=>'SJM',
                            Repository::MJM=>'MJM',
                            Repository::MALVIYANAGAR=>'MALVIYANAGAR',
                            Repository::SWAMICOLONY=>'SWAMICOLONY',
                            Repository::CHAKRADHARNGR=>'CHAKRADHARNGR',
                            Repository::DHAWALSARI=>'DHAWALSARI',
                            Repository::GORWHA=>'GORWHA'
                            
						);
			return ($docOf);
		}   
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_REPOSITORY;
                    
            $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
       
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
               $query .= " COUNT(*) as count";
				$count = 1;
            }
            
           $query .= " FROM ". TABLE_REPOSITORY ;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
           
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
			    
            if ( (Repository::getList($db, $details, 
				TABLE_REPOSITORY.'.id,'.TABLE_REPOSITORY.'.file_1,'.TABLE_REPOSITORY.'.file_2,'
				.TABLE_REPOSITORY.'.file_3,'.TABLE_REPOSITORY.'.file_4,'.TABLE_REPOSITORY.'.file_5,'
				.TABLE_REPOSITORY.'.print_file_1,'.TABLE_REPOSITORY.'.print_file_2,'.TABLE_REPOSITORY.'.print_file_3,'
				.TABLE_REPOSITORY.'.print_file_4,'.TABLE_REPOSITORY.'.print_file_5'." WHERE ".TABLE_REPOSITORY.".id = '$id'")) > 0 ) {
                $details = $details[0];
				$query = "DELETE FROM ". TABLE_REPOSITORY
							." WHERE id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage("The Record has been deleted.");    
					if(!empty($details['file_1'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['file_1']);
					}
					if(!empty($details['file_2'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['file_2']);
					}
					if(!empty($details['file_3'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['file_3']);
					}
					if(!empty($details['file_4'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['file_4']);
					}
					if(!empty($details['file_5'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['file_5']);
					}
					
					if(!empty($details['print_file_1'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['print_file_1']);
					}
					if(!empty($details['print_file_2'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['print_file_2']);
					}
					if(!empty($details['print_file_3'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['print_file_3']);
					}
					if(!empty($details['print_file_4'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['print_file_4']);
					}
					if(!empty($details['print_file_5'])){
						@unlink(DIR_FS_REPOSITORY_FILES."/".$details['print_file_5']);
					}
				}
				else {
					$messages->setErrorMessage("The Record was not deleted.");
				}
            }else{
				$messages->setErrorMessage("The Record was not found.");			
			}  
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            $files = $_FILES;
			
            if(empty($data['filename'])){                
                 $messages->setErrorMessage("Please Enter the Filename.");                 
            }
			if(empty($data['subject'])){                
                 $messages->setErrorMessage("Please Enter the Subject.");                 
            }
		    /* 
			if(empty($data['text'])){                
                 $messages->setErrorMessage("Please Enter the Body.");                 
            } */
			$data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){
                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 1 is greater than 1Mb');
                }
            }
            $data['file_2'] = '';           
            if((!empty($files['file_2']['name']))){
                
                $filename = $files['file_2']['name'];
                $data['file_2'] = $filename;
                $type = $files['file_2']['type'];
                $size = $files['file_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 2 is greater than 1Mb');
                }
            }
			$data['file_3'] = '';           
            if((!empty($files['file_3']['name']))){
                
                $filename = $files['file_3']['name'];
                $data['file_3'] = $filename;
                $type = $files['file_3']['type'];
                $size = $files['file_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 3 is greater than 1Mb');
                }
            }
            $data['file_4'] = '';           
            if((!empty($files['file_4']['name']))){
                
                $filename = $files['file_4']['name'];
                $data['file_4'] = $filename;
                $type = $files['file_4']['type'];
                $size = $files['file_4']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 4 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 4 is greater than 1Mb');
                }
            }
            $data['file_5'] = '';           
            if((!empty( $files['file_5']['name'] ))){
                
                $filename = $files['file_5']['name'];
                $data['file_5'] = $filename;
                $type = $files['file_5']['type'];
                $size = $files['file_5']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 5 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 5 is greater than 1Mb');
                }
            }
			
			
			$data['print_file_1'] = '';           
            if((!empty($files['print_file_1']['name']))){
                
                $filename = $files['print_file_1']['name'];
                $data['print_file_1'] = $filename;
                $type = $files['print_file_1']['type'];
                $size = $files['print_file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 1 is greater than 1Mb');
                }
            }
            $data['print_file_2'] = '';           
            if((!empty($files['print_file_2']['name']))){
                
                $filename = $files['print_file_2']['name'];
                $data['print_file_2'] = $filename;
                $type = $files['print_file_2']['type'];
                $size = $files['print_file_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 2 is greater than 1Mb');
                }
            }
			$data['print_file_3'] = '';           
            if((!empty($files['print_file_3']['name']))){
                
                $filename = $files['print_file_3']['name'];
                $data['print_file_3'] = $filename;
                $type = $files['print_file_3']['type'];
                $size = $files['print_file_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 3 is greater than 1Mb');
                }
            }
            $data['print_file_4'] = '';           
            if((!empty($files['print_file_4']['name']))){
                
                $filename = $files['print_file_4']['name'];
                $data['print_file_4'] = $filename;
                $type = $files['print_file_4']['type'];
                $size = $files['print_file_4']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 4 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 4 is greater than 1Mb');
                }
            }
            $data['print_file_5'] = '';           
            if((!empty( $files['print_file_5']['name'] ))){
                
                $filename = $files['print_file_5']['name'];
                $data['print_file_5'] = $filename;
                $type = $files['print_file_5']['type'];
                $size = $files['print_file_5']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 5 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 5 is greater than 1Mb');
                }
            }
			
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                   $flname=$val["name"];
                   $fltype=$val["type"];
                    if(!empty($flname) && $flname!='' && !empty($fltype)){
					
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of all files is greater than 5Mb');
            } 
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
           
            $files = $_FILES;
			
            if(empty($data['filename'])){                
                 $messages->setErrorMessage("Please Enter the Filename.");                 
            }
			if(empty($data['subject'])){                
                 $messages->setErrorMessage("Please Enter the Subject.");                 
            }
		    /* if(empty($data['text'])){                
                 $messages->setErrorMessage("Please Enter the Body.");                 
            } */
			$data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){
                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 1 is greater than 1Mb');
                }
            }
            $data['file_2'] = '';           
            if((!empty($files['file_2']['name']))){
                
                $filename = $files['file_2']['name'];
                $data['file_2'] = $filename;
                $type = $files['file_2']['type'];
                $size = $files['file_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 2 is greater than 1Mb');
                }
            }
			$data['file_3'] = '';           
            if((!empty($files['file_3']['name']))){
                
                $filename = $files['file_3']['name'];
                $data['file_3'] = $filename;
                $type = $files['file_3']['type'];
                $size = $files['file_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 3 is greater than 1Mb');
                }
            }
            $data['file_4'] = '';           
            if((!empty($files['file_4']['name']))){
                
                $filename = $files['file_4']['name'];
                $data['file_4'] = $filename;
                $type = $files['file_4']['type'];
                $size = $files['file_4']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 4 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 4 is greater than 1Mb');
                }
            }
            $data['file_5'] = '';           
            if((!empty( $files['file_5']['name'] ))){ 
                $filename = $files['file_5']['name'];
                $data['file_5'] = $filename;
                $type = $files['file_5']['type'];
                $size = $files['file_5']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 5 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 5 is greater than 1Mb');
                }
            } 
			
			$data['print_file_1'] = '';           
            if((!empty($files['print_file_1']['name']))){
                
                $filename = $files['print_file_1']['name'];
                $data['print_file_1'] = $filename;
                $type = $files['print_file_1']['type'];
                $size = $files['print_file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 1 is greater than 1Mb');
                }
            }
            $data['print_file_2'] = '';           
            if((!empty($files['print_file_2']['name']))){
                
                $filename = $files['print_file_2']['name'];
                $data['print_file_2'] = $filename;
                $type = $files['print_file_2']['type'];
                $size = $files['print_file_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 2 is greater than 1Mb');
                }
            }
			$data['print_file_3'] = '';           
            if((!empty($files['print_file_3']['name']))){
                
                $filename = $files['print_file_3']['name'];
                $data['print_file_3'] = $filename;
                $type = $files['print_file_3']['type'];
                $size = $files['print_file_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 3 is greater than 1Mb');
                }
            }
            $data['print_file_4'] = '';           
            if((!empty($files['print_file_4']['name']))){
                
                $filename = $files['print_file_4']['name'];
                $data['print_file_4'] = $filename;
                $type = $files['print_file_4']['type'];
                $size = $files['print_file_4']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 4 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 4 is greater than 1Mb');
                }
            }
            $data['print_file_5'] = '';           
            if((!empty( $files['print_file_5']['name'] ))){
                
                $filename = $files['print_file_5']['name'];
                $data['print_file_5'] = $filename;
                $type = $files['print_file_5']['type'];
                $size = $files['print_file_5']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of print_file 5 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of print_file 5 is greater than 1Mb');
                }
            }
			
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                   $flname=$val["name"];
                   $fltype=$val["type"];
                    if(!empty($flname) && $flname!='' && !empty($fltype)){					
                        if( !in_array($val["type"], $data["allowed_file_types"] ) ){
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of all files is greater than 5Mb');
            }                         
            
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
    
         /**
         * This function is used to send a mail
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function validateSendmail(&$data, $extra='') {
             foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email should be provided.');
            }
            elseif ( !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('Email Address is not valid.');
            }
            
			if(empty($data['type']) ){
				 $messages->setErrorMessage('Select EMAIL/PRINT.');
			}
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

		
    }
?>
