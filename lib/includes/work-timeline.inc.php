<?php 

	class WorkTimeline {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => WorkTimeline::BLOCKED,
							'ACTIVE'  => WorkTimeline::ACTIVE,
							'PENDING' => WorkTimeline::PENDING,
							'DELETED' => WorkTimeline::DELETED
						);
			return ($status);
		}
		
        const DESIGNER      = 1;
		const PROGRAMMER    = 2;
		const TESTER        = 3;
		const CLIENTINFO    = 4;
        
        function getSrsType() {
			$srsType = array('Designer' => WorkTimeline::DESIGNER,
							'Programmer'  => WorkTimeline::PROGRAMMER,
							'Tester' => WorkTimeline::TESTER,
							'Client/Information' => WorkTimeline::CLIENTINFO
						);
			return ($srsType);
		}
        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_WORK_TL
			        ." LEFT JOIN ". TABLE_BILL_ORDERS
                ." ON ". TABLE_WORK_TL .".order_id = ". TABLE_BILL_ORDERS .".id " ;
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
           
		//echo "<br/><br/>". $query;
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}



		/**
	     * This function is used to display the list of the Order Timelines.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getDisplayList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='') {

			if ( $required_fields == '' ) {
				$required_fields = 	TABLE_WORK_TL .".id "
									.",". TABLE_WORK_TL .".order_id "
									.",". TABLE_WORK_TL .".order_no "
									.",". TABLE_WORK_TL .".by_id "
									.",". TABLE_WORK_TL .".to_id "
									.",". TABLE_WORK_TL .".do_assign "
									.",". TABLE_WORK_TL .".status ";
			}		
			if ( !isset($condition) || $condition == "" ) {
				$condition	= " ORDER BY ".TABLE_WORK_TL.".do_assign DESC ";
			}
			
			$list	= NULL;
			WorkTimeline::getList( $db, $list, $required_fields, $condition, $from, $rpp );
			if(!empty($list)){
                foreach ( $list as $key=>$timeline ) {
                    include_once ( DIR_FS_INCLUDES .'/work-stage.inc.php');
                    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
                    
                    $to_id = '';
                    User::getUserDetails($db, $to_id, $timeline['to_id'], 'user_id, f_name, l_name, number');
                    $by_id = '';
                    User::getUserDetails($db, $by_id, $timeline['by_id'], 'user_id, f_name, l_name, number');
                    $list[$key]['to_name'] 	= $to_id;
                    $list[$key]['by_name'] 	= $by_id;
                    $list[$key]['status'] 	= WorkStage::getWorkStageTitle($db, $timeline['status']);
                }
            }
			return ( count($list) );
		}
        
		
		/**
	     * This function is used to retrieve the last comment that was added for the Order Timelines.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the data will be stored.
		 * @param mixed   string/array containing fields wich are to be retrieved.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getLastComment( &$db, &$list, $order_id, $required_fields='') {

			if ( $required_fields == '' ) {
				$required_fields = 	TABLE_WORK_TL .".id "
									.",". TABLE_WORK_TL .".order_id "
									.",". TABLE_WORK_TL .".order_no "
									.",". TABLE_WORK_TL .".by_id "
									.",". TABLE_WORK_TL .".to_id "
									.",". TABLE_WORK_TL .".do_assign "
									.",". TABLE_WORK_TL .".status ";
			}		
			$condition	= " WHERE ".TABLE_WORK_TL.".order_id = '$order_id' AND (".TABLE_WORK_TL.".forward_to = '' || ".TABLE_WORK_TL.".forward_to = '0') ORDER BY ".TABLE_WORK_TL.".do_assign DESC ";
			
			$list	= NULL;
			if ( WorkTimeline::getList( $db, $list, $required_fields, $condition) > 0 ) {
				include_once ( DIR_FS_INCLUDES .'/work-stage.inc.php');
				
				foreach ( $list as $key=>$timeline ) {
					//include_once ( DIR_FS_INCLUDES .'/user.inc.php');
	//				
	//				$to_id = '';
	//				User::getUserDetails($db, $to_id, $timeline['to_id'], 'user_id, f_name, l_name, number');
	//				$by_id = '';
	//				User::getUserDetails($db, $by_id, $timeline['by_id'], 'user_id, f_name, l_name, number');
	//				$list[$key]['to_name'] 	= $to_id;
	//				$list[$key]['by_name'] 	= $by_id;
					$list[$key]['status'] 	= WorkStage::getWorkStageTitle($db, $timeline['status']);
				}
			}
			return ( count($list) );
		}
		
        
        /**
		 * Function to validate the data supplied for adding a new Order Status.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			// Validate the stage.
			$index = array();
			array_key_search('id', $data['work_stage'], $lst_work_stage, $index);
			if ( count($index) == 0 ) {
				$messages->setErrorMessage('The selected Stage is not a valid stage.');
			}

			// Validate the Member.
// This is not required when adding the first comment, i.e. while added in the Order.
//			$index = array();
//			array_key_search('user_id', $data['to_id'], $work_details['team'], $index);
//			if ( count($index) == 0 ) {
//				$messages->setErrorMessage('The selected User is not a Member of this order.');
//			}
			/*
			if ( !isset($data['order_title']) || empty($data['order_title']) ) {
                $messages->setErrorMessage('Work Order Title cannot be empty, please enter Work Order Title.');
			}*/
			
			if ( !isset($data['work_comments']) || empty($data['work_comments']) ) {
                $messages->setErrorMessage('Comments cannot be empty, Please enter comments.');
			}
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating the Order Status.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the service title 
            if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage('Title should be provided.');
			}
			elseif ( strlen($data['title'])>100 ) {
				$messages->setErrorMessage('The Title can only be 100 characters long.');
			}
			else {
				// Check for duplicate Status title.
				$order_status = NULL;
				$condition_query = " WHERE title = '". $data["title"] ."'"
									." AND id != '". $data['id'] ."'";
				if ( (WorkTimeline::getList($db, $order_status, 'title', $condition_query)) > 0 ) {
					$messages->setErrorMessage('The Title already exists. Please enter some other title.');
				}
			}

            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}

        
		function validateForwardWork(&$data, $extra ) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			// Validate the stage.
			/*$index = array();
			array_key_search('id', $data['status'], $lst_work_stage, $index);
			if ( count($index) == 0 ) {
				$messages->setErrorMessage('The selected Stage is not a valid stage.');
			}*/

			// Validate the Member.
			$index = array();
			array_key_search('user_id', $data['to_id'], $work_details['team'], $index);
			if ( count($index) == 0 ) {
				$messages->setErrorMessage('The selected User is not a Member of this order.');
			}
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                   if(!empty($val["name"])){
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
          
           
           
            if($fileSize > $data['max_file_size']){
                 $messages->setErrorMessage('Size of all files is greater than 2Mb');
            }
            
            
			if ( !isset($data['comments']) || empty($data['comments']) ) {
                $messages->setErrorMessage('Comments cannot be empty, Please enter comments.');
			}
          
			if ( $messages->getErrorMessageCount() <= 0 ) {
                
				return (true);
			}
			else {
				return (false);
			}
		
		}

		
		
        /**
         * This function is used to update the status of the Order Status.
         *
         * @param    string      unique ID of the Order Status whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, WorkTimeline::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $order_status = NULL;
                
                if ( (WorkTimeline::getList($db, $order_status, TABLE_WORK_TL.'.id', " WHERE ".TABLE_WORK_TL.".id = '$id'")) > 0 ) {
                    $order_status = $order_status[0];
					$query = "UPDATE ". TABLE_WORK_TL
								." SET status = '$status_new' "
								." WHERE id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Order Status was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Order Status information from the database.
        *
        * @param   string      unique ID of the Order Status whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $order_status = NULL;
            if ( (WorkTimeline::getList($db, $order_status, TABLE_WORK_TL.".id, ".TABLE_WORK_TL.".status ", " WHERE ".TABLE_WORK_TL.".id = '$id'")) > 0 ) 
			{
                $order_status = $order_status[0];
				// Check if the Order Status has been assigned to any Order Timeline?
				$query = "SELECT id FROM ". TABLE_BILL_ORDERS_TL
							." WHERE status = '". $id ."'";
				if ( $db->query($query) ) {
					if ( $db->nf()>0 ) {
						$messages->setErrorMessage("The Order Status cannot be deleted, it has been assigned to a Order Timeline.");
					}
					else {
						if ( $order_status['status'] == WorkTimeline::BLOCKED || $order_status['status'] == WorkTimeline::DELETED) 
						{
							$query = "DELETE FROM ". TABLE_WORK_TL
										." WHERE id = '". $id ."'";
							if ( !$db->query($query) || $db->affected_rows()<=0 ) {
								$messages->setErrorMessage("The Order Status is not deleted.");
							}
							else {
								$messages->setOkMessage("The Order Status has been deleted.");
							}
						}
						else {
							$messages->setErrorMessage("Cannot delete Order Status.");
						}
					}
				}
				else {
					$messages->setErrorMessage("Your request cannot be processed right now. Please try again later.");
				}
			}
			else {
				$messages->setErrorMessage("Order Status not found.");
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}

		
		
		/**
		 * This function is used to retrieve the list of the Work Stage.
		 * for using.
		 *
		 */
		function getWorkTimelines(&$db, &$status) {
            
			WorkTimeline::getList($db, $status, 'id, title', " WHERE status='1' ORDER BY status_order ");
			
			return ($status);
		}
		
		/**
		 * This function is used to retrieve the Details about the Work Timeline 
		 * alongwith the Team Members.
		 *
		 * @param	id		bigint	the unique ID of the timeline record whose details are to be retrieved.
		 * @param	details	array	reference to the array in which the information will be stored.
		 * @param	extra	mixed	mixed array having the entities required in this function.
		 *							db = &$db				reference to the database class.
		 *							messages = $messages	reference to the messages class.
		 *							
		 *
		 */
		function getWorkDetails($id, &$details, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$details = NULL;
            
            $fields = TABLE_WORK_TL.".order_id,".TABLE_WORK_TL.".order_no ,".TABLE_WORK_TL.".to_id, ".TABLE_WORK_TL.".status, ".TABLE_BILL_ORDERS.".order_title " ;
			if ( WorkTimeline::getList($db, $details, $fields, " WHERE ".TABLE_WORK_TL.".id='$id'") ) {
				$details = $details[0];
				
				// Retrieve the Members of the Order using the Order ID.
				include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
				if ( Order::getTeamMembers($details['order_id'], $details['team'], $extra) ) {
					return (true);
				}
			}
			return (false);
		}
        
        function getOrderDetails($order_id, &$details, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            $details = NULL;
            include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
            $fields = TABLE_BILL_ORDERS.".id as order_id,".TABLE_BILL_ORDERS.".number as order_no,".TABLE_BILL_ORDERS.".status,".TABLE_BILL_ORDERS.".order_title " ;
            Order::getList($db, $details, $fields, " WHERE ".TABLE_BILL_ORDERS.".id='$order_id'");
            $details = $details[0];
            if(!empty($details)){
                if ( Order::getTeamMembers($order_id, $details['team'], $extra) ) {
                    return (true);
                }else{
                    return (false);
                }
            }
		}
		
        
		
		/**
		 * This function is used to retrieve the SRS Details about the Work Order. 
		 *
		 * @param	order_id	bigint	the unique ID of the Order record whose SRS are to be retrieved.
		 * @param	srs			array	reference to the array in which the information will be stored.
		 * @param	extra		mixed	mixed array having the entities required in this function.
		 *								db = &$db				reference to the database class.
		 *								messages = $messages	reference to the messages class.
		 *							
		 *
		 */
		function getSRS($order_id, &$srs, $extra, $condition_query='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$query = "SELECT * FROM ". TABLE_WORK_SRS
						." WHERE order_id = '$order_id' ";
			
			$query .= " ". $condition_query;
            $db->query($query);
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ) {
					$srs[] = processSQLData($db->result());
				}
			}
			else {
				$messages->setErrorMessage("The SRS was not found.");
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}		
		}
		
		
		/**
		 * This function is used to make the entries for the SRS against the Order. 
		 *
		 * @param	order_id	bigint	the unique ID of the Order record whose SRS are to be retrieved.
		 * @param	srs			array	reference to the array in which the information will be stored.
		 * @param	extra		mixed	mixed array having the entities required in this function.
		 *								db = &$db				reference to the database class.
		 *								messages = $messages	reference to the messages class.
		 *							
		 *
		 */
		function setSRS($data, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
//	no need to check here, already checked in the .		
//			// Check if the Order ID and SRS text is present or not.
//			if ( !isset($data['order_id']) || empty($data['order_id']) ) {
//				$messages->setErrorMessage("Cannot save SRS: The Order ID is empty.");
//			}
//			else {
//				// Check if the Order ID exists or not?
//			}
			
            if ( !isset($data['srs_type_id']) || empty($data['srs_type_id']) ) {
				$messages->setErrorMessage("Please select SRS type.");
			}
            
			if ( !isset($data['srs_text']) || empty($data['srs_text']) ) {
				$messages->setErrorMessage("Please enter the text for the SRS.");
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				$query = "INSERT INTO ". TABLE_WORK_SRS
							." SET "
								." order_id = '". $data['order_id'] ."'"
								.",added_by = '". $my['user_id'] ."'"
                                .",srs_type = '". $data['srs_type_id'] ."'"
								.",srs_text = '". $data['srs_text'] ."'"
                                .",ip       = '". $_SERVER['REMOTE_ADDR'] ."'"
								.",do_a 	= '". date('Y-m-d H:i:s', time()) ."'"
							;
				if ( $db->query($query) && ($db->affected_rows() > 0) ) {
					$messages->setOkMessage("SRS was saved.");
				}
				else {
					$messages->setErrorMessage("SRS was not saved, please try again later.");
				}
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}		
		}
		
    }
?>