<?php 

	class Prospects {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;
		
	
        
        const SENDMAILNO =0;
        const SENDMAILYES =1;
        
        const GOLD  = 1;
		const PLATINUM = 2;
		const SILVER = 3;
        
        const BILLING  = 1;
		const ST = 2;
		const SMS = 3;
        const ETASK = 4;
        const FIREQUERY = 5;
        
        const BYPHONE=1;
        const BYSMS=2;
        const BYMAIL=3;
        const BYVISIT=4;
	
	//const for authorisation bof
	const ACCOUNTS = 1;
    const MANAGEMENT = 2;
    const CONFIDENTIAL = 3;        
	//const for authorisation eof 
    //const ADDUSER = 5;
	
	function getFinancialStatus(){
		$arrF = array(
			1=>'0 Lacs to 10 Lacs',
			2=>'11 Lacs to 50 Lacs',
			3=>'51 Lacs to 99 Lacs',
			4=>'1 Cr to 5 Cr',
			5=>'6 Cr to 10 Cr',
			6=>'11 Cr to 50 Cr',
			7=>'51 Cr to 100 Cr',
			8=>'101 Cr to 500 Cr',
			9=>'501 Cr to 1000 Cr',
			10=>'1001 Cr to 3000 Cr',
			11=>'3001 Cr and above'
		);
	
		return ($arrF);
	}
	
	function getStatus() {
		$status = array('BLOCKED' => Prospects::BLOCKED,
				'ACTIVE'  => Prospects::ACTIVE,
				'PENDING' => Prospects::PENDING,
				//'DELETED' => Prospects::DELETED
				);
		return ($status);
	}
		
	function getAuthorization() {
		$authority = array(
			'ACCOUNTS' => Prospects::ACCOUNTS,
			'MANAGEMENT'  => Prospects::MANAGEMENT,
			'CONFIDENTIAL' => Prospects::CONFIDENTIAL
		);
		return ($authority);
	}
        
	function getMailStatus() {
		$mail_status = array('Send Mail' => Prospects::SENDMAILNO,
						'Dont Send Mail'  => Prospects::SENDMAILYES,
					);
		return ($mail_status);
	}
	
	function getGrades() {
		$grades = array('GOLD' => Prospects::GOLD,
						'PLATINUM'  => Prospects::PLATINUM,
						'SILVER' => Prospects::SILVER
					);
		return ($grades);
	}
        
        function getServices() {
			$services = array('Billing'         => Prospects::BILLING,
							'Support Ticket'  => Prospects::ST,
							'Sms'             => Prospects::SMS,
                            'eTask'           => Prospects::ETASK,
                            'Fire Query'    => Prospects::FIREQUERY
                            //'Add User'           => Prospects::ADDUSER
						);
			return ($services);
		}
		
        function getTitleType() {
			$titletype = array('Mr.' => 'Mr.',
							'Ms.'  => 'Ms.',
							'Mrs.'  => 'Mrs.',
							'Prof.'  => 'Prof.',
							'Dr.'  =>'Dr.'
						);
			return ($titletype);
		}
        
        function getFollowupType() {
			$titletype = array(Prospects::BYPHONE => 'By Phone',
							   Prospects::BYVISIT  => 'By Visit'
						);
			return ($titletype);
		}
        
        function getCountryCode( &$db, &$country_code){			
            
            $query= "SELECT * FROM ". TABLE_COUNTRIES 
					." WHERE ". TABLE_COUNTRIES .".status='1' ";
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$country_code[]= processSqlData($db->result());
					}
				}	                
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PROSPECTS;
			
            $query .= " ". $condition;
	  
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				 return ( $total );
			}
			else {
				return false;
			}	
		}
       
       

        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Prospects::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Prospects::getList($db, $user, 'user_id, access_level', " WHERE user_id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_PROSPECTS
								." SET status = '$status_new' "
								." WHERE user_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) {
						$messages->setOkMessage("The Status has been updated.");
					}
					else {
						$messages->setErrorMessage("The Status was not updated.");
					}
                     
                }
                else {
                    $messages->setErrorMessage("The Client was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (Prospects::getList($db, $user, 'user_id, status, access_level', " WHERE user_id = '$id'")) > 0 ) {
                $user = $user[0];
                if ( $user['status'] == Prospects::BLOCKED || $user['status'] == Prospects::DELETED) {                   
					// Delete the Reminders set for the Client.
					$query = "SELECT id FROM ". TABLE_PROSPECTS_REMINDERS
								." WHERE table_name = '". TABLE_PROSPECTS ."'"
								." AND reminder_for = '". $id ."'";
					if ( $db->query($query) ) {
						if ( $db->nf()>0 ) {
							$query = "DELETE FROM ". TABLE_PROSPECTS_REMINDERS
										." WHERE table_name = '". TABLE_PROSPECTS ."'"
										." AND reminder_for = '". $id ."'";
							if ( !$db->query($query) || $db->affected_rows()<=0 ) {
								$messages->setErrorMessage("The Reminders for the Client were not deleted.");
							}
						}
					}
					else {
						$messages->setErrorMessage("The Reminders for the Client were not deleted.");
					}
					
					// Delete the Contact Numbers set for the Client.
					$query = "SELECT id FROM ". TABLE_PROSPECTS_PHONE
								." WHERE table_name = '". TABLE_PROSPECTS ."'"
								." AND phone_of = '". $id ."'";
					if ( $db->query($query) ) {
						if ( $db->nf()>0 ) {
							$query = "DELETE FROM ". TABLE_PROSPECTS_PHONE
										." WHERE table_name = '". TABLE_PROSPECTS ."'"
										." AND phone_of = '". $id ."'";
							if ( !$db->query($query) || $db->affected_rows()<=0 ) {
								$messages->setErrorMessage("The Contact Numbers for the Client were not deleted.");
							}
						}
					}
					else {
						$messages->setErrorMessage("The Contact Numbers for the Client were not deleted.");
					}

					// Delete the Address's of the Client.
					$query = "SELECT id FROM ". TABLE_PROSPECTS_ADDRESS
								." WHERE table_name = '". TABLE_PROSPECTS ."'"
								." AND address_of = '". $id ."'";
					if ( $db->query($query) ) {
						if ( $db->nf()>0 ) {
							$query = "DELETE FROM ". TABLE_PROSPECTS_ADDRESS
										." WHERE table_name = '". TABLE_PROSPECTS ."'"
										." AND address_of = '". $id ."'";
							if ( !$db->query($query) || $db->affected_rows()<=0 ) {
								$messages->setErrorMessage("The Address for the Client were not deleted.");
							}
						}
					}
					else {
						$messages->setErrorMessage("The Address for the Client were not deleted.");
					}
 
					if ( $messages->getErrorMessageCount() <= 0 ) {
						// Delete the Client itself.
						$query = "DELETE FROM ". TABLE_PROSPECTS 
									." WHERE user_id = '". $id ."'";
						if ( $db->query($query) && $db->affected_rows()>0 ) {
							$messages->setOkMessage("The Client has been deleted.");
						}
						else {
							$messages->setErrorMessage("The Client was not deleted.");
						}
					}
                }
                else {
                    $messages->setErrorMessage("To delete the Client, he/she has to be Blocked.");
                }
            }
            else {
                $messages->setErrorMessage("The record for the Client was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateInviteClient(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            $defaultPwd ='smeerp';
            $data['password'] = $defaultPwd ;
           
            // Password.
            /*          
            if ( !isset($data['password']) || empty($data['password']) 
                    || !isset($data['re_password']) || empty($data['re_password']) ) {
                $messages->setErrorMessage('The Password cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['password']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                else {
                    if ( $data['password'] != $data['re_password'] ) {
                        $messages->setErrorMessage('The entered Password do not match.');
                    }
                }
            }
            */
         
            
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
			}
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !empty($data['email']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email'] ."'")>0 ) {
                    $messages->setErrorMessage("This email id is already exist.Please specify another.");
                }
            }
            // Validate the E-mail Addresses.
            if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }
            elseif ( !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('The Primary Email Address is not valid.');
            }
            
            
            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
             // Login ID.
            /*
            if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage('The Login ID cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{1,80}$/', $data['username']) ) {
                    $messages->setErrorMessage('The Login ID is invalid.');
                }
                else {
         
                    $user_id    = encParam($data['username'].'?'.$data['number']); // We donot allow '?' in username, hence used here.
                    $user_list  = NULL;
                    $condition  = " WHERE (user_id = '$user_id') "
                                    ." OR ( username = '". $data['username'] ."' "
                                    ." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";

                    if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
                        foreach ( $user_list as $user) {
                            $messages->setErrorMessage('The Login ID-Employee number pair is alreay taken. Please enter another.');
                        }
                    }
                    else {
                        $data['user_id'] = $user_id;
                    }

                }
            }
            */
             $condition  = " WHERE number = '". $data['number'] ."' AND number IS NOT NULL ";

            if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
                //foreach ( $user_list as $user) {
                    //$messages->setErrorMessage('The Login ID number is already taken. Please enter another.');
                //}
                $data['number'] = Prospects::getNewAccNumber($db);  
            }
            $data['username'] = Prospects::onlyAlphaNumericTitle($data['f_name']).".".Prospects::onlyAlphaNumericTitle($data['l_name']);
            //To create the unique user id
            $user_id    = encParam($data['username'].'?'.$data['number']);
            $data['user_id'] = $user_id;
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
        
        function onlyAlphaNumericTitle($title){
            $title = str_replace(' ','',$title);
            $title = preg_replace('/[^-A-Za-z0-9]/','', $title);
            return $title ;
        }
        
        function validateQuickAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			if(!empty($data['number'])){
				$user_id    = encParam($data['username'].'?'.$data['number']); 
				// We donot allow '?' in username, hence used here.
				$user_list  = NULL;
				$condition  = " WHERE (user_id = '$user_id') "
								." OR ( username = '". $data['username'] ."' "
								." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";

				if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
					foreach ( $user_list as $user) {
						$messages->setErrorMessage('The Login ID-Employee number pair is alreay taken. 
						Please enter another.');
					}
				}
				else {
					$data['user_id'] = $user_id;
				}
			}
			
			if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
			}
            
          /*   if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
			if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage('Billing name should be provided');
			} */
			if (empty($data['lead_source_to']) && empty($data['lead_source_from'])){
				$messages->setErrorMessage('Select one of two Lead Source.');				
			} 
			if ( !empty($data['email']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email'] ."' || email_1 = '". $data['email'] ."' || email_2 = '". $data['email'] ."'")>0 ) {
                    $messages->setErrorMessage("This email id is already exist.Please specify another.");
                }
            }
           /*  if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }
            elseif ( !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('The Primary Email Address is not valid.');
            } */
            
            
            /* if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
             */
            if ( !empty($data['email_1']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_1'] ."' || email_1 = '". $data['email_1'] ."' || email_2 = '". $data['email_1'] ."' || email_3 = '". $data['email_1']."' || email_4='".$data['email_1']."'")>0 ) {
                    $messages->setErrorMessage("The first Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_2']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_2'] ."' || email_1 = '". $data['email_2'] ."' || email_2 = '". $data['email_2'] ."' || email_3 = '". $data['email_2']."' || email_4='".$data['email_2']."'")>0 ) {
                    $messages->setErrorMessage("The second Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_3']) && !empty($data['email_3']) && !isValidEmail($data['email_3']) ) {
                $messages->setErrorMessage('The Third Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_3']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_3'] ."' || email_1 = '". $data['email_3'] ."' || email_2 = '". $data['email_3'] ."' || email_3 = '". $data['email_3']."' || email_4='".$data['email_3']."'")>0 ) {
                    $messages->setErrorMessage("The Third Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_4']) && !empty($data['email_4']) && !isValidEmail($data['email_4']) ) {
                $messages->setErrorMessage('The Four Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_4']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_4'] ."' || email_1 = '". $data['email_4'] ."' || email_2 = '". $data['email_4'] ."' || email_3 = '". $data['email_4']."' || email_4='".$data['email_4']."'")>0 ) {
                    $messages->setErrorMessage("The Fourth Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_5']) && !empty($data['email_5']) && !isValidEmail($data['email_5']) ) {
                $messages->setErrorMessage('The Fifth Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_5']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_5'] ."' || email_1 = '". $data['email_5'] ."' || email_2 = '". $data['email_5'] ."' || email_3 = '". $data['email_5']."' || email_4='".$data['email_5']."'")>0 ) {
                    $messages->setErrorMessage("The Fifth Other Email Address is already exist.Please specify another.");
                }
            }
            
            if(!empty($data['email']) && !empty($data['email_1'])){
                if ( $data['email'] == $data['email_1'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_1']) && !empty($data['email_2'])){
                if ( $data['email_1'] == $data['email_2'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_2']) && !empty($data['email_3'])){
                if ( $data['email_2'] == $data['email_3'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_3']) && !empty($data['email_4'])){
                if ( $data['email_3'] == $data['email_4'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
			
			if ( !isset($data['details']) || empty($data['details']) ) {
                $messages->setErrorMessage('Please enter the Lead details');
			}
			 
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
			
		}
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
             // Login ID.
            if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage('The Login ID cannot be empty.');
            }
            else {
                //{5,80} atleast 6 characters should be accepted
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{5,80}$/', $data['username']) ) {
                    $messages->setErrorMessage('The Login ID is invalid.');
                }
                else {
                    // Check for Dupllicate User ID, Login ID and Account Number.
// The following code has been commented to allow Duplicate Username but with different Account Number.
// For achieving this, the Unique User ID will be generated using the Username and Account Number, and 
// while checking for duplicate entry the Unique User ID and the Username-Account Number pair will be used.
//                    $user_id    = encParam($data['username']);
//                    $user_list  = NULL;
//                    $condition  = " WHERE (user_id = '$user_id') "
//                                    ." OR ( username = '". $data['username'] ."') "
//                                    ." OR ( number = '". $data['number'] ."' AND number IS NOT NULL ) ";
//                    
//                    if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
//                        foreach ( $user_list as $user) {
//                            if ( ($user_id == $user['user_id']) || ($data['username'] == $user['username']) ) {
//                                $messages->setErrorMessage('The Login ID is alreay taken. Please enter another.');
//                            }
//                            if ( $data['number'] == $user['number'] ) {
//                                $messages->setErrorMessage('The Account number is alreay taken. Please enter another.');
//                            }
//                        }
//                    }
//                    else {
//                        $data['user_id'] = $user_id;
//                    }
                    $user_id    = encParam($data['username'].'?'.$data['number']); 
					// We donot allow '?' in username, hence used here.
                    $user_list  = NULL;
                    $condition  = " WHERE (user_id = '$user_id') "
                                    ." OR ( username = '". $data['username'] ."' "
                                    ." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";

                    if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
                        foreach ( $user_list as $user) {
                            $messages->setErrorMessage('The Login ID-Employee number pair is alreay taken. Please enter another.');
                        }
                    }
                    else {
                        $data['user_id'] = $user_id;
                    }
                }
            }
            
            
            // Password.
            if ( !isset($data['password']) || empty($data['password']) 
                    || !isset($data['re_password']) || empty($data['re_password']) ) {
                $messages->setErrorMessage('The Password cannot be empty.');
            }
            else {
                //{5,80} atleast 6 characters should be accepted in password
                if ( !preg_match('/^[a-zA-Z0-9_]{5,30}$/', $data['password']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                else {
                    if ( $data['password'] != $data['re_password'] ) {
                        $messages->setErrorMessage('The entered Password do not match.');
                    }
                }
            }

			// Check the validity of the Accounts Manager.
            /* not req
			if ( !isset($data['manager']) || empty($data['manager']) ) {
				$messages->setErrorMessage('Enter Username or Account Number of the Accounts Manager.');
			}
			else {
                include_once (DIR_FS_INCLUDES .'/user.inc.php');
                $executive  = NULL;
                $condition  = ' WHERE ( username = \''. $data['manager'] .'\' '
                                .' OR number = \''. $data['manager'] .'\' )'
                                . ' AND status = \'1\'';
                if ( User::getList($db, $executive, 'user_id', $condition) > 0 ) {
                    $data['manager'] = $executive[0]['user_id'];
                }
                else {
                    $messages->setErrorMessage('The Accounts Manager was not found.');
                }
			}*/

            // Access Level.
           /*  if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage('Select the Access Level.');
            }
            else {
                $acc_index = array();
                array_key_search('access_level', $data['access_level'], $al_list, $acc_index);
                if ( count($acc_index)<=0 ) {
                    $messages->setErrorMessage('Selected Access Level not found.');
                }
            } */
            
            // Roles.
            /*
            if ( !isset($data['roles']) ) {
                $messages->setErrorMessage('Select atleast one Role for the Client.');
            }
            else {
                foreach ( $data['roles'] as $role ) {
                    $role_index = array();
                    array_key_search('id', $role, $role_list, $role_index);
                    if ( count($role_index)<=0 ) {
                        $messages->setErrorMessage('The Role with ID '. $role .'is not found.');
                    }
                }
                $data['roles'] = implode(',', $data['roles']);
            }
            */
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
			}
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !isset($data['gender']) || !in_array($data['gender'], array('m','f','o')) ) {
                $messages->setErrorMessage('Gender should be selected.');
            }
                       
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage('Billing name should be provided');
			}
           
            if ( !isset($data['industry']) || empty($data['industry']) ) {
                $messages->setErrorMessage('Industry should be selected');
			}
             if (empty($data['lead_source_to']) && empty($data['lead_source_from'])){
				$messages->setErrorMessage('Select one of two Lead Source.');				
			} 
            // Check the Important Dates.
            if ( !empty($data['do_birth']) ) {
                $temp = explode('/', $data['do_birth']);
                $data['do_birth'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['do_aniv']) ) {
                $temp = explode('/', $data['do_aniv']);
                $data['do_aniv'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['spouse_dob']) ) {
                $temp = explode('/', $data['spouse_dob']);
                $data['spouse_dob'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            // Validate the E-mail Addresses.
			 if($data['email'] != CLIENT_DEFAULT_EMAIL){
				if ( !empty($data['email']) ) {
					$list = NULL;
					if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email'] ."' || email_1 = '". $data['email'] ."' || email_2 = '". $data['email'] ."'")>0 ) {
						$messages->setErrorMessage("This email id is already exist.Please specify another.");
					}
				}
				if ( !isset($data['email']) || empty($data['email']) ) {
					$messages->setErrorMessage('Email Address should be provided.');
				}
				elseif ( !isValidEmail($data['email']) ) {
					$messages->setErrorMessage('The Primary Email Address is not valid.');
				}
            }
             
            
            
            if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_1']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_1'] ."' || email_1 = '". $data['email_1'] ."' || email_2 = '". $data['email_1'] ."' || email_3 = '". $data['email_1']."' || email_4='".$data['email_1']."'")>0 ) {
                    $messages->setErrorMessage("The first Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_2']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_2'] ."' || email_1 = '". $data['email_2'] ."' || email_2 = '". $data['email_2'] ."' || email_3 = '". $data['email_2']."' || email_4='".$data['email_2']."'")>0 ) {
                    $messages->setErrorMessage("The second Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_3']) && !empty($data['email_3']) && !isValidEmail($data['email_3']) ) {
                $messages->setErrorMessage('The Third Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_3']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_3'] ."' || email_1 = '". $data['email_3'] ."' || email_2 = '". $data['email_3'] ."' || email_3 = '". $data['email_3']."' || email_4='".$data['email_3']."'")>0 ) {
                    $messages->setErrorMessage("The Third Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_4']) && !empty($data['email_4']) && !isValidEmail($data['email_4']) ) {
                $messages->setErrorMessage('The Four Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_4']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_4'] ."' || email_1 = '". $data['email_4'] ."' || email_2 = '". $data['email_4'] ."' || email_3 = '". $data['email_4']."' || email_4='".$data['email_4']."'")>0 ) {
                    $messages->setErrorMessage("The Fourth Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_5']) && !empty($data['email_5']) && !isValidEmail($data['email_5']) ) {
                $messages->setErrorMessage('The Fifth Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_5']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_5'] ."' || email_1 = '". $data['email_5'] ."' || email_2 = '". $data['email_5'] ."' || email_3 = '". $data['email_5']."' || email_4='".$data['email_5']."'")>0 ) {
                    $messages->setErrorMessage("The Fifth Other Email Address is already exist.Please specify another.");
                }
            }
            
            if(!empty($data['email']) && !empty($data['email_1'])){
                if ( $data['email'] == $data['email_1'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_1']) && !empty($data['email_2'])){
                if ( $data['email_1'] == $data['email_2'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_2']) && !empty($data['email_3'])){
                if ( $data['email_2'] == $data['email_3'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_3']) && !empty($data['email_4'])){
                if ( $data['email_3'] == $data['email_4'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['additional_email'])){
                $arrEmail = explode(",",$data['additional_email']);
                if(!empty($arrEmail)){
                    foreach($arrEmail as $key =>$val){
                        if ( isset($val) && !empty($val) && !isValidEmail($val) ) {
                            $messages->setErrorMessage( $val.' is not valid email id added in additional email.' );
                        }
                    }
                }
            }
            // Validate the Addresses.
            $data['address_count'] = count($data['address_type']);
            for ( $i=0; $i<$data['address_count']; $i++ ) {
            $data['company_name'][$i]='';
                if ( !$region->validate($data['address_type'][$i],
                                  $data['company_name'][$i],
                                  $data['address'][$i],
                                  $data['city'][$i],
                                  $data['state'][$i],
                                  $data['country'][$i],
                                  $data['zipcode'][$i]
                                 )
                   ) {
                    $messages->setErrorMessage('There is/are error(s) in the Mailing Address.');
                    foreach ( $region->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
            }
            
            // Validate the Phone.
            $data['phone_count'] = count($data['p_type']);
            for ( $i=0; $i<$data['phone_count']; $i++ ) {
                if ( !empty($data['p_number'][$i]) ) {
                    if ( !$phone->validate($data['p_type'][$i], $data['cc'][$i], $data['ac'][$i], $data['p_number'][$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in the <b>'. $phone->getTypeTitle($data['p_type'][$i]) .' Contact Number</b>.');
                        foreach ( $phone->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }
            
            // Check the Reminders.
            for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                if ( !empty($data['remind_date_'.$i]) ) {
                    $temp = explode('/', $data['remind_date_'.$i]);
                    $data['remind_date_'.$i] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                    
                    if ( !$reminder->validate($data['remind_date_'.$i], $data['remind_text_'.$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                        foreach ( $reminder->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }
            
             if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Services should be selected.');
			}
            if ( $data['status'] != Prospects::PENDING && $data['status'] != Prospects::ACTIVE 
                    && $data['status'] != Prospects::BLOCKED && $data['status'] != Prospects::DELETED ) {
				$messages->setErrorMessage('The Status is not valid.');
			}
			

            // Validate the Client Managers.
			/*
            if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Client.");
            }
            else {
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result());
                    }
                    $team_count = count($data['team']);

                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }
                    // Uncomment the following and comment the above FOR loop if speed is preferred 
					// against a detailed error report as above.
                    //if ( $db->nf() < $team_count ) {
                    //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                    //}
                }
            }
			*/
            
            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // Password.
            if ( isset($data['password']) && !empty($data['password']) ) {
                if ( !isset($data['re_password']) || empty($data['re_password']) ) {
                    $messages->setErrorMessage('The Repeat Password cannot be empty.');
                }
                elseif ( !preg_match('/^[a-zA-Z0-9_]{6,30}$/', $data['password']) ) {
                    // atleast 6 chars              
                     $messages->setErrorMessage('The Password is invalid.');
                }
                elseif ( $data['password'] != $data['re_password'] ) {
                    $messages->setErrorMessage('The entered Password do not match.');
                }
                else {
                    $data['password'] = ",". TABLE_PROSPECTS .".password = '". encParam($data['password']) ."'";
                }
            }
            else {
                $data['password'] = '';
            }

            // Check the validity of the Accounts Manager.
            /* not req
            if ( !isset($data['manager']) || empty($data['manager']) ) {
                $messages->setErrorMessage('Enter Username or Account Number of the Accounts Manager.');
            }
            else {
                include_once (DIR_FS_INCLUDES .'/user.inc.php');
                $executive  = NULL;
                $condition  = ' WHERE ( username = \''. $data['manager'] .'\' '
                                .' OR number = \''. $data['manager'] .'\' )'
                                .' AND status = \'1\'';
                if ( User::getList($db, $executive, 'user_id', $condition) > 0 ) {
                    $data['manager'] = $executive[0]['user_id'];
                }
                else {
                    $messages->setErrorMessage('The Accounts Manager was not found.');
                }
            }*/


            // Access Level.
           /*  if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage('Select the Access Level.');
            }
            else {
                $acc_index = array();
                array_key_search('access_level', $data['access_level'], $al_list, $acc_index);
                if ( count($acc_index)<=0 ) {
                    $messages->setErrorMessage('Selected Access Level not found.');
                }
            } */
            
            // Roles.
            /*
            if ( !isset($data['roles']) ) {
                $messages->setErrorMessage('Select atleast one Role for the Client.');
            }
            else {
                foreach ( $data['roles'] as $role ) {
                    $role_index = array();
                    array_key_search('id', $role, $role_list, $role_index);
                    if ( count($role_index)<=0 ) {
                        $messages->setErrorMessage('The Role with ID '. $role .'is not found.');
                    }
                }
                $data['roles'] = implode(',', $data['roles']);
            }
            */
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
            }
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !isset($data['gender']) || !in_array($data['gender'], array('m','f','o')) ) {
                $messages->setErrorMessage('Gender should be selected.');
            }
            
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage('Billing name should be provided');
			}
           
            if ( !isset($data['industry']) || empty($data['industry']) ) {
                $messages->setErrorMessage('Industry should be selected');
			}
            if (empty($data['lead_source_to']) && empty($data['lead_source_from'])){
				$messages->setErrorMessage('Select one of two Lead Source.');				
			} 
            // Check the Important Dates.
            if ( !empty($data['do_birth']) ) {
                $temp = explode('/', $data['do_birth']);
                $data['do_birth'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['do_aniv']) ) {
                $temp = explode('/', $data['do_aniv']);
                $data['do_aniv'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['spouse_dob']) ) {
                $temp = explode('/', $data['spouse_dob']);
                $data['spouse_dob'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['do_reg']) ) {
                $temp = explode('/', $data['do_reg']);
                $data['do_reg'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
           
            // Validate the E-mail Addresses.
            if($data['email'] != CLIENT_DEFAULT_EMAIL){
				if ( !empty($data['email']) ) {
					$list = NULL;
					if ( Prospects::getList($db, $list, 'user_id', " WHERE (email = '". $data['email'] ."' || email_1 = '". $data['email'] ."' || email_2 = '". $data['email'] ."') AND user_id != '".$data['user_id']."'")>0 ) {
                    $messages->setErrorMessage("The first Other Email Address is already exist.Please specify another.");
                }
				}
				if ( !isset($data['email']) || empty($data['email']) ) {
					$messages->setErrorMessage('Email Address should be provided.');
				}
				elseif ( !isValidEmail($data['email']) ) {
					$messages->setErrorMessage('The Primary Email Address is not valid.');
				}
            }
             
            
            if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_1']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE (email = '". $data['email_1'] ."' || email_1 = '". $data['email_1'] ."' || email_2 = '". $data['email_1'] ."') AND user_id != '".$data['user_id']."'")>0 ) {
                    $messages->setErrorMessage("The first Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_2']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE (email = '". $data['email_2'] ."' || email_1 = '". $data['email_2'] ."' || email_2 = '". $data['email_2'] ."') AND user_id != '".$data['user_id']."'")>0 ) {
                    $messages->setErrorMessage("The second Other Email Address is already exist.Please specify another.");
                }
            }
            
            if(!empty($data['email']) && !empty($data['email_1'])){
                if ( $data['email'] == $data['email_1'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_1']) && !empty($data['email_2'])){
                if ( $data['email_1'] == $data['email_2'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email']) && !empty($data['email_2'])){
                if ( $data['email'] == $data['email_2'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            if(!empty($data['additional_email'])){
                $arrEmail = explode(",",$data['additional_email']);
                if(!empty($arrEmail)){
                    foreach($arrEmail as $key =>$val){
                        if ( isset($val) && !empty($val) && !isValidEmail($val) ) {
                            $messages->setErrorMessage( $val.' is not valid email id added in additional email.' );
                        }
                    }
                }
            }
            // Validate the Addresses.
            $data['address_count']  = count($data['address_type']);
            $preferred              = 0;
            for ( $i=0; $i<$data['address_count']; $i++ ) {
            $data['company_name'][$i]='';
                if ( !$region->validate($data['address_type'][$i],
                                  $data['company_name'][$i],
                                  $data['address'][$i],                                 
                                  $data['city'][$i],
                                  $data['state'][$i],
                                  $data['country'][$i],
                                  $data['zipcode'][$i]
                                 )
                   ) {
                    $messages->setErrorMessage('There is/are error(s) in the '. $region->getTypeTitle($data['address_type'][$i]) .' Address.');
                    foreach ( $region->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
                if ( isset($data['is_preferred'][$i]) && $data['is_preferred'][$i] == '1' ) {
                    $preferred += 1 ;
                }
            }
            // The elseif part is added for additional validation and security.
            // If speed is intended the elseif part can be removed and the
            // $preferred is to be converted to a BOOLEAN.
            // ($preferred = 0) to ($preferred = false), ($preferred += 1) to ($preferred = true)
            if ( $preferred == 0 ) {
                $messages->setErrorMessage('Atleast one Address is to be selected as preferred.');
            }
            elseif ( $preferred > 1 ) {
                $messages->setErrorMessage('Only one Address is to be selected as preferred.');
            }
            
            // Validate the Phone.
            $data['phone_count']= count($data['p_type']);
            $preferred          = 0;
            for ( $i=0; $i<$data['phone_count']; $i++ ) {
                if ( !empty($data['p_number'][$i]) ) {
                    if ( !$phone->validate($data['p_type'][$i], $data['cc'][$i], $data['ac'][$i], $data['p_number'][$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in the <b>'. $phone->getTypeTitle($data['p_type'][$i]) .' Contact Number</b>.');
                        foreach ( $phone->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
                if ( isset($data['p_is_preferred'][$i]) && $data['p_is_preferred'][$i] == '1' ) {
                    $preferred += 1 ;
                }
            }
            
            // The elseif part is added for additional validation and security.
            // If speed is intended the elseif part can be removed and the
            // $preferred is to be converted to a BOOLEAN.
            // ($preferred = 0) to ($preferred = false), ($preferred += 1) to ($preferred = true)
            if ( !$preferred ) {
                $messages->setErrorMessage('Atleast one Contact Number is to be selected as preferred.');
            }
            elseif ( $preferred > 1 ) {
                $messages->setErrorMessage('Only one Contact Number is to be selected as preferred.');
            }
            
            // Check the Reminders.
            for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                if ( !empty($data['remind_date_'.$i]) ) {
                    $temp = explode('/', $data['remind_date_'.$i]);
                    $data['remind_date_'.$i] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                    
                    if ( !$reminder->validate($data['remind_date_'.$i], $data['remind_text_'.$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                        foreach ( $reminder->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }
            if ( !isset($data['financial_status']) || empty($data['financial_status']) ) {
                $messages->setErrorMessage('Select Financial Status - Yearly Turnover / Yearly Income');
			}
			if ( !isset($data['company_branches']) || empty($data['company_branches']) ) {
                $messages->setErrorMessage('Are there any branches of the company? If yes provide details');
			}
			if ( !isset($data['company_profile']) || empty($data['company_profile']) ) {
                $messages->setErrorMessage('What exactly is the profile of company? What it does? What are its services or products like?');
			}
			if ( !isset($data['services_pitched']) || empty($data['services_pitched']) ) {
                $messages->setErrorMessage('Which of our services do you think can be pitched?');
			}
			
            /* 
			if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Services should be selected.');
			} */
            if ( $data['status'] != Prospects::PENDING && $data['status'] != Prospects::ACTIVE 
                    && $data['status'] != Prospects::BLOCKED && $data['status'] != Prospects::DELETED ) {
                $messages->setErrorMessage('The Status is not valid.');
            }
            
			// Validate the Client Managers.
			/*
            if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Client.");
            }
            else {
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result());
                    }
                    $team_count = count($data['team']);

                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }
                    // Uncomment the following and comment the above FOR loop if speed is preferred 
					// against a detailed error report as above.
                    //if ( $db->nf() < $team_count ) {
                    //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                    //}
                }
            }			
			*/
            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
             // Login ID.
            if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage('The Login ID cannot be empty.');
            }
            else {
                //{5,80} atleast 6 characters should be accepted
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{5,80}$/', $data['username']) ) {
                   
                    $messages->setErrorMessage('The Login ID is invalid.');
                    
                  
                }
                else {
                    // Check for Dupllicate User ID, Login ID and Account Number.
//                    $user_id    = encParam($data['username']);
//                    $user_list  = NULL;
//                    $condition  = " WHERE ( (user_id = '$user_id') "
//                                        ." OR ( username = '". $data['username'] ."') "
//                                        ." OR ( number = '". $data['number'] ."' AND number IS NOT NULL ) "
//                                    ." ) AND user_id != '". $data['user_id'] ."'";
//                    
//                    if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
//                        foreach ( $user_list as $user) {
//                            if ( ($user_id == $user['user_id']) || ($data['username'] == $user['username']) ) {
//                                $messages->setErrorMessage('The Login ID is alreay taken. Please enter another.');
//                            }
//                            if ( $data['number'] == $user['number'] ) {
//                                $messages->setErrorMessage('The Account number is alreay taken. Please enter another.');
//                            }
//                        }
//                    }
//                    else {
//                        $data['user_id'] = $user_id;
//                        
//                    }
						$user_list  = NULL;
						$condition  = " WHERE (user_id != '". $data['user_id'] ."') "
										." AND ( username = '". $data['username'] ."' "
										." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";
						
						if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
							foreach ( $user_list as $user) {
								$messages->setErrorMessage('The Login ID-Relationship number pair is alreay taken. Please enter another.');
							}
						}
                }
            }



            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
        
        
        /**
         * This function is used to retrieve the next account number in series.
         *
         * @param   object  database class object
         *
         * @return  int     the next number in series
         *
         */
         
        function getNewAccNumber(&$db){
            //$user_number= 6000;
            $user_number= 5000;
            $query      = "SELECT MAX(number) AS number FROM ". TABLE_PROSPECTS;

            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("number") > 0){
                $user_number = $db->f("number");
            }
			$user_number++;
            return $user_number;
        }

        
        function getRoles(&$db, $level, $role_ids='') {
            include_once ( DIR_FS_INCLUDES .'/user-roles.inc.php');
            
            $role_list = NULL;
            if ( !empty($role_ids) ) {
                if ( is_array($role_ids) ) {
                    $role_ids = ' AND id IN ('. ('\''. implode('\',', $role_ids) .'\'') .')';
                }
                else {
                    $role_ids = ' AND id IN ('. ('\''. str_replace(',', '\',\'', $role_ids) .'\'') .')';
                }
            }
            else {
                $role_ids = '';
            }
            
            $condition  = " WHERE access_level < '". $level ."'"
                            . $role_ids
                            ." AND status = '". UserRoles::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            
            if ( (UserRoles::getList($db, $role_list, 'id, title', $condition)) <= 0 ) {
                $role_list[] = array('id' => '', 'title' => 'No Role Available' );
            }
            
            return ($role_list);
        }
        
        
        function getAccessLevel(&$db, $level=0, $al_list='') {
            include_once ( DIR_FS_INCLUDES .'/settings-access-level.inc.php');

            $role_list = NULL;
            if ( !empty($al_list) ) {
                if ( is_array($al_list) ) {
                    $al_list = ' AND access_level IN ('. ('\''. implode('\',', $al_list) .'\'') .')';
                }
                else {
                    $al_list = ' AND access_level IN ('. ('\''. str_replace(',', '\',\'', $al_list) .'\'') .')';
                }
            }
            else {
                $al_list = '';
            }
            
            $access     = NULL;
            $condition  = " WHERE access_level < '". $level ."'"
                            . $al_list
                            ." AND status = '". AccessLevel::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            if ( (AccessLevel::getList($db, $access, 'title, access_level', $condition)) <= 0 ) {
                $access[] = array('title' => 'None', 'access_level' => '0' );
            }
            
            return ($access);
        }
        
        
        function getClientManager($db, $client_id, &$details, $fields='username') {
            $query = "SELECT $fields FROM ". TABLE_PROSPECTS
                        ." LEFT JOIN ". TABLE_USERS
                            ." ON ". TABLE_USERS .".user_id = ". TABLE_PROSPECTS .".manager "
                        ." WHERE ". TABLE_PROSPECTS .".user_id = '". $client_id ."'";
            if ( ($db->query($query)) && ($db->nf()>0) && ($db->next_record()>0) ) {
                $details = processSQLData($db->result());
                return (true);
            }
            return (false);
        }
        
        
        function getManager(&$db, $client_id='', $executive_id='', $fields='username') {
            include_once (DIR_FS_INCLUDES .'/user.inc.php');
            $executive  = '';
            
            if ( $client_id != '' ) {
                $condition  = " WHERE user_id = '$client_id'";
                if ( Prospects::getList($db, $executive, 'manager', $condition) > 0 ) {
                    $executive_id = $executive[0]['manager'];
                }
            }
            $executive = '';
            $condition  = ' WHERE user_id = \''. $executive_id .'\' ';
            if ( User::getList($db, $executive, $fields, $condition) > 0 ) {
                $fields = explode(',', $fields);
                if ( count($fields) == 1 ) {
                    $executive = $executive[0][$fields[0]];
                }
                else {
                    $executive = $executive[0];
                }
            }
            return ($executive);
        }
        
        
        
        function validateFollowupAdd(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            if($data['submitType']==1){
                //Validation for add followup
                if ( !isset($data['comment']) || empty($data['comment']) ) {
                    $messages->setErrorMessage('Comment should be provided.');
                }
                if ( !isset($data['type']) || empty($data['type']) ) {
                    $messages->setErrorMessage('Type should be provided.');
                }
            }
            if($data['submitType']==2){
                //Validation for send sms
            
                if ( (!isset($data['comment']) || empty($data['comment']) ) && !isset($data['send_default_msg']) ) {
                    $messages->setErrorMessage('Either Provide Message or mark send default message.');
                }
                if(!empty($data['client_mobile'])){
                    if(!is_numeric($data['client_mobile'])){
                        $messages->setErrorMessage('Mobile no should be number.');
                    }
                }
                
            }
            if($data['submitType']==3){
                //Validation for send mail
            
                if ( (!isset($data['comment']) || empty($data['comment']) ) && !isset($data['send_default_mail']) ) {
                    $messages->setErrorMessage('Either Provide Email Message or mark send default mail.');
                }
                if(!empty($data['client_email'])){
                    if(!is_numeric($data['client_email'])){
                        $messages->setErrorMessage('Mobile no should be number.');
                    }
                }
                
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        //Used in communication log
	    function getDetailsCommLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
             $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_INV_FOLLOWUP ;
            $query .= " LEFT JOIN ". TABLE_PROSPECTS
                        ." ON ". TABLE_PROSPECTS .".user_id = ".TABLE_INV_FOLLOWUP.".created_by ";
			$query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                return ( $total );
            }
            else {
                return false;
            }   
        }
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateSubUserAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
            $data['password']=$data['re_password']='smeerp';
            $data['username']="a".mktime();
            // Password.
            if ( !isset($data['password']) || empty($data['password']) 
                    || !isset($data['re_password']) || empty($data['re_password']) ) {
                $messages->setErrorMessage('The Password cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['password']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                else {
                    if ( $data['password'] != $data['re_password'] ) {
                        $messages->setErrorMessage('The entered Password do not match.');
                    }
                }
            }
            
            //if allow_ip == 2 ie specified then 
            if ( $data['allow_ip'] == 2 ) {
                
                if(empty($data['valid_ip'])){
                    $messages->setErrorMessage('Please enter valid IP List.');
                }
			}else{
                $data['valid_ip'] ='';
            }
            
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
			}
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !isset($data['gender']) || !in_array($data['gender'], array('m','f','o')) ) {
                $messages->setErrorMessage('Gender should be selected.');
            }
            
            // Check the Important Dates.
            if ( !empty($data['do_birth']) ) {
                $temp = explode('/', $data['do_birth']);
                $data['do_birth'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['do_aniv']) ) {
                $temp = explode('/', $data['do_aniv']);
                $data['do_aniv'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            if ( !empty($data['email']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email'] ."' || email_1 = '". $data['email'] ."' || email_2 = '". $data['email'] ."'")>0 ) {
                    $messages->setErrorMessage("This email id is already exist.Please specify another.");
                }
            }
            if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }
            elseif ( !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('The Primary Email Address is not valid.');
            }
            
            
            if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_1']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_1'] ."' || email_1 = '". $data['email_1'] ."' || email_2 = '". $data['email_1'] ."' || email_3 = '". $data['email_1']."' || email_4='".$data['email_1']."'")>0 ) {
                    $messages->setErrorMessage("The first Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_2']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_2'] ."' || email_1 = '". $data['email_2'] ."' || email_2 = '". $data['email_2'] ."' || email_3 = '". $data['email_2']."' || email_4='".$data['email_2']."'")>0 ) {
                    $messages->setErrorMessage("The second Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_3']) && !empty($data['email_3']) && !isValidEmail($data['email_3']) ) {
                $messages->setErrorMessage('The Third Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_3']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_3'] ."' || email_1 = '". $data['email_3'] ."' || email_2 = '". $data['email_3'] ."' || email_3 = '". $data['email_3']."' || email_4='".$data['email_3']."'")>0 ) {
                    $messages->setErrorMessage("The Third Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_4']) && !empty($data['email_4']) && !isValidEmail($data['email_4']) ) {
                $messages->setErrorMessage('The Four Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_4']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_4'] ."' || email_1 = '". $data['email_4'] ."' || email_2 = '". $data['email_4'] ."' || email_3 = '". $data['email_4']."' || email_4='".$data['email_4']."'")>0 ) {
                    $messages->setErrorMessage("The Fourth Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_5']) && !empty($data['email_5']) && !isValidEmail($data['email_5']) ) {
                $messages->setErrorMessage('The Fifth Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_5']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_5'] ."' || email_1 = '". $data['email_5'] ."' || email_2 = '". $data['email_5'] ."' || email_3 = '". $data['email_5']."' || email_4='".$data['email_5']."'")>0 ) {
                    $messages->setErrorMessage("The Fifth Other Email Address is already exist.Please specify another.");
                }
            }
            
            if(!empty($data['email']) && !empty($data['email_1'])){
                if ( $data['email'] == $data['email_1'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_1']) && !empty($data['email_2'])){
                if ( $data['email_1'] == $data['email_2'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_2']) && !empty($data['email_3'])){
                if ( $data['email_2'] == $data['email_3'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_3']) && !empty($data['email_4'])){
                if ( $data['email_3'] == $data['email_4'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            /* if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Plz select the services.');
			} */
            
            // Validate the Addresses.
            /*$data['address_count'] = count($data['address_type']);
            for ( $i=0; $i<$data['address_count']; $i++ ) {
                if ( !$region->validate($data['address_type'][$i],
                                  $data['company_name'][$i],
                                  $data['address'][$i],
                                  $data['city'][$i],
                                  $data['state'][$i],
                                  $data['country'][$i],
                                  $data['zipcode'][$i]
                                 )
                   ) {
                    $messages->setErrorMessage('There is/are error(s) in the Mailing Address.');
                    foreach ( $region->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
            }*/
            
            // Validate the Phone.
            $data['phone_count'] = count($data['p_type']);
            for ( $i=0; $i<$data['phone_count']; $i++ ) {
                if ( !empty($data['p_number'][$i]) ) {
                    if ( !$phone->validate($data['p_type'][$i], $data['cc'][$i], $data['ac'][$i], $data['p_number'][$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in the <b>'. $phone->getTypeTitle($data['p_type'][$i]) .' Contact Number</b>.');
                        foreach ( $phone->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }
            
            
            // Check the Reminders.
            /*for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                if ( !empty($data['remind_date_'.$i]) ) {
                    $temp = explode('/', $data['remind_date_'.$i]);
                    $data['remind_date_'.$i] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                    
                    if ( !$reminder->validate($data['remind_date_'.$i], $data['remind_text_'.$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                        foreach ( $reminder->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }*/
            
            if ( $data['status'] != Prospects::PENDING && $data['status'] != Prospects::ACTIVE 
                    && $data['status'] != Prospects::BLOCKED && $data['status'] != Prospects::DELETED ) {
				$messages->setErrorMessage('The Status is not valid.');
			}
            $data['username']='b'.mktime();
            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
             // Login ID.
             if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage('The Login ID cannot be empty.');
            }
            elseif ( !isset($data['number']) || empty($data['number']) ) {
                $messages->setErrorMessage('The Employee Number cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{1,80}$/', $data['username']) ) {
                    $messages->setErrorMessage('The Login ID is invalid.');
                }
                else {
                  
                    $user_id    = encParam($data['username'].'?'.$data['number']); // We donot allow '?' in username, hence used here.
                    $user_list  = NULL;
                    $condition  = " WHERE (user_id = '$user_id') "
                                    ." OR ( username = '". $data['username'] ."' "
                                    ." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";
                    
                    if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
                        foreach ( $user_list as $user) {
                            $messages->setErrorMessage('This Login ID is alreay taken. Please enter another.');
                        }
                    }
                    else {
                        $data['user_id'] = $user_id;
                    }
                }
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
        function validateSubUserUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
 
            // Password.
            if ( isset($data['password']) && !empty($data['password']) ) {
                if ( !isset($data['re_password']) || empty($data['re_password']) ) {
                    $messages->setErrorMessage('The Repeat Password cannot be empty.');
                }
                elseif ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['password']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                elseif ( $data['password'] != $data['re_password'] ) {
                    $messages->setErrorMessage('The entered Password do not match.');
                }
                else {
                    $data['password'] = ",". TABLE_PROSPECTS .".password = '". encParam($data['password']) ."'";
                }
            }
            else {
                $data['password'] = '';
            }

            //if allow_ip == 2 ie specified then 
            if ( $data['allow_ip'] == 2 ) {
                
                if(empty($data['valid_ip'])){
                    $messages->setErrorMessage('Please enter valid IP List.');
                }
			}else{
                $data['valid_ip'] ='';
            }
            
            // Access Level.
            /*
			if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage('Select the Access Level.');
            } else {
                $acc_index = array();
                array_key_search('access_level', $data['access_level'], $al_list, $acc_index);
                if ( count($acc_index)<=0 ) {
                    $messages->setErrorMessage('Selected Access Level not found.');
                }
            }*/
            
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
            }
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !isset($data['gender']) || !in_array($data['gender'], array('m','f','o')) ) {
                $messages->setErrorMessage('Gender should be selected.');
            }
            
            /* 
			if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Services should be selected.');
			} */
            
            // Check the Important Dates.
            if ( !empty($data['do_birth']) ) {
                $temp = explode('/', $data['do_birth']);
                $data['do_birth'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['do_aniv']) ) {
                $temp = explode('/', $data['do_aniv']);
                $data['do_aniv'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            // Validate the E-mail Addresses.
            if ( !empty($data['email']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE (email = '". $data['email'] ."' || email_1 = '". $data['email'] ."' || email_2 = '". $data['email'] ."') AND user_id != '".$data['user_id']."'")>0 ) {
                    $messages->setErrorMessage("This email id is already exist.Please specify another.");
                }
            }
            
            if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }
            elseif ( !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('The Primary Email Address is not valid.');
            }
            
            if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_1']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE (email = '". $data['email_1'] ."' || email_1 = '". $data['email_1'] ."' || email_2 = '". $data['email_1'] ."') AND user_id != '".$data['user_id']."'")>0 ) {
                    $messages->setErrorMessage("The first Other Email Address is already exist.Please specify another.");
                }
            }
            
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            if ( !empty($data['email_2']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE (email = '". $data['email_2'] ."' || email_1 = '". $data['email_2'] ."' || email_2 = '". $data['email_2'] ."') AND user_id != '".$data['user_id']."'")>0 ) {
                    $messages->setErrorMessage("The second Other Email Address is already exist.Please specify another.");
                }
            }
            
            if(!empty($data['email']) && !empty($data['email_1'])){
                if ( $data['email'] == $data['email_1'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email_1']) && !empty($data['email_2'])){
                if ( $data['email_1'] == $data['email_2'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            if(!empty($data['email']) && !empty($data['email_2'])){
                if ( $data['email'] == $data['email_2'] ){
                    $messages->setErrorMessage("The Email Address specified by you contain duplicate values.Please remove the duplicates.");
                }
            }
            
            // Validate the Addresses.
            /*$data['address_count']  = count($data['address_type']);
            $preferred              = 0;
            for ( $i=0; $i<$data['address_count']; $i++ ) {
                if ( !$region->validate($data['address_type'][$i],
                                  $data['address'][$i],
                                  $data['company_name'][$i],
                                  $data['city'][$i],
                                  $data['state'][$i],
                                  $data['country'][$i],
                                  $data['zipcode'][$i]
                                 )
                   ) {
                    $messages->setErrorMessage('There is/are error(s) in the '. $region->getTypeTitle($data['address_type'][$i]) .' Address.');
                    foreach ( $region->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
                if ( isset($data['is_preferred'][$i]) && $data['is_preferred'][$i] == '1' ) {
                    $preferred += 1 ;
                }
            }
            // The elseif part is added for additional validation and security.
            // If speed is intended the elseif part can be removed and the
            // $preferred is to be converted to a BOOLEAN.
            // ($preferred = 0) to ($preferred = false), ($preferred += 1) to ($preferred = true)
            if ( $preferred == 0 ) {
                $messages->setErrorMessage('Atleast one Address is to be selected as preferred.');
            }
            elseif ( $preferred > 1 ) {
                $messages->setErrorMessage('Only one Address is to be selected as preferred.');
            }*/
            
            // Validate the Phone.
            $data['phone_count']= count($data['p_type']);
            $preferred          = 0;
            for ( $i=0; $i<$data['phone_count']; $i++ ) {
                if ( !empty($data['p_number'][$i]) ) {
                    if ( !$phone->validate($data['p_type'][$i], $data['cc'][$i], $data['ac'][$i], $data['p_number'][$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in the <b>'. $phone->getTypeTitle($data['p_type'][$i]) .' Contact Number</b>.');
                        foreach ( $phone->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
                if ( isset($data['p_is_preferred'][$i]) && $data['p_is_preferred'][$i] == '1' ) {
                    $preferred += 1 ;
                }
            }
            
            // The elseif part is added for additional validation and security.
            // If speed is intended the elseif part can be removed and the
            // $preferred is to be converted to a BOOLEAN.
            // ($preferred = 0) to ($preferred = false), ($preferred += 1) to ($preferred = true)
            if ( !$preferred ) {
                $messages->setErrorMessage('Atleast one Contact Number is to be selected as preferred.');
            }
            elseif ( $preferred > 1 ) {
                $messages->setErrorMessage('Only one Contact Number is to be selected as preferred.');
            }
            
            if ( $data['status'] != Prospects::PENDING && $data['status'] != Prospects::ACTIVE 
                    && $data['status'] != Prospects::BLOCKED && $data['status'] != Prospects::DELETED ) {
                $messages->setErrorMessage('The Status is not valid.');
            }
            
            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
             // Login ID.
            /* if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage('The Login ID cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{1,80}$/', $data['username']) ) {
                    $messages->setErrorMessage('The Login ID is invalid.');
                }
                else {
						$user_list  = NULL;
						$condition  = " WHERE (user_id != '". $data['user_id'] ."') "
										." AND ( username = '". $data['username'] ."' "
										." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";
						
						if ( (Prospects::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
							foreach ( $user_list as $user) {
								$messages->setErrorMessage('The Login ID-Relationship number pair is alreay taken. Please enter another.');
							}
						}
                }
            } */

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
        
         /**
         * This function is used to update the mail status of the Client (i.e. whether to send the mail to client or not).
         *
         * @param    string      unique ID of the Client whose mail status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the mail status was changed.
         *                       false, if the mail status was not changed.
         *
         */
        function updateMailStatus($id, $send_mail, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($send_mail, Prospects::getMailStatus()) ) {
				$messages->setErrorMessage("The Mail Status spacified is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Prospects::getList($db, $user, 'user_id, access_level', " WHERE user_id = '$id'")) > 0 ) {
                    $user = $user[0];       
					$query = "UPDATE ". TABLE_PROSPECTS
								." SET send_mail = '$send_mail' "
								." WHERE user_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) {
						$messages->setOkMessage("The Send Mail Status has been updated.");
					}
					else {
						$messages->setErrorMessage("The Send Mail Status was not updated.");
					}
       
                }
                else {
                    $messages->setErrorMessage("The Client was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
		
		function clientPAddressPdf($data) {        
			$variables['images'] = DIR_WS_IMAGES_NC ;
			$body =  "<style type=\"text/css\" rel=\"stylesheet\">     
				body {
					background-color:#FFFFFF;
					font-family:Verdana, Arial, Helvetica, sans-serif;
					color:#333333;
					font-size:10px;
					font-weight:normal;
					margin:1px 5px 5px 5px;
					color:#323131;
					top:0px;
				}
				.print_name{
					color:#383939;  
					font-family: Arial,Verdana, Helvetica, sans-serif;
					font-size:16px;
					font-weight:bold;
				}
				.print_company{
					color:#383939;  
					font-family: Arial,Verdana, Helvetica, sans-serif;
					font-size:18px;
					font-weight:bold;
				}
				.print_addr{
					color:#6a6b6b;  
					font-family: Arial,Verdana, Helvetica, sans-serif;
					font-size:15px;
					font-weight:normal;
				}
				.print_phone{
					color:#6a6b6b;  
					font-family: Arial,Verdana, Helvetica, sans-serif;
					font-size:12px;
					font-weight:normal;
				}
				.pb10 {padding-bottom:10px;}
				.pl50 {padding-left:50px;}
		</style>
		<table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" align=\"left\" width=\"100%\">
		<tr>
			<td>
				<table cellpadding=\"1\" cellspacing=\"1\" border=\"0px\" width=\"100%\" style=\"padding-left:230px;padding-top:85px\">
				 
				<tr>
					<td class=\"sub-heading\">
						<div class=\"coloumn al wp65\" >
							<div class=\"pl50\">
								
								<div class=\"print_company pb10\"> 
								 To,</div>
								<div class=\"print_company pb10\">".$data['billing_name']."</div>
								<div class=\"print_addr\">".nl2br($data['b_addr'])."</div>      
								<div class=\"print_addr pb10\">";
								if(!empty($data['b_addr_city'])){
									$body.=$data['b_addr_city'] ;
								}
								if(!empty($data['b_addr_zip'])){
									$body.="&nbsp;".$data['b_addr_zip'] ;
								}
								if(!empty($data['b_addr_state'])){
									$body.="&nbsp;".$data['b_addr_state']."<br/>" ;
								} 
								if(!empty($data['b_addr_country'])){
									$body.="".$data['b_addr_country']." " ;
								} 						
								$body.="</div> ";
								if(!empty($data['phone'][0]['p_number'])){
									$body.="<div class=\"print_phone pb1\">Ph - "
									.$data['phone'][0]['cc']." - ".$data['phone'][0]['ac']." - ".$data['phone'][0]['p_number']."</div>" ;
								} 
								if(!empty($data['mobile'])){
									$body.="<div class=\"print_phone pb1\">
									Mobile No - ".$data['mobile']."</div>" ;
								} 
								$body.="
							</div>  
						</div>  
					</td>
				</tr>
				</table>
			</td>
		</tr>  
		</table>		
		";
			$data["number"]='prospect-address';
			$contentArr['content'] = $body ; 
			
			$contentArr['header'] = '' ;
			$contentArr['footer'] = '' ;
			createPAddressPDF($data["number"],$contentArr);
		}

    }
	
function createPAddressPDF( $q_no, $data ){
		require_once(DIR_FS_ADDONS .'/html2pdf/config.prospectaddr.inc.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
		parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
	  
		global 	$g_config;
		
		class ReportData extends Fetcher {
			var $content;
			var $url;
		
			function get_data($data) {
				return new FetchedDataURL($this->content, array(), "","");
			}
		
			function ReportData($content) {
				$this->content = $content;
			}
			
			function get_base_url() {
				return "";
			}
		}
		
		$contents 	= '';
		$contents = $data['content'];      
		
		// Configuration Settings.
		$g_config = array(
							'process_mode' 	=> 'single',
							'mode'		    => '',
							'pixels'		=> '750',
							'scalepoints'	=> '1',
							'renderimages'	=> '1',
							'renderlinks'	=> '1',
							'renderfields'	=> '1',
							'media'			=> 'A4',
							'cssmedia'		=> 'screen',
							//'leftmargin'	=> '30',
							'leftmargin'	=> '5',
							//'rightmargin'	=> '15',
							'rightmargin'	=> '5',
							'topmargin'		=> '5',
							'bottommargin'	=> '5',
							'encoding'		=> 'iso-8859-1',
							//'encoding'		=> 'UTF-8',
							'headerhtml'	=> $data['header'],
							'boydhtml'		=> $contents,
							'footerhtml'	=> $data['footer'],
							'watermarkhtml'	=> '',
							'pslevel'		=> '3',
							'method'		=> 'fpdf',
							'pdfversion'	=> '1.3',
							'output'		=> '2',
							'convert'		=> 'Convert File'
						);
		
		$media = Media::predefined('A4');
		$media->set_landscape(false);
		/*
		$media->set_margins(array(	'left' => 10,
									'right' => 10,
									'top' => 15,
									'bottom' => 35)
							);
		$media->set_pixels(700);
		*/
		 $media->set_margins(array(	'left' => 5,
									'right' => 5,
									'top' => 5,
									'bottom' => 5)
							);
		$media->set_pixels(740);
		
		$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
		$g_pt_scale = $g_px_scale * 1.43;
		
		$pipeline 					= PipelineFactory::create_default_pipeline("","");
	$pipeline->pre_tree_filters[] = new PreTreeFilterHeaderFooter( $g_config['headerhtml'],$g_config['footerhtml']);
	$pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
		if ($g_config['renderfields']) {
			$pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
		}

		// Changed so that script will try to fetch files via HTTP in this case) and to provide absolute 
		//paths to the image files.
		// http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
		//$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
		$pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
		//$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
		$pipeline->destination 			= new DestinationFile($q_no);
		 
		$pipeline->process($q_no, $media);
		$filename ="prospect-address.pdf";
		@chmod($filename, 0777);
		$contents = NULL;
		return true;
}
?>