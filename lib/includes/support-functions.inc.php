<?php

    /****************************************************
    * This function is used to read the corresponding 
    * email templates from the database and parse them
    * using the data array supplied.
    *
    * @return
    * The return value is an array the first value 
    * being the email Subject and the email Content.
    *****************************************************/
   function getParsedEmail(&$db, &$s, $variable, $data, &$email){
    
        $query = "SELECT * FROM ".TABLE_SETTINGS_EMAIL_TPL
                    ." WHERE name = '". $variable ."'"
                    ." AND status = '1'";
        if ( ($db->query($query)) && ($db->nf()>0) && $db->next_record() ){       
			$allowed_constant = array();
            $email_data = processSQLData($db->result());
            $temp  = explode("\n", $email_data["allowed_constant"]);
            if ( count($temp)>0 ) {
                foreach ( $temp as $value ) {
                    $value = explode(',', $value);
                    $allowed_constant = array_merge($allowed_constant, $value);
                }
            }
            //print_r($allowed_constant);exit;
            $assigned = array();
            foreach ( $allowed_constant as $value ) {
                if ( ($value = trim($value)) && !empty($value) ) {
                    preg_match('/(\{\$)(\w+)(\})/i', $value, $temp );//echo"<br/>\n";print_r($temp);
                    $assigned[] = $temp = $temp[2];
                    $s->assign(@$temp, @$data[$temp]);
                }
            }
 
            $email              = array();
            $email["subject"]   = $s->fetch("string:". $email_data["subject"]);
            $email["body"]      = $s->fetch("string:". $email_data["text"]);
            $email["isHTML"]    = $email_data["is_html"];
            $email["from_name"] = $email_data["from_name"];
            $email["from_email"]= $email_data["from_email"];
           
            // Clear other variables.
            
            $temp       = NULL;
            $assigned   = NULL;
            $key        = NULL;
            $value      = NULL;
            
            return (true);
        }
        else {
            return (false);
        }
    }

/*
 function getExecutivesWith(&$db, $conditions_arr) {
    $access_level   = '';
    $roles          = '';
    $condition      = '';
    $users          = array();

    foreach ( $conditions_arr as $conditions) {
        foreach ( $conditions as $type=>$value) {
            if ( $type == 'right') {
                $roles = getRights($db, $value);
                $roles = getRolesWithRight($db, $roles['id']);
                //print_r($roles);
            }
            elseif ( $type == 'access_level') {
                $access_level = $value;
            }
        }
        if ( count($roles)>0 ) {
            foreach ($roles as $role) {
                $condition .= " ( ( roles REGEXP '^". $role['id'] ."$' "
                                    ." OR roles REGEXP ',". $role['id'] ."$' "
                                    ." OR roles REGEXP ',". $role['id'] .",' "
                                    ." OR roles REGEXP '^". $role['id'] .",' ) "
                                ." AND access_level > $access_level) OR ";
            }
            
        }
    }
    $condition = substr($condition, 0, strlen($condition)-3);

    $query = "SELECT user_id, email, f_name, l_name FROM ". TABLE_USER
                ." WHERE $condition"
                ." AND status = '1'";
    if ( $db->query($query) && $db->nf()>0 ) {
        while ( $db->next_record() ) {
            $users[] = processSQLData($db->result());
        }
    }
    $access_level   = NULL;
    $roles          = NULL;
    $condition      = NULL;
    return ($users);
 }
*/
//Change on 2009-08-aug-17

function getExecutivesWith(&$db, $conditions_arr) {
    $access_level   = '';
    $roles          = '';
    $condition      = '';
    $users          = array();
    $access_sql='';
    foreach ( $conditions_arr as $conditions) {
        foreach ( $conditions as $type=>$value) {
            if ( $type == 'right') {
                $roles = getRights($db, $value);
                $roles = getRolesWithRight($db, $roles['id']);
                //print_r($roles);
            }
            elseif ( $type == 'access_level') {
                $access_level = $value;
                if(!isset($access_level)){
                    $access_sql = " AND access_level > $access_level ";
                }
            }
        }
        if ( count($roles)>0 ) {
            foreach ($roles as $role) {
                $condition .= " ( ( roles REGEXP '^". $role['id'] ."$' "
                                    ." OR roles REGEXP ',". $role['id'] ."$' "
                                    ." OR roles REGEXP ',". $role['id'] .",' "
                                    ." OR roles REGEXP '^". $role['id'] .",' ) "
                                .$access_sql.") OR ";
            }
            
        }
    }
    $condition = substr($condition, 0, strlen($condition)-3);

    $query = "SELECT user_id, email, f_name, l_name FROM ". TABLE_USER
                ." WHERE $condition"
                ." AND status = '1'";
    if ( $db->query($query) && $db->nf()>0 ) {
        while ( $db->next_record() ) {
            $users[] = processSQLData($db->result());
        }
    }
    $access_level   = NULL;
    $roles          = NULL;
    $condition      = NULL;
    return ($users);
 }

function getRights(&$db, $data) {
    $rights = array();
    
    $query = "SELECT id, title FROM ". TABLE_USER_RIGHTS
            ." WHERE id = '$data' OR value = '$data'";
    if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
        $rights = processSQLData($db->result());
    }
    return ($rights);
}
 

function getRolesWithRight(&$db, $rights) {
    $roles = array();
    
    if ( is_array($rights) ) {
        $condition = '';
        foreach ($rights as $right) {
            $condition .= " ( rights REGEXP '^". $right['id'] ."$' "
                        ." OR rights REGEXP ',". $right['id'] ."$' "
                        ." OR rights REGEXP ',". $right['id'] .",' "
                        ." OR rights REGEXP '^". $right['id'] .",' ) OR ";
        }
        $condition = substr($condition, 0, strlen($condition)-3);
    }
    else {
        $condition = " ( rights REGEXP '^". $rights ."$' "
                    ." OR rights REGEXP ',". $rights ."$' "
                    ." OR rights REGEXP ',". $rights .",' "
                    ." OR rights REGEXP '^". $rights .",' ) ";
    }
    
    $query = "SELECT id, title FROM ". TABLE_USER_ROLES
                ." WHERE $condition"
            ." AND status = '1'";
     if ( $db->query($query) && $db->nf()>0 ) {
        while ( $db->next_record() ) {
            $roles[] = processSQLData($db->result());
        }
     }
     return ($roles);
}


function getExecutivesWithRole(&$db, $roles) {
    $users = array();
    
    if ( is_array($roles) ) {
        $condition = '';
        foreach ($roles as $role) {
            $condition .= " ( roles REGEXP '^". $role['id'] ."$' "
                            ." OR roles REGEXP ',". $role['id'] ."$' "
                            ." OR roles REGEXP ',". $role['id'] .",' "
                            ." OR roles REGEXP '^". $role['id'] .",' ) OR ";
        }
        $condition = substr($condition, 0, strlen($condition)-3);
    }
    else {
        $condition = " ( roles REGEXP '^". $roles ."$' "
                        ." OR roles REGEXP ',". $roles ."$' "
                        ." OR roles REGEXP ',". $roles .",' "
                        ." OR roles REGEXP '^". $roles .",' ) ";
    }
    
    $query = "SELECT user_id, email, f_name, l_name FROM ". TABLE_USER
            ." WHERE $condition"
            ." AND status = '1'";
    if ( $db->query($query) && $db->nf()>0 ) {
        while ( $db->next_record() ) {
            $users[] = processSQLData($db->result());
        }
    }
    return ($users);
}

/**
 * This function is used to generate a random string with length between 
 * the specified by minimum length to maximum length. This function also 
 * used the digits array and alphabets array defined in the config.php.
 *
 * @param	$min		integer		(required) the minimum length of the string.
 * @param	$max		integer		(optional) the maximum length of the string.
 * 
 * @return	$rand_txt	string		the generated string.
 */
function getRandom($min=6, $max='', $rand_arr) { 
	$rand_txt	= '';
	$count 		= count($rand_arr)-1; 
	if ( empty($max) ) {
		$max = $min;
	}
	$length		= rand($min, $max);
	while ( strlen($rand_txt) < $length ) {
		$rand_txt .= $rand_arr[rand(0, $count)];
	} 
	return ($rand_txt);
}
function randomNumber($len,$characters=''){
	if(empty($characters)){
		$characters  = "23456789";//removed 0,1 to avoid confusion
	} 
	$randno = '';
	for ($i = 0; $i < $len; $i++) {
		$randno.= $characters[rand(0, strlen($characters))];
	}
	return $randno;
}
function randomString($len,$characters=''){
	if(empty($characters)){
		//$characters  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; //REmoved loO
		$characters  = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
	} 
	$randstring = '';
	for ($i = 0; $i < $len; $i++) {
		$randstring.= $characters[rand(0, strlen($characters))];
	}
	return $randstring;
}
?>