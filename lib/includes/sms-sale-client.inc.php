<?php
	class SmsSaleClient{
		
        const ACTIVE = 1;
		const DEACTIVE = 0;       
		
        /**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
         
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_SALE_CLIENT;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SMS_SALE_CLIENT;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
               return ( $total );
            }
            else {
                return false;
            }   
        }
    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
            $query = "SELECT * FROM ". TABLE_SMS_SALE_CLIENT
                        ." WHERE id = '$id'";
                        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[''] = processSqlData($db->result());
                    }
                }
            }
            
            $query = "DELETE FROM ". TABLE_SMS_SALE_CLIENT
                        ." WHERE id = '$id'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Record has been deleted.");                      
            }
            else {
                $messages->setErrorMessage("The Record was not deleted.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
           
            $sms_sale_list  = NULL;
            $condition  = " WHERE client_id='".$data['client_id']."' AND client_acc_id='".$data['client_acc_id']."'";

            if ( (SmsSaleClient::getList($db, $sms_sale_list, 'id', $condition)) > 0 ) {
                foreach ( $sms_sale_list as $sale) {
                    $messages->setErrorMessage('This Client and Account pair is alredy exists. Please select another.');
                }
            }
            
            if ( !isset($data['client_id']) || empty($data['client_id']) ) {
                $messages->setErrorMessage("Select Client.");
            }       
            if ( !isset($data['client_acc_id']) || empty($data['client_acc_id']) ) {
                $messages->setErrorMessage("Select Account.");
            }       
			if ( !isset($data['allotted_sms']) || empty($data['allotted_sms']) ) {
                $messages->setErrorMessage("No. of sms can not be empty.");
            }elseif (!is_numeric($data['allotted_sms']) ){
                $messages->setErrorMessage("No. of sms should be in numeric.");
            }
            if ( !isset($data['rate']) || empty($data['rate']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }elseif (!is_numeric($data['rate']) ){
                $messages->setErrorMessage("Rate should be in numeric.");
            }
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("Amount can not be empty.");
            }elseif (!is_numeric($data['amount']) ){
                $messages->setErrorMessage("Amount should be in numeric.");
            }        
            if ( !isset($data['do_expiry']) || empty($data['do_expiry']) ) {
                $messages->setErrorMessage("Plz enter date of expiry.");
            }
            if ( !empty($data['do_expiry']) ) {
                $temp = explode('/', $data['do_expiry']);
                $data['do_expiry'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }            
            if($data['balance_sms'] < $data['allotted_sms']){
                $messages->setErrorMessage("You are not allowed to alloted these many sms as it is more than your balance sms.");
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            
			if ( !isset($data['allotted_sms']) || empty($data['allotted_sms']) ) {
                $messages->setErrorMessage("No. of sms can not be empty.");
            }elseif (!is_numeric($data['allotted_sms']) ){
                $messages->setErrorMessage("No. of sms should be in numeric.");
            }
            if ( !isset($data['rate']) || empty($data['rate']) ) {
                $messages->setErrorMessage('Rate should be provided.');
            }elseif (!is_numeric($data['rate']) ){
                $messages->setErrorMessage("Rate should be in numeric.");
            }
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("Amount can not be empty.");
            }elseif (!is_numeric($data['amount']) ){
                $messages->setErrorMessage("Amount should be in numeric.");
            }
            if ( !isset($data['do_expiry']) || empty($data['do_expiry']) ) {
                $messages->setErrorMessage("Plz enter date of expiry.");
            }            
            if ( !empty($data['do_expiry']) ) {
                $temp = explode('/', $data['do_expiry']);
                $data['do_expiry'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if($data['balance_sms'] < $data['allotted_sms']){
                $messages->setErrorMessage("You are not allowed to alloted these many sms as it is more than your balance sms.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function getSaleHistoryLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;

			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				 $query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_SALE_CLIENT_HISTORY
                        ." LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id= " . TABLE_SMS_SALE_CLIENT_HISTORY.".client_id"
                        ." LEFT JOIN ".TABLE_SMS_CLIENT_ACCOUNT." ON ".TABLE_SMS_CLIENT_ACCOUNT.".id= " . TABLE_SMS_SALE_CLIENT_HISTORY.".client_acc_id"
                        ." LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id= " . TABLE_SMS_SALE_CLIENT_HISTORY.".created_by";
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
    }
?>
