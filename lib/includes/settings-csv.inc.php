<?php 

class CSV {

    function getRestrictions($type='') {
          $restrictions = array(
                            'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), // 1024 * 1024 = 1048576 = 1MB
                            //'MIME'      => array('text/csv','application/vnd.ms-excel', 'text/plain','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
                            'MIME'      => array('text/csv','application/vnd.ms-excel','text/plain',),
                            //'EXT'       => array('csv', 'txt','xlsx')
                            'EXT'       => array('csv','txt')
                             );
        
        if ( empty($type) ) {
            return ($restrictions);
        }
        else {
            return($restrictions[$type]);
        }
    }
    
    
    function getTables(&$db) {
        $count  = 0;
        $tables = array();
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[] = $temp[$i]['table_name'];
        }
        $temp   = NULL;
        return($tables);
    }
    
    
    function getTablesData(&$db, &$tables) {
        $count  = 0;
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[$i] = array('table_name'=> $temp[$i]['table_name'],
                                'fields'    => $db->metadata($temp[$i]['table_name'])
                               );
        }
        $temp   = NULL;
        return($count);
    }
    
    
    function validateUpload($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }

        if ( empty($file['file_csv']['error']) ) {
            if ( !empty($file['file_csv']['tmp_name']) ) {
                if ( $file['file_csv']['size'] == 0 ) {
                    $messages->setErrorMessage('The Uploaded file is empty.');
                }
                if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
                    $messages->setErrorMessage('The File type is not allowed to be uploaded.');
                }
                if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
                    $messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
                }
            }
            else {
                $messages->setErrorMessage('Server Error: File was not uploaded.');
            }
        }
        else {
            $messages->setErrorMessage('Unexpected Error: File was not uploaded.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    function validateUploadAdv($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }

        if ( empty($file['file_csv']['error']) ) {
            if ( !empty($file['file_csv']['tmp_name']) ) {
                if ( $file['file_csv']['size'] == 0 ) {
                    $messages->setErrorMessage('The Uploaded file is empty.');
                }
                if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
                    $messages->setErrorMessage('The File type is not allowed to be uploaded.');
                }
                if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
                    $messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
                }
                if ( !isset($data['is_field_names']) ) {
                    $messages->setErrorMessage('Please select one of the Option to answer if 1st row contains table fields.');
                }
                elseif ( $data['is_field_names'] == '1' ) {
                    // Check if the table name is present in the database or not.
                    if ( !in_array($data['table_name'], CSV::getTables($db)) ) {
                        $messages->setErrorMessage('The selected table was not found.');
                    }
                }
                elseif ( $data['is_field_names'] == '2' ) {
                    // Do nothing right now.
                }
                else {
                    $messages->setErrorMessage('Does the First Row contains Table Fields is not answered properly.');
                }
            }
            else {
                $messages->setErrorMessage('Server Error: File was not uploaded.');
            }
        }
        else {
            $messages->setErrorMessage('Unexpected Error: File was not uploaded.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    function duplicateFieldValue(&$db, $table, $field_name, $field_value) {
        $query = "SELECT $field_name FROM $table WHERE $field_name = '$field_value' ";
        
        if ( $db->query($query) && $db->nf()>0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
}
?>