<?php
$content1.= "
	<page pageset=\"old\" > 
		<table cellspacing=\"0\" cellpadding=\"0px\" border=\"0\" style=\"width:650px;\">
			<tr>
				<td style=\"width:10px;\"><img src=\"".$variables['images']."/sec-gray-corner.jpg\" width=\"10px\" height=\"15px\" 
				border=\"0\" /></td>
				<td style=\"width:600px;\" class=\"bg1 al content-heading\"><span class=\"pl2\">Terms and Conditions</span></td>
				<td style=\"width:10px;\"><img src=\"".$variables['images']."/sec-right-corner.jpg\" width=\"10px\" 
				height=\"15px\" border=\"0\" /></td>
			</tr>	 
			<tr>
				<td class=\"al pt3\" colspan=\"3\" valign=\"top\" style=\"width:650px;\" >
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:0px;width:650px;\">
					<tr>
						<td style=\"width:325px;\" valign=\"top\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:5px;width:325px;\">
							<tr>
								<td class=\"content-small-term pl pr5\">
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn\">By making payment against this invoice/proposal, it is agreed between ".$data['company_name']." ('".$data['company_quot_prefix']."') and the Customer as follows -</div>
								</div>
							   </td>
							</tr> 	
							<tr>
								<td class=\"content-small b pl pt2 pr5\"> A. Definitions</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term pl10 pr5\">
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Extra Efforts' or 'Additional Services' means any efforts or services which are not mentioned in the proposal.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Charges' means all fees, charges/tariffs, costs and rates chargeable by ".$data['company_quot_prefix']." from time to time for providing the Customer with the Service, for Software Products, for Goods and Additional Services and other Government levies.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Customer or Client' shall mean any person, partnership firm or private limited company or limited company or such other organisation authorized by ".$data['company_quot_prefix']." to use the services or to whom ".$data['company_quot_prefix']." sell goods or services.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Government' shall mean the Government of India and/or the State Government of Maharashtra or such other local authority, as the case may be.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Services' shall mean all/any services made available by ".$data['company_quot_prefix']." or via its associates or channel partners.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Software Products' of ".$data['company_quot_prefix']." are like myCraft<sup>TM</sup>, myGallery, Nerve Center<sup>TM</sup> etc. and are products/solutions developed by ".$data['company_quot_prefix']." and licenses for which are sold by ".$data['company_quot_prefix']." or via its associates or channel partners.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Goods' shall mean all the Goods/Material/Equipment sold by ".$data['company_quot_prefix']." or via its associates or channel partners.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'MySMEERP' is the online Customer Support Portal for support, billing, communication etc.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Renewal/Expiry Date' means the renewal/expiry date of Software Product/Equipment/Goods/Service as mentioned in Invoice.
									</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">'Support Email' means support@smeerptech.com
									</div>
								</div>												
								</td>
							</tr>	
							<tr>
								<td class=\"content-small b pl pt2 pr5\"> B. Provision of Services</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term pl10 pr5\" style=\"width:320px;\">
									<div class=\"row\">".$data['company_quot_prefix']." agrees to provide the Services or Goods to the Customer subject to the terms and conditions of this agreement.
									</div>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pl pt2 pr5\"> C. Obligations of ".$data['company_quot_prefix']."</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term pl10 pr5\" >
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." shall use reasonable efforts to make the Services available to the Customer at all times.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">The availability and quality of the Services may be affected by the factors outside ".$data['company_quot_prefix']."'s control such as physical obstructions, geographic, weather conditions and other causes of interference or faults.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">Services may be suspended in whole or in part at any time, without notice, if Network fails or requires maintenance. ".$data['company_quot_prefix']." will make all reasonable efforts to minimise the frequency and duration of such events. The Customer will remain liable for all charges during the period of suspension unless ".$data['company_quot_prefix']." in its discretion decides otherwise.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \"> ".$data['company_quot_prefix']." has the sole right and discretion to vary or increase the Charges at any time on reasonable notice to the Customer.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." reserves the right to apply a monthly/quarterly/half-yearly/yearly financial limit and such other conditions for Charges incurred by the Customer and to demand interim advance payment, suspend or disconnect access to the Services if such limits are exceeded.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;6.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." has the right to check the credentials of the Customer including the Customer's financial standing and to use the services of any person or agency for such purpose.</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pl pt2 pr5\"> D. Obligations of the Customer</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term pl10 pr5\">
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn \"> The Customer hereby expressly agrees:</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;1.</div>
									<div class=\"coloumn \"> To make payments for the Services on the following basis</div>
								</div>
								<div class=\"row pl15\" style=\"width:320px;\">(a)	payment will be due when ".$data['company_quot_prefix']." raises the billing statement on the Customer.</div>
								<div class=\"row pl15\" style=\"width:320px;\">(b)	payment will be made on or before the due date mentioned in the invoice or as per schedule mentioned in the proposal, failing which ".$data['company_quot_prefix']." shall be entitled to charge interest @18% per annum or INR 500 whichever is higher and/or late fees on all outstanding Charges from due date till date of payment &amp; shall be entitled to discontinue Services or delivery of Goods/Software Products, without notice, in its sole discretion. Cheque return will attract a penalty of Rs. 1000 separately.</div>
								<div class=\"row pl15\" style=\"width:320px;\">(c)	in case of delinquent payment or non-payment, neither the domain name ownership cannot be claimed by Customer nor can be transferred to any other service provider.</div>
								<div class=\"row pl15\">(d)	the Customer will pay all the costs of collection and legal expenses, with interest should it become necessary to refer the matter to a collection agency or to legal recourse to enforce payment.</div>
								<div class=\"row pl15\" style=\"width:320px;\">(e)	".$data['company_quot_prefix']." shall be entitled to apply payments/deposits/advances made by the Customer towards any Charges outstanding including for any other ".$data['company_quot_prefix']." service or goods availed by the customer.</div>
								<div class=\"row pl15\" style=\"width:320px;\">(f)	payments will be made in cash, credit card or A/c Payee cheque or demand draft or internet banking or any other instrument drawn on any bank in Jhena and payable at Jhena.</div>
								<div class=\"row pl15\">(g)	the Customer shall be liable for all Charges for the Services provided to the Customer whether or not the Services have been used by the Customer.</div>
								<div class=\"row pl15\" style=\"width:320px;\">(h)	in the event of any dispute regarding the Charges, the Customer agrees to pay ".$data['company_quot_prefix']." Charges billed pending resolution of such dispute.</div>
								<div class=\"row pl15\" style=\"width:320px;\">(i)	the Customer shall be liable to pay for the Services provided even if he/she does not receive the bills. It will be the Customer's responsibility to make enquiries in case of non-receipt of bills.</div>
								<div class=\"row pl15\" style=\"width:320px;\">(j)	Charges payable by the Customer are exclusive of taxes, duties or levies payable, unless expressly stated to the contrary in the billing statement.</div>
								<div class=\"row pl15\" style=\"width:320px;\">(k)	any advance/security deposit paid by the Customer shall be adjusted against any dues payable by the Customer to ".$data['company_quot_prefix']." and balance if any will be refunded by ".$data['company_quot_prefix']." within 60 days from the deactivation of the Services.</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;2.</div>
									<div class=\"coloumn \" >To make advance payment for Charges including service charges if billed to customer by ".$data['company_quot_prefix'].".</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;3.</div>
									<div class=\"coloumn \">To not use or cause or allow others to use the Services for any improper, immoral or unlawful purpose including in any manner (for e.g. Spamming, Bulk Emailing, Transmission of infected content etc.) which may jeopardise or impair the operation of the Network and/or the Services.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;4.</div>
									<div class=\"coloumn \">To comply with instructions issued by Government or ".$data['company_quot_prefix'].", concerning Customer's access to and use of Services.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;5.</div>
									<div class=\"coloumn \">To furnish correct and complete information and documents as required by ".$data['company_quot_prefix']." from time to time. The Services agreed to be provided by ".$data['company_quot_prefix']." shall always be subject to verification of the Customer's credentials and documents and if at any time, any information and/or documents furnished by the Customer is/are found incorrect or incomplete or suspicious, ".$data['company_quot_prefix']." shall be entitled to suspend/terminate the Service forthwith without any further notice.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;6.</div>
									<div class=\"coloumn \">That ".$data['company_quot_prefix']." may suspend the Service in whole or in part at any time without prior notice and without assigning any reason thereto. ".$data['company_quot_prefix']." reserves the right to Charge for re-activation.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;7.</div>
									<div class=\"coloumn \">Customer is liable (a) for the Charges during the period of suspension and thereafter (b) to pay the overages as applicable and (c) Charges towards Extra Efforts</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;8.</div>
									<div class=\"coloumn \">To comply with the applicable laws, rules and regulation regarding the use of the Services and procurement of the Equipment including but not limited to relevant tax laws and import control regulations.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;9.</div>
									<div class=\"coloumn \">To pay to ".$data['company_quot_prefix']." such amount as ".$data['company_quot_prefix']." may require as security for the due performance of the Customer's obligations under these terms and conditions. ".$data['company_quot_prefix']." may set off these amounts against any cost, damages or expenses which ".$data['company_quot_prefix']." may suffer or incur as a result of the Customer's failure to perform any of these obligations. Security deposit amount shall not carry any interest.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">10.</div>
									<div class=\"coloumn \">To inform ".$data['company_quot_prefix'].", in writing, of any changes in billing name or billing address or email id or cellular number. Any written communication, billing statement or notice from ".$data['company_quot_prefix']." to the Customer will be deemed as served within 48 hours of posting by ordinary mail or email.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">11.</div>
									<div class=\"coloumn \">To notify ".$data['company_quot_prefix']." immediately in case of any complaints with regards to the Services via email to Support Email</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">12.</div>
									<div class=\"coloumn \">Not to assign any right or interest under this agreement without ".$data['company_quot_prefix']."'s prior written consent.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">13.</div>
									<div class=\"coloumn \">To be bound at all times by any modifications and or variations made to these terms and conditions.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">14.</div>
									<div class=\"coloumn \">Customer is not entitled to assign/transfer/resell/lease/rent or create any charge/lien on the Service of any nature whatsoever or Software Product owned by ".$data['company_quot_prefix'].", without prior permission of ".$data['company_quot_prefix'].". Any transfer affected in contravention of the express terms contained herein, shall not absolve the Customer of his/her primary duty towards ".$data['company_quot_prefix']." for usages charges levied against the Customer.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">15.</div>
									<div class=\"coloumn \">To timely provide ".$data['company_quot_prefix']." with the certificates etc. towards any deductions as per the law.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">17.</div>
									<div class=\"coloumn \">Security and Backup of the data to which Customer is provided access with is the liability of Customer.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">18.</div>
									<div class=\"coloumn \">Customer must verify the credentials of the visiting ".$data['company_quot_prefix']."'s personnel.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">19.</div>
									<div class=\"coloumn \">Neither ".$data['company_quot_prefix']."'s personnel will install any illegal/pirated software/text/images for Customer nor any Illegal/pirated software/text/images are covered under maintenance service support.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">21.</div>
									<div class=\"coloumn \">Work will commence once ".$data['company_quot_prefix']." receives (a) Work/Purchase order referring proposal, scope of work and payment schedule (b) Duly signed and stamped copy of proposal (c) Duly filled, signed and stamped Customer Agreement form.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">22.</div>
									<div class=\"coloumn \">Customer must designate only one of his/her personnel to coordinate and to approve the work/delivery. Customer is not allowed to change designated personnel till completion of work. In case of factors outside control, change of personnel is allowed if agreeable to both the parties.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">23.</div>
									<div class=\"coloumn \">Domain and Web hosting account with data/email is deleted after seven days of Renewal/Expiry Date. Late renewal during these seven days attracts late payment Charges too.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">24.</div>
									<div class=\"coloumn \">".$data['company_quot_prefix']." will remind Customer about the renewal dates via SMS/Email automated notification. Customer is responsible to remember renewal dates and make renewal payments before 60 days of Renewal/Expiry Date.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">25.</div>
									<div class=\"coloumn \">Customer must provide information/references for his/her choice for design, images in JPEG format, content in editable MS Word format and Logo in editable CDR or PSD format via email and before commencement of work. Our designers will use all the images provided as-is. Besides basic cropping, ".$data['company_quot_prefix']." is unable to perform advanced manipulation. Logo design, Content writing or typing or proof-reading or extra design samples is separate job work needing Extra Efforts and is separately chargeable. Static page means an A4 size page containing text in Arial font size 12.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">26.</div>
									<div class=\"coloumn \">Customer is liable to pay any statutory duties or levies or taxes as applicable by the law. </div>
								</div>						
								</td>
							</tr>					
							</table>
						</td>
						<td style=\"width:325px;\" valign=\"top\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:2px;padding-right:2px;width:325px;\">
								<tr>
								<td class=\"content-small-term pr5\">
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">27.</div>
									<div class=\"coloumn \">Once the order is confirmed Customer must sign up with MySMEERP.</div>
								</div>						
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">28.</div>
									<div class=\"coloumn \">".$data['company_quot_prefix']." will not be responsible for any delay in schedule caused due to delay in providing the content to ".$data['company_quot_prefix']." or non-availability of any of Customer official or any other delay on Customer part. Customer will not hold back any of ".$data['company_quot_prefix']."'s payment for delays on part of Customer. Content received after the completion of work OR in excess as per proposal will be termed as extra and separately chargeable.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">29.</div>
									<div class=\"coloumn \">Transfer of domain name from his/her existing service provider to ".$data['company_quot_prefix']." is the sole responsibility of Customer.</div>
								</div>
								<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">30.</div>
									<div class=\"coloumn \">Customer must use MySMEERP or email to Support Email for service and support.</div>
								</div>						
								</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\"> E. Software Product License to use:</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">If billed for, Customer gets the license to use for the ".$data['company_quot_prefix']."'s Software Products. ".$data['company_quot_prefix']."'s Software Products are provided on license to use basis only on ".$data['company_quot_prefix']."'s web servers without ftp & hosting control panel access. The license to use of Software Products is non-transferable to any other Domain name or other person/party and is subject to yearly renewal against annual maintenance support charges as specified in the proposal.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">Under all/any circumstances, programming code of Software Products is the sole property of ".$data['company_quot_prefix']." and under no circumstances Customer can claim any rights towards the ownership of product or code. License of the product is only for using it i.e. License to use.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">Credits to Software Product and/or ".$data['company_quot_prefix']." are compulsorily mentioned on the Customer website. Credit to ".$data['company_quot_prefix']." is compulsorily mentioned on the designed and/or printed material of the Customer.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">Any customization done in the Software Product doesn't make the Customer the owner of product. Under all/any circumstances the Software Product remains the property of ".$data['company_quot_prefix']." with full rights over the entire product and customizations done.</div>
									</div>
									</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\"> F. Validity</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">Both parties agree that, this agreement has been duly authorised and executed and is valid and binding and is enforceable in law in accordance with its terms.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">The validity construction and performance of this agreement shall be governed by and interpreted in accordance with the laws of the Republic of India.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">The courts in Jhena shall have the jurisdiction.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">Should any provision of this agreement be or become ineffective or be held to be invalid, this shall not affect the validity of the remaining provisions. Any invalid provision in this agreement shall be replaced, interpreted or supplemented as the case may be in such a manner that the intended economic purpose of the agreement will be achieved.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">This agreement is complete and exclusive statement of agreement between parties and it supersedes all understanding or prior agreement, whether oral or written and all representations or other communications between parties.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;6.</div>
										<div class=\"coloumn \">These terms & conditions are subject to applicable Indian laws, rules and regulation framed thereunder and any statutory modifications or re-enactment for time being in force and any other Government regulations issued from time to time.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;7.</div>
										<div class=\"coloumn \">Prices/Availability/Specifications are subject to change without prior notice.</div>
									</div>
									</td>
								</tr>	
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\">G. Support, Goods Return, Warranties, Disclaimer of warranties</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;1.</div>
									<div class=\"coloumn \">".$data['company_quot_prefix']." sends Customer the information related to terms/conditions/services/offers/warnings/support/advisories etc. via email/sms/telephone. It is Customer's responsibility to do the needful accordingly.</div>
									</div>
									</td>
								</tr>	
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\" style=\"width:320px;\">
									<div class=\"coloumn pr5\">&nbsp;2.</div>
									<div class=\"coloumn \">Annual Maintenance Support Charge for renewal of License to use of Software Product is compulsory as specified in the proposal and includes services (a) Maintaining Software Product in working condition (b) FREE feasible patches and updates (c) Customer support via email, telephone and MySMEERP</div>
									</div>
									</td>
								</tr>						
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
										<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">Goods, Software Products, Services once sold will not be taken back or exchanged and payment will not be refunded under any circumstances. Our risk & responsibility ceases on delivery of the goods. After sales support services is valid only if applicable to purchased item and explicitly mentioned in the invoice.</div>
										</div>
									</td>
								</tr>	
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
										<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." makes no representation or warranty other than those set forth in this agreement. ".$data['company_quot_prefix']." expressly disclaims all other warranties express or implied, including, but not limited to any implied warranty or merchantability or fitness for a particular purpose.</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
										<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">Any goods or equipment re-sold by ".$data['company_quot_prefix']." will carry the warranty, if any, of the manufacturer. Service to such goods or equipment, if not explicitly mentioned in invoice, if any, will be provided by authorized service centre of the manufacturer. ".$data['company_quot_prefix']." is not liable towards service support or replacement under warranty, if any, of any goods or equipment manufactured by third party and that ".$data['company_quot_prefix']." is just reselling.</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\"> H. Disclaimer of liability</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." shall not be liable to the Customer for any loss or damage whatsoever or howsoever caused arising directly or indirectly in connection with this agreement, the Services or Products or Equipment or Goods, their use, application or otherwise except to the extent to which it is unlawful to exclude such liability.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">Malfunction/Limitation in working of any of ".$data['company_quot_prefix']."'s Software Product due to any change in third party Software/Hardware on which ".$data['company_quot_prefix']."'s Software Product is dependent for functioning is not covered under maintenance support and ".$data['company_quot_prefix']." cannot be held responsible for it. Though ".$data['company_quot_prefix']." shall use reasonable efforts for resolution of issue if feasible. However Customer will be liable towards Charges for these Extra Efforts.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">Under any circumstances, ".$data['company_quot_prefix']." will not be responsible for any kind of loss / damage of data / equipment etc. while / due to working ".$data['company_quot_prefix']."'s service personnel at Customer's site. All the maintenance and service related work executed is at the risk of Customer. Customer has rights to ask ".$data['company_quot_prefix']."'s service personnel to work under Customer's authorized personnel's vigilance, seek information and take decision regarding maintenance / service work.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">Notwithstanding the generality of (a) above, ".$data['company_quot_prefix']." expressly excludes liability for consequential loss, damage or for loss of profit, business revenue, goodwill or anticipated savings.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." at its discretion, may send to the Customer various information on his/her cellular number through SMS or on his/her email id through Email or on his/her billing address through mail or otherwise, as an Additional Service. In case the Customer does not wish to receive such information he/she may notify ".$data['company_quot_prefix']." via email at Support Email</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;6.</div>
										<div class=\"coloumn \">In the event that any exclusion contained in this agreement shall be held to be invalid for any reason, and ".$data['company_quot_prefix']." becomes liable for the loss or damage that it may otherwise not have been liable, such liability shall be limited to the cost of the Services actually paid for by the Customer to ".$data['company_quot_prefix']." during the relevant period.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;7.</div>
										<div class=\"coloumn \">Customer agrees to indemnify and keep ".$data['company_quot_prefix']." harmless and defend ".$data['company_quot_prefix']." at its own expense from and against all claims arising as a result of breach of this agreement and from all taxes, duties or levies.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;8.</div>
										<div class=\"coloumn \">".$data['company_quot_prefix']." advises Customer to make cash payments only after verifying the credentials (ID card etc.) of the visiting personnel of ".$data['company_quot_prefix'].". ".$data['company_quot_prefix']." encourages cheque, demand draft or internet banking transfer to avoid any untoward incident. ".$data['company_quot_prefix']." will not be liable for any such loss incurring due to handing over of cash payment to unauthorized personnel.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;9.</div>
										<div class=\"coloumn \">Customer agrees that any request/communication received from Customer's and his/her authorized personnel email ids or mobiles numbers on ".$data['company_quot_prefix']."'s Support Email via email or SMS shall be deemed to be valid request/communication from the Customer. Nothing herein shall apply with respect to the notice to be given by the Customer section J or any other provisions of this agreement.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">10.</div>
										<div class=\"coloumn \">Customer must regularly update and verify credentials of his / her authorized personnel registered with MySMEERP and in case of any issue must inform ".$data['company_quot_prefix']." immediately and confirm the same in writing to avoid loss/damage. Liability of any loss due to misuse/mistake/mishandling/loss of confidential information by/due to Customer or his/her authorized personnel remains with Customer and ".$data['company_quot_prefix']." will not be held responsible under any circumstances.</div>
									</div>							
									</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\">I. Termination</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">	Either party shall have the right to terminate the agreement by giving 60 days prior notice in writing.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;2.</div>
										<div class=\"coloumn \">Notwithstanding anything contained herein, ".$data['company_quot_prefix']." shall be entitled to terminate this agreement and the Services if (a)	the Government either suspends or terminates the License or Permission or the Services temporarily or otherwise (b)	at any time the Customer fails to satisfy the requisite credit checks or provides fraudulent information to ".$data['company_quot_prefix'].". (c)	the Customer fails to pay its subscription or the Charges due (d)	the Customer is in breach of any other terms of the agreement and the Customer does not remedy the breach within seven (7) days of the day of receipt of a written / email notice from ".$data['company_quot_prefix']." specifying the breach.</div>
										</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;3.</div>
										<div class=\"coloumn \">The agreement may also be terminated at the option of either party, on the happening of the following events (a) if either party is declared insolvent, bankrupt or is liquidated or dissolved (b)	if a trustee or receiver is appointed to take over the assets of either party (c)	if the Government requires any of this agreement to be revised in such a way as to cause significant adverse consequences to either party</div>
										</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;4.</div>
										<div class=\"coloumn \">Termination of this agreement under the preceding provisions shall be without prejudice to and in addition to any right or remedy available to the terminating party under any applicable law or statute.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;5.</div>
										<div class=\"coloumn \">In the event of termination of the agreement for any reason whatsoever, ".$data['company_quot_prefix']." shall be entitled to recover all outstanding Charges and dues from the Customer.</div>
									</div>
									<div class=\"row\" style=\"width:320px;\">
										<div class=\"coloumn pr5\">&nbsp;6.</div>
										<div class=\"coloumn \">If the agreement is terminated for reasons of fraudulent information provided or misuse or unlawful use by the Customer, the security deposit shall be forfeited.</div>
									</div>				
									</td>
								</tr>
								<tr>
									<td class=\"content-small b pl5 pt2 pr5\"> J. Miscellaneous</td>
								</tr>
								<tr>
									<td class=\"pt2 content-small-term pl10 pr5\">
									<div class=\"row\">
										<div class=\"coloumn pr5\">&nbsp;1.</div>
										<div class=\"coloumn \">All notices required to be given to 
										".$data['company_quot_prefix']." pursuant to this agreement shall be in writing and shall be directed by registered post to the Registered office at ".$data['company_name'].", Plot no. 17, MG street, Jhena 440 025. www.SMEERPTech.in support@smeerptech.com</div>
									</div>				
									</td>
								</tr>
							</table>
						</td>
					</tr>			
					</table>
				</td>
			</tr>
		</table>    
	</page>";
?>