<?php
	class ProspectsQuotation {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
        const DELETED = 3;
		const COMPLETED = 4;

		const NONEAPPROVAL  = 0;
		const SEND_FOR_APPROVAL  = 1;
		const APPROVED = 2;
        const DISAPPROVED = 3;
		
		const PENDING_INV_R   = 0;
		const COMPLETED_INV_R = 1;
		const CANCELLED_INV_R = 2;
		
		function getStatus() {
			$status = array(
							//'ACTIVE'    => ProspectsQuotation::ACTIVE,
							'PENDING'   => ProspectsQuotation::PENDING,
							'BLOCKED' => ProspectsQuotation::BLOCKED,
                            'CANCELLED'   => ProspectsQuotation::DELETED,
                            //'COMPLETED' => ProspectsQuotation::COMPLETED
						);
			return ($status);
		}
		function getQuotStatus(){
			$status = array(							 
							'NONE' => ProspectsQuotation::NONEAPPROVAL,
							'PENDING FOR APPROVAL' => ProspectsQuotation::SEND_FOR_APPROVAL,
							'APPROVED' 			=> ProspectsQuotation::APPROVED, 
                            'DISAPPROVED'  		=> ProspectsQuotation::DISAPPROVED 
						);
			return ($status);
		}
		function getReminderStatus(){
			$status = array('PENDING' => ProspectsQuotation::PENDING_INV_R,							 
							'COMPLETED'   => ProspectsQuotation::COMPLETED_INV_R,
                            'CANCELLED'   => ProspectsQuotation::CANCELLED_INV_R
						);
			return ($status);
		}
        /*
         array of days before which reminder of expiry has been sent
         ie 60 days before expiry,
            30 days before expiry
        */        
        function expirySetDays() {
			$arr = array(120=>120,
                         90=>90
						);
			return ($arr);
		}
        
        function getStatusTitle() {
            $status = array(ProspectsQuotation::BLOCKED    => 'Bad Debt',
                            ProspectsQuotation::ACTIVE     => 'Paid Fully',
                            ProspectsQuotation::PENDING    => 'Payment Pending',
                            ProspectsQuotation::DELETED    => 'Deleted',
                           // ProspectsQuotation::COMPLETED  => 'Renewed',
                            ProspectsQuotation::COMPLETED  => 'Completed',
                            'CANCELLED'         => 'Bad Debt',
                            'ACTIVE'            => 'Active',
                           //'ACTIVE'            => 'Paid Fully',
                            'PENDING'           => 'Payment Pending',
                            'DELETED'           => 'Deleted',
                            //'PROCESSED'         => 'Renewed',
                            //'PROCESSED'         => 'Completed',
                           );
            return ($status);
        }
        
		/**
		 *	Function to get all the ProspectsQuotations.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			//$query .= " FROM ". TABLE_PROSPECTS_QUOTATION;
			$query .= " FROM ". TABLE_PROSPECTS_QUOTATION ;
            $query .= " LEFT JOIN ". TABLE_PROSPECTS_ORDERS
                        ." ON ". TABLE_PROSPECTS_ORDERS .".number = ". TABLE_PROSPECTS_QUOTATION .".or_no ";
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_PROSPECTS_QUOTATION .".created_by ";
            $query .= " LEFT JOIN ". TABLE_PROSPECTS
                        ." ON ". TABLE_PROSPECTS .".user_id = ". TABLE_PROSPECTS_QUOTATION .".client ";            
			 $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            /* comment 2009-03-mar-21 BOF
            $query .= " FROM ". TABLE_PROSPECTS_QUOTATION;
            $query .= " LEFT JOIN ". TABLE_PROSPECTS
                        ." ON ". TABLE_PROSPECTS .".user_id = ". TABLE_PROSPECTS_QUOTATION .".client ";
            comment 2009-03-mar-21 EOF
            */
            
            $query .= " FROM ". TABLE_PROSPECTS_QUOTATION ;
            $query .= " LEFT JOIN ". TABLE_PROSPECTS_ORDERS
                        ." ON ". TABLE_PROSPECTS_ORDERS .".number = ". TABLE_PROSPECTS_QUOTATION .".or_no ";
            //$query .= " LEFT JOIN ". TABLE_BILL_ORD_P
                 //       ." ON ". TABLE_BILL_ORD_P .".ord_no = ". TABLE_PROSPECTS_QUOTATION .".or_no ";
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_PROSPECTS_QUOTATION .".created_by ";
            $query .= " LEFT JOIN ". TABLE_PROSPECTS
                        ." ON ". TABLE_PROSPECTS .".user_id = ". TABLE_PROSPECTS_QUOTATION .".client ";
            
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
               
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( in_array($status_new, ProspectsQuotation::getStatus()) ) {
                $order = NULL;
                if ( (ProspectsQuotation::getList($db, $invoice, TABLE_PROSPECTS_QUOTATION.'.id,'.TABLE_PROSPECTS_QUOTATION.'.access_level,'.TABLE_PROSPECTS_QUOTATION.'.created_by,'.TABLE_PROSPECTS_QUOTATION.'.status', " WHERE ".TABLE_PROSPECTS_QUOTATION.".id = '$id'")) > 0 ) {
                    $invoice = $invoice[0];
					
					/* // Check Access Level for the invoice.
					if ( ($invoice['created_by'] == $my['user_id']) && ($invoice['access_level'] >= $access_level) ) {
						$messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
					}
					elseif ( ($invoice['created_by'] != $my['user_id']) && ($invoice['access_level'] >= $access_level_ot) ) {
						$messages->setErrorMessage("Cannot change Status of Invoice created by others, with current Access Level.");
					}
					else { */
                    	if ( $invoice['status'] != ProspectsQuotation::COMPLETED ) {
                            $query = "UPDATE ". TABLE_PROSPECTS_QUOTATION
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("Status not updated. New and Old status is same.");
                        }
                    //}
                }
                else {
                    $messages->setErrorMessage("The Quotation was not found.");
                }
            }
            else {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $order = NULL;
            if ( (ProspectsQuotation::getList($db, $order, TABLE_PROSPECTS_QUOTATION.'.id,'.TABLE_PROSPECTS_QUOTATION.'.or_no,'.TABLE_PROSPECTS_QUOTATION.'.number, '.TABLE_PROSPECTS_QUOTATION.'.access_level,'.TABLE_PROSPECTS_QUOTATION.'.status', " WHERE ".TABLE_PROSPECTS_QUOTATION.".id = '$id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != ProspectsQuotation::COMPLETED ) {
                    if ( $order['status'] == ProspectsQuotation::BLOCKED ) {
                        //if ( $order['access_level'] < $access_level ) {
                             /* check receipt created or not if receipt created then dont delete it 
                             ie quotation will be deleted only after deleting the receipts
                            */
                            $query = " UPDATE ". TABLE_PROSPECTS_QUOTATION
                                            ." SET status ='".ProspectsQuotation::DELETED."' WHERE id = '$id'";
							$db->query($query);
							
                            $messages->setOkMessage("The Quotation has been deleted.");
                        /* }
                        else {
                            $messages->setErrorMessage("Cannot Delete Quotation with the current Access Level.");
                        } */
                    }
                    else {
                        $messages->setErrorMessage("To Delete Quotation it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Quotation cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Quotation was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        /**
		 * Function to validate the input from the User while sending the invoice via email.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
	    function validateSendInvoice(&$data, $extra='') {
             foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if(!empty($data['to'])){
				if( !isValidEmail($data['to'])){
					$messages->setErrorMessage('To Email Address is not valid.');
				}
			}
			if(empty($data['to']) && !isset($data['mail_client']) && !isset($data['mail_to_su']) 
				&& !isset($data['mail_to_all_su'])){
				$messages->setErrorMessage('To should be provided OR Select Prospect.');
			}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$files = $_FILES;
            // Validate the Order.
            $list = NULL;
            if ( ProspectsQuotation::getList($db, $list, TABLE_PROSPECTS_QUOTATION.'.id', " WHERE ".TABLE_PROSPECTS_QUOTATION.".or_no = '". $data['or_no'] ."' AND ".TABLE_PROSPECTS_QUOTATION.".status !='".ProspectsQuotation::DELETED."'")>0 ) {
                $messages->setErrorMessage("The LeadOrder has been processed already.");
            }
            else {
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_PROSPECTS_ORDERS
                            ." WHERE number = '". $data['or_no'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage("The LeadOrder was not found in the database.");
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            if ( ($db->f('status') != ACTIVE) && ($db->f('status') != PROCESSED) && ($db->f('status') != COMPLETED) ) {
                                $messages->setErrorMessage("The LeadOrder is not yet approved.");
                            }
                            $data['client'] = $db->f('client');
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Proposal for this LeadOrder.");
                        }
                    }
                }
            }
			if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            /*
			if(empty($data['financialYr'])){
                 $messages->setErrorMessage("Select Financial Yr.");
            }
             if(!empty($data['financialYr']) && !empty($data['company_id']) ){
            
                $_ALL_POST['number'] =$data['number']= getOldCounterNumber($db,'QUOT',$data['company_id'],$data['financialYr']); 
                //$_ALL_POST['number'] =$data['number']= getCounterNumber($db,'ORD',$data['company_id']); 
                if(empty($data['number']) ){
                    $messages->setErrorMessage("Quotation Number was not generated.");
                }
            }else{
                $messages->setErrorMessage(" Financial Year and Company Name are not Found.");
            } */
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creator's status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }
			
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Prospect for whom the Quotation is being Created.");
            }
            else {
                $query = "SELECT user_id  as user_id, number, f_name, l_name, email,org,billing_name, status FROM ". TABLE_PROSPECTS." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Prospect was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Prospect is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
			if (  empty($data['quotation_subject']) ) {
                $messages->setErrorMessage("Please enter the Proposal Subject.");
            }
			if (  empty($data['attn_prospect']) ) {
                $messages->setErrorMessage("Select Attn.");
            }
            
            // Check the billing name. 
           if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
                $region         = new RegionProspects();
                $region->setAddressOf(TABLE_PROSPECTS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
            // Format the Date of Invoice.
            if ( !isset($data['do_i']) || empty($data['do_i']) ) {
                $messages->setErrorMessage("The Date of Proposal is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_i']) ) {
                $messages->setErrorMessage("Format of the Date of Proposal is invalid.");
            }
            else {
                $data['do_i'] = explode('/', $data['do_i']);
                //generate invoice number on the basis of invoice date 
                
                $data['do_i'] = mktime(0, 0, 0, $data['do_i'][1], $data['do_i'][0], $data['do_i'][2]);
            }
           
             // Format the Date of expiry from date, if provided.
            if ( !empty($data['do_fe']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_fe']) ) {
                    $messages->setErrorMessage("Format of the Service From Date is invalid.");
                }
                else {
                    $data['do_fe'] = explode('/', $data['do_fe']);
                    $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                }
                
            }else{
                $data['do_fe'] = '';
            }      
             
             // Format the Date of Invoice Expiry, if provided.
            if ( !empty($data['do_e']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_e']) ) {
                    $messages->setErrorMessage("Format of the Date Renewal is invalid.");
                }
                else {
                    $data['do_e'] = explode('/', $data['do_e']);
                    $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                }                
            }else{
                 $data['do_e'] = '';
        
            }
            // Format the Proposal Payment Due Date.			 
            if ( !isset($data['do_d']) || empty($data['do_d']) ) {
                $messages->setErrorMessage("The Due date for Payment of Proposal is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_d']) ) {
                $messages->setErrorMessage("Format of the Due date for Payment of Proposal is invalid.");
            }
            else {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }           
            
			/* 
			if ( !isset($data['other_details']) || empty($data['other_details']) ) {
                $messages->setErrorMessage("Please enter Payment, Schedule & other terms");
            } */
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Proposal Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
           
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Proposal Amount in INR is not valid.");
                }
            }
            
           /*  if ( !isset($data['amount_words']) || empty($data['amount_words']) ) { 
                $messages->setErrorMessage("The Amount in words is not specified."); 
            }else{  
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                } 
            } */
			if(empty($data['page1'])){
                 $messages->setErrorMessage("Create your Proposal in well formate.");
            }
           
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}
			//If USER Creating Proposal - PDF Then Below Block Should be validated
			if($data['btnCreatePdf']){
				$data['number']=$data['inv_counter'] ='';
				if(!empty($data['company_id']) && !empty($data['do_i'])){				
					$detailInvNo = getPsQuotationNumber($data['do_i'],$data['company_id']); 				
					if(!empty($detailInvNo)){
						$data['number'] = $detailInvNo['number'];
						$data['inv_counter'] = $detailInvNo['inv_counter'];
					}
				}
				if(empty($data['number']) ){
					$messages->setErrorMessage("An Proposal Number was not generated.");
				}
				if( empty($data['inv_counter']) ){
					$messages->setErrorMessage("An Proposal Counter was not generated.");
				} 
				// Check for the duplicate Proposal Number.
				$list = NULL;
				if ( ProspectsQuotation::getList($db, $list, TABLE_PROSPECTS_QUOTATION.'.id', " WHERE ".TABLE_PROSPECTS_QUOTATION.".number = '". $data['number'] ."'")>0 ) {
					$messages->setErrorMessage("An Proposal with the same Number already exists.<br/>Please try again.");
				}
            }
            
            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // Validate the Order.
            
            $list = NULL;
            if ( ProspectsQuotation::getList($db, $list, TABLE_PROSPECTS_QUOTATION.'.id,'.TABLE_PROSPECTS_QUOTATION.'.created_by', " WHERE ".TABLE_PROSPECTS_QUOTATION.".or_no = '". $data['or_no'] ."' AND 
			".TABLE_PROSPECTS_QUOTATION.".status !='".ProspectsQuotation::DELETED."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_PROSPECTS_ORDERS
                                ." WHERE number = '". $data['or_no'] ."' AND status !='".ProspectsOrder::DELETED."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage('The Order was not found in the database.');
                    }
                    else {
                        $db->next_record();
                        if($access_level > $db->f('access_level') ) {
                            /* As complete ma
                            if ( $db->f('status') != COMPLETED ) {
                                $messages->setErrorMessage('The Order has not been marked as completed.');
                            }*/
                            $data['client'] = $db->f('client');
                        }else{
                            $messages->setErrorMessage('You donot have the Right to update Proposal for this Order.');
                        }
                    }
                }
            }
            else {
                $messages->setErrorMessage('The Proposal for the Order has not been created yet.');
            }
            
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage('Creator details cannot be retrieved, Please try again.');
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage('The Creator was not found.');
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage('The Creators status is not Active.');
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }

            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage('Select the Prospect for whom the Proposal is being Created.');
            }
            else {
                $query = "SELECT user_id as user_id, number, f_name, l_name, email, billing_name,status FROM ". TABLE_PROSPECTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Prospect was not found.");
                } else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Prospect is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            if (  empty($data['quotation_subject']) ) {
                $messages->setErrorMessage("Please enter the Proposal Subject.");
            }
            // Check the billing name.
            if (  empty($data['attn_prospect']) ) {
                $messages->setErrorMessage("Select Attn.");
            } 
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
                $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Prospect's Profile");
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
                $region         = new RegionProspects();
                $region->setAddressOf(TABLE_PROSPECTS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
            // Format the Date of Proposal.
			 
            if ( !isset($data['do_i']) || empty($data['do_i']) ) {
                $messages->setErrorMessage("The Date of Proposal is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_i']) ) {
                $messages->setErrorMessage("Format of the Date of Proposal is invalid.");
            }
            else {
                $data['do_i'] = explode('/', $data['do_i']);
                $data['do_i'] = mktime(0, 0, 0, $data['do_i'][1], $data['do_i'][0], $data['do_i'][2]);
            } 
            
           
                                     
            // Format the Date of expiry from date, if provided.
            if ( !empty($data['do_fe']) ) {            
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_fe']) ){
                    $messages->setErrorMessage("Format of the Service From Date is invalid.");
                }else{
                    $data['do_fe'] = explode('/', $data['do_fe']);
                    $data['do_fe'] = mktime(0, 0, 0, $data['do_fe'][1], $data['do_fe'][0], $data['do_fe'][2]);
                }                
            }else{
                $data['do_fe'] = '';
            }
                    
             // Format the Date of Invoice Expiry, if provided.
            if ( !empty($data['do_e']) ) {
                if ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_e']) ) {
                    $messages->setErrorMessage("Format of the Date Renewal is invalid.");
                }else{
                    $data['do_e'] = explode('/', $data['do_e']);
                    $data['do_e'] = mktime(0, 0, 0, $data['do_e'][1], $data['do_e'][0], $data['do_e'][2]);
                } 
            }else{
                 $data['do_e'] = '';        
            }           
            // Format the Proposal Payment Due Date.
            if ( !isset($data['do_d']) || empty($data['do_d']) ) {
                $messages->setErrorMessage("The Due date for Payment of Proposal is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_d']) ) {
                $messages->setErrorMessage("Format of the Due date for Payment of Proposal is invalid.");
            }
            else {
                $data['do_d'] = explode('/', $data['do_d']);
                $data['do_d'] = mktime(0, 0, 0, $data['do_d'][1], $data['do_d'][0], $data['do_d'][2]);
            }
            
            // Validate Services.
            /*
            if ( !isset($data['service_id']) || empty($data['service_id']) ) {
                $messages->setErrorMessage('Please select atleast one service.');
            }
            else {
                foreach ( $data['service_id'] as $service ) {
                    $index = array();
                    if ( !arraySearchR($service, $lst_service) ) {
                        $messages->setErrorMessage('The Service with ID "'. $service .'" was not found.');
                    }
                }
            }*/
            /*
            $data['query_p'] = 'INSERT INTO '. TABLE_BILL_INV_P .' (inv_no, particulars, p_amount) VALUES ';
            $is_part = false;
            foreach ( $data['particulars'] as $key=>$particular ) {
                if ( !empty($data['particulars'][$key]) || !empty($data['p_amount'][$key]) ) {
                    if ( empty($data['particulars'][$key]) ) {
                        $messages->setErrorMessage('The Particular field at Position '. ($key+1) .' cannot be empty.');
                    }
                    if ( empty($data['p_amount'][$key]) ) {
                        $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' cannot be empty.');
                    }
                    else {
                        if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['p_amount'][$key]) ) {
                            $messages->setErrorMessage('The Amount of Particular '. ($key+1) .' is not valid.');
                        }
                        else {
                            $is_part = true;
                            $data['query_p'] .= "('". $data['number'] ."', '". $data['particulars'][$key] ."', '". $data['p_amount'][$key] ."'),";
                        }
                    }
                }
            }
            if ( !$is_part ) {
                $messages->setErrorMessage('The Particular for the Invoice cannot be empty.');
            }
            else {
                $data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
            }
            */
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Proposal Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Proposal Amount in INR is not valid.");
                }
            }
            /*
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                $messages->setErrorMessage("The Amount in words is not specified.");
            }else{                            
                if(!Validation::isAlphabets($data['amount_words'])){
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
            }*/
            
            
            /* 
			if ( !isset($data['access_level']) ){
                $messages->setErrorMessage("Select the Access Level.");
            }elseif (!(isPresentAccessLevel($db, $data['access_level']))){
                $messages->setErrorMessage("Selected Access Level not found.");
            } */
           
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                $messages->setErrorMessage("The Status is not valid.");
            }
            
			//If USER Creating Proposal - PDF Then Below Block Should be validated
			if($data['btnCreatePdf']){
				$data['number']=$data['inv_counter'] ='';
				if(!empty($data['company_id']) && !empty($data['do_i'])){				
					$detailInvNo = getPsQuotationNumber($data['do_i'],$data['company_id']); 				
					if(!empty($detailInvNo)){
						$data['number'] = $detailInvNo['number'];
						$data['inv_counter'] = $detailInvNo['inv_counter'];
					}
				}
				if(empty($data['number']) ){
					$messages->setErrorMessage("An Proposal Number was not generated.");
				}
				if( empty($data['inv_counter']) ){
					$messages->setErrorMessage("An Proposal Counter was not generated.");
				} 
				// Check for the duplicate Proposal Number.
				$list = NULL;
				if ( ProspectsQuotation::getList($db, $list, TABLE_PROSPECTS_QUOTATION.'.id', " WHERE ".TABLE_PROSPECTS_QUOTATION.".number = '". $data['number'] ."'")>0 ) {
					$messages->setErrorMessage("An Proposal with the same Number already exists.<br/>Please try again.");
				}
            }
            
            // Check for the duplicate Invoice Number.
            /*
            $list = NULL;
            if ( Invoice::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Invoice with the same Number already exists.");
            }
            $list = NULL;
            */
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for 
         * each Invoice.
         *
         */
        function setOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_PROSPECTS_ORDERS
					." SET status = '$status'"
					." WHERE ( id = '$id' OR number = '$id' )";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for
         * each Invoice so that the service provided is updated in the
         * Clients Account.
         *
         */
        function UpdateClientService(&$db, $client_id, $services) {
            $return     = false;
            $c_service  = 'NULL';
            
            $query = "SELECT service_id FROM ". TABLE_PROSPECTS." WHERE user_id = '". $client_id ."'";
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
                $c_service = processSQLData($db->f('service_id'));
                if ( empty($c_service) ) {
                    $c_service = array();
                }
                else {
                    $c_service = explode(',', $c_service);
                }
                
                $c_service = arrayCombine($c_service, $services);
                $c_service = implode(',', $c_service);
                $query = "UPDATE ". TABLE_PROSPECTS
                            ." SET service_id = '$c_service'"
                            ." WHERE user_id = '$client_id'";
                if ( $db->query($query) ) {
                    $return = true;
                }
            }
            return ($return);
        }
        
        /**
         * This function is used to create the File in PDF or HTML format for the Invoice.
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createFile($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("prospects-quotation-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_PST_QUOT_HTML, 0777);
                if ( is_writable(DIR_FS_PST_QUOT_HTML) ) {
                    $file_name = DIR_FS_PST_QUOT_HTML ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Proposal was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Proposal is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
                   /*
                   $pdf_content = Invoice::createPdfContent($data);
                   $invoiceArr['content'] = $pdf_content ;
                   $invoiceArr['header'] = '' ;
                   $invoiceArr['footer'] = '' ;
                   createInvoicePDF($data["number"],$invoiceArr);
                   $file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";
				   */
            }elseif($type == 'HTMLPRINT'){
                $s->assign('data', $data);
                $file_data = $s->fetch("prospects-quotation-tpl-print.html");
                
                // Create a new file and save to disk.
               /*  $old_umask = umask(0);
                @chmod(DIR_FS_QUOTATIONS_FILES_HTMLPRINT, 0777);
                if ( is_writable(DIR_FS_QUOTATIONS_HTMLPRINT_FILES) ) {
                    $file_name = DIR_FS_QUOTATIONS_HTMLPRINT_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The quotation was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }else {
                    $messages->setErrorMessage("The Directory to store Invoice is not writable.");
                }
                umask($old_umask); */
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
       
        
    function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
		$query = "SELECT DISTINCT ";
		$count = $total=0;

		if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
			$query .= implode(",", $required_fields);
		}
		elseif ( !empty($required_fields) ) {
			$query .= " ". $required_fields;
		}
		else {
			 $query .= " COUNT(*) as count";
			$count = 1;
		}
		
		$query .= " FROM ". TABLE_PROSPECTS_ORD_P;
		
		$query .= " ". $condition;

		if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
			$query .= " LIMIT ". $from .", ". $rpp;
		}
	
		if ( $db->query($query) ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$list[] = processSqlData($db->result());
					if($count==1){
						$total =$db->f("count") ;							
					}else{
						$total =$db->nf();
					}
				}
			}                
			return ( $total );
		}
		else {
			return false;
		}
    }
	//This is latest version used in pdf creation 2013-10-oct-11
	function createPdf3File($data) {  
		if (!empty($data['do_i'])){
			$temp=$temp1=$dtArr=null;
			$dd  =$mm=$yy='';
			$temp = explode(" ",$data['do_i']) ;
			$temp1=$temp[0];
			$dtArr =explode("-",$temp1) ;
			$dd =$dtArr[2];
			$mm =$dtArr[1];
			$yy=$dtArr[0];
			//$do_i = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
			$do_i = date("d M Y",$data['do_i']);

			/* $body.="<div class=\"row wpx253\">
				<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\"> Date</span></div>
				<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_i."</span></div>
			</div>"; */
		}
		/* $body.="<div class=\"row wpx253\">
					<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Proposal No.</span></div>
					<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['number']."</span></div>
				</div>"; */
		if (!empty($data['do_d'])){
			$do_d = date("d M Y",$data['do_d']);
			/* $body.="
			<div class=\"row wpx253\">
				<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Proposal Valid till</span></div>
				<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_d."</span></div>
			</div>"; */
		}

	
		$variables['images'] = DIR_WS_IMAGES_NC ;
		$css=  "<style type=\"text/css\" rel=\"stylesheet\">
		 body {margin-right:auto;
            margin-left:auto;
        }
        #content {
        margin:0;
        width: 100%;
        text-align:center;
        }
        #content-main {
        width: 731px;
        text-align:center;
        margin-left:auto;
        margin-right:auto;
        }
        .heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:18px;
            color:#000000;
            text-decoration:none;
            font-weight:bold;
        }
        .sub-heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#686868;
            line-height:15px;
            text-decoration:none;
            font-weight:regular;
        }
        .invoice-border{ 
             background:url(".$variables['images']."/invoice-repeater-new.jpg);
             background-repeat:repeat-y;
             width:253px;
         }	
        .invoice-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#333333;
            text-decoration:none;
            font-weight:normal;
        }
        
        .content-heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:10px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:bold;
        }
        .address-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:10px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
		.address-text-small{
			font-family:Arial, Verdana, 'San serif';
            font-size:9px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
		
		}
        .green-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#176617;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .white-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .design-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#797979;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .custom-content{
            font-family:arial,verdana,sans-serif;
            font-size:11px;
            color:#646464;
        }
        .content{
            font-family:arial,verdana,sans-serif;
            font-size:11px;
            color:#4f4f4f;
            lignt-height:10px;
        }
		.contentsub{
            font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
            lignt-height:10px;
			font-weight:bold;
        }
		.contenthead{
            font-family:arial,verdana,sans-serif;
            font-size:13px;
            color:#4f4f4f;
            lignt-height:11px;
        }
        .content-small{
            font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#4f4f4f;
        }
		.content-small-term{
            font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#726e6e;
        }
		.content-small-7{
        	font-family:arial,verdana,sans-serif;
            font-size:7px;
            color:#4f4f4f;
	    }
		.content-small-10{
        	font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
	    }
        .spacer{background:url(".$variables['images']."/spacer.gif)repeat;}
        .bg1{background-color:#898989;}
        .bg2{background-color:#CCCCCC;}
        .bg3{background-color:#f4f4f4;}
        .bg4{background-color:#ba1a1c;}
        .bg5{background-color:#956565;}
        .bg6{background-color:#BEBE74;}
        .bg7{background-color:#48b548;}
        .bg8{background-color:#000000;}
        .bg9{background-color:#ca2825;}
        .bdb1{border-bottom:1px solid #000000;}
        .b {font-weight:bold;}
        .ac {text-align:center;}
        .al {text-align:left;}
        .ar {text-align:right;}
        .vt {vertical-align:top;}
        .vm {vertical-align:middle;}
        .vb {vertical-align:bottom;}
        .fs08 {font-size:0.8em;}
        .fs11 {font-size:1.1em;}
        .fst11{font-size:1.1em; font-family:'Arial Narrow',Verdana,Arial,Sans-Serif;}
        .fs15 {font-size:1.5em;}
        .lh15 {line-height:1.5em;}
        .lh10 {line-height:1em;}    
        .lh1 {line-height:0.1em;}    
        .pl2{padding-left:2px;}
        .pl5{padding-left:5px;}
        .pl8{padding-left:8px;}                                    
        .pl10{padding-left:10px;}
        .pl11{padding-left:11px;}
        .pl15{padding-left:15px;}
        .pl20{padding-left:20px;}
        .pl30{padding-left:30px;}
        .pl60{padding-left:60px;}
        .pl42{padding-left:42px;}                                    
        .pr2{padding-right:2px;}
        .pr5{padding-right:5px;}
        .pr10{padding-right:10px;}                                    
		.pb1{padding-bottom:1px;}
        .pb2{padding-bottom:2px;}
        .pb3{padding-bottom:3px;}
        .pb4{padding-bottom:4px;}
        .pb5{padding-bottom:5px;}        
        .pb10{padding-bottom:10px;}
		.pb13{padding-bottom:13px;} 
        .pb20{padding-bottom:20px;}
        .pb25{padding-bottom:25px;}
        .pb30{padding-bottom:30px;}
        .pb40{padding-bottom:40px;}
        .pb50{padding-bottom:50px;}
        .pb100{padding-bottom:100px;} 
        .pt1{padding-top:1px;}        
        .pt2{padding-top:2px;}
        .pt3{padding-top:3px;}
        .pt4{padding-top:4px;}
        .pt5{padding-top:5px;}
        .pt6{padding-top:6px;}
        .pt8{padding-top:8px;}
        .pt10{padding-top:10px;}
        .pt15{padding-top:15px;}
        .pt20{padding-top:20px;}
        .pt25{padding-top:25px;}
        .pt30{padding-top:30px;}
        .pt32{padding-top:32px;}
        .pt45{padding-top:45px;}                                    
        .b1{border:collapse;border:1px solid #000000;}
        .wp100{width:100%;}
        .wp90{width:90%;}
        .wp70{width:70%;}
        .wp65{width:65%;}
        .wp60{width:60%;}
        .wp55{width:55%;}
        .wp50{width:50%;}
        .wp45{width:45%;}
        .wp40{width:40%;}
        .wp35{width:35%;}
        .wp30{width:30%;}
        .wp28{width:28%;}
        .wp25{width:25%;}
        .wp20{width:20%;}
        .wp15{width:15%;}
        .wp13{width:13%;}
        .wp12{width:12%;}
        .wp10{width:10%;}
        .wp9{width:9%;}
        .wp5{width:5%;}
        .wp6{width:6%;}
        .wp4{width:4%;}                                    
        .wpx7{width:7px;}
        .wpx9{width:9px;}
        .wpx10{width:10px;}
        .wpx20{width:20px;}
        .wpx712{width:712px;}
        .wpx729{width:729px;}
        .wpx731{width:731px;}
        .wpx330{width:330px;}
        .wpx337{width:337px;}
        .wpx348{width:348px;}
       
        .wpx711{width:711px;}
        .wpx707{width:707px;}
        .wpx9{width:9px;}
        .wpx713{width:713px;}
        .wpx347{width:347px;}
        .wpx253{width:253px;}
        .wpx335{width:335px;}                                    
        .hp30{
            height:27px;
            border-top:1px solid #dddddd;
         }
        .hp22{height:22px;}
        .hp20{height:20px;}
        .hp4{height:4px;}
        .hp9{height:9px;}
        .hp8{height:8px;}
        .hp17{height:19px;}
        .borlr{
            border-left:1px solid #dddddd;
            border-right:1px solid #dddddd;
        }                                    
        .borl{
            border-left:1px solid #dddddd;
        }                                    
        .borr{
            border-right:1px solid #dddddd;
        }
        .borb{
            border-bottom:1px solid #dddddd;
        }                                    
        div.row {
            clear:both; 
            float:left;
        }
        div.coloumn {
            float:left;
        }
        div.coloumn-right {
            float:right;
        }                                    
        .seccolor{background-color:#999999;}
        .bg2{background-color:#f4f4f4;}                                    
        div.clear{
            clear:both;
            background-color:#f4f4f4;
        }
        .header-text{font-size:11px; font-family:arial; font-weight:normal;}
		.top-border{
			border:#999999 solid 1px;
		}
		.contentheading{
			font-family: Arial, Verdana,'San serif';
			font-size:22px;		 
			color:#cf1f25;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 10px 0px 10px 0px;
		}
		.contentsmallheading{
			font-family: Arial, Verdana,'San serif';
			font-size:16px;
			color:#cf1f25;			 
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.contentsmallheadingtext{
			font-family: Arial, Verdana,'San serif';
			font-size:14px;
			color:#2c2c2c;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.content-service{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 9px 0px 9px 0px;
		}
		.content-font{
			font-family: Arial, Verdana,'San serif';
			font-size:12px;
			line-height:13px;
			color:#3c3c3c;
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
		}
		.whytop-border
		{
			border:#999999 solid 1px;
			text-align:left;
		}

		.whycontentheading
		{
			font-family: Arial, Verdana,'San serif';
			font-size:22px;
			color:#cf1f25;	
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 5px 0px 10px 0px;
		}

		.whycontentsmallheading
		{
			font-family: Arial, Verdana,'San serif';
			font-size:16px;
			color:#cf1f25;		 
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.whycontentsmallheadingtext
		{
			font-family: Arial, Verdana,'San serif';
			font-size:14px;
			color:#2c2c2c;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		
		.whycontent
		{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:normal;    
			text-decoration:none;
			text-align:left;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 15px 0px 15px 15px;
		}

		.whycontent-font{
			font-family: Arial, Verdana,'San serif';
			font-size:11px;
			color:#3c3c3c;
			font-weight:normal;    
			text-decoration:none;
			text-align:center; 
		}

		.whycontentbottom{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:normal;    
			text-decoration:none;
			text-align:left;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 9px 0px 9px 9px;
		}

		.whycontentbottomheading{
			font-family: Arial, Verdana,'San serif';
			font-size:15px;
			color:#cf1f25;
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 10px 0px 10px 0px;
		}	
.service-heading-text{font-family:Arial;font-size:20px;line-height:25px;color:#fff;font-weight:bold}
.service-sub-heading-text{font-family:Arial;font-size:14px;line-height:20px;color:#fff;font-weight:bold}
.service-content-heading{font-family:Arial;font-size:25px;line-height:30px;color:#C4001F;font-weight:bold}
.service-content-sub-heading{font-family:Arial;font-size:28px;line-height:35px;color:#C4001F;font-weight:bold}
.service-content-text{font-family:Arial;font-size:15px;line-height:20px;color:#505050;font-weight:regular} 
</style> ";  
$variables['images'] = DIR_WS_IMAGES_NC; 
	$header_content ="<page backtop=\"100px\" backbottom=\"20px\" backleft=\"0px\" backright=\"0px\" pagegroup=\"new\">
		<page_header>
			<table align=\"center\" valign=\"top\" style=\"width:650px;\">
			<tr>
				<td  style=\"padding-top:0px;width:650px;\" valign=\"top\">
				<table  align=\"center\" valign=\"top\" style=\"width:650px;\">
					<tr>
						<td class=\"row pt3\" style=\"width:650px;\">";
						if($data['dummy']==1){
						
							if($data['service_pdf']==1){
								$header_content.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" valign=\"top\" style=\"width:650px;\">
								<tr>
									<td class=\"address-text al bg9 pl30\" style=\"height:75px;width:620px;padding-top:0px;padding-bottom:0px;\" valign=\"middle\">
										<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
									</td>
									
								</tr>
								<tr>
									<td class=\"address-text al bg8 pl10\" style=\"width:640px;height:5px;\">
										<span class=\"white-text b\"></span> 
									</td> 
								</tr>
								</table>";
							}else{						
								$data['number']='PET-QUOT-000000000'; 
								$header_content.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" valign=\"top\" style=\"width:650px;\">
									<tr>
										<td class=\"address-text al bg9 pl30\" style=\"height:75px;width:240px;padding-top:0px;padding-bottom:0px;border:0px;\" valign=\"middle\">
											<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
										</td>
										<td style=\"width:100px;font-family:Arial;font-size:14px;color:#ffffff;border:0px;\" align=\"center\" valign=\"middle\" class=\"bg9\">Not<br>Approved <br/>Proposal</td>
										<td class=\"address-text ar bg9 pr10\" style=\"height:75px;width:260px;padding-bottom:0px;border:0px;\" valign=\"middle\">
											<table align=\"right\" valign=\"middle\" border=\"0px\">
												<tr>
													<td class=\"al white-text pr5 b\">Date</td>
													<td class=\"al white-text\">".$do_i."</td>
												</tr>
												<tr>
													<td class=\"al white-text pr5 b\">Proposal No.</td>
													<td class=\"al white-text\">".$data['number']."</td>
												</tr> 
												<tr>
													<td class=\"al white-text pr5 b\">Proposal Valid till</td>
													<td class=\"al white-text\">".$do_d."</td>
												</tr>
											</table>
										</td> 
									</tr>
									<tr>
										<td colspan=\"3\" class=\"address-text al bg8 pl10\" style=\"width:640px;height:5px;\">
											<span class=\"white-text b\"></span> 
										</td> 
									</tr>
								</table>";
							}
						}else{
		$header_content.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" valign=\"top\" style=\"width:650px;\">
								<tr>
									<td class=\"address-text al bg9 pl30\" style=\"height:75px;width:300px;padding-top:0px;padding-bottom:0px;\" valign=\"middle\">
										<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
									</td>
									<td class=\"address-text ar bg9 pr10\" style=\"height:75px;width:300px;padding-bottom:0px;\" valign=\"middle\">
										<table align=\"right\" valign=\"middle\" border=\"0px\">
											<tr>
												<td class=\"al white-text pr5 b\">Date</td>
												<td class=\"al white-text\">".$do_i."</td>
											</tr>
											<tr>
												<td class=\"al white-text pr5 b\">Proposal No.</td>
												<td class=\"al white-text\">".$data['number']."</td>
											</tr> 
											<tr>
												<td class=\"al white-text pr5 b\">Proposal Valid till</td>
												<td class=\"al white-text\">".$do_d."</td>
											</tr>
										</table>
									</td> 
								</tr>
								<tr>
									<td colspan=\"2\" class=\"address-text al bg8 pl10\" style=\"width:640px;height:5px;\">
										<span class=\"white-text b\"></span> 
									</td> 
								</tr>
							</table>";
						}
		$header_content.="</td>
					</tr> 
				</table>
				</td>
				</tr>
			</table>
		</page_header>
		<page_footer> 
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" style=\"width:650px;\" align=\"center\">
				<tr>
					<td class=\"row bg9\" valign=\"top\" style=\"width:650px;\">
					<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" valign=\"top\" style=\"width:650px;\">
						<tr>
							<td class=\"address-text al\" style=\"width:650px;height:1px;\">	
							</td> 
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class=\"row pt5 pb5 bg8\" valign=\"top\" style=\"width:650px;height:20px;\">
					<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" valign=\"top\" style=\"width:650px;\" align=\"center\">
					<tr>
						<td class=\"address-text al pl10\" style=\"width:580px;\"> 
							<div class=\"address-text-small\">Head office : Plot No. 17, MG street, &nbsp; Jhena &nbsp; 440025 &nbsp; Maharashtra India</div> <div class=\"address-text-small\" style=\"padding-top:3px;\"> 9823099996, 9823099998, 9823088885 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sales@SMEERPTech.in &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; www.SMEERPTech.in 
							</div> 
						</td>
						<td class=\"address-text\" align=\"right\" style=\"width:70px;\">
							 <span class=\"address-text-small ar\">page [[page_cu]] / [[page_nb]] </span> 
						</td>
					</tr>
					</table>
					</td>
				</tr>
			</table>
		</page_footer>
		<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" border=\"0px\" style=\"width:650px;\" valign=\"top\"  > 
			<tr>
				<td style=\"padding-top:30px;padding-bottom:20px;\">
					<table border=\"0px\" cellpadding=\"0px\" cellspacing=\"0px\" style=\"width:650px;\" valign=\"top\">
						<tr>
							<td style=\"padding-top:15px;padding-bottom:15px;padding-left:10px;background-color:#ca2825;width:395px;align:left;\">
								<table border=\"0px\" cellpadding=\"0px\" cellspacing=\"0px\" style=\"align:left;width:100%;\">
									<tr><td><span style=\"padding-bottom:6px;font-size:14px;color:#ffffff;\"><b>PROPOSAL FOR</b></span></td></tr>
									<tr><td><span style=\"font-size:15px;color:#ffffff;\"><b>".$data['billing_name']."</b></span></td></tr> 
									<tr>
										<td style=\"font-family:Arial;font-size:13px;line-height:13px;color:#ffffff;\" class=\"al pt6\">"  ;  
										if (!empty($data['b_addr_city'])){
											//nl2br($data['b_addr'])
											$header_content.="".$data['b_addr_city'];
										}
										if (!empty($data['b_addr_zip'])){
											$header_content.="&nbsp;  ". $data['b_addr_zip'];
										}
										if (!empty($data['b_addr_state'])){
											$header_content.="&nbsp;  ". $data['b_addr_state'];
										} 
					$header_content.="	</td>
									</tr>
									<tr><td style=\"padding-top:6px;\"><span style=\"font-size:13px;color:#ffffff;\">Att:&nbsp;".$data['attn_prospect_name']."</span></td></tr>
								</table>
							</td>
							<td style=\"width:5px;\">&nbsp;</td>
							<td style=\"padding-top:15px;padding-bottom:15px;padding-left:10px;background-color:#ca2825;width:230px;align:right;\">
								<table border=\"0px\" cellpadding=\"0px\" cellspacing=\"0px\" style=\"align:right;width:90%;\">
								<tr><td><span style=\"padding-left:0px;padding-bottom:6px;font-size:14px;color:#ffffff;\"><b>PROPOSAL FROM</b></span></td></tr>
								<tr><td style=\"padding-left:0px;\">
									<div class=\"row\" style=\"font-family:Arial;font-size:13px;line-height:14px;color:#ffffff;\">					 
										<b>".$data['mkex_fname']." ".$data['mkex_lname']."</b>
									</div> 
									<div class=\"row pl pt3\" style=\"font-family:Arial;font-size:11px;line-height:12px;color:#ffffff;\">".$data['mkex_designation']."</div>";
									if(!empty($data['mkex_mobile1'])){
										$header_content.="<div class=\"row pl pt3\" style=\"font-family:Arial;font-size:11px;line-height:12px;color:#ffffff;\">+91 - ".$data['mkex_mobile1']."</div>";
									}
									if(!empty($data['mkex_email'])){
										$header_content.="<div class=\"row pl pt3\" style=\"font-family:Arial;font-size:11px;line-height:12px;color:#ffffff;\">".$data['mkex_email']."</div>"; 
									} 
				$header_content.="</td></tr>
								</table> 
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class=\"invoice-text".$watermark."\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['quotation_intro1']."
				</td>
			</tr>
		</table> 
	</page>
	";
	 
		$content1='';
		if(!empty($data['quotation_intro2'])){
			$content1.= "<page pageset=\"old\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['quotation_intro2']."
				</td>
			</tr>
			</table> 
			</page>";
		} 
		if(!empty($data['quotation_intro3'])){
			$content1.= "<page pageset=\"old\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['quotation_intro3']."
				</td>
			</tr>
			</table> 
			</page>"; 
		} 
if($data['service_pdf']==0){		
		if(!empty($data['quotation_intro4']) ){
			$content1.= "<page pageset=\"old\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['quotation_intro4']."
				</td>
			</tr>
			</table> 
			</page>";
		} 
		//Code for Covering letter bof 
		 
		$content1.= "
		<page pageset=\"old\" >
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td valign=\"top\" style=\"padding-left:0px;padding-top:0px;\"> <img src=\"".$variables['images']."/prp.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
				</td>
			</tr>
			<tr>
				<td valign=\"top\" style=\"padding-left:0px;padding-top:45px;font-family:Arial;\">
					<span style=\"font-size:13px;color:#333333;\"><b>To,</b></span><br/><br/>";
					 
					$content1.= "<div style=\"font-family:Arial;font-size:14px;color:#333333;\"><b>".$data['billing_name']."</b></div>
					<div  style=\"font-family:Arial;font-size:12px;line-height:14px;color:#333333;\" class=\"al pt6\">".nl2br($data['b_addr'])."</div>      
					<div  style=\"font-family:Arial;font-size:12px;line-height:14px;color:#333333;\" class=\"al\">";
						if (!empty($data['b_addr_city'])){
							$content1.=$data['b_addr_city'];
						}
						if (!empty($data['b_addr_zip'])){
							$content1.="&nbsp;  ". $data['b_addr_zip'];
						}
						if (!empty($data['b_addr_state'])){
							$content1.="&nbsp;  ". $data['b_addr_state'];
						} 
					$content1.="
					</div>
					<div  style=\"font-family:Arial;font-size:12px;line-height:14px;color:#333333;\" class=\"al\">";
					if (!empty($data['b_addr_country'])){
							$content1.= $data['b_addr_country'];
						}
					$content1.="
					</div>";  
			$content1.="</td>
			</tr>
			<tr>
				<td valign=\"top\" style=\"padding-left:0px;padding-top:25px;font-family:Arial;font-size:12px;color:#333333;\">
				<span class=\"al b\">Att&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$data['attn_prospect_name']." </span>
				</td>
			</tr>
			<tr>
				<td valign=\"top\" style=\"padding-left:0px;padding-top:10px;font-family:Arial;font-size:12px;color:#333333;\">
					<b>Subject</b>: Proposal with reference to your requirements 
				</td>
			</tr>
			<tr>
				<td valign=\"top\" style=\"padding-left:0px;padding-top:20px;font-family:Arial;font-size:12px;color:#333333;\">
					<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"wp100\">
					<tr>
						<td>
							<div class=\"pl b\">Dear Sir/Madam, </div>
						</td>
					</tr>
					<tr>
						<td style=\"padding-left:0px;padding-top:20px;font-family:Arial;font-size:12px;color:#333333;\">
							With reference to the discussion regarding your requirements, herewith please find our proposal 
							for your consideration.  					
						
						</td>
					</tr>
					<tr>
						<td style=\"padding-left:0px;padding-top:10px;font-family:Arial;font-size:12px;color:#333333;\">
							Should you however have any further technical / commercial query, kindly feel free to contact. 
							It would be our pleasure <br/>to be of service to you. 							
						</td>
					</tr>
					<tr>
						<td style=\"padding-left:0px;padding-top:10px;font-family:Arial;font-size:12px;color:#333333;\"> 
							Finally, I must <b>THANK !</b> for the very kind courtesy extended to us to discuss on above mentioned subject.   
							
						</td>
					</tr>
					<tr>
						<td style=\"padding-left:0px;padding-top:10px;font-family:Arial;font-size:12px;color:#333333;\">
							<div class=\"row \">				 
							Thanking you in anticipation and looking forward to a healthy business association.
							</div>
							<div class=\"row pl pt10\">					 
							With very profound regards.
							</div>
							<div class=\"row pl pt10\">					 
							Yours Sincerely,
							</div>
							<div class=\"row b pl pt32\">					 
							".$data['mkex_fname']." ".$data['mkex_lname']."
							</div> 
							<div class=\"row b pl pt3\">".$data['mkex_designation']."</div>";
							if(!empty($data['mkex_mobile1'])){
								$content1.="<div class=\"row pl pt3\">+91 - ".$data['mkex_mobile1']."</div>";
							}
							if(!empty($data['mkex_email'])){
								$content1.="<div class=\"row pl pt3\">".$data['mkex_email']."</div>";
								//<div class=\"row invoice-text b pl\" style=\"height:180px\"></div>
							}
						$content1.=" 
						</td>
					</tr>	
					</table>
				</td>
			</tr>
		</table>
		</page>"; 
	     
		if(!empty($data['page1'])){
			$content1.= "<page pageset=\"old\" >
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['page1']."
				</td>
			</tr>
			</table> 
			</page>";
		}
		if(!empty($data['page2'])){
			$content1.= "<page pageset=\"old\" >
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['page2']."
				</td>
			</tr>
			</table> 
			</page>";
		}
		if(!empty($data['page3'])){
			$content1.= "<page pageset=\"old\" >
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['page3']."
				</td>
			</tr>
			</table> 
			</page>";
		}
		if(!empty($data['optional'] )){
			foreach($data['optional'] as $key11=>$val11){
				if(!empty($val11)){ 
					$content1.= "<page pageset=\"old\" >
					<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
					<tr>
						<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$val11."
						</td>
					</tr>
					</table> 
					</page>";
				}
			}
		}
		if(!empty($data['quotation_term1'])){
			$content1.= "<page pageset=\"old\" >
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['quotation_term1']."
				</td>
			</tr>
			</table> 
			</page>";
		}
		if(!empty($data['quotation_term2'])){
			$content1.= "<page pageset=\"old\" >
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['quotation_term2']."
				</td>
			</tr>
			</table> 
			</page>";
		}
		if(!empty($data['quotation_term3'])){
			$content1.= "<page pageset=\"old\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:600px;\" valign=\"top\"  >
			<tr>
				<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['quotation_term3']."
				</td>
			</tr>
			</table> 
			</page>";
		} 
	
	$data['company_quot_prefix']='SMEERP';
	//Terms and conditions Bof
	$content1.= "
	<page pageset=\"old\" > 
		<table cellspacing=\"0\" cellpadding=\"0px\" border=\"0\" align=\"center\" style=\"width:650px;\">
			<tr>
				<td class=\"al pt3\" valign=\"top\" style=\"width:650px;\" >
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:0px;width:650px;\">
					<tr>
						<td style=\"width:330px;\" valign=\"top\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" style=\"padding-left:0px;padding-right:0px;width:330px;\">
							<tr>
								<td class=\"content-small-term pl\"  style=\"width:330px;\"> 
									PART B - Terms and Conditions:: By making payment against this invoice/proposal, it is agreed between SMEERP ('SELLER') and the CUSTOMER or CLIENT ('BUYER') as follows -  
							   </td>
							</tr> 	
							<tr>
								<td class=\"content-small b pl pt2\" style=\"width:330px;\">A. Definitions</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \" style=\"text-align:justify;padding-left:0px;padding-right:0px;width:330px;\">
								<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\"  style=\"width:330px;\">
									<tr>
										<td style=\"width:330px;text-align:justify;\">'Extra Efforts' or 'Additional Services' means any efforts or services which are not mentioned in the proposal.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Charges' means all fees, charges/tariffs, costs and rates chargeable by SELLER from time to time for providing the BUYER with the Service, for Software Products, for Goods and Additional Services and other Government levies.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Customer or Client or Buyer' shall mean any person, partnership firm or private limited company or limited company or such other organisation authorized by SELLER to use the services or to whom SELLER sell goods or services.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Government' shall mean the Government of India and/or the State Government of Maharashtra or such other local authority, as the case may be.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Services' shall mean all/any services made available by SELLER directly or via its associates or channel partners.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Software Products' are like myCraft<sup>TM</sup>, myGallery, Nerve Center<sup>&reg;</sup> etc. and are products/solutions developed by Aryan Salve and license to use for which are sold by SELLER directly or via its associates or channel partners.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'SaaS' is Software-As-A-Service provided to BUYER (if paid) / USER (if free) by SELLER to use Online (internet).
										</td>
									</tr>									
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Goods' shall mean all the Goods/Material/Equipment sold by SELLER directly or via its associates or channel partners.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'MySMEERP' is the Online Customer Support Portal for support, billing, communication etc.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Renewal/Expiry Date' means renewal/expiry date of Service / SaaS / Software Products / Goods as mentioned in Invoice.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;text-align:justify;\">'Support Email' means Support@SMEERPTech.in
										</td>
									</tr>												
								 </table>												
								</td>
							</tr>	
							<tr>
								<td class=\"content-small b pl pt2\"  style=\"width:330px;\"> B. Provision of Services or Goods</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term\"  style=\"width:330px;text-align:justify;\">
									SELLER agrees to provide the Services or Goods to the BUYER subject to the terms and conditions of this agreement. 
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pl pt2 \"  style=\"width:330px;\"> C. Obligations of SELLER</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term\"  style=\"width:330px;\">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. SELLER shall use reasonable efforts to make the Services / SaaS available to the BUYER at all times.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. The availability and quality of the Services / SaaS / Software Products / Goods may be affected by the factors outside SELLER control such as physical obstructions, geographic, weather conditions and other causes of interference or faults.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. Services / SaaS / Software Products may be suspended in whole or in part at any time, without notice, if Network fails or requires maintenance. SELLER will make all reasonable efforts to minimise the frequency and duration of such events. The BUYER will remain liable for all charges during the period of suspension unless SELLER in its discretion decides otherwise.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. SELLER has the sole right and discretion to vary or increase the Charges at any time on reasonable notice to the BUYER.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. SELLER reserves the right to apply a monthly/quarterly/half-yearly/yearly financial limit and such other conditions for Charges incurred by the BUYER and to demand interim advance payment, suspend or disconnect access to the Services / SaaS / Software Products if such limits are exceeded.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">6. SELLER has the right to check the credentials of the BUYER including the Customer's financial standing and to use the services of any person or agency for such purpose.</td>
									</tr>
									</table>									 
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pl pt2\"  style=\"width:330px;\"> D. Obligations of the BUYER</td>
							</tr> 	
							<tr>
								<td class=\"pt2 content-small-term \"  style=\"width:330px;\">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\"  style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">The BUYER hereby expressly agrees:
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">1.	The Technology and Human Beings neither are nor can ever be 100% perfect and therefore are vulnerable to faults / failure / delay / mistakes leading to disruption or delay or downtime of services. Therefore under all/any circumstances, BUYER can not hold SELLER responsible for any loss faced by BUYER due to such faults / failure / delay / mistakes.
										</td>
									</tr>									
									<tr>
										<td  style=\"width:330px;\">2. To make payments for the Services / SaaS / Software Products / Goods on the following basis
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(a) payment will be due when SELLER raises the Proforma-Invoice / Invoice / Bills / Billing Statement on the BUYER.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(b) payment will be made on or before the due date mentioned in the Proforma-Invoice / Invoice / Bills / Billing Statement or as per schedule mentioned in the Proposal, failing which SELLER shall be entitled to charge interest @18% per annum or INR 500 whichever is higher and/or late fees on all outstanding Charges from due date till date of payment &amp; shall be entitled to discontinue Services / SaaS / delivery of Goods / Software Products, without notice, in its sole discretion. Cheque return will attract a penalty of Rs. 500 separately.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(c) in case of delinquent payment or non-payment, under all/any circumstances the Domain-Name ownership can not be claimed by BUYER nor these can be transferred by BUYER to any other service provider. 
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(d) the BUYER will pay all the costs of collection and legal expenses, with interest should it become necessary to refer the matter to a collection agency or to legal recourse to enforce payment.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(e) SELLER shall be entitled to apply payments / deposits / advances made by the BUYER towards any Charges outstanding including for any other SELLER Services / SaaS / Software Products / Goods availed by the BUYER.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(f) payments will be made in Cash, Credit card or A/c Payee Cheque or Demand Draft or Internet Banking or any other instrument drawn on any bank in India and payable at Jhena 440025 (Maharashtra).
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(g) the BUYER shall be liable for all Charges for the Services / SaaS / Software Products / Goods provided to the BUYER whether or not the Services / SaaS / Software Products / Goods have been used by the BUYER.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(h) in the event of any dispute regarding the Charges, the BUYER agrees to pay SELLER Charges billed pending resolution of such dispute.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(i) the BUYER shall be liable to pay for the Services / SaaS / Software Products / Goods provided even if BUYER does not receive the Proforma-Invoice / Invoice / Bills / Billing statement. It will be the BUYER's responsibility to make enquiries in case of non-receipt of Proforma-Invoice / Invoice / Bills / Billing statement.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">(j) Charges payable by the BUYER are exclusive of taxes, duties or levies payable, unless expressly stated to the contrary in the Proposal / Proforma-Invoice / Invoice / Billing Statement / Bills.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">(k) any advance payment / security deposit paid by the BUYER shall be adjusted against any dues payable by the BUYER to SELLER and balance if any will be refunded by SELLER within 60 days from the deactivation of the Services.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">3. To make 100% advance payment for Charges including service charges if Proforma-Invoice / Invoice  is raised to BUYER by SELLER or if the Services / SaaS /Software Products / Goods are provided to BUYER.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">4. To not use or cause or allow others / anybody to use the Services / SaaS / Software Products / Goods for any improper, immoral or unlawful or illegal purpose including in any manner (for e.g. Spamming, Bulk Emailing, Transmission of infected content, Porn, Selling illegal products etc.).
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">5. To comply with instructions issued by Government or SELLER, concerning BUYER's access to and use of Services / SaaS / Software Products / Goods.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">6. To furnish correct and complete information and documents as required by SELLER from time to time. The Services / SaaS / Software Products / Goods agreed to be provided by SELLER shall always be subject to verification of the BUYER's credentials and documents and if at any time, any information and/or documents furnished by the BUYER is/are found incorrect or incomplete or suspicious, SELLER shall be entitled to suspend/terminate the Service forthwith without any further notice.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">7. That SELLER may suspend the Service / SaaS / Software Products or delivery of Goods in whole or in part at any time without prior notice and without assigning any reason thereto. SELLER reserves the right to Charge for re-activation or re-delivery.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">8. BUYER is liable (a) for the Charges during the period of suspension and thereafter (b) to pay the overages as applicable and (c) Charges towards Extra Efforts
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">9. To comply with the applicable laws, rules and regulation regarding the use of the Services / Saas / Software Products / Goods and procurement of the Equipment including but not limited to relevant tax laws and import control regulations.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">10. To pay to SELLER such amount as SELLER may require as security for the due performance of the BUYER's obligations under these terms and conditions. SELLER may set off these amounts against any cost, damages or expenses which SELLER may suffer or incur as a result of the BUYER's failure to perform any of these obligations. Security deposit amount shall not carry any interest.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">11. To inform SELLER, in writing, of any changes in billing name or billing address or email id or cellular number. Any Written Communication, Proforma-Invoice, Invoice , Billing statement or Notice from SELLER to the BUYER will be deemed as served within 48 hours of posting by ordinary mail or email.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">12. To notify SELLER immediately in case of any complaints with regards to the Services / SaaS / Software Products / Goods via email to Support Email. 
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">13. Not to assign any right or interest under this agreement without SELLER prior written consent.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">14. BUYER is not entitled to assign/transfer/resell/lease/rent or create any charge/lien on the Service / SaaS of any nature whatsoever or Software Product owned by SELLER, without prior permission of SELLER. Any transfer affected in contravention of the express terms contained herein, shall not absolve the BUYER of his/her primary duty towards SELLER for usages charges levied against the BUYER. 
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">15. To timely provide SELLER with the certificates etc. towards any deductions as per the law. 
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">16. Security and Backup of the data to which BUYER is provided access with is the liability of BUYER.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">17. BUYER must verify the credentials of the visiting SELLER personnel.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">18. Neither SELLER personnel will install any illegal/pirated software/text/images for BUYER nor any Illegal/pirated software/text/images are covered under maintenance service support.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">19. BUYER must provide (a) Work/Purchase-order referring to proposal having mention scope of work and payment schedule (b) Duly signed and stamped copy of proposal (c) Duly filled, signed and stamped Customer (BUYER) Agreement form. 
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">20. BUYER must designate only one of his/her (BUYER's) personnel to coordinate and to approve the work/delivery. BUYER is not allowed to change designated personnel till completion of work. In case of factors outside control, change of personnel is allowed if agreeable to both the parties. BUYER will not hold SELLER responsible for any failure/fault/delay/loss arising due to such change of personnel.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">21. Access to Service / SaaS / Software Products is restricted on Service expiry date. Domain / Web hosting / Email etc accounts or any Service / SaaS / Software Products with data/email etc are deleted after seven days of Renewal/Expiry Date if not renewed before. Late renewal during these seven days attracts late payment Charges too.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">22. To be bound at all times by any modifications and or variations made to these terms and conditions. 
										</td>
									</tr>								
									<tr>
										<td style=\"width:330px;padding-left:0px;\">23. SELLER will remind BUYER about the renewal dates via SMS/Email automated notifications. BUYER is responsible to remember renewal dates and make renewal payments before 60 days of Renewal/Expiry Date.  
										</td>
									</tr>
									</table>
								</td>
							</tr>
							</table>
						</td>
						<td style=\"width:330px;\" valign=\"top\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:0px;width:330px;\">
							<tr>
								<td class=\"content-small-term\"  style=\"width:330px;\">
								<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:0px;width:330px;\">  
									<tr>
										<td style=\"width:330px;\">24. BUYER is liable to pay any statutory duties or levies or taxes as applicable by the law. 
										</td>
									</tr> 
									<tr>
										<td style=\"width:330px;\">25. BUYER must provide information/references for his/her (BUYER's) choice for design, images in JPEG format, content in editable MS Word format and Logo in editable CDR or PSD format via email and before commencement of work. Our designers will use all the images provided as-is. Besides basic cropping, SELLER is unable to perform advanced manipulation. Logo design, Content creation or Content writing or copy-writing or typing or proof-reading or spell-check or grammar-check or extra design samples is separate job work needing Extra Efforts and is separately chargeable. Static page means an A4 size page containing text in Arial font size 12.
										</td>
									</tr>								
									<tr>
										<td  style=\"width:330px;\">26. SELLER will not be responsible for any delay/loss/fault/failure caused due to delay in providing the content to SELLER by BUYER or BUYER's Personnel or BUYER'S Associates or non-availability of any of BUYER's official or any other delay on BUYER's part. BUYER will not hold back any of SELLER payment for any delays/fault/failure due to any reason on part of BUYER. Content/Work received after the completion of work OR in excess as per proposal will be termed as extra and separately chargeable.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">27. Transfer of domain name from BUYER's existing service provider to SELLER is the sole responsibility of BUYER.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">28. BUYER must use MySMEERP or email to Support Email for service and support.</td>
									</tr>
								</table>									 
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2\"  style=\"width:330px;\"> E. SaaS / Software Product License to use:</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term\"  style=\"width:330px;\">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. If billed for, BUYER gets the single domain name license to use for the SELLER Software Products / SaaS. SELLER Software Products / SaaS are provided on single domain name license to use basis only on SELLER web servers without ftp & hosting control panel access. The license to use of Software Products / SaaS is non-transferable to any other Domain name or other person/party and is subject to yearly renewal against annual maintenance support charges.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. Under all/any circumstances, programming code of Software Products / SaaS is the sole property of Aryan Salve 9823078899 who has appointed SMEERP as authorized reseller and under no circumstances BUYER can claim any rights towards the ownership of Software Product or Programming code or SaaS. License of the Software product / SaaS is only for using it i.e. License to use.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. Credits to Service / Software Product / SaaS and/or SELLER are compulsorily mentioned on the BUYER's website. Credits to SELLER is compulsorily mentioned on the designed and/or printed material of the Customer.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. Any customization done in the Software Product / SaaS doesn't make the BUYER the owner of Software product / SaaS. Under all/any circumstances the Software Product / SaaS remains the property of Aryan Salve 9823078899 (who appointed SMEERP as authorized reseller) with full rights over entire Software product / SaaS and customizations done. 
										</td>
									</tr>
									</table>								 
								</td>
							</tr> 
							<tr>
								<td class=\"content-small b pt2\" style=\"width:330px;\"> F. Validity</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. Both parties (BUYER and SELLER) agree that, this agreement has been duly authorised and executed and is valid and binding and is enforceable in law in accordance with its terms.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. The validity construction and performance of this agreement shall be governed by and interpreted in accordance with the laws of the Republic of India.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. The courts in Jhena shall have the jurisdiction.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. Should any provision of this agreement be or become ineffective or be held to be invalid, this shall not affect the validity of the remaining provisions. Any invalid provision in this agreement shall be replaced, interpreted or supplemented as the case may be in such a manner that the intended economic purpose of the agreement will be achieved.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. This agreement is complete and exclusive statement of agreement between parties and it supersedes all understanding or prior agreement, whether oral or written and all representations or other communications between parties.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">6. These terms & conditions are subject to applicable Indian laws, rules and regulation framed thereunder and any statutory modifications or re-enactment for time being in force and any other Government regulations issued from time to time. </td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">7. Prices/Availability/Specifications are subject to change without prior notice. </td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2 pr5\" style=\"width:330px;\"> G. Support, Goods Return, Warranties, Disclaimer of warranties</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. SELLER sends BUYER the information related to terms / conditions / services / offers / warnings / support / advisories etc. via email / sms / telephone / letter. It is BUYER's responsibility to revert or act or do the needful accordingly.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. Annual Maintenance Support Charge for renewal of License to use of Service / Software Product / SaaS is compulsory as specified in the proposal and includes services (a) Maintaining Service / Software Product / SaaS in running / working condition (b) FREE feasible patches and updates (c) BUYER support via email, telephone and MySMEERP</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. Goods once sold will not be taken back or exchanged and payment will not be refunded under any circumstances. Our risk & responsibility ceases on delivery of the goods. After sales support services is valid only if applicable to purchased item and explicitly mentioned in the Proforma-Invoice / Invoice.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. SELLER makes no representation or warranty other than those set forth in this agreement. SELLER expressly disclaims all other warranties express or implied, including, but not limited to any implied warranty or merchantability or fitness for a particular purpose.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. Any Services / SaaS / Software Products / Goods or Equipment re-sold (where SELLER is a Reseller of third-party Services / Softwares / SaaS / Goods) by SELLER will carry the warranty, if any, of the manufacturer. Service to such Service / Software Products / SaaS / Goods or Equipment, if not explicitly mentioned in Proforma-Invoice / Invoice, if any, will be provided by authorized service centre of the manufacturer. SELLER is not liable towards service support or replacement under warranty, if any, of any Service / Software Products / SaaS / Goods or Equipment manufactured by third party and that SELLER is just reselling.</td>
									</tr> 
									</table>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2 pr5\" style=\"width:330px;\"> H. Disclaimer of liability</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. SELLER shall not be liable to the BUYER for any loss or damage whatsoever or howsoever caused arising directly or indirectly in connection with this agreement, the Services or Software Products or SaaS or Equipment or Goods, their use, application or otherwise except to the extent to which it is unlawful to exclude such liability.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. Malfunction/Limitation in working of any of SELLER Software Product / Service / SaaS / Goods due to any change in third party Software/Hardware on which SELLER Software Product / Saas / Service / Goods are dependent for functioning is not covered under maintenance support and SELLER cannot be held responsible for it. Though SELLER shall use reasonable efforts for resolution of issue if feasible. However BUYER will be liable towards Charges for these Extra Efforts.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. Under any circumstances, SELLER will not be responsible for any kind of loss / damage of data / equipment etc. while / due to working SELLER service personnel at BUYER's site. All the maintenance and service related work executed is at the risk of BUYER. BUYER has rights to ask SELLER service personnel to work under BUYER's or BUYER's authorized personnel's vigilance, seek information and take decision regarding maintenance / service work.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. Notwithstanding the generality of (a) above, SELLER expressly excludes liability for consequential loss, damage or for loss of profit, business revenue, goodwill or anticipated savings.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. SELLER at its discretion, may send to the BUYER various like information or notices or reminders etc on BUYER's / BUYER's Personnel cellular number through SMS or on BUYER's / BUYER's Personnel email id through Email or on BUYER's billing address through post/mail or otherwise. In case the BUYER does not wish to receive such information then BUYER may notify SELLER via email at Support Email or via Signed Letter at SELLER office address.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">6. In the event that any exclusion contained in this agreement shall be held to be invalid for any reason, and SELLER becomes liable for the loss or damage or failure or fault that it may otherwise not have been liable, such liability shall be limited to the cost of the Services / SaaS / Software Product / Goods actually paid for by the BUYER to SELLER during the relevant period.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">7. BUYER agrees to indemnify and keep SELLER harmless and defend SELLER at its (BUYER's) own expense from and against all claims arising as a result of breach of this agreement and from all taxes, duties or levies.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">8. SELLER advises BUYER to make cash payments only after verifying the credentials (ID card etc.) of the visiting personnel of SELLER. SELLER encourages cheque, demand draft or internet banking transfer to avoid any untoward incident. SELLER will not be liable for any such loss incurring due to handing over of cash payment to unauthorized personnel.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">9. BUYER agrees that any request/communication received from BUYER's and BUYER's authorized personnel email-ids / cellular mobile numbers on SELLER Support Email via email or SMS via Cellular mobile numbers shall be deemed to be valid request/communication from the BUYER. Nothing herein shall apply with respect to the notice to be given by the BUYER or any other provisions of this agreement.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">10. BUYER must regularly update and verify credentials of BUYER's authorized personnel registered and authorized for with MySMEERP and in case of any issue must inform SELLER immediately and confirm the same in writing to avoid loss/damage. Liability of any loss due to misuse/mistake/mishandling/loss of confidential information by/due to BUYER or BUYER's authorized personnel remains with BUYER and SELLER will not be held responsible under any circumstances.</td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2 pr5\" style=\"width:330px;\"> I. Termination</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. Either party shall have the right to terminate the agreement by giving 60 days prior notice in writing.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. Notwithstanding anything contained herein, SELLER shall be entitled to terminate this agreement and the Services / Saas / Software Product License if (a) the Government either suspends or terminates the License or Permission or the Services temporarily or otherwise (b) at any time the BUYER fails to satisfy the requisite credit checks or provides fraudulent information to SELLER. (c) the BUYER fails to pay its subscription or the Charges due (d) the BUYER is in breach of any other terms of the agreement and the BUYER does not remedy the breach within seven (7) days of the day of receipt of a written / email notice from SELLER specifying the breach.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. The agreement may also be terminated at the option of either party, on the happening of the following events (a) if either party is declared insolvent, bankrupt or is liquidated or dissolved (b)	if a trustee or receiver is appointed to take over the assets of either party (c) if the Government requires any of this agreement to be revised in such a way as to cause significant adverse consequences to either party</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. Termination of this agreement under the preceding provisions shall be without prejudice to and in addition to any right or remedy available to the terminating party under any applicable law or statute.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. In the event of termination of the agreement for any reason whatsoever, SELLER shall be entitled to recover all outstanding Charges and dues from the BUYER.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">6. If the agreement is terminated for reasons of fraudulent information provided or misuse or unlawful use by the BUYER, the security deposit shall be forfeited.</td>
									</tr>  
									</table>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2 pr5\" style=\"width:330px;\"> J. Miscellaneous</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. All notices required to be given to SELLER pursuant to this agreement shall be in writing and shall be directed by registered post to the Registered office at SMEERP, Plot No. 17, MG street, Jhena 440025 Maharashtra India, www.SMEERPTech.in Support@SMEERPTech.in</td>
									</tr>
									</table>
								</td>
							</tr>
							</table>
						</td>
					</tr>			
					</table>
				</td>
			</tr>
		</table> 
	</page>";
	//Terms and conditions Eof
}		
		
		$invoiceArr['content'] = ob_get_clean();
		$content = $css.$header_content.$content1 ;
		 $invoiceArr['content']=$content;
		$invoiceArr['header']='';
		$invoiceArr['footer']='';
	   
		createQuotationPDF3($data["filename"],$invoiceArr);
	}


function createPdfFileTest($data) {        
$variables['images'] = DIR_WS_IMAGES_NC ;
$css=  "<style type=\"text/css\" rel=\"stylesheet\">
    body{
		margin-right:auto;
        margin-left:auto;
       }
        #content {
			margin:0;
			width: 100%;
			text-align:center;
        }
        #content-main {
        width: 731px;
        text-align:center;
        margin-left:auto;
        margin-right:auto;
        }
        .heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:18px;
            color:#000000;
            text-decoration:none;
            font-weight:bold;
        }
        .sub-heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#686868;
            line-height:15px;
            text-decoration:none;
            font-weight:regular;
        }
        .invoice-border{ 
             background:url(".$variables['images']."/invoice-repeater-new.jpg);
             background-repeat:repeat-y;
             width:253px;
         }	
        .invoice-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#333333;
            text-decoration:none;
            font-weight:normal;
        }
        
        .content-heading{
            font-family:Arial, Verdana, 'San serif';
            font-size:10px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:bold;
        }
        .address-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:10px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
		.address-text-small{
			font-family:Arial, Verdana, 'San serif';
            font-size:9px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
		
		}
        .green-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#176617;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .white-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#FFFFFF;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .design-text{
            font-family:Arial, Verdana, 'San serif';
            font-size:11px;
            color:#797979;
            text-decoration:none;
            font-weight:regular;
            lignt-height:13px;
        }
        .custom-content{
            font-family:arial,verdana,sans-serif;
            font-size:11px;
            color:#646464;
        }
        .content{
            font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
            lignt-height:10px;
        }
		.contentsub{
            font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
            lignt-height:10px;
			font-weight:bold;
        }
		.contenthead{
            font-family:arial,verdana,sans-serif;
            font-size:13px;
            color:#4f4f4f;
            lignt-height:11px;
        }
        .content-small{
            font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#4f4f4f;
        }
		.content-small-term{
            font-family:arial,verdana,sans-serif;
            font-size:6px;
            color:#726e6e;
        }
		.content-small-7{
        	font-family:arial,verdana,sans-serif;
            font-size:7px;
            color:#4f4f4f;
	    }
		.content-small-10{
        	font-family:arial,verdana,sans-serif;
            font-size:10px;
            color:#4f4f4f;
	    }
        .spacer{background:url(".$variables['images']."/spacer.gif)repeat;}
        .bg1{background-color:#898989;}
        .bg2{background-color:#CCCCCC;}
        .bg3{background-color:#f4f4f4;}
        .bg4{background-color:#ba1a1c;}
        .bg5{background-color:#956565;}
        .bg6{background-color:#BEBE74;}
        .bg7{background-color:#48b548;}
        .bg8{background-color:#000000;}
        .bg9{background-color:#ca2825;}
        .bdb1{border-bottom:1px solid #000000;}
        .b {font-weight:bold;}
        .ac {text-align:center;}
        .al {text-align:left;}
        .ar {text-align:right;}
        .vt {vertical-align:top;}
        .vm {vertical-align:middle;}
        .vb {vertical-align:bottom;}
        .fs08 {font-size:0.8em;}
        .fs11 {font-size:1.1em;}
        .fst11{font-size:1.1em; font-family:'Arial Narrow',Verdana,Arial,Sans-Serif;}
        .fs15 {font-size:1.5em;}
        .lh15 {line-height:1.5em;}
        .lh10 {line-height:1em;}    
        .lh1 {line-height:0.1em;}    
        .pl2{padding-left:2px;}
        .pl5{padding-left:5px;}
        .pl8{padding-left:8px;}                                    
        .pl10{padding-left:10px;}
        .pl11{padding-left:11px;}
        .pl15{padding-left:15px;}
        .pl20{padding-left:20px;}
        .pl30{padding-left:30px;}
        .pl60{padding-left:60px;}
        .pl42{padding-left:42px;}                                    
        .pr2{padding-right:2px;}
        .pr5{padding-right:5px;}
        .pr10{padding-right:10px;}                                    
		.pb1{padding-bottom:1px;}
        .pb2{padding-bottom:2px;}
        .pb3{padding-bottom:3px;}
        .pb4{padding-bottom:4px;}
        .pb5{padding-bottom:5px;}        
        .pb10{padding-bottom:10px;}
		.pb13{padding-bottom:13px;} 
        .pb20{padding-bottom:20px;}
        .pb25{padding-bottom:25px;}
        .pb30{padding-bottom:30px;}
        .pb40{padding-bottom:40px;}
        .pb50{padding-bottom:50px;}
        .pb100{padding-bottom:100px;}
        
        .pt1{padding-top:1px;}        
        .pt2{padding-top:2px;}
        .pt3{padding-top:3px;}
        .pt4{padding-top:4px;}
        .pt5{padding-top:5px;}
        .pt6{padding-top:6px;}
        .pt8{padding-top:8px;}
        .pt10{padding-top:10px;}
        .pt15{padding-top:15px;}
        .pt20{padding-top:20px;}
        .pt25{padding-top:25px;}
        .pt30{padding-top:30px;}
        .pt32{padding-top:32px;}
        .pt45{padding-top:45px;}                                    
        .b1{border:collapse;border:1px solid #000000;}
        .wp100{width:100%;}
        .wp90{width:90%;}
        .wp70{width:70%;}
        .wp65{width:65%;}
        .wp60{width:60%;}
        .wp55{width:55%;}
        .wp50{width:50%;}
        .wp45{width:45%;}
        .wp40{width:40%;}
        .wp35{width:35%;}
        .wp30{width:30%;}
        .wp28{width:28%;}
        .wp25{width:25%;}
        .wp20{width:20%;}
        .wp15{width:15%;}
        .wp13{width:13%;}
        .wp12{width:12%;}
        .wp10{width:10%;}
        .wp9{width:9%;}
        .wp5{width:5%;}
        .wp6{width:6%;}
        .wp4{width:4%;}                                    
        .wpx7{width:7px;}
        .wpx9{width:9px;}
        .wpx10{width:10px;}
        .wpx20{width:20px;}
        .wpx712{width:712px;}
        .wpx729{width:729px;}
        .wpx731{width:731px;}
        .wpx330{width:330px;}
        .wpx337{width:337px;}
        .wpx348{width:348px;}
       
        .wpx711{width:711px;}
        .wpx707{width:707px;}
        .wpx9{width:9px;}
        .wpx713{width:713px;}
        .wpx347{width:347px;}
        .wpx253{width:253px;}
        .wpx335{width:335px;}                                    
        .hp30{
            height:27px;
            border-top:1px solid #dddddd;
        }
        .hp22{height:22px;}
        .hp20{height:20px;}
        .hp4{height:4px;}
        .hp9{height:9px;}
        .hp8{height:8px;}
        .hp17{height:19px;}
        .borlr{
            border-left:1px solid #dddddd;
            border-right:1px solid #dddddd;
        }                                    
        .borl{
            border-left:1px solid #dddddd;
        }                                    
        .borr{
            border-right:1px solid #dddddd;
        }
        .borb{
            border-bottom:1px solid #dddddd;
        }                                    
        div.row {
            clear:both; 
            float:left;
        }
        div.coloumn {
            float:left;
        }
        div.coloumn-right {
            float:right;
        }                                    
        .seccolor{background-color:#999999;}
        .bg2{background-color:#f4f4f4;}                                    
        div.clear{
            clear:both;
            background-color:#f4f4f4;
        }
        .header-text{font-size:11px; font-family:arial; font-weight:normal;}
		.top-border{
			border:#999999 solid 1px;
		}
		.contentheading{
			font-family: Arial, Verdana,'San serif';
			font-size:22px;		 
			color:#cf1f25;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 10px 0px 10px 0px;
		}
		.contentsmallheading{
			font-family: Arial, Verdana,'San serif';
			font-size:16px;
			color:#cf1f25;			 
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.contentsmallheadingtext{
			font-family: Arial, Verdana,'San serif';
			font-size:14px;
			color:#2c2c2c;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.content-service{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 9px 0px 9px 0px;
		}
		.content-font{
			font-family: Arial, Verdana,'San serif';
			font-size:11px;
			color:#3c3c3c;
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
		}
		.whytop-border
		{
			border:#999999 solid 1px;
			text-align:left;
		}

		.whycontentheading
		{
			font-family: Arial, Verdana,'San serif';
			font-size:22px;
			color:#cf1f25;	
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 5px 0px 10px 0px;
		}

		.whycontentsmallheading
		{
			font-family: Arial, Verdana,'San serif';
			font-size:16px;
			color:#cf1f25;		 
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		.whycontentsmallheadingtext
		{
			font-family: Arial, Verdana,'San serif';
			font-size:14px;
			color:#2c2c2c;			 
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 0px 0px 10px 0px;
		}
		
		.whycontent
		{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:normal;    
			text-decoration:none;
			text-align:left;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 15px 0px 15px 15px;
		}

		.whycontent-font
		{
			font-family: Arial, Verdana,'San serif';
			font-size:11px;
			color:#3c3c3c;
			font-weight:normal;    
			text-decoration:none;
			text-align:center;
			
		}

		.whycontentbottom
		{
			font-family: Arial;
			font-size:14px;
			color:#2c2c2c;
			font-weight:normal;    
			text-decoration:none;
			text-align:left;
			vertical-align:top;
			background-color:#fcfbe5;
			padding: 9px 0px 9px 9px;
		}

		.whycontentbottomheading
		{
			font-family: Arial, Verdana,'San serif';
			font-size:15px;
			color:#cf1f25;
			font-weight:bold;    
			text-decoration:none;
			text-align:center;
			vertical-align:top;
			padding: 10px 0px 10px 0px;
		}	
.service-heading-text{font-family:Arial;font-size:20px;line-height:25px;color:#fff;font-weight:bold}
.service-sub-heading-text{font-family:Arial;font-size:14px;line-height:20px;color:#fff;font-weight:bold}
.service-content-heading{font-family:Arial;font-size:25px;line-height:30px;color:#C4001F;font-weight:bold}
.service-content-sub-heading{font-family:Arial;font-size:28px;line-height:35px;color:#C4001F;font-weight:bold}
.service-content-text{font-family:Arial;font-size:15px;line-height:20px;color:#505050;font-weight:regular}		
</style> ";   
$header_content ="<page backtop=\"100px\" backbottom=\"20px\" backleft=\"0px\" backright=\"0px\" pagegroup=\"new\">
    <page_header>
        <table align=\"center\" valign=\"top\" style=\"width:650px;\">
			<tr>
			<td  style=\"padding-top:0px;width:650px;\" valign=\"top\">
			<table  align=\"center\" valign=\"top\" style=\"width:650px;\">
				<tr>
					<td class=\"row pt3\" style=\"width:650px;\">
						<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" valign=\"top\" style=\"width:650px\">
							<tr>
								<td class=\"address-text al bg9 pl30\" valign=\"middle\" style=\"width:620px;height:75px;padding-top:0px;padding-bottom:0px;\">
									<img src=\"".$variables['images']."/smeerp-prop-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\">
								</td> 
							</tr>
							<tr>
								<td class=\"address-text al bg8 pl10\" style=\"width:620px;height:5px;\">
									<span class=\"white-text b\"></span> 
								</td> 
							</tr>
						</table>
					</td>
				</tr> 
			</table>
			</td>
			</tr>
        </table>
    </page_header>
    <page_footer> 
        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" style=\"width:650px;\" align=\"center\">
		<tr>
		    <td class=\"row bg9\" valign=\"top\" style=\"width:650px;\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" valign=\"top\" style=\"width:650px;\">
				<tr>
					<td class=\"address-text al\" style=\"width:650px;height:1px;\">	
					</td> 
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class=\"row pt5 pb5 bg8\" valign=\"top\" style=\"width:650px;height:20px;\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" valign=\"top\" style=\"width:650px;\" align=\"center\">
			<tr>
				<td class=\"address-text al pl10\" style=\"width:570px;\"> 
					<div class=\"address-text-small\">Head office : Plot No. 17, MG street, &nbsp; Jhena &nbsp; 440025 &nbsp; Maharashtra India</div> <div class=\"address-text-small\" style=\"padding-top:3px;\"> 9823099996, 9823099998, 9823088885 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sales@SMEERPTech.in &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; www.SMEERPTech.in 
					</div> 
				</td>
				<td class=\"address-text\" align=\"right\" style=\"width:80px;\">
					 <span class=\"address-text-small ar\">page [[page_cu]] / [[page_nb]] </span> 
				</td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
    </page_footer>
	<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" border=\"0px\" style=\"width:650px;\" valign=\"top\"  >
		<tr>
			<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['content']."
			</td>
		</tr>
	</table> 
</page>
";
    
	/* 
	$content1='';
	for($i=1;$i<=1;$i++){
		 $content1.= "<page pageset=\"old\" >
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" align=\"center\" style=\"width:650px;\" valign=\"top\"  >
		<tr>
			<td class=\"invoice-text\" valign=\"top\" style=\"padding-left:0px;padding-right:0px;\">".$data['content']."
			</td>
		</tr>
		</table> 
		</page>";  
	}   */
	$variables['images'] = DIR_WS_IMAGES_NC;
	$data['company_quot_prefix']='SMEERP';
	//Terms and conditions Bof
	$content1.= "
	<page pageset=\"old\" > 
		<table cellspacing=\"0\" cellpadding=\"0px\" border=\"0\" align=\"center\" style=\"width:670px;\">
			<tr>
				<td class=\"al pt3\" valign=\"top\" style=\"width:650px;\" >
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:0px;width:670px;\">
					<tr>
						<td style=\"width:330px;\" valign=\"top\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\" style=\"padding-left:0px;padding-right:0px;width:330px;\">
							<tr>
								<td class=\"content-small-term pl\"  style=\"width:330px;\"> 
									PART B - Terms and Conditions:: By making payment against this invoice/proposal, it is agreed between SMEERP ('SELLER') and the CUSTOMER or CLIENT ('BUYER') as follows -  
							   </td>
							</tr> 	
							<tr>
								<td class=\"content-small b pl pt2\" style=\"width:330px;\">A. Definitions</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \" style=\"text-align:justify;padding-left:0px;padding-right:0px;width:330px;\">
								<table cellspacing=\"0\" cellpadding=\"0\" border=\"0px\"  style=\"width:330px;\">
									<tr>
										<td style=\"width:330px;text-align:justify;\">'Extra Efforts' or 'Additional Services' means any efforts or services which are not mentioned in the proposal.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Charges' means all fees, charges/tariffs, costs and rates chargeable by SELLER from time to time for providing the BUYER with the Service, for Software Products, for Goods and Additional Services and other Government levies.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Customer or Client or Buyer' shall mean any person, partnership firm or private limited company or limited company or such other organisation authorized by SELLER to use the services or to whom SELLER sell goods or services.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Government' shall mean the Government of India and/or the State Government of Maharashtra or such other local authority, as the case may be.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Services' shall mean all/any services made available by SELLER directly or via its associates or channel partners.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Software Products' are like myCraft<sup>TM</sup>, myGallery, Nerve Center<sup>&reg;</sup> etc. and are products/solutions developed by Aryan Salve and license to use for which are sold by SELLER directly or via its associates or channel partners.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'SaaS' is Software-As-A-Service provided to BUYER (if paid) / USER (if free) by SELLER to use Online (internet).
										</td>
									</tr>									
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Goods' shall mean all the Goods/Material/Equipment sold by SELLER directly or via its associates or channel partners.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'MySMEERP' is the Online Customer Support Portal for support, billing, communication etc.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;text-align:justify;\">'Renewal/Expiry Date' means renewal/expiry date of Service / SaaS / Software Products / Goods as mentioned in Invoice.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;text-align:justify;\">'Support Email' means Support@SMEERPTech.in
										</td>
									</tr>												
								 </table>												
								</td>
							</tr>	
							<tr>
								<td class=\"content-small b pl pt2\"  style=\"width:330px;\"> B. Provision of Services or Goods</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term\"  style=\"width:330px;text-align:justify;\">
									SELLER agrees to provide the Services or Goods to the BUYER subject to the terms and conditions of this agreement. 
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pl pt2 \"  style=\"width:330px;\"> C. Obligations of SELLER</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term\"  style=\"width:330px;\">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. SELLER shall use reasonable efforts to make the Services / SaaS available to the BUYER at all times.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. The availability and quality of the Services / SaaS / Software Products / Goods may be affected by the factors outside SELLER control such as physical obstructions, geographic, weather conditions and other causes of interference or faults.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. Services / SaaS / Software Products may be suspended in whole or in part at any time, without notice, if Network fails or requires maintenance. SELLER will make all reasonable efforts to minimise the frequency and duration of such events. The BUYER will remain liable for all charges during the period of suspension unless SELLER in its discretion decides otherwise.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. SELLER has the sole right and discretion to vary or increase the Charges at any time on reasonable notice to the BUYER.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. SELLER reserves the right to apply a monthly/quarterly/half-yearly/yearly financial limit and such other conditions for Charges incurred by the BUYER and to demand interim advance payment, suspend or disconnect access to the Services / SaaS / Software Products if such limits are exceeded.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">6. SELLER has the right to check the credentials of the BUYER including the Customer's financial standing and to use the services of any person or agency for such purpose.</td>
									</tr>
									</table>									 
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pl pt2\"  style=\"width:330px;\"> D. Obligations of the BUYER</td>
							</tr> 	
							<tr>
								<td class=\"pt2 content-small-term \"  style=\"width:330px;\">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\"  style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">The BUYER hereby expressly agrees:
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">1.	The Technology and Human Beings neither are nor can ever be 100% perfect and therefore are vulnerable to faults / failure / delay / mistakes leading to disruption or delay or downtime of services. Therefore under all/any circumstances, BUYER can not hold SELLER responsible for any loss faced by BUYER due to such faults / failure / delay / mistakes.
										</td>
									</tr>									
									<tr>
										<td  style=\"width:330px;\">2. To make payments for the Services / SaaS / Software Products / Goods on the following basis
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(a) payment will be due when SELLER raises the Proforma-Invoice / Invoice / Bills / Billing Statement on the BUYER.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(b) payment will be made on or before the due date mentioned in the Proforma-Invoice / Invoice / Bills / Billing Statement or as per schedule mentioned in the Proposal, failing which SELLER shall be entitled to charge interest @18% per annum or INR 500 whichever is higher and/or late fees on all outstanding Charges from due date till date of payment &amp; shall be entitled to discontinue Services / SaaS / delivery of Goods / Software Products, without notice, in its sole discretion. Cheque return will attract a penalty of Rs. 500 separately.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(c) in case of delinquent payment or non-payment, under all/any circumstances the Domain-Name ownership can not be claimed by BUYER nor these can be transferred by BUYER to any other service provider. 
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(d) the BUYER will pay all the costs of collection and legal expenses, with interest should it become necessary to refer the matter to a collection agency or to legal recourse to enforce payment.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(e) SELLER shall be entitled to apply payments / deposits / advances made by the BUYER towards any Charges outstanding including for any other SELLER Services / SaaS / Software Products / Goods availed by the BUYER.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(f) payments will be made in Cash, Credit card or A/c Payee Cheque or Demand Draft or Internet Banking or any other instrument drawn on any bank in India and payable at Jhena 440025 (Maharashtra).
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(g) the BUYER shall be liable for all Charges for the Services / SaaS / Software Products / Goods provided to the BUYER whether or not the Services / SaaS / Software Products / Goods have been used by the BUYER.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(h) in the event of any dispute regarding the Charges, the BUYER agrees to pay SELLER Charges billed pending resolution of such dispute.
										</td>
									</tr>
									<tr>
										<td  style=\"width:330px;padding-left:0px;\">(i) the BUYER shall be liable to pay for the Services / SaaS / Software Products / Goods provided even if BUYER does not receive the Proforma-Invoice / Invoice / Bills / Billing statement. It will be the BUYER's responsibility to make enquiries in case of non-receipt of Proforma-Invoice / Invoice / Bills / Billing statement.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">(j) Charges payable by the BUYER are exclusive of taxes, duties or levies payable, unless expressly stated to the contrary in the Proposal / Proforma-Invoice / Invoice / Billing Statement / Bills.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">(k) any advance payment / security deposit paid by the BUYER shall be adjusted against any dues payable by the BUYER to SELLER and balance if any will be refunded by SELLER within 60 days from the deactivation of the Services.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">3. To make 100% advance payment for Charges including service charges if Proforma-Invoice / Invoice  is raised to BUYER by SELLER or if the Services / SaaS /Software Products / Goods are provided to BUYER.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">4. To not use or cause or allow others / anybody to use the Services / SaaS / Software Products / Goods for any improper, immoral or unlawful or illegal purpose including in any manner (for e.g. Spamming, Bulk Emailing, Transmission of infected content, Porn, Selling illegal products etc.).
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">5. To comply with instructions issued by Government or SELLER, concerning BUYER's access to and use of Services / SaaS / Software Products / Goods.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">6. To furnish correct and complete information and documents as required by SELLER from time to time. The Services / SaaS / Software Products / Goods agreed to be provided by SELLER shall always be subject to verification of the BUYER's credentials and documents and if at any time, any information and/or documents furnished by the BUYER is/are found incorrect or incomplete or suspicious, SELLER shall be entitled to suspend/terminate the Service forthwith without any further notice.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">7. That SELLER may suspend the Service / SaaS / Software Products or delivery of Goods in whole or in part at any time without prior notice and without assigning any reason thereto. SELLER reserves the right to Charge for re-activation or re-delivery.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">8. BUYER is liable (a) for the Charges during the period of suspension and thereafter (b) to pay the overages as applicable and (c) Charges towards Extra Efforts
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">9. To comply with the applicable laws, rules and regulation regarding the use of the Services / Saas / Software Products / Goods and procurement of the Equipment including but not limited to relevant tax laws and import control regulations.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">10. To pay to SELLER such amount as SELLER may require as security for the due performance of the BUYER's obligations under these terms and conditions. SELLER may set off these amounts against any cost, damages or expenses which SELLER may suffer or incur as a result of the BUYER's failure to perform any of these obligations. Security deposit amount shall not carry any interest.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">11. To inform SELLER, in writing, of any changes in billing name or billing address or email id or cellular number. Any Written Communication, Proforma-Invoice, Invoice , Billing statement or Notice from SELLER to the BUYER will be deemed as served within 48 hours of posting by ordinary mail or email.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">12. To notify SELLER immediately in case of any complaints with regards to the Services / SaaS / Software Products / Goods via email to Support Email. 
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">13. Not to assign any right or interest under this agreement without SELLER prior written consent.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">14. BUYER is not entitled to assign/transfer/resell/lease/rent or create any charge/lien on the Service / SaaS of any nature whatsoever or Software Product owned by SELLER, without prior permission of SELLER. Any transfer affected in contravention of the express terms contained herein, shall not absolve the BUYER of his/her primary duty towards SELLER for usages charges levied against the BUYER. 
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">15. To timely provide SELLER with the certificates etc. towards any deductions as per the law. 
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">16. Security and Backup of the data to which BUYER is provided access with is the liability of BUYER.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">17. BUYER must verify the credentials of the visiting SELLER personnel.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">18. Neither SELLER personnel will install any illegal/pirated software/text/images for BUYER nor any Illegal/pirated software/text/images are covered under maintenance service support.  
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">19. BUYER must provide (a) Work/Purchase-order referring to proposal having mention scope of work and payment schedule (b) Duly signed and stamped copy of proposal (c) Duly filled, signed and stamped Customer (BUYER) Agreement form. 
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">20. BUYER must designate only one of his/her (BUYER's) personnel to coordinate and to approve the work/delivery. BUYER is not allowed to change designated personnel till completion of work. In case of factors outside control, change of personnel is allowed if agreeable to both the parties. BUYER will not hold SELLER responsible for any failure/fault/delay/loss arising due to such change of personnel.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">21. Access to Service / SaaS / Software Products is restricted on Service expiry date. Domain / Web hosting / Email etc accounts or any Service / SaaS / Software Products with data/email etc are deleted after seven days of Renewal/Expiry Date if not renewed before. Late renewal during these seven days attracts late payment Charges too.
										</td>
									</tr>
									<tr>
										<td style=\"width:330px;padding-left:0px;\">22. To be bound at all times by any modifications and or variations made to these terms and conditions. 
										</td>
									</tr>								
									<tr>
										<td style=\"width:330px;padding-left:0px;\">23. SELLER will remind BUYER about the renewal dates via SMS/Email automated notifications. BUYER is responsible to remember renewal dates and make renewal payments before 60 days of Renewal/Expiry Date.  
										</td>
									</tr>
									</table>
								</td>
							</tr>
							</table>
						</td>
						<td style=\"width:330px;\" valign=\"top\">
							<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:0px;width:330px;\">
							<tr>
								<td class=\"content-small-term\"  style=\"width:330px;\">
								<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding-left:0px;padding-right:0px;width:330px;\">  
									<tr>
										<td style=\"width:330px;\">24. BUYER is liable to pay any statutory duties or levies or taxes as applicable by the law. 
										</td>
									</tr> 
									<tr>
										<td style=\"width:330px;\">25. BUYER must provide information/references for his/her (BUYER's) choice for design, images in JPEG format, content in editable MS Word format and Logo in editable CDR or PSD format via email and before commencement of work. Our designers will use all the images provided as-is. Besides basic cropping, SELLER is unable to perform advanced manipulation. Logo design, Content creation or Content writing or copy-writing or typing or proof-reading or spell-check or grammar-check or extra design samples is separate job work needing Extra Efforts and is separately chargeable. Static page means an A4 size page containing text in Arial font size 12.
										</td>
									</tr>								
									<tr>
										<td  style=\"width:330px;\">26. SELLER will not be responsible for any delay/loss/fault/failure caused due to delay in providing the content to SELLER by BUYER or BUYER's Personnel or BUYER'S Associates or non-availability of any of BUYER's official or any other delay on BUYER's part. BUYER will not hold back any of SELLER payment for any delays/fault/failure due to any reason on part of BUYER. Content/Work received after the completion of work OR in excess as per proposal will be termed as extra and separately chargeable.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">27. Transfer of domain name from BUYER's existing service provider to SELLER is the sole responsibility of BUYER.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">28. BUYER must use MySMEERP or email to Support Email for service and support.</td>
									</tr>
								</table>									 
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2\"  style=\"width:330px;\"> E. SaaS / Software Product License to use:</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term\"  style=\"width:330px;\">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. If billed for, BUYER gets the single domain name license to use for the SELLER Software Products / SaaS. SELLER Software Products / SaaS are provided on single domain name license to use basis only on SELLER web servers without ftp & hosting control panel access. The license to use of Software Products / SaaS is non-transferable to any other Domain name or other person/party and is subject to yearly renewal against annual maintenance support charges.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. Under all/any circumstances, programming code of Software Products / SaaS is the sole property of Aryan Salve 9823078899 who has appointed SMEERP as authorized reseller and under no circumstances BUYER can claim any rights towards the ownership of Software Product or Programming code or SaaS. License of the Software product / SaaS is only for using it i.e. License to use.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. Credits to Service / Software Product / SaaS and/or SELLER are compulsorily mentioned on the BUYER's website. Credits to SELLER is compulsorily mentioned on the designed and/or printed material of the Customer.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. Any customization done in the Software Product / SaaS doesn't make the BUYER the owner of Software product / SaaS. Under all/any circumstances the Software Product / SaaS remains the property of Aryan Salve 9823078899 (who appointed SMEERP as authorized reseller) with full rights over entire Software product / SaaS and customizations done. 
										</td>
									</tr>
									</table>								 
								</td>
							</tr> 
							<tr>
								<td class=\"content-small b pt2\" style=\"width:330px;\"> F. Validity</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. Both parties (BUYER and SELLER) agree that, this agreement has been duly authorised and executed and is valid and binding and is enforceable in law in accordance with its terms.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. The validity construction and performance of this agreement shall be governed by and interpreted in accordance with the laws of the Republic of India.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. The courts in Jhena shall have the jurisdiction.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. Should any provision of this agreement be or become ineffective or be held to be invalid, this shall not affect the validity of the remaining provisions. Any invalid provision in this agreement shall be replaced, interpreted or supplemented as the case may be in such a manner that the intended economic purpose of the agreement will be achieved.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. This agreement is complete and exclusive statement of agreement between parties and it supersedes all understanding or prior agreement, whether oral or written and all representations or other communications between parties.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">6. These terms & conditions are subject to applicable Indian laws, rules and regulation framed thereunder and any statutory modifications or re-enactment for time being in force and any other Government regulations issued from time to time. </td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">7. Prices/Availability/Specifications are subject to change without prior notice. </td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2 pr5\" style=\"width:330px;\"> G. Support, Goods Return, Warranties, Disclaimer of warranties</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. SELLER sends BUYER the information related to terms / conditions / services / offers / warnings / support / advisories etc. via email / sms / telephone / letter. It is BUYER's responsibility to revert or act or do the needful accordingly.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. Annual Maintenance Support Charge for renewal of License to use of Service / Software Product / SaaS is compulsory as specified in the proposal and includes services (a) Maintaining Service / Software Product / SaaS in running / working condition (b) FREE feasible patches and updates (c) BUYER support via email, telephone and MySMEERP</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. Goods once sold will not be taken back or exchanged and payment will not be refunded under any circumstances. Our risk & responsibility ceases on delivery of the goods. After sales support services is valid only if applicable to purchased item and explicitly mentioned in the Proforma-Invoice / Invoice.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. SELLER makes no representation or warranty other than those set forth in this agreement. SELLER expressly disclaims all other warranties express or implied, including, but not limited to any implied warranty or merchantability or fitness for a particular purpose.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. Any Services / SaaS / Software Products / Goods or Equipment re-sold (where SELLER is a Reseller of third-party Services / Softwares / SaaS / Goods) by SELLER will carry the warranty, if any, of the manufacturer. Service to such Service / Software Products / SaaS / Goods or Equipment, if not explicitly mentioned in Proforma-Invoice / Invoice, if any, will be provided by authorized service centre of the manufacturer. SELLER is not liable towards service support or replacement under warranty, if any, of any Service / Software Products / SaaS / Goods or Equipment manufactured by third party and that SELLER is just reselling.</td>
									</tr> 
									</table>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2 pr5\" style=\"width:330px;\"> H. Disclaimer of liability</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. SELLER shall not be liable to the BUYER for any loss or damage whatsoever or howsoever caused arising directly or indirectly in connection with this agreement, the Services or Software Products or SaaS or Equipment or Goods, their use, application or otherwise except to the extent to which it is unlawful to exclude such liability.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. Malfunction/Limitation in working of any of SELLER Software Product / Service / SaaS / Goods due to any change in third party Software/Hardware on which SELLER Software Product / Saas / Service / Goods are dependent for functioning is not covered under maintenance support and SELLER cannot be held responsible for it. Though SELLER shall use reasonable efforts for resolution of issue if feasible. However BUYER will be liable towards Charges for these Extra Efforts.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. Under any circumstances, SELLER will not be responsible for any kind of loss / damage of data / equipment etc. while / due to working SELLER service personnel at BUYER's site. All the maintenance and service related work executed is at the risk of BUYER. BUYER has rights to ask SELLER service personnel to work under BUYER's or BUYER's authorized personnel's vigilance, seek information and take decision regarding maintenance / service work.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. Notwithstanding the generality of (a) above, SELLER expressly excludes liability for consequential loss, damage or for loss of profit, business revenue, goodwill or anticipated savings.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. SELLER at its discretion, may send to the BUYER various like information or notices or reminders etc on BUYER's / BUYER's Personnel cellular number through SMS or on BUYER's / BUYER's Personnel email id through Email or on BUYER's billing address through post/mail or otherwise. In case the BUYER does not wish to receive such information then BUYER may notify SELLER via email at Support Email or via Signed Letter at SELLER office address.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">6. In the event that any exclusion contained in this agreement shall be held to be invalid for any reason, and SELLER becomes liable for the loss or damage or failure or fault that it may otherwise not have been liable, such liability shall be limited to the cost of the Services / SaaS / Software Product / Goods actually paid for by the BUYER to SELLER during the relevant period.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">7. BUYER agrees to indemnify and keep SELLER harmless and defend SELLER at its (BUYER's) own expense from and against all claims arising as a result of breach of this agreement and from all taxes, duties or levies.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">8. SELLER advises BUYER to make cash payments only after verifying the credentials (ID card etc.) of the visiting personnel of SELLER. SELLER encourages cheque, demand draft or internet banking transfer to avoid any untoward incident. SELLER will not be liable for any such loss incurring due to handing over of cash payment to unauthorized personnel.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">9. BUYER agrees that any request/communication received from BUYER's and BUYER's authorized personnel email-ids / cellular mobile numbers on SELLER Support Email via email or SMS via Cellular mobile numbers shall be deemed to be valid request/communication from the BUYER. Nothing herein shall apply with respect to the notice to be given by the BUYER or any other provisions of this agreement.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">10. BUYER must regularly update and verify credentials of BUYER's authorized personnel registered and authorized for with MySMEERP and in case of any issue must inform SELLER immediately and confirm the same in writing to avoid loss/damage. Liability of any loss due to misuse/mistake/mishandling/loss of confidential information by/due to BUYER or BUYER's authorized personnel remains with BUYER and SELLER will not be held responsible under any circumstances.</td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2 pr5\" style=\"width:330px;\"> I. Termination</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. Either party shall have the right to terminate the agreement by giving 60 days prior notice in writing.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">2. Notwithstanding anything contained herein, SELLER shall be entitled to terminate this agreement and the Services / Saas / Software Product License if (a) the Government either suspends or terminates the License or Permission or the Services temporarily or otherwise (b) at any time the BUYER fails to satisfy the requisite credit checks or provides fraudulent information to SELLER. (c) the BUYER fails to pay its subscription or the Charges due (d) the BUYER is in breach of any other terms of the agreement and the BUYER does not remedy the breach within seven (7) days of the day of receipt of a written / email notice from SELLER specifying the breach.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">3. The agreement may also be terminated at the option of either party, on the happening of the following events (a) if either party is declared insolvent, bankrupt or is liquidated or dissolved (b)	if a trustee or receiver is appointed to take over the assets of either party (c) if the Government requires any of this agreement to be revised in such a way as to cause significant adverse consequences to either party</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">4. Termination of this agreement under the preceding provisions shall be without prejudice to and in addition to any right or remedy available to the terminating party under any applicable law or statute.</td>
									</tr>
									<tr>
										<td  style=\"width:330px;\">5. In the event of termination of the agreement for any reason whatsoever, SELLER shall be entitled to recover all outstanding Charges and dues from the BUYER.</td>
									</tr> 
									<tr>
										<td  style=\"width:330px;\">6. If the agreement is terminated for reasons of fraudulent information provided or misuse or unlawful use by the BUYER, the security deposit shall be forfeited.</td>
									</tr>  
									</table>
								</td>
							</tr>
							<tr>
								<td class=\"content-small b pt2 pr5\" style=\"width:330px;\"> J. Miscellaneous</td>
							</tr>
							<tr>
								<td class=\"pt2 content-small-term \">
									<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" 			style=\"padding-left:0px;padding-right:0px;width:330px;\"> 
									<tr>
										<td  style=\"width:330px;\">1. All notices required to be given to SELLER pursuant to this agreement shall be in writing and shall be directed by registered post to the Registered office at SMEERP, Plot No. 17, MG street, Jhena 440025 Maharashtra India, www.SMEERPTech.in Support@SMEERPTech.in</td>
									</tr>
									</table>
								</td>
							</tr>
							</table>
						</td>
					</tr>			
					</table>
				</td>
			</tr>
		</table> 
	</page>";
	//Terms and conditions Eof
	$invoiceArr['content'] = ob_get_clean();
	$content = $css.$header_content.$content1 ;
	//$content = $css.$header_content;
   	$invoiceArr['content']=$content;
   	$invoiceArr['header']='';
   	$invoiceArr['footer']=''; 
    createQuotationPDF3($data["filename"],$invoiceArr);
}


		function getBifurcation( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_INV_B;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
						
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }

    }
    
function createQuotationPDF3($q_no, $data){   
  require_once(DIR_FS_LIB .'/html3pdf/html2pdf.class.php'); 
    try
    {  
        //$html2pdf = new HTML2PDF('P', 'LETTER', 'en'); 
		$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10,10,10,10));
        //$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($data['content'], isset($data['vuehtml']));
		$filename = DIR_FS_PST_QUOT_PDF .'/'. $q_no .'.pdf'; 
		@chmod($filename, 0777);
        $html2pdf->Output($filename,'F');  
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }  
	
}    
function createQuotationPDF( $q_no, $data ) {
		require_once(DIR_FS_ADDONS .'/html2pdf/config.prospectsquot.inc.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
		parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
	  
		global 	$g_config;
		
		class ReportData extends Fetcher {
			var $content;
			var $url;
		
			function get_data($data) {
				return new FetchedDataURL($this->content, array(), "","");
			}
		
			function ReportData($content) {
				$this->content = $content;
			}
			
			function get_base_url() {
				return "";
			}
		} 
		$contents 	= '';
		$contents = $data['content'];      
		
		// Configuration Settings.
		$g_config = array(
							'process_mode' 	=> 'single',
							'mode'		    => '',
							//'pixels'		=> '750',
							'scalepoints'	=> '1',
							'renderimages'	=> '1',
							'renderlinks'	=> '1',
							'renderfields'	=> '1',
							'media'			=> 'LETTER',
							'cssmedia'		=> 'screen',
							//'leftmargin'	=> '30',
							'leftmargin'	=> '0',
							//'rightmargin'	=> '15',
							'rightmargin'	=> '0',
							'topmargin'		=> '0',
							'bottommargin'	=> '0',
							'encoding'		=> 'iso-8859-1',
							//'encoding'		=> 'UTF-8',
							'headerhtml'	=> $data['header'],
							'boydhtml'		=> $contents,
							'footerhtml'	=> $data['footer'],
							'watermarkhtml'	=> '',
							'pslevel'		=> '3',
							'method'		=> 'fpdf',
							'pdfversion'	=> '1.3',
							'output'		=> '2',
							'convert'		=> 'Convert File'
						);
		
		$media = Media::predefined('LETTER');
		//$media = Media::predefined($config->export->pdf->papersize);  //by aryan
		$media->set_landscape(false);
		/*
		$media->set_margins(array(	'left' => 10,
									'right' => 10,
									'top' => 15,
									'bottom' => 35)
							);
		$media->set_pixels(700);
		*/
		$media->set_margins(array(	'left' => 0,
									'right' => 0,
									'top' => 10,
									'bottom' => 10)
							); 
		//$media->set_pixels(795);
		
		$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
		$g_pt_scale = $g_px_scale * 1.43;
		
		$pipeline 						= PipelineFactory::create_default_pipeline("","");
		$pipeline->pre_tree_filters[] 	= new PreTreeFilterHeaderFooter( $g_config['headerhtml'], $g_config['footerhtml']);
		$pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
		if ($g_config['renderfields']) {
			$pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
		}

		// Changed so that script will try to fetch files via HTTP in this case) and to provide absolute paths to the image files.
		// http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
		//		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
		$pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
		//$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
		$pipeline->destination 			= new DestinationFile($q_no);
		 
		$pipeline->process($q_no, $media);
		$filename = DIR_FS_PST_QUOT_PDF .'/'. $q_no .'.pdf'; 
		@chmod($filename, 0777);
		$contents = NULL;
		return true;
}
    
?>
