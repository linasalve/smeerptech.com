<?php 

	class Newsletters {
		
       
        const DRAFT = 2;
        const COMPLETED = 1;
		const PENDING = 0;
		
		function getStatus(){
			$status = array('Completed' => Newsletters::COMPLETED,
							'Pending'  => Newsletters::PENDING,
							'Draft'  => Newsletters::DRAFT
						);
			return ($status);
		}
		     
        
		
		function getNewslettersEmailList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_NEWSLETTERS_SENT_EMAIL;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
		
		
		/**
		 *	Function to get all the support tickets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		
        function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			
			$query .= " FROM ". TABLE_NEWSLETTERS. " LEFT JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_NEWSLETTERS .".client 
			LEFT JOIN ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY." ON  ".TABLE_NEWSLETTERS.".nws_category_id = ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY.".id";
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
         function getNewNumber(&$db){
            
            $number= 300;
            $query      = "SELECT MAX(number) AS number FROM ". TABLE_NEWSLETTERS;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("number") > 0){
                $number = $db->f("number");
            }
			$number++;
            return $number;
        }
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            $files = $_FILES;
            
			if ( !isset($data['client']) || empty($data['client']) ) 
                $messages->setErrorMessage('Select Client.');
			
			if ( !isset($data['newsletters_smtp_id']) || empty($data['newsletters_smtp_id']) ) 
                $messages->setErrorMessage('Select SMTP Account id.');
			 
			if ( !isset($data['domain_name']) || empty($data['domain_name']) ) 
                $messages->setErrorMessage('Domain name should not be blank. Please check your SMTP details.');
			
			if ( !isset($data['domain_mailgun_key']) || empty($data['domain_mailgun_key']) ) 
                $messages->setErrorMessage('Please check your SMTP details.');
			
			
			if ( !isset($data['sockethost']) || empty($data['sockethost']) ) 
                $messages->setErrorMessage('Socket host cannot be empty.');
			
			if ( !isset($data['smtpauthuser']) || empty($data['smtpauthuser']) ) 
                $messages->setErrorMessage('Smtp auth user cannot be empty.');
			if ( !isset($data['smtpauthpass']) || empty($data['smtpauthpass']) ) 
                $messages->setErrorMessage('Smtp auth password cannot be empty.');
			if ( !isset($data['smtpport']) || empty($data['smtpport']) ) 
                $messages->setErrorMessage('Smtp port cannot be empty.');
			
			if ( !isset($data['from_name']) || empty($data['from_name']) ) 
                $messages->setErrorMessage('From name cannot be empty.');
			
			if ( !isset($data['from_email']) || empty($data['from_email']) ) 
                $messages->setErrorMessage('From_email cannot be empty.');
			
			if ( !isset($data['replyto_name']) || empty($data['replyto_name']) ) 
                $messages->setErrorMessage('Reply to name cannot be empty.');
			
			if ( !isset($data['replyto_email']) || empty($data['replyto_email']) ) 
                $messages->setErrorMessage('Reply to email cannot be empty.');
			
			if ( !isset($data['subject']) || empty($data['subject']) ) 
                $messages->setErrorMessage('Subject cannot be empty.');
            
            if ( !isset($data['text']) || empty($data['text']) ) 
                $messages->setErrorMessage('Email Body cannot be empty.');
            
		   
            
            $data['attachment'] = '';
            
            if((!empty($files['attachment']) && (!empty($files['attachment']['name'])))){
                $filename = $files['attachment']['name'];
                $data['attachment'] = $filename;
                $type = $files['attachment']['type'];
                $size = $files['attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        function validateUpdate(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            $files = $_FILES;
            
			if ( !isset($data['client']) || empty($data['client']) ) 
                $messages->setErrorMessage('Select Client.');
			
			if ( !isset($data['newsletters_smtp_id']) || empty($data['newsletters_smtp_id']) ) 
                $messages->setErrorMessage('Select SMTP Account id.');
			
			if ( !isset($data['domain_name']) || empty($data['domain_name']) ) 
                $messages->setErrorMessage('Domain name should not be blank. Please check your SMTP details.');
			
			if ( !isset($data['domain_mailgun_key']) || empty($data['domain_mailgun_key']) ) 
                $messages->setErrorMessage('Please check your SMTP details.');
			
			if ( !isset($data['sockethost']) || empty($data['sockethost']) ) 
                $messages->setErrorMessage('Socket host cannot be empty.');
			
			if ( !isset($data['smtpauthuser']) || empty($data['smtpauthuser']) ) 
                $messages->setErrorMessage('Smtp auth user cannot be empty.');
			if ( !isset($data['smtpauthpass']) || empty($data['smtpauthpass']) ) 
                $messages->setErrorMessage('Smtp auth password cannot be empty.');
			if ( !isset($data['smtpport']) || empty($data['smtpport']) ) 
                $messages->setErrorMessage('Smtp port cannot be empty.');
			
			if ( !isset($data['from_name']) || empty($data['from_name']) ) 
                $messages->setErrorMessage('From name cannot be empty.');
			
			if ( !isset($data['from_email']) || empty($data['from_email']) ) 
                $messages->setErrorMessage('From email cannot be empty.');
			
			if ( !isset($data['replyto_name']) || empty($data['replyto_name']) ) 
                $messages->setErrorMessage('Reply to name cannot be empty.');
			
			if ( !isset($data['replyto_email']) || empty($data['replyto_email']) ) 
                $messages->setErrorMessage('Reply to email cannot be empty.');
			
			if ( !isset($data['subject']) || empty($data['subject']) ) 
                $messages->setErrorMessage('Subject cannot be empty.');
            
            if ( !isset($data['text']) || empty($data['text']) ) 
                $messages->setErrorMessage('Email Body cannot be empty.');
            
            
		   
            
            $data['attachment'] = '';
            
            if((!empty($files['attachment']) && (!empty($files['attachment']['name'])))){
                $filename = $files['attachment']['name'];
                $data['attachment'] = $filename;
                $type = $files['attachment']['type'];
                 $size = $files['attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}        
          
    }
?>