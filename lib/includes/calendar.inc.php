<?php
	class Calendar {
		
		 const ACTIVE  = 1;
		 const DEACTIVE = 0;   
         
         function getStatus() {
			$status = array(
							'ACTIVE'   => Calendar::ACTIVE,
                            'DEACTIVE'    => Calendar::DEACTIVE
						);
			return ($status);
		 } 
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SETTINGS_CALENDAR;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SETTINGS_CALENDAR;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (Calendar::getList($db, $details, 'id', " WHERE id = '$id'")) > 0 ) {
               $details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_SETTINGS_CALENDAR
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
               /* }
                else {
                    $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                }*/
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
           
            if ( !isset($data['start_date']) || empty($data['start_date']) ) {
                $messages->setErrorMessage("Select the from data.");
            }
            
            if ( !empty($data['start_date']) ) {
                $temp = explode('/', $data['start_date']);
                
                $date_period_from=$temp[2] . $temp[1] . $temp[0];
                $data['start_date'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            if ( !isset($data['end_date']) || empty($data['end_date']) ) {
                $messages->setErrorMessage("Select the to data.");
            }
            
            if ( !empty($data['end_date']) ) {
                $temp = explode('/', $data['end_date']);
                
                $date_period_to=$temp[2] . $temp[1] . $temp[0];
                $data['end_date'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
          
            if ( !empty($data['start_date']) && !empty($data['end_date']))
            {
            	if($date_period_from > $date_period_to){
              		 $messages->setErrorMessage("From date can not be greater than to date.");
			    }	
		    }	
            
            if(empty($data['title']) ){                
                $messages->setErrorMessage("Title can not be empty.");
            }
            
            if(empty($data['holidays']) ){                
                $messages->setErrorMessage("Holidays can not be empty.");
            }
            
            $sql = "SELECT DATEDIFF('".$data['end_date']."','".$data['start_date']."') as total_days";
            $db->query($sql);
            $db->next_record();    
            $data['total_days']=$db->f('total_days');        
            
            if( ($data['total_days']) > 31 ){
                $messages->setErrorMessage("Period specified is greater than 31 days.");
            }
             
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
                   
            if ( !isset($data['start_date']) || empty($data['start_date']) ) {
                $messages->setErrorMessage("Select the from data.");
            }
            
            if ( !empty($data['start_date']) ) {
                $temp = explode('/', $data['start_date']);
                
                $date_period_from=$temp[2] . $temp[1] . $temp[0];
                $data['start_date'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            if ( !isset($data['end_date']) || empty($data['end_date']) ) {
                $messages->setErrorMessage("Select the to data.");
            }
            
            if ( !empty($data['end_date']) ) {
                $temp = explode('/', $data['end_date']);
                
                $date_period_to=$temp[2] . $temp[1] . $temp[0];
                $data['end_date'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
          
            if ( !empty($data['start_date']) && !empty($data['end_date']))
            {
            	if($date_period_from > $date_period_to){
              		 $messages->setErrorMessage("From date can not be greater than to date.");
			    }	
		    }	
            
            if(empty($data['title']) ){                
                $messages->setErrorMessage("Title can not be empty.");
            }
            
            if(empty($data['holidays']) ){                
                $messages->setErrorMessage("Holidays can not be empty.");
            }
            
            $sql = "SELECT DATEDIFF('".$data['end_date']."','".$data['start_date']."') as total_days";
            $db->query($sql);
            $db->next_record();    
            $data['total_days']=$db->f('total_days');        
            
            if( ($data['total_days']) > 31 ){
                $messages->setErrorMessage("Period specified is greater than 31 days.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
		
		function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_SETTINGS_CALENDAR
                            ." SET ".  TABLE_SETTINGS_CALENDAR .".status= '". Calendar::ACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Settings Calendar has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Settings Calendar has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ".  TABLE_SETTINGS_CALENDAR
                            ." SET ".  TABLE_SETTINGS_CALENDAR .".status = '". Calendar::DEACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Settings Calendar has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Settings Calendar has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    }
?>
