<?php
	class Scoresheet {
		
		function getScore() {
			$score = array(
							'3'   => '3',
                            '2'    => '2',
                            '1'    => '1',
                            '0'    => '0',
                            '-1'    => '-1',
                            '-2'    => '-2',
                            '-3'    => '-3'
						);
			return ($score);
		}      
       
        function getReason() {
			$reason = array(
							'1' => 'Reporting late to work',
                            '2' => 'Delay / Noncompletion of work',
                            '3' => 'Breaking rules & policies',
                            '4' => 'Absent without informing',
                            '5' => 'Disobeyance of orders / Task services',
                            '6' => 'Insult / Offence of colleagues',
                            '7' => 'Breaking chain of command',
                            '8' => 'Issuing a wrong statement',
                            '9' => 'Consideration to employee',
                            '10' => 'Experience and Feedback',
							'11' => 'Waste of Time',
							'12' => 'Mistake/error',
							'13' => 'Delay in Promise Deadline',
							'14' => 'Non-fulfillment of Responsibility',
						);
			return ($reason);
		}
        
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}elseif ( !empty($required_fields) ){
				$query .= " ". $required_fields;
			}else{
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SCORE_SHEET;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SCORE_SHEET;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (Scoresheet::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               
               /*if ( $details['access_level'] < $access_level ) {*/
                    $query = "DELETE FROM ". TABLE_SCORE_SHEET
                                ." WHERE id = '$id'";
                                
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
               /* }
                else {
                    $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                }*/
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
          
            if ( !isset($data['comment_to']) || empty($data['comment_to']) ) {
                $messages->setErrorMessage("Select Executives.");
            }
            
            if ( !isset($data['score']) || $data['score']=='' ) {
                $messages->setErrorMessage("Select Score.");
            }
            
            if ( (!isset($data['reason_id']) || empty($data['reason_id']))) {
                $messages->setErrorMessage("Enter specify reason.");
            }
			
			if( !isset($data['particulars']) || empty($data['particulars'])) {
                $messages->setErrorMessage("Specify particulars.");
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            if ( !isset($data['comment_to']) || empty($data['comment_to']) ) {
                $messages->setErrorMessage("Select Executives.");
            }
            
            if ( !isset($data['score']) || $data['score']=='' ) {
                $messages->setErrorMessage("Select Score.");
            }
            
            if ( (!isset($data['reason_id']) || empty($data['reason_id']))) {
                $messages->setErrorMessage("Enter specify reason.");
            }
			
			if( !isset($data['particulars']) || empty($data['particulars'])) {
                $messages->setErrorMessage("Specify particulars.");
            }
			
                         
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        //For communication log
        function validateCommentAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( !isset($data['to_id']) || empty($data['to_id']) ) {
                $messages->setErrorMessage("Select Executive.");
            }
            if ( !isset($data['comment']) || empty($data['comment']) ) {
                $messages->setErrorMessage("Comment can not be empty.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}

        }
         //Used in communication log
	    function getDetailsCommLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SCORE_SHEET_LOG;
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_SCORE_SHEET_LOG .".by_id  ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
		
		
function createPdfFile($data) {
$variables['images'] = DIR_WS_IMAGES_NC ;
           
	$body =  "<style type=\"text/css\" rel=\"stylesheet\">     
			body {
				
				margin-right:auto;
				margin-left:auto;
			}
			#content {
			margin:0;
			width: 100%;
			text-align:center;
			}
			#content-main {
			width: 731px;
			text-align:center;
			margin-left:auto;
			margin-right:auto;
			}
			.heading{
				font-family:Arial, Verdana, 'San serif';
				font-size:18px;
				color:#000000;
				text-decoration:none;
				font-weight:bold;
			}   
			.sub-heading{
				font-family:Arial, Verdana, 'San serif';
				font-size:11px;
				color:#686868;
				line-height:15px;
				text-decoration:none;
				font-weight:regular;
			}
			.invoice-border{ 
				 background:url(".$variables['images']."/invoice-repeater-new.jpg);
				 background-repeat:repeat-y;
				 width:253px;
			}
			.invoice-text{
				font-family:Arial, Verdana, 'San serif';
				font-size:12px;
				color:#000000;
				text-decoration:none;
				font-weight:bold;
			}
			.content-heading{
				font-family:Arial, Verdana, 'San serif';
				font-size:12px;
				color:#FFFFFF;
				text-decoration:none;
				font-weight:bold;
			}
			.address-text{
				font-family:Arial, Verdana, 'San serif';
				font-size:10px;
				color:#FFFFFF;
				text-decoration:none;
				font-weight:regular;
				lignt-height:13px;
			}
			.address-text-small{
				font-family:Arial, Verdana, 'San serif';
				font-size:9px;
				color:#FFFFFF;
				text-decoration:none;
				font-weight:regular;
				lignt-height:13px;
			}
			.green-text{
				font-family:Arial, Verdana, 'San serif';
				font-size:11px;
				color:#176617;
				text-decoration:none;
				font-weight:regular;
				lignt-height:13px;
			}
			.white-text{
				font-family:Arial, Verdana, 'San serif';
				font-size:11px;
				color:#FFFFFF;
				text-decoration:none;
				font-weight:regular;
				lignt-height:13px;
			}
			.white-text-12{
				font-family:Arial, Verdana, 'San serif';
				font-size:12px;
				color:#FFFFFF;
				text-decoration:none;
				font-weight:regular;
				lignt-height:13px;
			}
			.design-text{
				font-family:Arial, Verdana, 'San serif';
				font-size:11px;
				color:#797979;
				text-decoration:none;
				font-weight:regular;
				lignt-height:13px;
			}
			.custom-content{
				font-family:arial,verdana,sans-serif;
				font-size:11px;
				color:#646464;
			}
			.content{
				font-family:arial,verdana,sans-serif;
				font-size:11px;
				color:#4f4f4f;
				lignt-height:10px;
			}	
			.contentsub{
				font-family:arial,verdana,sans-serif;
				font-size:10px;
				color:#4f4f4f;
				lignt-height:10px;
				font-weight:bold;
			}
			.contenthead{
				font-family:arial,verdana,sans-serif;
				font-size:13px;
				color:#4f4f4f;
				lignt-height:11px;
			}		
			.content-small{
				font-family:arial,verdana,sans-serif;
				font-size:6px;
				color:#4f4f4f;
			}	
			.content-small-term{
				font-family:arial,verdana,sans-serif;
				font-size:6px;
				color:#726e6e;
			}
			.content-small-7{
				font-family:arial,verdana,sans-serif;
				font-size:7px;
				color:#4f4f4f;
			}
			.content-small-8{
				font-family:arial,verdana,sans-serif;
				font-size:8px;
				color:#4f4f4f;
			}
			.content-small-10{
				font-family:arial,verdana,sans-serif;
				font-size:10px;
				color:#4f4f4f;
			}
			.spacer{background:url(".$variables['images']."/spacer.gif) repeat;}
			.bg1{background-color:#898989;}
			.bg2{background-color:#CCCCCC;}
			.bg3{background-color:#f4f4f4;}
			.bg4{background-color:#ba1a1c;}
			.bg5{background-color:#956565;}
			.bg6{background-color:#BEBE74;}
			.bg7{background-color:#48b548;}
			.bg8{background-color:#000000;}
			.bg9{background-color:#bf1f21;}
			.bdb1{border-bottom:1px solid #000000;}			
			.b {font-weight:bold;}			
			.al {text-align:left;}
			.ac {text-align:center;}
			.ar {text-align:right;}			
			.vt {vertical-align:top;}
			.vm {vertical-align:middle;}
			.vb {vertical-align:bottom;}			
			.fs08 {font-size:0.8em;}
			.fs11 {font-size:1.1em;}
			.fst11{font-size:1.1em; font-family:'Arial Narrow',Verdana,Arial,Sans-Serif;}
			.fs15 {font-size:1.5em;}			
			.lh15 {line-height:1.5em;}
			.lh10 {line-height:1em;}			
			.pl2{padding-left:2px;}
			.pl5{padding-left:5px;}
			.pl8{padding-left:8px;}			
			.pl10{padding-left:10px;}
			.pl11{padding-left:11px;}
			.pl15{padding-left:15px;}
			.pl20{padding-left:20px;}
			.pl60{padding-left:60px;}
			.pl42{padding-left:42px;}			
			.pr2{padding-right:2px;}
			.pr5{padding-right:5px;}
			.pr10{padding-right:10px;}
			.pb2{padding-bottom:2px;}
			.pb3{padding-bottom:3px;}
			.pb4{padding-bottom:4px;}
			.pb5{padding-bottom:5px;}         
			.pb10{padding-bottom:10px;}
			.pb13{padding-bottom:13px;}
			.pb20{padding-bottom:20px;}
			.pb25{padding-bottom:25px;}
			.pb30{padding-bottom:30px;}
			.pb40{padding-bottom:40px;}
			.pb50{padding-bottom:50px;}
			.pb100{padding-bottom:100px;}			
			.pt1{padding-top:1px;}
			.pt2{padding-top:2px;}
			.pt3{padding-top:3px;}
			.pt4{padding-top:4px;}
			.pt5{padding-top:5px;}
			.pt6{padding-top:6px;}
			.pt8{padding-top:8px;}
			.pt10{padding-top:10px;}
			.pt15{padding-top:15px;}
			 .pt20{padding-top:20px;}
			.pt25{padding-top:25px;}		
			.pt30{padding-top:30px;}       
			.pt32{padding-top:32px;}
			.pt45{padding-top:45px;}
			.b1{border:collapse;border:1px solid #000000;}
			.wp100{width:100%;}
			.wp90{width:90%;}
			.wp70{width:70%;}
			.wp65{width:65%;}
			.wp60{width:60%;}
			.wp55{width:55%;}
			.wp50{width:50%;}
			.wp45{width:45%;}
			.wp40{width:40%;}
			.wp35{width:35%;}
			.wp30{width:30%;}
			.wp28{width:28%;}
			.wp25{width:25%;}
			.wp20{width:20%;}
			.wp15{width:15%;}
			.wp13{width:13%;}
			.wp12{width:12%;}
			.wp10{width:10%;}
			.wp9{width:9%;}
			.wp5{width:5%;}
			.wp6{width:6%;}
			.wp4{width:4%;}
			.wpx7{width:7px;}
			.wpx9{width:9px;}
			.wpx10{width:10px;}
			.wpx20{width:15px;}
			.wpx707{width:707px;}		 
			.wpx711{width:711px;}
			.wpx712{width:712px;}
			.wpx729{width:729px;}
			.wpx731{width:731px;}
			.wpx330{width:345px;}
			.wpx337{width:352px;}
			.wpx348{width:348px;}
			.wpx711{width:711px;}
			.wpx707{width:707px;}
			.wpx9{width:9px;}
			.wpx713{width:713px;}
			.wpx347{width:347px;}
			.wpx253{width:253px;}
			.wpx335{width:335px;}			
			.hp30{
				height:27px;
				border-top:1px solid #dddddd;
			}
			.hp22{height:22px;}
			.hp20{height:20px;}
			.hp4{height:4px;}
			.hp9{height:9px;}
			.hp8{height:8px;}
			.hp17{height:19px;}
			.borlr{
				border-left:1px solid #dddddd;
				border-right:1px solid #dddddd;
			}			
			.borl{
				border-left:1px solid #dddddd;
			}
			.borr{
				border-right:1px solid #dddddd;
			}
			.borb{
				border-bottom:1px solid #dddddd;
			}
			div.row {
				clear:both;
				float:left;
			}
			div.coloumn {
				float:left;
			}
			div.coloumn-right {
				float:right;
			}        
			.seccolor{background-color:#999999;}
			.bg2{background-color:#f4f4f4;}		
			div.clear{
				clear:both;
				background-color:#f4f4f4;
			}
			.header-text{font-size:11px; font-family:arial; font-weight:bold;}
	</style>        
	<div id=\"content\">
	<div id=\"content-main\">
	<div class=\"row wp100 al\" style=\"height:900px\">	 
		<div class=\"row al pt45\">
			<div><img src=\"".$variables['images']."/smeerp-inv-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\"></div>
		</div>
		<div class=\"row wpx731 al pb10 pt1\">
			<div class=\"coloumn al content wp65\">
				Motivated to Serve&nbsp;Determined to Excel <sup>TM</sup>
			</div>
			<div class=\"coloumn-right invoice-text ar\">";			
				
	$body.="</div>
		</div>";
	$body.= "<div class=\"row wp100 pt5\">
		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
		<tr>
			<td width=\"10px\" class=\"bg1\"><img src=\"".$variables['images']."/first-left-corner.jpg\" width=\"10px\" height=\"30px\" border=\"0\" /></td>		
			<td class=\"bg1 ar pr5\" width=\"150px\"><span class=\"content-heading b\">From</span></td>
			<td class=\"bg1 ar pr5\" width=\"100px\"><span class=\"content-heading b\">Date</span></td>
			<td class=\"bg1 al pr5 pl5\" width=\"300px\" ><span class=\"content-heading b\">Particulars</span></td>
			<td class=\"bg1 ar pr5\" width=\"110x\"><span class=\"content-heading b\">Score</span></td> 
			<td width=\"9px\" class=\"bg1\"><img src=\"".$variables['images']."/first-right-corner.jpg\" width=\"9px\" height=\"30px\" border=\"0\" /></td>
		</tr>";    
		$k=1;    
		
		foreach($data['particulars'] as $key=>$particular){
		 
			$body.="<tr>
			<td class=\"pb5 bg3 borl\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>
			<td class=\"bg3 pb5 content vt ar pr5\">".$particular['from_name']."</td>
			<td class=\"bg3 pb5 content vt ar pr5\">".$particular['score_date']."</td>
			<td class=\"bg3 pb5 content vt al pl10 pr5\">".nl2br($particular['particulars'])."</td>
			<td class=\"bg3 pb5 content vt ar pr5\">".$particular['score']."</td>
			";			 		
			$body.=" <td class=\"pb5 bg3 borr\">
				<img src=\"".$variables['images']."/spacer-img.jpg\" width=\"1px\" height=\"1px\"  border=\"0\" /></td>
			</tr>";
			$k++;	
		}
	
		$body.="<tr>
					<td width=\"10px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-left-corner.jpg\"  width=\"10px\" 
					height=\"9px\"  border=\"0\" /></td>";
					$body.="<td class=\"bg3 borb\" colspan=\"4\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>";
					$body.="<td width=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-right-corner.jpg\"  width=\"9px\" 
					height=\"9px\"  border=\"0\" /></td>
				</tr>
		</table>
		</div> 
		<div class=\"row wpx731 pb3 pt3\">&nbsp;
		</div>";		 
		$body.=" 
			<div class=\"row wpx731 bg9 pb3 pt3\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
					<tr>
						<td class=\"address-text al pl10\" width=\"393px\">
							<span class=\"white-text b\">SMEERP E-Technologies Pvt. Ltd.
						</td>
						<td class=\"address-text ar pr10\">
							<span class=\"white-text b\">www.smeerptech.com &nbsp; </span> 
						</td>
					</tr>
				</table>
			</div>
			<div class=\"row wp731 pt5 pb5 bg8\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
				<tr>
					<td class=\"address-text al pl10\" width=\"325px\">
						<span class=\"white-text\"><b>Worldwide Network :</b></span> 
						<span class=\"address-text-small\">INDIA, USA, UK, UAE, OMAN, AUSTRALIA <br/>
						MEXICO, CANADA, EUROPE, JAPAN, SINGAPORE, MALAYSIA</span> 
					</td>
					<td class=\"address-text ar pr10\" width=\"406px\">
						<span class=\"white-text\"><b>Corporate office:</b></span> 
						<span class=\"address-text-small\">
						17, MG street | Jhena 440 025 Maharashtra | India <br/></span> 
						<b>Dedicated Sales:</b> <span class=\"address-text-small\">
						98230-99998, 98230-88884, 98230-88885, 98230-99996, 98230-88889</span> 
					</td>
				</tr>
				</table>
			</div> 
		</div>
	  </div>
	</div>
	<div class=\"clear\"></div>" ;
	 
	//Terms 2 BOF
	//$invoiceArr['content'] = $body; 
	
	
	 $top_content = "<table width=\"100%\" border=\"0px\" cellspacing=\"0\" cellpadding=\"0\">";
	 $top_content .= "<tr>
					<td>
						<img src=".$variables['images']."/smeerp-inv-logo.jpg alt=\"SMEERP\" 
						title=\"SMEERP\" border=\"0\">
					</td>
				</tr>";
	 $top_content .= "<tr>
					<td valign=\"top\" style=\"font-family: arial,sans-serif; font-size: 11px; line-height: 15px; font-weight: bold; color: rgb(102, 102, 102);padding-bottom:10px;\" font-color=\"#4f4f4f;\">
						Motivated to Serve&nbsp;Determined to Excel <sup>TM</sup>
					</td>
				</tr>";	
		$footer_content .= "</table>";
		
		if(!empty($data['particulars'])){
			$data1 = "<tr><td>
			<table width=\"80%\"  border=\"1px\" cellspacing=\"0px\" cellpadding=\"5px\" 
			style=\"border:solid 1px;#cecece; \">";
			$data1 .= "<tr style=\"font-family: arial,sans-serif; font-size: 13px; line-height: 15px; font-weight: bold; color: rgb(102, 102, 102);\" font-color=\"#444444;\">
						<td >Sr</td>
						<td>Particulars</td>
						<td>Score</td>
						<td>Score Dt</td>
						<td>Score By</td>
					</tr>";
			$data2 ='';
			
			foreach($data['particulars'] as $key2=>$val2){
				$data2 .= "<tr style=\"font-family: arial,sans-serif; font-size: 13px; line-height: 15px; font-weight: normal; color: rgb(102, 102, 102);\" font-color=\"#444444;\">
								<td>".$val2['sr']."</td>
								<td>".$val2['particulars']."</td>
								<td>".$val2['score']."</td>
								<td>".$val2['score_date']."</td>
								<td>".$val2['from_name']."</td>
							</tr>";
					
				
					 
				 
			}
			$data1_clse .="</table></td></tr>";
			$full_data = $top_content.$data1.$data2.$data1_clse.$footer_content ; 
		}
	$table_content = "<tr>
					<td valign=\"top\" style=\"font-family: arial,sans-serif; font-size: 11px; line-height: 15px; font-weight: bold; color: rgb(102, 102, 102);\" font-color=\"#4f4f4f;\">
						
					</td>
				</tr>";
	 
		
		
		/* header("Content-Type: application/vnd.ms-word");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("content-disposition: attachment;filename=exported-list.doc");
		 
		$body1="<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=iso-8859-1\">";*/
		return $full_data  ;
		//echo $invoiceArr['content'] = $body; 
		//$invoiceArr['header'] = $invoiceArr['footer'] = '';
		//createScorePDF($data["number"],$invoiceArr);
		
	}
        
		
 }
	
	
function createScorePDF( $q_no, $data ) {
		require_once(DIR_FS_ADDONS .'/html2pdf/config.score.inc.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
		require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
		parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
	  
		global 	$g_config;
		
		class ReportData extends Fetcher {
			var $content;
			var $url;
		
			function get_data($data) {
				return new FetchedDataURL($this->content, array(), "","");
			}
		
			function ReportData($content) {
				$this->content = $content;
			}
			
			function get_base_url() {
				return "";
			}
		}
		
		$contents 	= '';
		$contents = $data['content'];      
		
		// Configuration Settings.
		$g_config = array(
							'process_mode' 	=> 'single',
							'mode'		    => '',
							'pixels'		=> '750',
							'scalepoints'	=> '1',
							'renderimages'	=> '1',
							'renderlinks'	=> '1',
							'renderfields'	=> '1',
							'media'			=> 'A4',
							'cssmedia'		=> 'screen',
							//'leftmargin'	=> '30',
							'leftmargin'	=> '5',
							//'rightmargin'	=> '15',
							'rightmargin'	=> '5',
							'topmargin'		=> '5',
							'bottommargin'	=> '5',
							'encoding'		=> 'iso-8859-1',
							//'encoding'	=> 'UTF-8',
							'headerhtml'	=> $data['header'],
							'boydhtml'		=> $contents,
							'footerhtml'	=> $data['footer'],
							'watermarkhtml'	=> '',
							'pslevel'		=> '3',
							'method'		=> 'fpdf',
							'pdfversion'	=> '1.3',
							'output'		=> '2',
							'convert'		=> 'Convert File'
						);
		
		$media = Media::predefined('A4');
		$media->set_landscape(false);
		/*
		$media->set_margins(array(	'left' => 10,
									'right' => 10,
									'top' => 15,
									'bottom' => 35)
							);
		$media->set_pixels(700);
		*/
		 $media->set_margins(array(	'left' => 5,
									'right' => 5,
									'top' => 5,
									'bottom' => 5)
							);
		$media->set_pixels(740);
		
		$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
		$g_pt_scale = $g_px_scale * 1.43;
		
		$pipeline 					= PipelineFactory::create_default_pipeline("","");
		$pipeline->pre_tree_filters[] = new PreTreeFilterHeaderFooter( $g_config['headerhtml'], 
		$g_config['footerhtml']);
		$pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
		if ($g_config['renderfields']) {
			$pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
		}

		// Changed so that script will try to fetch files via HTTP in this case) and to provide absolute 
		//paths to the image files.
		// http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
		//		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
		$pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
		//$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
		$pipeline->destination 			= new DestinationFile($q_no);
		 
		$pipeline->process($q_no, $media);
		$filename = DIR_FS_GN_FILES .'/'. $q_no .'.pdf'; 
        @chmod($filename, 0777);
		$contents = NULL;
		return true;
}

?>
