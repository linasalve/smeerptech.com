<?php
function output_file($file, $name, $mime_type=''){
		/*
		 This function takes a path to a file to output ($file),  the filename that the browser will see ($name) and  the MIME type of the file ($mime_type, optional).
		*/ 
		//Check the file premission
		if(!is_readable($file)) die('File not found or inaccessible!');
		 
		 $size = filesize($file);
		 $name = rawurldecode($name); 
		 /* Figure out the MIME type | Check in array */
		$known_mime_types=array(
			"pdf" => "application/pdf",
			"txt" => "text/plain",
			"html" => "text/html",
			"htm" => "text/html",
			"exe" => "application/octet-stream",
			"zip" => "application/zip",
			"doc" => "application/msword",
			"xls" => "application/vnd.ms-excel",
			"ppt" => "application/vnd.ms-powerpoint",
			"gif" => "image/gif",
			"png" => "image/png",
			"jpeg"=> "image/jpg",
			"jpg" =>  "image/jpg",
			"php" => "text/plain"
		 );
		 
		 if($mime_type==''){
			 $file_extension = strtolower(substr(strrchr($file,"."),1));
			 if(array_key_exists($file_extension, $known_mime_types)){
				$mime_type=$known_mime_types[$file_extension];
			 } else {
				$mime_type="application/force-download";
			 };
		 };
		 
		 //turn off output buffering to decrease cpu usage
		 @ob_end_clean(); 
		 
		 // required for IE, otherwise Content-Disposition may be ignored
		 if(ini_get('zlib.output_compression'))
		  ini_set('zlib.output_compression', 'Off');
		 
		 header('Content-Type: ' . $mime_type);
		 header('Content-Disposition: attachment; filename="'.$name.'"');
		 header("Content-Transfer-Encoding: binary");
		 header('Accept-Ranges: bytes');
		 
		 /* The three lines below basically make the 
			download non-cacheable */
		 header("Cache-control: private");
		 header('Pragma: private');
		 header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		 
		 // multipart-download and download resuming support
		 if(isset($_SERVER['HTTP_RANGE']))
		 {
			list($a, $range) = explode("=",$_SERVER['HTTP_RANGE'],2);
			list($range) = explode(",",$range,2);
			list($range, $range_end) = explode("-", $range);
			$range=intval($range);
			if(!$range_end) {
				$range_end=$size-1;
			} else {
				$range_end=intval($range_end);
			}
			/*
			------------------------------------------------------------------------------------------------------
			//This application is developed by www.webinfopedia.com
			//visit www.webinfopedia.com for PHP,Mysql,html5 and Designing tutorials for FREE!!!
			------------------------------------------------------------------------------------------------------
			*/
			$new_length = $range_end-$range+1;
			header("HTTP/1.1 206 Partial Content");
			header("Content-Length: $new_length");
			header("Content-Range: bytes $range-$range_end/$size");
		 } else {
			$new_length=$size;
			header("Content-Length: ".$size);
		 }
		 
		 /* Will output the file itself */
		 $chunksize = 1*(1024*1024); //you may want to change this
		 $bytes_send = 0;
		 if ($file = fopen($file, 'r'))
		 {
			if(isset($_SERVER['HTTP_RANGE']))
			fseek($file, $range);
		 
			while(!feof($file) && 
				(!connection_aborted()) && 
				($bytes_send<$new_length)
				  )
			{
				$buffer = fread($file, $chunksize);
				print($buffer); //echo($buffer); // can also possible
				flush();
				$bytes_send += strlen($buffer);
			}
		 fclose($file);
		 } else
		 //If no permissiion
		 die('Error - can not open file.');
		 //die
		 die();
	}

//XLS Write function BOF
function xlsEOF() {
	echo pack("ss", 0x0A, 0x00);
	return;
}
function xlsBOF() {
	echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
	return;
}
function xlsWriteNumber($Row, $Col, $Value) {
	echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
	echo pack("d", $Value);
	return;
}
function xlsWriteLabel($Row, $Col, $Value ) {
	$L = strlen($Value);
	echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
	echo $Value;
	return;
}
//XLS Write function EOF	
/* 
* fromDate, toDate formate yyyy-mm-dd hh:mm:ss
* dtFormate ie in which formate you want the date array
* if 1 = dd/mm/yyyy; 2 =yyyy-mm-dd
**/

function getDiffDates($fromDate,$toDate,$dtFormate=1){	
	//$fromDate = $toDate = yyyy-mm-dd hh:mm:ss
	$dateMonthYearArr = array();
	$fromDateTS = strtotime($fromDate);
	$toDateTS = strtotime($toDate);

	for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
	// use date() and $currentDateTS to format the dates in between
		if($dtFormate==1){
			$currentDateStr = date("d/m/Y",$currentDateTS);
		}else{
			$currentDateStr = date("Y-m-d",$currentDateTS);
		
		}
		
		$dateMonthYearArr[] = $currentDateStr;
	 
	}

	return $dateMonthYearArr;

}  
//$holidays=array("2008-12-25","2008-12-26","2009-01-01");
//echo getWorkingDays("2008-12-22","2009-01-02",$holidays);
function getWorkingDays($startDate,$endDate,$holidays){
    /*
	* The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    * We add one to include both dates in the interval.
	*/
	
    $days = (strtotime($endDate) - strtotime($startDate)) / 86400 + 1;
    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", strtotime($startDate));
    $the_last_day_of_week = date("N", strtotime($endDate));

    /*
	* The two can be equal in leap years when february has 29 days, the equal sign is added here
    * In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
	*/
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        // For 5 days week if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        /* 
		* (edit by Tokes to fix an edge case where the start day was a Sunday
        * and the end day was NOT a Saturday)
        * the day of the week for start is later than the day of the week for end
		*/
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            /* For 5 days week  
		    if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            } 
			*/
        }
        else {
            /*
			* the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            * so we skip an entire weekend and subtract 2 days
			*/
            //$no_remaining_days -= 2; For 5 days week
            $no_remaining_days -= 1;
        }
    }
	/*
    * The no. of business days is: (number of weeks between the two dates) * (5 working days) + the 
	* remainder february in none leap years gave a remainder of 0 but still calculated weekends 
	* between first and last day, 
	* this is one way to fix it
	*/
	
   //$workingDays = $no_full_weeks * 5; For 5 days week
   $workingDays = $no_full_weeks * 6;
    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
        $time_stamp=strtotime($holiday);
        //If the holiday doesn't fall in weekend
		/*  if (strtotime($startDate) <= $time_stamp && $time_stamp <= strtotime($endDate) && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
            $workingDays--; 
		*/
		if (strtotime($startDate) <= $time_stamp && $time_stamp <= strtotime($endDate) && date("N",$time_stamp) != 7)
            $workingDays--;
    }

    return $workingDays;
}



function getTotalDays($startDate,$endDate){
	$days = round((strtotime($endDate) - strtotime($startDate)) / 86400)+1;
	return  $days;
}
/* 
* fromDate, endDate, formate d M Y
* condDay ie ws = withSunday; wos=without Sunday
* returns array $dateArr['dates'],$dateArr['total_sundays']=count sunday;
**/
function getDateArray($startDate,$endDate,$formate,$condDay){
	$dates = array(); 
	$sunday=0;
	if(empty($formate)){
		$formate ='Y-m-d';
	}
	for($current = $startDate; $current != $endDate; $current = date('Y-m-d', strtotime("$current +1 day"))){ 
		$day = date('w',strtotime($current));
		if($day==0){
			$sunday++;
			if($condDay=='ws'){
				$dates[] =  date($formate, strtotime($current));
			}
		}else{
			$dates[] =  date($formate, strtotime($current));
		}
	}
	$dates[] =  date($formate, strtotime($endDate)); 
	$day = date('w',strtotime($endDate));
	if($day==0){
		$sunday++;
		if($condDay=='ws'){
			$dates[] =  date($formate, strtotime($current));
		}
	} 
	$dateArr['dates']=$dates ;
	$dateArr['total_sundays']= $sunday ;
	return  $dateArr;
}
 


   /********************************************************
	* function synchronizeTime($time_in_sec, $time_offset)
	*
	* This function is used to change the time in GMT to 
	* the local time.
	* $time_in_sec 
	*	is the time in seconds that is to be synchronized.
	* $time_offset 
	*	is the offset in seconds by which the time 
	* is to be changed. This is an optional parameter
	* and if not specified then the default value will 
	* be used that is specified in the config.php file
	********************************************************/
	function synchronizeTime($time_in_sec, $time_offset = 0) {
		global $local_setting;
        // Check if the passed value is integer or not
		
		if ( !preg_match("/^[0-9]/", $time_in_sec) ) {
			return ( false );
		}
		else {
			
			if ( !preg_match("/^[0-9]/", $time_offset) ) { // Done in two stages with purpose.
				
				return ( false );
			}
			else {
				if ( $time_in_sec <= 0 || $time_in_sec == "") {
					return ( 0 );
				}
				else {
					if ( $time_offset == 0 || $time_offset == "" ) {
						 $time_offset = $local_setting["time_offset"];
					}
					return ( $time_in_sec + $time_offset );
					
				}
			}
		}
	
	}
	
	function desynchronizeTime($time_in_sec, $time_offset = 0) {
        global $local_setting;
		// Check if the passed value is integer or not
		if ( !preg_match("/^[0-9]/",$time_in_sec) ) {
			return ( false );
		}
		else {
			if ( !preg_match("/^[0-9]/",$time_offset)) { // Done in two stages with purpose.				
				
				return ( false );
			}
			else {
				if ( $time_in_sec <= 0 || $time_in_sec == "") {
					return ( 0 );
				}
				else {
					if ( $time_offset == 0 || $time_offset == "" ) {
						$time_offset = $local_setting["time_offset"];
					}
					return ( $time_in_sec - $time_offset );
					
				}
			}
		}
	
	}

    /**
        * Function to combine two arrays, and the duplicate 
        * values are not repeated.
        *
        * @param   array   first one dimensional array.
        * @param   array   second one dimensional array.
        * @param   boolean if true, then array keys are also checked
        *                  if false, array keys are not checked only the values are checked.
        *
        * @return  array   the array containing the values from both the arrays.
        *  
    */
    function arrayCombine($array1, $array2, $check_keys=false) {
        $temp = array();
        
        if ( $check_keys ) {
            $temp = array_diff_assoc($array1, $array2);
            $temp = array_merge($temp, array_intersect_assoc($array2, $array1));
            $temp = array_merge($temp, array_diff_assoc($array2, $array1));
        }
        else {
            $temp = array_diff($array1, $array2);
            $temp = array_merge($temp, array_intersect($array2, $array1));
            $temp = array_merge($temp, array_diff($array2, $array1));
        }       
        return ($temp);
    }



    /**
     * This function is used to Parse the extra parameters 
     * that are transfered in between the URLs. This function
     * accepts the URL parameter and converts them into key=>value
     * pairs.
     *
     */
    function parseURL($url) {
        $param  = array();
        $temp   = array();
        $read   = false;
        $temp2  = explode('&', $url);
        
        if ( count($temp2)>0 && !empty($url) ) {
            foreach ($temp2 as $key=>$value) {
                $param = explode('=', $value);
                $temp[$param[0]] = $param[1];
            }
            $param  = array();
            if ( is_array($temp) && count($temp)>0 ) {
                foreach ($temp as $key=>$value) {
                
                    if ( !empty($key) && $key == 'start' && $value == 'url') {
                        $read  = true;
                    }
                    elseif ( !empty($key) && $key == 'end' && $value == 'url') {
                        $read  = false;
                    }
                    elseif ( $read ) {
                        $param[$key] = $value;
                    }
                }
            }
        }
        $temp   = NULL;
        $temp2  = NULL;
        $read   = NULL;
        return ($param);
    }


        /******************************************************
        * Check the validity of the email
         * VALID   : pradeep.agnihotri@pretechno.com,
         *           pradeep_agnihotri@rent.pretechno.com
         *           pradeepagnihotri@properties.pretechno.co.in
         *
         * INVALID : pradeep.agnihotri#pretechno.com
         *           pradeep.agnihotri@pretechno
         *
         * @param $email
         *   The email address that is to be checked for
         *   validity.
         * @return boolean
         *   If the email address is valid then return value
         *   is true else false.
         ******************************************************/
        function isValidEmail( $email ) {
            $pattern = "/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/";
            
            if (preg_match($pattern , $email)) {
                return (true);
            }
            else {
                return (false);
            }
        }

    /**
     * Function to remove the special characters from the table 
     * column names, inserted to distinguish between the coloumn 
     * of different tables with same name.
    */
    function clearSearchString($string, $char="#") {
        if ( ($pos = strpos($string, $char)) > -1 ) {
            $string = substr($string, 0, $pos);
        }
        return ($string);
    }

	// Function to Read the Menus.
    function getAllMenus(&$db, &$menu_list, $placement='', $menu_id=0, $level=0, $is_logged='', $site_id='') {
        include_once ( DIR_FS_INCLUDES .'/menu-settings.inc.php' );
            
        $result = MenuSetting::getMenus($db, $menu_list, $placement, $menu_id, $level, $is_logged, $site_id);
        
        return ( $result );
    }


    function getJSMenu(&$js_html, $menu_arr) {
        if ( !empty($menu_arr) ) {
            foreach ( $menu_arr as $key=>$menu ) {
                if ($key > 0 ){
                    $js_html .= '_cmSplit,';
                }
                if ( !empty($menu["sub_menu"]) ) {
                    $js_html .= '["", "'. $menu["title"] .'", "'. $menu["url"] .'", "'. $menu["target"] .'", "",';
                    getJSMenu( $js_html, $menu["sub_menu"] );
                    $js_html .= '],';
                }
                else {
                    $js_html .= '["", "'. $menu["title"] .'", "'. $menu["url"] .'", "'. $menu["target"] .'", ""],';
                }
            }
        }
        return(true);
    }

	/**
	 * This funciton searches for the specified value with a specific index in the array.
	 * This function works on multidimentional array and return an array of indexes that had been 
	 * traversed in a successful search from Top to Bottom.
	 *
	 */
	function array_key_search($needle_key, $needle_value, $array, &$index, $level=-1) {
		
		if ( is_array($array) && count($array)>0 ) {
			foreach ( $array as $key=>$value ) {
				array_push($index, $key);
				
				if ( $key == $needle_key && $value == $needle_value) {
					return (true);
				}
				if ( is_array($value) ) {
					if ( array_key_search($needle_key, $needle_value, $value, $index, $level+1) ) {
						return (true);
					}
				}
				array_pop($index);
			}
		}
		return (false);
	}
	

	function array_traverse(&$dest, $array, $level=0) {
		if ( is_array($array) && count($array)>0 ) {
			foreach ( $array as $key=>$value ) {
				$i = count($dest);
	//			$dest[] = str_repeat('>> ', $level). $value['title'];
	
				$dest[$i]['id'] 	= $value['id'];
				$dest[$i]['title'] 	= str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level). '&raquo;&nbsp;'. $value['title'];
	
				if ( is_array($value['sub_menu']) ) {
					array_traverse($dest, $value['sub_menu'], $level+1);
				}
			}
		}		
		return (true);
	}
	

	function getPageTitle(&$db, $page_id) {
		include_once (DIR_FS_INCLUDES .'/staticpages.inc.php');
		
		$page = NULL;
		Staticpages::getList($db, $page, 'heading', " WHERE page_id = '$page_id' ");
		
		if ( empty($page[0]['heading']) ) {
			$page = NULL;
			StaticPages::getList($db, $page, 'heading', " WHERE page_name = '$page_id' ");
		}
		
		return ($page[0]['heading']);
	}
	
	
	function getPageName(&$db, $page_id) {
		include_once (DIR_FS_INCLUDES .'/staticpages.inc.php');
		
		$page = NULL;
		Staticpages::getList($db, $page, 'page_name', " WHERE page_id = '$page_id' ");
		
		if ( empty($page[0]['page_name']) ) {
			$page = NULL;
			Staticpages::getList($db, $page, 'page_name', " WHERE page_name = '$page_id' ");
		}
		
		return ($page[0]['page_name']);
	}


    /**
	 * Function to retrieve the ID of the page, when the Page name is known.
	 *
	 */
	function getPageId(&$db, $page_name) {
		include_once (DIR_FS_INCLUDES .'/staticpages.inc.php');
		
		$page = NULL;
		
		if ( Staticpages::getList($db, $page, 'page_id', " WHERE page_name = '$page_name' AND page_status = '1' ") ) {
			return ($page[0]['page_id']);
		}
		
		return ('');
    }
	
	
	/**
	 * This function is used to retrieve to list of ecraft pages 
	 * belonging to the websites whose ID is present in the variable 
	 * $site_id.
	 *
	 */
	function getSitePages(&$db, &$page_list, $site_id) {
		include_once (DIR_FS_INCLUDES .'/staticpages.inc.php');
		
		if ( is_array($site_id) ) {
			$site_id = "'". implode("','", $site_id) ."'";
		}
		elseif ( strpos($site_id, ',')> -1 ) {
			$site_id = str_replace(' ', '', $site_id);
			$site_id = "'". str_replace(',', "','", $site_id) ."'";
		}
		else {
			$site_id = "'". $site_id ."'";
		}
		$condition_query = " WHERE site_id IN ($site_id) "
							." AND page_status = '1' ORDER BY page_name ASC ";
		
		if ( Staticpages::getList($db, $page_list, 'page_id, page_name', $condition_query) > 0 ) {
			return (true);
		}
		
		return (false);
	}
	

	/**
	 * Function to retrieve the Tickers for the current page.
	 */
    function getTickers(&$db, &$ticker, $placement, $page_id='') {
        if ( empty($placement) || !in_array($placement, array('LEFT', 'RIGHT', 'TOP', 'BOTTOM')) ) {
            return (false);
        }
        //$now = date("Y-m-d H:i:s", time());
        $now = date("Y-m-d H:i:s", time());
        
        $query = "SELECT type, text, url FROM ". TABLE_TICKERS
                    ." WHERE ( (expiry_date >= '". $now ."' OR expiry_date = '0000-00-00 00:00:00') "
                            ." AND activation_date <= '". $now ."' "
                        .")"
                    ." AND placement = '$placement' "
                    ." AND ( visible_on_page = '' "
                            ." OR ( visible_on_page REGEXP '^". $page_id ."$' "
                                ." OR visible_on_page REGEXP ',". $page_id ."$' "
                                ." OR visible_on_page REGEXP ',". $page_id .",' "
                                ." OR visible_on_page REGEXP '^". $page_id .",' )"
                            .") "
                    ." AND status = '1' "
                ." ORDER BY ticker_order " ;
        //echo "<br/>$query";
        if ( $db->query($query) && $db->nf()>0 ) {
            while ( $db->next_record() ) {
                $data = processSQLData($db->result());
                if ( $data['type'] == 'S' ) {
                    $data['url'] = sprintf(URL_S, $data['url'].'.html');
                    $data['target'] = '_blank';
                }
                elseif ( $data['type'] == 'D' ) {
                    $data['url'] = sprintf(URL_D, $data['url']);
                    $data['target'] = '_blank';
                }
                elseif ( $data['type'] == 'E' ) {
                    $data['url'] = sprintf(URL_E, $data['url']);
                    $data['target'] = '_blank';
                }
                elseif ( $data['type'] == 'P' ) {
                    $data['url'] = 'javascript:void(0);';
                    $data['target'] = '';
                }
                $ticker[]       = $data;
            }
            return (true);
        }
        return (false);
    }


	/**
	 * Function to retrieve all the website.
	 *
	 */
	function getAllSites(&$db, &$list) {
		include_once (DIR_FS_INCLUDES .'/sites.inc.php');
		
		if ( Sites::getList($db, $list, 'site_id, site_domain, site_status', ' ORDER BY site_name ASC') ) {
			return true;
		}
		return false;		
	}

	/**
	 * Function to retrieve all the website.
	 *
	 */
	function getStaticPages(&$db, &$list, $site_id) {
		include_once (DIR_FS_INCLUDES .'/staticpages.inc.php');
		
		if ( Staticpages::getList($db, $list, 'page_id, page_name, page_status', " WHERE site_id = '$site_id' ORDER BY page_name") ) {
			return true;
		}
		return false;		
	}
	
	

	/**
	 * Function to retrieve the menu links for the current user
	 *
	 *
	 */
	function getMenuLinks( &$db, $user_id, &$menu ) {
		
		// Retrieve the pages that the current user can access
		// getUserPerms( &$db, $user_id, &$pages);
		
		$curr_page = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'],"/")+1);
		if ( !empty($_SERVER['QUERY_STRING']) ) {
			$curr_page .= "?". $_SERVER['QUERY_STRING'];
		}
		//echo ($curr_page);
		
		$pages = array(
					array(	"title" => "Home", 
							"name" 	=> "index.php", 
							"link" 	=> DIR_WS_NC ."/index.php",
							"selected" => ($curr_page == "index.php")? 1:0,
							"sub" 	=> ""
						),
					array(	"title" => "Servers", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("server-list.php", "server-list.php?show=add")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "List", 
													"name" 	=> "server-list.php", 
													//"link" 	=> DIR_WS_NC ."/server-list.php",
													"link" 	=> DIR_WS_NC ."/server-list.php",
													"selected" => ($curr_page == "server-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add New", 
													"name" 	=> "server-add.php", 
													//"link" 	=> DIR_WS_NC ."/server-add.php",
													"link" 	=> DIR_WS_NC ."/server-list.php?show=add",
													"selected" => ($curr_page == "server-list.php?show=add")? 1:0,
													"sub" 	=> ""
												)
											)
						),
					array(	"title" => "Websites", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("websites-list.php", "websites-list.php?show=add", "websites-pages.php")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "List", 
													"name" 	=> "websites-list.php", 
													//"link" 	=> DIR_WS_NC ."/websites-list.php",
													"link" 	=> DIR_WS_NC ."/websites-list.php",
													"selected" => ($curr_page == "websites-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add New", 
													"name" 	=> "websites-add.php", 
													//"link" 	=> DIR_WS_NC ."/websites-add.php",
													"link" 	=> DIR_WS_NC ."/websites-list.php?show=add",
													"selected" => ($curr_page == "websites-list.php?show=add")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Web Page list", 
													"name" 	=> "websites-pages.php", 
													//"link" 	=> DIR_WS_NC ."/websites-pages.php",
													"link" 	=> DIR_WS_NC ."/websites-pages.php",
													"selected" => ($curr_page == "websites-pages.php")? 1:0,
													"sub" 	=> ""
												),
											)
						),
					array(	"title" => "Ticker", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("ticker-list.php", "ticker-list.php?action=show&show=add_form")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "List", 
													"name" 	=> "ticker-list.php", 
													//"link" 	=> DIR_WS_NC ."/ticker-list.php",
													"link" 	=> DIR_WS_NC ."/ticker-list.php",
													"selected" => ($curr_page == "ticker-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add New", 
													"name" 	=> "ticker-add.php", 
													//"link" 	=> DIR_WS_NC ."/ticker-add.php",
													"link" 	=> DIR_WS_NC ."/ticker-list.php?action=show&show=add_form",
													"selected" => ($curr_page == "ticker-list.php?action=show&show=add_form")? 1:0,
													"sub" 	=> ""
												)
											)
						),
					array(	"title" => "Gallery", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("gallery-list.php", "gallery-category-list.php")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "Pictures", 
													"name" 	=> "gallery-list.php", 
													//"link" 	=> DIR_WS_NC ."/gallery-list.php",
													"link" 	=> DIR_WS_NC ."/gallery-list.php",
													"selected" => ($curr_page == "gallery-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Categories", 
													"name" 	=> "gallery-category-list.php", 
													//"link" 	=> DIR_WS_NC ."/gallery-category-list.php",
													"link" 	=> DIR_WS_NC ."/gallery-category-list.php",
													"selected" => ($curr_page == "gallery-category-list.php")? 1:0,
													"sub" 	=> ""
												)
											)
						),
					array(	"title" => "Members", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("user-list.php", "user-list.php?show=add", "user-edit.php", "user-details.php", "user-contact-list.php", "prospective-client-list.php", "prospective-client-list.php?show=add", "prospective-client-edit.php", "prospective-client-details.php", "rival-client-list.php", "rival-client-list.php?show=add", "rival-client-edit.php", "rival-client-details.php", "newsletter-email-list.php", "newsletter-email-list.php?show=add", "newsletter-email-edit.php", "newsletter-email-details.php")))? 1:0,
							"sub" 	=> array(	
											array(	"title" => "Users", 
													"name" 	=> "user-list.php", 
													//"link" 	=> DIR_WS_NC ."/user-list.php",
													"link" 	=> DIR_WS_NC ."/user-list.php",
													"selected" => ($curr_page == "user-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add User", 
													"name" 	=> "user-add.php", 
													//"link" 	=> DIR_WS_NC ."/user-add.php",
													"link" 	=> DIR_WS_NC ."/user-list.php?show=add",
													"selected" => ($curr_page == "user-list.php?show=add")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "User Contact List", 
													"name" 	=> "user-contact-list.php", 
													//"link" 	=> DIR_WS_NC ."/user-contact-list.php",
													"link" 	=> DIR_WS_NC ."/user-contact-list.php",
													"selected" => ($curr_page == "user-contact-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Prospective Clients", 
													"name" 	=> "prospective-client-list.php", 
													//"link" 	=> DIR_WS_NC ."/prospective-client-list.php",
													"link" 	=> DIR_WS_NC ."/prospective-client-list.php",
													"selected" => ($curr_page == "prospective-client-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add Prospective Client", 
													"name" 	=> "prospective-client-add.php", 
													//"link" 	=> DIR_WS_NC ."/prospective-client-add.php",
													"link" 	=> DIR_WS_NC ."/prospective-client-list.php?show=add",
													"selected" => ($curr_page == "prospective-client-list.php?show=add")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Rival Clients", 
													"name" 	=> "rival-client-list.php", 
													//"link" 	=> DIR_WS_NC ."/rival-client-list.php",
													"link" 	=> DIR_WS_NC ."/rival-client-list.php",
													"selected" => ($curr_page == "rival-client-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add Rival Client", 
													"name" 	=> "rival-client-add.php", 
													//"link" 	=> DIR_WS_NC ."/rival-client-add.php",
													"link" 	=> DIR_WS_NC ."/rival-client-list.php?show=add",
													"selected" => ($curr_page == "rival-client-list.php?show=add")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Jobs Newsletter Emails", 
													"name" 	=> "newsletter-email-list.php", 
													//"link" 	=> DIR_WS_NC ."/newsletter-email-list.php",
													"link" 	=> DIR_WS_NC ."/newsletter-email-list.php",
													"selected" => ($curr_page == "newsletter-email-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add Email", 
													"name" 	=> "newsletter-email-add.php", 
													//"link" 	=> DIR_WS_NC ."/newsletter-email-add.php",
													"link" 	=> DIR_WS_NC ."/newsletter-email-list.php?show=add",
													"selected" => ($curr_page == "newsletter-email-list.php?show=add")? 1:0,
													"sub" 	=> ""
												)
											)
						),
					array(	"title" => "Support Tickets", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("support-user-list.php", "support-user-list.php?show=add", "support-ticket-list.php", "support-ticket-add.php?show=form", "support-ticket-view.php", "support-tickets-all.php", "support-ticket-status-list.php", "support-ticket-priority-list.php", "support-ticket-department-list.php")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "Users", 
													"name" 	=> "support-user-list.php", 
													//"link" 	=> DIR_WS_NC ."/support-user-list.php",
													"link" 	=> DIR_WS_NC ."/support-user-list.php",
													"selected" => ($curr_page == "support-user-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add User", 
													"name" 	=> "support-user-add.php", 
													//"link" 	=> DIR_WS_NC ."/support-user-add.php",
													"link" 	=> DIR_WS_NC ."/support-user-list.php?show=add",
													"selected" => ($curr_page == "support-user-list.php?show=add")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Tickets", 
													"name" 	=> "support-ticket-list.php", 
													//"link" 	=> DIR_WS_NC ."/support-ticket-list.php",
													"link" 	=> DIR_WS_NC ."/support-ticket-list.php",
													"selected" => ($curr_page == "support-ticket-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Write Ticket", 
													"name" 	=> "support-ticket-add.php", 
													//"link" 	=> DIR_WS_NC ."/support-ticket-add.php",
													"link" 	=> DIR_WS_NC ."/support-ticket-add.php?show=form",
													"selected" => ($curr_page == "support-ticket-add.php?show=form")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "View Ticket", 
													"name" 	=> "support-ticket-view.php", 
													//"link" 	=> DIR_WS_NC ."/support-ticket-view.php",
													"link" 	=> DIR_WS_NC ."/support-ticket-view.php",
													"selected" => ($curr_page == "support-ticket-view.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Write Ticket for All", 
													"name" 	=> "support-tickets-all.php", 
													//"link" 	=> DIR_WS_NC ."/support-tickets-all.php",
													"link" 	=> DIR_WS_NC ."/support-tickets-all.php",
													"selected" => ($curr_page == "support-tickets-all.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Status", 
													"name" 	=> "support-ticket-status-list.php", 
													//"link" 	=> DIR_WS_NC ."/support-ticket-status-list.php",
													"link" 	=> DIR_WS_NC ."/support-ticket-status-list.php",
													"selected" => ($curr_page == "support-ticket-status-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Priority", 
													"name" 	=> "support-ticket-priority-list.php", 
													//"link" 	=> DIR_WS_NC ."/support-ticket-priority-list.php",
													"link" 	=> DIR_WS_NC ."/support-ticket-priority-list.php",
													"selected" => ($curr_page == "support-ticket-priority-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Departments", 
													"name" 	=> "support-ticket-department-list.php", 
													//"link" 	=> DIR_WS_NC ."/support-ticket-department-list.php",
													"link" 	=> DIR_WS_NC ."/support-ticket-department-list.php",
													"selected" => ($curr_page == "support-ticket-department-list.php")? 1:0,
													"sub" 	=> ""
												)
											)
						),
					array(	"title" => "Billing", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("billing-user.php", "billing-order-list.php", "billing-order-list.php?show=add", "billing-invoice-list.php", "billing-invoice-list.php?show=add", "billing-receipt-list.php", "billing-receipt-list.php?show=add", "billing-other-list.php", "billing-other-list.php?show=add&misc_cat=Credit", "billing-other-list.php?show=add&misc_cat=Debit", "billing-other-list.php?show=add&misc_cat=Referral", "billing-account-detail.php")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "Users", 
													"name" 	=> "billing-user.php", 
													//"link" 	=> DIR_WS_NC ."/billing-user-list.php",
													"link" 	=> DIR_WS_NC ."/billing-user.php",
													"selected" => ($curr_page == "billing-user.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add User", 
													"name" 	=> "billing-user.php", 
													//"link" 	=> DIR_WS_NC ."/billing-user-add.php",
													"link" 	=> DIR_WS_NC ."/billing-user.php",
													"selected" => ($curr_page == "billing-user.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Orders", 
													"name" 	=> "billing-order-list.php", 
													//"link" 	=> DIR_WS_NC ."/billing-order-list.php",
													"link" 	=> DIR_WS_NC ."/billing-order-list.php",
													"selected" => ($curr_page == "billing-order-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Create Order", 
													"name" 	=> "billing-order-add.php", 
													//"link" 	=> DIR_WS_NC ."/billing-order-add.php",
													"link" 	=> DIR_WS_NC ."/billing-order-list.php?show=add",
													"selected" => ($curr_page == "billing-order-list.php?show=add")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Invoices", 
													"name" 	=> "billing-invoice-list.php", 
													//"link" 	=> DIR_WS_NC ."/billing-invoice-list.php",
													"link" 	=> DIR_WS_NC ."/billing-invoice-list.php",
													"selected" => ($curr_page == "billing-invoice-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "New Invoice ", 
													"name" 	=> "billing-invoice-add.php", 
													//"link" 	=> DIR_WS_NC ."/billing-invoice-add.php",
													"link" 	=> DIR_WS_NC ."/billing-invoice-list.php?show=add",
													"selected" => ($curr_page == "billing-invoice-list.php?show=add")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Receipt", 
													"name" 	=> "billing-receipt-list.php", 
													//"link" 	=> DIR_WS_NC ."/billing-receipt-list.php",
													"link" 	=> DIR_WS_NC ."/billing-receipt-list.php",
													"selected" => ($curr_page == "billing-receipt-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "New Receipt", 
													"name" 	=> "billing-receipt-add.php", 
													//"link" 	=> DIR_WS_NC ."/billing-receipt-add.php",
													"link" 	=> DIR_WS_NC ."/billing-receipt-list.php?show=add",
													"selected" => ($curr_page == "billing-receipt-list.php?show=add")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Other", 
													"name" 	=> "billing-other-list.php", 
													//"link" 	=> DIR_WS_NC ."/billing-other-list.php",
													"link" 	=> DIR_WS_NC ."/billing-other-list.php",
													"selected" => ($curr_page == "billing-other-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Credit", 
													"name" 	=> "billing-other-credit.php", 
													//"link" 	=> DIR_WS_NC ."/billing-other-credit.php",
													"link" 	=> DIR_WS_NC ."/billing-other-list.php?show=add&misc_cat=Credit",
													"selected" => ($curr_page == "billing-other-list.php?show=add&misc_cat=Credit")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Debit", 
													"name" 	=> "billing-other-debit.php", 
													//"link" 	=> DIR_WS_NC ."/billing-other-debit.php",
													"link" 	=> DIR_WS_NC ."/billing-other-list.php?show=add&misc_cat=Debit",
													"selected" => ($curr_page == "billing-other-list.php?show=add&misc_cat=Debit")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Referral", 
													"name" 	=> "billing-other-referral.php", 
													//"link" 	=> DIR_WS_NC ."/billing-other-referral.php",
													"link" 	=> DIR_WS_NC ."/billing-other-list.php?show=add&misc_cat=Referral",
													"selected" => ($curr_page == "billing-other-list.php?show=add&misc_cat=Referral")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Account Details", 
													"name" 	=> "billing-account-detail.php", 
													//"link" 	=> DIR_WS_NC ."/billing-account-detail.php",
													"link" 	=> DIR_WS_NC ."/billing-account-detail.php",
													"selected" => ($curr_page == "billing-account-detail.php")? 1:0,
													"sub" 	=> ""
												)
											)
						),
					array(	"title" => "Newsletter", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("newsletter-compose.php", "newsletter-list.php", "newsletter-smtp-list.php")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "Compose", 
													"name" 	=> "newsletter-compose.php",
													"link" 	=> DIR_WS_NC ."/newsletter-compose.php",
													"selected" => ($curr_page == "newsletter-compose.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Saved Newsletters", 
													"name" 	=> "newsletter-list.php", 
													"link" 	=> DIR_WS_NC ."/newsletter-list.php",
													"selected" => ($curr_page == "newsletter-list.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "SMTP Accounts", 
													"name" 	=> "newsletter-smtp-list.php",
													"link" 	=> DIR_WS_NC ."/newsletter-smtp-list.php",
													"selected" => ($curr_page == "newsletter-smtp-list.php")? 1:0,
													"sub" 	=> ""
												)
											)
						),
					array(	"title" => "Tools & Settings", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("settings-email-template.php", "settings-email-template-edit.php", "query.php", "settings-global.php", "settings-content.php")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "Email Templates", 
													"name" 	=> "settings-email-template.php",
													"link" 	=> DIR_WS_NC ."/settings-email-template.php",
													"selected" => ($curr_page == "settings-email-template.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Add Email Template", 
													"name" 	=> "settings-email-template-edit.php", 
													"link" 	=> DIR_WS_NC ."/settings-email-template-edit.php",
													"selected" => ($curr_page == "settings-email-template-edit.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Database Query", 
													"name" 	=> "query.php",
													"link" 	=> DIR_WS_NC ."/query.php",
													"selected" => ($curr_page == "query.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Global Variables", 
													"name" 	=> "settings-global.php",
													"link" 	=> DIR_WS_NC ."/settings-global.php",
													"selected" => ($curr_page == "settings-global.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Global Variables", 
													"name" 	=> "settings-content.php",
													"link" 	=> DIR_WS_NC ."/settings-content.php",
													"selected" => ($curr_page == "settings-content.php")? 1:0,
													"sub" 	=> ""
												)
											)
								
						),
					array(	"title" => "My Profile", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("myDetails.php", "myEdit.php", "myPassword.php")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "My Profile", 
													"name" 	=> "myDetails.php",
													"link" 	=> DIR_WS_NC ."/myDetails.php",
													"selected" => ($curr_page == "myDetails.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Edit", 
													"name" 	=> "myEdit.php",
													"link" 	=> DIR_WS_NC ."/myEdit.php",
													"selected" => ($curr_page == "myEdit.php")? 1:0,
													"sub" 	=> ""
												),
											array(	"title" => "Change Password", 
													"name" 	=> "myPassword.php",
													"link" 	=> DIR_WS_NC ."/myPassword.php",
													"selected" => ($curr_page == "myPassword.php")? 1:0,
													"sub" 	=> ""
												)
											)
						),
					array(	"title" => "Statistics & Reports", 
							"name" 	=> "", 
							"link" 	=> "",
							"selected" => (in_array($curr_page, array("ebilling-details-send.php")))? 1:0,
							"sub" 	=> array(
											array(	"title" => "Billing", 
													"name" 	=> "ebilling-details-send.php",
													"link" 	=> DIR_WS_NC ."/ebilling-details-send.php",
													"selected" => ($curr_page == "ebilling-details-send.php")? 1:0,
													"sub" 	=> ""
												)
											)
						)
				);
		$menu = $pages;
	}
	
    function encParam( $value, $encryption=ENCRYPTION ) {
        $return_value = $value;
        
        switch( $encryption )
        {
            // Use MD5 encryptor function
            case "MD5":
                $return_value   = md5( $value ) ;
                break;
            // Use SHA1 encryptor function
            case "SHA1":
                $return_value   = sha1( $value ) ;
                break;
            case "PLAIN":
                $return_value   = $value ;
                break;
            // Use MD5 encryptor function
            default :
                $return_value   = md5( $value ) ;
                break;
        }
        return $return_value;
    }

	/**
	 * Function to remove the slashes from data (i.e. data coming from database)
	 * @parameters
	 * $data 		@var 	array
	 * $CODE_HTML	@var	int		0 - No change in the data
	 *								1 - Encode the HTML entities in the data.
	 *								2 - Decode the HTML entities in the data.
	 */
	function processSqlData($data, $CODE_HTML='0', $QUOTES=ENT_NOQUOTES) {
		$cleared = NULL ;

		if (is_array($data)) {
			foreach ($data as $key=>$value) {
				$cleared[$key] = processSqlData($value, $CODE_HTML, $QUOTES) ;
			}
		}
		else {
			$cleared = trim($data);

			if ($CODE_HTML == '2')
				$cleared = stripslashes(html_entity_decode($cleared, $QUOTES) );
			elseif ($CODE_HTML == '1')
				$cleared = stripslashes(htmlentities($cleared, $QUOTES) );
			else
				$cleared = stripslashes( $cleared );
		}
		return $cleared ;
	}

	/**
	 * Function to add the slashes to data (i.e. data coming from User)
	 * @parameters
	 * $data 		@var 	array	
	 * $CODE_HTML	@var	int		0 - No change in the data
	 *								1 - Encode the HTML entities in the data.
	 *								2 - Decode the HTML entities in the data.
	 */
	function processUserData($data, $CODE_HTML=0, $QUOTES=ENT_NOQUOTES) {
		$processed = NULL ;
		
		if (is_array($data)) {
			foreach ($data as $key=>$value) {
				$processed[$key] = processUserData($value, $CODE_HTML, $QUOTES) ;
			}
		}
		else {
			$processed = trim($data);
			
			if ($CODE_HTML == 2)
				$processed = html_entity_decode($processed, $QUOTES);
			elseif ($CODE_HTML == 1)
				$processed = htmlentities(processed, $QUOTES);
			else
				//$processed = stripslashes( $processed );

			if ( !get_magic_quotes_gpc() )
				$processed = addslashes($processed);
		}
		return $processed ;
	}
	
	
	/*
	* This function is used to retrieve the list of 
	* available languages from the database.
	*/
	function getLanguages(&$db, &$lang) {
/*		
		$query = "SELECT "	. TABLE_LANGUAGES .".id"
						.",". TABLE_LANGUAGES .".name"
						.",". TABLE_LANGUAGES .".code"
					." FROM ". TABLE_LANGUAGES
					." WHERE ". TABLE_LANGUAGES .".status = '1' "
					." ORDER BY ". TABLE_LANGUAGES .".order ASC ";
		$db->query( $query );
		$lang = NULL;
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {
				$data = processSQLData( $db->Record );
				$lang[] = array("id" 	=> $data["id"], 
								"name" 	=> $data["name"],
								"code"	=> $data["code"]
							);
				$data = "";
			}
			return (true);
		}
		else {
			return (false);
		}
*/
		$lang[] = array("id" => 1, "name" => "English", "code" => "en_GB");
		return (true);
	}
	

	function SendMail($to, $from, $replyto, $subject, $message, $html = false, $response_flag = false, $cc="", $bcc="", $attch_file=""){
		global $sendmethod, $sockethost, $smtpauth, $smtpauthuser, $smtpauthpass, $socketfrom, $socketfromname, $socketreply, $socketreplyname, $smtpport;

		include_once (THIS_PATH .'/lib/class/class.phpmailer.php');

		$mail  = new phpmailer();

		if (file_exists(THIS_PATH .'/lib/class/language/phpmailer.lang-en.php')){
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		else{
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		if (isset($sendmethod) && $sendmethod == 'sendmail'){
			$mail -> IsSendmail();
		}
		elseif (isset($sendmethod) && $sendmethod == 'smtp'){
			$mail -> IsSMTP();
		}
		elseif (isset($sendmethod) && $sendmethod == 'mail'){
			$mail -> IsMail();
		}
		elseif (isset($sendmethod) && $sendmethod == 'qmail'){
			$mail -> IsQmail();
		}

		$mail -> Host 	= $sockethost;
		$mail -> Port	= $smtpport;

		if($smtpauth == 'TRUE'){
			$mail -> SMTPAuth = true;
			$mail -> Username = $smtpauthuser;
			$mail -> Password = $smtpauthpass;
		}
		
		if (is_array($to) && $to[0]["email"] <>""){
			foreach($to as $key=>$recipient){
				$mail -> AddAddress($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($to) && $to["email"] <>""){
			$mail -> AddAddress($to["email"], $to["name"]);
		}
		
		/*	If set send a copy to the sender	*/
		if ($response_flag){
			if(is_array($from) && $from["email"] <>""){
				$mail->addBcc($from["email"], $from["name"]);
			}
			else{
				$mail->addBcc($socketfrom, $socketfromname);
			}
		}
		
		
		if(is_array($cc) && $cc[0]["email"] <>""){
			foreach($cc as $key=>$recipient){
				$mail -> AddCC($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($cc) && $cc["email"] <>""){
			$mail -> AddCC($cc["email"], $cc["name"]);
		}
		
		if(is_array($bcc) && $bcc[0]["email"] <>""){
			foreach($bcc as $key=>$recipient){
				$mail->addBcc($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($bcc) && $bcc["email"] <>""){
			$mail->addBcc($bcc["email"], $bcc["name"]);
		}
	

		if(is_array($from) && $from["email"] <>""){
			$mail -> From     = $from["email"];
			$mail -> FromName = $from["name"];
		}
		else{
			$mail -> From     = $socketfrom ;
			$mail -> FromName = $socketfromname ;
		}	
		
		if(is_array($replyto) && $replyto["email"] <>""){
			$mail -> AddReplyTo($replyto["email"], $replyto["name"]);
		}

		if(is_array($attch_file)){
			foreach($attch_file as $key=>$val){
				if(!empty($val)){
					$mail -> AddAttachment( $val );	
				}
			}
		}else{
			if ( $attch_file != "" ) {
				$mail -> AddAttachment( $attch_file );		
			}
		}

		/*
		if ( $attch_file != "" ) {
			$mail -> AddAttachment( $attch_file );
		}*/

		$mail -> IsHTML($html);
		$mail -> Body    = $message;
		$mail -> Subject = $subject;
		//$mail -> SMTPDebug = true;

		if(!$mail -> Send()){
			return ('Error: '.$mail -> ErrorInfo);
		}
		else{
			return ('Email Sent. '.$mail -> ErrorInfo);
		}

		$mail -> ClearAddresses();
	}
	
	function SendMailAccounts($to, $from, $replyto, $subject, $message, $html = false, $response_flag = false, $cc="", $bcc="", $attch_file=""){
		global $sendmethodAcct, $sockethostAcct, $smtpauthAcct, $smtpauthuserAcct, $smtpauthpassAcct, $socketfromAcct, $socketfromnameAcct, $smtpportAcct;

		include_once (THIS_PATH .'/lib/class/class.phpmailer.php');

		$mail  = new phpmailer();

		if (file_exists(THIS_PATH .'/lib/class/language/phpmailer.lang-en.php')){
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		else{
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		if (isset($sendmethodAcct) && $sendmethodAcct == 'sendmail'){
			$mail -> IsSendmail();
		}
		elseif (isset($sendmethodAcct) && $sendmethodAcct == 'smtp'){
			$mail -> IsSMTP();
		}
		elseif (isset($sendmethodAcct) && $sendmethodAcct == 'mail'){
			$mail -> IsMail();
		}
		elseif (isset($sendmethodAcct) && $sendmethodAcct == 'qmail'){
			$mail -> IsQmail();
		}

		$mail -> Host 	= $sockethostAcct;
		$mail -> Port	= $smtpportAcct;

		if($smtpauthAcct == 'TRUE'){
			$mail -> SMTPAuth = true;
			$mail -> Username = $smtpauthuserAcct;
			$mail -> Password = $smtpauthpassAcct;
		}
		
		if (is_array($to) && $to[0]["email"] <>""){
			foreach($to as $key=>$recipient){
				$mail -> AddAddress($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($to) && $to["email"] <>""){
			$mail -> AddAddress($to["email"], $to["name"]);
		}
		
		/*	If set send a copy to the sender	*/
		if ($response_flag){
			if(is_array($from) && $from["email"] <>""){
				$mail->addBcc($from["email"], $from["name"]);
			}
			else{
				$mail->addBcc($socketfromAcct, $socketfromnameAcct);
			}
		}
		
		
		if(is_array($cc) && $cc[0]["email"] <>""){
			foreach($cc as $key=>$recipient){
				$mail -> AddCC($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($cc) && $cc["email"] <>""){
			$mail -> AddCC($cc["email"], $cc["name"]);
		}
		
		if(is_array($bcc) && $bcc[0]["email"] <>""){
			foreach($bcc as $key=>$recipient){
				$mail->addBcc($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($bcc) && $bcc["email"] <>""){
			$mail->addBcc($bcc["email"], $bcc["name"]);
		}
	

		if(is_array($from) && $from["email"] <>""){
			$mail -> From     = $from["email"];
			$mail -> FromName = $from["name"];
		}
		else{
			$mail -> From     = $socketfromAcct ;
			$mail -> FromName = $socketfromnameAcct ;
		}	
		
		if(is_array($replyto) && $replyto["email"] <>""){
			$mail -> AddReplyTo($replyto["email"], $replyto["name"]);
		}

		if(is_array($attch_file)){
			foreach($attch_file as $key=>$val){
				if(!empty($val)){
					$mail -> AddAttachment( $val );	
				}
			}
		}else{
			if ( $attch_file != "" ) {
				$mail -> AddAttachment( $attch_file );		
			}
		}
		/*
		if ( $attch_file != "" ) {
			$mail -> AddAttachment( $attch_file );
		}*/

		$mail -> IsHTML($html);
		$mail -> Body    = $message;
		$mail -> Subject = $subject;
		//$mail -> SMTPDebug = true;

		if(!$mail -> Send()){
			return ('Error: '.$mail -> ErrorInfo);
		}
		else{
			return ('Email Sent. '.$mail -> ErrorInfo);
		}
		$mail -> ClearAddresses();
	}
	
	function SendMailSales($to, $from, $replyto, $subject, $message, $html = false, $response_flag = false, $cc="", $bcc="", $attch_file=""){
		global $sendmethodSales, $sockethostSales, $smtpauthSales, $smtpauthuserSales, $smtpauthpassSales, $socketfromSales, $socketfromnameSales, $smtpportSales;

		include_once (THIS_PATH .'/lib/class/class.phpmailer.php');

		$mail  = new phpmailer();

		if (file_exists(THIS_PATH .'/lib/class/language/phpmailer.lang-en.php')){
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		else{
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		if (isset($sendmethodSales) && $sendmethodSales == 'sendmail'){
			$mail -> IsSendmail();
		}
		elseif (isset($sendmethodSales) && $sendmethodSales == 'smtp'){
			$mail -> IsSMTP();
		}
		elseif (isset($sendmethodSales) && $sendmethodSales == 'mail'){
			$mail -> IsMail();
		}
		elseif (isset($sendmethodSales) && $sendmethodSales == 'qmail'){
			$mail -> IsQmail();
		}

		$mail -> Host 	= $sockethostSales;
		$mail -> Port	= $smtpportSales;

		if($smtpauthSales == 'TRUE'){
			$mail -> SMTPAuth = true;
			$mail -> Username = $smtpauthuserSales;
			$mail -> Password = $smtpauthpassSales;
		}
		
		if (is_array($to) && $to[0]["email"] <>""){
			foreach($to as $key=>$recipient){
				$mail -> AddAddress($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($to) && $to["email"] <>""){
			$mail -> AddAddress($to["email"], $to["name"]);
		}
		
		/*	If set send a copy to the sender	*/
		if ($response_flag){
			if(is_array($from) && $from["email"] <>""){
				$mail->addBcc($from["email"], $from["name"]);
			}
			else{
				$mail->addBcc($socketfromSales, $socketfromnameSales);
			}
		}
		
		
		if(is_array($cc) && $cc[0]["email"] <>""){
			foreach($cc as $key=>$recipient){
				$mail -> AddCC($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($cc) && $cc["email"] <>""){
			$mail -> AddCC($cc["email"], $cc["name"]);
		}
		
		if(is_array($bcc) && $bcc[0]["email"] <>""){
			foreach($bcc as $key=>$recipient){
				$mail->addBcc($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($bcc) && $bcc["email"] <>""){
			$mail->addBcc($bcc["email"], $bcc["name"]);
		}
	

		if(is_array($from) && $from["email"] <>""){
			$mail -> From     = $from["email"];
			$mail -> FromName = $from["name"];
		}
		else{
			$mail -> From     = $socketfromSales ;
			$mail -> FromName = $socketfromnameSales ;
		}	
		
		if(is_array($replyto) && $replyto["email"] <>""){
			$mail -> AddReplyTo($replyto["email"], $replyto["name"]);
		}

		if(is_array($attch_file)){
			foreach($attch_file as $key=>$val){
				if(!empty($val)){
					$mail -> AddAttachment( $val );	
				}
			}
		}else{
			if ( $attch_file != "" ) {
				$mail -> AddAttachment( $attch_file );		
			}
		}
		/*
		if ( $attch_file != "" ) {
			$mail -> AddAttachment( $attch_file );
		}*/

		$mail -> IsHTML($html);
		$mail -> Body    = $message;
		$mail -> Subject = $subject;
		//$mail -> SMTPDebug = true;

		if(!$mail -> Send()){
			return ('Error: '.$mail -> ErrorInfo);
		}
		else{
			return ('Email Sent. '.$mail -> ErrorInfo);
		}
		$mail -> ClearAddresses();
	}
	
	function SendMailSupport($to, $from, $replyto, $subject, $message, $html = false, $response_flag = false, $cc="", $bcc="", $attch_file=""){
		global $sendmethodSupport, $sockethostSupport, $smtpauthSupport, $smtpauthuserSupport, $smtpauthpassSupport, $socketfromSupport, $socketfromnameSupport, $smtpportSupport; 

		include_once (THIS_PATH .'/lib/class/class.phpmailer.php');

		$mail  = new phpmailer();

		if (file_exists(THIS_PATH .'/lib/class/language/phpmailer.lang-en.php')){
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		else{
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		if (isset($sendmethodSupport) && $sendmethodSupport == 'sendmail'){
			$mail -> IsSendmail();
		}
		elseif (isset($sendmethodSupport) && $sendmethodSupport == 'smtp'){
			$mail -> IsSMTP();
		}
		elseif (isset($sendmethodSupport) && $sendmethodSupport == 'mail'){
			$mail -> IsMail();
		}
		elseif (isset($sendmethodSupport) && $sendmethodSupport == 'qmail'){
			$mail -> IsQmail();
		}

		$mail -> Host 	= $sockethostSupport;
		$mail -> Port	= $smtpportSupport;

		if($smtpauthSupport == 'TRUE'){
			$mail -> SMTPAuth = true;
			$mail -> Username = $smtpauthuserSupport;
			$mail -> Password = $smtpauthpassSupport;
		}
		
		if (is_array($to) && $to[0]["email"] <>""){
			foreach($to as $key=>$recipient){
				$mail -> AddAddress($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($to) && $to["email"] <>""){
			$mail -> AddAddress($to["email"], $to["name"]);
		}
		
		/*	If set send a copy to the sender	*/
		if ($response_flag){
			if(is_array($from) && $from["email"] <>""){
				$mail->addBcc($from["email"], $from["name"]);
			}
			else{
				$mail->addBcc($socketfromSupport, $socketfromnameSupport);
			}
		}
		
		
		if(is_array($cc) && $cc[0]["email"] <>""){
			foreach($cc as $key=>$recipient){
				$mail -> AddCC($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($cc) && $cc["email"] <>""){
			$mail -> AddCC($cc["email"], $cc["name"]);
		}
		
		if(is_array($bcc) && $bcc[0]["email"] <>""){
			foreach($bcc as $key=>$recipient){
				$mail->addBcc($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($bcc) && $bcc["email"] <>""){
			$mail->addBcc($bcc["email"], $bcc["name"]);
		}
		
		if(is_array($from) && $from["email"] <>""){
			$mail -> From     = $from["email"];
			$mail -> FromName = $from["name"];
		}
		else{
			$mail -> From     = $socketfromSupport ;
			$mail -> FromName = $socketfromnameSupport ;
		}	
		
		if(is_array($replyto) && $replyto["email"] <>""){
			$mail -> AddReplyTo($replyto["email"], $replyto["name"]);
		}

		if(is_array($attch_file)){
			foreach($attch_file as $key=>$val){
				if(!empty($val)){
					$mail -> AddAttachment( $val );	
				}
			}
		}else{
			if ( $attch_file != "" ) {
				$mail -> AddAttachment( $attch_file );		
			}
		}
		/*
		if ( $attch_file != "" ) {
			$mail -> AddAttachment( $attch_file );
		}*/

		$mail -> IsHTML($html);
		$mail -> Body    = $message;
		$mail -> Subject = $subject;
		//$mail -> SMTPDebug = true;
		if(!$mail -> Send()){
			return ('Error: '.$mail -> ErrorInfo); 
		}
		else{
			return ('Email Sent. '.$mail -> ErrorInfo);
		}
		$mail -> ClearAddresses();
	}
	
	/*
		SendMailAuth function is used to send mail with different authentication, different from-email
		****We have to pass array as below : 
		$auth_details['sendmethod']= '';
		$auth_details['sockethost']='';
		$auth_details['smtpauth']='';
		$auth_details['smtpauthuser']='';
		$auth_details['smtpauthpass']='';
		$auth_details['socketfrom']='';
		$auth_details['socketfromname']='';
		$auth_details['socketreply']='';
		$auth_details['socketreplyname']='';
		$auth_details['smtpport']=''; 
	*/
    function SendMailAuth($to, $from, $replyto, $subject, $message, $html = false, $response_flag = false, $cc="", $bcc="", $attch_file="",&$auth_details){
		//global $sendmethod, $sockethost, $smtpauth, $smtpauthuser, $smtpauthpass, $socketfrom, $socketfromname, $socketreply, $socketreplyname, $smtpport;

		include_once (THIS_PATH .'/lib/class/class.phpmailer.php');

		$mail  = new phpmailer();

		if (file_exists(THIS_PATH .'/lib/class/language/phpmailer.lang-en.php')){
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		else{
			$mail -> SetLanguage('en', THIS_PATH .'/lib/class/language/');
		}
		if (isset($auth_details['sendmethod']) && $auth_details['sendmethod'] == 'sendmail'){
			$mail -> IsSendmail();
		}
		elseif (isset($auth_details['sendmethod']) && $auth_details['sendmethod'] == 'smtp'){
			$mail -> IsSMTP();
		}
		elseif (isset($auth_details['sendmethod']) && $auth_details['sendmethod'] == 'mail'){
			$mail -> IsMail();
		}
		elseif (isset($auth_details['sendmethod']) && $auth_details['sendmethod'] == 'qmail'){
			$mail -> IsQmail();
		}

		$mail -> Host 	= $auth_details['sockethost'];
		$mail -> Port	= $auth_details['smtpport'];

		if($auth_details['smtpauth'] == 'TRUE'){
			$mail -> SMTPAuth = true;
			$mail -> Username = $auth_details['smtpauthuser'];
			$mail -> Password = $auth_details['smtpauthpass'];
		}
		
		if (is_array($to) && $to[0]["email"] <>""){
			foreach($to as $key=>$recipient){
				$mail -> AddAddress($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($to) && $to["email"] <>""){
			$mail -> AddAddress($to["email"], $to["name"]);
		}
		
		/*	If set send a copy to the sender	*/
		if ($response_flag){
			if(is_array($from) && $from["email"] <>""){
				$mail->addBcc($from["email"], $from["name"]);
			}
			else{
				$mail->addBcc($auth_details['socketfrom'], $auth_details['socketfromname']);
			}
		}
		
		
		if(is_array($cc) && $cc[0]["email"] <>""){
			foreach($cc as $key=>$recipient){
				$mail -> AddCC($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($cc) && $cc["email"] <>""){
			$mail -> AddCC($cc["email"], $cc["name"]);
		}
		
		if(is_array($bcc) && $bcc[0]["email"] <>""){
			foreach($bcc as $key=>$recipient){
				$mail->addBcc($recipient["email"], $recipient["name"]);
			}
		}
		elseif(is_array($bcc) && $bcc["email"] <>""){
			$mail->addBcc($bcc["email"], $bcc["name"]);
		}
	

		if(is_array($from) && $from["email"] <>""){
			$mail -> From     = $from["email"];
			$mail -> FromName = $from["name"];
		}
		else{
			$mail -> From     = $auth_details['socketfrom'] ;
			$mail -> FromName = $auth_details['socketfromname'] ;
		}	
		
		if(is_array($replyto) && $replyto["email"] <>""){
			$mail -> AddReplyTo($replyto["email"], $replyto["name"]);
		}

		if(is_array($attch_file)){
			foreach($attch_file as $key=>$val){
				if(!empty($val)){
					$mail -> AddAttachment( $val );	
				}
			}
		}else{
			if ( $attch_file != "" ) {
				$mail -> AddAttachment( $attch_file );		
			}
		}

		/*
		if ( $attch_file != "" ) {
			$mail -> AddAttachment( $attch_file );
		}*/

		$mail -> IsHTML($html);
		$mail -> Body    = $message;
		$mail -> Subject = $subject;
		//$mail -> SMTPDebug = true;

		if(!$mail -> Send()){
			return ('Error: '.$mail -> ErrorInfo);
		}
		else{
			return ('Email Sent. '.$mail -> ErrorInfo);
		}

		$mail -> ClearAddresses();
	}
	
	/**
	* This function is mainly used to retrieve the table name
	* from the array containing the table names and their
	* respective fields in a 3-d array.
	*/
	function findIndex( $needle, $_2DHaystack) {
		$index = "";
		foreach($_2DHaystack as $key => $value){
			$index = $key;

			if ( arraySearchR($needle, $value) ) {
				return $index;
			}
		}
		return (false);
	}

	
	/**
	* Search for a value in a multidimensional array.
	**/
	function arraySearchR($needle, $haystack, &$keys=NULL){
		$match = '';
		if (! is_array($keys)) {
			$keys = array();
		}
		foreach ($haystack as $key=>$value){
			if (is_array($value)) {
				$match = arraySearchR($needle, $value, $keys);
				if ($match) {
					array_unshift($keys, $key);
				}
			}
			if ($value === $needle) {
				$match=1;
				array_unshift($keys, $key);
			}
			if ($match) {
				return 1;
			}
		}
		return 0;
	}

	/**
	 * Function to show the page numbers where in a list the number of entries 
	 * displayed on one page are restricted. This function is to be used when 
	 * the list is displayed using AJAX.
	 *
	 * @param	int		the total number of records that are in the resultset.
	 * @param	int		the current page number which is being displayed.
	 * @param	int		the number of entries that are to be shown on one page.
	 * @param	string	the javascript fucntion that will be handling the AJAX call on the HTML page.
	 *
	 * @return	string	the complete generated HTML code is returned back.
	 *
	 */
	function showPaginationAjax($total_rec, $current_page=0, $perPage, $handler) {
		$pagination = "<div id=\"pagiantion\">" ;
		//	Reteieve Current Page number 
		if($current_page<0 || $current_page==0){
			$p = 1 ;
		}
		else{
			$p = $current_page ;
		}
		
		//	Calculate Total Pages
		$total_pages = (int)($total_rec/$perPage) ;
		
		if( ($total_rec%$perPage) > 0 ){
			$total_pages++ ;
		}

		//	To show pagination
		if($total_pages > 1) {
			$pagination .= "<div><span>Page ";
            $pagination .= "<input type=\"text\" name=\"goto\" id=\"goto\" value=\"". $p ."\" size=\"4\" class=\"inputbox\" />" ;
			$pagination .= " of $total_pages ";
            $pagination .= "<input type=\"button\" name=\"btnGo\" value=\"GO\" onClick=\"javascript:". $handler ."(document.getElementById('goto').value);\" class=\"button\" /><span></div>";
			//	Front Links
			if($p>1){
				$pagination .= "<div><span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(1);\">First</a></span>" ;
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". ($p-1) .");\">Previous</a></span>" ;	
			}
			else{
				$pagination .= "<div><span>First</span>" ;
				$pagination .= "<span>Previous</span>" ;
			}	
			
			// Middle links
			if( ($p%5) == 0){
				if( ($total_pages - $p) > 5){
					$start = $p-5 ? $p-5:1 ;
				}
				else{
					$start = $total_pages - 10 ;
					if($start <= 0){
						$start = 1 ;
					}	
				}	
				$end = $p+5 ;
			}
			else{	
				if($p > 9){
					if( ($total_pages - $p) > 5){
						$start = $p - 5 ;
					}
					else{
						$start = $total_pages - 10 ;
					}		
					$end = $p + 5 ;
				}
				else{	 
					$start = 1 ;
					$end = 10 ;
				}	
					
			}	
			for($i=$start; $i<=$end && $i<=$total_pages; $i++){
				if($i==$p){
					$pagination .= "<span class=\"selected\">$i</span>" ;
				}
				else{	
					$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". $i .");\">$i</a></span>" ;
				}	
			}
			//	Rear Links
			if($p>=1 && $p<$total_pages){
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". ($p+1) .");\">Next</a></span>" ;
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". $total_pages .");\">Last</a></span></div>" ;
			}
			else{
				$pagination .= "<span>Next</span>";
				$pagination .= "<span>Last</span></div>";
			}
		}	//	if($total_pages>1) end.
	
		return ($pagination) ;
	}
	//AJAX base pagination 2011-02-feb-18
	function ajaxPagination($total_rec, $current_page=0, $perPage,$handler,$pagename,$formname) {
		//$pagination = "<div id=\"pagiantion\">" ;
		$pagination = "" ;
		//	Reteieve Current Page number 
		if($current_page<0 || $current_page==0){
			$p = 1 ;
		}
		else{
			$p = $current_page ;
		}
		
		//	Calculate Total Pages
		$total_pages = (int)($total_rec/$perPage) ;
		
		if( ($total_rec%$perPage) > 0 ){
			$total_pages++ ;
		}

		//	To show pagination
		if($total_pages > 1) {
			//$pagination .= "<div><span class='pagination-notselected'>Page ";
            //$pagination .= "&nbsp;&nbsp;<input type=\"text\" name=\"goto\" id=\"goto\" value=\"". $p ."\" size=\"4\" class=\"input-pegination\" />" ;
			//$pagination .= "&nbsp; of $total_pages ";
            //$pagination .= "&nbsp;&nbsp;<input type=\"button\" name=\"btnGo\" value=\"GO\" onClick=\"javascript:". $handler ."(document.getElementById('goto').value);\" class=\"btnGO-pegination\" /><span></div>";
			//	Front Links
			if($p>1){
				$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler ."(1,'".$pagename."',".$formname.");\" class='pagination-link'>First&nbsp;</a></span>" ;
				$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler." (".($p-1).",'".$pagename."',".$formname.");\" class='pagination-link'>Previous&nbsp;</a></span>" ;	
			}
			else{
				$pagination .= "<span class='pagination-link'>First&nbsp;</span>" ;
				$pagination .= "<span class='pagination-link'>&nbsp;Previous&nbsp;</span>" ;
			}	
			
			// Middle links
			if( ($p%5) == 0){
				if( ($total_pages - $p) > 5){
					$start = $p-5 ? $p-5:1 ;
				}
				else{
					$start = $total_pages - 10 ;
					if($start <= 0){
						$start = 1 ;
					}	
				}	
				$end = $p+5 ;
			}
			else{	
				if($p > 9){
					if( ($total_pages - $p) > 5){
						$start = $p - 5 ;
					}
					else{
						$start = $total_pages - 10 ;
					}		
					$end = $p + 5 ;
				}
				else{	 
					$start = 1 ;
					$end = 10 ;
				}	
					
			}	
			for($i=$start; $i<=$end && $i<=$total_pages; $i++){
				if($i==$p){
					$pagination .= "<span class=\"pagination-selected\">$i&nbsp;</span>" ;
				}else{	
					$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler ."(". $i .",'".$pagename."',
					".$formname.");\" class='pagination-link'>$i&nbsp;</a></span>" ;
				}	
			}
			//	Rear Links
			if($p>=1 && $p<$total_pages){
				$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler ."(". ($p+1) .",'".$pagename."',
				".$formname.");\" class='pagination-link'>Next&nbsp;&nbsp;</a></span>" ;
				$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler ."(". $total_pages .",'".$pagename."',
				".$formname.");\" class='pagination-link'>Last</a></span>" ;
			}
			else{
				$pagination .= "<span class='pagination-link'>&nbsp;Next&nbsp;</span>";
				$pagination .= "<span class='pagination-link'>Last</span>";
			}
		}	//	if($total_pages>1) end.
	
		return ($pagination) ;
	}
    // Ajax pagination eof
	function ajaxPagination2($total_rec, $current_page=0, $perPage,$handler,$pagename,$formname,$extra='') {
		//$pagination = "<div id=\"pagiantion\">" ;
		$pagination = "" ;
		//	Reteieve Current Page number 
		if($current_page<0 || $current_page==0){
			$p = 1 ;
		}
		else{
			$p = $current_page ;
		}
		
		//	Calculate Total Pages
		$total_pages = (int)($total_rec/$perPage) ;
		
		if( ($total_rec%$perPage) > 0 ){
			$total_pages++ ;
		}

		//	To show pagination
		if($total_pages > 1) {
			//$pagination .= "<div><span class='pagination-notselected'>Page ";
            //$pagination .= "&nbsp;&nbsp;<input type=\"text\" name=\"goto\" id=\"goto\" value=\"". $p ."\" size=\"4\" class=\"input-pegination\" />" ;
			//$pagination .= "&nbsp; of $total_pages ";
            //$pagination .= "&nbsp;&nbsp;<input type=\"button\" name=\"btnGo\" value=\"GO\" onClick=\"javascript:". $handler ."(document.getElementById('goto').value);\" class=\"btnGO-pegination\" /><span></div>";
			//	Front Links
			if($p>1){
				$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler ."(1,'".$pagename."',".$formname.",".$extra.");\" class='pagination-link'>First&nbsp;</a></span>" ;
				$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler." (".($p-1).",'".$pagename."',".$formname.",".$extra.");\" class='pagination-link'>Previous&nbsp;</a></span>" ;	
			}
			else{
				$pagination .= "<span class='pagination-link'>First&nbsp;</span>" ;
				$pagination .= "<span class='pagination-link'>&nbsp;Previous&nbsp;</span>" ;
			}	
			
			// Middle links
			if( ($p%5) == 0){
				if( ($total_pages - $p) > 5){
					$start = $p-5 ? $p-5:1 ;
				}
				else{
					$start = $total_pages - 10 ;
					if($start <= 0){
						$start = 1 ;
					}	
				}	
				$end = $p+5 ;
			}
			else{	
				if($p > 9){
					if( ($total_pages - $p) > 5){
						$start = $p - 5 ;
					}
					else{
						$start = $total_pages - 10 ;
					}		
					$end = $p + 5 ;
				}
				else{	 
					$start = 1 ;
					$end = 10 ;
				}	
					
			}	
			for($i=$start; $i<=$end && $i<=$total_pages; $i++){
				if($i==$p){
					$pagination .= "<span class=\"pagination-selected\">$i&nbsp;</span>" ;
				}else{	
					$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler ."(". $i .",'".$pagename."',
					".$formname.",".$extra.");\" class='pagination-link'>$i&nbsp;</a></span>" ;
				}	
			}
			//	Rear Links
			if($p>=1 && $p<$total_pages){
				$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler ."(". ($p+1) .",'".$pagename."',
				".$formname.",".$extra.");\" class='pagination-link'>Next&nbsp;&nbsp;</a></span>" ;
				$pagination .= "<span><a href=\"#\" onClick=\"javascript:". $handler ."(". $total_pages .",'".$pagename."',
				".$formname.",".$extra.");\" class='pagination-link'>Last</a></span>" ;
			}
			else{
				$pagination .= "<span class='pagination-link'>&nbsp;Next&nbsp;</span>";
				$pagination .= "<span class='pagination-link'>Last</span>";
			}
		}	//	if($total_pages>1) end.
	
		return ($pagination) ;
	}
    function showPaginationAjaxWithExtra($total_rec, $current_page=0, $perPage, $extra, $handler) {
        $extra = htmlentities($extra);
		$pagination = "<div id=\"pagiantion\">" ;
		//	Reteieve Current Page number 
		if($current_page<0 || $current_page==0){
			$p = 1 ;
		}
		else{
			$p = $current_page ;
		}
		
		//	Calculate Total Pages
		$total_pages = (int)($total_rec/$perPage) ;
		
		if( ($total_rec%$perPage) > 0 ){
			$total_pages++ ;
		}

		//	To show pagination
		if($total_pages > 1) {
			//$pagination .= "<div><span>Page ";
           // $pagination .= "<input type=\"text\" name=\"goto\" id=\"goto\" value=\"". $p ."\" size=\"4\" class=\"inputbox\" />" ;
			//$pagination .= " of $total_pages ";
           // $pagination .= "<input type=\"button\" name=\"btnGo\" value=\"GO\" onClick=\"javascript:". $handler ."(document.getElementById('goto').value,'".$extra."');\" class=\"button\" /><span></div>";
			//	Front Links
			if($p>1){
				$pagination .= "<div><span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(1 ,'".$extra."');\">First</a></span>" ;
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". ($p-1) .",'".$extra."');\">Previous</a></span>" ;	
			}
			else{
				$pagination .= "<div><span>First</span>" ;
				$pagination .= "<span>Previous</span>" ;
			}	
			
			// Middle links
			if( ($p%5) == 0){
				if( ($total_pages - $p) > 5){
					$start = $p-5 ? $p-5:1 ;
				}
				else{
					$start = $total_pages - 10 ;
					if($start <= 0){
						$start = 1 ;
					}	
				}	
				$end = $p+5 ;
			}
			else{	
				if($p > 9){
					if( ($total_pages - $p) > 5){
						$start = $p - 5 ;
					}
					else{
						$start = $total_pages - 10 ;
					}		
					$end = $p + 5 ;
				}
				else{	 
					$start = 1 ;
					$end = 10 ;
				}	
					
			}	
			for($i=$start; $i<=$end && $i<=$total_pages; $i++){
				if($i==$p){
					$pagination .= "<span class=\"selected\">$i</span>" ;
				}
				else{	
					$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". $i .",'".$extra."');\">$i</a></span>" ;
				}	
			}
			//	Rear Links
			if($p>=1 && $p<$total_pages){
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". ($p+1) .",'".$extra."');\">Next</a></span>" ;
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". $total_pages .",'".$extra."');\">Last</a></span></div>" ;
			}
			else{
				$pagination .= "<span>Next</span>";
				$pagination .= "<span>Last</span></div>";
			}
		}	//	if($total_pages>1) end.
	
		return ($pagination) ;
	}
    
	function showMovePagination($total_rec, $current_page=0, $perPage, $extra,$xvar, $handler) {
        $extra = htmlentities($extra);
		$pagination = "<div id=\"pagiantion\">" ;
		//	Reteieve Current Page number 
		if($current_page<0 || $current_page==0){
			$p = 1 ;
		}
		else{
			$p = $current_page ;
		}
		
		//	Calculate Total Pages
		$total_pages = (int)($total_rec/$perPage) ;
		
		if( ($total_rec%$perPage) > 0 ){
			$total_pages++ ;
		}

		//	To show pagination
		if($total_pages > 1) {
			$pagination .= "<div><span>Page ";
            $pagination .= "<input type=\"text\" name=\"goto\" id=\"goto".$xvar."\" value=\"". $p ."\" size=\"4\" class=\"inputbox\" />" ;
			$pagination .= " of $total_pages ";
            $pagination .= "<input type=\"button\" name=\"btnGo\" value=\"GO\" onClick=\"javascript:". $handler ."(document.getElementById('goto".$xvar."').value,'".$xvar."','".$extra."');\" class=\"button\" /><span></div>";
			//	Front Links
			if($p>1){
				$pagination .= "<div><span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(1 ,'".$xvar."','".$extra."');\">First</a></span>" ;
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". ($p-1) .",'".$xvar."','".$extra."');\">Previous</a></span>" ;	
			}
			else{
				$pagination .= "<div><span>First</span>" ;
				$pagination .= "<span>Previous</span>" ;
			}	
			
			// Middle links
            /*
			if( ($p%5) == 0){
				if( ($total_pages - $p) > 5){
					$start = $p-5 ? $p-5:1 ;
				}
				else{
					$start = $total_pages - 10 ;
					
					if($start <= 0){
						$start = 1 ;
					}	
				}	
				$end = $p+5 ;
			}
			else{	
				if($p > 9){
					if( ($total_pages - $p) > 5){
						$start = $p - 5 ;
					}
					else{
						$start = $total_pages - 10 ;
					}		
					$end = $p + 5 ;
				}
				else{	 
					$start = 1 ;
					$end = 10 ;
				}	
					
			}	
            
			for($i=$start; $i<=$end && $i<=$total_pages; $i++){
				if($i==$p){
					$pagination .= "<span class=\"selected\">$i</span>" ;
				}
				else{	
					$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". $i .",'".$xvar."','".$extra."');\">$i</a></span>" ;
				}	
			}*/
			//	Rear Links
			if($p>=1 && $p<$total_pages){
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". ($p+1) .",'".$xvar."','".$extra."');\">Next</a></span>" ;
				$pagination .= "<span><a href=\"javascript:void(0);\" onClick=\"javascript:". $handler ."(". $total_pages .",'".$xvar."','".$extra."');\">Last</a></span></div>" ;
			}
			else{
				$pagination .= "<span>Next</span>";
				$pagination .= "<span>Last</span></div>";
			}
		}	//	if($total_pages>1) end.
	
		return ($pagination) ;
	}
    
    
    function showPaginationNext($total_rec, $current_page=0, $perPage, $url_name, $extra){
        $pagination = "" ;
        
        //	Reteieve Current Page number 
        if($current_page<0 || $current_page==0){
            $p = 1 ;
        }
        else{
            $p = $current_page ;
        }
        
        //	Additional URL paramaters
        if($extra<>""){
            $extra .= "&" ;
        }
        
        //	Calculate Total Pages
        $total_pages = (int)($total_rec/$perPage) ;
        
        if( ($total_rec%$perPage) > 0 ){
            $total_pages++ ;
        }
    
        //	To show pagination
         if($total_pages > 1){
            //$pagination .= "<span class=\"normal-text\">Showing Page ". $p ." of $total_pages </span><br/>" ;
            //	Front Links
            if($p>1){
                $pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=1\" class=\"link\"></a>&nbsp;" ;
                //$pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=". ($p-1) ."\" class=\"link\">Previous--10</a>&nbsp;" ;	
            }
            else{
                $pagination .= "<span class=\"form-field-link\">&nbsp;</span>" ;
                // comment on 2008-10-02
                //$pagination .= "<span style='padding-right:300px' class=\"form-field-link\">Previous&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;</span>" ;
            }	
                 
            //	Rear Links
            if($p>=1 && $p<$total_pages){
                $pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=". ($p+1) ."\" class=\"link\">Next</a>&nbsp;" ;
                $pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=$total_pages\" class=\"link\"></a>&nbsp;" ;
            }
            else{
                $pagination .= "<span class=\"form-field-link\">&nbsp;&nbsp;&nbsp;Next</span>&nbsp;" ;
                $pagination .= "<span class=\"form-field-link\"></span>" ;
            }
        }	//	if($total_pages>1) end.
        return ($pagination) ;
    }
    
    function showPaginationPrevious($total_rec, $current_page=0, $perPage, $url_name, $extra){
        $pagination = "" ;
        
        //	Reteieve Current Page number 
        if($current_page<0 || $current_page==0){
            $p = 1 ;
        }
        else{
            $p = $current_page ;
        }
        
        //	Additional URL paramaters
        if($extra<>""){
            $extra .= "&" ;
        }
        
        //	Calculate Total Pages
        $total_pages = (int)($total_rec/$perPage) ;
        
        if( ($total_rec%$perPage) > 0 ){
            $total_pages++ ;
        }
    
        //	To show pagination
         if($total_pages > 1){
            //$pagination .= "<span class=\"normal-text\">Showing Page ". $p ." of $total_pages </span><br/>" ;
            //	Front Links
            if($p>1){
                $pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=1\" class=\"link\"></a>&nbsp;" ;
                $pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=". ($p-1) ."\" class=\"link\">Previous</a>&nbsp;" ;	
            }
            else{
                $pagination .= "<span class=\"form-field-link\">&nbsp;</span>" ;
                //comment on 2008-10-02
                //$pagination .= "<span style='padding-right:300px' class=\"form-field-link\">Previous&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;</span>" ;
            }	
            
            //	Rear Links
            if($p>=1 && $p<$total_pages){
                $text = "Next";
                if($p==1){
                    $text = "Previous";
                    $pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=". ($p+1) ."\" class=\"link\">".$text."</a>&nbsp;" ;
                }
                //$pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=". ($p+1) ."\" class=\"link\">".$text."</a>&nbsp;" ;
                $pagination .= "<a href=\"".$_SERVER['PHP_SELF']."?".$extra . $url_name ."=$total_pages\" class=\"link\"></a>&nbsp;" ;
            }
            else{
                //$pagination .= "<span class=\"form-field-link\">&nbsp;&nbsp;&nbsp;Next</span>&nbsp;" ;
                $pagination .= "<span class=\"form-field-link\"></span>" ;
            }
        }	//	if($total_pages>1) end.
        return ($pagination) ;
    }
    /**
     * Function to retrieve the list of the access level
     * that will be available to the logged in User. 
     * The Logged In user has access to the Access Levels 
     * with value same or less than that of his/her Access Level.
     *
     * @param   access_level    the integer value of the access level possessed by the User.
     * 
     * @return  array           array containing the Title and value of the Access Levels.
     *
     */
    function getAccessLevel(&$db, $level=1, $is_level=false) {
        include_once ( DIR_FS_INCLUDES .'/settings-access-level.inc.php');

        $access     = NULL;
        if ( $is_level ) {
            $condition  = " WHERE access_level = '". $level ."'";
        }
        else {
            $condition  = " WHERE access_level < '". $level ."'";
        }
        $condition  .= " AND status = '". AccessLevel::ACTIVE ."'"
                        ." ORDER BY access_level ASC";
        if ( (AccessLevel::getList($db, $access, 'title, access_level', $condition)) <= 0 ) {
            $access[] = array('title' => 'None', 'access_level' => '0' );
        }
        
        return ($access);
    }


    function isPresentAccessLevel(&$db, $access_level) {
        include_once ( DIR_FS_INCLUDES .'/settings-access-level.inc.php');

        $access     = NULL;
        $condition  = " WHERE access_level = '". $access_level ."'"
                ." AND status = '". AccessLevel::ACTIVE ."'";
        if ( AccessLevel::getList($db, $access, 'id', $condition) ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    // Function used to manage number of ord/INV/RCT as per the financial-yr 
    
    function getCounterNumber(&$db,$counterFor,$company_id=''){
       
        $number ='';
        /*
        $query = "SELECT "	. TABLE_FINANCIAL_YEAR .".id"
						.",". TABLE_FINANCIAL_YEAR .".ord_counter"
						.",". TABLE_FINANCIAL_YEAR .".inv_counter"
						.",". TABLE_FINANCIAL_YEAR .".rct_counter"
						.",". TABLE_FINANCIAL_YEAR .".tra_counter"
					." FROM ". TABLE_FINANCIAL_YEAR
					." WHERE ".TABLE_FINANCIAL_YEAR.".year_range='".$yearRange."' ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1" ;
		*/
        
        $query = "SELECT "	. TABLE_FINANCIAL_YEAR .".id"
						.",". TABLE_FINANCIAL_YEAR .".year_range"
						//.",". TABLE_FINANCIAL_YEAR .".lead_counter"
						.",". TABLE_FINANCIAL_YEAR .".ord_counter"
						.",". TABLE_FINANCIAL_YEAR .".inv_counter"
						.",". TABLE_FINANCIAL_YEAR .".rct_counter"
						.",". TABLE_FINANCIAL_YEAR .".tra_counter"
						//.",". TABLE_FINANCIAL_YEAR .".flw_counter"
						//.",". TABLE_FINANCIAL_YEAR .".pqt_counter"
						.",". TABLE_FINANCIAL_YEAR .".qt_counter"
						.",". TABLE_FINANCIAL_YEAR .".company_prefix"
					." FROM ". TABLE_FINANCIAL_YEAR
					." WHERE ".TABLE_FINANCIAL_YEAR.".company_id ='".$company_id."' AND ".TABLE_FINANCIAL_YEAR.".is_financial_yr='1' ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1 " ;
					
                    //." WHERE ".TABLE_FINANCIAL_YEAR.".year_range='".$yearRange."' ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1" ;
					//." ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1" ;

        $db->query( $query );
		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {
                $yearRange = $db->f('year_range');
                
                if($counterFor=='LEAD'){                    
                    $counter = $db->f('lead_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-LEAD-'.$yearRange.'-'.processUserData($counter);
                }
                if($counterFor=='ORD'){                    
                    $counter = $db->f('ord_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-ORD-'.$yearRange.'-'.processUserData($counter);
                }
                if($counterFor=='INV'){                    
                    $counter = $db->f('inv_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-INV-'.$yearRange.'-'.processUserData($counter);
                }
                if($counterFor==CONTR_RPT){ 
                               
                    $counter = $db->f('rct_counter');
                    $company_prefix = $db->f('company_prefix');     
                    $number = $company_prefix."-".CONTR_RPT."-".$yearRange.'-'.processUserData($counter);
                }
                if($counterFor==CONTR_VCH){                    
                    $counter = $db->f('tra_counter');
                    $company_prefix = $db->f('company_prefix');     
                    $number = $company_prefix."-".CONTR_VCH."-".$yearRange.'-'.processUserData($counter);
                }
                if($counterFor =='FLW' ){
                   $counter = $db->f('flw_counter');
                   $company_prefix = $db->f('company_prefix');  
                   $number = $company_prefix.'-FLW-'.$yearRange.'-'.processUserData($counter);                           
                }
                /*
                if($counterFor =='PQT' ){
                    $counter = $db->f('pqt_counter');
                    $number = 'PT-PQT-'.$yearRange.'-'.processUserData($counter);             
                }*/
                
                if($counterFor =='QT' ){
                    $counter = $db->f('qt_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-QT-'.$yearRange.'-'.processUserData($counter);             
                }
            }
        }
        return $number ;
    }
    
    function updateCounterOf(&$db,$counterFor,$company_id=''){
        
        /*
        $query = "SELECT "	. TABLE_FINANCIAL_YEAR .".id"
						.",". TABLE_FINANCIAL_YEAR .".ord_counter"
						.",". TABLE_FINANCIAL_YEAR .".inv_counter"
						.",". TABLE_FINANCIAL_YEAR .".rct_counter"
						.",". TABLE_FINANCIAL_YEAR .".tra_counter"
					." FROM ". TABLE_FINANCIAL_YEAR
					." WHERE ".TABLE_FINANCIAL_YEAR.".year_range='".$yearRange."' ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1" ;
		*/
        
        $query = "SELECT "	. TABLE_FINANCIAL_YEAR .".id"
						.",". TABLE_FINANCIAL_YEAR .".year_range"
						//.",". TABLE_FINANCIAL_YEAR .".lead_counter"
						.",". TABLE_FINANCIAL_YEAR .".ord_counter"
						.",". TABLE_FINANCIAL_YEAR .".inv_counter"
						.",". TABLE_FINANCIAL_YEAR .".rct_counter"
						.",". TABLE_FINANCIAL_YEAR .".tra_counter"
                        //.",". TABLE_FINANCIAL_YEAR .".flw_counter"
						//.",". TABLE_FINANCIAL_YEAR .".pqt_counter"
						.",". TABLE_FINANCIAL_YEAR .".qt_counter"
                        .",". TABLE_FINANCIAL_YEAR .".company_prefix"
                        ." FROM ". TABLE_FINANCIAL_YEAR
                        ." WHERE ".TABLE_FINANCIAL_YEAR.".company_id ='".$company_id."' AND ".TABLE_FINANCIAL_YEAR.".is_financial_yr='1' ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1 " ;
					    //." ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1" ;
                        
        $db->query( $query );
		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {            
               $id = $db->f('id');
            }
        }
        /*
        if($counterFor =='LEAD' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".lead_counter = ".TABLE_FINANCIAL_YEAR.".lead_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id = ".$id ; 
                   
        }*/
        
        if($counterFor =='ORD' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".ord_counter = ".TABLE_FINANCIAL_YEAR.".ord_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id = ".$id ; 
                   
        }
        if($counterFor =='INV' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".inv_counter = ".TABLE_FINANCIAL_YEAR.".inv_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor == CONTR_RPT ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".rct_counter = ".TABLE_FINANCIAL_YEAR.".rct_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor == CONTR_VCH ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".tra_counter = ".TABLE_FINANCIAL_YEAR.".tra_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor =='FLW' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".flw_counter = ".TABLE_FINANCIAL_YEAR.".flw_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        /*
        if($counterFor =='PQT' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".pqt_counter = ".TABLE_FINANCIAL_YEAR.".pqt_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }*/
        
        if($counterFor =='QT' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".qt_counter = ".TABLE_FINANCIAL_YEAR.".qt_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        $executed = $db->query( $update );
       
        
        return $executed;
    }
    
    function getOldCounterNumber(&$db,$counterFor,$company_id='',$financial_yr=''){
       
        $number ='';
        
        $query = "SELECT ". TABLE_FINANCIAL_YEAR .".id"					
						.",". TABLE_FINANCIAL_YEAR .".year_range"
						//.",". TABLE_FINANCIAL_YEAR .".lead_counter"
						.",". TABLE_FINANCIAL_YEAR .".ord_counter"
						.",". TABLE_FINANCIAL_YEAR .".inv_counter"
						.",". TABLE_FINANCIAL_YEAR .".rct_counter"
						.",". TABLE_FINANCIAL_YEAR .".tra_counter"
						//.",". TABLE_FINANCIAL_YEAR .".flw_counter"
						//.",". TABLE_FINANCIAL_YEAR .".pqt_counter"
						.",". TABLE_FINANCIAL_YEAR .".qt_counter"
						.",". TABLE_FINANCIAL_YEAR .".company_prefix"
					." FROM ". TABLE_FINANCIAL_YEAR
					." WHERE ".TABLE_FINANCIAL_YEAR.".company_id ='".$company_id."' AND ".TABLE_FINANCIAL_YEAR.".year_range='".$financial_yr."' ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1 " ;
					
        $db->query( $query );
		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {
                //$yearRange = $db->f('year_range');
                $yearRange = $financial_yr;
                /*
                if($counterFor=='ORD'){                    
                    $counter = $db->f('ord_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-ORD-'.$yearRange.'-'.processUserData($counter);
                }
                if($counterFor=='INV'){                    
                    $counter = $db->f('inv_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-INV-'.$yearRange.'-'.processUserData($counter);
                }
                if($counterFor==CONTR_RPT){       
                    $counter = $db->f('rct_counter');
                    $company_prefix = $db->f('company_prefix');     
                    $number = $company_prefix."-".CONTR_RPT."-".$yearRange.'-'.processUserData($counter);
                }
                if($counterFor==CONTR_VCH){                    
                    $counter = $db->f('tra_counter');
                    $company_prefix = $db->f('company_prefix');     
                    $number = $company_prefix."-".CONTR_VCH."-".$yearRange.'-'.processUserData($counter);
                }
                if($counterFor =='FLW' ){
                   $counter = $db->f('flw_counter');
                   $company_prefix = $db->f('company_prefix');  
                   $number = $company_prefix.'-FLW-'.$yearRange.'-'.processUserData($counter);                           
                }
                
                if($counterFor =='QT' ){
                    $counter = $db->f('qt_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-QT-'.$yearRange.'-'.processUserData($counter);             
                }*/
                if($counterFor=='LEAD'){                    
                    $counter = $db->f('lead_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-LEAD-'.$yearRange.'-'.processUserData($counter);
                }
                if($counterFor=='ORD'){                    
                    $counter = $db->f('ord_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-ORD-'.$yearRange.'-'.processUserData($counter);
                }
                if($counterFor=='INV'){                    
                    $counter = $db->f('inv_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-INV-'.$yearRange.'-'.processUserData($counter);
                }
                if($counterFor==CONTR_RPT){ 
                               
                    $counter = $db->f('rct_counter');
                    $company_prefix = $db->f('company_prefix');     
                    $number = $company_prefix."-".CONTR_RPT."-".$yearRange.'-'.processUserData($counter);
                }
                if($counterFor==CONTR_VCH){                    
                    $counter = $db->f('tra_counter');
                    $company_prefix = $db->f('company_prefix');     
                    $number = $company_prefix."-".CONTR_VCH."-".$yearRange.'-'.processUserData($counter);
                }
                if($counterFor =='FLW' ){
                   $counter = $db->f('flw_counter');
                   $company_prefix = $db->f('company_prefix');  
                   $number = $company_prefix.'-FLW-'.$yearRange.'-'.processUserData($counter);                           
                }
                /*
                if($counterFor =='PQT' ){
                    $counter = $db->f('pqt_counter');
                    $number = 'PT-PQT-'.$yearRange.'-'.processUserData($counter);             
                }*/
                
                if($counterFor =='QT' ){
                    $counter = $db->f('qt_counter');
                    $company_prefix = $db->f('company_prefix');
                    $number = $company_prefix.'-QT-'.$yearRange.'-'.processUserData($counter);             
                }
            }
        }
        return $number ;
    }
    
    function updateOldCounterOf(&$db,$counterFor,$company_id='',$financial_yr=''){
        
        $query = "SELECT "	. TABLE_FINANCIAL_YEAR .".id"
					  	.",". TABLE_FINANCIAL_YEAR .".year_range"
						//.",". TABLE_FINANCIAL_YEAR .".lead_counter"
						.",". TABLE_FINANCIAL_YEAR .".ord_counter"
						.",". TABLE_FINANCIAL_YEAR .".inv_counter"
						.",". TABLE_FINANCIAL_YEAR .".rct_counter"
						.",". TABLE_FINANCIAL_YEAR .".tra_counter"
                        //.",". TABLE_FINANCIAL_YEAR .".flw_counter"
						//.",". TABLE_FINANCIAL_YEAR .".pqt_counter"
						.",". TABLE_FINANCIAL_YEAR .".qt_counter"
                        .",". TABLE_FINANCIAL_YEAR .".company_prefix"
                        ." FROM ". TABLE_FINANCIAL_YEAR
                        ." WHERE ".TABLE_FINANCIAL_YEAR.".company_id ='".$company_id."' AND ".TABLE_FINANCIAL_YEAR.".year_range='".$financial_yr."' ORDER BY ".TABLE_FINANCIAL_YEAR.".date DESC LIMIT 1 " ;
					    
        $db->query( $query );
		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {            
               $id = $db->f('id');
            }
        }
        /*
        if($counterFor =='ORD' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".ord_counter = ".TABLE_FINANCIAL_YEAR.".ord_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id = ".$id ; 
                   
        }
        if($counterFor =='INV' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".inv_counter = ".TABLE_FINANCIAL_YEAR.".inv_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor ==CONTR_RPT ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".rct_counter = ".TABLE_FINANCIAL_YEAR.".rct_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor ==CONTR_VCH ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".tra_counter = ".TABLE_FINANCIAL_YEAR.".tra_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor =='FLW' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".flw_counter = ".TABLE_FINANCIAL_YEAR.".flw_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        
        if($counterFor =='QT' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".qt_counter = ".TABLE_FINANCIAL_YEAR.".qt_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }*/
        if($counterFor =='ORD' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".ord_counter = ".TABLE_FINANCIAL_YEAR.".ord_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id = ".$id ; 
                   
        }
        if($counterFor =='INV' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".inv_counter = ".TABLE_FINANCIAL_YEAR.".inv_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor == CONTR_RPT ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".rct_counter = ".TABLE_FINANCIAL_YEAR.".rct_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor == CONTR_VCH ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".tra_counter = ".TABLE_FINANCIAL_YEAR.".tra_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        if($counterFor =='FLW' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".flw_counter = ".TABLE_FINANCIAL_YEAR.".flw_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        /*
        if($counterFor =='PQT' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".pqt_counter = ".TABLE_FINANCIAL_YEAR.".pqt_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }*/
        
        if($counterFor =='QT' ){
            $update = " UPDATE ".TABLE_FINANCIAL_YEAR
            ." SET ".TABLE_FINANCIAL_YEAR.".qt_counter = ".TABLE_FINANCIAL_YEAR.".qt_counter + 1 WHERE ".TABLE_FINANCIAL_YEAR.".id= ".$id ; 
                   
        }
        $executed = $db->query( $update );
       
        
        return $executed;
    }
	
	function  getLeadsOrderCounterNumber(&$db, $financial_yr=''){
		$number='';
		if(!empty($financial_yr)){
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"					
						.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".lead_ord_counter"
					." FROM ". TABLE_FINANCIAL_YEAR_RANGE
					." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".year_range='".$financial_yr."' 
					ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;		
		}else{
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"					
						.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".lead_ord_counter"
					." FROM ". TABLE_FINANCIAL_YEAR_RANGE
					." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".is_financial_yr='1' 
					ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;		
		}	

	   $db->query( $query );		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {
			    $counter = $db->f('lead_ord_counter');
			    $yearRange = $db->f('year_range');                
                $number = 'LEAD-'.$yearRange.'-'.processUserData($counter);
			}			
		}
		return $number;
	}
	function updateLeadsOrderCounterNumber(&$db, $financial_yr=''){
	
		if(!empty($financial_yr)){
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"
					  	.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".lead_ord_counter"
						." FROM ". TABLE_FINANCIAL_YEAR_RANGE
                        ." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".year_range='".$financial_yr."' 
					  ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;
		}else{
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"
					  	.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".lead_ord_counter"
						." FROM ". TABLE_FINANCIAL_YEAR_RANGE
                        ." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".is_financial_yr='1' 
						ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;
		}			    
        $db->query( $query );
		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {            
               $id = $db->f('id');
            }
        }
		$update = " UPDATE ".TABLE_FINANCIAL_YEAR_RANGE
				." SET ".TABLE_FINANCIAL_YEAR_RANGE.".lead_ord_counter = ".TABLE_FINANCIAL_YEAR_RANGE.".lead_ord_counter + 1 
				WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".id= ".$id ; 
		
        $executed = $db->query( $update );   
		return $executed ;	
	}
	//Function for counter for Lead Order EOF
	//Function for counter for Prospects Order BOF	
	function  getProspectsOrderCounterNumber(&$db, $financial_yr=''){
		$number='';
		if(!empty($financial_yr)){
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"					
						.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".prospects_ord_counter"
					." FROM ". TABLE_FINANCIAL_YEAR_RANGE
					." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".year_range='".$financial_yr."' 
					ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;		
		}else{
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"					
						.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".prospects_ord_counter"
					." FROM ". TABLE_FINANCIAL_YEAR_RANGE
					." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".is_financial_yr='1' 
					ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;		
		}	

	   $db->query( $query );		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {
			    $counter = $db->f('prospects_ord_counter');
			    $yearRange = $db->f('year_range');                
                $number = 'PSLEAD-'.$yearRange.'-'.processUserData($counter);
			}			
		}
		return $number;
	}
	function updateProspectsOrderCounterNumber(&$db, $financial_yr=''){
	
		if(!empty($financial_yr)){
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"
					  	.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".prospects_ord_counter"
						." FROM ". TABLE_FINANCIAL_YEAR_RANGE
                        ." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".year_range='".$financial_yr."' 
					  ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;
		}else{
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"
					  	.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".prospects_ord_counter"
						." FROM ". TABLE_FINANCIAL_YEAR_RANGE
                        ." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".is_financial_yr='1' 
						ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;
		}			    
        $db->query( $query );
		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {            
               $id = $db->f('id');
            }
        }
		$update = " UPDATE ".TABLE_FINANCIAL_YEAR_RANGE
				." SET ".TABLE_FINANCIAL_YEAR_RANGE.".prospects_ord_counter 
				= ".TABLE_FINANCIAL_YEAR_RANGE.".prospects_ord_counter + 1 
				WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".id= ".$id ; 
		
        $executed = $db->query( $update );   
		return $executed ;	
	}
	//Function for counter for Prospect Order EOF
	
    function  getOrderCounterNumber(&$db, $financial_yr=''){
		$number='';
		if(!empty($financial_yr)){
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"					
						.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".ord_counter"
					." FROM ". TABLE_FINANCIAL_YEAR_RANGE
					." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".year_range='".$financial_yr."' 
					ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;		
		}else{
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"					
						.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".ord_counter"
					." FROM ". TABLE_FINANCIAL_YEAR_RANGE
					." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".is_financial_yr='1' 
					ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;		
		}	

	   $db->query( $query );		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {
			    $counter = $db->f('ord_counter');
			    $yearRange = $db->f('year_range');                
                $number = 'ORD-'.$yearRange.'-'.processUserData($counter);
			}			
		}
		return $number;
	}
	function updateOrderCounterNumber(&$db, $financial_yr=''){
	
		if(!empty($financial_yr)){
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"
					  	.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".ord_counter"
						." FROM ". TABLE_FINANCIAL_YEAR_RANGE
                        ." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".year_range='".$financial_yr."' 
					  ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;
		}else{
			$query = "SELECT ". TABLE_FINANCIAL_YEAR_RANGE .".id"
					  	.",". TABLE_FINANCIAL_YEAR_RANGE .".year_range"
						.",". TABLE_FINANCIAL_YEAR_RANGE .".ord_counter"
						." FROM ". TABLE_FINANCIAL_YEAR_RANGE
                        ." WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".is_financial_yr='1' 
						ORDER BY ".TABLE_FINANCIAL_YEAR_RANGE.".date DESC LIMIT 1 " ;
		}			    
        $db->query( $query );
		
		if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {            
               $id = $db->f('id');
            }
        }
		$update = " UPDATE ".TABLE_FINANCIAL_YEAR_RANGE
				." SET ".TABLE_FINANCIAL_YEAR_RANGE.".ord_counter = ".TABLE_FINANCIAL_YEAR_RANGE.".ord_counter + 1 
				WHERE ".TABLE_FINANCIAL_YEAR_RANGE.".id= ".$id ; 
		
        $executed = $db->query( $update );   
		return $executed ;
	
	}
	
    // common function to get record 
    function getRecord($table,$fields,$condition){
        $data = array();
        $db 				= new db_local;
        $query = "SELECT ".$fields." FROM ". $table.$condition ;
    
        $db->query($query);
       
        if ( $db->query($query) && $db->nf()>0 ) {
            while ( $db->next_record() ) {
                $data = processSQLData($db->result());         
            }        
        }
        return $data;
    }
   
    function createSideMenu(&$top_menu,&$sideMenu,$menu_id,$parentArr,$uptolevel,$variables){
        $db = new db_local;            
   		   		
        foreach($top_menu as $key=> $value){
       
            $title = $value['title'];
            $parent_id = $value['parent_id'];
            
            $id = $value['id'];
            $sub_menu = $value['menu_sub'];
            $level = $value['level'];
            //$url_str=$value['url'];
            $url_str="#";
            if(!empty($value['page'])){
                $url_str= THIS_DOMAIN."/".$value['page'] ;
            }
            //echo "url is".$url_str;
            if($uptolevel!=''){
                if($level <= $uptolevel){
                
                    if($parent_id==0){        
                    	           
                    	 $sideMenu.= "<tr>";          
                        //$sideMenu.= str_repeat("&nbsp;&nbsp;&nbsp;",$level)." ".$title." ".$cat_id." ";                        
                        
                        $sideMenu.= "<td id=cat'$id' class=menu-parent><a href=$url_str>".$title."</a></td>"; 
                        $sideMenu.= "</tr>"; 
                        
                    }elseif( $parent_id > 0 ){
                                
                              
                        if(in_array($parent_id,$parentArr) || in_array($page_id,$parentArr)){
                        	$sideMenu.= "<tr>";           
                            $sideMenu.= "<td id=cat'$id' class=menu-child><a href=$url_str>".str_repeat("&nbsp;&nbsp;&nbsp;",$level)." ".$title."</a></td>";
                            $sideMenu.= "</tr>";
                        }
                        else{
                        	$sideMenu.= "<tr>";  
                            $sideMenu.= "<td id=cat'$id' class=hidden><a href=$url_str>".str_repeat("&nbsp;&nbsp;&nbsp;",$level)." ".$title."</a></td>";
                            $sideMenu.= "</tr>";
                        }                        
                    } 
                       
                }
            }else{      
            	
            	//$sideMenu= "<table border=1 cellspacing=0 cellpadding=0 id=border-table>";      	 
                   if($parent_id==0){     
                   
                        //$sideMenu.= str_repeat("&nbsp;&nbsp;&nbsp;",$level)." ".$title." ".$cat_id." ";
                         $sideMenu.= "<tr>"; 
                         $sideMenu.= "<td class=menu-parent><a href=$url_str>".str_repeat("&nbsp;&nbsp;&nbsp;",$level)."".$title."</a></td>";                                                
                         $sideMenu.= "</tr>"; 
                       }elseif( $parent_id > 0 ){
                               
                         if(in_array($parent_id,$parentArr) || in_array($id,$parentArr)){
                         	
                           $sideMenu.= "<tr>";
                           $sideMenu.= "<td class=menu-child>".str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",$level)."<img src=".$variables['images']."/bullet1.jpg border=0 
                           					width=31px height=22px /><a href=$url_str>" .$title."</a></td>";
                           $sideMenu.= "</tr>";
                        }
                        else{
                        	$sideMenu.= "<tr>";
                           	$sideMenu.= "<td class=hidden><a href=$url_str>".str_repeat("&nbsp;&nbsp;&nbsp;",$level)." ".$title."</a></td>";
                           	$sideMenu.= "</tr>";
                        }
                    }   
                   
              //  $sideMenu.= "</table>";          
            }
         
            if(!empty($sub_menu)){
                    createSideMenu($sub_menu,$sideMenu,$id,$parentArr,$uptolevel,$variables);
            }           
        }
        
    }

    //BOF Breadcrumbs
    function getBreadcrumbs($id, &$breadcrumbs) {
		$db	= new db_local();
        
		$query = "SELECT ".TABLE_MENU.".*, ".TABLE_SETTINGS_STATICPAGES.".page_name FROM ".
                    TABLE_MENU. " LEFT JOIN ".TABLE_SETTINGS_STATICPAGES." ON ".TABLE_MENU.".page= ".TABLE_SETTINGS_STATICPAGES.".page_id
				WHERE ".TABLE_MENU.".id='".$id."'";
        
		if ( $db->query($query) && $db->nf()>0 && $db->next_record()) {
						
			$breadcrumbs[] = array( 'id' => $id, 
                                    'name' => processSqlData($db->f("title")),
                                    'page_name' => processSqlData($db->f("page_name")),
                                    'page_url' => str_replace(' ', '-', strtolower($db->f('page_name'))) .'.html'
								   );
			
			if ( $db->f("group_id") != '') {
				getBreadcrumbs($db->f("group_id"), $breadcrumbs);
			}
		}
		
		return (true);	
	}
	//EOF Breadcrumbs 
    function getValidStr($title){
        $titlename = str_replace(' ','_',$title);
        $titlename = preg_replace('/[^-A-Za-z0-9]/','', $titlename); 
        return ($titlename) ;
    }
    
    function allCountryList() {
		
		$countrylist	= NULL;
		$query= "SELECT  * FROM ". TABLE_COUNTRIES ." WHERE status='1' ORDER By title";
		$dbrec = new db_local();
		$dbrec->query($query);
		
		if( $dbrec->nf() > 0 )
		{
			while($dbrec->next_record()) {
				  $countrylist[$dbrec->f("code")]=$dbrec->f("title");
			}
		}
		$dbrec = NULL;
		return $countrylist;	
	}
	function getRandomAssociate($user_id=''){
		$userDetails = array();
		if(empty($user_id)){
			//Aryan Salve, Mohit Salve, Radha Goss, Ranjit Jha
			$query = "SELECT ".TABLE_USER.".f_name,".TABLE_USER.".l_name, ".TABLE_USER.".number,"
			.TABLE_USER.".user_id, ".TABLE_USER.".desig, ".TABLE_USER.".email, ".TABLE_USER.".marketing_contact 
			FROM ".TABLE_USER." WHERE ".TABLE_USER.".status='1' 
			AND ".TABLE_USER.".random_select='1' AND ".TABLE_USER.".user_id	NOT IN 			('e68adc58f2062f58802e4cdcfec0af2d','38570b0a7e923f3349e40a4bcc5f6e31','37e1ec916de1e59e284731ca5f097d6e',
			'251e5204a6bcb24a84e09eaa2072de6b','ff2be6098003d6f376617f17710207e9')
			ORDER BY rand() LIMIT 1 ";
		}else{
			$query = "SELECT ".TABLE_USER.".f_name,".TABLE_USER.".l_name, ".TABLE_USER.".number,"
			.TABLE_USER.".user_id,  ".TABLE_USER.".desig, ".TABLE_USER.".email, ".TABLE_USER.".marketing_contact FROM ".TABLE_USER." WHERE 
			".TABLE_USER.".user_id= '".$user_id."' LIMIT 0,1 ";
		}
		$db  = new db_local();
		$db ->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$userDetails['name'] = $db->f("f_name")." ".$db->f("l_name");
				$userDetails['user_id'] = $db->f("user_id");
				$userDetails['desig'] = $db->f("desig");		
				$userDetails['email'] = $db->f("email");				
				$userDetails['marketing_contact'] = $db->f("marketing_contact");					
				$userDetails['number'] = $db->f("number");				
			}
		}
		return $userDetails;
	}
	
	function getCeoDetails(){
		$userDetails = array();
		$query = "SELECT ".TABLE_USER.".f_name,".TABLE_USER.".l_name, "
		.TABLE_USER.".user_id, ".TABLE_USER.".desig, ".TABLE_USER.".email, ".TABLE_USER.".marketing_contact FROM ".TABLE_USER." WHERE 
		".TABLE_USER.".user_id = 'e68adc58f2062f58802e4cdcfec0af2d' "; 
		//get details of Mr Aryan Salve - CEO SMEERP E-Technologies Pvt. Ltd.
		
		$query = "SELECT ".TABLE_USER.".f_name,".TABLE_USER.".l_name, "
		.TABLE_USER.".user_id, ".TABLE_USER.".desig, ".TABLE_USER.".email, ".TABLE_USER.".marketing_contact FROM ".TABLE_USER." WHERE 
		".TABLE_USER.".user_id = '4df83558e5b6d3592d8998e9b552d37a' "; //Stella Goss 
		$db  = new db_local();
		$db ->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()){
				$userDetails['name'] = $db->f("f_name")." ".$db->f("l_name");
				$userDetails['user_id'] = $db->f("user_id");
				$userDetails['desig'] = $db->f("desig");				
				$userDetails['email'] = $db->f("email");				
				$userDetails['marketing_contact'] = $db->f("marketing_contact");				
			}
		}
		return $userDetails;	
	}
	
	function getPsQuotationNumber($do_i,$company_id){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$inv_counter='';
		$dateChk= date("Y-m-d",$do_i);
		//$dateChk=$dateChk." 00:00:00" ;
		
		$query = "SELECT MAX(".TABLE_PROSPECTS_QUOTATION.".inv_counter) as inv_counter 
			FROM ".TABLE_PROSPECTS_QUOTATION." WHERE date_format(".TABLE_PROSPECTS_QUOTATION.".do_i, '%Y-%m-%d') = '".$dateChk."' 
			AND ".TABLE_PROSPECTS_QUOTATION.".company_id = ".$company_id ;

		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$inv_counter = $db->f("inv_counter") ;
				$inv_counter = $inv_counter+1;
			}
		}
		$date = date("dmY",$do_i);
		$query = "SELECT ".TABLE_SETTINGS_COMPANY.".prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id=".$company_id ;
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$prefix = $db->f("prefix")  ;
			}
		}
		
		$data['number'] = $prefix."-PROP-".$date.$inv_counter ;
		$data['inv_counter'] = $inv_counter ;
		return $data ; 	
	}
	
	function getQuotationNumber($do_i,$company_id){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$inv_counter='';
		$dateChk= date("Y-m-d",$do_i);
		//$dateChk=$dateChk." 00:00:00" ;
		
		$query = "SELECT MAX(".TABLE_LEADS_QUOTATION.".inv_counter) as inv_counter 
			FROM ".TABLE_LEADS_QUOTATION." WHERE 
			date_format(".TABLE_LEADS_QUOTATION.".do_i, '%Y-%m-%d') ='".$dateChk."' 		
			AND ".TABLE_LEADS_QUOTATION.".company_id = ".$company_id ;

		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$inv_counter = $db->f("inv_counter") ;
				$inv_counter=$inv_counter+1;
			}
		}
		$date = date("dmY",$do_i);
		$query = "SELECT ".TABLE_SETTINGS_COMPANY.".prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id=".$company_id ;
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$prefix = $db->f("prefix")  ;
			}
		}
		
		$data['number'] = $prefix."-QT-".$date.$inv_counter ;
		$data['inv_counter'] = $inv_counter ;
		return $data ; 
	
	}
	
	function getInvoiceProformaNumber($do_i,$company_id){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$inv_counter='';
		$dateChk= date("Y-m-d",$do_i);
		//$dateChk=$dateChk." 00:00:00" ;
		
		$query = "SELECT MAX(".TABLE_BILL_INV_PROFORMA.".inv_counter) as inv_counter 
			FROM ".TABLE_BILL_INV_PROFORMA." WHERE 
			date_format(".TABLE_BILL_INV_PROFORMA.".do_i, '%Y-%m-%d') ='".$dateChk."' 
			AND ".TABLE_BILL_INV_PROFORMA.".company_id = ".$company_id ;

		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$inv_counter = $db->f("inv_counter") ;
				$inv_counter=$inv_counter+1;
			}
		}
		$date = date("dmY",$do_i);
		$query = "SELECT ".TABLE_SETTINGS_COMPANY.".prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id=".$company_id ;
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$prefix = $db->f("prefix")  ;
			}
		}
		
		$data['number'] = $prefix."-PROINV-".$date.$inv_counter ;
		$data['inv_counter'] = $inv_counter ;
		return $data ; 
	
	}
	function getInvoiceNumber($do_i,$company_id){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$inv_counter='';
		$dateChk= date("Y-m-d",$do_i);
		//$dateChk=$dateChk." 00:00:00" ;
		
		$query = "SELECT MAX(".TABLE_BILL_INV.".inv_counter) as inv_counter 
			FROM ".TABLE_BILL_INV." WHERE 
			date_format(".TABLE_BILL_INV.".do_i, '%Y-%m-%d') ='".$dateChk."'  
			AND ".TABLE_BILL_INV.".company_id = ".$company_id ;

		$db  = new db_local();
		$db ->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$inv_counter = $db->f("inv_counter") ;
				$inv_counter=$inv_counter+1;
			}
		}
		$date = date("dmY",$do_i);
		$query = "SELECT ".TABLE_SETTINGS_COMPANY.".prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id=".$company_id ;
		$db  = new db_local();
		$db ->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$prefix = $db->f("prefix")  ;
			}
		}
		
		$data['number'] = $prefix."-INV-".$date.$inv_counter ;
		$data['inv_counter'] = $inv_counter ;
		return $data ; 	
	}
	
	function getReceiptNumber($do_r,$company_id){
		$data['number'] = '';
		$data['counter'] = '';
		$dateChk= date("Y-m-d",$do_r);
		//$date=$date." 00:00:00" ;
		$query = "SELECT MAX(".TABLE_BILL_RCPT.".rcpt_counter) as rcpt_counter 
			FROM ".TABLE_BILL_RCPT." WHERE  
			date_format(".TABLE_BILL_RCPT.".do_r, '%Y-%m-%d') ='".$dateChk."' 
			AND ".TABLE_BILL_RCPT.".company_id = '".$company_id."'" ;

		$db  = new db_local();
		$db ->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$rcpt_counter = $db->f("rcpt_counter")  ;
				$rcpt_counter = $rcpt_counter +1  ;
			}
		}
		$date = date("dmY",$do_r);
		$query = "SELECT ".TABLE_SETTINGS_COMPANY.".prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id=".$company_id ;
		$db  = new db_local();
		$db ->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$prefix = $db->f("prefix")  ;
			}
		}		
		$data['number'] = $prefix."-RPT-".$date.$rcpt_counter ;
		$data['rcpt_counter'] = $rcpt_counter ;
		return $data ; 	
	}
	
	function getDmNumber($do_i,$company_id){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$dm_counter='';
		$dateChk= date("Y-m-d",$do_i);
		//$dateChk=$dateChk." 00:00:00" ;
		
		$query = "SELECT MAX(".TABLE_BILL_INV_PROFORMA.".dm_counter) as dm_counter 
			FROM ".TABLE_BILL_INV_PROFORMA." WHERE 
			date_format(".TABLE_BILL_INV_PROFORMA.".dm_dt, '%Y-%m-%d') ='".$dateChk."' 
			AND ".TABLE_BILL_INV_PROFORMA.".company_id = ".$company_id ;

		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$dm_counter = $db->f("dm_counter") ;
				$dm_counter=$dm_counter+1;
			}
		}
		$date = date("dmY",$do_i);
		$query = "SELECT ".TABLE_SETTINGS_COMPANY.".prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id=".$company_id ;
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$prefix = $db->f("prefix")  ;
			}
		}
		
		$data['number'] = $prefix."-DM-".$date.$dm_counter ;
		$data['dm_counter'] = $dm_counter ;
		return $data ; 
	}
	function getLetterNumber($do_i,$company_id){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix = $counter = '';
		$dateChk= date("Y-m-d",$do_i); 
		
		$query = "SELECT MAX(".TABLE_LETTERS.".counter) as counter 
			FROM ".TABLE_LETTERS." WHERE 
			date_format(".TABLE_LETTERS.".do_l, '%Y-%m-%d') ='".$dateChk."' 
			AND ".TABLE_LETTERS.".company_id = ".$company_id ;

		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()){
				$counter = $db->f("counter") ;
				$counter = $counter + 1;
			}
		}
		$date = date("dmY",$do_i);
		$query = "SELECT ".TABLE_SETTINGS_COMPANY.".prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id=".$company_id ;
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$prefix = $db->f("prefix")  ;
			}
		}
		
		//$data['number'] = $prefix."-LT-".$date.$counter ;
		$data['number'] = $prefix."-".$date.$counter ;
		$data['counter'] = $counter ;
		return $data ; 
	}
	function getOrderNumber($do_o){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$ord_counter='';
		$dateChk= date("Y-m-d",$do_o);
//		$dateChk=$dateChk." 00:00:00" ;
		
		/* $query = "SELECT MAX(".TABLE_BILL_ORDERS.".ord_counter) as ord_counter 
			FROM ".TABLE_BILL_ORDERS." WHERE do_o='".$dateChk."' "	  ;
		*/
			
		$query = "SELECT MAX(".TABLE_BILL_ORDERS.".ord_counter) as ord_counter 
		FROM ".TABLE_BILL_ORDERS." 
		WHERE date_format(".TABLE_BILL_ORDERS.".do_o, '%Y-%m-%d') ='".$dateChk."' "	  ; 
			
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$ord_counter = $db->f("ord_counter") ;
				$ord_counter=$ord_counter+1;
			}
		}
		$date = date("dmY",$do_o);
		 
		$data['number'] = "ORD-".$date.$ord_counter ;
		$data['ord_counter'] = $ord_counter ;
		return $data ; 
	}
	
	function getLeadNumber($do_o){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$ord_counter='';
		$dateChk= date("Y-m-d",$do_o);
		//$dateChk=$dateChk." 00:00:00" ;
		
		/* 	
		$query = "SELECT MAX(".TABLE_LEADS_ORDERS.".ord_counter) as ord_counter 
		FROM ".TABLE_LEADS_ORDERS." WHERE do_o='".$dateChk."' "	  ; 
		*/
		
		$query = "SELECT MAX(".TABLE_LEADS_ORDERS.".ord_counter) as ord_counter 
		FROM ".TABLE_LEADS_ORDERS." WHERE 
		date_format(".TABLE_LEADS_ORDERS.".do_o, '%Y-%m-%d') ='".$dateChk."' "	  ;
		
		
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$ord_counter = $db->f("ord_counter") ;
				$ord_counter=$ord_counter+1;
			}
		}
		$date = date("dmY",$do_o);
		 
		$data['number'] = "LEAD-".$date.$ord_counter ;
		$data['ord_counter'] = $ord_counter ;
		return $data ; 
	}
	function getProspectLeadNumber($do_o){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$ord_counter='';
		$dateChk = date("Y-m-d",$do_o);
		//$dateChk = $dateChk." 00:00:00" ;
		
		/* $query = "SELECT MAX(".TABLE_PROSPECTS_ORDERS.".ord_counter) as ord_counter 
			FROM ".TABLE_PROSPECTS_ORDERS." WHERE do_o='".$dateChk."' "	  ; */
		
		$query = "SELECT MAX(".TABLE_PROSPECTS_ORDERS.".ord_counter) as ord_counter 
		FROM ".TABLE_PROSPECTS_ORDERS." 
		WHERE date_format(".TABLE_PROSPECTS_ORDERS.".do_o, '%Y-%m-%d') ='".$dateChk."' "	  ;
		
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$ord_counter = $db->f("ord_counter") ;
				$ord_counter=$ord_counter+1;
			}
		}
		$date = date("dmY",$do_o);
		 
		$data['number'] = "PSLEAD-".$date.$ord_counter ;
		$data['ord_counter'] = $ord_counter ;
		return $data ; 
	}
	function getPoPdfNumber($do_o){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$po_counter='';
		$dateChk = date("Y-m-d",$do_o); 
		
		$query = "SELECT MAX(".TABLE_PURCHASE_ORDER_PDF.".inv_counter) as po_counter 
			FROM ".TABLE_PURCHASE_ORDER_PDF." WHERE 
			date_format(".TABLE_PURCHASE_ORDER_PDF.".do_i, '%Y-%m-%d') ='".$dateChk."' " ;		
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$po_counter = $db->f("po_counter") ;
				$po_counter = $po_counter+1;
			}
		}
		$date = date("dmY",$do_o); 
		$data['number'] = "POP-".$date.$po_counter ;
		$data['inv_counter'] = $po_counter ;
		return $data ;  
	}
	function getPoNumber($do_o){
		$data['number'] = '';
		$data['counter'] = '';
		$prefix=$po_counter='';
		$dateChk = date("Y-m-d",$do_o);
		 
		
		$query = "SELECT MAX(".TABLE_PURCHASE_ORDER.".counter) as counter 
			FROM ".TABLE_PURCHASE_ORDER." WHERE 
			date_format(".TABLE_PURCHASE_ORDER.".bill_dt, '%Y-%m-%d') ='".$dateChk."' "	  ;
		
		$db  = new db_local();
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()) {
				$po_counter = $db->f("counter") ;
				$po_counter = $po_counter+1;
			}
		}
		$date = date("dmY",$do_o);
		 
		$data['number'] = "PO-".$date.$po_counter ;
		$data['counter'] = $po_counter ;
		return $data ;  
	}
	
	/*
		This function is used to add the follow-up against the Lead
		Parameters in array data 
		@ticket_no,@flw_ord_id,@ticket_owner_uid,@ticket_owner,@tck_owner_member_id,@tck_owner_member_name,@tck_owner_member_email
		@ticket_creator_uid,@ticket_creator,@ticket_subject,@ticket_text,@ticket_id,@display_name,@display_user_id,@display_designation
	*/
	function updateProspectLeadTicket(&$data){
	
		$db  = new db_local() ; 
		$data['hrs']='';
		$data['min']=5;  
		$hrs1 = (int) ($data['hrs'] * 6);
		$min1 = (int) ($data['min'] * 6);   
		
		if ( $data['ticket_id']>0 ) {
		
			$followup_query = "INSERT INTO ". TABLE_PROSPECTS_TICKETS ." SET "
			. TABLE_PROSPECTS_TICKETS .".ticket_no  = '". $data['ticket_no'] ."', "
			. TABLE_PROSPECTS_TICKETS .".flw_ord_id = '". $data['flw_ord_id'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_owner = '". $data['ticket_owner'] ."', "	 
			. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
			. TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
			. TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $data['ticket_creator_uid']."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $data['ticket_creator']."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_status     = '".ProspectsTicket::PENDINGWITHCLIENTS ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_child      = '".$data['ticket_id']."',"
			. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
			. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
			. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
			. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
			. TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
			. TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
			. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
			. TABLE_PROSPECTS_TICKETS .".display_user_id        = '".$data['display_user_id'] ."', "
			. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
			. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
			. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
			. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;	
			$db->query($followup_query) ;			 

		}else{
		
			$followup_query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
			. TABLE_PROSPECTS_TICKETS .".ticket_no   = '". $data['ticket_no'] ."', "
			. TABLE_PROSPECTS_TICKETS .".flw_ord_id         = '". $data['flw_ord_id'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
			. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
			. TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
			. TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $data['ticket_creator_uid'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $data['ticket_creator'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_status     = '".ProspectsTicket::PENDINGWITHCLIENTS ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_child      = '0',"
			. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
			. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
			. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
			. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
			. TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
			. TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
			. TABLE_PROSPECTS_TICKETS .".display_name           = '".$data['display_name']."', "
			. TABLE_PROSPECTS_TICKETS .".display_user_id        = '".$data['display_user_id'] ."', "
			. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
			. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
			. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
			. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ; 
			$db->query($followup_query) ; 
		} 
	} 	
	
	function remove_array_empty_values($array, $remove_null_number = true){
		$new_array = array();  
		$null_exceptions = array();  
		foreach ($array as $key => $value){
			$value = trim($value);
	  
			if($remove_null_number){
				 $null_exceptions[] = '0';
			}
	  
			if(!in_array($value, $null_exceptions) && $value != ""){
				 $new_array[] = $value;
			}
		}
		return $new_array;
	}
?>