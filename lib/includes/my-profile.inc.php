<?php
	class Myprofile {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => User::BLOCKED,
							'ACTIVE'  => User::ACTIVE,
							'PENDING' => User::PENDING,
							'DELETED' => User::DELETED
						);
			return ($status);
		}
		
        
		/**
		 *	Function to get all the Executives.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_USER;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}
		}
		/**
		 *	Function to get all the timesheet details of my.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
        function getTimesheetDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_USER_TIMESHEET;
            $query .= " LEFT JOIN ". TABLE_BILL_ORDERS
                        ." ON ". TABLE_USER_TIMESHEET .".order_id = ". TABLE_BILL_ORDERS .".id ";
             $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
               return ( $total );
            }
            else {
                return false;
            }   
        }
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateProfile(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
 
			//old password
			if( empty($data['oldpassword']))
				$messages->setErrorMessage('Old Password cannot be empty.');

			if( empty($data['newpassword']))
				$messages->setErrorMessage('New Password cannot be empty.');

            // Password.
            if ( isset($data['newpassword']) && !empty($data['newpassword']) ) {
                if ( !isset($data['retypepass']) || empty($data['retypepass']) ) {
                    $messages->setErrorMessage('The Repeat Password cannot be empty.');
                }
                elseif ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['newpassword']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                elseif ( $data['newpassword'] != $data['retypepass'] ) {
                    $messages->setErrorMessage('The entered Password do not match.');
                }
                else {
                    $data['newpassword'] = TABLE_USER .".password = '". encParam($data['newpassword']) ."'";
                }
				// Check whether old password entered is correct or not
				if( Myprofile::checkPassword($data,TABLE_USER) )
					$messages->setErrorMessage('The Old Password you have entered is wrong..');
            }
            else {
                $data['newpassword'] = '';
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
        function validateTimesheet(&$data, $extra=''){
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
 		
             //old password
			if( empty($data['order_id']))
				$messages->setErrorMessage('Please select project.');
                
            if( empty($data['hrs']) && empty($data['min']))
				$messages->setErrorMessage('Please select total hrs.');
            
            if(empty($data['hrs']) && !empty($data['min']) && $data['min'] < 10){
                $messages->setErrorMessage('Please select minimum 10 min.');
            }
            if( empty($data['date']))
				$messages->setErrorMessage('Please select date.');
            
            if( empty($data['note']))
				$messages->setErrorMessage('Please add work summary.');

            /*
            if( empty($data['st_hr']))
				$messages->setErrorMessage('Please select start time.');
            if( empty($data['ed_hr']))
				$messages->setErrorMessage('Please select end time.');
            if( !empty($data['st_hr']) &&  !empty($data['ed_hr'])){
                //if st type is AM
                if($data['st_type']==1){
                    $starttm=$data['st_hr'].$data['st_min'];
                    
                    if($starttm < 700 ){
                         $messages->setErrorMessage('Invalid start time.');
                    }
                }
                //if st type is AM
                if($data['st_type']==2){
                    $starttm=$data['st_hr'].$data['st_min'];
                    if($starttm > 1100 ){
                         $messages->setErrorMessage('Invalid start time.');
                    }
                }
                //if edtype is AM
                if($data['ed_type']==1){
                    $endtm=$data['ed_hr'].$data['ed_min'];
                    if($endtm < 700 ){
                         $messages->setErrorMessage('Invalid end time.');
                    }
                }
                //if edtype is AM
                if($data['ed_type']==2){
                    $endtm=$data['ed_hr'].$data['ed_min'];
                    if($endtm > 1100 ){
                         $messages->setErrorMessage('Invalid end time.');
                    }
                }
                if($data['st_type']!=$data['ed_type']){
                    if($data['st_type']==2) $data['st_hr']=$data['st_hr']+12;
                    if($data['ed_type']==2) $data['ed_hr']=$data['ed_hr']+12;
             
                }
                
                $sttime=$data['st_hr'].$data['st_min'] ;
                $edtime=$data['ed_hr'].$data['ed_min'] ;
               
                if($edtime <= $sttime){
                    $messages->setErrorMessage('Please check start time and end time.');
                }
            }
            */
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        
        }
        function validateUserProfile(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
 
			//old password
			if( empty($data['oldpassword']))
				$messages->setErrorMessage('Old Password cannot be empty.');

			if( empty($data['newpassword']))
				$messages->setErrorMessage('New Password cannot be empty.');

            // Password.
            if ( isset($data['newpassword']) && !empty($data['newpassword']) ) {
                if ( !isset($data['retypepass']) || empty($data['retypepass']) ) {
                    $messages->setErrorMessage('The Repeat Password cannot be empty.');
                }
                elseif ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['newpassword']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                elseif ( $data['newpassword'] != $data['retypepass'] ) {
                    $messages->setErrorMessage('The entered Password do not match.');
                }
                else {
                    $data['newpassword'] = TABLE_CLIENTS .".password = '". encParam($data['newpassword']) ."'";
                }
				// Check whether old password entered is correct or not
				if( Myprofile::checkPassword($data,TABLE_CLIENTS) )
					$messages->setErrorMessage('The Old Password you have entered is wrong..');
            }
            else {
                $data['newpassword'] = '';
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}

		function checkPassword($data,$table)
		{
			$db = new db_local;

			$sql = "SELECT user_id FROM ".$table." WHERE password='".encParam($data['oldpassword'])."'";
			//echo $sql;
			$db->query($sql);
			if( $db->nf() > 0 )
				return false;
			else
				return true;
		}
}
?>