<?php
	class Followup {
		
		const LEADENTRY = 1;
		const LEADTM  = 2;
		const LEADM  = 3;
		const LEADQUOT = 4;
        const CLIENT = 5;
        const ORDER = 6;
        const INVOICE = 7;
        const RECEIPT = 8;
       
                	
        function getTypes() {
			$types = array('REJECTED' => Followup::LEADENTRY,
                           'ACTIVE'   => Followup::LEADASSIGNTM,
                           'APPROVED' => Followup::LEADASSIGNM,
						);
			return ($types);
		}
        
        //validation for followup
        function validateFollowupAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
            if ( !isset($data['title_id']) || empty($data['title_id']) ) {
                $messages->setErrorMessage('Title cannot be empty.');
            }
            
            if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage('Remarks cannot be empty.');
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}

        }
       
		//Used in communication log
	    function getDetailsCommLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SALE_FOLLOWUP;
            $query .= " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_SALE_FOLLOWUP .".created_by  ";
			$query .= " LEFT JOIN ". TABLE_SALE_FOLLOWUP_TITLE
                        ." ON ". TABLE_SALE_FOLLOWUP_TITLE .".id = ". TABLE_SALE_FOLLOWUP .".title_id  ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
               return ( $total );
            }
            else {
                return false;
            }   
        }
		
    }
?>
