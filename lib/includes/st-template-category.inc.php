<?php
	class StTemplateCategory {
		
		const DEACTIVE = 0;
		const ACTIVE  = 1;      
        
		const NONE = 0; 
		const ST  = 1;  //Client/Vendor Comm   
		const BST  = 2; //Accounts Comm  
		const LST  = 3; //Marketing Sale Comm  
		const PST  = 4; //CEO MArketing Comm  
		const WP  = 5; //CEO MArketing Comm  
		
        function getStatus() {
			$status = array(
							'ACTIVE'   => StTemplateCategory::ACTIVE,
                            'DEACTIVE'    => StTemplateCategory::DEACTIVE
						);
			return ($status);
		}  
		
        
		
		 
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_ST_TEMPLATE_CATEGORY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_ST_TEMPLATE_CATEGORY;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  
		function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;			
			if ( (StTemplateCategory::getList($db, $details , TABLE_ST_TEMPLATE_CATEGORY.'.id' 
				, " WHERE ".TABLE_ST_TEMPLATE_CATEGORY.".id = '$id'")) > 0 ) {
				$details = $details[0];
				$query = " DELETE FROM ".TABLE_ST_TEMPLATE_CATEGORY." WHERE id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage("The Record has been deleted.");  
				} else {
					$messages->setErrorMessage("The Record was not deleted.");
				}
			}else{
				$messages->setErrorMessage("The Record was not found.");			
			}  
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
    
    
       
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			} 
			                
            if ( !isset($data['category']) || empty($data['category']) ) {
                $messages->setErrorMessage("Category can not be empty.");
            }
/* 			if ( !isset($data['details']) || empty($data['details']) ) {
                $messages->setErrorMessage("Details can not be empty.");
            }   */
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
            if ( !isset($data['category']) || empty($data['category']) ) {
                $messages->setErrorMessage("Title can not be empty.");
            }
			
			/* if ( !isset($data['details']) || empty($data['details']) ) {
                $messages->setErrorMessage("Details can not be empty.");
            } */ 
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }else {
                return (false);
            }
        }
        
       
        /**
         * This function is used to update the status of the Executive.
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}      

			$query = "UPDATE ". TABLE_ST_TEMPLATE_CATEGORY
						." SET status = '$status_new' "
						." WHERE id = '$id'";
			if ( $db->query($query) && $db->affected_rows()>0 ) {
				$messages->setOkMessage("The Status has been updated.");
			} else {
				$messages->setErrorMessage("The Status was not updated.");
			}			 
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        
		
    }
?>
