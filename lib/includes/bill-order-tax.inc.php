<?php 

class BillOrderTax {
		 
		
    function getRestrictions($type='') {
		// 1024 * 1024 = 1048576 = 1MB
        $restrictions = array(
							'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), 
							'MIME'      => array('text/csv','text/comma-separated-values','application/vnd.ms-excel', 'text/plain','text/x-csv','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
							'EXT'       => array('csv', 'txt','xlsx')
						);
        
        if ( empty($type) ) {
            return ($restrictions);
        }
        else {
            return($restrictions[$type]);
        }
    }
    
    function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}else{
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_BILL_ORD_TAX." LEFT JOIN ".TABLE_BILL_ORDERS." ON 
				".TABLE_BILL_ORD_TAX.".or_no = ".TABLE_BILL_ORDERS.".number ";
			
            $query .= " ". $condition;
	  
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
    function getTables(&$db) {
        $count  = 0;
        $tables = array();
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[] = $temp[$i]['table_name'];
        }
        $temp   = NULL;
        return($tables);
    }
    
    
    function getTablesData(&$db, &$tables) {
        $count  = 0;
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[$i] = array('table_name'=> $temp[$i]['table_name'],
                                'fields'    => $db->metadata($temp[$i]['table_name'])
                               );
        }
        $temp   = NULL;
        return($count);
    }
    
	function validateAdd($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
		if(empty($data['email'])){
			$messages->setErrorMessage('Enter valid Email address.');
		}
		if ( !isValidEmail($data['email']) ){
			$messages->setErrorMessage('Email Address is not valid.');
		}else{
			$table = TABLE_BILL_ORD_TAX;
			if ( BillOrderTax::duplicateFieldValue($db, $table, 'email', $data['email']) ) {
				$messages->setErrorMessage('Email '.$data['email']." is already exist in database");
			}
		}
		if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        } else {
            return (false);
        }
    }
	
	function validateUpdate($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
		if(empty($data['email'])){
			$messages->setErrorMessage('Enter valid Email address.');
		}
		if ( !isValidEmail($data['email']) ){
			$messages->setErrorMessage('Email Address is not valid.');
		}else{
			$table = TABLE_BILL_ORD_TAX;
			
			$condition_query1=" WHERE ".TABLE_BILL_ORD_TAX.".email='".$data['email']."' 
			AND ".TABLE_BILL_ORD_TAX.".id!=".$data['id'] ;
			if ( BillOrderTax::getList( $db, $list, 'id', $condition_query1)>1 ) {
				$messages->setErrorMessage('Email '.$data['email']." is already exist in database");
			}
		}
		if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        } else {
            return (false);
        }
    }
	
    function validateUpload($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }

        if ( empty($file['file_csv']['error']) ) {
            if ( !empty($file['file_csv']['tmp_name']) ) {
                if ( $file['file_csv']['size'] == 0 ) {
                    $messages->setErrorMessage('The Uploaded file is empty.');
                }
                if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
                    $messages->setErrorMessage('The File type is not allowed to be uploaded.');
                }
                if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
                    $messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
                }
            }
            else {
                $messages->setErrorMessage('Server Error: File was not uploaded.');
            }
        }
        else {
            $messages->setErrorMessage('Unexpected Error: File was not uploaded.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    function duplicateFieldValue(&$db, $table, $field_name, $condition) {
        $query = "SELECT ".$field_name." FROM ".$table." ".$condition;
        
        if ( $db->query($query) && $db->nf()>0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
	
	 
	
	 
}
?>