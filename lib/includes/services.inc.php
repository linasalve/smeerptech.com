<?php 

	class Services {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;
		
		
		//Used ServiceType
		const PREMIUM_EMAIL_ALLOTMENT 	= 'premium_email_allotment';
		const PREMIUM_EMAIL_SERVICE 	= 'premium_email';
		const ENTERPRISE_EMAIL_SERVICE 	= 'enterprise_email';
		
		function getServiceType(){
			$type = array('PREMIUM_EMAIL_ALLOTMENT' => Services::PREMIUM_EMAIL_ALLOTMENT);
			return ($type);
		}
		
		function getStatus() {
			$status = array('BLOCKED' => Services::BLOCKED,
							'ACTIVE'  => Services::ACTIVE,
							'PENDING' => Services::PENDING,
							'DELETED' => Services::DELETED
						);
			return ($status);
		}
		
        function getNewAccNumber(&$db){
            //$user_number= 6000;
            $user_number = 100;
            $query      = "SELECT MAX(number) AS number FROM ". TABLE_SETTINGS_SERVICES;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("number") > 0){
                $user_number = $db->f("number");
            }
			$user_number++;
            return $user_number;
        }
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SETTINGS_SERVICES;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
                
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				 return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Services::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Services::getList($db, $user, 'ss_id', " WHERE ss_id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_SETTINGS_SERVICES
								." SET ss_status = '$status_new' "
								." WHERE ss_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Service was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (Services::getList($db, $user, 'ss_id', " WHERE ss_id = '$id'")) > 0 ) 
			{
                $user = $user[0];
                if ( $user['status'] == Services::BLOCKED || $user['status'] == Services::DELETED) 
				{
					 $query = "UPDATE ". TABLE_SETTINGS_SERVICES." SET ss_status ='".Services::DELETED."'"
								." WHERE ss_id = '". $id ."'";
                    
					if ( !$db->query($query) || $db->affected_rows()<=0 ){                        
						$messages->setErrorMessage("The Service is not deleted.");
					}else{
	                    //$sql ="DELETE FROM ".TABLE_SETTINGS_SERVICES_PRICE." WHERE service_id='".$id."'";
                        //$db->query($sql) ;
                        
                        $messages->setOkMessage("The Service has been deleted.");
                    }
				}
				else
					$messages->setErrorMessage("Cannot delete service.");
			}
			else
				$messages->setErrorMessage("Service not found.");
		}
        
        function validateSubAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$files = $data['files'];
			$service = NULL;
            //validate the service division
            if(empty($data['ss_parent_id'])){   
                $messages->setErrorMessage('Service should be selected.');
            }
			//validate the service title 
            if ( !isset($data['ss_title']) || empty($data['ss_title']) ) {
                $messages->setErrorMessage('Sub Service title should be provided.');
            }
			if ( !isset($data['ss_particulars']) || empty($data['ss_particulars']) ) {
                $messages->setErrorMessage('Sub Service Particulars should be provided.');
            }
            $data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){
                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 1 is greater than 1Mb');
                }
            }
            $data['file_2'] = '';           
            if((!empty($files['file_2']['name']))){
                
                $filename = $files['file_2']['name'];
                $data['file_2'] = $filename;
                $type = $files['file_2']['type'];
                $size = $files['file_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 2 is greater than 1Mb');
                }
            }
			$data['file_3'] = '';           
            if((!empty($files['file_3']['name']))){
                
                $filename = $files['file_3']['name'];
                $data['file_3'] = $filename;
                $type = $files['file_3']['type'];
                $size = $files['file_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 3 is greater than 1Mb');
                }
            }
			$service = NULL;
            //validate the service division
            
			//validate the service title 
            
            
			//check for same service title name
			$service_title = trim($data["ss_title"]);
			$ss_parent_id = trim($data["ss_parent_id"]);
            
            if ( (Services::getList($db, $service, 'ss_title', " WHERE ss_title = '$service_title' AND ss_parent_id = '$ss_parent_id' ")) > 0 )
				$messages->setErrorMessage('Sub Service already exists. Please change the title.');
            
            //code to check price values BOf   
              
            for ( $i=0; $i < count($data['service_price']); $i++ ) {                        
                if ( empty($data['service_price'][$i]) ) {
                    $messages->setErrorMessage('The Price field at Position '. ($i+1) .' cannot be empty.');
                } else {
                    if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['service_price'][$i]) ) {
                        $messages->setErrorMessage('The Price field at Position '. ($i+1) .' is not valid.');
                    }
                }                    
                if(!empty($data['service_price'][$i])){
                    if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['service_price'][$i]) ) {
                        $messages->setErrorMessage('The Price field at Position '. ($i+1) .' is not valid.');
                    }
                }
            }         
                         
            //code to check price values eOf            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
        function validateSubUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			$files = $data['files'];
			$service = NULL;
            //validate the service division
            if(empty($data['ss_parent_id'])){   
                $messages->setErrorMessage('Service should be selected.');
            }
			//validate the service title 
            if ( !isset($data['ss_title']) || empty($data['ss_title']) ) {
                $messages->setErrorMessage('Sub Service title should be provided.');
            }
			if ( !isset($data['ss_particulars']) || empty($data['ss_particulars']) ) {
                $messages->setErrorMessage('Sub Service Particulars should be provided.');
            }
            $data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 1 is greater than 1Mb');
                }
            }
            $data['file_2'] = '';           
            if((!empty($files['file_2']['name']))){
                
                $filename = $files['file_2']['name'];
                $data['file_2'] = $filename;
                $type = $files['file_2']['type'];
                $size = $files['file_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 2 is greater than 1Mb');
                }
            }
			$data['file_3'] = '';           
            if((!empty($files['file_3']['name']))){
                
                $filename = $files['file_3']['name'];
                $data['file_3'] = $filename;
                $type = $files['file_3']['type'];
                $size = $files['file_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 3 is greater than 1Mb');
                }
            }
            
            //check for same service title name
			$service_title = trim($data["ss_title"]);
			$ss_parent_id = trim($data["ss_parent_id"]);
            $sid = $data["ss_id"];
            if ( (Services::getList($db, $service, 'ss_title', " WHERE ss_title = '$service_title' AND ss_parent_id = '$ss_parent_id' AND ss_id !='$sid'")) > 0 ){
				$messages->setErrorMessage('Sub Service already exists. Please change the title.');
            }
            
            //code to check price values BOf 
            for ( $i=0; $i < count($data['service_price']); $i++ ) {                        
                if ( empty($data['service_price'][$i]) ) {
                    $messages->setErrorMessage('The Price field at Position '. ($i+1) .' cannot be empty.');
                } else {
                    if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['service_price'][$i]) ) {
                        $messages->setErrorMessage('The Price field at Position '. ($i+1) .' is not valid.');
                    }
                }                    
                if(!empty($data['service_price'][$i])){
                    if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['service_price'][$i]) ) {
                        $messages->setErrorMessage('The Price field at Position '. ($i+1) .' is not valid.');
                    }
                }
            }             
            //code to check price values eOf       
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			$service = NULL;
            
			//validate the service title 
            if ( !isset($data['ss_title']) || empty($data['ss_title']) ) 
                $messages->setErrorMessage('Service title should be provided.');
			
            //validate the service division
            /*
            if(!isset($data['ss_division']) || empty($data['ss_division'])){
                $messages->setErrorMessage('Service division should be provided.');
            }
			*/
             //validate the service division
          
            if(!isset($data['ss_punch_line']) || empty($data['ss_punch_line'])){
                $messages->setErrorMessage('Punchline should be provided.');
            }
			//check for same service title name
			$service_title = trim($data["ss_title"]);
            
            if ( (Services::getList($db, $service, 'ss_title', " WHERE ss_title = '$service_title' && ss_parent_id=0")) > 0 ){
				$messages->setErrorMessage('Service title name already exists. Please change the title.');
            }
			
            if( !empty($data['tax1_name']) && empty($data['tax1_value']) ) {
                  $messages->setErrorMessage('Select tax value.');
            }
            if( !empty($data['tax1_value']) && empty($data['tax1_name']) ) {
                  $messages->setErrorMessage('Select tax name as you select the tax value.');
            }
			// added in  2010-10-oct-26
			if(!empty($data['tax1_name']) && !empty($data['tax1_value'])){
				//check sub tax1 and 2
				$condition_query=" WHERE status = '". ACTIVE ."' AND parent_id=".$data['tax1_id'] ;
				ServiceTax::getList($db, $taxc_list, 'count(*) as count', $condition_query);
				if(!empty($taxc_list)){
					$count = $taxc_list[0]['count'] ; 
					for($i=1;$i<=$count;$i++){
						$name1='tax1_sub'.$i.'_name';
						$val1='tax1_sub'.$i.'_value';
					
						if(empty($data[$name1]) || empty($data[$val1]) ){
							$messages->setErrorMessage('Select sub tax name'.$i.' and its value.');
						}
					}
				}
				
			}
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			//validate the service title 
            if ( !isset($data['ss_title']) || empty($data['ss_title']) ) {
                $messages->setErrorMessage('Service title should be provided.');
            }           
            /*
            if ( !isset($data['ss_division']) || empty($data['ss_division']) ) {
                $messages->setErrorMessage('Service division should be provided.');
            }*/
             //validate the service division          
            if ( !isset($data['ss_punch_line']) || empty($data['ss_punch_line']) ) {
                  $messages->setErrorMessage('Punchline should be provided.');
            }
            //check for same service title name
			$service_title = trim($data["ss_title"]);
			$sid = $data["ss_id"];
            if ( (Services::getList($db, $service, 'ss_title', " WHERE ss_title = '$service_title' && ss_id !='$sid' && ss_parent_id=0")) > 0 ){
				$messages->setErrorMessage('Service title name already exists. Please change the title.');
            }
            if ( !empty($data['tax1_name']) && empty($data['tax1_value']) ) {
                  $messages->setErrorMessage('Select tax value.');
            }
            if ( !empty($data['tax1_value']) && empty($data['tax1_name']) ) {
                  $messages->setErrorMessage('Select tax name as you select the tax value.');
            }
            // added in  2010-10-oct-26
			if(!empty($data['tax1_name']) && !empty($data['tax1_value']) ){
				$condition_query=" WHERE status = '". ACTIVE ."' AND parent_id=".$data['tax1_id'] ;
				ServiceTax::getList($db, $taxc_list, 'count(*) as count', $condition_query);
				if(!empty($taxc_list)){
					$count = $taxc_list[0]['count'] ; 
					for($i=1;$i<=$count;$i++){
						$name1='tax1_sub'.$i.'_name';
						$val1='tax1_sub'.$i.'_value';
					
						if(empty($data[$name1]) || empty($data[$val1]) ){
							$messages->setErrorMessage('Select sub tax name'.$i.' and its value.');
						}
					}
				}				
			}
            //code to check price values BOf 
            /*
            if(!empty($data['ss_parent_id'])){            
               for ( $i=0; $i < count($data['service_price']); $i++ ) {                        
                    if ( empty($data['service_price'][$i]) ) {
                        $messages->setErrorMessage('The Price field at Position '. ($i+1) .' cannot be empty.');
                    }                   
                    else {
                        if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['service_price'][$i]) ) {
                            $messages->setErrorMessage('The Price field at Position '. ($i+1) .' is not valid.');
                        }
                    }
               }        
            } 
            */            
            //code to check price values eOf       
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
        function getParent(&$db,&$services,$id='0'){
			$sql = "SELECT ".TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title FROM ".TABLE_SETTINGS_SERVICES." WHERE ss_parent_id='".$id."' AND ss_status='".Services::ACTIVE."'";
			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
					$services[] = processSqlData($db->Record);
                    //$title = processSqlData($db->f('ss_title')) ;
                   // $ss_id = $db->f('ss_id') ;
					//$services[$ss_id] = $title ;

				return ( $db->nf() );
			}
			else 
				return false;			
		}
        
        function getPriceList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp=''){
            $query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				 $query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SETTINGS_SERVICES_PRICE;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				 return ( $total );
			}
			else {
				return false;
			}	
        
        }
    }
?>