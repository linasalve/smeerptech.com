<?php
	class VendorsJobwork {
		
		const PENDING = 0;
		const COMPLETED  = 1;     
		const CANCELLED  = 2;     
		
        
		
        function getStatus() {
			$status = array(
							'PENDING'   => VendorsJobwork::PENDING,
                            'COMPLETED'    => VendorsJobwork::COMPLETED,
                            'CANCELLED'    => VendorsJobwork::CANCELLED
						);
			return ($status);
		}   
        
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count ";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_VENDORS_JOBWORK
            	    ." LEFT JOIN ". TABLE_CLIENTS 
                	." ON ". TABLE_VENDORS_JOBWORK .".vendors_id = ". TABLE_CLIENTS .".user_id "
					." LEFT JOIN ". TABLE_BILL_ORDERS 
					." ON ". TABLE_VENDORS_JOBWORK .".ord_id = ". TABLE_BILL_ORDERS .".id ";
            $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
       
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
           $query .= " FROM ". TABLE_VENDORS_JOBWORK
            	    ." LEFT JOIN ". TABLE_CLIENTS
                	." ON ". TABLE_VENDORS_JOBWORK .".vendors_id = ". TABLE_CLIENTS .".user_id ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
           
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
			$query = "DELETE FROM ". TABLE_VENDORS_JOBWORK
						." WHERE id = '$id'";
			if ( $db->query($query) && $db->affected_rows()>0 ) {
				$messages->setOkMessage("The Record has been deleted.");                      
			}
			else {
				$messages->setErrorMessage("The Record was not deleted.");
			}
                
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if(empty($data['do_job'])){                
                 $messages->setErrorMessage("Please Select Job date.");                 
            }
			if(empty($data['vendors_id'])){                
                 $messages->setErrorMessage("Please Select the Vendor.");                 
            }
			
            if(empty($data['title'])){                
                 $messages->setErrorMessage("Please Enter the Title.");                 
            }
		    if(empty($data['vendor_instructions'])){                
                 $messages->setErrorMessage("Please Enter the Vendor Instructions.");                 
            } 
			if(empty($data['team_instructions'])){                
                 $messages->setErrorMessage("Please Enter the Team Instructions.");                 
            } 
           
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
           
            /* 
			if(empty($data['title'])){                
                 $messages->setErrorMessage("Please Enter the Title.");                 
            }
		    if(empty($data['description'])){                
                 $messages->setErrorMessage("Please Enter the Description.");                 
            } 
			if(empty($data['vendors_id'])){                
                 $messages->setErrorMessage("Please Select the Vendor.");                 
            }
                                     
            */
            if(empty($data['vendor_instructions'])){                
                 $messages->setErrorMessage("Please Enter the Vendor Instructions.");                 
            } 
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
    
            /**
         * This function is used to update the status of the Executive.
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, VendorsJobwork::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid Status.");
			}
			else {
                $user = NULL;
                if ( (VendorsJobwork::getDetails($db, $data, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
                    $data = $data[0];
                   // if ( $user['access_level'] < $access_level ) {
                      $query = "UPDATE ". TABLE_VENDORS_JOBWORK
                                    ." SET status = '$status_new'"
                                    ." WHERE id = '$id'";
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            $messages->setOkMessage("The Status has been updated.");
                        }
                        else {
                            $messages->setErrorMessage("The Status was not updated.");
                        }
                    /*}
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of Executives with the current Access Level.");
                    }*/
                }
                else {
                    $messages->setErrorMessage("Record was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

		
    }
?>
