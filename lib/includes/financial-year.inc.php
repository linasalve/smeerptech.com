<?php
	class FinancialYear {
		
		const ISFINANCIALYEAR = 1;
		const NOTFINANCIALYEAR  = 0;      
        
        function getStatus() {
			$status = array(
							'Financial Year'   => FinancialYear::ISFINANCIALYEAR,
                            'Not Financial Year'    => FinancialYear::NOTFINANCIALYEAR
						);
			return ($status);
		}   
        
        function yearRange() {
			$year_range = array( 
					/*'2013-2014'    => '2013-2014'
					'2012-2013'    => '2012-2013',	*/
					'2011-2012'    => '2011-2012',
					'2010-2011'    => '2010-2011',
					'2009-2010' => '2009-2010',
					'2008-2009' => '2008-2009',
					'2007-2008' => '2007-2008'
				);
			
			return ($year_range);
		}
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_FINANCIAL_YEAR;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_FINANCIAL_YEAR;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_FINANCIAL_YEAR
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			} 
			
			 if ( !isset($data['year_range']) || empty($data['year_range']) ) {
                $messages->setErrorMessage("Select the Year Range.");
            } 
            if ( !isset($data['company_id']) || empty($data['company_id']) ) {
                $messages->setErrorMessage("Select the Company.");
            }
            
            if ( !empty($data['year_range']) && !empty($data['company_id']) ) {
                $list = NULL;
                if ( FinancialYear::getList($db, $list, 'id', " WHERE year_range = '". $data['year_range'] ."' AND company_id = '".$data['company_id']."'")>0 ) {
                    $messages->setErrorMessage("This Year range & Company is already existed. Please specify another.");
                }
            }
                  
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            
           if ( !isset($data['year_range']) || empty($data['year_range']) ) {
                $messages->setErrorMessage("Select the Year Range.");
            } 
            if ( !isset($data['company_id']) || empty($data['company_id']) ) {
                $messages->setErrorMessage("Select the Company.");
            }
            
            if ( !empty($data['year_range']) && !empty($data['company_id']) ) {
                $list = NULL;
                if ( FinancialYear::getList($db, $list, 'id', " WHERE year_range = '". $data['year_range'] ."' AND company_id = '".$data['company_id']."'")>0 ) {
                    $messages->setErrorMessage("This Year range & Company is already existed. Please specify another.");
                }
            }                
            
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
    
            /**
         * This function is used to update the status of the Executive.
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			
                $list = NULL;
                if ( (FinancialYear::getList($db, $list, 'id', " WHERE id = '$id'")) > 0 ) {
                    $list = $list[0];
                   // if ( $user['access_level'] < $access_level ) {
                    $query = "UPDATE ". TABLE_FINANCIAL_YEAR
                                    ." SET is_financial_yr = '0' "
                                    ." WHERE is_financial_yr = '1' ";
                    $db->query($query); 
                    $query = "UPDATE ". TABLE_FINANCIAL_YEAR
                                    ." SET is_financial_yr = '1' "
                                     ." WHERE id = '$id'";
                                     
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            $messages->setOkMessage("The Financial Year has been updated.");
                        }
                        else {
                            $messages->setErrorMessage("The Financial Year was not updated.");
                        }
                    /*}
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of Executives with the current Access Level.");
                    }*/
                }
                else {
                    $messages->setErrorMessage("The financial year was not found.");
                }
			
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

		
    }
?>
