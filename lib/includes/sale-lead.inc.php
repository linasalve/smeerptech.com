<?php 

	class Leads {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;
        const COMPLETED = 4;

        const CLIENT = 'CLIENT';
		const LEAD ='LEAD' ;
        
		const MEMBER_PROSPECTS = 1 ;
		const MEMBER_REFERRAL = 2 ;
		
		
        const NEWONE = 1;
        const POSITIVE  = 2;
		const NEGATIVE = 3;
		const ASSIGNED  = 4;
		const INPROCESS  = 5;
        const CONVERTED = 6;
		const RECYCLED = 7;
		const DEAD = 8;
        
		const AQUIRED = 1;
		const ACTIVERATING = 2;
		const MARKETFAILED = 3;
		const PCANCELLED = 4;
		const SHUTDOWN = 5;
		
		const SENDMAILNO =0;
        const SENDMAILYES =1;
		
		function getStatus() {
			$status = array('BLOCKED' => Leads::BLOCKED,
							'ACTIVE'  => Leads::ACTIVE,
							'PENDING' => Leads::PENDING,
							'DELETED' => Leads::DELETED,
							'COMPLETED' => Leads::COMPLETED,
						);
			return ($status);
		}
		function getMemberType(){
			$type = array(Leads::MEMBER_PROSPECTS => 'PROSPECTS', 
						Leads::MEMBER_REFERRAL =>'REFERRALS'  
					);
			return ($type);
		}
		
        function getLeadStatus() {
			$leadstatus = array('New' => Leads::NEWONE,
							'Positive'  => Leads::POSITIVE,
							'Negative'  => Leads::NEGATIVE,
							'Assigned'  => Leads::ASSIGNED,
							'In-Process' => Leads::INPROCESS,
							'Converted' => Leads::CONVERTED,
							//'Recycled' => Leads::RECYCLED,
							'Dead' => Leads::DEAD
						);
			return ($leadstatus);
		}
		
		function getLeadStatusTm() {
			$leadstatus = array('New' => Leads::NEWONE,
							'Positive'  => Leads::POSITIVE,
							'Negative'  => Leads::NEGATIVE
						);
			return ($leadstatus);
		}
        
        function getLeadSearchStatus() {
			$lead_search_status = array('New' => Leads::NEWONE,
							'Positive'  => Leads::POSITIVE,
							'Negative'  => Leads::NEGATIVE,
							'Assigned'  => Leads::ASSIGNED
						);
			return ($lead_search_status);
		}
		/*function getTypes() {
			$type = array('CLIENT' => Lead::CLIENT,
							'LEAD'  => Lead::LEAD
						
						);
			return ($type);
		}*/
        
		function getRatings() {
			$rating = array('Aquired' => Leads::AQUIRED,
							'Active'  => Leads::ACTIVERATING,
							'Market Failed'  => Leads::MARKETFAILED,
							'Project Cancelled'  => Leads::PCANCELLED,
							'Shutdown'  => Leads::SHUTDOWN
						);
			return ($rating);
		}
		
        function getCountryCode( &$db, &$country_code){			
            
            $query= "SELECT * FROM ". TABLE_COUNTRIES 
					." WHERE ". TABLE_COUNTRIES .".status='1' ";
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$country_code[]= processSqlData($db->result());
					}
				}	                
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        
		function getSource( &$db, &$source){			
			$query="SELECT * FROM ".TABLE_SALE_SOURCE." WHERE status='1'";
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$source[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		
		function getIndustry( &$db, &$industry){			
			$query="SELECT * FROM ".TABLE_SALE_INDUSTRY." WHERE status='1'";
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$industry[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		
		function getCampaign( &$db, &$campaign){			
			$query="SELECT * FROM ".TABLE_SALE_CAMPAIGN." WHERE status='1'";
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$campaign[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		
		function getRivals( &$db, &$rivals){			
			$query="SELECT * FROM ".TABLE_SALE_RIVALS;
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$rivals[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		
		function getUsers( &$db, &$users,$condition=''){
          if(!empty($condition)){
                $query="SELECT * FROM ".TABLE_USER.' '.$condition." ORDER BY f_name";
          }else{
          
            $query="SELECT * FROM ".TABLE_USER." WHERE status='1' ORDER BY f_name";
          }
			
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$users[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		
		function getFollowupTitle( &$db, &$titles){			
			$query="SELECT * FROM ".TABLE_SALE_FOLLOWUP_TITLE." WHERE status = '1' ";
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$titles[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {

			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			/*$query .= " FROM ". TABLE_SETTINGS_CURRENCY
						." LEFT JOIN ".TABLE_COUNTRIES
						." ON ".TABLE_SETTINGS_CURRENCY.".country_id=".TABLE_COUNTRIES.".code";*/
			//$query .= " FROM ". TABLE_SALE_LEADS;
            
            $query .= " FROM ". TABLE_SALE_LEADS;
                        //" LEFT JOIN ". TABLE_USER." ON ".TABLE_SALE_LEADS.".lead_assign_to = ".TABLE_USER.".user_id" ;
						//" LEFT JOIN ". TABLE_SALE_RIVALS." ON ".TABLE_CLIENTS.".rivals_id= ".TABLE_SALE_RIVALS.".id
						 //LEFT JOIN ". TABLE_SALE_CAMPAIGN." ON ".TABLE_CLIENTS.".lead_campaign_id= ".TABLE_SALE_CAMPAIGN.".id
						 //LEFT JOIN ". TABLE_SALE_INDUSTRY." ON ".TABLE_CLIENTS.".lead_industry_id= ".TABLE_SALE_INDUSTRY.".id
						 //LEFT JOIN ". TABLE_SALE_SOURCE." ON ".TABLE_CLIENTS.".lead_source_from= ".TABLE_SALE_SOURCE.".id";
						 //LEFT JOIN ". TABLE_SALE_SOURCE." ON ".TABLE_CLIENTS.".lead_source_to= ".TABLE_SALE_SOURCE.".id
						 
						 //LEFT JOIN ". TABLE_USER." ON ".TABLE_CLIENTS.".lead_assign_to= ".TABLE_USER.".user_id"
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
						
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        /**
		 *	Function to get all leads of marketing department wise
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getMrList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {

			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			/*$query .= " FROM ". TABLE_SETTINGS_CURRENCY
						." LEFT JOIN ".TABLE_COUNTRIES
						." ON ".TABLE_SETTINGS_CURRENCY.".country_id=".TABLE_COUNTRIES.".code";*/
			$query .= " FROM ". TABLE_SALE_LEADS.
                        " LEFT JOIN ". TABLE_USER." ON ".TABLE_SALE_LEADS.".lead_assign_to_mr = ".TABLE_USER.".user_id" ;
                        
						//" LEFT JOIN ". TABLE_SALE_RIVALS." ON ".TABLE_CLIENTS.".rivals_id= ".TABLE_SALE_RIVALS.".id
						 //LEFT JOIN ". TABLE_SALE_CAMPAIGN." ON ".TABLE_CLIENTS.".lead_campaign_id= ".TABLE_SALE_CAMPAIGN.".id
						 //LEFT JOIN ". TABLE_SALE_INDUSTRY." ON ".TABLE_CLIENTS.".lead_industry_id= ".TABLE_SALE_INDUSTRY.".id
						 //LEFT JOIN ". TABLE_SALE_SOURCE." ON ".TABLE_CLIENTS.".lead_source_from= ".TABLE_SALE_SOURCE.".id";
						 //LEFT JOIN ". TABLE_SALE_SOURCE." ON ".TABLE_CLIENTS.".lead_source_to= ".TABLE_SALE_SOURCE.".id
						 
						 //LEFT JOIN ". TABLE_USER." ON ".TABLE_CLIENTS.".lead_assign_to= ".TABLE_USER.".user_id"
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
						
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
		
		function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SALE_LEADS;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Leads::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Leads::getList($db, $user, 'lead_id, access_level', " WHERE lead_id = '$id'")) > 0 ) {
                    $user = $user[0];
                    if ( $user['access_level'] < $access_level ) {
                        $query = "UPDATE ". TABLE_SALE_LEADS
                                    ." SET status = '$status_new' "
                                    ." WHERE lead_id = '$id'";
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            $messages->setOkMessage("The Status has been updated.");
                        }
                        else {
                            $messages->setErrorMessage("The Status was not updated.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of Lead with the current Access Level.");
                    }
                }
                else {
                    $messages->setErrorMessage("The Lead Lead was not found.");
                }
			}
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        
        
        //This function is used to change the status of lead
        
        function updateLeadStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Leads::getLeadStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Leads::getList($db, $user, 'lead_id, access_level', " WHERE lead_id = '$id'")) > 0 ) {
                    $user = $user[0];
                    if ( $user['access_level'] < $access_level ) {
                        $query = "UPDATE ". TABLE_SALE_LEADS
                                    ." SET lead_status = '$status_new' "
                                    ." WHERE lead_id = '$id'";
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            
                            $messages->setOkMessage("The Status has been updated.");
							
                        }
                        else {
                            $messages->setErrorMessage("The Status was not updated.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of Lead with the current Access Level.");
                    }
                }
                else {
                    $messages->setErrorMessage("The Lead Lead was not found.");
                }
			}
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (Leads::getList($db, $user, 'lead_id, status, access_level', " WHERE lead_id = '$id'")) > 0 ) {
                $user = $user[0];
                if ( $user['status'] == Leads::BLOCKED || $user['status'] == Leads::DELETED) {
                    if ( $user['access_level'] < $access_level ) {
                        // Delete the Reminders set for the Client.
                        /*$query = "SELECT id FROM ". TABLE_USER_REMINDERS
                                    ." WHERE table_name = '". TABLE_CLIENTS ."'"
                                    ." AND reminder_for = '". $id ."'";
                        if ( $db->query($query) ) {
                            if ( $db->nf()>0 ) {
                                $query = "DELETE FROM ". TABLE_USER_REMINDERS
                                            ." WHERE table_name = '". TABLE_CLIENTS ."'"
                                            ." AND reminder_for = '". $id ."'";
                                if ( !$db->query($query) || $db->affected_rows()<=0 ) {
                                    $messages->setErrorMessage("The Reminders for the Lead were not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("The Reminders for the Lead were not deleted.");
                        }*/
                        
                        // Delete the Contact Numbers set for the Client.
                        $query = "SELECT id FROM ". TABLE_PHONE
                                    ." WHERE table_name = '". TABLE_SALE_LEADS ."'"
                                    ." AND phone_of = '". $id ."'";
                        if ( $db->query($query) ) {
                            if ( $db->nf()>0 ) {
                                $query = "DELETE FROM ". TABLE_PHONE
                                            ." WHERE table_name = '". TABLE_SALE_LEADS ."'"
                                            ." AND phone_of = '". $id ."'";
                                if ( !$db->query($query) || $db->affected_rows()<=0 ) {
                                    $messages->setErrorMessage("The Contact Numbers for the Lead Rival were not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("The Contact Numbers for the Lead Rival were not deleted.");
                        }

                        // Delete the Address's of the Client.
                        $query = "SELECT id FROM ". TABLE_ADDRESS
                                    ." WHERE table_name = '". TABLE_SALE_LEADS ."'"
                                    ." AND address_of = '". $id ."'";
                        if ( $db->query($query) ) {
                            if ( $db->nf()>0 ) {
                                $query = "DELETE FROM ". TABLE_ADDRESS
                                            ." WHERE table_name = '". TABLE_SALE_LEADS ."'"
                                            ." AND address_of = '". $id ."'";
                                if ( !$db->query($query) || $db->affected_rows()<=0 ) {
                                    $messages->setErrorMessage("The Address for the Lead Rival were not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("The Address for the Lead Rival were not deleted.");
                        }
                                
                        if ( $messages->getErrorMessageCount() <= 0 ) {
                            // Delete the Client itself.
                            $query = "DELETE FROM ". TABLE_SALE_LEADS 
                                        ." WHERE lead_id = '". $id ."'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Lead Rival has been deleted.");
                            }
                            else {
                                $messages->setErrorMessage("The Lead Rival was not deleted.");
                            }
                        }

                    }
                    else {
                        $messages->setErrorMessage("You do not have the Right to Delete the Lead Rival with the current Access Level.");
                    }
                }
                else {
                    $messages->setErrorMessage("To delete the Lead Rival, he/she has to be Blocked.");
                }
            }
            else {
                $messages->setErrorMessage("The record for the Lead Rival was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
         
        function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
 
            // Password.
            
           /* if ( !isset($data['password']) || empty($data['password']) 
                    || !isset($data['re_password']) || empty($data['re_password']) ) {
                $messages->setErrorMessage('The Password cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['password']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                else {
                    if ( $data['password'] != $data['re_password'] ) {
                        $messages->setErrorMessage('The entered Password do not match.');
                    }
                }
            }*/

			// Check the validity of the Accounts Manager.
			/*if ( !isset($data['manager']) || empty($data['manager']) ) {
				$messages->setErrorMessage('Enter Username or Account Number of the Accounts Manager.');
			}
			else {
                include_once (DIR_FS_INCLUDES .'/user.inc.php');
                $executive  = NULL;
                $condition  = ' WHERE ( username = \''. $data['manager'] .'\' '
                                .' OR number = \''. $data['manager'] .'\' )'
                                . ' AND status = \'1\'';
                if ( User::getList($db, $executive, 'user_id', $condition) > 0 ) {
                    $data['manager'] = $executive[0]['user_id'];
                }
                else {
                    $messages->setErrorMessage('The Accounts Manager was not found.');
                }
			}*/
             //Default access level and default role 
            //$data['access_level'] = '1'; // General
            //$data['roles'] = '13'; // Client Genaral
            
            // Access Level.
            /* if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage('Select the Access Level.');
            }
            else {
                $acc_index = array();
                array_key_search('access_level', $data['access_level'], $al_list, $acc_index);
                if ( count($acc_index)<=0 ) {
                    $messages->setErrorMessage('Selected Access Level not found.');
                }
            } */
            
            if ( !isset($data['company_name']) || empty($data['company_name']) ) {
                $messages->setErrorMessage('Company Name should be provided.');
			}
			if ( empty($data['member_type']) ) {
                $messages->setErrorMessage('Select Memeber type');
			}
            
			//to validate source.
			/* 
			if ( !isset($data['lead_source_to']) || empty($data['lead_source_to']) ) {
				if ( !isset($data['lead_source_from']) || empty($data['lead_source_from']) ) {
                $messages->setErrorMessage('Lead Source should be provided.');
				}
			}
			
			if (!empty($data['lead_source_to']) && !empty($data['lead_source_from'])){
				$messages->setErrorMessage('Select one of two Lead Source.');				
			} 
			*/
			
            // Roles.
            /*if ( !isset($data['roles']) ) {
                $messages->setErrorMessage('Select atleast one Role for the Client.');
            }
            else {
                foreach ( $data['roles'] as $role ) {
                    $role_index = array();
                    array_key_search('id', $role, $role_list, $role_index);
                    if ( count($role_index)<=0 ) {
                        $messages->setErrorMessage('The Role with ID '. $role .'is not found.');
                    }
                }
                if(is_array($data['roles'])){
                    $data['roles'] = implode(',', $data['roles']);
                }
               
            }*/
            
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
			}
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !isset($data['gender']) || !in_array($data['gender'], array('m','f','o')) ) {
                $messages->setErrorMessage('Gender should be selected.');
            }
            
            // Check the Important Dates.
            if ( !empty($data['do_birth']) ) {
                $temp = explode('/', $data['do_birth']);
                $data['do_birth'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['do_aniv']) ) {
                $temp = explode('/', $data['do_aniv']);
                $data['do_aniv'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            // Validate the E-mail Addresses.
           
            /*if ( !empty($data['email']) ) {
                $list = NULL;
                if ( Leads::getList($db, $list, 'lead_id', " WHERE email = '". $data['email'] ."' AND lead_id != '".$data['lead_id']."'")>0 ) {
                    $messages->setErrorMessage("This email id is already exist.Please specify another.");
                }
            }*/
            
            /*if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }*/
            if ( !empty($data['email']) && !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('The Primary Email Address is not valid.');
            }
           
            if ( !empty($data['email']) ) {
                $list = NULL;
                if ( Leads::getList($db, $list, 'lead_id', " WHERE email = '". $data['email'] ."'")>0 ) {
                    $messages->setErrorMessage("This email id is already exist.Please specify another.");
                }
            }
            
            /*if(!empty($data['email'])){
                if ( !isValidEmail($data['email']) ) {
                    $messages->setErrorMessage('The Primary Email Address is not valid.');
                }
            }*/
            
            if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            // Validate the Addresses.
			$data['address_count'] = count($data['address_type']);
            for ( $i=0; $i<$data['address_count']; $i++ ) {
				if ( !$regionlead->validate($data['address_type'][$i], $data['address'][$i], $data['company_name'][$i],
                    $data['city'][$i], $data['state'][$i], $data['country'][$i],$data['zipcode'][$i])) {
                    $messages->setErrorMessage('There is/are error(s) in the Mailing Address.');
                    foreach ( $regionlead->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
            }
                       
			//To check contact details
			if ( empty($data['p_number'][0]) && empty($data['p_number'][1]) && empty($data['p_number'][2]) ){
	               $messages->setErrorMessage('Contact Number should be provided');
			}
			
			// Validate the Phone.
			$data['phone_count'] = count($data['p_type']);
			for ( $i=0; $i<$data['phone_count']; $i++ ) {
				if ( !empty($data['p_number'][$i]) ) {
					if ( !$phonelead->validate($data['p_type'][$i], $data['cc'][$i], $data['ac'][$i], $data['p_number'][$i]) ) {
						$messages->setErrorMessage('There is/are error(s) in the <b>'. $phonelead->getTypeTitle($data['p_type'][$i]) .' Contact Number</b>.');
						foreach ( $phonelead->getError() as $errors) {
							$messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
						}
					}
				}
			}
			
            // Check the Reminders.
            /*for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                if ( !empty($data['remind_date_'.$i]) ) {
                    $temp = explode('/', $data['remind_date_'.$i]);
                    $data['remind_date_'.$i] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                    
                    if ( !$reminder->validate($data['remind_date_'.$i], $data['remind_text_'.$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                        foreach ( $reminder->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }*/
            /*
            if ( $data['lead_status'] != Quick::NEWONE && $data['lead_status'] != Quick::ASSIGNED 
                    && $data['lead_status'] != Quick::INPROCESS && $data['lead_status'] != Quick::CONVERTED && $data['lead_status'] != Quick::RECYCLED && $data['lead_status'] != Quick::DEAD) {
                		$messages->setErrorMessage('The Lead Status is not valid.');
            }*/

            // Validate the Client Managers.
           /* if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Client.");
            }
            else {
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result());
                    }
                    $team_count = count($data['team']);

                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }
                    // Uncomment the following and comment the above FOR loop if speed is preferred 
					// against a detailed error report as above.
                    //if ( $db->nf() < $team_count ) {
                    //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                    //}
                }
            }
			*/
            
            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
            // Login ID.
            //  default username ='smeerp';
           /* $data['username'] = 'smeerp';
             
            if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage('The Login ID cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{1,80}$/', $data['username']) ) {
                    $messages->setErrorMessage('The Login ID is invalid.');
                }
                else {
                    // Check for Dupllicate User ID, Login ID and Account Number.
// The following code has been commented to allow Duplicate Username but with different Account Number.
// For achieving this, the Unique User ID will be generated using the Username and Account Number, and 
// while checking for duplicate entry the Unique User ID and the Username-Account Number pair will be used.
//                    $user_id    = encParam($data['username']);
//                    $user_list  = NULL;
//                    $condition  = " WHERE (user_id = '$user_id') "
//                                    ." OR ( username = '". $data['username'] ."') "
//                                    ." OR ( number = '". $data['number'] ."' AND number IS NOT NULL ) ";
//                    
//                    if ( (Lead::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
//                        foreach ( $user_list as $user) {
//                            if ( ($user_id == $user['user_id']) || ($data['username'] == $user['username']) ) {
//                                $messages->setErrorMessage('The Login ID is alreay taken. Please enter another.');
//                            }
//                            if ( $data['number'] == $user['number'] ) {
//                                $messages->setErrorMessage('The Account number is alreay taken. Please enter another.');
//                            }
//                        }
//                    }
//                    else {
//                        $data['user_id'] = $user_id;
//                    }
                    $user_id    = encParam($data['username'].'?'.$data['number']); // We donot allow '?' in username, hence used here.
                    $user_list  = NULL;
                    $condition  = " WHERE (user_id = '$user_id') "
                                    ." OR ( number = '". $data['number'] ."' AND number IS NOT NULL ) ";

                   if ( (LeadsRivals::getList($db, $user_list, 'user_id, number', $condition)) > 0 ) {
                        foreach ( $user_list as $user) {
                            $messages->setErrorMessage('The Account number is alreay taken. Please enter another.');
                        }
                    }
                    else {
                        $data['user_id'] = $user_id;
                    }

                }
            }*/
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}        
		}
 
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
 
            // Password.
            /*if ( isset($data['password']) && !empty($data['password']) ) {
                if ( !isset($data['re_password']) || empty($data['re_password']) ) {
                    $messages->setErrorMessage('The Repeat Password cannot be empty.');
                }
                elseif ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['password']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                elseif ( $data['password'] != $data['re_password'] ) {
                    $messages->setErrorMessage('The entered Password do not match.');
                }
                else {
                    $data['password'] = ",". TABLE_CLIENTS .".password = '". encParam($data['password']) ."'";
                }
            }
            else {
                $data['password'] = '';
            }*/

            // Check the validity of the Accounts Manager.
            /*if ( !isset($data['manager']) || empty($data['manager']) ) {
                $messages->setErrorMessage('Enter Username or Account Number of the Accounts Manager.');
            }
            else {
                include_once (DIR_FS_INCLUDES .'/user.inc.php');
                $executive  = NULL;
                $condition  = ' WHERE ( username = \''. $data['manager'] .'\' '
                                .' OR number = \''. $data['manager'] .'\' )'
                                .' AND status = \'1\'';
                if ( User::getList($db, $executive, 'user_id', $condition) > 0 ) {
                    $data['manager'] = $executive[0]['user_id'];
                }
                else {
                    $messages->setErrorMessage('The Accounts Manager was not found.');
                }
            }*/
            //Default access level and default role 
            //$data['access_level'] = '1'; // General
            $data['roles'] = '13'; // Client Genaral
            // Access Level.
            /*
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage('Select the Access Level.');
            }
            else {
                $acc_index = array();
                array_key_search('access_level', $data['access_level'], $al_list, $acc_index);
                if ( count($acc_index)<=0 ) {
                    $messages->setErrorMessage('Selected Access Level not found.');
                }
            }
            
            // Roles.
            if ( !isset($data['roles']) ) {
                $messages->setErrorMessage('Select atleast one Role for the Client.');
            }
            else {
                foreach ( $data['roles'] as $role ) {
                    $role_index = array();
                    array_key_search('id', $role, $role_list, $role_index);
                    if ( count($role_index)<=0 ) {
                        $messages->setErrorMessage('The Role with ID '. $role .'is not found.');
                    }
                }
                $data['roles'] = implode(',', $data['roles']);
            }
            */
            
            if ( !isset($data['company_name']) || empty($data['company_name']) ) {
                $messages->setErrorMessage('Company Name should be provided.');
            }
            
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
            }
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !isset($data['gender']) || !in_array($data['gender'], array('m','f','o')) ) {
                $messages->setErrorMessage('Gender should be selected.');
            }
            
            // Check the Important Dates.
            if ( !empty($data['do_birth']) ) {
                $temp = explode('/', $data['do_birth']);
                $data['do_birth'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['do_aniv']) ) {
                $temp = explode('/', $data['do_aniv']);
                $data['do_aniv'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            // Validate the E-mail Addresses.
            /*if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }*/
            if ( isset($data['email_1']) && !empty($data['email']) && !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('The Primary Email Address is not valid.');
            }
            
            if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            
            // Validate the Addresses.
			$data['address_count'] = count($data['address_type']);
            for ( $i=0; $i<$data['address_count']; $i++ ) {
                if ( !$regionlead->validate($data['address_type'][$i],
                                  $data['company_name'][$i],
                                  $data['address'][$i],
                                  $data['city'][$i],
                                  $data['state'][$i],
                                  $data['country'][$i],
                                  $data['zipcode'][$i]
                                 )
                   ) {
                    $messages->setErrorMessage('There is/are error(s) in the Mailing Address.');
                    foreach ( $regionlead->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
            }
            // Validate the Addresses.
			
            /*$data['address_count']  = count($data['address_type']);
            $preferred = 0;
            for ( $i=0; $i<$data['address_count']; $i++ ) {
                if ( !$regionlead->validate($data['address_type'][$i],
                                  $data['address'][$i],
                                  $data['company_name'][$i],
                                  $data['city'][$i],
                                  $data['state'][$i],
                                  $data['country'][$i],
                                  $data['zipcode'][$i]
                                 )
                   ) {
                    $messages->setErrorMessage('There is/are error(s) in the '. $regionlead->getTypeTitle($data['address_type'][$i]) .' Address.');
                    foreach ( $regionlead->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
                if ( isset($data['is_preferred'][$i]) && $data['is_preferred'][$i] == '1' ) {
                    $preferred += 1 ;
                }
            }*/
            // The elseif part is added for additional validation and security.
            // If speed is intended the elseif part can be removed and the
            // $preferred is to be converted to a BOOLEAN.
            // ($preferred = 0) to ($preferred = false), ($preferred += 1) to ($preferred = true)
           /*if ( $preferred == 0 ) {
                $messages->setErrorMessage('Atleast one Address is to be selected as preferred.');
            }
            elseif ( $preferred > 1 ) {
                $messages->setErrorMessage('Only one Address is to be selected as preferred.');
           }*/
            
            // Validate the Phone.
            $data['phone_count']= count($data['p_type']);
            $preferred          = 0;
            for ( $i=0; $i<$data['phone_count']; $i++ ) {
                if ( !empty($data['p_number'][$i]) ) {
                    if ( !$phonelead->validate($data['p_type'][$i], $data['cc'][$i], $data['ac'][$i], $data['p_number'][$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in the <b>'. $phone->getTypeTitle($data['p_type'][$i]) .' Contact Number</b>.');
                        foreach ( $phonelead->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
                if ( isset($data['p_is_preferred'][$i]) && $data['p_is_preferred'][$i] == '1' ) {
                    $preferred += 1 ;
                }
            }
            
            // The elseif part is added for additional validation and security.
            // If speed is intended the elseif part can be removed and the
            // $preferred is to be converted to a BOOLEAN.
            // ($preferred = 0) to ($preferred = false), ($preferred += 1) to ($preferred = true)
            /*if ( !$preferred ) {
                $messages->setErrorMessage('Atleast one Contact Number is to be selected as preferred.');
            }
            elseif ( $preferred > 1 ) {
                $messages->setErrorMessage('Only one Contact Number is to be selected as preferred.');
            }*/
            
            // Check the Reminders.
            /*for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                if ( !empty($data['remind_date_'.$i]) ) {
                    $temp = explode('/', $data['remind_date_'.$i]);
                    $data['remind_date_'.$i] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                    
                    if ( !$reminder->validate($data['remind_date_'.$i], $data['remind_text_'.$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                        foreach ( $reminder->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }*/
            
            /*if ( $data['status'] != Lead::PENDING && $data['status'] != Lead::ACTIVE 
                    && $data['status'] != Lead::BLOCKED && $data['status'] != Lead::DELETED ) {
                $messages->setErrorMessage('The Status is not valid.');
            }*/
			
			if ( $data['lead_status'] != Leads::NEWONE && $data['lead_status'] != Leads::POSITIVE && $data['lead_status'] != Leads::NEGATIVE && $data['lead_status'] != Leads::ASSIGNED && $data['lead_status'] != Leads::INPROCESS && $data['lead_status'] != Leads::CONVERTED && $data['lead_status'] != Leads::RECYCLED && $data['lead_status'] != Leads::DEAD) {
		                $messages->setErrorMessage('The Status is not valid.');
            }

            if( empty($data['member_type']) ) {
                $messages->setErrorMessage('Select Memeber type');
			}
			// Validate the Client Managers.
           /* if ( !isset($data['team']) || empty($data['team']) ) {
                $messages->setErrorMessage("Please associate atleast one Executive with the Client.");
            }
            else {
                $team = "'". implode("','", $data['team']) ."'";
                
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id IN (". $team .") ";
                if ( $db->query($query) ) {
                    $team_db = NULL;
                    while ($db->next_record()) {
                        $team_db[] = processSqlData($db->result());
                    }
                    $team_count = count($data['team']);

                    for ( $i=0; $i<$team_count; $i++ ) {
                        $index = array();
                        if ( !array_key_search('user_id', $data['team'][$i], $team_db, $index) ) {
                            $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not found.");
                        }
                        else {
                            if ( $team_db[$index[0]]['status'] != ACTIVE ) {
                                $messages->setErrorMessage("The Executive <b>". $data['team_details'][$i] ."</b> is not Active.");
                            }
                            else {
                                $data['team_members'][$i] = $team_db[$index[0]];
                            }
                        }
                    }
                    // Uncomment the following and comment the above FOR loop if speed is preferred 
					// against a detailed error report as above.
                    //if ( $db->nf() < $team_count ) {
                    //    $messages->setErrorMessage("Atleat one of the Executives was not found.");
                    //}
                }
            }		*/	
			
            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
             // Login ID.
            //  default username ='smeerp';
           /* $data['username'] = 'smeerp';
            if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage('The Login ID cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{1,80}$/', $data['username']) ) {
                    $messages->setErrorMessage('The Login ID is invalid.');
                }
                else {
                    // Check for Dupllicate User ID, Login ID and Account Number.
//                    $user_id    = encParam($data['username']);
//                    $user_list  = NULL;
//                    $condition  = " WHERE ( (user_id = '$user_id') "
//                                        ." OR ( username = '". $data['username'] ."') "
//                                        ." OR ( number = '". $data['number'] ."' AND number IS NOT NULL ) "
//                                    ." ) AND user_id != '". $data['user_id'] ."'";
//                    
//                    if ( (Lead::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
//                        foreach ( $user_list as $user) {
//                            if ( ($user_id == $user['user_id']) || ($data['username'] == $user['username']) ) {
//                                $messages->setErrorMessage('The Login ID is alreay taken. Please enter another.');
//                            }
//                            if ( $data['number'] == $user['number'] ) {
//                                $messages->setErrorMessage('The Account number is alreay taken. Please enter another.');
//                            }
//                        }
//                    }
//                    else {
//                        $data['user_id'] = $user_id;
//                        
//                    }
 					//$user_id    = $data['number']; 
						$user_list  = NULL;
						 $condition  = " WHERE (user_id = '$user_id') "
                                    ." OR ( number = '". $data['number'] ."' AND number IS NOT NULL ) ";
						if(!isset($data['number']) || empty($data['number'])){
							if ( (Leads::getList($db, $user_list, 'user_id, number', $condition)) > 0 ) {
								foreach ( $user_list as $user) {
									$messages->setErrorMessage('The Account number is alreay taken. Please enter another.');
								}
							}
							else {
    		                    $data['user_id'] = $user_id;
            		        }
						}
                }
            }*/
			
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
        
        
        /**
         * This function is used to retrieve the next account number in series.
         *
         * @param   object  database class object
         *
         * @return  int     the next number in series
         *
         */
       /* function getNewAccNumber(&$db){
            $user_number= 6000;
            $query      = "SELECT MAX(number) AS number FROM ". TABLE_SALE_LEADS;

            if ( $db->query($query) && $db->nf()>0 && $db->next_record() && $db->f("number") > 0){
                $user_number = $db->f("number");
            }
			$user_number++;
            return $user_number;
        }*/

        
       /* function getRoles(&$db, $level, $role_ids='') {
            include_once ( DIR_FS_INCLUDES .'/user-roles.inc.php');
            
            $role_list = NULL;
            if ( !empty($role_ids) ) {
                if ( is_array($role_ids) ) {
                    $role_ids = ' AND id IN ('. ('\''. implode('\',', $role_ids) .'\'') .')';
                }
                else {
                    $role_ids = ' AND id IN ('. ('\''. str_replace(',', '\',\'', $role_ids) .'\'') .')';
                }
            }
            else {
                $role_ids = '';
            }
            
            $condition  = " WHERE access_level < '". $level ."'"
                            . $role_ids
                            ." AND status = '". UserRoles::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            
            if ( (UserRoles::getList($db, $role_list, 'id, title', $condition)) <= 0 ) {
                $role_list[] = array('id' => '', 'title' => 'No Role Available' );
            }
            
            return ($role_list);
        }*/
        
        
        function getAccessLevel(&$db, $level=0, $al_list='') {
            include_once ( DIR_FS_INCLUDES .'/settings-access-level.inc.php');

            $role_list = NULL;
            if ( !empty($al_list) ) {
                if ( is_array($al_list) ) {
                    $al_list = ' AND access_level IN ('. ('\''. implode('\',', $al_list) .'\'') .')';
                }
                else {
                    $al_list = ' AND access_level IN ('. ('\''. str_replace(',', '\',\'', $al_list) .'\'') .')';
                }
            }
            else {
                $al_list = '';
            }
            
            $access     = NULL;
            $condition  = " WHERE access_level < '". $level ."'"
                            . $al_list
                            ." AND status = '". AccessLevel::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            if ( (AccessLevel::getList($db, $access, 'title, access_level', $condition)) <= 0 ) {
                $access[] = array('title' => 'None', 'access_level' => '0' );
            }
            
            return ($access);
        }
        
        
        /*function getClientManager($db, $client_id, &$details, $fields='username') {
            $query = "SELECT $fields FROM ". TABLE_CLIENTS
                        ." LEFT JOIN ". TABLE_USERS
                            ." ON ". TABLE_USERS .".user_id = ". TABLE_CLIENTS .".manager "
                        ." WHERE ". TABLE_CLIENTS .".user_id = '". $client_id ."'";
            if ( ($db->query($query)) && ($db->nf()>0) && ($db->next_record()>0) ) {
                $details = processSQLData($db->result());
                return (true);
            }
            return (false);
        }*/
        
        
       /* function getManager(&$db, $client_id='', $executive_id='', $fields='username') {
            include_once (DIR_FS_INCLUDES .'/user.inc.php');
            $executive  = '';
            
            if ( $client_id != '' ) {
                $condition  = " WHERE user_id = '$client_id'";
                if ( Lead::getList($db, $executive, 'manager', $condition) > 0 ) {
                    $executive_id = $executive[0]['manager'];
                }
            }
            $executive = '';
            $condition  = ' WHERE user_id = \''. $executive_id .'\' ';
            if ( User::getList($db, $executive, $fields, $condition) > 0 ) {
                $fields = explode(',', $fields);
                if ( count($fields) == 1 ) {
                    $executive = $executive[0][$fields[0]];
                }
                else {
                    $executive = $executive[0];
                }
            }
            return ($executive);
        }*/
		
		/*function validateFollowupAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
           
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}

        }*/
		
        function updateHistory(&$db, $my, $data) {
            
            $query = "SELECT * FROM ".TABLE_SALE_LEADS." WHERE lead_id='".$data['lead_id']."'";
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
                    }
                }
            }
            $data=$list['0'];
            $query	= " INSERT INTO ". TABLE_LEAD_HISTORY
								." SET ".TABLE_LEAD_HISTORY .".lead_id  		= '". $data['lead_id'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".access_level		= '". $data['access_level'] ."'"
								.", ". TABLE_LEAD_HISTORY .".created_by			= '". $data['created_by'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".status	    		= '". $data['status'] ."'"
								.", ". TABLE_LEAD_HISTORY .".lead_status   		= '". $data['lead_status'] ."'"
								.", ". TABLE_LEAD_HISTORY .".lead_assign_to   	= '". $data['lead_assign_to'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".lead_assign_to_mr  = '". $data['lead_assign_to_mr'] ."'"
								.", ". TABLE_LEAD_HISTORY .".lead_owner		    = '". $data['lead_owner'] ."'"
								.", ". TABLE_LEAD_HISTORY .".lead_source_to		= '". $data['lead_source_to'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".lead_source_from	= '". $data['lead_source_from'] ."'"
								.", ". TABLE_LEAD_HISTORY .".lead_campaign_id   = '". $data['lead_campaign_id'] ."'"
								.", ". TABLE_LEAD_HISTORY .".lead_industry_id   = '". $data['lead_industry_id'] ."'"
								.", ". TABLE_LEAD_HISTORY .".no_emp			   	= '". $data['no_emp'] ."'"
								.", ". TABLE_LEAD_HISTORY .".rating			   	= '". $data['rating'] ."'"
								.", ". TABLE_LEAD_HISTORY .".annual_revenue   	= '". $data['annual_revenue'] ."'"
								.", ". TABLE_LEAD_HISTORY .".rivals_id   		= '". $data['rivals_id'] ."'"
								.", ". TABLE_LEAD_HISTORY .".email       		= '". $data['email'] ."'"
								.", ". TABLE_LEAD_HISTORY .".email_1     		= '". $data['email_1'] ."'"
								.", ". TABLE_LEAD_HISTORY .".email_2     		= '". $data['email_2'] ."'"
								.", ". TABLE_LEAD_HISTORY .".title       		= '". $data['title'] ."'"
								.", ". TABLE_LEAD_HISTORY .".f_name      		= '". $data['f_name'] ."'"
								.", ". TABLE_LEAD_HISTORY .".m_name      		= '". $data['m_name'] ."'"
								.", ". TABLE_LEAD_HISTORY .".l_name      		= '". $data['l_name'] ."'"
								.", ". TABLE_LEAD_HISTORY .".p_name      		= '". $data['p_name'] ."'"
								.", ". TABLE_LEAD_HISTORY .".desig       		= '". $data['desig'] ."'"
								.", ". TABLE_LEAD_HISTORY .".org         		= '". $data['org'] ."'"
								.", ". TABLE_LEAD_HISTORY .".domain      		= '". $data['domain'] ."'"
								.", ". TABLE_LEAD_HISTORY .".gender      		= '". $data['gender'] ."'"
								.", ". TABLE_LEAD_HISTORY .".do_birth    		= '". $data['do_birth'] ."'"
								.", ". TABLE_LEAD_HISTORY .".do_aniv     		= '". $data['do_aniv'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".do_add     		= '". $data['do_add'] ."'"
								.", ". TABLE_LEAD_HISTORY .".remarks     		= '". $data['remarks'] ."'"
								.", ". TABLE_LEAD_HISTORY .".already_exist_no	= '". $data['already_exist_no'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".followup_no		= '". $data['followup_no'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".lead_number		= '". $data['lead_number'] ."'"
								.", ". TABLE_LEAD_HISTORY .".updated_by		    = '". $my['uid'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".ip 	    	    = '". $data['ip'] ."'"
                                .", ". TABLE_LEAD_HISTORY .".do_update      	= '". date('Y-m-d H:i:s', time()) ."'";
								
            if ( $db->query($query) ) {
                 $history_id=$db->last_inserted_id();
                // Insert into address history.
                $addresslist=NULL;
                $query = "SELECT * FROM ".TABLE_LEAD_ADDRESS." WHERE address_of='".$data['lead_id']."'";
                if ( $db->query($query) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                            $addresslist[] = processSqlData($db->result());
                        }    
                    }
                }
                foreach ($addresslist as $key=>$value) {
                    $query	= " INSERT INTO ". TABLE_ADDRESS_HISTORY
                                    ." SET ".TABLE_ADDRESS_HISTORY .".h_id  		= '". $history_id ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".table_name		= '". $addresslist[$key]['table_name'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".address_of		= '". $addresslist[$key]['address_of'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".company_name    = '". $addresslist[$key]['company_name'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".address         = '". $addresslist[$key]['address'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".city            = '". $addresslist[$key]['city'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".state           = '". $addresslist[$key]['state'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".country         = '". $addresslist[$key]['country'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".zipcode         = '". $addresslist[$key]['zipcode'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".address_type    = '". $addresslist[$key]['address_type'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".is_preferred	= '". $addresslist[$key]['is_preferred'] ."'"
                                    .", ". TABLE_ADDRESS_HISTORY .".is_verified 	= '". $addresslist[$key]['is_verified'] ."'";
                    $db->query($query);                
                }
                
            
                // Insert into phone history.
                $phonelist=NULL;
                $query = "SELECT * FROM ".TABLE_LEAD_PHONE." WHERE phone_of='".$data['lead_id']."'";
                if ( $db->query($query) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                            $phonelist[] = processSqlData($db->result());
                        }    
                    }
                }
                foreach ($phonelist as $key=>$value) {
                    $query	= " INSERT INTO ". TABLE_PHONE_HISTORY
                                    ." SET ".TABLE_PHONE_HISTORY .".h_id  		    = '". $history_id ."'"
                                    .", ". TABLE_PHONE_HISTORY .".table_name		= '". $phonelist[$key]['table_name'] ."'"
                                    .", ". TABLE_PHONE_HISTORY .".phone_of		    = '". $phonelist[$key]['phone_of'] ."'"
                                    .", ". TABLE_PHONE_HISTORY .".cc    	    	= '". $phonelist[$key]['cc'] ."'"
                                    .", ". TABLE_PHONE_HISTORY .".ac               	= '". $phonelist[$key]['ac'] ."'"
                                    .", ". TABLE_PHONE_HISTORY .".p_number         	= '". $phonelist[$key]['p_number'] ."'"
                                    .", ". TABLE_PHONE_HISTORY .".p_type            = '". $phonelist[$key]['p_type'] ."'"
                                    .", ". TABLE_PHONE_HISTORY .".p_is_preferred	= '". $phonelist[$key]['p_is_preferred'] ."'"
                                    .", ". TABLE_PHONE_HISTORY .".p_is_verified 	= '". $phonelist[$key]['p_is_verified'] ."'";
                    $db->query($query);                
                }	
            }
        }
        
        //This function is used to perform action on status.
		function markDead($id, $status_new, $tbl_name, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( !in_array($status_new, Leads::getLeadStatus()) ) {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
            else {
                $order = NULL;
                $query = "UPDATE ". TABLE_SALE_LEADS
                                        ." SET lead_status = '$status_new' "
                                        ." WHERE lead_id = '$id'"; 
                if ( $db->query($query) && $db->affected_rows()>0 ) {
                
                    $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                            ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $followup_no ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".title_id 		= '". $data ['title_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".remarks 		= ' Lead has been Dead '"
                                //.",". TABLE_SALE_FOLLOWUP .".date    		= '". $data['date'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".time    		= '". $data ['hrs'].':'.$data ['min'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".time_type 		= '". $data ['time_type'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".lead_id    	= '". $id ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data ['lead_id'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".assign_to 		= '". $data ['assign_to'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $id ."'"
                                .",". TABLE_SALE_FOLLOWUP .".table_name    	= 'sale_leads'"
								.",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
								.",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
								.",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". FOLLOWUP::LEADM ."'"
                                .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d H:i:s') ."'";
                    $db->query($query);            
                    $messages->setOkMessage("The Status has been updated.");
                }
                else {
                    $messages->setErrorMessage("The Status was not updated.");
                }
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
    }
?>