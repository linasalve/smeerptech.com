<?php
	class SmsTemplate {
		
        const PENDING = 0;
		const APPROVED  = 1;
		const DISAPPROVED  = 2;
         
		function getStatus(){
			$status = array( 
						'PENDING'   => SmsTemplate::PENDING,
                        'APPROVED'   => SmsTemplate::APPROVED,
                        'DISAPPROVED' => SmsTemplate::DISAPPROVED
					);
			return ($status);
		}
		
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_TEMPLATES; 
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SMS_TEMPLATES;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (SmsTemplate::getList($db, $details, 'id', " WHERE api_id = '$id'")) == 0 ) {
                //$details = $details[0];
                //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_SMS_TEMPLATES
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            }
            else {
                $messages->setErrorMessage("The Selected Api is used in SMS Purchase. Please delete related record from the SMS Purchase");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
           
            if ( !isset($data['sms_account_id']) || empty($data['sms_account_id']) ) {
                $messages->setErrorMessage("Select account id.");
            }       
            if ( !isset($data['template_name']) || empty($data['template_name']) ) {
                $messages->setErrorMessage("Enter Template name.");
            }    
			if ( empty($file['sample_file']['error']) ) {
				if ( !empty($file['sample_file']['tmp_name']) ) {
					if ( $file['sample_file']['size'] == 0 ) {
						$messages->setErrorMessage('The Uploaded file is empty.');
					}
					if ( !in_array($file['sample_file']['type'], $CSV['MIME']) ) {
						$messages->setErrorMessage('The File type is not allowed to be uploaded.');
					}
					if ( $file['sample_file']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
						$messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
					}
				}
				else {
					$messages->setErrorMessage('Server Error: File was not uploaded.');
				}
			}
			else {
				$messages->setErrorMessage('Unexpected Error: File was not uploaded.');
			} 

			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            if ( !isset($data['api_id']) || empty($data['api_id']) ) {
                $messages->setErrorMessage("Select gateway.");
            }       
            if ( !isset($data['sender_name']) || empty($data['sender_name']) ) {
                $messages->setErrorMessage("Enter Sender Name.");
            }else{
				$sender_name = trim($data['sender_name']);
				$len = strlen($sender_name);
				if($len!=6){
					$messages->setErrorMessage("Sender Name should be 6 character ");
				}
			}     
			    
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            $query = "UPDATE ". TABLE_SMS_TEMPLATES	." SET status = '$status_new', status_marked_by='".$my['user_id']."', 
				status_marked_by_name='".$my['f_name']." ".$my['l_name'].", status_marked_dt ='".date('Y-m-d H:i:s')."'	"." WHERE id = '$id'";
				
			if ( $db->query($query) && $db->affected_rows()>0 ) {
				$messages->setOkMessage("The Status has been updated.");
			}
			else {
				$messages->setErrorMessage("The Status was not updated.");
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

    }
?>
