<?php
	class SmsClientAccount {  
		
        const ACTIVE = 1;
		const PENDING  = 0;
        
      
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_CLIENT_ACCOUNT;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SMS_CLIENT_ACCOUNT;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (SmsSaleClient::getList($db, $details, 'id', " WHERE client_acc_id = '$id'")) == 0 ) {
                //$details = $details[0];
                //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_SMS_CLIENT_ACCOUNT
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            /* }
            else {
                $messages->setErrorMessage("The Selected Client Account is in used. Please delete related record from the SMS Client Sale Module");
            } */
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
            if ( !isset($data['client_id']) || empty($data['client_id']) ) {
                $messages->setErrorMessage("Select Client.");
            }else{
                $edetails	= 	NULL;
                $condition_querye=" WHERE "." user_id='".$data['client_id']."'";
                Clients::getList( $db, $edetails, 'f_name, l_name, number, email, balance_sms, do_expiry', $condition_querye);
                $edetails = $edetails[0];
                $data['client_details'] = $edetails['f_name']." ".$edetails['l_name']." (".$edetails['number'].") Bal. Sms - ".$edetails['balance_sms'].' Dt of expiry - '.$edetails['do_expiry'] ;  
            } 
                
            if ( !isset($data['api_id']) || empty($data['api_id']) ) {
                $messages->setErrorMessage("Select API id.");
            }   
			if ( !isset($data['sender_id']) || empty($data['sender_id']) ) {
                $messages->setErrorMessage("Select Sender.");
            } 
            if ( !isset($data['name']) || empty($data['name']) ) {
                $messages->setErrorMessage("Account name shhould not be empty.");
            }  

			if ( !isset($data['credit_sms']) || empty($data['credit_sms']) ) {
                $messages->setErrorMessage("No. of sms can not be empty.");
            }elseif (!is_numeric($data['credit_sms']) ){
                $messages->setErrorMessage("No. of sms should be in numeric.");
            }else{
				if($edetails['balance_sms'] < $data['credit_sms']){
					$messages->setErrorMessage("Available balance sms (".$edetails['balance_sms'].") in client's account is less than the allotted sms (".$data['credit_sms'].").");
				}
			}
			/*  if ( !isset($data['unit_price']) || empty($data['unit_price']) ) {
                $messages->setErrorMessage('Unit price should be provided.');
            }elseif (!is_numeric($data['unit_price']) ){
                $messages->setErrorMessage("Unit price should be in numeric.");
            } */
            /* 
			if ( !isset($data['display_name']) || empty($data['display_name']) ) {
                $messages->setErrorMessage("Display name shhould not be empty.");
            }    
            if ( !isset($data['mobile_no']) || empty($data['mobile_no']) ) {
                $messages->setErrorMessage("Mobile no shhould not be empty.");
            }   
            if(!empty($data['mobile_no'])){
                if(!is_numeric($data['mobile_no']) ){
                    $messages->setErrorMessage("Mobile no should be number.");
                }
            } 
            $condition_query1= "WHERE api_id='".$data['api_id']."' AND gateway_id='".$data['gateway_id']."'";
            if( (SmsClientAccount::getList($db, $details, 'id', $condition_query1) ) == 0){
                $messages->setErrorMessage("This Gateway Api pair is already exists. Plz select another.");
            }
           
            if ( !isset($data['do_expiry']) || empty($data['do_expiry']) ) {
                $messages->setErrorMessage("Date of Expiry should not be empty.");
            }else{
                if ( isset($data['do_expiry']) && !empty($data['do_expiry']) ) {
                    $data['do_expiry'] = explode('/', $data['do_expiry']);
                    $data['do_expiry'] = mktime(0, 0, 0, $data['do_expiry'][1], $data['do_expiry'][0], $data['do_expiry'][2]);
                }
            }
             */
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            if ( !isset($data['client_id']) || empty($data['client_id']) ) {
                $messages->setErrorMessage("Select Client.");
            }else{
                $edetails	= 	NULL;
                $condition_querye=" WHERE "." user_id='".$data['client_id']."'";
                Clients::getList( $db, $edetails, 'f_name, l_name, number, email, balance_sms, do_expiry', $condition_querye);
                $edetails = $edetails[0];
                $data['client_details'] = $edetails['f_name']." ".$edetails['l_name']." (".$edetails['number'].") Bal. Sms - ".$edetails['balance_sms'].' Dt of expiry - '.$edetails['do_expiry'] ; 
            }
            /*  
			if ( !isset($data['gateway_id']) || empty($data['gateway_id']) ) {
                $messages->setErrorMessage("Select gateway.");
            }       
            if ( !isset($data['api_id']) || empty($data['api_id']) ) {
                $messages->setErrorMessage("Select API id.");
            }    
			*/
            if ( !isset($data['name']) || empty($data['name']) ) {
                $messages->setErrorMessage("Account name shhould not be empty.");
            }               
            if ( !empty($data['credit_sms']) ) {
                if (!is_numeric($data['credit_sms']) ){
					$messages->setErrorMessage("No. of sms should be in numeric.");
				}else{
					if($edetails['balance_sms'] < $data['credit_sms']){
						$messages->setErrorMessage("Available balance sms (".$edetails['balance_sms'].") in client's account is less than the allotted sms (".$data['credit_sms'].").");
					}
				}   
            }  
            
            /*
            if ( !isset($data['do_expiry']) || empty($data['do_expiry']) ) {
                $messages->setErrorMessage("Date of Expiry should not be empty.");
            }else{
                if ( isset($data['do_expiry']) && !empty($data['do_expiry']) ) {
                    $data['do_expiry'] = explode('/', $data['do_expiry']);
                    $data['do_expiry'] = mktime(0, 0, 0, $data['do_expiry'][1], $data['do_expiry'][0], $data['do_expiry'][2]);
                }
            }*/
            
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
    }
?>
