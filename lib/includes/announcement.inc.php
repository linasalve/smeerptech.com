<?php
	class Announcement {
		
		const DEACTIVE = 0;
		const ACTIVE  = 1;      
        
        function getStatus() {
			$status = array(
							'ACTIVE'   => Announcement::ACTIVE,
                            'DEACTIVE'    => Announcement::DEACTIVE
						);
			return ($status);
		}   
        
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_ANNOUNCEMENT;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_ANNOUNCEMENT;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;
							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_ANNOUNCEMENT
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			} 
			
			/*if ( !isset($data['do_announcement']) || empty($data['do_announcement']) ) {
                $messages->setErrorMessage("Select the Announcement date.");
            }
            
            if ( !empty($data['do_announcement']) ) {
                $temp = explode('/', $data['do_announcement']);
               
                $data['do_announcement'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }  */    
                       
             if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage("Title can not be empty.");
            }    
            
             if ( !isset($data['description']) || empty($data['description']) ) {
                $messages->setErrorMessage("Description can not be empty.");
            }  
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            
           /* if ( !isset($data['do_announcement']) || empty($data['do_announcement']) ) {
                $messages->setErrorMessage("Select the Announcement date.");
            }
            
            if ( !empty($data['do_announcement']) ) {
                $temp = explode('/', $data['do_announcement']);
                
               
                $data['do_announcement'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }*/
            
             if ( !isset($data['title']) || empty($data['title']) ) {
                $messages->setErrorMessage("Title can not be empty.");
            }    
            
             if ( !isset($data['description']) || empty($data['description']) ) {
                $messages->setErrorMessage("Description can not be empty.");
            }  
                   
            
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_ANNOUNCEMENT
                            ." SET ". TABLE_ANNOUNCEMENT .".status = '".Announcement::ACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Status has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Status has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_ANNOUNCEMENT
                            ." SET ". TABLE_ANNOUNCEMENT .".status = '".Announcement::DEACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Status has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Status has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    
           /**
         * This function is used to update the status of the Executive.
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, Announcement::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (Announcement::getList($db, $job, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
                    $job = $job[0];
                   // if ( $user['access_level'] < $access_level ) {
                      $query = "UPDATE ". TABLE_ANNOUNCEMENT
                                    ." SET status = '$status_new' "
                                    ." WHERE id = '$id'";
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            $messages->setOkMessage("The Status has been updated.");
                        }
                        else {
                            $messages->setErrorMessage("The Status was not updated.");
                        }
                    /*}
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of Executives with the current Access Level.");
                    }*/
                }
                else {
                    $messages->setErrorMessage("The ideas was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
		
    }
?>
