<?php
	/**
	 * Short description for file
	 * This file manages -- Coporate user Sign-Up
	 * Module for the ehotel
	 *
	 * PHP versions All
	 *
	 * @category   Includes File ( User-Corporate-Inc )
	 * @package    ehotel
	 * @author     
	 * @copyright  SMEERP Technologies
	 * @license    As described below
	 * @version    1.2.0
	 * @link       
	 * @see        -NA-
	 * @since      File available since Release 1.2.0 dt. Tuesday, 07 June, 2005
	 * @deprecated -NA-
	 */

	/*********************************************************
	* Licence:
	* This file is sole property of the installer.
	* Any type of copy or reproduction without the consent
	* of owner is prohibited.
	* If in any case used leave this part intact without 
	* any modification.
	* All Rights Reserved
	* Copyright 2007 Owner
	*******************************************************/
	class sendSMS
	{
		
		 function getRestrictions($type='') {
			// 1024 * 1024 = 1048576 = 1MB
			/* $restrictions = array(
								'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), 
								'MIME'      => array('text/csv','application/vnd.ms-excel', 'text/plain','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
								'EXT'       => array('csv', 'txt','xlsx')
								 ); */
			$restrictions = array(
								'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), 
								'MIME'      => array('application/vnd.ms-excel'),
								'EXT'       => array('xls')
								 );
			
			if ( empty($type) ) {
				return ($restrictions);
			}
			else {
				return($restrictions[$type]);
			}
		}
		function getGroupMobile($postdata)
		{
			$db         = new db_local;
            $condition  = "";
            if(!empty($postdata["group"])){
                foreach($postdata["group"] as $key=>$value)
                {
                    if ( $value != '' ) {
                        $condition .= " ( catID REGEXP '^$value$'"
                                        ." OR catID REGEXP ',$value$'"
                                        ." OR catID REGEXP ',$value,'"
                                        ." OR catID REGEXP '^$value,' ) OR ";
                    }
                }
            }
            if ( empty($condition) ) {
                return (false);
            }
            
            $condition = substr($condition, 0, strlen($condition)-3 );

			//default setting
			$email = "primary_mobile";

			//conditional setting
			if($postdata["primmob"] == "p")
			{
				$email = "primary_mobile";
				$mobflg = "p";
			}
			if($postdata["secndmob"] == "s")
			{
				$email = "secondary_mobile";
				$mobflg = "s";
			}
			if($postdata["primmob"] == "p" && $postdata["secndmob"] == "s")
			{
				$email = "primary_mobile,secondary_mobile";
				$mobflg = "ps";
			}

			$mob = "";
            $sql = "SELECT ".$email." FROM ".TABLE_SMS_CLIENTS_USER." WHERE $condition";
//echo $sql;
			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
				{
					if($mob != "")					
						$mob .= ", ";

					if($mobflg == "p")
					{
						if($db->f("primary_mobile") != 0)
							$mob .= $db->f("primary_mobile");
					}
					if($mobflg == "s")
					{
						if($db->f("secondary_mobile") != 0)
							$mob .= $db->f("secondary_mobile");
					}
					if($mobflg == "ps")
					{
						if($db->f("primary_mobile") != 0)
							$mob .= $db->f("primary_mobile");
						if($db->f("secondary_mobile") != 0)
							$mob .= ",".$db->f("secondary_mobile");
					}

				}	
				return $mob;
			}
		}

	    function validateMobile($mobile)
	    {
			if(is_numeric($mobile) && strlen($mobile) == 12 && substr($mobile,0,2) == 91 && substr($amobile,2,1) == 9)
	    		return true;
	    	else
	    		return false;
	    }

	    function getAllIncorrectNos($mobile)
	    {
	    	// 919960475875, 119969475875, 910960475875, 91096047587599,
	    	$incorrect_nos = "";
	    	$mobile = explode(",",$mobile);
	    	
	    	foreach($mobile as $key=>$value)
	    	{
				$value = trim($value);
	    		if(is_numeric($value) && strlen($value) == 12 && substr($value,0,2) == 91 && substr($value,2,1) == 9)
	    		{
	    		}
				else
				{
	    			if(!empty($incorrect_nos))
						$incorrect_nos .= ",".$value;
	    			else
	    				$incorrect_nos = $value;
				}
	    	}
	    	//echo $incorrect_nos;
	    	return $incorrect_nos;
	    }				
		/*
		*
		*/
		function validateTemplateSms( $posted_data, $extra ){	
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            if(Validation::isEmpty($posted_data["sms_account_id"] )){
                $messages->setErrorMessage("Please Account.");
            }else{
				if($posted_data["balance_sms"] == 0){
					$messages->setErrorMessage("Insufficient Credit. Please contact your service provider.");
				}
			}
			if(Validation::isEmpty($posted_data["sender_id"] )){
                $messages->setErrorMessage("Please Sender.");
            }
			
			if ( empty($file['filmobnum']['error']) ) {
				if ( !empty($file['filmobnum']['tmp_name']) ) {
					if ( $file['filmobnum']['size'] == 0 ) {
						$messages->setErrorMessage('The Uploaded file is empty.');
					}
					if ( !in_array($file['filmobnum']['type'], $CSV['MIME']) ) {
						$messages->setErrorMessage('The File type is not allowed to be uploaded.');
					}
					if ( $file['filmobnum']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
						$messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
					}
				}
				else {
					$messages->setErrorMessage('Server Error: File was not uploaded.');
				}
			}
			else {
				$messages->setErrorMessage('Unexpected Error: File was not uploaded.');
			}
			
			/* if( Validation::isEmpty( $posted_data["message1"] )) {
				$messages->setErrorMessage("Please enter the Message.");
			}elseif( !empty($posted_data["message1"]) 
					&& strlen($posted_data["message1"]) > '160' 
					&& $posted_data["chk_personal"]=='0')
			{
				
				$messages->setErrorMessage("Nos of characters are restricted to 145 only.");
			} */
			 

			if( $messages->getErrorMessageCount()<= 0 ) {
				return true;
			}
			else{
				return false;
			}
		}
		
		
		
		function validateRecord( $posted_data, $extra ){	
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			if($posted_data["balance_sms"] == 0){
				$messages->setErrorMessage("Insufficient Credit. Please contact your service provider.");
			}
            if(Validation::isEmpty($posted_data["sms_account_id"] )){
                $messages->setErrorMessage("Please Account.");
            }
			if(Validation::isEmpty($posted_data["sender_id"] )){
                $messages->setErrorMessage("Please Sender.");
            }
			
			if ( empty($file['filmobnum']['error']) ) {
				if ( !empty($file['filmobnum']['tmp_name']) ) {
					if ( $file['filmobnum']['size'] == 0 ) {
						$messages->setErrorMessage('The Uploaded file is empty.');
					}
					if ( !in_array($file['filmobnum']['type'], $CSV['MIME']) ) {
						$messages->setErrorMessage('The File type is not allowed to be uploaded.');
					}
					if ( $file['filmobnum']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
						$messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
					}
				}
				else {
					$messages->setErrorMessage('Server Error: File was not uploaded.');
				}
			}
			else {
				$messages->setErrorMessage('Unexpected Error: File was not uploaded.');
			}
			
			if( Validation::isEmpty( $posted_data["message1"] )) {
				$messages->setErrorMessage("Please enter the Message.");
			}elseif( !empty($posted_data["message1"]) 
					&& strlen($posted_data["message1"]) > '160' 
					&& $posted_data["chk_personal"]=='0')
			{
				
				$messages->setErrorMessage("Nos of characters are restricted to 145 only.");
			}
			/*
			if( Validation::isEmpty( $posted_data["mobile"] ) && !isset($_FILES['filmobnum']['name'])) {
				$messages->setErrorMessage("Please enter the mobile nos or select nos from the links provided.");
			}
			elseif(!isset($_FILES['filmobnum']['name']))
			{
				$incorrect_nos = sendSMS::getAllIncorrectNos($posted_data["mobile"]);
				if(!empty($incorrect_nos))
				{
					$messages->setErrorMessage("Following nos entered by you are invalid "."<b>".$incorrect_nos."</b>"
												."<br/> Please Correct the format  of mobile no
												<br/> Use 91 as country code ( INDIA ) 
												<br/> Mobile nos should start with digit 9 
												<br/> Mobile should be of 10 digits, including country code (91) it should be 12 digits 
												<br/><b> for e.g. 919371136693</b> ");
				}
			}
	    	if(!preg_match("/^([0-9]{12}|,|\s)+$/",$posted_data['mobile']) && !isset($_FILES['filmobnum']['name'] ))
				$messages->setErrorMessage("Phone numbers should be valid.");
			if( Validation::isEmpty( $posted_data["message1"] )) {
				$messages->setErrorMessage("Please enter the Message.");
			}
			elseif( !empty($posted_data["message1"]) 
					&& strlen($posted_data["message1"]) > '160' 
					&& $posted_data["chk_personal"]=='0')
			{
				
				$messages->setErrorMessage("Nos of characters are restricted to 145 only.");
			}
			elseif( !empty($posted_data["message2"]) 
					&& strlen($posted_data["message2"]) > '115' 
					&& $posted_data["chk_personal"]=='1')
			{
				
				$messages->setErrorMessage("Nos of characters are restricted to 100 only.");
			}*/

			if( $messages->getErrorMessageCount()<= 0 ) {
				return true;
			}
			else{
				return false;
			}
		}
		function sendBulkSms($mobile,$message,$display_name,$api_details){ 
			$db         = new db_local;   			
			$message = urlencode($message); 
			$url = $api_details['api_post_url']."?username=".$api_details['api_username']."&password=".$api_details['api_password']."&type=0&dlr=1&destination=".$mobile."&source=".$display_name."&message=".$message ;			
			
			/*
			$url="http://103.16.101.52:8080/sendsms/bulksms?username=pet-prec&password=123456&type=0&dlr=1&destination=".$mobile."&source=".$display_name."&message=".$message ;
			*/

			$format = 'application/json';
			$curl_session = curl_init();
			$headers = array(); 
			curl_setopt($curl_session, CURLOPT_URL, $url); 
			curl_setopt($curl_session, CURLOPT_HEADER, false);
			curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($curl_session); 
			curl_close($curl_session); 
			
			return $response ;
		
			/* $array = json_decode(stripslashes($response),true);
			print_R($array);
			*/ 
			
			
        }
        
		/*
		function checkResponse($response){
			$host = ProjectTask::HOST;
            $service_url = ProjectTask::SERVICE_URL ;
            $VF_USERNAME =ProjectTask::VF_USERNAME ;
            $VF_PASSWORD =ProjectTask::VF_PASSWORD ;
            $display_name= ProjectTask::DISPLAY_NAME;
			$msg_sent_sts = false;
			
			$service_url1 = $service_url."/response.php?" ;
 			$string = "Scheduleid=".trim($response);
			$response2 = ProjectTask::httpSmsConnection($host, $service_url1, "post", $string);
			$response2 = strtoupper($response2);
			$rspn1 = strstr($response2, 'DELIVRD', true);
			$rspn2 = strstr($response2, 'PENDING', true);
			
			if($rspn1=='DELIVRD' || $rspn2=='PENDING'){
				$msg_sent_sts = true;
			}
			return $msg_sent_sts ;
		}
		function httpSmsConnection($host, $service_url, $method, $data) {
			$service_url = trim($service_url);
			$data= trim($data);
            $fp = fsockopen($host, 80, $errno, $errstr, 30);
			if (!$fp) {
				echo "$errstr ($errno)<br />\n";
			} else {
				$out = "GET ".$service_url.$data."HTTP/1.0\r\n";
				$out .= "Host: ".$host."\r\n";
				$out .= "Content-Type: application/x-www-form-urlencoded\r\n";
				$out .= "Content-Length: ".strlen($data). "\r\n\r\n";
				$out .= "Connection: Close\r\n\r\n";
				fwrite($fp, $out);
				fwrite($fp, $data);
				while (!feof($fp)) {
					echo fgets($fp, 128);
				}
				fclose($fp);
			}
	    }
		*/
		/*
		*
		*/		
        /*
		function saveRecord( &$db, $posted_data, &$messages )
		{
            $ins_qry    = "";
            $ins_qry    = "INSERT INTO "
                            . TABLE_SMS_CLIENTS_USER
                            . " SET "
                            . " catID                          = '". $posted_data["cat_id"] ."'"
                            . " , " . " title                  = '". $posted_data["title"] ."'"
                            . " , " . " fname                  = '". $posted_data["fname"] ."'"
                            . " , " . " lname                  = '". $posted_data["lname"] ."'"
                            . " , " . " company_name           = '". $posted_data["company_name"] ."'"
                            . " , " . " designation            = '". $posted_data["designation"] ."'"
                            . " , " . " primary_email          = '". $posted_data["primary_email"] ."'"
							. " , " . " secondary_email        = '". $posted_data["secondary_email"] ."'"
                            . " , " . " r_address              = '". $posted_data["r_address"] ."'"
                            . " , " . " r_city                 = '". $posted_data["r_city"] ."'"
                            . " , " . " r_state                = '". $posted_data["r_state"] ."'"
                            . " , " . " r_country              = '". $posted_data["r_country"] ."'"
                            . " , " . " r_pincode              = '". $posted_data["r_pincode"] ."'"
                            . " , " . " r_phone_c_code         = '". $posted_data["r_phone_c_code"] ."'"
                            . " , " . " r_phone_s_code         = '". $posted_data["r_phone_s_code"] ."'"
                            . " , " . " r_phone                = '". $posted_data["r_phone"] ."'"
                            . " , " . " primary_mobile         = '". $posted_data["mobile1"] ."'"
							. " , " . " secondary_mobile       = '". $posted_data["mobile2"] ."'"	
                            ;
            //exit;
            $db->query( $ins_qry );
            if( $db->affected_rows() > 0 ) {
                return true;
            }
            else
            {
                return false;
            }
			//EOF :: if( $db->affected_rows() > 0 )
		}
        */
		//EOF :: Function
		
		
		
		/*
		** Function to fetch data for particular user
		** against its mobile no
		*/
		function getSubscriberRecord(&$db, $mobile, $required_fields, &$data, &$string_data)
		{
/*			$query = " SELECT ".$required_fields
					." FROM ".TABLE_SMS_CLIENTS_USER
					." WHERE ( primary_mobile REGEXP '^$mobile$'"
					." OR primary_mobile REGEXP ',$mobile$'"
					." OR primary_mobile REGEXP ',$mobile,'"
					." OR primary_mobile REGEXP '^$mobile,' ) ";*/
			$query = " SELECT ".$required_fields ." FROM ".TABLE_SMS_CLIENTS_USER ." WHERE primary_mobile='".$mobile."'";

			$db->query($query);
			if($db->nf() > 0)
			{
				$db->next_record();
				$data = array(
								"fname"=>$db->f("fname"),
								"lname"=>$db->f("lname")
							);
				//print_r($data);
				$string_data = $db->f("fname")." ".$db->f("lname");
				return true;
				
			}
			else
			{
				$data = "";
				return false;
			}
		}
		
		
		// this will give array of all periods in which message is to repeated
		function getAllTimeInterval()
		{
			$time_interval = array(
								"day" 	=> "Day",
								"month" => "Month",
								"year" 	=> "Year",
								"hour" 	=> "Hour",
								"min" 	=> "Minute",
							);
			return ($time_interval);
		}
		
		/* 
		* this will give array of all personalize 
		*/
		function getAllPersonalize()
		{
			$personal_arr = array(
									"Dear",
									"Hello",
									"Namaskar",
									"Salam",
									"Good Morning",
									"Good Evening",
									"Good Night",
									"Good Afternoon"
								);
			return ($personal_arr);
		}
		/* 
		* this will give array of all personalize fields to be send along with personalize words
		*/
		function getPersonalizeFields()
		{
			$personal_arr = array(
									"fname" => "First Name",
									"lname" => "Last Name",
								);
			return ($personal_arr);
		}
			
		function getSmsList(&$db, &$user_list, $required_fields, $condition="", $from="", $rpp="")
		{
			
			$query = "SELECT ";
			
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) 
			{
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) 
			{
				$query .= " ". $required_fields;
			}
			else 
			{
				$query .= " * ";
			}
			$query .= " FROM ". TABLE_SMS_HISTORY;
			
			$query .= " ". $condition;
			
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) 
			{
				$query .= " LIMIT ". $from .", ". $rpp;
			}
			//echo $query;
			if ( $db->query($query) ) 
			{
				if ( $db->nf() > 0 ) 
				{
					$i = 0;
					while ($db->next_record()) 
					{
						$user_list[$i]["sh_id"] = $db->f("sh_id");
						$user_list[$i]["sh_userid"] = $db->f("sh_userid");
						$user_list[$i]["sh_message"] = $db->f("sh_message");
						$user_list[$i]["sh_count"] = $db->f("sh_count");
						$user_list[$i]["sh_date"] = $db->f("sh_date");
						//$user_list[$i]["failstatus"] = sendSMS::getMsgType($db->f("sh_id"),"F");
						//$user_list[$i]["deliverystatus"] = sendSMS::getMsgType($db->f("sh_id"),"D");
						//$user_list[$i]["sendtatus"] = sendSMS::getMsgType($db->f("sh_id"),"S");	
						$user_list[$i]["status"] = sendSMS::getMsgType($db->f("sh_id"));		
                        $user_list[$i]["cf_name"] = $db->f("cf_name");				
                        $user_list[$i]["cl_name"] = $db->f("cl_name");		                        
						$i++;
					}
				}
				//print_r($hotel_list);
				return ( $db->nf() );
			}
			else 
			{
				return false;
			}
		}

		function getMsgType($shpshid)
		{
			$db = new db_local;
			
			$sql = "SELECT shp_msgstatus FROM ".TABLE_SMS_HISTORY_PHONE." WHERE shp_shid='".$shpshid."'";
			$db->query($sql);
			if($db->nf() > 0)
			{
				$db->next_record();
				return $db->f("shp_msgstatus");
			}
			else
				return 0;
		}	

		/*function viewmsgdetails(&$list_array,$shid)
		{
			$db = new db_local;

		    $list_query = "SELECT shp_phoneno,shp_msgstatus FROM ". TABLE_SMS_HISTORY_PHONE." WHERE shp_shid='".$shid."'";
			//echo $list_query;
		    $db->query( $list_query );
			if($db->nf() > 0)
			{
			    while(  $db->next_record()  )  
					$res[] = $db->Record;

				$list_array = $res;
				return true;	
			}
			else
				return false;
		}*/

		function getSmsHistoryId($mobileno)
		{
			$db = new db_local;
			$id = "";

			$sql = "SELECT shp_shid From ".TABLE_SMS_HISTORY_PHONE." WHERE shp_phoneno='".$mobileno."'";

			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
				{
					if($id != "")
						$id .= ",";

					$id .= $db->f("shp_shid");
				}
				return $id;
			}
		}
        
        //added in class 2009-06-jun-09
        function getAllSMSGroup($uid,&$output,$catID=0,$level = 0) {
            global $output;
       
            $db = new db_local;
            if(!empty($uid)){
                $query = "SELECT * FROM  ".TABLE_SMS_USER_GROUP." WHERE parentId='$catID' AND active='1' AND  groups_of_uid='".$uid."'";    
            }else{
            
                $query = "SELECT * FROM  ".TABLE_SMS_USER_GROUP." WHERE parentId='$catID' AND active='1'";
            }
            
            
            $db->query($query);
    
            if($db->nf() ==0) return;
    
            while($db->next_record()) {
                            $output[$db->f("catID")] = str_repeat('&nbsp;', $level * 4).'&#187; '.$db->f("CatName");
                            sendSMS::getAllSMSGroup($uid,$output,$db->f("catID"), $level+1);
            }
           
        }
        
        
        function getAccountList($uid){
            $db = new db_local;
            $accountList=array();
            $query = "SELECT ".TABLE_SMS_SALE_CLIENT.".id as sms_sale_id,".TABLE_SMS_SALE_CLIENT.".balance_sms, ".TABLE_SMS_CLIENT_ACCOUNT.".name as account_name
                        FROM  ".TABLE_SMS_SALE_CLIENT."
                        LEFT JOIN ".TABLE_SMS_CLIENT_ACCOUNT." ON ".TABLE_SMS_SALE_CLIENT.".client_id = ".TABLE_SMS_CLIENT_ACCOUNT.".client_id
                        WHERE ".TABLE_SMS_SALE_CLIENT.".client_id='$uid' AND ".TABLE_SMS_SALE_CLIENT.".balance_sms >0 AND ".TABLE_SMS_SALE_CLIENT.".do_expiry >= now()";
            $db->query($query);
            while($db->next_record()) {
                $accountList[] =  array(
                                        'id'=>$db->f("sms_sale_id"),
                                        'account_name'=>$db->f("account_name"),
                                        'balance_sms'=>$db->f("balance_sms"),
                                    );
            
            }
            return $accountList;
        }
        
        function getConnectionDetails($sale_id){
        
            $db = new db_local;
            $details=array();
            $query = "SELECT ".TABLE_SMS_API.".post_url,".TABLE_SMS_API.".username,".TABLE_SMS_API.".password,".TABLE_SMS_API.".host
                        FROM  ".TABLE_SMS_SALE_CLIENT."
                        LEFT JOIN ".TABLE_SMS_CLIENT_ACCOUNT." ON ".TABLE_SMS_SALE_CLIENT.".client_acc_id = ".TABLE_SMS_CLIENT_ACCOUNT.".id
                        LEFT JOIN ".TABLE_SMS_API." ON ".TABLE_SMS_API.".id = ".TABLE_SMS_CLIENT_ACCOUNT.".api_id
                        WHERE ".TABLE_SMS_SALE_CLIENT.".id='$sale_id'";
            $db->query($query);
            while($db->next_record()) {
                $details =  array(
                                        'post_url'=>$db->f("post_url"),
                                        'host'=>$db->f("host"),
                                        'username'=>$db->f("username"),
                                        'password'=>$db->f("password")
                                    );
            
            }
            return $details;
        
        }
	
	    /*function getSmsList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
        
			$query = "SELECT DISTINCT ";
			
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) ";
			}
			
			$query .= " FROM ". TABLE_SMS_HISTORY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				echo $query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
					}
				}
				
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}*/
        
        function viewMsgDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) ";
			}
			
			$query .= " FROM ". TABLE_SMS_HISTORY_PHONE;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
					}
				}
				
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
	}
?>
