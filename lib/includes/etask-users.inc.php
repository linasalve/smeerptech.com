<?php 

	class EtaskUser {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => EtaskUser::BLOCKED,
							'ACTIVE'  => EtaskUser::ACTIVE,
							'PENDING' => EtaskUser::PENDING,
							'DELETED' => EtaskUser::DELETED
						);
			return ($status);
		}
		
        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_ETASK_USER;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, EtaskUser::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (EtaskUser::getList($db, $user, 'su_id', " WHERE su_id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_ETASK_USER
								." SET status = '$status_new' "
								." WHERE su_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Account was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (EtaskUser::getList($db, $user, 'su_id', " WHERE su_id = '$id'")) > 0 ) 
			{
                $user = $user[0];
                if ( $user['status'] == EtaskUser::BLOCKED || $user['status'] == EtaskUser::DELETED) 
				{
					$query = "DELETE FROM ". TABLE_ETASK_USER
								." WHERE su_id = '". $id ."'";
					if ( !$db->query($query) || $db->affected_rows()<=0 ) 
						$messages->setErrorMessage("The User is not deleted.");
					else
						$messages->setOkMessage("The User has been deleted.");
				}
				else
					$messages->setErrorMessage("Cannot delete User.");
			}
			else
				$messages->setErrorMessage("User not found.");
		}

		function showUsers(&$userlist)
		{
			$db = new db_local;

			$query = "SELECT ".TABLE_USER .".user_id, concat(".TABLE_USER .".f_name,' ',".TABLE_USER .".l_name) as name,email "
					."FROM ". TABLE_USER ." WHERE ".TABLE_USER.".status='1' AND ".TABLE_USER.".user_id NOT IN (select su_id FROM ".TABLE_ETASK_USER.") ORDER BY ".TABLE_USER.".f_name";

			$db->query($query);
			while($db->next_record())
			{
				$userlist[] = $db->Record;	
			}
		}
    }
?>