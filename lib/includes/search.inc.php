<?php


	function array_qsort2 (&$array, $column=0, $order=SORT_ASC, $first=0, $last= -2){ 
	  // $array  - the array to be sorted 
	  // $column - index (column) on which to sort 
	  //          can be a string if using an associative array 
	  // $order  - SORT_ASC (default) for ascending or SORT_DESC for descending 
	  // $first  - start index (row) for partial array sort 
	  // $last  - stop  index (row) for partial array sort 
	
	  if($last == -2) $last = count($array) - 1; 
	  if($last > $first) { 
	   $alpha = $first; 
	   $omega = $last; 
	   $guess = $array[$alpha][$column]; 
	   while($omega >= $alpha) { 
		 if($order == SORT_ASC) { 
		   while($array[$alpha][$column] < $guess) $alpha++; 
		   while($array[$omega][$column] > $guess) $omega--; 
		 } else { 
		   while($array[$alpha][$column] > $guess) $alpha++; 
		   while($array[$omega][$column] < $guess) $omega--; 
		 } 
		 if($alpha > $omega) break; 
		 $temporary = $array[$alpha]; 
		 $array[$alpha++] = $array[$omega]; 
		 $array[$omega--] = $temporary; 
	   } 
	   array_qsort2 ($array, $column, $order, $first, $omega); 
	   array_qsort2 ($array, $column, $order, $alpha, $last); 
	  } 
	} 


?>