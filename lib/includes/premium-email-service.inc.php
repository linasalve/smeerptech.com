<?php
	class PremiumEmailService{
		
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PREMIUM_EMAIL_SERVICE." LEFT JOIN ".TABLE_CLIENTS." 
					ON ".TABLE_PREMIUM_EMAIL_SERVICE.".client = ".TABLE_CLIENTS.".user_id 
					LEFT JOIN ".TABLE_CLIENT_DOMAINS." ON ".TABLE_PREMIUM_EMAIL_SERVICE.".domain_id = ".TABLE_CLIENT_DOMAINS.".id 
				";
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}				
				return ( $total );
			}
			else{
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            } elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            } else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PREMIUM_EMAIL_SERVICE;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                return ( $total );
            } else {
                return false;
            }   
        }
    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;

            $query = "DELETE FROM ". TABLE_PREMIUM_EMAIL_SERVICE
                        ." WHERE id = '$id'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Record has been deleted.");                      
            }
            else {
                $messages->setErrorMessage("The Record was not deleted.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    
		function validateEnterpriseService(&$data, $extra=''){
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}       
			
            if( !isset($data['form_type']) || empty($data['form_type']) ) {
                $messages->setErrorMessage("Select option Change Password/Create Email/Allot Email Quota.");
            }
			if($data['form_type']=='change_pwd'){
				if(empty($data['user_email'])){
				   $messages->setErrorMessage("Choose email id whose password tobe changed.");
				}			
			}
			if($data['form_type']=='create_email'){
				if(empty($data['user_email2'])){
				   $messages->setErrorMessage("Enter valid email id tobe created.");
				}
				/* if($data['email_service']=='ex'){ //Exchange mail service
					if( ($data['exchangeMaxNumMailboxes'] >0 ) && ( $data['exchangeUsedStorage']==$data['exchangeMaxNumMailboxes'] ) ){ 
						$messages->setErrorMessage('You can not create email as all Enterprise Mailboxes utilised.'); 
					} 
				}
				if($data['email_service']=='rs'){ //Premium mail service
					if( ($data['rsMaxNumMailboxes'] >0 ) && ( $data['rsUsedStorage']==$data['rsMaxNumMailboxes'] ) ){ 
						$messages->setErrorMessage('You can not create email as all Premium Mailboxes utilised.'); 
					} 
				}
				if(empty($data['email_size'])){ 
					//Default is base mailbox size
					$messages->setErrorMessage('Choose size of mailbox.'); 
				}    */
			}
			if($data['form_type']=='delete_email'){
				if(empty($data['user_email'])){
				   $messages->setErrorMessage("Choose email id tobe deleted.");
				}			
			}
			if($data['form_type']=='allot_no_mailbox'){
				if(empty($data['no_of_mailboxes'])){  
				   $messages->setErrorMessage("Enter valid numbers of Mailboxes.");
				}else{
					if(!is_numeric($data['no_of_mailboxes'])){
						$messages->setErrorMessage("Enter valid number of Mailboxes");
					}
				}
				/* 
				if($data['email_service']=='ex'){ //Exchange mail service
					if( ($data['exchangeMaxNumMailboxes'] >0 ) && ( $data['exchangeUsedStorage']==$data['exchangeMaxNumMailboxes'] ) ){ 
						$messages->setErrorMessage('You can not create email as all Enterprise Mailboxes utilised.'); 
					} 
				}
				//$data['no_of_mailboxes_check']
				if($data['email_service']=='rs'){ //Premium mail service
					if( $data['domain_details']['rsEmailUsedStorage']   )   { 
						$messages->setErrorMessage('You can not create email as all Premium Mailboxes utilised.'); 
					} 
				} */
			}
			
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		
		
		}
		
		function validatePremiumService(&$data, $extra=''){
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}       
			
            if( !isset($data['form_type']) || empty($data['form_type']) ) {
                $messages->setErrorMessage("Select option Change Password/Create Email/Allot Email Quota.");
            }
			if($data['form_type']=='change_pwd'){
				if(empty($data['user_email'])){
				   $messages->setErrorMessage("Choose email id whose password tobe changed.");
				}			
			}
			if($data['form_type']=='create_email'){
				if(empty($data['user_email2'])){
				   $messages->setErrorMessage("Enter valid email id tobe created.");
				}
				if($data['email_service']=='ex'){ //Exchange mail service
					if( ($data['exchangeMaxNumMailboxes'] >0 ) && ( $data['exchangeUsedStorage']==$data['exchangeMaxNumMailboxes'] ) ){ 
						$messages->setErrorMessage('You can not create email as all Enterprise Mailboxes utilised.'); 
					} 
				}
				if($data['email_service']=='rs'){ //Premium mail service
					if( ($data['rsMaxNumMailboxes'] >0 ) && ( $data['rsUsedStorage']==$data['rsMaxNumMailboxes'] ) ){ 
						$messages->setErrorMessage('You can not create email as all Premium Mailboxes utilised.'); 
					} 
				}
				if(empty($data['email_size'])){ 
					//Default is base mailbox size
					$messages->setErrorMessage('Choose size of mailbox.'); 
				}   
			}
			if($data['form_type']=='delete_email'){
				if(empty($data['user_email'])){
				   $messages->setErrorMessage("Choose email id tobe deleted.");
				}			
			}
			if($data['form_type']=='allot_no_mailbox'){
				if(empty($data['no_of_mailboxes'])){  
				   $messages->setErrorMessage("Enter valid numbers of Mailboxes.");
				}else{
					if(!is_numeric($data['no_of_mailboxes'])){
						$messages->setErrorMessage("Enter valid number of Mailboxes");
					}
				}
				/* if($data['email_service']=='ex'){ //Exchange mail service
					if( ($data['exchangeMaxNumMailboxes'] >0 ) && ( $data['exchangeUsedStorage']==$data['exchangeMaxNumMailboxes'] ) ){ 
						$messages->setErrorMessage('You can not create email as all Enterprise Mailboxes utilised.'); 
					} 
				}
				//$data['no_of_mailboxes_check']
				if($data['email_service']=='rs'){ //Premium mail service
					if( $data['domain_details']['rsEmailUsedStorage']   )   { 
						$messages->setErrorMessage('You can not create email as all Premium Mailboxes utilised.'); 
					} 
				} */
			}
			
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}       
            if( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select client.");
            }	
            if ( !isset($data['domain_id']) || empty($data['domain_id']) ) {
                $messages->setErrorMessage("Select Domain.");
            }
			
			if(!empty($data['client']) && !empty($data['domain_id'])){
				$query = "SELECT id, client FROM ".TABLE_PREMIUM_EMAIL_SERVICE." WHERE client = '".trim($data['client'])."' 
				AND domain_id = '".trim($data['domain_id'])."' AND service_id = '".trim($data['service_id'])."' LIMIT 0,1"; 
				if ( $db->query($query) ){
					if ( $db->nf() > 0 ){
						$db->next_record();
                        $record = processSqlData($db->result());
						$messages->setErrorMessage('Domain '.$data['domain'].' is already exist with client
						'.$record['client_details']." with same service.");
					}
				}  
			}
			if ( !isset($data['fr_dt']) || empty($data['fr_dt']) ) {
                $messages->setErrorMessage("Select From date.");
            }
			
			if ( !isset($data['period_in_months']) || empty($data['period_in_months']) ) {
                $messages->setErrorMessage("Select Period in months.");
            }
			if ( !isset($data['price_per_month']) || empty($data['price_per_month']) ) {
                $messages->setErrorMessage("Enter Price per month.");
            }else{
				if(!is_numeric($data['price_per_month'])){
					$messages->setErrorMessage("Enter valid Price per month");
				}
			}
			if ( !isset($data['discount'])   ) {
                $messages->setErrorMessage("Enter discount.");
            }else{
				if(!is_numeric($data['discount'])){
					$messages->setErrorMessage("Enter valid discount");
				}
			}
 			
			// total_allotment
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
            
			
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
               	
            $query = " UPDATE ". TABLE_PREMIUM_EMAIL_SERVICE
                    ." SET ". TABLE_PREMIUM_EMAIL_SERVICE .".status = '1'" 
                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Company has been activated.");                      
            }
            else {
                $messages->setErrorMessage("The Company has not been activated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            $query = " UPDATE ". TABLE_PREMIUM_EMAIL_SERVICE
                    ." SET ". TABLE_PREMIUM_EMAIL_SERVICE .".status = '0'"
                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Company has been deactivated.");                      
            }
            else {
                $messages->setErrorMessage("The Company has not been deactivated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        } 
		
    }
?>
