<?php
	class UserRights{
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' =>UserRights::BLOCKED,
							'ACTIVE'  =>UserRights::ACTIVE,
							'PENDING' =>UserRights::PENDING,
							'DELETED' =>UserRights::DELETED
						);
			return ($status);
		}
		
		/**
		*	Function to get all User roles
		*
		*	@param Object of database
		*	@param Array of User role
		* 	@param required fields
		* 	@param condition
		*	return array of User roles
		*	otherwise return NULL
		*/		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_USER_RIGHTS;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->Record);
						if($count==1){
							$total = $db->f("count") ;							
						}else{
							$total = $db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
		
		/**
		 *  Function to retrieve the list of Languages customised to display the list.
		 *
		 */
/*
		 function getDisplayList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			if ( $required_fields == '' ) {
					$required_fields = TABLE_USER_RIGHTS .".id "
										.",". TABLE_USER_RIGHTS .".title "
										.",". TABLE_USER_RIGHTS .".value"
										.",". TABLE_USER_RIGHTS .".description "
										.",". TABLE_USER_RIGHTS .".status "
										;
				}
											
			if( $condition == "" ) {	
				$condition	.= " ORDER BY ". TABLE_USER_RIGHTS .".title ASC ";	
			}	
		
			$list	= NULL;
			UserRights::getList( $db,$list, $required_fields, $condition,$from,$rpp);
			return ( count($list) );
	   }
*/
       
	   //This function is used to perform action on status.		
		function updateStatus($id, $status_new, &$messages,$extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$db = new db_local;
			// Check if status is valid or not.
			if ( !in_array($status_new, UserRights::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
				 $query = "UPDATE ". TABLE_USER_RIGHTS
								." SET status = '$status_new' "
							." WHERE id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					
					$messages->setOkMessage("The Status has been changed.");
				}
				else {
					$messages->setErrorMessage("The Status of the Right was not changed.");
				}
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
      }
	
	//used to delete records.
	function delete($id,&$messages, $extra) {
		foreach ($extra as $key=>$value) {
			$$key = $value;
		}
		
		$query = "DELETE FROM ". TABLE_USER_RIGHTS
					." WHERE id = '$id'"
					." AND (status = '". UserRights::BLOCKED ."' OR status = '". UserRights::DELETED ."')";
		if ( $db->query($query) && $db->affected_rows()>0 ) {
			$messages->setOkMessage("The User Right has been deleted.");
		}
		else {
				$messages->setErrorMessage("The selected User Right was not found.");
		}
		
		if ( $messages->getErrorMessageCount() <= 0 ) {
			return (true);
		}
		else {
			return (false);
		}
		
	}
		 /**
		 * Function to validate the input from the User while Adding a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd($data, &$messages, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            if ( !isset($data['title']) || empty($data['title']) 
                 || (strlen($data['title'])<3) || (strlen($data['title'])>100)
               ) {
                $messages->setErrorMessage("The length of the Title should be between 3 to 100 characters.");
			}
            
            if ( !isset($data['value']) || empty($data['value'])
                 || (strlen($data['value'])<1) || (strlen($data['value'])>100)) {
                $messages->setErrorMessage("The length of the Value should be between 1 to 40 characters.");
			}
            else {
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_]{1,40}$/', $data['value']) ) {
                    $messages->setErrorMessage("The Value is not valid.");
                }
                else {
                    $condition = " WHERE value = '". $data['value'] ."'";
                    if ( UserRights::getList( $db, $list, 'title, value', $condition) > 0 ) {
                        $messages->setErrorMessage("A Right with the same Value already exists.");
                    }
                }
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status of the User Right is not valid.");
			}

			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate($data, &$messages, $extra='',$urights_id='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( !isset($data['title']) || empty($data['title']) 
                    || (strlen($data['title'])<3) || (strlen($data['title'])>100)
                ) {
                $messages->setErrorMessage("The length of the Title should be between 3 to 100 characters.");
            }
            
            if ( !isset($data['value']) || empty($data['value'])
                    || (strlen($data['value'])<1) || (strlen($data['value'])>100)) {
                $messages->setErrorMessage("The length of the Value should be between 1 to 40 characters.");
            }
            else {
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_]{1,40}$/', $data['value']) ) {
                    $messages->setErrorMessage("The Value is not valid.");
                }
                else {
                    $condition = " WHERE value = '". $data['value'] ."'"
                                    ." AND id != '". $data['right_id'] ."'";
                    if ( UserRights::getList( $db, $list, 'title, value', $condition) > 0 ) {
                        $messages->setErrorMessage("A Right with the same Value already exists.");
                    }
                }
            }


            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}

		function getParent(&$db,&$rights,$id='0')
		{
			$sql = "SELECT * FROM ".TABLE_USER_RIGHTS." WHERE parent_id='".$id."'";
			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
					$rights[] = processSqlData($db->Record);

				return ( $db->nf() );
			}
			else 
				return false;			
		}
}
?>
