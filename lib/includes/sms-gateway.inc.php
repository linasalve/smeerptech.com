<?php
	class SmsGateway {
		
        const ACTIVE = 1;
		const DEACTIVE = 0;       
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_GATEWAY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SMS_GATEWAY;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
            $query = "SELECT * FROM ". TABLE_SMS_API
                        ." WHERE gateway_id = '$id'";
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                        $messages->setErrorMessage("This Gateway is used in api.First delete the api that is using this.");
                    }
                else{            
                    $query = "DELETE FROM ". TABLE_SMS_GATEWAY
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                }
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
           
            if ( !isset($data['company']) || empty($data['company']) ) {
                $messages->setErrorMessage("Company can not be empty.");
            }       
            if ( !isset($data['contact_name1']) || empty($data['contact_name1']) ) {
                $messages->setErrorMessage("Contact name can not be empty.");
            }       
			if ( !isset($data['contact_dept1']) || empty($data['contact_dept1']) ) {
                $messages->setErrorMessage("Contact department can not be empty.");
            }       
			
            if ( !isset($data['contact_email1']) || empty($data['contact_email1']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }
            elseif ( !isValidEmail($data['contact_email1']) ) {
                $messages->setErrorMessage('The Email Address is not valid.');
            }
            
            if ( isset($data['contact_email2']) && !empty($data['contact_email2']) && !isValidEmail($data['contact_email2']) ) {
                $messages->setErrorMessage('The 2nd Email Address is not valid.');
            }
            
            if ( !isset($data['contact_phone1']) || empty($data['contact_phone1']) ) {
                $messages->setErrorMessage("Contact phone can not be empty.");
            }/*elseif ( !isnumeric($data['contact_phone1']) ) {
                $messages->setErrorMessage('Contact phone is not valid.');
            }  */         

			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            
            if ( !isset($data['company']) || empty($data['company']) ) {
                $messages->setErrorMessage("Company can not be empty.");
            }       
            if ( !isset($data['contact_name1']) || empty($data['contact_name1']) ) {
                $messages->setErrorMessage("Contact name can not be empty.");
            }       
			if ( !isset($data['contact_dept1']) || empty($data['contact_dept1']) ) {
                $messages->setErrorMessage("Contact department can not be empty.");
            }       
			
            if ( !isset($data['contact_email1']) || empty($data['contact_email1']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }
            elseif ( !isValidEmail($data['contact_email1']) ) {
                $messages->setErrorMessage('The Email Address is not valid.');
            }
            
            if ( isset($data['contact_email2']) && !empty($data['contact_email2']) && !isValidEmail($data['contact_email2']) ) {
                $messages->setErrorMessage('The 2nd Email Address is not valid.');
            }
            
            if ( !isset($data['contact_phone1']) || empty($data['contact_phone1']) ) {
                $messages->setErrorMessage("Contact phone can not be empty.");
            }/*elseif ( !isnumeric($data['contact_phone1']) ) {
                $messages->setErrorMessage('Contact phone is not valid.');
            }  */   
                   
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_SMS_GATEWAY
                            ." SET ". TABLE_SMS_GATEWAY .".status = '".SmsGateway::ACTIVE."'"
                            	 

                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The sms gateway has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The sms gateway has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_SMS_GATEWAY
                            ." SET ". TABLE_SMS_GATEWAY .".status = '".SmsGateway::DEACTIVE."'"
                            	 

                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The sms gateway has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The sms gateway has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
    }
?>
