<?php
	class User {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;
		
		//Current staff
		const CSTAFF_YES = 1;
		const CSTAFF_NO = 0;
		
		//Random selection
		const RSELECT_YES = 1;
		const RSELECT_NO = 0;
		
		//Send Mail
		const SENDM_YES = 1;
		const SENDM_NO = 0;
		
		//GROUPS BOF
		const MKT_MNGR = 1;
		const MKT_EXEC = 2;
		const MKT_CORE = 3;
		const MKT_ALL = 4;
		
		const PGM_MNGR = 5;
		const PGM_EXEC = 6;
		const PGM_CORE = 7;
		const PGM_ALL = 8;
		
		const DSG_MNGR = 9;
		const DSG_EXEC = 10;
		const DSG_CORE = 11;
		const DSG_ALL = 12;
		
		const ADM_MNGR = 13;
		const ADM_EXEC = 14;
		const ADM_CORE = 15;
		const ADM_ALL = 16;
		
		const ACC_MNGR = 17;
		const ACC_EXEC = 18;
		const ACC_CORE = 19;
		const ACC_ALL = 20;
		
		const BPO_MNGR = 21;
		const BPO_EXEC = 22;
		const BPO_CORE = 23;
		const BPO_ALL = 24;
		
		const CRM_MNGR = 25;
		const CRM_EXEC = 26;
		const CRM_CORE = 27;
		const CRM_ALL = 28;
		
		const OASST_MNGR = 29;
		
		
		const TP_ONSITE = 30;
		const TP_WH = 31;
		const TP_EXT = 32;
		const TP_PART = 33;
		const TP_OSTP = 34;
		const TP_ALL = 35;
		
		const TP_CM = 36; //TEAM CHARUDATTA MARATHE
		const TP_DRN = 37; // TEAM DRN
		const TP_SS = 38; //TEAM SAMEER SAOJI
		const TP_AR = 39; // TEAM AMIT RAJKOTIYA
		
		//GROUPS EOF
		
		function getGroups(){
			$groups = array(
				User::MKT_MNGR => 'Marketing & Sales - Managers',
				User::MKT_EXEC => 'Marketing & Sales - Executives',
				User::MKT_CORE => 'Marketing & Sales - Core',
				User::MKT_ALL =>  'Marketing & Sales - All',
				User::PGM_MNGR => 'Programmers - Managers',
				User::PGM_EXEC => 'Programmers - Executives',
				User::PGM_CORE => 'Programming - Core',
				User::PGM_ALL =>  'Programmers - All',
				User::DSG_MNGR => 'Designers - Managers',
				User::DSG_EXEC => 'Designers - Executive',
				User::DSG_CORE => 'Designers - Core',
				User::DSG_ALL =>  'Designers - All',
				User::ADM_MNGR => 'Admin - Managers',
				User::ADM_EXEC => 'Admin - Executives',
				User::ADM_CORE => 'Admin - Core',
				User::ADM_ALL =>  'Admin - All',
				User::ACC_MNGR => 'Accounts - Managers',
				User::ACC_EXEC => 'Accounts - Executives',
				User::ACC_CORE => 'Accounts - Core',
				User::ACC_ALL =>  'Accounts - All',
				User::BPO_MNGR => 'BPO - Managers',
				User::BPO_EXEC => 'BPO - Executives',
				User::BPO_CORE => 'BPO - Core',
				User::BPO_ALL =>  'BPO - All',
				User::CRM_MNGR => 'Relationships - Managers',
				User::CRM_EXEC => 'Relationships - Executives',
				User::CRM_CORE => 'Relationships - CORE',
				User::CRM_ALL =>  'Relationships - All',
				User::OASST_MNGR => 'Office Assistants',
				User::TP_ONSITE => 'Team Onsite',
				User::TP_WH =>    'Team Work from Home',
				User::TP_EXT =>   'Team External',
				User::TP_PART =>  'Team Part Time',
				User::TP_OSTP =>  'Team Online Support Pune',
				User::TP_ALL =>   'Team SMEERP',		
				User::TP_CM =>    'Team Charudatta Marathe',
				User::TP_DRN =>   'Team DRN',
				User::TP_SS =>    'Team Sameer Saoji',
				User::TP_AR =>    'Team Amit Rajkotiya'
			);
			
			return ($groups);
			
		}
		
		function getStatus() {
			$status = array('BLOCKED' => User::BLOCKED,
							'ACTIVE'  => User::ACTIVE,
							'PENDING' => User::PENDING,
							'DELETED' => User::DELETED
						);
			return ($status);
		}
		
        function getTitleType() {
			$titletype = array('Mr.' => 'Mr.',
                             'Ms.'  => 'Ms.',
							'Mrs.'  => 'Mrs.',                           
							'Prof.'  => 'Prof.',
							'Dr.'  =>'Dr.'
						);
			return ($titletype);
		}
        
        function getCountryCode( &$db, &$country_code){			
            
            $query= "SELECT * FROM ". TABLE_COUNTRIES 
					." WHERE ". TABLE_COUNTRIES .".status='1' ";
            
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$country_code[]= processSqlData($db->result());
					}
				}	                
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}

		/**
		 *	Function to get all the Executives.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_USER;
			
            $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}

	   
	   /**
	    * This function is used to retrieve the details of the User.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the details will be stored.
		 * @param mixed   array/string containing the fields that is to be retrieved.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                 boolean false, if the record is found.
         *
		 */
		function getUserDetails( &$db, &$list, $id, $required_fields='') {
			$condition	= " WHERE user_id = '$id' ";

			if ( User::getList( $db, $list, $required_fields, $condition) ) {
				$list = $list[0];
				return ( true );
			}
			else {
				return ( false );
			}
		}
	   
        
        /**
         * This function is used to update the status of the Executive.
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, User::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (User::getList($db, $user, 'user_id, access_level', " WHERE user_id = '$id'")) > 0 ) {
                    $user = $user[0];
                    if ( $user['access_level'] < $access_level ) {
                        $query = "UPDATE ". TABLE_USER
                                    ." SET status = '$status_new' "
                                    ." WHERE user_id = '$id'";
                        if ( $db->query($query) && $db->affected_rows()>0 ) {
                            $messages->setOkMessage("The Status has been updated.");
                        }
                        else {
                            $messages->setErrorMessage("The Status was not updated.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("You do not have the Right to change Status of Executives with the current Access Level.");
                    }
                }
                else {
                    $messages->setErrorMessage("The Executive was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Executive information from the database.
        *
        * @param   string      unique ID of the Executive whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (User::getList($db, $user, 'user_id, status, access_level', " WHERE user_id = '$id'")) > 0 ) {
                $user = $user[0];
                if ( $user['status'] == User::BLOCKED || $user['status'] == User::DELETED) {
                    if ( $user['access_level'] < $access_level ) {
                        // Delete the Reminders set for the Executive.
                        $query = "SELECT id FROM ". TABLE_USER_REMINDERS
                                    ." WHERE table_name = '". TABLE_USER ."'"
                                    ." AND reminder_for = '". $id ."'";
                        if ( $db->query($query) ) {
                            if ( $db->nf()>0 ) {
                                $query = "DELETE FROM ". TABLE_USER_REMINDERS
                                            ." WHERE table_name = '". TABLE_USER ."'"
                                            ." AND reminder_for = '". $id ."'";
                                if ( !$db->query($query) || $db->affected_rows()<=0 ) {
                                    $messages->setErrorMessage("The Reminders for the Executive were not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("The Reminders for the Executive were not deleted.");
                        }
                        
                        // Delete the Contact Numbers set for the Executive.
                        $query = "SELECT id FROM ". TABLE_PHONE
                                    ." WHERE table_name = '". TABLE_USER ."'"
                                    ." AND phone_of = '". $id ."'";
                        if ( $db->query($query) ) {
                            if ( $db->nf()>0 ) {
                                $query = "DELETE FROM ". TABLE_PHONE
                                            ." WHERE table_name = '". TABLE_USER ."'"
                                            ." AND phone_of = '". $id ."'";
                                if ( !$db->query($query) || $db->affected_rows()<=0 ) {
                                    $messages->setErrorMessage("The Contact Numbers for the Executive were not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("The Contact Numbers for the Executive were not deleted.");
                        }

                        // Delete the Address's of the Executive.
                        $query = "SELECT id FROM ". TABLE_ADDRESS
                                    ." WHERE table_name = '". TABLE_USER ."'"
                                    ." AND address_of = '". $id ."'";
                        if ( $db->query($query) ) {
                            if ( $db->nf()>0 ) {
                                $query = "DELETE FROM ". TABLE_ADDRESS
                                            ." WHERE table_name = '". TABLE_USER ."'"
                                            ." AND address_of = '". $id ."'";
                                if ( !$db->query($query) || $db->affected_rows()<=0 ) {
                                    $messages->setErrorMessage("The Address for the Executive were not deleted.");
                                }
                            }
                        }
                        else {
                            $messages->setErrorMessage("The Address for the Executive were not deleted.");
                        }
                                
                        if ( $messages->getErrorMessageCount() <= 0 ) {
                            // Delete the Executive itself.
                            $query = "DELETE FROM ". TABLE_USER 
                                        ." WHERE user_id = '". $id ."'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Executive has been deleted.");
                            }
                            else {
                                $messages->setErrorMessage("The Executive was not deleted.");
                            }
                        }

                    }
                    else {
                        $messages->setErrorMessage("You do not have the Right to Delete the Executives with the current Access Level.");
                    }
                }
                else {
                    $messages->setErrorMessage("To delete the Executive, he/she has to be Blocked.");
                }
            }
            else {
                $messages->setErrorMessage("The record for the Executive was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
		 * Function to validate the data supplied for adding a new Executive.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
             // Login ID.
            if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage('The Login ID cannot be empty.');
            }
            elseif ( !isset($data['number']) || empty($data['number']) ) {
                $messages->setErrorMessage('The Employee Number cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{1,80}$/', $data['username']) ) {
                    $messages->setErrorMessage('The Login ID is invalid.');
                }
                else {
                    // Check for Dupllicate User ID, Login ID and Account Number.
// The following code has been commented to allow Duplicate Username but with different Employee Number.
// For achieving this, the Unique User ID will be generated using the Username and Employee Number, and 
// while checking for duplicate entry the Unique User ID and the Username-Employee Number pair will be used.
//                    $user_id    = encParam($data['username']);
//                    $user_list  = NULL;
//                    $condition  = " WHERE (user_id = '$user_id') "
//                                    ." OR ( username = '". $data['username'] ."') "
//                                    ." OR ( number = '". $data['number'] ."' AND number IS NOT NULL ) ";
//                    
//                    if ( (User::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
//                        foreach ( $user_list as $user) {
//                            if ( ($user_id == $user['user_id']) || ($data['username'] == $user['username']) ) {
//                                $messages->setErrorMessage('The Login ID is alreay taken. Please enter another.');
//                            }
//                            if ( $data['number'] == $user['number'] ) {
//                                $messages->setErrorMessage('The Account number is alreay taken. Please enter another.');
//                            }
//                        }
//                    }
//                    else {
//                        $data['user_id'] = $user_id;
//                        
//                    }

                    $user_id    = encParam($data['username'].'?'.$data['number']); // We donot allow '?' in username, hence used here.
                    $user_list  = NULL;
                    $condition  = " WHERE (user_id = '$user_id') "
                                    ." OR ( username = '". $data['username'] ."' "
                                    ." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";
                    
                    if ( (User::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
                        foreach ( $user_list as $user) {
                            $messages->setErrorMessage('The Login ID-Employee number pair is alreay taken. Please enter another.');
                        }
                    }
                    else {
                        $data['user_id'] = $user_id;
                    }
                }
            }
            // Password.
            if ( !isset($data['password']) || empty($data['password']) 
                    || !isset($data['re_password']) || empty($data['re_password']) ) {
                $messages->setErrorMessage('The Password cannot be empty.');
            }
            else {
                if ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['password']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                else {
                    if ( $data['password'] != $data['re_password'] ) {
                        $messages->setErrorMessage('The entered Password do not match.');
                    }
                }
            }

            // Access Level.
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage('Select the Access Level.');
            }
            else {
                $acc_index = array();
                array_key_search('access_level', $data['access_level'], $al_list, $acc_index);
                if ( count($acc_index)<=0 ) {
                    $messages->setErrorMessage('Selected Access Level not found.');
                }
            }
            
            // Roles.
            if ( !isset($data['roles']) ) {
                $messages->setErrorMessage('Select atleast one Role for the Executive.');
            }
            else {
                foreach ( $data['roles'] as $role ) {
                    $role_index = array();
                    array_key_search('id', $role, $role_list, $role_index);
                    if ( count($role_index)<=0 ) {
                        $messages->setErrorMessage('The Role with ID '. $role .'is not found.');
                    }
                }
                $data['roles'] = implode(',', $data['roles']);
            }
             //if allow_ip == 2 ie specified then 
            if ( isset($data['allow_ip']) && $data['allow_ip'] == 2 ) {
                
                if(empty($data['valid_ip'])){
                    $messages->setErrorMessage('Please enter valid IP List.');
                }
			}else{
                $data['valid_ip'] ='';
            }
            
            if ( !isset($data['do_join']) || empty($data['do_join']) ) {
                $messages->setErrorMessage('Date of joining should be provided.');
			}
            
            if ( !isset($data['report_to']) || empty($data['report_to']) ) {
                $messages->setErrorMessage('Select the executives to whom it should report.');
			}
            
            if ( !empty($data['do_join']) ) {
                $temp = explode('/', $data['do_join']);
                $data['do_join'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }else{
                $data['do_join']='';
            }
            
            if ( !empty($data['do_resign']) ) {
                $temp = explode('/', $data['do_resign']);
                $data['do_resign'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }else{
                $data['do_resign']='';
            }
            
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
			}
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !isset($data['gender']) || !in_array($data['gender'], array('m','f','o')) ) {
                $messages->setErrorMessage('Gender should be selected.');
            }
            
            // Check the Important Dates.
            if ( !empty($data['do_birth']) ) {
                $temp = explode('/', $data['do_birth']);
                $data['do_birth'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            if ( !empty($data['do_aniv']) ) {
                $temp = explode('/', $data['do_aniv']);
                $data['do_aniv'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            // Validate the E-mail Addresses.
            if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }
            elseif ( !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('The Primary Email Address is not valid.');
            }
            
            if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            // Validate the Addresses.
            $data['address_count'] = count($data['address_type']);
            for ( $i=0; $i<$data['address_count']; $i++ ) {
            $data['company_name'][$i]='';
                if ( !$region->validate($data['address_type'][$i],
                                  $data['company_name'][$i],
                                  $data['address'][$i],
                                  $data['city'][$i],
                                  $data['state'][$i],
                                  $data['country'][$i],
                                  $data['zipcode'][$i]
                                 )
                   ) {
                    $messages->setErrorMessage('There is/are error(s) in the Mailing Address.');
                    foreach ( $region->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
            }
            
            // Validate the Phone.
            $data['phone_count'] = count($data['p_type']);
            for ( $i=0; $i<$data['phone_count']; $i++ ) {
                if ( !empty($data['p_number'][$i]) ) {
                    if ( !$phone->validate($data['p_type'][$i], $data['cc'][$i], $data['ac'][$i], $data['p_number'][$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in the <b>'. $phone->getTypeTitle($data['p_type'][$i]) .' Contact Number</b>.');
                        foreach ( $phone->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }
            
            // Check the Reminders.
            for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                if ( !empty($data['remind_date_'.$i]) ) {
                    $temp = explode('/', $data['remind_date_'.$i]);
                    $data['remind_date_'.$i] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                    
                    if ( !$reminder->validate($data['remind_date_'.$i], $data['remind_text_'.$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                        foreach ( $reminder->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }
            if((!empty($files['resume_attachment']) && (!empty($files['resume_attachment']['name'])))){
                $filename = $files['resume_attachment']['name'];
                // $data['resume_attachment'] = $filename;
                $type = $files['resume_attachment']['type'];
                $size = $files['resume_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 1Mb');
                }
            }
			if((!empty($files['team_photo']) && (!empty($files['team_photo']['name'])))){
                $filename = $files['team_photo']['name'];
                //$data['team_photo'] = $filename;
                $type = $files['team_photo']['type'];
                 $size = $files['team_photo']['size'];
                $max_size = ($data['max_photo_size'] * 1024);
                if ( !in_array($type, $data["allowed_photo_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Photo is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 50Kb');
                }
            }
            if ( $data['status'] != User::PENDING && $data['status'] != User::ACTIVE 
                    && $data['status'] != User::BLOCKED && $data['status'] != User::DELETED ) {
				$messages->setErrorMessage('The Status is not valid.');
			}
            
            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='',$perform='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
 
            // Password.
            if ( isset($data['password']) && !empty($data['password']) ) {
                if ( !isset($data['re_password']) || empty($data['re_password']) ) {
                    $messages->setErrorMessage('The Repeat Password cannot be empty.');
                }
                elseif ( !preg_match('/^[a-zA-Z0-9_]{1,30}$/', $data['password']) ) {
                    $messages->setErrorMessage('The Password is invalid.');
                }
                elseif ( $data['password'] != $data['re_password'] ) {
                    $messages->setErrorMessage('The entered Password do not match.');
                }
                else {
                    $data['password'] = ",". TABLE_USER .".password = '". encParam($data['password']) ."'";
                }
            }
            else {
                $data['password'] = '';
            }

            // Access Level.
			if( $perform == "")
			{
				if ( !isset($data['access_level']) ) {
					$messages->setErrorMessage('Select the Access Level.');
				}
				else {
					$acc_index = array();
					array_key_search('access_level', $data['access_level'], $al_list, $acc_index);
					if ( count($acc_index)<=0 ) {
						$messages->setErrorMessage('Selected Access Level not found.');
					}
				}
			}

            // Roles.
			if( $perform == "")
			{
				if ( !isset($data['roles']) ) {
					$messages->setErrorMessage('Select atleast one Role for the Executive.');
				}
				else {
					foreach ( $data['roles'] as $role ) {
						$role_index = array();
						array_key_search('id', $role, $role_list, $role_index);
						if ( count($role_index)<=0 ) {
							$messages->setErrorMessage('The Role with ID '. $role .'is not found.');
						}
					}
					$data['roles'] = implode(',', $data['roles']);
				}
			}
            //if allow_ip == 2 ie specified then 
            if ( isset($data['allow_ip']) && $data['allow_ip'] == 2 ) {
                
                if(empty($data['valid_ip'])){
                    $messages->setErrorMessage('Please enter valid IP List.');
                }
			}else{
                $data['valid_ip'] ='';
            }
            if( $perform == "")
			{
                if ( !isset($data['report_to']) || empty($data['report_to']) ) {
                    $messages->setErrorMessage('Select the executives to whom it should report.');
			    }
            }
            
            if( $perform == "")
			{
                if ( !isset($data['do_join']) || empty($data['do_join']) ) {
                    $messages->setErrorMessage('Date of joining should be provided.');
			    }
            }
            
            if ( !empty($data['do_join']) ) {
                $temp = explode('/', $data['do_join']);
                $data['do_join'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }else{
                $data['do_join'] ='';
            }
            
            if ( !empty($data['do_resign']) ) {
                $temp = explode('/', $data['do_resign']);
                $data['do_resign'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }else{
                $data['do_resign'] ='';
            }
            
            if ( !isset($data['f_name']) || empty($data['f_name']) ) {
                $messages->setErrorMessage('First Name should be provided.');
            }
            
            if ( !isset($data['l_name']) || empty($data['l_name']) ) {
                $messages->setErrorMessage('Last Name should be provided.');
            }
            
            if ( !isset($data['gender']) || !in_array($data['gender'], array('m','f','o')) ) {
                $messages->setErrorMessage('Gender should be selected.');
            }
            
            // Check the Important Dates.
            if ( !empty($data['do_birth']) ) {
                $temp = explode('/', $data['do_birth']);
                $data['do_birth'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }else{
             $data['do_birth'] ='';
            }
            if ( !empty($data['do_aniv']) ) {
                $temp = explode('/', $data['do_aniv']);
                $data['do_aniv'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }else{
            
            $data['do_aniv'] ='';
            }
            // Validate the E-mail Addresses.
            if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }
            elseif ( !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('The Primary Email Address is not valid.');
            }
            
            if ( isset($data['email_1']) && !empty($data['email_1']) && !isValidEmail($data['email_1']) ) {
                $messages->setErrorMessage('The first Other Email Address is not valid.');
            }
            if ( isset($data['email_2']) && !empty($data['email_2']) && !isValidEmail($data['email_2']) ) {
                $messages->setErrorMessage('The second Other Email Address is not valid.');
            }
            
            // Validate the Addresses.
            $data['address_count']  = count($data['address_type']);
            $preferred              = 0;
            for ( $i=0; $i<$data['address_count']; $i++ ) {
            $data['company_name'][$i]='';
                if ( !$region->validate($data['address_type'][$i],
                                  $data['company_name'][$i],
                                  $data['address'][$i],
                                  $data['city'][$i],
                                  $data['state'][$i],
                                  $data['country'][$i],
                                  $data['zipcode'][$i]
                                 )
                   ) {
                    $messages->setErrorMessage('There is/are error(s) in the '. $region->getTypeTitle($data['address_type'][$i]) .' Address.');
                    foreach ( $region->getError() as $errors) {
                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                    }
                }
                if ( isset($data['is_preferred'][$i]) && $data['is_preferred'][$i] == '1' ) {
                    $preferred += 1 ;
                }
            }
            // The elseif part is added for additional validation and security.
            // If speed is intended the elseif part can be removed and the
            // $preferred is to be converted to a BOOLEAN.
            // ($preferred = 0) to ($preferred = false), ($preferred += 1) to ($preferred = true)
            if ( $preferred == 0 ) {
                $messages->setErrorMessage('Atleast one Address is to be selected as preferred.');
            }
            elseif ( $preferred > 1 ) {
                $messages->setErrorMessage('Only one Address is to be selected as preferred.');
            }
            
            // Validate the Phone.
            $data['phone_count']= count($data['p_type']);
            $preferred          = 0;
            for ( $i=0; $i<$data['phone_count']; $i++ ) {
                if ( !empty($data['p_number'][$i]) ) {
                    if ( !$phone->validate($data['p_type'][$i], $data['cc'][$i], $data['ac'][$i], $data['p_number'][$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in the <b>'. $phone->getTypeTitle($data['p_type'][$i]) .' Contact Number</b>.');
                        foreach ( $phone->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
                if ( isset($data['p_is_preferred'][$i]) && $data['p_is_preferred'][$i] == '1' ) {
                    $preferred += 1 ;
                }
            }
            
            // The elseif part is added for additional validation and security.
            // If speed is intended the elseif part can be removed and the
            // $preferred is to be converted to a BOOLEAN.
            // ($preferred = 0) to ($preferred = false), ($preferred += 1) to ($preferred = true)
            if ( !$preferred ) {
                $messages->setErrorMessage('Atleast one Contact Number is to be selected as preferred.');
            }
            elseif ( $preferred > 1 ) {
                $messages->setErrorMessage('Only one Contact Number is to be selected as preferred.');
            }
            
            // Check the Reminders.
            for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                if ( !empty($data['remind_date_'.$i]) ) {
                    $temp = explode('/', $data['remind_date_'.$i]);
                    $data['remind_date_'.$i] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                    
                    if ( !$reminder->validate($data['remind_date_'.$i], $data['remind_text_'.$i]) ) {
                        $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                        foreach ( $reminder->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
            }

            if( $perform == "")
			{
				if ( $data['status'] != User::PENDING && $data['status'] != User::ACTIVE 
						&& $data['status'] != User::BLOCKED && $data['status'] != User::DELETED ) {
					$messages->setErrorMessage('The Status is not valid.');
				}
			}

            // The Login Id is checked at the last so that there is minimal time lapse in between 
            // validating the data and storing it in the database.
             // Login ID.
			if( $perform == "")
			{
				if ( !isset($data['username']) || empty($data['username']) ) {
					$messages->setErrorMessage('The Login ID cannot be empty.');
				}
				else {
					if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{1,80}$/', $data['username']) ) {
						$messages->setErrorMessage('The Login ID is invalid.');
					}
					else {
						// Check for Dupllicate User ID, Login ID and Account Number.
// The following code has been commented to allow Duplicate Username but with different Account Number.
// Here the Login ID can be changed, however the Employee Number and Unique User ID cannot be changed.
//						$user_id    = encParam($data['username']);
//						$user_list  = NULL;
//						$condition  = " WHERE ( (user_id = '$user_id') "
//											." OR ( username = '". $data['username'] ."') "
//											." OR ( number = '". $data['number'] ."' AND number IS NOT NULL ) "
//										." ) AND user_id != '". $data['user_id'] ."'";
//						
//						if ( (User::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
//							foreach ( $user_list as $user) {
//								if ( ($user_id == $user['user_id']) || ($data['username'] == $user['username']) ) {
//									$messages->setErrorMessage('The Login ID is alreay taken. Please enter another.');
//								}
//								if ( $data['number'] == $user['number'] ) {
//									$messages->setErrorMessage('The Account number is alreay taken. Please enter another.');
//								}
//							}
//						}
//						else {
//							$data['user_id'] = $user_id;
//							
//						}

						$user_list  = NULL;
						$condition  = " WHERE (user_id != '". $data['user_id'] ."') "
										." AND ( username = '". $data['username'] ."' "
										." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";
						
						if ( (User::getList($db, $user_list, 'user_id, username, number', $condition)) > 0 ) {
							foreach ( $user_list as $user) {
								$messages->setErrorMessage('The Login ID-Employee number pair is alreay taken. Please enter another.');
							}
						}
					}
				}
			}
			if((!empty($files['resume_attachment']) && (!empty($files['resume_attachment']['name'])))){
                $filename = $files['resume_attachment']['name'];
               // $data['resume_attachment'] = $filename;
                $type = $files['resume_attachment']['type'];
                 $size = $files['resume_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 1Mb');
                }
            }
			if((!empty($files['team_photo']) && (!empty($files['team_photo']['name'])))){
                $filename = $files['team_photo']['name'];
                //$data['team_photo'] = $filename;
                $type = $files['team_photo']['type'];
                $size = $files['team_photo']['size'];
                $max_size = ($data['max_photo_size'] * 1024);
                if ( !in_array($type, $data["allowed_photo_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Photo is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 5Kb');
                }
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
		}
        
		
		
        
        /**
         * This function is used to retrieve the next account number in series.
         *
         * @param   object  database class object
         *
         * @return  int     the next number in series
         *
         */
        function getNewAccNumber(&$db){
            $user_number= 5000;
            $query      = "SELECT MAX(number) AS number FROM ". TABLE_USER;

            if ( $db->query($query) && $db->nf()>0 && $db->next_record()){
                $user_number = $db->f("number");
                $user_number=$user_number +9;
            }
            return $user_number;
        }

        
        function getRoles(&$db, $level, $role_ids='') {
            include_once ( DIR_FS_INCLUDES .'/user-roles.inc.php');
            
            $role_list = NULL;
            if ( !empty($role_ids) ) {
                if ( is_array($role_ids) ) {
                    $role_ids = ' AND id IN ('. ('\''. implode('\',', $role_ids) .'\'') .')';
                }
                else {
                    $role_ids = ' AND id IN ('. ('\''. str_replace(',', '\',\'', $role_ids) .'\'') .')';
                }
            }
            else {
                $role_ids = '';
            }
            
            $condition  = " WHERE access_level < '". $level ."'"
                            . $role_ids
                            ." AND status = '". UserRoles::ACTIVE ."'"
                            ." ORDER BY access_level DESC";

            if ( (UserRoles::getList($db, $role_list, 'id, title', $condition)) <= 0 ) {
                $role_list[] = array('id' => '', 'title' => 'No Role Available' );
            }
        
            return ($role_list);
        }
        
        
        function getAccessLevel(&$db, $level=0, $al_list='') {
            include_once ( DIR_FS_INCLUDES .'/settings-access-level.inc.php');

            $role_list = NULL;
            if ( !empty($al_list) ) {
                if ( is_array($al_list) ) {
                    $al_list = ' AND access_level IN ('. ('\''. implode('\',', $al_list) .'\'') .')';
                }
                else {
                    $al_list = ' AND access_level IN ('. ('\''. str_replace(',', '\',\'', $al_list) .'\'') .')';
                }
            }
            else {
                $al_list = '';
            }
            
            $access     = NULL;
            $condition  = " WHERE access_level < '". $level ."'"
                            . $al_list
                            ." AND status = '". AccessLevel::ACTIVE ."'"
                            ." ORDER BY access_level DESC";
            if ( (AccessLevel::getList($db, $access, 'title, access_level', $condition)) <= 0 ) {
                $access[] = array('title' => 'None', 'access_level' => '0' );
            }
        
            return ($access);
        }
}
?>