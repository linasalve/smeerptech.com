<?php 

	class HrAttendanceMini {
		
        const DEACTIVE  = 0;
		const ACTIVE = 1; 
        
        function getStatus() {
            /*
             0			User has not marked his attendence for today .
             1			User has marked his attendence for today .
             2			User has marked his attendence for today and 
                                            also entered leaving time i.e. not present 
                                            in Office.
           */
			$status = array(
							'ACTIVE'   => HrAttendanceMini::ACTIVE,
                            'DEACTIVE'     => HrAttendanceMini::DEACTIVE                       
						);
			return ($status);
		}
        
        
		
		function getPresentyHrs(){
			$phrs = array(
							'1'=> 1,
							'0.25'=>0.25,
							'0.50'=>0.50,
							'0.75'=>0.75
						);
			return ($phrs);		
		}
		
		
         
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_HR_ATTENDANCE_MINI;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $module = NULL;
            if ( (HrAttendanceMini::getList($db, $module, 'att_id', " WHERE att_id = '$id'")) > 0 ) 
			{
                $module = $module[0];
					
                    $query = "DELETE FROM ". TABLE_HR_ATTENDANCE_MINI
								." WHERE att_id = '". $id ."'";
                    
					if ( $db->query($query) || $db->affected_rows()>=0 ){
                        $messages->setOkMessage("The Record has been deleted.");
                    }else{
                        $messages->setOkMessage("The Record was not deleted.");
                    }
			}
			else
				$messages->setErrorMessage("Record not found.");
		}
		
        function validateMemberSelect(&$data, $extra=''){
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			if ( !isset($data['executive_id']) || empty($data['executive_id']) ) {
                $messages->setErrorMessage('Select Executive.');
            }
			if(empty($data['from_date']) || empty($data['to_date'])){
				$messages->setErrorMessage('Select From date and to date.');
			}
			if(!empty($data['from_date'])){
                $temp1 = explode('/', $data['from_date']);
                $data["cmbMonth"]	= $temp1[1];
                $data["cmbDay"]	= $temp1[0];
                $data["cmbYear"]  = $temp1[2];
                $data['from_date1'] = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"];
            }
            if(!empty($data['to_date'])){
                $temp2 = explode('/', $data['to_date']);
                $data["cmbMonth"]	= $temp2[1];
                $data["cmbDay"]	= $temp2[0];
                $data["cmbYear"]  = $temp2[2];
                $data['to_date1'] = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"];
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
            /* $alreadyMark=0;
            if(!empty($data['executive']) && !empty($data['date'])){
                
                $query = "SELECT  id FROM ". TABLE_HR_ATTENDANCE_MINI 
                                           ." WHERE ".TABLE_HR_ATTENDANCE_MINI.".uid = '".$data['executive']."' 
                                           AND
                                           ".TABLE_HR_ATTENDANCE_MINI.".date = '".$dateCheck."'";
                $db->query($query);
                if ( $db->query($query) ) {
				    if ( $db->nf() > 0 ) {
                        $alreadyMark=1;
                        $messages->setErrorMessage('Attendance Already marked for Selected Associate on Selected date.');
                    }
                }
            } */
             
            if(!empty($data['dateList'])){
				foreach($data['dateList'] as $key11=>$value1){
					$key2 = $key11+1;
					if(!empty($data['executive_id1'][$key11]) && !empty($data['dateList'][$key11]) 
						&& !empty($data['present_score'][$key11])){
						
						 
						$temp2 = explode('/', $data['dateList'][$key11]);
						$data["cmbMonth"]	= $temp2[1];
						$data["cmbDay"]	= $temp2[0];
						$data["cmbYear"]  = $temp2[2];
						$dt = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"];
						//check if already
						$query = "SELECT  id FROM ". TABLE_HR_ATTENDANCE_MINI 
						   ." WHERE ".TABLE_HR_ATTENDANCE_MINI.".user_id = '".$data['executive_id1'][$key11]."' 
						   AND 
						   date_format(".TABLE_HR_ATTENDANCE_MINI.".attendance_dt, '%Y-%m-%d') = '".$dt."'";
						$db->query($query);
						if ( $db->query($query) ){
							if ( $db->nf() > 0 ){
								$messages->setErrorMessage('Attendance Already marked for Selected Associate on Selected date. Check date '.$data['dateList'][$key11]);
							}
						}
						
					}
					if(!empty($data['executive_id1'][$key11]) && empty($data['dateList'][$key11]) 
						&& empty($data['present_score'][$key11])){
					   $messages->setErrorMessage('Select Present day and date for no.'.$key2);
					}
					if(empty($data['executive_id1'][$key11]) && !empty($data['dateList'][$key11]) 
						&& empty($data['present_score'][$key11])){
					   $messages->setErrorMessage('Select team and present day for no.'.$key2);
					}
					if(empty($data['executive_id1'][$key11]) && empty($data['dateList'][$key11]) 
						&& !empty($data['present_score'][$key11])){
					   $messages->setErrorMessage('Select team and  date for no.'.$key2);
					}
					
					if(!empty($data['executive_id1'][$key11]) && !empty($data['dateList'][$key11]) 
						&& empty($data['present_score'][$key11])){
					   $messages->setErrorMessage('Select Present day for no.'.$key2);
					  
					  }
					if(empty($data['executive_id1'][$key11]) && !empty($data['dateList'][$key11]) 
						&& !empty($data['present_score'][$key11])){
					   $messages->setErrorMessage('Select team for no.'.$key2);
					}
					if(!empty($data['executive_id1'][$key11]) && empty($data['dateList'][$key11]) 
						&& !empty($data['present_score'][$key11])){
					   $messages->setErrorMessage('Select date for no.'.$key2);
					}
				}
			
			}
                  
             
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

			if(!empty($data['executive_id']) && !empty($data['dateList']) 
						&& !empty($data['present_score'])){
						
						 
				$temp2 = explode('/', $data['dateList']);
				$data["cmbMonth"]	= $temp2[1];
				$data["cmbDay"]	= $temp2[0];
				$data["cmbYear"]  = $temp2[2];
				$dt = $data['attendance_dt']=$data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"];
				//check if already
				$query = "SELECT  id FROM ". TABLE_HR_ATTENDANCE_MINI 
				   ." WHERE ".TABLE_HR_ATTENDANCE_MINI.".user_id = '".$data['executive_id']."' 
				   AND date_format(".TABLE_HR_ATTENDANCE_MINI.".attendance_dt, '%Y-%m-%d') = '".$dt."' 
				   AND ".TABLE_HR_ATTENDANCE_MINI.".id!='".$data['id']."'";
				$db->query($query);
				if ( $db->query($query) ){
					if ( $db->nf() > 0 ){
						$messages->setErrorMessage('Attendance Already marked for Selected Associate on Selected date. Check date '.$data['dateList']);
					}
				}
				
			}
			if(!empty($data['executive_id']) && empty($data['dateList']) 
				&& empty($data['present_score'])){
			   $messages->setErrorMessage('Select Present day and date ');
			}
			if(empty($data['executive_id']) && !empty($data['dateList']) 
				&& empty($data['present_score'])){
			   $messages->setErrorMessage('Select team and present day.');
			}
			if(empty($data['executive_id']) && empty($data['dateList']) 
				&& !empty($data['present_score'])){
			   $messages->setErrorMessage('Select team and  date.');
			}
			
			if(!empty($data['executive_id']) && !empty($data['dateList']) 
				&& empty($data['present_score'])){
			   $messages->setErrorMessage('Select Present day.');
			  					
			}
			if(empty($data['executive_id']) && !empty($data['dateList']) 
				&& !empty($data['present_score'])){
			   $messages->setErrorMessage('Select team.');
			}
			if(!empty($data['executive_id']) && empty($data['dateList']) 
				&& !empty($data['present_score'])){
			   $messages->setErrorMessage('Select date.');
			}
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
       
        
        
       
        
    }
?>