<?php
	class PaymentIAccountHead {
		
		       
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PAYMENT_IACCOUNT_HEAD;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
             $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PAYMENT_IACCOUNT_HEAD;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
            
            if( (Paymenttransaction::getList($db, $details, 'id', " WHERE  iaccount_head_id = '$id'")) == 0 ) {
              
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_PAYMENT_IACCOUNT_HEAD
                                ." WHERE id = '$id'";
                   
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            }
            else {
                $messages->setErrorMessage("Cannot Delete Record As Record exist in Payment Transaction with Account Head.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
            /*
            if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Transaction Type can not be empty.");
            }*/
            if ( !isset($data['account_head_name']) || empty($data['account_head_name']) ) {
                $messages->setErrorMessage("Account head name can not be empty.");
            }
            /* if ( !isset($data['account_head_code']) || empty($data['account_head_code']) ) {
                $messages->setErrorMessage("Code should not be empty.");
            }else{
                $condition  = " WHERE (account_head_code = '".$data['account_head_code']."') ";
                if ( (PaymentIAccountHead::getList($db, $list, 'id ', $condition)) > 0 ) {
                   $messages->setErrorMessage("Code is already exist.");
                }
            } */
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            } 
             
            /*
            if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Transaction Type can not be empty.");
            }*/      
            if ( !isset($data['account_head_name']) || empty($data['account_head_name']) ) {
                $messages->setErrorMessage("Account head name can not be empty.");
            }
            /* if ( !isset($data['account_head_code']) || empty($data['account_head_code']) ) {
                $messages->setErrorMessage("Code should not be empty.");
            }else{
                $condition  = " WHERE (account_head_code = '".$data['account_head_code']."') AND id!= '".$data['id']."'";
                if ( (PaymentIAccountHead::getList($db, $list, 'id ', $condition)) > 0 ) {
                   $messages->setErrorMessage("Code is already exist.");
                }
            } */
                   
          
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_PAYMENT_IACCOUNT_HEAD
                            ." SET ". TABLE_PAYMENT_IACCOUNT_HEAD .".status = '1'"
                            	 

                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Account head has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Account head has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_PAYMENT_IACCOUNT_HEAD
                            ." SET ". TABLE_PAYMENT_IACCOUNT_HEAD .".status = '0'"
                            	 

                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Account head has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Account head has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    
        
		
    }
?>
