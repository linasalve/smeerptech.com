<?php 

	class PlpPerformanceAppraisal {
		
		
		const ACTIVE  = 1;
		const DEACTIVE = 0;
		
        
        function getStatus() {
			$status = array(
							'ACTIVE'   => PlpPerformanceAppraisal::ACTIVE,
                            'DEACTIVE'    => PlpPerformanceAppraisal::DEACTIVE
						);
			return ($status);
		}      
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PLP_USER_INFO;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $module = NULL;
            if ( (PlpAppraisal::getList($db, $module, 'pa_app_id', " WHERE pa_app_id = '$id'")) > 0 ) 
			{
                $module = $module[0];
					
                    $query = "DELETE FROM ". TABLE_PLP_APPRAISAL
								." WHERE pa_app_id = '". $id ."'";
                    
					if ( $db->query($query) || $db->affected_rows()>=0 ){
                        $messages->setOkMessage("The Record has been deleted.");
                    }else{
                        $messages->setOkMessage("The Record was not deleted.");
                    }
			}
			else
				$messages->setErrorMessage("Record not found.");
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
			//validate the 
            if ( !isset($data['pa_app_criteria']) || empty($data['pa_app_criteria']) ) {
                $messages->setErrorMessage('Criteria cannot be empty.');
            }
            //validate the Particulars
            if ( !isset($data['pa_app_particulars']) || empty($data['pa_app_particulars']) ) {
                $messages->setErrorMessage('Particulars cannot be empty.');
            }
            
            if ( !isset($data['pa_app_percentage']) || empty($data['pa_app_percentage']) ) {
                $messages->setErrorMessage('PLP % cannot be empty.');
            }
            $list=null ;
            $condition_query = " WHERE pa_app_criteria = '". $data["pa_app_criteria"] ."' " ;
            if( PlpAppraisal::getList( $db, $list, TABLE_PLP_APPRAISAL.'.pa_app_id', $condition_query)>0){
                $messages->setErrorMessage("Criteria is already exist.");
            
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdatePerformance(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            if(in_array("",$data['performance'])){
			   $messages->setErrorMessage('Comments cannot be empty.');
			}
			if(in_array("",$data['txtperformance'])){
			   $messages->setErrorMessage('Assessed % cannot be empty.');
			}
			
            /*if ( !isset($data['pa_app_criteria']) || empty($data['pa_app_criteria']) ) {
                $messages->setErrorMessage('Criteria cannot be empty.');
            }
            
            if ( !isset($data['pa_app_particulars']) || empty($data['pa_app_particulars']) ) {
                $messages->setErrorMessage('Particulars cannot be empty.');
            }
            
            if ( !isset($data['pa_app_percentage']) || empty($data['pa_app_percentage']) ) {
                $messages->setErrorMessage('PLP % cannot be empty.');
            }
            $list=null ;
            $condition_query = " WHERE pa_app_criteria = '". $data["pa_app_criteria"] ."' AND pa_app_id != '". $data["pa_app_id"] ."'" ;
            if( PlpAppraisal::getList( $db, $list, TABLE_PLP_APPRAISAL.'.pa_app_id', $condition_query)>0){
                $messages->setErrorMessage("Criteria is already exist.");
            
            }*/
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
        function validateUpdateAppraisal(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            if(in_array("",$data['performance'])){
			   $messages->setErrorMessage('Self Assesment cannot be empty.');
			}
			if(in_array("",$data['txtperformance'])){
			   $messages->setErrorMessage('Assessed % cannot be empty.');
			}
			
            /*if ( !isset($data['pa_app_criteria']) || empty($data['pa_app_criteria']) ) {
                $messages->setErrorMessage('Criteria cannot be empty.');
            }
            
            if ( !isset($data['pa_app_particulars']) || empty($data['pa_app_particulars']) ) {
                $messages->setErrorMessage('Particulars cannot be empty.');
            }
            
            if ( !isset($data['pa_app_percentage']) || empty($data['pa_app_percentage']) ) {
                $messages->setErrorMessage('PLP % cannot be empty.');
            }
            $list=null ;
            $condition_query = " WHERE pa_app_criteria = '". $data["pa_app_criteria"] ."' AND pa_app_id != '". $data["pa_app_id"] ."'" ;
            if( PlpAppraisal::getList( $db, $list, TABLE_PLP_APPRAISAL.'.pa_app_id', $condition_query)>0){
                $messages->setErrorMessage("Criteria is already exist.");
            
            }*/
            
            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
            $query = " UPDATE ". TABLE_PLP_APPRAISAL
                    ." SET ". TABLE_PLP_APPRAISAL .".pa_app_status = '".PlpAppraisal::ACTIVE."'"
                    ." WHERE   	pa_app_id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Record has been activated.");                      
            }
            else {
                $messages->setErrorMessage("The Record has not been activated.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
               	
            $query = " UPDATE ". TABLE_PLP_APPRAISAL
                    ." SET ". TABLE_PLP_APPRAISAL .".pa_app_status = '".PlpAppraisal::DEACTIVE."'"
                    ." WHERE  pa_app_id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Record has been deactivated.");                      
            }
            else {
                $messages->setErrorMessage("The Record has not been deactivated.");
            }
                
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function getPlpUserPerformanceDetails(&$db, &$list, $required_fields='', $condition='', $from='', $rpp='') {
        
            $query = "SELECT ";
			 $count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PLP_USER_PERFORMANCE;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
        }
        
        function getPlpUserAppraisalDetails(&$db, &$list, $required_fields='', $condition='', $from='', $rpp='') {
        
            $query = "SELECT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PLP_USER_APPRAISAL;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
        }
        
    }
?>