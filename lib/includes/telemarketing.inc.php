<?php
	class Telemarketing {
		
		       
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				 $query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_TELEMARKETING;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_TELEMARKETING;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (Telemarketing::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               $details = $details[0];
               if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_TELEMARKETING
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                }
                else {
                    $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
           
            if ( !isset($data['name']) || empty($data['name']) ) {
                $messages->setErrorMessage("Name can not be empty.");
            }
            if ( !isset($data['contact_no']) || empty($data['contact_no']) ) {
                $messages->setErrorMessage("Contact no can not be empty.");
            }
            if(!empty($data['contact_no']) ){
                if(!is_numeric($data['contact_no'])){
                    $messages->setErrorMessage("Contact no should be numeric.");
                }
            }
            if ( !isset($data['company_address']) || empty($data['company_address']) ) {
                $messages->setErrorMessage("Address can not be empty.");
            }
            if ( !isset($data['call_date']) || empty($data['call_date']) ) {
                $messages->setErrorMessage("Call Date can not be empty.");
            }
            
            // Check for start date
            if(!empty($data['call_date'])){
                $dateArr= explode("/",$data['call_date']);
                $data['call_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
                
                $currDate=date('Y-m-d');
                if($data['call_date'] > $currDate){
                     $messages->setErrorMessage("Please select proper call date.");
                }
            }
             
           
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
             if ( !isset($data['name']) || empty($data['name']) ) {
                $messages->setErrorMessage("Name can not be empty.");
            }
            if ( !isset($data['contact_no']) || empty($data['contact_no']) ) {
                $messages->setErrorMessage("Contact no can not be empty.");
            }
            if(!empty($data['contact_no']) ){
                if(!is_numeric($data['contact_no'])){
                    $messages->setErrorMessage("Contact no should be numeric.");
                }
            }
            if ( !isset($data['company_address']) || empty($data['company_address']) ) {
                $messages->setErrorMessage("Address can not be empty.");
            }
            if ( !isset($data['call_date']) || empty($data['call_date']) ) {
                $messages->setErrorMessage("Call Date can not be empty.");
            }
            
            // Check for start date
            if(!empty($data['call_date'])){
                $dateArr= explode("/",$data['call_date']);
                $data['call_date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
                
                $currDate=date('Y-m-d');
                if($data['call_date'] > $currDate){
                     $messages->setErrorMessage("Please select proper call date.");
                }
            }          
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
		
    }
?>
