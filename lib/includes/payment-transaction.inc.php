<?php
	class Paymenttransaction {
		
		
		const PENDING = 0;
		const COMPLETED = 1;
        const CANCELLED_P = 2;
        //const COMPLETEDAS = 3;
        //const BOUNCED = 4;
        const CANCELLED_C = 5;
        const AUTOREVERSAL = 6;
        
        const DEACTIVATE_PENDING = 0;
        const DEACTIVATE_COMPLETED = 1;
        
        const PAYMENTIN = 1;
		const PAYMENTOUT = 2;
        const INTERNAL = 3;
		
		const BLANK = 0 ;
		const NOLINK = 1;
        const PARTIALLINK = 2;
        const COMPLETEDLINK = 3;
		
		const FOREXGAIN = 1;
        const ADJUSTMENT = 2;
		
        //const HANDLOAN = 2;
        //const BANKCHARGES = 3;
		
		const FORCEACTIVE = 1;
        const FORCEDEACTIVE = 0;
		
		const FORCECLOSED = 1;
        const FORCEOPEN = 0;
		
		const SALARY = 1;
		const BANKCHARGES = 2;
		const SALEPURCHASE = 3;
		const INTTRANS = 4;
		 
		function getBifurcateType(){
			$bstatus = array(
						Paymenttransaction::SALARY 		 => 'Salary',
						Paymenttransaction::BANKCHARGES => 'Bank Charges',
						Paymenttransaction::SALEPURCHASE => 'Sale/Purchase',
						Paymenttransaction::INTTRANS 	=> 'Between Our Bank Accounts'
					);
			return ($bstatus);
		}
		function getFStatus(){
			$fstatus = array(
						Paymenttransaction::FORCEOPEN => 'Force Open',
						Paymenttransaction::FORCECLOSED => 'Force Closed'
					);
			return ($fstatus);
		}
		function getTransactionType() {
			$transaction_type = array(
								'PAYMENT IN'   => Paymenttransaction::PAYMENTIN,
								'PAYMENT OUT'    => Paymenttransaction::PAYMENTOUT,
                                'INTERNAL'    => Paymenttransaction::INTERNAL
								//'HANDLOAN'   => Paymenttransaction::HANDLOAN,	
								//'BANK CHARGES'   => Paymenttransaction::BANKCHARGES
						);
			return ($transaction_type);
		}
		
		function getStatus() {
			$status = array(
							'PENDING'   => Paymenttransaction::PENDING,
                            'CANCELLED_P'   => Paymenttransaction::CANCELLED_P,
                            'COMPLETED'    => Paymenttransaction::COMPLETED                            
                            //'COMPLETED AS OF NOW'    => Paymenttransaction::COMPLETEDAS,
                            //'BOUNCED'    => Paymenttransaction::BOUNCED
						);
			return ($status);
		}
		
		function getGLType() {
			$type = array(
							Paymenttransaction::FOREXGAIN=>'FOREX GAIN' ,
                            Paymenttransaction::ADJUSTMENT=> 'AMOUNT ADJUSTMENT'
						);
			return ($type);
		}  
		function getLinkingStatus() {
			// Linking of transactions with bills and rcpts
			//Default is Blank, if no need to link bills/rcpt NOLINK, 
			//if partially linked bill/rcpt PARTIAL, if completly linked bills/rcpt COMPLETEDLINK
			$lstatus = array(
							'BLANK'   => Paymenttransaction::BLANK,
                            'NOLINK'   => Paymenttransaction::NOLINK,
                            'PARTIALLINK'    => Paymenttransaction::PARTIALLINK,  
                            'COMPLETEDLINK'    => Paymenttransaction::COMPLETEDLINK  
						);
			return ($lstatus);
		} 
		
        function getStatusAll() {
			$status = array(
							'PENDING'   => Paymenttransaction::PENDING,
                            'CANCELLED_P'   => Paymenttransaction::CANCELLED_P,
                            'COMPLETED'    => Paymenttransaction::COMPLETED,                            
                            //'COMPLETED AS OF NOW'    => Paymenttransaction::COMPLETEDAS,
                            //'BOUNCED'    => Paymenttransaction::BOUNCED,
                            'CANCELLED_C'    => Paymenttransaction::CANCELLED_C,
                            'AUTOREVERSAL'    => Paymenttransaction::AUTOREVERSAL
						);
			return ($status);
		}		
        function getReasonsOfDelete() {
			$status = array(
							''   => "Select Reason",
							'1'   => "Transaction Bounced",
                            '2'   => "Entry Person's Mistake",
                            '3'   => "Transaction Cancelled"
                            
						);
			return ($status);
		}		
        function getNewNumber(&$db){
            $number = 100;
            $query      = "SELECT MAX(number) AS number FROM ". TABLE_PAYMENT_TRANSACTION;
            if ( $db->query($query) && $db->nf()>0 && $db->next_record()){
                 $number1 =  $db->f("number");
                  $number1 = (int) $number1 ;
                if($number1 > 0){
                    $number = $number1 + 1;
                }else{
                    $number++;
                }
                
            }
            return $number;
        }
        /*function getPriority() {
			$priority = array(
							'HIGH'    => Paymentreminder::HIGH,
							'LOW'       => Paymentreminder::LOW,                            
							'MEDIUM'    => Paymentreminder::MEDIUM                            
						);
			return ($priority);
		} */     
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */	
		function getAccountHead( &$db, &$account_head,$type=''){
            if(!empty($type)){
                //$query="SELECT * FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE status='1' AND transaction_type='".$type."'";
                $query="SELECT * FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE status='1' ORDER by account_head_name";
            }else{
                 $query="SELECT * FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE status='1' ORDER by account_head_name";
            }        
			
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$account_head[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		function getIAccountHead( &$db, &$account_head,$type=''){
            if(!empty($type)){
                //$query="SELECT * FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE status='1' AND transaction_type='".$type."'";
                $query="SELECT * FROM ".TABLE_PAYMENT_IACCOUNT_HEAD." WHERE status='1' ORDER by account_head_name";
            }else{
                 $query="SELECT * FROM ".TABLE_PAYMENT_IACCOUNT_HEAD." WHERE status='1' ORDER by account_head_name";
            }        
			
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$account_head[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		function getParty( &$db, &$party){			
			$query="SELECT id, user_id, f_name as fname,l_name as lname,billing_name FROM ".TABLE_VENDORS." WHERE status='1' AND parent_id=''";
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$party[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		
		function getPaymentMode( &$db, &$payment_mode){			
			$query="SELECT * FROM ".TABLE_PAYMENT_MODE." WHERE status='1'";
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$payment_mode[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		 
		function getBank( &$db, &$bank){			
			$query="SELECT * FROM ".TABLE_PAYMENT_BANK." WHERE status='1'";
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$bank[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
        
        function getCompany( &$db, &$company){
			//$query="SELECT ".TABLE_SETTINGS_COMPANY.".* FROM ".TABLE_SETTINGS_COMPANY." INNER JOIN ".TABLE_FINANCIAL_YEAR." ON 	".TABLE_FINANCIAL_YEAR.".company_id= ".TABLE_SETTINGS_COMPANY.".id WHERE ".TABLE_SETTINGS_COMPANY.".status='1' AND ".TABLE_FINANCIAL_YEAR.".is_financial_yr='1' ";
			$query="SELECT ".TABLE_SETTINGS_COMPANY.".* FROM ".TABLE_SETTINGS_COMPANY ;
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$company[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		function getBillList( &$db, &$list, $required_fields, $condition='', $from='', $rpp=''){
        
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
               $query .= implode(",", $required_fields);               
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
                
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PAYMENT_PARTY_BILLS ;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
              $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }  
        }
        
        function getBillTransactionList(&$db, &$list, $required_fields, $condition='', $from='', $rpp=''){
            
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
               $query .= implode(",", $required_fields);               
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
                
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PAYMENT_TRANSACTION_BILLS ;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                  $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
        
        function validateBills(&$data,$extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			} 
			if(!isset($data['bill_id'])){
				$messages->setErrorMessage("Please Select the Bills and fill the amount.");
			}
            $data['tot_amount']=0;
			if(!empty($data['bill_id'])){
				foreach($data['bill_id'] as $key=>$val){
					if($data['validate_paid_amt'][$key]==1 ){				
						/* 
						if(empty($data['billamount'][$key])){					
							$messages->setErrorMessage("Please select Bill Paid Amount.");
						}
						if(empty($data['amount'][$key])){
							$messages->setErrorMessage("Amount should not be empty for bill.".$data['number'][$key]);
						} 
						*/
						if(!empty($data['billamount'][$key]) && !empty($data['amount'][$key])){
							if( $data['amount'][$key] > $data['billamount'][$key]){		
									$messages->setErrorMessage("Entered Amount should be less than equal to Bill Paid Amount.");
							}
							if(is_numeric($data['amount'][$key])){
									$data['tot_amount'] = $data['tot_amount'] + $data['amount'][$key];
							}
						}
					
					}else{
					
						if(!empty($data['amount'][$key])){
							if(!is_numeric($data['amount'][$key])){		
								$messages->setErrorMessage("Amount should be numric.");
							}
							if( $data['amount'][$key] > $data['bamount'][$key]){		
								$messages->setErrorMessage("Entered Amount should be less than equal to Bill 
								Amount Balance.");
							}
							if(is_numeric($data['amount'][$key])){
								$data['tot_amount'] = $data['tot_amount'] + $data['amount'][$key];
							}
						}else{
						   $messages->setErrorMessage("Amount should not be empty for bill ".$data['number'][$key]);					
						}
					}
				}
			}
            $pamount =  $data['pamount'];
            $totalBalance = (float) $data['totalBalance'];
			if($data['tot_amount']!= $totalBalance){
				if($data['tot_amount'] > $totalBalance){
					 $messages->setErrorMessage("Total Amount should not be greater than the Transaction amount. 
					 Transaction Balance Amount is ".$totalBalance." and total of bill amount is ".$data['tot_amount'] );
				}
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
            
        }
        
		function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
        	
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
               $query .= implode(",", $required_fields);               
            }elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
                
            }else{
                 $query .= " COUNT(DISTINCT(".TABLE_PAYMENT_TRANSACTION.".id)) as count";
				$count = 1;
            }
            
           /*  $query .= " FROM ". TABLE_PAYMENT_TRANSACTION 
					." LEFT JOIN ". TABLE_PAYMENT_BANK 
					." ON (". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id  
					OR ". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id
					OR ". TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id
					OR ". TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id
					) AND ".TABLE_PAYMENT_BANK.".type='1'"
					." LEFT JOIN ". TABLE_PAYMENT_MODE
					." ON ". TABLE_PAYMENT_TRANSACTION .".mode_id = ". TABLE_PAYMENT_MODE .".id "
					." LEFT JOIN ". TABLE_PAYMENT_ACCOUNT_HEAD
					." ON ". TABLE_PAYMENT_TRANSACTION .".account_head_id = ". TABLE_PAYMENT_ACCOUNT_HEAD .".id "
					." LEFT JOIN ". TABLE_SETTINGS_COMPANY
					." ON ". TABLE_PAYMENT_TRANSACTION .".company_id = ". TABLE_SETTINGS_COMPANY .".id "
					." LEFT JOIN ". TABLE_CLIENTS
					." ON ". TABLE_CLIENTS .".user_id = ". TABLE_PAYMENT_TRANSACTION .".client_id "
					." LEFT JOIN ". TABLE_VENDORS_BANK
					." ON ". TABLE_VENDORS_BANK .".user_id = ". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id "
					 ." LEFT JOIN ". TABLE_USER
					." ON ". TABLE_USER .".user_id = ". TABLE_PAYMENT_TRANSACTION .".executive_id ";
				 $query .= " ". $condition;
             */
			 
			  $query .= " FROM ". TABLE_PAYMENT_TRANSACTION 
					." LEFT JOIN ". TABLE_PAYMENT_BANK 
					." ON (". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id  
					OR ". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id
					) AND ".TABLE_PAYMENT_BANK.".type='1'"
					." LEFT JOIN ". TABLE_PAYMENT_MODE
					." ON ". TABLE_PAYMENT_TRANSACTION .".mode_id = ". TABLE_PAYMENT_MODE .".id "
					." LEFT JOIN ". TABLE_PAYMENT_ACCOUNT_HEAD
					." ON ". TABLE_PAYMENT_TRANSACTION .".account_head_id = ". TABLE_PAYMENT_ACCOUNT_HEAD .".id "
					." LEFT JOIN ". TABLE_SETTINGS_COMPANY
					." ON ". TABLE_PAYMENT_TRANSACTION .".company_id = ". TABLE_SETTINGS_COMPANY .".id "
					." LEFT JOIN ". TABLE_CLIENTS
					." ON ". TABLE_CLIENTS .".user_id = ". TABLE_PAYMENT_TRANSACTION .".client_id "
					." LEFT JOIN ". TABLE_VENDORS_BANK
					." ON ". TABLE_VENDORS_BANK .".user_id = ". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id "
					 ." LEFT JOIN ". TABLE_USER
					." ON ". TABLE_USER .".user_id = ". TABLE_PAYMENT_TRANSACTION .".executive_id ";
				 $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query.= " LIMIT ". $from .", ". $rpp;
            }
            //echo  $query;
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
		
        function validateAddShort(&$data,$extra='') {
           foreach ($extra as $key=>$value) {
				$$key = $value;
			}  
			$data['number'] = Paymenttransaction::getNewNumber($db);
            if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
			
        }
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateUpdateN(&$data, &$account_head,$extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			//$data['number'] = Paymenttransaction::getNewNumber($db);
			$sql=" SELECT status FROM ".TABLE_PAYMENT_TRANSACTION." WHERE id='".$data['id']."'";
			$db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
					$status_check = $db->f('status');
					if($status_check == Paymenttransaction::COMPLETED){ //Completed
						$messages->setErrorMessage("You can not update this transaction as this is marked
						as COMPLETED.");
					}
                }
            }			
			
			
			$query	=" SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
            $db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $account_head[]= processSqlData($db->result());
                }
            }
			if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
			if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Select transaction type.");
            }
			if ( !isset($data['tran_purpose']) || empty($data['tran_purpose']) ) {
                $messages->setErrorMessage("Select Transaction Purpose.");
            }
            if ( !isset($data['account_head_id']) || empty($data['account_head_id']) ) {
                $messages->setErrorMessage("Select account head.");
            }
			 
			if(!empty($data['transaction_type'])){
				if($data['transaction_type']==1){ //IN

					/* if(empty($data['ref_bank_id_fi']) && empty($data['client_id_fi']) 
						&& empty($data['executive_id_fi'])){
                        $messages->setErrorMessage("Please select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) 
						&& !empty($data['executive_id_fi'])){
                        $messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) && empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && empty($data['client_id_fi']) && !empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) && !empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}*/
					
					
					if($data['tran_purpose']==1){ //salary
						if(empty($data['ref_bank_id_fi'])){
							$messages->setErrorMessage("From - Bank Account.");
						}					
					}
					
					if(empty($data['credit_pay_bank_id'])){
						$messages->setErrorMessage("Select TO - Bank Account.");
					} 
					 
				}elseif($data['transaction_type']==2){ //out
				
					/* 
					if(empty($data['ref_bank_id_to']) && empty($data['client_id_to']) 
						&& empty($data['executive_id_to'])){
                        $messages->setErrorMessage("Please select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) 
						&& !empty($data['executive_id_to'])){
                        $messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) && empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && empty($data['client_id_to']) && !empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) && !empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					} */
					
					if(empty($data['debit_pay_bank_id'])){
						$messages->setErrorMessage("Select FROM - Bank Account.");
					}
				}elseif($data['transaction_type']==3){ //internal
				
					if(empty($data['credit_pay_bank_id'])){
						$messages->setErrorMessage("Select TO - Bank Account.");
					}
					if(empty($data['debit_pay_bank_id'])){
						$messages->setErrorMessage("Select FROM - Bank Account.");
					}
				}
			}
			
			if ( !isset($data['mode_id']) || empty($data['mode_id']) ) {
                $messages->setErrorMessage("Select payment mode.");
            }
            
            if($data['transaction_type']==Paymenttransaction::PAYMENTOUT || $data['transaction_type']==Paymenttransaction::INTERNAL){
				if($data['mode_id']==1){ //Cheque
					if(empty($data['cheque_no_id'])){
						$messages->setErrorMessage("Select Cheque No from the available Cheque List.");
					}else{
						$sql1= "SELECT ".TABLE_BANK_CHEQUE.".id,".TABLE_BANK_CHEQUE.".transaction_no
						FROM ".TABLE_BANK_CHEQUE." 
						WHERE ".TABLE_BANK_CHEQUE.".id='".$data['cheque_no_id']."' AND 
						".TABLE_BANK_CHEQUE.".status='1' AND ".TABLE_BANK_CHEQUE.".status_u='1' AND 
						".TABLE_BANK_CHEQUE.".transaction_no !='".$data['number']."'" ;
						$db->query( $sql1 );
						if( $db->nf() > 0 ){
							$trn_no = $db->f('transaction_no');
						   $messages->setErrorMessage("Selected Cheque No is already in-used with Tran No. ".$trn_no);
						}
					}
				}else{
					$data['cheque_no_id']=0;
				}
				
			}
            /* 
			if ( empty($data['do_transaction']) ) {
                $messages->setErrorMessage("Select Bank Transaction date.");
            }*/
            if ( empty($data['do_voucher']) ) {
                $messages->setErrorMessage("Select Voucher Date.");
            } 
            if ( !isset($data['pay_received_amt']) || empty($data['pay_received_amt']) ) {
                $messages->setErrorMessage("Payment Details amount cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt'])){					
					 $messages->setErrorMessage("Payment Details amount should be numeric value.");
				}
            }
			if(!isset($data['exchange_rate']) || empty($data['exchange_rate'])){
				$messages->setErrorMessage("Exchange Rate cannot be empty.");
			}else{
				if(!is_numeric($data['exchange_rate'])){					
					 $messages->setErrorMessage("Exchange Rate should be numeric value.");
				}
				
			}
            if ( !isset($data['pay_received_amt_words']) || empty($data['pay_received_amt_words']) ) {                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{   
                if(!Validation::isAlphabets($data['pay_received_amt_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }                
            }           
			if ( !isset($data['pay_received_amt_inr']) || empty($data['pay_received_amt_inr']) ) {
                $messages->setErrorMessage("Payment amount in INR cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt_inr'])){					
						 $messages->setErrorMessage("Payment amount in INR should be numeric value.");
					}
			}

			if($data['mode_id']!="2"){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					/*if(!is_numeric($data['pay_cheque_no'])){					
						$messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					}*/
				}
				if(empty($data['do_pay_received'])){
					$messages->setErrorMessage("Chq/DD/Tran Date should be required.");
				}
				if(empty($data['pay_bank_company'])){
					$messages->setErrorMessage("Bank/NBFC should be selected.");
				}
			}else{
				$data['pay_cheque_no']='';
				$data['do_pay_received']='';
				$data['pay_bank_company']='';
				$data['pay_branch']='';
			}
			if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
            $data['deposite_slip_attachment'] = '';
            
            if((!empty($files['deposite_slip_attachment']) && (!empty($files['deposite_slip_attachment']['name'])))){
                $filename = $files['deposite_slip_attachment']['name'];
                $data['deposite_slip_attachment'] = $filename;
                $type = $files['deposite_slip_attachment']['type'];
                 $size = $files['deposite_slip_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
		
		/**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAddN(&$data, &$account_head,$extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			$data['number'] = Paymenttransaction::getNewNumber($db);
			$query	=" SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
            $db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $account_head[]= processSqlData($db->result());
                }
            }
			if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
			if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Select transaction type.");
            } 
			if ( !isset($data['tran_purpose']) || empty($data['tran_purpose']) ) {
                $messages->setErrorMessage("Select Transaction Purpose.");
            }			
            if ( !isset($data['account_head_id']) || empty($data['account_head_id']) ) {
                $messages->setErrorMessage("Select account head.");
            }
			 			 
			if(!empty($data['transaction_type'])){
				if($data['transaction_type']==Paymenttransaction::PAYMENTIN){ //IN

					/* if(empty($data['ref_bank_id_fi']) && empty($data['client_id_fi']) 
						&& empty($data['executive_id_fi'])){
                        $messages->setErrorMessage("Please select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) 
						&& !empty($data['executive_id_fi'])){
                        $messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) && empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && empty($data['client_id_fi']) && !empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) && !empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					} */
					if($data['tran_purpose']==1){ //salary
						if(empty($data['ref_bank_id_fi'])){
							$messages->setErrorMessage("From - Bank Account.");
						}					
					}
					if(empty($data['credit_pay_bank_id'])){
						$messages->setErrorMessage("Select TO - Bank Account.");
					}
					 
				}elseif($data['transaction_type']==Paymenttransaction::PAYMENTOUT){ //out
				
					/* if(empty($data['ref_bank_id_to']) && empty($data['client_id_to']) 
						&& empty($data['executive_id_to'])){
                        $messages->setErrorMessage("Please select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) 
						&& !empty($data['executive_id_to'])){
                        $messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) && empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && empty($data['client_id_to']) && !empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) && !empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					} */
					
					if(empty($data['debit_pay_bank_id'])){
						$messages->setErrorMessage("Select FROM - Bank Account.");
					}
				}elseif($data['transaction_type']==Paymenttransaction::INTERNAL){ //internal
				
					if(empty($data['credit_pay_bank_id'])){
						$messages->setErrorMessage("Select TO - Bank Account.");
					}
					if(empty($data['debit_pay_bank_id'])){
						$messages->setErrorMessage("Select FROM - Bank Account.");
					}
				}
			}
			
			if ( !isset($data['mode_id']) || empty($data['mode_id']) ) {
                $messages->setErrorMessage("Select payment mode.");
            }
            
			if($data['transaction_type']==Paymenttransaction::PAYMENTOUT || $data['transaction_type']==Paymenttransaction::INTERNAL){
				if($data['mode_id']==1){ //Cheque
					if(empty($data['cheque_no_id'])){
						$messages->setErrorMessage("Select Cheque No from the available Cheque List.");
					}else{
						$sql1= "SELECT ".TABLE_BANK_CHEQUE.".id FROM ".TABLE_BANK_CHEQUE." 
						WHERE ".TABLE_BANK_CHEQUE.".id='".$data['cheque_no_id']."' AND 
						".TABLE_BANK_CHEQUE.".status='1' AND ".TABLE_BANK_CHEQUE.".status_u='1'" ;
						$db->query( $sql1 );
						if( $db->nf() > 0 ){
							 $messages->setErrorMessage("Selected Cheque No is already inused.");
						}
					}
				}else{
					$data['cheque_no_id']=0;
				}
				
			}
            
            /* 
			if ( empty($data['do_transaction']) ) {
                $messages->setErrorMessage("Select Bank Transaction date.");
            }*/
            if ( empty($data['do_voucher']) ) {
                $messages->setErrorMessage("Select Voucher Date.");
            } 
            if ( !isset($data['pay_received_amt']) || empty($data['pay_received_amt']) ) {
                $messages->setErrorMessage("Payment Details amount cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt'])){					
					 $messages->setErrorMessage("Payment Details amount should be numeric value.");
				}
            }
			if(!isset($data['exchange_rate']) || empty($data['exchange_rate'])){
				$messages->setErrorMessage("Exchange Rate cannot be empty.");
			}else{
				if(!is_numeric($data['exchange_rate'])){					
					 $messages->setErrorMessage("Exchange Rate should be numeric value.");
				}
				
			}
            if ( !isset($data['pay_received_amt_words']) || empty($data['pay_received_amt_words']) ) {                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{   
                if(!Validation::isAlphabets($data['pay_received_amt_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }                
            }           
			if ( !isset($data['pay_received_amt_inr']) || empty($data['pay_received_amt_inr']) ) {
                $messages->setErrorMessage("Payment amount in INR cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt_inr'])){					
						 $messages->setErrorMessage("Payment amount in INR should be numeric value.");
					}
			}

			if($data['mode_id']!="2"){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					/*if(!is_numeric($data['pay_cheque_no'])){					
						$messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					}*/
				}
				if(empty($data['do_pay_received'])){
					$messages->setErrorMessage("Chq/DD/Tran Date should be required.");
				}
				if(empty($data['pay_bank_company'])){
					$messages->setErrorMessage("Bank/NBFC should be selected.");
				}
			}else{
				$data['pay_cheque_no']='';
				$data['do_pay_received']='';
				$data['pay_bank_company']='';
				$data['pay_branch']='';
			}
			if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
            $data['deposite_slip_attachment'] = '';
            
            if((!empty($files['deposite_slip_attachment']) && (!empty($files['deposite_slip_attachment']['name'])))){
                $filename = $files['deposite_slip_attachment']['name'];
                $data['deposite_slip_attachment'] = $filename;
                $type = $files['deposite_slip_attachment']['type'];
                 $size = $files['deposite_slip_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
		
		/* 
			- TODO Add Quick Transaction
		*/
		
		function validateAddQuick(&$data, &$account_head,$extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			$data['number'] = Paymenttransaction::getNewNumber($db);
			$query	=" SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
            $db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $account_head[]= processSqlData($db->result());
                }
            }
			if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
			if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Select transaction type.");
            } 
			/* if ( !isset($data['tran_purpose']) || empty($data['tran_purpose']) ) {
                $messages->setErrorMessage("Select Transaction Purpose.");
            }			
            if ( !isset($data['account_head_id']) || empty($data['account_head_id']) ) {
                $messages->setErrorMessage("Select account head.");
            } */
			 			 
			if(!empty($data['transaction_type'])){
				if($data['transaction_type']==Paymenttransaction::PAYMENTIN){ //IN

					/*
					if(empty($data['ref_bank_id_fi']) && empty($data['client_id_fi']) 
						&& empty($data['executive_id_fi'])){
                        $messages->setErrorMessage("Please select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) 
						&& !empty($data['executive_id_fi'])){
                        $messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) && empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_fi']) && empty($data['client_id_fi']) && !empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(empty($data['ref_bank_id_fi']) && !empty($data['client_id_fi']) && !empty($data['executive_id_fi'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					} */
					/* if($data['tran_purpose']==1){ //salary
						if(empty($data['ref_bank_id_fi'])){
							$messages->setErrorMessage("From - Bank Account.");
						}					
					}
					if(empty($data['credit_pay_bank_id'])){
						$messages->setErrorMessage("Select TO - Bank Account.");
					} */
					 
				}elseif($data['transaction_type']==Paymenttransaction::PAYMENTOUT){ //out
				
					/* if(empty($data['ref_bank_id_to']) && empty($data['client_id_to']) 
						&& empty($data['executive_id_to'])){
                        $messages->setErrorMessage("Please select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) 
						&& !empty($data['executive_id_to'])){
                        $messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) && empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(!empty($data['ref_bank_id_to']) && empty($data['client_id_to']) && !empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					}elseif(empty($data['ref_bank_id_to']) && !empty($data['client_id_to']) && !empty($data['executive_id_to'])){
						$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					} */
					
					/* if(empty($data['debit_pay_bank_id'])){
						$messages->setErrorMessage("Select FROM - Bank Account.");
					} */
				}elseif($data['transaction_type']==Paymenttransaction::INTERNAL){ //internal
				
					/* if(empty($data['credit_pay_bank_id'])){
						$messages->setErrorMessage("Select TO - Bank Account.");
					}
					if(empty($data['debit_pay_bank_id'])){
						$messages->setErrorMessage("Select FROM - Bank Account.");
					} */
				}
			}
			
			if ( !isset($data['mode_id']) || empty($data['mode_id']) ) {
                $messages->setErrorMessage("Select payment mode.");
            }
            
			if($data['transaction_type']==Paymenttransaction::PAYMENTOUT || $data['transaction_type']==Paymenttransaction::INTERNAL){
				if($data['mode_id']==1){ //Cheque
					if(empty($data['cheque_no_id'])){
						$messages->setErrorMessage("Select Cheque No from the available Cheque List.");
					}else{
						$sql1= "SELECT ".TABLE_BANK_CHEQUE.".id FROM ".TABLE_BANK_CHEQUE." 
						WHERE ".TABLE_BANK_CHEQUE.".id='".$data['cheque_no_id']."' AND 
						".TABLE_BANK_CHEQUE.".status='1' AND ".TABLE_BANK_CHEQUE.".status_u='1'" ;
						$db->query( $sql1 );
						if( $db->nf() > 0 ){
							 $messages->setErrorMessage("Selected Cheque No is already inused.");
						}
					}
				}else{
					$data['cheque_no_id']=0;
				}
			}            
             
            if ( empty($data['do_voucher']) ) {
                $messages->setErrorMessage("Select Voucher Date.");
            } 
            if ( !isset($data['pay_received_amt']) || empty($data['pay_received_amt']) ) {
                $messages->setErrorMessage("Payment Details amount cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt'])){					
					 $messages->setErrorMessage("Payment Details amount should be numeric value.");
				}
            }
			if(!isset($data['exchange_rate']) || empty($data['exchange_rate'])){
				$messages->setErrorMessage("Exchange Rate cannot be empty.");
			}else{
				if(!is_numeric($data['exchange_rate'])){					
					 $messages->setErrorMessage("Exchange Rate should be numeric value.");
				} 
			}
            /*  
			if ( !isset($data['pay_received_amt_words']) || empty($data['pay_received_amt_words']) ) {                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{   
                if(!Validation::isAlphabets($data['pay_received_amt_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }                
            }           
			if ( !isset($data['pay_received_amt_inr']) || empty($data['pay_received_amt_inr']) ) {
                $messages->setErrorMessage("Payment amount in INR cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt_inr'])){					
						 $messages->setErrorMessage("Payment amount in INR should be numeric value.");
					}
			} */

			if($data['mode_id']!="2"){   

				/* if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					if(!is_numeric($data['pay_cheque_no'])){					
						$messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					}
				}
				if(empty($data['do_pay_received'])){
					$messages->setErrorMessage("Chq/DD/Tran Date should be required.");
				}
				if(empty($data['pay_bank_company'])){
					$messages->setErrorMessage("Bank/NBFC should be selected.");
				} */
			}else{
				$data['pay_cheque_no']='';
				$data['do_pay_received']='';
				$data['pay_bank_company']='';
				$data['pay_branch']='';
			}
			if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
            $data['deposite_slip_attachment'] = '';
            
            if((!empty($files['deposite_slip_attachment']) && (!empty($files['deposite_slip_attachment']['name'])))){
                $filename = $files['deposite_slip_attachment']['name'];
                $data['deposite_slip_attachment'] = $filename;
                $type = $files['deposite_slip_attachment']['type'];
                 $size = $files['deposite_slip_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, &$account_head,$extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
     
            //$query	="SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1' AND ".TABLE_PAYMENT_ACCOUNT_HEAD.".transaction_type= '".$data['transaction_type']."'";
            //$query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
           
            $query	=" SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
            $db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $account_head[]= processSqlData($db->result());
                }
            }
            /*
            if ( !isset($data['company_id']) || empty($data['company_id']) ) {
                $messages->setErrorMessage("Select company.");
            }*/
            $data['transaction_id']='';
            //$data['transaction_id'] = getCounterNumber($db,CONTR_VCH,$data['company_id']);
            //
			if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            /*
          
            if(empty($data['financialYr'])){
                 $messages->setErrorMessage("Select Financial Yr.");
            }
            */
            /*
            if(!empty($data['financialYr']) && !empty($data['company_id']) ){
            
                 $data['transaction_id']= getOldCounterNumber($db,CONTR_VCH,$data['company_id'],$data['financialYr']); 
                //$_ALL_POST['number'] =$data['number']= getCounterNumber($db,'ORD',$data['company_id']); 
                if(empty($data['transaction_id']) ){
                    $messages->setErrorMessage("Transaction Number was not generated.");
                }
            }*/
            //
            $data['number'] = Paymenttransaction::getNewNumber($db);
            /*
            if(!empty($data['number'])){
               
                $list = NULL;
                if ( Paymenttransaction::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                    $messages->setErrorMessage(" Transaction with the same Number already exists.<br/>Please try again.");
                    //$messages->setErrorMessage("An Order with the same Number already exists.");
                }              
            }*/
            
            
            if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Select transaction type.");
            }
            
            if ( !isset($data['account_head_id']) || empty($data['account_head_id']) ) {
                $messages->setErrorMessage("Select account head.");
            }
            // As restricted so validate the party/client/staff
            /* if($data['restricted_add']==0){
                if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
                    if ( empty($data['vendor_bank_id']) && empty($data['party_id']) && empty($data['client_id']) 
					&& empty($data['executive_id'])) {
                        $messages->setErrorMessage("Please select Vendor Bank or Vendor or Client or Staff.");                
                    }
                   
                }            
                if(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
                        $messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
                }elseif(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
				}elseif(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
				}elseif(!empty($data['vendor_bank_id']) && empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
				}elseif(empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
				}
                
            }else{
				if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
					
					if( !empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) 
						&& !empty($data['executive_id'])){
							$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}elseif( !empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && 
					empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}elseif( !empty($data['vendor_bank_id']) && !empty($data['party_id']) && empty($data['client_id']) && 
					!empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}elseif( !empty($data['vendor_bank_id']) && empty($data['party_id']) && !empty($data['client_id']) && 
					!empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}elseif( empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && 
					!empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}
				}
                ///$data['party_id']=$data['client_id']=$data['executive_id']='';
            } */
			if($data['restricted_add']==0){
                if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
                    if ( empty($data['vendor_bank_id']) && empty($data['client_id']) 
					&& empty($data['executive_id'])) {
                        $messages->setErrorMessage("Please select Vendor Bank or Client or Staff.");                
                    }
                   
                }            
                if(!empty($data['vendor_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
                        $messages->setErrorMessage("Either select Vendor Bank or Client or Staff.");
                }elseif(!empty($data['vendor_bank_id']) && !empty($data['client_id']) && empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Client or Staff.");
				}elseif(!empty($data['vendor_bank_id']) && empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Client or Staff.");
				}elseif(empty($data['vendor_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Client or Staff.");
				}
                
            }else{
				if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
					
					if( !empty($data['vendor_bank_id']) && !empty($data['client_id']) 
						&& !empty($data['executive_id'])){
							$messages->setErrorMessage("Either select Vendor Bank or Client or Staff.");
					}elseif( !empty($data['vendor_bank_id']) && !empty($data['client_id']) 
						&& empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Client or Staff.");
					}elseif( !empty($data['vendor_bank_id']) && empty($data['client_id']) && 
						!empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Client or Staff.");
					}elseif( empty($data['vendor_bank_id']) && !empty($data['client_id']) && 
					!empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Client or Staff.");
					}
				}
            } 
			if(!empty($data['vendor_bank_id'])){
				if(empty($data['ref_bank_id']) && $data['transaction_type']!= 3){
					$messages->setErrorMessage("Select ref bank account as you had selected Bank/CAs/Directors.");
				}				
			}
			if(!empty($data['client_id']) || !empty($data['executive_id'])){
				if(!empty($data['ref_bank_id'])){
					$messages->setErrorMessage("Please un-select the ref bank account, as you selected
					Client/Staff.");  
				}
			}
			/* 
			if( $data['transaction_type']== 2 && !empty($data['client_id'])){
				$messages->setErrorMessage("Select Vendor or Staff as Transaction Type is Payment Out.");			
			}
			 */
			
            if ( !isset($data['mode_id']) || empty($data['mode_id']) ) {
                $messages->setErrorMessage("Select payment mode.");
            }
            
            
            if ( empty($data['do_transaction']) ) {
                $messages->setErrorMessage("Select Bank Transaction date.");
            }
            if ( empty($data['do_voucher']) ) {
                $messages->setErrorMessage("Select Voucher Date.");
            }
            if ( !isset($data['pay_received_amt']) || empty($data['pay_received_amt']) ) {
                $messages->setErrorMessage("Payment Details amount cannot be empty.");
            }else{
					if(!is_numeric($data['pay_received_amt'])){					
						 $messages->setErrorMessage("Payment Details amount should be numeric value.");
					}
            }
			if(!isset($data['exchange_rate']) || empty($data['exchange_rate'])){
				$messages->setErrorMessage("Exchange Rate cannot be empty.");
			}else{
				if(!is_numeric($data['exchange_rate'])){					
					 $messages->setErrorMessage("Exchange Rate should be numeric value.");
				}
				
			}
            if ( !isset($data['pay_received_amt_words']) || empty($data['pay_received_amt_words']) ) {                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{   
                if(!Validation::isAlphabets($data['pay_received_amt_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }                
            }           
			if ( !isset($data['pay_received_amt_inr']) || empty($data['pay_received_amt_inr']) ) {
                $messages->setErrorMessage("Payment amount in INR cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt_inr'])){					
						 $messages->setErrorMessage("Payment amount in INR should be numeric value.");
					}
			}            
            if($data['transaction_type']=="1" || $data['transaction_type']=="3" ){   
                 $data['is_bill_receive']='';

				if ( !isset($data['credit_pay_amt']) || empty($data['credit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be credited can not be empty.");
				}else{
					if(!is_numeric($data['credit_pay_amt'])){					
						 $messages->setErrorMessage("Amount to be credited should be numeric value.");
					}				
				}
                if ( !isset($data['credit_pay_bank_id']) || empty($data['credit_pay_bank_id']) ) {
					$messages->setErrorMessage("Select Bank from which amount is credited.");
				}
			}
			
			if($data['transaction_type']=="2" || $data['transaction_type']=="3" ){   
                $data['is_receipt']='';

				if ( !isset($data['debit_pay_amt']) || empty($data['debit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be debited can not be empty.");
				}else{
					if(!is_numeric($data['debit_pay_amt'])){
						 $messages->setErrorMessage("Amount to be debited should be numeric value.");
					}
				}
                if(empty($data['gl_type'])){
					if ( !isset($data['debit_pay_bank_id']) || empty($data['debit_pay_bank_id']) ) {
						$messages->setErrorMessage("Select Bank from which amount is debited.");
					}
				}else{
					//if gainloss type selected debit_bank_id should be 0
					$data['debit_pay_bank_id'] = 0;
				}
			}
			
			if($data['mode_id']!="2"){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					/*if(!is_numeric($data['pay_cheque_no'])){					
						$messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					}*/
				}
				if(empty($data['do_pay_received'])){
					$messages->setErrorMessage("Chq/DD/Tran Date should be required.");
				}
				if(empty($data['pay_bank_company'])){
					$messages->setErrorMessage("Bank/Company should be selected.");
				}
				
				
			}else{
				$data['pay_cheque_no']='';
				$data['do_pay_received']='';
				$data['pay_bank_company']='';
				$data['pay_branch']='';
			}	
            if($data['is_pdc']=='1'){
                if ( !isset($data['do_pdc_chq']) || empty($data['do_pdc_chq']) ) {
                    $messages->setErrorMessage("Select PDC/Scheduled date.");
                }else{
                    //$dateArr=null;
                    //$dateArr= explode("/",$data['do_pdc_chq']);
                    //$data['do_pdc_chq'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
                    
                     //$data['do_pdc_chq'] = explode('/', $data['do_pdc_chq']);                               
                    // $data['do_pdc_chq'] = mktime(0, 0, 0, $data['do_pdc_chq'][1], $data['do_pdc_chq'][0], $data['do_pdc_chq'][2]);
                    
                }
                if ( !isset($data['pdc_chq_details']) || empty($data['pdc_chq_details']) ) {
                    $messages->setErrorMessage(" PDC details should not be empty.");
                }
            }else{
                 $data['do_pdc_chq'] = '';
                 $data['pdc_chq_details'] = '';
            }
            
            if(isset($data['is_receipt']) && $data['is_receipt']=='1'){
                if ( !isset($data['receipt_no']) || empty($data['receipt_no']) ) {
                    $messages->setErrorMessage("Receipt no. cannot be empty.");
                }
            }else{
                 $data['receipt_no'] = '';                 
            }
            
            if($data['is_bill_receive']=='1'){
                if ( !isset($data['bill_no']) || empty($data['bill_no']) ) {
                    $messages->setErrorMessage("Bill no. cannot be empty.");
                }
            }else{
                 $data['bill_no'] = '';                 
            }
          
             /*
			if ( !isset($data['voucher_no']) || empty($data['voucher_no']) ) {
                $messages->setErrorMessage("Voucher No. cannot be empty.");
            }*/
			 
            if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
            
            if($data['transaction_type']=="3"){   
				if ( !isset($data['credit_pay_amt']) || empty($data['debit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be debited and credited should be same.");
				}
			}
            $data['deposite_slip_attachment'] = '';
            
            if((!empty($files['deposite_slip_attachment']) && (!empty($files['deposite_slip_attachment']['name'])))){
                $filename = $files['deposite_slip_attachment']['name'];
                $data['deposite_slip_attachment'] = $filename;
                $type = $files['deposite_slip_attachment']['type'];
                 $size = $files['deposite_slip_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		function validatePUpdateChq(&$data, &$account_head, $extra='') {
            foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            /* if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            } */
			
            $query	="SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE 
			".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
            
            $db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $account_head[]= processSqlData($db->result());
                }
            }            
            $list = NULL;
            
            
            if ( !isset($data['account_head_id']) || empty($data['account_head_id']) ) {
                $messages->setErrorMessage("Select account head.");
            }          
         
			/* if(!empty($data['vendor_bank_id'])){
				if(empty($data['ref_bank_id']) && $data['transaction_type']!= 3){
					$messages->setErrorMessage("Select ref bank account as you had selected Bank/CAs/Directors.");
				}				
			} */
			/* if(!empty($data['client_id']) || !empty($data['executive_id'])){
				if(!empty($data['ref_bank_id'])){
					$messages->setErrorMessage("Please un-select the ref bank account, as you selected
					Client/Staff.");  
				}
			} */
			if($data['transaction_type']!=Paymenttransaction::INTERNAL && isset($data['ref_bank_id'])){
				if(empty($data['ref_bank_id']) && empty($data['client_id']) && empty($data['executive_id'])){
					$messages->setErrorMessage("Please select Client or Staff or SMEERP Bank Account.");
					
				}elseif(!empty($data['ref_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					
				}elseif(!empty($data['ref_bank_id']) && !empty($data['client_id']) && empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					
				}elseif(!empty($data['ref_bank_id']) && empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					
				}elseif(empty($data['ref_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Client or Staff or SMEERP Bank Account.");
					
				}
			}
            if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
            
			if((!empty($files['bill_attachment']) && (!empty($files['bill_attachment']['name'])))){                
				$filename = $files['bill_attachment']['name'];
              
                $type = $files['bill_attachment']['type'];
                $size = $files['bill_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
				 
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 300Kb');
                }
            }  
			$data['deposite_slip_attachment'] = '';
			 
            if((!empty($files['deposite_slip_attachment']) && (!empty($files['deposite_slip_attachment']['name'])))){
                $filename = $files['deposite_slip_attachment']['name'];
                $data['deposite_slip_attachment'] = $filename;
                $type = $files['deposite_slip_attachment']['type'];
                 $size = $files['deposite_slip_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
            			
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		
		
		/**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validatePAdd(&$data, &$account_head,$extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
			if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            $query	=" SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
            $db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $account_head[]= processSqlData($db->result());
                }
            }
            
            $data['transaction_id']='';
            //$data['transaction_id'] = getCounterNumber($db,CONTR_VCH,$data['company_id']);           
            $data['number'] = Paymenttransaction::getNewNumber($db);
			
            /*
            if(!empty($data['number'])){               
                $list = NULL;
                if ( Paymenttransaction::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage(" Transaction with the same Number already exists.<br/>Please try again.");
                //$messages->setErrorMessage("An Order with the same Number already exists.");
                }              
            }*/
            if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Select transaction type.");
            }
            
            if ( !isset($data['account_head_id']) || empty($data['account_head_id']) ) {
                $messages->setErrorMessage("Select account head.");
            }
            // As restricted so validate the party/client/staff
            /* if($data['restricted_add']==0){
                if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
                    if ( empty($data['vendor_bank_id']) && empty($data['party_id']) && empty($data['client_id']) && empty($data['executive_id'])) {
                        $messages->setErrorMessage("Please select Vendor Bank or Vendor or Client or Staff.");                
                    }
                }            
                if(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
                        $messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
                }elseif(!empty($data['vendor_bank_id']) &&!empty($data['party_id']) && !empty($data['client_id']) && empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
				}elseif(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
				}elseif(!empty($data['vendor_bank_id']) && empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
				}elseif(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
				}
                
            }else{
				if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
					if(!empty($data['vendor_bank_id']) &&!empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
							$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}elseif(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}elseif(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && empty($data['client_id']) && !empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}elseif(!empty($data['vendor_bank_id']) && empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}elseif(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Vendor or Client or Staff.");
					}
				}
                ///$data['party_id']=$data['client_id']=$data['executive_id']='';
            } */
			
			if($data['restricted_add']==0){
                if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
                    if ( empty($data['vendor_bank_id']) &&  empty($data['client_id']) && empty($data['executive_id'])) {
                        $messages->setErrorMessage("Please select Vendor Bank or Member or Staff.");                
                    }
                }            
                if(!empty($data['vendor_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
                        $messages->setErrorMessage("Either select Vendor Bank or Member or Staff.");
                }elseif(!empty($data['vendor_bank_id']) && !empty($data['client_id']) && empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Member or Staff.");
				}elseif(!empty($data['vendor_bank_id']) && empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Member or Staff.");
				}elseif(empty($data['vendor_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
					$messages->setErrorMessage("Either select Vendor Bank or Member or Staff.");
				}
                
            }else{
				if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
					if(!empty($data['vendor_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
							$messages->setErrorMessage("Either select Vendor Bank or Member or Staff.");
					}elseif(!empty($data['vendor_bank_id']) && !empty($data['client_id']) && empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Member or Staff.");
					}elseif(!empty($data['vendor_bank_id']) && empty($data['client_id']) && !empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Member or Staff.");
					}elseif(empty($data['vendor_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
						$messages->setErrorMessage("Either select Vendor Bank or Member or Staff.");
					}
				}
                ///$data['party_id']=$data['client_id']=$data['executive_id']='';
            }
			
			if(!empty($data['vendor_bank_id'])){
				if(empty($data['ref_bank_id']) && $data['transaction_type']!= 3){
					$messages->setErrorMessage("Select ref bank account as you had selected Bank/CAs/Directors.");
				}				
			}
			if(!empty($data['client_id']) || !empty($data['executive_id'])){
				if(!empty($data['ref_bank_id'])){
					$messages->setErrorMessage("Please un-select the ref bank account, as you selected
					Client/Staff.");  
				}
			}
			/* if( $data['transaction_type']== 2 && !empty($data['client_id'])){
				$messages->setErrorMessage("Select Vendor or Staff as Transaction Type is Payment Out.");			
			} */
            if ( !isset($data['mode_id']) || empty($data['mode_id']) ) {
                $messages->setErrorMessage("Select payment mode.");
            } 
			if ( empty($data['do_transaction']) ) {
                $messages->setErrorMessage("Select Bank Transaction date.");
            }
			if ( empty($data['do_voucher']) ) {
                $messages->setErrorMessage("Select Voucher Date.");
            }
            if ( !isset($data['pay_received_amt']) || empty($data['pay_received_amt']) ) {
                $messages->setErrorMessage("Payment Details amount cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt'])){					
					 $messages->setErrorMessage("Payment Details amount should be numeric value.");
				}
            }
			if(!isset($data['exchange_rate']) || empty($data['exchange_rate'])){
				$messages->setErrorMessage("Exchange Rate cannot be empty.");
			}else{
				if(!is_numeric($data['exchange_rate'])){					
					 $messages->setErrorMessage("Exchange Rate should be numeric value.");
				}
				
			}
            if ( !isset($data['pay_received_amt_words']) || empty($data['pay_received_amt_words']) ) {                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{   
                if(!Validation::isAlphabets($data['pay_received_amt_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }                
            }
           
			if ( !isset($data['pay_received_amt_inr']) || empty($data['pay_received_amt_inr']) ) {
                $messages->setErrorMessage("Payment amount in INR cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt_inr'])){					
						 $messages->setErrorMessage("Payment amount in INR should be numeric value.");
				}
			}			
            if($data['transaction_type']=="1" || $data['transaction_type']=="3" ){   
                 $data['is_bill_receive']='';

				if ( !isset($data['credit_pay_amt']) || empty($data['credit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be credited can not be empty.");
				}else{
					if(!is_numeric($data['credit_pay_amt'])){					
						 $messages->setErrorMessage("Amount to be credited should be numeric value.");
					}				
				}
				
                if(!empty($files['deposite_slip_attachment'])){
					if ( !isset($data['credit_pay_bank_id']) || empty($data['credit_pay_bank_id']) ) {
						$messages->setErrorMessage("Select Bank to which amount is credited.");
					}
				}
			}
			
			if($data['transaction_type']=="2" || $data['transaction_type']=="3" ){   
                $data['is_receipt']='';

				if ( !isset($data['debit_pay_amt']) || empty($data['debit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be debited can not be empty.");
				}else{
					if(!is_numeric($data['debit_pay_amt'])){
						 $messages->setErrorMessage("Amount to be debited should be numeric value.");
					}
				}
                if(empty($data['gl_type'])){
					if ( !isset($data['debit_pay_bank_id']) || empty($data['debit_pay_bank_id']) ) {
						$messages->setErrorMessage("Select Bank from which amount is debited.");
					}
				}else{
					//if gainloss type selected debit_bank_id should be 0
					$data['debit_pay_bank_id'] = 0;
				}
			}

			if($data['mode_id']!="2"){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					/*
					if(!is_numeric($data['pay_cheque_no'])){					
						$messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					}*/
				}
				if(empty($data['do_pay_received'])){
					$messages->setErrorMessage("Chq/DD/Tran Date should be required.");
				}
				if(empty($data['pay_bank_company'])){
					$messages->setErrorMessage("Bank/Company should be selected.");
				}
				
				
			}else{
				$data['pay_cheque_no']='';
				$data['do_pay_received']='';
				$data['pay_bank_company']='';
				$data['pay_branch']='';
			}			
            if($data['is_pdc']=='1'){
                if ( !isset($data['do_pdc_chq']) || empty($data['do_pdc_chq']) ) {
                    $messages->setErrorMessage("Select PDC/Scheduled date.");
                }else{ 
                }
                if ( !isset($data['pdc_chq_details']) || empty($data['pdc_chq_details']) ) {
                    $messages->setErrorMessage(" PDC details should not be empty.");
                }
            }else{
                 $data['do_pdc_chq'] = '';
                 $data['pdc_chq_details'] = '';
            }
            
            if(isset($data['is_receipt']) && $data['is_receipt']=='1'){
                if ( !isset($data['receipt_no']) || empty($data['receipt_no']) ) {
                    $messages->setErrorMessage("Receipt no. cannot be empty.");
                }
            }else{
                 $data['receipt_no'] = '';                 
            }
            
            if($data['is_bill_receive']=='1'){
                if ( !isset($data['bill_no']) || empty($data['bill_no']) ) {
                    $messages->setErrorMessage("Bill no. cannot be empty.");
                }
            }else{
                 $data['bill_no'] = '';                 
            }
          
             /*
			if ( !isset($data['voucher_no']) || empty($data['voucher_no']) ) {
                $messages->setErrorMessage("Voucher No. cannot be empty.");
            }*/
			 
            if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
            
            if($data['transaction_type']=="3"){   
				if ( !isset($data['credit_pay_amt']) || empty($data['debit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be debited and credited should be same.");
				}
			}
            $data['deposite_slip_attachment'] = '';
            
            if((!empty($files['deposite_slip_attachment']) && (!empty($files['deposite_slip_attachment']['name'])))){
                $filename = $files['deposite_slip_attachment']['name'];
                $data['deposite_slip_attachment'] = $filename;
                $type = $files['deposite_slip_attachment']['type'];
                 $size = $files['deposite_slip_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		
		
		
		function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;            
				$query = "DELETE FROM ". TABLE_PAYMENT_TRANSACTION
							." WHERE id = '$id'";
				
				$query1 = "DELETE FROM ". TABLE_PAYMENT_TRANSACTION_LOG
							." WHERE  task_id = '$id'";     
												
				if ( ( $db->query($query) || $db->query($query1) ) && ( $db->affected_rows()>0 ) ) {
					$messages->setOkMessage("The Record has been deleted.");                      
				}
				else {
					$messages->setErrorMessage("The Record was not deleted.");
				}
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
		 	
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$count = $total=0;
			$query = "SELECT DISTINCT ";
			
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				 $query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PAYMENT_TRANSACTION;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
               
	    //Used in communication log
	    function getDetailsCommLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count ";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PAYMENT_TRANSACTION_LOG;
            //$query .= " LEFT JOIN ". TABLE_USER
             //." ON ". TABLE_USER .".user_id = ". TABLE_PAYMENT_REMINDER_LOG .".by_id  ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
    

		function validateCommentAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( !isset($data['to_id']) || empty($data['to_id']) ) {
                //$messages->setErrorMessage("Select Executive.");
                $messages->setErrorMessage(" 'Comment to' Field should not be empty.");
            }
            /*if ( !isset($data['by_id']) || empty($data['by_id']) ) {
                //$messages->setErrorMessage("Select Executive.");
                 $messages->setErrorMessage(" 'Comment by' Field should not be empty.");
            }*/
            if ( !isset($data['pcomment']) || empty($data['pcomment']) ) {
                $messages->setErrorMessage("Comment should not be empty.");
            }
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}

        }
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, &$account_head, $extra='') {
            foreach ($extra as $key=>$value) {
				$$key = $value;
			}    
            
			 
            /*
            $query	="SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1' AND ".TABLE_PAYMENT_ACCOUNT_HEAD.".transaction_type= '".$data['transaction_type']."'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
             */
            $query	="SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
            
            $db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $account_head[]= processSqlData($db->result());
                }
            }
           
             
            if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            
            //$data['transaction_id'] = getCounterNumber($db,CONTR_VCH,$data['company_id']);
            
            $list = NULL;
            /*
            if ( Paymenttransaction::getList($db, $list, 'id', " WHERE transaction_id = '". $data['transaction_id'] ."'")>0 ) {
                $messages->setErrorMessage(" Transaction with the same Number already exists.<br/>Please try again.");
                //$messages->setErrorMessage("An Order with the same Number already exists.");
            }
            */
            if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Select transaction type.");
            }
            
            if ( !isset($data['account_head_id']) || empty($data['account_head_id']) ) {
                $messages->setErrorMessage("Select account head.");
            }          
           /*  if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
                if ( empty($data['vendor_bank_id']) && empty($data['party_id']) && empty($data['client_id']) && 
					empty($data['executive_id'])) {
                    $messages->setErrorMessage("Please select Vendor or Client or Staff.");                
                }
            }
            if(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && 
			!empty($data['executive_id'])){
                     $messages->setErrorMessage("Either select Vendor Accounts or Vendor or Client or Staff.");
            } */
			if(!empty($data['vendor_bank_id']) && !empty($data['client_id']) && !empty($data['executive_id'])){
                     $messages->setErrorMessage("Either select Vendor Accounts or Client or Staff.");
            }
			/* if( $data['transaction_type']== 2 && !empty($data['client_id'])){
				$messages->setErrorMessage("Select Vendor or Staff as Transaction Type is Payment Out.");			
			} */
			 
			if(!empty($data['vendor_bank_id'])){
				if(empty($data['ref_bank_id']) && $data['transaction_type']!= 3){
					$messages->setErrorMessage("Select ref bank account as you had selected Bank/CAs/Directors.");
				}				
			}
			if(!empty($data['client_id']) || !empty($data['executive_id'])){
				if(!empty($data['ref_bank_id'])){
					$messages->setErrorMessage("Please un-select the ref bank account, as you selected
					Client/Staff.");  
				}
			}
            if ( !isset($data['mode_id']) || empty($data['mode_id']) ) {
                $messages->setErrorMessage("Select payment mode.");
            } 
             
            if ( empty($data['do_transaction']) ) {
                $messages->setErrorMessage("Select Bank Transaction date.");
            }
            if ( empty($data['do_voucher']) ) {
                $messages->setErrorMessage("Select Voucher Date.");
            }
            if ( !isset($data['pay_received_amt']) || empty($data['pay_received_amt']) ) {
                $messages->setErrorMessage("Payment Details amount cannot be empty.");
            }else{
					if(!is_numeric($data['pay_received_amt'])){					
						 $messages->setErrorMessage("Payment Details amount should be numeric value.");
					}
            }			
			if(!isset($data['exchange_rate']) || empty($data['exchange_rate'])){
				$messages->setErrorMessage("Exchange Rate cannot be empty.");
			}else{
				if(!is_numeric($data['exchange_rate'])){					
					 $messages->setErrorMessage("Exchange Rate should be numeric value.");
				}
				
			}
            if ( !isset($data['pay_received_amt_words']) || empty($data['pay_received_amt_words']) ) {                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{   
                if(!Validation::isAlphabets($data['pay_received_amt_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
			if ( !isset($data['pay_received_amt_inr']) || empty($data['pay_received_amt_inr']) ) {
                $messages->setErrorMessage("Payment amount in INR cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt_inr'])){					
						 $messages->setErrorMessage("Payment amount in INR should be numeric value.");
					}
			}
            if($data['transaction_type']=="1" || $data['transaction_type']=="3" ){   
                 $data['is_bill_receive']=='';

				if ( !isset($data['credit_pay_amt']) || empty($data['credit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be credited can not be empty.");
				}else{
				    if(!is_numeric($data['credit_pay_amt'])){					
						 $messages->setErrorMessage("Amount to be credited should be numeric value.");
					}				
				}
                if ( !isset($data['credit_pay_bank_id']) || empty($data['credit_pay_bank_id']) ) {
					$messages->setErrorMessage("Select Bank to which amount is credited.");
				}
			}
			
			if($data['transaction_type']=="2" || $data['transaction_type']=="3" ){   
                $data['is_receipt']='';

				if ( !isset($data['debit_pay_amt']) || empty($data['debit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be debited can not be empty.");
				}else{
					if(!is_numeric($data['debit_pay_amt'])){					
						 $messages->setErrorMessage("Amount to be debited should be numeric value.");
					}
				}
                if(empty($data['gl_type'])){
					if ( !isset($data['debit_pay_bank_id']) || empty($data['debit_pay_bank_id']) ) {
						$messages->setErrorMessage("Select Bank from which amount is debited.");
					}
				}else{
					//if gainloss type selected debit_bank_id should be 0
					$data['debit_pay_bank_id'] = 0;
				}
			}
			
			
			if($data['mode_id']!="2"){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					/*
					if(!is_numeric($data['pay_cheque_no'])){					
						$messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					}
					*/
				}
				if(empty($data['do_pay_received'])){
					$messages->setErrorMessage("Chq/DD/Tran Date should be required.");
				}
				if(empty($data['pay_bank_company'])){
					$messages->setErrorMessage("Bank/Company should be selected.");
				}
				
				
			}else{
				$data['pay_cheque_no']='';
				$data['do_pay_received']='';
				$data['pay_bank_company']='';
				$data['pay_branch']='';
			}
            if($data['is_pdc']=='1'){
                if ( !isset($data['do_pdc_chq']) || empty($data['do_pdc_chq']) ) {
                    $messages->setErrorMessage("Select PDC/Scheduled date.");
                }else{
                    //$dateArr=null;
                    //$dateArr= explode("/",$data['do_pdc_chq']);
                    //$data['do_pdc_chq'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
                    
                    // $data['do_pdc_chq'] = explode('/', $data['do_pdc_chq']);                               
                    // $data['do_pdc_chq'] = mktime(0, 0, 0, $data['do_pdc_chq'][1], 
					//$data['do_pdc_chq'][0], $data['do_pdc_chq'][2]);
                    
                }
                if ( !isset($data['pdc_chq_details']) || empty($data['pdc_chq_details']) ) {
                    $messages->setErrorMessage(" PDC details should not be empty.");
                }
            }else{
                 $data['do_pdc_chq'] = '';
                 $data['pdc_chq_details'] = '';
            }
            
            if(isset($data['is_receipt']) && $data['is_receipt']=='1'){
                if ( !isset($data['receipt_no']) || empty($data['receipt_no']) ) {
                    $messages->setErrorMessage("Invoice no. cannot be empty.");
                }
            }else{
                 $data['receipt_no'] = '';                 
            }
            
            if($data['is_bill_receive']=='1'){
                if ( !isset($data['bill_no']) || empty($data['bill_no']) ) {
                    $messages->setErrorMessage("Bill no. cannot be empty.");
                }
            }else{
                 $data['bill_no'] = '';                 
            }
             /*
			if ( !isset($data['voucher_no']) || empty($data['voucher_no']) ) {
                $messages->setErrorMessage("Voucher No. cannot be empty.");
            }*/
			 
            if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
            
            if($data['transaction_type']=="3"){   
				if ( !isset($data['credit_pay_amt']) || empty($data['debit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be debited and credited should be same.");
				}
			}
			$data['cheque_attachment'] = '';  		
            if((!empty($files['cheque_attachment']) && (!empty($files['cheque_attachment']['name'])))){
                
				$filename = $files['cheque_attachment']['name'];
                $data['cheque_attachment'] = $filename;
                $type = $files['cheque_attachment']['type'];
                 $size = $files['cheque_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment is not allowed 
					 in cheque upload");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment in cheque upload is
					greater than 300Kb');
                }
            }
			 
			$data['deposite_slip_attachment'] = '';
			 
            if((!empty($files['deposite_slip_attachment']) && 
			(!empty($files['deposite_slip_attachment']['name'])))){
                $filename = $files['deposite_slip_attachment']['name'];
                $data['deposite_slip_attachment'] = $filename;
                $type = $files['deposite_slip_attachment']['type'];
                 $size = $files['deposite_slip_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment file is not 
					 allowed in deposite slip upload");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment in deposite slip
					upload is greater than 2Mb');
                }
            }
            			
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
         /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validatePUpdate(&$data, &$account_head, $extra='') {
            foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if(empty($data['company_id'])){
                 $messages->setErrorMessage("Select Company.");
            }
            $query	="SELECT account_head_name, id FROM ".TABLE_PAYMENT_ACCOUNT_HEAD." WHERE ".TABLE_PAYMENT_ACCOUNT_HEAD.".status ='1'";
            $query	.= " ORDER BY ".TABLE_PAYMENT_ACCOUNT_HEAD.".account_head_name ASC";
            
            $db->query( $query );
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $account_head[]= processSqlData($db->result());
                }
            }            
            $list = NULL;
            
            if ( !isset($data['transaction_type']) || empty($data['transaction_type']) ) {
                $messages->setErrorMessage("Select transaction type.");
            }
            
            if ( !isset($data['account_head_id']) || empty($data['account_head_id']) ) {
                $messages->setErrorMessage("Select account head.");
            }          
            /* if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
                if (empty($data['vendor_bank_id']) && empty($data['party_id']) && empty($data['client_id']) && 
				empty($data['executive_id'])) {
                    $messages->setErrorMessage("Please select Vendor Accounts or Vendor or Client or Staff.");                
                }
            } */
			if(isset($data['transaction_type']) && $data['transaction_type']!= 3){
                if (empty($data['vendor_bank_id']) && empty($data['client_id']) && 
				empty($data['executive_id'])) {
                    $messages->setErrorMessage("Please select Vendor Accounts or Clients or Staff.");                
                }
            }
            /* if(!empty($data['vendor_bank_id']) && !empty($data['party_id']) && !empty($data['client_id']) && 
				!empty($data['executive_id'])){
                     $messages->setErrorMessage("Either select Vendor Accounts or Vendor or Client or Staff.");
            } */
			if(!empty($data['vendor_bank_id']) && !empty($data['client_id']) && 
				!empty($data['executive_id'])){
                     $messages->setErrorMessage("Either select Vendor Accounts or Clients or Staff.");
            } 
			if(!empty($data['vendor_bank_id'])){
				if(empty($data['ref_bank_id']) && $data['transaction_type']!= 3){
					$messages->setErrorMessage("Select ref bank account as you had selected Bank/CAs/Directors.");
				}				
			}
			if(!empty($data['client_id']) || !empty($data['executive_id'])){
				if(!empty($data['ref_bank_id'])){
					$messages->setErrorMessage("Please un-select the ref bank account, as you selected
					Client/Staff.");  
				}
			}
			/* if( $data['transaction_type']== 2 && !empty($data['client_id'])){
				$messages->setErrorMessage("Select Vendor Accounts or Vendor or Staff as Transaction Type is Payment Out.");			
			} */
            if ( !isset($data['mode_id']) || empty($data['mode_id']) ) {
                $messages->setErrorMessage("Select payment mode.");
            } 
                        
            if ( empty($data['do_transaction']) ) {
                $messages->setErrorMessage("Select Bank Transaction date.");
            }
            if ( empty($data['do_voucher']) ) {
                $messages->setErrorMessage("Select Voucher Date.");
            }
            if ( !isset($data['pay_received_amt']) || empty($data['pay_received_amt']) ) {
                $messages->setErrorMessage("Payment Details amount cannot be empty.");
            }else{
					if(!is_numeric($data['pay_received_amt'])){					
						 $messages->setErrorMessage("Payment Details amount should be numeric value.");
					}
            }
			if ( !isset($data['pay_received_amt_words']) || empty($data['pay_received_amt_words']) ) {                
                $messages->setErrorMessage("The Amount in words is not specified.");
                
            }else{   
                if(!Validation::isAlphabets($data['pay_received_amt_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
            }
			if(!isset($data['exchange_rate']) || empty($data['exchange_rate'])){
				$messages->setErrorMessage("Exchange Rate cannot be empty.");
			}else{
				if(!is_numeric($data['exchange_rate'])){					
					 $messages->setErrorMessage("Exchange Rate should be numeric value.");
				}
				
			}           
			if ( !isset($data['pay_received_amt_inr']) || empty($data['pay_received_amt_inr']) ) {
                $messages->setErrorMessage("Payment amount in INR cannot be empty.");
            }else{
				if(!is_numeric($data['pay_received_amt_inr'])){					
						 $messages->setErrorMessage("Payment amount in INR should be numeric value.");
				}
			}
            if($data['transaction_type']=="1" || $data['transaction_type']=="3" ){   
                 $data['is_bill_receive']=='';

				if ( !isset($data['credit_pay_amt']) || empty($data['credit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be credited can not be empty.");
				}else{
					if(!is_numeric($data['credit_pay_amt'])){					
						 $messages->setErrorMessage("Amount to be credited should be numeric value.");
					}				
				}
                if(!empty($files['deposite_slip_attachment'])){
					if ( !isset($data['credit_pay_bank_id']) || empty($data['credit_pay_bank_id']) ) {
						$messages->setErrorMessage("Select Bank to which amount is credited.");
					}
				}
			}
			
			if($data['transaction_type']=="2" || $data['transaction_type']=="3" ){   
                $data['is_receipt']='';

				if ( !isset($data['debit_pay_amt']) || empty($data['debit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be debited can not be empty.");
				}else{
					if(!is_numeric($data['debit_pay_amt'])){					
						 $messages->setErrorMessage("Amount to be debited should be numeric value.");
					}
				
				}
                if(empty($data['gl_type'])){
					if ( !isset($data['debit_pay_bank_id']) || empty($data['debit_pay_bank_id']) ) {
						$messages->setErrorMessage("Select Bank from which amount is debited.");
					}
				}else{
					//if gainloss type selected debit_bank_id should be 0
					$data['debit_pay_bank_id'] = 0;
				}
			}
			
			if($data['mode_id']!="2"){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					/*
					if(!is_numeric($data['pay_cheque_no'])){					
						$messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					}*/
				}
				if(empty($data['do_pay_received'])){
					$messages->setErrorMessage("Chq/DD/Tran Date should be required.");
				}
				if(empty($data['pay_bank_company'])){
					$messages->setErrorMessage("Bank/Company should be selected.");
				}				
			}else{
				$data['pay_cheque_no']='';
				$data['do_pay_received']='';
				$data['pay_bank_company']='';
				$data['pay_branch']='';
			}
            if($data['is_pdc']=='1'){
                if ( !isset($data['do_pdc_chq']) || empty($data['do_pdc_chq']) ) {
                    $messages->setErrorMessage("Select PDC/Scheduled date.");
                }else{
                    
                    
                }
                if ( !isset($data['pdc_chq_details']) || empty($data['pdc_chq_details']) ) {
                    $messages->setErrorMessage(" PDC details should not be empty.");
                }
            }else{
                 $data['do_pdc_chq'] = '';
                 $data['pdc_chq_details'] = '';
            }
            
            if(isset($data['is_receipt']) && $data['is_receipt']=='1'){
                if ( !isset($data['receipt_no']) || empty($data['receipt_no']) ) {
                    $messages->setErrorMessage("Invoice no. cannot be empty.");
                }
            }else{
                 $data['receipt_no'] = '';                 
            }
            
            if($data['is_bill_receive']=='1'){
                if ( !isset($data['bill_no']) || empty($data['bill_no']) ) {
                    $messages->setErrorMessage("Bill no. cannot be empty.");
                }
            }else{
                 $data['bill_no'] = '';                 
            }
             /*
			if ( !isset($data['voucher_no']) || empty($data['voucher_no']) ) {
                $messages->setErrorMessage("Voucher No. cannot be empty.");
            }*/
			 
            if ( !isset($data['remarks']) || empty($data['remarks']) ) {
                $messages->setErrorMessage("Remark cannot be empty.");
            }  
            
            if($data['transaction_type']=="3"){   
				if ( !isset($data['credit_pay_amt']) || empty($data['debit_pay_amt']) ) {
					$messages->setErrorMessage("Amount to be debited and credited should be same.");
				}
			}
			if((!empty($files['bill_attachment']) && (!empty($files['bill_attachment']['name'])))){                
				$filename = $files['bill_attachment']['name'];
              
                $type = $files['bill_attachment']['type'];
                $size = $files['bill_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
				 
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 300Kb');
                }
            }  
			$data['deposite_slip_attachment'] = '';
			 
            if((!empty($files['deposite_slip_attachment']) && (!empty($files['deposite_slip_attachment']['name'])))){
                $filename = $files['deposite_slip_attachment']['name'];
                $data['deposite_slip_attachment'] = $filename;
                $type = $files['deposite_slip_attachment']['type'];
                 $size = $files['deposite_slip_attachment']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of Attachment file is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of attachment file is greater than 2Mb');
                }
            }
            			
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
		
    }
?>
