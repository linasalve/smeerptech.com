<?php 

	class MasterAccount {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' => MasterAccount::BLOCKED,
							'ACTIVE'  => MasterAccount::ACTIVE,
							'PENDING' => MasterAccount::PENDING,
							'DELETED' => MasterAccount::DELETED
						);
			return ($status);
		}
		
        
		/**
		 *	Function to get all the Clinets.
		 *
		 * @param object   reference to the database object.
		 * @param array    array in which the list will be stored.
		 * @param mixed    array or string containing the table fields that will be retrieved.
		 * @param string   string containing the filtering condition.
         * @param mixed    string or integer representing the starting record to fetch from.
         * @param mixed    string or integer representing number of records to fetch.
		 *	
		 * @return mixed   boolean false, if no records are fetched.
         *                   integer, the number of records that are fetched.
         *
		 */
		function getList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_MASTER_ACC;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
       
        
        /**
         * This function is used to update the status of the Client.
         *
         * @param    string      unique ID of the Client whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			// Check if status is valid or not.
			if ( !in_array($status_new, MasterAccount::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid a Status.");
			}
			else {
                $user = NULL;
                if ( (MasterAccount::getList($db, $user, 'ma_id', " WHERE ma_id = '$id'")) > 0 ) {
                    $user = $user[0];
					$query = "UPDATE ". TABLE_SMS_MASTER_ACC
								." SET ma_status = '$status_new' "
								." WHERE ma_id = '$id'";
					if ( $db->query($query) && $db->affected_rows()>0 ) 
						$messages->setOkMessage("The Status has been updated.");
					else
						$messages->setErrorMessage("The Status was not updated.");
                }
                else {
                    $messages->setErrorMessage("The Account was not found.");
                }
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
	
        /**
        * This function is used to remove the Client information from the database.
        *
        * @param   string      unique ID of the Client whose record has to be removed.
        * @param   array       array containing the objects and variables that will be needed.
        *
        * @return   boolean     true, if the status was changed.
        *                       false, if the status was not changed.
        *
        */
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            $user = NULL;
            if ( (MasterAccount::getList($db, $user, 'ss_id', " WHERE ss_id = '$id'")) > 0 ) 
			{
                $user = $user[0];
                if ( $user['status'] == MasterAccount::BLOCKED || $user['status'] == MasterAccount::DELETED) 
				{
					$query = "DELETE FROM ". TABLE_SETTINGS_SERVICES
								." WHERE ss_id = '". $id ."'";
					if ( !$db->query($query) || $db->affected_rows()<=0 ) 
						$messages->setErrorMessage("The Service is not deleted.");
					else
						$messages->setOkMessage("The Service has been deleted.");
				}
				else
					$messages->setErrorMessage("Cannot delete service.");
			}
			else
				$messages->setErrorMessage("Service not found.");
		}
        
        
        /**
		 * Function to validate the data supplied for adding a new Client.
		 *
		 * @param	array	    The array containing the information to be added.
         * @param   array       array containing the objects and variables that will be needed.
		 *
         * @return  boolean     true, if the status was changed.
         *                      false, if the status was not changed.
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
        if(empty($data['userid']))
            $messages->setErrorMessage('Please enter userid.');        
        if(empty($data['username']))
            $messages->setErrorMessage('Please enter username.');
        if(empty($data['pass']))
            $messages->setErrorMessage('Please enter password.');
        if(empty($data['totalsms']))
            $messages->setErrorMessage('Please enter total no of sms purchased.');
        if($data['amount'] == "")
            $messages->setErrorMessage('Please enter amount of sms package.');
        if($data['persmsrate'] == "")
            $messages->setErrorMessage('Please enter per sms rate.');
        if(empty($data['posturl']))
            $messages->setErrorMessage('Please enter post url.');
        if (!empty($data['posturl']) && !preg_match('|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $data['posturl']))
            $messages->setErrorMessage('Invalid url.');
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

        if(empty($data['ma_userid']))
            $messages->setErrorMessage('Please enter userid.');        
        if(empty($data['ma_username']))
            $messages->setErrorMessage('Please enter username.');
        if(empty($data['ma_password']))
            $messages->setErrorMessage('Please enter password.');
        if(empty($data['ma_posturl']))
            $messages->setErrorMessage('Please enter post url.');
        if (!empty($data['ma_posturl']) && !preg_match('|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $data['ma_posturl']))
            $messages->setErrorMessage('Invalid url.');

            if ( $messages->getErrorMessageCount() <= 0 ) 
                return (true);
            else
                return (false);
		}

		function viewList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_MASTER_ACC_HISTORY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}

    }
?>