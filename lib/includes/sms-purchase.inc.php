<?php
	class SmsPurchase {
		
        const ACTIVE = 1;
		const DEACTIVE = 0;       
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_PURCHASE;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_SMS_PURCHASE;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            
            $query = "SELECT * FROM ". TABLE_SMS_PURCHASE
                        ." WHERE id = '$id'";
                        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[''] = processSqlData($db->result());
                    }
                }
            }
            
            $query = "DELETE FROM ". TABLE_SMS_PURCHASE
                        ." WHERE id = '$id'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage("The Record has been deleted.");                      
            }
            else {
                $messages->setErrorMessage("The Record was not deleted.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
           
            $sms_purchase_list  = NULL;
            $condition  = " WHERE gateway_id='".$data['gateway_id']."' AND api_id='".$data['api_id']."'";

            if ( (SmsPurchase::getList($db, $sms_purchase_list, 'id', $condition)) > 0 ) {
                foreach ( $sms_purchase_list as $purchase) {
                    $messages->setErrorMessage('This Gateway and API pair is alredy exists. Please select another.');
                }
            }
            
            if ( !isset($data['gateway_id']) || empty($data['gateway_id']) ) {
                $messages->setErrorMessage("Select Gateway.");
            }       
            if ( !isset($data['api_id']) || empty($data['api_id']) ) {
                $messages->setErrorMessage("Select Api.");
            }       
			if ( !isset($data['allotted_sms']) || empty($data['allotted_sms']) ) {
                $messages->setErrorMessage("No. of sms can not be empty.");
            }elseif (!is_numeric($data['allotted_sms']) ){
                $messages->setErrorMessage("No. of sms should be in numeric.");
            }
            if ( !isset($data['rate']) || empty($data['rate']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }elseif (!is_numeric($data['rate']) ){
                $messages->setErrorMessage("Rate should be in numeric.");
            }
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("Amount can not be empty.");
            }elseif (!is_numeric($data['amount']) ){
                $messages->setErrorMessage("Amount should be in numeric.");
            }        

			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            
			if ( !isset($data['allotted_sms']) || empty($data['allotted_sms']) ) {
                $messages->setErrorMessage("No. of sms can not be empty.");
            }elseif (!is_numeric($data['allotted_sms']) ){
                $messages->setErrorMessage("No. of sms should be in numeric.");
            }
            if ( !isset($data['rate']) || empty($data['rate']) ) {
                $messages->setErrorMessage('Email Address should be provided.');
            }elseif (!is_numeric($data['rate']) ){
                $messages->setErrorMessage("Rate should be in numeric.");
            }
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("Amount can not be empty.");
            }elseif (!is_numeric($data['amount']) ){
                $messages->setErrorMessage("Amount should be in numeric.");
            }        
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function getPurchaseHistoryLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_SMS_PURCHASE_HISTORY
                        ." LEFT JOIN ".TABLE_SMS_GATEWAY." ON ".TABLE_SMS_GATEWAY.".id= " . TABLE_SMS_PURCHASE_HISTORY.".gateway_id"
                        ." LEFT JOIN ".TABLE_SMS_API." ON ".TABLE_SMS_API.".id= " . TABLE_SMS_PURCHASE_HISTORY.".api_id"
                        ." LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id= " . TABLE_SMS_PURCHASE_HISTORY.".created_by";
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
    }
?>
