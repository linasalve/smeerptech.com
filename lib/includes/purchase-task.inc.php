<?php
	class PurchaseTask {
		
		const PENDING = 0;
		const ACTIVE = 1;
		
        const CLOSE ='Close';
		
        const DESIGNER      = 1;
		const PROGRAMMER    = 2;
		const TESTER        = 3;
		const CLIENTINFO    = 4;
        
        const BYSMS = 1;
        const BYMAIL = 2; // Day Report
        const BYUSVISIT = 3;
        const BYCLIENTVISIT = 6;
        const BYPHONEFROMCLIENT = 4;
        const BYPHONEFROMSMEERP = 5;
        
        const  SAMPLE_DESIGN  = 1;
        const  MODIFIED_DESIGN  = 2;
        const  FLASH  = 3;
        const  HEADER_IMG = 4;  
        const  SAMPLE_IMG  = 5;
        const  CONTENT     =6;
        const  OTHER     =7;
        const  BROCHURE     =8;
        
        //used while sending sms 
        const HOST           = 'api.myvaluefirst.com';
        const SERVICE_URL    = '/psms/servlet/psms.Eservice2';
        const VF_USERNAME	= 'smeerp';
		
		const VF_PASSWORD	= 'smeerp17api';
		const VF_CUST_ID		= '';
		const VF_CONTRACT_ID	= '';
		//const DISPLAY_NAME	= 'SMEERP';
		const DISPLAY_NAME	= 'Support';
		//const DISPLAY_NAME	= 'PT-SUJIT';
        
         
		function getSrsType() {
			$srsType = array('Designer' => PurchaseTask::DESIGNER,
							'Programmer'  => PurchaseTask::PROGRAMMER,
							'Tester' => PurchaseTask::TESTER,
							'Client/Information' => PurchaseTask::CLIENTINFO
						);
			return ($srsType);
		}
        
        function getFollowupType() {
			$titletype = array(PurchaseTask::BYPHONEFROMCLIENT => 'Client Called',
							   PurchaseTask::BYPHONEFROMSMEERP  => 'We Called',
							   PurchaseTask::BYUSVISIT  => 'Visit By Us',
							   PurchaseTask::BYCLIENTVISIT  => 'Visit By Client'
						);
                        //,  PurchaseTask::BYMAIL  => 'By Email'
			return ($titletype);
		} 
		
		function getFollowupTypeAll() {
			$titletype = array(PurchaseTask::BYPHONEFROMCLIENT => 'Client Called',
							   PurchaseTask::BYPHONEFROMSMEERP  => 'We Called',
							   PurchaseTask::BYUSVISIT  => 'Visit By Us',
							   PurchaseTask::BYCLIENTVISIT  => 'Visit By Client',
							   PurchaseTask::BYSMS  => 'SMS FROM US',
							   PurchaseTask::BYMAIL  => 'Day Report'
						);
                        //,  PurchaseTask::BYMAIL  => 'By Email'
			return ($titletype);
		}
        function getFileType(){
            $filetype = array( PurchaseTask::SAMPLE_DESIGN => 'Sample Design',
							   PurchaseTask::MODIFIED_DESIGN  => 'Modified Design',
							   PurchaseTask::FLASH  => 'Flash',
							   PurchaseTask::HEADER_IMG  => 'Header Images',
							   PurchaseTask::SAMPLE_IMG  => 'Sample Images',
							   PurchaseTask::CONTENT  => 'Content',
							   PurchaseTask::BROCHURE  => 'Sample Brochure',
							   PurchaseTask::OTHER  => 'Other',
						);
			return ($filetype);
        }
        function getStdMsg(){
            $msglist = array(  1 => "Please find the attched design.",
							   2  => "We done the required changes in design. ",
							   3  => "sample text"
						);
			return ($msglist);
        }
        
		function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
        	$count = $total=0;
            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
               $query .= implode(",", $required_fields);               
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;                
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
          
            $query .= " FROM ". TABLE_PURCHASE_TASK_DETAILS
                        ." LEFT JOIN ". TABLE_PROJECT_TASK_MODULE
                		." ON ". TABLE_PURCHASE_TASK_DETAILS .".module_id = ". TABLE_PROJECT_TASK_MODULE .".id "
                		." LEFT JOIN ". TABLE_PROJECT_TASK_STATUS
                		." ON ". TABLE_PURCHASE_TASK_DETAILS .".status_id = ". TABLE_PROJECT_TASK_STATUS .".id "
                		." LEFT JOIN ". TABLE_PROJECT_TASK_PRIORITY
                		." ON ". TABLE_PURCHASE_TASK_DETAILS .".priority_id = ". TABLE_PROJECT_TASK_PRIORITY .".id "
                        ." LEFT JOIN ". TABLE_PROJECT_TASK_TYPE
                		." ON ". TABLE_PURCHASE_TASK_DETAILS .".type_id = ". TABLE_PROJECT_TASK_TYPE .".id "
                        ." LEFT JOIN ". TABLE_PROJECT_TASK_RESOLUTION
                		." ON ". TABLE_PURCHASE_TASK_DETAILS .".resolution_id = ". TABLE_PROJECT_TASK_RESOLUTION .".id ";
                        
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
              $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
               return ( $total );
            }
            else {
                return false;
            }   
        }
		
		 /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            //
           
            if ( empty($data["or_id"])   ) {
				$messages->setErrorMessage("Please Select Project / Order .");
			}
			if ( $data["module_id"] == "-2"  ) {
				$messages->setErrorMessage("The Task Module cannot be empty.");
			}	
			if ( $data["sub_module_id"] == "-2"  ) {
				$messages->setErrorMessage("The Task Sub module cannot be empty.");
			}
           	if ( $data["resolution_id"] == "-1"  ) {
				$messages->setErrorMessage("The Task resolution cannot be empty.");
			}
			if ( $data["priority_id"] == "-1"  ) {
				$messages->setErrorMessage("The Task Priority cannot be empty.");
			}
			if ( $data["status_id"] == "-1"  ) {
				$messages->setErrorMessage("The Task Status cannot be empty.");
			}
            
            if ( $data["title"] == ""  ) {
				$messages->setErrorMessage("The Task Title cannot be empty.");
			}
            
			if ( $data["details"] == "" ) {
				
                $messages->setErrorMessage("The Task Details cannot be empty.");
			}
           
          
            $fileSize = 0;
            if(!empty($files['attached_file'])){
                if(!empty( $files['attached_file']["name"])){                
                    if ( !in_array($files['attached_file']["type"], $data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    }
                    $fileSize += $files['attached_file']["size"] ;
                }
            }
         
            if($fileSize > $data['max_file_size']){
                 $messages->setErrorMessage('File size is greater than 2Mb');
            }
            /*
            if(!empty($files['attached_file']["name"])){
                if(empty($data['attached_desc'])){
                     $messages->setErrorMessage('File description Should not be Empty');
                }                 
            }
            */
            if(!empty($data['attached_desc'])){
            
                  if(empty($files['attached_file']["name"])){
                  
                        $messages->setErrorMessage('Please upload file as File Description is not empty.');
                  }
            }
            
            if(empty($data["hrs"]) && empty($data["min"])){
            
                $messages->setErrorMessage("Please select Estimated Hrs.");
            }
            if(isset($data['mail_client']) &&  !isset($data['is_visibleto_client'])){
                 $messages->setErrorMessage("As you are sending Notification To client, So Visible to Client Option should be selected.");
            }
            
             if(!isset($data['mail_client']) &&  isset($data['is_visibleto_client'])){
                 $messages->setErrorMessage("As Task will be visible to Client, So Send Notification to Client.");
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
        function validateCommentAdd(&$posted_data,$extra='') {
            foreach ($extra as $key=>$value) {
				$$key = $value;
			}  
       
            if ( empty($posted_data["or_id"])   ) {
				$messages->setErrorMessage("Please Select Project / Order .");
			}
            if ( $posted_data["details"] == "" ) {
				 $messages->setErrorMessage("The Task Details cannot be empty.");
			}	
           
            // print_R($posted_data);
            if(empty($posted_data["hrs"]) && empty($posted_data["min"])){
            
                $messages->setErrorMessage("Please select Time sheet Hrs.");
            }
            $fileSize = 0;
			
            if(!empty($files['attached_file'])){
                if(!empty( $files['attached_file']["name"])){                
                    if ( !in_array($files['attached_file']["type"], $posted_data["allowed_file_types"] ) ) {
                        $msg = $files['attached_file']["type"]." is not allowed ";
                        $messages->setErrorMessage($msg);
                    }  
                    $fileSize += $files['attached_file']["size"] ;
                    if(empty($posted_data['file_type'])){                     
                        $messages->setErrorMessage('Choose Type of File Upload');
                    }
                }
            }
            
            if($fileSize > $posted_data['max_file_size']){
                 $messages->setErrorMessage('Size of all files is greater than 1Mb');
            }
            /*
            if(!empty($files['attached_file']["name"])){
                if(empty($posted_data['attached_desc'])){
                     $messages->setErrorMessage('File description Should not be Empty as you are uploading the file.');
                }
            }*/
            if(!empty($posted_data['attached_desc'])){
                if(empty($files['attached_file']["name"])){
                        $messages->setErrorMessage('Please upload file as File Description is not empty.');
                }
            }
            
            if(isset($posted_data['mail_client']) &&  !isset($posted_data['is_visibleto_client'])){
                 $messages->setErrorMessage("As you are sending Notification To client, So Visible to Client Option should be selected.");
            }
            
            if(!isset($posted_data['mail_client']) &&  isset($posted_data['is_visibleto_client'])){
                 $messages->setErrorMessage("As Task will be visible to Client, So Send Notification to Client.");
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        
        function validateCommentAddQuick(&$posted_data,$extra='') {
            foreach ($extra as $key=>$value) {
				$$key = $value;
			}  
       
            if ( empty($posted_data["or_id"])   ) {
				$messages->setErrorMessage("Please Select Project / Order .");
			}
            if ( empty($posted_data["task_id"])   ) {
				$messages->setErrorMessage("Please Select Task .");
			}
            if ( $posted_data["details"] == ""  ) {
				$messages->setErrorMessage("The Task Comments cannot be empty.");
			}	
             if(empty($posted_data["hrs"]) && empty($posted_data["min"])){
            
                $messages->setErrorMessage("Please select Time sheet Hrs.");
            }
           
            $fileSize = 0;
            if(!empty($files['attached_file'])){
                if(!empty( $files['attached_file']["name"])){
                
                        if ( !in_array($files['attached_file']["type"], $posted_data["allowed_file_types"] ) ) {
                            $msg = $files['attached_file']["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $files['attached_file']["size"] ;
                       
                 }
            }
          
            if($fileSize > $posted_data['max_file_size']){
                 $messages->setErrorMessage('Size of all files is greater than 2Mb');
            }
            if(!empty($files['attached_file']["name"])){
                if(empty($posted_data['attached_desc'])){
                     $messages->setErrorMessage('File description Should not be Empty as you are uploading the file.');
                }
            }
            if(!empty($posted_data['attached_desc'])){
            
                  if(empty($files['attached_file']["name"])){
                  
                        $messages->setErrorMessage('Please upload file as File Description is not empty.');
                  }
            }
            
            if(isset($posted_data['mail_client']) &&  !isset($posted_data['is_visibleto_client'])){
                 $messages->setErrorMessage("As you are sending Notification To client, So Visible to Client Option should be selected.");
            }
            
            if(!isset($posted_data['mail_client']) &&  isset($posted_data['is_visibleto_client'])){
                 $messages->setErrorMessage("As Task will be visible to Client, So Send Notification to Client.");
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateBugAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            //
           
            if ( empty($data["task_id"])   ) {
				$messages->setErrorMessage("Please Select task .");
			}
            
            if ( empty($data["or_id"])   ) {
				$messages->setErrorMessage("Please Select Project / Order .");
			}
            
			if ( $data["module_id"] == "-2"  ) {
				$messages->setErrorMessage("The Module cannot be empty.");
			}	
			if ( $data["sub_module_id"] == "-2"  ) {
				$messages->setErrorMessage("The Sub module cannot be empty.");
			}
           
			if ( $data["resolution_id"] == "-1"  ) {
				$messages->setErrorMessage("The Resolution cannot be empty.");
			}
			if ( $data["priority_id"] == "-1"  ) {
				$messages->setErrorMessage("The Priority cannot be empty.");
			}	
			if ( $data["status_id"] == "-1"  ) {
				$messages->setErrorMessage("The Task Status cannot be empty.");
			}
            
            if ( $data["title"] == ""  ) {
				$messages->setErrorMessage("The Bug Title cannot be empty.");
			}
			if ( $data["details"] == ""  ) {
				$messages->setErrorMessage("The Bug Details cannot be empty.");
			}	
            
                       
            $fileSize = 0;
            if(!empty($files['attached_file'])){
                if(!empty( $files['attached_file']["name"])){
                
                        if ( !in_array($files['attached_file']["type"], $data["allowed_file_types"] ) ) {
                            $msg = $files['attached_file']["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $files['attached_file']["size"] ;
                       
                 }
            }
           
            if($fileSize > $data['max_file_size']){
                 $messages->setErrorMessage('File size is greater than 1Mb');
            }
            /*
            if(!empty($files['attached_file']["name"])){
                if(empty($data['attached_desc'])){
                     $messages->setErrorMessage('File description Should not be Empty');
                }
            }
            */
            if(!empty($data['attached_desc'])){
                if(empty($files['attached_file']["name"])){
                    $messages->setErrorMessage('Please upload file as File Description is not empty.');
                }
            }
            
            if(empty($data["hrs"]) && empty($data["min"])){
            
                $messages->setErrorMessage("Please select Timesheet Hrs.");
            }
            if(isset($data['mail_client']) &&  !isset($data['is_visibleto_client'])){
                 $messages->setErrorMessage("As you are sending Notification To client, So Visible to Client Option should be selected.");
            }
            
             if(!isset($data['mail_client']) &&  isset($data['is_visibleto_client'])){
                 $messages->setErrorMessage("As Bug will be visible to Client, So Send Notification to Client.");
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
         function validateBugCommentAdd(&$posted_data,$extra='') {
            foreach ($extra as $key=>$value) {
				$$key = $value;
			}  
            if ( empty($posted_data["task_id"])   ) {
				$messages->setErrorMessage("Please Select task .");
			}	
            if ( empty($posted_data["or_id"])   ) {
				$messages->setErrorMessage("Please Select Project / Order .");
			}
            if ( $posted_data["details"] == ""  ) {
				$messages->setErrorMessage("The Bug Comments cannot be empty.");
			}	
             if(empty($posted_data["hrs"]) && empty($posted_data["min"])){
            
                $messages->setErrorMessage("Please select Time sheet Hrs.");
            }
           
            $fileSize = 0;
            if(!empty($files['attached_file'])){
                if(!empty( $files['attached_file']["name"])){
                
                        if ( !in_array($files['attached_file']["type"], $posted_data["allowed_file_types"] ) ) {
                            $msg = $files['attached_file']["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $files['attached_file']["size"] ;
                       
                 }
            }
          
            if($fileSize > $posted_data['max_file_size']){
                 $messages->setErrorMessage('Size of all files is greater than 2Mb');
            }
            /*
            if(!empty($files['attached_file']["name"])){
                if(empty($posted_data['attached_desc'])){
                     $messages->setErrorMessage('File description Should not be Empty as you are uploading the file.');
                }
            }*/
            
            if(!empty($posted_data['attached_desc'])){
            
                  if(empty($files['attached_file']["name"])){
                  
                        $messages->setErrorMessage('Please upload file as File Description is not empty.');
                  }
            }
            
            if(isset($posted_data['mail_client']) &&  !isset($posted_data['is_visibleto_client'])){
                 $messages->setErrorMessage("As you are sending Notification To client, So Visible to Client Option should be selected.");
            }
            
            if(!isset($posted_data['mail_client']) &&  isset($posted_data['is_visibleto_client'])){
                 $messages->setErrorMessage("As Bug will be visible to Client, So Send Notification to Client.");
            }
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        
		function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;            
				$query = "DELETE FROM ". TABLE_PURCHASE_TASK_DETAILS
							." WHERE id = '$id'";
												
				if ( ( $db->query($query) || $db->query($query1) ) && ( $db->affected_rows()>0 ) ) {
					$messages->setOkMessage("The Record has been deleted.");                      
				}
				else {
					$messages->setErrorMessage("The Record was not deleted.");
				}
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
		 	
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$count = $total=0;
			$query = "SELECT DISTINCT ";
			
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PURCHASE_TASK_DETAILS
			." LEFT JOIN ". TABLE_PROJECT_TASK_MODULE
			." ON ". TABLE_PURCHASE_TASK_DETAILS .".module_id = ". TABLE_PROJECT_TASK_MODULE .".id "
			." LEFT JOIN ". TABLE_PROJECT_TASK_STATUS
			." ON ". TABLE_PURCHASE_TASK_DETAILS .".status_id = ". TABLE_PROJECT_TASK_STATUS .".id "
			." LEFT JOIN ". TABLE_PROJECT_TASK_PRIORITY
			." ON ". TABLE_PURCHASE_TASK_DETAILS .".priority_id = ". TABLE_PROJECT_TASK_PRIORITY .".id "
			." LEFT JOIN ". TABLE_PROJECT_TASK_TYPE
			." ON ". TABLE_PURCHASE_TASK_DETAILS .".type_id = ". TABLE_PROJECT_TASK_TYPE .".id "
			." LEFT JOIN ". TABLE_PROJECT_TASK_RESOLUTION
			." ON ". TABLE_PURCHASE_TASK_DETAILS .".resolution_id = ". TABLE_PROJECT_TASK_RESOLUTION .".id ";
                         
			//$query .= " FROM ". TABLE_PURCHASE_TASK_DETAILS  ;
              
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		   
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
               
	    
        function getSRS(&$db, &$srs,$required_fields,  $condition='', $from='', $rpp='') {
        
            $count = $total=0;

            $query = "SELECT DISTINCT ";
            
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
            
            $query .= " FROM ". TABLE_WORK_SRS ;
            
           	$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
                        
            if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$srs[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
        }
        
        
        /**
         * This function is used to make the entries for the SRS against the Order. 
         *
         * @param	order_id	bigint	the unique ID of the Order record whose SRS are to be retrieved.
         * @param	srs			array	reference to the array in which the information will be stored.
         * @param	extra		mixed	mixed array having the entities required in this function.
         *								db = &$db				reference to the database class.
         *								messages = $messages	reference to the messages class.
         *							
         *
         */
        function validateSRS($data, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            
            if ( !isset($data['srs_type']) || empty($data['srs_type']) ) {
                $messages->setErrorMessage("Please select SRS type.");
            }
            
            if ( !isset($data['srs_text']) || empty($data['srs_text']) ) {
                $messages->setErrorMessage("Please enter the text for the SRS.");
            }
            
            /*if ( $messages->getErrorMessageCount() <= 0 ) {
                $query = "INSERT INTO ". TABLE_WORK_SRS
                            ." SET "
                                ." order_id = '". $data['order_id'] ."'"
                                .",added_by = '". $my['user_id'] ."'"
                                .",srs_type = '". $data['srs_type'] ."'"
                                .",srs_text = '". $data['srs_text'] ."'"
                                .",ip       = '". $_SERVER['REMOTE_ADDR'] ."'"
                                .",do_a 	= '". date('Y-m-d H:i:s', time()) ."'" ;
                if ( $db->query($query) && ($db->affected_rows() > 0) ) {
                    $messages->setOkMessage("SRS was saved.");
                }
                else {
                    $messages->setErrorMessage("SRS was not saved, please try again later.");
                }
            }*/
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }		
        }
        
        function validateFollowupAdd(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            if($data['submitType']==1){
                //Validation for add followup
                if ( !isset($data['comment']) || empty($data['comment']) ) {
                    $messages->setErrorMessage('Comment should be provided.');
                }
                if ( !isset($data['type']) || empty($data['type']) ) {
                    $messages->setErrorMessage('Type should be provided.');
                }
                if ( !isset($data['client_contact_person']) || empty($data['client_contact_person']) ) {
                    $messages->setErrorMessage('Contact Person should be provided.');
                }
                if($data['type']==PurchaseTask::BYPHONEFROMCLIENT || $data['type']==PurchaseTask::BYPHONEFROMSMEERP){
                    if ( !isset($data['client_phone']) || empty($data['client_phone']) ) {
                        $messages->setErrorMessage('Contact No. should be provided.');
                    }
                }
                if ( !isset($data['department_id']) || empty($data['department_id']) ) {
                    $messages->setErrorMessage('Department should be provided.');
                }
            }
            
            if($data['submitType']==2){
                //Validation for send sms
            
                if ( (!isset($data['comment']) || empty($data['comment']) )  ) {
                    $messages->setErrorMessage('Provide Message.');
                }
                /*
                if($data['client_mobile_exist']==0 && empty($data['client_mobile'])){
                    $messages->setErrorMessage('Either Provide Mobile No or set mobile no. in client account.');
                }*/
                if(!isset($data['mail_client']) && !isset($data['mail_to_all_su']) && !isset($data['mail_to_su'])){
                      $messages->setErrorMessage('Please mark check box of Send SMS.');
                    
                }
                /*
                if(!empty($data['client_mobile'])){
                    if(!is_numeric($data['client_mobile'])){
                        $messages->setErrorMessage('Mobile no should be number.');
                    }
                }*/
            }
            
            if($data['submitType']==3){
                //Validation for send mail
            
                /*if ( (!isset($data['comment']) || empty($data['comment']) ) && !isset($data['send_default_mail']) ) {
                    $messages->setErrorMessage('Either Provide Email Message or mark send default mail.');
                }*/
                
                if ( (!isset($data['comment']) || empty($data['comment']) )) {
                    $messages->setErrorMessage('Email Message should be provided.');
                }
                if ( (!isset($data['email_subject']) || empty($data['email_subject']) ) ) {
                    $messages->setErrorMessage('Subject cannot be empty.');
                }
                
                $fileSize = 0;
                if(!empty($files['attached_file'])){
                    if(!empty( $files['attached_file']["name"])){
                    
                            if ( !in_array($files['attached_file']["type"], $data["allowed_file_types"] ) ) {
                                $msg = $files['attached_file']["type"]." is not allowed ";
                                $messages->setErrorMessage($msg);
                            }
                            $fileSize += $files['attached_file']["size"] ;
                           
                     }
                }
              
                if($fileSize > $data['max_file_size']){
                     $messages->setErrorMessage('File size is greater than 2Mb');
                }
                
                if ( !empty($data['client_email']) ) {
                    if ( !isValidEmail($data['client_email']) ) {
                        $messages->setErrorMessage('The Primary Email Address is not valid.');
                    }
                    if ( (!isset($data['client_contact_person']) || empty($data['client_contact_person']) ) ) {
                        $messages->setErrorMessage('Name should be provided.');
                    }
                }
                if ( (!empty($data['client_contact_person']) ) ) {
                    if ( (!isset($data['client_email']) || empty($data['client_email']) ) ) {
                        $messages->setErrorMessage('Email-id should be provided.');
                    }
                }
                if ( !isset($data['department_id']) || empty($data['department_id']) ) {
                    $messages->setErrorMessage('Department should be provided.');
                }
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        //Used in communication log
	    function getDetailsCommLog( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PROJECT_TASK_FOLLOWUP ;
            /*$query .= " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ".TABLE_PROJECT_TASK_FOLLOWUP.".created_by ";*/
			$query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
            
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                return ( $total );
            }
            else {
                return false;
            }   
        }
        
        //Code to send sms through followup 
        function sendSms($message,$mobileno,$display_name=''){
            $temp_response1 = '<?xml version="1.0" encoding="ISO-8859-1"?><MESSAGEACK><GUID GUID="297b6ef1-84fc-4e2f-a638-f3c19af48125" SUBMITDATE="2007-7-25 14:57:49" ID="1"></GUID></MESSAGEACK>';
            $temp_response2 = '<?xml version="1.0" encoding="ISO-8859-1"?><MESSAGEACK><GUID GUID="d99c3add-6cad-4a23-a926-3249cf621fd9" SUBMITDATE="2007-7-25 16:32:56" ID="1"></GUID></MESSAGEACK>';
            $host = PurchaseTask::HOST;
            $service_url = PurchaseTask::SERVICE_URL ;
            $VF_USERNAME =PurchaseTask::VF_USERNAME ;
            $VF_PASSWORD =PurchaseTask::VF_PASSWORD ;
            $display_name= PurchaseTask::DISPLAY_NAME;
            if(empty($display_name)){
                $display_name= PurchaseTask::DISPLAY_NAME;
            }
            $message .= "."."\n"."Regards."."\n"."Team SMEERP" ;
            $data = '<?xml version="1.0" encoding="ISO-8859-1"?>'
                        .'<!DOCTYPE MESSAGE SYSTEM "http://127.0.0.1/psms/dtd/messagev12.dtd" >'
                        .'<MESSAGE VER="1.2">'
                        .'<USER USERNAME="'. $VF_USERNAME .'" PASSWORD="'. $VF_PASSWORD .'"/>'
                        .'<SMS UDH="0" CODING="1" TEXT="'. $message .'" PROPERTY="0" ID="1">'
                        .'<ADDRESS FROM="'. $display_name. '" TO="'. $mobileno .'" SEQ="1" TAG="some clientside random data" />'
                        .'</SMS>'
                        .'</MESSAGE>';
    
            $data 	= 'data='. ($data);
            $action = 'action=send';
            
            $response = PurchaseTask::httpSmsConnection($host, $service_url, "post", $data.'&'.$action);
            return $response;
        
        }
        
        function httpSmsConnection($host, $service_url, $method, $data) {
            $response = '';
            $fp = fsockopen("$host", "80", $errno, $errstr, $timeout = 30);
    
            if (!$fp){
                //error tell us
                return ("$errstr ($errno)\n");
            }
            else {
                //send the server request
                fputs($fp, "POST $service_url HTTP/1.1\r\n");
                fputs($fp, "Host: $host\r\n");
                fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
                fputs($fp, "Content-length: ".strlen($data)."\r\n");
                fputs($fp, "Connection: close\r\n\r\n");
                fputs($fp, $data . "\r\n\r\n");
        
                //loop through the response from the server
                while(!feof($fp)) {
                    //fgets($fp, 4096);
                    $response .= fgets($fp, 4096);
                }
                //close fp - we are done with it
                fclose($fp);
            }
            return ($response);
	    }


    
    }
    
   
?>
