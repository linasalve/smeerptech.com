<?php
	class HrSalary {
		
		const DEACTIVE = 0;
		const ACTIVE  = 1;      
        
        function getStatus() {
			$status = array(
							'Paid'   => HrSalary::ACTIVE,
                            'Not Paid'    => HrSalary::DEACTIVE
						);
			return ($status);
		}   
        
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_HR_SALARY;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_HR_SALARY;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_HR_SALARY
                                ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
           
            if ( !isset($data['user_id']) || empty($data['user_id']) ) {
                $messages->setErrorMessage("User can not be empty.");
            }
            
            /*  
			if ( !isset($data['calendar_id']) || empty($data['calendar_id']) ) {
                $messages->setErrorMessage("Calendar can not be empty.");
            }elseif (!is_numeric($data['calendar_id']) ){
                $messages->setErrorMessage("Calendar should be in numeric.");
            }  
			*/
			if(empty($data['from_date']) || empty($data['to_date'])){
				$messages->setErrorMessage("From date and to date should be selected");
			}else{
				$data['from_date'] = explode('/', $data['from_date']);	
                $data['from_date'] = mktime(0, 0, 0, $data['from_date'][1], $data['from_date'][0], $data['from_date'][2]);
				 $data['to_date'] = explode('/', $data['to_date']);	
                $data['to_date'] = mktime(0, 0, 0, $data['to_date'][1], $data['to_date'][0], $data['to_date'][2]);
			}
            
			
			if ((!empty($data['total_days']) && !is_numeric($data['total_days'])) || $data['total_days']==0 ){
			    $messages->setErrorMessage("Enter Total Days of the Month.");
		    }
			if( (!empty($data['working_days']) && !is_numeric($data['working_days']) ) || $data['working_days']==0){
			    $messages->setErrorMessage("Enter No of Working Days");
		    }
         	if (!empty($data['present_days']) && !is_numeric($data['present_days']) ){
			    $messages->setErrorMessage("Enter No of Days Present.");
		    }
                       
					   
            if ( !isset($data['promised_payment']) || empty($data['promised_payment']) ) {
                $messages->setErrorMessage("Promised Basic Pay can not be empty.");
            }elseif (!is_numeric($data['promised_payment']) ){
                $messages->setErrorMessage("Promised Basic Pay should be in numeric.");
            }
            
            if ( !isset($data['earned_payment']) || empty($data['earned_payment']) ) {
                $messages->setErrorMessage("Payable Salary can not be empty.");
            }elseif (!is_numeric($data['earned_payment']) ){
                $messages->setErrorMessage("Payable Salary should be in numeric.");
            }
             
            /* if (!empty($data['cafeteria_deduction']) && !is_numeric($data['cafeteria_deduction']) ){
                $messages->setErrorMessage("Cafeteria Deduction should be in numeric.");
            } 
		   
		    if (!empty($data['phone_deduction']) && !is_numeric($data['phone_deduction']) ){
                $messages->setErrorMessage("Phone Deduction should be in numeric.");
            }
            
		    if (!empty($data['pt_deduction']) && !is_numeric($data['pt_deduction'])){
			    $messages->setErrorMessage("PT Deduction should be in numeric.");
		    } 
		    
		    if (!empty($data['pf_deduction']) && !is_numeric($data['pf_deduction']) ){
    			$messages->setErrorMessage("PF Deduction should be in numeric.");
		    }
		    
		    if (!empty($data['esi_deduction']) && !is_numeric($data['esi_deduction']) ){
			    $messages->setErrorMessage("ESI Deduction should be in numeric.");
		    } 
			
		    if (!empty($data['cafeteria_deduction']) && !is_numeric($data['uniform_deposite']) ){
			    $messages->setErrorMessage("Uniform Deposite should be in numeric.");
		    } 
			 */
		    
			
            if ( !isset($data['total_earned_payment']) || empty($data['total_earned_payment']) ) {
                $messages->setErrorMessage("Total Earned Payment can not be empty.");
            }elseif (!is_numeric($data['total_earned_payment']) ){
                $messages->setErrorMessage("Total Earned Payment should be in numeric.");
            }
            
          /*  if (!empty($data['paid_salary'])) {
			   if (!is_numeric($data['paid_salary']) ){
					$messages->setErrorMessage("Paid Salary should be in numeric.");
				} 
			} */
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
            
           if ( !isset($data['user_id']) || empty($data['user_id']) ) {
                $messages->setErrorMessage("User can not be empty.");
            }
            
            /*  
			if ( !isset($data['calendar_id']) || empty($data['calendar_id']) ) {
                $messages->setErrorMessage("Calendar can not be empty.");
            }elseif (!is_numeric($data['calendar_id']) ){
                $messages->setErrorMessage("Calendar should be in numeric.");
            }  
			*/
			if(empty($data['from_date']) || empty($data['to_date'])){
				$messages->setErrorMessage("From date and to date should be selected");
			}else{
				 $data['from_date'] = explode('/', $data['from_date']);	
                $data['from_date'] = mktime(0, 0, 0, $data['from_date'][1], $data['from_date'][0], $data['from_date'][2]);
				 $data['to_date'] = explode('/', $data['to_date']);	
                $data['to_date'] = mktime(0, 0, 0, $data['to_date'][1], $data['to_date'][0], $data['to_date'][2]);
			}
            
			
			
			if ((!empty($data['total_days']) && !is_numeric($data['total_days'])) || $data['total_days']==0 ){
			    $messages->setErrorMessage("Enter Total Days of the Month.");
		    }
			if( (!empty($data['working_days']) && !is_numeric($data['working_days']) ) || $data['working_days']==0){
			    $messages->setErrorMessage("Enter No of Working Days");
		    }
         	if (!empty($data['present_days']) && !is_numeric($data['present_days']) ){
			    $messages->setErrorMessage("Present Days should be in numeric.");
		    }
                       
					   
            if ( !isset($data['promised_payment']) || empty($data['promised_payment']) ) {
                $messages->setErrorMessage("Promised Basic Pay can not be empty.");
            }elseif (!is_numeric($data['promised_payment']) ){
                $messages->setErrorMessage("Promised Basic Pay should be in numeric.");
            }
            
            if ( !isset($data['earned_payment']) || empty($data['earned_payment']) ) {
                $messages->setErrorMessage("Earned Payment can not be empty.");
            }elseif (!is_numeric($data['earned_payment']) ){
                $messages->setErrorMessage("Earned Payment should be in numeric.");
            }
            
            /* if (!empty($data['cafeteria_deduction']) && !is_numeric($data['cafeteria_deduction']) ){
                $messages->setErrorMessage("Cafeteria Deduction should be in numeric.");
            } 
		   
		    if (!empty($data['phone_deduction']) && !is_numeric($data['phone_deduction']) ){
                $messages->setErrorMessage("Phone Deduction should be in numeric.");
            }
            
		    if (!empty($data['pt_deduction']) && !is_numeric($data['pt_deduction'])){
			    $messages->setErrorMessage("PT Deduction should be in numeric.");
		    } 
		    
		    if (!empty($data['pf_deduction']) && !is_numeric($data['pf_deduction']) ){
    			$messages->setErrorMessage("PF Deduction should be in numeric.");
		    }
		    
		    if (!empty($data['esi_deduction']) && !is_numeric($data['esi_deduction']) ){
			    $messages->setErrorMessage("ESI Deduction should be in numeric.");
		    } 
			
		    if (!empty($data['cafeteria_deduction']) && !is_numeric($data['uniform_deposite']) ){
			    $messages->setErrorMessage("Uniform Deposite should be in numeric.");
		    } 
			
		    if (!empty($data['adv']) && !is_numeric($data['adv']) ){
			    $messages->setErrorMessage("AVD Deduction should be in numeric.");
		    } 
			 */
			if (!empty($data['worked_hrs']) && !is_numeric($data['worked_hrs']) ){
			    $messages->setErrorMessage("Enter total worked hours.");
		    } 
            if ( !isset($data['total_earned_payment']) || empty($data['total_earned_payment']) ) {
                $messages->setErrorMessage("Total Earned Payment can not be empty.");
            }elseif (!is_numeric($data['total_earned_payment']) ){
                $messages->setErrorMessage("Total Earned Payment should be in numeric.");
            }
            
           /*  if (!empty($data['paid_salary'])) {
			   if (!is_numeric($data['paid_salary']) ){
					$messages->setErrorMessage("Paid Salary should be in numeric.");
				} 
			} */
                 
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_HR_SALARY
                            ." SET ". TABLE_HR_SALARY .".status = '".HrSalary::ACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Hr Salary has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Hr Salary has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_HR_SALARY
                            ." SET ". TABLE_HR_SALARY .".status = '".HrSalary::DEACTIVE."'"
                            ." WHERE id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Hr Salary has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Hr Salary has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    
        
		
    }
?>
