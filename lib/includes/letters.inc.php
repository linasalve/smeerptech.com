<?php
	class Letters {
		
		const PENDING = 0;
		const COMPLETED  = 1;     
		const CANCELLED  = 2;     
		
        
		    
		const EMAILDOC  = 1; 
		const PRINTDOC  = 2; 
		
		const PET  = 1;   
		const PT  = 2;   
		const JCM  = 3;   
		const PJM  = 4;   
		const SJM  = 5;   
		const MJM  = 6;   
		const MALVIYANAGAR = 7; 
		const SWAMICOLONY  = 8; 
		const CHAKRADHARNGR= 9; 
		const DHAWALSARI= 10; 
		const GORWHA= 11; 
		
		
		
		
        function getStatus() {
			$status = array(
							'PENDING'   => Letters::PENDING,
                            'COMPLETED'    => Letters::COMPLETED,
                            'CANCELLED'    => Letters::CANCELLED
						);
			return ($status);
		}   
         
         
		/**
		 *	Function to get all the Quotation Subjects.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_LETTERS;
                    
            $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
       
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
               $query .= " COUNT(*) as count";
				$count = 1;
            }
            
           $query .= " FROM ". TABLE_LETTERS ;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
           
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
			    
            if ( (Letters::getList($db, $details, 
				TABLE_LETTERS.'.id,'.TABLE_LETTERS.'.file_1,'.TABLE_LETTERS.'.file_2,'
				.TABLE_LETTERS.'.file_3,'.TABLE_LETTERS.'.file_4,'.TABLE_LETTERS.'.file_5'
				, " WHERE ".TABLE_LETTERS.".id = '$id'")) > 0 ) {
                $details = $details[0];
				$query = "DELETE FROM ". TABLE_LETTERS
							." WHERE id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage("The Record has been deleted.");    
					if(!empty($details['file_1'])){
						@unlink(DIR_FS_LETTERS_FILES."/".$details['file_1']);
					}
					if(!empty($details['file_2'])){
						@unlink(DIR_FS_LETTERS_FILES."/".$details['file_2']);
					}
					if(!empty($details['file_3'])){
						@unlink(DIR_FS_LETTERS_FILES."/".$details['file_3']);
					}
					if(!empty($details['file_4'])){
						@unlink(DIR_FS_LETTERS_FILES."/".$details['file_4']);
					}
					if(!empty($details['file_5'])){
						@unlink(DIR_FS_LETTERS_FILES."/".$details['file_5']);
					}
				}
				else {
					$messages->setErrorMessage("The Record was not deleted.");
				}
            }else{
				$messages->setErrorMessage("The Record was not found.");			
			}  
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            $files = $_FILES;
			if(empty($data['company_id'])){                
                $messages->setErrorMessage("Please Select Company.");                 
            }
			if(empty($data['do_l'])){                
                $messages->setErrorMessage("Please Select Date of Letter.");                 
            }else{
				$data['do_l'] = explode('/', $data['do_l']);
                $data['do_l'] = mktime(0, 0, 0, $data['do_l'][1], $data['do_l'][0], $data['do_l'][2]);
			}
			$data['number'] = $data['counter'] = '';
			if(!empty($data['company_id']) && !empty($data['do_l'])){				
				$detailNo = getLetterNumber($data['do_l'],$data['company_id']); 				
				if(!empty($detailNo)){
					$data['number'] = $detailNo['number'];
					$data['counter'] = $detailNo['counter'];
				}
			}
			if(empty($data['number'])){
				$messages->setErrorMessage("Please Select Date of Letter and Company.");                 
			}
            if(empty($data['filename'])){                
                 $messages->setErrorMessage("Please Enter the Filename.");                 
            }
			if(empty($data['subject'])){                
                 $messages->setErrorMessage("Please Enter the Subject.");                 
            }
		    if(empty($data['text'])){                
                 $messages->setErrorMessage("Please Enter the Body.");                 
            }
			$data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){
                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 1 is greater than 1Mb');
                }
            }
            $data['file_2'] = '';           
            if((!empty($files['file_2']['name']))){
                
                $filename = $files['file_2']['name'];
                $data['file_2'] = $filename;
                $type = $files['file_2']['type'];
                $size = $files['file_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 2 is greater than 1Mb');
                }
            }
			$data['file_3'] = '';           
            if((!empty($files['file_3']['name']))){
                
                $filename = $files['file_3']['name'];
                $data['file_3'] = $filename;
                $type = $files['file_3']['type'];
                $size = $files['file_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 3 is greater than 1Mb');
                }
            }
            $data['file_4'] = '';           
            if((!empty($files['file_4']['name']))){
                
                $filename = $files['file_4']['name'];
                $data['file_4'] = $filename;
                $type = $files['file_4']['type'];
                $size = $files['file_4']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 4 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 4 is greater than 1Mb');
                }
            }
            $data['file_5'] = '';           
            if((!empty( $files['file_5']['name'] ))){
                
                $filename = $files['file_5']['name'];
                $data['file_5'] = $filename;
                $type = $files['file_5']['type'];
                $size = $files['file_5']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 5 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 5 is greater than 1Mb');
                }
            }
			 
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                   $flname=$val["name"];
                   $fltype=$val["type"];
                    if(!empty($flname) && $flname!='' && !empty($fltype)){
					
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of all files is greater than 5Mb');
            } 
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
           
            $files = $_FILES;
			
            if(empty($data['filename'])){                
                 $messages->setErrorMessage("Please Enter the Filename.");                 
            }
			if(empty($data['subject'])){                
                 $messages->setErrorMessage("Please Enter the Subject.");                 
            }
		    if(empty($data['text'])){                
                 $messages->setErrorMessage("Please Enter the Body.");                 
            }
			$data['file_1'] = '';           
            if((!empty($files['file_1']['name']))){
                
                $filename = $files['file_1']['name'];
                $data['file_1'] = $filename;
                $type = $files['file_1']['type'];
                $size = $files['file_1']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 1 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 1 is greater than 1Mb');
                }
            }
            $data['file_2'] = '';           
            if((!empty($files['file_2']['name']))){
                
                $filename = $files['file_2']['name'];
                $data['file_2'] = $filename;
                $type = $files['file_2']['type'];
                $size = $files['file_2']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 2 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 2 is greater than 1Mb');
                }
            }
			$data['file_3'] = '';           
            if((!empty($files['file_3']['name']))){
                
                $filename = $files['file_3']['name'];
                $data['file_3'] = $filename;
                $type = $files['file_3']['type'];
                $size = $files['file_3']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 3 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 3 is greater than 1Mb');
                }
            }
            $data['file_4'] = '';           
            if((!empty($files['file_4']['name']))){
                
                $filename = $files['file_4']['name'];
                $data['file_4'] = $filename;
                $type = $files['file_4']['type'];
                $size = $files['file_4']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 4 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 4 is greater than 1Mb');
                }
            }
            $data['file_5'] = '';           
            if((!empty( $files['file_5']['name'] ))){
                
                $filename = $files['file_5']['name'];
                $data['file_5'] = $filename;
                $type = $files['file_5']['type'];
                $size = $files['file_5']['size'];
                $max_size = ($data['max_file_size'] * 1024);
                if ( !in_array($type, $data["allowed_file_types"] ) ) {
                     $messages->setErrorMessage("$type type Of file 5 is not allowed");
                }
                if($size > $max_size){
                    $messages->setErrorMessage('Size of file 5 is greater than 1Mb');
                }
            }
			 
            $fileSize = 0;
            if(!empty($data['files'])){
                foreach($data['files'] as $key => $val){
                   $flname=$val["name"];
                   $fltype=$val["type"];
                    if(!empty($flname) && $flname!='' && !empty($fltype)){
					
                        if ( !in_array($val["type"], $data["allowed_file_types"] ) ) {
                            $msg = $val["type"]." is not allowed ";
                            $messages->setErrorMessage($msg);
                        }
                        $fileSize += $val["size"] ;
                    }
                }
            }
            if($fileSize > $data['max_mfile_size']){
                 $messages->setErrorMessage('Size of all files is greater than 5Mb');
            }                         
            
            
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
    
         /**
         * This function is used to send a mail
         *
         * @param    string      unique ID of the Executive whose status is to be changed.
         * @param    integer     the new status value that is to be set.
         * @param    array       array containing the objects and variables that will be needed.
         *
         * @return   boolean     true, if the status was changed.
         *                       false, if the status was not changed.
         *
         */
        function validateSendmail(&$data, $extra='') {
             foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            if ( !isset($data['email']) || empty($data['email']) ) {
                $messages->setErrorMessage('Email should be provided.');
            }
            elseif ( !isValidEmail($data['email']) ) {
                $messages->setErrorMessage('Email Address is not valid.');
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

		
    }
?>
