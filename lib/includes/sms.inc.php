<?php
	class smsList
	{
		/*
		* get the list of corporate users
		*/
		function getList(&$db, &$user_list, $required_fields, $condition="", $from="", $rpp="")
		{
			$query = "SELECT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) 
			{
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) 
			{
				$query .= " ". $required_fields;
			}
			else 
			{
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			$query .= " FROM ". TABLE_SMS_HISTORY;
			
			$query .= " ". $condition;
			
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) 
			{
				$query .= " LIMIT ". $from .", ". $rpp;
			}
			//echo $query;
			if ( $db->query($query) ) 
			{
				if ( $db->nf() > 0 ) 
				{
					while ($db->next_record()) 
					{
						$user_list[] = array(
											"sh_id"	=>	$db->f("sh_id"),
											"sh_userid"	=> $db->f("sh_userid"),
											"sh_message" => $db->f("sh_message"),
											"sh_count" => $db->f("sh_count"),
											"sh_date" => $db->f("sh_date"),
                                            "f_name" => $db->f("f_name"),
                                            "l_name" => $db->f("l_name"),
											"status" => smsList::getMsgType($db->f("sh_id"))
											);
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				//print_r($hotel_list);
				return ( $total );
			}
			else 
			{
				return false;
			}
		}

		function getMsgType($shpshid)
		{
			$db = new db_local;
			
			$sql = "SELECT shp_msgstatus FROM ".TABLE_SMS_HISTORY_PHONE." WHERE shp_shid='".$shpshid."'";
			$db->query($sql);
			if($db->nf() > 0)
			{
				$db->next_record();
				return $db->f("shp_msgstatus");
			}
			else
				return 0;
		}

		function getSmsHistoryId($mobileno)
		{
			$db = new db_local;
			$id = "";

			$sql = "SELECT shp_shid From ".TABLE_SMS_HISTORY_PHONE." WHERE shp_phoneno='".$mobileno."'";

			$db->query($sql);
			if($db->nf() > 0)
			{
				while($db->next_record())
				{
					if($id != "")
						$id .= ",";

					$id .= $db->f("shp_shid");
				}
				return $id;
			}
		}

		function viewList(&$db, &$user_list, $required_fields, $condition="", $from="", $rpp="")
		{
			$query = "SELECT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) 
			{
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) 
			{
				$query .= " ". $required_fields;
			}else{
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			$query .= " FROM ". TABLE_SMS_HISTORY_PHONE;
			
			$query .= " ". $condition;
			
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) 
			{
				$query .= " LIMIT ". $from .", ". $rpp;
			}
			//echo $query;
			if ( $db->query($query) ) 
			{
				if ( $db->nf() > 0 ) 
				{
					while ($db->next_record()){ 
						$user_list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				return ( $total );
			}
			else 
			{
				return false;
			}
				
		}
	}
?>
