<?php

//2010-04-apr-30 bof
function retrieve_message($mbox, $messageid){
   global $charset,$htmlmsg,$plainmsg,$attachments;
   $htmlmsg = $plainmsg = $charset = '';
   $attachments = array();
   
   //HEADER   
   $header = imap_header($mbox, $messageid);
   //BODY
   $structure = imap_fetchstructure($mbox, $messageid);

	//print_R($structure);
   $message['subject'] = $header->subject;
   $message['fromaddress'] =   $header->fromaddress;
   $message['toaddress'] =   $header->toaddress;
   $message['to'] =   $header->to;
   $message['from'] =   $header->from;
   $message['to_email']='';
   $message['from_email']='';
   $message['to_email_1']='';
   $message['cc_email']='';
   $message['cc_email_1']='';
   $message['bcc_email']='';
   $message['bcc_email_1']='';
   $message['subject'] = fix_text($message['subject']);
   $message['headers_imap'] = imap_fetchbody($mbox,$messageid,"0"); ## GET THE HEADER OF MESSAGE
   
   if(empty($message['subject'])){
        $message['subject']='Email subject not mentioned';
   }
    if(isset($message['from']) && !empty($message['from'])){
        foreach($message['from'] as $key=>$val){           
            $message['from_email'] = $val->mailbox."@".$val->host ;
        }       
    }
  
  if(isset($message['to']) && !empty($message['to'])){
        foreach($message['to'] as $key=>$val){
            if($val->host !='pretechno.com' && $val->host !='smeerptech.com' && $val->host !='smeerptech.com'){
                $message['to_email'] .= $val->mailbox."@".$val->host."," ;
            }
            $message['to_email_1'] .= $val->mailbox."@".$val->host."," ;
        }
        if(!empty($message['to_email'])){
            $message['to_email'] = trim($message['to_email'],',');
        }
        if(!empty($message['to_email_1'])){
            $message['to_email_1'] = trim($message['to_email_1'],',');
        }
   }
  
    if(isset($header->ccaddress)){      
       $message['ccaddress'] =   $header->ccaddress;
       $message['cc'] =   $header->cc;
       if(isset($message['cc']) && !empty($message['cc'])){
            foreach($message['cc'] as $key=>$val){
                if($val->host !='pretechno.com' && $val->host !='smeerptech.com' && $val->host !='smeerptech.com'){
                    $message['cc_email'] .= $val->mailbox."@".$val->host."," ;
                }
                 $message['cc_email_1'] .= $val->mailbox."@".$val->host."," ;
            }
            if(!empty($message['cc_email'])){
                $message['cc_email'] = trim($message['cc_email'],',');
            }
            if(!empty($message['cc_email_1'])){
                $message['cc_email_1'] = trim($message['cc_email_1'],',');
            }
        }
    }  
    if(isset($header->bccaddress)){
        $message['bccaddress'] =   $header->bccaddress;
        $message['bcc'] =   $header->bcc;
        if(isset($message['bcc']) && !empty($message['bcc'])){
            foreach($message['bcc'] as $key=>$val){
                if($val->host !='pretechno.com' && $val->host !='smeerptech.com' && $val->host !='smeerptech.com'){
                    $message['bcc_email'] .= $val->mailbox."@".$val->host."," ;
                }
                $message['bcc_email_1'] .= $val->mailbox."@".$val->host."," ;
            }
            if(!empty($message['bcc_email'])){
                $message['bcc_email'] = trim($message['bcc_email'],',');
            }
            if(!empty($message['bcc_email_1'])){
                $message['bcc_email_1'] = trim($message['bcc_email_1'],',');
            }
        }
    } 
   $message['date'] =   $header->date;
   $message['udate'] =   $header->udate;
   //$message['udate'] = date('Y-m-d H:i:s',$message['udate']);
   
   //OTHER DETAILS BOF
    if (!$structure->parts)  // simple
        getpart($mbox, $messageid,$structure,0);  // pass 0 as part-number
    else {  // multipart: cycle through each part
        foreach ($structure->parts as $partno0=>$p)
            getpart($mbox, $messageid,$p,$partno0+1);
    }
   //OTHER DETAILS EOF
   if(!empty($plainmsg)){
        $message['body'].=$plainmsg;   
   }
   if(!empty($htmlmsg)){
        $message['body_html'].=str_get_html($htmlmsg);   
        //$html = str_get_html($htmlmsg);   
        //$message['body_html'].=$html->plaintext;
   }
   $message['charset']=$charset ;
   $message['attachments'] = $attachments ;
   
   return $message ;
}
//2010-04-apr-30 eof


function getpart($mbox,$mid,$p,$partno) {
    // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
    global $htmlmsg,$plainmsg,$charset,$attachments;

    // DECODE DATA
    $data = ($partno)?
        imap_fetchbody($mbox,$mid,$partno):  // multipart
        imap_body($mbox,$mid);  // simple
    // Any part may be encoded, even plain text messages, so check everything.
    if ($p->encoding==4)
        $data = quoted_printable_decode($data);
    elseif ($p->encoding==3)
        $data = base64_decode($data);

    // PARAMETERS
    // get all parameters, like charset, filenames of attachments, etc.
    $params = array();
    if ($p->parameters)
        foreach ($p->parameters as $x){
			$attribute= fix_text($x->attribute);
			$value= fix_text($x->value);
            $params[strtolower($attribute)] = $value;
            //$params[strtolower($x->attribute)] = $x->value;
		}
    if ($p->dparameters)
        foreach ($p->dparameters as $x){
			$attribute= fix_text($x->attribute);
			$value= fix_text($x->value);
            $params[strtolower($attribute)] = $value;
		}

    // ATTACHMENT
    // Any part with a filename is an attachment,
    // so an attached text file (type 0) is not mistaken as the message.
    if ($params['filename'] || $params['name']) {
        // filename may be given as 'Filename' or 'Name' or both
        $filename = ($params['filename'])? $params['filename'] : $params['name'];
        // filename may be encoded, so see imap_mime_header_decode()
		$attachments[$filename] = $data;  // this is a problem if two files have same name
    }

    // TEXT
    if ($p->type==0 && $data) {
        // Messages may be split in different parts because of inline attachments,
        // so append parts together with blank row.
        if (strtolower($p->subtype)=='plain')
            $plainmsg .= trim($data) ."\n\n";
        else
            $htmlmsg .= $data ."<br><br>";
        $charset = $params['charset'];  // assume all parts are same charset
    }

    // EMBEDDED MESSAGE
    // Many bounce notifications embed the original message as type 2,
    // but AOL uses type 1 (multipart), which is not handled here.
    // There are no PHP functions to parse embedded messages,
    // so this just appends the raw source to the main message.
    elseif ($p->type==2 && $data) {
        $plainmsg .= $data."\n\n";
    }

    // SUBPART RECURSION
    if ($p->parts) {
        foreach ($p->parts as $partno0=>$p2)
            getpart($mbox,$mid,$p2,$partno.'.'.($partno0+1));  // 1.2, 1.2.1, etc.
    }
}

function fix_text($var){
    if(ereg("=\?.{0,}\?[Bb]\?",$var)){
    $var = split("=\?.{0,}\?[Bb]\?",$var);
    
    while(list($key,$value)=each($var)){
    if(ereg("\?=",$value)){
    $arrTemp=split("\?=",$value);
    $arrTemp[0]=base64_decode($arrTemp[0]);
    $var[$key]=join("",$arrTemp);
    }}
    $var=join("",$var);
    }
    
    if(ereg("=\?.{0,}\?Q\?",$var)){
    $var = quoted_printable_decode($var);
    $var = ereg_replace("=\?.{0,}\?[Qq]\?","",$var);
    $var = ereg_replace("\?=","",$var);
    }
    return trim($var);
}


?>