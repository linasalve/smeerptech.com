<?php
	class UserRoles{
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
		const DELETED = 3;

		function getStatus() {
			$status = array('BLOCKED' =>UserRoles::BLOCKED,
							'ACTIVE'  =>UserRoles::ACTIVE,
							'PENDING' =>UserRoles::PENDING,
							'DELETED' =>UserRoles::DELETED
						);
			return ($status);
		}
		/**
		*	Function to get all User roles
		*
		*	@param Object of database
		*	@param Array of User role
		* 	@param required fields
		* 	@param condition
		*	return array of User roles
		*	otherwise return NULL
		*/		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_USER_ROLES;
			
			  $query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
		
		/**
		 *  Function to retrieve the list of Languages customised to display the list.
		 *
		 */
/*
		 function getDisplayList( &$db, &$list, $required_fields='', $condition='', $from='', $rpp='' ) {
			if ( $required_fields == '' ) {
					$required_fields = TABLE_USER_ROLES .".id "
										.",". TABLE_USER_ROLES .".title "
										//Rights column is commented here
										.",". TABLE_USER_ROLES .".rights "
										.",". TABLE_USER_ROLES .".status "
										;
				}
											
			if( $condition == "" ) {	
				$condition	.= " ORDER BY ". TABLE_USER_ROLES .".title ASC ";	
			}	
		
			$list	= NULL;
			UserRoles::getList( $db,$list, $required_fields, $condition,$from,$rpp);
			return ( count($list) );
	   }
*/

	   //This function is used to perform action on status.		
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			  
            // Check if status is valid or not.
			if ( !in_array($status_new, UserRoles::getStatus()) ) {
				$messages->setErrorMessage("The Status is not a valid Status for this Role.");
			}
			else {
				 $query = "UPDATE ". TABLE_USER_ROLES
								." SET status = '$status_new' "
							." WHERE id = '$id'";
				if ( $db->query($query) && $db->affected_rows()>0 ) {
					$messages->setOkMessage("Status has been updated successfully.");
				}
				else {
					$messages->setErrorMessage("Status is not changed for this role.");
				}
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
      }
	
		//used to delete records.
		function delete(&$db,$id, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			$list = NULL;
            if ( UserRoles::getList( $db, $list, 'id, status', " WHERE id = '$id'") > 0 ) {
                // Check if the User Role is blocked or not.
                $list = $list[0];
                if ( $list['status'] != UserRoles::BLOCKED &&  $list['status'] != UserRoles::DELETED ) {
                    $messages->setErrorMessage("To Delete a User Role its Status should be changed to 'Blocked'.");
                }
                
                if ( $messages->getErrorMessageCount() <= 0 ) {
                    // Delete the User Role
                    $query = "DELETE FROM ". TABLE_USER_ROLES ." WHERE id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("Users Role has been deleted.");
                    }
                    else {
                        $messages->setErrorMessage("Internal Error: The User Role was not deleted.");
                    }
                }
            }
            else {
                $messages->setErrorMessage("The User Role was not found, it may have been deleted or not created yet.");
            }

			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
			
		}

        
	    /**
		 * Function used to view the details of the User Role.
		 *
		 */
		  function getView( &$db, &$list, $role_id) {
				$required_fields ='';
				$required_fields = TABLE_USER_ROLES .".id "
									.",". TABLE_USER_ROLES .".title "
									.",". TABLE_USER_ROLES .".rights "
                                    .",". TABLE_USER_ROLES .".description "
									.",". TABLE_USER_ROLES .".status "
									;

                $condition  = " WHERE ".TABLE_USER_ROLES.".id = '". $role_id ."'";
                $list       = NULL;
                
                if ( UserRoles::getList( $db, $list, $required_fields, $condition) > 0 ) {
                    $list = $list[0];
                    $rights = explode(',', $list['rights']);
                    $rights = "'". implode("','", $rights) ."'";
                    $condition = " WHERE id IN (". $rights .") AND status = '1'"
                                    ." ORDER BY title ASC";
                    $lst_rights= NULL;
                    UserRoles::getUserRights( $db, $lst_rights, 'title', $condition);
                    $list['rights'] = $lst_rights;
                    return (true);
                }
                else {
                    return (false);
                }
	     }
		
		 /**
		 * Function used to get records by Users role ID.
		 *
		 */
		  function getUsersRoleById( &$db, &$list, $ur_id, $required_fields='', $condition='', $from='', $rpp='' ) {
				
				if ( !isset($required_fields) || $required_fields == '' ) {
					$required_fields = TABLE_USER_ROLES .".id "
									.",". TABLE_USER_ROLES .".title "
									//Rights column is commented here
									.",". TABLE_USER_ROLES .".rights "
									.",". TABLE_USER_ROLES .".description "
									
									;
				}
				
				if( $condition == "" ) {	
					$condition	.= " WHERE ".TABLE_USER_ROLES.".id=".$ur_id." ORDER BY ". TABLE_USER_ROLES .".title ASC ";	
				}	
				$list	= NULL;
				UserRoles::getList( $db, $list, $required_fields, $condition);
	     }
		
		/**
		 * Function to validate the input from the User while Adding a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd($data, &$messages, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
			
            if ( empty($data['title']) ) {
				$messages->setErrorMessage("Title should be provided for User Role.");
			}
           /*  elseif ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_-\s()]{2,80}$/', $data['title']) ) {
                $messages->setErrorMessage("Title of User Role is invalid.");
                $messages->setErrorMessage("It can contain a-z, A-z, 0-9, _ and spaces.");
                $messages->setErrorMessage("It should begin with an Alphabet and length should be in between 2 to 80.");
			} */
            else {
                $list = NULL;
                if (UserRoles::getList($db, $list, 'id', " WHERE title = '". $data['title'] ."'") > 0 ) {
                    $messages->setErrorMessage("A User Role with same title is already present.");
                }
            }
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }

            if ( empty($data['status']) && $data['status'] != '0' ) {
				$messages->setErrorMessage("Select the Status for User Role.");
			}
            elseif ( $data['status'] != UserRoles::PENDING && $data['status'] != UserRoles::ACTIVE 
                     && $data['status'] != UserRoles::BLOCKED && $data['status'] != UserRoles::DELETED ) {
                $messages->setErrorMessage("The Status of the User Role is not valid.");
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating a new User Role.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate($data, &$messages, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( empty($data['title']) ) {
                $messages->setErrorMessage("Title should be provided for User Role.");
            }
           /*  elseif ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_-\s()]{2,80}$/', $data['title']) ) {
                $messages->setErrorMessage("Title of User Role is invalid.");
                $messages->setErrorMessage("It can contain a-z, A-z, 0-9, _ and spaces.");
                $messages->setErrorMessage("It should begin with an Alphabet and length should be in between 2 to 80.");
            } */			
            else {
                $condition_query = " WHERE title = '". $data['title'] ."'"
                                    ." AND id != '". $data['role_id'] ."' ";
                $list = NULL;
                if (UserRoles::getList($db, $list, 'id', $condition_query) > 0 ) {
                    $messages->setErrorMessage("A User Role with same title is already present.");
                }
            }
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }

            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
		
		//This function is used to get userRights From rights table.
		function getUserRights( &$db, &$list, $required_fields, $condition=''){
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_USER_RIGHTS;
			
			$query .= " ". $condition;
			
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				return ( $total );
			}
			else {
				return false;
			}	
		}
		
		//This function is used to get userRights From rights table.
		function getUserRightsList( &$db, &$list, $required_fields, $condition='', $from='', $rpp=''){
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_USER_RIGHTS;
			
			$query .= " ". $condition;
			
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
			
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				return ( $total );
			}
			else {
				return false;
			}	
		}
 
}
