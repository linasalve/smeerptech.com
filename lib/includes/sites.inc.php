<?php

class Sites {
	
	
	function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
		$query = "SELECT DISTINCT ";
		$count = $total=0;
		if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
			$query .= implode(",", $required_fields);
		}
		elseif ( !empty($required_fields) ) {
			$query .= " ". $required_fields;
		}
		else {
			$query .= " COUNT(*) as count";
			$count = 1;
		}
		
		$query .= " FROM ". TABLE_SITES;
		
		$query .= " ". $condition;

		if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
			$query .= " LIMIT ". $from .", ". $rpp;
		}
		if ( $db->query($query) ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$list[] = processSqlData($db->result());
					if($count==1){
						$total =$db->f("count") ;							
					}else{
						$total =$db->nf();
					}
				}
			}
			return ( $total );
		}
		else {
			return false;
		}	
	}


}
?>