<?php 

class Newsletter {

    
    function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
        $query = "SELECT ";
        $count = $total=0;    
        if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
            $query .= implode(",", $required_fields);
        }
        elseif ( !empty($required_fields) ) {
            $query .= " ". $required_fields;
        }
        else {
            $query .= " COUNT(*) as count";
			$count = 1;
        }
            
        $query .= " FROM ". TABLE_NEWSLETTER;
            
        $query .= " ". $condition;
    
        if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
            $query .= " LIMIT ". $from .", ". $rpp;
        }
        //echo $query;
        if ( $db->query($query) ) {
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $list[] = $db->result();
					if($count==1){
						$total =$db->f("count") ;							
					}else{
						$total =$db->nf();
					}
                }
            }
            return ( $total );
        }
        else {
            return false;
        }
    }
    function getListQueue( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
        $query = "SELECT ";
         $count = $total=0;      
        if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
            $query .= implode(",", $required_fields);
        }
        elseif ( !empty($required_fields) ) {
            $query .= " ". $required_fields;
        }
        else {
            $query .= " COUNT(*) as count";
			$count = 1;
        }
            
        $query .= " FROM ". TABLE_NEWSLETTER_TEMP;
            
        $query .= " ". $condition;
    
        if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
            $query .= " LIMIT ". $from .", ". $rpp;
        }
        //echo $query;
        if ( $db->query($query) ) {
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $list[] = $db->result();
					if($count==1){
						$total =$db->f("count") ;							
					}else{
						$total =$db->nf();
					}
                }
            }
            return ( $total );
        }
        else {
            return false;
        }
    }
    function validateAdd(&$data, $extra='') {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }      
        $to =  array() ;
        if ( !empty($data["to"]) ) {
			
			$email_arr = explode(",", $data["to"]);
			
			foreach ($email_arr as $key=>$item){
				
				$name 	= trim(substr($item, 0, strrpos($item, "<")-1) ) ;
				$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;

				if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/" , $email)) {
					$messages->setErrorMessage("Invalid email address is present in the receivers 'To' list.");
					break;
				}
				$to[] = array (	"name" => $name,
								"email" => $email
							);
				//break;
			}
		}
		else {
			$messages->setErrorMessage("Please enter alteast one recipient in the 'To' field.");
		}
        $data["to_email"] = $to;
        
        $cc = array() ;
		if ( !empty($data["cc"]) ) {
		
			$email_arr = explode(",", $data["cc"]);

			foreach ($email_arr as $key=>$item) {
				$name 	= trim(substr($item, 0, strrpos($item, "<")-1) ) ;
				$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;

				if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/" , $email)) {
					$messages->setErrorMessage("Invalid email address is present in the receivers 'CC' list.");
					break;
				}
				$cc[] = array (	"name" => $name,
								"email" => $email
							);
			}
		}	
        $data["cc_email"]=$cc;
        
		$bcc = array() ;
		if ( !empty($data["bcc"]) ) {
			
			$email_arr = explode(",", $data["bcc"]);		
			foreach ($email_arr as $key=>$item) {
				
				$name 	= trim(substr($item, 0, strrpos($item, "<")-1) ) ;
				$email 	= trim(substr($item, strrpos($item, "<")+1, ( (int)strrpos($item, ">")-(int)strrpos($item, "<") - 1 ) )) ;

				if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/" , $email)) {
					$messages->setErrorMessage("Invalid email address is present in the receivers 'BCC' list.");
					break;
				}
				$bcc[] = array (	"name" => $name,
									"email" => $email
								);
			}
		}	
		$data["bcc_email"]=$bcc;
		if ( !empty($data["batch"]) ) {
			if ( !is_numeric($data["batch"]) ) {
				$messages->setErrorMessage("Enter digits only in 'Batch'.");
			}
		}
		
		if ( $data["subject"] == "" ) {
			$messages->setErrorMessage("Please enter a Subject for the Mail.");
		}
				
		if ( @$data["save_newsletter"] == "1" && $data["save_newsletter_title"] == "") {
			$messages->setErrorMessage("Please enter the title for the Newsletter to identify later on.");
		}
		if ( @$data["save_newsletter"] == "" &&  @$data["direct_send"] != "1") {
			$messages->setErrorMessage("Please select whether to 'Save and Add to queue/Send' or 'Only Save' Newsletter.");
		}
           
            
                       
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    /*
    function getGroupsList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
        $query = "SELECT ";
            
        if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
            $query .= implode(",", $required_fields);
        }
        elseif ( !empty($required_fields) ) {
            $query .= " ". $required_fields;
        }
        else {
            $query .= " COUNT(*) ";
        }
            
        $query .= " FROM ". TABLE_NEWSLETTER_CATEGORY;
            
        $query .= " ". $condition;
    
        if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
            $query .= " LIMIT ". $from .", ". $rpp;
        }
        //echo $query;
        if ( $db->query($query) ) {
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $list[] = $db->result();
                }
            }
            return ( $db->nf() );
        }
        else {
            return false;
        }
    }*/
    
    
    /*
    function getSubscriberList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
        $query = "SELECT ";
            
        if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
            $query .= implode(",", $required_fields);
        }
        elseif ( !empty($required_fields) ) {
            $query .= " ". $required_fields;
        }
        else {
            $query .= " COUNT(*) ";
        }
            
        $query .= " FROM ". TABLE_SUBSCRIBERS;
            
        $query .= " ". $condition;
    
        if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
            $query .= " LIMIT ". $from .", ". $rpp;
        }
        //echo $query;
        if ( $db->query($query) ) {
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $list[] = $db->result();
                }
            }
            return ( $db->nf() );
        }
        else {
            return false;
        }
    }
    
    
    
    function getNewsCategory(&$db, &$list) {
        $required_fields = "id, name ";
        $condition_query = " WHERE status = '1' ORDER BY name ASC";

        if ( Newsletter::getGroupsList($db, $list, $required_fields, $condition_query) ) {
            return true;
        }
        else {
            return false;
        }
    }
    */
    /*
    function validateAdd(&$data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
        
        if ( empty($data['category']) || count($data['category'])<0 ) {
            $messages->setErrorMessage("Please select atleast one Group as recipient.");
        }
        else {
            // Check if all the selected Groups are actually present in the valid group list.
            $data['category_csv'] = '';
            foreach ( $data['category'] as $key=>$category ) {
                $index_arr = array();
                array_key_search('id', $category, $lst_cat, $index_arr);
                if ( count($index_arr) <= 0 ) {
                    $messages->setErrorMessage('Atleast one of the selected group was not valid.');
                    break;
                }
                else {
                    $data['category_csv'] .= $category .',';
                }
            }
            if ( !empty($data['category_csv']) ) {
                $data['category_csv'] = substr($data['category_csv'], 0, (strlen($data['category_csv'])-1) );
            }
        }
        
        if ( !isset($data['title']) || empty($data['title']) ) {
            $messages->setErrorMessage('Please enter the title to identify the newsletter.');
        }
        
        if ( !isset($data['subject']) || empty($data['subject']) ) {
            $messages->setErrorMessage('Please enter the subject for the newsletter.');
        }
        
        if ( !isset($data['content']) || empty($data['content']) || $data['content'] == '<br>' ) {
            $messages->setErrorMessage('Please enter the text for the newsletter.');
        }
        
        if ( isset($data['type']) && $data['type'] == '0' ) {
            $data['type'] = 'SENT';
        }
        elseif ( isset($data['type']) && $data['type'] == '1' ) {
            $data['type'] = 'DRAFT';
        }
        else {
            $messages->setErrorMessage('Please select a valid choice, whether to send and save or to save only.');
        }
        
        $data['do_c']       = date('Y-m-d H:i:s', time());
        $data['archived']   = '0';
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    
    function validateUpdate(&$data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
        
        if ( empty($data['category']) || count($data['category'])<0 ) {
            $messages->setErrorMessage("Please select atleast one Group as recipient.");
        }
        else {
            // Check if all the selected Groups are actually present in the valid group list.
                    $data['category_csv'] = '';
            foreach ( $data['category'] as $key=>$category ) {
                $index_arr = array();
                array_key_search('id', $category, $lst_cat, $index_arr);
                if ( count($index_arr) <= 0 ) {
                    $messages->setErrorMessage('Atleast one of the selected group was not valid.');
                    break;
                }
                else {
                    $data['category_csv'] .= $category .',';
                }
            }
            if ( !empty($data['category_csv']) ) {
                $data['category_csv'] = substr($data['category_csv'], 0, (strlen($data['category_csv'])-1) );
            }
        }
        
        if ( !isset($data['title']) || empty($data['title']) ) {
            $messages->setErrorMessage('Please enter the title to identify the newsletter.');
        }
        
        if ( !isset($data['subject']) || empty($data['subject']) ) {
            $messages->setErrorMessage('Please enter the subject for the newsletter.');
        }
        
        if ( !isset($data['content']) || empty($data['content']) || $data['content'] == '<br>' ) {
            $messages->setErrorMessage('Please enter the text for the newsletter.');
        }
        
        if ( isset($data['type']) && $data['type'] == '0' ) {
            $data['type'] = 'SENT';
        }
        elseif ( isset($data['type']) && $data['type'] == '1' ) {
            $data['type'] = 'DRAFT';
        }
        else {
            $messages->setErrorMessage('Please select a valid choice, whether to send and save or to save only.');
        }
        
        $data['do_c']       = date('Y-m-d H:i:s', time());
        //$data['archived']   = '0';
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }*/
    
        
    function changeStatus($id, $status, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }

        $list = NULL;
        if ( Newsletter::getList($db, $list, 'id', " WHERE id='$id'") ) {
            $query = "UPDATE ". TABLE_NEWSLETTER
                    ." SET archived = '". $status ."'"
                    ." WHERE id = '". $id ."'";
            if ( $db->query($query) ) {
                if ( $db->affected_rows()>0 ) {
                    $messages->setOkMessage('The Newsletter Archive status has been changed.');
                }
                else {
                    $messages->setOkMessage('There was no change in Newsletter Archive status.');
                }
            }
            else {
                $messages->setErrorMessage('Your request was not completed. Please try again.');
            }
        }
        else {
            $messages->setErrorMessage('The Newsletter was not found.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    
    function delete($id, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
    
        $list = NULL;
        if (Newsletter::getList($db, $list, 'n_id', " WHERE n_id='$id'")) {
            
            $query = "SELECT temp_email_id FROM ".TABLE_NEWSLETTER_TEMP_EMAILS
                        ." WHERE ".TABLE_NEWSLETTER_TEMP_EMAILS.".n_id= ". $id ."";
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    $messages->setErrorMessage('The Newsletter was not deleted. It is present in the Queue.');
                }
                else {
                    $query = "DELETE FROM ". TABLE_NEWSLETTER
                            ." WHERE n_id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage('The Newsletter has been deleted.');
                    }
                    else {
                        $messages->setErrorMessage('Your request was not completed. Please try again.');
                    }
                }
            }
            else {
                $messages->setErrorMessage('Your request was not completed. Please try again.');
            }
        }
        else {
            $messages->setErrorMessage('The Newsletter was not found.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    function deleteQueue($id, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
    
        $list = NULL;
        if (Newsletter::getListQueue($db, $list, 'temp_news_id,batches_remaining', " WHERE temp_news_id='$id'")>0) {
            $list=$list[0];
            $batch= $list['batches_remaining'];
            
            $result = false;
			if ( !empty($batch) ) {
				$batch = "'". str_replace(',', "','", $batch) ."'";
				
				$query="DELETE FROM ". TABLE_NEWSLETTER_TEMP_EMAILS ." WHERE temp_email_id IN (". $batch .")";
				if ( $db->query($query) )
					$result = true;
			}
			else {
				$result = true;
			}
            if ( $result ) {
				$query="DELETE FROM  ". TABLE_NEWSLETTER_TEMP ." WHERE  temp_news_id ='". $id ."'";
				$db->query($query);				
				$messages->setOkMessage("Newsletter deleted from the queue.");				
			}
			else {
				$messages->setOkMessage("Newsletter was not deleted from the queue.");
			}
          
        }
        else {
            $messages->setErrorMessage('The Newsletter was not found in the queue.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    /*
    function validateSubscriberAdd(&$data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
        
        if ( empty($data['category']) || count($data['category'])<0 ) {
            $messages->setErrorMessage("Please select atleast one Group for the Subscriber.");
        }
        else {
            // Check if all the selected Groups are actually present in the valid group list.
                    $data['category_csv'] = '';
            foreach ( $data['category'] as $key=>$category ) {
                $index_arr = array();
                array_key_search('id', $category, $lst_cat, $index_arr);
                if ( count($index_arr) <= 0 ) {
                    $messages->setErrorMessage('Atleast one of the selected group was not valid.');
                    break;
                }
                else {
                    $data['category_csv'] .= $category .',';
                }
            }
            if ( !empty($data['category_csv']) ) {
                $data['category_csv'] = substr($data['category_csv'], 0, (strlen($data['category_csv'])-1) );
            }
        }
        
      // if ( !isset($data['first_name']) || empty($data['first_name']) ) {
     //       $messages->setErrorMessage('Please enter the first name.');
     //   }
     //   elseif ( strlen($data['first_name']) > 50 ) {
     //       $messages->setErrorMessage('The length of the first name is more than 50.');
      //  }
        
     //   if ( !isset($data['last_name']) || empty($data['last_name']) ) {
     //       $messages->setErrorMessage('Please enter the last name.');
     /   }
     //   elseif ( strlen($data['last_name']) > 50 ) {
     //       $messages->setErrorMessage('The length of the last name is more than 50.');
        //}
        
//        if ( (!isset($data['primary_email']) || $data['primary_email'] == '') && (!isset($data['mobile']) || $data['mobile'] == '') ) {
//          $messages->setErrorMessage('Please enter a mobile number or email address.');
//     }
        //else {
            if ( isset($data['mobile']) && !isValidInt($data['mobile']) ) {
                $messages->setErrorMessage('Mobile number must be Numeric digits.');
            }
            if ( isset($data['primary_email']) && !isValidEmail($data['primary_email']) ) {
                $messages->setErrorMessage('The entered email address is not valid.');
            }
            else {
                // Check if the email address is aready present in the database?
                $query = "SELECT id FROM ". TABLE_SUBSCRIBERS
                            ." WHERE primary_email = '". $data['primary_email'] ."'";
                if ( $db->query($query) ) {
                    if ( $db->nf()>0 ) {
                        $messages->setErrorMessage('The Email address is already added. Please specify another.');
                    }
                }
                else {
                    $messages->setErrorMessage('Your request was not completed: Please try again.');
                }
            }
        //}
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    function validateSubscriberUpdate(&$data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
        
        if ( empty($data['category']) || count($data['category'])<0 ) {
            $messages->setErrorMessage("Please select atleast one Group for the Subscriber.");
        }
        else {
            // Check if all the selected Groups are actually present in the valid group list.
                    $data['category_csv'] = '';
            foreach ( $data['category'] as $key=>$category ) {
                $index_arr = array();
                array_key_search('id', $category, $lst_cat, $index_arr);
                if ( count($index_arr) <= 0 ) {
                    $messages->setErrorMessage('Atleast one of the selected group was not valid.');
                    break;
                }
                else {
                    $data['category_csv'] .= $category .',';
                }
            }
            if ( !empty($data['category_csv']) ) {
                $data['category_csv'] = substr($data['category_csv'], 0, (strlen($data['category_csv'])-1) );
            }
        }
        
       // if ( !isset($data['first_name']) || empty($data['first_name']) ) {
         //   $messages->setErrorMessage('Please enter the first name.');
       // }
      //  elseif ( strlen($data['first_name']) > 50 ) {
      //      $messages->setErrorMessage('The length of the first name is more than 50.');
      //  }
        
      //  if ( !isset($data['last_name']) || empty($data['last_name']) ) {
      //      $messages->setErrorMessage('Please enter the last name.');
      //  }
      //  elseif ( strlen($data['last_name']) > 50 ) {
      //      $messages->setErrorMessage('The length of the last name is more than 50.');
      //  }
        
        if ( (!isset($data['primary_email']) || $data['primary_email'] == '') && (!isset($data['mobile']) || $data['mobile'] == '') ) {
            $messages->setErrorMessage('Please enter a mobile number or email address.');
        }
        else {
            if ( isset($data['mobile']) && !isValidInt($data['mobile']) ) {
                $messages->setErrorMessage('The entered mobile number is not valid.');
            }
            if ( isset($data['primary_email']) && !isValidEmail($data['primary_email']) ) {
                $messages->setErrorMessage('The entered email address is not valid.');
            }
            else {
                // Check if the email address is aready present in the database?
                        $query = "SELECT id FROM ". TABLE_SUBSCRIBERS
                                    ." WHERE primary_email = '". $data['primary_email'] ."'"
                                    ." AND id != '". $data['id'] ."'";
                if ( $db->query($query) ) {
                    if ( $db->nf()>0 ) {
                        $messages->setErrorMessage('The Email address is already added. Please specify another.');
                    }
                }
                else {
                    $messages->setErrorMessage('Your request was not completed: Please try again.');
                }
            }
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    function deleteSubscriber($id, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
    
        $list = NULL;
        if (Newsletter::getSubscriberList($db, $list, 'id', " WHERE id='$id'")) {
            $query = "DELETE FROM ". TABLE_SUBSCRIBERS
                        ." WHERE id = '". $id ."'";
            if ( $db->query($query) && $db->affected_rows()>0 ) {
                $messages->setOkMessage('The Subscriber has been deleted.');
            }
            else {
                $messages->setErrorMessage('Your request was not completed. Please try again.');
            }
        }
        else {
            $messages->setErrorMessage('The Subscriber was not found.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    function changeStatusSubscriber($id, $status, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }

        $list = NULL;
        if ( Newsletter::getSubscriberList($db, $list, 'id', " WHERE id='$id'") ) {
            $query = "UPDATE ". TABLE_SUBSCRIBERS
                        ." SET status = '". $status ."'"
                        ." WHERE id = '". $id ."'";
            if ( $db->query($query) ) {
                if ( $db->affected_rows()>0 ) {
                    $messages->setOkMessage('The Subscriber status has been changed.');
                }
                else {
                    $messages->setOkMessage('There was no change in Subscriber status.');
                }
            }
            else {
                $messages->setErrorMessage('Your request was not completed. Please try again.');
            }
        }
        else {
            $messages->setErrorMessage('The Subscriber was not found.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    

    function addToQueue($id, $cat_arr, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
    
        // Read the subscribers information for each category.
        $lst_subs = array();
        foreach ( $cat_arr as $key=>$category ) {
            $temp = array();
            Newsletter::getSubscriberDetails($db, $temp, $category);
            $lst_subs = array_merge($lst_subs, $temp);
        }
        
        if ( count($lst_subs) > 0 ) {
            $added = false;
            foreach ( $lst_subs as $key=>$subscriber ) {
                $query = " INSERT INTO ". TABLE_NEWSLETTER_TEMP
                            ." SET newsletter_id = '". $id ."'"
                            .", to_email    = '". $subscriber['primary_email'] ."'"
                            .", to_fname    = '". $subscriber['first_name'] ."'"
                            .", to_lname    = '". $subscriber['last_name'] ."'"
                            .", from_email  = '". NEWSLETTER_FROM_EMAIL ."'"
                            .", from_name   = '". NEWSLETTER_FROM_NAME ."'";
                if ( $db->query($query) && $db->affected_rows()>0 && !$added ) {
                    $messages->setOkMessage('The Newsletter has been sent.');
                    $added = true;
                }
            }
        }
        else {
            $messages->setErrorMessage('Newsletter not sent: No Subscribers are present in the category(ies).');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    
    function getSubscriberDetails(&$db, &$lst_subs, $cat_id)
    {
        $required_fields = " primary_email, first_name, last_name, catid ";
        $condition_query = " WHERE ( catid REGEXP '^$cat_id$'"
                                    ." OR catid REGEXP ',$cat_id$'"
                                    ." OR catid REGEXP ',$cat_id,'"
                                    ." OR catid REGEXP '^$cat_id,' ) "
                            ." AND status = '1'"
                            ." GROUP BY primary_email";

        if ( Newsletter::getSubscriberList($db, $lst_subs, $required_fields, $condition_query) ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    
    
    
    
    function getRestrictions($type='') {
        $restrictions = array(
                            'MAX_SIZE'  => array('bytes' => 1048576, 'display' => '1024 Kb'), // 1024 * 1024 = 1048576 = 1MB
                            'MIME'      => array('text/csv', 'text/plain','application/octet-stream'),
                            'EXT'       => array('csv', 'txt')
                             );
        
        if ( empty($type) ) {
            return ($restrictions);
        }
        else {
            return($restrictions[$type]);
        }
    }
    
    function getTables(&$db, &$tables) {
        $count  = 0;
        $temp   = $db->table_names();
        
        $count  = count($temp);
        for ( $i=0;$i<$count;$i++ ) {
            $tables[$i] = array('table_name'=> $temp[$i]['table_name'],
                                'fields'    => $db->metadata($temp[$i]['table_name'])
                               );
        }
		
        $temp   = NULL;
        return($count);
    }
    
    
    function validateUpload($data, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
        
        if ( empty($file['file_csv']['error']) ) {
            if ( !empty($file['file_csv']['tmp_name']) ) {
                if ( $file['file_csv']['size'] == 0 ) {
                    $messages->setErrorMessage('The Uploaded file is empty.');
                }
                if ( !in_array($file['file_csv']['type'], $CSV['MIME']) ) {
                    $messages->setErrorMessage('The File type is not allowed to be uploaded.');
                }
                if ( $file['file_csv']['size'] > $CSV['MAX_SIZE']['bytes'] ) {
                    $messages->setErrorMessage('The File size is more than the allowed '. $CSV['MAX_SIZE']['display'] .'.');
                }
            }
            else {
                $messages->setErrorMessage('Server Error: File was not uploaded.');
            }
        }
        else {
            $messages->setErrorMessage('Unexpected Error: File was not uploaded.');
        }
        
        if ( $messages->getErrorMessageCount() <= 0 ) {
      
            return (true);
        }
        else {
            return (false);
        }
    }
    
    
    function duplicateFieldValue(&$db, $table, $field_name, $field_value, &$duplicate_record=-1) {
       $query = "SELECT uid, catID FROM $table WHERE $field_name = '$field_value' ";
       //$query .= " AND ( catID REGEXP '^". $catid ."$' OR catID REGEXP ',". $catid ."$' OR catID REGEXP ',". $catid .",' OR catID REGEXP '^". $catid .",' ) ";
        if ( $db->query($query) && $db->nf()>0 ) {
            if ( $duplicate_record != -1 ) {
                $db->next_record();
                $duplicate_record = processData($db->Record);
            }
            return (true);
        }
        else {
            return (false);
        }
    }

    
    function getCatId(&$db, $table1, $field_name, $field_value) {
		if (!is_numeric($field_value)) {
			$query = "SELECT $field_name FROM $table1 WHERE CatName  = '$field_value' ";
		}
		else {
			$query = "SELECT $field_name FROM $table1 WHERE catID = '$field_value' ";
		}
        
		if ( $db->query($query) && $db->nf()>0 ) {
			$db->next_record();
			$catid = $db->f('catID');
			return ($catid);
		}
		else {
            return (false);
          
		//	$sql_query = '';
		//	if ( !is_numeric($field_value) ) {
		//		$sql_query = "INSERT INTO $table1 SET name = '$field_value', status='1' ";
		//	}
		//	else {
        //        $sql_query = "INSERT INTO $table1 SET name = '$field_value', status='1' ";
		//	}
			
		//	if ($db->query($sql_query)) {
        //        $db->next_record();
        //        $catid = $db->last_inserted_id();
          //      return ($catid);
		//	}
		//	else {
		//		return (false);
		//	}
            
		}
	
    }
    
    
    function getCategoryTree(&$lst_cat, $extra, $parent=0, $level=0) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }
        $tmp_lst = NULL;
        
        $query = " SELECT * FROM ". TABLE_NEWSLETTER_CATEGORY
                    ." WHERE " . TABLE_NEWSLETTER_CATEGORY .".parent = '$parent'"
                    ." ORDER BY name ASC ";

        if ( $db->query($query) && $db->nf()>0 ) {
            while ( $db->next_record() ) {
                $tmp_lst[] = processSQLData($db->result());
            }
            foreach ( $tmp_lst as $key=>$tcat ) {
                $lst_cat[$tcat['id']] = str_repeat('&nbsp;', $level * 4) . $tcat['name'];
                
                Newsletter::getCategoryTree($lst_cat, $extra, $tcat['id'], $level+1);
            }
        }
        
        return true;
    }
    
    
    function isCategoryChild($id, $match_parent, $extra) {
        foreach ($extra as $key=>$value) {
            $$key = $value;
        }

        $query = " SELECT parent FROM ". TABLE_NEWSLETTER_CATEGORY
                    ." WHERE " . TABLE_NEWSLETTER_CATEGORY .".id = '$id'"
                    ." ORDER BY name ASC ";

        if ( $db->query($query) ) {
            if ( $db->nf()>0 && $db->next_record() ) {
                $id = $db->f('parent');
                if ( $id == $match_parent ) {
                    return (true);
                }
                elseif ( $id != '0' ) {
                    return ( Newsletter::isCategoryChild($id, $match_parent, $extra) );
                }
                else {
                    return (false);
                }
            }
        }
        return (true);
    }

    
    function getCategoryTitles(&$db, $cat_id) {
        $cat_name   = array();
        $cat_id     = explode(",", $cat_id);
        $count      = count($cat_id);
        $i          = 0;
        
        for ( $i=0; $i<$count; $i++ ) {
            $query = " SELECT name FROM ". TABLE_NEWSLETTER_CATEGORY
                        ." WHERE id = '". $cat_id[$i] ."'"
                        ." ORDER BY name ASC";
            if ( $db->query($query) && $db->next_record() ) {
                $cat_name[] = $db->f("name");
            }
        }
        return $cat_name;
    }*/
    
}
?>