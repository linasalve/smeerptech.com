<?php
	class ProposalQuote {
		 
		 

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
             
            $data['query_p']='';
            $is_part = false;
            $isrenewable = false;
           
            if(isset($data['team_member'])){
                $count1= count($data['team_member']) ;
				$count2 = count(array_unique($data['team_member'])) ; 
				if( $count1!=$count2 ){
					 $messages->setErrorMessage('Please check the entries, You selected duplicate team members');
				} 
                foreach ( $data['team_member'] as $key=>$particular ) {
					
                    if(empty($data['team_member'][$key]) ){
                         $messages->setErrorMessage('The Team Member field at Position '. ($key+1) .' cannot be empty.');
                    }
					if(empty($data['hrs'][$key]) ){
                         $messages->setErrorMessage('The Hours field at Position '. ($key+1) .' cannot be empty.');
                    }
					
					/* 
					if(empty($data['do_complete'][$key])){
                         $messages->setErrorMessage('The Date of Completion field at Position '. ($key+1) .' cannot be empty.');
                    }
					*/
                    if ( ( !empty($data['team_member'][$key]) || !empty($data['hrs'][$key]) ) && $count1==$count2) {
                        
						$table = TABLE_USER;
						$condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['team_member'][$key] ."' LIMIT 0,1 " ;
						$fields1 =  TABLE_USER .'.current_salary';
						$exeCreted= getRecord($table,$fields1,$condition2);                
						$currentSalary = $exeCreted['current_salary'] ;				
						 
						$data['per_hour_salary'][$key] = $currentSalary / STD_WORKING_HRS;
						$data['per_hour_salary'][$key] =  number_format($data['per_hour_salary'][$key],2);
						$worked_min = ( $data['hrs'][$key] * 60 )  ;
						$worked_hour = ($worked_min/60) ;
						$worked_hour =  number_format($worked_hour,2);
						$worked_hour_salary = ($data['per_hour_salary'][$key] * $worked_hour);						
						 
						 
                    }
                }
            }else{
                //temp. commented 
                //$messages->setErrorMessage('No any Particulars available.');
            }
			
			 
             

            
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		 
    }
    
?>
