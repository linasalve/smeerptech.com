<?php
	class PlpPeriod {
		
		 const ACTIVE  = 1;
		 const DEACTIVE = 0;   
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_PLP_PERIOD;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_PLP_PERIOD;
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	  

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            if ( (PlpPeriod::getList($db, $details, 'plp_period_id', " WHERE plp_period_id = '$id'")) > 0 ) {
               $details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_PLP_PERIOD
                                ." WHERE plp_period_id = '$id'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
               /* }
                else {
                    $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                }*/
            }
            else {
                $messages->setErrorMessage("The Record was not found.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            
           
            if ( !isset($data['plp_period_start']) || empty($data['plp_period_start']) ) {
                $messages->setErrorMessage("Select the from data.");
            }
            
            if ( !empty($data['plp_period_start']) ) {
                $temp = explode('/', $data['plp_period_start']);
                
                $date_period_from=$temp[2] . $temp[1] . $temp[0];
                $data['plp_period_start'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            if ( !isset($data['plp_period_end']) || empty($data['plp_period_end']) ) {
                $messages->setErrorMessage("Select the to data.");
            }
            
            if ( !empty($data['plp_period_end']) ) {
                $temp = explode('/', $data['plp_period_end']);
                
                $date_period_to=$temp[2] . $temp[1] . $temp[0];
                $data['plp_period_end'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
          
            if ( !empty($data['plp_period_start']) && !empty($data['plp_period_end']))
            {
            	if($date_period_from > $date_period_to){
              		 $messages->setErrorMessage("From date can not be greater than to date.");
			    }	
              	
		    }	
            
            if(empty($data['plp_period_title']) ){                
                $messages->setErrorMessage("Title can not be empty.");
            }
          
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
                   
            if ( !isset($data['plp_period_start']) || empty($data['plp_period_start']) ) {
                $messages->setErrorMessage("Select the from data.");
            }
            
            if ( !empty($data['plp_period_start']) ) {
                $temp = explode('/', $data['plp_period_start']);
                
                $date_period_from=$temp[2] . $temp[1] . $temp[0];
                $data['plp_period_start'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
            if ( !isset($data['plp_period_end']) || empty($data['plp_period_end']) ) {
                $messages->setErrorMessage("Select the to data.");
            }
            
            if ( !empty($data['plp_period_end']) ) {
                $temp = explode('/', $data['plp_period_end']);
                $date_period_to=$temp[2] . $temp[1] . $temp[0];
                $data['plp_period_end'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
            }
            
             if ( !empty($data['plp_period_start']) && !empty($data['plp_period_end']))
            {
            	if($date_period_from > $date_period_to){
              		 $messages->setErrorMessage("From date can not be greater than to date.");
			    }	
              	
		    }	
		    
            if(empty($data['plp_period_title']) ){                
                $messages->setErrorMessage("Title can not be empty.");
            }   
          
                       
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
		
		function active($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ". TABLE_PLP_PERIOD
                            ." SET ".  TABLE_PLP_PERIOD .".plp_period_status= '". PlpPeriod::ACTIVE."'"
                            	 

                            ." WHERE plp_period_id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Plp period has been activated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Plp period has not been activated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
        
        function deactive($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Addressbook::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
               	
                    $query = " UPDATE ".  TABLE_PLP_PERIOD
                            ." SET ".  TABLE_PLP_PERIOD .".plp_period_status = '". PlpPeriod::DEACTIVE."'"
                            	 

                            ." WHERE plp_period_id = '". $id ."'";
                    if ( $db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Plp period has been deactivated.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Plp period has not been deactivated.");
                    }
                //}
                //else {
                   // $messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
            //}
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    }
?>
