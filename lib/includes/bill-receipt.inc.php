<?php
	class Receipt {
		
		const BLOCKED = 0;
		const ACTIVE  = 1;
		const PENDING = 2;
        const DELETED = 3;
		const COMPLETED = 4;

		function getStatus() {
			$status = array('BLOCKED' => Receipt::BLOCKED,
							'ACTIVE'    => Receipt::ACTIVE,
						//	'PENDING'   => Receipt::PENDING,
                            'DELETED'   => Receipt::DELETED,
                            //'PROCESSED' => Receipt::COMPLETED
						);
			return ($status);
		}
        
        function getStatusTitle() {
            $status = array(Receipt::BLOCKED    => 'Blocked',
                            Receipt::ACTIVE     => 'Active',
                        //  Receipt::PENDING    => 'Pending',
                            Receipt::DELETED    => 'Delete',
                          //Receipt::COMPLETED  => 'Completed',
                            'CANCELLED'         => 'Blocked',
                            'ACTIVE'            => 'Active',
                            'PENDING'           => 'Pending',
                            'DELETED'           => 'Delete',
                          //'PROCESSED'         => 'Completed',
                           );
            return ($status);
        }
		function getCompany( &$db, &$company){
			$query="SELECT ".TABLE_SETTINGS_COMPANY.".* FROM ".TABLE_SETTINGS_COMPANY ;
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$company[]= processSqlData($db->result());
					}
				}			
				return ( $db->nf() );
			}
			else {
				return false;
			}	
		}
		function getPaymentMode() {
			$mode = array(	'OB'	=> 'Online Banking',
							'CASH'	=> 'Cash',
							'CC'	=> 'Credit/Debit Card',
							'CC_SWP'=> 'Credit/Debit Card Swap',
							'CDC'	=> 'Client Deposited Cash',
							'CHQ'	=> 'Cheque',
                            'DD'   	=> 'Demad Draft',
							'PP'	=> 'Paypal',
							'TDSC'	=> 'TDS Certificate',
							'WIRE'	=> 'Wire Transfer',
                            'OTHER' => 'Other'
						);
			return ($mode);
		}
        
        function entryPayTransactionIn($data,$my){
         
                $db1 		= new db_local; // database handle
                $data['transaction_id'] = getCounterNumber($db1,CONTR_VCH,$data['company_id']);
                 
                $query	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION
                                ." SET ". TABLE_PAYMENT_TRANSACTION .".transaction_id       = '". $data['transaction_id'] ."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".company_id           = '". $data['company_id'] ."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".date                 = '". $variables['date'] ."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".user_id              = '". $my['uid'] ."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".transaction_type     = '". Paymenttransaction::PAYMENTIN."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".do_transaction       = '". $data['company_id']."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".account_head_id      = '' "
                                    .",". TABLE_PAYMENT_TRANSACTION .".party_id             = '' "
                                    .",". TABLE_PAYMENT_TRANSACTION .".party_name           = '' "                                
                                    .",". TABLE_PAYMENT_TRANSACTION .".mode_id              = '". $data['p_mode']."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt     = '". $data['amount_inr'] ."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".pay_cheque_no        = '". $data['pay_cheque_no']."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".do_pay_received      = '". $data['do_pay_received']."'"                                
                                    .",". TABLE_PAYMENT_TRANSACTION .".pay_bank_company     = '". $data['pay_bank_company']."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".pay_branch           = '". $data['pay_branch']."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id   = '". $data['company_id']."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_amt       = '". $data['credit_pay_amt'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".do_credit_trans      = '". $data['do_credit_trans'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_trans_no  = '". $data['company_id']."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".is_receipt           = '1'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".receipt_no           = '". $data['number']."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id    = '". $data['debit_pay_bank_id'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_amt        = '". $data['debit_pay_amt'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".do_debit_trans       = '". $data['do_debit_trans'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_trans_no   = '". $data['debit_pay_trans_no'] ."'"                                
                                    //.",". TABLE_PAYMENT_TRANSACTION .".is_bill_receive      = '". $data['is_bill_receive'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".bill_no              = '". $data['bill_no'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".voucher_no           = '". $data['voucher_no'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".particulars          = '". $data['particulars'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".remarks              = '". $data['remarks'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".is_pdc               = '". $data['is_pdc'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".do_pdc_chq           = '". $data['do_pdc_chq'] ."'"
                                    //.",". TABLE_PAYMENT_TRANSACTION .".pdc_chq_details      = '". $data['pdc_chq_details'] ."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".status               = '". Paymenttransaction::PENDING ."'";
             $db1->query($select_query);
             updateCounterOf($db1,CONTR_VCH,$data['company_id']);
        
        }
        
		/**
		 *	Function to get all the Receipts.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_BILL_RCPT;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }         
                        
            /*
            $query .= " FROM ". TABLE_BILL_RCPT ;
            $query .= " LEFT JOIN ". TABLE_BILL_INV
                        ." ON ". TABLE_BILL_INV.".number = ".TABLE_BILL_RCPT .".inv_no " 
                        ." LEFT JOIN ". TABLE_BILL_ORD_P
                        ." ON ". TABLE_BILL_ORD_P.".ord_no = ".TABLE_BILL_INV .".or_no " 
                        ." LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_RCPT .".created_by "
                        ." LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_RCPT .".client ";
            $query .= " ". $condition;
            */
            
           /*  $query .= " FROM ". TABLE_BILL_RCPT ;
            $query .= " LEFT JOIN ". TABLE_BILL_INV
                        ." ON ". TABLE_BILL_INV.".number = ".TABLE_BILL_RCPT .".inv_no " 
                        ." LEFT JOIN ". TABLE_BILL_ORDERS
                        ." ON ". TABLE_BILL_ORDERS.".number = ".TABLE_BILL_INV .".or_no " 
                        ." LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_RCPT .".created_by "
                        ." LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_RCPT .".client "; */
			
			$query .= " FROM ". TABLE_BILL_RCPT ;
            $query .= " LEFT JOIN ". TABLE_BILL_INV
                        ." ON ". TABLE_BILL_INV.".number = ".TABLE_BILL_RCPT .".inv_no " 
                        ." LEFT JOIN ". TABLE_BILL_ORDERS
                        ." ON ". TABLE_BILL_ORDERS.".number = ".TABLE_BILL_RCPT .".ord_no " 
                        ." LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_RCPT .".created_by "
                        ." LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_RCPT .".client ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   //This function is used to perform action on status.
		function updateStatus($id, $status_new, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

            // Check if status is valid or not.
            if ( in_array($status_new, Receipt::getStatus()) ) {
                $receipt = NULL;
                if ( (Receipt::getList($db, $receipt, 'id, access_level, created_by, status', " WHERE id = '$id'")) > 0 ) {
                    $receipt = $receipt[0];
					
					// Check Access Level for the receipt.
					if ( ($receipt['created_by'] == $my['user_id']) && ($receipt['access_level'] >= $access_level) ) {
						$messages->setErrorMessage("You do not have the Right to change Status with the current Access Level.");
					}
					elseif ( ($receipt['created_by'] != $my['user_id']) && ($receipt['access_level'] >= $access_level_ot) ) {
						$messages->setErrorMessage("Cannot change Status of Receipt created by others, with current Access Level.");
					}
					else {
                    	if ( $receipt['status'] != $status_new ) {
                            $query = "UPDATE ". TABLE_BILL_RCPT
                                        ." SET status = '$status_new' "
                                        ." WHERE id = '$id'";
                            if ( $db->query($query) && $db->affected_rows()>0 ) {
                                $messages->setOkMessage("The Status has been updated.");
                            }
                            else {
                                $messages->setErrorMessage("The Status was not updated.");
                            }
                        }
                        else {
                            $messages->setErrorMessage("Status not updated. New and Old status is same.");
                        }
                    }
                }
                else {
                    $messages->setErrorMessage("The Receipt was not found.");
                }
            }
            else {
                $messages->setErrorMessage("The Status is not a valid a Status.");
            }
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }

		function deleteProfmRcpt($id, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			$receipt = NULL;
			if ( (Receipt::getList($db, $receipt, 'id, number, amount,amount_inr, profm_invoice_opening_balance,profm_invoice_opening_balance_inr,ord_no,inv_no, 
				inv_profm_no, client,access_level, created_by, status,voucher_id', " WHERE id = '$id'")) > 0 ) {
				$receipt = $receipt[0];
				
				// Check Access Level for the receipt.
				//if ( ($receipt['created_by'] == $my['user_id']) && ($receipt['access_level'] >= $access_level) ) {
				//	$messages->setErrorMessage("You do not have the Right to Delete Receipt with the current Access Level.");
				//}
				//elseif ( ($receipt['created_by'] != $my['user_id']) && ($receipt['access_level'] >= $access_level_ot) ) {
				//	$messages->setErrorMessage("Cannot Delete the Receipt created by others, with current Access Level.");
				//}
				//else {
					if ( $receipt['status'] == Receipt::BLOCKED ) {
                        
                         $query = "UPDATE ". TABLE_BILL_INV_PROFORMA ." " 
						."SET ". TABLE_BILL_INV_PROFORMA.".balance =".($receipt['profm_invoice_opening_balance']).",
                        ".TABLE_BILL_INV_PROFORMA.".paid_status ='0',"
                        . TABLE_BILL_INV_PROFORMA.".balance_inr = ".($receipt['profm_invoice_opening_balance_inr'])
						." WHERE ". TABLE_BILL_INV_PROFORMA .".client='".$receipt['client']."' 
						AND ".TABLE_BILL_INV_PROFORMA.".number = '".$receipt['inv_profm_no']."' " ;                            
           
                        $db->query($query);
						if(!empty($receipt['inv_no'])){
							$query = "UPDATE ". TABLE_BILL_INV." " 
							."SET ".TABLE_BILL_INV.".status ='".INVOICE::PENDING."'                                 
							WHERE ". TABLE_BILL_INV .".client='".$receipt['client']."' 
							AND ".TABLE_BILL_INV.".number = '".$receipt['inv_no']."' " ;    
							$db->query($query);
						}
						if(!empty($receipt['ord_no'])){
							$query1 = "UPDATE ". TABLE_BILL_ORDERS." " 
							."SET ".TABLE_BILL_ORDERS.".paid_status ='0',
							".TABLE_BILL_ORDERS.".status = '".Receipt::ACTIVE."',
							".TABLE_BILL_ORDERS.".balance = ".($receipt['profm_invoice_opening_balance']).",
							".TABLE_BILL_ORDERS.".balance_inr = ".($receipt['profm_invoice_opening_balance_inr'])."
							WHERE ".TABLE_BILL_ORDERS.".number = '".$receipt['ord_no']."' " ;  
							$db->query($query1);
						}
						//Update the transaction
						$sql1= "SELECT receipt_no,proforma_invoice_no FROM ".TABLE_PAYMENT_TRANSACTION."  
						WHERE id =".$receipt['voucher_id']." LIMIT 0,1";
						$db->query($sql1); 
						if ( $db->nf() > 0 ) {
							while ($db->next_record()) {								 
								$receipt_no = $db->f('receipt_no') ;
								$receipt_no_txt = trim($receipt_no,",");
								$receipt_no_arr = explode(",",$receipt_no_txt); 
								$receipt_no_arr = array_flip($receipt_no_arr); 
								unset($receipt_no_arr[$receipt['number']]);
								if(!empty($receipt_no_arr)){
								    $receipt_no_arr = array_flip($receipt_no_arr); 
									$receipt_no_text = implode(",",$receipt_no_arr);
									$receipt_no_text = ",".$receipt_no_text ;
								}else{
									$receipt_no_text = '';
								}
								
								$proforma_invoice_no = $db->f('proforma_invoice_no') ;
								$proforma_invoice_no_txt = trim($proforma_proforma_invoice_no,",");
								$proforma_invoice_no_arr = explode(",",$proforma_invoice_no_txt); 
								$proforma_invoice_no_arr = array_flip($proforma_invoice_no_arr); 
								//unset($proforma_invoice_no_arr[$receipt['inv_no']]);
								unset($proforma_invoice_no_arr[$receipt['inv_profm_no']]);
								if(!empty($proforma_invoice_no_arr)){
								    $proforma_invoice_no_arr = array_flip($proforma_invoice_no_arr); 
									$proforma_invoice_no_text = implode(",",$proforma_invoice_no_arr);
									$proforma_invoice_no_text = ",".$proforma_invoice_no_text ;
								}else{
									$proforma_invoice_no_text = '';
								}								
							}
						}
						
						$sql_vh=" UPDATE ".TABLE_PAYMENT_TRANSACTION 
						." SET pay_received_amt_rcpt = ( pay_received_amt_rcpt + '".	$receipt['amount'] ."' ),"
                        ." pay_received_amt_rcpt_inr = ( pay_received_amt_rcpt_inr + '".$receipt['amount_inr'] ."'),"                        ." proforma_invoice_no = '".$proforma_invoice_no_text."',"
                        ." receipt_no = '".$receipt_no_text."' WHERE id =".$receipt['voucher_id'];
						$db->query($sql_vh);   
						
						$sql="SELECT pay_received_amt_rcpt,pay_received_amt
						FROM ".TABLE_PAYMENT_TRANSACTION." WHERE id =".$receipt['voucher_id'];
						$db->query( $sql );
						$pay_received_amt_rcpt_tr=0;
						if( $db->nf() > 0 ){
							while($db->next_record()){
								$pay_received_amt_rcpt_tr = $db->f('pay_received_amt_rcpt') ;
								$pay_received_amt_tr = $db->f('pay_received_amt') ;
							}
						}
						
						if($pay_received_amt_rcpt_tr == $pay_received_amt_tr ){
							$sql_vh1 = " UPDATE ".TABLE_PAYMENT_TRANSACTION." SET 
							linking_status ='".Paymenttransaction::BLANK."'  WHERE id =".$receipt['voucher_id'];
							$db->query( $sql_vh1 );	
						}elseif($pay_received_amt_rcpt_tr < $pay_received_amt_tr){
							$sql_vh1 = " UPDATE ".TABLE_PAYMENT_TRANSACTION." SET 
							linking_status 	='".Paymenttransaction::PARTIALLINK."' WHERE id =".$receipt['voucher_id'];
							$db->query( $sql_vh1 );	
						}
							
                        //$query = "UPDATE ". TABLE_BILL_RCPT." SET status='".Receipt::DELETED."'"
									//." WHERE id = '$id'";   
                                    
                        $query = "UPDATE ". TABLE_BILL_RCPT." SET status='".Receipt::DELETED."', 
                                    reason_of_delete ='".$data['reason_of_delete']."',
                                    delete_by ='".$my['uid']."',
                                    delete_ip ='".$_SERVER['REMOTE_ADDR']."',
                                    do_delete='".date("Y-m-d")."'
                                    WHERE id = '$id'";       
                        if ( $db->query($query) && $db->affected_rows() > 0 ) {                        
                        
                         		$messages->setOkMessage("The Receipt has been deleted.");
						}
						else {
							    $messages->setErrorMessage("The Receipt was not deleted.");
						}
                         
                         
					}
					else {
						$messages->setErrorMessage("To delete the Receipt, change its status to blocked.");
					}
				//}
			}
			else {
				$messages->setErrorMessage("The Receipt was not found.");
			}

			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
        function delete($id, $extra) {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}

			$receipt = NULL;
			if ( (Receipt::getList($db, $receipt, 'id, number, amount,amount_inr,invoice_opening_balance,invoice_opening_balance_inr, 
				inv_no, client,access_level, created_by, status,voucher_id', " WHERE id = '$id'")) > 0 ) {
				$receipt = $receipt[0];
				
				// Check Access Level for the receipt.
				//if ( ($receipt['created_by'] == $my['user_id']) && ($receipt['access_level'] >= $access_level) ) {
				//	$messages->setErrorMessage("You do not have the Right to Delete Receipt with the current Access Level.");
				//}
				//elseif ( ($receipt['created_by'] != $my['user_id']) && ($receipt['access_level'] >= $access_level_ot) ) {
				//	$messages->setErrorMessage("Cannot Delete the Receipt created by others, with current Access Level.");
				//}
				//else {
					if ( $receipt['status'] == Receipt::BLOCKED ) {
                        /*
                        $query = "UPDATE ". TABLE_BILL_INV ." " 
							."SET ". TABLE_BILL_INV.".balance = "
									. TABLE_BILL_INV.".balance + ". floatval($receipt['amount']) ." ,"
                                    . TABLE_BILL_INV.".status ='".INVOICE::PENDING."'"
                                    . TABLE_BILL_INV.".balance_inr = "
									. TABLE_BILL_INV.".balance_inr + ". floatval($receipt['amount_inr']) ." "
							." WHERE ". TABLE_BILL_INV .".client='".$receipt['client']."' AND ".TABLE_BILL_INV.".number = '".$receipt['inv_no']."' " ;                            
                        
                        */
                         $query = "UPDATE ". TABLE_BILL_INV ." " 
							."SET ". TABLE_BILL_INV.".balance =".($receipt['invoice_opening_balance']).",
                                    ".TABLE_BILL_INV.".status ='".INVOICE::PENDING."',"
                                    . TABLE_BILL_INV.".balance_inr = ".($receipt['invoice_opening_balance_inr'])
							." WHERE ". TABLE_BILL_INV .".client='".$receipt['client']."' AND 
							".TABLE_BILL_INV.".number = '".$receipt['inv_no']."' " ;                            
           
                        $db->query($query);
						
						//Update the transaction
						$sql1= "SELECT receipt_no,invoice_no FROM ".TABLE_PAYMENT_TRANSACTION."  WHERE 
						id =".$receipt['voucher_id']." LIMIT 0,1";
						$db->query($sql1); 
						if ( $db->nf() > 0 ) {
							while ($db->next_record()) {								 
								$receipt_no = $db->f('receipt_no') ;
								$receipt_no_txt = trim($receipt_no,",");
								$receipt_no_arr = explode(",",$receipt_no_txt); 
								$receipt_no_arr = array_flip($receipt_no_arr); 
								unset($receipt_no_arr[$receipt['number']]);
								if(!empty($receipt_no_arr)){
								    $receipt_no_arr = array_flip($receipt_no_arr); 
									$receipt_no_text = implode(",",$receipt_no_arr);
									$receipt_no_text = ",".$receipt_no_text ;
								}else{
									$receipt_no_text = '';
								}
								
								$invoice_no = $db->f('invoice_no') ;
								$invoice_no_txt = trim($invoice_no,",");
								$invoice_no_arr = explode(",",$invoice_no_txt); 
								$invoice_no_arr = array_flip($invoice_no_arr); 
								unset($invoice_no_arr[$receipt['inv_no']]);
								if(!empty($invoice_no_arr)){
								    $invoice_no_arr = array_flip($invoice_no_arr); 
									$invoice_no_text = implode(",",$invoice_no_arr);
									$invoice_no_text = ",".$invoice_no_text ;
								}else{
									$invoice_no_text = '';
								}
							}
						}
						
						$sql_vh=" UPDATE ".TABLE_PAYMENT_TRANSACTION 
									." SET pay_received_amt_rcpt = ( pay_received_amt_rcpt + '".	$receipt['amount'] ."' ),"
                                    ." pay_received_amt_rcpt_inr = ( pay_received_amt_rcpt_inr + '".$receipt['amount_inr'] ."'),"                                    ." invoice_no = '".$invoice_no_text."',"
                                    ." receipt_no = '".$receipt_no_text."' WHERE id =".$receipt['voucher_id'];
						$db->query($sql_vh);   
                    	
						$sql="SELECT pay_received_amt_rcpt,pay_received_amt
						FROM ".TABLE_PAYMENT_TRANSACTION." WHERE id =".$receipt['voucher_id'];
						$db->query( $sql );
						$pay_received_amt_rcpt_tr=0;
						if( $db->nf() > 0 ){
							while($db->next_record()){
								$pay_received_amt_rcpt_tr = $db->f('pay_received_amt_rcpt') ;
								$pay_received_amt_tr = $db->f('pay_received_amt') ;
							}
						}
						
						if($pay_received_amt_rcpt_tr == $pay_received_amt_tr ){
							$sql_vh1 = " UPDATE ".TABLE_PAYMENT_TRANSACTION." SET 
							linking_status ='".Paymenttransaction::BLANK."'  WHERE id =".$receipt['voucher_id'];
							$db->query( $sql_vh1 );	
						}elseif($pay_received_amt_rcpt_tr < $pay_received_amt_tr){
							$sql_vh1 = " UPDATE ".TABLE_PAYMENT_TRANSACTION." SET 
							linking_status 	='".Paymenttransaction::PARTIALLINK."' WHERE id =".$receipt['voucher_id'];
							$db->query( $sql_vh1 );	
						}
						
                        //$query = "UPDATE ". TABLE_BILL_RCPT." SET status='".Receipt::DELETED."'"
									//." WHERE id = '$id'";   
                                    
                        $query = "UPDATE ". TABLE_BILL_RCPT." SET status='".Receipt::DELETED."', 
                                    reason_of_delete ='".$data['reason_of_delete']."',
                                    delete_by ='".$my['uid']."',
                                    delete_ip ='".$_SERVER['REMOTE_ADDR']."',
                                    do_delete='".date("Y-m-d")."'
                                    WHERE id = '$id'";       
                        if ( $db->query($query) && $db->affected_rows() > 0 ) {                        
                        
                         		$messages->setOkMessage("The Receipt has been deleted.");
						}
						else {
							    $messages->setErrorMessage("The Receipt was not deleted.");
						}
                         
                        // Update the Invoice by deducting the Receipt Amount.
                        /*
                        $query = "UPDATE ". TABLE_BILL_INV ." " 
							."SET ". TABLE_BILL_INV.".balance = "
										. TABLE_BILL_INV.".balance + ". floatval($receipt['amount']) ." ,"
                                    . TABLE_BILL_INV.".balance_inr = "
										. TABLE_BILL_INV.".balance_inr + ". floatval($receipt['amount_inr']) ." "
							." WHERE ". TABLE_BILL_INV .".client='".$receipt['client']."' AND ".TABLE_BILL_INV.".number = '".$receipt['inv_no']."' " ;                            
                        $db->query($query);
                        
						$query = "DELETE FROM ". TABLE_BILL_RCPT
									." WHERE id = '$id'";                                    
                                   
						if ( $db->query($query) && $db->affected_rows()>0 ) {
                           
                            $query = "DELETE FROM ". TABLE_BILL_RCPT_B
                                        ." WHERE  rcpt_no = '".$receipt['number']."'" ;                                
                            $db->query($query);
                            
                            $query = "DELETE FROM ". TABLE_BILL_RCPT_P
                                    ." WHERE  rcpt_no = '".$receipt['number']."'" ;                                
                            $db->query($query);
                           
                            //code to delete the file bof
                            //$file_name = DIR_FS_RCPT_FILES ."/". $receipt["number"] .".html";
                            //unlink( $file_name );
                            //code to delete the file eof 
                                    
							$messages->setOkMessage("The Receipt has been deleted.");
						}
						else {
							$messages->setErrorMessage("The Receipt was not deleted.");
						}
                        */
					}
					else {
						$messages->setErrorMessage("To delete the Receipt, change its status to blocked.");
					}
				//}
			}
			else {
				$messages->setErrorMessage("The Receipt was not found.");
			}

			
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
           
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creators' status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }
           /*
  		    if(empty($data['financialYr'])){
                 $messages->setErrorMessage("Select Financial Yr.");
            } 
			*/
			if($data['company_id']!= $data['voucher_company_id']){
				$messages->setErrorMessage("Check invoice Company id and Voucher Company id.");
			}
			/*
            if(!empty($data['financialYr']) && !empty($data['company_id']) ){
            
                $_ALL_POST['number'] =$data['number']= getOldCounterNumber($db,CONTR_RPT,$data['company_id'],$data['financialYr']); 
                //$_ALL_POST['number'] =$data['number']= getCounterNumber($db,'ORD',$data['company_id']); 
                if(empty($data['number']) ){
                    $messages->setErrorMessage("Receipt Number was not generated.");
                }
            }else{
                $messages->setErrorMessage(" Financial Year and Company Name are not Found.");
            }*/
			
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Client for whom the Receipt is being Created.");
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, billing_name,status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            
            // Check the billing name.
          
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
               $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
    
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
			
			if(empty($data['voucher'])){
                 $messages->setErrorMessage("Select Payment Voucher.");
            }
            // Format the Date of Receipt.
            if ( !isset($data['do_r']) || empty($data['do_r']) ) {
                $messages->setErrorMessage("The Date of Receipt is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_r']) ) {
                $messages->setErrorMessage("Format of the Date of Receipt is invalid.");
            }
            else {
                $data['do_r'] = explode('/', $data['do_r']);
                //generate invoice number on the basis of invoice date
                //$data['number'] = "PT". $data['do_r'][2]."-RCT-".$data['do_r'][1]. $data['do_r'][0].date("hi-s") ;
                $data['do_r'] = mktime(0, 0, 0, $data['do_r'][1], $data['do_r'][0], $data['do_r'][2]);
            }
			if ( !isset($data['do_transaction']) || empty($data['do_transaction']) ) {
                $messages->setErrorMessage("The Date of Transaction Dt is not specified.");
            }elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_transaction']) ) {
                $messages->setErrorMessage("Format of the Transaction Dt is invalid.");
            }else {
                $data['do_transaction'] = explode('/', $data['do_transaction']);
                //generate invoice number on the basis of invoice date                
 $data['do_transaction'] = mktime(0, 0, 0, $data['do_transaction'][1], $data['do_transaction'][0], $data['do_transaction'][2]);
            }
			
			if ( !isset($data['do_voucher']) || empty($data['do_voucher']) ) {
                $messages->setErrorMessage("The Date of Transaction Dt is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_voucher']) ) {
                $messages->setErrorMessage("Format of the Transaction Dt is invalid.");
            }else {
                $data['do_voucher'] = explode('/', $data['do_voucher']);
                //generate invoice number on the basis of invoice date               
 $data['do_voucher'] = mktime(0, 0, 0, $data['do_voucher'][1], $data['do_voucher'][0], $data['do_voucher'][2]);
            }
			 
            $data['number']=$data['rcpt_counter'] ='';
			if(!empty($data['company_id']) && !empty($data['do_r'])){				
				$detailRcptNo = getReceiptNumber($data['do_r'],$data['company_id']); 				
				if(!empty($detailRcptNo)){
					$data['number'] = $detailRcptNo['number'];
					$data['rcpt_counter'] = $detailRcptNo['rcpt_counter'];
				}
			}
            if(empty($data['p_mode'])){
                $messages->setErrorMessage("Please Select Payment Mode.");
            }
			
            if($data['p_mode']=="1"||$data['p_mode']=="3" || $data['p_mode']=="4" || $data['p_mode']=="5" || $data['p_mode']=="6" 
			|| $data['p_mode']=="7" || $data['p_mode']=="8" ){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque/DD/Trans./Card No. can not be empty.");
				}else{
					/* if(!is_numeric($data['pay_cheque_no'])){					
						 $messages->setErrorMessage("Pay Cheque/DD/Trans./Card No. should be numeric value.");
					} */
				}
				/*
				if(empty($data['do_pay_received'])){
				
					$messages->setErrorMessage("Cheque/DD/Trans./Card date should not be blank.");
				}
				if(empty($data['pay_bank_company'])){
				
					$messages->setErrorMessage("Bank name should be required.");
				}
				if(empty($data['pay_branch'])){				
					$messages->setErrorMessage("Branch should be required.");
				}*/
				
			}
            if(!empty($data['voucher']) && !empty($data['inv_id']) ){
				$query = "SELECT id FROM ". TABLE_BILL_RCPT
                         ." WHERE voucher_id = '". $data['voucher'] ."' AND inv_id = '". $data['inv_id'] ."' 
						  AND status != '".Receipt::DELETED."'";
                if ( ($db->query($query)) && ($db->nf() > 0) ) {
                    $messages->setErrorMessage("The transaction ".$data['voucher_no']." and Invoice no ".$data['inv_no']." are already linked.");
                }
			}			
            if(!empty($data['do_pay_received'])){
                $data['do_pay_received'] = explode('/', $data['do_pay_received']);
                $data['do_pay_received'] = mktime(0, 0, 0, $data['do_pay_received'][1], $data['do_pay_received'][0], $data['do_pay_received'][2]);
                $data['do_pay_received'] = date('Y-m-d H:i:s', $data['do_pay_received']);
            }else{
                $data['do_pay_received'] ='';
            }
            
			
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            elseif ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Receipt Amount is not valid.");
            }
			
            if ( $data['currency_id'] != $data['vcurrency_id']  ) {
                $messages->setErrorMessage("Please check the Voucher /Transaction Currency and Invoice Currency");
            }
			$data['voucher_amount']= (float) $data['voucher_amount'];
			if ( $data['amount'] > $data['voucher_amount']  ) {
                $messages->setErrorMessage("Receipt Amount is greater than Voucher Amount.");
            }
			
            if ( $data['amount'] > $data['balance']  ) {
                $messages->setErrorMessage("Receipt Amount is greater than balance Invoice amount.");
            }
            
           
            
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Receipt Amount in INR is not valid.");
                }
            }
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                $messages->setErrorMessage("The Amount in words is not specified.");
            }else{
            
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
               
            }
            
            
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}

			// Check the payment mode.
            /*
			$p_mode_list = Receipt::getPaymentMode();
			if ( !array_key_exists($data['p_mode'], $p_mode_list) ) {
				$messages->setErrorMessage("The selected Payment Mode in not valid.");
			}
            */
            
            // Check for the duplicate Receipt Number.
            $list = NULL;
            if ( Receipt::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("A Receipt with the same Number already exists.");
            }
            $list = NULL;
            
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		 /**
		 * Function to validate the input with the details of Proforma Invoice 
		 * from the User while Adding.		
		 * @param	array	The array containing the information to be added.
		 * 2011-08-aug-11
		 *
		 *
		 */
		
		function validateAddPI(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
         
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage("Creator details cannot be retrieved, Please try again.");
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The Creator was not found.");
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Creators' status is not Active.");
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }
            /* if(empty($data['financialYr'])){
                 $messages->setErrorMessage("Select Financial Yr.");
            } */
			if($data['company_id']!= $data['voucher_company_id']){
				$messages->setErrorMessage("Check Proforma invoice Company id and Voucher Company id.");
			}
			/*
            if(!empty($data['financialYr']) && !empty($data['company_id']) ){
            
                $_ALL_POST['number'] =$data['number']= getOldCounterNumber($db,CONTR_RPT,$data['company_id'],$data['financialYr']); 
                //$_ALL_POST['number'] =$data['number']= getCounterNumber($db,'ORD',$data['company_id']); 
                if(empty($data['number']) ){
                    $messages->setErrorMessage("Receipt Number was not generated.");
                }
            }else{
                $messages->setErrorMessage(" Financial Year and Company Name are not Found.");
            }*/
			
            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage("Select the Client for whom the Receipt is being Created.");
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, billing_name,status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            
            // Check the billing name.
          
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
               $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
    
            }
            
            if ( !isset($data['billing_address']) || empty($data['billing_address']) ) {
                $messages->setErrorMessage("Please select the Billing Address.");
            }
            else {
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS, $data['client']['user_id']);
                if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                    $messages->setErrorMessage("The selected billing address was not found.");
                }
            }
            
			
			if(empty($data['voucher'])){
                 $messages->setErrorMessage("Select Payment Voucher.");
            }
            // Format the Date of Receipt.
            if ( !isset($data['do_r']) || empty($data['do_r']) ) {
                $messages->setErrorMessage("The Date of Receipt is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_r']) ) {
                $messages->setErrorMessage("Format of the Date of Receipt is invalid.");
            }
            else {
                $data['do_r'] = explode('/', $data['do_r']);
                //generate invoice number on the basis of invoice date
                //$data['number'] = "PT". $data['do_r'][2]."-RCT-".$data['do_r'][1]. $data['do_r'][0].date("hi-s") ;
                $data['do_r'] = mktime(0, 0, 0, $data['do_r'][1], $data['do_r'][0], $data['do_r'][2]);
            }
			if ( !isset($data['do_transaction']) || empty($data['do_transaction']) ) {
                $messages->setErrorMessage("The Date of Transaction Dt is not specified.");
            }elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_transaction']) ) {
                $messages->setErrorMessage("Format of the Transaction Dt is invalid.");
            }else {
                $data['do_transaction'] = explode('/', $data['do_transaction']);
                //generate invoice number on the basis of invoice date                
 $data['do_transaction'] = mktime(0, 0, 0, $data['do_transaction'][1], $data['do_transaction'][0], $data['do_transaction'][2]);
            }
			
			if ( !isset($data['do_voucher']) || empty($data['do_voucher']) ) {
                $messages->setErrorMessage("The Date of Voucher is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_voucher']) ) {
                $messages->setErrorMessage("Format of the Voucher Dt is invalid.");
            }else {
                $data['do_voucher'] = explode('/', $data['do_voucher']);
                //generate invoice number on the basis of invoice date               
 $data['do_voucher'] = mktime(0, 0, 0, $data['do_voucher'][1], $data['do_voucher'][0], $data['do_voucher'][2]);
            }
			 
            $data['number']=$data['rcpt_counter'] ='';
			if(!empty($data['company_id']) && !empty($data['do_r'])){
				 
				$detailRcptNo = getReceiptNumber($data['do_r'],$data['company_id']); 				
				if(!empty($detailRcptNo)){
					$data['number'] = $detailRcptNo['number'];
					$data['rcpt_counter'] = $detailRcptNo['rcpt_counter'];
				}
			}
            if(empty($data['p_mode'])){
                $messages->setErrorMessage("Please Select Payment Mode.");
            }
			
            /* if($data['p_mode']=="1"||$data['p_mode']=="3" || $data['p_mode']=="4" || $data['p_mode']=="5" || $data['p_mode']=="6" 
			|| $data['p_mode']=="7" || $data['p_mode']=="8" ){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque/DD/Trans./Card No. can not be empty.");
				}else{
					if(!is_numeric($data['pay_cheque_no'])){					
						 $messages->setErrorMessage("Pay Cheque/DD/Trans./Card No. should be numeric value.");
					} 
				}
				
				if(empty($data['do_pay_received'])){
				
					$messages->setErrorMessage("Cheque/DD/Trans./Card date should not be blank.");
				}
				if(empty($data['pay_bank_company'])){
				
					$messages->setErrorMessage("Bank name should be required.");
				}
				if(empty($data['pay_branch'])){				
					$messages->setErrorMessage("Branch should be required.");
				}
				
			} */
			if(!empty($data['voucher']) && !empty($data['inv_profm_id']) ){
				$query = "SELECT id FROM ". TABLE_BILL_RCPT
                        ." WHERE voucher_id = '". $data['voucher'] ."' AND 
						inv_profm_id = '". $data['inv_profm_id'] ."' 
						AND status != '".Receipt::DELETED."'";
                if ( ($db->query($query)) && ($db->nf() > 0) ) {
                    $messages->setErrorMessage("The transaction ".$data['voucher_no']." and Proforma Invoice no ".$data['inv_profm_no']." are already linked.");
                }
			}
            if(!empty($data['do_pay_received'])){
                $data['do_pay_received'] = explode('/', $data['do_pay_received']);
                $data['do_pay_received'] = mktime(0, 0, 0, $data['do_pay_received'][1], $data['do_pay_received'][0], $data['do_pay_received'][2]);
                $data['do_pay_received'] = date('Y-m-d H:i:s', $data['do_pay_received']);
            }else{
                $data['do_pay_received'] ='';
            }
            
			
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            elseif ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Receipt Amount is not valid.");
            }
			
            if ( $data['currency_id'] != $data['vcurrency_id']  ) {
                $messages->setErrorMessage("Please check the Voucher /Transaction Currency and Proforma Invoice Currency");
            }
			$data['voucher_amount']= (float) $data['voucher_amount'];
			if ( $data['amount'] > $data['voucher_amount']  ) {
                $messages->setErrorMessage("Receipt Amount is greater than Voucher Amount.");
            }
			
            if ( $data['amount'] > $data['balance']  ) {
                $messages->setErrorMessage("Receipt Amount is greater than balance Proforma Invoice amount.");
            }
            
           
            
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Receipt Amount in INR is not valid.");
                }
            }
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                $messages->setErrorMessage("The Amount in words is not specified.");
            }else{
            
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
               
            }
            
            
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
				$messages->setErrorMessage("The Status is not valid.");
			}

			// Check the payment mode.
            /*
			$p_mode_list = Receipt::getPaymentMode();
			if ( !array_key_exists($data['p_mode'], $p_mode_list) ) {
				$messages->setErrorMessage("The selected Payment Mode in not valid.");
			}
            */
            
            // Check for the duplicate Receipt Number.
            $list = NULL;
            if ( Receipt::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("A Receipt with the same Number already exists.");
            }
            $list = NULL;
            
            //$messages->setErrorMessage("Do not execute.");
            
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		
		/**
		 * Function to validate the input with the details of Proforma Invoice 
		 * 2011-08-aug-11
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateUpdatePI(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // Validate the Order.
            $list = NULL;
            if ( Receipt::getList($db, $list, 'id, created_by', " WHERE number = '". $data['number'] ."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_BILL_ORDERS
                                ." WHERE number = '". $data['or_no'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage('The Order was not found in the database.');
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            if ( $db->f('status') != COMPLETED ) {
                                $messages->setErrorMessage('The Order has not been marked as completed.');
                            }
                            $data['client'] = $db->f('client');
                        }
                        else {
                            $messages->setErrorMessage('You donot have the Right to update Invoice for this Order.');
                        }
                    }
                }
            }
            else {
                $messages->setErrorMessage('The Proforma Invoice for the Order has not been created yet.');
            }
            
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage('Creator details cannot be retrieved, Please try again.');
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage('The Creator was not found.');
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage('The Creators\' status is not Active.');
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }

            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage('Select the Client for whom the Proforma Invoice is being Created.');
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, billing_name, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
               $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
    
            }
            if ( !isset($data['do_r']) || empty($data['do_r']) ) {
                $messages->setErrorMessage("The Date of Receipt is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_r']) ) {
                $messages->setErrorMessage("Format of the Date of Receipt is invalid.");
            }
            else {
                $data['do_r'] = explode('/', $data['do_r']);
                //generate invoice number on the basis of invoice date
                //$data['number'] = "PT". $data['do_r'][2]."-RCT-".$data['do_r'][1]. $data['do_r'][0].date("hi-s") ;
                $data['do_r'] = mktime(0, 0, 0, $data['do_r'][1], $data['do_r'][0], $data['do_r'][2]);
            }
            
            if(empty($data['p_mode'])){
                $messages->setErrorMessage("Please Select Payment Mode.");
            }
            if($data['p_mode']!="1"){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					if(!is_numeric($data['pay_cheque_no'])){					
						 $messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					}
				}
			}
            if(!empty($data['do_pay_received'])){
                $data['do_pay_received'] = explode('/', $data['do_pay_received']);
                $data['do_pay_received'] = mktime(0, 0, 0, $data['do_pay_received'][1], $data['do_pay_received'][0], $data['do_pay_received'][2]);
                $data['do_pay_received'] = date('Y-m-d H:i:s', $data['do_pay_received']);
            }else{
                $data['do_pay_received'] ='';
            }
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Receipt Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Receipt Amount in INR is not valid.");
                }
            }         
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                $messages->setErrorMessage("The Amount in words is not specified.");
            }else{
            
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
            
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                $messages->setErrorMessage("The Status is not valid.");
            }

            
            // Check for the duplicate Invoice Number.
            /*
            $list = NULL;
            if ( Receipt::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Invoice with the same Number already exists.");
            }
            */
            $list = NULL;
            
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
         
		/**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }

            // Validate the Order.
            $list = NULL;
            if ( Receipt::getList($db, $list, 'id, created_by', " WHERE number = '". $data['number'] ."'")>0 ) {
                $data['created_by'] = $list[0]['created_by'];
                
                if ( isset($data['or_no']) && !empty($data['or_no']) ) {
                    $query = "SELECT client, access_level, status FROM ". TABLE_BILL_ORDERS
                                ." WHERE number = '". $data['or_no'] ."'";
                    if ( $db->query($query) && $db->nf() <= 0 ) {
                        $messages->setErrorMessage('The Order was not found in the database.');
                    }
                    else {
                        $db->next_record();
                        if ( $access_level > $db->f('access_level') ) {
                            if ( $db->f('status') != COMPLETED ) {
                                $messages->setErrorMessage('The Order has not been marked as completed.');
                            }
                            $data['client'] = $db->f('client');
                        }
                        else {
                            $messages->setErrorMessage('You donot have the Right to update Invoice for this Order.');
                        }
                    }
                }
            }
            else {
                $messages->setErrorMessage('The Invoice for the Order has not been created yet.');
            }
            
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $messages->setErrorMessage('Creator details cannot be retrieved, Please try again.');
            }
            else {
                $query = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $data['created_by'] ."'"
                            ." OR username = '". $data['created_by'] ."'"
                            ." OR number = '". $data['created_by'] ."'" ;
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage('The Creator was not found.');
                }
                else {
                    $db->next_record();
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage('The Creators\' status is not Active.');
                    }
                    else {
                        $data['creator'] = processSQLData($db->result());
                    }
                }
            }

            if ( !isset($data['client']) || empty($data['client']) ) {
                $messages->setErrorMessage('Select the Client for whom the Invoice is being Created.');
            }
            else {
                $query = "SELECT user_id, number, manager, f_name, l_name, email, billing_name, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $data['client'] ."'";
                if ( ($db->query($query)) && ($db->nf() <= 0) ) {
                    $messages->setErrorMessage("The selected Client was not found.");
                }
                else {
                    $db->next_record();
                    //print_r($db->result());
                    if ( $db->f('status') != ACTIVE ) {
                        $messages->setErrorMessage("The Client is not Active.");
                    }
                    else {
                        $data['client'] = processSQLData($db->result());
                        $data['billing_name'] = $db->f('billing_name') ;
                    }
                }
            }
            if ( !isset($data['billing_name']) || empty($data['billing_name']) ) {
               $messages->setErrorMessage("Billing Name is not specified in the Selected Client.<br/> Please update the Billing Name in Client's Profile");
    
            }
            if ( !isset($data['do_r']) || empty($data['do_r']) ) {
                $messages->setErrorMessage("The Date of Receipt is not specified.");
            }
            elseif ( !preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data['do_r']) ) {
                $messages->setErrorMessage("Format of the Date of Receipt is invalid.");
            }
            else {
                $data['do_r'] = explode('/', $data['do_r']);
                //generate invoice number on the basis of invoice date
                //$data['number'] = "PT". $data['do_r'][2]."-RCT-".$data['do_r'][1]. $data['do_r'][0].date("hi-s") ;
                $data['do_r'] = mktime(0, 0, 0, $data['do_r'][1], $data['do_r'][0], $data['do_r'][2]);
            }
            
            if(empty($data['p_mode'])){
                $messages->setErrorMessage("Please Select Payment Mode.");
            }
            if($data['p_mode']!="1"){   

				if ( !isset($data['pay_cheque_no']) || empty($data['pay_cheque_no']) ) {
					$messages->setErrorMessage("Cheque No. can not be empty.");
				}else{
					/* if(!is_numeric($data['pay_cheque_no'])){					
						 $messages->setErrorMessage("Pay Cheque No. should be numeric value.");
					} */
				}
			}
            if(!empty($data['do_pay_received'])){
                $data['do_pay_received'] = explode('/', $data['do_pay_received']);
                $data['do_pay_received'] = mktime(0, 0, 0, $data['do_pay_received'][1], $data['do_pay_received'][0], $data['do_pay_received'][2]);
                $data['do_pay_received'] = date('Y-m-d H:i:s', $data['do_pay_received']);
            }else{
                $data['do_pay_received'] ='';
            }
            
            if ( !isset($data['amount']) || empty($data['amount']) ) {
                $messages->setErrorMessage("The Amount is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount']) ) {
                    $messages->setErrorMessage("The Receipt Amount is not valid.");
                }
                else {
                    $data['balance'] = $data['amount'];
                }
            }
            
            if ( !isset($data['amount_inr']) || empty($data['amount_inr']) ) {
                $messages->setErrorMessage("The Amount in INR is not specified.");
            }
            else {
                if ( !preg_match('/^([0-9]\d*)(\.\d{1,2})?$/', $data['amount_inr']) ) {
                    $messages->setErrorMessage("The Receipt Amount in INR is not valid.");
                }
            }         
            
            if ( !isset($data['amount_words']) || empty($data['amount_words']) ) {
                $messages->setErrorMessage("The Amount in words is not specified.");
            }else{
            
                if(!Validation::isAlphabets($data['amount_words'])){
             
                     $messages->setErrorMessage("The Amount in words is not valid.");
                }
                
            }
            
            
            if ( !isset($data['access_level']) ) {
                $messages->setErrorMessage("Select the Access Level.");
            }
            elseif ( !(isPresentAccessLevel($db, $data['access_level'])) ) {
                $messages->setErrorMessage("Selected Access Level not found.");
            }
            
            if ( $data['status'] != PENDING && $data['status'] != ACTIVE 
                    && $data['status'] != BLOCKED && $data['status'] != DELETED ) {
                $messages->setErrorMessage("The Status is not valid.");
            }

            
            // Check for the duplicate Invoice Number.
            /*
            $list = NULL;
            if ( Receipt::getList($db, $list, 'id', " WHERE number = '". $data['number'] ."'")>0 ) {
                $messages->setErrorMessage("An Invoice with the same Number already exists.");
            }
            */
            $list = NULL;
            
            //$messages->setErrorMessage("Do not execute.");
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for 
         * each Invoice.
         *
         */
        function setOrderStatus(&$db, $id, $status) {
            $query = "UPDATE ". TABLE_BILL_ORDERS
                        ." SET status = '$status'"
                        ." WHERE ( id = '$id' OR number = '$id' )";
            if ( $db->query($query) ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
        /**
         * This function is called after a new Invoice has been created
         * against an Order. This function is called only once for
         * each Invoice so that the service provided is updated in the
         * Members Account.
         *
         */
        function UpdateClientService(&$db, $client_id, $services) {
            $return     = false;
            $c_service  = 'NULL';
            
            $query = "SELECT service_id FROM ". TABLE_CLIENTS
                        ." WHERE user_id = '". $client_id ."'";
            if ( $db->query($query) && $db->nf()>0 && $db->next_record() ) {
                $c_service = processSQLData($db->f('service_id'));
                if ( empty($c_service) ) {
                    $c_service = array();
                }
                else {
                    $c_service = explode(',', $c_service);
                }
                
                $c_service = arrayCombine($c_service, $services);
                $c_service = implode(',', $c_service);
                $query = "UPDATE ". TABLE_CLIENTS
                            ." SET service_id = '$c_service'"
                            ." WHERE user_id = '$client_id'";
                if ( $db->query($query) ) {
                    $return = true;
                }
            }
            return ($return);
        }

        /**
         * This function is used to create the File in PDF or HTML format for the Invoice.
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createFile($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("bill-receipt-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_RPT_HTML_FILES, 0777);
                if ( is_writable(DIR_FS_RPT_HTML_FILES) ) {
                    $file_name = DIR_FS_RPT_HTML_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Receipt was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Receipt is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
            
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
        
        /**
         *  This function is used While migrating the old data of receipt 2009-04-apr-08
         * This function is used to create the File in PDF or HTML format for the Receipt.
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createFileOld($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("bill-receipt-old-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_RPT_FILES, 0777);
                if ( is_writable(DIR_FS_RPT_FILES) ) {
                    $file_name = DIR_FS_RPT_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Receipt was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Receipt is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
            
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
        
         /**
         *  This function is used While migrating the old data of receipt 2009-04-apr-08
         * This function is used to create the File in PDF or HTML format for the Receipt.
         *
         * @param   array       the array containing the Invoice data.
         * @param   string      the string denoting what file is to be created.
         * @param   array       array containing other additional data.
         * 
         * @return  boolean     true, if the file was created.
         *                      false, if the file was not created.
         */
        function createMigrateReceipt($data, $type='HTML', $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            
            if ( $type == 'HTML' ) {
                $s->assign('data', $data);
                $file_data = $s->fetch("bill-migrate-receipt-tpl.html");
                
                // Create a new file and save to disk.
                $old_umask = umask(0);
                @chmod(DIR_FS_RCPT_FILES, 0777);
                if ( is_writable(DIR_FS_RCPT_FILES) ) {
                    $file_name = DIR_FS_RCPT_FILES ."/". $data["number"] .".html";
                    $hwnd = fopen( $file_name, "w" );
                    if ( !fwrite($hwnd, $file_data, strlen($file_data)) ) {
                        $messages->setErrorMessage("The Receipt was not stored in file.");
                    }
                    else {
                        @chmod($file_name, 0777);
                    }
                }
                else {
                    $messages->setErrorMessage("The Directory to store Receipt is not writable.");
                }
                umask($old_umask);
            }
            elseif ( $type == 'PDF' ) {
            
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return ($file_name);
            }
            else {
                return (false);
            }
        }
        
        function getParticulars( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_ORD_P;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }
        
        
        function getBifurcation( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_BILL_RCPT_B;
            
            $query .= " ". $condition;
    
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }
        }

		
		/**
		 * This function is used to retrieve the details of the invoice.
		 *
		 */
		function getInvoice($inv_id, &$invoice, &$extra) {
            include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
			
			foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
            $condition_query = " WHERE (". TABLE_BILL_INV .".id = '". $inv_id ."' "
                                ." OR ". TABLE_BILL_INV .".number = '". $inv_id ."')";
            $fields = TABLE_BILL_INV .'.*'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.status AS c_status';
						
			if ( Invoice::getDetails($db, $invoice, $fields, $condition_query) > 0 ) {
                $invoice = $invoice['0'];

				// Set up the Client Details field.
				$invoice['client_details'] = $invoice['c_f_name'] .' '. $invoice['c_l_name']
												.' ('. $invoice['c_number'] .')'
												.' ('. $invoice['c_email'] .')';
				
				// Set up the dates.
//				$invoice['do_i']  = explode(' ', $invoice['do_i']);
//				$temp               = explode('-', $invoice['do_i'][0]);
//				$invoice['do_i']  = NULL;
//				$invoice['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
//				
//				$invoice['do_d']  = explode(' ', $invoice['do_d']);
//				$temp               = explode('-', $invoice['do_d'][0]);
//				$invoice['do_d']  = NULL;
//				$invoice['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
//				
//				$invoice['do_e']  = explode(' ', $invoice['do_e']);
//				$temp               = explode('-', $invoice['do_e'][0]);
//				$invoice['do_e']  = NULL;
//				$invoice['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				
				// Read the Particulars.
				$temp = NULL;
				$temp_p = NULL;
				$condition_query = "WHERE ord_no = '". $invoice['or_no'] ."'";
				Invoice::getParticulars($db, $temp, '*', $condition_query);
				foreach ( $temp as $pKey => $parti ) {
					
                   if($temp[$pKey]['s_type']=='1'){
                        $temp[$pKey]['s_type']='Years';
                   }elseif($temp[$pKey]['s_type']=='2'){
                        $temp[$pKey]['s_type']='Quantity';
                   }
                    $temp_p[]=array(
                                    'p_id'=>$temp[$pKey]['id'],
                                    'particulars'=>$temp[$pKey]['particulars'],
                                    'p_amount' =>number_format($temp[$pKey]['p_amount'] ,2),                                   
                                    's_type' =>$temp[$pKey]['s_type'] ,                                   
                                    's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                    's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
                                    's_id' =>$temp[$pKey]['s_id'] ,                                   
                                    'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                    'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                    'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                    'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
                                    'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                    'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                    'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,                                   
                                    'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
                                    'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
                                    'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
                                    'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                    'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                                    );
				}
                $invoice['particular_details'] = $temp_p ;
				$condition_query = '';
				
				// Read the Bifurcations.
                /*
				$temp = NULL;
				$condition_query = "WHERE inv_no = '". $invoice['number'] ."'";
				if ( Invoice::getBifurcation($db, $temp, '*', $condition_query)>0 ) {
					$temp            		= $temp[0];
					$invoice['domain']    	= $temp['domain'];
					$invoice['server1_rs']	= $temp['server1_rs'];
					$invoice['server2_sb']	= $temp['server2_sb'];
					$invoice['develop']   	= $temp['develop'];
					$invoice['product']   	= $temp['product'];
					$invoice['design']    	= $temp['design'];
					$invoice['bpo']       	= $temp['bpo'];
					$invoice['seo']       	= $temp['seo'];
					$invoice['consult']   	= $temp['consult'];
					$invoice['research']  	= $temp['research'];
					$invoice['advert']    	= $temp['advert'];
					$invoice['webmaint']  	= $temp['webmaint'];
					$invoice['network']   	= $temp['network'];
					$invoice['hardware']  	= $temp['hardware'];
				}*/
				$condition_query = '';
				
				
            }
            else {
                $messages->setErrorMessage("The Invoice was not found or you do not have the Permission to access this Invoice.");
            }
								
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		/** New Function 2011-08-aug-11 
		 *  This function is used to retrieve the details of the invoice.
		 *
		*/
		
		function getProformaInvoice($inv_id, &$invoice, &$extra) {
            include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
			
			foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
            $condition_query = " WHERE (". TABLE_BILL_INV_PROFORMA .".id = '". $inv_id ."' "
                                ." OR ". TABLE_BILL_INV_PROFORMA .".number = '". $inv_id ."')";
            $fields = TABLE_BILL_INV_PROFORMA .'.*'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.status AS c_status';
						
			if ( InvoiceProforma::getDetails($db, $invoice, $fields, $condition_query) > 0 ) {
                $invoice = $invoice['0'];

				// Set up the Client Details field.
				$invoice['client_details'] = $invoice['c_f_name'] .' '. $invoice['c_l_name']
												.' ('. $invoice['c_number'] .')'
												.' ('. $invoice['c_email'] .')';
				
				// Set up the dates.
//				$invoice['do_i']  = explode(' ', $invoice['do_i']);
//				$temp               = explode('-', $invoice['do_i'][0]);
//				$invoice['do_i']  = NULL;
//				$invoice['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
//				
//				$invoice['do_d']  = explode(' ', $invoice['do_d']);
//				$temp               = explode('-', $invoice['do_d'][0]);
//				$invoice['do_d']  = NULL;
//				$invoice['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
//				
//				$invoice['do_e']  = explode(' ', $invoice['do_e']);
//				$temp               = explode('-', $invoice['do_e'][0]);
//				$invoice['do_e']  = NULL;
//				$invoice['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				
				// Read the Particulars.
				$temp = NULL;
				$temp_p = NULL;
				$condition_query = "WHERE ord_no = '". $invoice['or_no'] ."'";
				InvoiceProforma::getParticulars($db, $temp, '*', $condition_query);
				foreach ( $temp as $pKey => $parti ) {
					
                   if($temp[$pKey]['s_type']=='1'){
                        $temp[$pKey]['s_type']='Years';
                   }elseif($temp[$pKey]['s_type']=='2'){
                        $temp[$pKey]['s_type']='Quantity';
                   }
                    $temp_p[]=array(
                                    'p_id'=>$temp[$pKey]['id'],
                                    'particulars'=>$temp[$pKey]['particulars'],
                                    'p_amount' =>number_format($temp[$pKey]['p_amount'] ,2),                                   
                                    's_type' =>$temp[$pKey]['s_type'] ,                                   
                                    's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                    's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
                                    's_id' =>$temp[$pKey]['s_id'] ,                                   
                                    'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                    'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                    'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                    'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
                                    'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                    'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                    'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,                                   
                                    'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
                                    'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
                                    'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
                                    'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                    'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                                    );
				}
                $invoice['particular_details'] = $temp_p ;
				$condition_query = '';
				
				// Read the Bifurcations.
                /*
				$temp = NULL;
				$condition_query = "WHERE inv_no = '". $invoice['number'] ."'";
				if ( Invoice::getBifurcation($db, $temp, '*', $condition_query)>0 ) {
					$temp            		= $temp[0];
					$invoice['domain']    	= $temp['domain'];
					$invoice['server1_rs']	= $temp['server1_rs'];
					$invoice['server2_sb']	= $temp['server2_sb'];
					$invoice['develop']   	= $temp['develop'];
					$invoice['product']   	= $temp['product'];
					$invoice['design']    	= $temp['design'];
					$invoice['bpo']       	= $temp['bpo'];
					$invoice['seo']       	= $temp['seo'];
					$invoice['consult']   	= $temp['consult'];
					$invoice['research']  	= $temp['research'];
					$invoice['advert']    	= $temp['advert'];
					$invoice['webmaint']  	= $temp['webmaint'];
					$invoice['network']   	= $temp['network'];
					$invoice['hardware']  	= $temp['hardware'];
				}*/
				$condition_query = '';
				
				
            }
            else {
                $messages->setErrorMessage("The Proforma Invoice was not found or you do not have the Permission to access this Invoice.");
            }
								
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
		/**
		 * This function is used to retreive all the Receipts 
		 * created for the specific Invoice.
		 *
		 */
		function getProformaInvoiceReceipts($inv_no, &$receipt, &$extra) {
			foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
			$required_fields = TABLE_BILL_RCPT .".id"
							.",". TABLE_BILL_RCPT .".number"
							.",". TABLE_BILL_RCPT .".amount"
							.",". TABLE_BILL_RCPT .".amount_inr"
							.",". TABLE_BILL_RCPT .".discount"
							.",". TABLE_BILL_RCPT .".do_r";
			
			$condition_query = " WHERE inv_profm_no = '". $inv_no ."' AND status!='".Receipt::DELETED."'"
								." ORDER BY do_r ASC ";
			if ( Receipt::getList($db, $receipt, $required_fields, $condition_query) > 0 ) {
				/*
                foreach ( $receipt as $key=> $rcpt ) {
					$list = NULL;
					if ( Receipt::getParticulars( $db, $list, 'particulars, p_amount', " WHERE rcpt_no = '". $rcpt['number'] ."'") ) {
						$receipt[$key]['particulars'] = $list;
					}
					$list = NULL;
					if ( Receipt::getBifurcation( $db, $list, '*', " WHERE rcpt_no = '". $rcpt['number'] ."'") ) {
						$receipt[$key]['bifur'] = $list[0];
					}
				}
                */
				return (true);
			}
			else {
				return (false);
			}
		}
		/**
		 * This function is used to retreive all the Receipts 
		 * created for the specific Invoice.
		 *
		 */
		function getInvoiceReceipts($inv_no, &$receipt, &$extra) {
			foreach ($extra as $key=>$value) {
                $$key = $value;
            }
			
			$required_fields = TABLE_BILL_RCPT .".id"
								.",". TABLE_BILL_RCPT .".number"
								.",". TABLE_BILL_RCPT .".amount"
								.",". TABLE_BILL_RCPT .".amount_inr"
								.",". TABLE_BILL_RCPT .".discount"
								.",". TABLE_BILL_RCPT .".do_r";
			
			$condition_query = " WHERE inv_no = '". $inv_no ."' AND status!='".Receipt::DELETED."'"
								." ORDER BY do_r ASC ";
			if ( Receipt::getList($db, $receipt, $required_fields, $condition_query) > 0 ) {
				/*
                foreach ( $receipt as $key=> $rcpt ) {
					$list = NULL;
					if ( Receipt::getParticulars( $db, $list, 'particulars, p_amount', " WHERE rcpt_no = '". $rcpt['number'] ."'") ) {
						$receipt[$key]['particulars'] = $list;
					}
					$list = NULL;
					if ( Receipt::getBifurcation( $db, $list, '*', " WHERE rcpt_no = '". $rcpt['number'] ."'") ) {
						$receipt[$key]['bifur'] = $list[0];
					}
				}
                */
				return (true);
			}
			else {
				return (false);
			}
		}
        // new function added to get balance 
        
        function getBalanceAmt($invoice, &$extra) {
        
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            //Calculate for actual amount BOF
            $balanceAmt =null;
            $balance['balance'] = 0;
            $totalBalanceAmt  = 0;
            $required_fields = " SUM(".TABLE_BILL_RCPT.".amount) as total_amt " ;
            $condition_query = " WHERE inv_no = '". $invoice['number'] ."' AND status !='".Receipt::DELETED."'"
								." GROUP BY inv_no ";
            if ( Receipt::getList($db, $balanceAmt, $required_fields, $condition_query) >0){
               
                $totalAmt = $balanceAmt[0]['total_amt'];
                $totalBalanceAmt = $invoice['amount'] - $totalAmt ;
                if($totalBalanceAmt < 0){
                  $totalBalanceAmt =0;
                }
                $balance['balance'] =$totalBalanceAmt ;                
            }
            //Calculate for actual amount EOF
            //Calculate for Inr amount BOF
            $balanceinrAmt =null;
            $balance['balance_inr'] = 0;
            $totalBalanceinrAmt  = 0;
            $required_fields = " SUM(".TABLE_BILL_RCPT.".amount_inr) as total_inr " ;
            $condition_query = " WHERE inv_no = '". $invoice['number'] ."' AND status !='".Receipt::DELETED."'"
								." GROUP BY inv_no ";
            if ( Receipt::getList($db, $balanceinrAmt, $required_fields, $condition_query) >0){
               
                $totalinrAmt = $balanceinrAmt[0]['total_inr'];
                $totalBalanceinrAmt = $invoice['amount_inr'] - $totalinrAmt ;
                //if($totalBalanceinrAmt < 0){
                 // $totalBalanceinrAmt =0;
                //}
                $balance['balance_inr'] =$totalBalanceinrAmt ;
                
            }
            //Calculate for Inr amount BOF
            
            return $balance;
        }
			
		/**
		 * This function is used to retrieve the balance for the invoice.
		 *
		 */
		function getBalance($invoice, $receipt_list) {
			// Calcualte total amount paid and the invoice balance.

			$balance['amount']		= (int)($invoice['amount']);
			$balance['amount_inr']	= (int)($invoice['amount_inr']);
            
			
			foreach ( $receipt_list as $key=>$receipt ) {
				$balance['amount'] 		= $balance['amount'] - (int)($receipt['amount']);
				
            
			}
			return ($balance);
		}
        
        /**
         * Function to validate the input from the User while sending the invoice via email.
         *
         * @param	array	The array containing the information to be added.
         *
         *
         *
         */
        function validateSendReceipt(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            if ( !isset($data['to']) || empty($data['to']) ) {
                $messages->setErrorMessage('To should be provided.');
            }
            elseif ( !isValidEmail($data['to']) ) {
                $messages->setErrorMessage('To Email Address is not valid.');
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        
        
    function createRptPdfFile($data) {        
        $variables['images'] = DIR_WS_IMAGES_NC ;
        $body="<style media=\"all\" type=\"text/css\">
				  body {
						margin-right:auto;
						margin-left:auto;
					}
					
					#content {
						margin:0;
						width: 100%;
						text-align:center;
					}
					#content-main {
						width: 731px;
						text-align:center;
						margin-left:auto;
						margin-right:auto;
					}
					
					.heading{
						font-family:Arial, Verdana, 'San serif';
						font-size:18px;
						color:#000000;
						text-decoration:none;
						font-weight:bold;
					}
					.sub-heading{
						font-family:Arial, Verdana, 'San serif';
						font-size:11px;
						color:#686868;
						line-height:15px;
						text-decoration:none;
						font-weight:regular;
					}
					.invoice-border{ 
						 background:url(".$variables['images']."/invoice-repeater-new.jpg);
						 background-repeat:repeat-y;
						 width:253px;
					 }	
					.invoice-text{
						font-family:Arial, Verdana, 'San serif';
						font-size:12px;
						color:#000000;
						text-decoration:none;
						font-weight:bold;
					}
					
					.content-heading{
						font-family:Arial, Verdana, 'San serif';
						font-size:12px;
						color:#FFFFFF;
						text-decoration:none;
						font-weight:bold;
					}
					.address-text{
						font-family:Arial, Verdana, 'San serif';
						font-size:10px;
						color:#FFFFFF;
						text-decoration:none;
						font-weight:regular;
						lignt-height:13px;
					}
					.green-text{
                        font-family:Arial, Verdana, 'San serif';
                        font-size:11px;
                        color:#176617;
                        text-decoration:none;
                        font-weight:regular;
                        lignt-height:13px;
                    }
                    .white-text{
                        font-family:Arial, Verdana, 'San serif';
                        font-size:11px;
                        color:#FFFFFF;
                        text-decoration:none;
                        font-weight:regular;
                        lignt-height:13px;
                    }
					.design-text{
						font-family:Arial, Verdana, 'San serif';
						font-size:11px;
						color:#797979;
						text-decoration:none;
						font-weight:regular;
						lignt-height:13px;
					}
					.custom-content{
						font-family:arial,verdana,sans-serif;
						font-size:11px;
						color:#646464;
					}	
					
					.content{
						font-family:arial,verdana,sans-serif;
						font-size:11px;
						color:#4f4f4f;
						lignt-height:10px;
					}
					.content-small{
						font-family:arial,verdana,sans-serif;
						font-size:9px;
						color:#4f4f4f;
					}
					
					.spacer{background:url(".$variables['images']."/spacer.gif)repeat;}
					.bg1{background-color:#898989;}
					.bg2{background-color:#CCCCCC;}
					.bg3{background-color:#f4f4f4;}
					.bg4{background-color:#ba1a1c;}
					.bg5{background-color:#956565;}
					.bg6{background-color:#BEBE74;}
					.bg7{background-color:#48b548;}
                    .bg8{background-color:#000000;}
                    .bg9{background-color:#bf1f21;}
                    .bdb1{border-bottom:1px solid #000000;}
					.b {font-weight:bold;}
					.ac {text-align:center;}
					.al {text-align:left;}
					.ar {text-align:right;}
					.vt {vertical-align:top;}
					.vm {vertical-align:middle;}
					.vb {vertical-align:bottom;}
					.fs08 {font-size:0.8em;}
					.fs11 {font-size:1.1em;}
					.fst11{font-size:1.1em; font-family:'Arial Narrow',Verdana,Arial,Sans-Serif;}
					.fs15 {font-size:1.5em;}
					.lh15 {line-height:1.5em;}
					.lh10 {line-height:1em;}  
					.pl2{padding-left:2px;}
					.pl5{padding-left:5px;}
					.pl8{padding-left:8px;}                                    
					.pl10{padding-left:10px;}
					.pl11{padding-left:11px;}
					.pl15{padding-left:15px;}
					.pl20{padding-left:20px;}
					.pl60{padding-left:60px;}
					.pl42{padding-left:42px;}                                    
					.pr1{padding-right:1px;}
					.pr2{padding-right:2px;}
					.pr5{padding-right:5px;}
					.pr10{padding-right:10px;}                                    
					.pb2{padding-bottom:2px;}
					.pb3{padding-bottom:3px;}
                    .pb4{padding-bottom:4px;}
					.pb5{padding-bottom:5px;}
					.pb10{padding-bottom:10px;}
					.pb13{padding-bottom:13px;}  
					.pt1{padding-top:1px;}        
					.pt2{padding-top:2px;}
					.pt3{padding-top:3px;}
					.pt4{padding-top:4px;}
					.pt5{padding-top:5px;}
					.pt6{padding-top:6px;}
					.pt8{padding-top:8px;}
					.pt10{padding-top:10px;}
					.pt15{padding-top:15px;}
					.pt32{padding-top:32px;}
					.pt45{padding-top:45px;}                                    
					.b1{border:collapse;border:1px solid #000000;}
					.wp100{width:100%;}
					.wp90{width:90%;}
					.wp70{width:70%;}
					.wp65{width:65%;}
					.wp60{width:60%;}
					.wp55{width:55%;}
					.wp50{width:50%;}
					.wp45{width:45%;}
					.wp40{width:40%;}
					.wp35{width:35%;}
					.wp30{width:30%;}
					.wp28{width:28%;}
					.wp25{width:25%;}
					.wp20{width:20%;}
					.wp15{width:15%;}
					.wp13{width:13%;}
					.wp12{width:12%;}
					.wp10{width:10%;}
					.wp9{width:9%;}
					.wp5{width:5%;}
					.wp6{width:6%;}
					.wp4{width:4%;}                                    
					.wpx10{width:10px;}
					.wpx9{width:9px;}
					.wpx7{width:7px;}
					.wpx712{width:712px;}
					.wpx729{width:729px;}
					.wpx731{width:731px;}
					.wpx330{width:330px;}
					.wpx337{width:337px;}
					.wpx348{width:348px;}
					.wpx20{width:20px;}
					.wpx711{width:711px;}
					.wpx707{width:707px;}
					.wpx9{width:9px;}
					.wpx713{width:713px;}
					.wpx347{width:347px;}
					.wpx253{width:253px;}
					.wpx335{width:335px;}                                    
					.hp30{
						height:27px;
						border-top:1px solid #dddddd;
					}
					.hp22{height:22px;}
					.hp20{height:20px;}
					.hp4{height:4px;}
					.hp9{height:9px;}
					.hp8{height:8px;}
					.hp17{height:19px;}
					.borlr{
						border-left:1px solid #dddddd;
						border-right:1px solid #dddddd;
					}                                    
					.borl{
						border-left:1px solid #dddddd;
					}                                    
					.borr{
						border-right:1px solid #dddddd;
					}
					.borb{
						border-bottom:1px solid #dddddd;
					}                                    
					div.row {
						clear:both;
						float:left;
					}
					div.coloumn {
						float:left;
					}
					div.coloumn-right {
						float:right;
					}                                    
					.seccolor{background-color:#999999;}
					.bg2{background-color:#f4f4f4;}                                    
					div.clear{
						clear:both;
						background-color:#f4f4f4;
					}
					.header-text{font-size:11px; font-family:arial; font-weight:normal;}
				</style>
<div id=\"content\">
<div id=\"content-main\">
<div class=\"row al pb10 pt45\">
    <div><img src=\"".$variables['images']."/smeerp-inv-logo.jpg\" alt=\"SMEERP\" title=\"SMEERP\" border=\"0\"></div>
</div>    
        <div class=\"row wpx731 pb5 pt32\">
        	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
        		<tr>
        			<td width=\"478px\" class=\"pl42 al\" valign=\"top\">";
						if (!empty($data['old_billing_address']))
						{
							$body.="              
									<div class=\"heading pb2\">".nl2br($data['billing_name'])."</div>
									<div class=\"sub-heading\">".nl2br($data['old_billing_address'])."</div>  
								 ";
						} 
						else
						{
							$body.="
									<div class=\"heading pb2\">".nl2br($data['billing_name'])."</div>
									<div class=\"sub-heading\">".nl2br($data['b_addr'])."</div>      
									<div class=\"sub-heading\">";
										if (!empty($data['b_addr_city']))
										{
											$body.=$data['b_addr_city'];
										}
										if (!empty($data['b_addr_zip']))
										{
											$body.="&nbsp;". $data['b_addr_zip'];
										}
										if (!empty($data['b_addr_state']))
										{
											$body.="&nbsp;". $data['b_addr_state'];
										}
										if (!empty($data['b_addr_country']))
										{
											$body.="<br/>". $data['b_addr_country'];
										}
									$body.="</div>         
								";  
						}
			$body.="</td>
					<td width=\"253px\" valign=\top\" align=\"right\">
        	        	<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
        	        	    <tr>
        	        	        <td class=\"b al invoice-text pl5 pb3\">RECEIPT</td>
        	        	    </tr>
        	        	    <tr>
        	        	    	<td><img src=\"".$variables['images']."/invoice-top-corner-new.jpg\"  width=\"253px\" height=\"9\"  border=\"0\" /></td>
        	        	    </tr>
        	        	    <tr>
        	        	    	<td class=\"invoice-border\">
									<div class=\"row wpx253\">
										<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Relationship No.</span></div>
										<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['client']['number']."</span></div>
									</div>
									<div class=\"row wpx253\">
										<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Receipt No.</span></div>
										<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['number']."</span></div>
									</div>";
									$do_r = date("d M Y",$data['do_r']);
									$body.="<div class=\"row wpx253\">
										<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Receipt Date</span></div>
										<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$do_r."</span></div>
									</div>";
									if (!empty($data['currency_abbr']))
									{				
									$body.="<div class=\"row wpx253\">
										<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Receipt Currency</span></div>
										<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['currency_abbr']."</span></div>
									</div>";     
									}                
									$body.="<div class=\"row wpx253\">
										<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Invoice No.</span></div>
										<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['inv_no']."</span></div>
									</div>";
									if(!empty($data['inv_profm_no'])){
										$body.="<div class=\"row wpx253\">
												<div class=\"coloumn al custom-content pt2\"><span class=\"b pl20\">Proforma Inv. No.</span></div>
												<div class=\"coloumn-right custom-content pt2\"><span class=\"pr10\">".$data['inv_profm_no']."</span></div>
											</div>";
									}
								$body.="</td>
        	        	    </tr>
        	        	    <tr>
        	        	    	<td><img src=\"".$variables['images']."/invoice-bottom-corner-new.jpg\"  width=\"253px\" height=\"9\"  border=\"0\" /></td>
        	        	    </tr>
						</table>
					</td>
				</tr>
			</table>
			</div> 
            <div class=\"row wpx731\">
                <div class=\"green-text ac b\">
                    Go Green initiative! Every 3000 sheets of paper cost us a tree. Log on to MySMEERP to opt for 'Soft Copy Only'.
                </div>
            </div>
			<div class=\"row wp100 pt5\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
					<tr>
						<td width=\"10px\" class=\"bg1\"><img src=\"".$variables['images']."/first-left-corner.jpg\" width=\"10px\" 
						height=\"30px\" border=\"0\" /></td>
						<td class=\"bg1 al pl5\" width=\"600px\"><span class=\"content-heading b\">Receipt Particulars</span></td>
						<td class=\"bg1 ar pr1\" width=\"112px\"><span class=\"content-heading b\">Amount</span></td>
						<td width=\"9px\" class=\"bg1\"><img src=\"".$variables['images']."/first-right-corner.jpg\"  width=\"9px\" 
						height=\"30px\"  border=\"0\" /></td>
					</tr>               
					<tr>
						<td class=\"pb5 bg3 borl\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\"  border=\"0\" /></td>
						<td class=\"bg3 pb5 content al pl10 pt10\">
							Payment for the Invoice : ".$data['inv_no']."
						</td>
						<td class=\"bg3 pb5 content ar pr1 pt10\">
							".$data['amount']."
						</td>
						<td class=\" pb5 bg3 borr\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\"  border=\"0\" /></td>
					</tr>
					<tr>
						<td width=\"10px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-left-corner.jpg\"  width=\"10px\" 
						height=\"9px\" border=\"0\" /></td>
						<td class=\"bg3 borb\" colspan=\"2\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\"  
						border=\"0\" /></td>
						<td width=\"9px\" class=\"bg3\"><img src=\"".$variables['images']."/first-bottom-right-corner.jpg\"  width=\"9px\" 
						height=\"9px\"  border=\"0\"/></td>
					</tr>
				</table>
			</div>        
			<div class=\"row wp100 pt10\">
				<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"right\">
					<tr>
						<td align=\"right\"><span class=\"content pr10 b\">Grand Total :</span></td>";
					if ($data['do_r_chk'] <= $data['do_rs_symbol']){
						$body.="<td align=\"right\" width=\"100px\">
						<span class=\"content pr10 b\">".$data['currency_symbol']." ".$data['amount']."</span></td>";
					}else{
						$body.="<td align=\"right\" width=\"100px\"><span class=\"content pr10 b\"><img src=\"".$variables['images']."/".
						$data['currency_symbol'].".jpg\" border=\"0\" /> ".$data['amount']."</span></td>";		 
					}						
					$body.="</tr>
					<tr>
						<td align=\"right\">
							<span class=\"content pr10 b\">Amount in words ";
							$body.=" :
						</td>
						<td align=\"right\">
							<span class=\"content pr10 b\">";
							if (!empty($data['currency_name']))
							{ 
								$body.= $data['currency_name']." ";
							} 
							$body.= $data['amount_words']." only
						</td>
					</tr>
				</table>
			</div> ";
        	if (!empty($data['payment_mode_name'])){
            
        	$body.="<div class=\"row wp100 pt10\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
				<tr>
			        <td width=\"10px\"><img src=\"".$variables['images']."/sec-gray-corner.jpg\"  width=\"10px\" height=\"22\"  border=\"0\" /></td>
			        <td class=\"bg1 wpx711 al content-heading\"><span class=\"pl5\">Payment Details</span></td>
			        <td width=\"10px\"><img src=\"".$variables['images']."/sec-right-corner.jpg\"  width=\"10px\" height=\"22\"  border=\"0\" /></td>
			     </tr>
			     <tr>
			     	<td colspan=\"3\" class=\"bg2 pt10 content\">
			     		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\" class=\"wp100 content\">
                            <tr>
                                <td align=\"left\" width=\"20%\" class=\"content vt pl20 b\">Mode Of Payment</td>
                                <td align=\"left\" width=\"3%\" class=\"content vt b \">:</td>
                                <td align=\"left\" width=\"77%\" class=\"content vt\">".$data['payment_mode_name']."</td>
                            </tr>";
                            if (!empty($data['pay_cheque_no']))
                            { 
                            $body.="<tr>
                                <td align=\"left\" width=\"20%\" class=\"content vt pl20 b\">Chq/DD/Tran No.</td>
                                <td align=\"left\" width=\"3%\" class=\"content vt b\">:</td>
                                <td align=\"left\" width=\"77%\" class=\"content vt\">".$data['pay_cheque_no']."</td>
                            </tr>";              
                            }
                            if (!empty($data['do_pay_received']))
                            { 
                            	$temp=$temp1=$dtArr=null;
								$dd  =$mm=$yy='';
								$temp = explode(" ",$data['do_pay_received']) ;
								$temp1=$temp[0];
								$dtArr =explode("-",$temp1) ;
								$dd =$dtArr[2];
								$mm =$dtArr[1];
								$yy=$dtArr[0];
								$do_pay_received = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
                            $body.="<tr>
                                <td align=\"left\" width=\"20%\" class=\"content vt pl20 b\">Chq/DD/Tran Date</td>
                                <td align=\"left\" width=\"3%\" class=\"content vt b\">:</td>
                                <td align=\"left\" width=\"77%\" class=\"content vt\">".$do_pay_received."
                                </td>            
                            </tr>";                                                  
                            }
							if (!empty($data['voucher_total_amount']))
                            { 
                            $body.="<tr>
                                <td align=\"left\" width=\"20%\" class=\"content vt pl20 b\">Chq/DD/Tran Amount</td>
                                <td align=\"left\" width=\"3%\" class=\"content vt b\">:</td>";
								if($data['do_r_chk'] <= $data['do_rs_symbol']){
									$body.="<td align=\"left\" width=\"77%\" 
									class=\"content vt\">".$data['currency_symbol']." ".$data['voucher_total_amount']."</td>";
								}else{
									$body.="<td align=\"left\" width=\"77%\" class=\"content vt\"><img src=\"".$variables['images']."/".
									$data['currency_symbol'].".jpg\" border=\"0\" /> ".$data['voucher_total_amount']."</td>";
								}
								
								
                            $body.="
                            </tr>";              
                            }
                            if (!empty($data['pay_bank_company_name']))
                            {
                            $body.="<tr>
                                <td align=\"left\" width=\"20%\" class=\"content vt pl20 b\">Bank</td>
                                <td align=\"left\" width=\"3%\" class=\"content vt b\">:</td>
                                <td align=\"left\" width=\"77%\" class=\"content vt\">".$data['pay_bank_company_name']."</td>
                            </tr>";              
                            }
                            if (!empty($data['pay_branch']))
                            { 
                            $body.="<tr>
                                <td align=\"left\" width=\"20%\" class=\"content vt pl20 b\">Branch</td>
                                <td align=\"left\" width=\"3%\" class=\"content vt b\">:</td>
                                <td align=\"left\" width=\"77%\" class=\"content vt\">".$data['pay_branch']."</td>
                            </tr>";              
                            }
                            if (!empty($data['p_details'])) 
                            {
                            $body.="<tr>
                                <td align=\"left\" width=\"20%\" class=\"content vt pl20 b\">Details</td>
                                <td align=\"left\" width=\"3%\" class=\"content vt b\">:</td>
                                <td align=\"left\" width=\"77%\" class=\"content vt\">".nl2br($data['p_details'])."</td>
                            </tr>";              
                            }
                            $body.="<tr>
                                <td align=\"left\" colspan=\"3\" class=\"content vt pt10 pl20\"></td>
                            </tr>
                            <tr>
                                <td align=\"left\" colspan=\"3\" class=\"content vt pl20\">
                                    <span class=\"b\">Note :</span> 
                                    Except cash, other modes of payment (cheque etc) subject to realisation.
                                </td>
                            </tr>
                        </table>  
					</td>
			     </tr>
			     <tr>
			     	<td class=\"bg2 al\"><img src=\"".$variables['images']."/sec-left-bottom-corner.jpg\" width=\"9px\" height=\"8px\" 
                    border=\"0\" /></td>
			        <td class=\"bg2\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\" border=\"0\" /></td>
			        <td class=\"bg2 ar\"><img src=\"".$variables['images']."/sec-right-bottom-corner.jpg\"  width=\"9px\" height=\"8px\" 
                    border=\"0\"/></td>
			     </tr>
			</table>   
		 </div>";       
        }
        if (!empty($data['historyrcp']))
        {      
        	$body.="<div class=\"row wp100 pt10\">
			<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
				<tr>
			        <td width=\"10px\"><img src=\"".$variables['images']."/sec-gray-corner.jpg\"  width=\"10px\" height=\"22\"  border=\"0\" /></td>
			        <td class=\"bg1 wpx711 al content-heading\"><span class=\"pl5\">Transaction History for the Invoice</span></td>
			        <td width=\"10px\"><img src=\"".$variables['images']."/sec-right-corner.jpg\"  width=\"10px\" height=\"22\"  border=\"0\" /></td>
			     </tr>
			     <tr>
			     	<td colspan=\"3\" class=\"bg2 pt10 pl15 pr10\">
			     		<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
							<tr>
								<td class=\"al content b pl5\">Invoice No.</td>
								<td class=\"content b pl5 pr5\">:</td>
								<td class=\"content al\">".$data['inv_no']."</td>
								<td class=\"pl42\">&nbsp;</td>
								<td class=\"al content b\">Date</td>
								<td class=\"content b pl5 pr5\">:</td>";
								$temp=$temp1=$dtArr=null;
								$dd  =$mm=$yy='';
								$temp = explode(" ",$data['do_i']) ;
								$temp1=$temp[0];
								$dtArr =explode("-",$temp1) ;
								$dd =$dtArr[2];
								$mm =$dtArr[1];
								$yy=$dtArr[0];
								$do_i = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
								$body.="<td class=\"content al\">".$do_i."</td>
								<td class=\"pl42\">&nbsp;</td>
								<td class=\"al content b\">Amount</td>
								<td class=\"content b pl5 pr5\">:</td>";
								if($data['do_r_chk'] <= $data['do_rs_symbol']){
									$body.="<td class=\"content ar\">".$data['currency_symbol']." ".$data['inv_amount']."</td>";
								}else{
									$body.="<td class=\"content ar\"><img src=\"".$variables['images']."/".
									$data['currency_symbol'].".jpg\" border=\"0\" /> ".$data['inv_amount']."</td>";
								}
								
							$body.="</tr>";
							foreach($data['historyrcp'] as $key=>$historyr)
							 {
								$temp=$temp1=$dtArr=null;
								$dd  =$mm=$yy='';
								$temp = explode(" ",$historyr['do_r']) ;
								$temp1=$temp[0];
								$dtArr =explode("-",$temp1) ;
								$dd =$dtArr[2];
								$mm =$dtArr[1];
								$yy=$dtArr[0];
								$do_rh = date("d M Y",mktime('0','0','0',$mm,$dd,$yy));
							  $body.="
									<tr>
										<td class=\"al content b pl5\">Receipt No.</td>
										<td class=\"content b pl5 pr5\">:</td>
										<td class=\"content al\">".$historyr['number']."</td>
										<td class=\"pl42\">&nbsp;</td>
										<td class=\"al content b\">Date</td>
										<td class=\"content b pl5 pr5\">:</td>
										<td class=\"content al\">".$do_rh."</td>
										<td class=\"pl42\">&nbsp;</td>
										<td class=\"al content b\">Amount</td>
										<td class=\"content b pl5 pr5\">:</td>";
										if($data['do_r_chk'] <= $data['do_rs_symbol']){		
											$body.="<td class=\"content ar\">".$data['currency_symbol']." ".$historyr['amount']."</td>";
										}else{
											$body.="<td class=\"content ar\">
											<img src=\"".$variables['images']."/".$data['currency_symbol'].".jpg\" border=\"0\" /> ".
											$historyr['amount']."</td>";
										}
								$body.="</tr>";
							 }	
						$body.="</table>
			     	</td>
			     </tr>
			     <tr>
			     	<td class=\"bg2 al\"><img src=\"".$variables['images']."/sec-left-bottom-corner.jpg\"  width=\"9px\" height=\"8px\" border=\"0\" /></td>
			        <td class=\"bg2\"><img src=\"".$variables['images']."/spacer-img.jpg\"  width=\"1px\" height=\"1px\"  border=\"0\" /></td>
			        <td class=\"bg2 ar\"><img src=\"".$variables['images']."/sec-right-bottom-corner.jpg\"  width=\"9px\" height=\"8px\"  border=\"0\" /></td>
			     </tr>
			</table>   
		</div>";
        }
		$body.="<div class=\"row wp100 pb10 pt10 pr5 lh15 ar content\">";
		$body.="<span class=\"b pr10\">Invoice Balance on ".$do_r." ";  
               if ($data['invbalance'] <= 0 ){
                    $body.=" : NIL ";
                }else{ 
                  $body.= ": &nbsp;"; 
                  if($data['do_r_chk'] <= $data['do_rs_symbol']){		
                  	 $body.= $data['currency_symbol'];
                  }else{
				     $body.= "<img src=\"".$variables['images']."/".$data['currency_symbol'].".jpg\" border=\"0\" /> ";
				  }			  
                  $body.=" ".$data['invbalance']; 
                }
          $body.="</span>
		</div>        
        <div class=\"row wpx731 bg9 pb3 pt3\">
            <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
                <tr>
                    <td class=\"address-text al pl20\" width=\"393px\">
                        <span class=\"white-text b\">".$data['company_name']."
                    </td>
                    <td class=\"address-text ar pr10\">
                        <span class=\"white-text b\">www.SMEERPTech.in</span> 
                    </td>
                </tr>
            </table>
        </div>
		<div class=\"row wp731 pt5 pb5 bg8 \">
        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"731px\">
            <tr>
                <td class=\"address-text al pl20\" width=\"265px\">
                    <span class=\"white-text\"><b>Questions?</b></span> Contact us with a support ticket <br/>or call us at the numbers to the right.
                </td>
                <td class=\"address-text ar pr10\" width=\"466px\">
                    <span class=\"white-text\"><b>SMEERPPlex,</b></span> 17, MG street | Jhena 440 025 Maharashtra | India<br/>
                    <b>INDIA</b> : 99290-699991, 99290-699992   |   <b>USA & CANADA</b> : 1-837-621-1000
                </td>
            </tr>
        </table>
        </div> 
		<div class=\"clear\"></div>
    </div>
</div>        
</body>";
        $receiptArr['content'] = $body ;
        $receiptArr['header'] = '' ;
        $receiptArr['footer'] = '' ;
        createReceiptPDF($data["number"],$receiptArr);
    
    }
        
 }
 
 
 function createReceiptPDF( $q_no, $data ) {
    require_once(DIR_FS_ADDONS .'/html2pdf/config.rpt.inc.php');
    require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.factory.class.php');
    require_once(DIR_FS_ADDONS .'/html2pdf/pipeline.class.php');
    parse_config_file(DIR_FS_ADDONS .'/html2pdf/html2ps.config');
  
    global 	$g_config;
    
    class ReportData extends Fetcher {
        var $content;
        var $url;
    
        function get_data($data) {
            return new FetchedDataURL($this->content, array(), "","");
        }
    
        function ReportData($content) {
            $this->content = $content;
        }
        
        function get_base_url() {
            return "";
        }
    }
    
    $contents 	= '';
    $contents = $data['content'];      
    
    // Configuration Settings.
    $g_config = array(
                        'process_mode' 	=> 'single',
                        'mode'		    => '',
                        'pixels'		=> '750',
                        'scalepoints'	=> '1',
                        'renderimages'	=> '1',
                        'renderlinks'	=> '1',
                        'renderfields'	=> '1',
                        'media'			=> 'A4',
                        'cssmedia'		=> 'screen',
                        //'leftmargin'	=> '30',
                        'leftmargin'	=> '5',
                        //'rightmargin'	=> '15',
                        'rightmargin'	=> '5',
                        'topmargin'		=> '5',
                        'bottommargin'	=> '5',
                        'encoding'		=> 'iso-8859-1',
                        //'encoding'		=> 'UTF-8',
                        'headerhtml'	=> $data['header'],
                        'boydhtml'		=> $contents,
                        'footerhtml'	=> $data['footer'],
                        'watermarkhtml'	=> '',
                        'pslevel'		=> '3',
                        'method'		=> 'fpdf',
                        'pdfversion'	=> '1.3',
                        'output'		=> '2',
                        'convert'		=> 'Convert File'
                    );
    
    $media = Media::predefined('A4');
    $media->set_landscape(false);
    /*
    $media->set_margins(array(	'left' => 10,
                                'right' => 10,
                                'top' => 15,
                                'bottom' => 35)
                        );
    $media->set_pixels(700);
    */
     $media->set_margins(array(	'left' => 5,
                                'right' => 5,
                                'top' => 5,
                                'bottom' => 5)
                        );
    $media->set_pixels(740);
    
    $g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;
    $g_pt_scale = $g_px_scale * 1.43;
    
    $pipeline 						= PipelineFactory::create_default_pipeline("","");
    $pipeline->pre_tree_filters[] 	= new PreTreeFilterHeaderFooter( $g_config['headerhtml'], $g_config['footerhtml']);
    $pipeline->pre_tree_filters[] 	= new PreTreeFilterHTML2PSFields("","","");
    if ($g_config['renderfields']) {
        $pipeline->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
    }

    // Changed so that script will try to fetch files via HTTP in this case) and to provide absolute paths to the image files.
    // http://forum.tufat.com/showthread.php?t=28085&highlight=image+pdf
    //		$pipeline->fetchers 			= array(new ReportData($g_config['boydhtml']));	
    $pipeline->fetchers[] 			= new ReportData($g_config['boydhtml']);
    //$pipeline->fetchers 			= array(new ReportData('This is the ocntent.'));	
    $pipeline->destination 			= new DestinationFile($q_no);
     
    $pipeline->process($q_no, $media);
    $filename = DIR_FS_RPT_PDF_FILES .'/'. $q_no .'.pdf'; 
    @chmod($filename, 0777);
    $contents = NULL;
    return true;
}
 
 
 
	// end of class.
?>
