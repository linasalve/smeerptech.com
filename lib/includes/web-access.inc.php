<?php
	class Webaccess {
		
        const INCOMING = 1;
		const OUTGOING = 2;
        
        const COMPLETED = 1;
		const PENDING = 2;
        
        function getTypes() {
			$type = array(
							'Payment In'   => Webaccess::INCOMING,
                            'Payment Out'    => Webaccess::OUTGOING
						);
			return ($type);
		}      
        function getStatus() {
			$status = array(
							'PENDING'   => Webaccess::PENDING,
                            'COMPLETED'    => Webaccess::COMPLETED
						);
			return ($status);
		}    
		/**
		 *	Function to get all the Orders.
		 *
		 *	@param Object of database
		 *	@param Array of User role
		 * 	@param required fields
		 * 	@param condition
		 *	return array of User roles
		 *	otherwise return NULL
		 */		
		function getList( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
			$query = "SELECT DISTINCT ";
			$count = $total=0;
			if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
				$query .= implode(",", $required_fields);
			}
			elseif ( !empty($required_fields) ) {
				$query .= " ". $required_fields;
			}
			else {
				$query .= " COUNT(*) as count";
				$count = 1;
			}
			
			$query .= " FROM ". TABLE_WEB_ACCESS_LIST ;
			
			$query .= " ". $condition;
	
			if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
				$query .= " LIMIT ". $from .", ". $rpp;
			}
		
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
					}
				}
				
				return ( $total );
			}
			else {
				return false;
			}	
		}
        
        
        function getDetails( &$db, &$list, $required_fields, $condition='', $from='', $rpp='' ) {
            $query = "SELECT DISTINCT ";
            $count = $total=0;
            if ( !empty($required_fields) && is_array($required_fields) && count($required_fields)>0 ) {
                $query .= implode(",", $required_fields);
            }
            elseif ( !empty($required_fields) ) {
                $query .= " ". $required_fields;
            }
            else {
                $query .= " COUNT(*) as count";
				$count = 1;
            }
            
            $query .= " FROM ". TABLE_WEB_ACCESS_LIST;
            $query .= " LEFT JOIN ". TABLE_WEB_ACCESS_GROUPS
             ." ON ". TABLE_WEB_ACCESS_LIST .".group_id = ". TABLE_WEB_ACCESS_GROUPS .".catID  ";
            $query .= " ". $condition;
            
            if ( ((!empty($from) || $from == 0 ) && ($from >= 0)) && ((!empty($rpp) || $rpp == 0 ) && ($rpp > 0)) ) {
                $query .= " LIMIT ". $from .", ". $rpp;
            }
        
            if ( $db->query($query) ) {
                if ( $db->nf() > 0 ) {
                    while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
						if($count==1){
							$total =$db->f("count") ;							
						}else{
							$total =$db->nf();
						}
                    }
                }
                
                return ( $total );
            }
            else {
                return false;
            }   
        }
       
	   
	    
        
    

    
        function delete($id, $extra) {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }
            $details = NULL;
            //if ( (Paymentin::getList($db, $details, 'id, access_level', " WHERE id = '$id'")) > 0 ) {
               //$details = $details[0];
               //if ( $details['access_level'] < $access_level ) {
                    $query = "DELETE FROM ". TABLE_WEB_ACCESS_LIST
                                ." WHERE id = '$id'";
                                
                        
                   
                                
                    if ($db->query($query) && $db->affected_rows()>0 ) {
                        $messages->setOkMessage("The Record has been deleted.");                      
                    }
                    else {
                        $messages->setErrorMessage("The Record was not deleted.");
                    }
               // }
                //else {
                    //$messages->setErrorMessage("Cannot Delete Record with the current Access Level.");
                //}
            //}
            //else {
                //$messages->setErrorMessage("The Record was not found.");
           // }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
            
        }
    

        /**
		 * Function to validate the input from the User while Adding.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
		function validateAdd(&$data, $extra='') {
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}        
            if(empty($data['group_id']) ){                
                $messages->setErrorMessage("Group can not be empty.");
            }
            
            if(empty($data['fIp1']) && empty($data['fIp2']) && empty($data['fIp3']) && empty($data['fIp4'])){
                $data['fromIp']='';
            
            }else{
                $data['fromIp']=$data['fIp1'].".".$data['fIp2'].".".$data['fIp3'].".".$data['fIp4'] ;
                
                if(empty($data['fIp1']) || empty($data['fIp2']) || empty($data['fIp3']) || empty($data['fIp4']) || !is_numeric($data['fIp1']) || !is_numeric($data['fIp2']) || !is_numeric($data['fIp3']) || !is_numeric($data['fIp3']) || !is_numeric($data['fIp4'])){
                    $messages->setErrorMessage("Please enter Valid From IP Field.");
                }
            
            }
            
            if(empty($data['tIp1']) && empty($data['tIp2']) && empty($data['tIp3']) && empty($data['tIp4'])){
                $data['toIp']='';
                
            }else{
                $data['toIp']=$data['tIp1'].".".$data['tIp2'].".".$data['tIp3'].".".$data['tIp4'] ;
                
                if(empty($data['tIp1']) || empty($data['tIp2']) || empty($data['tIp3']) || empty($data['tIp4']) || !is_numeric($data['tIp1']) || !is_numeric($data['tIp2']) || !is_numeric($data['tIp3']) || !is_numeric($data['tIp3']) || !is_numeric($data['tIp4'])){
                    $messages->setErrorMessage("Please enter Valid To IP Field.");
                    
                }
            
            }
            
            if( ( empty($data['fromIp']) && empty($data['toIp']) ) &&  empty($data['collected_ip']) ){
                $messages->setErrorMessage("Please enter  Valid IP range Or IP address.");            
            }
                        
            if(empty($data['collected_ip']) ){
            
                if(empty($data['fromIp']) || empty($data['toIp'])){
                
                    $messages->setErrorMessage("Please enter Valid IP range.");       
                }
                   
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
            
                $data['fromIp'] = $data['fIp1'].$data['fIp2'].$data['fIp3'].$data['fIp4'] ;
                
                $data['toIp'] = $data['tIp1'].$data['tIp2'].$data['tIp3'].$data['tIp4'] ;
                
                
                if( $data['toIp'] < $data['fromIp']){
                    $messages->setErrorMessage("Please enter Valid IP range.");  
                }
            }
            
          
         
            
                       
			if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
		}
        
		function validateWebaccess(&$data, $extra=''){
              	
			foreach ($extra as $key=>$value) {
				$$key = $value;
			}
            
            if($data['access_type']!='N'){            
                
                if(empty($data['access_type']) || !isset($data['access_type']) ){ 
                
                    $messages->setErrorMessage("Please Choose the Option (White list / Black List).");
                }
                if(empty($data['group_id']) || !isset($data['group_id']) ){ 
                
                    $messages->setErrorMessage("Please Select Group.");
                }
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				return (true);
			}
			else {
				return (false);
			}
        }
		 /**
		 * Function to validate the input from the User while Updating.
		 *
		 * @param	array	The array containing the information to be added.
		 *
		 *
		 *
		 */
        function validateUpdate(&$data, $extra='') {
            foreach ($extra as $key=>$value) {
                $$key = $value;
            }       
           if(empty($data['group_id']) ){                
                $messages->setErrorMessage("Group can not be empty.");
            }
            
            if(empty($data['fIp1']) && empty($data['fIp2']) && empty($data['fIp3']) && empty($data['fIp4'])){
                $data['fromIp']='';
            
            }else{
                $data['fromIp']=$data['fIp1'].".".$data['fIp2'].".".$data['fIp3'].".".$data['fIp4'] ;
                
                if(empty($data['fIp1']) || empty($data['fIp2']) || empty($data['fIp3']) || empty($data['fIp4']) || !is_numeric($data['fIp1']) || !is_numeric($data['fIp2']) || !is_numeric($data['fIp3']) || !is_numeric($data['fIp3']) || !is_numeric($data['fIp4'])){
                    $messages->setErrorMessage("Please enter Valid From IP Field.");
                }
            
            }
            
            if(empty($data['tIp1']) && empty($data['tIp2']) && empty($data['tIp3']) && empty($data['tIp4'])){
                $data['toIp']='';
                
            }else{
                $data['toIp']=$data['tIp1'].".".$data['tIp2'].".".$data['tIp3'].".".$data['tIp4'] ;
                
                if(empty($data['tIp1']) || empty($data['tIp2']) || empty($data['tIp3']) || empty($data['tIp4']) || !is_numeric($data['tIp1']) || !is_numeric($data['tIp2']) || !is_numeric($data['tIp3']) || !is_numeric($data['tIp3']) || !is_numeric($data['tIp4'])){
                    $messages->setErrorMessage("Please enter Valid To IP Field.");
                    
                }
            
            }
            
            if( ( empty($data['fromIp']) && empty($data['toIp']) ) &&  empty($data['collected_ip']) ){
                $messages->setErrorMessage("Please enter  Valid IP range Or IP address.");            
            }
                        
            if(empty($data['collected_ip']) ){
            
                if(empty($data['fromIp']) || empty($data['toIp'])){
                
                    $messages->setErrorMessage("Please enter Valid IP range.");       
                }
                   
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
            
                $data['fromIp'] = $data['fIp1'].$data['fIp2'].$data['fIp3'].$data['fIp4'] ;
                
                $data['toIp'] = $data['tIp1'].$data['tIp2'].$data['tIp3'].$data['tIp4'] ;
                
                
                if( $data['toIp'] < $data['fromIp']){
                    $messages->setErrorMessage("Please enter Valid IP range.");  
                }
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
                return (true);
            }
            else {
                return (false);
            }
        }
        // functions for group
            function isWebAccessGroupChild($id, $match_parent, $extra) {
                foreach ($extra as $key=>$value) {
                    $$key = $value;
                }
        
                $query = " SELECT parentID FROM ". TABLE_WEB_ACCESS_GROUPS
                        ." WHERE " . TABLE_WEB_ACCESS_GROUPS .".catID = '$id'"
                        ." ORDER BY CatName ASC ";
        
                if ( $db->query($query) ) {
                    if ( $db->nf()>0 && $db->next_record() ) {
                        $id = $db->f('parentID');
                        if ( $id == $match_parent ) {
                            return (true);
                        }
                        elseif ( $id != '0' ) {
                            return ( Webaccess::isWebAccessGroupChild($id, $match_parent, $extra) );
                        }
                        else {
                            return (false);
                        }
                    }
                }
                return (true);
            }
            
            function getAllWebAccessGroups($catID=0,$level = 0) {
                global $output;
        
                $db = new db_local;
        
                $query = "SELECT * FROM  ".TABLE_WEB_ACCESS_GROUPS." WHERE parentId='$catID' AND active='1'";
                $db->query($query);
        
                if($db->nf() ==0) return;
        
                while($db->next_record()) {
                                $output[$db->f("catID")] = str_repeat('&nbsp;', $level * 4).'&#187; '.$db->f("CatName");
                                Webaccess::getAllWebAccessGroups($db->f("catID"), $level+1);
                }
            }
		
    }
?>
