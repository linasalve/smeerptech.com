<?php
/*
 * Session Management for PHP5
 *
 * Copyright (c) 1998-2000 NetUSE AG
 *                    Boris Erdmann, Kristian Koehntopp
 *
 * $Id: prepend.php,v 1.1.1.1 2004/09/01 12:26:16 bhushan Exp $
 *
 */

$_PHPLIB = array();

# Can't control your include path?
# Point this to your PHPLIB base directory. Use a trailing "/"!
$_PHPLIB["libdir"]  = DIR_FS_LIB ."/";


	/*
	 * For compatiilty with systems running older versions
	 * of PHP
	 */
	global $HTTP_POST_VARS ;
	global $HTTP_GET_VARS ;
	$HTTP_POST_VARS = $_POST ;
	$HTTP_GET_VARS 	= $_GET ;

    require($_PHPLIB["libdir"] . "db_mysql.inc.php");  /* Change this to match your database. */
    //require($_PHPLIB["libdir"] . "ct_sql.inc");   /* Change this to match your data storage container */
    require($_PHPLIB["libdir"] . "session.inc.php");   /* Required for everything below.      */
    //require($_PHPLIB["libdir"] . "session_custom.inc");   /* Required for using PHPLIB storage container for sessions.      */
    require($_PHPLIB["libdir"] . "auth.inc.php");      /* Disable this, if you are not using authentication. */
    require($_PHPLIB["libdir"] . "perm.inc.php");      /* Disable this, if you are not using permission checks. */
    //require($_PHPLIB["libdir"] . "user.inc");      /* Disable this, if you are not using per-user variables. */

	/* Additional require statements go below this line */
	# require($_PHPLIB["libdir"] . "menu.inc");      /* Enable to use Menu */

	/* Additional require statements go before this line */

    require($_PHPLIB["libdir"] . "local.inc.php");     /* Required, contains your local configuration. */
    require($_PHPLIB["libdir"] . "page.inc.php");      /* Required, contains the page management functions. */
    //require( DIR_FS_ADDONS . "/phpthumb.class.php" );	/* Required, contains the immage functions. */
    require( DIR_FS_SMARTY .'/Smarty.class.php' ); 
    require( DIR_FS_CLASS .'Messager.class.php' );
    
?>
