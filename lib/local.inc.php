<?php

class DB_Local extends DB_Sql {
  var $Host     = DB_HOST;
  var $Database = DB_NAME;
  var $User     = DB_USER;
  var $Password = DB_PASS;
}

class MP_Session extends Session {
  var $classname      = "MP_Session";

  var $cookiename     = "";                ## defaults to classname
  var $magic          = "Hocuspocus";      ## ID seed
  var $mode           = "cookie";          ## We propagate session IDs with cookies
  var $fallback_mode  = "get";
  var $lifetime       = 0;                 ## 0 = do session cookies, else minutes
  var $that_class     = "Local_CT_Sql";    ## name of data storage container class
  var $gc_probability = 5;  
  var $allowcache     = "no";              ## "public", "private", or "no"

}

class MP_Auth extends Auth {
	var $classname      = "MP_Auth";
	var $lifetime       = 60; // minutes
	var $nobody    		= false;
	
	var $database_class = "DB_Local";
	var $database_table = TABLE_CLIENTS;
	var $db_table_roles	= TABLE_USER_ROLES;
//    var $db_table_login = TABLE_SETTINGS_LOGIN;
	
	function auth_loginform() {
		global $sess;
		global $_PHPLIB;
		
        include(DIR_FS_MP_TEMPLATES . "/loginform.ihtml");
	}
	
	function auth_validatelogin() {
		global $HTTP_POST_VARS;

		if ( isset($HTTP_POST_VARS["username"]) ) {
			$this->auth["uname"] = $HTTP_POST_VARS["username"];        ## This provides access for "loginform.ihtml"
			$HTTP_POST_VARS["username"] = processUserData($HTTP_POST_VARS["username"]);
		}
		else {
			$HTTP_POST_VARS["username"] = '';
		}
		if ( isset($HTTP_POST_VARS["password"]) ) {
			$HTTP_POST_VARS["password"] = encParam( processUserData($HTTP_POST_VARS["password"]), ENCRYPTION );
		}
		else {
			$HTTP_POST_VARS["password"] = '';
		}
	
		$uid    = false;
        $query  = "SELECT user_id, number, username, access_level, roles, email, f_name, l_name, do_login, department					
					FROM ". $this->database_table
                    ." WHERE ". TABLE_AUTH_USERNAME ." = '". $HTTP_POST_VARS["username"] ."'"
                    ." AND ". TABLE_AUTH_PASSWORD ." = '". $HTTP_POST_VARS["password"] ."'"
                    . TABLE_AUTH_CONDITION;
		
        $this->db->query($query);
        //echo $query;
        if ( ($this->db->nf()>0) && ($this->db->next_record()) ) {
			$uid 					    = $this->db->f("user_id");
            $this->auth["user_id"]      = $this->db->f("user_id");
            $this->auth["number"]       = $this->db->f("number");
            $this->auth["username"]     = $this->db->f("username");
            $this->auth["email"]        = $this->db->f("email");
            $this->auth["f_name"]       = $this->db->f("f_name");
            $this->auth["l_name"]       = $this->db->f("l_name");
            $this->auth["access_level"] = $this->db->f("access_level");
            $this->auth["allow_ip"]     	  = $this->db->f("allow_ip");
            $this->auth["department"]     	  = $this->db->f("department");
           
   			$this->auth["logged_in"]    = true; // This helps in displaying the message that the session has expired.
            $this->auth['perm']         = array();
            
            // Read the Roles assigned to the User Role.
            $roles = '\''. str_replace(',', '\',\'', str_replace(' ', '', $this->db->f("roles"))) .'\'';
            $query = sprintf("select rights from %s where id in (%s)"
                            , $this->db_table_roles, $roles);
            if ( $this->db->query($query) && $this->db->nf()>0 ) {
                while( $this->db->next_record()) {
                    $rights = '';
                    if ( ($rights = @explode(',', $this->db->f('rights'))) && is_array($rights) ) {
                        $this->auth['perm'] = arrayCombine($this->auth['perm'], $rights);
                    }
                }
            }
            $roles = NULL;
            $query = NULL;
            
			 // Update the last login field.
            $query = "UPDATE ". $this->database_table
                     ." SET do_login = '". (date("Y-m-d H:i:s", time())) ."'"
                     ." WHERE user_id = '$uid'";
            $this->db->query($query);
			$query = NULL;
		}
		return $uid;
	}
}

class NC_Session extends Session {
  var $classname      = "NC_Session";

  var $cookiename     = "";                ## defaults to classname
  var $magic          = "Hocuspocus";      ## ID seed
  var $mode           = "cookie";          ## We propagate session IDs with cookies
  var $fallback_mode  = "get";
  var $lifetime       = 0;                 ## 0 = do session cookies, else minutes
  var $that_class     = "Local_CT_Sql";    ## name of data storage container class
  var $gc_probability = 5;  
  var $allowcache     = "no";              ## "public", "private", or "no"

}

class NC_Auth extends Auth {
	var $classname      = "NC_Auth";
	var $lifetime       = 60; // minutes
	var $nobody    		= false;
	
	var $database_class = "DB_Local";
	var $database_table = TABLE_AUTH_USER;
	var $db_table_roles	= TABLE_USER_ROLES;
//    var $db_table_login = TABLE_SETTINGS_LOGIN;
	
	function auth_loginform() {
		global $sess;
		global $_PHPLIB;
		
        include(DIR_FS_NC_TEMPLATES . "/loginform.ihtml");
	}
	
	function auth_validatelogin() {
		global $HTTP_POST_VARS;

		if ( isset($HTTP_POST_VARS["username"]) ) {
			$this->auth["uname"] = $HTTP_POST_VARS["username"];        ## This provides access for "loginform.ihtml"
			$HTTP_POST_VARS["username"] = processUserData($HTTP_POST_VARS["username"]);
		}
		else {
			$HTTP_POST_VARS["username"] = '';
		}
		if ( isset($HTTP_POST_VARS["password"]) ) {
			$HTTP_POST_VARS["password"] = encParam( processUserData($HTTP_POST_VARS["password"]), ENCRYPTION );
		}
		else {
			$HTTP_POST_VARS["password"] = '';
		}
		if ( isset($HTTP_POST_VARS["number"]) ) {
			$this->auth["number"] = $HTTP_POST_VARS["number"];        ## This provides access for "loginform.ihtml"
			$HTTP_POST_VARS["number"] = processUserData($HTTP_POST_VARS["number"]);
		} else {
			$HTTP_POST_VARS["number"] = '';
		}
	
		$uid    = false;
        $query  = "SELECT user_id, number, username, access_level, roles, email,email_password, mkt_person_cron,
		f_name, l_name,desig, do_login,current_salary, allow_ip,
		valid_ip, department,team_photo,marketing_contact,do_birth,my_reporting_members,task_members  FROM ". $this->database_table
                    ." WHERE ". TABLE_AUTH_USERNAME ." = '". $HTTP_POST_VARS["username"] ."'"
                    ." AND ". TABLE_AUTH_PASSWORD ." = '". $HTTP_POST_VARS["password"] ."'"
					." AND number = '". $HTTP_POST_VARS["number"] ."'"
                    . TABLE_AUTH_CONDITION;
		
        $this->db->query($query);
        //echo $query;
        if ( ($this->db->nf()>0) && ($this->db->next_record()) ) {
			$uid 					    	= $this->db->f("user_id");
            $this->auth["user_id"]      	= $this->db->f("user_id");
            $this->auth["number"]       	= $this->db->f("number");
            $this->auth["username"]     	= $this->db->f("username");
			$this->auth["email"]        	= $this->db->f("email");
			$this->auth["email_password"]   = $this->db->f("email_password");
            $this->auth["marketing_contact"]= $this->db->f("marketing_contact");
            $this->auth["mkt_person_cron"]	= $this->db->f("mkt_person_cron");
			$this->auth["desig"]        	= $this->db->f("desig");
            $this->auth["f_name"]       	= $this->db->f("f_name");
            $this->auth["l_name"]       	= $this->db->f("l_name");
            $this->auth["access_level"] 	= $this->db->f("access_level");
            $this->auth["do_login"]     	= $this->db->f("do_login");
            $this->auth["allow_ip"]     	= $this->db->f("allow_ip");
            $this->auth["valid_ip"]     	= $this->db->f("valid_ip");
            $this->auth["department"]     	= $this->db->f("department");
			$this->auth["current_salary"]   = $this->db->f("current_salary");
			$this->auth["my_reporting_members"]     = $this->db->f("my_reporting_members");
			$this->auth["task_members"]     = $this->db->f("task_members");
			$this->auth["do_birth"]   =  $do_birth= $this->db->f("do_birth");
			$this->auth["team_photo"]     = $this->db->f("team_photo");
			$rand= rand(1,10);
			 
			$dob=strtotime($do_birth);
			$do_birth1 = date('d-m',$dob);
			$checkBD = date('d')."-".date('m') ;
			$this->auth["birthday"]   = '';
			if($do_birth1==$checkBD){
				$this->auth["birthday"]   = $rand.".gif";
			}
					
			$this->auth["logged_in"]    = true; // This helps in displaying the message that the session has expired.
            $this->auth['rights']       = array();  
			
			
			$this->auth['perm']         = array();
            $this->auth['permissions']       = array();  
            // Read the Roles assigned to the User Role.
            
			$roles = '\''. str_replace(',', '\',\'', str_replace(' ', '', $this->db->f("roles"))) .'\'';
            $query = sprintf("select rights from %s where id in (%s)"
                            , $this->db_table_roles, $roles);
            if ( $this->db->query($query) && $this->db->nf()>0 ) {
                while( $this->db->next_record()) {
                    $rights = '';
                    if ( ($rights = @explode(',', $this->db->f('rights'))) && is_array($rights) ) {
                        $this->auth['perm'] = arrayCombine($this->auth['perm'], $rights);
                    }
                }
            }
            $permissions=array();
            //if($this->auth["number"]!=5000){
                //Call permissions at once
                $query = "SELECT id, value FROM ". TABLE_USER_RIGHTS
                    ." WHERE status = '1' ";
                if ( $this->db->query($query) && $this->db->nf()>0 ) {
                   while ( $this->db->next_record() ) {
                        $permissions[$this->db->f('id')] = $this->db->f('value');
                    }
                }
           //}
            $this->auth['permissions']  = $permissions;
			
			
			/*
			$rights='';
			$roles = '\''. str_replace(',', '\',\'', str_replace(' ', '', $this->db->f("roles"))) .'\'';
            $query = sprintf("select rights_values from %s where id in (%s)"
                            , $this->db_table_roles, $roles);
            if ( $this->db->query($query) && $this->db->nf()>0 ) {
                while( $this->db->next_record()) {
                    $rights.= $this->db->f('rights_values').",";
                }
				$rights= trim($rights,",");
				$rightsArr = array_unique(explode(",",$rights));
				$this->auth["rights"]  = ",".implode(",",$rightsArr).",";
            }
			*/
            //print_R($this->auth['permissions']);
            $roles = NULL;
            $query = NULL;
			
            // Update the last login field.
            $query = "UPDATE ". $this->database_table
                     ." SET do_login = '". (date("Y-m-d H:i:s", time())) ."',"
					 ." login_ip ='".$_SERVER['REMOTE_ADDR']."'"
                     ." WHERE user_id = '$uid'";
            $this->db->query($query);
			$query = NULL;
			//Entry in login stats
			$ip = $_SERVER['REMOTE_ADDR'] ;      
			$login_status=1;
			$date=date("Y-m-d H:i:s");
			$insert = " INSERT INTO ".TABLE_LOGIN_STATS
						. " SET "
						.TABLE_LOGIN_STATS.".number = '".$this->auth["user_id"]."'"
						.",".TABLE_LOGIN_STATS.".username 	= '".$this->auth['username']."'"
						.",".TABLE_LOGIN_STATS.".password 	= ''"
						.",".TABLE_LOGIN_STATS.".terms 	= '1'"
						.",".TABLE_LOGIN_STATS.".date 	= '". $date ."'"
						.",".TABLE_LOGIN_STATS.".ip 	= '". $ip ."'"
						.",".TABLE_LOGIN_STATS.".login_status 	= '". $login_status ."'" ;
             $this->db->query($insert);
           
		}
		else {
			$this->auth["logged_in"]    = false;
		}
		return $uid;
	}
}

class NC_Default_Auth extends NC_Auth {
  var $classname = "NC_Default_Auth";
  
  var $nobody    = true;
}

class MP_Default_Auth extends MP_Auth {
  var $classname = "MP_Default_Auth";
  
  var $nobody    = true;
}

class NC_Perm extends Perm {
  var $classname = "NC_Perm";
  var $database_table = TABLE_USER_RIGHTS;
  var $permissions = array();

    function __construct() {
        global $auth;       
        //exit;
        // Initialize all the possible Permissions (Rights).
        /*
         $query = "SELECT id, value FROM ". $this->database_table
                    ." WHERE status = '1'";
        if ( $auth->db->query($query) && $auth->db->nf()>0 ) {
           while ( $auth->db->next_record() ) {
                $this->permissions[$auth->db->f('id')] = $auth->db->f('value');
            }
        }*/
        
    }
  
	function has_new($perms){
		global $auth;
		if ( !is_array($perms) ) {
			$check = ",".trim($perms)."," ;
			 
			 
			$pos = strpos($auth->auth["rights"], $check);
			
			if($pos===false){
				
				return (false);
				
			}else{
				
				return (true);
			}
		}else{
			  
		}
		
		
		
	}
	function has($perms) {
		global $auth; 
		if ( !is_array($perms) ) {
			$perms = explode(',', $perms);
            $perms = processUserData($perms);
         
            foreach ( $perms as $key=>$right ) {             
                if ( ($right = array_search($right, $auth->auth['permissions'])) ) {
                    $perms[$key] = $right; 
                }
                else { 
                   // Queried right is not present, so return true.
                   // return (true);  
				   // Queried right is not present, so return false changed by Leena Salve 2013-10-oct-16
                    return (false);  
                }
            } 
		} 
		if ( !is_array($auth->auth['perm']) ) {
			$user_perms = explode(',', $auth->auth['perm']);
		}
		else {
            $user_perms = $auth->auth['perm'];
        }
       
		$user_perms = processUserData($user_perms);
       
        //echo "[";print_r($user_perms);echo "]";
        if ( !empty($user_perms) && is_array($user_perms) ) {
            $has_not = array_diff($perms, $user_perms);
        }
        else {
            $has_not = array(1);
        }
        
        if ( count($has_not)>0 ) {
			return (false);
		}
		else {
			return (true);
		}
	}

  function perm_invalid($does_have, $must_have) {
    global $perm, $auth, $sess;
    global $_PHPLIB;
    
    include($_PHPLIB["libdir"] . "perminvalid.ihtml");
  }
}

class MP_Perm extends Perm {
  var $classname = "MP_Perm";
  var $database_table = TABLE_USER_RIGHTS;
  var $permissions = array();

    function __construct() {
        global $auth;
        // Initialize all the possible Permissions (Rights).
        $query = "SELECT id, value FROM ". $this->database_table
                    ." WHERE status = '1'";
        if ( $auth->db->query($query) && $auth->db->nf()>0 ) {
            while ( $auth->db->next_record() ) {
                $this->permissions[$auth->db->f('id')] = $auth->db->f('value');
            }
        }
    }
  
  
	function has($perms) {
		global $auth;
		
		if ( !is_array($perms) ) {
			$perms = explode(',', $perms);
            $perms = processUserData($perms);
            
            foreach ( $perms as $key=>$right ) {
                if ( ($right = array_search($right, $this->permissions)) ) {
                    $perms[$key] = $right;
                }
                else {
                    // Queried right is not present, so return true.
                    return (true);
                }
            }
		}
		if ( !is_array($auth->auth['perm']) ) {
			$user_perms = explode(',', $auth->auth['perm']);
		}
		else {
            $user_perms = $auth->auth['perm'];
        }
		$user_perms = processUserData($user_perms);
        
        if ( !empty($user_perms) && is_array($user_perms) ) {
            $has_not = array_diff($perms, $user_perms);
        }
        else {
            $has_not = array(1);
        }
		
        if ( count($has_not)>0 ) {
			return (false);
		}
		else {
			return (true);
		}
	}

  function perm_invalid($does_have, $must_have) {
    global $perm, $auth, $sess;
    global $_PHPLIB;
    
    include($_PHPLIB["libdir"] . "perminvalid.ihtml");
  }
}


class PT_Session extends Session {
  var $classname      = "PT_Session";

  var $cookiename     = "";                ## defaults to classname
  var $magic          = "Hocupocus";      ## ID seed
  var $mode           = "cookie";          ## We propagate session IDs with cookies
  var $fallback_mode  = "get";
  var $lifetime       = 0;                 ## 0 = do session cookies, else minutes
  var $that_class     = "Local_CT_Sql";    ## name of data storage container class
  var $gc_probability = 5;  
  var $allowcache     = "no";              ## "public", "private", or "no"

}

class PT_Auth extends Auth {
	var $classname      = "PT_Auth";
	var $lifetime       = 60; // minutes
	var $nobody    		= false;
	
	var $database_class = "DB_Local";
	var $database_table = TABLE_AUTH_USER;
	var $db_table_roles	= TABLE_USER_ROLES;
//    var $db_table_login = TABLE_SETTINGS_LOGIN;
	
	function auth_loginform() {
		global $sess;
		global $_PHPLIB;
		
        include(DIR_FS_NC_TEMPLATES . "/loginform.ihtml");
	}
	
	function auth_validatelogin() {
		global $HTTP_POST_VARS;

		if ( isset($HTTP_POST_VARS["username"]) ) {
			$this->auth["uname"] = $HTTP_POST_VARS["username"];        ## This provides access for "loginform.ihtml"
			$HTTP_POST_VARS["username"] = processUserData($HTTP_POST_VARS["username"]);
		}
		else {
			$HTTP_POST_VARS["username"] = '';
		}
		if ( isset($HTTP_POST_VARS["password"]) ) {
			$HTTP_POST_VARS["password"] = encParam( processUserData($HTTP_POST_VARS["password"]), ENCRYPTION );
		}
		else {
			$HTTP_POST_VARS["password"] = '';
		}
	
		$uid    = false;
        $query  = "SELECT user_id, number, username, access_level, roles, email, f_name, l_name, do_login  FROM ". $this->database_table
                    ." WHERE ". TABLE_AUTH_USERNAME ." = '". $HTTP_POST_VARS["username"] ."'"
                    ." AND ". TABLE_AUTH_PASSWORD ." = '". $HTTP_POST_VARS["password"] ."'"
                    . TABLE_AUTH_CONDITION;
		
        $this->db->query($query);
        //echo $query;
        if ( ($this->db->nf()>0) && ($this->db->next_record()) ) {
			$uid 					    = $this->db->f("user_id");
            $this->auth["user_id"]      = $this->db->f("user_id");
            $this->auth["number"]       = $this->db->f("number");
            $this->auth["username"]     = $this->db->f("username");
            $this->auth["email"]        = $this->db->f("email");
            $this->auth["f_name"]       = $this->db->f("f_name");
            $this->auth["l_name"]       = $this->db->f("l_name");
            $this->auth["access_level"] = $this->db->f("access_level");
            $this->auth["do_login"]     = $this->db->f("do_login");
			$this->auth["logged_in"]    = true; // This helps in displaying the message that the session has expired.
            $this->auth['perm']         = array();
            
            // Read the Roles assigned to the User Role.
            $roles = '\''. str_replace(',', '\',\'', str_replace(' ', '', $this->db->f("roles"))) .'\'';
            $query = sprintf("select rights from %s where id in (%s)"
                            , $this->db_table_roles, $roles);
            if ( $this->db->query($query) && $this->db->nf()>0 ) {
                while( $this->db->next_record()) {
                    $rights = '';
                    if ( ($rights = @explode(',', $this->db->f('rights'))) && is_array($rights) ) {
                        $this->auth['perm'] = arrayCombine($this->auth['perm'], $rights);
                    }
                }
            }
            $roles = NULL;
            $query = NULL;
            
			 // Update the last login field.
            $query = "UPDATE ". $this->database_table
                     ." SET do_login = '". (date("Y-m-d H:i:s", time())) ."'"
                     ." WHERE user_id = '$uid'";
            $this->db->query($query);
			$query = NULL;
		}
		return $uid;
	}
}

class PT_Default_Auth extends PT_Auth {
  var $classname = "PT_Default_Auth";
  
  var $nobody    = true;

}


?>