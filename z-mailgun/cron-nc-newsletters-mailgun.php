<?php
  
    if (!defined( "THIS_DOMAIN")) {
       //include('/var/www/html/leena/smeerptech.com/smeerptech.com/lib/config.php');
		include ('/var/www/vhosts/smeerp.erp.in/httpdocs/lib/config.php');	
	}
  
   
	$db 		= new db_local; // database handle
	$s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class.
	
	//putenv("TZ=GMT"); // done in config.php

	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true;
    
    require 'mailgun-php/vendor/autoload.php';
	use Mailgun\Mailgun; 


    
    include_once (DIR_FS_INCLUDES .'/newsletters-members.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    
    $db_new 		= new db_local; // database handle
     	$temp_query = " SELECT ". TABLE_NEWSLETTERS_BATCHES .".id, "
				.TABLE_NEWSLETTERS_BATCHES .".newsletters_id,"
				.TABLE_NEWSLETTERS_BATCHES .".nws_category_id ,"
				.TABLE_NEWSLETTERS_BATCHES .".number ,"
				.TABLE_NEWSLETTERS_BATCHES .".gmail_subject ,"
				.TABLE_NEWSLETTERS_BATCHES .".identity_no ,"
				.TABLE_NEWSLETTERS_BATCHES .".subject,"
				.TABLE_NEWSLETTERS_BATCHES .".text,"
				.TABLE_NEWSLETTERS_BATCHES .".eml_template,"
				.TABLE_NEWSLETTERS_BATCHES .".sendmethod,"
				.TABLE_NEWSLETTERS_BATCHES .".sockethost,"
				.TABLE_NEWSLETTERS_BATCHES .".smtpauth,"
				.TABLE_NEWSLETTERS_BATCHES .".smtpauthuser,"
				.TABLE_NEWSLETTERS_BATCHES .".smtpauthpass,"
				.TABLE_NEWSLETTERS_BATCHES .".smtpport,"
				.TABLE_NEWSLETTERS_BATCHES .".from_name,"
				.TABLE_NEWSLETTERS_BATCHES .".from_email,"				
				.TABLE_NEWSLETTERS_BATCHES .".replyto_name,"
				.TABLE_NEWSLETTERS_BATCHES .".replyto_email,"
				.TABLE_NEWSLETTERS_BATCHES .".attachment," 
				.TABLE_NEWSLETTERS_BATCHES .".total_batches,"
				.TABLE_NEWSLETTERS_BATCHES .".pending_batches,"
				.TABLE_NEWSLETTERS_BATCHES .".total_members,"
				.TABLE_NEWSLETTERS_BATCHES .".domain_mailgun_key,"
				.TABLE_NEWSLETTERS_BATCHES .".domain_name,"
				.TABLE_NEWSLETTERS_BATCHES .".per_batch "
				." FROM ". TABLE_NEWSLETTERS_BATCHES
				." WHERE ". TABLE_NEWSLETTERS_BATCHES .".pending_batches !=0"
				." ORDER BY do_e ASC"
				." LIMIT 0, 1";
	$db->query($temp_query);
	$data = array();
	if ( $db->nf()>0 ) {
		$db->next_record();
		$data = processSqlData( $db->Record );
		//print_R($data);
		$batch_no=0;
	}
	if(!empty($data)){
		$batch_no = ($data['total_batches']- $data['pending_batches'])+1 ;
		$batch_id = $data['id'];
		$pending_batches = $data['pending_batches'];
		$from=0;
		if($batch_no==1){			 
			$from=0;
		}else{			 	
			//$from = ( $data['total_batches']- $data['pending_batches'] )	* $data['per_batch'] + 1;
			$from = ($data['total_batches'] - $data['pending_batches'] ) * $data['per_batch'] ;
		}
		
		$condition ='';
		$attachfilename = $data['attachment'];
		$domain_mailgun_key = $data['domain_mailgun_key'];
		$mgClient = new Mailgun($domain_mailgun_key);
		$domain = $data['domain_name'];
		
	 
		//$subject = processSqlData($data['subject']);
		//$text = processSqlData($data['text']);
		$newsletters_id = $data['newsletters_id']; 
		$nws_category_id  = $data['nws_category_id']; 
		$subject = $data['subject'];
		$text = $data['text'];
		$eml_template = $data['eml_template'];
		$per_batch = $data['per_batch'];
	    $sql = "SELECT ".TABLE_NEWSLETTERS_MEMBERS.".id,".TABLE_NEWSLETTERS_MEMBERS.".f_name,".TABLE_NEWSLETTERS_MEMBERS.".l_name,".TABLE_NEWSLETTERS_MEMBERS.".email"." FROM ".TABLE_NEWSLETTERS_MEMBERS." WHERE ( ".TABLE_NEWSLETTERS_MEMBERS.".email_bounce_status!='".NewslettersMembers::EMAILBOUNCE."' AND ".TABLE_NEWSLETTERS_MEMBERS.".unsubscribed_status!='".NewslettersMembers::UNSUBSCRIBED."' AND 
		".TABLE_NEWSLETTERS_MEMBERS.".nws_category_id IN ('".$nws_category_id."') )" 	;  
		$sql .= " LIMIT ".$from.", ".$per_batch;
		
		 
		$db->query( $sql ); 
		
		if ( $db->nf() > 0 ) {
			$to_recipient='';
			while( $db->next_record() ) {
				$details = processSqlData( $db->Record ); 
				$client_name = $details['f_name']." ".$details['l_name']; 
				$to_member_id = $details['id'];
				$client_email = trim($details['email']); 
				$to_email.= $client_email.",";
				$to_recipient.= '"'.$client_email.'":{"id":'.$to_member_id.'},'  ;
			}
			 
			
			if(!empty($to_email)){
				$to_email = trim($to_email,',');
				$to_recipient = trim($to_recipient,',');
				
				/***************************************
				* Send the notification to the Client
				* of the new Ticket being created
				****************************************/
				//$unsubid = base64_encode($client_email);
				$nws_id = base64_encode($newsletters_id);
				$data['unsubscribed'] = ''   ;
				$data['html_link'] = THIS_DOMAIN.'/newsletters.php?id='.$nws_id   ;
				$data['subject'] =   $subject ;
				$data['text']    =   $text ;
				$data['attachment']   =   $attachfilename ;
				$data['counter']   =   $batch_no ;
				$file_name = DIR_FS_NWS_FILES ."/". $attachfilename;
				 //Send a copy to check the mail for clients
				$sendStatus = '';
				
				
				$auth_details['sendmethod']=  $data['sendmethod'];
				$auth_details['sockethost']= $data['sockethost'] ;
				$auth_details['smtpauth']=  $data['smtpauth'] ;
				$auth_details['smtpauthuser']= $data['smtpauthuser'];
				$auth_details['smtpauthpass'] = $data['smtpauthpass'];
				$auth_details['socketfrom'] = $data['socketfrom'];
				$auth_details['socketfromname'] = $data['socketfromname'];
				$auth_details['smtpport'] =  $data['smtpport'];
				$data['nws_number'] = $data['number']."-".$data['identity_no'] ;
				 
				if ( getParsedEmail($db_new, $s, $eml_template, $data, $email) ) { 
					
					$result = $mgClient->sendMessage($domain, array(
					'from'    => ''.$data["from_name"].' <'.$data["from_email"].'>',
					'to'      => ''.$to_email.'',
					'subject' => $email["subject"],
					'html'    => $email["body"],
					'recipient-variables' => '{'.$to_recipient.'}'
					));
				 
					  
					if($result->http_response_code==200){					
						echo "Mail sent";
						print_R($result);
					}else{
						echo "Mail not sent";
						print_R($result);
					}
					
					/*
					if(!empty($client_email)){
						$to     = '';
						$reply_to=$cc_to_sender=$cc=$bcc='';
						$to[]   = array('name' => $client_email, 
						'email' => $client_email);
						$to_email=$client_email;
						$from   =  array('name' => $data["from_name"],'email' => $data['from_email']);			 
						$reply_to = array('name' => $data["replyto_name"],'email' => $data['replyto_email']);	 
						
						 $sendStatus = SendMailAuth($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name,$auth_details); 
					}
					*/
				}
			
			
			
			
					
				
					
					/* $pos    =  strpos($sendStatus,'Email Sent');                   
					if($pos == false){
						$status=1;
					}else{
						$status=0;
					}		
					 
					$query1 = "INSERT INTO 
					  " . TABLE_NEWSLETTERS_SENT_EMAIL ." SET " 
					  . TABLE_NEWSLETTERS_SENT_EMAIL .".newsletters_id        = '". $newsletters_id."', " 
					  . TABLE_NEWSLETTERS_SENT_EMAIL .".to_name = '". $client_name ."', "
					  . TABLE_NEWSLETTERS_SENT_EMAIL .".to_email       = '".$to_email."', "
					  . TABLE_NEWSLETTERS_SENT_EMAIL .".to_member_id    = '".$to_member_id."', "
					  . TABLE_NEWSLETTERS_SENT_EMAIL .".batch_no    = '".$batch_no."', " 
					  . TABLE_NEWSLETTERS_SENT_EMAIL .".status     = '".$status."', " 
					  . TABLE_NEWSLETTERS_SENT_EMAIL .".do_sent    = '".date('Y-m-d H:i:s')."' "; 
					  
					echo $query1."<br/>" ;  
					
					$db_new->query($query1); */
				 
				
			}
			
		}else{
		
			$pending_batches=1;
		}
		
		//Update the pending batches of current batch
		$pending = $pending_batches - 1;
		$sql=" UPDATE ".TABLE_NEWSLETTERS_BATCHES." SET 
			".TABLE_NEWSLETTERS_BATCHES.".pending_batches= ".$pending." 
			WHERE ".TABLE_NEWSLETTERS_BATCHES.".id='".$batch_id."'";
		 $db->query($sql);
		 if($pending<=0){
			$sqlUpdate=" UPDATE ".TABLE_NEWSLETTERS." SET ".TABLE_NEWSLETTERS.".status='1',
			".TABLE_NEWSLETTERS.".do_completion='".date('Y-m-d H:i:s')."'
			WHERE ".TABLE_NEWSLETTERS.".id = ".$newsletters_id ;
			$db->query($sqlUpdate);
		 } 
    }
    $sql="DELETE FROM ".TABLE_NEWSLETTERS_BATCHES." WHERE ".TABLE_NEWSLETTERS_BATCHES.".pending_batches=0" ;
    $db->query($sql);
	
	
$list= $fields= $fields1 =$body=$from = $reply_to= $subject= $isHtml=$admin_name=$newsletters_id=$admin_email=$followups=$sqlUpdate=$pending = $pending_batches = null;
 

ob_end_flush();
$db->close();
$db_new->close();
$db = null;
$db_new = null;
$s = null;
$messages=null; 
$datetime =null;
?>
