<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/currency.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
    
    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');
    $deleted 	= isset($_GET["deleted"])   ? $_GET["deleted"]  : ( isset($_POST["deleted"])    ? $_POST["deleted"] : '0');
    $inv_no 	= isset($_GET["inv_no"])    ? $_GET["inv_no"]   : ( isset($_POST["inv_no"])     ? $_POST["inv_no"]  : '');

    $hid 		= isset($_GET["hid"])         ? $_GET["hid"]        : ( isset($_POST["hid"])          ? $_POST["hid"]       :'');
    $action='Invoice';
	
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    $condition_url =$condition_query ='';
    //By default search in On
    $_SEARCH["searched"]    = true ;
    if($deleted){    
        $messages->setOkMessage("Invoice $inv_no has been deleted.");
    }
    if($added){    
        $messages->setOkMessage("New Invoice $inv_no has been created.");
    }
   
    
    if ( $perm->has('nc_bl_inv') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_BILL_INV  =>  array(  'ID'            => 'id',
                                                            'Invoice No.'   => 'number',
															'Amount'     => 'amount',
															'Proforma Invoice No.' => 'inv_profm_no',
                                                            'Order No.'     => 'or_no',
                                                            'Remarks'       => 'remarks'
                                                        ),
								TABLE_BILL_ORDERS =>  array('Order Title'   => 'order_title-'.TABLE_BILL_ORDERS,
								'Financial Year'   => 'financial_yr-'.TABLE_BILL_ORDERS
								),
								TABLE_BILL_ORD_P => array('Order Particulars' => 'particulars-'.TABLE_BILL_ORD_P ),
                                // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                TABLE_USER      =>  array(  'Creator Number'    => 'number-'.TABLE_USER,
                                                            'Creator Username'  => 'username-'.TABLE_USER,
                                                            'Creator Email'     => 'email-'.TABLE_USER,
                                                            'Creator First Name'=> 'f_name-'.TABLE_USER,
                                                            'Creator Last Name' => 'l_name-'.TABLE_USER,
														),
								TABLE_CLIENTS 	=>  array(  'Billing Name'          => 'billing_name-'.TABLE_CLIENTS,
                                                            'Client Number'       => 'number-'.TABLE_CLIENTS,
                                                            'Client Username'     => 'username-'.TABLE_CLIENTS,
                                                            'Client Email'        => 'email-'.TABLE_CLIENTS,
                                                            'Client Company Name' => 'org-'.TABLE_CLIENTS,
                                                            'Client First Name'   => 'f_name-'.TABLE_CLIENTS,
                                                            'Client Last Name'    => 'l_name-'.TABLE_CLIENTS,
														)
                            );
        
        $sOrderByArray  = array(
                                TABLE_BILL_INV => array('ID'                => 'id',
                                                        'Invoice No.'       => 'number',
                                                        'Date of Expiry'    => 'do_e',
                                                        'Date of Invoice'   => 'do_i',
                                                        'Date of Creation'  => 'do_c',
                                                        'Status'            => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
           
            if(!empty($hid)){
                $_SEARCH['sOrderBy']= $sOrderBy = 'id';
                $variables['hid']=$hid;
            }else{
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_i';
            }
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_BILL_INV;
        }

        // Read the available Status for the Pre Order.
        $variables['status']        = Invoice::getStatus();
        $variables['status_title']  = Invoice::getStatusTitle();
       
        //use switch case here to perform action. 
        switch ($perform) {
            case ('add_pre_order'):
            case ('add'): {            
                include (DIR_FS_NC.'/bill-invoice-add.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('generate'): {
				$action.=' Add';
                include (DIR_FS_NC.'/bill-invoice-generate.php');
				$page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*Not require 
            case ('add_o'): {
            
                include (DIR_FS_NC.'/bill-invoice-add-o.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            
            case ('edit_pre_order'):
            case ('edit'): {
                include (DIR_FS_NC .'/bill-invoice-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_o'): {
                include (DIR_FS_NC .'/bill-invoice-edit-old.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_pre_order'):
            case ('view'): {
                include (DIR_FS_NC .'/bill-invoice-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('view_file'): {
                include (DIR_FS_NC .'/bill-invoice-view-file.php');
                break;
            }
            case ('change_status'): {
                 $searchStr=1;
                include ( DIR_FS_NC .'/bill-invoice-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/bill-invoice-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('list_prcpt'): { 
				
				$action.= ' - Pending Receipts';
                $searchStr = 1;
				$_SEARCH["chk_status"]  = 'AND';
				$_SEARCH["sStatus"]     = array(Invoice::PENDING);            
            
				$condition_query = " WHERE (". TABLE_BILL_INV .".status = '". Invoice::PENDING ."')"   ; 
				$condition_url  = "&chk_status=AND&sStatus=".Invoice::PENDING ;
                
				include (DIR_FS_NC .'/bill-invoice-list.php');			
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('send'): {
                
                include (DIR_FS_NC .'/bill-invoice-send.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('delete_pre_order'):
            case ('delete'): {
                
                include ( DIR_FS_NC .'/bill-invoice-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
             /*BOF added on 30-april-2009*/
            case ('followup'): {
                include (DIR_FS_NC .'/bill-invoice-followup.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*EOF added on 30-april-2009*/
            case ('list_pre_order'):
            case ('list'):
            default: {
				$action.=' List';
                $searchStr = 1;
                include (DIR_FS_NC .'/bill-invoice-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
	$s->assign("action", $action);
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>