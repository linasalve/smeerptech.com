<?php
    if ( $perm->has('nc_pr_edit') ) {
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
    
        include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
        include_once ( DIR_FS_CLASS .'/ReminderProspects.class.php');
        include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new RegionProspects();
        $phone          = new PhoneProspects(TABLE_PROSPECTS);
        $reminder       = new ReminderProspects(TABLE_PROSPECTS);
        $access_level   = $my['access_level'];
        
        
        //For Prospects list allotment
        if ( $perm->has('nc_pr') && $perm->has('nc_pr_list')) {
            $variables['can_view_clist']     = true;
        }
        
        
        $al_list    = getAccessLevel($db, $access_level);
        //$role_list  = Prospects::getRoles($db, $access_level);

        $grade_list  = Prospects::getGrades();
        
        //BOF read the available services
	    $authorization_list  = Prospects::getAuthorization();
        $services_list  = Prospects::getServices();
        //EOF read the available services
        
		// Read the available source
       	Leads::getSource($db,$source);
		
        //BOF read the available industries
        $industry_list = NULL;
        $required_fields ='*';
        $condition_query="WHERE status='".Industry::ACTIVE."'";
		Industry::getList($db,$industry_list,$required_fields,$condition_query);
        //EOF read the available industries
        
        //BOF read the available industries
		Prospects::getCountryCode($db,$country_code);
		$lst_financial = Prospects::getFinancialStatus();
        //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder
                        );
            
            if ( Prospects::validateUpdate($data, $extra) ) {
                $service_idstr='';
                if(isset($_POST['service_id']) && !empty($_POST['service_id'])){
                    $service_ids=$_POST['service_id'];
                    $service_idstr=implode(",",$service_ids);
                    $service_idstr = ",".$service_idstr.",";	
                }
				$authorization_idstr='';
                if(isset($_POST['authorization_id']) && !empty($_POST['authorization_id'])){
                    $authorization_ids=$_POST['authorization_id'];
                    $authorization_idstr=implode(",",$authorization_ids);
                    $authorization_idstr = ",".$authorization_idstr.",";	
                }
		
                //Default entry remove me 
                /*                
                $service_idstr = Prospects::BILLING.",".Prospects::ST ;    
                if(!empty($service_idstr)){
                    $service_idstr = ",".$service_idstr.",";	
                }*/
                
                $industry_idstr='';
                if(isset($_POST['industry']) && !empty($_POST['industry'])){
                    $industry_ids=$_POST['industry'];
                    $industry_idstr= implode(",",$industry_ids);
                }
                if(!empty($industry_idstr)){
                    $industry_idstr=",".$industry_idstr.",";	
                }
                $alloted_clientsstr='';
                if(isset($_POST['alloted_clients']) && !empty($_POST['alloted_clients'])){
                    $alloted_clients = $_POST['alloted_clients'];
                    $alloted_clientsstr = implode(",",$alloted_clients);
                }
                if(!empty($alloted_clientsstr)){
                    $alloted_clientsstr=",".$alloted_clientsstr.",";
                }
                if( isset($data['check_email'])){
					$data['check_email']=1;
				}else{
					$data['check_email']=0;
				} 
                $query  = " UPDATE ". TABLE_PROSPECTS
                    ." SET "
					."". TABLE_PROSPECTS .".service_id   = '". $service_idstr ."'"
					.",". TABLE_PROSPECTS .".authorization_id   = '". $authorization_idstr ."'"
					/* 
						.",". TABLE_PROSPECTS .".number      = '". $data['number'] ."'"
						.",". TABLE_PROSPECTS .".manager     = '". $data['manager'] ."'" 	
					*/
					.",". TABLE_PROSPECTS .".team			= '". implode(',', $data['team']) ."'"
					.",". TABLE_PROSPECTS .".username    = '". $data['username'] ."'"
					.",". TABLE_PROSPECTS .".lead_source_to    = '". $data['lead_source_to'] ."'"
					.",". TABLE_PROSPECTS .".lead_source_from  = '". $data['lead_source_from'] ."'"
					. $data['password']
					//.",". TABLE_PROSPECTS .".access_level= '". $data['access_level'] ."'"
					.",". TABLE_PROSPECTS .".updated_by	= '". $my['uid'] ."'"
					.",". TABLE_PROSPECTS .".updated_by_name	= '". $my['f_name']." ".$my['l_name'] ."'"
					.",". TABLE_PROSPECTS .".do_update    = '".date("Y-m-d h:i:s") ."'"
					.",". TABLE_PROSPECTS .".email       = '". $data['email'] ."'"
					.",". TABLE_PROSPECTS .".check_email = '". $data['check_email'] ."'"
					.",". TABLE_PROSPECTS .".email_1     = '". $data['email_1'] ."'"						    
					.",". TABLE_PROSPECTS .".email_2     = '". $data['email_2'] ."'"						
					.",". TABLE_PROSPECTS .".email_3     	= '". $data['email_3'] ."'"							   
					.",". TABLE_PROSPECTS .".email_4     	= '". $data['email_4'] ."'"	
					.",". TABLE_PROSPECTS .".additional_email = '". $data['additional_email'] ."'"
					.",". TABLE_PROSPECTS .".title       = '". $data['title'] ."'"
					.",". TABLE_PROSPECTS .".f_name      = '". $data['f_name'] ."'"
					.",". TABLE_PROSPECTS .".m_name      = '". $data['m_name'] ."'"
					.",". TABLE_PROSPECTS .".l_name      = '". $data['l_name'] ."'"
					.",". TABLE_PROSPECTS .".p_name      = '". $data['p_name'] ."'"                           
					.",". TABLE_PROSPECTS .".grade      	= '". $data['grade'] ."'"
					.",". TABLE_PROSPECTS .".credit_limit = '". $data['credit_limit'] ."'"
					.",". TABLE_PROSPECTS .".desig       = '". $data['desig'] ."'"
					.",". TABLE_PROSPECTS .".org         = '". $data['org'] ."'"
					.",". TABLE_PROSPECTS .".domain      = '". $data['domain'] ."'"
					.",". TABLE_PROSPECTS .".gender      = '". $data['gender'] ."'"
					.",". TABLE_PROSPECTS .".do_birth    = '". $data['do_birth'] ."'"
					.",". TABLE_PROSPECTS .".do_aniv     = '". $data['do_aniv'] ."'"
					.",". TABLE_PROSPECTS .".do_reg     = '". $data['do_reg'] ."'"
					.",". TABLE_PROSPECTS .".remarks     = '". $data['remarks'] ."'"
					.",". TABLE_PROSPECTS .".financial_status     = '". $data['financial_status'] ."'"
					.",". TABLE_PROSPECTS .".total_employees     = '". $data['total_employees'] ."'"
					.",". TABLE_PROSPECTS .".company_branches     = '". $data['company_branches'] ."'"
					.",". TABLE_PROSPECTS .".company_profile     = '". $data['company_profile'] ."'"
					.",". TABLE_PROSPECTS .".services_pitched     = '". $data['services_pitched'] ."'"
					.",". TABLE_PROSPECTS .".industry     = '". $industry_idstr ."'"
					.",". TABLE_PROSPECTS .".wt_you_do    = '". $data['wt_you_do'] ."'"
					.",". TABLE_PROSPECTS .".mobile1      = '". $data['mobile1'] ."'"
					.",". TABLE_PROSPECTS .".mobile2      = '". $data['mobile2'] ."'"
					.",". TABLE_PROSPECTS .".alloted_clients   = '". $alloted_clientsstr ."'"
					.",". TABLE_PROSPECTS .".billing_name   = '". $data['billing_name'] ."'"
					.",". TABLE_PROSPECTS .".spouse_dob   = '". $data['spouse_dob'] ."'"
					.",". TABLE_PROSPECTS .".status      = '". $data['status'] ."'" 
				." WHERE ". TABLE_PROSPECTS .".user_id   = '". $data['user_id'] ."'";
                
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['user_id'];
					$messages->setOkMessage("Prospects Profile has been updated.");
					//Push email id into Newsletters Table BOF
					$table = TABLE_NEWSLETTERS_MEMBERS;
					include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
					if(!empty($data['email'])){
						$domain1 = strtolower(strstr($data['email'], '@'));
						$condi1= " WHERE email='".strtolower(trim($data['email']))."'";
					    if( $domain1!= 'smeerptech.com' && 
							!NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi1) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email']))."'" ;
						  $db->query($sql1);
					    }
					}
					if(!empty($data['email_1'])){
						$domain2 = strtolower(strstr($data['email_1'], '@'));
						$condi2= " WHERE email='".strtolower(trim($data['email_1']))."'";
					    if ( $domain2!='smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi2) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_1']))."'" ;
						  $db->query($sql1);
					    }
					}
					if(!empty($data['email_2'])){
						$domain3 = strtolower(strstr($data['email_2'], '@'));
						$condi3  = " WHERE email='".strtolower(trim($data['email_2']))."'";
					    if ($domain3!= 'smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi3) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_2']))."'" ;
						  $db->query($sql1);
					    }
					}
					//Push email id into Newsletters Table EOF
                    // Update the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        if ( isset($data['a_remove'][$i]) && $data['a_remove'][$i] == '1') {
                            if ( ! $region->delete($db, $data['address_id'][$i], $variables['hid'], TABLE_PROSPECTS) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setOkMessage($errors['description']);
                                }
                            }
                        }
                        else {
                            $address_arr = array('id'=> isset($data['address_id'][$i])? $data['address_id'][$i] : '0',
								'address_type'  => $data['address_type'][$i],
								'company_name'  => '',
								'address'       => $data['address'][$i],
								'city'          => $data['city'][$i],
								'state'         => $data['state'][$i],
								'country'       => $data['country'][$i],
								'zipcode'       => $data['zipcode'][$i],
								'is_preferred'  => isset($data['is_preferred'][$i])? $data['is_preferred'][$i] : '0',
								'is_verified'   => isset($data['is_verified'][$i])? $data['is_verified'][$i] : '0'
                               );
                            
                            if ( !$region->update($variables['hid'], TABLE_PROSPECTS, $address_arr) ) {
                                foreach ( $region->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_PROSPECTS) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $phone_arr = array( 'id'=> isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
							'p_type' => $data['p_type'][$i],
							'cc'  	 => $data['cc'][$i],
							'ac'     => $data['ac'][$i],
							'p_number'  => $data['p_number'][$i],
							'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
							'p_is_verified' => isset($data['p_is_verified'][$i]) ? $data['p_is_verified'][$i] : '0'
                            );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_PROSPECTS, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.
										$errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Update the Reminders.
                    for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['remind_date_'.$i]) ) {
                            if ( isset($data['r_remove_'.$i]) && $data['r_remove_'.$i] == '1') {
                                if ( ! $reminder->delete($db, $data['remind_id_'.$i], $variables['hid'], TABLE_PROSPECTS) ) {
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $reminder_arr = array(
								'id'=>(isset($data['remind_id_'.$i])? $data['remind_id_'.$i] : ''),
								'do_reminder'   => $data['remind_date_'.$i],
								'description'   => $data['remind_text_'.$i]
                                );
                                if ( ! $reminder->update($db, $variables['hid'], TABLE_PROSPECTS, $reminder_arr) ) {
                                    $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Send The Email to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  
						array('right'=>'nc_pr_add_notif', 'access_level'=>$data['access_level'])) );
                    
                    /* foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link'] = DIR_WS_NC .'/prospects.php?perform=view_details&user_id='. $data['user_id'];
                        $email = NULL;
                        if( getParsedEmail($db, $s, 'PROSPECT_INFO_UPDATE_MANAGERS', $data, $email)){
                            $to  = '';
                            //echo " subject : ".$email['subject'];
                            //echo "<br/> body : ".$email['body'];
                            $to[] = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }                        
                    }
                     */
                    // Send Email to the Client whose infromation was updated.
                    /*
                    $data['link']       = DIR_WS_NC;
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'CLIENT_INFO_UPDATE', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['f_name'] .' '. $data['l_name'], 'email' => $data['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    } 
                    */
                    /*SEND SERVICE PDF BOF*/
					if($_ALL_POST['send_service_mail']=='1'){
						include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
						include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php'); 
						$id=39;
						$fields = TABLE_ST_TEMPLATE.'.*'  ;            
						$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
						SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1); 
						if(!empty($details)){					
							$details = $details[0];
							$data['template_id'] = $details['id'];
							$data['template_file_1'] = $details['file_1'];
							$data['template_file_2'] = $details['file_2'];
							$data['template_file_3'] = $details['file_3'];
							$data['subject'] = processUserData($details['subject']);
							$data['text'] = processUserData($details['details']);
							$data['ticket_owner'] = $data['f_name']." ".$data['l_name'];
							$data['ticket_owner_uid'] = $variables['hid'] ;
							$ticket_no  =  ProspectsTicket::getNewNumber($db);
							$ticket_status = ProspectsTicket::PENDINGWITHCLIENTS;
							$data['display_name']=$data['display_user_id']=$data['display_designation']='';				
							/* $randomUser = getCeoDetails();
							$data['display_name'] = $randomUser['name'];
							$data['display_user_id'] = $randomUser['user_id'];
							$data['display_designation'] = $randomUser['desig']; */
							$data['display_name'] = $my['f_name']." ".$my['l_name'];
							$data['display_user_id'] = $my['user_id'];
							$data['display_designation'] = $my['desig']; 
							if($my['department'] == ID_MARKETING){
								//This is the marketing person identity
								$data['tck_owner_member_id'] = $my['user_id'];
								$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
								$data['tck_owner_member_email'] = $my['email'];
								$data['marketing_email'] = $my['email'];
								$data['marketing_contact'] = $my['marketing_contact'];
								$data['marketing'] = 1;
								if($my['user_id'] == CEO_USER_ID){									
									$randomUser = getRandomAssociate(PROSPECT_ORDER_CLOSED_BY);
									$data['tck_owner_member_id'] = $my['user_id'];
									$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
									$data['tck_owner_member_email'] = $my['email']; 
									$data['project_manager'] = $data['order_closed_by'] = $data['tck_owner_member_id'] = $my['user_id'] ; 
									$data['marketing_email'] = $randomUser['email']  ;
									$data['marketing_contact'] = $randomUser['marketing_contact'];
									$data['marketing'] = 1; 
									$data['display_name'] = $randomUser['name']  ;
									$data['display_user_id'] = $randomUser['user_id']  ;
									$data['display_designation'] = $randomUser['desig']  ; 
									$data['order_closed_by2'] = 	$randomUser['user_id'];
									$data['order_closed_by2_name'] = $randomUser['name'];									
								}
								
							}else{ 
								$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
								$data['tck_owner_member_name']= SALES_MEMBER_USER_NAME;
								$data['tck_owner_member_email'] = SALES_MEMBER_USER_EMAIL;
								$data['display_name'] = SALES_MEMBER_USER_NAME;
								$data['marketing_email'] = SALES_MEMBER_USER_EMAIL;
								$data['marketing_contact'] = '';
								$data['marketing'] = 0;
							}
							
													
							//Check that this user's ticket is exist or not
							$ticket_id = 0;
							$sql = " SELECT ticket_id,ticket_no FROM ".TABLE_PROSPECTS_TICKETS." WHERE 
								".TABLE_PROSPECTS_TICKETS.".ticket_owner_uid = '".$data['user_id']."' 
								AND ".TABLE_PROSPECTS_TICKETS.".service_pdf_tkt ='1' AND 
								".TABLE_PROSPECTS_TICKETS.".ticket_child=0 LIMIT 0,1";
							$db->query($sql) ;
							$mticket_no='';
							if($db->nf() > 0){
								$db->next_record() ;
								$ticket_id = $db->f("ticket_id") ; 
								$mticket_no = $db->f("ticket_no") ;
							}
							
							if($ticket_id>0){
								
								$query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
								. TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
								. TABLE_PROSPECTS_TICKETS .".template_id       = '". $data['template_id'] ."', "
								. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
								. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
								. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_creator  = '". $my['f_name']." ". $my['l_name'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_subject  = '". $data['subject'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_text     = '". $data['text'] ."', "
								. TABLE_PROSPECTS_TICKETS .".mail_to_all_su  = '".$mail_to_all_su."', "
								. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_status   = '".$ticket_status ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_child    = '".$ticket_id."', "
								. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
								. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
								. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
								. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
								. TABLE_PROSPECTS_TICKETS .".service_pdf_tkt = '1', "
								. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
								. TABLE_PROSPECTS_TICKETS .".ticket_replied  = '0', "
								. TABLE_PROSPECTS_TICKETS .".from_admin_panel = '".ProspectsTicket::ADMIN_PANEL."', "
								. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
								. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
								. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
								. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
								. TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name']."', "
								. TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
								. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
								. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
								. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
								
							
							}else{
								$mticket_no	=$ticket_no;
								$query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
								. TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
								. TABLE_PROSPECTS_TICKETS .".template_id       = '". $data['template_id'] ."', "
								. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
								. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
								. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid = '". $my['uid'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_creator  	= '". $my['f_name']." ". $my['l_name'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_subject  	= '". $data['subject'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_text     	= '". $data['text'] ."', "
								. TABLE_PROSPECTS_TICKETS .".mail_to_all_su  	= '".$mail_to_all_su."', "
								. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_status   	= '".$ticket_status ."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_child    	= '0', "
								. TABLE_PROSPECTS_TICKETS .".hrs1 				= '". $hrs1 ."', "
								. TABLE_PROSPECTS_TICKETS .".min1 				= '". $min1 ."', "
								. TABLE_PROSPECTS_TICKETS .".hrs 				= '". $data['hrs'] ."', "
								. TABLE_PROSPECTS_TICKETS .".min 				= '". $data['min']."', "
								. TABLE_PROSPECTS_TICKETS .".service_pdf_tkt 	= '1', "
								. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
								. TABLE_PROSPECTS_TICKETS .".ticket_replied  = '0', "
								. TABLE_PROSPECTS_TICKETS .".from_admin_panel = '".ProspectsTicket::ADMIN_PANEL."', "
								. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
								. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
								. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
								. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
								. TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name']."', "
								. TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
								. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
								. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
								. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
								. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
								. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
							}
							if(!empty($data['template_file_1'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
							}
							if(!empty($data['template_file_2'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
							}
							if(!empty($data['template_file_3'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
							}
							if(!empty($data['email']) || !empty($data['email_1']) || !empty($data['email_2'])){
								$db->query($query) ;
								$variables['hid'] = $db->last_inserted_id() ;
								$mail_send_to_su='';
								$data['mticket_no'] = $mticket_no ;
								
								if ( getParsedEmail($db, $s, 'EMAIL_PST_MEMBER', $data, $email) ){          
									if(!empty($data['marketing_email'])){
										$email["from_name"] =   $email["from_name"]." - ".$data['tck_owner_member_name'];
										$email["from_email"] = $data['marketing_email'];
									}
									$to = '';    
									if(!empty($data['email'])){ 
										$to[]   = array('name' => $data['email'] , 'email' => 
										$data['email']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email'];
										//echo $email["body"];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */ 
									}
									if(!empty($data['email_1'])){
										//$to = '';
										$to[]   = array('name' => $data['email_1'] , 'email' => 
										$data['email_1']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email_1'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
									}
									if(!empty($data['email_2'])){
										//$to = '';
										$to[]   = array('name' => $data['email_2'] , 'email' => 
										$data['email_2']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email_2'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
									if(!empty($to)){
										//$to = '';
										$bcc[]  = array('name' => $data['ticket_owner'] , 'email' => 
										$smeerp_client_email);        
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); 
									} 
								} 
								if(!empty($mail_send_to_su)){
									$query_update = "UPDATE ". TABLE_PROSPECTS_TICKETS 
										." SET ". TABLE_PROSPECTS_TICKETS .".mail_send_to_su
										= '".trim($mail_send_to_su,",")."'"                                   
										." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
									$db->query($query_update) ;
								}
							} 
						}
					}
					 
					/*SEND SERVICE PDF EOF*/ 
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Prospects Profile was not updated.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
       /*  if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/prospects-list.php');
        }
        else { */
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'            => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
                
                // Preserve the Addresses.
                $count = count($_ALL_POST['city']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['address_list'][] = array(   'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                //'company_name'   => isset($_ALL_POST['company_name'][$i])? $_ALL_POST['company_name'][$i]:'',
                                                                'is_verified'   => isset($_ALL_POST['is_verified'][$i])? $_ALL_POST['is_verified'][$i]:'',
                                                                'is_preferred'  => isset($_ALL_POST['is_preferred'][$i])? $_ALL_POST['is_preferred'][$i]:'',
                                                                'address_type'  => isset($_ALL_POST['address_type'][$i])? $_ALL_POST['address_type'][$i]:'',
                                                                'address'       => isset($_ALL_POST['address'][$i])? $_ALL_POST['address'][$i]:'',
                                                                'city'          => isset($_ALL_POST['city'][$i])? $_ALL_POST['city'][$i]:'',
                                                                'state'         => isset($_ALL_POST['state'][$i])? $_ALL_POST['state'][$i]:'',
                                                                'zipcode'       => isset($_ALL_POST['zipcode'][$i])? $_ALL_POST['zipcode'][$i]:'',
                                                                'country'       => isset($_ALL_POST['country'][$i])? $_ALL_POST['country'][$i]:'',
                                                     );
//                    }
                }

                
                // Preserve the Reminders.
                $count = $_ALL_POST['reminder_count'];
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['remind_date_'.$i) && !empty($_ALL_POST['remind_date_'.$i) ) {
                        $_ALL_POST['reminder_list'][] = array(  'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'do_reminder'   => isset($_ALL_POST['remind_date_'.$i])? $_ALL_POST['remind_date_'.$i]:'',
                                                                'description'   => isset($_ALL_POST['remind_text_'.$i])? $_ALL_POST['remind_text_'.$i]:'',
                                                     );
//                    }
                }
                
                if ( !empty($_ALL_POST['spouse_dob']) ) {
                    $temp = explode('/', $_ALL_POST['spouse_dob']);
                    $_ALL_POST['spouse_dob'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                }
                
                if ( !empty($_ALL_POST['do_reg']) ) {
                    $temp = explode('/', $_ALL_POST['do_reg']);
                    $_ALL_POST['do_reg'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                }
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( Prospects::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                    if ( empty($_ALL_POST['country']) ) {
                        $_ALL_POST['country'] = '91';
                    }
                    $_ALL_POST['password'] = '';
                    $_ALL_POST['manager']  = Prospects::getManager($db, '', $_ALL_POST['manager']);
		    //$_ALL_POST['roles']  = explode(',', $_ALL_POST['roles']);
					
                    if(!empty($_ALL_POST['service_id'])){
                        $_ALL_POST['service_id'] = trim($_ALL_POST['service_id'],",");
                        $_ALL_POST['service_id']    = explode(",", $_ALL_POST['service_id']);
                    }
                    if(!empty($_ALL_POST['authorization_id'])){
                        $_ALL_POST['authorization_id'] = trim($_ALL_POST['authorization_id'],",");
                        $_ALL_POST['authorization_id'] = explode(",", $_ALL_POST['authorization_id']);
                    }
                    $_ALL_POST['industry']    = explode(",", $_ALL_POST['industry']);
					// BO: Read the Team Members Information.
					if ( !empty($_ALL_POST['team']) && !is_array($_ALL_POST['team']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						$_ALL_POST['team'] = trim($_ALL_POST['team'],',');
						$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
						$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
						$_ALL_POST['team_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['team'] = array();
						if(!empty($_ALL_POST['team_members'])){
							foreach ( $_ALL_POST['team_members'] as $key=>$members) {
							
								$_ALL_POST['team'][] = $members['user_id'];
								$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
							}
						}
				    }
                
					// EO: Read the Team Members Information.
					 // BO: Read the Team Members Information.
					if ( !empty($_ALL_POST['alloted_clients']) && !is_array($_ALL_POST['alloted_clients']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
                        $_ALL_POST['alloted_clients'] = trim($_ALL_POST['alloted_clients'],',');
						$_ALL_POST['alloted_clients']          = explode(',', $_ALL_POST['alloted_clients']);
						$temp                       = "'". implode("','", $_ALL_POST['alloted_clients']) ."'";
						$_ALL_POST['allotted_to_clients']  = '';
						$_ALL_POST['client_details']  = array();
						Prospects::getList($db, $_ALL_POST['allotted_to_clients'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['alloted_clients'] = array();
						foreach ( $_ALL_POST['allotted_to_clients'] as $key1=>$members1) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['alloted_clients'][] = $members1['user_id'];
							$_ALL_POST['client_details'][] = $members1['f_name'] .' '. $members1['l_name'] .' 
							('. $members1['number'] .')';
						}
					}
                    //Uncomment me after editing Prospects list
                    if(!empty($_ALL_POST['username'])){
                        if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{5,80}$/', $_ALL_POST['username']) ) {
                            $_ALL_POST['username']='smeerp';
                        }  
                    }
					// EO: Read the Team Members Information.
					
                    // Check the Access Level.
					// Read the Contact Numbers.
					$phone->setPhoneOf(TABLE_PROSPECTS, $user_id);
					$_ALL_POST['phone'] = $phone->get($db);
					/* if(empty($_ALL_POST['phone'])){
						$_ALL_POST['phone'] = array('id'=>'');
					} */
					// Read the Addresses.
					$region->setAddressOf(TABLE_PROSPECTS, $user_id);
					$_ALL_POST['address_list'] = $region->get();
					 
					if(empty($_ALL_POST['address_list'])){
						$_ALL_POST['address_list'] = array('id'=>'');
					}
					// Read the Reminders.
					$reminder->setReminderOf(TABLE_PROSPECTS, $user_id);
					$_ALL_POST['reminder_list'] = $reminder->get($db);
                }
            }
 
            if ( !empty($_ALL_POST['user_id']) ) {

                // Check the Access Level.
               

                    $phone_types    = $phone->getTypeList();
                    $address_types  = $region->getTypeList();
                    $lst_country    = $region->getCountryList();
                    $lst_state      = $region->getStateList();
                    $lst_city       = $region->getCityList();
                     
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    //print_r($_ALL_POST);
              
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    $hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    $hidden[] = array('name'=> 'ajx','value' => $ajx);
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    $page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
					$page["var"][] = array('variable' => 'source', 'value' => 'source');
                    $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
                    $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
                    $page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
					$page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
                    $page["var"][] = array('variable' => 'lst_financial', 'value' => 'lst_financial');
                    $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
                    $page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-edit.html');
                 
            } else {
                $messages->setErrorMessage("The Selected Prospect was not found.");
            }
        //}
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>