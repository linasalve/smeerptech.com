<?php
        /* Get if anyone want to add executive in diff div ie other than 'add_executive_info' then pass div name here 
            By default div name is add_executive_info
        */
         $div	= isset($_GET["div"]) 	? $_GET["div"]	: 'add_party_info';
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = "WHERE ( parent_id = '' )";
        }
        else {
            $condition_query .= " AND ( parent_id = '' )";
        }
       
        /*$access_level   = $my['access_level'];
        
        // If the User has the Right to View Clients of the same Access Level.
        if ( $perm->has('nc_ue_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " (". TABLE_USER .".access_level < $access_level ) ";
        $condition_query .= ')';
		if(!empty($order_by_table))
        	$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        */
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	Vendors::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        Vendors::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_view_list']     = true;
        $variables['can_add']           = true;
        $variables['can_edit']          = true;
        $variables['can_delete']        = true;
        $variables['can_view_details']  = true;
        $variables['can_change_status'] = true;
/*
        if ( $perm->has('nc_u_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_u_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_u_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_u_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_u_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_u_status') ) {
            $variables['can_change_status']     = true;
        }
*/
        $page["var"][] = array('variable' => 'div', 'value' => 'div');
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-choose-list.html');
  
?>