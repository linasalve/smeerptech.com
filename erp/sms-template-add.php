<?php
  if ( $perm->has('nc_sms_api_add') ) { 
        
		$id      = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
		
		$_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
		if(!empty($id)){ 
			$fields = TABLE_SMS_TEMPLATES .'.*' ;
			$condition_query1 = " WHERE ". TABLE_SMS_TEMPLATES.".id= '". $id."'" ;			
			if ( SmsTemplate::getDetails($db, $_ALL_POST, $fields, $condition_query1) > 0 ) {
				$_ALL_POST=$_ALL_POST[0];
				$_ALL_POST['template_name']='';
			}
		}
       
		$lst_account = null;
        $fields = TABLE_SMS_CLIENT_ACCOUNT.'.id'
                   .','. TABLE_SMS_CLIENT_ACCOUNT .'.name'
                   .','. TABLE_SMS_CLIENT_ACCOUNT .'.balance_sms'
                   .','. TABLE_SMS_CLIENT_ACCOUNT .'.do_expiry'
                   .','. TABLE_SMS_CLIENT_ACCOUNT .'.client_id';
		//$cond1 = " WHERE ".TABLE_SMS_CLIENT_ACCOUNT.".status = '".SmsClientAccount::ACTIVE."'";
        SmsClientAccount::getList($db,$lst_account,$fields,$cond1); 
		
		// Include the payment party class 
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data	= processUserData($_ALL_POST);
            $files  = processUserData($_FILES); 
			$CSV 	= sendSMS::getRestrictions();
			$extra  = array( 
							'db' 		=> &$db,
                            'file'      => &$files,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );          
                
                if ( SmsTemplate::validateAdd($data, $extra) ) {
					if(!empty($data['sms_account_id'])){
						 
						 $fields = TABLE_SMS_CLIENT_ACCOUNT.'.id'
						.','. TABLE_SMS_CLIENT_ACCOUNT .'.name'
					   .','. TABLE_SMS_CLIENT_ACCOUNT .'.balance_sms'
					   .','. TABLE_SMS_CLIENT_ACCOUNT .'.do_expiry'
					   .','. TABLE_SMS_CLIENT_ACCOUNT .'.client_id';
						$cond1 = " WHERE ".TABLE_SMS_CLIENT_ACCOUNT.".id = '".$data['sms_account_id']."'";
						SmsClientAccount::getList($db,$account_details,$fields,$cond1);
						$account_details = $account_details[0];
					}
					
					$message_duplicate = $message = '';
						
					if(!empty($files)){
						$filedata["extension"]='';
						$filedata = pathinfo($files['sample_file']["name"]);                   
						$ext = $filedata["extension"] ; 
						$data['sample_file'] = date('Y-m-d')."-".mktime()."-template.".$ext ; 
						if(move_uploaded_file($files['sample_file']['tmp_name'], DIR_FS_SMS_TEMPLATES."/".$data['sample_file'])){ 
						  @chmod(DIR_FS_SMS_TEMPLATES."/".$data['sample_file'],0777);
						}
					} 
					$template_message = $data['col1']."________". $data['col2'] ."________".$data['col3']."________".$data['col4']."________".$data['col5']."________" ;
					
					
                    $query	= " INSERT INTO ".TABLE_SMS_TEMPLATES
                            ." SET ".TABLE_SMS_TEMPLATES .".sms_account_id = '". $data['sms_account_id'] ."'"  
							.",". TABLE_SMS_TEMPLATES .".client_id	= '". $account_details['client_id'] ."'"                      
							.",". TABLE_SMS_TEMPLATES .".template_name	= '". $data['template_name'] ."'"                      
							.",". TABLE_SMS_TEMPLATES .".template_message	= '". $template_message ."'"                      
							.",". TABLE_SMS_TEMPLATES .".sample_file	= '". $data['sample_file'] ."'"                      
							.",". TABLE_SMS_TEMPLATES .".col1	= '". $data['col1'] ."'"     
							.",". TABLE_SMS_TEMPLATES .".col2	= '". $data['col2'] ."'"     
							.",". TABLE_SMS_TEMPLATES .".col3	= '". $data['col3'] ."'"     
							.",". TABLE_SMS_TEMPLATES .".col4	= '". $data['col4'] ."'"     
							.",". TABLE_SMS_TEMPLATES .".col5	= '". $data['col5'] ."'"     
							.",". TABLE_SMS_TEMPLATES .".status_marked_by 	= '". $my['user_id'] ."'"                    
							.",". TABLE_SMS_TEMPLATES .".status_marked_by_name = '".  $my['f_name']." ".$my['l_name'] ."'"     
							.",". TABLE_SMS_TEMPLATES .".status_marked_dt = '".date('Y-m-d H:i:s') ."'"    							
							.",". TABLE_SMS_TEMPLATES .".created_by 	= '". $my['user_id'] ."'"                    
							.",". TABLE_SMS_TEMPLATES .".created_by_name 	= '". $my['f_name']." ".$my['l_name'] ."'"                    
							.",". TABLE_SMS_TEMPLATES .".status 		= '". $data['status'] ."'"                    
							.",". TABLE_SMS_TEMPLATES .".ip 			= '". $_SERVER['REMOTE_ADDR'] ."'"        
							.",". TABLE_SMS_TEMPLATES .".do_e = '". 	date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sms-template.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sms-template.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sms-template.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'lst_account', 'value' => 'lst_account');     
            
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-template-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
