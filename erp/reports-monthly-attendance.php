<?php


if ( $perm->has('nc_rp_m_att') ) {   

    
    include_once ( DIR_FS_INCLUDES .'/hr-attendance.inc.php' );
    
    $month	= isset($_GET["month"]) 	? $_GET["month"]	: ( isset($_POST["month"]) 	? $_POST["month"] : '' );
    $year	= isset($_GET["year"]) 	? $_GET["year"]	: ( isset($_POST["year"]) 	? $_POST["year"] : '' );
    $_SEARCH["month"] 	= $month;
    $_SEARCH["year"] 	= $year;
    $_ALL_POST = $_GET 	? $_GET	: ($_POST 	? $_POST : '' );
    $extra_url=$pagination ='';
    $reportList=array();
    $db1 		= new db_local;
    $lst_month = Reports::getMonth();
    $lst_year = Reports::getYear();
     $dateList=array();
    //to view report date should be compulsary
    if(array_key_exists('submit_search',$_ALL_POST)){
    
        
         $data		= processUserData($_ALL_POST);
         
         $extra = array( 'db' 				=> &$db,
                        'messages'          => &$messages
                    );          
                    
        if (Reports::validateMonthAttAdd($data, $extra) ) {
            
           
            
            $sql =" SELECT COUNT(*) as count FROM 
			".TABLE_USER." WHERE ".TABLE_USER.".do_resign = '0000-00-00 00:00:00' AND 
			".TABLE_USER.".status='".User::ACTIVE."'";
            
            $db->query($sql) ;
            if($db->nf() > 0){
				while($db->next_record()){
              
					$total = $db->f('count');
				}
            }else{
                $total = 0 ;
            }
            
            $days = date('t',mktime(0,0,0,$data['month'],1,$data['year']) );
            $month_name = $lst_month[$data['month']];
            
            
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            
			$sql =" SELECT ".TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".user_id FROM 
			".TABLE_USER." WHERE ".TABLE_USER.".do_resign = '0000-00-00 00:00:00' AND 
			".TABLE_USER.".status='".User::ACTIVE."'";
			$query .= " LIMIT ". $next_record .", ". $rpp ;
		    $db->query($sql) ;
           
            while($db->next_record()){
              
                $name = $db->f('f_name')." ".$db->f('l_name');
                $user_id = $db->f('user_id');
                $details=array();
                $presentCount=0;
                for($i=1;$i<=$days;$i++){
                    $pstatus='-';
                    $i = strlen($i)==1 ? '0'.$i : $i ;
                    $mn = strlen($data['month'])==1 ? '0'.$data['month'] : $data['month'] ;
                    $chdate = $data['year']."-".$data['month']."-".$i ;
                   
                    $sql1 = " SELECT pstatus, MIN(".TABLE_HR_ATTENDANCE.".att_time_in) as att_time_in, 
					MAX(".TABLE_HR_ATTENDANCE.".att_time_out) as att_time_out FROM 
					".TABLE_HR_ATTENDANCE." WHERE ".TABLE_HR_ATTENDANCE.".date='".$chdate."' AND  
					".TABLE_HR_ATTENDANCE.".uid='".$user_id."' 
                    GROUP BY ".TABLE_HR_ATTENDANCE.".uid,".TABLE_HR_ATTENDANCE.".date
                    ORDER BY ".TABLE_HR_ATTENDANCE.".att_time_in, ".TABLE_HR_ATTENDANCE.".uid ASC LIMIT 0,1";
                                       
                    $db1->query($sql1) ;
                    $time_in ='';
                    $time_out='';
                    $bgcolor_in = '';
                    $bgcolor_out = '';
					
                    while($db1->next_record()){
                     $cstatus =  $db1->f('pstatus');
                     $att_time_in =  $db1->f('att_time_in');
                     $att_time_out =  $db1->f('att_time_out');
                     $bgcolor_in = '';
                     $time_in_c = '';
                     $time_out_c = '';
                     
                     if($cstatus==HrAttendance::PRESENT ){
                       
                        $pstatus = 'Present';
                        $presentCount++;
                        if($att_time_in !='0000-00-00 00:00:00'){
                           $time_in = synchronizeTime($datetime->dateToUnixTime($att_time_in), $my["time_offset"] );
                        }
                        if($att_time_out!='0000-00-00 00:00:00'){
                            $time_out = synchronizeTime($datetime->dateToUnixTime($att_time_out), $my["time_offset"] );
              
                        }
                       
                        if($time_in !='0000-00-00 00:00:00' && !empty($time_in)){
                            $time_in_c = (int) date("Hi",$time_in);
                           
                            if($time_in_c >IN_TIME_CHECK){
                                $bgcolor_in=IN_COLOR;
                            }else{
                                $bgcolor_in='';
                            }
                        }
                        if($time_out!='0000-00-00 00:00:00' && !empty($time_out)){
                            $time_out_c = date("Hi",$time_out);
                            if($time_out_c > OUT_TIME_CHECK){
                                $bgcolor_out= OUT_COLOR;
                            }else{
                                $bgcolor_out='';
                            }
                        }
                        
                        
                      }elseif($cstatus==HrAttendance::PAIDLEAVE){
                           $pstatus = 'Paid Leave';
                           $time_in ='';
                           $time_out='';
                      }elseif($cstatus==HrAttendance::NONPAIDLEAVE){
                            $pstatus = 'NonPaid Leave';
                            $time_in ='';
                            $time_out='';
                      }
                     
                    }
                    
                   
                    $details[$i] = array('status'=>$pstatus,
                                         'time_in'=>$time_in,
                                         'time_out'=>$time_out,
                                         'bgcolor_in'=>$bgcolor_in,
                                         'bgcolor_out'=>$bgcolor_out,
                                        );
                     if(empty($dateList[$i])){
                                
                       $dateList[$i]=$i;
                     }
                }
      
                $list[] = array(
                                'name'=>$name,
                                'attendance'=>$details,
                                'presentCount'=>$presentCount
                                );
                
            }
            
           
        }
    }   
            
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
   
        
 
 
    $page["var"][] = array('variable' => 'lst_month', 'value' => 'lst_month');
    $page["var"][] = array('variable' => 'lst_year', 'value' => 'lst_year');
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'dateList', 'value' => 'dateList');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'totalHrsArr', 'value' => 'totalHrsArr');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-monthly-attendance.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>