<?php
	if (!defined("THIS_DOMAIN")){
		require_once("../lib/config.php"); 
	} 

	page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                ));

    include( DIR_FS_NC ."/header.php" );    
	include( DIR_FS_NC ."/dashboard-orders.php");    
	//include( DIR_FS_NC ."/dashboard-orders-new.php");    

    //$page["section"][] = array('container'=>'CONTENT', 'page' => 'content-new.html');
    //$page["section"][] = array('container'=>'INDEX', 'page' => 'index-new.html'); 
    
	$page["section"][] = array('container'=>'CONTENT', 'page' => 'content.html');
    $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');  

    //$page["section"][] = array('container'=>'INDEX', 'page' => 'index-1.html');
    //$page["section"][] = array('container'=>'INDEX', 'page' => 'main-content.html');
   
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	
	//Unset the value variables
	if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            unset(${$fetch["value"]});
        }
    }
	include_once( DIR_FS_NC ."/flush.php");
?>