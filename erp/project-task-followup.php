<?php   
if( $perm->has('nc_p_ts_add_flw')){
    
    $or_id 			= isset($_GET["or_id"]) ? $_GET["or_id"]        : ( isset($_POST["or_id"])          ? $_POST["or_id"]       :'');
    $client_id 			= isset($_GET["client_id"]) ? $_GET["client_id"]        : ( isset($_POST["client_id"])          ? $_POST["client_id"]       :'');
   
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    include_once (DIR_FS_INCLUDES .'/department.inc.php');
    include_once (DIR_FS_INCLUDES .'/clients.inc.php');
    
    $order_title='';
    
    if(!empty($or_id)){
        $order_list = NULL;
        $required_fields ='client'.','.'order_title';
        $condition_query="WHERE id='".$or_id."'";
		Order::getList($db,$order_list,$required_fields,$condition_query);
        $client_id = $order_list[0]['client'];
        $order_title = $order_list[0]['order_title'];        
        
    }
    $department_list = NULL;
    $condition_querydept= "WHERE status = '". ACTIVE ."' ORDER BY department_name ASC";
    Department::getList($db, $department_list, 'id,department_name', $condition_querydept);    
    $required_fields1 = 'title,f_name,l_name,email,status,send_mail,mobile1,mobile2';
    $condition_query1 = " WHERE user_id='".$client_id."' ";
    Clients::getList($db,$client_details,$required_fields1,$condition_query1);
    $client_details=$client_details[0];
    $subUserDetails=null;
    Clients::getList($db, $subUserDetails, 'number,user_id, f_name, l_name,status,send_mail,email,email_1,email_2,email_3,email_4,
    mobile1,mobile2', " WHERE parent_id = '".$client_id."' ");
  
    $_ALL_POST	    = NULL;
    $access_level   = $my['access_level'];
    $submitType =1;
    $variables['flwType']=array_flip(ProjectTask::getFollowupType());
   
    if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
       
        $_ALL_POST  = $_POST;
        
        $data = processUserData($_ALL_POST);
        $files		= processUserData($_FILES);
        
        $submitType=$data['submitType'];
        $extra = array( 'db'        => &$db,
                'access_level'      => $access_level,
                'files'          => &$files,
                'messages'          => &$messages
            );
            
        $data['allowed_file_types'] = $allowed_file_types ;
        $data['max_file_size']= MAX_MULTIPLE_FILE_SIZE ;     
        
        if ( ProjectTask::validateFollowupAdd($data, $extra) ) {
             $mobile1 ='';
             $mobile2 ='';
             $client_mobile = $sms_send_to='';
             
            if ($data['submitType'] == '3' ) {
                $data ['type'] = ProjectTask::BYMAIL;
                $i=1;
                $attachfilename='';
                if(!empty($files["attached_file"]["name"])){
                   
                    //$val["name"] = str_replace(' ','-',$val["name"]);
                    $filedata["extension"]='';
                    $filedata = pathinfo($files["attached_file"]["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    $attachfilename = mktime()."-".$i.".".$ext ;
                  
                   if(!empty($files["attached_file"]["name"])){
                 
                        if (move_uploaded_file($files["attached_file"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename, 0777);
                           
                        }
                    }
                    $i++;                    
                }
                
                if(empty($data['client_email'])){                               
                    $data['client_contact_person'] = $client_details['title']." ".$client_details['f_name']." ".$client_details['l_name'];
                    $data['client_email'] = $client_details['email'];
                }
            }elseif($data['submitType'] == '2' ) {
                $data ['type'] = ProjectTask::BYSMS;
                $message = $data['comment'] ;
                $counter=0;
                if(isset($data['mail_client'])){
                    if(!empty($client_details['mobile1'])){
                         $mobile1 = "91".$client_details['mobile1'];
                         $client_mobile .= ",".$mobile1;
                         ProjectTask::sendSms($message,$mobile1);
                         $counter = $counter+1;
                    }
                    if(!empty($client_details['mobile2'])){
                         $mobile2 = "91".$client_details['mobile2'];
                         $client_mobile .= ','.$mobile2;
                         ProjectTask::sendSms($message,$mobile2);
                         $counter = $counter+1;
                    }
                    $sms_send_to .= ','.$client_details['f_name']." ".$client_details['l_name'];
                }
               
                if(isset($data['mail_to_all_su']) || isset($data['mail_to_su']) ){
                    $subUsersmsDetails = null;
                    $mobile='';
                    if(isset($_POST['mail_to_all_su'])){
                        Clients::getList($db, $subUsersmsDetails, 'number, f_name, l_name,send_mail, mobile1,mobile2', " 
                        WHERE parent_id = '".$client_id."' AND  send_mail='".Clients::SENDMAILYES."'   AND status='".Clients::ACTIVE."' AND ( mobile1 !='' OR mobile2 != '' )  ");
                    }elseif(isset($data['mail_to_su']) && !empty($data['mail_to_su'])){
                        $mail_to_su= implode("','",$data['mail_to_su']);
                        Clients::getList($db, $subUsersmsDetails, 'number, f_name, l_name,send_mail, mobile1,mobile2', " 
                        WHERE parent_id = '".$client_id."' AND  send_mail='".Clients::SENDMAILYES."'  
                        AND user_id IN('".$mail_to_su."')
                        AND status='".Clients::ACTIVE."' AND ( mobile1 !='' OR mobile2 != '' )  ");
                    }
                    if(!empty($subUsersmsDetails)){
                        foreach ($subUsersmsDetails  as $key=>$value){
                            if(!empty($value['mobile1'])){
                                $mobile = "91".$value['mobile1'];
                                $client_mobile .= ",".$mobile;
                                ProjectTask::sendSms($message,$mobile);
                                $counter = $counter+1;
                            }
                            if(!empty($value['mobile2'])){
                                $mobile = "91".$value['mobile2'];
                                $client_mobile .= ",".$mobile;
                                ProjectTask::sendSms($message,$mobile);
                                $counter = $counter+1;
                            }   
                            $sms_send_to .= ','.$value['f_name']." ".$value['l_name']      ;                 
                        }
                    }
                }
                $sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
                $db->query($sql); 
                
                /*
                if(empty($data['client_mobile']) && $data['client_mobile_exist'] ==1){
                    $client_details = $client_details['0'];
                    $mobile1 = "91".$client_details['mobile1'];
                    $mobile2 = "91".$client_details['mobile2'];  
                                   
                    if(!empty($mobile1)){
                       $client_mobile = $mobile1;   
                       $sms_response .=  ProjectTask::sendSms($message,$mobile1)."<br/>";
                    }
                    if(!empty($mobile2)){
                        if(!empty($client_mobile)){
                            $client_mobile.= "," ;
                        }
                        $client_mobile.= $mobile2 ;
                        $sms_response .=  ProjectTask::sendSms($message,$mobile2);
                        
                    }                    
                }elseif(!empty($data['client_mobile'])){
                
                     $client_mobile ="91".$data['client_mobile'];
                     $sms_response = ProjectTask::sendSms($message,$client_mobile);
                }  
                */                
                //  update sms counter
                
            }
           
            $query	= " INSERT INTO ".TABLE_PROJECT_TASK_FOLLOWUP
                    ." SET ". TABLE_PROJECT_TASK_FOLLOWUP .".or_id 	= '". $or_id ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".client_id 		= '". $client_id ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".type 		= '". $data ['type'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".comment 		= '". $data['comment'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".client_contact_person  = '". $data['client_contact_person'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".client_comment    		= '". $data['client_comment'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".client_email    		= '". $data['client_email'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".client_mobile    		= '". $client_mobile."," ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".sms_send_to    		= '". $sms_send_to."," ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".client_phone    		= '". $data['client_phone'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".email_subject    		= '". $data['email_subject'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".attached_file    		= '". $attachfilename ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".department_id    		= '". $data['department_id'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".ip         	    = '". $_SERVER['REMOTE_ADDR'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
                        .",". TABLE_PROJECT_TASK_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
                         
            $db->query($query); 
            
            if ($data['submitType'] == '3' ) {
                
                $file_name = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename;
                
                $data['af_name'] =$my['f_name'];
                $data['al_name'] = $my['l_name'] ;
                $data['subject'] = $data['email_subject'] ;
                $data['message'] = $data['comment'] ;
                $cc_to_sender = $cc = $bcc = NULL;
                if ( getParsedEmail($db, $s, 'TASK_FOLLOWUP_CLIENT', $data, $email) ) {
                    $to     = '';                    
                    $to[] = array(	"name" 	=> $data['client_contact_person'] ,
                                "email" => $data['client_email']
                             );                   
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], $cc_to_sender, $cc, $bcc, $file_name);
                }
                
                $data['project_title'] = $order_title ;
                if ( getParsedEmail($db, $s, 'TASK_FOLLOWUP_ADMIN', $data, $email) ) {
                    $to     = '';                    
                    $to[] = array(	"name" 	=> $admin_name ,
                                "email" => $admin_email
                             );                   
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], $cc_to_sender, $cc, $bcc, $file_name);
                }
                $messages->setOkMessage("Email has been sent successfully.");
                //SendMail($to, $from, $reply_to, $data['email_subject'], $data['comment'], $is_html, $sendCopy, $cc, $bcc, $file_name);
            }
        }
    }
   
     // get list of all comments of change log BOF
    $condition_query1 = $condition_url1 =$extra_url= '';
    $condition_url1 .="&perform=".$perform."&or_id=".$or_id."&client_id=".$client_id;
    if(!empty($or_id)){
        $followup_sql = " WHERE ".TABLE_PROJECT_TASK_FOLLOWUP.".or_id = '".$or_id."'";
    }elseif(!empty($client_id)){
        $followup_sql = " WHERE ".TABLE_PROJECT_TASK_FOLLOWUP.".client_id = '".$client_id."'";
    }
    $perform = 'followup';
    $followupList	= 	NULL;
    $fields = TABLE_PROJECT_TASK_FOLLOWUP.".id " ;
    $condition_query1 = " LEFT JOIN ".TABLE_SETTINGS_DEPARTMENT." ON ".TABLE_SETTINGS_DEPARTMENT.".id= ".TABLE_PROJECT_TASK_FOLLOWUP.".department_id";
    $condition_query1 .= " LEFT JOIN ".TABLE_BILL_ORDERS." ON ".TABLE_BILL_ORDERS.".id= ".TABLE_PROJECT_TASK_FOLLOWUP.".or_id";
    $condition_query1 .= " LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id= ".TABLE_PROJECT_TASK_FOLLOWUP.".created_by  ".$followup_sql ;
    $condition_query1 .= " ORDER BY ".TABLE_PROJECT_TASK_FOLLOWUP.".do_e DESC";
    $total	=	ProjectTask::getDetailsCommLog( $db, $followupList, $fields , $condition_query1);
   
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
    //$extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  .= "&perform=".$perform;
    $extra_url  .= "&or_id=".$or_id ;
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $followupList	= NULL;
    $fields = TABLE_PROJECT_TASK_FOLLOWUP .".*,".TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_SETTINGS_DEPARTMENT.".department_name,".TABLE_BILL_ORDERS.".order_title";   
    ProjectTask::getDetailsCommLog( $db, $followupList, $fields, $condition_query1, $next_record, $rpp);
    $condition_url .="&perform=".$perform;
  
    $fList=array();
    $typeArr = ProjectTask::getFollowupTypeAll();
    if(!empty($followupList)){
        foreach( $followupList as $key=>$val){        
           $val['type_title'] = $typeArr[$val['type']];
           $fList[$key]=$val;
        }
    }
    /*
    $fList=array();
    if(!empty($followupLog)){
        foreach( $followupLog as $key=>$val){  
           $fList[$key]=$val;
        }
    }
    */
    $variables['can_view_client_contact_details'] = false;
    if ( $perm->has('nc_uc_contact_details') ) {
        $variables['can_view_client_contact_details'] = true;
    }
    $variables['client_sendmail'] = Clients::SENDMAILNO;
    $hidden[] = array('name'=> 'or_id', 'value' => $or_id);
    $hidden[] = array('name'=> 'client_id', 'value' => $client_id);
    $hidden[] = array('name'=> 'perform', 'value' => 'followup');
    $hidden[] = array('name'=> 'act', 'value' => 'save');
   
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'order_title', 'value' => 'order_title');
    $page["var"][] = array('variable' => 'followupList', 'value' => 'fList');
    $page["var"][] = array('variable' => 'department_list', 'value' => 'department_list');
    $page["var"][] = array('variable' => 'submitType', 'value' => 'submitType');
    $page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
    $page["var"][] = array('variable' => 'client_details', 'value' => 'client_details');
    //$page["var"][] = array('variable' => 'titles', 'value' => 'titles'); 
    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task-followup.html');  
}else{
    $messages->setErrorMessage("You do not have the Permission to access this module.");
}   
?>