<?php

// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/old-nc-db.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/common-functions.inc.php');
    ini_set("max_execution_time",0); // TO EXECUTE A SCRIPT WITH UN LIMITED TIME
    
    // define tables of old nc BOF
    define("TABLE_USER_OLD","auth_user");
    define("TABLE_MEMBER_DETAILS_OLD","member_details");
	define("TABLE_BILL_INVOICE_OLD","bills_invoice");
	define("TABLE_BILL_ORDERS_OLD","bills_orders");
	//define("TABLE_BILL_MISC","bills_misc");
	//define("TABLE_BILL_USER","bills_member_account");
	define("TABLE_BILL_RECEIPT_OLD","bills_receipt");
	define("TABLE_MIGRATING_ORDER","migrating_order_info");
    // define tables of old nc EOF
   


    $db1 		= new db_local; // database handle
    // connect with old nc d/b
    $con = DB::makeConnection();
    
    echo $checkdate = '2007-03-31 23:59:59' ;
    
    $timedate= mktime("23","59","59", "03", "32", "2007") ; // 31=> 32 as per given in old nc billOrders.php
    $timedate= mktime("00","00","00", 03, 32, 2007) ; // 31=> 32 as per given in old nc billOrders.php
    //$query = "SELECT ".TABLE_BILL_ORDERS_OLD.".*  FROM ".TABLE_BILL_ORDERS_OLD." WHERE order_created_date <= ".$timedate  ;
    //echo $query = "SELECT ".TABLE_BILL_ORDERS_OLD.".*  FROM ".TABLE_BILL_ORDERS_OLD." WHERE order_date <= ".$timedate  ;
    $query = "SELECT ".TABLE_BILL_ORDERS_OLD.".*  FROM ".TABLE_BILL_ORDERS_OLD." "  ;
   
   /*
    $orderExistContent = $clientNotExistContent = $orderEnterContent = '' ;       
	$result = mysql_query($query,$con);
     $listOid = $listOidDone = $userNotFound=array();
	if( mysql_num_rows($result) > 0){
        while ($row = mysql_fetch_array($result)){
            $error=array();
            $uid = $row['uid'];
            $order_id = $row['order_id'];
            
            $b_name = $row['b_name'];
            $b_add1 = $row['b_add1'];
            $b_add2 = $row['b_add2'];
            $order_particulars = $row['order_particulars'];
            $order_status = $row['order_status'];
            $order_date = $row['order_date'];
            $order_no = $row['order_no'];
            $order_created_date = $row['order_created_date'];
           
            $order_date  = date("Y-m-d",$order_date );
            $order_created_date =  date("Y-m-d",$order_created_date );
            
            $client_details = $b_name."\n" ;
            $client_details .= $b_add1."\n" ;
            $client_details .= $b_add2."\n" ;
            // access level 65535 for TOP
            $access_level = "65535";            
            //username aryan 
            $created_by = "e68adc58f2062f58802e4cdcfec0af2d";           
            //aryan project manager
            $manager="e68adc58f2062f58802e4cdcfec0af2d";
            $order_closed_by = "e68adc58f2062f58802e4cdcfec0af2d";
            $order_type =2 ;//TARGETED
            //aryan
            $teamStr ="e68adc58f2062f58802e4cdcfec0af2d";
            // by default  work_stage is QC IN PROCESS
            $work_stage = "10";
            
            $order_list  = NULL;
            //$sqlQuery =" SELECT count(*) as count FROM  ".TABLE_BILL_ORDERS                            
           // $sqlQuery .= " WHERE (number = '".$order_no."') " ;
                        
             $query = "SELECT id FROM ". TABLE_BILL_ORDERS
                        ." WHERE number = '". $order_no."'";
            echo "<br/>";
            if ( $db->query($query) && $db->nf() > 0 ) {
               //$messages->setErrorMessage("Already exist order no.");
                $orderExistContent .= "<br/> Already exist $order_no order no"."\n";
                $listOid[]= $order_id  ;       
                 continue;
            }
            //check user for created order was exist or not 
            $sql ='SELECT * FROM '.TABLE_CLIENTS." WHERE ".TABLE_CLIENTS .".user_id = '".$uid."'";
            $db->query($sql);
            if($db->nf() == 0 ){
            
                  $clientNotExistContent .= "<br/> Client of current order is not exist. User Id is '$uid' and Order No '$order_no' "."\n";
                  $userNotFound[] = $uid;
                  continue;
            }
            //get total amount from invoice table
            $invoice_amount = 0;
            $con1 = DB::makeConnection();
            $sql11 = " SELECT invoice_amount FROM ".TABLE_BILL_INVOICE_OLD." WHERE ".TABLE_BILL_INVOICE_OLD.".order_no ='".$order_no."'" ;
            $result_inv = mysql_query($sql11,$con1);
            if( mysql_num_rows($result_inv) ){
                while ($row_inv = mysql_fetch_array($result_inv)){
                
                
                     echo "<br/>".$invoice_amount = $row_inv['invoice_amount'];
                }
            }
            DB::closeConnection($con1);
            $db 		= new db_local; // database handle
            //if ( empty($error) ){      

                $po_order_status = '4';    
                $poid = 0;
                // create pre order
                
                $order_title = substr(processUserData($order_particulars), 0, 20);  
                if(empty($order_title)){
                    $order_title = $order_no.": Default title ";
                }
                echo "<br/>";
                $order_particulars .= "\n"." Invoice Amount ".$invoice_amount ;
                //create order
                 echo $query	= " INSERT INTO ".TABLE_BILL_ORDERS
                            ." SET ". TABLE_BILL_ORDERS .".number = '".processUserData($order_no )."'"
                                .",". TABLE_BILL_ORDERS .".po_id = '". processUserData($poid )."'"
                                .",". TABLE_BILL_ORDERS .".access_level = '".processUserData($access_level )."'"
                                .",". TABLE_BILL_ORDERS .".company_id = '1'"
                                .",". TABLE_BILL_ORDERS .".client = '".processUserData($uid)."'"
                                .",". TABLE_BILL_ORDERS .".created_by = '".processUserData($created_by )."'"
                                .",". TABLE_BILL_ORDERS .".order_title	= '".processUserData($order_title )."'"
                                .",". TABLE_BILL_ORDERS .".team = '". processUserData($teamStr )."'"
                                .",". TABLE_BILL_ORDERS .".old_particulars = '".processUserData($order_particulars )."'"
                                .",". TABLE_BILL_ORDERS .".details = '".processUserData($order_particulars )."'"
                                .",". TABLE_BILL_ORDERS .".do_c = '".processUserData($order_created_date)."'"
                                .",". TABLE_BILL_ORDERS .".do_o = '".processUserData($order_date )."'"
                                .",". TABLE_BILL_ORDERS .".amount = '".$invoice_amount."'"
                                .",". TABLE_BILL_ORDERS .".do_d = '".processUserData($order_date )."'"
                                .",". TABLE_BILL_ORDERS .".project_manager ='".processUserData($manager)."'"
                                .",". TABLE_BILL_ORDERS .".order_closed_by ='".processUserData($order_closed_by)."'"
                                .",". TABLE_BILL_ORDERS .".st_date         ='' "
                                .",". TABLE_BILL_ORDERS .".es_ed_date  =''"
                                .",". TABLE_BILL_ORDERS .".es_hrs      =''"
                                .",". TABLE_BILL_ORDERS .".is_old      ='1'"
                                .",". TABLE_BILL_ORDERS .".status ='".processUserData($order_status)."'" ;
                
                    $db->query($query)   ;
                    $oid = $db->last_inserted_id();
                    
               echo "<br/>";
                
                $poid='';
                //Add the Record to the Work Timeline.
                
                //migration info               
                echo $sql2 = "INSERT INTO ".TABLE_MIGRATING_ORDER
                       ." SET ". TABLE_MIGRATING_ORDER .".o_no     = '". $order_no ."'"
                       .",". TABLE_MIGRATING_ORDER .".old_oid      = '". $order_id ."'"
                       .",". TABLE_MIGRATING_ORDER .".new_oid      = '". $oid ."'" 
                       .",". TABLE_MIGRATING_ORDER .".new_pid       = '". $poid  ."'" ;
                       
                 $db->query($sql2);
                
                 $listOidDone[]= $order_id  ;
                 $orderEnterContent .= " Order Entered now ".$order_id." \n" ;
                 
            //}else{
               // $listOid[]= $order_id  ;                            
            //}
            
        }
    }
    // already entered users list
    if(!empty( $listOid)){
        echo "<br>";
        echo " Following order are complted previously";
        echo "<br>";
        print_r($listOid);
        echo "<br>";
        
    }
    if($listOidDone){
        echo "Following orders are created Now";
        echo "<br>";
        print_r($listOidDone);
        echo "<br>";
    }
    if(!empty($userNotFound)){
        echo "Following users are not found in database ";
        echo "<br>";
        print_r($userNotFound);
        echo "<br>";
    }
    
    //write the errors in file BOF 
    $file = 'migrate-orders.txt';
    $fp = fopen($file, 'a+');
    $content = " ----------- Script date ".date('Y-m-d')." ----------- \n" ;
	$content .= "------------Already orderExistContent------------"."\n" ;
	$content .= $orderExistContent ;
	$content .= "------------ clientNotExist in New Database ------------"."\n" ;
    $content .= $clientNotExistContent ;
    $content .= "------------ Script entered order (Currently entered orders) ------------"."\n" ;
    echo $content .= $orderEnterContent ;
    
    if (fwrite($fp, $content) === FALSE) {
        echo " Cannot write to file ($file)";
        exit;
    }
     fclose($fp);
	 */
    //write the errors in file EOF 
    
    
?>