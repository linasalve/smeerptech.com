<?php
    if ( $perm->has('nc_csv_upload') ) {
        $_ALL_POST  = NULL;
        $data       = NULL;
        
        $CSV = CSV::getRestrictions();
        // The table in which the data will be imported.
        $table = 'temp_user';
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            $extra = array( 'file'      => $_FILES,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );

            if ( CSV::validateUpload($data, $extra) ) {
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $empty      = 0;
                $index      = array();
                $handle     = fopen($_FILES['file_csv']['tmp_name'],"rb");
                
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
                    if ( $row == 1 ) {
                        $num = count($data);
                        for ($i=0; $i < $num; $i++) {
                            switch ($data[$i]) {
                                case ('user_id'): {
                                    $index['user_id'] = $i;
                                    break;
                                }
                                case ('number'): {
                                    $index['number'] = $i;
                                    break;
                                }
                                case ('username'): {
                                    $index['username'] = $i;
                                    break;
                                }
                                case ('password'): {
                                    $index['password'] = $i;
                                    break;
                                }
                                case ('access_level'): {
                                    $index['access_level'] = $i;
                                    break;
                                }
                                case ('roles'): {
                                    $index['roles'] = $i;
                                    break;
                                }
                                case ('status'): {
                                    $index['status'] = $i;
                                    break;
                                }
                                case ('email'): {
                                    $index['email'] = $i;
                                    break;
                                }
                                case ('email_1'): {
                                    $index['email_1'] = $i;
                                    break;
                                }
                                case ('email_2'): {
                                    $index['email_2'] = $i;
                                    break;
                                }
                                case ('title'): {
                                    $index['title'] = $i;
                                    break;
                                }
                                case ('f_name'): {
                                    $index['f_name'] = $i;
                                    break;
                                }
                                case ('m_name'): {
                                    $index['m_name'] = $i;
                                    break;
                                }
                                case ('l_name'): {
                                    $index['l_name'] = $i;
                                    break;
                                }
                                case ('p_name'): {
                                    $index['p_name'] = $i;
                                    break;
                                }
                                case ('desig'): {
                                    $index['desig'] = $i;
                                    break;
                                }
                                case ('org'): {
                                    $index['org'] = $i;
                                    break;
                                }
                                case ('domain'): {
                                    $index['domain'] = $i;
                                    break;
                                }
                                case ('gender'): {
                                    $index['gender'] = $i;
                                    break;
                                }
                                case ('do_birth'): {
                                    $index['do_birth'] = $i;
                                    break;
                                }
                                case ('do_aniv'): {
                                    $index['do_aniv'] = $i;
                                    break;
                                }
                                case ('do_add'): {
                                    $index['do_add'] = $i;
                                    break;
                                }
                                case ('do_reg'): {
                                    $index['do_reg'] = $i;
                                    break;
                                }
                                case ('policy_check'): {
                                    $index['policy_check'] = $i;
                                    break;
                                }
                                case ('do_login'): {
                                    $index['do_login'] = $i;
                                    break;
                                }
                                case ('remarks'): {
                                    $index['remarks'] = $i;
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        // The Row is not the First row.
                        if ( isset($index['user_id']) && $data[$index['user_id']] != '' ) {
                            if ( !CSV::duplicateFieldValue($db, $table, 'user_id', $data[$index['user_id']]) ) {
                                // Prepare the query using the index created above.
                                $query = " SET ";
                                foreach ( $index as $field=>$f_index ) {
                                    $query .= "$field = '". $data[$f_index] ."', ";
                                }
                                $query = substr($query, 0, (strlen($query)-2) );
                                // To INSERT.
                                $query = "INSERT INTO $table ". $query;
                                if ( $db->query($query) && $db->affected_rows()>0 ) {
                                    $added += 1;
                                }
                            }
                            else {
                                $duplicate += 1;
                            }
                        }
                        else {
                            $empty += 1;
                        }
                    }
                }
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been added.');
                if ($duplicate)
                    $messages->setOkMessage($duplicate .' out of '. $row .' records were duplicate and neglected.');
                if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
        }
        
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/settings-csv.php');
        }
        else {
            $table_list = NULL;
            CSV::getTables($db, $table_list);
            //print_r($table_list);
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'upload');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            $page["var"][] = array('variable' => 'table_list', 'value' => 'table_list');
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-csv-upload.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>