<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_v_su_edit') ) {
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
        $client_id	= isset($_GET["client_id"]) ? $_GET["client_id"] : ( isset($_POST["client_id"]) ? $_POST["client_id"] : '' );
        
        include_once ( DIR_FS_CLASS .'/RegionVendor.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneVendor.class.php');
        //include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
        //include ( DIR_FS_INCLUDES .'/sale-industry.inc.php');
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new RegionVendor();
        $phone          = new PhoneVendor(TABLE_VENDORS);
        //$reminder       = new UserReminder(TABLE_VENDORS);
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_v_su_edit_al') ) {
            $access_level += 1;
        }
        //For Clients list allotment
        /*if ( $perm->has('nc_uc') && $perm->has('nc_uc_list')) {
            $variables['can_view_clist']     = true;
        }*/
        
        $al_list    = getAccessLevel($db, $access_level);
        //$role_list  = Clients::getRoles($db, $access_level);

        //$grade_list  = Clients::getGrades();
        
        //BOF read the available services
        $services_list  = Vendors::getServices();
		$authorization_list  = Vendors::getAuthorization();
        //EOF read the available services
        
        //BOF read the available industries
        /*$industry_list = NULL;
        $required_fields ='*';
        $condition_query="WHERE status='".Industry::ACTIVE."'";
		Industry::getList($db,$industry_list,$required_fields,$condition_query);*/
        //EOF read the available industries
        
        //BOF read the available industries
		Vendors::getCountryCode($db,$country_code);
        //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder
                        );
            
            if ( Vendors::validateSubUserUpdate($data, $extra) ) {
            
                $service_ids = $data['service_id'];
                $service_idstr = implode(",",$service_ids);
                
                if(!empty($service_idstr)){
                    $service_idstr=",".$service_idstr.",";	
                }
                $authorization_idstr = '';
                if(isset($_POST['authorization_id']) && !empty($_POST['authorization_id'])){
                    $authorization_ids=$_POST['authorization_id'];
                    $authorization_idstr=implode(",",$authorization_ids);
                    $authorization_idstr = ",".$authorization_idstr.",";	
                }
				
                $query  = " UPDATE ". TABLE_VENDORS
                        ." SET "
                            ."". TABLE_VENDORS .".service_id   = '". $service_idstr ."'"
							.",". TABLE_VENDORS .".authorization_id   = '". $authorization_idstr ."'"
                           /* .",". TABLE_VENDORS .".number      = '". $data['number'] ."'"
                            .",". TABLE_VENDORS .".manager     = '". $data['manager'] ."'"*/
							//.",". TABLE_VENDORS .".team			= '". implode(',', $data['team']) ."'"
                            .",". TABLE_VENDORS .".username    = '". $data['username'] ."'"
                            . $data['password']
                            //.",". TABLE_VENDORS .".access_level= '". $data['access_level'] ."'"
                            .",". TABLE_VENDORS .".email       = '". $data['email'] ."'"
                            .",". TABLE_VENDORS .".email_1     = '". $data['email_1'] ."'"
                            .",". TABLE_VENDORS .".email_2     = '". $data['email_2'] ."'"
                            .",". TABLE_VENDORS .".title       = '". $data['title'] ."'"
                            .",". TABLE_VENDORS .".f_name      = '". $data['f_name'] ."'"
                            .",". TABLE_VENDORS .".m_name      = '". $data['m_name'] ."'"
                            .",". TABLE_VENDORS .".l_name      = '". $data['l_name'] ."'"
                            .",". TABLE_VENDORS .".p_name      = '". $data['p_name'] ."'"                            
                            //.",". TABLE_VENDORS .".grade      	= '". $data['grade'] ."'"
                            //.",". TABLE_VENDORS .".credit_limit = '". $data['credit_limit'] ."'"
                            .",". TABLE_VENDORS .".desig       = '". $data['desig'] ."'"
                            .",". TABLE_VENDORS .".org         = '". $data['org'] ."'"
                            .",". TABLE_VENDORS .".domain      = '". $data['domain'] ."'"
                            .",". TABLE_VENDORS .".gender      = '". $data['gender'] ."'"
                            .",". TABLE_VENDORS .".do_birth    = '". $data['do_birth'] ."'"
                            .",". TABLE_VENDORS .".do_aniv     = '". $data['do_aniv'] ."'"
                            .",". TABLE_VENDORS .".remarks     = '". $data['remarks'] ."'"
                            .",". TABLE_VENDORS .".mobile1      = '". $data['mobile1'] ."'"
                            .",". TABLE_VENDORS .".mobile2      = '". $data['mobile2'] ."'"
                            .",". TABLE_VENDORS .".allow_ip   = '". $data['allow_ip'] ."'"
                            .",". TABLE_VENDORS .".valid_ip    = '". $data['valid_ip'] ."'"
                            .",". TABLE_VENDORS .".status      = '". $data['status'] ."'" 
                        ." WHERE ". TABLE_VENDORS .".user_id   = '". $data['user_id'] ."'";
                
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['user_id'];

                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_VENDORS) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $phone_arr = array( 'id'            => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
                                                    'p_type'        => $data['p_type'][$i],
                                                    'cc'            => $data['cc'][$i],
                                                    'ac'            => $data['ac'][$i],
                                                    'p_number'      => $data['p_number'][$i],
                                                    'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
                                                    'p_is_verified' => isset($data['p_is_verified'][$i]) ? $data['p_is_verified'][$i] : '0'
                                                );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_VENDORS, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    $messages->setOkMessage("Vendors Profile has been updated.");
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Vendors Profile was not updated.');
                }
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            //$client_id=$client_id
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/vendors-su-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'            => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( Vendors::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                    if ( empty($_ALL_POST['country']) ) {
                            $_ALL_POST['country'] = '91';
                    }
                    $_ALL_POST['password']='';
                    
                    if(!empty($_ALL_POST['service_id'])){
                        $_ALL_POST['service_id'] = trim($_ALL_POST['service_id']);
                        $_ALL_POST['service_id']    = explode(",", $_ALL_POST['service_id']);
                    }
                    if(!empty($_ALL_POST['authorization_id'])){
                        $_ALL_POST['authorization_id'] = trim($_ALL_POST['authorization_id'],",");
                        $_ALL_POST['authorization_id'] = explode(",", $_ALL_POST['authorization_id']);
                    }
                    // Check the Access Level.
                    if ( $_ALL_POST['access_level'] < $access_level ) {
                        // Read the Contact Numbers.
                        $phone->setPhoneOf(TABLE_VENDORS, $user_id);
                        $_ALL_POST['phone'] = $phone->get($db);
                        
                        // Read the Addresses.
                        $region->setAddressOf(TABLE_VENDORS, $user_id);
                        $_ALL_POST['address_list'] = $region->get();
                        
                        // Read the Reminders.
                        /*$reminder->setReminderOf(TABLE_VENDORS, $user_id);
                        $_ALL_POST['reminder_list'] = $reminder->get($db);*/
                    }
                    
                }
            }
 
            if ( !empty($_ALL_POST['user_id']) ) {

                // Check the Access Level.
                if ( $_ALL_POST['access_level'] < $access_level ) {

                    $phone_types    = $phone->getTypeList();
                    
                    $condition_query = " WHERE user_id = '". $client_id ."' ";
                    if ( Vendors::getList($db, $_ALL_POST['main_client_details'], 'service_id', $condition_query) > 0 ) {
                        $_ALL_POST['main_client_details'] = $_ALL_POST['main_client_details'][0];
                        $_ALL_POST['main_client_details']['service_id'] = trim($_ALL_POST['main_client_details']['service_id'],",");
                        $my_services_list = explode(",",$_ALL_POST['main_client_details']['service_id']);
                    }
                    //$address_types  = $region->getTypeList();
                    //$lst_country    = $region->getCountryList();
                    //$lst_state      = $region->getStateList();
                    //$lst_city       = $region->getCityList();
                    
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    
              
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    $hidden[] = array('name'=> 'client_id', 'value' => $client_id);
                    //$hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    //$page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
                    //$page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
                    //$page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    //$page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
                    //$page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
					$page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
                    $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
                    $page["var"][] = array('variable' => 'my_services_list', 'value' => 'my_services_list');
                    //$page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
                    $page["var"][] = array('variable' => 'allowipArr', 'value' => 'allowipArr');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-su-edit.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Vendor with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected Vendor was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>