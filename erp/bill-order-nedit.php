<?php
if ( $perm->has('nc_bl_or_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/company.inc.php');
        include_once ( DIR_FS_INCLUDES .'/quotation.inc.php');
        include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
        include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
        include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
		include_once (DIR_FS_INCLUDES .'/services.inc.php');
		include_once ( DIR_FS_INCLUDES.'/service-tax-price.inc.php');
        include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
		
		$or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $po_id          = isset($_GET["po_id"]) ? $_GET["po_id"] : ( isset($_POST["po_id"]) ? $_POST["po_id"] : '' );
        //$is_inv_cr    = isset($_GET["is_inv_cr"]) ? $_GET["is_inv_cr"] : ( isset($_POST["is_inv_cr"]) ? $_POST["is_inv_cr"] : '' );
		
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }
		  
        include_once (DIR_FS_INCLUDES .'/user.inc.php');
        // list of executives to assign project manager
        /*  
		$lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields); 
		*/
		$lst_iah = null;
        Paymenttransaction::getIAccountHead($db,$lst_iah);
        
        
		$stype_list = Order::getSTypes();
		$no_list = Order::getNos();
		
		$op_list = Order::getRoundOffOp();
		$roundOff_list = Order::getRoundOff();
		$discountType = Order::getDiscountType();
		
        // Read the taxlist 
		$subtax_list    = NULL;		
		$tax_list    = NULL;
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=0 ORDER BY id ASC";
		ServiceTax::getList($db, $tax_list, 'id,tax_name', $condition_queryst);
		
		$tax_vlist    = NULL;
		$condition_queryvt= " WHERE status = '". ACTIVE ."' ORDER BY tax_price ASC";
		ServiceTaxPrice::getList($db, $tax_vlist, 'id,tax_price,tax_percent', $condition_queryvt);
		
		
        //Get company list 
        $lst_company = null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
        
        // Read the available source
       	Leads::getSource($db,$source);
        //code to generate $vendorOptionList BOF
       /*  $vendorSql = "SELECT user_id,billing_name FROM ".TABLE_CLIENTS." WHERE parent_id ='' AND member_type LIKE '%,2,%'" ;
        $db->query( $vendorSql );
        $vendorList['0'] = "" ;
        if( $db->nf() > 0 ){
            while($db->next_record())
			{
                $vendorList[$db->f('user_id')] = $db->f('billing_name') ;
            }
        } */
        //code to generate $vendorOptionList EOF
        
        
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);
        //Read Order dEtails BOF  
         if($messages->getErrorMessageCount() <= 0){
       
            // Read the Order which is to be Updated.
            $fields = TABLE_BILL_ORDERS .'.*'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.billing_name AS billing_name'
                        .','. TABLE_CLIENTS .'.status AS c_status';
            
            $condition_query = " WHERE (". TABLE_BILL_ORDERS .".id = '". $or_id ."' "
                                    ." OR ". TABLE_BILL_ORDERS .".number = '". $or_id ."')";

            $condition_query .= " AND ( ";
    
            // If my has created this Order.
            $condition_query .= " (". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // If my is present in the Team (Remove if Team Member cannot edit the Order).
            $condition_query .= " OR ( ( "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
                                        . " ) "
                                        ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // If my is the Client Manager (Remove if Client manager cannot edit the Order)
            $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // Check if the User has the Right to Edit Orders created by other Users.
            if ( $perm->has('nc_bl_or_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_bl_or_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_BILL_ORDERS. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level_o ) ";
            }
            $condition_query .= " ) ";
            
            /*
            $condition_query .= " AND ( ". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";

            // Check if the User has the Right to edit Orders created by other Users.
            if ( $perm->has('nc_bl_or_edit_ot') ) {
                $access_level   = $my['access_level'];
                if ( $perm->has('nc_bl_or_edit_ot_al') ) {
                    $access_level += 1;
                }
                $condition_query .= " OR ( ". TABLE_BILL_ORDERS. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            }
            */
            
          
            
            if ( Order::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                
                $_ALL_POST = $_ALL_POST['0'];
				$old_ord_referred_status = $_ALL_POST['ord_referred_status'];
				$old_ord_discount_against_id = $_ALL_POST['ord_discount_against_id'];
				
                $is_inv_cr = $is_invoice_created=0;
                $sql= " SELECT COUNT(*) as count FROM ".TABLE_BILL_INV." WHERE 
				".TABLE_BILL_INV.".or_no='".$_ALL_POST['number']."' AND ".TABLE_BILL_INV.".status !='".Invoice::DELETED."' LIMIT 0,1";
			    $db->query($sql);               
			    if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$count = $db->f('count') ;
						if( $count > 0){
							$is_invoice_created = 1;
						}
					}
				}
                
				if($is_invoice_created==1 || $_ALL_POST['is_invoiceprofm_create'] ==1){
					$is_inv_cr = 1;
				}
			   
			   
                //Check whether is_invoice_created created or not
                
                
                //if ( $_ALL_POST['access_level'] < $access_level ) {
                    // Set up the Client Details field.
                    $_ALL_POST['client_details'] = $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
                                                    .' ('. $_ALL_POST['billing_name'] .')';
                                                    //.' ('. $_ALL_POST['c_email'] .')';
                    
                    
                    //include ( DIR_FS_INCLUDES .'/user.inc.php');
                    // Read the Order Creator's Information. No need to show Creator on Edit Form.
                   //$_ALL_POST['creator']= '';
                   //User::getList($db, $_ALL_POST['creator'], 'number,email,f_name,l_name', " WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                   //$_ALL_POST['creator'] = $_ALL_POST['creator'][0];
                
                    // Read the Team Members Information.
					$_ALL_POST['team'] = trim($_ALL_POST['team'],",") ;
                    $_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
                    $temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
                    $_ALL_POST['team_members']  = '';
                    $_ALL_POST['team_details']  = array();
                    User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
                    $_ALL_POST['team'] = array();
                    foreach ( $_ALL_POST['team_members'] as $key=>$members) {
                  
                        $_ALL_POST['team'][] = $members['user_id'];
                        $_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
                    }
					 // Read the Clients Sub Members Information.
					$_ALL_POST['clients_su']= trim($_ALL_POST['clients_su']);
                    $_ALL_POST['clients_su']          = explode(',', $_ALL_POST['clients_su']);
                    $temp_su                       = "'". implode("','", $_ALL_POST['clients_su']) ."'";
                    $_ALL_POST['clients_su_members']  = '';
                    $_ALL_POST['clients_su_details']  = array();
                    Clients::getList($db, $_ALL_POST['clients_su_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp_su .")");
                    $_ALL_POST['clients_su'] = array();
					if(!empty($_ALL_POST['clients_su_members'])){
						foreach ( $_ALL_POST['clients_su_members'] as $key=>$members) {
					  
							$_ALL_POST['clients_su'][] = $members['user_id'];
							//$_ALL_POST['clients_su_details'][] = $members['f_name'] .' '.$members['l_name'] .' ('. $members['number'] .')';
							$_ALL_POST['clients_su_details'][] = $members['f_name'] .' '. $members['l_name'] .'';
						}
					}
                    //date of order
                    $_ALL_POST['do_o']  = explode(' ', $_ALL_POST['do_o']);
                    $temp               = explode('-', $_ALL_POST['do_o'][0]);
                    $_ALL_POST['do_o']  = NULL;
                    $_ALL_POST['do_o']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    // Setup the date of delivery.
                    $_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
                    $temp               = explode('-', $_ALL_POST['do_d'][0]);
                    $_ALL_POST['do_d']  = NULL;
                    $_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    $po_id              = $_ALL_POST['po_id'];
                    
                    // Setup the start date and est date
                    if( $_ALL_POST['st_date'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['st_date']  ='';
                    }else{
                        $_ALL_POST['st_date']  = explode(' ', $_ALL_POST['st_date']);
                        $temp               = explode('-', $_ALL_POST['st_date'][0]);
                        $_ALL_POST['st_date']  = NULL;
                        $_ALL_POST['st_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    // Setup the start date and est date
                    
                    if( $_ALL_POST['es_ed_date'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['es_ed_date']  ='';
                    }else{
                        $_ALL_POST['es_ed_date']  = explode(' ', $_ALL_POST['es_ed_date']);
                        $temp               = explode('-', $_ALL_POST['es_ed_date'][0]);
                        $_ALL_POST['es_ed_date']  = NULL;
                        $_ALL_POST['es_ed_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    if( $_ALL_POST['do_e'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['do_e']  ='';
                    }else{
                        $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                        $temp               = explode('-', $_ALL_POST['do_e'][0]);
                        $_ALL_POST['do_e']  = NULL;
                        $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    if( $_ALL_POST['do_fe'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['do_fe']  ='';
                    }else{
                        $_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
                        $temp               = explode('-', $_ALL_POST['do_fe'][0]);
                        $_ALL_POST['do_fe']  = NULL;
                        $_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
					
					$_ALL_POST['ord_discount_against_id_old'] = $_ALL_POST['ord_discount_against_id'];
					//$_ALL_POST['show_period']=1;
					
                    //Get Quotation files BOF            
                    if( !empty($_ALL_POST['quotation_no'])){
                        $rejectedFiles = array();                       
                        Quotation::allRejectedQuotation($db,$_ALL_POST['parent_q_id'],$rejectedFiles);
                    }                     
                    //Get Quotation files EOF
                    
                    //if order_closed_by is empty 
                    /*
                    if(empty($_ALL_POST['order_type'])){
                        $_ALL_POST['order_type'] = Order::TARGETED ;
                    }
                    if(empty($_ALL_POST['order_closed_by'])){
                        $_ALL_POST['order_closed_by'] = 'e68adc58f2062f58802e4cdcfec0af2d' ;
                    }*/
                    
                     // Set up the Services.
                    //$_ALL_POST['service_id'] = explode(',', $_ALL_POST['service_id']);
                   
                    // Read the Particulars.
                    $temp_p = NULL;
                    $condition_query_p ='';
                    $service_idArr= array();
                    $condition_query_p = " WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $_ALL_POST['number'] ."'";
                    Order::getParticulars($db, $temp_p, '*', $condition_query_p);
                    if(!empty($temp_p)){                       
						foreach ( $temp_p as $pKey => $parti ) {                            
							$_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];	
							$_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
                            $_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];							
                            $_ALL_POST['ss_title'][$pKey]       = $temp_p[$pKey]['ss_title'];							
							$_ALL_POST['s_type'][$pKey]       = $temp_p[$pKey]['s_type'];
							$_ALL_POST['s_quantity'][$pKey]       = $temp_p[$pKey]['s_quantity'];
							$_ALL_POST['s_amount'][$pKey]       = $temp_p[$pKey]['s_amount'];
							$_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
							$_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
							$_ALL_POST['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];
							$_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];							
							$_ALL_POST['ss_div_id'][$pKey]   = $temp_p[$pKey]['ss_div_id'];							
							$_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
							$_ALL_POST['tax1_id_check'][$pKey]   = $temp_p[$pKey]['tax1_id'];
							$_ALL_POST['tax1_value'][$pKey]   = $temp_p[$pKey]['tax1_value'];
							$tax1_value[]   = $temp_p[$pKey]['tax1_value'];
							$_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
							$_ALL_POST['tax1_number'][$pKey]   = $temp_p[$pKey]['tax1_number'];
							$_ALL_POST['tax1_amount'][$pKey]   = $temp_p[$pKey]['tax1_amount'];					
							$_ALL_POST['tax1_sub1_name'][$pKey]   = $temp_p[$pKey]['tax1_sub1_name'];
							$_ALL_POST['tax1_sub1_id_check'][$pKey]   = $temp_p[$pKey]['tax1_sub1_id'];
							$tax1sub1id[]= $temp_p[$pKey]['tax1_sub1_id'];
							$_ALL_POST['tax1_sub1_value'][$pKey]   = $temp_p[$pKey]['tax1_sub1_value'];
							$tax1_sub1_value[]   = $temp_p[$pKey]['tax1_sub1_value'];
							$_ALL_POST['tax1_sub1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub1_pvalue'];
							$_ALL_POST['tax1_sub1_number'][$pKey]   = $temp_p[$pKey]['tax1_sub1_number'];
							$_ALL_POST['tax1_sub1_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub1_amount'];					
							$_ALL_POST['tax1_sub2_name'][$pKey]   = $temp_p[$pKey]['tax1_sub2_name'];
							$_ALL_POST['tax1_sub2_id_check'][$pKey]   = $temp_p[$pKey]['tax1_sub2_id'];
						
							$_ALL_POST['tax1_sub2_value'][$pKey]   = $temp_p[$pKey]['tax1_sub2_value'];
							$tax1_sub2_value[]  = $temp_p[$pKey]['tax1_sub2_value'];
							$_ALL_POST['tax1_sub2_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub2_pvalue'];
							$_ALL_POST['tax1_sub2_number'][$pKey]   = $temp_p[$pKey]['tax1_sub2_number'];
							$_ALL_POST['tax1_sub2_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub2_amount'];					 
							$_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
							$_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
							$_ALL_POST['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
                            $_ALL_POST['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
                            $_ALL_POST['is_renewable'][$pKey]   = $temp_p[$pKey]['is_renewable'];
                            $_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
							$_ALL_POST['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
                            $_ALL_POST['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];							
							$_ALL_POST['tax1_sid_opt'][$pKey]=array();
							$_ALL_POST['tax1_sid_opt_count'][$pKey]=0;
							
							if($_ALL_POST['tax1_id_check'][$pKey]>0){
								//tax1_sub_id Options of taxes 
								//$_ALL_POST['tax1_sid_opt'][$pKey] = array('0'=>2);
								$tax_opt = array();
								$tax_id = $_ALL_POST['tax1_id_check'][$pKey];
								$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
								ORDER BY tax_name ASC ";
								ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
								if(!empty($tax_opt)){
									$_ALL_POST['tax1_sid_opt'][$pKey] = $tax_opt ;  
									$_ALL_POST['tax1_sid_opt_count'][$pKey] = count($tax_opt) ;  
								}
							}
							$_ALL_POST['sub_sid_list'][$pKey]= $_ALL_POST['subsid'][$pKey]=$ss_sub_id_details=array();

							if(!empty($_ALL_POST['s_id'][$pKey])){
								$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = 
								".$_ALL_POST['s_id'][$pKey]." 
								AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
								$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id 
								= ".$_ALL_POST['currency_id']." 
								AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;
								
								$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
								TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
								$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." 
								".$condition_query_sid ; 
								 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

								if ( $db->query($query) ) {
									if ( $db->nf() > 0 ) {
										while ($db->next_record()) {
											$ss_subtitle =  trim($db->f("ss_title")).",";
											$ss_subprice =  $db->f("ss_price") ;
											$ss_subid =  $db->f("ss_id") ;
											$ss_sub_id_details[] = array(
												'ss_title'=>$ss_subtitle,
												'ss_price'=>$ss_subprice,
												'ss_id'=>$ss_subid	
											);
										}
									}
								} 
						   }
						   $_ALL_POST['subsidstr'][$pKey] =  $_ALL_POST['sub_s_id'][$pKey] ;
						   $_ALL_POST['sub_sid_list'][$pKey]=$ss_sub_id_details;
						   $sub_s_id_str='';
						   $sub_s_id_str =trim($_ALL_POST['subsidstr'][$pKey],",");
						   if(!empty($sub_s_id_str)){
								$_ALL_POST['subsid'][$pKey] = explode(",",$sub_s_id_str);
								$sub_s_id_str=",".$sub_s_id_str.",";
						   }						   
						}
						
						$_ALL_POST['tax1_value'] = $tax1_value;
						$_ALL_POST['tax1_sub1_value'] = $tax1_sub1_value;
						$_ALL_POST['tax1_sub2_value'] = $tax1_sub2_value;
                    }
                    $_ALL_POST['service_id'] = $service_idArr;
                    $order_of_str =trim($_ALL_POST['order_of'],","); 
					$_ALL_POST['order_of'] = explode(",",$order_of_str);					
                    $_ALL_POST['po_filename']=$_ALL_POST['po_filename'];
                    $_ALL_POST['filename_1']=$_ALL_POST['filename_1'];
                    $_ALL_POST['filename_2']=$_ALL_POST['filename_2'];
                    $_ALL_POST['filename_3']=$_ALL_POST['filename_3']; 
					$total_tax_paid_amount = $_ALL_POST['total_tax_paid_amount'];
                    $_ALL_POST['wp_support_team'] = $wp_support_team = $_ALL_POST['wp_support_team']; 
                    $data=$_ALL_POST;
                //}
                //else {
                    //$messages->setErrorMessage("You do not have the Permission to view the Order with the current Access Level.");
               // }
            }else{
                $messages->setErrorMessage("The Order was not found or you do not have the Permission to access 
				this Order.");
            }
            //get order details eof
        
        }        
        
		// Read the Services.on the basis of order renewable or not
        include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY sequence_services ASC"; 
        Services::getList($db, $lst_service, 'ss_division,ss_id,ss_title,ss_punch_line,tax1_id, tax1_name, tax1_value,
		is_renewable ', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
                $ss_id= $key['ss_id'];
                $ss_div_id= $key['ss_division'];
                $ss_title= $key['ss_title'];
                $ss_punch_line= $key['ss_punch_line'];
                $tax1_id = $key['tax1_id'];
                $tax1_name = $key['tax1_name'];
                $tax1_value = $key['tax1_value'];
                $is_renewable = $key['is_renewable'];
                $tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
                $keyNew['ss_div_id'] = $ss_div_id ; 
                $keyNew['ss_id'] = $ss_id ; 
                $keyNew['ss_title'] = $ss_title ; 
                $keyNew['ss_punch_line'] = $ss_punch_line ; 
                $keyNew['tax1_id'] = $tax1_id ; 
                $keyNew['tax1_name'] = $tax1_name ; 
                $keyNew['tax1_value'] = $tax1_value ; 
                $keyNew['tax1_pvalue'] = $tax1_pvalue ; 
                $keyNew['is_renewable'] = $is_renewable ; 
                $lst_service[$val] = $keyNew;
            }
        }
        //Read Order dEtails EOF          
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn']) || isset($_POST['btnReturnInv'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);            
            $files       = processUserData($_FILES);
            if($is_inv_cr == 1){
			
				// Read the Particulars.
                    $temp_p = NULL;
                    $condition_query_p ='';
                    $service_idArr= array();
                    $condition_query_p = " WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $_ALL_POST['number'] ."'";
                    Order::getParticulars($db, $temp_p, '*', $condition_query_p);
                    if(!empty($temp_p)){                       
						foreach ( $temp_p as $pKey => $parti ) {                            
							$_ALL_POST['p_id'][$pKey] = $data['p_id'][$pKey]  = $temp_p[$pKey]['id'];	
							$_ALL_POST['sub_s_id'][$pKey]    = $data['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
                            $_ALL_POST['s_id'][$pKey]        =  $data['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];							
							$_ALL_POST['ss_title'][$pKey]   =   $data['ss_title'][$pKey]       = $temp_p[$pKey]['ss_title'];							
							$_ALL_POST['s_type'][$pKey]      = $data['s_type'][$pKey]       = $temp_p[$pKey]['s_type'];
							$_ALL_POST['s_quantity'][$pKey]  = $data['s_quantity'][$pKey]       = $temp_p[$pKey]['s_quantity'];
							$_ALL_POST['s_amount'][$pKey]  =$data['s_amount'][$pKey]  =  $temp_p[$pKey]['s_amount'];
							$_ALL_POST['particulars'][$pKey]= $data['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
							$_ALL_POST['p_amount'][$pKey]   =$data['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
							$_ALL_POST['d_amount'][$pKey]   =$data['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];
							$_ALL_POST['ss_punch_line'][$pKey]   = $data['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];							
							$_ALL_POST['ss_div_id'][$pKey]   =$data['ss_div_id'][$pKey]   = $temp_p[$pKey]['ss_div_id'];							
							$_ALL_POST['tax1_name'][$pKey]   =$data['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
							$_ALL_POST['tax1_id_check'][$pKey]   = $data['tax1_id_check'][$pKey]   = $temp_p[$pKey]['tax1_id'];
							$_ALL_POST['tax1_value'][$pKey]   =$data['tax1_value'][$pKey]   = $temp_p[$pKey]['tax1_value'];
							$tax1_value[]   = $temp_p[$pKey]['tax1_value'];
							$_ALL_POST['tax1_pvalue'][$pKey]   =$data['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
							$_ALL_POST['tax1_number'][$pKey]   =$data['tax1_number'][$pKey]   = $temp_p[$pKey]['tax1_number'];
							$_ALL_POST['tax1_amount'][$pKey]   =$data['tax1_amount'][$pKey]   = $temp_p[$pKey]['tax1_amount'];					
							$_ALL_POST['tax1_sub1_name'][$pKey]   =$data['tax1_sub1_name'][$pKey]   = $temp_p[$pKey]['tax1_sub1_name'];
							$_ALL_POST['tax1_sub1_id_check'][$pKey]   =$data['tax1_sub1_id_check'][$pKey]   = $temp_p[$pKey]['tax1_sub1_id'];
							$tax1sub1id[]= $temp_p[$pKey]['tax1_sub1_id'];
							$_ALL_POST['tax1_sub1_value'][$pKey]   = $data['tax1_sub1_value'][$pKey]   = $temp_p[$pKey]['tax1_sub1_value'];
							$tax1_sub1_value[]   = $temp_p[$pKey]['tax1_sub1_value'];
							$_ALL_POST['tax1_sub1_pvalue'][$pKey]   =$data['tax1_sub1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub1_pvalue'];
							$_ALL_POST['tax1_sub1_number'][$pKey]   =$data['tax1_sub1_number'][$pKey]   = $temp_p[$pKey]['tax1_sub1_number'];
							$_ALL_POST['tax1_sub1_amount'][$pKey]   = $data['tax1_sub1_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub1_amount'];					
							$_ALL_POST['tax1_sub2_name'][$pKey]   =$data['tax1_sub2_name'][$pKey]   = $temp_p[$pKey]['tax1_sub2_name'];
							$_ALL_POST['tax1_sub2_id_check'][$pKey]   =$data['tax1_sub2_id_check'][$pKey]   = $temp_p[$pKey]['tax1_sub2_id'];
						
							$_ALL_POST['tax1_sub2_value'][$pKey]   =$data['tax1_sub2_value'][$pKey]   = $temp_p[$pKey]['tax1_sub2_value'];
							$tax1_sub2_value[]  = $temp_p[$pKey]['tax1_sub2_value'];
							$_ALL_POST['tax1_sub2_pvalue'][$pKey]   =$data['tax1_sub2_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub2_pvalue'];
							$_ALL_POST['tax1_sub2_number'][$pKey]   =$data['tax1_sub2_number'][$pKey]   = $temp_p[$pKey]['tax1_sub2_number'];
							$_ALL_POST['tax1_sub2_amount'][$pKey]   = $data['tax1_sub2_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub2_amount'];					 
							$_ALL_POST['stot_amount'][$pKey]   = $data['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
							$_ALL_POST['tot_amount'][$pKey]   = $data['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
							$_ALL_POST['discount_type'][$pKey]   = $data['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
                            $_ALL_POST['pp_amount'][$pKey]   = $data['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
                            $_ALL_POST['is_renewable'][$pKey]   = $data['is_renewable'][$pKey]   = $temp_p[$pKey]['is_renewable'];
                            $_ALL_POST['stot_amount'][$pKey]   = $data['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
							$_ALL_POST['vendor'][$pKey]   = $data['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
                            $_ALL_POST['purchase_particulars'][$pKey] = $data['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];							
							$_ALL_POST['tax1_sid_opt'][$pKey]=$data['tax1_sid_opt'][$pKey]=array();
							$_ALL_POST['tax1_sid_opt_count'][$pKey]=$data['tax1_sid_opt_count'][$pKey]=0;
							
							if($data['tax1_id_check'][$pKey]>0){
								//tax1_sub_id Options of taxes 
								//$_ALL_POST['tax1_sid_opt'][$pKey] = array('0'=>2);
								$tax_opt = array();
								$tax_id = $data['tax1_id_check'][$pKey];
								$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
								ORDER BY tax_name ASC ";
								ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
								if(!empty($tax_opt)){
									$_ALL_POST['tax1_sid_opt'][$pKey] = $data['tax1_sid_opt'][$pKey] = $data['tax1_sid_opt'][$pKey] = $tax_opt ;  
									$_ALL_POST['tax1_sid_opt_count'][$pKey] = $data['tax1_sid_opt_count'][$pKey] = $data['tax1_sid_opt_count'][$pKey] = count($tax_opt) ;  
								}
							}
							$_ALL_POST['sub_sid_list'][$pKey]= $data['sub_sid_list'][$pKey]= $data['sub_sid_list'][$pKey]= $data['subsid'][$pKey]=$ss_sub_id_details=array();

							if(!empty($data['s_id'][$pKey])){
								$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = 
								".$data['s_id'][$pKey]." 
								AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
								$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id 
								= ".$data['currency_id']." 
								AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;
								
								$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
								TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
								$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." 
								".$condition_query_sid ; 
								 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

								if ( $db->query($query) ) {
									if ( $db->nf() > 0 ) {
										while ($db->next_record()) {
											$ss_subtitle =  trim($db->f("ss_title")).",";
											$ss_subprice =  $db->f("ss_price") ;
											$ss_subid =  $db->f("ss_id") ;
											$ss_sub_id_details[] = array(
												'ss_title'=>$ss_subtitle,
												'ss_price'=>$ss_subprice,
												'ss_id'=>$ss_subid	
											);
										}
									}
								} 
						   }
						   $_ALL_POST['subsidstr'][$pKey] = $_ALL_POST['sub_s_id'][$pKey] = $data['subsidstr'][$pKey] =  $data['sub_s_id'][$pKey] ;
						   $_ALL_POST['sub_sid_list'][$pKey]= $data['sub_sid_list'][$pKey]=$ss_sub_id_details;
						   $sub_s_id_str='';
						   $sub_s_id_str =trim($data['subsidstr'][$pKey],",");
						   if(!empty($sub_s_id_str)){
								$_ALL_POST['subsid'][$pKey] = $data['subsid'][$pKey] = explode(",",$sub_s_id_str);
								$sub_s_id_str=",".$sub_s_id_str.",";
						   }						   
						}
						
						$_ALL_POST['tax1_value'] =$data['tax1_value'] = $tax1_value;
						$_ALL_POST['tax1_sub1_value'] = $data['tax1_sub1_value'] = $tax1_sub1_value;
						$_ALL_POST['tax1_sub2_value']= $data['tax1_sub2_value']=$data['tax1_sub2_value'] = $tax1_sub2_value;
                    }
			}
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_MULTIPLE_FILE_SIZE     ;
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'lst_service'       => $lst_service,
                            'messages'          => &$messages,
							'data'          => &$data
                        );
            $data['do_c'] = time();
            if ( Order::validateUpdateN($data, $extra) ) {			
                $company1 =null;
                $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                $fields_c1 = " prefix,name ";
                Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
                if(!empty($company1)){                  
                   $data['company_name'] = $company1[0]['name'];
                   $data['company_prefix'] = $company1[0]['prefix'];
                }                
                $po_file_sql="";
                $file1_sql="";
                $file2_sql="";
                $file3_sql="";
                $extra_d_sql="";
                                
                //As order form differentiated as per the permissions 
                if($data['access_extra_details']){
					$currency1 =null;
					$condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
					$fields1 = "*";
					Currency::getList( $db, $currency1, $fields1, $condition_query1);              
					if(!empty($currency1)){
					   $data['currency_abbr'] = $currency1[0]['abbr'];
					   $data['currency_name'] = $currency1[0]['currency_name'];
					   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
					   $data['currency_country'] = $currency1[0]['country_name'];
					}
                
                
					if(!empty($data['po_filename'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['po_filename']["name"]);
					   
						$ext = $filedata["extension"] ; 
						//$attachfilename = $username."-".mktime().".".$ext ;
					   
						//$attachfilename = "PO-".$data['number'].".".$ext ;
						$attachfilename = "PO-".mktime().".".$ext ;
						$data['po_filename'] = $attachfilename;
						$_ALL_POST['po_filename'] = $attachfilename;
						if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_po_filename']))
						{
							@unlink( DIR_FS_ORDER_FILES."/".$data['old_po_filename']);
						}
						if(move_uploaded_file ($files['po_filename']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
							@chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
						}
						$po_file_sql = ",". TABLE_BILL_ORDERS .".po_filename = '". $attachfilename ."'";
						$_ALL_POST['po_filename']=$attachfilename;
						
					}else{
					
						$_ALL_POST['po_filename']=$_ALL_POST['old_po_filename'];
						if(isset($data['delete_po_fl'])){
							if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_po_filename'])){
								@unlink( DIR_FS_ORDER_FILES."/".$data['old_po_filename']);
							}
							$po_file_sql = ",". TABLE_BILL_ORDERS .".po_filename = ''";
							$_ALL_POST['po_filename']='';
						}
					}
                        
					if(!empty($data['filename_1'])){
						$filedata["extension"]=$attachfilename='';
						$filedata = pathinfo($files['filename_1']["name"]);
					   
						$ext = $filedata["extension"] ; 
						//$attachfilename = $username."-".mktime().".".$ext ;
					   
					   // $attachfilename = "WO1-".$data['number'].".".$ext ;
						$attachfilename = "WO1-".mktime().".".$ext ;
						$data['filename_1'] = $attachfilename;
						if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_1'])){
							@unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_1']);
						}
						if (move_uploaded_file ($files['filename_1']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
							  @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
						}
						$file1_sql=",". TABLE_BILL_ORDERS .".filename_1      = '". $attachfilename ."'" ;
						$file1_sql.=",". TABLE_BILL_ORDERS .".filecaption_1      = '". $data['filecaption_1'] ."'";
						$_ALL_POST['filename_1']=$attachfilename;
					}elseif (!empty($data['old_filename_1'])){
						$file1_sql=",". TABLE_BILL_ORDERS .".filecaption_1      = '". $data['filecaption_1'] ."'";
						$_ALL_POST['filename_1']=$_ALL_POST['old_filename_1'];
						
						if(isset($data['delete_fl_1'])){               
						
							if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_1'])){
								@unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_1']);
							}
							$file1_sql=",". TABLE_BILL_ORDERS .".filename_1      = ''" ;
							$file1_sql.=",". TABLE_BILL_ORDERS .".filecaption_1  = '". $data['filecaption_1'] ."'";
							$_ALL_POST['filename_1']='';
						}
					}
                        
					if(!empty($data['filename_2'])){
						$filedata["extension"]=$attachfilename='';
						$filedata = pathinfo($files['filename_2']["name"]);
					   
						$ext = $filedata["extension"] ; 
						//$attachfilename = $username."-".mktime().".".$ext ;                           
						//$attachfilename = "WO2-".$data['number'].".".$ext ;
						
						$attachfilename = "WO2-".mktime().".".$ext ;
						$data['filename_2'] = $attachfilename;
						if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_2'])){
							 @unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_2']);
						}                            
						if (move_uploaded_file ($files['filename_2']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
							  @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
						}
						$file2_sql=",". TABLE_BILL_ORDERS .".filename_2      = '". $attachfilename ."'";
						$file2_sql.=",". TABLE_BILL_ORDERS .".filecaption_2      = '". $data['filecaption_2'] ."'";
						$_ALL_POST['filename_2']=$attachfilename;
					}elseif (!empty($data['old_filename_2'])){
						$file2_sql=",". TABLE_BILL_ORDERS .".filecaption_2      = '". $data['filecaption_2'] ."'";
						$_ALL_POST['filename_2']=$_ALL_POST['old_filename_2'];
						if(isset($data['delete_fl_2'])){               
						
							if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_2'])){
								@unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_2']);
							}
							$file2_sql=",". TABLE_BILL_ORDERS .".filename_2      = ''" ;
							$file2_sql.=",". TABLE_BILL_ORDERS .".filecaption_2  = '". $data['filecaption_2'] ."'";
							$_ALL_POST['filename_2']='';
						}
					}
                        
					if(!empty($data['filename_3'])){
						$filedata["extension"] = $attachfilename='';
						$filedata = pathinfo($files['filename_3']["name"]);                   
						$ext = $filedata["extension"] ;                    
						//$attachfilename = "WO3-".$data['number'].".".$ext ;
						$attachfilename = "WO3-".mktime().".".$ext ;
						$data['filename_3'] = $attachfilename;
						if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_3']))
						{
							@unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_3']);
						}
						if (move_uploaded_file ($files['filename_3']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
							   @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
						}
						$file3_sql = ",". TABLE_BILL_ORDERS .".filename_3   = '". $data['filename_3'] ."'";
						$file3_sql.= ",". TABLE_BILL_ORDERS .".filecaption_3 = '". $data['filecaption_3'] ."'";
						$_ALL_POST['filename_3']=$attachfilename;
					}elseif(!empty($data['old_filename_3'])){
						$file3_sql=",". TABLE_BILL_ORDERS .".filecaption_3      = '". $data['filecaption_3'] ."'";
						$_ALL_POST['filename_3']=$_ALL_POST['old_filename_3'];
						if(isset($data['delete_fl_3'])){               
						
							if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_3'])){
								@unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_3']);
							}
							$file3_sql=",". TABLE_BILL_ORDERS .".filename_3      = ''" ;
							$file3_sql.=",". TABLE_BILL_ORDERS .".filecaption_3  = '". $data['filecaption_3'] ."'";
							$_ALL_POST['filename_3']='';
						}
					}
					if(!empty($data['do_e']) && !empty($data['do_fe'])){                    
						$do_e  = date('Y-m-d H:i:s', $data['do_e']) ;
						$do_fe = date('Y-m-d H:i:s', $data['do_fe']) ;
					}
					$sure_no_renewal=0;
					if(isset($data['sure_no_renewal'])){
						$sure_no_renewal = $data['sure_no_renewal'];
					} 
					$show_period=1;
					if(isset($data['show_period'])){
						$show_period = $data['show_period'];
					} 
					$extra_d_sql = ",". TABLE_BILL_ORDERS .".order_domain	    = '". $data['order_domain'] ."'"
					.",". TABLE_BILL_ORDERS .".currency_id      = '". $data['currency_id'] ."'"
					.",". TABLE_BILL_ORDERS .".currency_abbr    = '". $data['currency_abbr'] ."'"
					.",".TABLE_BILL_ORDERS .".currency_name    = '".  $data['currency_name'] ."'"
					.",". TABLE_BILL_ORDERS .".currency_symbol  = '". $data['currency_symbol'] ."'"
					.",". TABLE_BILL_ORDERS .".currency_country = '". $data['currency_country'] ."'"                    
					.",". TABLE_BILL_ORDERS .".amount           = '". $data['amount'] ."'"
					.",". TABLE_BILL_ORDERS .".amount_inr       = '". $data['amount'] ."'"
					.",". TABLE_BILL_ORDERS .".stotal_amount    = '". $data['stotal_amount'] ."'"
					.",". TABLE_BILL_ORDERS .".existing_client	= '". $data['existing_client'] ."'"
					.",". TABLE_BILL_ORDERS .".client_s_client	= '". $data['client_s_client'] ."'"
					.",". TABLE_BILL_ORDERS .".order_type	    = '". $data['order_type'] ."'"
					.",". TABLE_BILL_ORDERS .".order_closed_by	= '". $data['order_closed_by'] ."'"
					.",". TABLE_BILL_ORDERS .".old_particulars  = '". $data['old_particulars'] ."'"
					.",". TABLE_BILL_ORDERS .".details          = '". $data['details'] ."'"
					.",". TABLE_BILL_ORDERS .".discount_explanation = '". $data['discount_explanation'] ."'"
					.",". TABLE_BILL_ORDERS .".discount_explanation2 = '". $data['discount_explanation2'] ."'"
					.",". TABLE_BILL_ORDERS .".do_e             = '". $do_e."'"
					.",". TABLE_BILL_ORDERS .".do_fe            = '". $do_fe."'"
					.",". TABLE_BILL_ORDERS .".is_renewable      = '". $data['isrenewable'] ."'"
					.",". TABLE_BILL_ORDERS .".sure_no_renewal  = '". $sure_no_renewal ."'"
					.",". TABLE_BILL_ORDERS .".show_period  = '". $show_period ."'"
					.",". TABLE_BILL_ORDERS .".is_renewal_ord   = '". $data['is_renewal_ord'] ."'" ;
                }
                $clients_su_str='';
                if(!empty($data['clients_su'])){
					$clients_su_str = implode(",", $data['clients_su']);
					$clients_su_str = ",".$clients_su_str."," ;
				}
                if($is_inv_cr == 0){
					$tax_ids_str='';				
					if(!empty($data['alltax_ids'])){
						$tax_ids_str = implode(",", $data['alltax_ids']);
						$tax_ids_str = ",".$tax_ids_str."," ;
					}
					$s_id_str ='';
					if(!empty($data['s_id'])){
						$s_id_str = implode(",", $data['s_id']);
						$s_id_str = ",".$s_id_str."," ;
					}
					$sql_ids = ",". TABLE_BILL_ORDERS .".service_id       = '". $s_id_str ."'"
							  .",". TABLE_BILL_ORDERS .".tax_ids	    	= '". $tax_ids_str ."'" ;
				}
				$order_of_str='';				
				if(!empty($data['order_of'])){
					$order_of_str = implode(",", $data['order_of']);
					$order_of_str = ",".$order_of_str."," ;
				}
				$wp_support_team = $wp_support_team_name = $wp_sql = '';
				if(empty($data['wp_support_team'])){
					$sql="SELECT DISTINCT(user_id),f_name, l_name  FROM ".TABLE_USER." WHERE ".TABLE_USER.".groups 
						LIKE '%,". User::TP_OSTP.",%' ORDER BY RAND() LIMIT 0,3 " ;
					$db->query($sql);
					
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ){ 
							$wp_support_team .= $db->f('user_id').",";
							$wp_support_team_name .= $db->f('f_name')." ".$db->f('l_name').",";
						}
						$wp_support_team = trim($wp_support_team,',');
						$wp_support_team = ",".$wp_support_team."," ;
					}
					$wp_sql = ",". TABLE_BILL_ORDERS .".wp_support_team  = '".$wp_support_team ."'
							  ,".TABLE_BILL_ORDERS.".wp_support_team_name  = '".$wp_support_team_name ."'" ;
					
				}
				$special = 0;
				if(isset($data['special'])){
					$special = $data['special'];
				} 
				$bal_sql ='';
				if($is_inv_cr!=1){
					$bal_sql = ",". TABLE_BILL_ORDERS .".balance  = '". $data['amount'] ."'"
							   .",". TABLE_BILL_ORDERS .".balance_inr  = '". $data['amount'] ."'" ;
				}
				//If referred Order is used BOF
				$referred_sql = $sql3='';
				if($old_ord_referred_status!=Order::RUSED && (!empty($data['ord_referred_by']) || !empty($data['ord_referred_by_team']))){
					$referred_sql = ",".TABLE_BILL_ORDERS .".ord_referred_by_name = '". $data['ord_referred_by_name']."'"
								.",". TABLE_BILL_ORDERS .".ord_referred_by 	= '". $data['ord_referred_by']."'"
								.",". TABLE_BILL_ORDERS .".ord_referred_by_team_name = '". $data['ord_referred_by_team_name']."'"
								.",". TABLE_BILL_ORDERS .".ord_referred_by_team = '". $data['ord_referred_by_team']."'"
								.",". TABLE_BILL_ORDERS .".ord_referred_amount 	= '". $data['ord_referred_amount']."'"
								.",". TABLE_BILL_ORDERS .".ord_referred_status 	= '". $data['ord_referred_status']."'" ;
				}
				//If referred Order is used EOF
				//For ORD DISC - We had selected new ORD 
				$referral_ord_sql = $sql4 = '';
				if($data['ord_discount_against_id'] != $data['ord_discount_against_id_old']){
					$referral_ord_sql = ",". TABLE_BILL_ORDERS .".ord_discount_to_name = '". $data['ord_discount_to_name']."'"
					.",". TABLE_BILL_ORDERS .".ord_discount_to 		= '". $data['ord_discount_to']."'"
					.",". TABLE_BILL_ORDERS .".ord_discount_amt 	= '". $data['ord_discount_amt']."'"
					.",". TABLE_BILL_ORDERS .".ord_discount_against_no 		= '". $data['ord_discount_against_no']."'"
					.",". TABLE_BILL_ORDERS .".ord_discount_against_id 		= '". $data['ord_discount_against_id']."'" ;
					
					if(!empty($data['ord_discount_against_id_old'])){
						$sql4 = "UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".ord_referred_used_against_no ='',
							".TABLE_BILL_ORDERS.".ord_referred_status ='1',".TABLE_BILL_ORDERS.".ord_referred_used_against_id =''
							WHERE ".TABLE_BILL_ORDERS.".id =".$data['ord_discount_against_id_old'] ;
					}			
				}
				
                $query  = " UPDATE ". TABLE_BILL_ORDERS
                            ." SET ". TABLE_BILL_ORDERS .".number       = '". $data['number'] ."'"
							.",". TABLE_BILL_ORDERS .".access_level     = '". $data['access_level'] ."'"
							.",". TABLE_BILL_ORDERS .".iaccount_head_id	= '". $data['iaccount_head_id'] ."'"
							.",". TABLE_BILL_ORDERS .".client           = '". $data['client']['user_id'] ."'"
							.",". TABLE_BILL_ORDERS .".order_title	    = '". $data['order_title'] ."'"
							.",". TABLE_BILL_ORDERS .".cc_code	    	= '". $data['cc_code'] ."'"
							.",". TABLE_BILL_ORDERS .".order_title_mail	= '". $data['order_title'] ."'"
							.",". TABLE_BILL_ORDERS .".delivery_at	    = '". $data['delivery_at'] ."'"
							.",". TABLE_BILL_ORDERS .".special           = '". $special."'"
							.",". TABLE_BILL_ORDERS .".order_of       	= '". $order_of_str ."'"
							.$sql_ids
							.$po_file_sql
							.$file1_sql
							.$file2_sql
							.$file3_sql
							.$extra_d_sql  
							.$wp_sql  
							.$bal_sql
							.$referral_ord_sql
							.$referred_sql
							.",". TABLE_BILL_ORDERS .".round_off_op 	= '". $data['round_off_op'] ."'" 
							.",". TABLE_BILL_ORDERS .".round_off 		= '". $data['round_off'] ."'"  	
							.",". TABLE_BILL_ORDERS .".do_o             = '". date('Y-m-d H:i:s', $data['do_o']) ."'"
							.",". TABLE_BILL_ORDERS .".project_manager  = '". $data['project_manager']."'"
							.",". TABLE_BILL_ORDERS .".incentives  		= '". $data['incentives']."'"
							.",". TABLE_BILL_ORDERS .".source_to        = '". $data['source_to'] ."'"
							.",". TABLE_BILL_ORDERS .".source_from      = '". $data['source_from'] ."'"                            
							.",". TABLE_BILL_ORDERS .".team             = ',".implode(",", $data['team']) .",'"                            
							.",". TABLE_BILL_ORDERS .".clients_su       = '". $clients_su_str ."'"                            
							.",". TABLE_BILL_ORDERS .".do_d             = '". date('Y-m-d H:i:s', $data['do_d']) ."'"                            
							.",". TABLE_BILL_ORDERS .".st_date          = '". $data['st_date']."'"
							.",". TABLE_BILL_ORDERS .".es_ed_date       = '". $data['es_ed_date']."'"
							.",". TABLE_BILL_ORDERS .".es_hrs           = '". $data['es_hrs']."'"
						   // .",". TABLE_BILL_ORDERS .".status = '".         $data['status'] ."'" 
						." WHERE id = '". $or_id ."'";
                
                
                if ( $db->query($query)  ) {
                    $messages->setOkMessage("Order has been updated.");
                    $variables['hid'] = $or_id;
                    //Particulars edit bof 
					if($data['ord_discount_against_id'] != $data['ord_discount_against_id_old']){
						if(!empty($sql4)){
							$db->query($sql4) ;
						}
					}
					
                //As order form differentiated as per the permissions 
                if($data['access_extra_details'] && $is_inv_cr!=1){
				
					$count1 = count($data['s_id']);
					$count2 = count($data['p_id']);
					$checkArr =array();
					if($count1 > $count2){
						$checkArr =$data['s_id'] ;					
					}elseif($count1 < $count2){
						$checkArr =$data['p_id'] ;
					}elseif($count1 == $count2){					
						$checkArr =$data['s_id'] ;
					}
					
				
                    if(isset($data['s_id'])){
                        foreach ( $checkArr as $key=>$pid ) {
                            if(!empty($data['p_id'][$key]) && isset($data['p_id'][$key])){
                                if(!isset($data['particulars'][$key]) || empty($data['particulars'][$key]) ){
                                   //delete pid
                                   //Get amount from TABLE_BILL_ORD_P and updatein                                    
                                    $query1 = "DELETE FROM ". TABLE_BILL_ORD_P
                                                ." WHERE id = '".$data['p_id'][$key]."'";
                                    $db->query($query1);
                                
                                }else{
									$data['particulars'][$key] = trim($data['particulars'][$key],',');
                                    //Get tax number BOF
									/* $data['tax1_number'][$key]='';
									if(!empty($data['tax1_id_check'][$key])){
										$vendorSql ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." WHERE 
										id='".$data['tax1_id_check'][$key]."'" ;
										$db->query( $vendorSql );
										if( $db->nf() > 0 ){
											while($db->next_record()){			
												$data['tax1_number'][$key] = $db->f('tax_number')  ;
											}
										}
									}
										
									$data['tax1_sub1_number'][$key]='';
									if(!empty($data['tax1_sub1_id_check'][$key])){
										$vendorSql ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." WHERE 
										id='".$data['tax1_sub1_id_check'][$key]."'" ;
										$db->query( $vendorSql );
										if( $db->nf() > 0 ){
											while($db->next_record()){			
												$data['tax1_sub1_number'][$key] = $db->f('tax_number')  ;
											}
										}
									}
									$data['tax1_sub2_number'][$key]='';
									if(!empty($data['tax1_sub2_id_check'][$key])){
										$vendorSql ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." WHERE 
										id='".$data['tax1_sub2_id_check'][$key]."'" ;
										$db->query( $vendorSql );
										if( $db->nf() > 0 ){
											while($db->next_record()){			
												$data['tax1_sub2_number'][$key] = $db->f('tax_number')  ;
											}
										}
									} */
								   //Get tax number EOF	
                                    $do_e=$do_fe ='';
                                    if($data['is_renewable'][$key] =='1'){
									  $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
									  $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
									  $isrenewable = true;
                                    }
																
                                   //Get vendor name BOF                                     
									$data['vendor_name'][$key]='';
									if(!empty($data['vendor'][$key])){
										$vendorSql = "SELECT billing_name FROM ".TABLE_CLIENTS." 
										WHERE user_id='".$data['vendor'][$key]."'" ;
										$db->query( $vendorSql );
										if( $db->nf() > 0 )
										{
											while($db->next_record()){			
												$data['vendor_name'][$key] = $db->f('billing_name')  ;
											}
										}
									}
                                    //Get vendor name EOF 
									$sub_s_id_str='';
									$sub_s_id_str =trim($data['subsidstr'][$key],",");
									if(!empty($sub_s_id_str)){
										$data['subsid'][$key] = explode(",",$sub_s_id_str);
										$sub_s_id_str=",".$sub_s_id_str.",";
									}									
                                    $query2  = " UPDATE ". TABLE_BILL_ORD_P
                                    ." SET ". TABLE_BILL_ORD_P .".particulars 
									 = '".processUserData(trim($data['particulars'][$key],','))  ."'"
                                    .",". TABLE_BILL_ORD_P .".p_amount = '".        $data['p_amount'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".ss_div_id = '".        $data['ss_div_id'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_id = '".        $data['s_id'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".sub_s_id    = '".$sub_s_id_str."'"
                                    .",". TABLE_BILL_ORD_P .".s_type = '".        $data['s_type'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_quantity = '".        $data['s_quantity'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_amount = '".        $data['s_amount'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".ss_title = '".     
									processUserData($data['ss_title'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".ss_punch_line	= '". 
									processUserData($data['ss_punch_line'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_id	= '".   
									processUserData($data['tax1_id_check'][$key])."'"
                                    //.",". TABLE_BILL_ORD_P .".tax1_number	= '".   processUserData($data['tax1_number'][$key])."'"
									.",". TABLE_BILL_ORD_P .".tax1_name	= '".processUserData($data['tax1_name_str'][$key])."'"
									.",". TABLE_BILL_ORD_P .".tax1_value= '".   processUserData($data['tax1_value'][$key]) ."'"
									.",". TABLE_BILL_ORD_P .".tax1_pvalue='". processUserData($data['tax1_pvalue'][$key])  ."'"
									.",". TABLE_BILL_ORD_P .".tax1_amount= '".processUserData($data['tax1_amount'][$key])."'"						.",". TABLE_BILL_ORD_P .".tax1_sub1_id='".processUserData($data['tax1_sub1_id_check'][$key])."'"
                                    //.",". TABLE_BILL_ORD_P .".tax1_sub1_number	= '".processUserData($data['tax1_sub1_number'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_sub1_name	= '".   processUserData($data['tax1_sub1_name_str'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_sub1_value	= '".   processUserData($data['tax1_sub1_value'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_sub1_pvalue	= '". processUserData($data['tax1_sub1_pvalue'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_sub1_amount	= '". processUserData($data['tax1_sub1_amount'][$key])  ."'"
									.",". TABLE_BILL_ORD_P .".tax1_sub2_id	= '".   processUserData($data['tax1_sub2_id_check'][$key])."'"
                                   // .",". TABLE_BILL_ORD_P .".tax1_sub2_number	= '".   processUserData($data['tax1_sub2_number'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_sub2_name	= '".   processUserData($data['tax1_sub2_name_str'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_sub2_value	= '".   processUserData($data['tax1_sub2_value'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_sub2_pvalue	= '". processUserData($data['tax1_sub2_pvalue'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_sub2_amount	= '". processUserData($data['tax1_sub2_amount'][$key])  ."'"
									.",". TABLE_BILL_ORD_P .".d_amount	= '".    processUserData($data['d_amount'][$key]) ."'"  
									.",". TABLE_BILL_ORD_P .".discount_type = '". processUserData($data['discount_type'][$key])  ."'"
									.",". TABLE_BILL_ORD_P .".stot_amount = '".   processUserData($data['stot_amount'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".tot_amount = '".   processUserData($data['tot_amount'][$key])  ."'"     
									.",". TABLE_BILL_ORD_P .".pp_amount = '". processUserData($data['pp_amount'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".vendor = '". processUserData($data['vendor'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".vendor_name = '". processUserData($data['vendor_name'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".purchase_particulars = '".processUserData($data['purchase_particulars'][$key]) ."'"
									.",". TABLE_BILL_ORD_P .".is_renewable = '".$data['is_renewable'][$key]."'"                            
                                    .",". TABLE_BILL_ORD_P .".do_e = '".$do_e."'"                            
                                    .",". TABLE_BILL_ORD_P .".do_fe = '".$do_fe."'"                            
                                    ." WHERE id = '". $data['p_id'][$key] ."'" ;
                                    
                                    $db->query($query2);
                                }
                              
                            }else{
                                //insert
                                if(!empty($data['particulars'][$key])){
                                    $data['particulars'][$key] = trim($data['particulars'][$key],',');
									$do_e=$do_fe ='';
                                    if($data['is_renewable'][$key] =='1'){
									  $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
									  $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
									  $isrenewable = true;
                                    }                                    
                                   //Get vendor name BOF                                     
									$data['vendor_name'][$key]='';
									if(!empty($data['vendor'][$key])){										
										$vendorSql = "SELECT billing_name FROM ".TABLE_CLIENTS." 
										WHERE user_id='".$data['vendor'][$key]."'" ;
										$db->query( $vendorSql );
										if( $db->nf() > 0 ){
											while($db->next_record()){			
												$data['vendor_name'][$key] = $db->f('billing_name')  ;
											}
										}
									}
                                    //Get vendor name EOF
									$sub_s_id_str='';
									$sub_s_id_str =trim($data['subsidstr'][$key],",");
									if(!empty($sub_s_id_str)){
										$data['subsid'][$key] = explode(",",$sub_s_id_str);
										$sub_s_id_str=",".$sub_s_id_str.",";
									}
									
						$query3  = " INSERT INTO ". TABLE_BILL_ORD_P
						." SET "
						. TABLE_BILL_ORD_P .".particulars='".processUserData(trim($data['particulars'][$key],','))."'"
						.",". TABLE_BILL_ORD_P .".ord_no = '".$data['number']  ."'"
						.",". TABLE_BILL_ORD_P .".p_amount = '".$data['p_amount'][$key]  ."'"
						.",". TABLE_BILL_ORD_P .".ss_div_id = '".$data['ss_div_id'][$key]  ."'"
						.",". TABLE_BILL_ORD_P .".s_id = '". $data['s_id'][$key]  ."'"
						.",". TABLE_BILL_ORD_P .".sub_s_id = '".$sub_s_id_str  ."'"
						.",". TABLE_BILL_ORD_P .".s_type = '".$data['s_type'][$key]  ."'"
						.",". TABLE_BILL_ORD_P .".s_quantity = '".$data['s_quantity'][$key]  ."'"
						.",". TABLE_BILL_ORD_P .".s_amount = '". $data['s_amount'][$key]  ."'"
						.",". TABLE_BILL_ORD_P .".ss_title = '". processUserData($data['ss_title'][$key])  ."'"
						.",". TABLE_BILL_ORD_P .".ss_punch_line	='".  processUserData($data['ss_punch_line'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_id	= '".   processUserData($data['tax1_id_check'][$key])."'"
						//.",". TABLE_BILL_ORD_P .".tax1_number	= '".processUserData($data['tax1_number'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_name	= '".   processUserData($data['tax1_name_str'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_value	= '".   processUserData($data['tax1_value'][$key]) ."'"
						.",". TABLE_BILL_ORD_P .".tax1_pvalue	= '". processUserData($data['tax1_pvalue'][$key])  ."'"
						.",". TABLE_BILL_ORD_P .".tax1_amount	= '". processUserData($data['tax1_amount'][$key])  ."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub1_id	= '".   processUserData($data['tax1_sub1_id_check'][$key])."'"
						//.",". TABLE_BILL_ORD_P .".tax1_sub1_number= '".  processUserData($data['tax1_sub1_number'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub1_name='".processUserData($data['tax1_sub1_name_str'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub1_value= '".processUserData($data['tax1_sub1_value'][$key]) ."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub1_pvalue='". processUserData($data['tax1_sub1_pvalue'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub1_amount='". processUserData($data['tax1_sub1_amount'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub2_id= '".   processUserData($data['tax1_sub2_id_check'][$key])."'"
						//.",". TABLE_BILL_ORD_P .".tax1_sub2_number = '".processUserData($data['tax1_sub2_number'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub2_name= '".processUserData($data['tax1_sub2_name_str'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub2_value= '". processUserData($data['tax1_sub2_value'][$key]) ."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub2_pvalue='". processUserData($data['tax1_sub2_pvalue'][$key])."'"
						.",". TABLE_BILL_ORD_P .".tax1_sub2_amount= '". processUserData($data['tax1_sub2_amount'][$key])  ."'"
						.",". TABLE_BILL_ORD_P .".d_amount	= '".    processUserData($data['d_amount'][$key]) ."'"  
						.",". TABLE_BILL_ORD_P .".stot_amount	= '".    processUserData($data['stot_amount'][$key]) ."'"        
						.",". TABLE_BILL_ORD_P .".tot_amount = '".   processUserData($data['tot_amount'][$key])  ."'"
						.",". TABLE_BILL_ORD_P .".discount_type = '". processUserData($data['discount_type'][$key])  ."'"
						.",". TABLE_BILL_ORD_P .".pp_amount = '". processUserData($data['pp_amount'][$key]) ."'"
						.",". TABLE_BILL_ORD_P .".vendor = '". processUserData($data['vendor'][$key]) ."'"
						.",". TABLE_BILL_ORD_P .".vendor_name = '". processUserData($data['vendor_name'][$key]) ."'"
						.",". TABLE_BILL_ORD_P .".purchase_particulars= 
						'".processUserData($data['purchase_particulars'][$key])."'"	 
						.",". TABLE_BILL_ORD_P .".is_renewable = '".$data['is_renewable'][$key]."'"                            
						.",". TABLE_BILL_ORD_P .".do_e = '".$do_e."'"                            
						.",". TABLE_BILL_ORD_P .".do_fe = '".$do_fe."'"     ;
                                   $db->query($query3);  
                                }                                   
                            }
                        }
                    }
                    //Particulars edit eof 
				 }
                    
                    // Mark the Pre Order as PROCESSED.
                    
              
                    // Send the notification mails to the concerned persons.
                     
					include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    /*$users = getExecutivesWith($db, array(  
						array('right'=>'nc_bl_or_edit', 'access_level'=>$data['access_level']),
						array('right'=>'nc_bl_or_edit_al', 'access_level'=>($data['access_level']-1) ),
                                                            
                                                        )
                                            );

                    // Organize the data to be sent in email.
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    $data['updater']= array('f_name'=> $my['f_name'],
                                            'l_name'=> $my['l_name'],
                                            'number'=> $my['number']);
                                            
                    
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, 
					f_name, l_name, email');                    
                    // Send mail to the Concerned Executives with same access level and higher access levels
                    if(isset($data['mail_exec_ac'])){ 
                  
                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_MANAGERS', $data, $email) ) {
                                $to     = '';                              
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }
                    // Send Email to the Client.
                      
                    if(isset($data['mail_client'])){
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_CLIENT', $data, $email) ) {
                            $to     = '';                           
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 
							'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                   
                                     
                    // Send Email to Team Members.
                    if(isset($data['mail_executive'])){    
                        // For Speed notification is sent in one email to all the team members.
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_TEAM', $data, $email) ) {
                            $to     = '';
                            foreach ( $data['team_members'] as $key=>$executive) {
                                $to[]   = array('name' => $executive['f_name'] .' '. $executive['l_name'], 'email' => $executive['email']);
                            }
                           
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        // Send Email to the Creator of the Order.
                    } 
					*/
					$data['internal_ac'] = '';
					if(!empty($data['iaccount_head_id'])){
						$table = TABLE_PAYMENT_IACCOUNT_HEAD;
						$fields1 =  TABLE_PAYMENT_IACCOUNT_HEAD .'.account_head_name as iaccount_head_name' ;
						$condition1 = " WHERE ".TABLE_PAYMENT_IACCOUNT_HEAD .".id =".$data['iaccount_head_id']." " ;
						$iaccArr = getRecord($table,$fields1,$condition1);
						if(!empty($iaccArr)){
							$data['internal_ac']  = $iaccArr['iaccount_head_name'];
						}
					}					
					$data1['tax1_total_amount']=$data1['tax1_sub1_total_amount']=$data1['tax1_sub2_total_amount']=0;
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['number'] ."'";
                    Order::getParticulars($db, $temp, '*', $condition_query);
                    $particulars = "" ;
					if(!empty($temp)){
						
						$particulars .="<table cellpadding='0' cellspacing='0' border='1' width='100%'>" ;
                        foreach ( $temp as $pKey => $parti ){
							$sr = $pKey + 1;
							$service_type = $temp[$pKey]['is_renewable']  ? 'Renewable' : 'Non-Renewable' ;
                            $temp_p[] = array(
                                            'particulars'=>$temp[$pKey]['particulars'],
                                            'ss_punch_line' =>$temp[$pKey]['ss_punch_line']  
                                            );
							  	 	
							$ss_title =  $temp[$pKey]['ss_title'] ;
							$ss_punch_line =  $temp[$pKey]['ss_punch_line'] ;
							$p_amount =  $temp[$pKey]['p_amount'] ;
							$s_type =  $temp[$pKey]['s_type'] ;
							$s_quantity =  $temp[$pKey]['s_quantity'] ;
							$samount =  $temp[$pKey]['samount'] ;
							$discount_type =  $temp[$pKey]['discount_type'] ; 
							$stot_amount =  $temp[$pKey]['stot_amount'] ; 
							$tax1_name =  $temp[$pKey]['tax1_name'] ; 
							$tax1_value =  $temp[$pKey]['tax1_value'] ; 
							$tax1_amount =  $temp[$pKey]['tax1_amount'] ; 
							$tax1_sub1_name =  $temp[$pKey]['tax1_sub1_name'] ; 
							$tax1_sub1_value =  $temp[$pKey]['tax1_sub1_value'] ; 
							$tax1_sub1_amount =  $temp[$pKey]['tax1_sub1_amount'] ; 
							$tax1_sub2_name =  $temp[$pKey]['tax1_sub2_name'] ; 
							$tax1_sub2_value =  $temp[$pKey]['tax1_sub2_value'] ; 
							$tax1_sub2_amount =  $temp[$pKey]['tax1_sub2_amount'] ; 
							$d_amount =  $temp[$pKey]['d_amount'] ; 
							$stot_amount =  $temp[$pKey]['stot_amount'] ; 
							$tot_amount =  $temp[$pKey]['tot_amount'] ; 
							$discount_type =  $temp[$pKey]['discount_type'] ; 
							$particulars .="
							<tr>
								<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'><b>".$sr."</b> Punchline : ".$temp[$pKey]['ss_punch_line'] ." - Service type  : ".$service_type  ."</td/>
							</tr>
							<tr>
								<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Services : ".$ss_title ."</td/>
							</tr>
							<tr>
								<td> 
									<table border='0'>
										<tr>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Price</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Type</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Qty</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Amt</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Discount</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>DiscType</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Sub Tot</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Taxname</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tax%</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tax Amt</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tot Amt</td>
										</tr>
										<tr>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$p_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$s_type."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$s_quantity."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$samount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$d_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$discount_type."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$stot_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_name."<br/>".$tax1_sub1_name."<br/>".$tax1_sub2_name."
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_value."<br/>".$tax1_sub1_value."<br/>".$tax1_sub2_value."
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_amount."<br/>".$tax1_sub1_amount."<br/>".$tax1_sub2_amount."
											
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tot_amount."</td>
										</tr>
									</table>
								</td/>
							</tr>
							" ;
							
							//tax bof
							
							$data1['total_d_amount'] = $data1['total_d_amount'] + $temp[$pKey]['d_amount'] ; 
							$data1['tax1_id'] = $temp[$pKey]['tax1_id'] ; 
							$data1['tax1_number'] = $temp[$pKey]['tax1_number'] ; 
							$data1['tax1_name'] =  $temp[$pKey]['tax1_name'] ; 
							$data1['tax1_value'] = $temp[$pKey]['tax1_value'] ; 
							$data1['tax1_pvalue'] = $temp[$pKey]['tax1_pvalue'] ; 
							$data1['tax1_total_amount'] = $data1['tax1_total_amount']+$temp[$pKey]['tax1_amount'] ; 
							
							$data1['tax1_sub1_id'] = $temp[$pKey]['tax1_sub1_id'] ; 
							$data1['tax1_sub1_number']= $temp[$pKey]['tax1_sub1_number'] ; 
							$data1['tax1_sub1_name'] = $temp[$pKey]['tax1_sub1_name'] ; 
							$data1['tax1_sub1_value'] = $temp[$pKey]['tax1_sub1_value'] ; 
							$data1['tax1_sub1_pvalue'] = $temp[$pKey]['tax1_sub1_pvalue'] ; 
							$data1['tax1_sub1_total_amount'] = $data1['tax1_sub1_total_amount'] + $temp[$pKey]['tax1_sub1_amount'] ; 
							
							$data1['tax1_sub2_id'] = $temp[$pKey]['tax1_sub2_id'] ; 
							$data1['tax1_sub2_number'] = $temp[$pKey]['tax1_sub2_number'] ; 
							$data1['tax1_sub2_name'] =  $temp[$pKey]['tax1_sub2_name'] ; 
							$data1['tax1_sub2_value'] = $temp[$pKey]['tax1_sub2_value'] ; 
							$data1['tax1_sub2_pvalue'] = $temp[$pKey]['tax1_sub2_pvalue'] ; 
							$data1['tax1_sub2_total_amount'] =  $data1['tax1_sub2_total_amount'] + 
							$temp[$pKey]['tax1_sub2_amount'] ;  
						   //tax eof
							
                        }
						$particulars .= "</table>";
						$data1['total_tax_amount'] = $data1['tax1_total_amount'] + $data1['tax1_sub1_total_amount']+$data1['tax1_sub2_total_amount'] ; 
						$data1['total_tax_balance_amount'] = $data1['total_tax_amount'] - $total_tax_paid_amount ;
						$sqlu  = " UPDATE ".TABLE_BILL_ORDERS." SET "
						."". TABLE_BILL_ORDERS .".total_tax_amount = '".$data1['total_tax_amount'] ."'"
						.",".TABLE_BILL_ORDERS .".total_tax_balance_amount='".$data1['total_tax_balance_amount'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_id = '". $data1['tax1_id'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_number = '". $data1['tax1_number'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_name = '". $data1['tax1_name'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_value = '". $data1['tax1_value'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_pvalue = '". $data1['tax1_pvalue'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_total_amount = '". $data1['tax1_total_amount'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub1_id = '". $data1['tax1_sub1_id'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub1_number = '". $data1['tax1_sub1_number'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub1_name = '". $data1['tax1_sub1_name'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub1_value = '". $data1['tax1_sub1_value'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub1_pvalue = '". $data1['tax1_sub1_pvalue'] ."'"
					    .",". TABLE_BILL_ORDERS.".tax1_sub1_total_amount='".$data1['tax1_sub1_total_amount'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub2_id = '". $data1['tax1_sub2_id'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub2_number = '". $data1['tax1_sub2_number'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub2_name = '". $data1['tax1_sub2_name'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub2_value = '". $data1['tax1_sub2_value'] ."'"
						.",". TABLE_BILL_ORDERS .".tax1_sub2_pvalue = '". $data1['tax1_sub2_pvalue'] ."'"
						.",". TABLE_BILL_ORDERS.".tax1_sub2_total_amount='". $data1['tax1_sub2_total_amount']."'"
						.",". TABLE_BILL_ORDERS.".total_d_amount='". $data1['total_d_amount']."'"
						." WHERE ".TABLE_BILL_ORDERS.".id ='".$or_id ."'";
						$db->query($sqlu);
                    }
					
					$sub_clients_members =''; 
					if( !empty($clients_su_str)){
						$clients_su_str = trim($clients_su_str,",");
						$clients_su_str = str_replace(",","','",$clients_su_str);
						
						Clients::getList($db, $_ALL_POST['clients_su_members'], 'user_id,number,
						f_name,l_name', "WHERE user_id IN ('". $clients_su_str ."')");
						
						if(!empty($_ALL_POST['clients_su_members'])){
							foreach ( $_ALL_POST['clients_su_members'] as $key=>$members) {							 
								$sub_clients_members .= $members['f_name'] .' '. $members['l_name'] .'';
							}
						}
					}
					$team_members='';
					$team_str = implode(",", $data['team']) ;
					 if(!empty($team_str )){
						$team_str = trim($team_str,",");
						$team_str = str_replace(",","','",$team_str);
					 
						User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN ('". $team_str ."')");
						
						foreach ( $_ALL_POST['team_members'] as $key=>$members) {
						
							 $team_members .= $members['f_name'] .' '. $members['l_name']."," ;
							 
						}
					}
			 
                    $data['client_name']= $data['client']['f_name'] .' '. $data['client']['l_name']; 
                    $data['updated_by']= $my['f_name'].' '.$my['l_name']; 
                    $data['sub_clients_members']=$sub_clients_members; 
                    $data['team_members']=$team_members; 
                    $data['particulars1']=$particulars; 
                    $data['currency']=$data['currency_name']; 
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    $email = NULL;
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'ORDER_UPDATE_TO_ADMIN', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $bill_ord_name , 'email' => $bill_ord_email);                        
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                        
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                       
                    }
					$_ALL_POST['ord_discount_against_id_old']=$_ALL_POST['ord_discount_against_id'];
                   /*  if(empty($data['particulars'])){
						$data['particulars']=array('0'=>'');
                    } */
                    
                }
                
            }
			
        }else{
			if(empty($data['particulars'])){
				$data['particulars']=array('0'=>'');
			}      
		}
   
        if(isset($_POST['btnReturnInv']) && $messages->getErrorMessageCount() <= 0 ){
			$url1 = DIR_WS_NC."/bill-invoice-profm.php?perform=add&or_id=".$or_id;
			  
           // header("Location:".DIR_WS_NC."/bill-invoice-profm.php?perform=add&or_id=".$or_id);
		 
			echo '<html>
					<head>
						<meta http-equiv="Refresh" content="0;url='.$url1.'" />
					</head><body></body>
				  </html>';
			
        }
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
            header("Location:".DIR_WS_NC."/bill-order.php?perform=nedit&or_id=".$or_id."&is_inv_cr=".$is_inv_cr."&updated=1");			 
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $or_id;
            $action="Orders List";
			$searchStr = 1;
			if(!empty($or_id)){
				$condition_query= " WHERE ".TABLE_BILL_ORDERS.".id='".$or_id."'";
			}
            include ( DIR_FS_NC .'/bill-order-list.php');
        }
        else {
            
           

            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['po_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['po_access_level'], $al_list, $index);
                $_ALL_POST['po_access_level'] = $al_list[$index[0]]['title'];
            }
            $variables['can_access_extra_details'] = false;
            if ( $perm->has('nc_bl_or_extra_details') ) {
                $variables['can_access_extra_details'] = true;
            }
			
			$variables['can_mark_special'] = false;
			
			if ( $perm->has('nc_bl_or_spl') ) {
				$variables['can_mark_special'] = true;
			}
			$oarry = Order::getTypeArray();
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'or_id', 'value' => $or_id);
            $hidden[] = array('name'=> 'po_id', 'value' => $po_id);
            $hidden[] = array('name'=> 'is_inv_cr', 'value' => $is_inv_cr);
            $hidden[] = array('name'=> 'wp_support_team', 'value' => $wp_support_team);
            $hidden[] = array('name'=> 'perform', 'value' => 'nedit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //print_r($_ALL_POST);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => 'or_id', 'value' => 'or_id');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'oarry', 'value' => 'oarry');
			$page["var"][] = array('variable' => 'data', 'value' => 'data');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'is_inv_cr', 'value' => 'is_inv_cr');
            //$page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
            $page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
			
			$page["var"][] = array('variable' => 'stype_list', 'value' => 'stype_list');  
			$page["var"][] = array('variable' => 'discountType', 'value' => 'discountType');  
			$page["var"][] = array('variable' => 'no_list', 'value' => 'no_list');  
            $page["var"][] = array('variable' => 'op_list', 'value' => 'op_list');     
            $page["var"][] = array('variable' => 'roundOff_list', 'value' => 'roundOff_list');    		
            $page["var"][] = array('variable' => 'tax_list', 'value' => 'tax_list');    
			$page["var"][] = array('variable' => 'subtax_list', 'value' => 'subtax_list');			
            $page["var"][] = array('variable' => 'tax_vlist', 'value' => 'tax_vlist');     
			
            $page["var"][] = array('variable' => 'source', 'value' => 'source');
			$page["var"][] = array('variable' => 'lst_iah', 'value' => 'lst_iah');
            $page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-nedit.html');
        }
    } else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>