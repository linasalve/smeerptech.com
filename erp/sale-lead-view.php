<?php

    if ( $perm->has('nc_sl_ld_details') ) {
        $lead_id = isset($_GET['lead_id']) ? $_GET['lead_id'] : ( isset($_POST['lead_id'] ) ? $_POST['lead_id'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_sl_ld_details_al') ) {
            $access_level += 1;
        }
		
		$query="SELECT ".TABLE_SALE_LEADS.".*, ".TABLE_SALE_CAMPAIGN.".title as campaign_title , ".TABLE_SALE_INDUSTRY.".title as 				               	industry_title , ".TABLE_SALE_SOURCE.".title as source_title_to , ".
				TABLE_USER.".f_name as lead_assign_to_f_name, ".TABLE_USER.".l_name as lead_assign_to_l_name ,".TABLE_SALE_RIVALS.".f_name as rival_f_name ,".TABLE_SALE_RIVALS.".l_name as rival_l_name ,".TABLE_SALE_RIVALS.".company_name as rival_company_name
				FROM ".		  TABLE_SALE_LEADS. " LEFT JOIN ". TABLE_SALE_CAMPAIGN." ON ".TABLE_SALE_LEADS.".lead_campaign_id= ".TABLE_SALE_CAMPAIGN.".id
				LEFT JOIN ". TABLE_SALE_INDUSTRY." ON ".TABLE_SALE_LEADS.".lead_industry_id= ".TABLE_SALE_INDUSTRY.".id
				LEFT JOIN ". TABLE_SALE_SOURCE." ON ".TABLE_SALE_LEADS.".lead_source_to= ".TABLE_SALE_SOURCE.".id
				LEFT JOIN ". TABLE_USER." ON ".TABLE_SALE_LEADS.".lead_assign_to= ".TABLE_USER.".user_id
				LEFT JOIN ". TABLE_SALE_RIVALS." ON ".TABLE_SALE_LEADS.".rivals_id= ".TABLE_SALE_RIVALS.".id
				WHERE ".TABLE_SALE_LEADS.".lead_id='".$lead_id."'";
					
		$sourcequery="SELECT ".TABLE_SALE_LEADS.".*, ".TABLE_SALE_SOURCE.".title as source_title_from FROM ".TABLE_SALE_LEADS.
					" LEFT JOIN ". TABLE_SALE_SOURCE." ON ".TABLE_SALE_LEADS.".lead_source_from= ".TABLE_SALE_SOURCE.".id					
					WHERE ".TABLE_SALE_LEADS.".lead_id='".$lead_id."'";
		 if ( ($db->query($sourcequery)) > 0 ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$source_title_from =$db->f('source_title_from');
				}
			}
		}							
        if ( ($db->query($query)) > 0 ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$list[] = processSqlData($db->result());
				}
			}		
/*        if ( Lead::getList($db, $list, '*', " WHERE user_id = '$user_id'") > 0 ) {*/
            $_ALL_POST = $list['0'];
            
            //if ( $_ALL_POST['access_level'] < $access_level ) {
                include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
                //include_once ( DIR_FS_CLASS .'/UserReminder.class.php');

                $regionlead         = new RegionLead();
                $phonelead          = new PhoneLead(TABLE_SALE_LEADS);
                //$reminder       = new UserReminder(TABLE_LEADS);

                //$_ALL_POST['manager'] = Lead::getManager($db, '', $_ALL_POST['manager']);

                // Read the available Access Level and User Roles.
                $_ALL_POST['access_level']  = Leads::getAccessLevel($db, $access_level, $_ALL_POST['access_level']);
                //$_ALL_POST['roles']         = Lead::getRoles($db, $access_level, $_ALL_POST['roles']);
                //print_r($role_list);
                // Read the Contact Numbers.
                $phonelead->setPhoneOf(TABLE_SALE_LEADS, $lead_id);
                $_ALL_POST['phone'] = $phonelead->get($db);
                
                // Read the Addresses.
                $regionlead->setAddressOf(TABLE_SALE_LEADS, $lead_id);
                $_ALL_POST['address_list'] = $regionlead->get();
                
                // Read the Reminders.
                //$reminder->setReminderOf(TABLE_LEADS_RIVALS, $user_id);
                //$_ALL_POST['reminder_list'] = $reminder->get($db);

                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    			$page["var"][] = array('variable' => 'source_title_from', 'value' => 'source_title_from');
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-view.html');
           // }
            //else {
              //  $messages->setErrorMessage("You do not have the Permission to view the Lead with the current Access Level.");
            //}
        }
        else {
            $messages->setErrorMessage("The Lead details were not found. ");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>