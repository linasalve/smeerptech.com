<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_uc_edit') ) {
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
    
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
        //include ( DIR_FS_INCLUDES .'/sale-industry.inc.php');
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new Region();
        $phone          = new Phone(TABLE_CLIENTS);
        $reminder       = new UserReminder(TABLE_CLIENTS);
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_uc_edit_al') ) {
            $access_level += 1;
        }
        //For Clients list allotment
        if ( $perm->has('nc_uc') && ( $perm->has('nc_uc_list') || $perm->has('nc_uc_search'))){
            $variables['can_view_clist']     = true;
        }
        
        
        $al_list    = getAccessLevel($db, $access_level);
        //$role_list  = Clients::getRoles($db, $access_level);

        $grade_list  = Clients::getGrades();
        
        //BOF read the available services
	    $authorization_list  = Clients::getAuthorization();
        $services_list  = Clients::getServices();
		$memberTypeList  = Clients::getMemberType();
        //EOF read the available services
        
        //BOF read the available industries
        $industry_list = NULL;
        $required_fields ='*';
        $condition_query="WHERE status='".Industry::ACTIVE."'";
		Industry::getList($db,$industry_list,$required_fields,$condition_query);
        //EOF read the available industries
        
        //BOF read the available industries
		Clients::getCountryCode($db,$country_code);
        //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list, 
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder
                        );
            
            if ( Clients::validateUpdate($data, $extra) ) {
                $service_idstr='';
                if(isset($_POST['service_id']) && !empty($_POST['service_id'])){
                    $service_ids=$_POST['service_id'];
                    $service_idstr=implode(",",$service_ids);
                    $service_idstr = ",".$service_idstr.",";	
                }
				$authorization_idstr='';
                if(isset($_POST['authorization_id']) && !empty($_POST['authorization_id'])){
                    $authorization_ids=$_POST['authorization_id'];
                    $authorization_idstr=implode(",",$authorization_ids);
                    $authorization_idstr = ",".$authorization_idstr.",";	
                }
		
                //Default entry remove me 
                /*                
                $service_idstr = Clients::BILLING.",".Clients::ST ;    
                if(!empty($service_idstr)){
                    $service_idstr = ",".$service_idstr.",";	
                }*/
                
                $industry_idstr='';
                if(isset($_POST['industry']) && !empty($_POST['industry'])){
                    $industry_ids=$_POST['industry'];
                    $industry_idstr= implode(",",$industry_ids);
                }
                if(!empty($industry_idstr)){
                    $industry_idstr=",".$industry_idstr.",";	
                }
				$membertype_ids = $_POST['member_type'];
                $membertypestr=implode(",",$membertype_ids);
                if(!empty($membertypestr)){
                    $membertypestr=",".$membertypestr.",";
                }
                $alloted_clientsstr='';
                if(isset($_POST['alloted_clients']) && !empty($_POST['alloted_clients'])){
                    $alloted_clients=$_POST['alloted_clients'];
                    $alloted_clientsstr=implode(",",$alloted_clients);
                }
                if(!empty($alloted_clientsstr)){
                    $alloted_clientsstr=",".$alloted_clientsstr.",";
                }
				//Premium Email Management Users
				$premium_email_userstr = '';
                $premium_email_user = $_POST['premium_email_user'];
                $premium_email_userstr = implode(",",$premium_email_user);               
                if(!empty($premium_email_userstr)){
                    $premium_email_userstr = ",".$premium_email_userstr.",";
                }				
				//Premium Email Admin Users
				$premium_email_adminstr = '';
                $premium_email_admin = $_POST['premium_email_admin'];
                $premium_email_adminstr = implode(",",$premium_email_admin);               
                if(!empty($premium_email_adminstr)){
                    $premium_email_adminstr = ",".$premium_email_adminstr.",";
                }				
				//Premium Email Billing Users
				$premium_email_buserstr = '';
                $premium_email_buser = $_POST['premium_email_buser'];
                $premium_email_buserstr = implode(",",$premium_email_buser);               
                if(!empty($premium_email_buserstr)){
                    $premium_email_buserstr = ",".$premium_email_buserstr.",";
                }
                if( isset($data['check_email'])){
					$data['check_email']=1;
				}else{
					$data['check_email']=0;
				}
				if( isset($data['is_classified'])){
					$data['is_classified']=1;
				}else{
					$data['is_classified']=0;
				}
				if( isset($data['is_tran_purchase_classified'])){
					$data['is_tran_purchase_classified']=1;
				}else{
					$data['is_tran_purchase_classified']=0;
				}
				$query  = " UPDATE ". TABLE_CLIENTS
                    ." SET "
					."". TABLE_CLIENTS .".service_id   = '". $service_idstr ."'"
					.",". TABLE_CLIENTS .".authorization_id  = '". $authorization_idstr ."'"
					.",". TABLE_CLIENTS .".member_type   = '". $membertypestr ."'"
					.",". TABLE_CLIENTS .".enable_login  = '". $data['enable_login'] ."'"
				   /* .",". TABLE_CLIENTS .".number      = '". $data['number'] ."'"
					.",". TABLE_CLIENTS .".manager     = '". $data['manager'] ."'"*/
					.",". TABLE_CLIENTS .".search_keywords 	 = '". $data['search_keywords'] ."'"
					.",". TABLE_CLIENTS .".company_details 	 = '". $data['company_details'] ."'"
					.",". TABLE_CLIENTS .".connected_via 	 = '". $data['connected_via'] ."'"
					.",". TABLE_CLIENTS .".team			= '". implode(',', $data['team']) ."'"
					.",". TABLE_CLIENTS .".username    = '". $data['username'] ."'"
					. $data['password']
					.",". TABLE_CLIENTS .".access_level= '". $data['access_level'] ."'"
					.",". TABLE_CLIENTS .".updated_by	= '". $my['uid'] ."'"
					.",". TABLE_CLIENTS .".updated_by_name	= '". $my['f_name']." ".$my['l_name'] ."'"
					.",". TABLE_CLIENTS .".do_update    = '".date("Y-m-d h:i:s") ."'"
					.",". TABLE_CLIENTS .".email       = '". $data['email'] ."'"
					.",". TABLE_CLIENTS .".check_email = '". $data['check_email'] ."'"
					.",". TABLE_CLIENTS .".is_classified = '". $data['is_classified'] ."'"
					.",". TABLE_CLIENTS .".is_tran_purchase_classified = '". $data['is_tran_purchase_classified'] ."'"
					.",". TABLE_CLIENTS .".email_1     = '". $data['email_1'] ."'"						    
					.",". TABLE_CLIENTS .".email_2     = '". $data['email_2'] ."'"						
					.",". TABLE_CLIENTS .".email_3     	= '". $data['email_3'] ."'"							
					.",". TABLE_CLIENTS .".email_4     	= '". $data['email_4'] ."'"	
					.",". TABLE_CLIENTS .".additional_email = '". $data['additional_email'] ."'"
					.",". TABLE_CLIENTS .".title       = '". $data['title'] ."'"
					.",". TABLE_CLIENTS .".f_name      = '". $data['f_name'] ."'"
					.",". TABLE_CLIENTS .".m_name      = '". $data['m_name'] ."'"
					.",". TABLE_CLIENTS .".l_name      = '". $data['l_name'] ."'"
					.",". TABLE_CLIENTS .".p_name      = '". $data['p_name'] ."'"  
					.",". TABLE_CLIENTS .".desig       	= '". $data['desig'] ."'"						
					.",". TABLE_CLIENTS .".grade      	= '". $data['grade'] ."'"
					.",". TABLE_CLIENTS .".risk_type      	= '". $data['risk_type'] ."'"
					.",". TABLE_CLIENTS .".credit_limit = '". $data['credit_limit'] ."'"
					.",". TABLE_CLIENTS .".cst_no         = '". $data['cst_no'] ."'"
					.",". TABLE_CLIENTS .".cst_no_details = '". $data['cst_no_details'] ."'"
					.",". TABLE_CLIENTS .".vat_no       = '". $data['vat_no'] ."'"
					.",". TABLE_CLIENTS .".vat_no_details  = '". $data['vat_no_details'] ."'"
					.",". TABLE_CLIENTS .".service_tax_no  = '". $data['service_tax_no'] ."'"
					.",". TABLE_CLIENTS .".st_no_details  = '". $data['st_no_details'] ."'"
					.",". TABLE_CLIENTS .".pan_no  		= '". $data['pan_no'] ."'"							 
                    .",". TABLE_CLIENTS .".pan_no_details = '".$data['pan_no_details']."'"	
					.",". TABLE_CLIENTS .".bank1_details = '".$data['bank1_details']."'"							 
                    .",". TABLE_CLIENTS .".bank2_details = '".$data['bank2_details']."'"	
					.",". TABLE_CLIENTS .".org         = '". $data['org'] ."'"
					.",". TABLE_CLIENTS .".domain      = '". $data['domain'] ."'"
					.",". TABLE_CLIENTS .".gender      = '". $data['gender'] ."'"
					.",". TABLE_CLIENTS .".do_birth    = '". $data['do_birth'] ."'"
					.",". TABLE_CLIENTS .".do_aniv     = '". $data['do_aniv'] ."'"
					.",". TABLE_CLIENTS .".do_reg     = '". $data['do_reg'] ."'"
					.",". TABLE_CLIENTS .".remarks     = '". $data['remarks'] ."'"
					.",". TABLE_CLIENTS .".special_remarks 	= '". $data['special_remarks'] ."'"
					.",". TABLE_CLIENTS .".payment_remarks 	= '". $data['payment_remarks'] ."'"
					.",". TABLE_CLIENTS .".search_keywords 	 = '". $data['search_keywords'] ."'"
					.",". TABLE_CLIENTS .".company_details 	 = '". $data['company_details'] ."'"
					.",". TABLE_CLIENTS .".connected_via 	 = '". $data['connected_via'] ."'"
					.",". TABLE_CLIENTS .".industry     = '". $industry_idstr ."'"
					.",". TABLE_CLIENTS .".industry_details    = '". $data['industry_details'] ."'"
					.",". TABLE_CLIENTS .".wt_you_do    = '". $data['wt_you_do'] ."'"
					.",". TABLE_CLIENTS .".mobile1      = '". $data['mobile1'] ."'"
					.",". TABLE_CLIENTS .".mobile2      = '". $data['mobile2'] ."'"
					.",". TABLE_CLIENTS .".alloted_clients   = '". $alloted_clientsstr ."'"
					.",". TABLE_CLIENTS .".premium_email_mgmt   = '". $premium_email_userstr ."'"
					.",". TABLE_CLIENTS .".premium_email_admin   = '". $premium_email_adminstr ."'"
					.",". TABLE_CLIENTS .".premium_email_billing   = '". $premium_email_buserstr ."'"
					.",". TABLE_CLIENTS .".billing_name   = '". $data['billing_name'] ."'"
				   .",". TABLE_CLIENTS .".spouse_dob   = '". $data['spouse_dob'] ."'"
					.",". TABLE_CLIENTS .".status      = '". $data['status'] ."'" 
				." WHERE ". TABLE_CLIENTS .".user_id   = '". $data['user_id'] ."'";
                
                if ( $db->query($query) ) {
					$variables['hid'] = $data['user_id'];
					//Update member type BOF
					 $query1 = "UPDATE ".TABLE_CLIENTS." SET member_type='".$membertypestr."'
					 WHERE parent_id = '". $data['user_id'] ."' ";
					 $db->query($query1);  
					//Update member type EOF					
					//Push email id into Newsletters Table BOF
					$table = TABLE_NEWSLETTERS_MEMBERS;
					include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
					if(!empty($data['email'])){
						$domain1 = strtolower(strstr($data['email'], '@'));
						$condi1= " WHERE email='".strtolower(trim($data['email']))."'";
						if( $domain1!= 'smeerptech.com' && 
							!NewslettersMembers::duplicateFieldValue($db, $table, 'email',$condi1) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email']))."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_1'])){
						$domain2 = strtolower(strstr($data['email_1'], '@'));
						$condi2= " WHERE email='".strtolower(trim($data['email_1']))."'";
						if( $domain2!='smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email',$condi2) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_1']))."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_2'])){
						$domain3 = strtolower(strstr($data['email_2'], '@'));
						$condi3= " WHERE email='".strtolower(trim($data['email_2']))."'";
						if($domain3!= 'smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi3) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_2']))."'" ;
						  $db->query($sql1);
						}
					}
					//Push email id into Newsletters Table EOF
					
                    // Update the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        if ( isset($data['a_remove'][$i]) && $data['a_remove'][$i] == '1') {
                            if ( ! $region->delete($db, $data['address_id'][$i], $variables['hid'], TABLE_CLIENTS) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setOkMessage($errors['description']);
                                }
                            }
                        }else{
                            $address_arr = array(   
								'id'   => isset($data['address_id'][$i])? $data['address_id'][$i] : '0',
								'address_type'  => $data['address_type'][$i],
								'company_name'  => '',
								'address'       => $data['address'][$i],
								'city'          => $data['city'][$i],
								'state'         => $data['state'][$i],
								'country'       => $data['country'][$i],
								'zipcode'       => $data['zipcode'][$i],
								'is_preferred'  => isset($data['is_preferred'][$i])? $data['is_preferred'][$i] : '0',
								'is_verified'   => isset($data['is_verified'][$i])? $data['is_verified'][$i] : '0'
                            );
                            
                            if ( !$region->update($variables['hid'], TABLE_CLIENTS, $address_arr) ) {
                                foreach ( $region->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. 
									$errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_CLIENTS) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                              $phone_arr = array( 
								'id'            => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
								'p_type'        => $data['p_type'][$i],
								'cc'            => $data['cc'][$i],
								'ac'            => $data['ac'][$i],
								'p_number'      => $data['p_number'][$i],
								'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
								'p_is_verified' => isset($data['p_is_verified'][$i])?$data['p_is_verified'][$i]:'0'
                               );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_CLIENTS, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Update the Reminders.
                    for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['do_reminder'][$i]) && 
							!empty($data['subject'][$i]) && !empty($data['description'][$i]) ) {
                            
							$reminder_arr = array(
								'id' => (isset($data['remind_id'][$i])? $data['remind_id'][$i] : ''),
								'do_reminder'   => $data['do_reminder'][$i],
								'subject'   	=> $data['subject'][$i],
								'description'   => $data['description'][$i]
							);
							
							
							
                            if(!$reminder->update($db, $variables['hid'], TABLE_CLIENTS, $reminder_arr)){
                                $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                foreach ( $reminder->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. 
									$errors['description']);
                                }
                            }
                        }
                    }
                    // Send The Email to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_uc_add_notif', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_uc_add_notif_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    /*$manager = Clients::getManager($db, '', $data['manager'], 'f_name,l_name,email');
                    $data['manager_name']   = $manager['f_name'] .' '. $manager['l_name'];
                    $data['manager_email']  = $manager['email'];
                    $data['manager_phone']  = '';
                    */
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link']       = DIR_WS_NC .'/clients.php?perform=view_details&user_id='. $data['user_id'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'CLIENT_INFO_UPDATE_MANAGERS', $data, $email) ) {
                            $to     = '';
                            //echo " subject : ".$email['subject'];
                            //echo "<br/> body : ".$email['body'];
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }                        
                    }
                    
                    // Send Email to the Client whose infromation was updated.
					if($_ALL_POST['send_service_mail']=='1'){
						/*SEND SERVICE PDF BOF*/
						include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
						include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php'); 
						$id = 39; 
						$fields = TABLE_ST_TEMPLATE.'.*'  ;            
						$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
						SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1);
						
						if(!empty($details)){
						
							$details = $details[0];
							$data['template_id'] = $details['id'];
							$data['template_file_1'] = $details['file_1'];
							$data['template_file_2'] = $details['file_2'];
							$data['template_file_3'] = $details['file_3'];
							$data['subject'] = processUserData($details['subject']);
							$data['text'] = processUserData($details['details']);
							
							$data['display_name']=$data['display_user_id']=$data['display_designation']='';				
							$randomUser = getCeoDetails();
							$data['display_name'] = $randomUser['name'];
							$data['display_user_id'] = $randomUser['user_id'];
							$data['display_designation'] = $randomUser['desig'];
							$data['ticket_owner'] = $data['f_name']." ".$data['l_name'];
							$data['ticket_owner_uid'] = $variables['hid'] ;
							$ticket_no  =  SupportTicket::getNewNumber($db);
							$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
													
							//Check that this user's ticket is exist or not
							$ticket_id = 0;
							$sql = " SELECT ticket_id,ticket_no FROM ".TABLE_ST_TICKETS." WHERE 
								".TABLE_ST_TICKETS.".ticket_owner_uid = '".$data['user_id']."' 
								AND ".TABLE_ST_TICKETS.".service_pdf_tkt ='1' AND 
								".TABLE_ST_TICKETS.".ticket_child=0 LIMIT 0,1";
							$db->query($sql) ;
							$mticket_no='';
							if($db->nf() > 0){
								$db->next_record() ;
								$ticket_id = $db->f("ticket_id") ; 
								$mticket_no = $db->f("ticket_no") ;
							}
							
							if($ticket_id>0){
								
								$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
								. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
								. TABLE_ST_TICKETS .".template_id       = '". $data['template_id'] ."', "
								. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
								. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
								. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
								. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
								. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
								. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
								. TABLE_ST_TICKETS .".ticket_creator  = '". $my['f_name']." ". $my['l_name'] ."', "
								. TABLE_ST_TICKETS .".ticket_subject  = '". $data['subject'] ."', "
								. TABLE_ST_TICKETS .".ticket_text     = '". $data['text'] ."', "
								. TABLE_ST_TICKETS .".mail_to_all_su  = '".$mail_to_all_su."', "
								. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
								. TABLE_ST_TICKETS .".ticket_status   = '".$ticket_status ."', "
								. TABLE_ST_TICKETS .".ticket_child    = '".$ticket_id."', "
								. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
								. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
								. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
								. TABLE_ST_TICKETS .".min = '". $data['min']."', "
								. TABLE_ST_TICKETS .".service_pdf_tkt = '1', "
								. TABLE_ST_TICKETS .".ticket_response_time = '0', "
								. TABLE_ST_TICKETS .".ticket_replied  = '0', "
								. TABLE_ST_TICKETS .".from_admin_panel = '".SupportTicket::ADMIN_PANEL."', "
								. TABLE_ST_TICKETS .".display_name   = '". $data['display_name'] ."', "
								. TABLE_ST_TICKETS .".display_user_id = '". $data['display_user_id'] ."', "
								. TABLE_ST_TICKETS .".display_designation = '". $data['display_designation'] ."', "
								. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
								. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
								. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
								. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
								. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ;
								
							
							}else{
								$mticket_no	=$ticket_no;
								$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
								. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
								. TABLE_ST_TICKETS .".template_id       = '". $data['template_id'] ."', "
								. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
								. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
								. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
								. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
								. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
								. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
								. TABLE_ST_TICKETS .".ticket_creator  = '". $my['f_name']." ". $my['l_name'] ."', "
								. TABLE_ST_TICKETS .".ticket_subject  = '". $data['subject'] ."', "
								. TABLE_ST_TICKETS .".ticket_text     = '". $data['text'] ."', "
								. TABLE_ST_TICKETS .".mail_to_all_su  = '".$mail_to_all_su."', "
								. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
								. TABLE_ST_TICKETS .".ticket_status   = '".$ticket_status ."', "
								. TABLE_ST_TICKETS .".ticket_child    = '0', "
								. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
								. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
								. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
								. TABLE_ST_TICKETS .".min = '". $data['min']."', "
								. TABLE_ST_TICKETS .".service_pdf_tkt = '1', "
								. TABLE_ST_TICKETS .".ticket_response_time = '0', "
								. TABLE_ST_TICKETS .".ticket_replied  = '0', "
								. TABLE_ST_TICKETS .".from_admin_panel = '".SupportTicket::ADMIN_PANEL."', "
								. TABLE_ST_TICKETS .".display_name   = '". $data['display_name'] ."', "
								. TABLE_ST_TICKETS .".display_user_id = '". $data['display_user_id'] ."', "
								. TABLE_ST_TICKETS .".display_designation = '". $data['display_designation'] ."', "
								. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
								. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
								. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
								. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
								. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ;
							}
							if(!empty($data['template_file_1'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
							}
							if(!empty($data['template_file_2'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
							}
							if(!empty($data['template_file_3'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
							}
							if(!empty($data['email']) || !empty($data['email_1']) || !empty($data['email_2'])){
								$db->query($query) ;
								$variables['hid'] = $db->last_inserted_id() ;
								$mail_send_to_su='';
								$data['mticket_no'] = $mticket_no ;
								
								if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ){                                    
									$to = '';
									if(!empty($data['email'])){
										
										$to[]   = array('name' => $data['email'] , 'email' => 
										$data['email']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email'];
										//echo $email["body"];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
										 
									}
									if(!empty($data['email_1'])){
										//$to = '';
										$to[]   = array('name' => $data['email_1'] , 'email' => 
										$data['email_1']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email_1'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
									if(!empty($data['email_2'])){
										//$to = '';
										$to[]   = array('name' => $data['email_2'] , 'email' => 
										$data['email_2']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email_2'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
									if(!empty($to)){
										//$to = '';
										$bcc[]  = array('name' => $data['ticket_owner'] , 'email' => 
										$smeerp_client_email);        
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name);
										//echo  $email["body"];
									}

								}
								
								if(!empty($mail_send_to_su)){
									$query_update = "UPDATE ". TABLE_ST_TICKETS 
										." SET ". TABLE_ST_TICKETS .".mail_send_to_su
										= '".trim($mail_send_to_su,",")."'"                                   
										." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
									$db->query($query_update) ;
								}
							}
							
						  }
					}
					/*SEND SERVICE PDF EOF*/ 
                    $messages->setOkMessage("Clients Profile has been updated.");
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }else{
                    $messages->setErrorMessage('Clients Profile was not updated.');
                }
            }
        }else{
			$_ALL_POST['send_service_mail']=1;
		}

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/clients-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'            => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
                
                // Preserve the Addresses.
                $count = count($_ALL_POST['city']);
                for ( $i=0; $i<$count; $i++ ) {
//                  if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['address_list'][] = array(   
						'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
						//'company_name'   => isset($_ALL_POST['company_name'][$i])? $_ALL_POST['company_name'][$i]:'',
						'is_verified'   => isset($_ALL_POST['is_verified'][$i])? $_ALL_POST['is_verified'][$i]:'',
						'is_preferred'  => isset($_ALL_POST['is_preferred'][$i])? $_ALL_POST['is_preferred'][$i]:'',
						'address_type'  => isset($_ALL_POST['address_type'][$i])? $_ALL_POST['address_type'][$i]:'',
						'address'       => isset($_ALL_POST['address'][$i])? $_ALL_POST['address'][$i]:'',
						'city'          => isset($_ALL_POST['city'][$i])? $_ALL_POST['city'][$i]:'',
						'state'         => isset($_ALL_POST['state'][$i])? $_ALL_POST['state'][$i]:'',
						'zipcode'       => isset($_ALL_POST['zipcode'][$i])? $_ALL_POST['zipcode'][$i]:'',
						'country'       => isset($_ALL_POST['country'][$i])? $_ALL_POST['country'][$i]:'',
                        );
//                  }
                }

                
                // Preserve the Reminders.
				/*         
				$count = $_ALL_POST['reminder_count'];
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['remind_date_'.$i) && !empty($_ALL_POST['remind_date_'.$i) ) {
                        $_ALL_POST['reminder_list'][] = array(  'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'do_reminder'   => isset($_ALL_POST['remind_date_'.$i])? $_ALL_POST['remind_date_'.$i]:'',
                                                                'description'   => isset($_ALL_POST['remind_text_'.$i])? $_ALL_POST['remind_text_'.$i]:'',
                                                     );
//                    }
                }*/
                
                if( !empty($_ALL_POST['spouse_dob'])){
                    $temp = explode('/', $_ALL_POST['spouse_dob']);
                    $_ALL_POST['spouse_dob'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                }
                
                if( !empty($_ALL_POST['do_reg']) ){
                    $temp = explode('/', $_ALL_POST['do_reg']);
                    $_ALL_POST['do_reg'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                }
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( Clients::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
					 
                    if ( empty($_ALL_POST['country']) ) {
                        $_ALL_POST['country'] = '91';
                    }
                    $_ALL_POST['password'] = '';
                    $_ALL_POST['manager']  = Clients::getManager($db, '', $_ALL_POST['manager']);
					//$_ALL_POST['roles']  = explode(',', $_ALL_POST['roles']);
					
                    if(!empty($_ALL_POST['service_id'])){
                        $_ALL_POST['service_id'] = trim($_ALL_POST['service_id'],",");
                        $_ALL_POST['service_id']    = explode(",", $_ALL_POST['service_id']);
                    }
                    if(!empty($_ALL_POST['authorization_id'])){
                        $_ALL_POST['authorization_id'] = trim($_ALL_POST['authorization_id'],",");
                        $_ALL_POST['authorization_id'] = explode(",", $_ALL_POST['authorization_id']);
                    }
                    $_ALL_POST['industry']    = explode(",", $_ALL_POST['industry']);
					
					if(!empty($_ALL_POST['member_type'])){
                        $_ALL_POST['member_type'] = trim($_ALL_POST['member_type'],",");
                        $_ALL_POST['member_type'] = explode(",", $_ALL_POST['member_type']);
                    }
					// BO: Read the Team Members Information.
					if ( !empty($_ALL_POST['team']) && !is_array($_ALL_POST['team']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						$_ALL_POST['team'] = trim($_ALL_POST['team'],',');
						$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
						$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
						$_ALL_POST['team_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['team_members'],'user_id,number,f_name,l_name', 
						" WHERE user_id IN (". $temp .")");
						$_ALL_POST['team'] = array();
						if(!empty($_ALL_POST['team_members'])){
							foreach ( $_ALL_POST['team_members'] as $key=>$members) {
							//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {

								$_ALL_POST['team'][] = $members['user_id'];
								$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' 
								('. $members['number'] .')';
							}
						}
				    }
                
					// EO: Read the Team Members Information.
					 // BO: Read the Team Members Information.
					if ( !empty($_ALL_POST['alloted_clients']) && !is_array($_ALL_POST['alloted_clients']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
                        $_ALL_POST['alloted_clients'] = trim($_ALL_POST['alloted_clients'],',');
						$_ALL_POST['alloted_clients']          = explode(',', $_ALL_POST['alloted_clients']);
						$temp                       = "'". implode("','", $_ALL_POST['alloted_clients']) ."'";
						$_ALL_POST['allotted_to_clients']  = '';
						$_ALL_POST['client_details']  = array();
						Clients::getList($db, $_ALL_POST['allotted_to_clients'], 'user_id,number,f_name,l_name', 
						" WHERE user_id IN (". $temp .")");
						$_ALL_POST['alloted_clients'] = array();
						foreach ( $_ALL_POST['allotted_to_clients'] as $key1=>$members1) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['alloted_clients'][] = $members1['user_id'];
							$_ALL_POST['client_details'][] = $members1['f_name'] .' '. $members1['l_name'] .' 
							('. $members1['number'] .')';
						}
					}
					
					//premium_email_admin
					//premium_email_billing 
					if ( !empty($_ALL_POST['premium_email_mgmt']) && !is_array($_ALL_POST['premium_email_mgmt']) ) {
						
                        $_ALL_POST['premium_email_mgmt'] = trim($_ALL_POST['premium_email_mgmt'],',');
						$_ALL_POST['premium_email_mgmt']          = explode(',', $_ALL_POST['premium_email_mgmt']);
						$temp                       = "'". implode("','", $_ALL_POST['premium_email_mgmt']) ."'";
						$_ALL_POST['premium_email_mgmt']  = '';
						$_ALL_POST['premium_email_user']  = array();
						Clients::getList($db, $_ALL_POST['premium_email_mgmt1'], 'user_id,number,f_name,l_name', 
						" WHERE user_id IN (". $temp .")");
						$_ALL_POST['premium_email_mgmt'] = array();
						foreach ( $_ALL_POST['premium_email_mgmt1'] as $key1=>$members1) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['premium_email_user'][] = $members1['user_id'];
							$_ALL_POST['premium_email_user_details'][] = $members1['f_name'] .' '. $members1['l_name'] .' 
							('. $members1['number'] .')';
						}
					}
					if ( !empty($_ALL_POST['premium_email_admin']) && !is_array($_ALL_POST['premium_email_admin']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
                        $_ALL_POST['premium_email_admin'] = trim($_ALL_POST['premium_email_admin'],',');
						$_ALL_POST['premium_email_admin']          = explode(',', $_ALL_POST['premium_email_admin']);
						$temp                       = "'". implode("','", $_ALL_POST['premium_email_admin']) ."'";
						$_ALL_POST['premium_email_admin']  = '';
						$_ALL_POST['client_details']  = array();
						Clients::getList($db, $_ALL_POST['premium_email_admin1'], 'user_id,number,f_name,l_name', 
						" WHERE user_id IN (". $temp .")");
						$_ALL_POST['premium_email_admin'] = array();
						foreach ( $_ALL_POST['premium_email_admin1'] as $key1=>$members1) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['premium_email_admin'][] = $members1['user_id'];
							$_ALL_POST['premium_email_admin_details'][] = $members1['f_name'] .' '. $members1['l_name'] .' 
							('. $members1['number'] .')';
						}
					}
					if ( !empty($_ALL_POST['premium_email_billing']) && !is_array($_ALL_POST['premium_email_billing']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
                        $_ALL_POST['premium_email_billing'] = trim($_ALL_POST['premium_email_billing'],',');
						$_ALL_POST['premium_email_billing']          = explode(',', $_ALL_POST['premium_email_billing']);
						$temp                       = "'". implode("','", $_ALL_POST['premium_email_billing']) ."'";
						$_ALL_POST['premium_email_buser']  = '';
						$_ALL_POST['premium_email_buser_details']  = array();
						Clients::getList($db, $_ALL_POST['premium_email_billing1'], 'user_id,number,f_name,l_name', 
						" WHERE user_id IN (". $temp .")");
						$_ALL_POST['premium_email_billing'] = array();
						foreach ( $_ALL_POST['premium_email_billing1'] as $key1=>$members1) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['premium_email_buser'][] = $members1['user_id'];
							$_ALL_POST['premium_email_buser_details'][] = $members1['f_name'] .' '. $members1['l_name'] .' 
							('. $members1['number'] .')';
						}
					}
                    //Uncomment me after editing clients list
                   /*  if(!empty($_ALL_POST['username'])){
                        if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{5,80}$/', $_ALL_POST['username']) ) {
                            $_ALL_POST['username']='smeerp';
                        }  
                    } */
                    
					// EO: Read the Team Members Information.
                    // Check the Access Level.
                    
					// Read the Contact Numbers.
					$phone->setPhoneOf(TABLE_CLIENTS, $user_id);
					$_ALL_POST['phone'] = $phone->get($db);
					
					// Read the Addresses.
					$region->setAddressOf(TABLE_CLIENTS, $user_id);
					$_ALL_POST['address_list'] = $region->get();
					
					// Read the Reminders.
					$reminder->setReminderOf(TABLE_CLIENTS, $user_id);
					$_ALL_POST['reminder_list1'] = $reminder->get($db);
					if(!empty($_ALL_POST['reminder_list1'])){
						foreach($_ALL_POST['reminder_list1'] as $key1=>$val1){
							$_ALL_POST['remind_id'][]= $val1['id'];
							$_ALL_POST['subject'][]= $val1['subject'];
							$_ALL_POST['description'][]= $val1['description'];
							$data['do_reminder'][]=$val1['do_reminder'];
							/* $temp1 = explode(" ",$val1['do_reminder']);
							$temp  = explode("-",$temp1[0]);
							$data['do_reminder'][]= $temp[2]."/".$temp[1]."/".$temp[0]; */
						}
					}                    
                }
            }
 
            if ( !empty($_ALL_POST['user_id']) ) {
	
                // Check the Access Level.
                //if ( $_ALL_POST['access_level'] < $access_level ) {

                    $phone_types    = $phone->getTypeList();
                    $address_types  = $region->getTypeList();
                    $lst_country    = $region->getCountryList();
                    $lst_state      = $region->getStateList();
                    $lst_city       = $region->getCityList();
                    $region  =	$phone =	$reminder =	null;
					$_ALL_POST['reminder_list']=array(0=>'',1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'');
					$rType = Clients::getRTypeArray();
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    //print_r($_ALL_POST);
              
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    $hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
					$page["var"][] = array('variable' => 'memberTypeList', 'value' => 'memberTypeList');
                    $page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
					$page["var"][] = array('variable' => 'rType', 'value' => 'rType');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'data', 'value' => 'data');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
                    $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
                    $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
                    $page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
					$page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
                    $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
                    $page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-edit.html');
               
			    /*
			    }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Client with the current Access Level.");
                } */
            }
            else {
                $messages->setErrorMessage("The Selected Client was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>