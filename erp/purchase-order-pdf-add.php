<?php
    if ( $perm->has('nc_pop_add') ) {
         
       include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
       include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php');
	   include_once ( DIR_FS_INCLUDES .'/purchase-order-pdf.inc.php');
        
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $exchange_rate          = isset($_POST["exchange_rate"]) ? $_POST["exchange_rate"] : '1' ;
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level']+1;
       
        $_ALL_POST['exchange_rate'] = $exchange_rate; 
        
        $extra = array( 'db' 				=> &$db,
                        'access_level'      => $access_level,
                        'messages'          => &$messages
                        );
                        
        $currency = NULL ;
        $required_fields = '*' ;
		Currency::getList($db,$currency,$required_fields);        
        
		//Get company list 
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                   .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
		
      
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn']) || isset($_POST['btnInvReturn']) || 
		isset($_POST['btnInvFlwReturn']) ) && $_POST['act'] == 'save') {			
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,                        
                            'messages'          => &$messages
                        );
            $data['do_c'] = time();
            if(PurchaseOrderPdf::validateAdd($data, $extra)){
                //get currency abbr from d/b bof
                $mail_client = 0;
                if(isset($data['mail_client'])){
                    $mail_client = 1;
                }
                $currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                $fields1 = "*";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);              
                if(!empty($currency1)){
                   $data['currency_abbr'] = $currency1[0]['abbr'];
                   $data['currency_name'] = $currency1[0]['currency_name'];
                   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
                   $data['currency_country'] = $currency1[0]['country_name'];
                }
                //get currency abbr from d/b eof                      
                $company1 =null;
                $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                $fields_c1 = "name,prefix,quot_prefix, 
				tin_no,service_tax_regn,pan_no,cst_no,vat_no,do_implement,do_implement_st";
                Company::getList( $db, $company1, $fields_c1, $condition_query_c1); 
				$data['do_tax']=$data['do_st']=0;
				$do_tax=$do_st='';
                if(!empty($company1)){                  
                   $data['company_name'] = $company1[0]['name'];
                   $data['company_prefix'] = $company1[0]['prefix'];
                   $data['company_quot_prefix'] = $company1[0]['quot_prefix'];
				   $data['do_tax']     = $company1[0]['do_implement'];	
				   $data['do_st']     = $company1[0]['do_implement_st'];	
				   
				   $data['tin_no'] 			 = '';
				   $data['service_tax_regn'] 	 = '';
				   $data['pan_no'] 			 = $company1[0]['pan_no'];
				   $data['cst_no'] 			 = '';
				   $data['vat_no'] 			 = '';
				   
				    if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
						$do_tax=$data['do_tax'];
						$data['do_tax']= strtotime($data['do_tax']);				
					}					
					if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){
					   $data['tin_no'] 			 = $company1[0]['tin_no'];
					   $data['cst_no'] 			 = $company1[0]['cst_no'];
					   $data['vat_no'] 			 = $company1[0]['vat_no'];
					}
					if( $data['do_st']!=0 &&  $data['do_st']!='0000:00:00 00:00:00'){
						$do_st = $data['do_st'];
						$data['do_st'] = strtotime($data['do_st']);				
					}
					if($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00'){
					   $data['service_tax_regn'] = $company1[0]['service_tax_regn'];
					}
					
                }
                $data['invoice_title'] = 'Purchase Order';
                $data['profm']='Proforma';				 
                //Get billing address BOF
                 include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS,  $data['client']['user_id']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $address_list  =  $address_list;
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
                //$data['b_address'] = $address_list ;
                //Get billing address EOF
				$show_period=$data['show_period_chk']=0;
				if(isset($data['show_period'])){
					$data['show_period_chk'] = 1;
				}
                

                if(!empty($data['do_e'])){
                    $data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                    $data['is_renewable']=1;
                }else{
                    $data['do_e'] ='';
                    $data['is_renewable']=0;
                }
                if(!empty($data['do_fe'])){
                    $data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }else{
                    $data['do_fe'] ='';
                }
				              
                $sub_str='';
				
                $query	= " INSERT INTO ".TABLE_PURCHASE_ORDER_PDF
			." SET ". TABLE_PURCHASE_ORDER_PDF .".number = '".        $data['number'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".inv_counter = '".         $data['inv_counter'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".show_period = '". $data['show_period_chk']."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".po_terms_condition = '".$data['po_terms_condition']."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".or_id = '".         $data['or_id'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".or_no = '".         $data['or_no'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".service_id = '". ",".trim($data['service_id'],",").","."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".tax_ids = '".         $data['tax_ids'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".delivery_at = '".   $data['delivery_at'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".access_level = '".  $data['access_level'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".created_by = '".     $my['user_id'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".client = '".        $data['client']['user_id'] ."'"            	
			.",". TABLE_PURCHASE_ORDER_PDF .".currency_abbr = '". processUserData($data['currency_abbr']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".currency_id = '".   $data['currency_id'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".currency_name = '".   processUserData($data['currency_name']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".currency_symbol = '". processUserData($data['currency_symbol']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".currency_country ='". processUserData($data['currency_country'])."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".exchange_rate = '". $data['exchange_rate'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".amount = '".        $data['amount'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".round_off = '".     $data['round_off'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".round_off_op = '".  $data['round_off_op'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".amount_inr = '".    $data['amount_inr'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".amount_words = '".  $data['amount_words'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".do_c = '".          date('Y-m-d H:i:s', $data['do_c']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".do_e = '".          $data['do_e'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".do_fe = '".         $data['do_fe']."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".remarks = '".       $data['remarks'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".company_id = '".    $data['company_id'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".company_name = '". processUserData($data['company_name']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".tin_no   		   = '". $data['tin_no'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".service_tax_regn   = '". $data['service_tax_regn'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".do_st   		  = '". $do_st ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".pan_no  		   = '". $data['pan_no'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".cst_no  		   = '". $data['cst_no'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".vat_no  		   = '". $data['vat_no'] ."'"						
			.",". TABLE_PURCHASE_ORDER_PDF .".do_tax   		  = '". $do_tax ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".company_prefix = '".processUserData($data['company_prefix']) ."'"
			.",".TABLE_PURCHASE_ORDER_PDF .".company_quot_prefix= '".processUserData($data['company_quot_prefix'])."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".billing_name      = '".processUserData($data['billing_name']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".billing_address     = '".processUserData($data['billing_address']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".b_addr_company_name 
			= '".processUserData($data['b_addr_company_name']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".b_addr              = '".processUserData($data['b_addr'])."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".b_addr_city         = '".processUserData($data['b_addr_city'] )."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".b_addr_state        = '".processUserData($data['b_addr_state']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".b_addr_country      = '".processUserData($data['b_addr_country']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".b_addr_zip          = '".processUserData($data['b_addr_zip']) ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".is_renewable        = '". $data['is_renewable'] ."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".ip                  = '".$_SERVER['REMOTE_ADDR']."'"
			.",". TABLE_PURCHASE_ORDER_PDF .".status              = '".$data['status'] ."'" ;

                     
						
                $data['amount']= number_format($data['amount'],2);
                $data['amount_inr']= number_format($data['amount_inr'],2);
                //$data['balance']= number_format($data['balance'],2);
                $data['amount_words']=processSqlData($data['amount_words']);
                $data['po_terms_condition']=processSqlData($data['po_terms_condition']);
                $data['do_i_chk'] = $data['do_i'];			 
			    $data['do_rs_symbol'] = strtotime('2010-07-20');
			    $data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
			    $data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
					
                    $variables['hid'] = $db->last_inserted_id(); 
					
                    //Update in order, proforma is_pdf_create =1                       
                    $query1 = "UPDATE ".TABLE_PURCHASE_ORDER." SET 
					".TABLE_PURCHASE_ORDER.".is_pdf_create='1', 
					".TABLE_PURCHASE_ORDER.".status='".PurchaseOrder::COMPLETED."',
					".TABLE_PURCHASE_ORDER.".popdf_no='".$data['number']."'					
					WHERE ".TABLE_PURCHASE_ORDER.".number = '".$data['or_no']."'";
                    $db->query($query1);     
					
                    $messages->setOkMessage("New Purchase Order PDF has been created."); 
					$data['sub_total_amount'] = 0;
					$data['tax1_total_amount'] = $data['tax1_sub1_total_amount'] = $data['tax1_sub2_total_amount']= 0;
					
                    $temp = NULL;
                    $temp_p = NULL; 
                    $condition_query = "WHERE ".TABLE_PURCHASE_ORDER_P.".bill_id = '".$data['or_id']."'";
                    $data['show_discount']=0;
                    $data['show_nos']=0;
                    $data['colsSpanNo1']=5;
                    $data['colsSpanNo2']=6;
                    PurchaseOrder::getParticulars($db, $temp, '*', $condition_query);
                    if(!empty($temp)){                    
                        /*
						$data['wd1']=10;
                        $data['wdinvp']=217;
                        $data['wdpri']=70;
                        $data['wdnos']=70;
                        $data['wdamt']=70;
                        $data['wddisc']=50;
                        $data['wdst']=70;
                        //$data['wdtx']=40; as three columns are removed so manage its width in other cols
                       // $data['wdtxp']=50;
                        //$data['wdtotam']=75;
                        $data['wd2']=9;*/
						
						$data['wd1']=10;
                        $data['wdinvp']=245;//217+28
                        $data['wdpri']=98;//70+28
                        $data['wdnos']=98;//70+28
                        $data['wdamt']=97;//70+27
                        $data['wddisc']=77; //50+27
                        $data['wdst']=97;   //70+27                     
                        $data['wd2']=9;
                        
                        foreach ( $temp as $pKey => $parti ) {                        
                            /*
                               if($temp[$pKey]['s_type']=='1'){
                                    $temp[$pKey]['s_type']='Years';
                               }elseif($temp[$pKey]['s_type']=='2'){
                                    $temp[$pKey]['s_type']='Quantity';
                               }
                            */     
                            if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
                                $data['show_discount'] = 1; 
                            }
                            if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
                                $data['show_nos']=1;     
                            }                            
                            $temp_p[]=array(
									'p_id'=>$temp[$pKey]['id'],
									'particulars'=>$temp[$pKey]['particulars'],
									'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
									's_type' =>$temp[$pKey]['s_type'] ,                                   
									's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
									's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
									's_id' =>$temp[$pKey]['s_id'] ,                                   
									'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
									'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
									'tax1_name' =>$temp[$pKey]['tax1_name'] ,    
									'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                                     
									'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
									'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,     
									'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2),									
									'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,    
									'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                                     
									'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
									'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,     
									'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,									
									'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,    
									'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                                     
									'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
									'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,     
									'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,									
									'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
									'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
									'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
									'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
									'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                                );
								
							if(($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00')
								|| ($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00')
							){
								//if(!empty($temp[$pKey]['tax1_id'])){
									$data['sub_total_amount'] = $data['sub_total_amount'] + 
									$temp[$pKey]['stot_amount'] ; 
									
									$data['tax1_id'] =   $temp[$pKey]['tax1_id'] ; 
									$data['tax1_number'] =   $temp[$pKey]['tax1_number'] ; 
									$data['tax1_name'] =   $temp[$pKey]['tax1_name'] ; 
									$data['tax1_value'] =   $temp[$pKey]['tax1_value'] ; 
									$data['tax1_pvalue'] =   $temp[$pKey]['tax1_pvalue'] ; 
									$data['tax1_total_amount'] =  $data['tax1_total_amount'] + 
									$temp[$pKey]['tax1_amount'] ; 
									
									$data['tax1_sub1_id'] =   $temp[$pKey]['tax1_sub1_id'] ; 
									$data['tax1_sub1_number'] =   $temp[$pKey]['tax1_sub1_number'] ; 
									$data['tax1_sub1_name'] =   $temp[$pKey]['tax1_sub1_name'] ; 
									$data['tax1_sub1_value'] =   $temp[$pKey]['tax1_sub1_value'] ; 
									$data['tax1_sub1_pvalue'] =   $temp[$pKey]['tax1_sub1_pvalue'] ; 
									$data['tax1_sub1_total_amount'] =  $data['tax1_sub1_total_amount'] + 									$temp[$pKey]['tax1_sub1_amount'] ; 
									
									$data['tax1_sub2_id'] =   $temp[$pKey]['tax1_sub2_id'] ; 
									$data['tax1_sub2_number'] =   $temp[$pKey]['tax1_sub2_number'] ; 
									$data['tax1_sub2_name'] =   $temp[$pKey]['tax1_sub2_name'] ; 
									$data['tax1_sub2_value'] =   $temp[$pKey]['tax1_sub2_value'] ; 
									$data['tax1_sub2_pvalue'] =   $temp[$pKey]['tax1_sub2_pvalue'] ; 
									$data['tax1_sub2_total_amount'] =  $data['tax1_sub2_total_amount'] + 
									$temp[$pKey]['tax1_sub2_amount'] ; 
								//}
							}							
                        }
						
                        if($data['show_discount'] ==0){ 
                            // disc : 50 & subtot : 70
                            /* 
						    $data['wdinvp']= $data['wdinvp']+20 + 40;
                            $data['wdpri'] = $data['wdpri'] + 5 + 6;
                            $data['wdamt'] = $data['wdamt'] + 5+ 6;                            
                            $data['wdtx']  = $data['wdtx'] + 5+ 6;
                            $data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            $data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							*/
							// disc : 77 & subtot : 97
							$data['wdinvp']= $data['wdinvp']+50 + 70;
                            $data['wdpri'] = $data['wdpri'] +10 + 10;
                            $data['wdamt'] = $data['wdamt'] + 17+ 17;                            
                            //$data['wdtx']  = $data['wdtx'] + 5+ 6;
                            //$data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            //$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
                            
                        }
                        if($data['show_nos'] ==0){
                            // disc : 70
							/* 
						    $data['wdinvp']= $data['wdinvp']+30;
                            $data['wdpri'] = $data['wdpri'] + 5;
                            $data['wdamt'] = $data['wdamt'] + 5;
                            $data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;
							*/
						    // disc :98
							$data['wdinvp']= $data['wdinvp']+80;
                            $data['wdpri'] = $data['wdpri'] + 8;
                            $data['wdamt'] = $data['wdamt'] + 10;
                            /*
							$data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;*/
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;
                        }                        
                    }
					$sub_tax_str='';
                    if(!empty($data['tax1_name'])){
					
						//get the tax's declaration bof
						$condition1=" WHERE id= ".$data['tax1_id']." LIMIT 0,1" ;
						$required_fields='declaration';
						$parentTaxDetails=array();
						$data['tax1_declaration']='';
						ServiceTax::getDetails( $db, $parentTaxDetails, $required_fields, $condition1);
						if(!empty($parentTaxDetails)){
							$arr = $parentTaxDetails[0];
							$data['tax1_declaration'] = $arr['declaration'];
						}
						
						//get the tax's declaration eof
					
					
						$sqlu  = " UPDATE ".TABLE_PURCHASE_ORDER_PDF
                        ." SET "
						."". TABLE_PURCHASE_ORDER_PDF .".sub_total_amount = '".$data['sub_total_amount'] ."'"                        .",". TABLE_PURCHASE_ORDER_PDF .".tax1_id = '". $data['tax1_id'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_number = '". $data['tax1_number'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_name = '". $data['tax1_name'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_declaration = '". $data['tax1_declaration'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_value = '". $data['tax1_value'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_pvalue = '". $data['tax1_pvalue'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_total_amount = '". $data['tax1_total_amount'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub1_id = '". $data['tax1_sub1_id'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub1_number = '". $data['tax1_sub1_number'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub1_name = '". $data['tax1_sub1_name'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub1_value = '". $data['tax1_sub1_value'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub1_pvalue = '". $data['tax1_sub1_pvalue'] ."'"
					   .",". TABLE_PURCHASE_ORDER_PDF.".tax1_sub1_total_amount='".$data['tax1_sub1_total_amount'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub2_id = '". $data['tax1_sub2_id'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub2_number = '". $data['tax1_sub2_number'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub2_name = '". $data['tax1_sub2_name'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub2_value = '". $data['tax1_sub2_value'] ."'"
						.",". TABLE_PURCHASE_ORDER_PDF .".tax1_sub2_pvalue = '". $data['tax1_sub2_pvalue'] ."'"
					    .",". TABLE_PURCHASE_ORDER_PDF.".tax1_sub2_total_amount='".$data['tax1_sub2_total_amount']."'"
						." WHERE ".TABLE_PURCHASE_ORDER_PDF.".id ='".$variables['hid'] ."'";	
							
						$db->query($sqlu);
						
						$data['sub_total_amount']=number_format($data['sub_total_amount'],2);
						$data['tax1_total_amount']=number_format($data['tax1_total_amount'],2);
						$data['tax1_sub1_total_amount']=number_format($data['tax1_sub1_total_amount'],2);
						$data['tax1_sub2_total_amount']=number_format($data['tax1_sub2_total_amount'],2);
					}
					
                    $data['particulars'] = $temp_p ;                    
                    // Update the Order. for time being                                    
                    $or_id = NULL;                    
                    // Create the Invoice PDF, HTML in file.
					$extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = ''; 
					//*comment it as not worked in LOCAL
					
					PurchaseOrderPdf::createPdfFile($data);					 
                    $file_name = DIR_FS_PO_FILES."/". $data["number"] .".pdf";
					
                    // Send the notification mails to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    
					//CREATE TAX INVOICE BOF
                                        
                
                    $data['link']   = DIR_WS_MP .'/purchase-order-pdf.php?perform=view&inv_id='. $variables['hid'];
                    // Read the Client Manager information.
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, 
					f_name, l_name, email');
                    // Send Email to the Client.
                    /*  
				    if( isset($mail_client) &&  $mail_client==1 ){
                        $email = NULL;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'POP_CREATE_TO_CLIENT', $data, $email) ) {
                            $to     = '';
                            
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 
							'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);    
							echo $email["body"];
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
							
                        }
                    } */
                    
                    //Send mail after create invoice to admin bof
                    $data['link']   = DIR_WS_NC .'/purchase-order-pdf.php?perform=view&inv_id='. $variables['hid'];    
                    $email = NULL;
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'POP_CREATE_TO_ADMIN', $data, $email) ) {
                        $to     = '';
                       
                        $to[]   = array('name' => $bill_inv_name , 'email' => $bill_inv_email);                        
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,
						$cc,$bcc,$file_name);
                    }
				
                    //Send mail after create invoice to admin eof
                    
                    
                    
                    
                    $_ALL_POST  = NULL;
                    
                    $inv_no =$data['number'];
                    $data       = NULL;
                }
            }
            
        }
        else {
            // Set up the default values to be displayed.
            // $_ALL_POST['number'] = "PT". date("Y") ."-INV-". date("mdhi-s") ;
            //As invoice-number formate changed on the basis of ( fanancial yr + sr no )           
        }
    
       
       
        
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        
        
		if ( !empty($or_id) ) {
			// Read the Order details.
			include_once ( DIR_FS_INCLUDES .'/purchase-order.inc.php');
			$order  = NULL ;              
			
			$fields = TABLE_PURCHASE_ORDER.'.id,'.TABLE_PURCHASE_ORDER .'.bill_dt AS do_o,
			'.TABLE_PURCHASE_ORDER .'.number AS or_no,'. TABLE_PURCHASE_ORDER .'.access_level, 
			'. TABLE_PURCHASE_ORDER .'.status ';
			//$fields .= ', '.TABLE_PURCHASE_ORDER.'.details';
			$fields .= ', '.TABLE_PURCHASE_ORDER.'.order_title ';
			//$fields .= ', '.TABLE_PURCHASE_ORDER.'.do_e, '.TABLE_PURCHASE_ORDER .'.do_fe';
			$fields .= ', '.TABLE_PURCHASE_ORDER.'.currency_id, '. TABLE_PURCHASE_ORDER .'.company_id ,
			'.TABLE_PURCHASE_ORDER .'.bill_amount as amount, '. TABLE_PURCHASE_ORDER .'.round_off_op,'
			.TABLE_PURCHASE_ORDER .'.round_off';
			$fields .= ', '.TABLE_CLIENTS.'.user_id, '. TABLE_CLIENTS .'.number, '.TABLE_CLIENTS.'.f_name, 
			'.TABLE_CLIENTS.'.l_name';
			$fields .= ', '.TABLE_CLIENTS.'.billing_name' ; 
			//$fields .= ', '.TABLE_PURCHASE_ORDER.'.delivery_at' ;
			$fields .= ', '.TABLE_PURCHASE_ORDER.'.tax_ids' ;
			$fields .= ', '.TABLE_PURCHASE_ORDER.'.service_id' ;
			//$fields .= ', '.TABLE_PURCHASE_ORDER.'.special' ;
			$fields .= ', '.TABLE_PURCHASE_ORDER.'.is_pdf_create' ;
			$fields .= ', '.TABLE_PURCHASE_ORDER.'.party_id as client, '. TABLE_USER .'.number AS u_number, 
			'.TABLE_USER.'.f_name AS u_f_name, '.TABLE_USER.'.l_name AS u_l_name ';
			
			if ( PurchaseOrder::getList($db, $order, $fields, " 
				WHERE ".TABLE_PURCHASE_ORDER.".id = '". $or_id ."' ") > 0 ) {
				$order = $order[0];
				
				if ( !$order['is_pdf_create'] ){
				
				    if($order['status'] == PurchaseOrder::ACTIVE) {
						if ( $access_level > $order['access_level'] ) {
						
							$_ALL_POST['or_no'] = $order['or_no'];
							$_ALL_POST['company_id'] = $order['company_id'];
							$_ALL_POST['special'] = $order['special'];
							$_ALL_POST['delivery_at']  = $order['delivery_at'];
							$_ALL_POST['or_details'] = $order['details'];
							$_ALL_POST['or_user_id'] = $order['user_id'];
							$_ALL_POST['or_number']  = $order['number'];
							$_ALL_POST['or_f_name']   = $order['f_name'];
							$_ALL_POST['or_l_name']   = $order['l_name'];
							$_ALL_POST['billing_name']   = $order['billing_name'];
							$_ALL_POST['or_access_level']   = $order['access_level'];
							 $_ALL_POST['do_o']     = $order['do_o'];
							
							$_ALL_POST['u_f_name']          = $order['u_f_name'];
							$_ALL_POST['u_l_name']          = $order['u_l_name'];
							$_ALL_POST['u_number']          = $order['u_number'];
							$_ALL_POST['order_title']       = $order['order_title'];
							$_ALL_POST['order_closed_by']   = $order['order_closed_by'];
							$_ALL_POST['do_e']   = $order['do_e'];
							$_ALL_POST['do_fe']   = $order['do_fe'];
							$_ALL_POST['tax_ids']   = $order['tax_ids'];
							$_ALL_POST['service_id']   = $order['service_id'];
						  
							if($_ALL_POST['do_e'] !='0000-00-00 00:00:00'){
								$_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
								$temp               = explode('-', $_ALL_POST['do_e'][0]);
								$_ALL_POST['do_e']  = NULL;
								$_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
							}else{                            
								$_ALL_POST['do_e']  ='';
							}
							
							if($_ALL_POST['do_fe'] !='0000-00-00 00:00:00'){
								$_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
								$temp               = explode('-', $_ALL_POST['do_fe'][0]);
								$_ALL_POST['do_fe']  = NULL;
								$_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
							}else{                            
								$_ALL_POST['do_fe']  ='';
							}
							if($_ALL_POST['do_o'] !='0000-00-00 00:00:00'){
								$_ALL_POST['do_i']  = explode(' ', $_ALL_POST['do_o']);
								$temp               = explode('-', $_ALL_POST['do_i'][0]);
								$_ALL_POST['do_i']  = NULL;
								$_ALL_POST['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
							}
							$_ALL_POST['show_period']  =0;	
							if(!empty($_ALL_POST['do_e']) && !empty($_ALL_POST['do_fe'])){
								$_ALL_POST['show_period']  =1;
							}
							
							/* 
							if(!empty($order['order_closed_by'])){
								$clientfields='';
								$condition = TABLE_USER." WHERE  user_id='".$order['order_closed_by']."'";
								$clientfields .= ' '.TABLE_USER.'.user_id,
								'. TABLE_USER .'.number, '.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
								User::getList($db, $closed_by, $clientfields, $condition) ;
								$_ALL_POST['order_closed_by_name']   = $closed_by['0'];
							} */
													
							
							$_ALL_POST['round_off_op']= $order['round_off_op'];
							$_ALL_POST['round_off']= $order['round_off'];
							$_ALL_POST['amount']= $order['amount'];
							$_ALL_POST['gramount']= number_format($order['amount'],2);
							$_ALL_POST['amount_inr']= $order['amount'] * $_ALL_POST['exchange_rate'];
							$_ALL_POST['currency_id']= $order['currency_id'];
							//$_ALL_POST['company_id']= $order['company_id'];
						   
							/*get particulars details bof*/
							$temp = NULL;
							$temp_p = NULL;
							$condition_query = " WHERE bill_id  = '". $order['id'] ."' ";
							PurchaseOrder::getParticulars($db, $temp, '*', $condition_query);
							if(!empty($temp)){
								foreach ( $temp as $pKey => $parti ) {
								 
									$temp_p[]=array(
									'p_id'=>$temp[$pKey]['id'],
									'particulars'=>$temp[$pKey]['particulars'],
									'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,      
									's_type' =>$temp[$pKey]['s_type'] ,                                   
									's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
									's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,
									's_id' =>$temp[$pKey]['s_id'] ,                                   
									'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
									'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                    'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
									'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
									'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
									'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
									'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) , 
									'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,
									'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,
									'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,
									'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,
									'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) , 
									'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,
									'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,
									'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,
									'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,
									'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,                                    'd_amount' =>number_format($temp[$pKey]['d_amount'],2),                                    'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                    'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                    'discount_type' =>$temp[$pKey]['discount_type'] ,                                    'pp_amount' =>number_format($temp[$pKey]['pp_amount'] ,2)                                   );
								}
							}
							$_ALL_POST['particulars']=$temp_p ;
							
							/*get particulars details eof*/
							$order = NULL;
							// Read the Client Addresses.
							include_once ( DIR_FS_CLASS .'/Region.class.php');
							$region         = new Region();
							$region->setAddressOf(TABLE_CLIENTS, $_ALL_POST['or_user_id']);
							$_ALL_POST['address_list']  = $region->get();
						  
							if ( (!isset($_POST['btnCreate']) && !isset($_POST['btnReturn'])) ) {
								$_ALL_POST['billing_name']  = $_ALL_POST['billing_name'] ;
							 
							}

							$al_list = getAccessLevel($db, $access_level);
							$index = array();
							if ( isset($_ALL_POST['or_access_level']) ) {
								array_key_search('access_level', $_ALL_POST['or_access_level'], $al_list, $index);
								$_ALL_POST['or_access_level'] = $al_list[$index[0]]['title'];
							}
							
							 
			
							$hidden[] = array('name'=> 'or_id' ,'value' => $or_id);
							$hidden[] = array('name'=> 'special' ,'value' => $_ALL_POST['special']);
							$hidden[] = array('name'=> 'or_no' ,'value' => $_ALL_POST['or_no']);
							$hidden[] = array('name'=> 'tax_ids' ,'value' => $_ALL_POST['tax_ids']);
							$hidden[] = array('name'=> 'service_id' ,'value' => $_ALL_POST['service_id']);
							$hidden[] = array('name'=> 'delivery_at' ,'value' => $_ALL_POST['delivery_at']);
							$hidden[] = array('name'=> 'client' ,'value' => $_ALL_POST['or_user_id']);
							$hidden[] = array('name'=> 'round_off' ,'value' => $_ALL_POST['round_off']);
							$hidden[] = array('name'=> 'round_off_op' ,'value' => $_ALL_POST['round_off_op']);
							//$hidden[] = array('name'=> 'company_id' ,'value' => $_ALL_POST['company_id']);
							
							$hidden[] = array('name'=> 'perform' ,'value' => 'add');
							$hidden[] = array('name'=> 'act' , 'value' => 'save');
							$hidden[] = array('name'=> 'ajx','value' => $ajx);
				
							$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
							$page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
							$page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
							$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
							$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
						 
					 
							$page["var"][] = array('variable' => 'currency', 'value' => 'currency');
							
							$page["section"][] = array('container'=>'CONTENT_MAIN', 
								'page' => 'purchase-order-pdf-add.html');
							
						}
						else {
							$messages->setErrorMessage("You donot have the Right to create PO PDF for 
							this PO.");
						    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');				
							$_ALL_POST = '';
						}
					}
					else {
						$messages->setErrorMessage("The Purchse Order is not yet approved.");
					    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
				
				
						$_ALL_POST = '';
					}
				}else{
					$messages->setErrorMessage("The Purchse Order PDF is created for this Purchase Order.");
					$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');				
				}
			}else {
				$messages->setErrorMessage("The Selected Purchase Order was not found.");
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');				
			}
		}
       
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
	    $page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order.html');
    }
?>