<?php
    if ( $perm->has('nc_bl_inv_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        
        $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $or_no          = isset($_GET["or_no"]) ? $_GET["or_no"] : ( isset($_POST["or_no"]) ? $_POST["or_no"] : '' );

        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_inv_edit_al') ) {
            $access_level += 1;
        }
        
        $currency = NULL;
        $required_fields ='*';
        Currency::getList($db,$currency,$required_fields);
        
        //show invoice bof
         // Read the Invoice which is to be Updated.
        $fields = TABLE_BILL_INV .'.*'
                    .','. TABLE_BILL_ORDERS .'.old_particulars'
                    .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                    .','. TABLE_CLIENTS .'.number AS c_number'
                    .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                    .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                    .','. TABLE_CLIENTS .'.email AS c_email'
                    .','. TABLE_CLIENTS .'.status AS c_status' ;
        
        $condition_query = " WHERE (". TABLE_BILL_INV .".id = '". $inv_id ."' "
                            ." OR ". TABLE_BILL_INV .".number = '". $inv_id ."')";

        $condition_query .= " AND ( ";

        // If my has created this Invoice.
        $condition_query .= " (". TABLE_BILL_INV .".created_by = '". $my['user_id'] ."' "
                            ." AND ". TABLE_BILL_INV .".access_level < $access_level ) ";
        
        // If my is the Client Manager (Remove if Client manager cannot edit the Order)
        $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                            ." AND ". TABLE_BILL_INV .".access_level < $access_level ) ";
        
        // Check if the User has the Right to Edit Invoices created by other Users.
        if ( $perm->has('nc_bl_inv_edit_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_bl_inv_edit_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_BILL_INV. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_INV .".access_level < $access_level_o ) ";
        }
        $condition_query .= " )";
        
        if ( Invoice::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];
            
           
            
            if ( $_ALL_POST['access_level'] < $access_level ) {
           
                // Set up the Client Details field.
                $_ALL_POST['client_details']= $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
                                                .' ('. $_ALL_POST['c_number'] .')'
                                                .' ('. $_ALL_POST['c_email'] .')';
                
                // Set up the dates.
                $_ALL_POST['do_i']  = explode(' ', $_ALL_POST['do_i']);
                $temp               = explode('-', $_ALL_POST['do_i'][0]);
                $_ALL_POST['do_i']  = NULL;
                $_ALL_POST['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                
                $_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
                $temp               = explode('-', $_ALL_POST['do_d'][0]);
                $_ALL_POST['do_d']  = NULL;
                $_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                
                if($_ALL_POST['do_e'] !='0000-00-00 00:00:00'){
                    $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                    $temp               = explode('-', $_ALL_POST['do_e'][0]);
                    $_ALL_POST['do_e']  = NULL;
                    $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                }else{
                    $_ALL_POST['do_e']  ='';
                }
                
                if($_ALL_POST['do_fe'] !='0000-00-00 00:00:00'){
                    $_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
                    $temp               = explode('-', $_ALL_POST['do_fe'][0]);
                    $_ALL_POST['do_fe']  = NULL;
                    $_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                }else{
                
                    $_ALL_POST['do_fe']  ='';
                }
				$show_period_chk = $_ALL_POST['show_period'];
                // Set up the Services.
                //$_ALL_POST['service_id'] = explode(',', $_ALL_POST['service_id']);                  
                $condition_query = '';
               
                
               
                $or_no              = $_ALL_POST['or_no'];
            }
            else {
          
                $messages->setErrorMessage("You do not have the Permission to edit the Invoice with the current Access Level.");
            }
        }
        else {
           
            $messages->setErrorMessage("The Invoice was not found or you do not have the Permission to access this Invoice.");
        }
        //show invoice eof
        
      
      
      
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])  || isset($_POST['btnReturnRcp']) ) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);

            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                           // 'lst_service'       => $lst_service,
                            'messages'          => &$messages
                        );

            if ( Invoice::validateUpdate($data, $extra) ) {
                
                $invDetails =null;
                $condition_query1 = " WHERE ".TABLE_BILL_INV.".number ='".$data['number']."'";
                $fields1 = TABLE_BILL_INV.".currency_abbr,".TABLE_BILL_INV.".currency_name,
				".TABLE_BILL_INV.".currency_symbol,".TABLE_BILL_INV.".currency_country,
				".TABLE_BILL_INV.".client,
				".TABLE_BILL_INV.".round_off_op,
				".TABLE_BILL_INV.".round_off ,
				".TABLE_BILL_INV.".company_quot_prefix,
				".TABLE_BILL_INV.".company_name,
				".TABLE_BILL_INV.".b_addr_company_name,".TABLE_BILL_INV.".b_addr,
				".TABLE_BILL_INV.".b_addr_city,".TABLE_BILL_INV.".b_addr_state,
				".TABLE_BILL_INV.".b_addr_country,".TABLE_BILL_INV.".b_addr_zip, 
				".TABLE_BILL_INV.".tin_no, 
				".TABLE_BILL_INV.".cst_no,".TABLE_BILL_INV.".vat_no,".TABLE_BILL_INV.".do_tax,
				".TABLE_BILL_INV.".service_tax_regn,".TABLE_BILL_INV.".pan_no,
				".TABLE_BILL_INV.".delivery_at,
				".TABLE_BILL_INV.".tax1_declaration,
				".TABLE_BILL_INV.".sub_total_amount,".TABLE_BILL_INV.".tax1_id,
				".TABLE_BILL_INV.".tax1_number,".TABLE_BILL_INV.".tax1_name,
				".TABLE_BILL_INV.".tax1_value,".TABLE_BILL_INV.".tax1_pvalue,
				".TABLE_BILL_INV.".tax1_total_amount,
				".TABLE_BILL_INV.".tax1_sub1_id,".TABLE_BILL_INV.".tax1_sub1_number,
				".TABLE_BILL_INV.".tax1_sub1_name,".TABLE_BILL_INV.".tax1_sub1_value,
				".TABLE_BILL_INV.".tax1_sub1_pvalue,".TABLE_BILL_INV.".tax1_sub1_total_amount,				
				".TABLE_BILL_INV.".tax1_sub2_id,".TABLE_BILL_INV.".tax1_sub2_number,
				".TABLE_BILL_INV.".tax1_sub2_name,".TABLE_BILL_INV.".tax1_sub2_value,
				".TABLE_BILL_INV.".tax1_sub2_pvalue,".TABLE_BILL_INV.".tax1_sub2_total_amount	";
				
                Invoice::getList( $db, $invDetails, $fields1, $condition_query1);
                $data['currency_abbr'] = '';
                $data['currency_name'] = '';
                $data['currency_symbol'] ='';
                $data['currency_country'] ='';
                $data['b_addr_company_name'] ='';
                $data['b_addr'] ='';
                $data['b_addr_city'] ='';
                $data['b_addr_state'] ='';
                $data['b_addr_country'] ='';
                $data['b_addr_zip'] ='';
                
                if(!empty($invDetails)){
                   $invDetails=$invDetails[0];
				   $data['client'] = $invDetails['client'];
				   $data['currency_abbr'] = $invDetails['currency_abbr'];
                   $data['currency_name'] = $invDetails['currency_name'];
                   $data['currency_symbol'] = $invDetails['currency_symbol'];
                   $data['currency_country'] = $invDetails['currency_country'];
				   $data['round_off_op'] =  $invDetails['round_off_op'];
                   $data['round_off'] =  $invDetails['round_off'];
                   $data['company_name'] =  $invDetails['company_name'];
				   $data['company_quot_prefix'] =  $invDetails['company_quot_prefix'];
                   $data['b_addr_company_name'] =  $invDetails['b_addr_company_name'];
                   $data['b_addr'] =  $invDetails['b_addr'];
                   $data['b_addr_city'] =  $invDetails['b_addr_city'];
                   $data['b_addr_state'] =  $invDetails['b_addr_state'];
                   $data['b_addr_country'] =  $invDetails['b_addr_country'];
                   $data['b_addr_zip'] =  $invDetails['b_addr_zip'];
				   
				   $data['tax1_declaration'] =  $invDetails['tax1_declaration'];
					
					$data['sub_total_amount'] = number_format($invDetails['sub_total_amount'],2);
					$data['tax1_id'] 	 =  $invDetails['tax1_id'];
					$data['tax1_number'] =  $invDetails['tax1_number'];
					$data['tax1_name']   =  $invDetails['tax1_name'];
					$data['tax1_name']   =  $invDetails['tax1_name'];
					$data['tax1_value']  =  $invDetails['tax1_value'];
					$data['tax1_pvalue'] =  $invDetails['tax1_pvalue'];
					$data['tax1_total_amount']  = number_format($invDetails['tax1_total_amount'],2);
					
					$data['tax1_sub1_id'] =  $invDetails['tax1_sub1_id'];
					$data['tax1_sub1_number'] =  $invDetails['tax1_sub1_number'];
					$data['tax1_sub1_name'] =  $invDetails['tax1_sub1_name'];
					$data['tax1_sub1_value'] =  $invDetails['tax1_sub1_value'];
					$data['tax1_sub1_pvalue'] =  $invDetails['tax1_sub1_pvalue'];
					$data['tax1_sub1_total_amount'] = number_format($invDetails['tax1_sub1_total_amount'],2);
					
					$data['tax1_sub2_id'] =  $invDetails['tax1_sub2_id'];
					$data['tax1_sub2_number'] =  $invDetails['tax1_sub2_number'];
					$data['tax1_sub2_name'] =  $invDetails['tax1_sub2_name'];
					$data['tax1_sub2_value'] =  $invDetails['tax1_sub2_value'];
					$data['tax1_sub2_pvalue'] =  $invDetails['tax1_sub2_pvalue'];
					$data['tax1_sub2_total_amount'] = number_format($invDetails['tax1_sub2_total_amount'],2);
					
					$data['delivery_at'] =  $invDetails['delivery_at'];
					$data['pan_no'] =  $invDetails['pan_no'];
					
				  
					$data['do_tax'] =  $invDetails['do_tax'];	
				   
					if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
						$data['do_tax']= strtotime($data['do_tax']);				
					}
					if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){
						
					   $data['tin_no'] =  $invDetails['tin_no'];
					   $data['service_tax_regn'] =  $invDetails['service_tax_regn'];
					  
					   $data['cst_no'] =  $invDetails['cst_no'];
					   $data['vat_no'] =  $invDetails['vat_no'];
					}
				}
				
				
                //Get billing address BOF
                /*
				include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS,  $data['client']['user_id']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
                //$data['b_address'] = $address_list ;
                //Get billing address EOF
                */
				
                $show_period=$data['show_period_chk']=0;
				if(isset($data['show_period'])){
					$data['show_period_chk'] = 1;
				}
				
                $data['do_i_chk'] = $data['do_i'];
				$data['do_rs_symbol'] = strtotime('2010-07-20');
                $data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
				$data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
                if(!empty($data['do_e'])){
                    $data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                }
                if(!empty($data['do_fe'])){
                    $data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }                
                $data['invoice_title']='INVOICE';
				$data['profm']='';
				if(!empty($data['tax1_name'])){
					$data['invoice_title']='TAX INVOICE';
				}
				
                $query  = " UPDATE ".TABLE_BILL_INV
                            ." SET "
							."". TABLE_BILL_INV .".remarks = '".$data['remarks'] ."'"                              
							.",". TABLE_BILL_INV .".exchange_rate = '". $data['exchange_rate'] ."'"
							.",". TABLE_BILL_INV .".amount = '".        $data['amount'] ."'"
							.",". TABLE_BILL_INV .".amount_inr = '".    $data['amount_inr'] ."'"
							//.",". TABLE_BILL_INV .".amount_words = '".  $data['amount_words'] ."'"
							.",". TABLE_BILL_INV .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
							.",". TABLE_BILL_INV .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
							.",". TABLE_BILL_INV .".do_e = '".          $data['do_e'] ."'" //date of expiry
							.",". TABLE_BILL_INV .".do_fe = '".         $data['do_fe']."'" //date of expiry from
							//.",". TABLE_BILL_INV .".access_level = '".  $data['access_level'] ."'"
							.",". TABLE_BILL_INV .".remarks = '".       $data['remarks'] ."'"
							.",". TABLE_BILL_INV .".balance = '".       $data['balance'] ."'"
							.",". TABLE_BILL_INV .".balance_inr = '".   $data['amount_inr'] ."'"
							.",". TABLE_BILL_INV .".amount_words = '".   $data['amount_words'] ."'"
							.",". TABLE_BILL_INV .".billing_name      = '".processUserData($data['billing_name']) ."'"
							.",". TABLE_BILL_INV .".billing_address ='". processUserData($data['billing_address']) ."'"
							.",". TABLE_BILL_INV .".b_addr_company_name = '".processUserData($data['b_addr_company_name']) ."'"
							.",". TABLE_BILL_INV .".b_addr              = '".processUserData($data['b_addr']) ."'"
							.",". TABLE_BILL_INV .".b_addr_city         = '".processUserData($data['b_addr_city'] )."'"
							.",". TABLE_BILL_INV .".b_addr_state        = '".processUserData( $data['b_addr_state']) ."'"
							.",". TABLE_BILL_INV .".b_addr_country      = '".processUserData($data['b_addr_country']) ."'"
							.",". TABLE_BILL_INV .".b_addr_zip          = '". $data['b_addr_zip'] ."'"
							.",". TABLE_BILL_INV .".status ='". $data['status'] ."'"
							." WHERE ".TABLE_BILL_INV.".number ='".$data['number']."'";
                
               if ( $db->query($query) ) {
                    $messages->setOkMessage('The Invoice "'. $data['number'] .'" has been Updated.');
                    
                    $data['amount']= number_format($data['amount'],2);
                    $data['amount_inr']= number_format($data['amount_inr'],2);
                    $data['balance']= number_format($data['balance'],2);
                    $data['amount_words']=processSqlData($data['amount_words']);
                    
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
                    Order::getParticulars($db, $temp, '*', $condition_query);
                    $data['show_discount']=0;
                    $data['show_nos']=0;
                    //$data['colsSpanNo1']=8;
                    //$data['colsSpanNo2']=9;
					$data['colsSpanNo1']=5;
                    $data['colsSpanNo2']=6;
                    if(!empty($temp)){
                         /*
						$data['wd1']=10;
                        $data['wdinvp']=217;
                        $data['wdpri']=70;
                        $data['wdnos']=70;
                        $data['wdamt']=70;
                        $data['wddisc']=50;
                        $data['wdst']=70;
                        //$data['wdtx']=40; as three columns are removed so manage its width in other cols
                       // $data['wdtxp']=50;
                        //$data['wdtotam']=75;
                        $data['wd2']=9;*/
						
						$data['wd1']=10;
                        $data['wdinvp']=265; //217+28=245+20
                        $data['wdpri']=88;   //70+28=98-10
                        $data['wdnos']=88;   //70+28=98-10
                        $data['wdamt']=97;   //70+27=97
                        $data['wddisc']=77;  //50+27
                        $data['wdst']=97;    //70+27                     
                        $data['wd2']=9;
                    
                        foreach ( $temp as $pKey => $parti ) {
                            /*
                               if($temp[$pKey]['s_type']=='1'){
                                    $temp[$pKey]['s_type']='Years';
                               }elseif($temp[$pKey]['s_type']=='2'){
                                    $temp[$pKey]['s_type']='Quantity';
                               }
                            */
                            if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
                                $data['show_discount']=1;
                                $show_discountNo=1;
                            }
                            if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
                                $data['show_nos']=1;
                                $show_nos=1;
                            }
                            $temp_p[]=array(
                                'p_id'=>$temp[$pKey]['id'],
								'particulars'=>$temp[$pKey]['particulars'],
								'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
								's_type' =>$temp[$pKey]['s_type'] ,                                   
								's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
								's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
								's_id' =>$temp[$pKey]['s_id'] ,                                   
								'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
								'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
								'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
								'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
								'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
								'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
								'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,   
								'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,                                   
								'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                   
								'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
								'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,                                   
								'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,
								'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,                                   
								'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                   
								'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
								'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,                                   
								'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,                                
								'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
								'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
								'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
								'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
								'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                            );
                        }
                        if($data['show_discount'] ==0){ 
                            // disc : 50 & subtot : 70
                           /* $data['wdinvp']= $data['wdinvp']+20 + 40;
                            $data['wdpri'] = $data['wdpri'] + 5 + 6;
                            $data['wdamt'] = $data['wdamt'] + 5+ 6;                            
                            $data['wdtx']  = $data['wdtx'] + 5+ 6;
                            $data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            $data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							*/
							 // disc : 77 & subtot : 97
							$data['wdinvp']= $data['wdinvp']+50 + 70;
                            $data['wdpri'] = $data['wdpri'] +10 + 10;
                            $data['wdamt'] = $data['wdamt'] + 17+ 17;                            
                            //$data['wdtx']  = $data['wdtx'] + 5+ 6;
                            //$data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            //$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
                            
                        }
                        if($data['show_nos'] ==0){
                            // disc : 70
                           /* $data['wdinvp']= $data['wdinvp']+30;
                            $data['wdpri'] = $data['wdpri'] + 5;
                            $data['wdamt'] = $data['wdamt'] + 5;
                            $data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;
							*/
						    // disc :98
							$data['wdinvp']= $data['wdinvp']+80;
                            $data['wdpri'] = $data['wdpri'] + 8;
                            $data['wdamt'] = $data['wdamt'] + 10;
                            /*
							$data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;*/
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;
                            
                        }
                    }
                    $data['particulars']=$temp_p ;
                    //get invoice details bof
                    //Get details of PAN,ST,VAT,CST BOF
					$table = TABLE_CLIENTS;
					$fields1 =  TABLE_CLIENTS .'.cst_no, '.TABLE_CLIENTS .'.vat_no, 
					'.TABLE_CLIENTS .'. service_tax_no,'.TABLE_CLIENTS .'. pan_no' ;
					$condition1 = " WHERE ".TABLE_CLIENTS .".user_id ='".$data['client']."' " ;
					$clientArr = getRecord($table,$fields1,$condition1);
					if(!empty($clientArr)){
						$data['client_cst_no'] = $clientArr['cst_no'];
						$data['client_vat_no'] = $clientArr['vat_no'];
						$data['client_st_no'] = $clientArr['service_tax_no'];
						$data['client_pan_no'] = $clientArr['pan_no'];
					}
					//Get details of PAN,ST,VAT,CST EOF
                    // Create the Invoice PDF, HTML in file.
                    $extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch =$attch1= '';
                    if ( !($attch1 = Invoice::createFile($data, 'HTMLPRINT', $extra)) ) {
                        $messages->setErrorMessage("The Invoice print html  file was not created.");
                    }
                    if ( !($attch = Invoice::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Invoice file was not created.");
                    }
                    Invoice::createPdfFile($data); 
                    //Invoice::createPdfFileSJM($data); 
                    $file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";
                }
                
                  
                /*
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage('The Invoice "'. $data['number'] .'" has been Updated.');
                    $or_id = NULL;
                    
                    // Update the Client Services.
                    /*
                    if ( !Invoice::UpdateClientService($db, $data['client']['user_id'], $data['service_id']) ) {
                        $messages->setErrorMessage("The Clients Services were not updated.");
                    }
                    // Create the Invoice PDF, HTML in file.
                    $extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';
                    if ( !($attch = Invoice::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Invoice file was not created.");
                    }
                    
                    // Send the notification mails to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_inv_add', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_inv_add_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );

                    // Organize the data to be sent in email.
                    $data['link']   = DIR_WS_MP .'/bill-invoice.php?perform=view&inv_id='. $variables['hid'];

                    // Read the Client Manager information.
                    include ( DIR_FS_INCLUDES .'/clients.inc.php');
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');

                    // Send Email to the Client.
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    
                    $data['link']   = DIR_WS_NC .'/bill-invoice.php?perform=view&inv_id='. $variables['hid'];
                    // Send mail to the Concerned Executives.
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_INV_NEW_MANAGERS', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                        }
                    }
                    
                    // Send Email to the Client Manager.
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT_MANAGER', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['manager']['f_name'] .' '. $data['manager']['l_name'], 'email' => $data['manager']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                    }
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }*/
            }
        }
        else {
      
           
        }
        
        if(isset($_POST['btnReturnRcp']) && $messages->getErrorMessageCount() <= 0 ){
        
           header("Location:".DIR_WS_NC."/bill-receipt.php?perform=add&inv_id=".$inv_id."&inv_no=".$data['number']);           
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $inv_id;
			$condition_query='';
			$searchStr=1;
            include ( DIR_FS_NC .'/bill-invoice-list.php');
        }
        else {
         
        
        
        
        
        
            if ( !empty($or_no) ) {
         
                // Read the Order details.
                include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
                $order  = NULL;
                /*
                $fields = 'id,'. TABLE_BILL_ORDERS .'.number AS or_no,'. TABLE_BILL_ORDERS .'.access_level, '. TABLE_BILL_ORDERS .'.status, particulars, details';
                $fields .= ', client, user_id, '. TABLE_CLIENTS .'.number, f_name, l_name';
                */
                 
                $fields = TABLE_BILL_ORDERS.'.id,'.TABLE_BILL_ORDERS .'.do_o AS do_o,'.TABLE_BILL_ORDERS .'.number AS or_no,'. TABLE_BILL_ORDERS .'.access_level, '. TABLE_BILL_ORDERS .'.status, '.TABLE_BILL_ORDERS.'.details';
                $fields .= ', '.TABLE_BILL_ORDERS.'.order_title, '. TABLE_BILL_ORDERS .'.order_closed_by';
                $fields .= ', '.TABLE_BILL_ORDERS.'.is_renewable ';
                $fields .= ', '.TABLE_BILL_ORDERS.'.currency_id, '. TABLE_BILL_ORDERS .'.company_id  ,'.TABLE_BILL_ORDERS .'.amount';
                $fields .= ', '.TABLE_CLIENTS.'.user_id, '. TABLE_CLIENTS .'.number, '.TABLE_CLIENTS.'.f_name, '.TABLE_CLIENTS.'.l_name';
                $fields .= ', '.TABLE_BILL_ORDERS.'.client, '. TABLE_USER .'.number AS u_number, '.TABLE_USER.'.f_name AS u_f_name, '.TABLE_USER.'.l_name AS u_l_name ';
                
   
               
                if ( Order::getDetails($db, $order, $fields, "WHERE ". TABLE_BILL_ORDERS .".number = '". $or_no ."' ") > 0 ) {
              
                    $order = $order[0];
                  
                        if ( $access_level > $order['access_level'] ) {
                            /*$_ALL_POST['or_no']             = $order['or_no'];                            
                            $_ALL_POST['or_details']        = $order['details'];
                            $_ALL_POST['or_user_id']        = $order['user_id'];
                            $_ALL_POST['or_number']         = $order['number'];
                            $_ALL_POST['or_f_name']         = $order['f_name'];
                            $_ALL_POST['or_l_name']         = $order['l_name'];
                            $_ALL_POST['or_access_level']   = $order['access_level'];*/
                            
                             $_ALL_POST['or_no']             = $order['or_no'];
                           // $_ALL_POST['or_particulars']    = $order['particulars'];
                            $_ALL_POST['or_details']        = $order['details'];
                            $_ALL_POST['or_user_id']        = $order['user_id'];
                            $_ALL_POST['or_number']         = $order['number'];
                            $_ALL_POST['or_f_name']         = $order['f_name'];
                            $_ALL_POST['or_l_name']         = $order['l_name'];
                            $_ALL_POST['or_access_level']   = $order['access_level'];
                         
                            $_ALL_POST['do_o']              = $order['do_o'];
                            $_ALL_POST['u_f_name']          = $order['u_f_name'];
                            $_ALL_POST['u_l_name']          = $order['u_l_name'];
                            $_ALL_POST['u_number']          = $order['u_number'];
                            $_ALL_POST['order_title']       = $order['order_title'];
                            $_ALL_POST['order_closed_by']   = $order['order_closed_by'];
                            $_ALL_POST['is_renewable']   = $order['is_renewable'];
                            
                            if(!empty($order['order_closed_by'])){
                                
                                $condition = TABLE_USER." WHERE  user_id='".$order['order_closed_by']."'";
                                $clientfields = ' '.TABLE_USER.'.user_id, '. TABLE_USER .'.number, '.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
                                User::getList($db, $closed_by, $clientfields, $condition) ;
                                $_ALL_POST['order_closed_by_name']   = $closed_by['0'];
                            }
                                                    
                            
                            $_ALL_POST['amount']= $order['amount'];
                            $_ALL_POST['gramount']= number_format($order['amount'],2);
                            $_ALL_POST['amount_inr']= $order['amount'] * $_ALL_POST['exchange_rate'];
                            $_ALL_POST['currency_id']= $order['currency_id'];
                            $_ALL_POST['company_id']= $order['company_id'];
                           
                           /*get particulars details bof*/
                            $temp = NULL;
                            $temp_p = NULL;
                            $condition_query = "WHERE ord_no = '". $order['or_no'] ."'";
                            Order::getParticulars($db, $temp, '*', $condition_query);
                            if(!empty($temp)){
                                foreach ( $temp as $pKey => $parti ) {
                                  /*
                                    if($temp[$pKey]['s_type']=='1'){
                                        $temp[$pKey]['s_type']='Years';
                                    }elseif($temp[$pKey]['s_type']=='2'){
                                        $temp[$pKey]['s_type']='Quantity';
                                    }*/
                                    $temp_p[]=array(
                                                    'p_id'=>$temp[$pKey]['id'],
                                                    'particulars'=>$temp[$pKey]['particulars'],
                                                    'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
                                                    's_id' =>$temp[$pKey]['s_id'] ,                                   
                                                    's_type' =>$temp[$pKey]['s_type'] ,                                   
                                                    's_quantity' =>$temp[$pKey]['s_quantity'] , 
                                                    's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                                      
                                                    'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                                    'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                                    'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                                    'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
                                                    'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                                    'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,    
                                                    'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,                                                        
                                                    'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
                                                    'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
                                                    'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
                                                    'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                                    'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                                                    );
                                    
                                }
                            }
                            $_ALL_POST['particulars']=$temp_p ;
                           
                            $order = NULL;
                            // Read the Client Addresses.
                            include_once ( DIR_FS_CLASS .'/Region.class.php');
                            $region         = new Region();
                            $region->setAddressOf(TABLE_CLIENTS, $_ALL_POST['or_user_id']);
                            $_ALL_POST['address_list']  = $region->get();                           
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to edit invoice for this Order.");
                            $_ALL_POST = '';
                        }
                        
                 
                }
                else {
                    $messages->setErrorMessage("The Selected Order was not found.");
                }
            }

            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['or_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['or_access_level'], $al_list, $index);
                $_ALL_POST['or_access_level'] = $al_list[$index[0]]['title'];
            }
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'inv_id', 'value' => $inv_id);
            $hidden[] = array('name'=> 'show_period', 'value' => $show_period_chk);
			$hidden[] = array('name'=> 'or_no', 'value' => $or_no);
            $hidden[] = array('name'=> 'is_renewable', 'value' => $_ALL_POST['is_renewable']);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
            
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            //$page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            //$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-edit.html');
        }
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>