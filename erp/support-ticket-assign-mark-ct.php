<?php

    if ( $perm->has('nc_st_as_status') ) {
		
		include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
	    $ticket_id = isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
        
        $access_level = $my['access_level'];
                
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
       // SupportTicket::deactive($ticket_id, $extra);
		
		
		$query = "SELECT ". TABLE_ST_TICKETS .".ticket_date, 
		". TABLE_ST_TICKETS .".from_admin_panel,
		". TABLE_ST_TICKETS .".ticket_creator_uid,
		". TABLE_ST_TICKETS .".ticket_owner_uid,
		". TABLE_ST_TICKETS .".ticket_owner,
		". TABLE_ST_TICKETS .".ticket_subject,
		". TABLE_ST_TICKETS .".ticket_no,
		". TABLE_ST_TICKETS.".status "
			." FROM ". TABLE_ST_TICKETS 
		." WHERE (". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' ".") "
		." ORDER BY ". TABLE_ST_TICKETS .".ticket_date DESC LIMIT 0,1 ";				
		/*. TABLE_ST_TICKETS .".ticket_child = '". $ticket_id ."' "." OR "*/
		$db->query($query) ;
		if($db->nf() > 0){
			$db->next_record() ;
			$from_admin_panel = $db->f("from_admin_panel") ;
			$last_time = $db->f("ticket_date") ;
			$ticket_response_time = time() - $last_time ;
			$status = $db->f("status") ;
			$data['ticket_owner'] = $db->f("ticket_owner") ;
			$data['ticket_owner_uid'] = $db->f("ticket_owner_uid") ;
			$data['ticket_creator_uid'] = $db->f("ticket_creator_uid") ;
			$data['ticket_subject']  = $db->f("ticket_subject") ;
			$data['mticket_no']  = $db->f("ticket_no") ;
			if($from_admin_panel==0){
				//if posted by client
				$str = "'".$data['ticket_creator_uid']."','".$data['ticket_owner_uid']."'" ;
			}else{
				$str = "'".$data['ticket_owner_uid']."'" ;
			}
			
		}else{
			$ticket_response_time = 0 ;
		}
		$ticket_date 	= time() ;
		$ticket_replied = '0' ;

		$mail_to_all_su=0;
		if(isset($data['mail_to_all_su']) ){
			$mail_to_all_su=1;
		}
		$mail_to_additional_email=0;
		if(isset($data['mail_to_additional_email']) ){
			$mail_to_additional_email=1;
		}
		$mail_client=0;
		if(isset($data['mail_client']) ){
			$mail_client=1;
		}
		//disply random name BOF 
		$data['display_name'] = $data['display_user_id']=$data['display_designation']='';
		$randomUser = getRandomAssociate($data['executive_id']);
		$data['display_name'] = $randomUser['name'];
		$data['display_user_id'] = $randomUser['user_id'];
		$data['display_designation'] = $randomUser['desig'];
		//disply random name EOF
		
		//Get email id to send mail on client's id
		 
		$fields1 =  TABLE_CLIENTS .'.email,'.TABLE_CLIENTS.'.email_1,'.TABLE_CLIENTS.'.email_2,'.TABLE_CLIENTS.'.email_3' ;
		$condition_query1 = " WHERE ".TABLE_CLIENTS .".user_id IN(".$str.")" ;
		Clients::getList($db, $clientArr, $fields1, $condition_query1);
		 
		$mail_send_to_su='';
 
		if(!empty($clientArr)){
			foreach($clientArr as $key=>$val1){			
			 
				if(!empty($val1['email'])){				 
					$mail_send_to_su.= $val1['email'] ;
					$to[]   = array('name' => $val1['email'], 'email' => $val1['email']);
				}
				if(!empty($val1['email_1'])){
					$mail_send_to_su.= $val1['email_1'] ;
					$to[]   = array('name' => $val1['email_1'], 'email' => $val1['email_1']);
				}
				if(!empty($val['email_2'])){
					$mail_send_to_su.= $val1['email_2'] ;
					$to[]   = array('name' => $val1['email_2'], 'email' => $val1['email_2']);
				}
			}		
			$ticket_status = SupportTicket::PENDINGWITHCLIENTS;		
			$data['ticket_text'] = " This is to inform you that your support request is attended & resolved. <br/>
				Please let us know if you have any query or need any further assistance. " ;	 
			 
			$mail_client = '';
			$ticket_no  =  SupportTicket::getNewNumber($db);
			$data['min'] = 5;
			$data['hrs'] = 0;
			$hrs1 = (int) ($data['hrs'] * 6);
			$min1 = (int) ($data['min'] * 6);  
				$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
				. TABLE_ST_TICKETS .".ticket_no          = '". $ticket_no ."', "
				. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
				. TABLE_ST_TICKETS .".invoice_profm_id   = '". $invoice_profm_id ."', "
				. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
				/*
				. TABLE_ST_TICKETS .".template_id        = '". $data['template_id'] ."', "
				. TABLE_ST_TICKETS .".template_file_1    = '". $data['template_file_1'] ."', "
				. TABLE_ST_TICKETS .".template_file_2    = '". $data['template_file_2'] ."', "
				. TABLE_ST_TICKETS .".template_file_3    = '". $data['template_file_3'] ."', "
				. TABLE_ST_TICKETS .".ss_id      	     = '". $data['ss_id'] ."', "
				. TABLE_ST_TICKETS .".ss_title   		 = '". $data['ss_title'] ."', "
				. TABLE_ST_TICKETS .".ss_file_1   		 = '". $data['ss_file_1'] ."', "
				. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
				. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
				. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
				. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
				. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
				. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
				. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
				. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
			    */
				. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
				. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
				. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
				. TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
				. TABLE_ST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
				. TABLE_ST_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
				//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
				//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
				//. TABLE_ST_TICKETS .".ticket_status     = '".$data['ticket_status']."', "
				. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status."', "
				. TABLE_ST_TICKETS .".ticket_child      = '".$ticket_id."', "
				. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
				. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
				. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
				. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
				. TABLE_ST_TICKETS .".min = '". $data['min']."', "
				. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
				. TABLE_ST_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
				. TABLE_ST_TICKETS .".ticket_replied    = '".$ticket_replied."', "
				. TABLE_ST_TICKETS .".from_admin_panel  = '".SupportTicket::ADMIN_PANEL."', "
				. TABLE_ST_TICKETS .".mail_client    = '".$mail_client."', "
				. TABLE_ST_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', " 
				. TABLE_ST_TICKETS .".mail_send_to_su    = '".$mail_send_to_su."', " 
				. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
				. TABLE_ST_TICKETS .".domain_name       = '". $data['domain_name'] ."', "
				. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
				. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
				. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
				. TABLE_ST_TICKETS .".is_private        = '". $data['is_private'] ."', "
				. TABLE_ST_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
				. TABLE_ST_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
				. TABLE_ST_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "
				. TABLE_ST_TICKETS .".to_email           = '". $data['to'] ."', "
				. TABLE_ST_TICKETS .".cc_email           = '". $data['cc'] ."', "
				. TABLE_ST_TICKETS .".bcc_email          = '". $data['bcc'] ."', "
				. TABLE_ST_TICKETS .".to_email_1         = '". $data['to_email_1'] ."', "
				. TABLE_ST_TICKETS .".cc_email_1         = '". $data['cc_email_1'] ."', "
				. TABLE_ST_TICKETS .".bcc_email_1        = '". $data['bcc_email_1'] ."', "
				. TABLE_ST_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
			
			//unset($GLOBALS["flag"]);
			//unset($GLOBALS["submit"]);
						
			if ($db->query($query) && $db->affected_rows() > 0) {
				
				$variables['hid'] = $ticket_child  = $db->last_inserted_id() ;
				
				$assign_members=$assign_members_details='';
				
				 //Mark pending towards client bof
				$query_update = "UPDATE ". TABLE_ST_TICKETS 
						." SET ". TABLE_ST_TICKETS .".ticket_status = '".$ticket_status."'"                            
						.",".TABLE_ST_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
						.",".TABLE_ST_TICKETS.".last_comment_from = '".SupportTicket::ADMIN_COMMENT."'"
						.",".TABLE_ST_TICKETS.".last_comment = '".$data['ticket_text']."'"
						.",".TABLE_ST_TICKETS.".assign_members = ''"
						.",".TABLE_ST_TICKETS.".assign_members_details = ''"
						.",".TABLE_ST_TICKETS.".last_comment_by = '".$my['uid'] ."'"
						.",".TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
						." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				$db->query($query_update) ;
				
				/**************************************************************************
				* Send the notification to the ticket owner and the Administrators
				* of the new Ticket being created
				**************************************************************************/
				
				$data['ticket_no']    =   $ticket_no ;
				$data['mticket_no']   =   $data['mticket_no'] ;
				/*foreach( $status as $key=>$val){                           
					if ($data['ticket_status'] == $val['status_id'])
					{
						$data['status']=$val['status_name'];
					}
				}*/
				
				$data['replied_by']   =   $my['f_name']." ". $my['l_name'] ;
				$data['subject'] =   $data['ticket_subject'] ;
				$data['text']    =   $data['ticket_text'] ;
				$data['attachment']   =   $attachfilename ;
				$data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $ticket_id;
				$file_name = '';
				
				if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
					$from   = $reply_to = array('name' => $email["from_name"],'email' => $email['from_email']);	
					/*
					if(!empty($data['email'])){
						$to[]   = array('name' => $data['email'], 'email' => $data['email']);
					}
					if(!empty($data['email_1'])){
						$to[]   = array('name' => $data['email_1'], 'email' => $data['email_1']);
						$from   = $reply_to = array('name' => $email["from_name"],'email' => $email['from_email']);	
					}
					if(!empty($data['email_2'])){
						$to[]   = array('name' => $data['email_2'], 'email' => $data['email_2']);
						$from   = $reply_to = array('name' => $email["from_name"],'email' => $email['from_email']);	
					}*/
					
					if(!empty($to)){
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); 
						/*
						echo $email["subject"] ;
						echo $email["body"] ;*/
					}
				}
				$messages->setOkMessage("This communication is marked as Completed.");
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'support-ticket-assign-mark-ct.html');
				
			}
		}
			 
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this module.");
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
