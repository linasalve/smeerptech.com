<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
   
    include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php'); 
	
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	                                  
    $variables['date'] = date("Y-m-d H:i:s");
        
        
        
       //use switch case here to perform action. 
        switch ($perform) { 
            
            case ('view'):
            default: {
				 
                include (DIR_FS_NC .'/work-policy-view.php');
                // CONTENT = CONTENT
				if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'work-policy.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        
     
 
    // always assign
    $s->assign("variables", $variables);
   
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
