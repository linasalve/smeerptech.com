<?php

    if ( $perm->has('nc_sl_ld_q_fview') ) {
        $q_id = isset ($_GET['q_id']) ? $_GET['q_id'] : ( isset($_POST['q_id'] ) ? $_POST['q_id'] :'');
        $type   = isset ($_GET['type']) ? $_GET['type'] : ( isset($_POST['type'] ) ? $_POST['type'] :'');
        
        $condition_query= " WHERE ". TABLE_SALE_QUOTATION .".id = '". $q_id ."'"
							." OR ". TABLE_SALE_QUOTATION .".number = '". $q_id ."'";
        
      
        $list	= NULL;
        if ( Quotation::getDetails( $db, $list, TABLE_SALE_QUOTATION .'.id, '. TABLE_SALE_QUOTATION .'.number', $condition_query) ) {
            
            if ( $type == 'html' ) {
                $file_path = DIR_FS_QUOTATIONS_HTML ."/";
                $file_name = $list[0]["number"] .".html";
                $content_type = 'text/html';
            }
            elseif ( $type == 'pdf' ) {
                $file_path = DIR_FS_QUOTATIONS_PDF ."/";
                $file_name = $list[0]["number"] .".pdf";
                $content_type = 'application/pdf';
            }
            else {
                echo('<script language="javascript" type="text/javascript">alert("Invalid File specified");</script>');
            }
            
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-type: $content_type");
            header("Content-Disposition: attachment; filename=". $file_name );
            header("Content-Length: ".filesize($file_path.$file_name));
            readfile($file_path.$file_name);
        }
        else {
            echo('<script language="javascript" type="text/javascript">alert("The File is not found or you do not have the permission to view the file.");</script>');
        }
    }
    else {
        echo('<script language="javascript" type="text/javascript">alert("You do not have the permission to view the file.");</script>');
    }
    exit;
?>