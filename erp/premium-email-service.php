<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/premium-email-service.inc.php');
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
    $msg 		= isset($_GET["msg"])     ? $_GET["msg"]    : ( isset($_POST["msg"])      ? $_POST["msg"]   :'');
    $main_tpl 		= isset($_GET["main_tpl"])     ? $_GET["main_tpl"]    : ( isset($_POST["main_tpl"])      ? $_POST["main_tpl"]   :'1');
    $condition_url = $condition_query = '';
    
	if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
    $condition_query ='';
   
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
	if(!empty($msg)){
         $messages->setOkMessage($msg);
    }
    if($added){
         $messages->setOkMessage("Record has been added successfully.");
    }
    
    if ( $perm->has('nc_pre_email_serv') ) {  
        $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
                                TABLE_CLIENT_DOMAINS   =>  array(
                                                            'Domain'      => 'domain'                                             
														)
                                );
        
        $sOrderByArray  = array(
                                TABLE_CLIENT_DOMAINS => array(                                 						
                                                          'Domain'      => 'domain'                                           
                                                        )
                                );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PREMIUM_EMAIL_SERVICE;
        }
        $where_added =false ;
		
        //$premium_email_service_id = 144;
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {            
                if( $perm->has('nc_pre_email_serv') ){                
					include (DIR_FS_NC.'/premium-email-service-add.php');
                }else{
					$messages->setErrorMessage("You do not have the permission to access this module.");
				}
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'premium-email-service.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('enterprise_add'): {            
                if( $perm->has('nc_pre_email_serv') ){                
					include (DIR_FS_NC.'/premium-email-service-enterprise-add.php');
                }else{
					$messages->setErrorMessage("You do not have the permission to access this module.");
				}
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'premium-email-service.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('enterprise_service'): {            
                if( $perm->has('nc_pre_email_serv') ){					
					include (DIR_FS_NC.'/premium-email-service-enterprise-service.php');
                }else{
					$messages->setErrorMessage("You do not have the permission to access this module.");
				}
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'premium-email-service.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('premium_allotment'): {            
                if( $perm->has('nc_pre_email_serv') ){					
					include (DIR_FS_NC.'/premium-email-service-allotment.php');
                }else{
					$messages->setErrorMessage("You do not have the permission to access this module.");
				}
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'premium-email-service.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit'): {                
                if( $perm->has('nc_pre_email_serv') ) {
					include (DIR_FS_NC .'/premium-email-service-edit.php');
                }else{
					$messages->setErrorMessage("You do not have the permission to access this module.");
				}
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'premium-email-service.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }           
            /*
			case ('view'): {
                include (DIR_FS_NC .'/address-book-view.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			*/            
            case ('search'): {
            	if( $perm->has('nc_pre_email_serv') ){            	
					include(DIR_FS_NC."/premium-email-service-search.php");
                }else{
					$messages->setErrorMessage("You do not have the permission to access this module.");
				}
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'premium-email-service.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            } 
            case ('list'):
            default:{
            	if( $perm->has('nc_pre_email_serv') ) {
            		include (DIR_FS_NC .'/premium-email-service-list.php');
				}else{
					$messages->setErrorMessage("You do not have the permission to access this module.");
				}
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'premium-email-service.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    
	} else {
        $messages->setErrorMessage("You donot have the Right to Access this Module."); 
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'premium-email-service.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html'); 
    }
    
    // always assign
    $s->assign("main_tpl", $main_tpl);
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>