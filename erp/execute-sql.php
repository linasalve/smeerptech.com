<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
        include_once ( DIR_FS_INCLUDES .'/task-reminder.inc.php'); 
    /*
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    */
   
    //include_once ( DIR_FS_NC ."/header.php");
    $db 		= new db_local; // database handle
  
    // 2009-08-aug-15 
    /*    
    $sql='ALTER TABLE `clients` ADD `billing_name` VARCHAR( 255 ) NOT NULL ';
    $db->query($sql);
   
     $sql='ALTER TABLE `clients` ADD `alloted_clients` TEXT NOT NULL '; 
     $db->query($sql); 
     
  
      $sql="ALTER TABLE `project_task_details` CHANGE `mail_senior` `mark_imp` ENUM( '0', '1' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' ";
      $db->query($sql); 
      $sql="select do_delete from  billing_inv limit 0,1";     
       if ( $db->query($sql) ) {
            if ( $db->nf() > 0 ) {            
                while ($db->next_record()) {
                echo $db->f('do_delete') ;
                }
            }
        }      
   echo $sql="ALTER TABLE `billing_inv` ADD INDEX `or_no` ( `or_no` )";
   $db->query($sql);
   echo$sql="ALTER TABLE `billing_orders` DROP INDEX `po_id` ";
   $db->query($sql);  
   echo$sql="ALTER TABLE `billing_orders` ADD INDEX `or_no` ( `number` ) ";
   $db->query($sql);   
   echo$sql= "ALTER TABLE `billing_orders` ADD `is_invoice_create` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `is_renewable` ";
   $db->query($sql);      
    echo$sql= "ALTER TABLE `billing_orders` ADD `delete_by` VARCHAR( 200 ) NOT NULL AFTER `do_delete` ,
           ADD `delete_ip` VARCHAR( 50 ) NOT NULL AFTER `delete_by` ";
    $db->query($sql);
    echo$sql= "ALTER TABLE `billing_inv` ADD `delete_by` VARCHAR( 200 ) NOT NULL ,ADD `delete_ip` VARCHAR( 50 ) NOT NULL";
    $db->query($sql);     
    echo$sql= "ALTER TABLE `billing_rcpt` ADD `delete_by` VARCHAR( 200 ) NOT NULL ,ADD `delete_ip` VARCHAR( 20 ) NOT NULL";
    $db->query($sql);
    */
     //2009-09-sep-04 
     /*
    $sql = "ALTER TABLE `billing_pre_order` ADD `payment_details` TEXT NOT NULL AFTER `client_details` ,
            ADD `remark` TEXT NOT NULL AFTER `payment_details` " ;

    $db->query($sql);
    
    $sql="ALTER TABLE `billing_pre_order` ADD `ip` VARCHAR( 50 ) NOT NULL " ;
    $db->query($sql);
     
    $sql = "CREATE TABLE `settings_events` (
    `eventsid` bigint( 11 ) NOT NULL AUTO_INCREMENT ,
    `events_date` bigint( 20 ) NOT NULL default '0',
    `events_exp_date` bigint( 20 ) NOT NULL default '0',
    `event_time` bigint( 20 ) NOT NULL default '0',
    `events_title` varchar( 200 ) default NULL ,
    `event_type` varchar( 255 ) NOT NULL ,
    `events_introtext` varchar( 200 ) default NULL ,
    `events_content` text,
    `events_source` tinytext,
    `events_place` enum( 'N', 'O' ) NOT NULL default 'N',
    `status` enum( '1', '0' ) NOT NULL default '1',
    PRIMARY KEY ( `eventsid` ) 
    ) ENGINE = MYISAM DEFAULT CHARSET = latin1";
    $db->query($sql);

    $sql="ALTER TABLE `settings_events` CHANGE `eventsid` `id` BIGINT( 11 ) NOT NULL AUTO_INCREMENT ,
    CHANGE `events_date` `events_date` DATETIME NOT NULL ,
    CHANGE `events_exp_date` `events_exp_date` DATETIME NOT NULL ,
    CHANGE `event_time` `event_time` VARCHAR( 55 ) NOT NULL DEFAULT '0',
    CHANGE `events_source` `events_source` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL";

    $db->query($sql);

    $sql="ALTER TABLE `settings_events` ADD `do_e` DATETIME NOT NULL ,ADD `created_by` VARCHAR( 255 ) NOT NULL ,ADD `ip` VARCHAR( 50 ) NOT NULL ";
    $db->query($sql);
    */
    
  /*  
$sql="ALTER TABLE `payment_transaction` ADD `company_name` VARCHAR( 255 ) NOT NULL AFTER `company_id` ,
ADD `company_prefix` VARCHAR( 200 ) NOT NULL AFTER `company_name` ";
$db->query($sql);

$sql="ALTER TABLE `payment_transaction` ADD `pay_received_amt_words` VARCHAR( 255 ) NOT NULL AFTER `pay_received_amt` ";
$db->query($sql);

$sql="ALTER TABLE `payment_transaction` ADD `do_status_update` DATETIME NOT NULL AFTER `status` ,
ADD `status_update_by` VARCHAR( 200 ) NOT NULL AFTER `do_status_update` ,
ADD `status_update_ip` VARCHAR( 50 ) NOT NULL AFTER `status_update_by` ";
$db->query($sql);

$sql="ALTER TABLE `payment_transaction` ADD `will_receipt_generate` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `is_receipt` ";
$db->query($sql);

$sql="ALTER TABLE `payment_party` ADD `created_by` VARCHAR( 200 ) NOT NULL AFTER `status` ,
ADD `ip` VARCHAR( 50 ) NOT NULL AFTER `created_by`" ;
$db->query($sql);

$sql="ALTER TABLE `payment_party` ADD `address` TEXT NOT NULL AFTER `company_name` ,
ADD `contact_no` VARCHAR( 255 ) NOT NULL AFTER `address`" ;
$db->query($sql);

$sql="ALTER TABLE `payment_transaction` ADD `status_deactivate` ENUM( '0', '1', '2' ) NOT NULL DEFAULT '2' COMMENT 'if 0 pending, 1 completed, 2 for default' AFTER `pdc_chq_details` ";
$db->query($sql);*

$sql="ALTER TABLE `payment_transaction` ADD `reference_transaction_id` VARCHAR( 50 ) NOT NULL AFTER `pdc_chq_details` ,
ADD `is_auto_entry` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `reference_transaction_id`" ;
$db->query($sql);

$sql="ALTER TABLE `payment_transaction` ADD `executive_id` VARCHAR( 200 ) NOT NULL AFTER `client_id` ";
$db->query($sql);


$sql="ALTER TABLE `payment_transaction` ADD `reason_of_deactivate` TEXT NOT NULL ,
ADD `reason` INT NOT NULL ";
$db->query($sql);


//2009-09-sep-09 
$sql = "ALTER TABLE `ticket_tickets` ADD `status` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `is_classified`";
$db->query($sql);

$sql = "ALTER TABLE `ticket_tickets` ADD `using_imap` ENUM( '0', '1' ) NOT NULL AFTER `is_classified`";
$db->query($sql);

$sql = "CREATE TABLE `contact_information` (
  `id` int(11) NOT NULL auto_increment,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `phone` int(20) NOT NULL,
  `pin` int(20) NOT NULL,
  `country` varchar(20) NOT NULL,
  `fax` int(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `url` varchar(50) NOT NULL,
  `comments` text NOT NULL,
  `organization` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1" ;
$db->query($sql);

$sql = "ALTER TABLE `ticket_tickets` ADD `status` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `is_classified`";
$db->query($sql);

$sql = "CREATE TABLE `service_book` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`title` varchar( 255 ) NOT NULL ,
`description` text NOT NULL ,
`do_e` datetime NOT NULL ,
`status` enum( '0', '1' ) NOT NULL ,
`ip` varchar( 255 ) NOT NULL ,
`access_level` smallint( 5 ) NOT NULL ,
`created_by` varchar( 255 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM DEFAULT CHARSET = latin1";
$db->query($sql);

$sql = "CREATE TABLE `ideas` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`title` varchar( 255 ) NOT NULL ,
`description` text NOT NULL ,
`do_e` datetime NOT NULL ,
`status` enum( '0', '1' ) NOT NULL ,
`ip` varchar( 255 ) NOT NULL ,
`access_level` smallint( 5 ) NOT NULL ,
`created_by` varchar( 255 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM DEFAULT CHARSET = latin1";
$db->query($sql);


$sql = "CREATE TABLE `knowledge_share` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`title` varchar( 255 ) NOT NULL ,
`description` text NOT NULL ,
`do_e` datetime NOT NULL ,
`status` enum( '0', '1' ) NOT NULL ,
`ip` varchar( 255 ) NOT NULL ,
`access_level` smallint( 5 ) NOT NULL ,
`created_by` varchar( 255 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM DEFAULT CHARSET = latin1";
$db->query($sql);

$sql = "CREATE TABLE `announcement` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`title` VARCHAR( 255 ) NOT NULL ,
`description` TEXT NOT NULL ,
`do_announcement` DATETIME NOT NULL ,
`status` ENUM( '0', '1' ) NOT NULL DEFAULT '0',
`do_e` DATETIME NOT NULL ,
`created_by` VARCHAR( 255 ) NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL 
) ENGINE = MYISAM ";
$db->query($sql);


$sql = "ALTER TABLE `announcement` ADD `access_level` SMALLINT( 5 ) NOT NULL";
$db->query($sql);


$sql =  "ALTER TABLE `visitor_book` ADD `company_name` VARCHAR( 255 ) NOT NULL AFTER `name`" ;
$db->query($sql);*/
 
/* 
2009-09-sep-10
$sql = "ALTER TABLE `ticket_tickets` CHANGE `ticket_no` `ticket_no` BIGINT NOT NULL ";
$db->query($sql);

$sql = "ALTER TABLE `ticket_tickets` ADD `ticket_rating` INT NOT NULL AFTER `ticket_replied` " ;
$db->query($sql);


echo $sql = "TRUNCATE TABLE `ticket_tickets`";
$db->query($sql);

echo $sql = "CREATE TABLE `billing_ord_upload_log` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`task_id` int( 11 ) NOT NULL ,
`to_id` text NOT NULL ,
`by_id` varchar( 40 ) NOT NULL ,
`comment` text NOT NULL ,
`do_e` datetime NOT NULL ,
`created_by` varchar( 255 ) NOT NULL ,
`ip` varchar( 50 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM DEFAULT CHARSET = latin1 AUTO_INCREMENT =1" ;

$db->query($sql);


echo  $sql =" CREATE TABLE `user_skills` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `do_e` datetime NOT NULL,
  `status` enum('0','1') NOT NULL,
  `ip` varchar(255) NOT NULL,
  `access_level` smallint(5) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 " ;

$db->query($sql);

echo $sql=" UPDATE clients set `password` = 'e10adc3949ba59abbe56e057f20f883e2', status='1' WHERE number=5406";
$db->query($sql);

*//* 2009-09-sep-18 */
 /*
 echo $sql ="ALTER TABLE `billing_ord_p` ADD `s_div_id` INT NOT NULL COMMENT 'Service division id ' AFTER `p_amount`" ;
 $db->query($sql);
 
 
 echo $sql ="ALTER TABLE `settings_services` ADD `tax1_id` INT NOT NULL AFTER `ss_client_detail`" ;
 $db->query($sql);
 
 
 echo $sql ="ALTER TABLE `billing_ord_p` ADD `tax1_id` INT NOT NULL AFTER `ss_punch_line` ";
 $db->query($sql);
 
 

 //2009-09-sep-19
  
 echo $sql ="ALTER TABLE `billing_ord_p` CHANGE `s_div_id` `ss_div_id` INT( 11 ) NOT NULL COMMENT 'Service division id ' "; 
 $db->query($sql);
    
   $sql="select s_div_id from  billing_ord_p limit 0,1";     
   if ( $db->query($sql) ) {
        if ( $db->nf() > 0 ) {            
            while ($db->next_record()) {
            echo $db->f('s_div_id') ;
            }
        }
    }

 echo $sql ="ALTER TABLE `reminders` ADD `inv_no` VARCHAR( 255 ) NOT NULL AFTER `is_html` ,
ADD `client_id` VARCHAR( 255 ) NOT NULL AFTER `inv_no` ";

$db->query($sql);

2009-09-sep-20

 echo $sql ="CREATE TABLE `billing_inv_reminder` (
`id` INT NOT NULL AUTO_INCREMENT ,
`client_id` VARCHAR( 255 ) NOT NULL ,
`inv_id` INT NOT NULL ,
`inv_no` VARCHAR( 255 ) NOT NULL ,
`do_expiry` DATETIME NOT NULL ,
`days_before_expiry` INT NOT NULL ,
`send_reminder_dt` DATETIME NOT NULL ,
`status` ENUM( '0', '1', '2' ) NOT NULL COMMENT '0 pending,1 completed, 2 cancelled',
`date` DATETIME NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL ,
`created_by` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM";
$db->query($sql);


echo $sql ="CREATE TABLE `billing_inv_reminder_cron` (
`id` INT NOT NULL AUTO_INCREMENT ,
`client_id` VARCHAR( 255 ) NOT NULL ,
`inv_id` INT NOT NULL ,
`inv_no` VARCHAR( 255 ) NOT NULL ,
`do_expiry` DATETIME NOT NULL ,
`days_before_expiry` INT NOT NULL ,
`send_reminder_dt` DATETIME NOT NULL ,
`status` ENUM( '0', '1', '2' ) NOT NULL COMMENT '0 pending,1 completed, 2 cancelled',
`date` DATETIME NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL ,
`created_by` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM" ;
$db->query($sql);


echo $sql ="ALTER TABLE `billing_orders` ADD `client_s_client` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `existing_client`" ;
$db->query($sql);

echo $sql ="ALTER TABLE `payment_party` ADD `billing_name` VARCHAR( 255 ) NOT NULL AFTER `lname`" ;
$db->query($sql);
*/

/*
2009-09-sep-21


echo $sql= "ALTER TABLE `service_tax` ADD `tax_number` VARCHAR( 200 ) NOT NULL AFTER `tax_name` ";
$db->query($sql);

echo $sql= "ALTER TABLE `billing_ord_p` ADD `tax1_number` VARCHAR( 200 ) NOT NULL AFTER `tax1_id`";
$db->query($sql);


echo $sql= "ALTER TABLE `billing_ord_p` ADD `tax1_amount` DOUBLE NOT NULL DEFAULT '0.0' AFTER `tax1_pvalue` ";
$db->query($sql);

echo $sql= "ALTER TABLE `service_tax` ADD `do_e` DATETIME NOT NULL AFTER `status` ,
ADD `ip` VARCHAR( 50 ) NOT NULL AFTER `do_e` "; 
*/

/*
echo $sql="UPDATE financial_year SET inv_counter=2, rct_counter=1 WHERE year_range='2009-2010' ";
echo "<br/>";
echo $db->query($sql);

echo "---<br/>";
echo $sql = "DELETE FROM billing_inv Where id =2";
echo $db->query($sql);
echo "<br/>";
echo $sql="TRUNCATE TABLE billing_rcpt";
echo $db->query($sql);

echo $sql = "DELETE FROM billing_inv_reminder_cron where inv_id =2";
echo $db->query($sql);
echo "<br/>";
echo $sql = "DELETE FROM billing_inv_reminder where inv_id =2";
echo $db->query($sql);
*/
/*
 $sql="select * from financial_year ";     
   if ( $db->query($sql) ) {
        if ( $db->nf() > 0 ) {            
            while ($db->next_record()) {
            echo "year_range ".$db->f('year_range') ;
            echo " inv_counter ".$db->f('inv_counter') ;
            echo "<br/>";
            echo "rct_counter ".$db->f('rct_counter') ;
             echo "<br/>";
            }
        }
    }
*/


//2009-09-sep-25 
/*
echo $sql = "ALTER TABLE `billing_orders` ADD `is_renewal_ord` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `is_renewable` ";
echo $db->query($sql);
*/

//2009-09-sep-27
/*
echo $sql = "ALTER TABLE `settings_services` CHANGE `ss_status` `ss_status` ENUM( '0', '1', '2', '3' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ";
echo $db->query($sql);

echo $sql = "ALTER TABLE `billing_rcpt` ADD `payment_voucher` TEXT NOT NULL AFTER `old_billing_address` ";
echo $db->query($sql);

echo $sql = "ALTER TABLE `payment_transaction` CHANGE `receipt_no` `receipt_no` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL  ";
echo $db->query($sql);
*/

//2009-09-sep-29 
/*
echo $sql =  "ALTER TABLE `ticket_tickets` ADD `from_admin_panel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'if 0 then client do the entry, if 1 then support team added' AFTER `using_imap`" ;
echo $db->query($sql);

echo $sql =  "ALTER TABLE `ticket_tickets` CHANGE `status` `status` ENUM( '0', '1' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1' COMMENT 'Used for active/deactive' " ;
echo $db->query($sql);


echo $sql =  "ALTER TABLE `ticket_tickets` ADD `mail_to_all_su` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `using_imap`" ;
echo $db->query($sql);

echo $sql="ALTER TABLE `ticket_tickets` ADD `is_cron` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'if commented by cron then set to1' AFTER `is_classified` " ;
echo $db->query($sql);

echo $sql="ALTER TABLE `ticket_tickets` ADD `do_comment` DATETIME NOT NULL COMMENT 'date of last comment posted ' AFTER `do_e`" ;
echo $db->query($sql);
*/
//2009-09-sep-30
/*
echo $sql="
CREATE TABLE IF NOT EXISTS `project_task_followup` (
  `id` int(11) NOT NULL auto_increment,
  `or_id` bigint(20) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `comment` text NOT NULL,
  `client_contact_person` varchar(255) NOT NULL,
  `client_comment` text NOT NULL,
  `client_email` varchar(255) NOT NULL,
  `client_mobile` varchar(255) NOT NULL,
  `client_phone` varchar(255) NOT NULL,
  `email_subject` varchar(255) NOT NULL,
  `attached_file` varchar(255) NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `do_e` datetime NOT NULL,
  `ip` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

echo $db->query($sql);

echo $sql="ALTER TABLE `ticket_tickets` ADD `last_comment_from` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'if last_comment by client then 0, by admin then 1' AFTER `do_comment` ";
echo $db->query($sql);
*/

//2009-10-oct-03
/*
echo $sql="ALTER TABLE `ticket_tickets` ADD `to` TEXT NOT NULL AFTER `last_comment_from` ,
   ADD `cc` TEXT NOT NULL AFTER `to` ,
   ADD `bcc` TEXT NOT NULL AFTER `cc` ,
   ADD `from_email` TEXT NOT NULL AFTER `bcc` ,
   ADD `mail_send_to_su` TEXT NOT NULL AFTER `from_email`" ;   
echo $db->query($sql);


echo $sql="ALTER TABLE `ticket_tickets` CHANGE `to` `to_email` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `cc` `cc_email` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `bcc` `bcc_email` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ";
echo $db->query($sql);


echo $sql="ALTER TABLE `ticket_tickets` CHANGE `mail_send_to_su` `mail_send_to_su` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Contain email id of the subuser to whom mail are send' ";
echo $db->query($sql);

echo $sql="ALTER TABLE `ticket_tickets` ADD `to_email_1` TEXT NOT NULL AFTER `bcc_email` ,
  ADD `cc_email_1` TEXT NOT NULL AFTER `to_email_1` ,
  ADD `bcc_email_1` TEXT NOT NULL AFTER `cc_email_1`" ;

echo $db->query($sql);
*/

//2009-10-05 
/*
echo $sql="ALTER TABLE `ticket_tickets` ADD INDEX `tc_idx` ( `ticket_child` , `ticket_status` ) ";
echo $db->query($sql);

echo $sql="ALTER TABLE `ticket_tickets` ADD `last_comment` TEXT NOT NULL AFTER `last_comment_from` ";
echo $db->query($sql);
*/

/*
echo $sql=' update `billing_ord_p` set sub_s_id=concat(",",sub_s_id,",") ';
echo $db->query($sql);*/

/*
 $sql="select * from ticket_tickets where  ticket_id=40";     
   if ( $db->query($sql) ) {
        if ( $db->nf() > 0 ) {            
            while ($db->next_record()) {
            echo "to_email ".$db->f('to_email') ;
            echo "<br/>";
            echo "to_email_1 ".$db->f('to_email_1') ;
            
            echo "<br/>";
            echo "cc_email ".$db->f('cc_email') ;
             echo "<br/>";
            echo "from_email ".$db->f('from_email') ;
      
            }
        }
    }    
    */
/*
echo $sql="CREATE TABLE IF NOT EXISTS `petty_transactions` (
  `id` int(11) NOT NULL auto_increment,
  `person_by` varchar(200) NOT NULL,
  `person_to` varchar(200) NOT NULL,
  `purpose` text NOT NULL,
  `bill_received` enum('0','1') NOT NULL default '0',
  `bill_no` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `type` varchar(5) NOT NULL,
  `status` varchar(5) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `access_level` smallint(6) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `do_e` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

echo $db->query($sql);

echo $sql="ALTER TABLE `work_srs` ADD `do_update` DATETIME NOT NULL COMMENT 'date of update' AFTER `do_a` ";
echo $db->query($sql);

echo $sql="ALTER TABLE `work_srs` ADD `updated_by` VARCHAR( 200 ) NOT NULL AFTER `added_by` ";
echo $db->query($sql);
*/

//2009-10-oct-11
/*
echo $sql="ALTER TABLE `work_srs` ADD `updated_by_name` VARCHAR( 255 ) NOT NULL AFTER `updated_by`  ";
echo $db->query($sql);

echo $sql=" ALTER TABLE `work_srs` CHANGE `added_by` `added_by` VARCHAR( 200 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL    ";
echo $db->query($sql);

echo $sql="ALTER TABLE `work_srs` ADD `added_by_name` VARCHAR( 255 ) NOT NULL AFTER `added_by` ";
echo $db->query($sql);
*/
/*
echo $sql="ALTER TABLE `project_task_details` ADD `last_comment` TEXT NOT NULL AFTER `is_client` ,
ADD `last_comment_by` VARCHAR( 200 ) NOT NULL AFTER `last_comment` ,
ADD `last_comment_by_name` VARCHAR( 255 ) NOT NULL AFTER `last_comment_by` ,
ADD `last_comment_dt` DATETIME NOT NULL AFTER `last_comment_by_name` ,
ADD `last_bug_dt` DATETIME NOT NULL AFTER `last_comment_dt` ,
ADD `last_bug_comment_dt` DATETIME NOT NULL AFTER `last_bug_dt`" ;
echo $db->query($sql);
echo "<br/>";
echo $sql="ALTER TABLE `score_sheet` ADD `reason_id` INT NOT NULL AFTER `date`" ;
echo $db->query($sql);
echo "<br/>";
echo $sql="CREATE TABLE `score_sheet_log` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`task_id` int( 11 ) NOT NULL ,
`to_id` text NOT NULL ,
`by_id` varchar( 40 ) NOT NULL ,
`comment` text NOT NULL ,
`do_e` datetime NOT NULL ,
`created_by` varchar( 255 ) NOT NULL ,
`ip` varchar( 50 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM DEFAULT CHARSET = latin1 AUTO_INCREMENT =1";
echo $db->query($sql);
echo "<br/>";
echo $sql="ALTER TABLE `project_task_details` ADD `added_by_name` VARCHAR( 255 ) NOT NULL AFTER `added_by` ";
echo $db->query($sql);

echo "<br/>";

echo $sql="CREATE TABLE `billing_inv_followup` (
`id` INT NOT NULL AUTO_INCREMENT ,
`client_id` VARCHAR( 255 ) NOT NULL ,
`type` INT NOT NULL ,
`comment` TEXT NOT NULL ,
`client_comment` TEXT NOT NULL ,
`client_email` VARCHAR( 255 ) NOT NULL ,
`client_mobile` VARCHAR( 255 ) NOT NULL ,
`client_phone` VARCHAR( 255 ) NOT NULL ,
`created_by` VARCHAR( 255 ) NOT NULL ,
`do_e` DATETIME NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM ";
echo $db->query($sql);
echo "<br/>";

echo $sql="CREATE TABLE `billing_inv_followup_details` (
`id` INT NOT NULL AUTO_INCREMENT ,
`inv_flw_id` INT NOT NULL ,
`ord_no` VARCHAR( 200 ) NOT NULL ,
`inv_no` VARCHAR( 200 ) NOT NULL ,
`inv_amount` DOUBLE NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM ";
echo $db->query($sql);
echo "<br/>";

echo $sql="CREATE TABLE `payment_party_contacts` (
  `id` int(11) NOT NULL auto_increment,
  `c_company_name` varchar(255) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(80) NOT NULL,
  `email_1` varchar(80) NOT NULL,
  `email_2` varchar(80) NOT NULL,
  `contact_details_of` varchar(80) NOT NULL,
  `cc1` varchar(15) NOT NULL,
  `ac1` varchar(15) NOT NULL,
  `p_number1` varchar(25) NOT NULL,
  `p_type1` enum('h','w','m','p','f','o') NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `zipcode` varchar(16) NOT NULL,
  `cc2` varchar(15) NOT NULL,
  `ac2` varchar(15) NOT NULL,
  `p_number2` varchar(25) NOT NULL,
  `p_type2` enum('h','w','m','p','f','o') NOT NULL,
  `cc3` varchar(15) NOT NULL,
  `ac3` varchar(15) NOT NULL,
  `p_number3` varchar(25) NOT NULL,
  `p_type3` enum('h','w','m','p','f','o') NOT NULL,
  `is_preferred` enum('0','1') NOT NULL,
  `is_verified` enum('0','1') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1" ;

echo $db->query($sql);
echo "<br/>";

echo $sql="ALTER TABLE `user_reminders` DROP INDEX `id` ":
echo $db->query($sql);
echo "<br/>";


echo $sql="CREATE TABLE `payment_party_reminders` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `table_name` varchar(100) NOT NULL default '',
  `reminder_for` varchar(40) NOT NULL default '',
  `do_reminder` datetime NOT NULL default '0000-00-00 00:00:00',
  `description` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `table_name` (`table_name`),
  KEY `reminder_for` (`reminder_for`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
echo $db->query($sql);
echo "<br/>";


echo $sql="ALTER TABLE `billing_inv` ADD INDEX `client_idx` ( `status` , `client` )";
echo $db->query($sql);
echo "<br/>";


echo $sql="ALTER TABLE `billing_inv_followup` ADD `status` ENUM( '0', '1', '2' ) NOT NULL DEFAULT '2' COMMENT '0 pending, 1completed, 2 none' AFTER `client_phone`" ;
echo $db->query($sql);
echo "<br/>";
echo $sql="ALTER TABLE `billing_inv_followup` ADD `client_contact_person` VARCHAR( 255 ) NOT NULL AFTER `comment` ";
echo $db->query($sql);
echo "<br/>";

echo $sql="ALTER TABLE `payment_party` ADD `desig` VARCHAR( 100 ) NOT NULL AFTER `contact_no` ,
ADD `org` VARCHAR( 200 ) NOT NULL AFTER `desig` ,
ADD `domain` VARCHAR( 100 ) NOT NULL AFTER `org`" ;
echo $db->query($sql);
echo "<br/>";

echo $sql="DELETE FROM `work_srs` WHERE id IN(12,13)";
echo $db->query($sql);
echo "<br/>";
//define('TABLE_PAYMENT_PARTY_REMINDERS', 'payment_party_reminders');

echo $sql="ALTER TABLE `project_task_details` ADD `file_type` VARCHAR( 10 ) NOT NULL AFTER `last_updated` ,
ADD `file_counter` INT NOT NULL DEFAULT '0' AFTER `file_type` ";
echo $db->query($sql);
echo "<br/>";

echo $sql=" ALTER TABLE `settings_email_tpl` CHANGE `name` `name` VARCHAR( 150 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `description` `description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `from_name` `from_name` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `from_email` `from_email` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `subject` `subject` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ";
echo $db->query($sql);

echo $sql=" ALTER TABLE `project_task_details` ADD `attached_type` VARCHAR( 255 ) NOT NULL AFTER `attached_file`" ;
echo $db->query($sql);
*/

//2009-10-oct-15 bof
/*
echo $sql="ALTER TABLE `score_sheet` ADD `last_comment` TEXT NOT NULL ,
ADD `last_comment_dt` DATETIME NOT NULL ,
ADD `last_comment_by` VARCHAR( 40 ) NOT NULL ,
ADD `last_comment_by_name` VARCHAR( 255 ) NOT NULL ,
ADD `last_comment_to` VARCHAR( 40 ) NOT NULL ,
ADD `last_comment_to_name` VARCHAR( 255 ) NOT NULL" ;
echo $db->query($sql);


//query to alter the table score_sheet
echo $sql="ALTER TABLE `score_sheet` ADD `hide_my_name` ENUM( '0', '1' ) NOT NULL DEFAULT '0',
ADD `hide_my_comment` ENUM( '0', '1' ) NOT NULL DEFAULT '0'" ;
echo $db->query($sql);

//query to alter the table inward
echo $sql="ALTER TABLE `inward` ADD `last_comment` TEXT NOT NULL ,
ADD `last_comment_dt` DATETIME NOT NULL ,
ADD `last_comment_by` VARCHAR( 40 ) NOT NULL ,
ADD `last_comment_by_name` VARCHAR( 255 ) NOT NULL ,
ADD `last_comment_to` VARCHAR( 40 ) NOT NULL ,
ADD `last_comment_to_name` VARCHAR( 255 ) NOT NULL";
echo $db->query($sql);

//query to alter the table visitor_book
echo $sql="ALTER TABLE `visitor_book` ADD `last_comment` TEXT NOT NULL ,
ADD `last_comment_dt` DATETIME NOT NULL ,
ADD `last_comment_by` VARCHAR( 40 ) NOT NULL ,
ADD `last_comment_by_name` VARCHAR( 255 ) NOT NULL ,
ADD `last_comment_to` VARCHAR( 40 ) NOT NULL ,
ADD `last_comment_to_name` VARCHAR( 255 ) NOT NULL ";

echo $db->query($sql);

//query to alter the table incoming_calls
echo $sql="ALTER TABLE `incoming_calls` ADD `last_comment` TEXT NOT NULL ,
ADD `last_comment_dt` DATETIME NOT NULL ,
ADD `last_comment_by` VARCHAR( 40 ) NOT NULL ,
ADD `last_comment_by_name` VARCHAR( 255 ) NOT NULL ,
ADD `last_comment_to` VARCHAR( 40 ) NOT NULL ,
ADD `last_comment_to_name` VARCHAR( 255 ) NOT NULL" ;

echo $db->query($sql);

echo $sql=" ALTER TABLE `clients` ADD `email_3` VARCHAR( 100 ) NOT NULL ,
ADD `email_4` VARCHAR( 100 ) NOT NULL" ;

echo $db->query($sql);*/
/*2009-10-oct-21
echo $sql="ALTER TABLE `billing_rcpt` ADD `ord_no` VARCHAR( 25 ) NOT NULL AFTER `inv_no`" ;
echo $db->query($sql);

echo $sql="UPDATE ticket_tickets SET `ip` = '' WHERE `using_imap` = '1' " ;
echo $db->query($sql);

echo $sql="ALTER TABLE `payment_account_head` ADD `account_head_code` VARCHAR( 15 ) NOT NULL AFTER `account_head_name`" ;
echo $db->query($sql);
*/
//2009-10=oct-25
/* 
echo $sql="ALTER TABLE `ticket_tickets` ADD `headers_imap` TEXT NOT NULL AFTER `using_imap` ";
echo $db->query($sql);
*/

//2009-10-oct-26
/*
echo $sql = " CREATE TABLE `payment_party_bills` (
  `id` int(11) NOT NULL auto_increment,
  `party_id` int(11) NOT NULL,
  `bill_dt` datetime NOT NULL,
  `pay_due_dt` datetime NOT NULL,
  `bill_no` varchar(100) NOT NULL,
  `bill_details` text NOT NULL,
  `amount` double NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `access_level` smallint(6) NOT NULL,
  `do_e` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

echo $db->query($sql);
*/
/*
echo $sql = " ALTER TABLE `payment_party_bills` ADD `bill_particulars` TEXT NOT NULL AFTER `bill_details`" ;
echo $db->query($sql);
*/
/*
 echo $sql = "DROP TABLE `payment_party_bills`  ";
echo $db->query($sql);
echo $sql = "CREATE TABLE `payment_party_bills`(
  `id` int(11) NOT NULL auto_increment,
  `party_id` int(11) NOT NULL,
  `bill_dt` datetime NOT NULL,
  `pay_due_dt` datetime NOT NULL,
  `bill_no` varchar(100) NOT NULL,
  `bill_details` text NOT NULL,
  `bill_particulars` text NOT NULL,
  `amount` double NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `access_level` smallint(6) NOT NULL,
  `do_e` datetime NOT NULL,
  `status` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 " ;

echo $db->query($sql);
echo $sql = " TRUNCATE TABLE `payment_party_bills`    ";
echo $db->query($sql);

echo $sql = "ALTER TABLE `payment_party_bills` ADD `executive_id` VARCHAR( 200 ) NOT NULL AFTER `party_id` ";
echo $db->query($sql);
*/
 
 //2009-10-oct-27
 /*
 echo $sql = "ALTER TABLE `clients` ADD `additional_email` TEXT NOT NULL COMMENT 'Add extra email emails here. This will be used if needs to share the subusers email.'";
 echo $db->query($sql);
*/
//2009-10-oct-28

/*
echo $sql = "ALTER TABLE `clients` DROP `email_3` ,DROP `email_4` ,DROP `additional_email` ";
echo $db->query($sql);
echo $sql = " ALTER TABLE `clients` ADD `email_3` VARCHAR( 100 ) NOT NULL AFTER `email_2` ,
ADD `email_4` VARCHAR( 100 ) NOT NULL AFTER `email_3` ,
ADD `additional_email` TEXT NOT NULL AFTER `email_4` ";
echo $db->query($sql);
echo $sql = "ALTER TABLE `ticket_tickets` ADD `mail_to_additional_email` ENUM( '0', '1' ) NOT NULL AFTER `mail_to_all_su`" ;
echo $db->query($sql);
*/
//2009-10-oct-30
/*
echo $sql = "ALTER TABLE `project_task_followup` ADD `mail_to_all_su` ENUM( '0', '1' ) NOT NULL AFTER `attached_file` ";
echo $db->query($sql);
echo $sql = "ALTER TABLE `project_task_followup` ADD `mail_to_additional_email` ENUM( '0', '1' ) NOT NULL AFTER `mail_to_all_su` ,
ADD `mail_send_to_su` TEXT NOT NULL AFTER `mail_to_additional_email` ";
echo $db->query($sql);
*/
//2009-10-oct-31
/*
echo $sql = "ALTER TABLE `ticket_tickets` CHANGE `ticket_subject` `ticket_subject` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";
echo $db->query($sql);

echo $sql = "CREATE TABLE `ticket_template` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`title` VARCHAR( 255 ) NOT NULL ,
`details` TEXT NOT NULL ,
`status` ENUM( '0', '1' ) NOT NULL ,
`do_e` DATETIME NOT NULL ,
`ip` VARCHAR( 255 ) NOT NULL ,
`access_level` SMALLINT( 5 ) NOT NULL ,
`created_by` VARCHAR( 255 ) NOT NULL 
) ENGINE = MYISAM" ;

echo $db->query($sql);


echo $sql = "CREATE TABLE  `project_task_template`(
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`title` varchar( 255 ) NOT NULL ,
`details` text NOT NULL ,
`status` enum( '0', '1' ) NOT NULL ,
`do_e` datetime NOT NULL ,
`ip` varchar( 255 ) NOT NULL ,
`access_level` smallint( 5 ) NOT NULL ,
`created_by` varchar( 255 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM DEFAULT CHARSET = latin1;" ;

echo $db->query($sql);
*/

//2009-11-nov-05
/*
echo $sql = "ALTER TABLE `task_reminder` ADD `on_behalf_id` VARCHAR( 255 ) NOT NULL AFTER `allotted_to_addr`";
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder_history` ADD `on_behalf_id` VARCHAR( 255 ) NOT NULL AFTER `allotted_to_addr`";
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder` ADD `job_id` INT NOT NULL AFTER `task`" ;
echo $db->query($sql);


echo $sql = "ALTER TABLE `login_stats` CHANGE `login_status` `login_status` ENUM( '0', '1', '2' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '0 for failure,1 for success,2 for invalid ip' ";
echo $db->query($sql);
echo $sql = "ALTER TABLE `ticket_tickets` ADD `mail_client` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `headers_imap`" ;
echo $db->query($sql);
*/

//2009-11-nov-07
/*
echo $sql = "ALTER TABLE `task_reminder` ADD `order_id` INT( 11 ) NOT NULL AFTER `job_id`" ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder` CHANGE `order_id` `order_id` BIGINT( 11 ) NOT NULL"; 
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder_history` ADD `order_id` BIGINT( 11 ) NOT NULL AFTER `task_id` ";
echo $db->query($sql);


echo $sql = "ALTER TABLE `task_reminder` ADD `hr` VARCHAR( 50 ) NOT NULL AFTER `do_deadline` ,
ADD `min` VARCHAR( 50 ) NOT NULL AFTER `hr` ,
ADD `time_type` ENUM( '0', '1' ) NOT NULL AFTER `min`" ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder_history` ADD `hr` VARCHAR( 50 ) NOT NULL AFTER `do_deadline` ,
ADD `min` VARCHAR( 50 ) NOT NULL AFTER `hr` ,
ADD `time_type` ENUM( '0', '1' ) NOT NULL AFTER `min`" ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder` CHANGE `hr` `hrs` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL" ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder_history` CHANGE `hr` `hrs` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE 
latin1_swedish_ci NOT NULL ";
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder` CHANGE `time_type` `time_type` ENUM( '1', '2' ) CHARACTER SET latin1 
COLLATE latin1_swedish_ci NOT NULL ";
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder_history` CHANGE `time_type` `time_type` ENUM( '1', '2' ) CHARACTER 
SET latin1 COLLATE latin1_swedish_ci NOT NULL";
echo $db->query($sql);



echo $sql = "CREATE TABLE `jobs` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`title` varchar( 255 ) NOT NULL ,
`description` text NOT NULL ,
`do_e` datetime NOT NULL ,
`status` enum( '0', '1', '2' ) NOT NULL ,
`ip` varchar( 255 ) NOT NULL ,
`access_level` smallint( 5 ) NOT NULL ,
`created_by` varchar( 255 ) NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM DEFAULT CHARSET = latin1 AUTO_INCREMENT =1";

echo $db->query($sql);


echo $sql = "ALTER TABLE `jobs` ADD `do_job` DATETIME NOT NULL AFTER `description`" ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder_history` ADD `updated_by` VARCHAR( 255 ) NOT NULL AFTER `do_u`" ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `task_reminder_history` ADD `job_id` INT( 11 ) NOT NULL AFTER `task_id`" ;
echo $db->query($sql);
*/
//2009-11-nov-09
/*
echo $sql = "ALTER TABLE `clients` ADD `send_mail` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'if 1 then we will send updates to person, if 0 then updates will not sent to him'";
echo $db->query($sql);
*/
//2009-11-nov-10
/*
echo $sql = "ALTER TABLE `project_task_details` ADD `ticket_no` VARCHAR( 150 ) NOT NULL AFTER `last_bug_comment_dt`" ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `ticket_tickets` ADD `on_behalf` VARCHAR( 255 ) NOT NULL AFTER `last_comment` ";
echo $db->query($sql);
*/

//2009-11-nov-14

/*echo $sql = "ALTER TABLE `project_task_followup` ADD `department_id` INT( 11 ) NOT NULL AFTER `mail_send_to_su" ;
echo $db->query($sql);*/

//2009-11-nov-20

/*echo $sql = "CREATE TABLE IF NOT EXISTS `vendors` (
  `user_id` varchar(40) NOT NULL default '',
  `parent_id` varchar(40) NOT NULL,
  `service_id` text NOT NULL,
  `number` mediumint(11) unsigned NOT NULL default '0',
  `manager` varchar(40) NOT NULL,
  `team` text NOT NULL,
  `username` varchar(80) NOT NULL default '',
  `password` varchar(40) NOT NULL default '',
  `access_level` smallint(5) unsigned NOT NULL default '0',
  `created_by` varchar(40) NOT NULL,
  `roles` varchar(50) NOT NULL default '',
  `status` enum('0','1','2','3') NOT NULL default '2',
  `credit_limit` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL,
  `lead_assign_to` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL default '',
  `email_1` varchar(80) NOT NULL default '',
  `email_2` varchar(80) NOT NULL default '',
  `email_3` varchar(100) NOT NULL,
  `email_4` varchar(100) NOT NULL,
  `additional_email` text NOT NULL,
  `title` varchar(10) NOT NULL default '',
  `f_name` varchar(20) NOT NULL default '',
  `m_name` varchar(20) NOT NULL default '',
  `l_name` varchar(20) NOT NULL default '',
  `p_name` varchar(20) NOT NULL default '',
  `desig` varchar(100) NOT NULL default '',
  `org` varchar(200) NOT NULL default '',
  `domain` varchar(100) NOT NULL default '',
  `gender` enum('m','f','o') NOT NULL default 'm',
  `do_birth` datetime NOT NULL default '0000-00-00 00:00:00',
  `do_aniv` datetime NOT NULL default '0000-00-00 00:00:00',
  `do_add` datetime NOT NULL default '0000-00-00 00:00:00',
  `do_reg` datetime NOT NULL default '0000-00-00 00:00:00',
  `policy_check` enum('0','1') NOT NULL default '1',
  `ip_reg` varchar(100) NOT NULL,
  `do_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `allow_ip` enum('1','2') NOT NULL default '1' COMMENT '1 for ANY and 2 for Specified Ip ',
  `valid_ip` text NOT NULL COMMENT 'Valid ip list comma seperated',
  `remarks` text NOT NULL,
  `lead_id` int(11) NOT NULL,
  `industry` text NOT NULL,
  `wt_you_do` text NOT NULL,
  `mobile1` varchar(20) NOT NULL,
  `mobile2` varchar(20) NOT NULL,
  `spouse_dob` date NOT NULL,
  `alloted_clients` text NOT NULL,
  `billing_name` varchar(255) NOT NULL,
  `is_soft_copy` enum('0','1','2') NOT NULL default '0',
  `send_mail` enum('0','1') NOT NULL default '1' COMMENT 'if 1 then we will send updates to person, if 0 then updates will not sent to him',
  PRIMARY KEY  (`user_id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1";
echo $db->query($sql);

echo $sql = "CREATE TABLE IF NOT EXISTS `vendors_address` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `table_name` varchar(30) NOT NULL default '',
  `address_of` varchar(100) NOT NULL default '',
  `company_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL default '',
  `state` varchar(100) NOT NULL default '',
  `country` varchar(50) NOT NULL default '',
  `zipcode` varchar(16) NOT NULL default '',
  `address_type` enum('p','m','o','h','w') NOT NULL default 'p',
  `is_preferred` enum('0','1') NOT NULL default '0',
  `is_verified` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `table_name` (`table_name`),
  KEY `address_of` (`address_of`),
  KEY `table_name_2` (`table_name`),
  KEY `table_name_3` (`table_name`),
  KEY `id_2` (`id`),
  KEY `table_name_4` (`table_name`),
  KEY `address_of_2` (`address_of`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
echo $db->query($sql);

echo $sql = "CREATE TABLE IF NOT EXISTS `vendors_phone` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `table_name` varchar(50) NOT NULL default '',
  `phone_of` varchar(80) NOT NULL default '',
  `cc` varchar(15) NOT NULL,
  `ac` varchar(15) NOT NULL,
  `p_number` varchar(25) NOT NULL,
  `p_type` enum('h','w','m','p','f','o') NOT NULL default 'w',
  `p_is_preferred` enum('0','1') NOT NULL default '0',
  `p_is_verified` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `table_name` (`table_name`),
  KEY `phone_of` (`phone_of`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
echo $db->query($sql);


echo $sql = "CREATE TABLE IF NOT EXISTS `vendors_reminders` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `table_name` varchar(100) NOT NULL default '',
  `reminder_for` varchar(40) NOT NULL default '',
  `do_reminder` datetime NOT NULL default '0000-00-00 00:00:00',
  `description` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `table_name` (`table_name`),
  KEY `reminder_for` (`reminder_for`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1" ;
echo $db->query($sql);

echo $sql = "CREATE TABLE IF NOT EXISTS `vendors_ticket_template` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `do_e` datetime NOT NULL,
  `ip` varchar(255) NOT NULL,
  `access_level` smallint(5) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
echo $db->query($sql);

echo $sql = "CREATE TABLE IF NOT EXISTS `vendors_ticket_tickets` (
  `ticket_id` int(11) NOT NULL auto_increment,
  `ticket_no` bigint(20) NOT NULL,
  `ticket_owner_uid` varchar(32) NOT NULL default '',
  `ticket_owner` varchar(32) NOT NULL default '',
  `ticket_creator_uid` varchar(32) NOT NULL default '',
  `ticket_creator` varchar(32) NOT NULL default '',
  `ticket_subject` varchar(255) NOT NULL,
  `ticket_text` text NOT NULL,
  `ticket_department` varchar(32) NOT NULL default '',
  `ticket_priority` varchar(16) NOT NULL default '',
  `ticket_status` varchar(16) NOT NULL default '',
  `ticket_child` int(11) NOT NULL default '0',
  `ticket_attachment` varchar(100) NOT NULL default '',
  `ticket_attachment_path` tinytext NOT NULL,
  `ticket_response_time` int(11) NOT NULL default '0',
  `ticket_replied` enum('0','1') NOT NULL default '0',
  `ticket_rating` int(11) NOT NULL,
  `domain_name` varchar(255) NOT NULL,
  `assign_members` text NOT NULL COMMENT 'Ticket assignt to members',
  `ticket_date` int(11) NOT NULL default '0',
  `is_private` enum('0','1') NOT NULL default '0',
  `is_classified` enum('0','1') NOT NULL default '0',
  `is_cron` enum('0','1') NOT NULL default '0' COMMENT 'if commented by cron then set to1',
  `using_imap` enum('0','1') NOT NULL,
  `headers_imap` text NOT NULL,
  `mail_client` enum('0','1') NOT NULL default '1',
  `mail_to_all_su` enum('0','1') NOT NULL default '0',
  `mail_to_additional_email` enum('0','1') NOT NULL,
  `from_admin_panel` enum('0','1') NOT NULL default '0' COMMENT 'if 0 then client do the entry, if 1 then support team added',
  `status` enum('0','1') NOT NULL default '1' COMMENT 'Used for active/deactive',
  `do_e` date NOT NULL,
  `do_comment` datetime NOT NULL COMMENT 'date of last comment posted ',
  `last_comment_from` enum('0','1') NOT NULL default '0' COMMENT 'if last_comment by client then 0,  by admin then 1',
  `last_comment` text NOT NULL,
  `on_behalf` varchar(255) NOT NULL,
  `to_email` text NOT NULL,
  `cc_email` text NOT NULL,
  `bcc_email` text NOT NULL,
  `to_email_1` text NOT NULL,
  `cc_email_1` text NOT NULL,
  `bcc_email_1` text NOT NULL,
  `from_email` text NOT NULL,
  `mail_send_to_su` text NOT NULL COMMENT 'Contain email id of the subuser to whom mail are send',
  `ip` varchar(100) NOT NULL,
  PRIMARY KEY  (`ticket_id`),
  KEY `tc_idx` (`ticket_child`,`ticket_status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
echo $db->query($sql);*/
/*
echo $sql = "TRUNCATE TABLE 'vendors'";
echo $db->query($sql);

echo $sql = "TRUNCATE TABLE 'vendors_address'";
echo $db->query($sql);

echo $sql = "TRUNCATE TABLE 'vendors_phone'";
echo $db->query($sql);

echo $sql = "TRUNCATE TABLE 'vendors_reminders'";
echo $db->query($sql);

echo $sql = "TRUNCATE TABLE 'vendors_ticket_template'";
echo $db->query($sql);

echo $sql = "TRUNCATE TABLE 'vendors_ticket_tickets'";
echo $db->query($sql);

echo $sql = "ALTER TABLE `project_task_followup` ADD `sms_response` TEXT NOT NULL AFTER `client_mobile`" ;
echo $db->query($sql);
*/
/*
 $sql="select count(*) as count from site_settings limit 2";     
   if ( $db->query($sql) ) {
        if ( $db->nf() > 0 ) {            
            while ($db->next_record()) {
            echo "count ".$db->f('count') ;       
            }
        }
    }
*/
/*
echo $sql = "ALTER TABLE `site_settings` ADD `sms_counter` BIGINT NOT NULL AFTER `dynamic_page_title`" ;
echo $db->query($sql);
*/

/*
echo $sql = "ALTER TABLE `project_task_followup` CHANGE `sms_response` `sms_send_to` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL " ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `project_task_followup` CHANGE `client_mobile` `client_mobile` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL " ;
echo $db->query($sql);
*/
//2009-11-nov-23
/*
echo $sql = "ALTER TABLE `score_sheet` CHANGE `comment_to` `comment_to` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL "; 
echo $db->query($sql);
*/
/*
$sql="select sms_counter from  site_settings limit 0,1";     
   if ( $db->query($sql) ) {
        if ( $db->nf() > 0 ) {            
            while ($db->next_record()) {
            echo "<br/>sms_counter ".$db->f('sms_counter') ;       
                  
            }
        }
    }
*/  
/*
echo $sql = "
CREATE TABLE IF NOT EXISTS `navigation` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `page` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `selected_page` text NOT NULL,
  `sequence` int(11) NOT NULL default '0',
  `perm_name` varchar(255) NOT NULL,
  `main_perm_name` varchar(255) NOT NULL,
  `do_e` datetime NOT NULL,
  `status` enum('0','1') NOT NULL,
  `ip` varchar(255) NOT NULL,
  `access_level` smallint(6) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=252 ; "; 
echo $db->query($sql);


echo $sql = "INSERT INTO `navigation` (`id`, `parent_id`, `title`, `page`, `image`, `level`, `selected_page`, `sequence`, `perm_name`, `main_perm_name`, `do_e`, `status`, `ip`, `access_level`, `created_by`) VALUES
(1, 0, 'Home', 'index.php', '', 0, 'index.php', 1, 'no_perm', '', '2009-11-21 00:00:00', '1', '192.168.1.32', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(2, 0, 'Address book', '#', '', 0, '', 3, '', '', '2009-11-21 00:00:00', '1', '192.168.1.32', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(5, 0, 'Associates', '#', '', 0, '', 4, '', '', '2009-11-25 00:00:00', '1', '192.168.1.32', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(3, 2, 'List', 'address-book.php?perform=list', '', 0, 'address-book.php?perform=list', 1, 'nc_ab_list', 'nc_ab', '2009-11-21 19:20:34', '1', '192.168.1.32', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(4, 2, 'Add', 'address-book.php?perform=add', '', 0, 'address-book.php?perform=add', 2, 'nc_ab_add', 'nc_ab', '2009-11-21 19:22:59', '1', '192.168.1.32', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(6, 0, 'Billing', '#', '', 0, '', 5, '', '', '2009-11-25 00:00:00', '1', '192.168.1.32', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(7, 5, 'List', 'user.php?perform=list', '', 0, 'user.php?perform=list', 1, 'nc_ue_list', 'nc_ue', '2009-11-25 16:54:59', '1', '192.168.1.32', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(8, 5, 'Add', 'user.php?perform=add', '', 0, 'user.php?perform=add', 2, 'nc_ue_add', 'nc_ue', '2009-11-25 16:55:37', '1', '192.168.1.32', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(50, 6, 'List Invoices', 'bill-invoice.php?perform=list', '', 0, 'bill-invoice.php?perform=list', 1, 'nc_bl_inv_list', 'nc_bl_inv', '2009-11-26 12:04:11', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(9, 0, 'Circulars', '#', '', 0, '', 6, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(10, 0, 'Menu', 'modules.php', '', 0, '', 2, 'no_perm', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(11, 0, 'Clients', '#', '', 0, '', 7, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(12, 0, 'Complain Box', '#', '', 0, '', 8, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(13, 0, 'Console', '#', '', 0, '', 9, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(14, 0, 'Enquiry', '#', '', 0, '', 10, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(15, 0, 'Fire Query', '#', '', 0, '', 11, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(16, 0, 'General', '#', '', 0, '', 12, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(17, 0, 'Hardware Entry', '#', '', 0, '', 13, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(18, 0, 'HR Module', '#', '', 0, '', 14, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(19, 0, 'Ideas', '#', '', 0, '', 15, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(20, 0, 'Knowledge Share', '#', '', 0, '', 17, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(21, 0, 'Inward/Outward', '#', '', 0, '', 16, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(22, 0, 'Login Stats', '#', '', 0, '', 18, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(23, 0, 'Manage Websites', '#', '', 0, '', 19, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(24, 0, 'Marketing', '#', '', 0, '', 20, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(25, 0, 'My Profile', '#', '', 0, '', 21, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(26, 0, 'NC Navigation', '#', '', 0, '', 22, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(27, 0, 'Newsletter', '#', '', 0, '', 23, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(28, 0, 'Ongoing Appraisal', '#', '', 0, '', 24, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(29, 0, 'Orders', '#', '', 0, '', 25, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(30, 0, 'Payement Transaction', '#', '', 0, '', 26, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(31, 0, 'Project Task', '#', '', 0, '', 27, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(32, 0, 'Quotation', '#', '', 0, '', 28, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(33, 0, 'Reports', '#', '', 0, '', 29, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(34, 0, 'Scheduler', '#', '', 0, '', 30, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(35, 0, 'Service Book', '#', '', 0, '', 31, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(36, 0, 'SMS', '#', '', 0, '', 32, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(37, 0, 'Support Ticket', '#', '', 0, '', 33, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(38, 0, 'Telephone Calls Logs', '#', '', 0, '', 34, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(39, 0, 'Uploads', '#', '', 0, '', 35, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(40, 0, 'Vendors', '#', '', 0, '', 36, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(41, 0, 'Vendor Communication', '#', '', 0, '', 37, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(42, 0, 'Vendor Communication Template', '#', '', 0, '', 38, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(43, 0, 'Visitor Book', '#', '', 0, '', 39, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(44, 0, 'Website Access', '#', '', 0, '', 40, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(45, 0, 'Settings Billing', '#', '', 0, '', 41, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(46, 0, 'Settings Marketing', '#', '', 0, '', 42, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(47, 0, 'Settings Payment Transaction', '#', '', 0, '', 43, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(48, 0, 'Settings Project Task', '#', '', 0, '', 44, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(49, 0, 'Settings Scheduler', '#', '', 0, '', 45, '', '', '2009-11-26 00:00:00', '1', '192.168.1.16', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(51, 6, 'List Receipts', 'bill-receipt.php?perform=list', '', 0, 'bill-receipt.php?perform=list', 2, 'nc_bl_rcpt_list', 'nc_bl_rcpt', '2009-11-26 12:05:08', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(52, 9, 'List', 'announcement.php?perform=list', '', 0, 'announcement.php?perform=list', 1, 'nc_anc_list', 'nc_anc', '2009-11-26 12:06:06', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(53, 9, 'Add', 'announcement.php?perform=add', '', 0, 'announcement.php?perform=add', 2, 'nc_anc_add', 'nc_anc', '2009-11-26 12:06:49', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(54, 11, 'List', 'clients.php?perform=list', '', 0, 'clients.php?perform=list', 1, 'nc_uc_list', 'nc_uc', '2009-11-26 12:07:45', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(55, 11, 'Add', 'clients.php?perform=add', '', 0, 'clients.php?perform=add', 2, 'nc_uc_add', 'nc_uc', '2009-11-26 12:09:07', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(56, 12, 'List', 'complain-box.php?perform=list', '', 0, 'complain-box.php?perform=list', 1, 'nc_cb_list', 'nc_cb', '2009-11-26 12:10:07', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(57, 12, 'Add', 'complain-box.php?perform=add', '', 0, 'complain-box.php?perform=add', 2, 'nc_cb_add', 'nc_cb', '2009-11-26 12:10:51', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(58, 13, 'Edit', 'site-settings.php?perofrm=edit', '', 0, 'site-settings.php?perofrm=edit', 1, 'nc_site_s_edit', 'nc_site_s', '2009-11-26 12:11:39', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(59, 14, 'List', 'enquiry.php?perform=list', '', 0, 'enquiry.php?perform=list', 1, 'nc_equ_list', 'nc_equ', '2009-11-26 12:12:28', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(60, 15, 'Fire Query List', 'fire-query.php?perform=list', '', 0, 'fire-query.php?perform=list', 1, 'nc_fq_list', 'nc_fq', '2009-11-26 12:13:25', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(61, 15, 'Fire Query Add', 'fire-query.php?perform=add', '', 0, 'fire-query.php?perform=add', 2, 'nc_fq_add', 'nc_fq', '2009-11-26 12:14:14', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(62, 15, 'Priority List', 'fq-priority.php?perform=list', '', 0, 'fq-priority.php?perform=list', 3, 'nc_fq_pr_list', 'nc_fq_pr', '2009-11-26 12:15:29', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(63, 15, 'Priority Add', 'fq-priority.php?perform=add', '', 0, 'fq-priority.php?perform=add', 4, 'nc_fq_pr_add', 'nc_fq_pr', '2009-11-26 12:16:26', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(64, 15, 'Status List', 'fq-status.php?perform=list', '', 0, 'fq-status.php?perform=list', 5, 'nc_fq_s_list', 'nc_fq_s', '2009-11-26 12:17:25', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(65, 15, 'Status Add', 'fq-status.php?perform=add', '', 0, 'fq-status.php?perform=add', 6, 'nc_fq_s_add', 'nc_fq_s', '2009-11-26 12:18:23', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(66, 15, 'Fire Query Department List', 'fq-department.php?perform=list', '', 0, 'fq-department.php?perform=list', 7, 'nc_fq_d_list', 'nc_fq_d', '2009-11-26 12:19:26', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(67, 15, 'Fire Query Department Add', 'fq-department.php?perform=add', '', 0, 'fq-department.php?perform=add', 8, 'nc_fq_d_add', 'nc_fq_d', '2009-11-26 12:20:21', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(68, 15, 'Alloted to Client List', 'fq-alloted-client.php?perform=list', '', 0, 'fq-alloted-client.php?perform=list', 9, 'nc_fq_at_c_list', 'nc_fq_at_c', '2009-11-26 12:21:22', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(69, 15, 'Alloted to Client Add', 'fq-alloted-client.php?perform=add', '', 0, 'fq-alloted-client.php?perform=add', 10, 'nc_fq_at_c_add', 'nc_fq_at_c', '2009-11-26 12:22:18', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(70, 16, 'Company List', 'company.php?perform=list', '', 0, 'company.php?perform=list', 1, 'nc_cmp_list', 'nc_cmp', '2009-11-26 12:24:13', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(71, 16, 'Company Add', 'company.php?perform=add', '', 0, 'company.php?perform=add', 2, 'nc_cmp_add', 'nc_cmp', '2009-11-26 12:25:02', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(72, 16, 'Settings Calendar List', 'settings-calendar.php?perform=list', '', 0, 'settings-calendar.php?perform=list', 3, 'nc_cal_list', 'nc_cal', '2009-11-26 12:28:48', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(73, 16, 'Settings Calendar Add', 'settings-calendar.php?perform=add', '', 0, 'settings-calendar.php?perform=add', 4, 'nc_cal_add', 'nc_cal', '2009-11-26 12:55:44', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(74, 16, 'Department List', 'department.php?perform=list', '', 0, 'department.php?perform=list', 5, 'nc_dp_list', 'nc_dp', '2009-11-26 12:56:42', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(75, 16, 'Department Add', 'department.php?perform=add', '', 0, 'department.php?perform=add', 6, 'nc_dp_add', 'nc_dp', '2009-11-26 12:57:36', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(76, 16, 'Country List', 'country.php?perform=list', '', 0, 'country.php?perform=list', 7, 'nc_ctry_list', 'nc_ctry', '2009-11-26 12:58:31', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(77, 16, 'Country Add', 'country.php?perform=add', '', 0, 'country.php?perform=add', 8, 'nc_ctry_add', 'nc_ctry', '2009-11-26 13:00:28', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(78, 16, 'State List', 'state.php?perform=list', '', 0, 'state.php?perform=list', 9, 'nc_state_list', 'nc_state', '2009-11-26 13:01:25', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(79, 16, 'State Add', 'state.php?perform=add', '', 0, 'state.php?perform=add', 10, 'nc_state_add', 'nc_state', '2009-11-26 13:02:33', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(80, 16, 'City List', 'city.php?perform=list', '', 0, 'city.php?perform=list', 11, 'nc_city_list', 'nc_city', '2009-11-26 13:08:41', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(81, 16, 'City Add', 'city.php?perform=add', '', 0, 'city.php?perform=add', 12, 'nc_city_add', 'nc_city', '2009-11-26 13:09:34', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(82, 16, 'Manage Access Level List', 'settings-access-level.php?perform=list', '', 0, 'settings-access-level.php?perform=list', 13, 'nc_al_list', 'nc_al', '2009-11-26 13:10:35', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(83, 16, 'Manage Access Level Add', 'settings-access-level.php?perform=add', '', 0, 'settings-access-level.php?perform=add', 14, 'nc_al_add', 'nc_al', '2009-11-26 13:11:37', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(84, 16, 'Manage User Rights List', 'user-rights.php?perform=list', '', 0, 'user-rights.php?perform=list', 15, 'nc_urht_list', 'nc_urht', '2009-11-26 13:12:38', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(85, 16, 'Manage User Rights Add', 'user-rights.php?perform=add', '', 0, 'user-rights.php?perform=add', 16, 'nc_urht_add', 'nc_urht', '2009-11-26 13:13:34', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(86, 16, 'Manage User Roles List', 'user-roles.php?perform=list', '', 0, 'user-roles.php?perform=list', 17, 'nc_urol_list', 'nc_urol', '2009-11-26 13:15:11', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(87, 16, 'Manage User Roles Add', 'user-roles.php?perform=add', '', 0, 'user-roles.php?perform=add', 18, 'nc_urol_add', 'nc_urol', '2009-11-26 13:16:03', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(88, 16, 'Email Templates List', 'settings-templates.php?perform=list', '', 0, 'settings-templates.php?perform=list', 19, 'nc_emtpl_list', 'nc_emtpl', '2009-11-26 13:18:07', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(89, 16, 'Email Templates Add', 'settings-templates.php?perform=add', '', 0, 'settings-templates.php?perform=add', 20, 'nc_emtpl_add', 'nc_emtpl', '2009-11-26 13:19:06', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(90, 16, 'Terms and Conditions For Registartion List', 'terms-registration.php?perform=list', '', 0, 'terms-registration.php?perform=list', 21, 'nc_tcr_list', 'nc_tcr', '2009-11-26 13:20:22', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(91, 16, 'Terms&nbsp;and&nbsp;Conditions&nbsp;For&nbsp;Registartion&nbsp;Add', 'terms-registration.php?perform=add', '', 0, 'terms-registration.php?perform=add', 22, 'nc_tcr_add', 'nc_tcr', '2009-11-26 13:21:28', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(92, 16, 'Quotation&nbsp;Terms and Conditions&nbsp;List', 'terms-condition.php?perform=list', '', 0, 'terms-condition.php?perform=list', 23, 'nc_tc_list', 'nc_tc', '2009-11-26 13:23:33', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(93, 16, 'Quotation&nbsp;Terms and Conditions&nbsp;Add', 'terms-condition.php?perform=add', '', 0, 'terms-condition.php?perform=add', 24, 'nc_tc_add', 'nc_tc', '2009-11-26 13:24:28', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(94, 16, 'Global&nbsp;Settings&nbsp;List', 'global-settings.php?perform=list', '', 0, 'global-settings.php?perform=list', 25, 'nc_glb_list', 'nc_glb', '2009-11-26 13:25:33', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(95, 16, 'Global&nbsp;Settings&nbsp;Add', 'global-settings.php?perform=add', '', 0, 'global-settings.php?perform=add', 26, 'nc_glb_add', 'nc_glb', '2009-11-26 13:26:34', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(96, 16, 'Skills&nbsp;List', 'user-skills.php?perform=list', '', 0, 'user-skills.php?perform=list', 27, 'nc_usk_list', 'nc_usk', '2009-11-26 13:27:32', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(97, 16, 'Skills&nbsp;Add', 'user-skills.php?perform=add', '', 0, 'user-skills.php?perform=add', 28, 'nc_usk_add', 'nc_usk', '2009-11-26 13:28:24', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(98, 16, 'Mark&nbsp;Financial&nbsp;List', 'financial-year.php?perform=list', '', 0, 'financial-year.php?perform=list', 29, 'nc_fy_list', 'nc_fy', '2009-11-26 13:29:18', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(99, 16, 'Mark&nbsp;Financial&nbsp;Add', 'financial-year.php?perform=add', '', 0, 'financial-year.php?perform=add', 30, 'nc_fy_add', 'nc_fy', '2009-11-26 13:30:15', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(100, 17, 'List', 'hardware.php?perform=list', '', 0, 'hardware.php?perform=list', 1, 'nc_hd_list', 'nc_hd', '2009-11-26 13:32:11', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(103, 18, 'Hr&nbsp;Attendance&nbsp;List', 'hr-attendance.php?perform=list', '', 0, 'hr-attendance.php?perform=list', 1, 'nc_hr_att_list', 'nc_hr_att', '2009-11-26 14:14:14', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(102, 17, 'Add', 'hardware.php?perform=add', '', 0, 'hardware.php?perform=add', 2, 'nc_hd_add', 'nc_hd', '2009-11-26 13:34:08', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(104, 18, 'Hr&nbsp;Attendance&nbsp;Add', 'hr-attendance.php?perform=add', '', 0, 'hr-attendance.php?perform=add', 2, 'nc_hr_att_add', 'nc_hr_att', '2009-11-26 14:15:00', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(105, 18, 'Hr&nbsp;Salary&nbsp;List', 'hr-salary.php?perform=list', '', 0, 'hr-salary.php?perform=list', 3, 'nc_hr_sal_list', 'nc_hr_sal', '2009-11-26 14:15:49', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(106, 18, 'Hr&nbsp;Salary&nbsp;Add', 'hr-salary.php?perform=add', '', 0, 'hr-salary.php?perform=add', 4, 'nc_hr_sal_add', 'nc_hr_sal', '2009-11-26 14:16:37', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(107, 19, 'List', 'ideas.php?perform=list', '', 0, 'ideas.php?perform=list', 1, 'nc_ide_list', 'nc_ide', '2009-11-26 14:17:32', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(108, 19, 'Add', 'ideas.php?perform=add', '', 0, 'ideas.php?perform=add', 2, 'nc_ide_add', 'nc_ide', '2009-11-26 14:18:23', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(109, 21, 'List', 'inward.php?perform=list', '', 0, 'inward.php?perform=list', 1, 'nc_inward_list', 'nc_inward', '2009-11-26 14:19:23', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(110, 21, 'Add', 'inward.php?perform=add', '', 0, 'inward.php?perform=add', 2, 'nc_inward_add', 'nc_inward', '2009-11-26 14:20:28', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(111, 20, 'List', 'knowledge-share.php?perform=list', '', 0, 'knowledge-share.php?perform=list', 1, 'nc_ksh_list', 'nc_ksh', '2009-11-26 14:21:21', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(112, 20, 'Add', 'knowledge-share.php?perform=add', '', 0, 'knowledge-share.php?perform=add', 2, 'nc_ksh_add', 'nc_ksh', '2009-11-26 14:22:06', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(113, 22, 'List', 'login-stats.php?perform=list', '', 0, 'login-stats.php?perform=list', 1, 'nc_lg_sts', 'nc_lg_sts', '2009-11-26 14:23:49', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(114, 23, 'Page List', 'staticpages.php?perform=list', '', 0, 'staticpages.php?perform=list', 1, 'nc_site_sp_list', 'nc_site', '2009-11-26 14:27:18', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(115, 23, 'Add&nbsp;Page', 'staticpages.php?perform=add', '', 0, 'staticpages.php?perform=add', 2, 'nc_site_sp_add', 'nc_site', '2009-11-26 14:28:12', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(116, 23, 'Menu&nbsp;List', 'menu-settings.php?perform=list', '', 0, 'menu-settings.php?perform=list', 3, 'nc_site_menu_list', 'nc_site', '2009-11-26 14:29:07', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(117, 23, 'Add&nbsp;Menu', 'menu-settings.php?perform=add', '', 0, 'menu-settings.php?perform=add', 4, 'nc_site_menu_add', 'nc_site', '2009-11-26 14:30:05', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(118, 23, 'Ticker&nbsp;List', 'ticker.php?perform=list', '', 0, 'ticker.php?perform=list', 5, 'nc_site_tkr_list', 'nc_site', '2009-11-26 14:30:59', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(119, 23, 'Add&nbsp;Ticker', 'ticker.php?perform=add', '', 0, 'ticker.php?perform=add', 6, 'nc_site_tkr_add', 'nc_site', '2009-11-26 14:31:48', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(120, 23, 'Manage&nbsp;CSS', 'css-edit.php', '', 0, 'css-edit.php', 7, 'nc_site_css_edit', 'nc_site', '2009-11-26 14:32:46', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(121, 23, 'News & Events List', 'news-events.php?perform=list', '', 0, 'news-events.php?perform=list', 8, 'nc_ne_list', 'nc_ne', '2009-11-26 14:34:20', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(122, 23, 'Add News & Events', 'news-events.php?perform=add', '', 0, 'news-events.php?perform=add', 9, 'nc_ne_add', 'nc_ne', '2009-11-26 14:35:18', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(123, 24, 'Prospective&nbsp;List', 'sale-quick.php?perform=list', '', 0, 'sale-quick.php?perform=list', 1, 'nc_sl_ld_qe_list', 'nc_sl_ld_qe', '2009-11-26 14:38:14', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(124, 24, 'Prospective&nbsp;Add', 'sale-quick.php?perform=add', '', 0, 'sale-quick.php?perform=add', 2, 'nc_sl_ld_qe_add', 'nc_sl_ld_qe', '2009-11-26 14:38:55', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(125, 24, 'Lead&nbsp;Assign&nbsp;To&nbsp;CRM&nbsp;Member', 'sale-lead-assign.php?perform=list', '', 0, 'sale-lead-assign.php?perform=list', 3, 'nc_sl_ld_asig_list', 'nc_sl_ld_asig', '2009-11-26 14:40:21', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(126, 24, 'CRM&nbsp;Leads', 'sale-lead.php?perform=list', '', 0, 'sale-lead.php?perform=list', 4, 'nc_sl_ld_list', 'nc_sl_ld', '2009-11-26 14:41:49', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(127, 24, 'Lead&nbsp;Assign&nbsp;To&nbsp;Marketing&nbsp;Member', 'sale-lead-assign-mr.php?perform=list', '', 0, 'sale-lead-assign-mr.php?perform=list', 5, 'nc_sl_ld_asig_mr_list', 'nc_sl_ld_asig_mr', '2009-11-26 14:42:53', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(128, 24, 'Marketing&nbsp;Leads&nbsp;List', 'sale-lead.php?perform=list_mr', '', 0, 'sale-lead.php?perform=list_mr', 6, 'nc_sl_ld_mr_list', 'nc_sl_ld', '2009-11-26 14:44:24', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(129, 24, 'Marketing&nbsp;Leads&nbsp;Add', 'sale-lead.php?perform=add_mr', '', 0, 'sale-lead.php?perform=add_mr', 5, 'nc_sl_ld_mr_add', 'nc_sl_ld', '2009-11-26 14:45:31', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(130, 25, 'Edit', 'my-profile.php?perform=edit', '', 0, 'my-profile.php?perform=edit', 1, 'nc_myprofile_edit', 'nc_myprofile', '2009-11-26 14:46:46', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(131, 25, 'Change Password', 'my-profile.php?perform=change_password', '', 0, 'my-profile.php?perform=change_password', 2, 'nc_myprofile_password', 'nc_myprofile', '2009-11-26 14:47:30', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(132, 26, 'Menu&nbsp;List', 'navigation-main.php?perform=list', '', 0, 'navigation-main.php?perform=list', 1, 'nc_nav_list', 'nc_nav', '2009-11-26 14:49:04', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(133, 26, 'Menu&nbsp;Add', 'navigation-main.php?perform=add', '', 0, 'navigation-main.php?perform=add', 2, 'nc_nav_add', 'nc_nav', '2009-11-26 14:49:48', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(134, 26, 'Sub Menu&nbsp;List', 'navigation.php?perform=list', '', 0, 'navigation.php?perform=list', 3, 'nc_nav_list', 'nc_nav', '2009-11-26 14:50:38', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(135, 26, 'Sub Menu&nbsp;Add', 'navigation.php?perform=add', '', 0, 'navigation.php?perform=add', 4, 'nc_nav_add', 'nc_nav', '2009-11-26 14:51:25', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(136, 27, 'Newsletter&nbsp;List', 'newsletter.php?perform=list', '', 0, 'newsletter.php?perform=list', 1, 'nc_nwl_list', 'nc_nwl', '2009-11-26 14:52:47', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(137, 27, 'Newsletter&nbsp;Add', 'newsletter.php?perform=add', '', 0, 'newsletter.php?perform=add', 2, 'nc_nwl_add', 'nc_nwl', '2009-11-26 14:53:48', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(138, 27, 'Newsletter&nbsp;Queue&nbsp;List&nbsp;Module', 'newsletter.php?perform=qlist', '', 0, 'newsletter.php?perform=qlist', 3, 'nc_nwl_qlist', 'nc_nwl', '2009-11-26 15:04:57', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(139, 27, 'Newsletter&nbsp;SMTP&nbsp;List', 'newsletter-smtp.php?perform=list', '', 0, 'newsletter-smtp.php?perform=list', 4, 'nc_nwl_smtp_list', 'nc_nwl_smtp', '2009-11-26 15:09:48', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(140, 27, 'Newsletter&nbsp;SMTP&nbsp;Add', 'newsletter-smtp.php?perform=add', '', 0, 'newsletter-smtp.php?perform=add', 5, 'nc_nwl_smtp_add', 'nc_nwl_smtp', '2009-11-26 15:10:46', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(141, 28, 'List', 'score-sheet.php?perform=list', '', 0, 'score-sheet.php?perform=list', 1, 'nc_ss_list', 'nc_ss', '2009-11-26 15:11:54', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(142, 28, 'Add', 'score-sheet.php?perform=add', '', 0, 'score-sheet.php?perform=add', 2, 'nc_ss_add', 'nc_ss', '2009-11-26 15:12:36', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(143, 29, 'List', 'bill-order.php?perform=list', '', 0, 'bill-order.php?perform=list', 1, 'nc_bl_or_list', 'nc_bl_or', '2009-11-26 15:13:30', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(144, 29, 'Add', 'bill-order.php?perform=add', '', 0, 'bill-order.php?perform=add', 2, 'nc_bl_or_add', 'nc_bl_or', '2009-11-26 15:14:11', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(145, 29, 'Bill&nbsp;Pre&nbsp;Order&nbsp;List', 'bill-pre-order.php?perform=list', '', 0, 'bill-pre-order.php?perform=list', 3, 'nc_bl_po_list', 'nc_bl_po', '2009-11-26 15:14:54', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(146, 29, 'Bill&nbsp;Pre&nbsp;Order&nbsp;Add', 'bill-pre-order.php?perform=add', '', 0, 'bill-pre-order.php?perform=add', 4, 'nc_bl_po_add', 'nc_bl_po', '2009-11-26 15:15:35', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(147, 30, 'List', 'payment-transaction.php?perform=list', '', 0, 'payment-transaction.php?perform=list', 1, 'nc_p_pt_list', 'nc_p_pt', '2009-11-26 15:16:45', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(148, 30, 'Add', 'payment-transaction.php?perform=add', '', 0, 'payment-transaction.php?perform=add', 2, 'nc_p_pt_add', 'nc_p_pt', '2009-11-26 15:17:23', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(149, 31, 'Project Updates List', 'project-updates.php?perform=list', '', 0, 'project-updates.php?perform=list', 1, 'nc_pu_list', 'nc_pu', '2009-11-26 15:18:22', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(150, 32, 'List Quotations', 'sale-quotation.php?perform=list', '', 0, 'sale-quotation.php?perform=list', 1, 'nc_sl_ld_q_list', 'nc_sl_ld_q', '2009-11-26 15:19:07', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(151, 32, 'Account Approval To Create Order', 'bill-receipt.php?perform=list', '', 0, 'sale-quotation.php?perform=mark_ac', 2, 'nc_sl_ld_qac_list', 'nc_sl_ld_qac', '2009-11-26 15:20:16', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(152, 32, 'Marketing&nbsp;Quotation Subject&nbsp;List', 'quotation-subject.php?perform=list', '', 0, 'quotation-subject.php?perform=list', 3, 'nc_quot_sub_list', 'nc_quot_sub', '2009-11-26 15:27:28', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(153, 32, 'Marketing&nbsp;Quotation Subject&nbsp;Add', 'quotation-subject.php?perform=add', '', 0, 'quotation-subject.php?perform=add', 4, 'nc_quot_sub_add', 'nc_quot_sub', '2009-11-26 15:28:11', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(154, 33, 'Project wise reports', 'reports.php?perform=project-wise', '', 0, 'reports.php?perform=project-wise', 1, 'nc_rp_pr', 'nc_rp', '2009-11-26 15:47:42', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(155, 33, 'Executive wise reports', 'reports.php?perform=executive-wise', '', 0, 'reports.php?perform=executive-wise', 2, 'nc_rp_ex', 'nc_rp', '2009-11-26 15:48:26', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(156, 33, 'Day wise reports', 'reports.php?perform=day-wise', '', 0, 'reports.php?perform=day-wise', 3, 'nc_rp_dy', 'nc_rp', '2009-11-26 15:49:10', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(157, 33, 'Task Reminder reports', 'reports.php?perform=task-reminder', '', 0, 'reports.php?perform=task-reminder', 4, 'nc_rp_tr', 'nc_rp', '2009-11-26 15:51:54', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(158, 33, 'Executive score reports', 'reports.php?perform=executive-score', '', 0, 'reports.php?perform=executive-score', 5, 'nc_rp_ss', 'nc_rp', '2009-11-26 15:53:15', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(159, 33, 'Monthly&nbsp;Attendance', 'reports.php?perform=month-att', '', 0, 'reports.php?perform=month-att', 6, 'nc_rp_m_att', 'nc_rp', '2009-11-26 15:54:16', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(160, 33, 'Executive&nbsp;Attendance', 'reports.php?perform=exec-month-att', '', 0, 'reports.php?perform=exec-month-att', 7, 'nc_rp_ex_m_att', 'nc_rp', '2009-11-26 15:55:22', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(161, 34, 'View&nbsp;Calendar', 'task-reminder.php?perform=view_calendar', '', 0, 'task-reminder.php?perform=view_calendar', 1, 'nc_ts_list', 'nc_ts', '2009-11-26 16:11:12', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(162, 34, 'List', 'task-reminder.php?perform=list', '', 0, 'task-reminder.php?perform=list', 2, 'nc_ts_list', 'nc_ts', '2009-11-26 16:12:21', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(163, 34, 'Add', 'task-reminder.php?perform=add', '', 0, 'task-reminder.php?perform=add', 3, 'nc_ts_add', 'nc_ts', '2009-11-26 16:13:49', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(164, 35, 'List', 'service-book.php?perform=list', '', 0, 'service-book.php?perform=list', 1, 'nc_sb_list', 'nc_sb', '2009-11-26 16:14:55', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(165, 35, 'Add', 'service_book_add', '', 0, 'service_book_add', 2, 'nc_sb_add', 'nc_sb', '2009-11-26 17:02:13', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(166, 36, 'Sms API List', 'sms-api.php?perform=list', '', 0, 'sms-api.php?perform=list', 1, 'nc_sms_api_list', 'nc_sms_api', '2009-11-26 17:04:19', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(167, 36, 'Sms API&nbsp;Add', 'sms-api.php?perform=add', '', 0, 'sms-api.php?perform=add', 2, 'nc_sms_api_add', 'nc_sms_api', '2009-11-26 17:05:58', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(168, 36, 'Sms Gateway List', 'sms-gateway.php?perform=list', '', 0, 'sms-gateway.php?perform=list', 3, 'nc_sms_gtw_list', 'nc_sms_gtw', '2009-11-26 17:06:50', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(169, 36, 'Sms Gateway&nbsp;Add', 'sms-gateway.php?perform=add', '', 0, 'sms-gateway.php?perform=add', 4, 'nc_sms_gtw_add', 'nc_sms_gtw', '2009-11-26 17:17:00', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(170, 36, 'Sms Purchase List', 'sms-purchase.php?perform=list', '', 0, 'sms-purchase.php?perform=list', 5, 'nc_sms_pr_list', 'nc_sms_pr', '2009-11-26 17:17:41', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(171, 36, 'Sms Purchase&nbsp;Add', 'sms-purchase.php?perform=add', '', 0, 'sms-purchase.php?perform=add', 6, 'nc_sms_pr_add', 'nc_sms_pr', '2009-11-26 17:18:23', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(172, 36, 'Sms Sale Client List', 'sms-sale-client.php?perform=list', '', 0, 'sms-sale-client.php?perform=list', 7, 'nc_sms_sl_list', 'nc_sms_sl', '2009-11-26 17:19:46', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(173, 36, 'Sms Sale Client Add', 'sms-sale-client.php?perform=add', '', 0, 'sms-sale-client.php?perform=add', 8, 'nc_sms_sl_add', 'nc_sms_sl', '2009-11-26 17:21:18', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(174, 36, 'Send&nbsp;SMS&nbsp;Details&nbsp;List', 'sms.php?perform=list', '', 0, 'sms.php?perform=list', 9, 'nc_sms_lst_list', 'nc_sms_lst', '2009-11-26 17:22:11', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(175, 36, 'Scheduled&nbsp;SMS&nbsp;Details&nbsp;List', 'scheduled-sms.php?perform=list', '', 0, 'scheduled-sms.php?perform=list', 10, 'nc_sms_schd_list', 'nc_sms_schd', '2009-11-26 17:26:14', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(176, 36, 'Scheduled&nbsp;SMS&nbsp;Details&nbsp;Add', 'scheduled-sms.php?perform=add', '', 0, 'scheduled-sms.php?perform=add', 11, 'nc_sms_schd_add', 'nc_sms_lst', '2009-11-26 17:28:57', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(177, 36, 'Sms&nbsp;Client&nbsp;Account&nbsp;List', 'sms-client-account.php?perform=list', '', 0, 'sms-client-account.php?perform=list', 12, 'nc_sms_ca_list', 'nc_sms_ca', '2009-11-26 17:30:32', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(178, 36, 'Sms&nbsp;Client&nbsp;Account&nbsp;Add', 'sms-client-account.php?perform=add', '', 0, 'sms-client-account.php?perform=add', 13, 'nc_sms_ca_add', 'nc_sms_ca', '2009-11-26 17:31:26', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(179, 37, 'Support Ticket List', 'support-ticket.php?perform=list', '', 0, 'support-ticket.php?perform=list', 1, 'nc_st_list', 'nc_st', '2009-11-26 17:32:30', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(180, 37, 'Support Ticket Add', 'support-ticket.php?perform=add', '', 0, 'support-ticket.php?perform=add', 2, 'nc_st_add', 'nc_st', '2009-11-26 17:33:18', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(181, 37, 'Support&nbsp;Ticket&nbsp;for&nbsp;all&nbsp;Clients', 'support-ticket.php?perform=add_all', '', 0, 'support-ticket.php?perform=add_all', 3, 'nc_st_add_all', 'nc_st', '2009-11-26 17:35:09', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(182, 37, 'Assign Support Ticket', 'support-ticket-assign.php?perform=list', '', 0, 'support-ticket-assign.php?perform=list', 4, 'nc_st_as_list', 'nc_st_as', '2009-11-26 17:36:04', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(183, 37, 'Support Ticket Priority List', 'ticket-priority.php?perform=list', '', 0, 'ticket-priority.php?perform=list', 5, 'nc_st_pr_list', 'nc_st_pr', '2009-11-26 17:37:10', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(184, 37, 'Support Ticket Priority Add', 'ticket-priority.php?perform=add', '', 0, 'ticket-priority.php?perform=add', 6, 'nc_st_pr_add', 'nc_st_pr', '2009-11-26 17:38:20', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(185, 37, 'Support Ticket Status List', 'ticket-status.php?perform=list', '', 0, 'ticket-status.php?perform=list', 7, 'nc_st_sta_list', 'nc_st_sta', '2009-11-26 17:39:25', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(186, 37, 'Support Ticket Status Add', 'ticket-status.php?perform=add', '', 0, 'ticket-status.php?perform=add', 8, 'nc_st_sta_add', 'nc_st_sta', '2009-11-26 17:40:49', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(187, 37, 'Support Ticket Template List', 'support-ticket-template.php?perform=list', '', 0, 'support-ticket-template.php?perform=list', 9, 'nc_st_t_list', 'nc_st_t', '2009-11-26 17:41:46', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(188, 37, 'Support Ticket Template Add', 'support-ticket-template.php?perform=add', '', 0, 'support-ticket-template.php?perform=add', 10, 'nc_st_t_add', 'nc_st_t', '2009-11-26 17:42:35', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(189, 38, 'List', 'incoming-calls.php?perform=list', '', 0, 'incoming-calls.php?perform=list', 1, 'nc_cl_list', 'nc_cl', '2009-11-26 17:45:38', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(190, 38, 'Add', 'incoming-calls.php?perform=add', '', 0, 'incoming-calls.php?perform=add', 2, 'nc_cl_add', 'nc_cl', '2009-11-26 17:46:25', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(191, 39, 'List', 'bill-order-upload.php?perform=list', '', 0, 'bill-order-upload.php?perform=list', 1, 'nc_bl_or_up_list', 'nc_bl_or_up', '2009-11-26 17:55:48', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(192, 39, 'Add', 'bill-order-upload.php?perform=add', '', 0, 'bill-order-upload.php?perform=add', 2, 'nc_bl_or_up_add', 'nc_bl_or_up', '2009-11-26 17:56:45', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(193, 40, 'List', 'vendors.php?perform=list', '', 0, 'vendors.php?perform=list', 1, 'nc_uv_list', 'nc_uv', '2009-11-26 17:59:35', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(194, 40, 'Add', 'vendors.php?perform=add', '', 0, 'vendors.php?perform=add', 2, 'nc_uv_add', 'nc_uv', '2009-11-26 18:00:22', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(195, 41, 'Vendor&nbsp;Communication&nbsp;List', 'vendors-support-ticket.php?perform=list', '', 0, 'vendors-support-ticket.php?perform=list', 1, 'nc_v_st_list', 'nc_v_st', '2009-11-26 18:04:16', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(196, 41, 'Vendor&nbsp;Communication&nbsp;Add', 'vendors-support-ticket.php?perform=add', '', 0, 'vendors-support-ticket.php?perform=add', 2, 'nc_v_st_add', 'nc_v_st', '2009-11-26 18:06:13', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(197, 41, 'Assign&nbsp;Support&nbsp;Ticket', 'vendors-support-ticket-assign.php?perform=list', '', 0, 'vendors-support-ticket-assign.php?perform=list', 3, 'nc_v_st_as_list', 'nc_v_st_as', '2009-11-26 18:07:23', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(198, 42, 'Vendor&nbsp;Communication&nbsp;Template&nbsp;List', 'vendors-support-ticket-template.php?perform=list', '', 0, 'vendors-support-ticket-template.php?perform=list', 1, 'nc_v_st_t_list', 'nc_v_st_t', '2009-11-26 18:10:49', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(199, 42, 'Vendor&nbsp;Communication&nbsp;Template&nbsp;Add', 'vendors-support-ticket-template.php?perform=add', '', 0, 'vendors-support-ticket-template.php?perform=add', 2, 'nc_v_st_t_add', 'nc_v_st_t', '2009-11-26 18:11:46', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(200, 43, 'List', 'visitor-book.php?perform=list', '', 0, 'visitor-book.php?perform=list', 1, 'nc_vb_list', 'nc_vb', '2009-11-26 18:12:41', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(201, 43, 'Add', 'visitor-book.php?perform=add', '', 0, 'visitor-book.php?perform=add', 2, 'nc_vb_add', 'nc_vb', '2009-11-26 18:13:17', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(202, 44, 'Website Access List', 'web-access.php?perform=list', '', 0, 'web-access.php?perform=list', 1, 'nc_wb_ac_ls', 'nc_wb_ac', '2009-11-26 18:14:12', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(203, 44, 'Website Access Add', 'web-access.php?perform=add', '', 0, 'web-access.php?perform=add', 2, 'nc_wb_ac_add', 'nc_wb_ac', '2009-11-26 18:44:50', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(204, 44, 'Mark&nbsp;White/Black&nbsp;List', 'web-access.php?perform=mark', '', 0, 'web-access.php?perform=mark', 3, 'nc_wb_ac_mark', 'nc_wb_ac', '2009-11-26 18:45:58', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(205, 44, 'Group', 'web-access-groups.php?perform=list', '', 0, 'web-access-groups.php?perform=list', 4, 'nc_wb_ac_gr_ls', 'nc_wb_ac', '2009-11-26 18:46:52', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(206, 45, 'Manage&nbsp;Currency&nbsp;List', 'currency.php?perform=list', '', 0, 'currency.php?perform=list', 1, 'nc_cur_list', 'nc_cur', '2009-11-26 18:49:25', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(207, 45, 'Manage&nbsp;Currency&nbsp;Add', 'currency.php?perform=add', '', 0, 'currency.php?perform=add', 2, 'nc_cur_add', 'nc_cur', '2009-11-26 18:50:13', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(208, 45, 'Service&nbsp;Division&nbsp;List', 'service-division.php?perform=list', '', 0, 'service-division.php?perform=list', 3, 'nc_sr_div_list', 'nc_sr_div', '2009-11-26 18:51:05', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(209, 45, 'Service&nbsp;Division&nbsp;Add', 'service-division.php?perform=add', '', 0, 'service-division.php?perform=add', 4, 'nc_sr_div_add', 'nc_sr_div', '2009-11-26 18:51:52', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(210, 45, 'Services&nbsp;List', 'services-main.php?perform=list', '', 0, 'services-main.php?perform=list', 5, 'nc_sr_list', 'nc_sr', '2009-11-26 18:52:44', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(211, 45, 'Services&nbsp;Add', 'services-main.php?perform=add', '', 0, 'services-main.php?perform=add', 6, 'nc_sr_add', 'nc_sr', '2009-11-26 18:53:30', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(212, 45, 'Sub Services&nbsp;List', 'services.php?perform=list', '', 0, 'services.php?perform=list', 7, 'nc_sr_list', 'nc_sr', '2009-11-26 18:54:21', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(213, 45, 'Sub Services&nbsp;Add', 'services.php?perform=add', '', 0, 'services.php?perform=add', 8, 'nc_sr_add', 'nc_sr', '2009-11-26 18:55:10', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(214, 45, 'Manage&nbsp;Tax&nbsp;Name&nbsp;List', 'manage-tax.php?perform=list', '', 0, 'manage-tax.php?perform=list', 9, 'nc_mt_list', 'nc_mt', '2009-11-26 18:56:28', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(215, 45, 'Manage&nbsp;Tax&nbsp;Name&nbsp;Add', 'manage-tax.php?perform=add', '', 0, 'manage-tax.php?perform=add', 10, 'nc_mt_add', 'nc_mt', '2009-11-26 18:57:29', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(216, 45, 'Manage&nbsp;Tax&nbsp;Price&nbsp;List', 'manage-tax-price.php?perform=list', '', 0, 'manage-tax-price.php?perform=list', 11, 'nc_mtp_list', 'nc_mtp', '2009-11-26 18:58:15', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(217, 45, 'Manage&nbsp;Tax&nbsp;Price&nbsp;Add', 'manage-tax-price.php?perform=add', '', 0, 'manage-tax-price.php?perform=add', 12, 'nc_mtp_add', 'nc_mtp', '2009-11-26 18:59:02', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(218, 45, 'Manage&nbsp;Vendor&nbsp;List', 'manage-vendors.php?perform=list', '', 0, 'manage-vendors.php?perform=list', 13, 'nc_vendor_list', 'nc_vendor', '2009-11-26 18:59:58', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(219, 45, 'Manage&nbsp;Vendor&nbsp;Add', 'manage-vendors.php?perform=add', '', 0, 'manage-vendors.php?perform=add', 14, 'nc_vendor_add', 'nc_vendor', '2009-11-26 19:00:50', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(220, 46, 'Marketing&nbsp;Source&nbsp;List', 'sale-source.php?perform=list', '', 0, 'sale-source.php?perform=list', 1, 'nc_sl_sr_list', 'nc_sl_sr', '2009-11-26 19:03:03', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(221, 46, 'Marketing&nbsp;Source&nbsp;Add', 'sale-source.php?perform=add', '', 0, 'sale-source.php?perform=add', 2, 'nc_sl_sr_add', 'nc_sl_sr', '2009-11-26 19:05:17', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(222, 46, 'Marketing&nbsp;Campaign&nbsp;List', 'sale-campaign.php?perform=list', '', 0, 'sale-campaign.php?perform=list', 3, 'nc_sl_camp_list', 'nc_sl_camp', '2009-11-26 19:06:07', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(223, 46, 'Marketing&nbsp;Campaign&nbsp;Add', 'sale-campaign.php?perform=add', '', 0, 'sale-campaign.php?perform=add', 4, 'nc_sl_camp_add', 'nc_sl_camp', '2009-11-26 19:07:09', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(224, 46, 'Marketing&nbsp;Industry&nbsp;List', 'sale-industry.php?perform=list', '', 0, 'sale-industry.php?perform=list', 5, 'nc_sl_ind_list', 'nc_sl_ind', '2009-11-26 19:07:55', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(225, 46, 'Marketing&nbsp;Industry&nbsp;Add', 'sale-industry.php?perform=add', '', 0, 'sale-industry.php?perform=add', 6, 'nc_sl_ind_add', 'nc_sl_ind', '2009-11-26 19:08:48', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(226, 46, 'Marketing&nbsp;Rivals&nbsp;List', 'sale-rivals.php?perform=list', '', 0, 'sale-rivals.php?perform=list', 7, 'nc_sl_riv_list', 'nc_sl_riv', '2009-11-26 19:09:40', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(227, 46, 'Marketing&nbsp;Rivals&nbsp;Add', 'sale-rivals.php?perform=add', '', 0, 'sale-rivals.php?perform=add', 8, 'nc_sl_riv_add', 'nc_sl_riv', '2009-11-26 19:10:30', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(228, 46, 'Marketing&nbsp;Followup Title&nbsp;List', 'sale-followup-title.php?perform=list', '', 0, 'sale-followup-title.php?perform=list', 9, 'nc_sl_flw_tl_list', 'nc_sl_flw_tl', '2009-11-26 19:14:03', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(229, 46, 'Marketing&nbsp;Followup Title&nbsp;Add', 'sale-followup-title.php?perform=add', '', 0, 'sale-followup-title.php?perform=add', 10, 'nc_sl_flw_tl_add', 'nc_sl_flw_tl', '2009-11-26 19:14:51', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(230, 34, 'Activity&nbsp;List', 'jobs.php?perform=list', '', 0, 'jobs.php?perform=list', 4, 'nc_jb_list', 'nc_jb', '2009-11-26 19:15:52', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(231, 34, 'Activity&nbsp;Add', 'jobs.php?perform=add', '', 0, 'jobs.php?perform=add', 5, 'nc_jb_add', 'nc_jb', '2009-11-26 19:16:45', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(232, 47, 'Payment In/Out-Account head List', 'payment-manage-account-head.php?perform=list', '', 0, 'payment-manage-account-head.php?perform=list', 3, 'nc_p_ah_list', 'nc_p_ah', '2009-11-26 19:19:10', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(233, 47, 'Payment In/Out-Account head Add', 'payment-manage-account-head.php?perform=add', '', 0, 'payment-manage-account-head.php?perform=add', 4, 'nc_p_ah_add', 'nc_p_ah', '2009-11-26 19:20:10', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(234, 47, 'Payment Party Bills List', 'payment-party-bills.php?perform=list', '', 0, 'payment-party-bills.php?perform=list', 1, 'nc_p_pb_list', 'nc_p_pb', '2009-11-26 19:21:37', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(235, 47, 'Payment Party Bills Add', 'payment-party-bills.php?perform=add', '', 0, 'payment-party-bills.php?perform=add', 2, 'nc_p_pb_add', 'nc_p_pb', '2009-11-26 19:22:26', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(236, 47, 'Payment In/Out-Manage Bank List', 'payment-manage-bank.php?perform=list', '', 0, 'payment-manage-bank.php?perform=list', 5, 'nc_p_mb_list', 'nc_p_mb', '2009-11-26 19:25:06', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(237, 47, 'Payment In/Out-Manage Bank Add', 'payment-manage-bank.php?perform=add', '', 0, 'payment-manage-bank.php?perform=add', 6, 'nc_p_mb_add', 'nc_p_mb', '2009-11-26 19:25:52', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(238, 47, 'Payment In/Out-Manage Party List', 'payment-manage-party.php?perform=list', '', 0, 'payment-manage-party.php?perform=list', 7, 'nc_p_mp_list', 'nc_p_mp', '2009-11-26 19:26:49', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(239, 47, 'Payment In/Out-Manage Party Add', 'payment-manage-party.php?perform=add', '', 0, 'payment-manage-party.php?perform=add', 8, 'nc_p_mp_add', 'nc_p_mp', '2009-11-26 19:27:48', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8');
";
echo $db->query($sql);

echo $sql = "INSERT INTO `navigation` (`id`, `parent_id`, `title`, `page`, `image`, `level`, `selected_page`, `sequence`, `perm_name`, `main_perm_name`, `do_e`, `status`, `ip`, `access_level`, `created_by`) VALUES
(240, 47, 'Payment In/Out-Manage Mode List', 'payment-manage-payment-mode.php?perform=list', '', 0, 'payment-manage-payment-mode.php?perform=list', 9, 'nc_p_pm_list', 'nc_p_pm', '2009-11-26 19:28:38', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(241, 47, 'Payment In/Out-Manage Mode Add', 'payment-manage-payment-mode.php?perform=add', '', 0, 'payment-manage-payment-mode.php?perform=add', 10, 'nc_p_pm_add', 'nc_p_pm', '2009-11-26 19:29:27', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(242, 48, 'Project Status List', 'project-status.php?perform=list', '', 0, 'project-status.php?perform=list', 1, 'nc_pts_sta_list', 'nc_pts_sta', '2009-11-26 19:30:13', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(243, 48, 'Project Status Add', 'project-status.php?perform=add', '', 0, 'project-status.php?perform=add', 2, 'nc_pts_sta_add', 'nc_pts_sta', '2009-11-26 19:31:14', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(244, 48, 'Project Type List', 'project-type.php?perform=list', '', 0, 'project-type.php?perform=list', 3, 'nc_pts_type_list', 'nc_pts_type', '2009-11-26 19:32:45', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(245, 48, 'Project Type Add', 'project-type.php?perform=add', '', 0, 'project-type.php?perform=add', 4, 'nc_pts_type_add', 'nc_pts_type', '2009-11-26 19:33:32', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(246, 48, 'Project Priority List', 'project-priority.php?perform=list', '', 0, 'project-priority.php?perform=list', 5, 'nc_pts_pr_list', 'nc_pts_pr', '2009-11-26 19:34:53', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(247, 48, 'Project Priority Add', 'project-priority.php?perform=add', '', 0, 'project-priority.php?perform=add', 6, 'nc_pts_pr_add', 'nc_pts_pr', '2009-11-26 19:35:41', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(248, 48, 'Project Resolution List', 'project-resolution.php?perform=list', '', 0, 'project-resolution.php?perform=list', 7, 'nc_pts_res_list', 'nc_pts_res', '2009-11-26 19:36:30', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(249, 48, 'Project Resolution Add', 'project-resolution.php?perform=add', '', 0, 'project-resolution.php?perform=add', 8, 'nc_pts_res_add', 'nc_pts_res', '2009-11-26 19:37:04', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(250, 48, 'Project Task Template List', 'project-task-template.php?perform=list', '', 0, 'project-task-template.php?perform=list', 9, 'nc_pt_t_list', 'nc_pt_t', '2009-11-26 19:37:45', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8'),
(251, 48, 'Project Task Template Add', 'project-task-template.php?perform=add', '', 0, 'project-task-template.php?perform=add', 10, 'nc_pt_t_add', 'nc_pt_t', '2009-11-26 19:38:26', '1', '192.168.1.79', 32767, '50a9c7dbf0fa09e8969978317dca12e8');
";
echo $db->query($sql);
*/


//2009-11-nov-27 
/*
echo $sql = "ALTER TABLE `score_sheet` ADD `comment_to_name` TEXT NOT NULL AFTER `comment_to`" ;
echo $db->query($sql);

echo $sql = "ALTER TABLE `score_sheet` ADD `created_by_name` VARCHAR( 255 ) NOT NULL AFTER `created_by`" ;
echo $db->query($sql);
//2009-11-nov-27

echo $sql="ALTER TABLE `contact_information` ADD `salutation` VARCHAR( 20 ) NOT NULL AFTER `id` ;";
echo $db->query($sql);

echo $sql="ALTER TABLE `contact_information` ADD `contact_no` VARCHAR( 255 ) NOT NULL AFTER `phone`" ;
echo $db->query($sql);

echo $sql="ALTER TABLE `contact_information` ADD `ip` VARCHAR( 50 ) NOT NULL AFTER `organization` ,
ADD `site_id` INT NOT NULL AFTER `ip` " ;
echo $db->query($sql);

echo $sql="ALTER TABLE `site_settings` ADD `total_enquiries_posted_ask` INT NOT NULL AFTER `total_enquiries_posted` ,
ADD `total_enquiries_posted_task` INT NOT NULL AFTER `total_enquiries_posted_ask` ,
ADD `total_enquiries_posted_buzz` INT NOT NULL AFTER `total_enquiries_posted_task` ,
ADD `total_product_enquiries_posted_ask` INT NOT NULL AFTER `total_enquiries_posted_buzz` ";

echo $db->query($sql);

echo $sql="CREATE TABLE IF NOT EXISTS `askop_post_product_inquiry` (
  `id` int(11) NOT NULL auto_increment,
  `prod_name` varchar(100) NOT NULL,
  `prod_id` bigint(20) NOT NULL,
  `cat_id` bigint(20) NOT NULL,
  `prod_url` varchar(255) NOT NULL,
  `product_type` varchar(20) NOT NULL,
  `salutation` varchar(255) NOT NULL,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `fax` varchar(15) NOT NULL,
  `email` varchar(80) NOT NULL,
  `desig` varchar(100) NOT NULL,
  `org` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `zipcode` varchar(16) NOT NULL,
  `comments` text NOT NULL,
  `user_id` varchar(40) NOT NULL,
  `agree` enum('0','1') NOT NULL default '0',
  `date` datetime NOT NULL,
  `ip` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";

echo $db->query($sql);

echo $sql=" ALTER TABLE `contact_information` CHANGE `site_id` `site_id` INT( 11 ) NOT NULL DEFAULT '1'";
echo $db->query($sql);
*/

 //2009-11-nov-28
 /*
 echo $sql= "ALTER TABLE `askop_post_product_inquiry` ADD `category_name` VARCHAR( 255 ) NOT NULL AFTER `cat_id`" ;
 echo $db->query( $sql);
*/
/*
 echo $sql= "
 CREATE TABLE IF NOT EXISTS `order_bifurcation` (
  `id` int(11) NOT NULL auto_increment,
  `ord_id` int(11) NOT NULL,
  `ord_bifc_service` int(11) NOT NULL,
  `details` text NOT NULL,
  `price` double NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_by_name` varchar(255) NOT NULL,
  `do_e` datetime NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;" ;
 echo $db->query( $sql);
 
 echo $sql= "CREATE TABLE IF NOT EXISTS `order_bifurcation_services` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL default '1',
  `created_by` varchar(255) NOT NULL,
  `do_e` datetime NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
 " ;
 echo $db->query( $sql);
 */
 
 
 //2009-12-dec-06
/*
 echo $sql= "ALTER TABLE `payment_party_bills` ADD `number` VARCHAR( 40 ) NOT NULL AFTER `party_id` ";
echo $db->query( $sql);

echo $sql= "ALTER TABLE `payment_party_bills` ADD `late_pay_dt` DATETIME NOT NULL AFTER `pay_due_dt` ";
echo $db->query( $sql);


echo $sql= "ALTER TABLE `payment_party_bills` ADD `bill_amount` DOUBLE NOT NULL AFTER `amount` ,
ADD `late_amount` DOUBLE NOT NULL AFTER `bill_amount` ,
ADD `balance` DOUBLE NOT NULL AFTER `late_amount` ";
echo $db->query( $sql);



echo $sql= "ALTER TABLE `payment_party_bills` ADD `period_from` DATETIME NOT NULL AFTER `balance` ,
ADD `period_to` DATETIME NOT NULL AFTER `period_from` ";
echo $db->query( $sql);

echo $sql= "ALTER TABLE `payment_party_bills` ADD `bill_status` ENUM( '0', '1' ) NOT NULL DEFAULT
'0' COMMENT '0 for unpaid 1 for paid' AFTER `period_to`" ;
echo $db->query( $sql);

echo $sql= "CREATE TABLE `payment_bills_transaction` (
`id` INT NOT NULL AUTO_INCREMENT ,
`transaction_id` INT NOT NULL ,
`bill_id` INT NOT NULL ,
`amount` DOUBLE NOT NULL ,
`details` TEXT NOT NULL ,
`created_by` VARCHAR( 255 ) NOT NULL ,
`ip` VARCHAR( 25 ) NOT NULL ,
`date` DATETIME NOT NULL ,
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM ";
echo $db->query( $sql);
*/
/*
echo $sql= "RENAME TABLE `vendors` TO `vendors_2`" ;
echo $db->query( $sql);*/

/*
echo $sql= "CREATE TABLE IF NOT EXISTS `vendors` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` varchar(40) NOT NULL default '',
  `parent_id` varchar(40) NOT NULL,
  `service_id` text NOT NULL,
  `number` mediumint(11) unsigned NOT NULL default '0',
  `manager` varchar(40) NOT NULL,
  `team` text NOT NULL,
  `username` varchar(80) NOT NULL default '',
  `password` varchar(40) NOT NULL default '',
  `access_level` smallint(5) unsigned NOT NULL default '0',
  `created_by` varchar(40) NOT NULL,
  `roles` varchar(50) NOT NULL default '',
  `status` enum('0','1','2','3') NOT NULL default '2',
  `credit_limit` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL,
  `lead_assign_to` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL default '',
  `email_1` varchar(80) NOT NULL default '',
  `email_2` varchar(80) NOT NULL default '',
  `email_3` varchar(100) NOT NULL,
  `email_4` varchar(100) NOT NULL,
  `additional_email` text NOT NULL,
  `title` varchar(10) NOT NULL default '',
  `f_name` varchar(20) NOT NULL default '',
  `m_name` varchar(20) NOT NULL default '',
  `l_name` varchar(20) NOT NULL default '',
  `p_name` varchar(20) NOT NULL default '',
  `desig` varchar(100) NOT NULL default '',
  `org` varchar(200) NOT NULL default '',
  `domain` varchar(100) NOT NULL default '',
  `gender` enum('m','f','o') NOT NULL default 'm',
  `do_birth` datetime NOT NULL default '0000-00-00 00:00:00',
  `do_aniv` datetime NOT NULL default '0000-00-00 00:00:00',
  `do_add` datetime NOT NULL default '0000-00-00 00:00:00',
  `do_reg` datetime NOT NULL default '0000-00-00 00:00:00',
  `policy_check` enum('0','1') NOT NULL default '1',
  `ip_reg` varchar(100) NOT NULL,
  `do_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `allow_ip` enum('1','2') NOT NULL default '1' COMMENT '1 for ANY and 2 for Specified Ip ',
  `valid_ip` text NOT NULL COMMENT 'Valid ip list comma seperated',
  `remarks` text NOT NULL,
  `lead_id` int(11) NOT NULL,
  `industry` text NOT NULL,
  `wt_you_do` text NOT NULL,
  `mobile1` varchar(20) NOT NULL,
  `mobile2` varchar(20) NOT NULL,
  `spouse_dob` date NOT NULL,
  `alloted_clients` text NOT NULL,
  `billing_name` varchar(255) NOT NULL,
  `is_soft_copy` enum('0','1','2') NOT NULL default '0',
  `send_mail` enum('0','1') NOT NULL default '1' COMMENT 'if 1 then we will send updates to person, if 0 then updates will not sent to him',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
echo $db->query( $sql);

*/

/*
$db1 		= new db_local; // database handle

$sql="select * from  vendors_2";     
if ( $db->query($sql) ) {
    if ( $db->nf() > 0 ) {            
        while ($db->next_record()) {
        
            $user_id = $db->f('user_id') ;
            $parent_id = $db->f('parent_id') ;
            $service_id = $db->f('service_id') ;
            $number = $db->f('number') ;
            $manager = $db->f('manager') ;
            $team = $db->f('team') ;
            $username = $db->f('username') ;
            $password = $db->f('password') ;
            $access_level = $db->f('access_level') ;
            $created_by = $db->f('created_by') ;
            $roles = $db->f('roles') ;
            $status = $db->f('status') ;
            $credit_limit = $db->f('credit_limit') ;
            $grade = $db->f('grade') ;
            $lead_assign_to = $db->f('lead_assign_to') ;
            $email = $db->f('email') ;
            $email_1 = $db->f('email_1') ;
            $email_2 = $db->f('email_2') ;
            $email_3 = $db->f('email_3') ;
            $email_4 = $db->f('email_4') ;
            $additional_email = $db->f('additional_email') ;
            $title = $db->f('title') ;
            $f_name = $db->f('f_name') ;
            $m_name = $db->f('m_name') ;
            $l_name = $db->f('l_name') ;
            $p_name = $db->f('p_name') ;
            $desig = $db->f('desig') ;
            $org = $db->f('org') ;
            $domain = $db->f('domain') ;
            $gender = $db->f('gender') ;
            $do_birth = $db->f('do_birth') ;
            $do_add = $db->f('do_add') ;
            $do_reg = $db->f('do_reg') ;
            $policy_check = $db->f('policy_check') ;
            $ip_reg = $db->f('ip_reg') ;
            $do_login = $db->f('do_login') ;
            $remarks = $db->f('remarks') ;
            $industry = $db->f('industry') ;
            $wt_you_do = $db->f('wt_you_do') ;
            $mobile1 = $db->f('mobile1') ;
            $mobile2 = $db->f('mobile2') ;
            $billing_name = $db->f('billing_name') ;
            
                 echo$query	= " INSERT INTO vendors
                             SET user_id = '".     processUserData($user_id) ."'"  
                            .",parent_id = '". 	   processUserData($parent_id) ."'"
                            .",service_id = '".    processUserData($service_id)."'"
                            .",number = '".        processUserData($number) ."'"
                            .",manager = '".       processUserData($manager) ."'"
                            .",team = '".          processUserData($team) ."'"
                            .",username = '".      processUserData($username) ."'"
                            .",password = '".      processUserData($password) ."'"
                            .",access_level = '".  processUserData($access_level) ."'"
                            .",created_by = '".    processUserData($created_by) ."'"
                            .",roles = '".         $roles ."'"
                            .",status = '".        $status ."'"
                            .",credit_limit = '".  $credit_limit ."'"
                            .",grade = '".         $grade ."'"
                            .",lead_assign_to = '".$lead_assign_to ."'"
                            .",email = '".         processUserData($email) ."'"
                            .",email_1= '".        processUserData($email_1) ."'"
                            .",email_2= '".        processUserData($email_2) ."'"
                            .",email_3= '".        processUserData($email_3) ."'"
                            .",email_4= '".         processUserData($email_4) ."'"
                            .",additional_email= '".processUserData($additional_email) ."'"
                            .",title= '".         processUserData($title) ."'"
                            .",f_name= '".         processUserData($f_name) ."'"
                            .",m_name= '".         processUserData($m_name )."'"
                            .",l_name= '".         processUserData($l_name) ."'"
                            .",p_name= '".         processUserData($p_name) ."'"
                            .",desig= '".         processUserData($desig) ."'"
                            .",org = '".          processUserData($org)  ."'"
                            .",domain = '".       processUserData($domain)  ."'"
                            .",gender = '".       $gender  ."'"
                            .",do_birth = '".       $do_birth  ."'"
                            .",do_add = '".       $do_add  ."'"
                            .",do_reg = '".       $do_reg  ."'"
                            .",policy_check = '".  $policy_check  ."'"
                            .",ip_reg = '".        $ip_reg  ."'"
                            .",do_login = '".       $do_login  ."'"
                            .",remarks = '".       processUserData($remarks)  ."'"
                            .",industry = '".       processUserData($industry)  ."'"
                            .",wt_you_do = '".       processUserData($wt_you_do)  ."'"
                            .",mobile1 = '".       $mobile1  ."'"
                            .",mobile2 = '".       $mobile2  ."'"
                            .",billing_name = '".     processUserData($billing_name)  ."'" ;
                           
             echo "<br/>";
             $db1->query( $query);
        }
    }
}    
*/
/*
$query="RENAME TABLE `vendors_2`  TO `vendors_notused`" ;
 $db->query( $query);
*/
/*
$db1 		= new db_local; // database handle
$number=100;
$sql="select id from  payment_party_bills order by id";     
if ( $db->query($sql) ) {
    if ( $db->nf() > 0 ) {            
        while ($db->next_record()) {
            $number++;
            $id = $db->f('id') ;
            echo    $query	= " UPDATE payment_party_bills
                             SET number = '".$number ."' WHERE id='".$id."'";
            echo "<br/>";     
            $db1->query($query);
        }
    
    }
}
*/
/*
 echo $query="update `payment_party_bills` SET balance = amount" ;
 $db->query($query) ;
 */


/* 
$sql="ALTER TABLE `clients` DROP INDEX `user_id_2`";
$db->query($sql) ;

$sql="ALTER TABLE `user` DROP INDEX `user_id_2` ";
$db->query($sql);

$sql="ALTER TABLE `hr_attendance` DROP INDEX `att_id` ";
$db->query($sql);

$sql="ALTER TABLE `hr_attendance` ADD INDEX `uid` ( `uid` ) ";
$db->query($sql) ;

$sql="ALTER TABLE `announcement` ADD INDEX `idstatus` ( `status` ) ";
$db->query($sql);


$sql="ALTER TABLE `billing_orders` DROP INDEX `or_no` ,
ADD INDEX `or_no` ( `number` , `status` ) ";
$db->query($sql);

$sql="ALTER TABLE `billing_orders` DROP INDEX `or_no` ,
ADD INDEX `or_no` ( `status` , `number` ) ";
$db->query($sql);

$sql="ALTER TABLE `clients` DROP INDEX `user_id` ";
$db->query($sql);

$sql="ALTER TABLE `user` DROP INDEX `user_id` ";
$db->query($sql);

$sql="ALTER TABLE `clients` ADD INDEX `idx_p` ( `parent_id` ) ";
$db->query($sql);
*/

/*2009-12-dec-20*/
/*
echo $sql="ALTER TABLE `project_task_details` ADD `mail_send_to_su` TEXT NOT NULL AFTER `ticket_no` ";
echo $db->query($sql);


echo $sql="update ticket_tickets set ticket_owner_uid='626599a0acfb6c65e7153afa788a2da0',ticket_owner='M R Pimplapure'
where ticket_no=4032 ";
echo $db->query($sql);*/

/*2009-12-dec-29
 echo $sql="select username,password ,number from user where number=5061 "    ;exit;
       if ( $db->query($sql) ) {
            if ( $db->nf() > 0 ) {            
                while ($db->next_record()) {
                echo $db->f('username') ;
                echo "<br/>";
                echo $db->f('password') ;
                 echo "<br/>";
                echo $db->f('number') ;
                
                
                }
            }
        }   */


//2009-12-dec-27
/*
 echo $sql="ALTER TABLE `clients` ADD `updated_by` VARCHAR( 255 ) NOT NULL AFTER `gender` ,
ADD `updated_by_name` INT( 255 ) NOT NULL AFTER `updated_by` ,
ADD `do_update` DATETIME NOT NULL AFTER `updated_by_name`";
 $db->query($sql) ;*/
//2010-01-jan-04
/*
$sql="ALTER TABLE `task_reminder` ADD `type` INT NOT NULL AFTER `task`";
echo $db->query($sql);
$sql="ALTER TABLE `task_reminder_history` ADD `type` INT NOT NULL AFTER `task`";
echo $db->query($sql);
*/  
//2010-01-jan-12
/*
 echo $sql="UPDATE user set number=5259 where number=5251 "   ;
 $db->query($sql) ;
*/


//2010-10-jan-20
/*
$sql="RENAME TABLE `payment_bills_transaction`  TO `payment_transaction_bills`" ;
echo $db->query($sql);

echo $sql="ALTER TABLE `payment_transaction_bills` ADD `balance_amt` DOUBLE NOT NULL AFTER `amount`";
echo $db->query($sql);*/
/*
echo $sql="ALTER TABLE `payment_transaction` ADD `period_from` DATETIME NOT NULL AFTER `voucher_no` ,
ADD `period_to` DATETIME NOT NULL AFTER `period_from` " ;
echo $db->query($sql);
*/


//2010-01-jan-25
/*
echo $sql="CREATE TABLE `items` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 255 ) NOT NULL ,
`status` ENUM( '0', '1', '2' ) NOT NULL ,
`total_quantity` INT NOT NULL ,
`created_by` VARCHAR( 200 ) NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL ,
`do_e` DATETIME NOT NULL 
) ENGINE = MYISAM ";
echo $db->query($sql);

echo $sql="CREATE TABLE `items_purchase` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`item_id` INT NOT NULL COMMENT 'Ref Id from item',
`item_name` VARCHAR( 255 ) NOT NULL ,
`item_description` TEXT NOT NULL ,
`code` VARCHAR( 255 ) NOT NULL ,
`model_no` VARCHAR( 200 ) NOT NULL ,
`serial_no` VARCHAR( 200 ) NOT NULL ,
`batch_no` VARCHAR( 200 ) NOT NULL ,
`brand` VARCHAR( 200 ) NOT NULL ,
`expiry_dt` DATETIME NOT NULL ,
`quantity` INT NOT NULL ,
`balance_quantity` INT NOT NULL ,
`price` DOUBLE NOT NULL ,
`vendor_id` VARCHAR( 200 ) NOT NULL ,
`remarks` TEXT NOT NULL ,
`do_e` DATETIME NOT NULL ,
`created_by` VARCHAR( 200 ) NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL 
) ENGINE = MYISAM" ;

echo $db->query($sql);

echo $sql="CREATE TABLE IF NOT EXISTS `items_sold` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itm_purchase_id` int(11) NOT NULL COMMENT 'Ref Id from item_purchase',
  `order_id` int(11) NOT NULL,
  `vendor_id` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sale_price` double NOT NULL,
  `remark` text NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `do_e` datetime NOT NULL,
  `ip` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 " ;
echo $db->query($sql);



echo $sql="ALTER TABLE `items_purchase` ADD `pp_bill_id` INT NOT NULL COMMENT 'Ref id from payment party bills' AFTER `vendor_id`" ;
echo $db->query($sql);

echo $sql="CREATE TABLE `company_document` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`document` VARCHAR( 255 ) NOT NULL ,
`file_name` VARCHAR( 255 ) NOT NULL ,
`do_e` DATETIME NOT NULL ,
`access_level` SMALLINT NOT NULL ,
`created_by` VARCHAR( 255 ) NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL 
) ENGINE = MYISAM ";
echo $db->query($sql);

echo $sql="CREATE TABLE `company_document_send` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`cd_id` INT NOT NULL COMMENT 'Ref id from company_document',
`name` VARCHAR( 255 ) NOT NULL ,
`email` VARCHAR( 255 ) NOT NULL ,
`send_by` VARCHAR( 255 ) NOT NULL ,
`do_e` DATETIME NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL 
) ENGINE = MYISAM ";

echo $db->query($sql);
*/
/*
 echo $sql="select party_id ,number,amount,balance from payment_party_bills "    ;
   if ( $db->query($sql) ) {
        if ( $db->nf() > 0 ) {            
            while ($db->next_record()) {
            echo "party id ".$db->f('party_id') ;
            echo "  ";
            echo "amount ".$db->f('amount') ;
            echo "balance ".$db->f('balance') ;
             echo "<br/>";
           
            
            
            }
        }
    } 
*/
       
/*    
 echo $sql='SELECT * FROM `payment_party`
where id =43';

if ( $db->query($sql) ) {
    if ( $db->nf() > 0 ) {            
        while ($db->next_record()) {  
         
        echo "fname ".$db->f('fname') ;
        echo "  ";
        echo "lname ".$db->f('lname') ;
        echo "billing_name ".$db->f('billing_name') ;
        echo " company_name ".$db->f('company_name') ;
         echo "<br/>";
       
        
        
        }
    }
}

*/

//2010-02-feb-11
/*
echo $sql="ALTER TABLE `payment_party_bills` CHANGE `party_id` `party_id` VARCHAR( 255 ) NOT NULL"    ;
    echo $db->query($sql) */
    
//2010-02-feb-13
/*
echo $sql="CREATE TABLE `payment_bills_against` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`vendor_id` VARCHAR( 255 ) NOT NULL ,
`bill_against` VARCHAR( 255 ) NOT NULL ,
`status` ENUM( '0', '1' ) NOT NULL ,
`ip` VARCHAR( 50 ) NOT NULL ,
`do_e` INT NOT NULL ,
`created_by` VARCHAR( 255 ) NOT NULL 
) ENGINE = MYISAM 
"    ;
    echo $db->query($sql) ;

echo $sql="ALTER TABLE `payment_party_bills` ADD `bill_against_id` INT NOT NULL AFTER `executive_id` ";
echo $db->query($sql) ;*/

/*
echo $sql="ALTER TABLE `payment_bills_against` CHANGE `do_e` `do_e` DATETIME NOT NULL ";
echo $db->query($sql) ;
*/

//2010-02-feb-17
/*
echo $sql="ALTER TABLE `payment_transaction` CHANGE `party_id` `party_id` VARCHAR( 255 ) NOT NULL ";
echo $db->query($sql) ;*/

//2010-03-mar-28
/*
echo $sql="ALTER TABLE `ticket_tickets` ADD `sms_to` TEXT NOT NULL AFTER `mail_to_additional_email` ,
ADD `sms_to_number` TEXT NOT NULL AFTER `sms_to`";
echo $db->query($sql) ;

//2010-04-apr-08

echo $sql="ALTER TABLE `payment_transaction` ADD `service_id` INT NOT NULL COMMENT 'Ref id from settings_services' AFTER `user_id`";
echo $db->query($sql) ;


echo $sql="ALTER TABLE `ticket_tickets` ADD `ticket_attachment_1` VARCHAR( 100 ) NOT NULL AFTER `ticket_attachment` ,
ADD `ticket_attachment_2` VARCHAR( 100 ) NOT NULL AFTER `ticket_attachment_1`";
echo $db->query($sql) ;*/
/*
echo $sql="ALTER TABLE `vendors_ticket_tickets` ADD `ticket_attachment_1` VARCHAR( 100 ) NOT NULL AFTER `ticket_attachment` ,
ADD `ticket_attachment_2` VARCHAR( 100 ) NOT NULL AFTER `ticket_attachment_1`";
echo $db->query($sql) ;


echo $sql="ALTER TABLE `vendors_ticket_tickets` ADD `sms_to` TEXT NOT NULL AFTER `mail_to_additional_email` ,
ADD `sms_to_number` TEXT NOT NULL AFTER `sms_to`";
echo $db->query($sql) ;

*/

//
/* 2010-04-apr-19
echo $sql="ALTER TABLE `task_reminder` ADD ` last_comment` TEXT NOT NULL AFTER `on_behalf_id` ,
ADD `last_comment_dt` DATETIME NOT NULL AFTER ` last_comment` ,
ADD `last_comment_by` VARCHAR( 40 ) NOT NULL AFTER `last_comment_dt` ,
ADD `last_comment_by_name` VARCHAR( 255 ) NOT NULL AFTER `last_comment_by` ,
ADD `last_comment_to` VARCHAR( 40 ) NOT NULL AFTER `last_comment_by_name` ,
ADD `last_comment_to_name` VARCHAR( 255 ) NOT NULL AFTER `last_comment_to` ";
echo $db->query($sql) ;
echo $sql="ALTER TABLE `task_reminder` CHANGE ` last_comment` `last_comment` TEXT CHARACTER SET 
latin1 COLLATE latin1_swedish_ci NOT NULL ";
echo $db->query($sql) ;
echo $sql="ALTER TABLE `task_reminder` ADD `task_no` BIGINT NOT NULL AFTER `task`";
echo $db->query($sql) ;
*/
/*
echo $sql="ALTER TABLE `task_reminder` CHANGE `last_comment_by` `last_comment_by` VARCHAR( 100 ) 
CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `last_comment_to` `last_comment_to` VARCHAR( 100 ) CHARACTER SET 
latin1 COLLATE latin1_swedish_ci NOT NULL";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder` CHANGE `created_by` `created_by` VARCHAR( 100 ) 
CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_log` ADD `task_no` BIGINT NOT NULL AFTER `task_id`";
echo $db->query($sql) ;
echo $sql="ALTER TABLE `task_reminder_log` CHANGE `by_id` `by_id` 
VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";
echo $db->query($sql) ;

*/

/*
echo $sql="UPDATE  `user` SET `access_level` 
= '65535' WHERE `user`.`number` =  '5061' ";
echo $db->query($sql) ;
*/


//2010-04-apr-21
/*
echo $sql=" ALTER TABLE `payment_transaction` ADD `financial_yr` VARCHAR( 50 ) NOT NULL AFTER `transaction_id`";
echo $db->query($sql) ;
*/

/*
 $sql="select * from  task_reminder limit 0,1";     
   if ( $db->query($sql) ) {
        if ( $db->nf() > 0 ) {            
            $db->next_record();
             echo " id ".$db->f('id') ;         echo "<br/>";
             echo " task".$db->f('task') ;         echo "<br/>";
             echo " task_no".$db->f('task_no') ;         echo "<br/>";
             echo "   	job_id".$db->f('job_id') ;         echo "<br/>";
             echo "   	order_id".$db->f('order_id') ;         echo "<br/>";
             echo "   	comment".$db->f('comment') ;         echo "<br/>";
             echo "   	headers_imap".$db->f('headers_imap') ;         echo "<br/>";
             echo "   do_r".$db->f('do_r') ;         echo "<br/>";
             echo "   do_deadline".$db->f('do_deadline') ;         echo "<br/>";
             echo "   hrs".$db->f('hrs') ;         echo "<br/>";
             echo "   min".$db->f('min') ;         echo "<br/>";
             echo "   time_type".$db->f('time_type') ;         echo "<br/>";
             echo "   do_completion".$db->f('do_completion') ;         echo "<br/>";
             echo "   do_e".$db->f('do_e') ;         echo "<br/>";
             echo "   status".$db->f('status') ;         echo "<br/>";
             echo "   priority".$db->f('priority') ;         echo "<br/>";
             echo "   allotted_to".$db->f('allotted_to') ;         echo "<br/>";
             echo "   allotted_to_client".$db->f('allotted_to_client') ;         echo "<br/>";
             echo "   allotted_to_lead".$db->f('allotted_to_lead') ;         echo "<br/>";
             echo "   allotted_to_addr".$db->f('allotted_to_addr') ;         echo "<br/>";
             echo "   on_behalf_id".$db->f('on_behalf_id') ;         echo "<br/>";
             echo "   last_comment".$db->f('last_comment') ;         echo "<br/>";
             echo "   last_comment_dt".$db->f('last_comment_dt') ;         echo "<br/>";
             echo "   last_comment_by".$db->f('last_comment_by') ;         echo "<br/>";
             echo "   last_comment_by_name".$db->f('last_comment_by_name') ;         echo "<br/>";
             echo "   last_comment_to".$db->f('last_comment_to') ;         echo "<br/>";
             echo "   last_comment_to_name".$db->f('last_comment_to_name') ;         echo "<br/>";
             echo "   access_level".$db->f('access_level') ;         echo "<br/>";
            
        }
    }


*/
//2010-04-apr-22
/*
echo $sql="ALTER TABLE `task_reminder` ADD `allotted_to_name` TEXT NOT NULL AFTER `allotted_to` ";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder` ADD `allotted_to_client_name` TEXT NOT NULL AFTER `allotted_to_client`";
echo $db->query($sql) ;

echo $sql=" ALTER TABLE `task_reminder` ADD `allotted_to_lead_name` TEXT NOT NULL AFTER `allotted_to_lead`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder` ADD `allotted_to_addr_name` TEXT NOT NULL AFTER `allotted_to_addr`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder` ADD `allotted_to_vendor` TEXT NOT NULL AFTER `allotted_to_addr_name` ,
ADD `allotted_to_vendor_name` TEXT NOT NULL AFTER `allotted_to_vendor`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder` CHANGE `allotted_to_vendor` `allotted_to_vendor` TEXT CHARACTER SET 
latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Users From Vendors'";
echo $db->query($sql) ;


echo $sql="ALTER TABLE `payment_transaction` ADD `number` BIGINT NOT NULL";
echo $db->query($sql) ;
    */

//2010-04-apr-24
/*
echo $sql="ALTER TABLE `task_reminder` CHANGE `last_comment_to` `last_comment_to` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `last_comment_to_name` `last_comment_to_name` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_history` ADD `task_no` BIGINT NOT NULL AFTER `task_id`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_history` ADD `allotted_to_name` TEXT NOT NULL AFTER `allotted_to`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_history` ADD `allotted_to_client_name` TEXT NOT NULL AFTER `allotted_to_client`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_history` ADD `allotted_to_lead_name` TEXT NOT NULL AFTER `allotted_to_lead`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_history` ADD `allotted_to_addr_name` TEXT NOT NULL AFTER `allotted_to_addr`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_history` ADD `allotted_to_vendor` TEXT NOT NULL AFTER `allotted_to_addr_name` ,
ADD `allotted_to_vendor_name` TEXT NOT NULL AFTER `allotted_to_vendor`";
echo $db->query($sql) ;


echo $sql="ALTER TABLE `task_reminder` ADD `on_behalf_id_name` VARCHAR( 255 ) NOT NULL AFTER `on_behalf_id`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_history` ADD `on_behalf_id_name` VARCHAR( 255 ) NOT NULL AFTER `on_behalf_id`";
echo $db->query($sql) ;

echo $sql="ALTER TABLE `task_reminder_history` ADD `updated_by_name` VARCHAR( 255 ) NOT NULL AFTER `updated_by`";
echo $db->query($sql) ;
*/

//2010-04-apr-25
/*
echo $sql="ALTER TABLE `task_reminder` ADD `completed_by` VARCHAR( 200 ) NOT NULL AFTER `do_completion` ,
ADD `completed_by_name` VARCHAR( 255 ) NOT NULL AFTER `completed_by`";
echo $db->query($sql) ;
*/
/*

echo $sql="ALTER TABLE `task_reminder` ADD `mail_send_to` TEXT NOT NULL AFTER `on_behalf_id_name`";
echo $db->query($sql) ;
echo $sql="ALTER TABLE `task_reminder_history` ADD `mail_send_to` TEXT NOT NULL AFTER `on_behalf_id_name`";
echo $db->query($sql) ;
echo $sql="ALTER TABLE `task_reminder_log` ADD `mail_send_to` TEXT NOT NULL AFTER `comment`";
echo $db->query($sql) ;
echo $sql="ALTER TABLE `task_reminder` ADD `mail_send_to_client` TEXT NOT NULL AFTER `mail_send_to` ,
ADD `mail_send_to_vendor` TEXT NOT NULL AFTER `mail_send_to_client`";
echo $db->query($sql) ;
echo $sql="ALTER TABLE `task_reminder_log` ADD `mail_send_to_client` TEXT NOT NULL AFTER `mail_send_to` ,
ADD `mail_send_to_vendor` TEXT NOT NULL AFTER `mail_send_to_client`";
echo $db->query($sql) ;
echo $sql="ALTER TABLE `task_reminder_history` ADD `mail_send_to_client` TEXT NOT NULL AFTER `mail_send_to` ,
ADD `mail_send_to_vendor` TEXT NOT NULL AFTER `mail_send_to_client`";
echo $db->query($sql) ;
*/
/*
$sql1= "update task_reminder set task_no= 0";
echo  $db->query($sql1);


 $dbnew=new db_local();    
 $task_no  =  Taskreminder::getNewNumber($db);
 $sql="select * from  task_reminder order by id ASC";     
   if ( $db->query($sql) ) {
       if ( $db->nf() > 0 ) {            
            while ($db->next_record()) {
             $id= $db->f('id') ;
                $task_no  =  Taskreminder::getNewNumber($dbnew);
               echo  $sql1= "update task_reminder set task_no= ".$task_no." WHERE id ='".$id."'";
                echo    $dbnew->query($sql1);
                echo "<br/>";
            }
        }
    }
*/
/*
echo $query="ALTER TABLE `task_reminder` ADD `headers_imap` TEXT NOT NULL AFTER `comment`";
echo $db->query($query) ;
*/
/*

 $sql="select * from  task_reminder limit 0,1";     
   if ( $db->query($sql) ) {
        if ( $db->nf() > 0 ) {            
            $db->next_record();
             echo " id ".$db->f('id') ;         echo "<br/>";
             echo " task".$db->f('task') ;         echo "<br/>";
             echo " task_no".$db->f('task_no') ;         echo "<br/>";
             echo "   	job_id".$db->f('job_id') ;         echo "<br/>";
             echo "   	order_id".$db->f('order_id') ;         echo "<br/>";
             echo "   	comment".$db->f('comment') ;         echo "<br/>";
             echo "   	headers_imap".$db->f('headers_imap') ;         echo "<br/>";
             echo "   do_r".$db->f('do_r') ;         echo "<br/>";
             echo "   do_deadline".$db->f('do_deadline') ;         echo "<br/>";
             echo "   hrs".$db->f('hrs') ;         echo "<br/>";
             echo "   min".$db->f('min') ;         echo "<br/>";
             echo "   time_type".$db->f('time_type') ;         echo "<br/>";
             echo "   do_completion".$db->f('do_completion') ;         echo "<br/>";
             echo "   do_e".$db->f('do_e') ;         echo "<br/>";
             echo "   status".$db->f('status') ;         echo "<br/>";
             echo "   priority".$db->f('priority') ;         echo "<br/>";
             echo "   allotted_to".$db->f('allotted_to') ;         echo "<br/>";
             echo "   allotted_to_client".$db->f('allotted_to_client') ;         echo "<br/>";
             echo "   allotted_to_lead".$db->f('allotted_to_lead') ;         echo "<br/>";
             echo "   allotted_to_addr".$db->f('allotted_to_addr') ;         echo "<br/>";
             echo "   on_behalf_id".$db->f('on_behalf_id') ;         echo "<br/>";
             echo "   last_comment".$db->f('last_comment') ;         echo "<br/>";
             echo "   last_comment_dt".$db->f('last_comment_dt') ;         echo "<br/>";
             echo "   last_comment_by".$db->f('last_comment_by') ;         echo "<br/>";
             echo "   last_comment_by_name".$db->f('last_comment_by_name') ;         echo "<br/>";
             echo "   last_comment_to".$db->f('last_comment_to') ;         echo "<br/>";
             echo "   last_comment_to_name".$db->f('last_comment_to_name') ;         echo "<br/>";
             echo "   access_level".$db->f('access_level') ;         echo "<br/>";
            
        }
    }*/
    /*
    echo $query="ALTER TABLE `payment_party_bills` CHANGE `number` `number` BIGINT NOT NULL";
echo $db->query($query) ;*/
/*
echo $sql1="UPDATE payment_party_bills set number=101 where id=2 " ;
echo $db->query($sql1) ;
echo $sql1="UPDATE payment_party_bills set number=102 where id=3 " ;
echo $db->query($sql1) ;
echo $sql1="UPDATE payment_party_bills set number=103 where id=4 " ;
echo $db->query($sql1) ;


echo $sql1="UPDATE payment_party_bills set number=1001 where id=902 " ;
echo $db->query($sql1) ;
echo $sql1="UPDATE payment_party_bills set number=1002 where id=903 " ;
echo $db->query($sql1) ;
echo $sql1="UPDATE payment_party_bills set number=1003 where id=904 " ;
echo $db->query($sql1) ;*/
/*
/*
$sql="select number from  payment_party_bills  where id in(2,3,4)";     
       if ( $db->query($sql) ) {
            if ( $db->nf() > 0 ) {            
                while ($db->next_record()) {
                echo $db->f('do_delete') ;
                }
            }
        }  
*/

//2010-04-apr-30
/*
echo $sql1="ALTER TABLE `ticket_tickets` ADD `attachment_imap` TEXT NOT NULL AFTER `headers_imap`";
echo $db->query($sql1) ;
*/
/*
echo $sql="select  headers_imap,ticket_no from  ticket_tickets  where ticket_no = 5052";     
       if ( $db->query($sql) ) {
            if ( $db->nf() > 0 ) {            
                while ($db->next_record()) {
                echo $db->f('headers_imap') ;
                echo $db->f('ticket_no') ;
                }
            }
        } 
 	 */
/*
echo $sql="ALTER TABLE `vendors_ticket_tickets` ADD `attachment_imap` TEXT NOT NULL AFTER `headers_imap` ";
echo $db->query($sql);
*/
/*

echo $sql="select * from  task_reminder  where task_no = 3683";     
if ( $db->query($sql) ) {
    if ( $db->nf() > 0 ) {            
        while ($db->next_record()) {
        echo $db->f('allotted_to') ;
        echo "<br/>";
        echo $db->f('allotted_to_name') ;
        }
    }
} 
*/

/*
echo $sql="ALTER TABLE `ticket_tickets` ADD `last_comment_by` VARCHAR( 100 ) NOT NULL AFTER `last_comment` ,
ADD `last_comment_by_name` VARCHAR( 255 ) NOT NULL AFTER `last_comment_by`";
$db->query($sql) ;

echo $sql="ALTER TABLE `vendors_ticket_tickets` ADD `last_comment_by` VARCHAR( 100 ) NOT NULL AFTER `last_comment` ,
ADD `last_comment_by_name` VARCHAR( 255 ) NOT NULL AFTER `last_comment_by`" ;

$db->query($sql) ;
*/

//2010-05-apr-5
 /*
echo $sql="ALTER TABLE `payment_party_bills` ADD `created_by_name` VARCHAR( 255 ) NOT NULL AFTER `created_by`";
$db->query($sql) ;
echo $sql="ALTER TABLE `payment_party_bills` ADD `updated_by` VARCHAR( 200 ) NOT NULL AFTER `created_by_name` ,
ADD `updated_by_name` VARCHAR( 255 ) NOT NULL AFTER `updated_by`" ;
$db->query($sql) ;
echo $sql="ALTER TABLE `payment_party_bills` ADD `do_u` DATETIME NOT NULL AFTER `do_e`" ;
$db->query($sql) ;5143,,5142
*/

//2010-05-may-23
/*
echo $sql=" UPDATE ticket_tickets set `ticket_date` = '".mktime(0, 0, 0, 5, 13, 2010)."'  WHERE ticket_no IN(5123,5122,5121 )";
$db->query($sql);

echo $sql=" UPDATE ticket_tickets set `ticket_date` = '".mktime(0, 0, 0, 5, 12, 2010)."'  WHERE ticket_no IN(5119,5118)";
$db->query($sql);

echo $sql=" UPDATE ticket_tickets set `ticket_date` = '".mktime(0, 0, 0, 5, 11, 2010)."'  WHERE ticket_no IN(5115)";
$db->query($sql);
echo $sql=" UPDATE ticket_tickets set `ticket_date` = '".mktime(0, 0, 0, 5, 10, 2010)."'  WHERE ticket_no IN(5114)";
$db->query($sql);
*/
/*
echo $sql=" UPDATE ticket_tickets set `ticket_date` = '".mktime(0, 0, 0, 5, 8, 2010)."'  WHERE ticket_no IN(5113)";
$db->query($sql);*/

//2010-05-may-25
/*
echo $sql=" ALTER TABLE `billing_orders` ADD `clients_su` TEXT NOT NULL AFTER `team`" ;
$db->query($sql);
*/
/*
echo $sql=" ALTER TABLE `ticket_tickets` ADD `order_id` BIGINT NOT NULL COMMENT 'assign order, then ST will be visible to all members of that order' AFTER `assign_members`";
$db->query($sql);

echo $sql="ALTER TABLE `ticket_tickets` ADD `order_details` VARCHAR( 255 ) NOT NULL AFTER `order_id`";
$db->query($sql);
*/
//2010-05-may-25
/*
echo $sql="ALTER TABLE `hr_attendance` CHANGE `pstatus` `pstatus` ENUM( '1', '2', '3', '4', '5' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Presenty status 1 for Present,2 for absent,3 for paidleave,4 nonpaidleave'";
$db->query($sql);

echo $sql="UPDATE `billing_orders` SET team = CONCAT(',' , team , ',') ";
$db->query($sql);*/
/*
echo $sql="ALTER TABLE `ticket_tickets` ADD `hrs` VARCHAR( 50 ) NOT NULL AFTER `ticket_response_time`  ,
ADD `min` VARCHAR( 50 ) NOT NULL AFTER `hrs` ,
ADD `hrs1` VARCHAR( 50 ) NOT NULL AFTER `min` ,
ADD `min1` VARCHAR( 50 ) NOT NULL AFTER `hrs1` ";
$db->query($sql);
*/

//2010-05-may-29
/*
 echo $sql="update `payment_party_bills` set  `amount`=0,`balance`=0";
 $db->query($sql);

  echo $sql="ALTER TABLE `payment_party_bills` ADD `after_pay_dt` DATETIME NOT NULL AFTER `late_pay_dt`";
  $db->query($sql);
 
  echo $sql="ALTER TABLE `payment_party_bills` ADD `after_amount` DOUBLE NOT NULL AFTER `late_amount`";
 $db->query($sql);
*/

//2010-06-jun-04
/*
 echo $sql="ALTER TABLE `payment_transaction_bills` ADD `created_by_name` VARCHAR( 255 ) NOT NULL AFTER `created_by`";
 $db->query($sql);*/
 
 
 
 /*

 echo $sql="update `payment_transaction` set  remarks='Transaction Type- Payment In.
Credited To- Tirupati Urban Co.Bank
A/c no -2102

CHEQUE DETAILS-

Bank Name- State Bank of Travancore
Cheque No- 054340
Cheque Dt- 25 May,2010
Amount - 2500
Client Name- Anuradha Paul' WHERE id=1104";
 
 echo $db->query($sql);
*/

echo $sql = "update `billing_orders` set clients_su='' where number='PT-ORD-2010-2011-57'";
echo $db->query($sql);

?>
    