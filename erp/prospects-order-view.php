<?php
    if ( $perm->has('nc_ps_or_details') ) {
        $or_id = isset ($_GET['or_id']) ? $_GET['or_id'] : ( isset($_POST['or_id'] ) ? $_POST['or_id'] :'');

        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        $access_level += 1;
     

        $condition_query = " WHERE (". TABLE_PROSPECTS_ORDERS .".id = '". $or_id ."' "
                                ." OR ". TABLE_PROSPECTS_ORDERS .".number = '". $or_id ."')";
 
        $fields = TABLE_PROSPECTS_ORDERS .'.*'
                .','. TABLE_PROSPECTS .'.user_id AS c_user_id'
                .','. TABLE_PROSPECTS .'.manager AS c_manager'
                .','. TABLE_PROSPECTS .'.number AS c_number'
                .','. TABLE_PROSPECTS .'.f_name AS c_f_name'
                .','. TABLE_PROSPECTS .'.l_name AS c_l_name'
                .','. TABLE_PROSPECTS .'.email AS c_email'
                .','. TABLE_PROSPECTS .'.status AS c_status';
        if ( ProspectsOrder::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];

            if ( $_ALL_POST['access_level'] < $access_level ) {
                include ( DIR_FS_INCLUDES .'/user.inc.php');
                // Read the Order Creator's Information.
                $_ALL_POST['creator']= '';
                User::getList($db, $_ALL_POST['creator'], 'user_id,username,number,email,f_name,l_name', 
				" WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                $_ALL_POST['creator'] = $_ALL_POST['creator'][0];
                
                // Read the Project Managers Information.
                $_ALL_POST['projectmanager']= '';
                User::getList($db, $_ALL_POST['projectmanager'], 'user_id,username,number,email,f_name,l_name', "
				WHERE user_id = '". $_ALL_POST['project_manager'] ."'");
                $_ALL_POST['projectmanager'] = $_ALL_POST['projectmanager'][0];
               
                // Read the Client Managers Information.
                $_ALL_POST['manager']= '';
                User::getList($db, $_ALL_POST['manager'], 'user_id,username,number,email,f_name,l_name', " WHERE user_id = '". $_ALL_POST['c_manager'] ."'");
                $_ALL_POST['manager'] = $_ALL_POST['manager'][0];
                
                // Read the Team Members Information.
                $_ALL_POST['team'] = "'". implode("','", (explode(',', $_ALL_POST['team']))) ."'";
                $_ALL_POST['team_members']= '';
                User::getList($db, $_ALL_POST['team_members'], 'user_id,username,number,email,f_name,l_name', " WHERE user_id IN (". $_ALL_POST['team'] .")");
                
                $_ALL_POST['access_level'] = getAccessLevel($db, $_ALL_POST['access_level'], true);
                $_ALL_POST['access_level'] = $_ALL_POST['access_level'][0];
                
                
                $condition_query='';
                $condition_query = " WHERE ord_no = '". $_ALL_POST['number'] ."'";
				ProspectsOrder::getParticulars($db, $temp, '*', $condition_query);
                $_ALL_POST['particulars']=$temp;
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-order-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Order with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("Records not found to view.");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>