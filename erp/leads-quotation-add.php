<?php
    if ( $perm->has('nc_ld_qt_add') ) {
        //include_once ( DIR_FS_INCLUDES .'/currency.inc.php');      
       include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php');
       include_once ( DIR_FS_INCLUDES .'/financial-year.inc.php');
       include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php');
       include_once ( DIR_FS_INCLUDES .'/leads-quotation.inc.php');
	   
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $exchange_rate          = isset($_POST["exchange_rate"]) ? $_POST["exchange_rate"] : '1' ;
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level'];
        
        $_ALL_POST['exchange_rate'] = $exchange_rate; 
        
        $extra = array( 'db' 				=> &$db,
                        'access_level'      => $access_level,
                         'messages'          => &$messages
                        );
                        
        $currency = NULL ;
        $required_fields = '*' ;
		Currency::getList($db,$currency,$required_fields);        
        //echo $time_start = microtime(true); 
		//Get company list 
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                   .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
      
        //getfinancial yr bof
        $lst_fyr = null;
       // $lst_fyr = FinancialYear::yearRange();
        //getfinancial yr eof        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn']) || isset($_POST['btnReturnRcp']) ) && 
		$_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
			$files      = processUserData($_FILES);
			$data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_FILE_SIZE ;
            $data['max_mfile_size'] = MAX_MULTIPLE_FILE_SIZE ;
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,                        
                            'messages'          => &$messages
                        );
            $data['do_c'] = time();
         
           
            if ( LeadsQuotation::validateAdd($data, $extra) ) {
                if(!empty($files['attach_file1']["name"])){
                    $filedata["extension"]	=	'';
                    $filedata = pathinfo($files['attach_file1']["name"]);
                    $ext = $filedata["extension"] ; 
                    $attachfilename = $data['number']."-file1.".$ext ;
                    $data['attach_file1'] = $attachfilename;
                    if(move_uploaded_file($files['attach_file1']['tmp_name'], DIR_FS_QUOTATIONS."/".$attachfilename)){
                        @chmod(DIR_FS_QUOTATIONS."/".$attachfilename,0777);
                    }
                }
				if(!empty($files['attach_file2']["name"])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['attach_file2']["name"]);
                    $ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-file2.".$ext ;
                    $data['attach_file2'] = $attachfilename;
                    if(move_uploaded_file($files['attach_file2']['tmp_name'], DIR_FS_QUOTATIONS."/".$attachfilename)){
                        @chmod(DIR_FS_QUOTATIONS."/".$attachfilename,0777);
                    }
                }				
				if(!empty($files['attach_file3']["name"])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['attach_file3']["name"]);
                    $ext = $filedata["extension"] ; 
                    $attachfilename = $data['number']."-file3.".$ext ;
                    $data['attach_file3'] = $attachfilename;
                    if(move_uploaded_file($files['attach_file3']['tmp_name'], DIR_FS_QUOTATIONS."/".$attachfilename)){
                        @chmod(DIR_FS_QUOTATIONS."/".$attachfilename,0777);
                    }
                }
				
				if(!empty($files['attach_file4']["name"])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['attach_file4']["name"]);
                    $ext = $filedata["extension"] ; 
                    $attachfilename = $data['number']."-file4.".$ext ;
                    $data['attach_file4'] = $attachfilename;
                    if(move_uploaded_file($files['attach_file4']['tmp_name'], DIR_FS_QUOTATIONS."/".$attachfilename)){
                        @chmod(DIR_FS_QUOTATIONS."/".$attachfilename,0777);
                    }
                }
				if(!empty($files['attach_file5']["name"])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['attach_file5']["name"]);                   
                    $ext = $filedata["extension"] ; 
                    $attachfilename = $data['number']."-file5.".$ext ;
                    $data['attach_file5'] = $attachfilename;
                    if(move_uploaded_file($files['attach_file5']['tmp_name'], DIR_FS_QUOTATIONS."/".$attachfilename)){
                        @chmod(DIR_FS_QUOTATIONS."/".$attachfilename,0777);
                    }
                }
                //get currency abbr from d/b bof
                $mail_client = 0;
                if(isset($data['mail_client'])){
                    $mail_client = 1;
                }
                $currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                $fields1 = "*";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);              
                if(!empty($currency1)){
                   $data['currency_abbr'] = $currency1[0]['abbr'];
                   $data['currency_name'] = $currency1[0]['currency_name'];
                   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
                   $data['currency_country'] = $currency1[0]['country_name'];
                }
                //get currency abbr from d/b eof                      
                $company1 =null;
                $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                 $fields_c1 = "name,prefix,quot_prefix, tin_no,service_tax_regn,pan_no,cst_no,vat_no,do_implement,do_implement_st";
                Company::getList( $db, $company1, $fields_c1, $condition_query_c1); 
				$data['do_tax']=$data['do_st']=0;
				$do_tax=$do_st='';
                if(!empty($company1)){                  
                   $data['company_name'] = $company1[0]['name'];
                   $data['company_prefix'] = $company1[0]['prefix'];
				   $data['company_quot_prefix'] = $company1[0]['quot_prefix'];
				   $data['do_tax']     = $company1[0]['do_implement'];	
				   $data['do_st']     = $company1[0]['do_implement_st'];	
				   
				   $data['tin_no'] 			 = '';
				   $data['service_tax_regn'] 	 = '';
				   $data['pan_no'] 			 = $company1[0]['pan_no'];
				   $data['cst_no'] 			 = '';
				   $data['vat_no'] 			 = '';
				   
				    if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
						$do_tax=$data['do_tax'];
						$data['do_tax']= strtotime($data['do_tax']);				
					}
					if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){
					   $data['tin_no'] 			 = $company1[0]['tin_no'];
					   $data['service_tax_regn'] = $company1[0]['service_tax_regn'];					  
					   $data['cst_no'] 			 = $company1[0]['cst_no'];
					   $data['vat_no'] 			 = $company1[0]['vat_no'];
					}
					if( $data['do_st']!=0 &&  $data['do_st']!='0000:00:00 00:00:00'){
						$do_st = $data['do_st'];
						$data['do_st'] = strtotime($data['do_st']);				
					}
					if($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00'){
					   $data['service_tax_regn'] = $company1[0]['service_tax_regn'];
					}
                }
                $data['invoice_title']= ' PROPOSAL ';
                
                //Get billing address BOF
                 include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                $region         = new RegionLead();
                $region->setAddressOf(TABLE_SALE_LEADS,  $data['client']['user_id']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $address_list  =  $address_list;
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
                //$data['b_address'] = $address_list ;
                //Get billing address EOF


                if(!empty($data['do_e'])){
                    $data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                    $data['is_renewable']=1;
                }else{
                    $data['do_e'] ='';
                    $data['is_renewable']=0;
                }
                if(!empty($data['do_fe'])){
                    $data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }else{
                    $data['do_fe'] ='';
                }
              
                $sub_str='';
                
                  $query	= " INSERT INTO ".TABLE_LEADS_QUOTATION
				." SET ". TABLE_LEADS_QUOTATION .".number = '".        $data['number'] ."'"
				.",". TABLE_LEADS_QUOTATION .".inv_counter = '".         $data['inv_counter'] ."'"
				.",". TABLE_LEADS_QUOTATION .".quotation_subject = '".  $data['quotation_subject'] ."'"
				.",". TABLE_LEADS_QUOTATION .".other_details = '".  $data['other_details'] ."'"		
				.",". TABLE_LEADS_QUOTATION .".order_closed_by = '".         $data['order_closed_by'] ."'"
				.",". TABLE_LEADS_QUOTATION .".or_id = '".         $data['or_id'] ."'"						
				.",". TABLE_LEADS_QUOTATION .".or_no = '".         $data['or_no'] ."'"
				.",". TABLE_LEADS_QUOTATION .".service_id = '". ",".trim($data['service_id'],",").","."'"
				.",". TABLE_LEADS_QUOTATION .".tax_ids = '".         $data['tax_ids'] ."'"
				.",". TABLE_LEADS_QUOTATION .".delivery_at = '".   $data['delivery_at'] ."'"
				.",". TABLE_LEADS_QUOTATION .".access_level = '".  $my['access_level'] ."'"
				.",". TABLE_LEADS_QUOTATION .".created_by = '".     $my['user_id'] ."'"
				.",". TABLE_LEADS_QUOTATION .".client = '".        $data['client']['user_id'] ."'"  
				.",". TABLE_LEADS_QUOTATION .".currency_abbr = '". processUserData($data['currency_abbr']) ."'"
				.",". TABLE_LEADS_QUOTATION .".currency_id = '".   $data['currency_id'] ."'"
				.",". TABLE_LEADS_QUOTATION .".currency_name = '".   processUserData($data['currency_name']) ."'"
				.",". TABLE_LEADS_QUOTATION .".currency_symbol = '". processUserData($data['currency_symbol']) ."'"
				.",". TABLE_LEADS_QUOTATION .".currency_country = '". processUserData($data['currency_country']) ."'"
				.",". TABLE_LEADS_QUOTATION .".exchange_rate = '". $data['exchange_rate'] ."'"
				.",". TABLE_LEADS_QUOTATION .".amount = '".        $data['amount'] ."'"
				.",". TABLE_LEADS_QUOTATION .".round_off = '".     $data['round_off'] ."'"
				.",". TABLE_LEADS_QUOTATION .".round_off_op = '".  $data['round_off_op'] ."'"
				.",". TABLE_LEADS_QUOTATION .".dont_show_total = '".  $data['dont_show_total'] ."'"
				.",". TABLE_LEADS_QUOTATION .".amount_inr = '".    $data['amount_inr'] ."'"
				.",". TABLE_LEADS_QUOTATION .".amount_words = '".  $data['amount_words'] ."'"
				.",". TABLE_LEADS_QUOTATION .".do_c = '".          date('Y-m-d H:i:s', $data['do_c']) ."'"
				.",". TABLE_LEADS_QUOTATION .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
				.",". TABLE_LEADS_QUOTATION .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
				.",". TABLE_LEADS_QUOTATION .".do_e = '".          $data['do_e'] ."'"
				.",". TABLE_LEADS_QUOTATION .".do_fe = '".         $data['do_fe']."'"
				.",". TABLE_LEADS_QUOTATION .".attach_file1 = '".   $data['attach_file1'] ."'"
				.",". TABLE_LEADS_QUOTATION .".attach_file2 = '".   $data['attach_file2'] ."'"
				.",". TABLE_LEADS_QUOTATION .".attach_file3 = '".   $data['attach_file3'] ."'"
				.",". TABLE_LEADS_QUOTATION .".attach_file4 = '".   $data['attach_file4'] ."'"
				.",". TABLE_LEADS_QUOTATION .".attach_file5 = '".   $data['attach_file5'] ."'"
				.",". TABLE_LEADS_QUOTATION .".remarks = '".       $data['remarks'] ."'"
				.",". TABLE_LEADS_QUOTATION .".balance = '".       $data['balance'] ."'"
				.",". TABLE_LEADS_QUOTATION .".balance_inr = '".   $data['amount_inr'] ."'"
				.",". TABLE_LEADS_QUOTATION .".company_id = '".    $data['company_id'] ."'"
				.",". TABLE_LEADS_QUOTATION .".company_name = '". processUserData($data['company_name']) ."'"
				.",". TABLE_LEADS_QUOTATION .".tin_no   		   = '". $data['tin_no'] ."'"
				.",". TABLE_LEADS_QUOTATION .".service_tax_regn   = '". $data['service_tax_regn'] ."'"
				.",". TABLE_LEADS_QUOTATION .".do_st   		  = '". $do_st ."'"
				.",". TABLE_LEADS_QUOTATION .".pan_no  		   = '". $data['pan_no'] ."'"
				.",". TABLE_LEADS_QUOTATION .".cst_no  		   = '". $data['cst_no'] ."'"
				.",". TABLE_LEADS_QUOTATION .".vat_no  		   = '". $data['vat_no'] ."'"						
				.",". TABLE_LEADS_QUOTATION .".do_tax   		  = '". $do_tax ."'"
				.",". TABLE_LEADS_QUOTATION .".company_prefix = '".processUserData($data['company_prefix']) ."'"
				.",". TABLE_LEADS_QUOTATION .".billing_name      = '".processUserData($data['billing_name']) ."'"
				.",". TABLE_LEADS_QUOTATION .".billing_address     = '".processUserData($data['billing_address']) ."'"
				.",". TABLE_LEADS_QUOTATION .".b_addr_company_name = '".processUserData($data['b_addr_company_name']) ."'"
				.",". TABLE_LEADS_QUOTATION .".b_addr              = '".processUserData($data['b_addr'])."'"
				.",". TABLE_LEADS_QUOTATION .".b_addr_city         = '".processUserData($data['b_addr_city'] )."'"
				.",". TABLE_LEADS_QUOTATION .".b_addr_state        = '".processUserData($data['b_addr_state']) ."'"
				.",". TABLE_LEADS_QUOTATION .".b_addr_country      = '".processUserData($data['b_addr_country']) ."'"
				.",". TABLE_LEADS_QUOTATION .".b_addr_zip          = '".processUserData($data['b_addr_zip']) ."'"
				.",". TABLE_LEADS_QUOTATION .".is_renewable        = '". $data['is_renewable'] ."'"
				.",". TABLE_LEADS_QUOTATION .".ip                  = '".$_SERVER['REMOTE_ADDR']."'"
				.",". TABLE_LEADS_QUOTATION .".status              = '".$data['status'] ."'" ;
				
                
                $data['amount']= number_format($data['amount'],2);
                $data['amount_inr']= number_format($data['amount_inr'],2);
                $data['balance']= number_format($data['balance'],2);
                $data['amount_words']=processSqlData($data['amount_words']);
                $data['do_i_chk'] = $data['do_i'];			 
			    $data['do_rs_symbol'] = strtotime('2010-07-20');				 
			    $data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
			    $data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
					
                    $variables['hid'] = $db->last_inserted_id();      
                    //Update in order, invoice_created =1                       
                    $query1 = "UPDATE ".TABLE_LEADS_ORDERS." SET ".TABLE_LEADS_ORDERS.".is_invoice_create='1' WHERE 
                        ".TABLE_LEADS_ORDERS.".number = '".$data['or_no']."'";
                    $db->query($query1);
                    
                    $messages->setOkMessage("New Proposal has been created.");
                    //After insert, update invoice counter in 
                    $data['sub_total_amount'] = 0;
					$data['tax1_total_amount']=$data['tax1_sub1_total_amount'] = $data['tax1_sub2_total_amount'] = 0;
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = " WHERE ".TABLE_LEADS_ORD_P.".ord_no = '". $data['or_no'] ."'";
    				$data['show_discount']=0;
                    $data['show_nos']=0;
                    $data['colsSpanNo1']=5;
                    $data['colsSpanNo2']=6; 
                    LeadsOrder::getParticulars($db, $temp, '*', $condition_query);
                    if(!empty($temp)){                    
						$data['wd1']=10;
                        $data['wdinvp']=245;//217+28
                        $data['wdpri']=98;//70+28
                        $data['wdnos']=98;//70+28
                        $data['wdamt']=97;//70+27
                        $data['wddisc']=77; //50+27
                        $data['wdst']=97;   //70+27                     
                        $data['wd2']=9;
                        
                        
                        foreach ( $temp as $pKey => $parti ) {                        
                            /*
                               if($temp[$pKey]['s_type']=='1'){
                                    $temp[$pKey]['s_type']='Years';
                               }elseif($temp[$pKey]['s_type']=='2'){
                                    $temp[$pKey]['s_type']='Quantity';
                               }
                            */     
                            if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
                                $data['show_discount'] = 1; 
                            }
                            if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
                                $data['show_nos']=1;     
                            }                            
                            $temp_p[]=array(
									'p_id'=>$temp[$pKey]['id'],
									'particulars'=>$temp[$pKey]['particulars'],
									'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,
                           			's_type' =>$temp[$pKey]['s_type'] ,                                   
									's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
									's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,
									's_id' =>$temp[$pKey]['s_id'] ,                                   
									'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
									'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
									'tax1_name' =>$temp[$pKey]['tax1_name'] ,    
									'tax1_number' =>$temp[$pKey]['tax1_number'] ,
									'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
									'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,     
									'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) , 
									'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,    
									'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                                     
									'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
									'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,     
									'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,									
									'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,    
									'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                                     
									'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
									'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,     
									'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,		
									'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                    
									'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                          
									'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,  
									'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
									'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
									);
							if(($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00')
								|| ($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00')
							){
								//if(!empty($temp[$pKey]['tax1_id'])){
									$data['sub_total_amount'] = $data['sub_total_amount'] + $temp[$pKey]['stot_amount'] ; 
									
									$data['tax1_id'] =   $temp[$pKey]['tax1_id'] ; 
									$data['tax1_number'] =   $temp[$pKey]['tax1_number'] ; 
									$data['tax1_name'] =   $temp[$pKey]['tax1_name'] ; 
									$data['tax1_value'] =   $temp[$pKey]['tax1_value'] ; 
									$data['tax1_pvalue'] =   $temp[$pKey]['tax1_pvalue'] ; 
									$data['tax1_total_amount'] =  $data['tax1_total_amount'] + $temp[$pKey]['tax1_amount'] ; 
									
									$data['tax1_sub1_id'] =   $temp[$pKey]['tax1_sub1_id'] ; 
									$data['tax1_sub1_number'] =   $temp[$pKey]['tax1_sub1_number'] ; 
									$data['tax1_sub1_name'] =   $temp[$pKey]['tax1_sub1_name'] ; 
									$data['tax1_sub1_value'] =   $temp[$pKey]['tax1_sub1_value'] ; 
									$data['tax1_sub1_pvalue'] =   $temp[$pKey]['tax1_sub1_pvalue'] ; 
									$data['tax1_sub1_total_amount'] =  $data['tax1_sub1_total_amount'] + $temp[$pKey]['tax1_sub1_amount'] ; 
									
									$data['tax1_sub2_id'] =   $temp[$pKey]['tax1_sub2_id'] ; 
									$data['tax1_sub2_number'] =   $temp[$pKey]['tax1_sub2_number'] ; 
									$data['tax1_sub2_name'] =   $temp[$pKey]['tax1_sub2_name'] ; 
									$data['tax1_sub2_value'] =   $temp[$pKey]['tax1_sub2_value'] ; 
									$data['tax1_sub2_pvalue'] =   $temp[$pKey]['tax1_sub2_pvalue'] ; 
									$data['tax1_sub2_total_amount'] =  $data['tax1_sub2_total_amount'] + 
									$temp[$pKey]['tax1_sub2_amount'] ; 
								//}
							}
                        }
                         if($data['show_discount'] ==0){ 
                            // disc : 50 & subtot : 70
                           /* $data['wdinvp']= $data['wdinvp']+20 + 40;
                            $data['wdpri'] = $data['wdpri'] + 5 + 6;
                            $data['wdamt'] = $data['wdamt'] + 5+ 6;                            
                            $data['wdtx']  = $data['wdtx'] + 5+ 6;
                            $data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            $data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							*/
							 // disc : 77 & subtot : 97
							$data['wdinvp']= $data['wdinvp']+50 + 70;
                            $data['wdpri'] = $data['wdpri'] +10 + 10;
                            $data['wdamt'] = $data['wdamt'] + 17+ 17;                            
                            //$data['wdtx']  = $data['wdtx'] + 5+ 6;
                            //$data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            //$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
                            
                        }
                        if($data['show_nos'] ==0){
                            // disc : 70
                           /* $data['wdinvp']= $data['wdinvp']+30;
                            $data['wdpri'] = $data['wdpri'] + 5;
                            $data['wdamt'] = $data['wdamt'] + 5;
                            $data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;
							*/
						    // disc :98
							$data['wdinvp']= $data['wdinvp']+80;
                            $data['wdpri'] = $data['wdpri'] + 8;
                            $data['wdamt'] = $data['wdamt'] + 10;
                            /*
							$data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;*/
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;
                            
                        }                        
                    }
					
					$sub_tax_str='';
                    if(!empty($data['tax1_name'])){
					
						//get the tax's declaration bof
						$condition1=" WHERE id= ".$data['tax1_id']." LIMIT 0,1" ;
						$required_fields='declaration';
						$parentTaxDetails=array();
						$data['tax1_declaration']='';
						ServiceTax::getDetails( $db, $parentTaxDetails, $required_fields, $condition1);
						if(!empty($parentTaxDetails)){
							$arr = $parentTaxDetails[0];
							$data['tax1_declaration'] = $arr['declaration'];
						}
						
						//get the tax's declaration eof
					
					
						$sqlu  = " UPDATE ".TABLE_LEADS_QUOTATION
                            ." SET "
							."". TABLE_LEADS_QUOTATION .".sub_total_amount = '".$data['sub_total_amount'] ."'"      
							.",". TABLE_LEADS_QUOTATION .".tax1_id = '". $data['tax1_id'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_number = '". $data['tax1_number'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_name = '". $data['tax1_name'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_declaration = '". $data['tax1_declaration'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_value = '". $data['tax1_value'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_pvalue = '". $data['tax1_pvalue'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_total_amount = '". $data['tax1_total_amount'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub1_id = '". $data['tax1_sub1_id'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub1_number = '". $data['tax1_sub1_number'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub1_name = '". $data['tax1_sub1_name'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub1_value = '". $data['tax1_sub1_value'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub1_pvalue = '". $data['tax1_sub1_pvalue'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub1_total_amount = '". $data['tax1_sub1_total_amount'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub2_id = '". $data['tax1_sub2_id'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub2_number = '". $data['tax1_sub2_number'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub2_name = '". $data['tax1_sub2_name'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub2_value = '". $data['tax1_sub2_value'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub2_pvalue = '". $data['tax1_sub2_pvalue'] ."'"
							.",". TABLE_LEADS_QUOTATION .".tax1_sub2_total_amount = '". $data['tax1_sub2_total_amount'] ."'"
							." WHERE ".TABLE_LEADS_QUOTATION.".id ='".$variables['hid'] ."'";	
						
						$db->query($sqlu);
						
						$data['sub_total_amount']=number_format($data['sub_total_amount'],2);
						$data['tax1_total_amount']=number_format($data['tax1_total_amount'],2);
						$data['tax1_sub1_total_amount']=number_format($data['tax1_sub1_total_amount'],2);
						$data['tax1_sub2_total_amount']=number_format($data['tax1_sub2_total_amount'],2);
					}
					
					
					$data['marketing'] = 0;
					$data['mkex_fname']   = '';
					$data['mkex_lname']   = '';
					$data['mkex_designation'] ='';
					$data['mkex_mobile1']   ='';					
					$data['mkex_email']   ='';					
                    if(!empty($data['order_closed_by'])){
						$clientfields='';
						$condition = TABLE_USER." WHERE  user_id='".$data['order_closed_by']."'";
						$clientfields .= ' '.TABLE_USER.'.marketing_contact, '. TABLE_USER .'.desig,'.TABLE_USER .'.email,'
						.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name,'.TABLE_USER.'.department';
						User::getList($db, $closed_by, $clientfields, $condition) ;
						$data['department']   = $closed_by['0']['department'];
						$data['mkex_fname']   = $closed_by['0']['f_name'];
						$data['mkex_lname']   = $closed_by['0']['l_name'];
						$data['mkex_designation']   = $closed_by['0']['desig'];
						$data['mkex_mobile1']   = $closed_by['0']['marketing_contact'];
						$data['mkex_email']   = $closed_by['0']['email'];
						$data['display_name']  = $data['mkex_fname']." ".$data['mkex_lname'] ;
						$data['display_designation']  = $data['mkex_designation'] ;
						if($data['department'] == ID_MARKETING){
							$data['marketing_email'] =$data['mkex_email'] ;
							$data['marketing_contact'] = $data['mkex_mobile1']  ;
							$data['marketing'] = 1;
						
						}else{
							$data['marketing_email'] = SALES_MEMBER_USER_EMAIL;
							$data['marketing_contact'] = '';
							$data['marketing'] = 0;
						}
					}
                    $data['particulars'] = $temp_p ;                    
                    // Update the Order. for time being
                                    
                    $or_id = NULL;
                    
                    // Create the Invoice PDF, HTML in file.
                    
					$extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';     
					
                    /*  
				    if ( !($attch1 = LeadsQuotation::createFile($data, 'HTMLPRINT', $extra)) ) {
                        $messages->setErrorMessage("The Invoice print html  file was not created.");
                    }   
					*/                 
                    if ( !($attch = LeadsQuotation::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Proposal html file was not created.");
                    }
					//comment it as not worked in LOCAL
                    LeadsQuotation::createPdfFile($data);					
                    $file_name = DIR_FS_QUOTATIONS_PDF ."/". $data["number"] .".pdf";
					/*
					$file_name = DIR_FS_QUOTATIONS_PDF ."/". $data["number"] .".pdf";
					include_once ( DIR_FS_INCLUDES .'/html2pdf.inc.php');
					LeadsQuotation::createPdfFile($data,$file_name);						
					*/
                    // Send the notification mails to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                   
                    // Organize the data to be sent in email.
                    $data['link']   = DIR_WS_MP .'/leads-quotation.php?perform=view&inv_id='. $variables['hid'];
                    // Read the Client Manager information.
                  
                    
                    
                    // Send Email to the Client.
                   /*  
				    if( isset($mail_client) &&  $mail_client==1 ){
                        $email = NULL;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'QUOTATION_CREATE_TO_CLIENT', $data, $email) ) {
                            $email["from_name"] =  $data['display_name'];
							$email["from_email"] = $data['marketing_email'];
							$to     = '';
                            $to[]   = array('name' => $data['client']['f_name'] .' '. 
							$data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,
							$cc,$bcc,$file_name);
                        }
                    }*/
                    //Send mail after create invoice to admin bof
                    $data['link']   = DIR_WS_NC .'/lead-quotation.php?perform=view&inv_id='. $variables['hid'];    
                    $email = NULL;
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'QUOTATION_CREATE_TO_ADMIN', $data, $email) ) {
                        $to     = '';
                         $email["from_name"] =  $data['display_name'];
						 $email["from_email"] = $data['marketing_email'];
						//echo " subject ".$email["subject"] ;
                        //echo "<br/>";
                        //echo " body ".$email["body"];
                         $to[]   = array('name' => $sales_name , 'email' => $sales_email);                            
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);    
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,
						$bcc,$file_name);
                    }
                    //Send mail after create invoice to admin eof
                    //to flush the data.
                    
                    $_ALL_POST  = NULL;
                    //$_ALL_POST['number'] = getCounterNumber($db,'INV',$data['company_id']); 
                    $inv_no =$data['number'];
                    $data       = NULL;
                }
				
            }
            
        }
        else {
            // Set up the default values to be displayed.
            // $_ALL_POST['number'] = "PT". date("Y") ."-INV-". date("mdhi-s") ;
            //As invoice-number formate changed on the basis of ( fanancial yr + sr no )           
        } 
      
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
        
           header("Location:".DIR_WS_NC."/leads-quotation.php?added=1&hid=".$variables['hid']."&inv_no=".$inv_no);           
        } 
        
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/leads-quotation.php");
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
             header("Location:".DIR_WS_NC."/leads-quotation.php?added=1&hid=".$variables['hid']."&inv_no=".$inv_no);
            // include ( DIR_FS_NC .'/bill-invoice-list.php');
        }
        else {
        
            if ( !empty($or_id) ) {
                // Read the Order details.
                include_once ( DIR_FS_INCLUDES .'/leads-order.inc.php');
                $order  = NULL;              
                
                $fields = TABLE_LEADS_ORDERS.'.id,'.TABLE_LEADS_ORDERS .'.do_o AS do_o
				,'.TABLE_LEADS_ORDERS .'.number AS or_no,'. TABLE_LEADS_ORDERS .'.access_level, 
				'. TABLE_LEADS_ORDERS .'.status, '.TABLE_LEADS_ORDERS.'.details';
                $fields .= ', '.TABLE_LEADS_ORDERS.'.order_title, '. TABLE_LEADS_ORDERS .'.order_closed_by';
                $fields .= ', '.TABLE_LEADS_ORDERS.'.do_e, '.TABLE_LEADS_ORDERS .'.do_fe';
                $fields .= ', '.TABLE_LEADS_ORDERS.'.currency_id, '. TABLE_LEADS_ORDERS .'.company_id  ,
				'.TABLE_LEADS_ORDERS .'.amount,'. TABLE_LEADS_ORDERS .'.round_off_op,'.TABLE_LEADS_ORDERS .'.round_off';
                $fields .= ', '.TABLE_SALE_LEADS.'.lead_id as user_id, '. TABLE_SALE_LEADS .'.number, '.TABLE_SALE_LEADS.'.f_name, 
				'.TABLE_SALE_LEADS.'.l_name,'.TABLE_SALE_LEADS.'.billing_name';
				$fields .= ', '.TABLE_LEADS_ORDERS.'.delivery_at' ;
				$fields .= ', '.TABLE_LEADS_ORDERS.'.tax_ids' ;
				$fields .= ', '.TABLE_LEADS_ORDERS.'.service_id' ;
				$fields .= ', '.TABLE_LEADS_ORDERS.'.dont_show_total' ;
				//$fields .= ', '.TABLE_LEADS_ORDERS.'.is_inv_create' ;
                $fields .= ', '.TABLE_LEADS_ORDERS.'.client, '. TABLE_USER .'.number AS u_number, 
				'.TABLE_USER.'.f_name AS u_f_name, '.TABLE_USER.'.l_name AS u_l_name ';
                
                if ( LeadsOrder::getDetails($db, $order, $fields, " WHERE ".TABLE_LEADS_ORDERS.".id = '". $or_id ."' ") > 0 ) {
                    $order = $order[0];
                     
                    if ( ($order['status'] == LeadsOrder::ACTIVE) || ($order['status'] == LeadsOrder::PROCESSED) || 
					($order['status'] == LeadsOrder::COMPLETED) ) {
                        if ( $access_level >= $order['access_level'] ) {
                            $_ALL_POST['or_no']             = $order['or_no'];
                           // $_ALL_POST['or_particulars']  = $order['particulars'];
                            $_ALL_POST['or_details']        = $order['details'];
                            $_ALL_POST['or_user_id']        = $order['user_id'];
                            $_ALL_POST['or_number']         = $order['number'];
                            $_ALL_POST['or_f_name']         = $order['f_name'];
                            $_ALL_POST['or_l_name']         = $order['l_name'];
                            $_ALL_POST['or_access_level']   = $order['access_level'];
                            $_ALL_POST['billing_name']   = $order['billing_name'];
                         
                            $_ALL_POST['do_o']              = $order['do_o'];
                            $_ALL_POST['u_f_name']          = $order['u_f_name'];
                            $_ALL_POST['u_l_name']          = $order['u_l_name'];
                            $_ALL_POST['u_number']          = $order['u_number'];
                            $_ALL_POST['quotation_subject'] = $order['order_title'];
                            $_ALL_POST['order_closed_by']   = $order['order_closed_by'];
                            $_ALL_POST['do_e']   = $order['do_e'];
                            $_ALL_POST['do_fe']   = $order['do_fe'];
							$_ALL_POST['tax_ids']   = $order['tax_ids'];
							$_ALL_POST['service_id']   = $order['service_id'];
							
                            if($_ALL_POST['do_e'] !='0000-00-00 00:00:00'){
                                $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                                $temp               = explode('-', $_ALL_POST['do_e'][0]);
                                $_ALL_POST['do_e']  = NULL;
                                $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                            }else{                            
                                $_ALL_POST['do_e']  ='';
                            }
                            
                            if($_ALL_POST['do_fe'] !='0000-00-00 00:00:00'){
                                $_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
                                $temp               = explode('-', $_ALL_POST['do_fe'][0]);
                                $_ALL_POST['do_fe']  = NULL;
                                $_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                            }else{                            
                                $_ALL_POST['do_fe']  ='';
                            }
                                    
                            
                            
                            if(!empty($order['order_closed_by'])){
                                $clientfields='';
                                $condition = TABLE_USER." WHERE  user_id='".$order['order_closed_by']."'";
                                $clientfields .= ' '.TABLE_USER.'.user_id, '. TABLE_USER .'.number, 
								'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
                                User::getList($db, $closed_by, $clientfields, $condition) ;
                                $_ALL_POST['order_closed_by_name']   = $closed_by['0'];
                            }
                                                    
                            $_ALL_POST['round_off_op']= $order['round_off_op'];
							$_ALL_POST['round_off']= $order['round_off'];
                            $_ALL_POST['amount']= $order['amount'];
                            $_ALL_POST['gramount']= number_format($order['amount'],2);
                            $_ALL_POST['amount_inr']= $order['amount'] * $_ALL_POST['exchange_rate'];
                            $_ALL_POST['currency_id']= $order['currency_id'];
							$_ALL_POST['dont_show_total']= $order['dont_show_total'];
                            //$_ALL_POST['company_id']= $order['company_id'];
                           
                            /*get particulars details bof*/
                            $temp = NULL;
                            $temp_p = NULL;
                            $condition_query = " WHERE ord_no = '". $order['or_no'] ."' ";
                            LeadsOrder::getParticulars($db, $temp, '*', $condition_query);
                            if(!empty($temp)){
                                foreach ( $temp as $pKey => $parti ) {
                                 /*if($temp[$pKey]['s_type']=='1'){
                                    $temp[$pKey]['s_type']='Yrs';
                                  }elseif($temp[$pKey]['s_type']=='2'){
                                    $temp[$pKey]['s_type']='Qty';
                                  }*/
                                    $temp_p[]=array(
									'p_id'=>$temp[$pKey]['id'],
									'particulars'=>$temp[$pKey]['particulars'],
									'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,      
									's_type' =>$temp[$pKey]['s_type'] ,                                   
									's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
									's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                    
									's_id' =>$temp[$pKey]['s_id'] ,                                   
									'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
									'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
									'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
									'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
									'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
									'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
									'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,
									'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,                                   
									'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                   
									'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
									'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,                                   
									'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) , 
									'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,                                   
									'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                   
									'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
									'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,                                   
									'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,									
									'd_amount' =>number_format($temp[$pKey]['d_amount'],2),                                   
									'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                  
									'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
									'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
									'pp_amount' =>number_format($temp[$pKey]['pp_amount'] ,2)                                   
                                    );
                                }
                            }
                            $_ALL_POST['particulars']=$temp_p ;
                            
                            /*get particulars details eof*/
                            $order = NULL;
                            // Read the Client Addresses.
                            include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                            $region         = new RegionLead();
                            $region->setAddressOf(TABLE_SALE_LEADS, $_ALL_POST['or_user_id']);
                            $_ALL_POST['address_list']  = $region->get();
                          
                            if ( (!isset($_POST['btnCreate']) && !isset($_POST['btnReturn'])) ) {
                                $_ALL_POST['billing_name']  = $_ALL_POST['billing_name'];
                                //$_ALL_POST['particulars']   = array('');
                                //$_ALL_POST['p_amount']      = array(0);
                            }

							$al_list = getAccessLevel($db, $access_level);
							$index = array();
							if ( isset($_ALL_POST['or_access_level']) ) {
								array_key_search('access_level', $_ALL_POST['or_access_level'], $al_list, $index);
								$_ALL_POST['or_access_level'] = $al_list[$index[0]]['title'];
							}
							
                            // As invoice-number formate changed on the basis of ( fanancial yr +  company_id + sr no ) 
                            //$_ALL_POST['number'] = getCounterNumber($db,'INV',$_ALL_POST['company_id']); comment as require dropdown to select financial yr
                            
            
							$hidden[] = array('name'=> 'or_id' ,'value' => $or_id);
							$hidden[] = array('name'=> 'or_no' ,'value' => $_ALL_POST['or_no']);
							$hidden[] = array('name'=> 'tax_ids' ,'value' => $_ALL_POST['tax_ids']);
							$hidden[] = array('name'=> 'service_id' ,'value' => $_ALL_POST['service_id']);
							$hidden[] = array('name'=> 'delivery_at' ,'value' => $_ALL_POST['delivery_at']);
							$hidden[] = array('name'=> 'client' ,'value' => $_ALL_POST['or_user_id']);
							$hidden[] = array('name'=> 'round_off' ,'value' => $_ALL_POST['round_off']);
							$hidden[] = array('name'=> 'round_off_op' ,'value' => $_ALL_POST['round_off_op']);
							$hidden[] = array('name'=> 'order_closed_by' ,'value' => $_ALL_POST['order_closed_by']);
							$hidden[] = array('name'=> 'dont_show_total' ,'value' => $_ALL_POST['dont_show_total']);
							//$hidden[] = array('name'=> 'company_id' ,'value' => $_ALL_POST['company_id']);
							
							$hidden[] = array('name'=> 'perform' ,'value' => 'add');
							$hidden[] = array('name'=> 'act' , 'value' => 'save');
				
							$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
							$page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
							$page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
							$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
							$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                            $page["var"][] = array('variable' => 'lst_fyr', 'value' => 'lst_fyr');
                            //$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
							//$page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
							$page["var"][] = array('variable' => 'currency', 'value' => 'currency');
							
							$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-quotation-add.html');
							
					  }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create proposal for this Proposal.");
                            $_ALL_POST = '';
                        }
                    }
                    else {
                        $messages->setErrorMessage("The Lead Order is not yet approved.");
                        $_ALL_POST = '';
                    }
                }
                else {
                    $messages->setErrorMessage("The Selected Lead Order was not found.");
                }
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>