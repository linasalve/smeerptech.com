<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
  
    $searchStr = 0;
    if ( $sString != "" ) 
    {
        $searchStr = 1;
        
         // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
       
        //$where_added = true;
        $sub_query='';        
        if ( $search_table == "Any" ) 
        {
            
            if ( $where_added ) 
            {
                $condition_query .= "OR ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ){
              
                if ( $table != "Any" ) 
                { 
                    
					if($table == TABLE_BANK_STATEMENT){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    } 
                    
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= " AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE 
				'%".trim($sString) ."%' )" ;
                
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
    //code for search EOF 2009-03-mar-21 
     
     
	 
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"]) ? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
	$dt_field = isset($_POST["dt_field"])? $_POST["dt_field"] : (isset($_GET["dt_field"]) ? $_GET["dt_field"]  :'');
 
	
	// BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from) ){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_BANK_STATEMENT .".".$dt_field." >= '". $dfa ."'";        
        $condition_url  .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;
    }
    
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_BANK_STATEMENT .".".$dt_field." <= '". $dta ."'";
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }    
    // EO: Upto Date
     
	 
	// BO: Status data.
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR') && isset($sStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }      
        
        $condition_query .= " ". TABLE_BANK_STATEMENT .".status IN ('". $sStatus ."') "; 
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatus";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
    }
	//EO: Status
	
	// BO: Link Status data.
    $chk_lstatus = isset($_POST["chk_lstatus"])   ? $_POST["chk_lstatus"]  : (isset($_GET["chk_lstatus"])   ? $_GET["chk_lstatus"]   :'');
    $lstatus    = isset($_POST["lstatus"])      ? $_POST["lstatus"]     : (isset($_GET["lstatus"])      ? $_GET["lstatus"]      :'');
    if ( ($chk_lstatus == 'AND' || $chk_lstatus == 'OR') && isset($lstatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_lstatus;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }      
        
        $condition_query .= " ". TABLE_BANK_STATEMENT.".status_u IN ('". $lstatus ."') "; 
        $condition_url          .= "&chk_lstatus=$chk_lstatus&lstatus=$lstatus";
        $_SEARCH["chk_lstatus"]  = $chk_lstatus;
        $_SEARCH["lstatus"]     = $lstatus;
    }
	//EO: Link Status
	
	// BO: Bank data.
    $chk_bank = isset($_POST["chk_bank"])   ? $_POST["chk_bank"]  : (isset($_GET["chk_bank"])   ? $_GET["chk_bank"]   :'');
    $bank    = isset($_POST["bank"])      ? $_POST["bank"]     : (isset($_GET["bank"])      ? $_GET["bank"]      :'');
    if ( ($chk_bank == 'AND' || $chk_bank == 'OR') && isset($bank)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_bank;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }      
        
        $condition_query .= " ". TABLE_BANK_STATEMENT.".bank_id IN ('". $bank ."') "; 
        $condition_url          .= "&chk_bank=$chk_bank&bank=$bank";
        $_SEARCH["chk_bank"]  = $chk_bank;
        $_SEARCH["bank"]     = $bank;
    }
	//EO: Bank
	
	// BO: Company data.
    $chk_company = isset($_POST["chk_company"])   ? $_POST["chk_company"]  : (isset($_GET["chk_company"])   ? $_GET["chk_company"]   :'');
    $company_id    = isset($_POST["company_id"])      ? $_POST["company_id"]     : (isset($_GET["company_id"])      ? $_GET["company_id"]      :'');
    if ( ($chk_company == 'AND' || $chk_company == 'OR') && isset($company_id)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_company;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }      
        
        $condition_query .= " ". TABLE_BANK_STATEMENT.".company_id IN ('". $company_id ."') "; 
        $condition_url          .= "&chk_company=$chk_company&company_id=$company_id";
        $_SEARCH["chk_company"]  = $chk_company;
        $_SEARCH["company_id"]     = $company_id;
    }
	// EO: Company data.
	 
    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/bank-statement-list.php');
?>