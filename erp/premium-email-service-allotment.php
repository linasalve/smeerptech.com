<?php
	$_ALL_POST      = NULL; 
	$data           = NULL; 
	$id	= isset($_GET["id"])         ? $_GET["id"]        : ( isset($_POST["id"])          ? $_POST["id"]       :'');
	include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
	include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
	
	$condition_query1 = 'WHERE '.TABLE_PREMIUM_EMAIL_SERVICE.'.id='.$id;
    $fields = TABLE_PREMIUM_EMAIL_SERVICE.'.*,'.TABLE_CLIENTS.'.billing_name,
	'.TABLE_CLIENTS.'.premium_email_mgmt,'.TABLE_CLIENTS.'.premium_email_admin, '.TABLE_CLIENTS.'.premium_email_billing,
	'.TABLE_CLIENT_DOMAINS.'.domain' ;
	
	PremiumEmailService::getList($db, $list, $fields, $condition_query1 );
    if(!empty($list)){
		$details = $list[0];
		/* 
			$no1 = rand(10,99);
			$no2 = rand(10,99);
			$spec1 = randomString(1,'+#-=!$%@^&<*_>');
			$str1 = randomString(3);
			$str2 = randomString(3);
			$email_password = $str1.$no1.$spec1.$str2.$no2; 
		*/
		$premium_email_mgmt_to = $premium_email_admin_to = $premium_email_billing_to = array() ;
		$premium_email_mgmt_to_str = $premium_email_admin_to_str = $premium_email_billing_to_str = '';
		$premium_email_mgmt_to[]  	= array('name' => $support_auth_email , 'email' => $support_auth_email); 
		$premium_email_admin_to[]   = array('name' => $support_auth_email , 'email' => $support_auth_email); 
		$premium_email_billing_to[] = array('name' => $support_auth_email , 'email' => $support_auth_email); 		
		$premium_email_mgmt_to_str = $premium_email_admin_to_str = $premium_email_billing_to_str = $support_auth_email.",";
		
		if(!empty($details['premium_email_mgmt'])){
			$premium_email_mgmt = $details['premium_email_mgmt'] ;
			$details['premium_email_mgmt'] = trim($details['premium_email_mgmt'],',');
			$details['premium_email_mgmt'] = explode(',', $details['premium_email_mgmt']);
			$temp                       	= "'". implode("','", $details['premium_email_mgmt']) ."'";
			Clients::getList($db, $details['premium_email_mgmt1'], 'user_id, number, f_name, l_name, email, email_1, email_2', 
			" WHERE user_id IN (". $temp .")");
			$premium_email_mgmt_to_str ='';
			foreach( $details['premium_email_mgmt1'] as $key1=>$members1) {
				if(!empty($members1['email'])){
					$premium_email_mgmt_to[]   = array('name' => $members1['email'] , 'email' => $members1['email']); 
					$premium_email_mgmt_to_str.= $members1['email'].",";
				}
				if(!empty($members1['email_1'])){
					$premium_email_mgmt_to[]   = array('name' => $members1['email_1'] , 'email' => $members1['email_1']); 
					$premium_email_mgmt_to_str.= $members1['email_1'].",";
				}
				if(!empty($members1['email_2'])){
					$premium_email_mgmt_to[]   = array('name' => $members1['email_2'] , 'email' => $members1['email_2']); 
					$premium_email_mgmt_to_str.= $members1['email_2'].",";
				}				 
			}
		}
		if(!empty($details['premium_email_admin'])){
			$premium_email_admin = $details['premium_email_admin'] ;
			$details['premium_email_admin'] = trim($details['premium_email_admin'],',');
			$details['premium_email_admin'] = explode(',', $details['premium_email_admin']);
			$temp   = "'". implode("','", $details['premium_email_admin']) ."'";
			Clients::getList($db, $details['premium_email_admin1'], 'user_id, number, f_name, l_name, email, email_1, email_2', 
			" WHERE user_id IN (". $temp .")");
			foreach( $details['premium_email_admin1'] as $key1=>$members2){
				if(!empty($members2['email'])){
					$premium_email_admin_to[]   = array('name' => $members2['email'] , 'email' => $members2['email']); 
					$premium_email_admin_to_str.= $members2['email'].",";
				}
				if(!empty($members2['email_1'])){
					$premium_email_admin_to[]   = array('name' => $members2['email_1'] , 'email' => $members2['email_1']); 
					$premium_email_admin_to_str.= $members2['email_1'].",";
				}
				if(!empty($members2['email_2'])){
					$premium_email_admin_to[]   = array('name' => $members2['email_2'] , 'email' => $members2['email_2']); 
					$premium_email_admin_to_str.= $members2['email_2'].",";
				}				 
			}
		}
		
		if(!empty($details['premium_email_billing'])){
			$premium_email_billing = $details['premium_email_billing'] ;
			$details['premium_email_billing'] = trim($details['premium_email_billing'],',');
			$details['premium_email_billing'] = explode(',', $details['premium_email_billing']);
			$temp   = "'". implode("','", $details['premium_email_billing']) ."'";
			Clients::getList($db, $details['premium_email_billing1'], 'user_id, number, f_name, l_name, email, email_1, email_2', 
			" WHERE user_id IN (". $temp .")");			
			foreach( $details['premium_email_billing1'] as $key1=>$members3) {
				if(!empty($members3['email'])){
					$premium_email_billing_to[] = array('name' => $members3['email'] , 'email' => $members3['email']); 
					$premium_email_billing_to_str.= $members3['email'].",";
				}
				if(!empty($members3['email_1'])){
					$premium_email_billing_to[] = array('name' => $members3['email_1'] , 'email' => $members3['email_1']); 
					$premium_email_billing_to_str.= $members3['email_1'].",";
				}
				if(!empty($members3['email_2'])){
					$premium_email_billing_to[] = array('name' => $members3['email_2'] , 'email' => $members3['email_2']); 
					$premium_email_billing_to_str.= $members3['email_2'].",";
				}				 
			}
		}		
		
		$format = 'application/json';		
		$_ALL_DATA['email_domain'] = $details['domain'] ;
		$url_string = "/customers/all/domains/".$_ALL_DATA['email_domain'];  
		//$url_string = "/customers/all/domains?domainNames=".$_ALL_DATA['email_domain'];   
		$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
		$headers = array();
		$headers = array("Accept: $format"); 
		$curl_session = curl_init();
		curl_setopt($curl_session, CURLOPT_URL, $url);
		$time_stamp = date('YmdHis'); 
		$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
		$signature = base64_encode(sha1($data_to_sign, true)); 
		$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
		$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
		//http://stackoverflow.com/questions/14346540/php-get-json-using-curl-sorting-returned-array 
		curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
	    $httpResponse1 = curl_exec($curl_session); 
		curl_close($curl_session);						
		$_ALL_DATA['domain_details'] = array();
		$_ALL_DATA['domain_details'] = json_decode(stripslashes($httpResponse1),true);
		if(json_last_error() == JSON_ERROR_NONE){
			if(!empty($_ALL_DATA['domain_details'])){
				$serviceType = $_ALL_DATA['domain_details']['serviceType']; 
				if($serviceType == 'rsemail' || $serviceType =='both'){
					//$_ALL_DATA['domain_details']['serviceType'] ; 
					$accountNumber = $_ALL_DATA['domain_details']['accountNumber'] ;  
					$url_string = "/customers/".$accountNumber."/domains/".$_ALL_DATA['email_domain']."/rs/mailboxes?size=500&offset=0"; 
					$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
					$headers = array();
					$headers = array("Accept: $format"); 
					$curl_session = curl_init();
					curl_setopt($curl_session, CURLOPT_URL, $url1);
					$time_stamp = date('YmdHis'); 
					$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
					$signature = base64_encode(sha1($data_to_sign, true)); 
					$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
					$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
					// comment me for time being
					//curl_setopt($curl_session, CURLOPT_HEADER, true);
					curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
					$httpResponse = curl_exec($curl_session);
					curl_close($curl_session);	
					$_ALL_DATA['rs_email'] = json_decode(stripslashes($httpResponse),true); 
				} 
				
				if($serviceType == 'exchange' || $serviceType =='both'){
					$accountNumber = $_ALL_DATA['domain_details']['accountNumber'] ;  
					$url_string = "/customers/".$accountNumber."/domains/".$_ALL_DATA['email_domain']."/ex/mailboxes?size=500&offset=0"; 
					$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
					$headers = array(); 
					$headers = array("Accept: $format"); 
					$curl_session = curl_init();
					curl_setopt($curl_session, CURLOPT_URL, $url1);
					$time_stamp = date('YmdHis'); 
					$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
					$signature = base64_encode(sha1($data_to_sign, true)); 
					$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
					$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
					// comment me for time being
					//curl_setopt($curl_session, CURLOPT_HEADER, true);
					curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
					$httpResponse = curl_exec($curl_session);
					curl_close($curl_session);	
					$_ALL_DATA['ex_email'] = json_decode(stripslashes($httpResponse),true); 
				}  
			}
		} 
	}
   
	
	if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST 	= $_POST;
		$data		= processUserData($_ALL_POST);
		$data['domain_details'] = $_ALL_DATA['domain_details'] ; 
		
		$extra = array( 'db' 				=> &$db,
						'access_level'      => $access_level,
						'messages'          => &$messages
					);
			
		if ( PremiumEmailService::validatePremiumService($data, $extra) ) { 
			/* 	
			$no1 = rand(10,99);
			$no2 = rand(10,99); 
			*/
			$no1 = randomNumber(2);
			$no2 = randomNumber(2);			
			$spec1 = randomString(1,'+#-=!$%@^&<*_>');
			$str1 = randomString(3);
			$str2 = randomString(3);
			$email_password = $str1.$no1.$spec1.$str2.$no2;
			$format = 'application/json';
			
			if($data['form_type']=='change_pwd'){
				//$emailid = $data['user_email']."@".$_ALL_DATA['email_domain'] ;
				$url_string="/customers/all/domains/".$_ALL_DATA['email_domain']."/rs/mailboxes/".$data['user_email'];
				$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
				$headers = array();
				$headers = array("Accept: $format");
				$curl_session = curl_init();
				curl_setopt($curl_session, CURLOPT_URL, $url);
				$time_stamp = date('YmdHis'); 
				$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
				$signature = base64_encode(sha1($data_to_sign, true)); 
				$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
				$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";				
				$fields =  array( 
					'password' => $email_password 
				);
				curl_setopt($curl_session, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($curl_session, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($curl_session, CURLOPT_HEADER, true);
				curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
				$httpResponse = curl_exec($curl_session);
				curl_close($curl_session);	
				if(strpos($httpResponse, 'HTTP/1.1 200 OK')!==false){
					$msg = "Password changed successfully <br/>
					Email: ".$data['user_email'].'@'.$_ALL_DATA['email_domain']." <br/>
					New Password: ".$email_password."." ;
					$messages->setOkMessage($msg);
					$ticket_no  =  SupportTicket::getNewNumber($db);
					$data['ticket_subject']	= 'Password Changed - '.$data['user_email'].'@'.$_ALL_DATA['email_domain'];
					$data['ticket_text']= "<div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:18px;\">Dear Sir/Madam, <br/> Please check new password as below:</div><div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:15px;\">
					Email: ".$data['user_email']."@".$_ALL_DATA['email_domain']." 
					New Password: ".$email_password."
					</div>";
					$mail_to_all_su = $premium_email_mgmt_to_str;
					$attachfilename=$ticket_attachment_path='';
					$mail_to_all_su=0;
					if(isset($data['mail_to_all_su']) ){
						$mail_to_all_su=1;
					}
					$mail_to_additional_email=0;
					if(isset($data['mail_to_additional_email']) ){
						$mail_to_additional_email=1;
					}
					$mail_client=0;
					if(isset($data['mail_client']) ){
						$mail_client=1;
					}
					$mail_ceo=0;
					if(isset($data['mail_ceo'])){
						$mail_ceo = 1;
					}
					$data['ticket_owner_uid']=$details['client'];
					$data['ticket_owner'] = $details['billing_name'];
					
					//disply random name BOF 
					$data['display_name']=$data['display_user_id']=$data['display_designation']='';
					$randomUser = getRandomAssociate($data['executive_id']);
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];					
					//disply random name EOF		
					/* 
					if(!isset($data['ticket_status']) ){                    
						$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
					}else{
						$ticket_status = $data['ticket_status'];
					} */
					$data['min']=5;
					$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
					$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
					$min1 = (int) ($data['min'] * HRS_FACTOR);  
					$sent_inv_id_str = $sent_inv_no_str ='';
					if(empty($data['ss_id'])){		
						$data['ss_title']='';
						$data['ss_file_1']='';
						$data['ss_file_2']='';
						$data['ss_file_3']='';
					}
				
					$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
					. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
					. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
					. TABLE_ST_TICKETS .".invoice_profm_id    = '". $invoice_profm_id ."', "
					. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
					. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
					. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
					. TABLE_ST_TICKETS .".on_behalf        = '". $data['on_behalf'] ."', "
					. TABLE_ST_TICKETS .".template_id      = '". $data['template_id'] ."', "
					. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
					. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
					. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
					. TABLE_ST_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
					. TABLE_ST_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
					. TABLE_ST_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
					. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
					. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
					. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
					. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
					. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
					. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
					. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
					. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
					. TABLE_ST_TICKETS .".ticket_creator_uid    = '". $my['uid'] ."', "
					. TABLE_ST_TICKETS .".ticket_creator         = '". $my['f_name']." ". $my['l_name'] ."', "
					. TABLE_ST_TICKETS .".ticket_subject    	 = '". $data['ticket_subject'] ."', "
					. TABLE_ST_TICKETS .".ticket_mail_subject    = '". $data['ticket_subject'] ."', "
					. TABLE_ST_TICKETS .".ticket_text         = '". $data['ticket_text'] ."', "
					. TABLE_ST_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
					. TABLE_ST_TICKETS.".last_comment_by 	  = '".$my['uid'] ."', "
					. TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
					//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
					//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
					//. TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
					. TABLE_ST_TICKETS .".mail_ceo        = '".$mail_ceo."', "
					. TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
					. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
					. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
					. TABLE_ST_TICKETS .".ticket_child      = '0', "
					. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
					. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
					. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
					. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
					. TABLE_ST_TICKETS .".min = '". $data['min']."', "
					. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
					. TABLE_ST_TICKETS .".ticket_response_time = '0', "
					. TABLE_ST_TICKETS .".ticket_replied        = '0', "
					. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
					//. TABLE_ST_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
					. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
					. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
					. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
					. TABLE_ST_TICKETS .".is_private           = '". $data['is_private'] ."', "
					. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
					. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
					. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
					. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
					. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ; 
					 
					if ($db->query($query) && $db->affected_rows() > 0) { 
						$variables['hid'] = $db->last_inserted_id() ;
						$data['ticket_no']    =   $ticket_no ;
						$data['domain']   = THIS_DOMAIN;                        
						//$data['replied_by']   =   $data['ticket_owner'] ;
						$data['mticket_no']   =   $ticket_no ;
						$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
						$data['subject']    =     $data['ticket_subject'] ;
						$data['text']    	=  	 $data['ticket_text'] ;
						$data['attachment']   =   $attachfilename ;
						$data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $variables['hid'];
						if(!empty($premium_email_mgmt_to)){
							getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
							$cc_to_sender=$cc=$bcc='';
							/* 
							$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
							'email' => $smeerp_client_email); 
							*/
							$from = $reply_to= array('name' => $email["from_name"], 'email' => $email['from_email']); 
							SendMailSupport($premium_email_mgmt_to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name);
						}
					}
				}
			}
			if($data['form_type']=='delete_email'){
				$accountNumber = $_ALL_DATA['domain_details']['accountNumber'] ;  
				$serv_type =$data['email_service'];
				$url_string="/customers/".$accountNumber."/domains/".$_ALL_DATA['email_domain']."/".$serv_type."/mailboxes/".$data['user_email']; 
				$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
				$headers = array(); 
				$headers = array("Accept: $format");
				$curl_session = curl_init();
				curl_setopt($curl_session, CURLOPT_URL, $url);
				$time_stamp = date('YmdHis'); 
				$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
				$signature = base64_encode(sha1($data_to_sign, true)); 
				$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
				$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";					 
				//curl_setopt($curl_session, CURLOPT_POST,1);
				curl_setopt($curl_session, CURLOPT_CUSTOMREQUEST, 'DELETE');
				//curl_setopt($curl_session, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($curl_session, CURLOPT_HEADER, true);
				curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
				$httpResponse2 = curl_exec($curl_session); 						 
				curl_close($curl_session);
 				if(strpos($httpResponse2, 'HTTP/1.1 200 OK') !== false ){
					$emailid = $data['user_email']."@".$_ALL_DATA['email_domain'] ;
					$msg = $emailid." email-id has been deleted successfully.";
					$messages->setOkMessage($msg);
					//Create Ticket BOF
					$ticket_no  =  SupportTicket::getNewNumber($db);  
					$data['ticket_subject']	= "Email id Deleted - ".$emailid.".";
					$data['ticket_text']= "<div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:18px;\">Dear Sir/Madam, <br/>  </div><div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:15px;\">
					 ".$emailid." email-id has been deleted successfully. 
					</div>";
					$data['ticket_owner_uid'] = $details['client'];
					$data['ticket_owner'] = $details['billing_name'];
					$data['hrs']=0;
					$data['min']=5;					
					$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
					$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
					$min1 = (int) ($data['min'] * HRS_FACTOR);
					//disply random name BOF 
					$data['display_name']=$data['display_user_id']=$data['display_designation']='';
					$randomUser = getRandomAssociate($data['executive_id']);
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
					$mail_to_all_su = $premium_email_mgmt_to_str;
					$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
						. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
						. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
						. TABLE_ST_TICKETS .".invoice_profm_id    = '". $invoice_profm_id ."', "
						. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
						. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
						. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
						. TABLE_ST_TICKETS .".on_behalf        = '". $data['on_behalf'] ."', "
						. TABLE_ST_TICKETS .".template_id      = '". $data['template_id'] ."', "
						. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_ST_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
						. TABLE_ST_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
						. TABLE_ST_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
						. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
						. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
						. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
						. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
						. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
						. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
						. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
						. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
						. TABLE_ST_TICKETS .".ticket_creator_uid    = '". $my['uid'] ."', "
						. TABLE_ST_TICKETS .".ticket_creator         = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_ST_TICKETS .".ticket_subject    	 = '". $data['ticket_subject'] ."', "
						. TABLE_ST_TICKETS .".ticket_mail_subject    = '". $data['ticket_subject'] ."', "
						. TABLE_ST_TICKETS .".ticket_text         = '". $data['ticket_text'] ."', "
						. TABLE_ST_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
						. TABLE_ST_TICKETS.".last_comment_by 	  = '".$my['uid'] ."', "
						. TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
						//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
						//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
						//. TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
						. TABLE_ST_TICKETS .".mail_ceo        = '".$mail_ceo."', "
						. TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
						. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
						. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
						. TABLE_ST_TICKETS .".ticket_child      = '0', "
						. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_ST_TICKETS .".min = '". $data['min']."', "
						. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
						. TABLE_ST_TICKETS .".ticket_response_time = '0', "
						. TABLE_ST_TICKETS .".ticket_replied        = '0', "
						. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
						//. TABLE_ST_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
						. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
						. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
						. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
						. TABLE_ST_TICKETS .".is_private           = '". $data['is_private'] ."', "
						. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
						. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ; 
						 
						if ($db->query($query) && $db->affected_rows() > 0) { 
							$variables['hid'] = $db->last_inserted_id() ;
							$data['ticket_no']    =   $ticket_no ;
							$data['domain']   = THIS_DOMAIN;                        
							//$data['replied_by']   =   $data['ticket_owner'] ;
							$data['mticket_no']   =   $ticket_no ;
							$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
							$data['subject']    =     $data['ticket_subject'] ;
							$data['text']    =   $data['ticket_text'] ;							
							 
							if(!empty($premium_email_mgmt_to)){
								getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
								$cc_to_sender=$cc=$bcc='';
								/* 
								$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
								'email' => $smeerp_client_email); 
								*/
								$from = $reply_to= array('name' => $email["from_name"], 'email' => $email['from_email']); 
								SendMailSupport($premium_email_mgmt_to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
								$cc_to_sender,$cc,$bcc,$file_name);
							}
						}   
					
					//Create Ticket EOF
					
					
					
										
					//Get all details of domain after deleting emailid BOF
						$_ALL_DATA['email_domain'] = $details['domain'] ;
						$url_string = "/customers/all/domains/".$_ALL_DATA['email_domain'];  
						//$url_string = "/customers/all/domains?domainNames=".$_ALL_DATA['email_domain'];   
						$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
						$headers = array();
						$headers = array("Accept: $format"); 
						$curl_session = curl_init();
						curl_setopt($curl_session, CURLOPT_URL, $url);
						$time_stamp = date('YmdHis'); 
						$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
						$signature = base64_encode(sha1($data_to_sign, true)); 
						$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
						$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
						//http://stackoverflow.com/questions/14346540/php-get-json-using-curl-sorting-returned-array
						curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
						$httpResponse1 = curl_exec($curl_session); 
						curl_close($curl_session);						
						$_ALL_DATA['domain_details'] = array();
						$_ALL_DATA['domain_details'] = json_decode(stripslashes($httpResponse1),true);
						if(json_last_error() == JSON_ERROR_NONE){
							if(!empty($_ALL_DATA['domain_details'])){
								$serviceType = $_ALL_DATA['domain_details']['serviceType']; 
								if($serviceType == 'rsemail' || $serviceType =='both'){
									//$_ALL_DATA['domain_details']['serviceType'] ; 
									$accountNumber = $_ALL_DATA['domain_details']['accountNumber'] ;  
									$url_string = "/customers/".$accountNumber."/domains/".$_ALL_DATA['email_domain']."/rs/mailboxes?size=500&offset=0"; 
									$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
									$headers = array();
									$headers = array("Accept: $format"); 
									$curl_session = curl_init();
									curl_setopt($curl_session, CURLOPT_URL, $url1);
									$time_stamp = date('YmdHis'); 
									$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
									$signature = base64_encode(sha1($data_to_sign, true)); 
									$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
									$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
									// comment me for time being
									//curl_setopt($curl_session, CURLOPT_HEADER, true);
									curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
									curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
									$httpResponse = curl_exec($curl_session);
									curl_close($curl_session);	
									$_ALL_DATA['rs_email'] = json_decode(stripslashes($httpResponse),true); 
								} 
								
								if($serviceType == 'exchange' || $serviceType =='both'){
									$accountNumber = $_ALL_DATA['domain_details']['accountNumber'] ;  
									$url_string = "/customers/".$accountNumber."/domains/".$_ALL_DATA['email_domain']."/ex/mailboxes?size=500&offset=0"; 
									$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
									$headers = array(); 
									$headers = array("Accept: $format"); 
									$curl_session = curl_init();
									curl_setopt($curl_session, CURLOPT_URL, $url1);
									$time_stamp = date('YmdHis'); 
									$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
									$signature = base64_encode(sha1($data_to_sign, true)); 
									$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
									$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
									// comment me for time being
									//curl_setopt($curl_session, CURLOPT_HEADER, true);
									curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
									curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
									$httpResponse = curl_exec($curl_session);
									curl_close($curl_session);	
									$_ALL_DATA['ex_email'] = json_decode(stripslashes($httpResponse),true); 
								}  
							}
						}					
					//Get all details of domain after deleting emailid EOF
				}else{ 
					$messages->setErrorMessage($httpResponse2); 				 	
				}
			}
			if($data['form_type']=='create_email'){
				$accountNumber = $_ALL_DATA['domain_details']['accountNumber'] ;  
				$serv_type =$data['email_service'];
				$url_string="/customers/".$accountNumber."/domains/".$_ALL_DATA['email_domain']."/".$serv_type."/mailboxes/".$data['user_email2']; 
				$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
				$headers = array(); 
				$headers = array("Accept: $format");
				$curl_session = curl_init();
				curl_setopt($curl_session, CURLOPT_URL, $url);
				$time_stamp = date('YmdHis'); 
				$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
				$signature = base64_encode(sha1($data_to_sign, true)); 
				$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
				$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";					 
				$emailsize = $data['email_size']; //emai size in MB
				$fields =  array( 
					  'password' => $email_password,
					  'size' => $emailsize, //ie1MB
					  'lastName' => 'A',
					  'firstName' =>  'B',
					  'initials' =>  'A'
				);
				curl_setopt($curl_session, CURLOPT_POST,1);
				curl_setopt($curl_session, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($curl_session, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($curl_session, CURLOPT_HEADER, true);
				curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
				$httpResponse2 = curl_exec($curl_session); 						 
				curl_close($curl_session);

 				if(strpos($httpResponse2, 'HTTP/1.1 200 OK') !== false ){							
					$emailid = $data['user_email2']."@".$_ALL_DATA['email_domain'] ;
					$msg = $emailid." Email id created successfully.";
					$messages->setOkMessage($msg);
					$ticket_no  =  SupportTicket::getNewNumber($db);  
					$data['ticket_subject']	= "New Email Created -  ".$emailid;
					$data['ticket_text']= "<div style=\"font-family:Arial, san-serif;font-size:12px;font-weight:normal;line-height:14px;\"><b>Dear Sir / Madam,</b><br /><br />As per your support request, new email id is created. Please find below the details.<br />
						Email: &nbsp;&nbsp; ".$emailid." <br/>
						Password: &nbsp;&nbsp; ".$email_password." <br/><br/>
						<b>NOTE:</b> Take care while copying password as extra space is prefixed or suffixed while copying password with mouse.<br /><br />
						<b>Mail Client (Outlook, MacMail, Mobiles etc) Configuration:</b><br />
						<b>Incoming address POP3:</b> &nbsp;&nbsp; pop.".$_ALL_DATA['email_domain']."<br/>
						<b>Outgoing address SMTP:</b> &nbsp;&nbsp;  smtp.".$_ALL_DATA['email_domain']."<br/>
						<b>Incoming port POP3:</b> &nbsp;&nbsp;  110<br/>
						<b>Outgoing port SMTP:</b> &nbsp;&nbsp;  587 (for some mobiles 25 works)<br/>
						<b>My SMTP Server Requires Authentication:</b> &nbsp;&nbsp; Checked (Ticked)<br /><br />
						For any further information, please visit http://support.smeerptech.com
					</div>";
					$data['ticket_owner_uid']=$details['client'];
					$data['ticket_owner'] = $details['billing_name'];
					$data['hrs']=0;
					$data['min']=5;					
					$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
					$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
					$min1 = (int) ($data['min'] * HRS_FACTOR);
					//disply random name BOF 
					$data['display_name']=$data['display_user_id']=$data['display_designation']='';
					$randomUser = getRandomAssociate($data['executive_id']);
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
					$mail_to_all_su = $premium_email_mgmt_to_str;
					$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
						. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
						. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
						. TABLE_ST_TICKETS .".invoice_profm_id    = '". $invoice_profm_id ."', "
						. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
						. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
						. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
						. TABLE_ST_TICKETS .".on_behalf        = '". $data['on_behalf'] ."', "
						. TABLE_ST_TICKETS .".template_id      = '". $data['template_id'] ."', "
						. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_ST_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
						. TABLE_ST_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
						. TABLE_ST_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
						. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
						. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
						. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
						. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
						. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
						. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
						. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
						. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
						. TABLE_ST_TICKETS .".ticket_creator_uid    = '". $my['uid'] ."', "
						. TABLE_ST_TICKETS .".ticket_creator         = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_ST_TICKETS .".ticket_subject    	 = '". $data['ticket_subject'] ."', "
						. TABLE_ST_TICKETS .".ticket_mail_subject    = '". $data['ticket_subject'] ."', "
						. TABLE_ST_TICKETS .".ticket_text         = '". $data['ticket_text'] ."', "
						. TABLE_ST_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
						. TABLE_ST_TICKETS.".last_comment_by 	  = '".$my['uid'] ."', "
						. TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
						//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
						//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
						//. TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
						. TABLE_ST_TICKETS .".mail_ceo        = '".$mail_ceo."', "
						. TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
						. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
						. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
						. TABLE_ST_TICKETS .".ticket_child      = '0', "
						. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_ST_TICKETS .".min = '". $data['min']."', "
						. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
						. TABLE_ST_TICKETS .".ticket_response_time = '0', "
						. TABLE_ST_TICKETS .".ticket_replied        = '0', "
						. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
						//. TABLE_ST_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
						. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
						. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
						. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
						. TABLE_ST_TICKETS .".is_private           = '". $data['is_private'] ."', "
						. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
						. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ; 
						 
						if ($db->query($query) && $db->affected_rows() > 0) { 
							$variables['hid'] = $db->last_inserted_id() ;
							$data['ticket_no']    =   $ticket_no ;
							$data['domain']   = THIS_DOMAIN;                        
							//$data['replied_by']   =   $data['ticket_owner'] ;
							$data['mticket_no']   =   $ticket_no ;
							$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
							$data['subject']    =     $data['ticket_subject'] ;
							$data['text']    =   $data['ticket_text'] ;							
							 
							if(!empty($premium_email_mgmt_to)){
								getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
								$cc_to_sender=$cc=$bcc='';
								/* 
								$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
								'email' => $smeerp_client_email); 
								*/
								$from = $reply_to= array('name' => $email["from_name"], 'email' => $email['from_email']); 
								SendMailSupport($premium_email_mgmt_to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
								$cc_to_sender,$cc,$bcc,$file_name);
							}
						}   
						
						$accountNumber = $_ALL_DATA['domain_details']['accountNumber'] ;  
						$url_string = "/customers/".$accountNumber."/domains/".$_ALL_DATA['email_domain']."/rs/mailboxes?size=500&offset=0"; 
						$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
						$headers = array();
						$headers = array("Accept: $format"); 
						$curl_session = curl_init();
						curl_setopt($curl_session, CURLOPT_URL, $url1);
						$time_stamp = date('YmdHis'); 
						$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
						$signature = base64_encode(sha1($data_to_sign, true)); 
						$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
						$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
						// comment me for time being
						//curl_setopt($curl_session, CURLOPT_HEADER, true);
						curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
						$httpResponse = curl_exec($curl_session);
						curl_close($curl_session);	
						$_ALL_DATA['rs_email'] = array();
						$_ALL_DATA['rs_email'] = json_decode(stripslashes($httpResponse),true); 						
				}else{
					$messages->setErrorMessage($httpResponse2); 
				}			 
			}
			
			if($data['form_type']=='allot_no_mailbox'){
				$accountNumber = $_ALL_DATA['domain_details']['accountNumber'] ;  
				$serv_type = $data['email_service'];
				$url_string = "/customers/".$accountNumber."/domains/".$_ALL_DATA['email_domain'] ; 
				$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
				$headers = array(); 
				$headers = array("Accept: $format");
				$curl_session = curl_init();
				curl_setopt($curl_session, CURLOPT_URL, $url);
				$time_stamp = date('YmdHis'); 
				$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
				$signature = base64_encode(sha1($data_to_sign, true)); 
				$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
				$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";					 
				 $_ALL_POST['no_of_mailboxes1']  =  $_ALL_POST['no_of_mailboxes'] + $_ALL_DATA['domain_details']['rsEmailMaxNumberMailboxes'];
				$fields =  array( 
					'rsEmailMaxNumberMailboxes' => $_ALL_POST['no_of_mailboxes1'] 
				);
				curl_setopt($curl_session, CURLOPT_POST,1);
				curl_setopt($curl_session, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($curl_session, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($curl_session, CURLOPT_HEADER, true);
				curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
				$httpResponse3 = curl_exec($curl_session); 						 
				curl_close($curl_session);

 				if(strpos($httpResponse3, 'HTTP/1.1 200 OK') !== false ){
					$msg = $data['no_of_mailboxes']." no. of mailboxes allotted successfully in domain name ".$_ALL_DATA['email_domain'].".
					<br/> Total Max No. of Mailboxes are ".$_ALL_POST['no_of_mailboxes1']."
					";
					$messages->setOkMessage($msg);
					
					$data['ticket_owner_uid']	=	$details['client'];
					$data['ticket_owner'] = $details['billing_name'];
					$data['hrs'] = 0;
					$data['min'] = 5;
					$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
					$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
					$min1 = (int) ($data['min'] * HRS_FACTOR);
					//disply random name BOF  
					$ticket_no  =  SupportTicket::getNewNumber($db);
					$data['ticket_subject']	= "No of Mailbox(s) allotted - (".$data['no_of_mailboxes'].") on Domain ".$_ALL_DATA['email_domain'].".";
					$data['ticket_text']= "<div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:18px;\">Dear Sir/Madam,<br/>
					(".$data['no_of_mailboxes'].") no. of mailboxes allotted successfully in domain name ".$_ALL_DATA['email_domain'].".
					<br/> Total Maximum No. of Mailboxes are ".$_ALL_POST['no_of_mailboxes1']."	on domain ".$_ALL_DATA['email_domain']."			 
					</div>";
					$data['display_name']=$data['display_user_id']=$data['display_designation']='';
					$randomUser = getRandomAssociate($data['executive_id']);
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
					$mail_to_all_su = $premium_email_mgmt_to_str;
					$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
						. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
						. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
						. TABLE_ST_TICKETS .".invoice_profm_id    = '". $invoice_profm_id ."', "
						. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
						. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
						. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
						. TABLE_ST_TICKETS .".on_behalf        = '". $data['on_behalf'] ."', "
						. TABLE_ST_TICKETS .".template_id      = '". $data['template_id'] ."', "
						. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_ST_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
						. TABLE_ST_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
						. TABLE_ST_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
						. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
						. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
						. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
						. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
						. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
						. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
						. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
						. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
						. TABLE_ST_TICKETS .".ticket_creator_uid    = '". $my['uid'] ."', "
						. TABLE_ST_TICKETS .".ticket_creator         = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_ST_TICKETS .".ticket_subject    	 = '". $data['ticket_subject'] ."', "
						. TABLE_ST_TICKETS .".ticket_mail_subject    = '". $data['ticket_subject'] ."', "
						. TABLE_ST_TICKETS .".ticket_text         = '". $data['ticket_text'] ."', "
						. TABLE_ST_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
						. TABLE_ST_TICKETS.".last_comment_by 	  = '".$my['uid'] ."', "
						. TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
						//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
						//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
						//. TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
						. TABLE_ST_TICKETS .".mail_ceo        = '".$mail_ceo."', "
						. TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
						. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
						. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
						. TABLE_ST_TICKETS .".ticket_child      = '0', "
						. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_ST_TICKETS .".min = '". $data['min']."', "
						. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
						. TABLE_ST_TICKETS .".ticket_response_time = '0', "
						. TABLE_ST_TICKETS .".ticket_replied        = '0', "
						. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
						//. TABLE_ST_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
						. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
						. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
						. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
						. TABLE_ST_TICKETS .".is_private           = '". $data['is_private'] ."', "
						. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
						. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ; 
						 
						if ($db->query($query) ) { 
							$variables['hid'] = $db->last_inserted_id() ;
							$data['ticket_no']    =   $ticket_no ;
							$data['domain']   = THIS_DOMAIN;                        
							//$data['replied_by']   =   $data['ticket_owner'] ;
							$data['mticket_no']   =   $ticket_no ;
							$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
							$data['subject']    =     $data['ticket_subject'] ;
							$data['text']    =   $data['ticket_text'] ;							
							 
							if(!empty($premium_email_mgmt_to)){
								getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
								$cc_to_sender=$cc=$bcc='';
								/* 
								$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
								'email' => $smeerp_client_email); 
								*/
								$from = $reply_to= array('name' => $email["from_name"], 'email' => $email['from_email']); 
								SendMailSupport($premium_email_mgmt_to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
								$cc_to_sender,$cc,$bcc,$file_name);
								
							}
							header("Location:".DIR_WS_NC."/premium-email-service.php?perform=premium_allotment&id=".$id."&msg=".$msg);
						} 
				}else{
					$messages->setErrorMessage($httpResponse3); 
				}			
			}
			//to flush the data.
			$_ALL_POST	= NULL;
			$data		= NULL; 
		}
	}
	
	if(isset($_POST['btnCancel'])){
		header("Location:".DIR_WS_NC."/premium-email-service.php");
	}
	
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	/* 
	if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
		header("Location:".DIR_WS_NC."/premium-email-service.php?perform=premium_allotment&id=".$id."&msg=".$msg);
	} 
	*/
	
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	/* if( isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ) {
		//header("Location:".DIR_WS_NC."/premium-email-service.php?perform=premium_allotment&id=".$id."&msg=".$msg);   
		include_once ( DIR_FS_INCLUDES .'/premium-email-service-allotment.php');
	}
	*/
	
	$hidden[] = array('name'=> 'perform' ,'value' => 'premium_allotment');
	$hidden[] = array('name'=> 'act' , 'value' => 'save');           
	$hidden[] = array('name'=> 'id' , 'value' => $id);           
	
	$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
	$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');  
	$page["var"][] = array('variable' => '_ALL_DATA', 'value' => '_ALL_DATA');  
	$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');		
	$page["var"][] = array('variable' => 'domainList', 'value' => 'domainList');		
	$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'premium-email-service-allotment.html');
	 
?>