<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    
                    
                    foreach( $field_arr as $key => $field ) {
                            $condition_query .= " ". $table .".". clearSearchString($field,'-') ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                    }   
                }
            }
            $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType,'-') ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
    $sStatusStr='';
    $chk_status = isset($_POST["chk_status"]) ? $_POST["chk_status"]  : (isset($_GET["chk_status"])? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' ) && $sStatus!='') {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
        }
        
       $condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".status IN ('". $sStatusStr ."') ";
       $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";   
       $_SEARCH["chk_status"]  = $chk_status;
       $_SEARCH["sStatus"]     = $sStatus;
    }
    // EO: Status data.
	
	
	
	// BO: Force Status data.
    $fStatusStr='';
    $chk_fstatus = isset($_POST["chk_fstatus"])   ? $_POST["chk_fstatus"]  : (isset($_GET["chk_fstatus"])   ? $_GET["chk_fstatus"]   :'');
    $fStatus    = isset($_POST["fStatus"])      ? $_POST["fStatus"]     : (isset($_GET["fStatus"])      ? $_GET["fStatus"]      :'');
    if ( $chk_fstatus == 'AND' && $fStatus!='') {
        
		if ( $where_added ) {
            $condition_query .= " ". $chk_fstatus;
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($fStatus)){
             $fStatusStr = implode("','", $fStatus) ;
             $fStatusStr1 = implode(",", $fStatus) ; 
        }else{
            $fStatusStr = str_replace(',',"','",$fStatus);
            $fStatusStr1 = $fStatus ;
            $fStatus = explode(",",$fStatus) ; 
        }
	    $condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".f_status IN ('". $fStatusStr ."') ";
	    $condition_url          .= "&chk_fstatus=$chk_fstatus&fStatus=$fStatusStr1";   
	    $_SEARCH["chk_fstatus"]  = $chk_fstatus;
	    $_SEARCH["fStatus"]     = $fStatus;
    }
    // BO: BillStatus data.
    $bStatusStr='';
    $chk_bstatus = isset($_POST["chk_bstatus"])   ? $_POST["chk_bstatus"]  : (isset($_GET["chk_bstatus"])   ? $_GET["chk_bstatus"]   :'');
    $bStatus    = isset($_POST["bStatus"])      ? $_POST["bStatus"]     : (isset($_GET["bStatus"])      ? $_GET["bStatus"]      :'');
    if ( ($chk_bstatus == 'AND' || $chk_bstatus == 'OR' ) && $bStatus!='') {
        if ( $where_added ){
            $condition_query .= " ". $chk_bstatus;
        }else{
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($bStatus)){
             $bStatusStr = implode("','", $bStatus) ;
             $bStatusStr1 = implode(",", $bStatus) ;
        }else{
            $bStatusStr = str_replace(',',"','",$bStatus);
            $bStatusStr1 = $bStatus ;
            $bStatus = explode(",",$bStatus) ;
            
        } 
       $condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_status IN ('". $bStatusStr ."') ";
       $condition_url          .= "&chk_bstatus=$chk_bstatus&bStatus=$bStatusStr1";   
       $_SEARCH["chk_bstatus"]  = $chk_bstatus;
       $_SEARCH["bStatus"]     = $bStatus;
    }
    // EO: Status data.
	
	// BO:fStatus data.
	/*
    $fStatusStr='';
    $chk_fstatus = isset($_POST["chk_fstatus"])   ? $_POST["chk_fstatus"]  : (isset($_GET["chk_fstatus"])   ? $_GET["chk_fstatus"]   :'');
    $fStatus    = isset($_POST["fStatus"])      ? $_POST["fStatus"]     : (isset($_GET["fStatus"])      ? $_GET["fStatus"]      :'');
    if ($chk_fstatus == 'AND'  && $fStatus!=''){
		 
		$searchStr = 1;
        if ( $where_added ){
            $condition_query .= " ". $chk_fstatus;
        }else{
            $condition_query.= ' WHERE ';
            $where_added    = true;
        } 
        if(is_array($fStatus)){
             $fStatusStr = implode("','", $fStatus) ;
             $fStatusStr1 = implode(",", $fStatus) ; 
        }else{
            $fStatusStr = str_replace(',',"','",$fStatus);
            $fStatusStr1 = $fStatus ;
            $fStatus = explode(",",$fStatus) ;
        } 
        $condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".f_status IN ('". $fStatusStr ."') ";
        $condition_url   .= "&chk_fstatus=$chk_fstatus&fStatus=$fStatusStr1";   
        $_SEARCH["chk_fstatus"]  = $chk_fstatus;
        $_SEARCH["fStatus"]     = $fStatus;
    }*/
    // EO: fStatus data.
	
	//BO : Mode
    $sGlIdStr='';
    $chk_gl = isset($_POST["chk_gl"])   ? $_POST["chk_gl"]  : (isset($_GET["chk_gl"])   ? $_GET["chk_gl"]   :'');
    $sGlId    = isset($_POST["sGlId"])      ? $_POST["sGlId"]     : (isset($_GET["sGlId"])      ?$_GET["sGlId"]     :'');
    if ( ($chk_gl == 'AND' || $chk_gl == 'OR' ) && $sGlId!='') {
        $searchStr = 1;
		if ( $where_added ){
           $condition_query .= " ". $chk_gl;
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
              
        if(is_array($sGlId)){
             $sGlIdStr = implode("','", $sGlId) ;
             $sGlIdStr1 = implode(",", $sGlId) ; 
        }else{
            $sGlIdStr = str_replace(',',"','",$sGlId);
            $sGlIdStr1 = $sModeId ;
            $sGlId = explode(",",$sGlId) ;
        } 
        $condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".gl_type  IN ('". $sGlIdStr ."') ";
        $condition_url          .= "&chk_gl=$chk_gl&sGlId=$sGlIdStr1";   
        $_SEARCH["chk_gl"]  = $chk_gl;
        $_SEARCH["sGlId"]     = $sGlId;
    } 
    //EO : Mode
	//BO: Tax Search
	$chk_tax = isset($_POST["chk_tax"]) ? $_POST["chk_tax"]  : (isset($_GET["chk_tax"])   ? $_GET["chk_tax"]   :'');
	$tax_ids = isset($_POST["tax_ids"]) ? $_POST["tax_ids"]:(isset($_GET["tax_ids"]) ? 
	$_GET["tax_ids"]      :'');
	if ( ( $chk_tax == 'AND' || $chk_tax == 'OR') &&  isset($tax_ids)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_tax;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		if($tax_ids=='-1'){
			$condition_query .= " ( ". TABLE_PAYMENT_PARTY_BILLS .".tax_ids = '%,0,%' OR  ".TABLE_PAYMENT_PARTY_BILLS .".tax_ids='' )" ;  
		}else{
			$condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".tax_ids LIKE '%,".$tax_ids.",%' ";  
		}
		$condition_url          .= "&chk_tax=$chk_tax&tax_ids=$tax_ids";
		$_SEARCH["chk_tax"]  = $chk_tax;
		$_SEARCH["tax_ids"]     = $tax_ids;        
	}
	//BO: Tax Search
	
	
	//BO: USED FOR Tax Status
	$chk_utax = isset($_POST["chk_utax"]) ? $_POST["chk_utax"]  : (isset($_GET["chk_utax"])   ? $_GET["chk_utax"]   :'');
	$used_for_tax = isset($_POST["used_for_tax"]) ? $_POST["used_for_tax"]:(isset($_GET["used_for_tax"]) ? 
	$_GET["used_for_tax"]      :'');
	if ( ( $chk_utax == 'AND' || $chk_utax == 'OR') &&  isset($used_for_tax)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_utax;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		 
		$condition_query .= "( ". TABLE_PAYMENT_PARTY_BILLS .".used_for_tax = '".$used_for_tax."' )" ;  
		 
		$condition_url .= "&chk_utax=$chk_utax&used_for_tax=$used_for_tax";
		$_SEARCH["chk_utax"]  = $chk_utax;
		$_SEARCH["used_for_tax"]     = $used_for_tax;        
	}
	//BO: USED FOR Tax Status
	//Client serach bof
	$search_client = isset($_POST["search_client"])   ? $_POST["search_client"]  : (isset($_GET["search_client"])   ? $_GET["search_client"]   :'');
	
	if(!empty($search_client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }else{
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".party_id ='". $search_client ."' ";        
        $condition_url   .= "&search_client=$search_client";
		
		$table = TABLE_CLIENTS;
		$fields1 =  TABLE_CLIENTS .'.f_name,'.TABLE_CLIENTS.'.l_name,'.TABLE_CLIENTS.'.billing_name' ;
		$condition1 = " WHERE ".TABLE_CLIENTS .".user_id ='".$search_client."' " ;
		$arr = getRecord($table,$fields1,$condition1);
		if(!empty($arr)){
			$search_client_details = $arr['f_name']." ".$arr['l_name']." (".$arr['billing_name'].")";
		}
        $_SEARCH["search_client_details"]  = $search_client_details;
        $_SEARCH["search_client"]     = $search_client;       
	
	}
	//Client serach eof
    
    //Bank/CAs serach bof
	$search_vendor_bank_id = isset($_POST["search_vendor_bank_id"])   ? $_POST["search_vendor_bank_id"]  : (isset($_GET["search_vendor_bank_id"])   ? $_GET["search_vendor_bank_id"]   :'');
	
	if(!empty($search_vendor_bank_id)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".vendor_bank_id ='". $search_vendor_bank_id ."' ";        
        $condition_url  .= "&search_vendor_bank_id=$search_vendor_bank_id";
		$table = TABLE_VENDORS_BANK;
		$fields1 =  TABLE_VENDORS_BANK .'.f_name,'.TABLE_VENDORS_BANK.'.l_name,'.TABLE_VENDORS_BANK.'.billing_name' ;
		$condition1 = " WHERE ".TABLE_VENDORS_BANK .".user_id ='".$search_vendor_bank_id."' " ;
		$arr = getRecord($table,$fields1,$condition1);
		if(!empty($arr)){
			$search_vendor_bank_details = $arr['f_name']." ".$arr['l_name']." (".$arr['billing_name'].")";
		}
        $_SEARCH["search_vendor_bank_id"]  = $search_vendor_bank_id;
        $_SEARCH["search_vendor_bank_details"]     = $search_vendor_bank_details; 
	}
	//Bank/CAs  serach eof
	





	
	//BO : Company
    $sCompIdStr='';
    $chk_cmp = isset($_POST["chk_cmp"])   ? $_POST["chk_cmp"]  : (isset($_GET["chk_cmp"])   ? $_GET["chk_cmp"]   :'');
    $sCompId    = isset($_POST["sCompId"])      ? $_POST["sCompId"]     : (isset($_GET["sCompId"])      ?$_GET["sCompId"]     :'');
     
    if ( ($chk_cmp == 'AND' || $chk_cmp == 'OR' ) && !empty($sCompId)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_cmp;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
              
        if(is_array($sCompId)){
            $sCompIdStr = implode("','", $sCompId) ;
            $sCompIdStr1 = implode(",", $sCompId) ;
        }else{
            $sCompIdStr = str_replace(',',"','",$sCompId);
            $sCompIdStr1 = $sCompId ;
            $sCompId = explode(",",$sCompId) ;
        }
        $condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".company_id  IN ('". $sCompIdStr ."') ";
        $condition_url          .= "&chk_cmp=$chk_cmp&sCompId=$sCompIdStr1";   
        $_SEARCH["chk_cmp"]  = $chk_cmp;
        $_SEARCH["sCompId"]     = $sCompId;
    } 
    //EO : Company
	
	//Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])    ? $_POST["dt_field"]      : (isset($_GET["dt_field"])  ? $_GET["dt_field"] :'');
 
    // BO: From Date
    if(($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }else{
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        //$condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".do_e >= '". $dfa ."'";
        $condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".".$dt_field." >= '". $dfa ."'";
        
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
		 $_SEARCH["dt_field"]       = $dt_field;
    }
    // EO: From Date
    
    // BO: Upto Date
    if (( $chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)){
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_PAYMENT_PARTY_BILLS .".".$dt_field." <= '". $dta ."'";        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
		 $_SEARCH["dt_field"]       = $dt_field;
    }
    
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/payment-party-bills-list.php');
?>
