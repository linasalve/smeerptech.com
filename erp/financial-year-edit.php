<?php
if ( $perm->has('nc_fy_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        include_once (DIR_FS_INCLUDES .'/financial-year.inc.php');
        include_once (DIR_FS_INCLUDES .'/company.inc.php');	
       	$lst_company = NULL;
        $required_fields ='*';
		Company::getList($db,$lst_company,$required_fields);
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST  = $_POST;
		$data       = processUserData($_ALL_POST);
                        
		$extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
		if ( FinancialYear::validateUpdate($data, $extra) ) {
			
			$db_prefix 	= new db_local();
			
			$prefix_query="SELECT prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id = '".$data['company_id']."'";
			
			$db_prefix->query( $prefix_query );
			$db_prefix->next_record();
			$prefix=$db_prefix->f("prefix");
			
			$query  = " UPDATE ". TABLE_FINANCIAL_YEAR
					." SET ". TABLE_FINANCIAL_YEAR .".year_range = '".		$data['year_range'] ."'"                            		
						.",". TABLE_FINANCIAL_YEAR .".company_prefix = '". 		$prefix ."'"						
                    	.",". TABLE_FINANCIAL_YEAR .".company_id = '". 		$data['company_id'] ."'"						
                    ." WHERE id = '". $id ."'";
                
			if ( $db->query($query) && $db->affected_rows() > 0 ) {
				$messages->setOkMessage("Record has been updated successfully.");
				$variables['hid'] = $id;
			}
			$data       = NULL;
		}
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_FINANCIAL_YEAR .'.*'  ;            
            $condition_query = " WHERE (". TABLE_FINANCIAL_YEAR .".id = '". $id ."' )";
            
	if ( FinancialYear::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                
		}
	else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
		}
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/financial-year-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
      
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');               
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'financial-year-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
