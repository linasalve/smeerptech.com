<?php
    if ( $perm->has('nc_ss_add') ) {
		include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }*/
		 // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
  /*       $condition1 = " WHERE status='".User::ACTIVE."' AND user_id NOT IN('50a9c7dbf0fa09e8969978317dca12e8',
        'e68adc58f2062f58802e4cdcfec0af2d','febc8f8ac083f5fc27e032c81e7b536a') ORDER BY f_name";    */    

		$condition1 = " WHERE status='".User::ACTIVE."' ORDER BY f_name";   		
        User::getList($db,$lst_executive,$fields,$condition1);
        // BO:  Read the Team members list ie member list in dropdown box  EOF 
		 
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
            
            if(empty($data['hide_my_name'])){
            	$data['hide_my_name']=0;
            }	            
            if(empty($data['hide_my_comment'])){
            	$data['hide_my_comment']=0;
            }
            
            $data['comment_to']=$data['comment_by']=$my['user_id'];
            $data['score']=3;
            
            if ( Scoresheet::validateAdd($data, $extra) ) { 
                
                $comment_to = implode(",",$data['comment_to']);
				$comment_to_user = str_replace(",","','",$comment_to);
                $condition_ct = " status='".User::ACTIVE."' AND user_id IN('".$comment_to_user."') ORDER BY f_name";
                $ct_field =TABLE_USER.".user_id, CONCAT_WS(' ', ".TABLE_USER.".f_name,
					".TABLE_USER.".l_name) AS name " ;
                $sql= " SELECT ".$ct_field." FROM ".TABLE_USER." WHERE ".$condition_ct;
				$db1 = new db_local; // database handle
                if ( $db->query($sql) ) {
					if ( $db->nf() > 0 ) {
						while($db->next_record()){ 
							$comment_to = $db->f('user_id') ;
							$comment_to_name = $db->f('name'); 
							
							$query	= " INSERT INTO ".TABLE_SCORE_SHEET
                            ." SET ". TABLE_SCORE_SHEET .".comment_to =  ',".$comment_to.",'"
							.",". TABLE_SCORE_SHEET .".comment_to_name = ',".$comment_to_name .",'"
							.",". TABLE_SCORE_SHEET .".particulars = '".  $data['particulars'] ."'"
							.",". TABLE_SCORE_SHEET .".score       = '".  $data['score'] ."'"
							.",". TABLE_SCORE_SHEET .".reason_id = '". $data['reason_id'] ."'"
							.",". TABLE_SCORE_SHEET .".hide_my_name = '". $data['hide_my_name'] ."'"
							.",". TABLE_SCORE_SHEET .".hide_my_comment = '". $data['hide_my_comment'] ."'"
							.",". TABLE_SCORE_SHEET .".access_level = '".$my['access_level'] ."'"
							.",". TABLE_SCORE_SHEET .".created_by = '".$data['created_by'] ."'"
							.",". TABLE_SCORE_SHEET .".created_by_name = '".$my['f_name']." ".$my['l_name']."'"
							.",". TABLE_SCORE_SHEET .".date = '". date('Y-m-d') ."'";
							
							//echo "<br/>";
							$db1->query($query);	
							$dataDetails['date'] =  date('d M Y h:i:s') ;
							$dataDetails['score'] = $data['score'] ;
							$dataDetails['particulars'] = $data['particulars'] ;
							$dataDetails['score_to'] = $comment_to_name ;
							$dataDetails['score_by'] = $my['f_name']." ".$my['l_name']  ;
							/*
							if( getParsedEmail($db1, $s, 'SCORE_ADD', $dataDetails, $email)){
								$to     = $bcc = '';
								$to[]   = array('name' => 'Admin', 'email' => $admin_email);
								$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);  
							} */
						} 
						$messages->setOkMessage("Score entry has been done.");
					}
				} 
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
         
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/learning.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){ 
            header("Location:".DIR_WS_NC."/learning.php?perform=list");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/learning.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');   
			$error_messages = $messages->getErrorMessages();
			$success_messages = $messages->getOkMessages();
            /*  
		    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');      
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'score-sheet-add.html'); 
			*/
			?>
			
			
			<table cellpadding="1" cellspacing="1" align="center" style="border:0;width:99%;">
			<tr>
				<td style="padding-left:5px;" align="left"><span class="pre_color txt12 bold">New Learning</span></td>
			</tr>
			<tr>
				<td style="padding-right:5px;" class="txthelp" align="right">The &lsquo;<span class="star">*</span>&rsquo; marked fields are compulsory</td>
			</tr>
			<tr>
				<td align="center"><?php include(DIR_FS_NC."/messages.php"); ?></td>
			</tr>
			<tr>
				<td style="padding-left:00px;">
					<div class="coloumn" style="height:100%;width:70%;">
						<form name="frmAdd" action="<?php echo $variables['nc'].'/learning.php';?>" method="post">
						<table cellpadding="1" cellspacing="1" border="0" width="100%" align="left" class="tbl_form">
						    
						<tr class="{cycle values='odd,even'}">
							<td><span class="star">*</span></td>
							<td><span class="txtfield">Particulars</span></td>
							<td>
								<textarea name="particulars" class="inputbox" cols="80" rows="10"><?php echo $_ALL_POST['particulars']; ?></textarea>
								<br/><span class="txthelp">Enter your learning</span>
							</td>
						</tr> 
						<!--
						<tr class="{cycle values='odd,even'}">
							<td><span class="star">*</span></td>
							<td><span class="txtfield">Score</span></td>
							<td>
							   <select name="score" width="200px">
								<option value="">Score</option>
								{foreach from=$variables.score item=score1 key=skey}
								{strip}<option value="{$score1}"                        
									{if ($score == $score1) } selected="selected"{/if}                      
								>{$skey}</option>{/strip}
								{/foreach}
								</select>
								<br/><span class="txthelp">Select score.</span>
							</td>
						</tr>
						<tr class="{cycle values='odd,even'}">
							<td><span class="star"></span></td>
							<td><span class="txtfield"></span></td>
							<td>
								<input type="checkbox" name="hide_my_name" value="1" {if $_ALL_POST.hide_my_name == "1"}checked="checked"{/if}/>
								Hide my name
							</td>
						</tr>    -->         
						<tr class="{cycle values='odd,even'}">
							<td class="normaltext" colspan="2">&nbsp;</td>
							<td class="normaltext" valign="top">
								<input type="submit" name="btnCreate" value="Save" class="button"/>
								&nbsp;&nbsp;&nbsp;
								<input type="submit" name="btnReturn" value="Save &amp; Return" class="button"/>
								&nbsp;&nbsp;&nbsp;
								<input type="submit" name="btnCancel" value="Cancel" class="button">
								<?php 
								foreach($hidden as $key=>$item){
									echo "<input type=\"hidden\" name=\"".$item['name']."\" value=\"".$item['value']."\" />";
								}
								?>
							</td>
						</tr>
						</table>
						</form>
					</div>
				</td>
			</tr>
			</table> 	
			
			<?php			
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>