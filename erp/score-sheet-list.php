<?php
    if ( $perm->has('nc_ss_list') ) {        
        include (DIR_FS_NC .'/score-sheet-board.php');

		if ( $perm->has('nc_ss_all_list') ) {
		 
		}elseif ( $perm->has('nc_ss_mylist') ) {
		 
			if(empty($condition_query)){
			
				$condition_query .= " WHERE ( ".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$my['user_id'].",%' 
					OR ".TABLE_SCORE_SHEET.".created_by ='".$my['user_id']."')";
			}else{
			 
				$condition_query .= " AND ( ".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$my['user_id'].",%' 
					OR ".TABLE_SCORE_SHEET.".created_by ='".$my['user_id']."')";
			}
		 
		}
		
		
		 $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
            $_SEARCH['search_by']=1;
        }
        $_SEARCH['searched'] = 1;
		
        // To count total records.
        $list	= 	NULL;
        if( isset($_SEARCH['search_by']) && $_SEARCH['search_by'] ==1){        
            $condition_query = " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_SCORE_SHEET .".created_by ".$condition_query ;
                        
        }elseif(isset($_SEARCH['search_by']) && $_SEARCH['search_by'] ==2){        
            $condition_query = " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_SCORE_SHEET .".comment_to ".$condition_query ;                        
        }
       
		
        $total	=	Scoresheet::getDetails( $db, $list, '', $condition_query);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
        $list	= NULL;
        $fields = TABLE_SCORE_SHEET .'.*';       
        Scoresheet::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        $reasonArr = array();
		$reasonArr = Scoresheet::getReason();
       
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               $executive=array();
               $string = str_replace(",","','", $val['comment_to']);
               $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
               User::getList($db,$executive,$fields1,$condition1);
               $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['comment_to'] = $executivename ;
               
               $exeCreted=$exeCreatedname='';
               $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
               User::getList($db,$exeCreted,$fields1,$condition2);
              
               foreach($exeCreted as $key2=>$val2){
                    $exeCreatedname .= $val2['f_name']." ".$val2['l_name']."<br/>" ;
               } 
               $val['created_by'] = $exeCreatedname;             
               
               if(!empty($val['reason_id'])){
                   $val['reason_title']= $reasonArr[$val['reason_id']];
               }
               
               $fList[$key]=$val;
            }
        }
      
        
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_add_followup']  = false;
       	$variables['can_view_hname']  	= false;
       	$variables['can_view_hcomment'] = false;
       	$variables['can_print'] = false;
       
        if ( $perm->has('nc_ss_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_ss_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_ss_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_ss_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_ss_details') ) {
            $variables['can_view_details'] = false;
        }
        if ( $perm->has('nc_ss_add_comment') ) {
            $variables['can_add_followup'] = true;
        }
        if ( $perm->has('nc_ss_hname') ) {
            $variables['can_view_hname'] = true;
        }
        if ( $perm->has('nc_ss_hcomment') ) {
            $variables['can_view_hcomment'] = true;
        }
        if ( $perm->has('nc_ss_print') ) {
            $variables['can_print'] = true;
        }
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN

        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'score-sheet-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>