<?php
    if ( $perm->has('nc_sugg_add') ) {
        
      
        $access_level   = $my['access_level'];
         
		 
        
       
        $allotted_to_str1=$allotted_to_client_str1=$allotted_to_lead_str1=$allotted_to_addr_str1='';
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
             
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
            $allotted_to_str1=$allotted_to_client_str1=$allotted_to_lead_str1=$allotted_to_addr_str1='';
            $allotted_to_name = $allotted_to_client_name = $allotted_to_lead_name=$allotted_to_addr_name ='';
            
            
            
            if ( Suggestions::validateAdd($data, $extra) ) {
            
                if(!empty($data['allotted_to'])) {
                    $allotted_to_str1 = implode(",",$data['allotted_to']);
                    //$allotted_to_str1.=",".$data['created_by'];
                    $allotted_to_str = ",".$allotted_to_str1.",";   
                    
                    $sql= " SELECT f_name,l_name FROM ".TABLE_USER." WHERE user_id 
					IN('".str_replace(",","','",$allotted_to_str1)."') AND ".TABLE_USER.".status='".User::ACTIVE."'";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_name1 = trim($allotted_to_name,",") ;
                        $allotted_to_name = ",".$allotted_to_name1."," ;
                    }
                } 
				
				$task_no  =  Suggestions::getNewNumber($db);
                $query	= " INSERT INTO ".TABLE_SUGGESTIONS
						." SET ". TABLE_SUGGESTIONS .".task = '".   $data['task'] ."'"
						.",". TABLE_SUGGESTIONS .".task_no = '".              $task_no ."'"                        
						//.",". TABLE_SUGGESTIONS .".is_imp = '".               $is_imp ."'"                        
						.",". TABLE_SUGGESTIONS .".comment = '".              $data['comment'] ."'"
						.",". TABLE_SUGGESTIONS .".access_level = '".         $my['access_level'] ."'"
						.",". TABLE_SUGGESTIONS .".created_by = '".           $data['created_by'] ."'"
						.",". TABLE_SUGGESTIONS .".created_by_name = '".      $my['f_name']." ". $my['l_name']."'"
						.",". TABLE_SUGGESTIONS .".do_r = '".                 $data['do_r'] ."'"
						.",". TABLE_SUGGESTIONS .".do_deadline = '".          $data['do_deadline'] ."'"
						//.",". TABLE_SUGGESTIONS .".type = '".                 $data['type'] ."'"
						.",". TABLE_SUGGESTIONS .".status = '".               $data['status'] ."'"
						//.",". TABLE_SUGGESTIONS .".priority = '".             $data['priority'] ."'"
						//.",". TABLE_SUGGESTIONS .".send_sms = '".             $data['send_sms'] ."'"
						//.",". TABLE_SUGGESTIONS .".groups = '".          $groupsStr ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to = '".          $allotted_to_str ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to_name = '".     $allotted_to_name ."'"
						/* 
						.",". TABLE_SUGGESTIONS .".allotted_to_client = '".   $allotted_to_client_str ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to_client_name = '".$allotted_to_client_name ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to_vendor = '".   $allotted_to_vendor_str ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to_vendor_name = '".$allotted_to_vendor_name ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to_lead = '".     $allotted_to_lead_str ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to_lead_name = '".$allotted_to_lead_name ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to_addr = '".     $allotted_to_addr_str ."'"
						.",". TABLE_SUGGESTIONS .".allotted_to_addr_name = '".$allotted_to_addr_name."'"
						.",". TABLE_SUGGESTIONS .".on_behalf_id = '".         $data['on_behalf_id'] ."'"
						.",". TABLE_SUGGESTIONS .".order_id = '".             $data['order_id'] ."'"
						.",". TABLE_SUGGESTIONS .".job_id = '".               $data['job_id'] ."'" 
						.",". TABLE_SUGGESTIONS .".hrs = '".                  $data['hrs'] ."'"
						.",". TABLE_SUGGESTIONS .".min = '".                  $data['min'] ."'"
						.",". TABLE_SUGGESTIONS .".time_type = '".            $data['time_type'] ."'"*/
						.",". TABLE_SUGGESTIONS .".do_e = '".                 date('Y-m-d') ."'";
            
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                
                    $messages->setOkMessage("Task entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                   
                    $statusArr = Suggestions::getStatus();
                    $statusArr = array_flip($statusArr);
                    //$priorityArr = Suggestions::getPriority();
                    //$priorityArr = array_flip($priorityArr);
                    $data['task_title']       =   $_ALL_POST['task'];
                    $data['task']       =   $_ALL_POST['comment'];
                    $data['task_no']       =   $task_no;
                    //$data['comment']    =   $data['comment'] ;
                    $data['do_r']       =   $data['do_r'];
                    $data['do_deadline']=   $data['do_deadline'];
                    $data['af_name']    =   $my['f_name'] ;
                    $data['al_name']    =   $my['l_name'] ;
                    $data['status']     =   $statusArr[$data['status']] ;
                    $data['priority']     =   $priorityArr[$data['priority']] ;
                    $send_to = $send_to_client= $send_to_vendor='';
                    // Send Email to the users involved BOF  
                    //include ( DIR_FS_INCLUDES .'/user.inc.php');
                    /*
                    if(!empty( $allotted_to_str1)){                          
                        $allotted_to_str1 = trim($allotted_to_str1,",");
                        $string = str_replace(",","','", $allotted_to_str1);
                    }
                    */
                    if(!empty($data['allotted_to'])) {
                        //$string1 = implode("','",$data['allotted_to']);
                        $string1 = $allotted_to_str1;
                        //$string2 = $data['created_by'];
                        //$string = $string1."','".$string2;
                        $string = $string1;
						
                    }else{
						$string1 = $allotted_to_str1;
                        //$string2 = $data['created_by'];
                        //$string = $string1."','".$string2;
						 $string = $string1;
                    }
                  
                    User::getList($db, $userDetails, 'number, f_name, l_name, email,mobile1,mobile2', " WHERE 
					user_id IN('".$string."') AND ".TABLE_USER.".status='".User::ACTIVE."'" );
					
					
                    $data['mail_exec']=0;
                    
					/*
                    if(!empty($userDetails) && isset($data['mail_exec']) && $data['mail_exec']==1){
                        foreach ( $userDetails as $user ) {
                             $team_members .=$user['f_name']." ".$user['l_name']."<br/>" ;							 
                        }
						$data['team_members'] = $team_members ;
						foreach ( $userDetails as $user ) {
                            $data['uf_name']    =   $user['f_name'];
                            $data['ul_name']    =   $user['l_name'] ;
                            $email = NULL;                            
                            if ( getParsedEmail($db, $s, 'SUGGESTION_ADD', $data, $email) ) {

                               
								$to = '';
								$to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 
								'email' => $user['email']);                          
								$send_to .=$user['email'].",";
								$from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);

                            }
                        }
                        $send_to = trim($send_to,",");
                    }else{
                        foreach ( $userDetails as $user ) {
                             $team_members .=$user['f_name']." ".$user['l_name']."<br/>" ;							 
                        }
						$data['team_members'] = $team_members ;
						if(in_array('e68adc58f2062f58802e4cdcfec0af2d',$data['allotted_to'])){
							//Aryan Sir alloted in task then send mail
							$data['uf_name']    =   'Aryan';
                            $data['ul_name']    =  'Salve' ;
							$user['email'] 		= 'aryan@smeerptech.com';
                            $email = NULL;                            
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_ADD', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $user['uf_name'] .' '. $user['ul_name'], 'email' => 
								$user['email']);                          	   
								echo $email['subject']."";
                                $send_to .=$user['email'].",";
                          $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
								
							}
						}  
                    }*/
					$data['sms_sent_mobile']='';
					$counter=0;
					if($data['send_sms']==1){
						foreach ( $userDetails as $user ) {
							if(!empty($user['mobile1'])){
								
								$data['sms_sent_mobile'] .= $user['mobile1'].",";
								$data['text'] = substr($data['task'], 0, 130);
								
								$mobile1 = "91".$user['mobile1'] ;
								ProjectTask::sendSms($data['text'],$mobile1);
								$counter = $counter+1;
							}
							if(!empty($user['mobile2'])){
								$mobile2 = "91".$user['mobile2'];
								$data['text'] = substr($data['task'], 0, 130);
								
								ProjectTask::sendSms($data['text'],$mobile2);
								$counter = $counter+1;
								$data['sms_sent_mobile'] .= $user['mobile2'].",";
							}
                            if(!empty($data['sms_sent_mobile'])){
								
								$query_update = "UPDATE ". TABLE_SUGGESTIONS 
                                ." SET ". TABLE_SUGGESTIONS .".sms_sent_mobile 
									= '".trim($data['sms_sent_mobile'],",").  "' "
                                    ." WHERE ". TABLE_SUGGESTIONS .".id = '". $variables['hid'] ."' " ;
								$db->query($query_update) ;
							
							}
							 
						
							//update sms counter
							if($counter >0){
								$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
								$db->query($sql); 
							}
							 
                        }						
						 
					}
                    $data['team_members'] = $team_members ;
                    
                    // Send Email to the users involved EOF
                 
                     
                    
                    
                    //Set the list email-id to whom the mails are sent bof
                  /*   $query_update = "UPDATE ". TABLE_SUGGESTIONS 
                                ." SET ". TABLE_SUGGESTIONS .".mail_send_to = '".trim($send_to,",")."', mail_send_to_client='".trim($send_to_client,",")."', mail_send_to_vendor ='".trim($send_to_vendor,",")."' "                                    
                                ." WHERE ". TABLE_SUGGESTIONS .".id = '". $variables['hid'] ."' " ;
                    $db->query($query_update) ; */
                    //Set the list subuser's email-id to whom the mails are sent eof 
                    
                    
                    // Send Email to the taskadmin BOF  
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'TASK_REMINDER_ADMIN', $data, $email) ) {
                        $to = '';
                        $to[]   = array('name' => $admin_name , 'email' => $admin_email);                           
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                       /*
  					    print_r($to);
                        print_r($from);
                        echo $email["body"];
                        echo $email["subject"]; 
						*/
                        
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                        $cc_to_sender,$cc,$bcc,$file_name);
                    }
                    // Send Email to the admin EOF  
                }
             
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
       
        //Check permissions to add executives
        //$variables['can_view_ulist']     = false;
        $variables['can_view_clist']     = false;
        $variables['can_view_sllist']     = false;
        $variables['can_view_exlist']     = false;
        $variables['can_add_on_behalf']     = false;
        $variables['can_add_impt']     = false;
        $variables['statuspending'] = Suggestions::PENDING ;
        //For Users
        
		
        /* if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/task-reminder.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/task-reminder.php?perform=list");
        } */
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
           
            //include ( DIR_FS_NC .'/payment-transaction-list.php');
            header("Location:".DIR_WS_NC."/suggestions.php?perform=list&added=1");
            
        }else {
            $_ALL_POST['do_r']=$do_r;
            
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
			$hidden[] = array('name'=> 'ajx' ,'value' => $ajx);
			$page["var"][] = array('variable' => 'groups_list', 'value' => 'groups_list');			
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'variables', 'value' => 'variables'); 
            $page["var"][] = array('variable' => 'lst_job', 'value' => 'lst_job'); 
            $page["var"][] = array('variable' => 'lst_type', 'value' => 'lst_type');  
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'suggestions-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>