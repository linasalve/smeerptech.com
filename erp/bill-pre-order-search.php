<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    if ( $table == TABLE_USER) {
                        // Retrieve the User ID from the User Table.
                        $sub_query  = "SELECT user_id FROM ". $table ." WHERE (";
                        foreach( $field_arr as $key => $field ) {
                            $sub_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= ')';
                        
                        if ( $db->query($sub_query) && $db->nf()>0 ) {
                            // Prepare the sub-query to include the User ID in the main query.
                            if ( $where_added ) {
                                $sub_query = "OR (";
                            }
                            else {
                                $sub_query = " WHERE (";
                                $where_added = true;
                            }
                            $user_id = '';
                            while ( $db->next_record() ) {
                                $user_id .= "'". $db->f('user_id') ."',";
                            }
                            $user_id = substr( $user_id, 0, strlen( $user_id ) - 1 );
                            $sub_query .= ' created_by IN ('. $user_id .') ';
                            $sub_query .= ') ';
                        }
                        else {
                            
                            if ( $where_added ) {
                                $sub_query = "OR (0) ";
                            }
                            else {
                                $sub_query = " WHERE (0) ";
                                $where_added = true;
                            }
                        }
                        $condition_query    .= $sub_query;
                        $where_added        = true;
                    }
                    else {
                        foreach( $field_arr as $key => $field ) {
                            $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                        }
                        $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
                    }
                }
            }
            $condition_query .= ") ";
        }
        elseif ( $search_table == TABLE_USER ) {
            // Retrieve the IDs from the User Table.
            $sub_query      = "SELECT user_id FROM ". $search_table ." WHERE (";
            $sub_query      .= " (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $sub_query      .= ') ';
            if ( $db->query($sub_query) && $db->nf()>0 ) {
                // Prepare the sub-query to include the User ID in the main query.
                $sub_query = " WHERE (";
               
                $user_id = '';
                while ( $db->next_record() ) {
                    $user_id .= "'". $db->f('user_id') ."',";
                }
                $user_id = substr( $user_id, 0, strlen( $user_id ) - 1 );
                $sub_query .= ' created_by IN ('. $user_id .') ';
                $sub_query .= ') ';
            }
            else {
                $sub_query = " WHERE (0)";
            }
            
            $condition_query    .= $sub_query;
            $where_added        = true;
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
    // BO: Status data.
    $sStatusStr = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR') && isset($sStatus)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        $condition_query .= " ". TABLE_BILL_PO .".status IN ('". $sStatusStr ."') ";
        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;   
    }

    // EO: Status data   
    // EO: Status data.
    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    
    // BO: From Date
    if ( $chk_date_from == 'AND' || $chk_date_from == 'OR' && !empty($date_from)) {
        if ( $where_added ) {
            $condition_query .= $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " do_po >= '". $dfa ."'";
   
    $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
    $_SEARCH["chk_date_from"]   = $chk_date_from;
    $_SEARCH["date_from"]       = $date_from;
     }
    // EO: From Date
    
    // BO: Upto Date
    if ( $chk_date_to == 'AND' || $chk_date_to == 'OR' && !empty($date_to)) {
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " do_po <= '". $dta ."'";
    
    $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
    $_SEARCH["chk_date_to"] = $chk_date_to ;
    $_SEARCH["date_to"]     = $date_to ;
    }
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/bill-pre-order-list.php');
?>
