<?php
    if ( $perm->has('nc_bl_rcpt_details') ) {
        
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/payment-mode.inc.php');
        include_once ( DIR_FS_INCLUDES .'/payment-bank.inc.php');
        
    
		$rcpt_id         = isset($_GET["rcpt_id"])? $_GET["rcpt_id"]: ( isset($_POST["rcpt_id"])? $_POST["rcpt_id"]: '' );

        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_rcpt_details_al') ) {
            $access_level += 1;
        }

        $condition_query = " WHERE (". TABLE_BILL_RCPT .".id = '". $rcpt_id ."' "
                                ." OR ". TABLE_BILL_RCPT .".number = '". $rcpt_id ."')";

        $condition_query .= " AND ( ";

		// If my has created this Invoice.
		$condition_query .= " (". TABLE_BILL_RCPT .".created_by = '". $my['user_id'] ."' "
							." AND ". TABLE_BILL_RCPT .".access_level < $access_level ) ";
		
		// If my is the Client Manager (Remove if Client manager cannot edit the Order)
		$condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
							." AND ". TABLE_BILL_RCPT .".access_level < $access_level ) ";

        
		// Check if the User has the Right to View Receipts created by other Users.
		if ( $perm->has('nc_bl_rcpt_details_ot') ) {
			$access_level_o   = $my['access_level'];
			if ( $perm->has('nc_bl_rcpt_details_ot_al') ) {
				$access_level_o += 1;
			}
			$condition_query .= " OR ( ". TABLE_BILL_RCPT. ".created_by != '". $my['user_id'] ."' "
								." AND ". TABLE_BILL_RCPT .".access_level < $access_level_o ) ";
		}
		$condition_query .= " )";
			
		$fields = TABLE_BILL_RCPT .'.*'
					.','. TABLE_BILL_INV .'.or_no'
					.','. TABLE_BILL_INV .'.amount as inv_amount'
					.','. TABLE_BILL_INV .'.amount_words as inv_amount_words'
					.','. TABLE_CLIENTS .'.user_id AS c_user_id'
					.','. TABLE_CLIENTS .'.manager AS c_manager'
					.','. TABLE_CLIENTS .'.number AS c_number'
					.','. TABLE_CLIENTS .'.f_name AS c_f_name'
					.','. TABLE_CLIENTS .'.l_name AS c_l_name'
					.','. TABLE_CLIENTS .'.email AS c_email'
					.','. TABLE_CLIENTS .'.status AS c_status';
					
        if ( Receipt::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];

            if ( $_ALL_POST['access_level'] < $access_level ) {
				// Set up the Client Details field.
				$_ALL_POST['client_details']= $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
												.' ('. $_ALL_POST['c_number'] .')'
												.' ('. $_ALL_POST['c_email'] .')';
												
				include ( DIR_FS_INCLUDES .'/user.inc.php');
                // Read the Receipt Creator's Information.
                $_ALL_POST['creator']= '';
                User::getList($db, $_ALL_POST['creator'], 'user_id,username,number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                $_ALL_POST['creator'] = $_ALL_POST['creator'][0];
				
                // Read the Client Managers Information.
                $_ALL_POST['manager']= '';
                User::getList($db, $_ALL_POST['manager'], 'user_id,username,number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['c_manager'] ."'");
                $_ALL_POST['manager'] = $_ALL_POST['manager'][0];
				
				
				// Read the Particulars.
                $temp = NULL;
				//$condition_query = " WHERE inv_no = '". $_ALL_POST['number'] ."'";
                $condition_query = " WHERE ord_no = '". $_ALL_POST['or_no'] ."'";
				Receipt::getParticulars($db, $temp, '*', $condition_query);
                $_ALL_POST['particulars']=$temp;
                
                include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
				$_ALL_POST['services'] = NULL;
				
                $condition_query= " LEFT JOIN ".TABLE_BILL_ORD_P." ON ".TABLE_BILL_ORD_P.".s_id =".TABLE_SETTINGS_SERVICES.".ss_id 
                                    WHERE ".TABLE_BILL_ORD_P.".ord_no='".$_ALL_POST['or_no']."' ORDER BY ss_title ASC";
                $fields = TABLE_SETTINGS_SERVICES.'.ss_id,'.TABLE_SETTINGS_SERVICES.'.ss_title' ;
				Services::getList($db, $_ALL_POST['services'],  $fields, $condition_query);
			
				$condition_query = '';
                // //Payment details bof
                $pm_Arr	= NULL;
                $fpm = TABLE_PAYMENT_MODE.'.payment_mode' ;
                $conditionpm= " WHERE ".TABLE_PAYMENT_MODE.".id=".$_ALL_POST['p_mode'];
                Paymentmode::getDetails($db,$pm_Arr, $fpm, $conditionpm);
                if(!empty($pm_Arr)){
                    $pm_Arr = $pm_Arr[0];
                    $_ALL_POST['payment_mode'] = $pm_Arr['payment_mode'];
                }
                
                if($_ALL_POST['p_mode'] == '1' ){
                    //ie p_mode is cheque
                    
                    if($_ALL_POST['do_pay_received'] =='0000-00-00'){
                        $_ALL_POST['do_pay_received'] ='';
                    }
                    if(!empty($_ALL_POST['pay_bank_company'])){                    
                        $bankarr	= NULL;
                        $conditionfb = " WHERE ".TABLE_PAYMENT_BANK.".id = ".$_ALL_POST['pay_bank_company'];
                        $fpb = TABLE_PAYMENT_BANK.'.bank_name' ;
                        Paymentbank::getDetails($db, $bankarr, $fpb, $conditionfb);
                        if(!empty($bankarr)){
                            $bankarr = $bankarr[0];
                            $_ALL_POST['pay_bank_company'] = $bankarr['bank_name'];
                        }                                
                    }
                }
        
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-receipt-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Receipt with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("Records not found to view.");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>