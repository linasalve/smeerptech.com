<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
	include_once ( DIR_FS_CLASS."/class.sms.php");	

    if ( $perm->has('nc_sms_crbal') ) {
		$objSms = new sms();

		$sql = "SELECT ma_username,ma_password,ma_posturl FROM ".TABLE_SMS_MASTER_ACC;
		$db->query($sql);
		if($db->nf() > 0)
		{
			$db->next_record();
			$res["username"] = $db->f("ma_username");
			$res["password"] = $db->f("ma_password");
	
			$objSms->setAttribute($res["username"],$res["password"],@$res["ma_posturl"]);
			$response = $objSms->getCreditBalance();
			preg_match_all('|<Credit Limit="(.*)" Used="(.*)"/>|U', $response,$result);

			$arr[0] = $result[1][0]; // limit
			$arr[1]	= $result[2][0]; // used	
			$bal    = ($arr[0] - $arr[1]);
	
			$s->assign("limit",$arr[0]);
			$s->assign("used",$arr[1]);
			$s->assign("bal",$bal);
		}	
		else
			echo "no";
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Access this Module.");
    }
    
    $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-credit-balance.html');
    $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    
	//$page["var"][] = array('variable' => 'limit', 'value' => $arr[0]);

    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>