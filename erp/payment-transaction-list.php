<?php

if ( $perm->has('nc_p_pt_list')  || $perm->has('nc_p_pt_search') ) {  
    
    if($searchStr==1){ 
		include (DIR_FS_NC .'/payment-transaction-board.php');
    }
    
    // If the task is created by the my.
    $access_level   = $my['access_level'];
    
	/*
    if ( $perm->has('nc_ts_list_al') ) {
        $access_level += 1;
    }
    */
    $show			= isset($_GET["show"])         ? $_GET["show"]        : ( isset($_POST["show"])          ? $_POST["show"]       :''); 
    
    //***************EOF**********
	
    if ( empty($condition_query )) { 
            $where_added=true;
			$condition_query = " WHERE ".TABLE_PAYMENT_TRANSACTION .".status = '". Paymenttransaction::PENDING ."'
			AND ".TABLE_PAYMENT_TRANSACTION .".f_status = '". Paymenttransaction::FORCEOPEN ."'" ;
        
        
        $_SEARCH["chk_status"]  = 'AND';
        $_SEARCH["sStatus"]    = array(Paymenttransaction::PENDING);
        $_SEARCH["chk_fstatus"]  = 'AND';
		$_SEARCH["fStatus"]    = array(Paymenttransaction::FORCEOPEN);
		 
        $perform = 'search';
        $condition_url   = "&chk_status=".$_SEARCH["chk_status"]."&sStatus=".Paymenttransaction::PENDING."&chk_fstatus=".$_SEARCH["chk_fstatus"]."&fStatus=".Paymenttransaction::FORCEOPEN ;  
    } 
	
	if ($perm->has('nc_uc_tran_purchase_classified') ) {
			  
	}else{			 
		if( $where_added ) {
			$condition_query .= " AND ";
		}else{
			$condition_query.= ' WHERE ';
			$where_added    = true;
		} 
		$condition_query .= "  (".TABLE_CLIENTS .".is_tran_purchase_classified ='".Clients::CLASSIFIED_NO ."' 
									OR ".TABLE_PAYMENT_TRANSACTION.".executive_id='')" ;     
	}
    //***************BOF**********
    
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;

    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    $extra_url  = '';    
    if ( isset($condition_url) && !empty($condition_url) ){
        $extra_url  = $condition_url;
    }        
    $condition_url .="&perform=".$perform."&show=1&rpp=$rpp";
    //echo $condition_query;
    $fList =$plist	= $list	= array();
	$total=$total_pending=0;
	$pagination='';
	
	$totalAmt =array();
	$totalAmountInr=$totalAmount=$totalBalanceInr=$totalBalance='';
	/* $fields=" COUNT(".TABLE_PAYMENT_TRANSACTION.".id) as a, SUM(".TABLE_PAYMENT_TRANSACTION.".pay_received_amt_inr) as totalAmountInr, 
	SUM(".TABLE_PAYMENT_TRANSACTION.".pay_received_amt_rcpt_inr) as totalBalanceInr, 
	SUM(".TABLE_PAYMENT_TRANSACTION.".pay_received_amt) as totalAmount, 
	SUM(".TABLE_PAYMENT_TRANSACTION.".pay_received_amt_rcpt) as totalBalance ";
	Paymenttransaction::getDetails(  $db, $totalAmt, $fields, $condition_query);
	if(!empty($totalAmt)){
		$totalAmountInr		=number_format($totalAmt[0]['totalAmountInr'],2);
		$totalAmount		=number_format($totalAmt[0]['totalAmount'],2);
		$totalBalanceInr 	= number_format($totalAmt[0]['totalBalanceInr'],2);
		$totalBalance 		= number_format($totalAmt[0]['totalBalance'],2);
	}  */
	
	//Get total Amount BOF
	$sql = " SELECT SUM( c.pay_received_amt_inr ) AS totalAmountInr,
			SUM( c.pay_received_amt_rcpt_inr ) AS totalBalanceInr,
			SUM( c.pay_received_amt ) AS totalAmount,
			SUM( c.pay_received_amt_rcpt ) AS totalBalance
		FROM ( SELECT DISTINCT(".TABLE_PAYMENT_TRANSACTION.".id),".TABLE_PAYMENT_TRANSACTION.".pay_received_amt_inr,
		".TABLE_PAYMENT_TRANSACTION.".pay_received_amt_rcpt_inr,
		".TABLE_PAYMENT_TRANSACTION.".pay_received_amt,".TABLE_PAYMENT_TRANSACTION.".pay_received_amt_rcpt
		FROM ".TABLE_PAYMENT_TRANSACTION." LEFT JOIN ". TABLE_PAYMENT_BANK 
		." ON (". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id  
		OR ". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id
		OR ". TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id
		OR ". TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id = ". TABLE_PAYMENT_BANK .".id
		) AND ".TABLE_PAYMENT_BANK.".type='1'"
		." LEFT JOIN ". TABLE_PAYMENT_MODE ." ON 
		". TABLE_PAYMENT_TRANSACTION .".mode_id = ". TABLE_PAYMENT_MODE .".id "
		." LEFT JOIN ". TABLE_PAYMENT_ACCOUNT_HEAD
		." ON ". TABLE_PAYMENT_TRANSACTION .".account_head_id = ". TABLE_PAYMENT_ACCOUNT_HEAD .".id "
		." LEFT JOIN ". TABLE_SETTINGS_COMPANY
		." ON ". TABLE_PAYMENT_TRANSACTION .".company_id = ". TABLE_SETTINGS_COMPANY .".id "
		." LEFT JOIN ". TABLE_CLIENTS
		." ON ". TABLE_CLIENTS .".user_id = ". TABLE_PAYMENT_TRANSACTION .".client_id "
		 ." LEFT JOIN ". TABLE_VENDORS_BANK
		." ON ". TABLE_VENDORS_BANK .".user_id = ". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id " 
		 ." LEFT JOIN ". TABLE_USER 
		." ON ". TABLE_USER .".user_id = ". TABLE_PAYMENT_TRANSACTION .".executive_id ";
	$sql .= " ". $condition_query.") as c"; 
	if ( $db->query($sql) ) {
		if ( $db->nf() > 0 ) {
			while ($db->next_record()) {
				$totalAmountInr =$db->f("totalAmountInr") ;
				$totalAmount =$db->f("totalAmount") ;
				$totalBalanceInr =$db->f("totalBalanceInr") ;
				$totalBalance =$db->f("totalBalance") ;
			}
		}
	}
	//Get total Amount EOF
	
 
	if($searchStr==1){
		//count total Pending records bof
		$plist	= 	NULL;    
		$condition_query_pending = " WHERE ".TABLE_PAYMENT_TRANSACTION.".status='".Paymenttransaction::PENDING."'" ;
		$total_pending	=	Paymenttransaction::getDetails( $db, $plist, '', $condition_query_pending);
		//count total Pending records eof
		//echo $condition_query;
		// To count total records.
		$list	= 	NULL;    
		$total	=	Paymenttransaction::getDetails($db, $list, '', $condition_query);  
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
		   
		$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  = '&start=url'. $extra_url .'&end=url';

		$list	= NULL;
		$fields = TABLE_PAYMENT_TRANSACTION .".*,"
		.TABLE_PAYMENT_ACCOUNT_HEAD .".account_head_name, " . TABLE_PAYMENT_MODE .".payment_mode ,"
		.TABLE_USER .".f_name as efname,".TABLE_USER .".l_name as elname,"
		.TABLE_CLIENTS .".f_name as cfname,".TABLE_CLIENTS .".l_name as clname,".TABLE_CLIENTS .".billing_name as cbilling_name ,"
		//.TABLE_VENDORS .".f_name as fname, ". TABLE_VENDORS .".l_name as lname, ". TABLE_VENDORS .".billing_name,"
		.TABLE_VENDORS_BANK .".f_name as vfname, ". TABLE_VENDORS_BANK .".l_name as vlname, ".TABLE_VENDORS_BANK .".billing_name 
		as vbilling_name";	 
		
		 
		Paymenttransaction::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
		$fList=array();
		$statusArr = Paymenttransaction::getStatusAll();
		$statusArr = array_flip($statusArr);
		
		if(!empty($list)){
			foreach( $list as $key=>$val){
			
				$val['iaccount_head_name'] ='';
				if(!empty($val['iaccount_head_id'])){	
					$table = TABLE_PAYMENT_IACCOUNT_HEAD;
					$fields1 =  TABLE_PAYMENT_IACCOUNT_HEAD .'.account_head_name as iaccount_head_name' ;
					$condition1 = " WHERE ".TABLE_PAYMENT_IACCOUNT_HEAD .".id =".$val['iaccount_head_id']." " ;
					$iaccArr = getRecord($table,$fields1,$condition1);
					if(!empty($iaccArr)){
						$val['iaccount_head_name'] = $iaccArr['iaccount_head_name'];
					}
				} 
			   //$string = str_replace(",","','", $val['allotted_to']);
			   $fields1 =  TABLE_AUTH_USER .'.first_name'.','. TABLE_AUTH_USER .'.last_name';
			   if(!empty($string)){
				$condition1 = " WHERE ".TABLE_AUTH_USER .".user_id IN('".$string."') " ;
			   }          
			   $val['rowspan']=$bankname='';
			   $val['status_name'] = $statusArr[$val['status']];
			   if(!empty($val['ref_credit_pay_bank_id'])){
					$banknameArr=array();
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['ref_credit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$val['ref_credit_pay_bank_name'] =  $banknameArr['bank_name'] ;                    
					} 
			    }
			    if(!empty($val['ref_debit_pay_bank_id'])){
					$banknameArr=array();
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['ref_debit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$val['ref_debit_pay_bank_name'] =  $banknameArr['bank_name'] ;                    
					} 
			    }
			   //get bank name on the basis of type IN/OUT
			   if( $val['transaction_type'] ==Paymenttransaction::PAYMENTIN ){ 
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['credit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$bankname = $banknameArr['bank_name'] ;                    
					}
					$val['transaction_typename']='Payment In';
			   }elseif($val['transaction_type'] ==Paymenttransaction::PAYMENTOUT){ 
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['debit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$bankname = $banknameArr['bank_name'] ;                    
					}
					$val['transaction_typename']='Payment Out';
			   }elseif($val['transaction_type'] ==Paymenttransaction::INTERNAL){
			   
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['debit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$debitbankname = $banknameArr['bank_name'] ;
					}                
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['credit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$creditbankname = $banknameArr['bank_name'] ;   
					}
					$val['transaction_typename']='Internal';
					//$val['rowspan']=3;
				}
				$val['default_trdt']='';
				if ($val['do_transaction']=='0000-00-00 00:00:00' && $val['status']== Paymenttransaction::PENDING){
					$val['do_transaction']= date("Y-m-d h:i:s");
					$val['default_trdt']='Default Date';
				}
				
				if($val['transaction_type'] == Paymenttransaction::INTERNAL){
					$val['debitbankname'] = $debitbankname ;           
					$val['creditbankname'] = $creditbankname ;  
				}
				else{
					$val['bankname'] = $bankname ;           
				}
				$val['pay_bank_company_name']='';
				if(!empty($val['pay_bank_company'])){
					
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['pay_bank_company'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr1 = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr1)){
						$val['pay_bank_company_name'] = $banknameArr1['bank_name'] ;                    
					}
				}				
				$val['auto_entry']='';       
				if($val['is_auto_entry']=='1'){
					$val['auto_entry']='Auto Reversal';               
				}				
				//check transaction id followup in VST BOF FOR (VENDORS)
					$val['vticket_id']='';
					$sql2 = " SELECT ticket_id FROM ".TABLE_VENDORS_ST_TICKETS." WHERE 
					".TABLE_VENDORS_ST_TICKETS.".transaction_id = '".$val['id']."' LIMIT 0,1";
					if ( $db->query($sql2) ) {
						if ( $db->nf() > 0 ) {
							while ($db->next_record()) {
							   $val['vticket_id']= $db->f('ticket_id');
							}
						}                   
					}
				//check invoice id followup in ST EOF
				$val['attachment_imap_arr'] = array();
				if(!empty($val['attachment_imap'])){
					$val['attachment_imap_arr']=explode(",",$val['attachment_imap']);
				}
				/*
				$val['cf_name'] = "";
				$val['cl_name'] =""; 
				$val['org'] = "";
				*/
				/*
				if(!empty($val['client_id'])){				
					$table = TABLE_CLIENTS;
					$condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$val['client_id'] ."' " ;
					$fields1 =  TABLE_CLIENTS .'.f_name,l_name,org' ;
					$clientDetails = getRecord($table,$fields1,$condition2);
					if(!empty($clientDetails['f_name']) ){
						 $val['cfname'] = $clientDetails['f_name'] ;      
						 $val['clname'] = $clientDetails['l_name'] ;      
						 $val['org'] = $clientDetails['org'] ;      
					}
				}*/
				//check invoice id followup in ST EOF
				$val['attachment_imap_arr'] = array();
				if(!empty($val['attachment_imap'])){
					$val['attachment_imap_arr'] = explode(",",$val['attachment_imap']);
				}
				$pinvArr = array();
				if(!empty($val['proforma_invoice_no'])){
					$proforma_invoice_no1 = trim($val['proforma_invoice_no'],",");
					$proforma_invoice_no_str = str_replace(",","','",$proforma_invoice_no1);
					$sql_pf = " SELECT ".TABLE_BILL_INV_PROFORMA.".number as profm_inv, ".TABLE_BILL_INV.".number as inv_no
					FROM ".TABLE_BILL_INV_PROFORMA." LEFT JOIN ".TABLE_BILL_INV." ON 
					".TABLE_BILL_INV_PROFORMA.".number= ".TABLE_BILL_INV.".inv_profm_no
					WHERE ".TABLE_BILL_INV_PROFORMA.".number IN('".$proforma_invoice_no_str."')"; 
					if ( $db->query($sql_pf) ){
						if ( $db->nf() > 0 ){
							while ($db->next_record()){
							   $profm_inv = $db->f('profm_inv');
							   $inv_no = $db->f('inv_no');
							   $pinvArr[] = array('profm_inv'=>$profm_inv,
													'inv_no'=>$inv_no); 
							}
						}                   
					}
				}
				
				if(!empty($val['receipt_no'])){
					$rcptArr = explode(",",$val['receipt_no']);
					if(!empty($rcptArr)){
						$rcpt_no='';
						foreach($rcptArr as $key2=>$val2){
							$rcpt_no.="<a href='".DIR_WS_NC."/bill-receipt.php?sString=".$val2."&sType=number&chk_status=AND&sStatus=1&sOrderBy=do_r&sOrder=DESC&rpp=10&dt_field=do_r&action=search&perform=search' target=_blank>".$val2."</a> &nbsp;";
						}
					} 
					$all_vch="<br><a href='".DIR_WS_NC."/bill-receipt.php?sString=".$val['number']."&sType=voucher_no&chk_status=AND&sStatus=1&sOrderBy=do_r&sOrder=DESC&rpp=10&dt_field=do_r&action=search&perform=search' target=_blank>all rcpt</a>";
					$val['receipt_no'] = $rcpt_no.$all_vch ;
				}
				if(!empty($val['bill_nc_no'])){
					$billNcArr = explode(",",$val['bill_nc_no']);
					if(!empty($billNcArr)){
						$bill_nc_no='';
						foreach($billNcArr as $key2=>$val2){
							$bill_nc_no.="<a href='".DIR_WS_NC."/payment-party-bills.php?sString=".$val2."&sType=number&chk_status=AND&sStatus=1&sOrderBy=do_r&sOrder=DESC&rpp=10&dt_field=do_r&action=search&perform=search' target=_blank>".$val2."</a> &nbsp;";
						}
					} 
					$all_nc_vch="<br><a href='".DIR_WS_NC."/bill-receipt.php?sString=".$val['number']."&sType=voucher_no&chk_status=AND&sStatus=1&sOrderBy=do_r&sOrder=DESC&rpp=10&dt_field=do_r&action=search&perform=search' target=_blank>all rcpt</a>";
					$val['bill_nc_no'] = $bill_nc_no.$all_nc_vch ;
				}
			   $val['pay_received_amount']=round($val['pay_received_amt'],2);
			   $val['pay_received_amt'] = number_format($val['pay_received_amt'],2);
			   $val['tr_cr_bank_amt'] = number_format($val['tr_cr_bank_amt'],2);
			   $val['tr_db_bank_amt'] = number_format($val['tr_db_bank_amt'],2);
			   $val['pinvArr']= $pinvArr;
			   if( $key%2==0 ){
				$val['class']= 'odd';
			   }else{
				$val['class']= 'even';
			   }
			   $fList[$key]=$val;
			}
		}
	}
    // Set the Permissions.
    $variables['can_view_list'] = false;    
    $variables['can_add'] = false;
    $variables['can_edit'] = false;
    $variables['can_change_status'] = false;
    $variables['can_print'] = false;
    $variables['can_edit_completed'] = false;
    $variables['can_delete'] = false;
    $variables['can_view_details'] =false;
    $variables['can_allot_bills'] =false;
	$variables['can_allot_bank_stmt'] = false;
    $variables['can_view_board'] = false;    
    $variables['can_mark_link_status'] = false;    
    $variables['can_tstatus'] = false;
    if ( $perm->has('nc_p_pt_board') ) {
        $variables['can_view_board'] = true;
    } 
     
    if ( $perm->has('nc_p_pt_list') ) {
        $variables['can_view_list'] = true;
    } 
    if ( $perm->has('nc_p_pt_print') ) {
        $variables['can_print'] = true;
    } 
    if ( $perm->has('nc_p_pt_status') ) {
        $variables['can_change_status'] = true;
    }
	if ( $perm->has('nc_p_pt_link_status') ) {
        $variables['can_mark_link_status'] = true;
    }	
    if ( $perm->has('nc_p_pt_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_p_pt_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_p_pt_edit_completed') ) {
        $variables['can_edit_completed'] = true;
    }
    if ( $perm->has('nc_p_pt_delete') ) {
        $variables['can_delete'] = true;
    }
    if ( $perm->has('nc_p_pt_details') ) {
        $variables['can_view_details'] = true;
    }
    if ( $perm->has('nc_p_pt_bl') ) {
        $variables['can_allot_bills'] = true;
    }
	if ( $perm->has('nc_p_pt_bk_stmt') ) {
        $variables['can_allot_bank_stmt'] = true;
    } 
	if ( $perm->has('nc_p_pt_tstatus') ) {
        $variables['can_tstatus'] = true;
    }
	if ( $perm->has('nc_v_st') && $perm->has('nc_v_st_flw_tra') ) {
            $variables['can_followup'] = true;
    }
	
	$variables['tran_path'] = DIR_WS_TRANSACTIONS_FILES;	
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');    
    //$page["var"][] = array('variable' => 'bank_list', 'value' => 'bank_list');    
    //$page["var"][] = array('variable' => 'total_bank', 'value' => 'total_bank');    
   // $page["var"][] = array('variable' => 'complete_bank_balance', 'value' => 'complete_bank_balance');  
    $page["var"][] = array('variable' => 'total', 'value' => 'total');
    $page["var"][] = array('variable' => 'totalAmountInr', 'value' => 'totalAmountInr');
    $page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount');
    $page["var"][] = array('variable' => 'totalBalanceInr', 'value' => 'totalBalanceInr');
    $page["var"][] = array('variable' => 'totalBalance', 'value' => 'totalBalance');
    $page["var"][] = array('variable' => 'total_records', 'value' => 'total');   
    $page["var"][] = array('variable' => 'total_pending', 'value' => 'total_pending');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-list.html');
    
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
   
}
?>
