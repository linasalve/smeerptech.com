<?php
	if ( $perm->has('nc_al_edit') ) {
        $al_id	= isset($_GET["al_id"]) ? $_GET["al_id"] 	: ( isset($_POST["al_id"]) ? $_POST["al_id"] : '' );
    
        $data		= "";
        $_ALL_POST	= "";
        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 				=> &$db,
                            'messages' 			=> $messages,
                        );
            
            if (AccessLevel::validateUpdate($al_id, $data, $extra)) {
                
                $query	= " UPDATE ".TABLE_SETTINGS_ACC_LVL
                            ." SET ". TABLE_SETTINGS_ACC_LVL .".title = '". $data['title'] ."'"
                                .",". TABLE_SETTINGS_ACC_LVL .".access_level = '". $data['access_level'] ."'"
                                .",". TABLE_SETTINGS_ACC_LVL .".description = '". $data['description'] ."'"
                                .",". TABLE_SETTINGS_ACC_LVL .".status = '". $data['status'] ."'"
                            ." WHERE id = '". $al_id ."' ";
                if ( $db->query($query) ) {
                    if ( $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Email Template has been Updated Successfully.");
                    }
                    else {
                        $messages->setOkMessage("There were no changes in the Email Template.");
                    }
                }
                // Clear all the data.
                $_ALL_POST	= "";
                $data		= "";
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
                || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
                
            $variables['hid'] = $al_id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/settings-access-level-list.php');
            /*
            if ( isset($_POST['action']) && $_POST['action'] == 'search' ) {
                include ( DIR_FS_NC .'/settings-access-level-search.php');
            }
            else {
                include ( DIR_FS_NC .'/settings-access-level-list.php');
            }*/
        }
        else {
            $condition_query    = " WHERE id = '". $al_id ."' ";
            $data               = NULL;
            if ( AccessLevel::getList($db, $data, '*', $condition_query) > 0 ) {
                $data       =   $data['0'];
                
                // These parameters will be used when returning the control back to the List page.
                foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                    $hidden[]   = array('name'=> $key, 'value' => $value);
                }
                $hidden[]   = array('name'=> 'perform' , 'value' => 'edit');
                $hidden[]   = array('name'=> 'act' , 'value' => 'save');
                $hidden[]   = array('name'=> 'al_id' , 'value' => $al_id);
            }
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
    
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-access-level-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>