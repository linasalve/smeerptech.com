<?php
	if ( $perm->has('nc_etask_usr_delete') ) {
		$su_id	= isset($_GET["su_id"]) ? $_GET["su_id"] : ( isset($_POST["su_id"]) ? $_POST["su_id"] : '' );
		
		$extra = array( 'db' 		=> &$db,
						'messages'  => &$messages
					);
		EtaskUser::delete($su_id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/etask-users-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Change Status.");
    }
?>
