<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/pre-quote.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $condition_url ='';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
     //By default search in On
    $_SEARCH["searched"]    = true ;
     // Read the available Status 
    //$variables['types'] = PreQuote::getTypes();
    
    if ( $perm->has('nc_sl_pq') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_SALE_PRE_QUOTE   =>  array(  'ID'            => 'id',
                                                                'Pre Quote No.'     => 'number',
                                                                'Subject'       => 'subject'
                                                        ),
                                 // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                /*TABLE_USER      =>  array(  'Creator Number'    => 'number-user',
                                                            'Creator Username'  => 'username-user',
                                                            'Creator Email'     => 'email-user',
                                                            'Creator First Name'=> 'f_name-user',
                                                            'Creator Last Name' => 'l_name-user',
                                                ),
								TABLE_CLIENTS 	=>  array(  'Customer Number'    => 'number-clients',
                                                            'Customer Company'  => 'org-clients',
                                                            'Customer Username'  => 'username-clients',
                                                            'Customer Email'     => 'email-clients',
                                                            'Customer First Name'=> 'f_name-clients',
                                                            'Customer Last Name' => 'l_name-clients',
                                                ),*/
								
                            );
        
        $sOrderByArray  = array(
                                TABLE_SALE_PRE_QUOTE => array( 'ID'                => 'id',
                                                            'Pre Quote No.'  => 'number',
                                                            'Date of Creation'  => 'do_e',
                                                            'Status'            => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SALE_PRE_QUOTE;
        }

        // Read the available Status for the Pre Order.
        $variables['status'] = PreQuote::getStatus();
        
        
        //use switch case here to perform action. 
        switch ($perform) {
            case ('add_pre_quote'):
            case ('add'): {
            
                include (DIR_FS_NC.'/sale-pre-quote-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-pre-quote.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_pre_quote'):
            /*case ('edit'): {
                include (DIR_FS_NC .'/sale-pre-quote-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-pre-quote.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            case ('view_pre_quote'):
            case ('view'): {
                include (DIR_FS_NC .'/sale-pre-quote-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('change_status'): {
                include ( DIR_FS_NC .'/sale-pre-quote-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-pre-quote.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/sale-pre-quote-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-pre-quote.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            /*case ('delete_pre_quote'):
            case ('delete'): {
                include ( DIR_FS_NC .'/sale-pre-quote-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-pre-quote.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            case ('list_pre_quote'):
            case ('list'):
            default: {
                include (DIR_FS_NC .'/sale-pre-quote-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-pre-quote.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-pre-quote.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>