<?php
	if ( $perm->has('nc_sl_ld_asig_delete') ) {
		$lead_id	= isset($_GET["lead_id"]) ? $_GET["lead_id"] : ( isset($_POST["lead_id"]) ? $_POST["lead_id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'access_level' 	=> $my['access_level'],
						'messages' 		=> $messages
					);
		Leads::delete($lead_id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/sale-lead-assign-list.php');
	}
	else {
		$messages->setErrorMessage("You donot have the Right to Access this module.");
	}
?>