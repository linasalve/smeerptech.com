<?php
    if ( $perm->has('nc_ps_qt_add_intro') ) {
        //include_once ( DIR_FS_INCLUDES .'/currency.inc.php');      
        include_once ( DIR_FS_INCLUDES .'/prospects.inc.php' ); 
        include_once ( DIR_FS_INCLUDES .'/prospects-quotation.inc.php');
	   
	   //error_reporting(E_ALL);
	   
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $exchange_rate  = isset($_POST["exchange_rate"]) ? $_POST["exchange_rate"] : '1' ;
        
        $_ALL_POST      = NULL;
        $data           = NULL;
         
        $access_level   = $my['access_level']+1;        
        $_ALL_POST['exchange_rate'] = $exchange_rate; 
        $extra = array( 'db' 				=> &$db,
                        'access_level'      => $access_level,
                         'messages'          => &$messages
                        );
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn']) || isset($_POST['btnReturnRcp']) ) && 
			$_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
			$files       = processUserData($_FILES);
			 
			$data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_MULTIPLE_FILE_SIZE     ;
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,                        
                            'messages'          => &$messages
                        );
            $data['do_c'] = time(); 
            if ( !empty($data['content'])){
                
                $_ALL_POST["filename"] = 'test-proposal';
                $data["filename"] = 'test-proposal';
                 
				// Create the Invoice PDF, HTML in file. 
				$extra = array( 's'         => &$s,
								'messages'  => &$messages
							  );
				$attch = '';     
				$file_name = DIR_FS_PST_QUOT_PDF ."/". $data["filename"] .".pdf";
				if(file_exists($file_name)){
					@unlink($file_name);
				} 
				//comment it as not worked in LOCAL
				ProspectsQuotation::createPdfFileTest($_ALL_POST);  
				$inv_no =$data['number'];
				$messages->setOkMessage("Test Proposal Created.");
				//$data       = NULL;  
            } 
        } 
       
       /*  if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){ 
           header("Location:".DIR_WS_NC."/prospects-quotation.php?perform=add-test&added=1");           
        } */
          
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
       /*  if ( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
             header("Location:".DIR_WS_NC."/prospects-quotation.php?perform=add-test&added=1");           
        }
        else {  */
			$file_path = DIR_WS_PST_QUOT_PDF;
			$hidden[] = array('name'=> 'perform' ,'value' => 'add-test');
			$hidden[] = array('name'=> 'act' , 'value' => 'save');

			$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
			$page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
			$page["var"][] = array('variable' => 'file_path', 'value' => 'file_path');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-quotation-add-test.html');  
       // }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>