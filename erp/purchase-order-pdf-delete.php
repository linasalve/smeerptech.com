<?php

if ( $perm->has('nc_pop_delete') ) {
	   $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level']+1 ;
		
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        
        
      
	if ( (PurchaseOrderPdf::getList($db, $invoice, TABLE_PURCHASE_ORDER_PDF.'.id,'.TABLE_PURCHASE_ORDER_PDF.'.or_no, '
	.TABLE_PURCHASE_ORDER_PDF.'.number, '.TABLE_PURCHASE_ORDER_PDF.'.access_level, '.TABLE_PURCHASE_ORDER_PDF.'.status', " WHERE ".TABLE_PURCHASE_ORDER_PDF.".id = '$id'")) > 0 ) {
			 
			$invoice = $invoice[0];
		
			if ( $invoice['status'] != PurchaseOrderPdf::ACTIVE ) {
				if ( $invoice['status'] == PurchaseOrderPdf::BLOCKED ) {
					if ( $invoice['access_level'] < $access_level ) {
							
						if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
							$data		= processUserData($_POST);
							if(empty($data['reason_of_delete'])){
								$messages->setErrorMessage("Please Specify Reason To Delete The PO Pdf 
								".$data['number']);
							}
							if($messages->getErrorMessageCount() <= 0){                    
								//Sql to delete invoice 
								$query = "UPDATE ". TABLE_PURCHASE_ORDER_PDF
										." SET status = '".PurchaseOrderPdf::DELETED."', 
										reason_of_delete = '".$data['reason_of_delete']."',
										delete_by = '".$my['uid']."',
										delete_ip = '".$_SERVER['REMOTE_ADDR']."',
										do_delete = '".date("Y-m-d")."'
										WHERE id = '$id'";
														
								if ( $db->query($query) && $db->affected_rows()>0 ) {    
									//Update in order, is_pdf_create =0                       
									$query = "UPDATE ".TABLE_PURCHASE_ORDER." SET 
									".TABLE_PURCHASE_ORDER.".is_pdf_create = '0', 
									".TABLE_PURCHASE_ORDER.".popdf_no='', 
									".TABLE_PURCHASE_ORDER.".status='".PurchaseOrder::ACTIVE."'
									WHERE ".TABLE_PURCHASE_ORDER.".number = '".$invoice['or_no']."'";
									$db->query($query);
									
								  
									$messages->setOkMessage("The Purchse Order Pdf was deleted.");
									
								
								} else {
									$messages->setErrorMessage("The Purchse Order Pdf was not deleted.");
									 
								}
							}
						 
						}
						//Add Followup of order delete EOF
						$hidden[] = array('name'=> 'id' ,'value' => $id);
						$hidden[] = array('name'=> 'number' ,'value' => $invoice['number']);
						$hidden[] = array('name'=> 'perform' ,'value' => $perform);
						$hidden[] = array('name'=> 'ajx','value' => $ajx);
						$hidden[] = array('name'=> 'act' , 'value' => 'save');
						$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
						$page["section"][] = array('container'=>'CONTENT_MAIN', 
						'page' => 'purchase-order-pdf-delete.html');
						
						
					}else {
						$messages->setErrorMessage("Cannot Delete Purchse Order Pdf ".$invoice['number']." with the current Access Level.");
						 $page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
						$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order-pdf.html');
					}
				}
				else {
					$messages->setErrorMessage("To Delete Purchse Order Pdf ".$invoice['number']." 
					it should be Blocked.");
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
					$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order-pdf.html');
				}
			}
			else {
				$messages->setErrorMessage("To Delete Purchse Order Pdf ".$invoice['number']." it should be Blocked.");
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order-pdf.html');
			}
		}else{
		
			$messages->setErrorMessage("The Purchse Order Pdf ".$invoice['number']." was not found.");
			$page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order-pdf.html');
		}
	
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the Purchse Order Pdf.");
		$page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order-pdf.html');
    }

?>
