<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/bill-pre-order.inc.php' );
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
    $condition_url = $condition_query = '';

    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if($added){
         $messages->setOkMessage("Record has been added successfully.");
    }
    
    if ( $perm->has('nc_bl_po') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_BILL_PO   =>  array(  
                                                            'Quotation No.' => 'q_no',
                                                            'Order Details' => 'order_details',
                                                            'Client Details' => 'client_details',
                                                            'Payment Details'=> 'payment_details',
                                                            'Remark'=> 'remark'
                                                        ),
                                TABLE_USER      =>  array(  'Creator Number'    => 'number#user',
                                                            'Creator Username'  => 'username#user',
                                                            'Creator Email'     => 'email#user',
                                                            'Creator First Name'=> 'f_name#user',
                                                            'Creator Last Name' => 'l_name#user',
                                                ),
                            );
        
        $sOrderByArray  = array(
                                TABLE_BILL_PO => array( 'ID'    => 'id',
                                                        'Date'  => 'do_po',
                                                        'Status'=> 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_po';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_BILL_PO;
        }

        // Read the available Status for the Pre Order.
        $variables['status'] = PreOrder::getStatus();
        
        //use switch case here to perform action. 
        switch ($perform) {
            case ('add_pre_order'):
            case ('add'): {
            
                include (DIR_FS_NC.'/bill-pre-order-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-pre-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_pre_order'):
            case ('edit'): {
                include (DIR_FS_NC .'/bill-pre-order-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-pre-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_pre_order'):
            case ('view'): {
                include (DIR_FS_NC .'/bill-pre-order-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('change_status'): {
            	$perform='list';
                include ( DIR_FS_NC .'/bill-pre-order-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-pre-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/bill-pre-order-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-pre-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('delete_pre_order'):
            case ('delete'): {
            	$perform='list';
                include ( DIR_FS_NC .'/bill-pre-order-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-pre-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_attachment'): {             
                include (DIR_FS_NC .'/bill-pre-order-view-attachment.php');
                break;
            }     
            case ('list_pre_order'):
            case ('list'):
            default: {
                include (DIR_FS_NC .'/bill-pre-order-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-pre-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-pre-order.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
