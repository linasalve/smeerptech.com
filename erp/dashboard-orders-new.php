<?php
	if ( $perm->has('nc_dashboard_orders') ) {
    
		include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
		include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
        include_once ( DIR_FS_INCLUDES .'/user.inc.php');
		include_once ( DIR_FS_INCLUDES .'/work-stage.inc.php');
        
		if ( empty($x) ) {
			$x              = 1;
			$next_record    = 0 ;
		} else {
			$next_record    = ($x-1) * $rpp;
		}
        
		
		if ( empty($xwm) ) {
			$xwm            = 1;
			$next_record_wm	= 0 ;
		}
		else {
			$next_record_wm	= ($xwm-1) * $rpp;
		}
        if ( empty($xr) ) {
			$xr           = 1;
			$next_record_r	= 0 ;
		}
		else {
			$next_record_r	= ($xr-1) * $rpp;
		}
        if ( empty($xld) ) {
			$xld= 1;
			$next_record_ld	= 0 ;
		}
		else {
			$next_record_ld	= ($xld-1) * $rpp;
		}
      
		$variables["x"]     = $x;
		
		$variables["xwm"]   = $xwm;
		$variables["xld"]   = $xld;
	
		
		
		include_once('dashboard-ord.php');
    
	
	
     
    
    //code for Reminder BOf
   
    // To count total records.
        $list_r	= 	NULL;
        $condition_query_r = '';
        $time  =time();
        $rDate  = date('Y-m-d 00:00:00', $time );
        $db 		= new db_local;
        $condition_query_r = " WHERE ".TABLE_TASK_REMINDER.".status ='".Taskreminder::PENDING."' AND
                                ( "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' "
                            . " ) ORDER BY do_r DESC";
        $total_r	=	Taskreminder::getDetails( $db, $list_r, '', $condition_query_r);
        $paginationrem = showMovePagination($total_r, $xr, $rpp,$extra,'xr', 'movePage');
        //echo $condition_query;
    
        $list_r	= NULL;
        $fields_r = TABLE_TASK_REMINDER .'.id, '.TABLE_TASK_REMINDER .'.comment, 
        '.TABLE_TASK_REMINDER .'.allotted_to, '.TABLE_TASK_REMINDER .'.status ,'.TABLE_TASK_REMINDER .'.priority, 
        '. TABLE_TASK_REMINDER .'.do_r'   ;
        Taskreminder::getDetails( $db, $list_r, $fields_r, $condition_query_r, $next_record_r, $rpp);
        $flist_r = array();
        if(!empty($list_r)){
			$priorityArr = Taskreminder::getPriority();
            $priorityArr = array_flip($priorityArr);
            foreach( $list_r as $key=>$val){      
				if(!empty($val['priority'])){
               
					$val['priority'] = $priorityArr[$val['priority']];
				}
               $flist_r[$key]=$val;
            }
			$priorityArr =array();
        }
        $db->close();
        $page["var"][] = array('variable' => 'list_r', 'value' => 'flist_r');
       /****** Task BOARD BOF ********/
     
       //TABLE_TASK_REMINDER .".do_r <= '". $rDate ."' " ." AND
        $db 		= new db_local;
        $sqlTask1 = " SELECT COUNT(*) as totTask FROM ".TABLE_TASK_REMINDER ."   
                         WHERE   ( "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' "
                            . " ) AND ".TABLE_TASK_REMINDER.".status IN( '".Taskreminder::PENDING."','".Taskreminder::COMPLETED."' ) ";
         
        $db->query($sqlTask1);   
        $totTask         =0;
        if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
                $totTask=$db->f('totTask'); 
                $taskSummary['totTask']=$totTask;
            }
        }
        
        $db->close();
        $db 		= new db_local;
        $sqlTask3 = " SELECT COUNT(*) as cot FROM ".TABLE_TASK_REMINDER ."   
                         WHERE ". TABLE_TASK_REMINDER .".do_r <= '". $rDate ."' "
                            ." AND ( "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' "
                            . " ) AND ".TABLE_TASK_REMINDER.".status IN( '".Taskreminder::COMPLETED."' ) "
                            . "  AND ".TABLE_TASK_REMINDER.".do_completion <= ".TABLE_TASK_REMINDER.".do_r";
         
         $db->query($sqlTask3);  
         $cot =0;
         if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
                $cot=$db->f('cot'); //cot => Completed on time
                $tdd=$db->f('tdd'); //cot => Completed on time
                 $taskSummary['cot']=$cot;
                
            }
        }
        $db->close();
		 $db 		= new db_local;
         $sqlTask4 = " SELECT COUNT(*) as cd , SUM(DATEDIFF(do_completion,do_r) ) as tdd FROM 
		 ".TABLE_TASK_REMINDER ."   
                         WHERE ( "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' "
                            . " ) AND ".TABLE_TASK_REMINDER.".status IN( '".Taskreminder::COMPLETED."' ) "
                            . "  AND ".TABLE_TASK_REMINDER.".do_completion > ".TABLE_TASK_REMINDER.".do_r";
         
         $db->query($sqlTask4);  
         $cd =0;
         $tdd =0;
         if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
                $cd=$db->f('cd'); //cd => Completed with delay
                $tdd=$db->f('tdd'); //cd => Completed with delay
                $taskSummary['cd']=$cd;
                 $taskSummary['tdd']=$tdd;
            }
        }
		 $db->close();
         $page["var"][] = array('variable' => 'taskSummary', 'value' => 'taskSummary');
        /****** Task BOARD EOF ********/
        
    //code for Reminder EOf
    
    /****** SCORE BOARD BOF ********/
    include_once ( DIR_FS_INCLUDES .'/score-sheet.inc.php' );
    
	$dt= strtotime('-1 month', strtotime(date("Ymd")));
	$month1 = date("m-Y",$dt);
	
	$dt= strtotime('-2 month', strtotime(date("Ymd")));
	$month2 = date("m-Y",$dt);
	
	$dt= strtotime('-3 month', strtotime(date("Ymd")));
	$month3 = date("m-Y",$dt);
	 
    $scoreMonthArr = array(
                        $month1 ,
                        $month2 ,
                        $month3 
                    );
    
    
    $listb	= NULL;
    $fields = TABLE_SCORE_SHEET .'.*' ; 
           
    foreach($scoreMonthArr as $key=>$val){
        
		$db 		= new db_local; // database handle
        $listn = null;
        $condition_query1 = " WHERE DATE_FORMAT(".TABLE_SCORE_SHEET.".date,'%m-%Y') ='".$val."'";
        $condition_query1 .= " AND ".TABLE_SCORE_SHEET.".score IN(-1,-2,-3) ";
        $condition_query1 .= " AND  ".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$my['uid'].",%' ";
        $fields1 = " SUM(".TABLE_SCORE_SHEET.".score) as negetiveScore ";
        Scoresheet::getDetails( $db, $listn, $fields1, $condition_query1);
   
        $listp=null;
        $condition_query1 = " WHERE DATE_FORMAT(".TABLE_SCORE_SHEET.".date,'%m-%Y') ='".$val."'";
        $condition_query1 .= " AND ".TABLE_SCORE_SHEET.".score IN(1,2,3) ";
        $condition_query1 .= " AND  ".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$my['uid'].",%' ";
        $fields1 = " SUM(".TABLE_SCORE_SHEET.".score) as positiveScore ";
        Scoresheet::getDetails( $db, $listp, $fields1, $condition_query1);
        $negetiveScore = empty($listn[0]['negetiveScore']) ? 0 : $listn[0]['negetiveScore'] ;
        $positiveScore = empty($listn[0]['positiveScore']) ? 0 : $listn[0]['positiveScore'] ;
       
        $scoreList[] = array(
                            'month'=>'01-'.$val,                           
                            'negetiveScore'=>$negetiveScore,
                            'positiveScore'=>$positiveScore
                            );
		$db->close();
				
    }
    
	$db 		= new db_local; // database handle
    $condition_query1 = '';
    $tlistp = null;
    $fields1 = "  SUM(".TABLE_SCORE_SHEET.".score) as totalPositive " ;
    $condition_query1 = " WHERE ".TABLE_SCORE_SHEET.".score IN(1,2,3) ";
    $condition_query1 .= " AND  ".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$my['uid'].",%' ";
    Scoresheet::getDetails( $db, $tlistp, $fields1, $condition_query1);
    $totalPositive = $tlistp[0]['totalPositive'] ;
    $db->close();
	
	$db 		= new db_local; // database handle
    $condition_query1 = '';
    $tlistp = null;
    $fields1 = "  SUM(".TABLE_SCORE_SHEET.".score) as totalNegative " ;
    $condition_query1 = " WHERE ".TABLE_SCORE_SHEET.".score IN(-1,-2,-3) ";
    $condition_query1 .= " AND  ".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$my['uid'].",%' ";
    Scoresheet::getDetails( $db, $tlistp, $fields1, $condition_query1);
    $totalNegative = $tlistp[0]['totalNegative'] ;
    
    $scoreDetails = array(
        'totalPositive'=>$totalPositive,
        'totalNegative'=>$totalNegative,
        'scoreList'=>$scoreList
    );
    $db->close(); 
    $page["var"][] = array('variable' => 'scoreDetails', 'value' => 'scoreDetails');
    
    
    /*******SCORE BOARD EOF*******/
    
	
	
	//Marketing Targets BOF
	$db1 = new db_local; // database handle
	$db 		= new db_local; // database handle
	$sql_tm=" SELECT * FROM ".TABLE_ORDM_TARGET." WHERE ".TABLE_ORDM_TARGET.".executive_id='".$my['uid']."' 
	ORDER BY id LIMIT 0, 3";
	$db->query($sql_tm);            
	if ( $db->nf()>0 ) {
		while ( $db->next_record() ) { 
			$frm_dt = $db->f('frm_dt'); 
			$to_dt = $db->f('to_dt'); 
			 
			$duration = date('d M Y',strtotime($frm_dt))." - ".date('d M Y',strtotime($to_dt))  ;
			$target_amount = $db->f('target_amount'); 
			$sql2 = "SELECT SUM(".TABLE_ORDIH_TARGET.".target_amt) as total FROM ".TABLE_ORDIH_TARGET." 
			WHERE team_id = '".$my['uid']."' AND ".TABLE_ORDIH_TARGET.".do_it >='".$frm_dt."' 
			AND ".TABLE_ORDIH_TARGET.".do_it <= '".$to_dt."'" ;
			$db1->query($sql2);            
			if ( $db1->nf()>0 ) {
				while ( $db1->next_record() ) { 
					$total = $db1->f('total');
				}
			}
			$data['targets'][] = array(
				'duration'=>$duration ,
				'target_amount'=>$target_amount ,
				'total'=>$total 
			);
		} 	
	}
	$db->close();
			
	//Marketing Targets EOF
	 
	//
	
    //Code for My Leads BOF
    include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
		$db 		= new db_local; // database handle
        $leadlist	= 	NULL;
        $condition_query_lead ='';
        $condition_query_lead .= " WHERE  ( ". TABLE_SALE_LEADS .".lead_assign_to ='".$my['user_id']."' || ". TABLE_SALE_LEADS .".lead_assign_to_mr ='".$my['user_id']."') "; 
        $condition_query_lead .= " ORDER BY ".TABLE_SALE_LEADS.".do_add ASC ";
        $totalld	=	Leads::getList( $db, $leadlist, '', $condition_query_lead);      
        $paginationld = showMovePagination($totalld, $xld, $rpp,$extra,'xld', 'movePage');
    
        $leadlist	= NULL;
        $fields ='';
        $fields = TABLE_SALE_LEADS.".lead_id, ".TABLE_SALE_LEADS.".f_name,".TABLE_SALE_LEADS.".m_name,".TABLE_SALE_LEADS.".l_name," ;
        $fields .= TABLE_SALE_LEADS.".email, ". TABLE_SALE_LEADS.".lead_status " ;
        //$fields .= TABLE_SALE_LEADS.".lead_assign_to, ".TABLE_SALE_LEADS.".status " ;
        Leads::getList( $db, $leadlist, $fields , $condition_query_lead, $next_record_ld, $rpp);
        $leadStatusArr =Leads::getLeadStatus();
        $leadStatusArr  = array_flip($leadStatusArr );
          
        $fleadlist=array();
        if(!empty($fleadlist)){
            foreach( $fleadlist as $key=>$val){               
              
                $leadstatus = $val['lead_status'];
                $val['lead_status']    = $leadStatusArr[$leadstatus] ;     
               
                $fleadlist[$key]=$val;
            }
        }
		$db->close(); 
        $page["var"][] = array('variable' => 'fleadlist', 'value' => 'fleadlist');
		
        //Code for My Leads EOF
		

		
		//$page["var"][] = array('variable' => 'paginationwm', 'value' => 'paginationwm');
		$page["var"][] = array('variable' => 'paginationld', 'value' => 'paginationld');
		$page["var"][] = array('variable' => 'paginationrem', 'value' => 'paginationrem');
        
		
		$page["var"][] = array('variable' => 'variables', 'value' => 'variables');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
		
		
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'dashboard-orders-new.html');
	}
?>