<?php
    if ( $perm->has('nc_urol_add') ) {
        $data		= NULL;
        $extra      = NULL;
        $_ALL_POST	= NULL;
        $chk_output	= NULL;
        $chk_rights	= NULL;
        $rights_array = NULL;
        $al_list    = getAccessLevel($db, $my['access_level']);
        $x1 			= isset($_GET["x1"])         ? $_GET["x1"]        : ( isset($_POST["x1"])          ? $_POST["x1"]       :'');
		$rpp1 		= isset($_GET["rpp1"])       ? $_GET["rpp1"]      : ( isset($_POST["rpp1"])        ? $_POST["rpp1"]     : 5  );// Result per page
        if ( empty($x1) ) {
			$x1              = 1;
			$next_record1    = 0 ;
		}else {
			$next_record1    = ($x1-1) * $rpp1;
		}
		
		//read access leveles
        //$userrights_list		= NULL;
        //UserRoles::getUserRights( $db, $userrights_list, 'id, parent_id, title', 'ORDER BY value ASC');
		$list	= 	NULL;
		$condition = " WHERE parent_id = 0 ";
        echo  $total	=	UserRoles::getUserRights( $db, $list, '', $condition);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url; 
        }
        $extra_url  .= "&x1=$x1&rpp1=$rpp1";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $condition_url .="&rpp1=".$rpp1."&perform=".$perform;    
        $pagination = showPaginationAjaxWithExtra($total, $x1, $rpp1, $condition_url, 'changePageWithExtra');

        //$list	= NULL;
        //UserRights::getList( $db, $list, 'id, parent_id, title', $condition_query, $next_record, $rpp);
		$userrightsParent_list	= NULL;
        UserRoles::getUserRights( $db, $userrightsParent_list, 'id, parent_id, title', $condition,$next_record1, $rpp1);
        $rightList=array();
        if(!empty($userrightsParent_list)){
            foreach( $userrightsParent_list as $key=>$val){
                $userRtlist=null;
                $id = $val['id'];
                $parent_id = $val['parent_id'];
                $title= $val['title'];
                $condition = " WHERE parent_id = '".$id."'";
                UserRoles::getUserRights( $db, $userRtlist, 'id, parent_id, title', $condition);
               
                $rightList[]= array('id'=>$id,
                                    'parent_id'=>$parent_id,
                                    'title'=>$title,
                                    'child'=>$userRtlist
                                    ); 
                 
            }
        }

     
        
        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
    
            if (isset($data['chk_rights']) &&  $data['chk_rights']!='') {
                //insert comma(,) when select multipule rights.
                $chk_output[]	= $data['chk_rights'];
                foreach( $chk_output as $key=>$val )
                {
                    $rights_array	=  $val;
                                            
                }
                $chk_rights 	=	implode(",", $rights_array);
                //$data['chk_rights']    = explode(",", $data['rights']);
            }
            
            $extra = array( 'db' 				=> &$db,
                            'data' 				=> $data,
                            'userrights_list' 	=> $userrights_list,
                            'rights_array' 		=> $rights_array,
                            'messages' 			=> $messages,
                        );
            
            if (UserRoles::validateAdd($data, $messages, $extra)) {
                
				$rights_values =$value = '';
				$sql = " SELECT value FROM ".TABLE_USER_RIGHTS." WHERE id IN(".$chk_rights.") " ; 
				$db->query($sql);
				if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$value.=$db->f('value').",";
					}
					$rights_values = trim($value,",");
				}
				
                $query	= " INSERT INTO ".TABLE_USER_ROLES
                            ." SET ". TABLE_USER_ROLES .".title 			='". $data['title'] ."'"
                                    .",". TABLE_USER_ROLES .".rights			='". $chk_rights ."'"
									.",". TABLE_USER_ROLES .".rights_values			='". $rights_values ."'"
                                    .",". TABLE_USER_ROLES .".description	='". $data['description'] ."'"
                                    .",". TABLE_USER_ROLES .".access_level   ='". $data['access_level'] ."'"
                                    .",". TABLE_USER_ROLES .".status			='". $data['status'] ."'";
                
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $messages->setOkMessage("User Role Added Successfully.");	
                    $variables['hid'] = $db->last_inserted_id();
                    
                    // to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
            }
        }
        
       /* // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
                || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            // to flush the data.
            $_ALL_POST  = NULL;
            $data       = NULL;
            include ( DIR_FS_NC .'/user-roles-list.php');
        }*/
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/user-roles.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/user-roles.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/user-roles.php?added=1");   
        }
        else {
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
    
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'rightList', 'value' => 'rightList');
            $page["var"][] = array('variable' => 'extra', 'value' => 'extra');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            
    
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-roles-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>