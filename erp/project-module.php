<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/project-module.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
    
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	$parent_id 	= isset($_GET["parent_id"])         ? $_GET["parent_id"]        : ( isset($_POST["parent_id"])          ? $_POST["parent_id"]       :'');
    $added 		= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $or_id 	    = isset($_GET["or_id"])     ? $_GET["or_id"]    : ( isset($_POST["or_id"])      ? $_POST["or_id"]   :'');
    $condition_query='';

    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if($added){
         $messages->setOkMessage("New Module entry has been done.");
    }
    
    //get order details BOF
    $order = NULL;
    if ( Order::getList($db, $order, 'team, order_title,id as order_id,client', " WHERE id = '$or_id'" ) > 0 ) {
        include_once ( DIR_FS_INCLUDES .'/user.inc.php');
        
       
        $order = $order[0];
        $order['team'] = "'". implode("','", (explode(',', $order['team']))) ."'";
        $order['team_members']= '';
        if ( User::getList($db, $team_members, 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $order['team'] .")") > 0 ) {
           
        }   
    }
    
    if ( $perm->has('nc_pts_mod') ) {

        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_PROJECT_TASK_MODULE   =>  array(  'Title'        	=> 'title',
                                                                        'Details'       => 'details',
																	  )
                            );
        
        $sOrderByArray  = array(
                                TABLE_PROJECT_TASK_MODULE => array(	  'Title'        	=> 'title',
																	  'Date'        	=> 'do_e',
																	  )
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
			$_SEARCH['searched']=1;
            $order_by_table     = TABLE_PROJECT_TASK_MODULE;
        }
    
        
        //use switch case here to perform action. 
        switch ($perform) {
            case ('add_modules'):
            case ('add'): {
            
                include (DIR_FS_NC.'/project-module-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-module.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_modules'):
            case ('edit'): {
                include (DIR_FS_NC .'/project-module-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-module.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/project-module-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-module.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('active'):{
            	include ( DIR_FS_NC .'/project-module-active.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-module.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
			}
            case ('deactive'): {
                include ( DIR_FS_NC .'/project-module-deactive.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-module.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('delete_modules'):
            case ('delete'): {
                include ( DIR_FS_NC .'/project-module-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-module.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list'):
            default: {
                include (DIR_FS_NC .'/project-module-list.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-module.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-module.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("or_id", $or_id);
    $s->assign("order", $order);
    $s->assign("team_members", $team_members);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>