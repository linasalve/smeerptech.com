<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/sms-api.inc.php' ); 
    include_once ( DIR_FS_INCLUDES .'/sms-sender.inc.php' ); 
    
    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');

    $condition_url = $condition_query = '';
    
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
     //By default search in On
    $_SEARCH["searched"]    = true ;
    
     // Read the available Status 
    
    $variables['scrub']= SmsApi::SCRUBBED;
    $variables['nonscrub']= SmsApi::NONSCRUBBED;
    
    if($added){
        $messages->setOkMessage("Record has been added successfully.");
    }
    
    if ( $perm->has('nc_sms_api') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_SMS_API   =>  array(  'Name'     => 'name',
                                                            'Username'   => 'username',
                                                            'Vendor/Company'   => 'gateway_name',
                                                            'Added by'   => 'created_by_name',
                                                            'Post Url'      => 'post_url'
                                                        ),
								TABLE_SMS_SENDER  =>  array(  'Sender Name'     => 'sender_name' 
                                                        ) 
                               
								
								
                            );
        
        $sOrderByArray  = array(
                                TABLE_SMS_SENDER => array( 'Sender Name'     => 'sender_name',
                                                        'Date of Entry'  => 'do_e'
                                                    )
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SMS_SENDER;
        }

        // Read the available Status for the Pre Order.
      
        //use switch case here to perform action. 
        switch ($perform) {
        
            case ('add'): {
            
                include (DIR_FS_NC.'/sms-sender-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-sender.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }           
        
            case ('edit'): {
                include (DIR_FS_NC .'/sms-sender-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-sender.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
          
            case ('view'): {
                include (DIR_FS_NC .'/sms-sender-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*case ('change_status'): {
                include ( DIR_FS_NC .'/sms-sender-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-sender.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            case ('search'): {
                include(DIR_FS_NC."/sms-sender-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-sender.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
          
            case ('delete'): {
                $perform='list';
                include ( DIR_FS_NC .'/sms-sender-delete.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-sender.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
          
            case ('list'):
            default: {
                include (DIR_FS_NC .'/sms-sender-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-sender.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-sender.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>