<?php
if ( $perm->has('nc_hr_sal_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
         
        
        include_once (DIR_FS_INCLUDES .'/calendar.inc.php');
        include_once (DIR_FS_INCLUDES .'/user.inc.php');
        
		$lst_calendar = NULL;
        $required_fields ='id,title';
        $condition =" where status='".Calendar::ACTIVE."'";
		Calendar::getList($db,$lst_calendar,$required_fields,$condition);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( HrSalary::validateUpdate($data, $extra) ) {
			
				$absent_days = $data['working_days'] - $data['present_days'] ;
				$query  = " UPDATE ". TABLE_HR_SALARY
					 ." SET ".TABLE_HR_SALARY .".user_id = '". $data['user_id'] ."'"  
					//.",". TABLE_HR_SALARY .".calendar_id = '". 		$data['calendar_id'] ."'"   
					.",". TABLE_HR_SALARY .".from_date = '". date('Y-m-d',$data['from_date']) ."'"  
					.",". TABLE_HR_SALARY .".to_date = '". 		date('Y-m-d',$data['to_date']) ."'"   
					.",". TABLE_HR_SALARY .".total_days = '". 		$data['total_days'] ."'"   
					.",". TABLE_HR_SALARY .".working_days = '". $data['working_days'] ."'"   
					.",". TABLE_HR_SALARY .".present_days = '". $data['present_days'] ."'"
					.",". TABLE_HR_SALARY .".absent_days = '". 	$absent_days ."'"  					
					.",". TABLE_HR_SALARY .".present_days_desc = '".$data['present_days_desc'] ."'"   
					.",". TABLE_HR_SALARY .".promised_payment = '". $data['promised_payment']."'"   
					.",". TABLE_HR_SALARY .".30_per_appr_pay = '".$data['30_per_appr_pay'] ."'"   
					.",". TABLE_HR_SALARY .".no_projects = '". 		$data['no_projects'] ."'"   
					.",". TABLE_HR_SALARY .".no_projects_desc = '". 		$data['no_projects_desc'] ."'"   
					.",". TABLE_HR_SALARY .".worked_hrs = '". 		$data['worked_hrs'] ."'"   
					.",". TABLE_HR_SALARY .".calculation_type = '". 		$data['calculation_type'] ."'"   
					.",". TABLE_HR_SALARY .".per_day = '". 		$data['per_day'] ."'"   
					.",". TABLE_HR_SALARY .".per_hour = '". 		$data['per_hour'] ."'"   
					.",". TABLE_HR_SALARY .".incentive_amount = '". 		$data['incentive_amount'] ."'" 
					.",". TABLE_HR_SALARY .".earned_payment = '". 		$data['earned_payment'] ."'"   
					.",". TABLE_HR_SALARY .".30_perof_earned_pay = '". 	$data['30_perof_earned_pay'] ."'"   
					.",". TABLE_HR_SALARY .".30_appraisal_score = '". 	$data['30_appraisal_score'] ."'"   
					.",". TABLE_HR_SALARY .".30_appraisal_score_desc = '". 	$data['30_appraisal_score_desc'] ."'"   
					.",". TABLE_HR_SALARY .".earned_30_per_appr_pay = '". 	$data['earned_30_per_appr_pay'] ."'"   
				    .",". TABLE_HR_SALARY.".earned_30_per_appr_pay_desc='".$data['earned_30_per_appr_pay_desc']."'"   
					.",". TABLE_HR_SALARY .".cafeteria_deduction = '". 		$data['cafeteria_deduction'] ."'"   
					.",". TABLE_HR_SALARY .".phone_deduction = '". 		$data['phone_deduction'] ."'"   
					.",". TABLE_HR_SALARY .".pt_deduction = '". 		$data['pt_deduction'] ."'"      
					.",". TABLE_HR_SALARY .".pf_deduction = '". 		$data['pf_deduction'] ."'"                    .",". TABLE_HR_SALARY .".esi_deduction = '". 		$data['esi_deduction'] ."'"                    
					.",". TABLE_HR_SALARY .".uniform_deposite = '". $data['uniform_deposite'] ."'"                    .",". TABLE_HR_SALARY .".adv = '". 		$data['adv'] ."'"                              
					.",". TABLE_HR_SALARY .".deduction_amount = '".$data['deduction_amount']."'" 
					.",". TABLE_HR_SALARY .".deduction_amount_desc = '".$data['deduction_amount_desc'] ."'" 
					.",". TABLE_HR_SALARY .".total_earned_payment = '".$data['total_earned_payment'] ."'"  
					.",". TABLE_HR_SALARY .".paid_salary = '". 		$data['paid_salary'] ."'"   
					.",". TABLE_HR_SALARY .".note = '". 		$data['note'] ."'"   
					.",". TABLE_HR_SALARY .".updated_by = '". 		$my['user_id'] ."'"    
					.",". TABLE_HR_SALARY .".updated_by_name = '". 		$my['f_name']." ".$my['l_name']."'"    
					.",". TABLE_HR_SALARY .".ip_u     = '". 		$_SERVER['REMOTE_ADDR'] ."'"  
					.",". TABLE_HR_SALARY .".do_update  = '". 		date('Y-m-d H:i:s')."'"
					." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Hr Salary has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_HR_SALARY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_HR_SALARY .".id = '". $id ."' )";
            
            if ( HrSalary::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                $id         = $_ALL_POST['id'];
                if ( !empty($_ALL_POST['user_id']) && !is_array($_ALL_POST['user_id']) ) {
                    $members =array();
                    $_ALL_POST['user_id']  = $_ALL_POST['user_id'];
                    User::getList($db, $members, 'user_id,number,f_name,l_name,email', "WHERE user_id='".$_ALL_POST['user_id']."'");
                    $_ALL_POST['executive_details'] = $members[0]['f_name'] .' '. $members[0]['l_name'] .' ('. $members[0]['number'] .')';
                }
				
				if(isset($_ALL_POST['from_date']) && $_ALL_POST['from_date']!='0000-00-00 00:00:00'){
					$temp  =null;
					$_ALL_POST['from_date']  = explode(' ', $_ALL_POST['from_date']);
					$temp               = explode('-', $_ALL_POST['from_date'][0]);
					$_ALL_POST['from_date']  = NULL;
					$_ALL_POST['from_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['from_date']  = '';                            
				}
				
				if(isset($_ALL_POST['to_date']) && $_ALL_POST['to_date']!='0000-00-00 00:00:00'){
					$temp  =null;
					$_ALL_POST['to_date']  = explode(' ', $_ALL_POST['to_date']);
					$temp               = explode('-', $_ALL_POST['to_date'][0]);
					$_ALL_POST['to_date']  = NULL;
					$_ALL_POST['to_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['to_date']  = '';                            
				}
				
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/hr-salary-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'lst_calendar', 'value' => 'lst_calendar');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-salary-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
