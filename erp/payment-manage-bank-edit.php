<?php
  if ( $perm->has('nc_p_mb_edit') ) {
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
      
        include_once (DIR_FS_INCLUDES .'/payment-bank.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Paymentbank::validateUpdate($data, $extra)){
				$data['company_name'] = '';
                $data['company_prefix'] ='';
                if(!empty($data['company_id'])){
                    $company1 =null;
                    $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                    $fields_c1 = " prefix,name ";
                    Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
                    if(!empty($company1)){                  
                       $data['company_name'] = $company1[0]['name'];
                       $data['company_prefix'] = $company1[0]['prefix'];
                    }
                }				
			
                $query  = " UPDATE ". TABLE_PAYMENT_BANK
						." SET ". TABLE_PAYMENT_BANK .".bank_name = '". 			$data['bank_name'] ."'"                        .",". TABLE_PAYMENT_BANK .".bank_address = '". 		$data['bank_address'] ."'"         
						.",". TABLE_PAYMENT_BANK .".type = '". 			$data['type'] ."'"  
						.",". TABLE_PAYMENT_BANK .".company_id = '". $data['company_id'] ."'"  
						.",". TABLE_PAYMENT_BANK .".company_name = '". $data['company_name'] ."'"  
						.",". TABLE_PAYMENT_BANK .".company_prefix = '". $data['company_prefix'] ."'"  
						.",". TABLE_PAYMENT_BANK .".type = '".$data['type'] ."'"  
						.",". TABLE_PAYMENT_BANK .".status = '".$data['status'] ."'"  
						.",". TABLE_PAYMENT_BANK .".opening_balance = '".$data['opening_balance'] ."'"    
						.",". TABLE_PAYMENT_BANK .".balance_amt = '".$data['opening_balance'] ."'"
						." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Bank has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_PAYMENT_BANK .'.*'  ;            
            $condition_query = " WHERE (". TABLE_PAYMENT_BANK .".id = '". $id ."' )";
            
            if ( Paymentbank::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/payment-manage-bank-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-manage-bank-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-add-permission.html');
        
    }
?>
