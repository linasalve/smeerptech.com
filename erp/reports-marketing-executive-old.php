<?php

if ( $perm->has('nc_rp_mkt_ex') ) {

	    include_once ( DIR_FS_INCLUDES .'/leads-order.inc.php');     
		include_once ( DIR_FS_INCLUDES .'/leads-quotation.inc.php');	
         //to view report member id should be compulsary
        $executive	= isset($_GET["executive"]) 	? $_GET["executive"]	: ( isset($_POST["executive"]) 	? $_POST["executive"] : '' );
        $executive_details	= isset($_GET["executive_details"]) 	? $_GET["executive_details"]	: ( isset($_POST["executive_details"]) 	? $_POST["executive_details"] : '' );
        $_SEARCH["executive"] 	= $executive;       
        $_SEARCH["executive_details"] 	= $executive_details;       
        $where_added    = true;
        $allData = $_GET 	? $_GET	: ($_POST 	? $_POST : '' );
        $extra_url=$condition_query=$condition_url=$pagination ='';
        
		// Read the Date data.
                $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
                $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
                $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
                $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
                
		include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
		$fieldsu=TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".number,".TABLE_USER.".user_id" ;
		$ustr= str_replace(",","','",trim($my['my_reporting_members'],","));
	    $condition_query_u  = " WHERE ".TABLE_USER.".status='".User::ACTIVE."' AND ".TABLE_USER.".user_id IN('".$ustr."','".$my['uid']."')" ;
		User::getList( $db, $ulist, $fieldsu, $condition_query_u);
		
            
        if(array_key_exists('submit_search',$allData)){
        
            if(!empty($executive) && !empty($chk_date_from) && !empty($date_from) 
				&& !empty($chk_date_to) && !empty($date_to) ){        
                
                // BO: From Date
                if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from)) {
                    
                    $dfa = explode('/', $date_from);
                   // $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
                    $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] ;
                    
                    $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
                    $_SEARCH["chk_date_from"]   = $chk_date_from;
                    $_SEARCH["date_from"]       = $date_from;
                }
                
                // EO: From Date
                
                // BO: Upto Date
                if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)){
                    
                    $dta = explode('/', $date_to);
                    //$dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';                   
                    $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0];                   
                    
                    $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
                    $_SEARCH["chk_date_to"] = $chk_date_to ;
                    $_SEARCH["date_to"]     = $date_to ;
                }                
                // EO: Upto Date    

				$condition_query = " WHERE ".TABLE_USER.".user_id IN ('".$executive."')";
				
			    $list	= 	NULL;
				$total	=	User::getList($db, $list, '', $condition_query);
			
				$extra_url  = '';
				if ( isset($condition_url) && !empty($condition_url) ) {
					$extra_url  = $condition_url;
				}
				$condition_url .="&perform=".$perform;            
				$extra_url  .= "&x=$x&rpp=$rpp";
				$extra_url  = '&start=url'. $extra_url .'&end=url';
				
				
				$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
			
				$list	= NULL;
				$fields=TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".number, ".TABLE_USER.".user_id " ;
				
				User::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
                 
                
				foreach($list as $key=>$val){
				
					
					$name = $val['f_name']." ".$val['l_name']." - ( ".$val['number']." ) " ;
					$prospects = '';
					$leads = '';
					$negative_leads = $active_leads = $crm_negative_leads = $crm_positive_leads = '';
					$leads_members = $proposals = '';
					$orders_members = $followups = '';
					
					$sql = " SELECT COUNT(lead_id) as  prospects FROM ".TABLE_SALE_LEADS." 
						WHERE ".TABLE_SALE_LEADS.".created_by='".$val['user_id']."' AND 
						date_format(".TABLE_SALE_LEADS.".do_add,'%Y-%m-%d') >= '".$dfa."' AND 
						date_format(".TABLE_SALE_LEADS.".do_add,'%Y-%m-%d') <= '".$dta."'";
					
					 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$prospects =$db->f('prospects');			
						}
					}
					 
					$sql = " SELECT COUNT(id) as leads FROM ".TABLE_LEADS_ORDERS." 
						WHERE ".TABLE_LEADS_ORDERS.".created_by='".$val['user_id']."' AND
						date_format(".TABLE_LEADS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND 
						date_format(".TABLE_LEADS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'						
						";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$leads =$db->f('leads');			
						}
					}
					 
					
					$sql = " SELECT COUNT(id) as negative_leads FROM ".TABLE_LEADS_ORDERS." 
						WHERE ".TABLE_LEADS_ORDERS.".status_by = '".$val['user_id']."' 
						AND date_format(".TABLE_LEADS_ORDERS.".status_dt,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_LEADS_ORDERS.".status_dt,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_LEADS_ORDERS.".status='".LeadsOrder::NEGATIVE."'";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$negative_leads =$db->f('negative_leads');
						}
					}	
					
					$sql = " SELECT COUNT(id) as active_leads FROM ".TABLE_LEADS_ORDERS." 
						WHERE ".TABLE_LEADS_ORDERS.".team LIKE '%,".$val['user_id'].",%' 
						AND ".TABLE_LEADS_ORDERS.".status='".LeadsOrder::ACTIVE."'
						AND date_format(".TABLE_LEADS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_LEADS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'						
						";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$active_leads =$db->f('active_leads');
					
						}
					}
					
					 
					$sql = " SELECT COUNT(id) as crm_negative_leads FROM ".TABLE_LEADS_ORDERS." 
						WHERE ".TABLE_LEADS_ORDERS.".crm_status_by = '".$val['user_id']."' 
						AND date_format(".TABLE_LEADS_ORDERS.".crm_status_dt,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_LEADS_ORDERS.".crm_status_dt,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_LEADS_ORDERS.".crm_status='".LeadsOrder::CRM_NEGATIVE."'";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$crm_negative_leads =$db->f('crm_negative_leads');
					
						}
					}
					 
					$sql = " SELECT COUNT(id) as crm_positive_leads FROM ".TABLE_LEADS_ORDERS." 
						WHERE ".TABLE_LEADS_ORDERS.".crm_status_by = '".$val['user_id']."' 
						AND date_format(".TABLE_LEADS_ORDERS.".crm_status_dt,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_LEADS_ORDERS.".crm_status_dt,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_LEADS_ORDERS.".crm_status='".LeadsOrder::CRM_POSITIVE."'";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$crm_positive_leads =$db->f('crm_positive_leads');
						}
					}
					
					 
					$sql = " SELECT COUNT(id) as leads_members FROM ".TABLE_LEADS_ORDERS." 
						WHERE ".TABLE_LEADS_ORDERS.".team LIKE '%,".$val['user_id'].",%' 
						AND date_format(".TABLE_LEADS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_LEADS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						
						";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$leads_members =$db->f('leads_members');
					
						}
					}
					 
				    $sql = " SELECT COUNT(id) as proposals FROM ".TABLE_LEADS_QUOTATION." 
						WHERE ".TABLE_LEADS_QUOTATION.".created_by = '".$val['user_id']."' 
						AND date_format(".TABLE_LEADS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_LEADS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_LEADS_QUOTATION.".status IN 
						('".LeadsQuotation::ACTIVE."','".LeadsQuotation::PENDING."')";
						
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$proposals =$db->f('proposals');
					
						} 
					}
					
					 
					$sql = " SELECT COUNT(id) as orders_members FROM ".TABLE_BILL_ORDERS." 
						WHERE ".TABLE_BILL_ORDERS.".team LIKE '%,".$val['user_id'].",%' 
						AND date_format(".TABLE_BILL_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_BILL_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$orders_members =$db->f('orders_members');
					
						}
					}
					 
					$sql = " SELECT COUNT(ticket_id) as followups FROM ".TABLE_LD_TICKETS." 
						WHERE ".TABLE_LD_TICKETS.".ticket_creator_uid = '".$val['user_id']."' 
						AND date_format(".TABLE_LD_TICKETS.".do_e,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_LD_TICKETS.".do_e,'%Y-%m-%d') <= '".$dta."'
						";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$followups =$db->f('followups');
					
						}
					}
					
					
					$name = $val['f_name']." ".$val['l_name']." - ( ".$val['number']." ) " ;
					
					$fList[]= array(
						'name'=>$name,
						'prospects'=>$prospects,
						'leads'=>$leads,
						'negative_leads'=>$negative_leads,
						'active_leads'=>$active_leads,
						'crm_negative_leads'=>$crm_negative_leads,
						'crm_positive_leads'=>$crm_positive_leads,
						'leads_members'=>$leads_members,
						'proposals'=>$proposals,
						'followups'=>$followups,
						'orders_members'=>$orders_members
					);
				
				}
               
              
                $extra_url  .= "&x=$x&rpp=$rpp&executive=$executive&executive_details=$executive_details&submit_search=1";
                $extra_url  = '&start=url'. $extra_url .'&end=url';
            
			
				
				//Datewise Leads Details Along With The Leads Reporting BOF
				
				 
				 
				 
				 
				
				
				
				
				
				
				
				
				
				
				
				
				
			
			
			}else{
                $messages->setErrorMessage('Please select Executive and Date Duration, From date and To date.');
            }
			
			
			
			
        }   
            
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    
    $variables['can_view_list_all'] = false;
    if ( $perm->has('nc_ld_or_list_all') ) {
		$variables['can_view_list_all'] = true;
	}
     
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
	$page["var"][] = array('variable' => 'ulist', 'value' => 'ulist');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    
    
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-marketing-executive.html');
}
else {
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>