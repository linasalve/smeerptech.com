<?php
	if ( $perm->has('nc_bl_inv_status') ) {
        $inv_id	= isset($_GET["inv_id"]) ? $_GET["inv_id"] 	: ( isset($_POST["inv_id"]) ? $_POST["inv_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        if ( $perm->has('nc_bl_inv_status_al') ) {
            $access_level += 1;
        }
		
		$access_level_ot = 0;
        if ( $perm->has('nc_bl_inv_status_ot') ) {
            $access_level_ot = $my['access_level'];
			if ( $perm->has('nc_bl_inv_status_ot_al') ) {
				$access_level_ot += 1;
			}
        }
        
        $extra = array( 'db'           		=> &$db,
                        'my' 				=> $my,
						'access_level' 		=> $access_level,
						'access_level_ot' 	=> $access_level_ot,
                        'messages'     		=> &$messages
                    );
        Invoice::updateStatus($inv_id, $status, $extra);
        
        $perform   = 'search';
		$searchStr = 1 ;
        $action    = " Invoice List ";		 
		if(!empty($inv_id)){
			 $condition_query = " WHERE ".TABLE_BILL_INV.".id = '".$inv_id."'";
		}
        // Display the  list.
        //include ( DIR_FS_NC .'/bill-invoice-search.php');
        
        include ( DIR_FS_NC .'/bill-invoice-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>