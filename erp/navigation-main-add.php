<?php
    if ( $perm->has('nc_nav_add') ) {

        $_ALL_POST	= NULL;
        $data       = NULL;
        $access_level   = $my['access_level'];

        /* $services = NULL;
		Navigation::getParent($db,$services);  */       
        
		$menu=array(); 
        $menu = Navigation::getComboMenus();
		
        $_ALL_POST['parent_id']=0; 
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $extra = array( 'db' 		=> &$db,
            				'access_level'      => $access_level,
                            'messages'  => $messages
                        );
            
           
            if ( Navigation::validateAdd($data, $extra) ) {
                
                $query	= " INSERT INTO ". TABLE_NAVIGATION
                        ." SET ". TABLE_NAVIGATION .".title     		  	= '". $data['title'] ."'"                             
							.",". TABLE_NAVIGATION .".parent_id        		= '". $data['parent_id'] ."'"
							.",". TABLE_NAVIGATION .".page        		= '". $data['page1'] ."'"
                            .",". TABLE_NAVIGATION .".image        		= '". $data['image'] ."'"							
					        .",". TABLE_NAVIGATION .".level    		  	= '0'"
					        .",". TABLE_NAVIGATION .".selected_page    	= '". $data['selected_page'] ."'"
                            .",". TABLE_NAVIGATION .".sequence    		= '". $data['sequence'] ."'"
							.",". TABLE_NAVIGATION .".perm_name    		= '". $data['perm_name'] ."'"
							.",". TABLE_NAVIGATION .".access_level 		= '". $my['access_level'] ."'"
							.",". TABLE_NAVIGATION .".status 				= '". $data['status'] ."'"                                
							.",". TABLE_NAVIGATION .".ip     				= '". $_SERVER['REMOTE_ADDR'] ."'"                                
							.",". TABLE_NAVIGATION .".created_by 			= '". $my['user_id'] ."'"
                            .",". TABLE_NAVIGATION .".do_e   		 	  	= '". date('Y-m-d') ."'";
                            
                if ($db->query($query) && $db->affected_rows() > 0){       
                	$messages->setOkMessage("The New Service has been added.");         
                    $variables['hid'] =$variables['id'] =  $db->last_inserted_id();                
                }
                
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
            $_ALL_POST	= $data;
        }
        
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/navigation-main.php?perform=add&added=1&hid=".$variables['hid']);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/navigation-main.php?perform=list");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/navigation-main.php?added=1&hid=".$variables['hid']);   
            
        }else{
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'menu', 'value' => 'menu');
            $page["var"][] = array('variable' => 'services', 'value' => 'services');            
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'navigation-main-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
