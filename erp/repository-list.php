<?php
if ( $perm->has('nc_rps_list') ) {
        
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    $lst_type = null;
	$lst_type = Repository::getType();
	$lst_docof = null;
	$lst_docof = Repository::getDocOf();
	
    // To count total records.
    $list	= 	NULL;
    $total	=	Repository::getList( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_REPOSITORY.'.*' ;
    Repository::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
	if(!empty($list)){
		foreach( $list as $key=>$val){        		
			$val['doc_of_name']=$val['type_name']='';
			if(!empty($val['doc_of'])){
				$val['doc_of_name']=$lst_docof[$val['doc_of']] ;
			}
			if(!empty($val['type'])){
				$val['type_name']=$lst_type[$val['type']] ;
			}			
			$fList[$key]=$val;
		
		}
	}
    
    
    $variables['path'] = DIR_WS_REPOSITORY_FILES;
    $variables['can_view_list'] = false;
    $variables['can_add'] = false;
	$variables['can_edit'] = false;
	$variables['can_delete'] = false;
	$variables['can_sendmail'] = false;
	 
    // Set the Permissions.   
    if ( $perm->has('nc_rps_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_rps_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_rps_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_rps_delete') ) {
        $variables['can_delete'] = true;
    }
	if ( $perm->has('nc_rps_sendmail') ) {
        $variables['can_sendmail'] = true;
    }

    //repository_path mime_content_type();
	
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
	
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'repository-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
