<?php
  if ( $perm->has('nc_st_tc_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
	   
		
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
			 
          
            
                 
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
						);
            
          
               
				if( StTemplateCategory::validateAdd($data, $extra) ) { 
					 
                   $query	= " INSERT INTO ".TABLE_ST_TEMPLATE_CATEGORY
					." SET ".TABLE_ST_TEMPLATE_CATEGORY .".category = '". $data['category'] ."'"                     
					.",". TABLE_ST_TEMPLATE_CATEGORY .".details 	= '". $data['details'] ."'" 
					.",". TABLE_ST_TEMPLATE_CATEGORY .".status   = '". $data['status'] ."'" 
					.",". TABLE_ST_TEMPLATE_CATEGORY .".ip     = '".   $_SERVER['REMOTE_ADDR'] ."'"                    .",". TABLE_ST_TEMPLATE_CATEGORY .".added_by = 	  '".$my['user_id']."'"
					.",". TABLE_ST_TEMPLATE_CATEGORY .".added_by_name = '".$my['f_name']." ".$my['l_name'] ."'"
					.",". TABLE_ST_TEMPLATE_CATEGORY .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                    
					
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Record has been added successfully.");
                        $variables['hid'] = $db->last_inserted_id();              
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    //$data		= NULL;
		        }
        }               
        
        
			$hidden[] = array('name'=> 'ajx','value' => $ajx);
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
                
             
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'st-template-category-add.html');
         
    }else {
        
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
