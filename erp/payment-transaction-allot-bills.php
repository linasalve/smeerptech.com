<?php    
    if ( $perm->has('nc_p_pt_bl') ) {
		include_once ( DIR_FS_INCLUDES .'/payment-party-bills.inc.php' );
        $transaction_id = isset ($_GET['transaction_id']) ? $_GET['transaction_id'] : ( isset($_POST['transaction_id'] ) ? 
		$_POST['transaction_id'] :''); 
        $exchange_rate = isset ($_GET['exchange_rate']) ? $_GET['exchange_rate'] : ( isset($_POST['exchange_rate'] ) ? $_POST['exchange_rate'] :'');
        $transaction_number = isset ($_GET['transaction_number']) ? $_GET['transaction_number'] : ( isset($_POST['transaction_number'] ) ? 
		$_POST['transaction_number'] :'');
        $party_id = isset ($_GET['party_id']) ? $_GET['party_id'] : ( isset($_POST['party_id'] ) ? $_POST['party_id'] :'');
        $vendor_bank_id = isset ($_GET['vendor_bank_id']) ? $_GET['vendor_bank_id'] : ( isset($_POST['vendor_bank_id'] ) ? $_POST['vendor_bank_id'] :'');
        $executive_id = isset ($_GET['executive_id']) ? $_GET['executive_id'] : ( isset($_POST['executive_id'] ) ? $_POST['executive_id'] :'');
        $pamount = isset ($_GET['pamount']) ? $_GET['pamount'] : ( isset($_POST['pamount'] ) ? $_POST['pamount'] :'');
        $company_id = isset ($_GET['company_id']) ? $_GET['company_id'] : ( isset($_POST['company_id'] ) ? $_POST['company_id'] :'');
		$abill_id = isset ($_GET['abill_id']) ? $_GET['abill_id'] : ( isset($_POST['abill_id'] ) ? $_POST['abill_id'] :'');
		
		$rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     :  10);// Result per page 
		if(empty($rpp)){
			$rpp = RESULTS_PER_PAGE;
		} 
		$condition_query ='';
		if ( empty($x) ) {
			$x              = 1;
			$pageno              = 1;
			$next_record    = 0 ;
		}else {
			$next_record    = ($x-1) * $rpp;
		}
		$variables["x"]     = $x;
		$variables["pageno"]     = $pageno;
		$variables["rpp"]   = $rpp;
		
        include_once ( DIR_FS_INCLUDES .'/payment-party-bills.inc.php');
		
        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $db1 = new db_local; // database handle
        
        
		//GET TRA DETAILS BOF
		$fields = TABLE_PAYMENT_TRANSACTION .".transaction_id,".TABLE_PAYMENT_TRANSACTION.".pay_received_amt,
		".TABLE_PAYMENT_TRANSACTION.".pay_cheque_no,".TABLE_PAYMENT_TRANSACTION.".pay_bank_company,"
		.TABLE_PAYMENT_TRANSACTION.".transaction_type,".TABLE_PAYMENT_TRANSACTION.".pay_branch,"
		.TABLE_PAYMENT_TRANSACTION.".credit_pay_bank_id,".TABLE_PAYMENT_TRANSACTION.".debit_pay_bank_id,"
		.TABLE_PAYMENT_TRANSACTION.".currency_abbr,".TABLE_PAYMENT_TRANSACTION.".currency_id,"
		.TABLE_PAYMENT_TRANSACTION.".currency_name,".TABLE_PAYMENT_TRANSACTION.".currency_symbol,"
		.TABLE_PAYMENT_TRANSACTION.".currency_country,".TABLE_PAYMENT_TRANSACTION.".exchange_rate,"
		.TABLE_PAYMENT_TRANSACTION.".behalf_vendor_bank,".TABLE_PAYMENT_TRANSACTION.".behalf_client,"
		.TABLE_PAYMENT_TRANSACTION.".behalf_executive,"
		.TABLE_PAYMENT_TRANSACTION.".behalf_company_id,"
		.TABLE_USER .".f_name as efname,".TABLE_USER .".l_name as elname,"
        .TABLE_CLIENTS .".f_name as cfname,".TABLE_CLIENTS .".l_name as clname,"
		.TABLE_CLIENTS .".billing_name as cbilling_name ";
		//.TABLE_VENDORS .".f_name as fname, ". TABLE_VENDORS .".l_name as lname, ". TABLE_VENDORS .".billing_name";
        $condition_query_tra = "WHERE ".TABLE_PAYMENT_TRANSACTION.".id=".$transaction_id."";
		Paymenttransaction::getDetails( $db, $traDetails, $fields, $condition_query_tra);
		if(!empty($traDetails)){
			$traDetails=$traDetails[0];
			$currency_id = $traDetails['currency_id'];
			$exchange_rate = $traDetails['exchange_rate'];
			$behalf_vendor_bank = $traDetails['behalf_vendor_bank'];
			$behalf_client 		= $traDetails['behalf_client'];
			$behalf_executive 	= $traDetails['behalf_executive'];
			$behalf_company_id 	= $traDetails['behalf_company_id'];
			 
			if( $traDetails['transaction_type'] ==Paymenttransaction::PAYMENTIN ){
           
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$traDetails['credit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $bankname = $banknameArr['bank_name'] ;                    
                }
                $traDetails['transaction_typename']='Payment In';
                $traDetails['bankname']= $bankname;
            }elseif($traDetails['transaction_type'] ==Paymenttransaction::PAYMENTOUT){
           
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$traDetails['debit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $bankname = $banknameArr['bank_name'] ;                    
                }
                $traDetails['transaction_typename']='Payment Out';
				$traDetails['bankname']= $bankname;
            }
			 
		}
		
		//GET TRA DETAILS EOF        		
		if(isset($abill_id) && $abill_id>0){
		
			$fields = " SUM(".TABLE_PAYMENT_TRANSACTION_BILLS.".amount) as totDelAmount, 
			SUM(".TABLE_PAYMENT_TRANSACTION_BILLS.".amount_inr) as totDelAmountInr,
			".TABLE_PAYMENT_TRANSACTION_BILLS.".bill_number";
			$sql="SELECT ".$fields ." FROM ".TABLE_PAYMENT_TRANSACTION_BILLS." WHERE 
			bill_id='".$abill_id."' AND transaction_id='".$transaction_id."'";
			$db->query($sql);
			$abill_no='';
			$delAmount=0;
			if($db->nf()>0 ){
				while ($db->next_record()) {				 
					$delAmount = $db->f('totDelAmount');					
					$delAmountInr = $db->f('totDelAmountInr');					
					$abill_no = $db->f('bill_number');
				}
			}
			$sql1=" SELECT transaction_number,amount,balance,amount_inr,balance_inr,exchange_rate
			FROM ".TABLE_PAYMENT_PARTY_BILLS." WHERE id= ".$abill_id." LIMIT 0,1 " ;
			$delTranNo=$billPaidAmt=$billBalAmt=0;
			$delTranNoArr=array();
			$db->query($sql1);
			if($db->nf()>0 ){
				while ($db->next_record()) {
					$delTranNo = $db->f('transaction_number');	
					$billPaidAmt = $db->f('amount');	
					$billBalAmt = $db->f('balance');	
					$billPaidAmtInr = $db->f('amount_inr');	
					$billBalAmtInr = $db->f('balance_inr');
					$billExcRt = $db->f('exchange_rate');
					$delTranNoArr =explode(",",$delTranNo);
					$delTranNoArr=array_flip($delTranNoArr);
					unset($delTranNoArr[$transaction_number]);
					 $delTranNoArr=array_flip($delTranNoArr);					
					$tranStr=implode(",",$delTranNoArr);
					$tranStr = trim($tranStr,",");
					if(!empty($tranStr)){
						$tranStr=",".$tranStr ;
					}
				}
			}			
			$sql1=" SELECT bill_nc_no, exchange_rate FROM ".TABLE_PAYMENT_TRANSACTION."  WHERE id= ".$transaction_id." LIMIT 0,1" ;
			$delBillNcNo=0;
			$delTranNoArr=array();
			$db->query($sql1);
			if($db->nf()>0 ){
				while ($db->next_record()) {
					$delBillNcNo = $db->f('bill_nc_no');	
					$billPaidExRt = $db->f('exchange_rate');	
					$delBillNcNoArr =explode(",",$delBillNcNo);
					$delBillNcNoArr=array_flip($delBillNcNoArr);
					unset($delBillNcNoArr[$abill_no]);
					$delBillNcNoArr=array_flip($delBillNcNoArr);					 
					$tranbillncStr=implode(",",$delBillNcNoArr);
					$tranbillncStr = trim($tranbillncStr,",");
					if(!empty($tranbillncStr)){
						$tranbillncStr=",".$tranbillncStr ;
					}
				}
			}			 
			$sql_bill_status1='';
			 
			
			if($billPaidAmt>=0 && $delAmount >=0 ){
				$updateAmt = $billBalAmt + $delAmount ;
				$updateAmtInr = $billBalAmtInr + $delAmountInr ;
				
				if($billPaidAmt > $updateAmt){
					$sql_bill_status1 = " bill_status ='".PaymentPartyBills::BILL_STATUS_PARTIALPAID."'," ;
				}elseif($billPaidAmt == $updateAmt){
					$sql_bill_status1 = " bill_status ='".PaymentPartyBills::BILL_STATUS_UNPAID."'," ;
				}
				$sql = "UPDATE ".TABLE_PAYMENT_PARTY_BILLS." SET  
							".$sql_bill_status1." ".TABLE_PAYMENT_PARTY_BILLS.".balance = ".$updateAmt.",
							".TABLE_PAYMENT_PARTY_BILLS.".balance_inr = ".$updateAmtInr.",
							".TABLE_PAYMENT_PARTY_BILLS.".transaction_number = '".$tranStr."' 
							WHERE  ".TABLE_PAYMENT_PARTY_BILLS.".id= ".$abill_id ;
				 
				$db->query($sql);
				$pay_received_amt = $delAmount;
				$pay_received_amt_rcpt_inr = $delAmountInr ;
				$sql = "UPDATE ".TABLE_PAYMENT_TRANSACTION." SET  
						".TABLE_PAYMENT_TRANSACTION.".bill_nc_no = '".$tranbillncStr."',
						".TABLE_PAYMENT_TRANSACTION.".pay_received_amt_rcpt = 
						(pay_received_amt_rcpt + '".$pay_received_amt ."'),
						".TABLE_PAYMENT_TRANSACTION.".pay_received_amt_rcpt_inr 
						= ( pay_received_amt_rcpt_inr + '".$pay_received_amt_rcpt_inr ."') 
						 WHERE  ".TABLE_PAYMENT_TRANSACTION.".id= ".$transaction_id ;	
				$db->query($sql);
				
				$sql = "DELETE FROM ".TABLE_PAYMENT_TRANSACTION_BILLS."  
					 WHERE bill_id='".$abill_id."' AND transaction_id='".$transaction_id."'";
				$db->query($sql);
			}
			
		}
		
		$fields = "SUM(".TABLE_PAYMENT_TRANSACTION_BILLS.".amount) as totamt ";
        $condition_query = " INNER JOIN  ".TABLE_PAYMENT_PARTY_BILLS." ON 
		".TABLE_PAYMENT_PARTY_BILLS.".id= ".TABLE_PAYMENT_TRANSACTION_BILLS.".bill_id 
		WHERE transaction_id='".$transaction_id."'";
        if ( Paymenttransaction::getBillTransactionList($db, $totalAmtArr, $fields, $condition_query) > 0 ) {           
            $totalAmt =$totalAmtArr[0]['totamt'];
            $totalBalance =$pamount-$totalAmt;
        }
		//$proper_dt='2009-03-31 00:00:00';
		//$proper_dt='2010-03-31 00:00:00';
		$proper_dt='2011-03-31 00:00:00';
        if(array_key_exists('allot_bills',$_POST)){
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $extra = array( 'db' 				=> &$db,
                            //'access_level'      => $access_level,
							'messages'          => &$messages
                        );
           
            if(Paymenttransaction::validateBills($data, $extra) ){
				
                foreach($data['bill_id'] as $key=>$val){
					if($data['validate_paid_amt'][$key]==1 ){	
					  	if(($data['amount'][$key]==0 && $data['billamount'][$key]==0 )|| 
						($data['amount'][$key]>0 && $data['billamount'][$key]>0) ){
							$forexGain=$forexLoss=0;
							$balance=-1;
							$balance = $data['billamount'][$key] - $data['amount'][$key];
							$amount_inr = $data['billamount'][$key] *  $data['bill_exchange_rate'][$key] ;
							$balance_inr = $amount_inr - ( $data['amount'][$key] * $traDetails['exchange_rate']);
							$sql_bill_status='';
							if($balance==0){
								$sql_bill_status = " bill_status ='".PaymentPartyBills::BILL_STATUS_PAID."'," ;
							}elseif($balance>0){						
								$sql_bill_status = " bill_status ='".PaymentPartyBills::BILL_STATUS_PARTIALPAID."'," ;
							}
							
							//Set paid amount is bill amt/late amt/after amt
							$sql = " UPDATE ".TABLE_PAYMENT_PARTY_BILLS." SET amount =".$data['billamount'][$key].",
								amount_inr =".$amount_inr.",
							  ".$sql_bill_status." balance = ".$balance.", balance_inr = ".$balance_inr.",
							  transaction_number = CONCAT( transaction_number, ',".$transaction_number."' ) 
							  WHERE  id= ".$data['bill_id'][$key] ;
							$db->query($sql); 
							if($balance_inr>0){
								$forex_gain = $balance_inr;
							}else{
								$forex_loss = $balance_inr;
							}
							$paid_amt =  $data['amount'][$key];
							$paid_amt_inr =$paid_amt * $traDetails['exchange_rate'] ;
							
					$query	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION_BILLS
				." SET ". TABLE_PAYMENT_TRANSACTION_BILLS .".transaction_id = '". $data['transaction_id'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".bill_id = '".$data['bill_id'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".gl_type_name = '".$data['gl_type_name'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".gl_type = '".$data['gl_type'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".bill_number = '".$data['number'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_abbr = '".$traDetails['currency_abbr'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_id = '".$traDetails['currency_id'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_name = '".$traDetails['currency_name'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_symbol = '".$traDetails['currency_symbol'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_country = '".$traDetails['currency_country'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".exchange_rate = '".$traDetails['exchange_rate'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".forex_gain = '".$forex_gain ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".forex_loss = '".$forex_loss ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".amount = '".$paid_amt."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".amount_inr = '". $paid_amt_inr."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".balance_amt = ".$balance.""
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".balance_amt_inr = '". $balance_inr."'" 				
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".details = '".$data['details'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".ip     = '". $_SERVER['REMOTE_ADDR'] ."'"        
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".created_by = '".$my['user_id'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".created_by_name = '".$my['f_name']." ".$my['l_name']."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".date   = '".date('Y-m-d H:i:s')."'";
							$db->query($query); 
							 
							if ( !empty($data['number'][$key]) ) {
								$data['amount_inr'][$key] = ( $data['amount'][$key] * $traDetails['exchange_rate']) ;
								$sql_vh = " UPDATE ".TABLE_PAYMENT_TRANSACTION 
								." SET pay_received_amt_rcpt =(pay_received_amt_rcpt - '".$data['amount'][$key] ."'),"
					." pay_received_amt_rcpt_inr = ( pay_received_amt_rcpt_inr - '".$data['amount_inr'][$key] ."'),"                    ." bill_nc_no = CONCAT( bill_nc_no, ',".$data['number'][$key]."' )"  ." WHERE id =".$data['transaction_id'];
								
								$db->query($sql_vh);	

								// Update the status in transaction EOF
								$sql="SELECT pay_received_amt_rcpt FROM ".TABLE_PAYMENT_TRANSACTION." WHERE 
								id =".$data['transaction_id'];
								$db->query( $sql );
								$pay_received_amt_rcpt=0;
								if( $db->nf() > 0 ){
									while($db->next_record()){
										$pay_received_amt_rcpt = $db->f('pay_received_amt_rcpt') ;
									}
								}
								if($pay_received_amt_rcpt <=0 ){
									$sql_vh1=" UPDATE ".TABLE_PAYMENT_TRANSACTION." SET linking_status
										='".Paymenttransaction::COMPLETEDLINK."'  WHERE id =".$data['transaction_id'];
									$db->query( $sql_vh1 );	
								}elseif($pay_received_amt_rcpt>0){
									$sql_vh1=" UPDATE ".TABLE_PAYMENT_TRANSACTION." SET linking_status 
									='".Paymenttransaction::PARTIALLINK."' WHERE id =".$data['transaction_id'];
									$db->query( $sql_vh1 );	
								}
								// Update the status in transaction EOF
							} 
						}
					}else{
				
						 
						if(($data['amount'][$key]==0 && $data['billamount'][$key]==0 )|| 
						($data['amount'][$key]>0 && $data['billamount'][$key]>0) ){
						    $forexGain=$forexLoss=0;
							$balance =  -1;
							$balance_inr=0;
							///$balance = $data['billamount'][$key] - $data['amount'][$key];
							///$amount_inr = $data['billamount'][$key] *  $data['bill_exchange_rate'][$key] ;
							$amount_inr = ( $data['amount'][$key] * $traDetails['exchange_rate']);
							
							 $sql = "UPDATE ".TABLE_PAYMENT_PARTY_BILLS." SET 
							 balance = balance - ".$data['amount'][$key].",
							 balance_inr = balance_inr - ".$amount_inr.",
							 transaction_number = CONCAT( transaction_number, ',".$transaction_number."' ) 
							WHERE  id=".$data['bill_id'][$key] ;
							$db->query($sql);
							$sql="SELECT balance, balance_inr FROM ".TABLE_PAYMENT_PARTY_BILLS." WHERE 
							id=".$data['bill_id'][$key] ;
							$db->query($sql);
							if($db->nf()>0 ){
								while ($db->next_record()) {
									  $balance = $db->f('balance');
									  $balance_inr = $db->f('balance_inr');
								}
							} 
							if($balance_inr>0){
								$forex_gain=$balance_inr;
							}else{
								$forex_loss=$balance_inr;
							}
							if($balance==0){								
								$sql = "UPDATE ".TABLE_PAYMENT_PARTY_BILLS." SET bill_status 
								= '".PaymentPartyBills::BILL_STATUS_PAID."' WHERE  id=".$data['bill_id'][$key] ;
								$db->query($sql);
							}elseif($balance>0){
								$sql = "UPDATE ".TABLE_PAYMENT_PARTY_BILLS." SET bill_status 
								= '".PaymentPartyBills::BILL_STATUS_PARTIALPAID."' WHERE  
								id=".$data['bill_id'][$key] ;
								$db->query($sql);
							}
							$paid_amt =  $data['amount'][$key];
							$paid_amt_inr =$paid_amt * $traDetails['exchange_rate'] ;
							 $query	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION_BILLS
				." SET ". TABLE_PAYMENT_TRANSACTION_BILLS .".transaction_id = '". $data['transaction_id'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".bill_id = '". $data['bill_id'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".gl_type_name = '".$data['gl_type_name'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".gl_type = '".$data['gl_type'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".bill_number = '".$data['number'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_abbr = '".$traDetails['currency_abbr'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_id = '".$traDetails['currency_id'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_name = '".$traDetails['currency_name'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_symbol = '".$traDetails['currency_symbol'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".currency_country = '".$traDetails['currency_country'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".exchange_rate = '".$traDetails['exchange_rate'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".forex_gain = '".$forex_gain ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".forex_loss = '".$forex_loss ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".amount = '".$paid_amt."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".amount_inr = '". $paid_amt_inr."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".balance_amt = ".$balance.""
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".balance_amt_inr = '". $balance_inr."'" 	
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".details = '".$data['details'][$key] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".ip = '".$_SERVER['REMOTE_ADDR'] ."'" 
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".created_by = '". $my['user_id'] ."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".created_by_name = 	'".$my['f_name']." ". $my['l_name']."'"
				.",". TABLE_PAYMENT_TRANSACTION_BILLS .".date = '".date('Y-m-d H:i:s')."'";
	
							$db->query($query);
							
							//update the bill nos in transactions and the amount
							if ( !empty($data['number'][$key]) ) {
								$data['amount_inr'][$key] = ( $data['amount'][$key] * $traDetails['exchange_rate']) ;
							 
								$sql_vh = " UPDATE ".TABLE_PAYMENT_TRANSACTION 
						." SET pay_received_amt_rcpt = ( pay_received_amt_rcpt-'".$data['amount'][$key]."' ),"
						." pay_received_amt_rcpt_inr=( pay_received_amt_rcpt_inr-'".$data['amount_inr'][$key]."' ),"                        ." bill_nc_no = CONCAT( bill_nc_no, ',".$data['number'][$key]."' )"                        
						." WHERE id =".$data['transaction_id'];
								$db->query($sql_vh);
								// Update the status in transaction EOF
								$sql="SELECT pay_received_amt_rcpt FROM ".TABLE_PAYMENT_TRANSACTION." WHERE 
								id =".$data['transaction_id'];
								$db->query( $sql );
								$pay_received_amt_rcpt=0;
								if( $db->nf() > 0 ){
									while($db->next_record()){
										$pay_received_amt_rcpt = $db->f('pay_received_amt_rcpt') ;
									}
								}
								if($pay_received_amt_rcpt <=0 ){
									$sql_vh1=" UPDATE ".TABLE_PAYMENT_TRANSACTION." SET linking_status
										='".Paymenttransaction::COMPLETEDLINK."'  WHERE id =".$data['transaction_id'];
									$db->query( $sql_vh1 );	
								}elseif($pay_received_amt_rcpt>0){
									$sql_vh1=" UPDATE ".TABLE_PAYMENT_TRANSACTION." SET linking_status 
									='".Paymenttransaction::PARTIALLINK."' WHERE id =".$data['transaction_id'];
									$db->query( $sql_vh1 );	
								}
								// Update the status in transaction EOF
							} 
							
						}
						 
					}
					$messages->setOkMessage("Record has been added successfully.");
					
                }   
                header("Location:".DIR_WS_NC."/payment-transaction.php?perform=allot_bills&transaction_id=".$data['transaction_id']."&transaction_number=".$transaction_number."&exchange_rate=".$exchange_rate."&party_id=".$party_id."&executive_id=".$executive_id."&pamount=".$pamount."&company_id=".$company_id);  
  
			} 
        
        }else{
        
            // Read the record details
             if(!empty( $vendor_bank_id) || !empty( $party_id) || !empty($executive_id) ){
				 $behalf_sql='';
				if(!empty($behalf_vendor_bank)){
					$behalf_sql .= " OR ".TABLE_PAYMENT_PARTY_BILLS.".vendor_bank_id ='".$behalf_vendor_bank."'";
				}
				if(!empty($behalf_client)){
					$behalf_sql .= " OR ".TABLE_PAYMENT_PARTY_BILLS.".party_id ='".$behalf_client."'";
				}
				if(!empty($behalf_executive)){
					$behalf_sql .= " OR ".TABLE_PAYMENT_PARTY_BILLS.".executive_id ='".$behalf_executive."'";
				}
				 
			 
                if(!empty($party_id)){
                    $fields = TABLE_PAYMENT_PARTY_BILLS.".number,".TABLE_PAYMENT_PARTY_BILLS.".id,";
                    $fields .= TABLE_PAYMENT_PARTY_BILLS.".balance as bamount,".TABLE_PAYMENT_PARTY_BILLS.".amount,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs, ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_dt, ".TABLE_PAYMENT_PARTY_BILLS.".pay_due_dt,
					".TABLE_PAYMENT_PARTY_BILLS.".late_pay_dt ,".TABLE_PAYMENT_PARTY_BILLS.".after_pay_dt ,
                    ".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".bill_amount,
					".TABLE_PAYMENT_PARTY_BILLS.".late_amount,".TABLE_PAYMENT_PARTY_BILLS.".after_amount,
                    ".TABLE_PAYMENT_PARTY_BILLS.".period_from,".TABLE_PAYMENT_PARTY_BILLS.".period_to,
					".TABLE_PAYMENT_PARTY_BILLS.".gl_type_name,".TABLE_PAYMENT_PARTY_BILLS.".gl_type,
					".TABLE_PAYMENT_PARTY_BILLS.".exchange_rate,".TABLE_PAYMENT_PARTY_BILLS.".bill_attachment,
					".TABLE_PAYMENT_PARTY_BILLS.".currency_name,".TABLE_PAYMENT_PARTY_BILLS.".currency_id,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_details ";
					
					$sub_query ='';
					 
					$sub_query = " AND ".TABLE_PAYMENT_PARTY_BILLS.".number in (5474,5470,5469,5468,5467,5142,5141,5140,5139,5135,5134,5133,5132,5131,
					5130,5129,5128,5127,5126,5125,5124,5123,5122,5121,5120,5119,5138,5137,5136)";	 
					
					$condition_query = ' WHERE ('.TABLE_PAYMENT_PARTY_BILLS.".bill_status
					='".PaymentPartyBills::BILL_STATUS_PARTIALPAID."' OR 
					".TABLE_PAYMENT_PARTY_BILLS.".bill_status='".PaymentPartyBills::BILL_STATUS_UNPAID."')
					AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt > '".$proper_dt."'
					AND ".TABLE_PAYMENT_PARTY_BILLS.".currency_id ='".$currency_id."'
					AND ".TABLE_PAYMENT_PARTY_BILLS.".company_id IN('".$company_id."','".$behalf_company_id."')
					AND ".TABLE_PAYMENT_PARTY_BILLS.".f_status ='0' 
					AND ".TABLE_PAYMENT_PARTY_BILLS.".status = '".PaymentPartyBills::ACTIVE."'
					AND (".TABLE_PAYMENT_PARTY_BILLS.".party_id ='".$party_id."'" .$behalf_sql." ) ".$sub_query."
					ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".id ASC";
				 
					 
					
                }elseif(!empty($executive_id)){
                    $fields = TABLE_PAYMENT_PARTY_BILLS.".number,".TABLE_PAYMENT_PARTY_BILLS.".id,";
                    $fields .= TABLE_PAYMENT_PARTY_BILLS.".balance as bamount,".TABLE_PAYMENT_PARTY_BILLS.".amount,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs, ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_dt, ".TABLE_PAYMENT_PARTY_BILLS.".pay_due_dt,
					".TABLE_PAYMENT_PARTY_BILLS.".late_pay_dt,".TABLE_PAYMENT_PARTY_BILLS.".after_pay_dt, 
					".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".bill_amount, 
					".TABLE_PAYMENT_PARTY_BILLS.".late_amount, ".TABLE_PAYMENT_PARTY_BILLS.".after_amount, 
					".TABLE_PAYMENT_PARTY_BILLS.".period_from, ".TABLE_PAYMENT_PARTY_BILLS.".period_to, 
					".TABLE_PAYMENT_PARTY_BILLS.".gl_type_name,".TABLE_PAYMENT_PARTY_BILLS.".gl_type,
					".TABLE_PAYMENT_PARTY_BILLS.".exchange_rate,".TABLE_PAYMENT_PARTY_BILLS.".bill_attachment,
					".TABLE_PAYMENT_PARTY_BILLS.".currency_name,".TABLE_PAYMENT_PARTY_BILLS.".currency_id,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_details";
                    
					$condition_query = ' WHERE ('.TABLE_PAYMENT_PARTY_BILLS.".bill_status=
					'".PaymentPartyBills::BILL_STATUS_PARTIALPAID."' OR 
					".TABLE_PAYMENT_PARTY_BILLS.".bill_status='".PaymentPartyBills::BILL_STATUS_UNPAID."')
					AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt > '".$proper_dt."'
					AND (".TABLE_PAYMENT_PARTY_BILLS.".executive_id ='".$executive_id."'".$behalf_sql.")
					AND ".TABLE_PAYMENT_PARTY_BILLS.".currency_id ='".$currency_id."'
					AND ".TABLE_PAYMENT_PARTY_BILLS.".company_id IN('".$company_id."','".$behalf_company_id."')
					AND ".TABLE_PAYMENT_PARTY_BILLS.".f_status ='0'
					AND ".TABLE_PAYMENT_PARTY_BILLS.".status = '".PaymentPartyBills::ACTIVE."'
					ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".id ASC";
                }if(!empty($vendor_bank_id)){
                    $fields = TABLE_PAYMENT_PARTY_BILLS.".number,".TABLE_PAYMENT_PARTY_BILLS.".id,";
                    $fields .= TABLE_PAYMENT_PARTY_BILLS.".balance as bamount,".TABLE_PAYMENT_PARTY_BILLS.".amount,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs, ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_dt, ".TABLE_PAYMENT_PARTY_BILLS.".pay_due_dt,
					".TABLE_PAYMENT_PARTY_BILLS.".late_pay_dt ,".TABLE_PAYMENT_PARTY_BILLS.".after_pay_dt ,
                    ".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".bill_amount,
					".TABLE_PAYMENT_PARTY_BILLS.".late_amount,".TABLE_PAYMENT_PARTY_BILLS.".after_amount,
                    ".TABLE_PAYMENT_PARTY_BILLS.".period_from,".TABLE_PAYMENT_PARTY_BILLS.".period_to,
					".TABLE_PAYMENT_PARTY_BILLS.".gl_type_name,".TABLE_PAYMENT_PARTY_BILLS.".gl_type,
					".TABLE_PAYMENT_PARTY_BILLS.".exchange_rate,
                    ".TABLE_PAYMENT_PARTY_BILLS.".bill_attachment,
					".TABLE_PAYMENT_PARTY_BILLS.".currency_name,".TABLE_PAYMENT_PARTY_BILLS.".currency_id,
					".TABLE_PAYMENT_PARTY_BILLS.".bill_details ";
					
					$condition_query = ' WHERE ('.TABLE_PAYMENT_PARTY_BILLS.".bill_status
					='".PaymentPartyBills::BILL_STATUS_PARTIALPAID."' OR 
					".TABLE_PAYMENT_PARTY_BILLS.".bill_status='".PaymentPartyBills::BILL_STATUS_UNPAID."')
					AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt > '".$proper_dt."'
					AND ".TABLE_PAYMENT_PARTY_BILLS.".currency_id ='".$currency_id."'
					AND ".TABLE_PAYMENT_PARTY_BILLS.".company_id IN('".$company_id."','".$behalf_company_id."')
					AND ".TABLE_PAYMENT_PARTY_BILLS.".f_status ='0'
					AND ".TABLE_PAYMENT_PARTY_BILLS.".status = '".PaymentPartyBills::ACTIVE."'
					AND (".TABLE_PAYMENT_PARTY_BILLS.".vendor_bank_id ='".$vendor_bank_id."'" .$behalf_sql.") 
					ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".id ASC";
					
                }
				$condition_query = $condition_query." LIMIT 0,30 ";
			    if($totalBalance>0){
				 
					if( Paymenttransaction::getBillList($db, $_ALL_POST, $fields, $condition_query) > 0 ){
					  
						if(!empty($_ALL_POST)){
							$bill =$number= array();
							$bamount = array();
							foreach($_ALL_POST as $key=>$val){
								$bill[]=$val['id'];
								$gl_type_name[]=$val['gl_type_name'];
								$gl_type[]=$val['gl_type'];
								$currency[]=$val['currency_name'];
								$bill_exchange_rate[]=$val['exchange_rate'];
								$currencyid[]=$val['currency_id'];
								$bill_attachment[]=$val['bill_attachment'];
								$bamount[]=$val['bamount'];
								$number[]=$val['number'];
								$billamount[]=$val['amount'];
								$bill_no[]=$val['bill_no'];
								$bill_dt[]=$val['bill_dt'];
								$pay_due_dt[]=$val['pay_due_dt'];
								$late_pay_dt[]=$val['late_pay_dt'];
								$after_pay_dt[]=$val['after_pay_dt'];
								$bill_amount[]=$val['bill_amount'];
								$bill_amount_adjs[]=$val['bill_amount_adjs'];
								$bill_amount_perbill[]=$val['bill_amount_perbill']; 
								$late_amount[]=$val['late_amount'];
								$after_amount[]=$val['after_amount'];
								$period_from[]=$val['period_from'];
								$period_to[]=$val['period_to'];
								$bill_details[]=$val['bill_details'];							
								
								$val['validate_paid_amt']=0;
								if($val['amount']==0){
									$val['validate_paid_amt'] = 1 ;								
								}							
								$validate_paid_amt[] = $val['validate_paid_amt'];
							} 
							//$_ALL_POST['bill_id']=$bill;
							$_ALL_POST['bill_id1']=$bill;							
							$_ALL_POST['gl_type_name']=$gl_type_name;
							$_ALL_POST['gl_type']=$gl_type;
							$_ALL_POST['currency']=$currency;
							$_ALL_POST['bill_exchange_rate']=$bill_exchange_rate;
							$_ALL_POST['bill_attachment']=$bill_attachment;
							$_ALL_POST['billamount']=$billamount;
							$_ALL_POST['bamount']=$bamount;
							$_ALL_POST['number']=$number;
							$_ALL_POST['bill_dt']=$bill_dt;
							$_ALL_POST['bill_no']=$bill_no;
							$_ALL_POST['pay_due_dt']=$pay_due_dt;
							$_ALL_POST['late_pay_dt']=$late_pay_dt;
							$_ALL_POST['after_pay_dt']=$after_pay_dt;
							$_ALL_POST['bill_amount']=$bill_amount;
							$_ALL_POST['bill_amount_adjs']=$bill_amount_adjs;
							$_ALL_POST['bill_amount_perbill']=$bill_amount_perbill;
							$_ALL_POST['late_amount']=$late_amount;
							$_ALL_POST['after_amount']=$after_amount;
							$_ALL_POST['period_from']=$period_from;
							$_ALL_POST['period_to']=$period_to;
							$_ALL_POST['bill_details']=$bill_details;
							$_ALL_POST['validate_paid_amt']=$validate_paid_amt;
						}
					}
				}elseif($totalBalance==0){
					if(!empty($party_id)){									 
						$fields = TABLE_PAYMENT_PARTY_BILLS.".number,".TABLE_PAYMENT_PARTY_BILLS.".id,";
						$fields .= TABLE_PAYMENT_PARTY_BILLS.".balance as bamount,".TABLE_PAYMENT_PARTY_BILLS.".amount,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs, ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_dt, ".TABLE_PAYMENT_PARTY_BILLS.".pay_due_dt,
						".TABLE_PAYMENT_PARTY_BILLS.".late_pay_dt ,".TABLE_PAYMENT_PARTY_BILLS.".after_pay_dt ,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".bill_amount,
						".TABLE_PAYMENT_PARTY_BILLS.".late_amount,".TABLE_PAYMENT_PARTY_BILLS.".after_amount,
						".TABLE_PAYMENT_PARTY_BILLS.".period_from,".TABLE_PAYMENT_PARTY_BILLS.".period_to,
						".TABLE_PAYMENT_PARTY_BILLS.".gl_type_name,".TABLE_PAYMENT_PARTY_BILLS.".gl_type,
						".TABLE_PAYMENT_PARTY_BILLS.".exchange_rate,".TABLE_PAYMENT_PARTY_BILLS.".bill_attachment,
						".TABLE_PAYMENT_PARTY_BILLS.".currency_name,".TABLE_PAYMENT_PARTY_BILLS.".currency_id,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_details ";
						
						//$sub_query ='';
						 
						$sub_query = " AND ".TABLE_PAYMENT_PARTY_BILLS.".number in (5474,5470,5469,5468,5467,5142,5141,5140,5139,5135,5134,5133,5132,5131,
					5130,5129,5128,5127,5126,5125,5124,5123,5122,5121,5120,5119,5138,5137,5136)";				
						 
						$condition_query = ' WHERE ('.TABLE_PAYMENT_PARTY_BILLS.".bill_status
						='".PaymentPartyBills::BILL_STATUS_PARTIALPAID."' OR 
						".TABLE_PAYMENT_PARTY_BILLS.".bill_status='".PaymentPartyBills::BILL_STATUS_UNPAID."')
						AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt > '".$proper_dt."'
						AND ".TABLE_PAYMENT_PARTY_BILLS.".currency_id ='".$currency_id."'
						AND ".TABLE_PAYMENT_PARTY_BILLS.".company_id IN('".$company_id."','".$behalf_company_id."')
						AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs=0
						AND ".TABLE_PAYMENT_PARTY_BILLS.".f_status ='0'
						AND ".TABLE_PAYMENT_PARTY_BILLS.".status = '".PaymentPartyBills::ACTIVE."'
						AND (".TABLE_PAYMENT_PARTY_BILLS.".party_id ='".$party_id."'" .$behalf_sql.") ".$sub_query."
						ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".id ASC";
						 
					}elseif(!empty($executive_id)){				
						$fields = TABLE_PAYMENT_PARTY_BILLS.".number,".TABLE_PAYMENT_PARTY_BILLS.".id,";
						$fields .= TABLE_PAYMENT_PARTY_BILLS.".balance as bamount,".TABLE_PAYMENT_PARTY_BILLS.".amount,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs, ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_dt, ".TABLE_PAYMENT_PARTY_BILLS.".pay_due_dt,
						".TABLE_PAYMENT_PARTY_BILLS.".late_pay_dt,".TABLE_PAYMENT_PARTY_BILLS.".after_pay_dt, 
						".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".bill_amount, 
						".TABLE_PAYMENT_PARTY_BILLS.".late_amount, ".TABLE_PAYMENT_PARTY_BILLS.".after_amount, 
						".TABLE_PAYMENT_PARTY_BILLS.".period_from, ".TABLE_PAYMENT_PARTY_BILLS.".period_to, 
						".TABLE_PAYMENT_PARTY_BILLS.".gl_type_name,".TABLE_PAYMENT_PARTY_BILLS.".gl_type,
						".TABLE_PAYMENT_PARTY_BILLS.".exchange_rate,".TABLE_PAYMENT_PARTY_BILLS.".bill_attachment,
						".TABLE_PAYMENT_PARTY_BILLS.".currency_name,".TABLE_PAYMENT_PARTY_BILLS.".currency_id,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_details";
						
						$condition_query = ' WHERE ('.TABLE_PAYMENT_PARTY_BILLS.".bill_status=
						'".PaymentPartyBills::BILL_STATUS_PARTIALPAID."' OR 
						".TABLE_PAYMENT_PARTY_BILLS.".bill_status='".PaymentPartyBills::BILL_STATUS_UNPAID."')
						AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt > '".$proper_dt."'
						AND (".TABLE_PAYMENT_PARTY_BILLS.".executive_id ='".$executive_id."' ".$behalf_sql.")
						AND ".TABLE_PAYMENT_PARTY_BILLS.".currency_id ='".$currency_id."'
						AND ".TABLE_PAYMENT_PARTY_BILLS.".company_id IN('".$company_id."','".$behalf_company_id."')
						AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs=0
						AND ".TABLE_PAYMENT_PARTY_BILLS.".f_status ='0'
						AND ".TABLE_PAYMENT_PARTY_BILLS.".status = '".PaymentPartyBills::ACTIVE."'
						ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".id ASC";
					}if(!empty($vendor_bank_id)){
						$fields = TABLE_PAYMENT_PARTY_BILLS.".number,".TABLE_PAYMENT_PARTY_BILLS.".id,";
						$fields .= TABLE_PAYMENT_PARTY_BILLS.".balance as bamount,".TABLE_PAYMENT_PARTY_BILLS.".amount,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs, ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_dt, ".TABLE_PAYMENT_PARTY_BILLS.".pay_due_dt,
						".TABLE_PAYMENT_PARTY_BILLS.".late_pay_dt ,".TABLE_PAYMENT_PARTY_BILLS.".after_pay_dt ,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".bill_amount,
						".TABLE_PAYMENT_PARTY_BILLS.".late_amount,".TABLE_PAYMENT_PARTY_BILLS.".after_amount,
						".TABLE_PAYMENT_PARTY_BILLS.".period_from,".TABLE_PAYMENT_PARTY_BILLS.".period_to,
						".TABLE_PAYMENT_PARTY_BILLS.".gl_type_name,".TABLE_PAYMENT_PARTY_BILLS.".gl_type,
						".TABLE_PAYMENT_PARTY_BILLS.".exchange_rate,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_attachment,
						".TABLE_PAYMENT_PARTY_BILLS.".currency_name,".TABLE_PAYMENT_PARTY_BILLS.".currency_id,
						".TABLE_PAYMENT_PARTY_BILLS.".bill_details ";
						
						$condition_query = ' WHERE ('.TABLE_PAYMENT_PARTY_BILLS.".bill_status
						='".PaymentPartyBills::BILL_STATUS_PARTIALPAID."' OR 
						".TABLE_PAYMENT_PARTY_BILLS.".bill_status='".PaymentPartyBills::BILL_STATUS_UNPAID."')
						AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt > '".$proper_dt."'
						AND ".TABLE_PAYMENT_PARTY_BILLS.".currency_id ='".$currency_id."'
						AND ".TABLE_PAYMENT_PARTY_BILLS.".company_id IN('".$company_id."','".$behalf_company_id."')
						AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs=0
						AND ".TABLE_PAYMENT_PARTY_BILLS.".f_status ='0'
						AND ".TABLE_PAYMENT_PARTY_BILLS.".status = '".PaymentPartyBills::ACTIVE."'
						AND (".TABLE_PAYMENT_PARTY_BILLS.".vendor_bank_id ='".$vendor_bank_id."' 
						".$behalf_sql." ) ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".id ASC ";						
					}
					
					$condition_query = $condition_query." LIMIT 0,30 ";
					 
					if ( Paymenttransaction::getBillList($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
					  
						if(!empty($_ALL_POST)){
							$bill =$number= array();
							$bamount = array();
							foreach($_ALL_POST as $key=>$val){
								$bill[]=$val['id'];
								$gl_type_name[]=$val['gl_type_name'];
								$gl_type[]=$val['gl_type'];
								$currency[]=$val['currency_name'];
								$bill_exchange_rate[]=$val['exchange_rate'];
								$currencyid[]=$val['currency_id'];
								$bill_attachment[]=$val['bill_attachment'];
								$bamount[]=$val['bamount'];
								$number[]=$val['number'];
								$billamount[]=$val['amount'];
								$bill_no[]=$val['bill_no'];
								$bill_dt[]=$val['bill_dt'];
								$pay_due_dt[]=$val['pay_due_dt'];
								$late_pay_dt[]=$val['late_pay_dt'];
								$after_pay_dt[]=$val['after_pay_dt'];
								$bill_amount[]=$val['bill_amount'];
								$bill_amount_adjs[]=$val['bill_amount_adjs'];
								$bill_amount_perbill[]=$val['bill_amount_perbill']; 
								$late_amount[]=$val['late_amount'];
								$after_amount[]=$val['after_amount'];
								$period_from[]=$val['period_from'];
								$period_to[]=$val['period_to'];
								$bill_details[]=$val['bill_details'];							
								
								$val['validate_paid_amt']=0;
								if($val['amount']==0){
									$val['validate_paid_amt'] = 1 ;								
								}							
								$validate_paid_amt[] = $val['validate_paid_amt'];
							} 
							//$_ALL_POST['bill_id']=$bill;
							$_ALL_POST['bill_id1']=$bill;
							$_ALL_POST['gl_type_name']=$gl_type_name;
							$_ALL_POST['gl_type']=$gl_type;
							$_ALL_POST['currency']=$currency;
							$_ALL_POST['bill_exchange_rate']=$bill_exchange_rate;
							$_ALL_POST['bill_attachment']=$bill_attachment;
							$_ALL_POST['billamount']=$billamount;
							$_ALL_POST['bamount']=$bamount;
							$_ALL_POST['number']=$number;
							$_ALL_POST['bill_dt']=$bill_dt;
							$_ALL_POST['bill_no']=$bill_no;
							$_ALL_POST['pay_due_dt']=$pay_due_dt;
							$_ALL_POST['late_pay_dt']=$late_pay_dt;
							$_ALL_POST['after_pay_dt']=$after_pay_dt;
							$_ALL_POST['bill_amount']=$bill_amount;
							$_ALL_POST['bill_amount_adjs']=$bill_amount_adjs;
							$_ALL_POST['bill_amount_perbill']=$bill_amount_perbill;
							$_ALL_POST['late_amount']=$late_amount;
							$_ALL_POST['after_amount']=$after_amount;
							$_ALL_POST['period_from']=$period_from;
							$_ALL_POST['period_to']=$period_to;
							$_ALL_POST['bill_details']=$bill_details;
							$_ALL_POST['validate_paid_amt']=$validate_paid_amt;
						}
					}
				
				
				}
            }
        }
        
        $fields = TABLE_PAYMENT_TRANSACTION_BILLS.".*,".TABLE_PAYMENT_PARTY_BILLS.".number,
		".TABLE_PAYMENT_PARTY_BILLS.".balance, ";
        $fields .=  TABLE_PAYMENT_PARTY_BILLS.".amount as paidamount,".TABLE_PAYMENT_PARTY_BILLS.".bill_dt,
			".TABLE_PAYMENT_PARTY_BILLS.".pay_due_dt,".TABLE_PAYMENT_PARTY_BILLS.".late_pay_dt ,
			".TABLE_PAYMENT_PARTY_BILLS.".after_pay_dt , ".TABLE_PAYMENT_PARTY_BILLS.".bill_no,
			".TABLE_PAYMENT_PARTY_BILLS.".gl_type_name,
			".TABLE_PAYMENT_PARTY_BILLS.".gl_type,
			".TABLE_PAYMENT_PARTY_BILLS.".bill_amount,
			".TABLE_PAYMENT_PARTY_BILLS.".exchange_rate as bill_exchange_rate,
			".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs,
			".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill,
			".TABLE_PAYMENT_PARTY_BILLS.".late_amount,".TABLE_PAYMENT_PARTY_BILLS.".after_amount,
			".TABLE_PAYMENT_PARTY_BILLS.".bill_attachment,
			".TABLE_PAYMENT_PARTY_BILLS.".period_from,".TABLE_PAYMENT_PARTY_BILLS.".period_to,
			".TABLE_PAYMENT_PARTY_BILLS.".bill_details";
        $condition_query = " INNER JOIN  ".TABLE_PAYMENT_PARTY_BILLS." ON 
		".TABLE_PAYMENT_PARTY_BILLS.".id= ".TABLE_PAYMENT_TRANSACTION_BILLS.".bill_id WHERE transaction_id='".$transaction_id."'
		ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".id DESC";
        if( Paymenttransaction::getBillTransactionList($db, $list, $fields, $condition_query) > 0 ){
        
        }
	
		
		 
		
		 
        
        $hidden[] = array('name'=> 'perform' ,'value' => 'allot_bills');
        $hidden[] = array('name'=> 'exchange_rate' , 'value' => $exchange_rate);    
        $hidden[] = array('name'=> 'transaction_id' , 'value' => $transaction_id);    
        $hidden[] = array('name'=> 'transaction_number' , 'value' => $transaction_number);    
        $hidden[] = array('name'=> 'pamount' , 'value' => $pamount);    
        $hidden[] = array('name'=> 'totalBalance' , 'value' => $totalBalance);    
        $hidden[] = array('name'=> 'party_id' , 'value' => $party_id);    
		$hidden[] = array('name'=> 'company_id' , 'value' => $company_id);    
        $hidden[] = array('name'=> 'executive_id' , 'value' => $executive_id);    
        $hidden[] = array('name'=> 'vendor_bank_id' , 'value' => $vendor_bank_id);    
        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
        
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        $page["var"][] = array('variable' => 'traDetails', 'value' => 'traDetails');
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'exchange_rate', 'value' => 'exchange_rate');
        $page["var"][] = array('variable' => 'transaction_id', 'value' => 'transaction_id');
        $page["var"][] = array('variable' => 'transaction_number', 'value' => 'transaction_number');
        $page["var"][] = array('variable' => 'pamount', 'value' => 'pamount');
        $page["var"][] = array('variable' => 'totalAmt', 'value' => 'totalAmt');
        $page["var"][] = array('variable' => 'totalBalance', 'value' => 'totalBalance');
        $page["var"][] = array('variable' => 'party_id', 'value' => 'party_id');
        $page["var"][] = array('variable' => 'executive_id', 'value' => 'executive_id');
        $page["var"][] = array('variable' => 'vendor_bank_id', 'value' => 'vendor_bank_id');
        $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-allot-bills.html');            
        
    } else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
        
    }
?>
