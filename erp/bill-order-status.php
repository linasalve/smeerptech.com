<?php
	if ( $perm->has('nc_bl_or_status') ) {
        $or_id	= isset($_GET["or_id"])  ? $_GET["or_id"] 	: ( isset($_POST["or_id"]) 	? $_POST["or_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        if ( $perm->has('nc_bl_or_status_al') ) {
            $access_level += 1;
        }
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                    );
        Order::updateStatus($or_id, $status, $extra);
        
        // Display the  list.
		$perform = 'search';
        $searchStr = 1;
        $action = "Orders List";		 
		if(!empty($or_id)){
			 $condition_query = " WHERE ".TABLE_BILL_ORDERS.".id='".$or_id."'";
		}
        // Display the  list.
        include ( DIR_FS_NC .'/bill-order-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>