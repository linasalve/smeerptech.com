<?php

if ( $perm->has('nc_rp_mkt_ex') ) {

	    include_once ( DIR_FS_INCLUDES .'/leads-order.inc.php');     
		include_once ( DIR_FS_INCLUDES .'/leads-quotation.inc.php');	
		include_once( DIR_FS_CLASS .'/RegionLead.class.php');
        include_once( DIR_FS_CLASS .'/PhoneLead.class.php');
         //to view report member id should be compulsary
        $executive	= isset($_GET["executive"]) 	? $_GET["executive"]	: ( isset($_POST["executive"]) 	? $_POST["executive"] : '' );
        $executive_details	= isset($_GET["executive_details"]) 	? $_GET["executive_details"]	: ( isset($_POST["executive_details"]) 	? $_POST["executive_details"] : '' );
            
        $where_added    = true;
        $allData = $_GET 	? $_GET	: ($_POST 	? $_POST : '' );
        $extra_url=$condition_query=$condition_url=$pagination ='';
        
		// Read the Date data.
		$chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
		$date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
		$chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
		$date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
		$dt_field       = isset($_POST["dt_field"])   ? $_POST["dt_field"]      : (isset($_GET["dt_field"]) ? $_GET["dt_field"]:'');
		$sStatus       = isset($_POST["sStatus"])   ? $_POST["sStatus"]      : (isset($_GET["sStatus"]) ? $_GET["sStatus"]:'1');
		

		$_SEARCH["sStatus"] 	= $sStatus;       
		$_SEARCH["executive"] 	= $executive;       
        $_SEARCH["executive_details"] 	= $executive_details; 
		$variables['status'] = LeadsOrder::getStatus();
		
		include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
		$fieldsu=TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".number,".TABLE_USER.".user_id" ;
		$ustr= str_replace(",","','",trim($my['my_reporting_members'],","));
	    $condition_query_u  = " WHERE ".TABLE_USER.".status='".User::ACTIVE."' AND ".TABLE_USER.".user_id IN('".$ustr."','".$my['uid']."')" ;
		User::getList( $db, $ulist, $fieldsu, $condition_query_u);
            
        if(array_key_exists('submit_search',$allData)){
        
            if(!empty($executive) && !empty($chk_date_from) && !empty($date_from) 
				&& !empty($chk_date_to) && !empty($date_to) ){        
                
				if(!empty($dt_field)){					
					$condition_url      .= "&dt_field=$dt_field" ;
					$_SEARCH["dt_field"] = $dt_field ;
				}
				
                // BO: From Date
                if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from)) {
                    
                    $dfa = explode('/', $date_from);
                    $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
					
					if($dt_field=='do_add'){
						$condition_query1 .= " AND ". TABLE_SALE_LEADS .".do_add >= '". $dfa ."' ";
					
					}elseif($dt_field=='do_c'){
						$condition_query1 .= " AND ". TABLE_LEADS_ORDERS .".do_c >= '". $dfa ."' ";					
					}
					
                    $condition_url   .= "&chk_date_from=$chk_date_from&date_from=$date_from";
                    $_SEARCH["chk_date_from"]   = $chk_date_from;
                    $_SEARCH["date_from"]       = $date_from;
                }                
                // EO: From Date
                
                // BO: Upto Date
                if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)){
                    
                    $dta = explode('/', $date_to);
                    $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';                   
                     
					if($dt_field=='do_add'){
						$condition_query1 .= " AND ". TABLE_SALE_LEADS .".do_add <= '". $dta ."' ";
					
					}elseif($dt_field=='do_c'){
						$condition_query1 .= " AND ". TABLE_LEADS_ORDERS .".do_c <= '". $dta ."' ";					
					}
					
                    $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
                    $_SEARCH["chk_date_to"] = $chk_date_to ;
                    $_SEARCH["date_to"]     = $date_to ;
                }
				
				
				
				
				if(isset($sStatus)){
				
					$condition_query1 .= " AND ". TABLE_LEADS_ORDERS .".status = '". $sStatus ."' ";	
					$condition_url .= "&sStatus=$sStatus";
					$_SEARCH["sStatus"] = $sStatus ;
				}
                // EO: Upto Date    
				/*
				
				$condition_query = " WHERE ".TABLE_USER.".user_id IN ('".$executive."')";				
			    $list	= 	NULL;
				$total	=	User::getList($db, $list, '', $condition_query);
			
				$extra_url  = '';
				if ( isset($condition_url) && !empty($condition_url) ) {
					$extra_url  = $condition_url;
				}
				$condition_url .="&perform=".$perform;            
				$extra_url  .= "&x=$x&rpp=$rpp";
				$extra_url  = '&start=url'. $extra_url .'&end=url';
				$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');			
				$list	= NULL;
				$fields=TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".number, ".TABLE_USER.".user_id " ;				
				User::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
                */
                
              
                $extra_url  .= "&executive=$executive&executive_details=$executive_details&submit_search=1&perform=".$perform;
                $extra_url  = '&start=url'.$extra_url.'&end=url';
            
				if ( isset($condition_url) && !empty($condition_url) ) {
					$extra_url  .= $condition_url;
				}
				
				//Datewise Leads Details Along With The Leads Reporting BOF
				
				$sql = "SELECT DISTINCT (".TABLE_SALE_LEADS.".lead_id) as lead_id," 
					.TABLE_SALE_LEADS.".do_add,".TABLE_SALE_LEADS.".company_name,	"
					.TABLE_SALE_LEADS.".lead_industry_id,					
					".TABLE_SALE_LEADS.".f_name, ".TABLE_SALE_LEADS.".l_name, ".TABLE_SALE_LEADS.".email,".TABLE_SALE_LEADS.".email_1, 
					".TABLE_SALE_LEADS.".email_2,".TABLE_SALE_LEADS.".email_3, ".TABLE_SALE_LEADS.".email_4, ".TABLE_SALE_LEADS.".mobile1, 
					".TABLE_SALE_LEADS.".mobile2, ".TABLE_SALE_LEADS.".mobile3, ".TABLE_SALE_LEADS.".annual_revenue, 
					".TABLE_SALE_LEADS.".no_emp, ".TABLE_SALE_LEADS.".kind_of_person , ".TABLE_LEADS_ORDERS.".ref_website, 
					".TABLE_LEADS_ORDERS.".color_likes, ".TABLE_LEADS_ORDERS.".color_dislikes, 
					".TABLE_LEADS_ORDERS.".requirements, 
					".TABLE_LEADS_ORDERS.".total_expenses, 
					".TABLE_LEADS_ORDERS.".total_visits, 
					".TABLE_LEADS_ORDERS.".total_calls, 
					".TABLE_LEADS_ORDERS.".total_km, 
					".TABLE_LEADS_ORDERS.".competitor_quote, 
					".TABLE_LEADS_ORDERS.".competitor_details, 
					".TABLE_LEADS_ORDERS.".prospect_budget, 
					".TABLE_LEADS_ORDERS.".prospect_can_spent_amt, 
					".TABLE_LEADS_ORDERS.".target_service, 
					".TABLE_LEADS_ORDERS.".amount,
					".TABLE_LEADS_ORDERS.".status,
					".TABLE_LEADS_ORDERS.".id as lead_ord_id,
					".TABLE_LEADS_ORDERS.".number
				FROM ".TABLE_SALE_LEADS." JOIN ".TABLE_LEADS_ORDERS." ON (".TABLE_LEADS_ORDERS.".client = ".TABLE_SALE_LEADS.".lead_id )";
				$condition_query = " WHERE ( ". TABLE_LEADS_ORDERS .".team LIKE '%,". $executive .",%' ". "  "."  ) "; 
				
				//Datewise Leads Details Along With The Leads Reporting EOF
				$query = $sql.$condition_query.$condition_query1 ;
				$order_by = " ORDER BY ". TABLE_LEADS_ORDERS .".do_e DESC ";
				$query  .=  $order_by ;
				$db->query($query);  
				$total = $db->nf();    
				$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $extra_url, 'changePageWithExtra');			
		        $query .= " LIMIT ". $next_record .", ". $rpp;
				$db->query($query);
				$db1 		= new db_local;
				$r_list	= 	NULL;
				$regionlead     = new RegionLead('91');
				$phonelead      = new PhoneLead(TABLE_SALE_LEADS);
        
				if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
					
						$lead_ord_id =$db->f('lead_ord_id');
						$lead_id =$db->f('lead_id');
						$status =$db->f('status');
						$do_add =$db->f('do_add');
						$company_name =$db->f('company_name');
						$f_name =$db->f('f_name');
						$l_name =$db->f('l_name');
						$email =$db->f('email');
						$email_1 =$db->f('email_1');
						$email_2 = $db->f('email_2');
						$email_3 = $db->f('email_3');
						$email_4 = $db->f('email_4');
						$mobile1 = $db->f('mobile1');
						$mobile2 = $db->f('mobile2');
						$mobile3 = $db->f('mobile3');
						$annual_revenue = $db->f('annual_revenue');
						$no_emp =$db->f('no_emp');
						$kind_of_person =$db->f('kind_of_person');
						$ref_website =$db->f('ref_website');
						$color_likes =$db->f('color_likes');
						$color_dislikes =$db->f('color_dislikes');
						$requirements =$db->f('requirements');
						$total_expenses =$db->f('total_expenses');
						$total_visits = $db->f('total_visits');
						$total_calls = $db->f('total_calls');
						$total_km = $db->f('total_km');
						$reason_of_delete = $db->f('reason_of_delete');
						$amount = number_format($db->f('amount'),2);
						$number =$db->f('number');
						$status =$db->f('status');
						$competitor_quote = $db->f('competitor_quote');
						$competitor_details = $db->f('competitor_details');
						$prospect_budget = $db->f('prospect_budget');
						$prospect_can_spent_amt = $db->f('prospect_can_spent_amt');
						$target_service = $db->f('target_service');							
						$lead_industry_id = $db->f('lead_industry_id');
						//Get industry name BOF
						$lead_industry='';
						$sql = " SELECT title FROM ".TABLE_SALE_INDUSTRY." WHERE id =".$lead_industry_id;
						$db1->query($sql);
						if ( $db1->nf()>0 ) {
							while ( $db1->next_record() ) { 
								$lead_industry = $db1->f('title');
							}
						}
						//Get industry name EOF
						$phone = $address_list = $followup = array();
						$phonelead->setPhoneOf(TABLE_SALE_LEADS,$lead_id);
						$phone = $phonelead->get($db1);
						//Read the Addresses.
						$regionlead->setAddressOf(TABLE_SALE_LEADS, $lead_id);
						$address_list = $regionlead->get();
						$listLog=array();
						$condition_query_2 = " WHERE ".TABLE_LEADS_ORD_FLW.".lead_id = '".$lead_ord_id."' " ;
						$condition_query_2 .= " ORDER BY ".TABLE_LEADS_ORD_FLW.".id DESC";
						LeadsOrder::getFollowupLog( $db1, $listLog, "*", $condition_query_2, 0,10);
						 
						$list[] = array(
							'lead_id'=>$lead_id,
							'do_add'=>$do_add,
							'company_name'=>$company_name,
							'f_name'=>$f_name,
							'l_name'=>$l_name,
							'email'=>$email,
							'email_1'=>$email_1,
							'email_2'=>$email_2,
							'email_3'=>$email_3,
							'email_4'=>$email_4,
							'mobile1'=>$mobile1,
							'mobile2'=>$mobile2,
							'mobile3'=>$mobile3,
							'phone'=>$phone,
							'address_list'=>$address_list,
							'annual_revenue'=>$annual_revenue,
							'no_emp'=>$no_emp,
							'kind_of_person'=>$kind_of_person,
							'ref_website'=>$ref_website,
							'color_likes'=>$color_likes,
							'color_dislikes'=>$color_dislikes,
							'requirements'=>$requirements,
							'total_expenses'=>$total_expenses,
							'total_visits'=>$total_visits,
							'total_calls'=>$total_calls,
							'total_km'=>$total_km,
							'number'=>$number,
							'amount'=>$amount,
							'status'=>$status,
							'competitor_quote'=>$competitor_quote,
							'prospect_budget'=>$prospect_budget,
							'prospect_can_spent_amt'=>$prospect_can_spent_amt,
							'target_service'=>$target_service, 			
							'lead_industry'=>$lead_industry,								
							'followup'=>$listLog 							
						);
					}
				}
			
			}else{
                $messages->setErrorMessage('Please select Executive and Date Duration, From date and To date.');
            }
        }   
            
   
     
    
    $variables['can_view_list_all'] = false;
    if ( $perm->has('nc_ld_or_list_all') ) {
		$variables['can_view_list_all'] = true;
	}
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
	$page["var"][] = array('variable' => 'ulist', 'value' => 'ulist');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    
    
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-marketing-executive-sales.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>