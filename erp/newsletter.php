<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/newsletter.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/newsletter-smtp.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/address-book.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
	
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $added 			= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $condition_url='';
    $condition_query1='';
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
    $condition_query ='';
   
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if($added){
         $messages->setOkMessage("Newsletter entry has been done.");
    }
    
    if ( $perm->has('nc_nwl') ) {

       
        //This is because we are displaying queue list in same forms
        if($perform=='qlist' || $perform=='qsearch'||$perform=='qdelete'){
        
                $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
                                    TABLE_NEWSLETTER_TEMP     => array(
                                                             "Newsletter Subject"=> "subject",
                                                               "Newsletter Message"   => "message"
                                                        )
                                );
        
                $sOrderByArray  = array(
                                         TABLE_NEWSLETTER_TEMP     => array(
                                                                    "Newsletter Subject"   => "subject"                                                      
                                                                  
                                                                )
                                    );
            
                // Set the sorting order of the user list.
                if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
                    $_SEARCH['sOrderBy']= $sOrderBy = 'send_on_date';
                    $_SEARCH['sOrder']  = $sOrder   = 'DESC';
                    $order_by_table     = TABLE_NEWSLETTER_TEMP;
                }
        
        }else{
        
             $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
                                    TABLE_NEWSLETTER     => array(
                                                            "Newsletter Name"   => "n_title",
                                                            "Newsletter Subject"=> "subject",
                                                            "Newsletter Body"   => "message"
                                                        )
                                );
        
            $sOrderByArray  = array(
                                    TABLE_NEWSLETTER     => array(
                                                            "Newsletter Name"   => "n_title",
                                                            "Newsletter Date"   => "do_e"
                                                        )
                                    );
    
            // Set the sorting order of the user list.
            if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
                $_SEARCH['sOrder']  = $sOrder   = 'DESC';
                $order_by_table     = TABLE_NEWSLETTER;
            }
        
        
        }
        
        //use switch case here to perform action. 
        switch ($perform) {
            
            case ('qlist'):
            {
                 
                include (DIR_FS_NC.'/newsletter-queue-list.php');
                //include(DIR_FS_NC."/newsletter-queue-search.php");
               
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('qsearch'): {
               // include (DIR_FS_NC.'/newsletter-queue-list.php');
                include(DIR_FS_NC."/newsletter-queue-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('qdelete'): {
                include ( DIR_FS_NC .'/newsletter-queue-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
             case ('add'): {
            
                include (DIR_FS_NC.'/newsletter-add.php');
                
                 $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('resend'): {
                include (DIR_FS_NC .'/newsletter-resend.php');
                
                  $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('delete'): {
                include ( DIR_FS_NC .'/newsletter-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list_clients'):{
               
                include (DIR_FS_NC .'/newsletter-clients-list.php');
                include (DIR_FS_NC .'/newsletter-clients-search.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter-clients-list.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
                break;
            }
             case ('list_address_book'):{
               
                include (DIR_FS_NC .'/newsletter-address-book-list.php');
                include (DIR_FS_NC .'/newsletter-address-book-search.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter-address-book-list.html');
                
                $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
                break;
            }
            case ('list_user'):{
               
                include (DIR_FS_NC .'/newsletter-user-list.php');
                include (DIR_FS_NC .'/newsletter-user-search.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter-user-list.html');
                
                $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
                break;
            }
           
            case ('view'): {
                include (DIR_FS_NC.'/newsletter-view.php');            
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/newsletter-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list'):
            default: {
                include (DIR_FS_NC.'/newsletter-list.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletter.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>