<?php
    if ( $perm->has('nc_bl_or_up_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( BillOrderUpload::validateUpdate($data, $extra) ) {
                $data['upload_path'] = addslashes($data['upload_path']);
                $query  = " UPDATE ". TABLE_BILL_ORD_UPLOAD
                           ." SET ". TABLE_BILL_ORD_UPLOAD .".order_id = '".        $data['order_id'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".upload_path = '".    $data['upload_path'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".details = '".        $data['details'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".type = '".             	$data['type'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".status = '". 		$data['status'] ."'"                            
                            ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Path Upload entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_BILL_ORD_UPLOAD .'.*'  ;            
            $condition_query = " WHERE (". TABLE_BILL_ORD_UPLOAD .".id = '". $id ."' )";
           /* $condition_query .= " AND ( ";
            // If my has created this record.
            $condition_query .= " (". TABLE_BILL_ORD_UPLOAD .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORD_UPLOAD .".access_level < $access_level ) ";              
            // Check if the User has the Right to Edit Orders created by other Users.
            
            if ( $perm->has('nc_e_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_e_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_BILL_ORD_UPLOAD. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORD_UPLOAD .".access_level < $access_level_o ) ";
            }
            $condition_query .= " )";*/
          
            if ( BillOrderUpload::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                $id         = $_ALL_POST['id'];
                // BO: Read the Order Information.
                if ( !empty($_ALL_POST['order_id']) && !is_array($_ALL_POST['order_id']) ) {
                    $_ALL_POST['order_id']  = $_ALL_POST['order_id'];
                    Order::getList($db, $_ALL_POST['order_details'], 'order_title', "WHERE id='".$_ALL_POST['order_id']."'");
                    $_ALL_POST['order_details'] = $_ALL_POST['order_details']['0']['order_title'];
                }
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/bill-order-upload-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-upload-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
