<?php

    if ( $perm->has('nc_ps_qt_details') ) {
        $inv_id = isset ($_GET['inv_id']) ? $_GET['inv_id'] : ( isset($_POST['inv_id'] ) ? $_POST['inv_id'] :'');
        $type   = isset ($_GET['type']) ? $_GET['type'] : ( isset($_POST['type'] ) ? $_POST['type'] :'');
        $filename   = isset ($_GET['filename']) ? $_GET['filename'] : ( isset($_POST['filename'] ) ? $_POST['filename'] :'');
        
        $condition_query= " WHERE ". TABLE_PROSPECTS_QUOTATION .".id = '". $inv_id ."'"
							." OR ". TABLE_PROSPECTS_QUOTATION .".number = '". $inv_id ."'";
        
 

        $list	= NULL;
        if ( ProspectsQuotation::getDetails( $db, $list, TABLE_PROSPECTS_QUOTATION .'.id, 
		'. TABLE_PROSPECTS_QUOTATION .'.filename,'. TABLE_PROSPECTS_QUOTATION .'.attach_file1,'. TABLE_PROSPECTS_QUOTATION .'.attach_file2,'. TABLE_PROSPECTS_QUOTATION .'.attach_file3,'. TABLE_PROSPECTS_QUOTATION .'.attach_file4,'. TABLE_PROSPECTS_QUOTATION .'.attach_file5', $condition_query) ) {
          
            if ( $type == 'html' ) {
                $file_path = DIR_FS_PST_QUOT_HTML ."/";
                $file_name = $list[0]["filename"] .".html";
                $content_type = 'text/html';
            }elseif ( $type == 'htmlprint' ) {
                $file_path = DIR_FS_PST_QUOT_HTMLPRINT ."/";
                $file_name = $list[0]["filename"] .".html";
                $content_type = 'text/html';
            }
            elseif ( $type == 'pdf' ) {
                $file_path = DIR_FS_PST_QUOT_PDF."/";
                $file_name = $list[0]["filename"] .".pdf";
                $content_type = 'application/pdf';
            }
			elseif ( $type == 'attach_file1' ) {
                $file_path = DIR_FS_PST_QUOT."/";
                $file_name = $filename;
                $content_type = '';
			 
            }
			elseif ( $type == 'attach_file2' ) {
                $file_path = DIR_FS_PST_QUOT."/";
               $file_name = $filename;
                $content_type = '';
				 
            }
			elseif ( $type == 'attach_file3' ) {
                $file_path = DIR_FS_PST_QUOT."/";
				$file_name = $filename;
                $content_type = '';
            }
			elseif ( $type == 'attach_file4' ) {
                $file_path = DIR_FS_PST_QUOT."/";
                $file_name = $filename;
                $content_type = '';
            }
			elseif ( $type == 'attach_file5' ) {
                $file_path = DIR_FS_PST_QUOT."/";
                $file_name = $filename;
                $content_type = '';
            } else {
                echo('<script language="javascript" type="text/javascript">alert("Invalid File specified");</script>');
            }
            
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-type: $content_type");
            header("Content-Disposition: attachment; filename=". $file_name );
            header("Content-Length: ".filesize($file_path.$file_name));
            readfile($file_path.$file_name);
        }
        else {
            echo('<script language="javascript" type="text/javascript">alert("The File is not found or you do not have the permission to view the file.");</script>');
        }
    }
    else {
        echo('<script language="javascript" type="text/javascript">alert("You do not have the permission to view the list.");</script>');
    }
    exit;
?>