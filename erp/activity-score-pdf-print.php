<?php
 
    if ( $perm->has('nc_as_print') ) {		
		
        //include_once ( DIR_FS_INCLUDES .'/score-sheet.inc.php');
		$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
        $date_from = isset($_GET['date_from']) ? $_GET['date_from'] : ( isset($_POST['date_from'] ) ? $_POST['date_from'] :'');
        $date_to = isset($_GET['date_to']) ? $_GET['date_to'] : ( isset($_POST['date_to'] ) ? $_POST['date_to'] :'');
        
        $list           = NULL;
        $_ALL_POST      = NULL;
         
		
        if ( !empty($user_id) && !empty($date_from) && !empty($date_to)){
			
			$dfa = explode('/', $date_from);
			$dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
			 
			$dta = explode('/', $date_to);
            $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
			
			$lst_executive = Null;
			$fields = TABLE_USER .'.user_id'
					   .','. TABLE_USER .'.f_name'
					   .','. TABLE_USER .'.l_name';
			$condition1 = " WHERE ".TABLE_USER.".user_id ='".$user_id."'";        
			User::getList($db,$lst_executive,$fields,$condition1);
			if(!empty($lst_executive)){
				$user_data = $lst_executive[0];
				$heading = "Score details of ".$user_data['f_name']." ".$user_data['l_name'].". For Period 
				 ".date('d M Y',strtotime($dfa))." - ".date('d M Y',strtotime($dta));
			}
			
             
			$condition_query = " WHERE comment_to LIKE ',%".$user_id."%,' AND 
			".TABLE_SCORE_SHEET .".date >= '". $dfa ."' AND ".TABLE_SCORE_SHEET .".date <= '". $dta ."'";
			$total	=	ActivityScore::getDetails( $db, $list, '', $condition_query);
			
			$per_page = 100;
			$pdfContent ='';
			$total_batches =  ceil($total/$per_page) ;
			$key1=0;	
			$row=3;
			if($total_batches >0){
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Disposition: attachment;filename=exported-list.xls ");
				header("Content-Transfer-Encoding: binary ");
				xlsBOF();
				$header = iconv("UTF-8","ISO-8859-9",$heading);
				xlsWriteLabel(0,0,$header);
				
				$headers="#|Particulars|Score|Score Dt|Score by|";
				$tab_header = split("\|",$headers);		
				$valueCount = count($tab_header);
				for ($z = 0; $z < $valueCount-1; $z++)
				{
					$tab_header[$z] = iconv("UTF-8","ISO-8859-9",$tab_header[$z]);
					xlsWriteLabel(2,$z,$tab_header[$z]);
				}
				 
			 
				
				
				for($i=0;$i<$total_batches;$i++){
				  $from = $i * $per_page;
				  $limit_sql= " LIMIT ".$from.", ".$per_page ;
				  $sql=" SELECT * FROM ".TABLE_SCORE_SHEET." ".$condition_query." ".$limit_sql;
				  $scoreDetails['create_pdf'] = 1;
				  $scoreDetails["number"] ="score" ;
				 
				  if ( $db->query($sql) ) {
					if ( $db->nf() > 0 ) {
						$scoreDetails['particulars']=array();
						while ($db->next_record()) {
							$key1++; 
						
						   $scoreDetails['particulars'][] =array(
							'from_name'=>$db->f('created_by_name'),
							'score_date'=>date('d M Y',strtotime($db->f('date'))),
							'particulars'=>$db->f('particulars'),
							'score'=>$db->f('score'),							
							'sr'=>$key1
						   );
						  					   
						     
						    $particulars = $db->f('particulars');
							$particulars = wordwrap($particulars, 20, "\n");
						    $xls_key1 = iconv("UTF-8","ISO-8859-9",$key1);
						    $xls_particulars = iconv("UTF-8","ISO-8859-9",$particulars);
						    $xls_score= iconv("UTF-8","ISO-8859-9",$db->f('score'));
						    $xls_date= iconv("UTF-8","ISO-8859-9",date('d M Y',strtotime($db->f('date'))));
						    $created_by_name= iconv("UTF-8","ISO-8859-9",$db->f('created_by_name'));
							xlsWriteLabel($row,0,$xls_key1);
							xlsWriteLabel($row,1,$xls_particulars);
							xlsWriteLabel($row,2,$xls_score);
							xlsWriteLabel($row,3,$xls_date);
							xlsWriteLabel($row,4,$created_by_name);
							$row++;
						}
					}
					//$score_data = ActivityScore::createPdfFile($scoreDetails);
					
				  }
				  //$score_data1 .= " "." ".$score_data ;
				  
				}
				xlsEOF();
/* 				$score_data	= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" >";
				$score_data .= "<tr>
					<td align=\"left\"> ";
		
				header("Content-Type: application/vnd.ms-word");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("content-disposition: attachment;filename=exported-list.doc"); 
		
				$output = "<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=iso-8859-1\">";
				print $output.$score_data1;
 */				
				
				/* 
				$scoreDetails['particulars']=array();
				$scoreDetails['create_pdf']=1 ;
				$scoreDetails['content'] = $pdfContent ; 
				$scoreDetails['header'] =$scoreDetails['footer']=''; */
				//ActivityScore::createPdfFile($scoreDetails);
				
				/* $file_path = DIR_FS_GN_FILES ."/";
				$file_name = "score.pdf";
				$content_type = 'application/pdf';
				header("Pragma: public"); // required
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false); // required for certain browsers 
				header("Content-type: $content_type");
				header("Content-Disposition: attachment; filename=". $file_name );
				header("Content-Length: ".filesize($file_path.$file_name));
				readfile($file_path.$file_name); */
				exit; 
				$page["var"][] = array('variable' => 'data', 'value' => 'data');

				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');  
            } 
        }
        else {
            $messages->setErrorMessage("The Executives details were not found. ");
        }  
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>