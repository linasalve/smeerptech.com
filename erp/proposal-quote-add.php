<?php
 if ( $perm->has('nc_pqt_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];		
		
		//Get Order Details EOF
		$total_amt = 0;
		$variables['showbidform']=1;
		$_ALL_POST_DETAILS=$list= array();
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        $condition1 = " WHERE ".TABLE_USER.".status='".User::ACTIVE."' 
		ORDER BY ".TABLE_USER.".f_name";    //AND ".TABLE_USER.".current_staff='1'     
        User::getList($db,$lst_executive,$fields,$condition1);
		//$_ALL_POST['transaction_type'] = Paymenttransaction::PAYMENTIN;
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
				$_ALL_POST 	= $_POST;
				$data		= processUserData($_ALL_POST);
           
				$extra = array( 'db' 				=> &$db,
                           	'messages'          => &$messages 
                        );
            
   
				if ( ProposalQuote::validateAdd($data,$extra) ) {
			
					
					$tot_projected_hour_salary=$tot_hrs =0;
                   
                        if(isset($data['team_member'])){
							  $temp_p = $bidDetails = NULL;	
							foreach ( $data['team_member'] as $key=>$particular ) {								
								if ( !empty($data['team_member'][$key]) || !empty($data['hrs'][$key]) ) {
									 
									$table = TABLE_USER;
									$condition2 = " WHERE ".TABLE_USER .".user_id = 
									'".$data['team_member'][$key] ."' LIMIT 0,1 " ;
									$fields1 =  TABLE_USER.'.current_salary,'. TABLE_USER.'.f_name,'.
									TABLE_USER .'.l_name';
									$exeCreted = getRecord($table,$fields1,$condition2);                
									$currentSalary = $data['current_salary'][$key] = $exeCreted['current_salary'] ;				
									$data['name'][$key] = $exeCreted['f_name']." ".$exeCreted['l_name'] ;
									$data['per_hour_salary'][$key] = round($currentSalary / STD_WORKING_HRS);
								 
								 
									$data['projected_hour_salary'][$key] = ( $data['per_hour_salary'][$key] * 
									$data['hrs'][$key]);									  
									$data['hrs1'][$key]  = ( 3 * $data['hrs'][$key] );
									
									$bidDetails[$key]['projected_hour_salary']	=  
									$data['projected_hour_salary'][$key] * SALARY_FACTOR;
									
									$tot_projected_hour_salary =  $tot_projected_hour_salary + 
									$bidDetails[$key]['projected_hour_salary'];
									$tot_hrs = $tot_hrs + $data['hrs'][$key]	; 
									$bidDetails[$key]['hrs']  = $data['hrs'][$key];
									$bidDetails[$key]['team_member_name']  =$data['name'][$key];
									$bidDetails[$key]['remarks'] = $data['remarks'][$key] ;
									
								}
								
							}           
							$created_by='team';							
							$list[$created_by][] =  array("bid_details" =>$bidDetails,
														"tot_projected_hour_salary" =>$tot_projected_hour_salary,
														"tot_hrs" =>$tot_hrs 
																		
															);
						}		
                    
					$_ALL_POST=null;
					 
                $_ALL_POST['team_member']=array('0'=>'');
            }
                
        }else{
			$_ALL_POST['team_member']=array('0'=>'');
		}
		 
    
        
           
		
		
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'ajx' ,'value' => $ajx);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
  
            $page["var"][] = array('variable' => '_ALL_POST_DETAILS', 'value' => '_ALL_POST_DETAILS');      
            $page["var"][] = array('variable' => 'list', 'value' => 'list');      
            $page["var"][] = array('variable' => 'total_amt', 'value' => 'total_amt');      
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'proposal-quote-add.html');
        
    }
    else {
         $messages->setErrorMessage("You donot have the Permisson to Access this module.");
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
    }
?>
