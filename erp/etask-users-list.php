<?php

    if ( $perm->has('nc_etask_usr_list') ) {
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }

		if(@$condition_query == "")
			$cndtn = " WHERE ".TABLE_ETASK_USER.".parent_id = '0'";
		else
			$cndtn = " AND ".TABLE_ETASK_USER.".parent_id = '0'";	
	
		@$condition_query .= $cndtn;
		$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        // To count total records.
        $list	= 	NULL;
        $total	=	EtaskUser::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        EtaskUser::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_delete']        = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_etask_usr_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_etask_usr_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_etask_usr_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_etask_usr_status') ) {
            $variables['can_change_status']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'etask-users-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>