<?php
    
    if ( $perm->has('nc_inward_details') ) {
        $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');

        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        

        // Read the record details
        $fields = TABLE_IN_WARD .'.*'  ;            
        $condition_query = " WHERE (". TABLE_IN_WARD .".id = '". $id ."' )";
        
        $typeArr = Inward::getTypes();
        $typeArr = array_flip($typeArr);
        $ctypeArr = Inward::getContentTypes();

        $fields = TABLE_IN_WARD .'.*'  ;
        if ( Inward::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];
            
            $content_type = $_ALL_POST['content_type'];
            $type = $_ALL_POST['type'];
            $typename =$typeArr[$type];
            if($content_type!=0){
                $content_type = $ctypeArr[$content_type];
            }else{
                $content_type='';
            }
            $_ALL_POST['type'] = $typename ;                
            $_ALL_POST['content_type'] = $content_type ; 
            
            
            
            //if ( $_ALL_POST['access_level'] < $access_level ) {
                //include ( DIR_FS_INCLUDES .'/user.inc.php');
                // Read the Order Creator's Information.
                $_ALL_POST['creator']= '';
                User::getList($db, $_ALL_POST['creator'], 'user_id,username,number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                $_ALL_POST['creator'] = $_ALL_POST['creator'][0];
                $_ALL_POST['access_level'] = getAccessLevel($db, $_ALL_POST['access_level'], true);
                $_ALL_POST['access_level'] = $_ALL_POST['access_level'][0];
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'inward-view.html');
            /*}
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Inward details with the current Access Level.");
            }*/
        }
        else {
            $messages->setErrorMessage("Records not found to view.");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
