<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
	include_once (DIR_FS_INCLUDES .'/project-status.inc.php');
    include_once (DIR_FS_INCLUDES .'/clients.inc.php');
    
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$or_id 		= isset($_GET["or_id"])     ? $_GET["or_id"]        : ( isset($_POST["or_id"])          ? $_POST["or_id"]       :'');
	$id 		= isset($_GET["id"])        ? $_GET["id"]        : ( isset($_POST["id"])          ? $_POST["id"]       :'');
	$task_id 	= isset($_GET["task_id"])   ? $_GET["task_id"]        : ( isset($_POST["task_id"])          ? $_POST["task_id"]       :'');
    $added 		= isset($_GET["added"])     ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	//$xd 		= isset($_GET["xd"])        ? $_GET["xd"]        : ( isset($_POST["xd"])          ? $_POST["xd"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $actual_time 	= isset($_GET["actual_time"])       ? $_GET["actual_time"]      : ( isset($_POST["actual_time"])        ? $_POST["actual_time"]     : 1);// ie by default every one can show actual value timesteet values
 
 
    
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
       
    }
    $condition_query =$condition_url ='';
   
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    
    /*
    if ( empty($xd) ) {
        $xd              = 1;
        $next_recordd    = 0 ;
    }
    else {
        $next_recordd    = ($xd-1) * $rpp;
    }*/
    
    $variables["x"]     = $x;
    //$variables["xd"]     = $xd;
    $variables["rpp"]   = $rpp;
    
    
    if($added){    
         $messages->setOkMessage("New Bug entry has been done.");
    }
    

    //get order details BOF
    $order = NULL;
    if ( Order::getList($db, $order, 'team, order_title,id as order_id,client', " WHERE id = '$or_id'" ) > 0 ) {
        include_once ( DIR_FS_INCLUDES .'/user.inc.php');
        
       
        $order = $order[0];
        $order['team'] = "'". implode("','", (explode(',', $order['team']))) ."'";
        $order['team_members']= '';
        if ( User::getList($db, $team_members, 'user_id, number, f_name, l_name, email', "WHERE user_id IN (". $order['team'] .")") > 0 ) {
           
        }
        
    }       
    //get order details EOF
    
    
    //get task details BOF
    $task_details = null ; 
    $condition_queryt = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".id='".$task_id."'" ;
    $fieldst = TABLE_PROJECT_TASK_DETAILS.".id, ".TABLE_PROJECT_TASK_DETAILS.".added_by,".TABLE_PROJECT_TASK_DETAILS.".title as task_title,".TABLE_PROJECT_TASK_DETAILS.".details,".TABLE_PROJECT_TASK_DETAILS.".last_updated,".TABLE_PROJECT_TASK_MODULE.".title as module" ;
    //$fieldst .= TABLE_PROJECT_TASK_STATUS.".title as status,".TABLE_PROJECT_TASK_PRIORITY.".title as priority, ".TABLE_PROJECT_TASK_TYPE.".title as type,".TABLE_PROJECT_TASK_RESOLUTION.".title as resolution" ;
    ProjectTask::getList( $db, $task_details, $fieldst, $condition_queryt);
    if(!empty($task_details)){
        $task_details =  $task_details[0];
    }
    //get task details EOF
     
     
    $statusArr	= NULL;
    $condition_querys = " WHERE status ='".ProjectTask::ACTIVE."'";
    ProjectStatus::getList( $db, $statusArr, 'id, title', $condition_querys);
    
    
     // Set the Permissions.
   
    if ( $perm->has('nc_p_tsb_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_p_tsb_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_p_tsb_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_p_tsb_delete') ) {
        $variables['can_delete'] = true;
    }
   
    if ( $perm->has('nc_p_tsb_details') ) {
        $variables['can_view_details'] = true;
    }
    if ( $perm->has('nc_p_ts_list') ) {
        $variables['can_view_task_list'] = true;
    }
    if ( $perm->has('nc_pts_mod_list') ) {
        $variables['can_view_module']     = true;
    }
    if ( $perm->has('nc_pts_mod_add') ) {
        $variables['can_add_module']     = true;
    }
     //To show actual total timesheet ie ( timesheet * 3)
    if ( $perm->has('nc_p_ts_ac_tot') ) {
        $variables['can_view_actual_tot_time'] = true;
    }
    if ( $perm->has('nc_p_ts') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
                                    TABLE_PROJECT_TASK_DETAILS   =>  array(
                                                                'Title'    => 'title',
                                                                'Details'      => 'details'         
                                                            ),
                                    TABLE_PROJECT_TASK_MODULE   =>  array(
                                                                'Module Name'    => 'title-project_task_module'
                                                            ),
                                );
        
        $sOrderByArray  = array(
                                TABLE_PROJECT_TASK_DETAILS => array(                                 
                                                                'Title'    => 'title',
                                                                'Details'  => 'details'                                               
                                                            ),
                                );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PROJECT_TASK_DETAILS;
        }
        $where_added =false ;
       
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {
            
                include (DIR_FS_NC.'/project-task-bug-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task-bug.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('details'): {
                include (DIR_FS_NC .'/project-task-bug-details.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task-bug.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            /*case ('view'): {
                include (DIR_FS_NC .'/address-book-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }*/
            
            case ('search'): {
            	
                include(DIR_FS_NC."/project-task-bug-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task-bug.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('delete'): {
                include ( DIR_FS_NC .'/project-task-bug-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task-bug.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
           
            case ('list'):
            default: {
            	
                include (DIR_FS_NC .'/project-task-bug-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task-bug.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        //$messages->setErrorMessage("You donot have the Right to Access this Module.");
        
         $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task-bug.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
       
    }
    $project_id = $or_id;
    
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("or_id", $or_id);
    $s->assign("actual_time", $actual_time);
    $s->assign("task_id", $task_id);
    $s->assign("id", $id);
    $s->assign("project_id", $project_id);
    $s->assign("order", $order);
    $s->assign("statusArr", $statusArr);
    $s->assign("task_details", $task_details);
    $s->assign("team_members", $team_members);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
