<?php

    if ( $perm->has('nc_st_vbk_list') ) {
        
        include_once ( DIR_FS_CLASS .'/PhoneVendorBank.class.php');
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
        }
        else {
            $condition_query .= ' AND (';
        }
        
        $access_level   = $my['access_level'];  
       
         //show the list of Vendors of same access levels
        if ( $perm->has('nc_vbk_list_all') ) { 
            $condition_query .= "  parent_id = '' )"; 
        }else{      
            $condition_query .= "".TABLE_VENDORS_BANK .".access_level <= $access_level ";    
            $condition_query .= ')';
            $condition_query .= " AND parent_id = '' ";            
        }
        //$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        $condition_query .= " ORDER BY do_reg DESC ";
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	VendorsBank::::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        VendorsBank::::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
        
        /*$phone  = new Phone(TABLE_VENDORS_BANK);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_VENDORS_BANK, $list[$i]['user_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}*/
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_view_sub_user'] = false;

        if ( $perm->has('nc_vbk_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_vbk_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_vbk_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_vbk_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_vbk_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_vbk_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_vbk_su_list') ) {
            $variables['can_view_sub_user'] = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-bank-st-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>