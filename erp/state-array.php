<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    //include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
  
    
   $state_arr = array(
					'1'		=>'Andaman & Nicobar',
					'2'		=>'Andhra Pradesh',
					'3'		=>'Arunachal Pradesh',
					'4'		=>'Assam',
					'5'		=>'Bihar',
					'6'		=>'Chandigarh',
					'7'		=>'Chattisgarh',
					'8'		=>'Dadra and Nagar Haveli',
					'9'		=>'Daman & Diu',
					'10'	=>'Delhi',
					'11'	=>'Goa',
					'12'	=>'Gujarat',
					'13'	=>'Haryana',
					'14'	=>'Himachal Pradesh',
					'15'	=>'Jammu & Kashmir',
					'16'	=>'Jharkhand',
					'17'	=>'Karnataka',
					'18'	=>'Kerala',
					'19'	=>'Lakshadweep',
					'20'	=>'Madhya Pradesh',
					'21'	=>'Maharashtra',
					'22'	=>'Manipur',
					'23'	=>'Meghalaya',
					'24'	=>'Mizoram',
					'25'	=>'Nagaland',
					'26'	=>'Orissa',
					'27'	=>'Pondicherry',
					'28'	=>'Punjab',
					'29'	=>'Rajasthan',
					'30'	=>'Sikkim',
					'31'	=>'Tamil Nadu',
					'32'	=>'Tripura',
					'33'	=>'Uttar Pradesh',
					'34'	=>'Uttaranchal',
					'35'	=>'West Bengal'
				);
    foreach($stateArray as $val)
    {
        $query	= " INSERT INTO ".TABLE_LIST_STATE
                            ." SET ".TABLE_LIST_STATE .".name = '". $val ."'"  
                                    .",". TABLE_LIST_STATE .".country_id    = '92'"                                
                                    .",". TABLE_LIST_STATE .".country_code  = '91'"                                
                                	.",". TABLE_LIST_STATE .".status        = '1'"                                
                                    .",". TABLE_LIST_STATE .".ip            = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                                	.",". TABLE_LIST_STATE .".date          = '". 		date('Y-m-d H:i:s')."'";
        $db->query($query);                                    
                                    
    }
        $page["var"][] = array('variable' => 'stateArray', 'value' => 'stateArray');
        
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>