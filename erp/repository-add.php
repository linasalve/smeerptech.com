<?php
  if ( $perm->has('nc_rps_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
	   
        $lst_type = null;
        $lst_type = Repository::getType();
       
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
			$files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_REPOSITORY_FILE_SIZE     ;
                 
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
						);
            
          
               
				if ( Repository::validateAdd($data, $extra) ) {  
					
					
					if(!empty($data['file_1'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_1']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-1".".".$ext ;
						$data['file_1'] = $attachfilename;
						
						if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   
						  @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}
					if(!empty($data['file_2'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_2']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-2".".".$ext ;
						$data['file_2'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_2']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}
					if(!empty($data['file_3'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_3']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-3".".".$ext ;
						$data['file_3'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_3']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}				
					if(!empty($data['file_4'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_4']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-4".".".$ext ;
						$data['file_4'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_4']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}
					if(!empty($data['file_5'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_5']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-5".".".$ext ;
						$data['file_5'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_5']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}	
					
					//Print Files
					if(!empty($data['print_file_1'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_1']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-1".".".$ext ;
						$data['print_file_1'] = $attachfilename;
						
						if (move_uploaded_file ($files['print_file_1']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   
						  @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}
					
					if(!empty($data['print_file_2'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_2']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-2".".".$ext ;
						$data['print_file_2'] = $attachfilename;                    
						if (move_uploaded_file ($files['print_file_2']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}
					if(!empty($data['print_file_3'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_3']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-3".".".$ext ;
						$data['print_file_3'] = $attachfilename;                    
						if (move_uploaded_file ($files['print_file_3']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}				
					if(!empty($data['print_file_4'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_4']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-4".".".$ext ;
						$data['print_file_4'] = $attachfilename;                    
						if (move_uploaded_file ($files['print_file_4']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}
					if(!empty($data['print_file_5'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_5']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-5".".".$ext ;
						$data['print_file_5'] = $attachfilename;                    
						if (move_uploaded_file ($files['print_file_5']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						}
					}
					
                    $query	= " INSERT INTO ".TABLE_REPOSITORY
                            ." SET ".TABLE_REPOSITORY .".filename = '". $data['filename'] ."'"                            
							.",". TABLE_REPOSITORY .".subject 	= '". $data['subject'] ."'"
							.",". TABLE_REPOSITORY .".type 	= '". $data['type'] ."'"
							.",". TABLE_REPOSITORY .".doc_of 	= '". $data['doc_of'] ."'"
                            .",". TABLE_REPOSITORY .".text 		  = '". $data['text'] ."'"
                            .",". TABLE_REPOSITORY .".description = '". $data['description'] ."'"
                            .",". TABLE_REPOSITORY .".file_1 = '".    $data['file_1'] ."'"
                            .",". TABLE_REPOSITORY .".file_2 = '".    $data['file_2'] ."'"
                            .",". TABLE_REPOSITORY .".file_3 = '".    $data['file_3'] ."'"
                            .",". TABLE_REPOSITORY .".file_4 = '".    $data['file_4'] ."'"
                            .",". TABLE_REPOSITORY .".file_5 = '".    $data['file_5'] ."'"                        
							.",". TABLE_REPOSITORY .".print_file_1 = '".$data['print_file_1'] ."'"
                            .",". TABLE_REPOSITORY .".print_file_2 = '".$data['print_file_2'] ."'"
                            .",". TABLE_REPOSITORY .".print_file_3 = '".$data['print_file_3'] ."'"
                            .",". TABLE_REPOSITORY .".print_file_4 = '".$data['print_file_4'] ."'"
                            .",". TABLE_REPOSITORY .".print_file_5 = '".$data['print_file_5'] ."'"
                            .",". TABLE_REPOSITORY .".ip     = '". 	  $_SERVER['REMOTE_ADDR'] ."'"                                
                            .",". TABLE_REPOSITORY .".created_by = 	  '".$my['user_id']."'"
                            .",". TABLE_REPOSITORY .".created_by_name = '".$my['f_name']." ".$my['l_name'] ."'"
                            .",". TABLE_REPOSITORY .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                    
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Record has been added successfully.");
                        $variables['hid'] = $db->last_inserted_id();              
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    //$data		= NULL;
		        }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/repository.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/repository.php");
        }
       
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/repository.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            
            $page["var"][] = array('variable' => 'lst_type', 'value' => 'lst_type');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'repository-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
