<?php

    if ( $perm->has('nc_nwl_list') ) {
    
		// Status
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched']=1;
        //Perform value pass for search as we are useing the same form for newsletter-queue-search
        $_SEARCH['search'] = 'search' ;
        // To count total records.
        $list	= 	NULL;
        $fields = TABLE_NEWSLETTER.'.n_id ' ;
        $total	=	Newsletter::getList( $db, $list, '', $condition_query);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
              
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
       
       
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields = TABLE_NEWSLETTER .'.n_id'
                    .','. TABLE_NEWSLETTER .'.n_title'
                    .','. TABLE_NEWSLETTER .'.subject'
                    .','. TABLE_NEWSLETTER .'.send_on_date'
                    .','. TABLE_NEWSLETTER .'.sent_date' ;
        Newsletter::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        /*
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){     
                
                $flist[$key]=$val;
            }
        }
        */
        
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_view_qlist']     = false;
        $variables['can_add']           = false;
        $variables['can_resend']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
      
        if ( $perm->has('nc_nwl_qlist') ) {
            $variables['can_view_qlist'] = true;
        }
        
        if ( $perm->has('nc_nwl_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_nwl_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_nwl_resend') ) {
            $variables['can_resend'] = true;
        }
        if ( $perm->has('nc_nwl_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_nwl_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_nwl_status') ) {
            $variables['can_change_status'] = true;
        }
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletter-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>