<?php
 
	$id= isset($_GET["id"])? $_GET["id"] : ( isset($_POST["id"])? $_POST["id"]: '' );
	$status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

	$access_level = $my['access_level'];
	$access_level += 1;
	
	
	$extra = array( 'db' 		    => &$db,
					'access_level'  => $access_level,
					'messages' 	    => $messages
				);
	
	Policies::updateStatus($id, $status, $extra);
	$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'policies-status.html');
	// Display the  list.
	//include ( DIR_FS_NC .'/prospects-list.php');
    
?>