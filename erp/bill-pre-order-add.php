<?php
    if ( $perm->has('nc_bl_po_add') ) {
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
       /*if ( $perm->has('nc_bl_po_add_al') ) {
            $access_level += 1;
        }*/
        $al_list        = getAccessLevel($db, $access_level);
        
         // Read the Services.
        include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
        include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
        
        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC";
        Services::getList($db, $lst_service, 'ss_id,ss_title,ss_punch_line, tax1_name, tax1_value ', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
                    $ss_id= $key['ss_id'];
                    $ss_title= $key['ss_title'];
                    $ss_punch_line= $key['ss_punch_line'];
                    $tax1_name = $key['tax1_name'];
                    $tax1_value = $key['tax1_value'];
                    $tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
                    
                    $keyNew['ss_id'] = $ss_id ; 
                    $keyNew['ss_title'] = $ss_title ; 
                    $keyNew['ss_punch_line'] = $ss_punch_line ; 
                    $keyNew['tax1_name'] = $tax1_name ; 
                    $keyNew['tax1_value'] = $tax1_value ; 
                    $keyNew['tax1_pvalue'] = $tax1_pvalue ; 
                    $lst_service[$val] = $keyNew;
            }
        }
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
            $data['files'] = $files;
            $data['allowed_file_types'] = $allowed_file_types;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ;
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'data' 				=> $data,
                            'messages'          => $messages
                        );
            $data['do_po'] = time();
            
            if ( PreOrder::validateAdd($data, $messages, $extra) ) {
            
                $data['files_list'] = '';                    
                foreach($data['files'] as $key => $val){
                    
                    //$val["name"] = str_replace(' ','-',$val["name"]);
                    $filedata["extension"]='';
                    $filedata = pathinfo($val["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    $attachfilename = "QUOT-".mktime().".".$ext ;
                  
                   if(!empty($val["name"])){
                 
                        if (copy($val['tmp_name'], DIR_FS_QUOTATION_FILES."/".$attachfilename)){
                            $data['files_list'] .= $attachfilename."," ;
                        }
                    }
                }
                $query	= " INSERT INTO ".TABLE_BILL_PO
                                ." SET ". TABLE_BILL_PO .".q_no 		    = '". $data['q_no'] ."'"
                                    .",". TABLE_BILL_PO .".access_level     = '". $data['access_level'] ."'"
                                    .",". TABLE_BILL_PO .".created_by       = '". $data['created_by'] ."'"
                                    .",". TABLE_BILL_PO .".order_details    = '". $data['order_details'] ."'"
                                    .",". TABLE_BILL_PO .".client_details   = '". $data['client_details'] ."'"
                                    .",". TABLE_BILL_PO .".payment_details   = '". $data['payment_details'] ."'"
                                    .",". TABLE_BILL_PO .".remark   		= '". $data['remark'] ."'"
                                    .",". TABLE_BILL_PO .".attach_files		= '". $data['files_list'] ."'"
                                    .",". TABLE_BILL_PO .".do_po            = '". date('Y-m-d H:i:s', $data['do_po']) ."'"
                                    .",". TABLE_BILL_PO .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                                    .",". TABLE_BILL_PO .".status		    = '". $data['status'] ."'" ;
                
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $messages->setOkMessage("New Pre-Order has been created");
                    $variables['hid'] = $db->last_inserted_id();
                    
                    // Send the notification mails to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_po_app', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_po_app_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );

                    include ( DIR_FS_INCLUDES .'/user.inc.php');
                    $data['link'] = DIR_WS_NC .'/bill-pre-order.php?perform=view&po_id='. $variables['hid'];
                    $data['creator']= '';
                    $data['date']   = date('d M, Y', $data['do_po']);
                    User::getList($db, $data['creator'], 'username,number,email,f_name,l_name', "WHERE user_id = '". $data['created_by'] ."'");
                    $data['creator'] = $data['creator'][0];

                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_PO_NEW_MANAGERS', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }

                    // Send Email to the Creator of the Pre-Order.
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_PO_NEW', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
//        else {
//            // Set up the default values to be displayed.
//            $_ALL_POST['client_details']= "Name: \nEmail: \nOrganization: \nPhone No: \nAddress: \nOther Details: \n";
//            $_ALL_POST['order_details'] = "Product: \nService: \nDomain: \nDesigning: \nHosting: \nOther Details: \n";
//        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/bill-pre-order-list.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/bill-pre-order.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/bill-pre-order.php?added=1");   
        }
        else {
        	
        	  // Read the Services templates.
            /*
            include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
            $services = NULL;
            $condition_query = "WHERE ss_status = '". ACTIVE ."' ORDER BY ss_title ASC";
            Services::getList($db, $services, 'ss_id,ss_title,ss_order_tpl,ss_client_detail', $condition_query);
            $count = count($services);
            $rep = array("\n", "\r");
            for ( $i=0; $i<$count; $i++ ) {
                //$services[$i]['ss_order_tpl'] = str_replace($rep, "<br/>", $services[$i]['ss_order_tpl']);
                //$services[$i]['ss_client_detail'] = str_replace($rep, "<br/>", $services[$i]['ss_client_detail']);
                $services[$i]['ss_order_tpl']       = preg_replace("/(\r\n|\r|\n)/", "\\n", $services[$i]['ss_order_tpl']);
                $services[$i]['ss_client_detail']   = preg_replace("/(\r\n|\r|\n)/", "\\n", $services[$i]['ss_client_detail']);
            }
            */
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'services', 'value' => 'services');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            $page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-pre-order-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
        
        
?>
