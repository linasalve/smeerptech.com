<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    
                    $table = TABLE_CLIENT_DOMAINS ;
                    foreach( $field_arr as $key => $field ) {
                            $condition_query .= " ". $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                        }
                    
                }
            }
            $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    //Client serach bof
	$search_client = isset($_POST["search_client"])   ? $_POST["search_client"]  : (isset($_GET["search_client"])   ? $_GET["search_client"]   :'');
	$search_client_details = isset($_POST["search_client_details"]) ? $_POST["search_client_details"]  : (isset($_GET["search_client_details"])   ? $_GET["search_client_details"]   :'');
	
	if(!empty($search_client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_CLIENT_DOMAINS .".client ='". $search_client ."' ";        
        $condition_url          .= "&search_client_details=$search_client_details&search_client=$search_client";
        $_SEARCH["search_client_details"]  = $search_client_details;
        $_SEARCH["search_client"]     = $search_client;  
	}
	//Client serach eof
    
    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    
    // BO: From Date
    if (( $chk_date_from == 'AND' || $chk_date_from == 'OR')  && !empty($date_from)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_CLIENT_DOMAINS .".do_e >= '". $dfa ."'";
        
         $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
    }
   
    // EO: From Date
    
    // BO: Upto Date
    if (( $chk_date_to == 'AND' || $chk_date_to == 'OR')  && !empty($date_to)){
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_CLIENT_DOMAINS .".do_e <= '". $dta ."'";
        
         $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
    }
   
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/client-domains-list.php');
?>
