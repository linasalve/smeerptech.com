<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_sl_ld_edit') ) {
        $lead_id = isset($_GET["lead_id"]) ? $_GET["lead_id"] : ( isset($_POST["lead_id"]) ? $_POST["lead_id"] : '' );
        
        include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
    
        $_ALL_POST      = NULL;
        $data           = NULL;
        $regionlead     = new RegionLead('91');
        $phonelead      = new PhoneLead(TABLE_SALE_LEADS);
        //$reminder     = new UserReminder(TABLE_LEADS);
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_sl_ld_edit_al') ) {
            $access_level += 1;
        }
        $al_list    = getAccessLevel($db, $access_level);
        //$role_list  = Lead::getRoles($db, $access_level);s
        //BOF read the available industries
		Leads::getCountryCode($db,$country_code);
        //EOF read the available industries
        $memberTypeList  = Leads::getMemberType();
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save'){
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'regionlead'=> $regionlead,
                            'phonelead' => $phonelead,
                            //'reminder'  => $reminder
                        );
            
            if ( Leads::validateUpdate($data, $extra) ) {
            
                Leads::updateHistory($db, $my, $data);
                $membertype_ids=$_POST['member_type'];
				$membertypestr=implode(",",$membertype_ids);
				if(!empty($membertypestr)){
					$membertypestr=",".$membertypestr.",";
				}
				$query  = " UPDATE ". TABLE_SALE_LEADS
                        ." SET ". TABLE_SALE_LEADS .".company_name   		= '". $data['company_name'] ."'"
							.",". TABLE_SALE_LEADS .".billing_name   		= '". $data['company_name'] ."'"
							.", ". TABLE_SALE_LEADS .".kind_of_person  		= '". $data['kind_of_person'] ."'"
							//.",". TABLE_SALE_LEADS .".lead_owner   		= '". $my['f_name'] ."'"
							//.",". TABLE_SALE_LEADS .".lead_assign_to   	= '". $data['lead_assign_to'] ."'"
							.",". TABLE_SALE_LEADS .".lead_source_from		= '". $data['lead_source_from'] ."'"
							.",". TABLE_SALE_LEADS .".lead_source_to		= '". $data['lead_source_to'] ."'"
							.",". TABLE_SALE_LEADS .".lead_campaign_id   	= '". $data['lead_campaign_id'] ."'"
							.",". TABLE_SALE_LEADS .".lead_industry_id   	= '". $data['lead_industry_id'] ."'"
							.",". TABLE_SALE_LEADS .".no_emp			   	= '". $data['no_emp'] ."'"
							.",". TABLE_SALE_LEADS .".rating			   	= '". $data['rating'] ."'"
							.",". TABLE_SALE_LEADS .".annual_revenue   		= '". $data['annual_revenue'] ."'"
							.",". TABLE_SALE_LEADS .".rivals_id		   		= '". $data['rivals_id'] ."'"
                            .",". TABLE_SALE_LEADS .".email       			= '". $data['email'] ."'"
                            .",". TABLE_SALE_LEADS .".email_1     			= '". $data['email_1'] ."'"
                            .",". TABLE_SALE_LEADS .".email_2     			= '". $data['email_2'] ."'"
							.",". TABLE_SALE_LEADS .".mobile1     			= '". $data['mobile1'] ."'"
                            .",". TABLE_SALE_LEADS .".mobile2     			= '". $data['mobile2'] ."'"
                            .",". TABLE_SALE_LEADS .".mobile3     			= '". $data['mobile3'] ."'"
                            .",". TABLE_SALE_LEADS .".title       			= '". $data['title'] ."'"
                            .",". TABLE_SALE_LEADS .".f_name      			= '". $data['f_name'] ."'"
                            .",". TABLE_SALE_LEADS .".m_name      			= '". $data['m_name'] ."'"
                            .",". TABLE_SALE_LEADS .".l_name      			= '". $data['l_name'] ."'"
                            .",". TABLE_SALE_LEADS .".p_name      			= '". $data['p_name'] ."'"
                            .",". TABLE_SALE_LEADS .".desig       			= '". $data['desig'] ."'"
                            .",". TABLE_SALE_LEADS .".org         			= '". $data['org'] ."'"
							.", ". TABLE_SALE_LEADS .".member_type   		= '". $membertypestr ."'"
                            .",". TABLE_SALE_LEADS .".domain      			= '". $data['domain'] ."'"
                            .",". TABLE_SALE_LEADS .".gender      			= '". $data['gender'] ."'"
                            .",". TABLE_SALE_LEADS .".do_birth    			= '". $data['do_birth'] ."'"
                            .",". TABLE_SALE_LEADS .".do_aniv     			= '". $data['do_aniv'] ."'"
                            .",". TABLE_SALE_LEADS .".remarks     			= '". $data['remarks'] ."'" 
                            .",". TABLE_SALE_LEADS .".updated_by     		= '". $my['uid'] ."'" 
                            .",". TABLE_SALE_LEADS .".updated_by_name  	= '". $my['f_name']." ".$my['l_name'] ."'" 
                            .",". TABLE_SALE_LEADS .".do_update     	= '".date('Y-m-d h:i:s')."'" 
                            .", ". TABLE_SALE_LEADS .".ip_update    				= '". $_SERVER['REMOTE_ADDR'] ."'"
							.",". TABLE_SALE_LEADS .".lead_status   		= '". $data['lead_status'] ."'" 
							." WHERE ". TABLE_SALE_LEADS .".lead_id   		= '". $data['lead_id'] ."'";
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['lead_id'];
					 // Update the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
					
                        if ( isset($data['a_remove'][$i]) && $data['a_remove'][$i] == '1') {
                            if ( ! $regionlead->delete($db, $data['address_id'][$i], $variables['hid'],TABLE_SALE_LEADS) ) {
                                foreach ( $regionlead->getError() as $errors) {
                                    $messages->setOkMessage($errors['description']);
                                }
                            }
                        }
                        else {
                            $address_arr = array('id'   => isset($data['address_id'][$i])? $data['address_id'][$i] : '0',
                                                    'address_type'  => $data['address_type'][$i],
                                                    'company_name'  => $data['company_name'][$i],
                                                    'address'       => $data['address'][$i],
                                                    'city'          => $data['city'][$i],
                                                    'state'         => $data['state'][$i],
                                                    'country'       => $data['country'][$i],
                                                    'zipcode'       => $data['zipcode'][$i],
                                                    'is_preferred'  => isset($data['is_preferred'][$i])? $data['is_preferred'][$i] : '0',
                                                    'is_verified'   => isset($data['is_verified'][$i])? $data['is_verified'][$i] : '0'
                                                );
                            
                            if ( !$regionlead->update($variables['hid'], TABLE_SALE_LEADS, $address_arr) ) {
                                foreach ( $regionlead->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phonelead->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_SALE_LEADS) ) {
                                    foreach ( $phonelead->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $phone_arr = array( 'id'            => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
                                                    'p_type'        => $data['p_type'][$i],
                                                    'cc'            => $data['cc'][$i],
                                                    'ac'            => $data['ac'][$i],
                                                    'p_number'      => $data['p_number'][$i],
                                                    'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
                                                    'p_is_verified' => isset($data['p_is_verified'][$i]) ? $data['p_is_verified'][$i] : '0'
                                                );
                                if ( ! $phonelead->update($db, $variables['hid'], TABLE_SALE_LEADS, $phone_arr) ) {
                                    foreach ( $phonelead->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    // Update the Reminders.
					//Push email id into Newsletters Table BOF
					$table = TABLE_NEWSLETTERS_MEMBERS;
					include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
					if(!empty($data['email'])){
						$domain1 = strtolower(strstr($data['email'], '@'));
						if( $domain1!= 'smeerptech.com' && 
							!NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email']) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email']."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_1'])){
						$domain2 = strtolower(strstr($data['email_1'], '@'));
						if( $domain2!='smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email_1']) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email_1']."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_2'])){
						$domain3 = strtolower(strstr($data['email_2'], '@'));
						if($domain3!= 'smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email_2']) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email_2']."'" ;
						  $db->query($sql1);
						}
					}
					//Push email id into Newsletters Table EOF
					
					/*SEND SERVICE PDF BOF*/
					/*
					include_once ( DIR_FS_INCLUDES .'/leads-ticket.inc.php');
					include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');	
					include ( DIR_FS_INCLUDES .'/support-functions.inc.php');					
					$id=39;
					$fields = TABLE_ST_TEMPLATE.'.*'  ;            
					$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
					SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1);
					
					if(!empty($details)){
					
						$details = $details[0];
						$data['template_id'] = $details['id'];
						$data['template_file_1'] = $details['file_1'];
						$data['template_file_2'] = $details['file_2'];
						$data['template_file_3'] = $details['file_3'];
						$data['subject'] = processUserData($details['subject']);
						$data['text'] = processUserData($details['details']);
						$data['display_name'] = $data['display_user_id']=$data['display_designation']='';				
						
						 
						//$randomUser = getCeoDetails();
						//$data['display_name'] = $randomUser['name'];
						//$data['display_user_id'] = $randomUser['user_id'];
						//$data['display_designation'] = $randomUser['desig']; 
						
						if($my['department'] == ID_MARKETING){
							//This is the marketing person identity
							$data['tck_owner_member_id'] = $my['user_id'];
							$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
							$data['tck_owner_member_email'] = $my['email'];
							$data['marketing_email'] = $my['email'];
							$data['marketing_contact'] = $my['marketing_contact'];
							$data['marketing'] = 1;								
							$data['display_name'] = $my['f_name']." ".$my['l_name'];
							$data['display_user_id'] = $my['user_id'];
							$data['display_designation'] = $my['desig'];
						}else{ 
							$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
							$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
							$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;
							$data['marketing_email'] = SALES_MEMBER_USER_EMAIL;
							$data['marketing_contact'] = '';
							$data['marketing'] = 0;								
							$data['display_name'] = SALES_MEMBER_USER_NAME;
							$data['display_user_id'] = SALES_MEMBER_USER_ID;
							$data['display_designation'] = '';								
						}	
						
						$data['name'] = $data['f_name']." ".$data['l_name'] ;
						$data['ticket_owner'] = $data['company_name']." ";
						if(!empty($data['name'])){
							$data['ticket_owner'] = $data['company_name']." ".$data['name'];
						}
						$data['ticket_owner_uid'] = $variables['hid'] ;
						$ticket_no  =  LeadsTicket::getNewNumber($db);
						$ticket_status = LeadsTicket::PENDINGWITHCLIENTS;
												
						//Check that this user's ticket is exist or not
						$ticket_id = 0;
						$sql = " SELECT ticket_id,ticket_no FROM ".TABLE_LD_TICKETS." WHERE 
							".TABLE_LD_TICKETS.".ticket_owner_uid = '".$data['ticket_owner_uid']."' 
							AND ".TABLE_LD_TICKETS.".service_pdf_tkt ='1' AND 
							".TABLE_LD_TICKETS.".ticket_child=0 LIMIT 0,1";
						$db->query($sql) ;
						$mticket_no='';
						if($db->nf() > 0){
							$db->next_record() ;
							$ticket_id = $db->f("ticket_id") ; 
							$mticket_no = $db->f("ticket_no") ;
						}
						
						if($ticket_id>0){
							
							$query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
							. TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_LD_TICKETS .".template_id       = '". $data['template_id'] ."', "
							. TABLE_LD_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_LD_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_LD_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_email = '". $data['tck_owner_member_email'] ."', "
							. TABLE_LD_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_LD_TICKETS .".ticket_creator  = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_LD_TICKETS .".ticket_subject  = '". $data['subject'] ."', "
							. TABLE_LD_TICKETS .".ticket_text     = '". $data['text'] ."', "
							. TABLE_LD_TICKETS .".mail_to_all_su  = '".$mail_to_all_su."', "
							. TABLE_LD_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_LD_TICKETS .".ticket_status   = '".$ticket_status ."', "
							. TABLE_LD_TICKETS .".ticket_child    = '".$ticket_id."', "
							. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_LD_TICKETS .".min = '". $data['min']."', "
							. TABLE_LD_TICKETS .".service_pdf_tkt = '1', "
							. TABLE_LD_TICKETS .".ticket_response_time = '0', "
							. TABLE_LD_TICKETS .".ticket_replied  = '0', "
							. TABLE_LD_TICKETS .".from_admin_panel = '".LeadsTicket::ADMIN_PANEL."', "
							. TABLE_LD_TICKETS .".display_name   = '". $data['display_name'] ."', "
							. TABLE_LD_TICKETS .".display_user_id = '". $data['display_user_id'] ."', "
							. TABLE_LD_TICKETS .".display_designation = '". $data['display_designation'] ."', "
							. TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
							. TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
						
						}else{
							$mticket_no	=$ticket_no;
						 	$query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
							. TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_LD_TICKETS .".template_id       = '". $data['template_id'] ."', "
							. TABLE_LD_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_LD_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_LD_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_email = '". $data['tck_owner_member_email'] ."', "
							. TABLE_LD_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_LD_TICKETS .".ticket_creator  = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_LD_TICKETS .".ticket_subject  = '". $data['subject'] ."', "
							. TABLE_LD_TICKETS .".ticket_text     = '". $data['text'] ."', "
							. TABLE_LD_TICKETS .".mail_to_all_su  = '".$mail_to_all_su."', "
							. TABLE_LD_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_LD_TICKETS .".ticket_status   = '".$ticket_status ."', "
							. TABLE_LD_TICKETS .".ticket_child    = '0', "
							. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_LD_TICKETS .".min = '". $data['min']."', "
							. TABLE_LD_TICKETS .".service_pdf_tkt = '1', "
							. TABLE_LD_TICKETS .".ticket_response_time = '0', "
							. TABLE_LD_TICKETS .".ticket_replied  = '0', "
							. TABLE_LD_TICKETS .".from_admin_panel = '".LeadsTicket::ADMIN_PANEL."', "
							. TABLE_LD_TICKETS .".display_name   = '". $data['display_name'] ."', "
							. TABLE_LD_TICKETS .".display_user_id = '". $data['display_user_id'] ."', "
							. TABLE_LD_TICKETS .".display_designation = '". $data['display_designation'] ."', "
							. TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
							. TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
						}
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
						if(!empty($data['email']) || !empty($data['email_1']) || !empty($data['email_2'])){
							$db->query($query) ;
							$variables['hid'] = $db->last_inserted_id() ;
							$mail_send_to_su='';
							$data['mticket_no'] = $mticket_no ;
							
							if ( getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email) ){                                    
								if(!empty($data['email'])){
									//$to = '';
									$to[]   = array('name' => $data['ticket_owner'] , 'email' => 
									$data['email']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email'];
									//echo $email["body"];
									 
									 
								}
								if(!empty($data['email_1'])){
									//$to = '';
									$to[]   = array('name' => $data['ticket_owner'] , 'email' => 
									$data['email_1']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email_1'];
									 
								}
								if(!empty($data['email_2'])){
									//$to = '';
									$to[]   = array('name' => $data['ticket_owner'] , 'email' => 
									$data['email_2']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email_2'];
									 
								}
								if(!empty($smeerp_client_email)){
									//$to = '';
									$bcc[]  = array('name' => $data['ticket_owner'] , 'email' => 
									$smeerp_client_email);    
								}
							}
							if(!empty($to)){
								 
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,
									$cc,$bcc,$file_name);
							}
							
							if(!empty($mail_send_to_su)){
								$query_update = "UPDATE ". TABLE_LD_TICKETS 
                                    ." SET ". TABLE_LD_TICKETS .".mail_send_to_su
									= '".trim($mail_send_to_su,",")."'"                                   
                                    ." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
								$db->query($query_update) ;
							}
						}
						
					}
					*/
					/*SEND SERVICE PDF EOF*/
                   
                     
                    
                    // Send The Email to the concerned persons.
                   
                    $messages->setOkMessage("Lead Details has been updated.");
                    $phone_details ='';
					$phonelead->setPhoneOf(TABLE_SALE_LEADS, $variables['hid']);
					$data['phone'] = $phonelead->get($db);
					if(!empty($data['phone'])){
						foreach($data['phone'] as $key=>$val){
							$sr=$key+1 ;
							$phone_details .= "Sr.".$sr.". "."(".$val['title'].")".$val['cc']."-".$val['ac']."-".$val['p_number']."<br/>";
						
						}
					}
					
					
					// Read the Addresses.
					$regionlead->setAddressOf(TABLE_SALE_LEADS, $variables['hid']);
					$data['address_list'] = $regionlead->get();
					if(!empty($data['address_list'])){
						foreach($data['address_list'] as $key1=>$val1){
							$sr1 = $key1+1 ;
							$address_details .= "Sr.".$sr1.". "."(".$val1['title'].")".$val1['address']." ".$val1['city_title']." ".$val1['state_title']." ".$val1['zipcode']." ".$val1['c_title']."<br/>";
						
						}
					}
					
					//SEND MAIL TO ADMIN BOF
					$data['updated_by']=$my['f_name']." ".$my['l_name'] ;
					$data['do_update']= date('d M Y');
					$data['id'] = $variables['hid'] ;
					$data['company_name'] = $data['company_name'] ;
					$data['name']= $data['f_name']." ".$data['l_name'] ;
					$data['no_emp'] = $data['no_emp'] ;
					$data['gender_name'] = ($data['gender']=='m') ? 'Male' : 'Female' ;
					$data['email'] 	 = $data['email'] ;
					$data['email_1'] = $data['email_1'] ;
					$data['email_2'] = $data['email_2'] ;
					$data['mobile1'] = $data['mobile1'] ;
					$data['mobile2'] = $data['mobile2'] ;
					$data['mobile3'] = $data['mobile3'] ;
					$data['phone_details'] = $phone_details ;
					$data['address_details'] = $address_details ;
					include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
					/* if ( getParsedEmail($db, $s, 'PROSPECT_UPDATE', $data, $email)) {
						$to     = '';
						$to[]   = array('name' => $sales_name, 'email' => $sales_email);
						$from   = $reply_to = array('name' => $sales_name, 'email' => $sales_email);
						//echo $email["body"]; exit;
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
					} */
                    //to flush the data.
                    //$_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Lead was not updated.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $lead_id;
            include ( DIR_FS_NC .'/sale-lead-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
				if($count==0){
					$count=1;
				}
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'            => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
                
                // Preserve the Addresses.
                $count = count($_ALL_POST['city']);
				if($count==0){
					$count=1;
				}
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['address_list'][] = array(   'id'  => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'is_verified'   => isset($_ALL_POST['is_verified'][$i])? $_ALL_POST['is_verified'][$i]:'',
                                                                'is_preferred'  => isset($_ALL_POST['is_preferred'][$i])? $_ALL_POST['is_preferred'][$i]:'',
                                                                'address_type'  => isset($_ALL_POST['address_type'][$i])? $_ALL_POST['address_type'][$i]:'',
                                                                'address'       => isset($_ALL_POST['address'][$i])? $_ALL_POST['address'][$i]:'',
                                                                'city'          => isset($_ALL_POST['city'][$i])? $_ALL_POST['city'][$i]:'',
                                                                'state'         => isset($_ALL_POST['state'][$i])? $_ALL_POST['state'][$i]:'',
                                                                'zipcode'       => isset($_ALL_POST['zipcode'][$i])? $_ALL_POST['zipcode'][$i]:'',
                                                                'country'       => isset($_ALL_POST['country'][$i])? $_ALL_POST['country'][$i]:'',
                                                     );
//                    }
                }

                
                // Preserve the Reminders.
                //$count = $_ALL_POST['reminder_count'];
                /*for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['remind_date_'.$i) && !empty($_ALL_POST['remind_date_'.$i) ) {
                        $_ALL_POST['reminder_list'][] = array(  'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'do_reminder'   => isset($_ALL_POST['remind_date_'.$i])? $_ALL_POST['remind_date_'.$i]:'',
                                                                'description'   => isset($_ALL_POST['remind_text_'.$i])? $_ALL_POST['remind_text_'.$i]:'',
                                                     );
//                    }
                }*/
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE lead_id = '". $lead_id ."' ";
                $_ALL_POST      = NULL;
                if ( Leads::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                 
                    //$_ALL_POST['manager'] 	= Lead::getManager($db, '', $_ALL_POST['manager']);
					//$_ALL_POST['roles'] 	= explode(',', $_ALL_POST['roles']);
					
					// BO: Read the Team Members Information.
					/*if ( !empty($_ALL_POST['team']) && !is_array($_ALL_POST['team']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
						$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
						$_ALL_POST['team_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['team'] = array();
						foreach ( $_ALL_POST['team_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['team'][] = $members['user_id'];
							$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}*/
					// EO: Read the Team Members Information.
					
                    // Check the Access Level.
                     
					// Read the Contact Numbers.
					$phonelead->setPhoneOf(TABLE_SALE_LEADS, $lead_id);
					$_ALL_POST['phone'] = $phonelead->get($db);
					
					// Read the Addresses.
					$regionlead->setAddressOf(TABLE_SALE_LEADS, $lead_id);
					$_ALL_POST['address_list'] = $regionlead->get();
					 
					// Read the Reminders.
					//$reminder->setReminderOf(TABLE_LEADS, $user_id);
					//$_ALL_POST['reminder_list'] = $reminder->get($db);
					if(!empty($_ALL_POST['member_type'])){
                        $_ALL_POST['member_type'] = trim($_ALL_POST['member_type'],",");
                        $_ALL_POST['member_type'] = explode(",",$_ALL_POST['member_type']);
                    }
                   
                }
            }

            if ( !empty($_ALL_POST['lead_id']) ) {

                // Check the Access Level.
                //if ( $my['access_level'] < $access_level ) {

                    $phone_types    = $phonelead->getTypeList();
                    $address_types  = $regionlead->getTypeList();
                    $lst_country    = $regionlead->getCountryList();
                    $lst_state      = $regionlead->getStateList();
                    $lst_city       = $regionlead->getCityList();
                    
                    if(empty($_ALL_POST['phone'])){
                        $_ALL_POST['phone'][0]['id']='';
                        $_ALL_POST['phone'][0]['cc']=0;
                        $_ALL_POST['phone'][0]['ac']=0;
                        $_ALL_POST['phone'][0]['p_number']=0;
                        $_ALL_POST['phone'][0]['p_type']='h';
                        $_ALL_POST['phone'][0]['p_is_preferred']=1;
                        $_ALL_POST['phone'][0]['p_is_verified']=0;
                        $_ALL_POST['phone'][0]['title']='Home';
                    }
					
					if(empty($_ALL_POST['address_list'])){
                        $_ALL_POST['address_list'][0]['id']='';
                        $_ALL_POST['address_list'][0]['address_type']='';
                        $_ALL_POST['address_list'][0]['is_verified']='';
                        $_ALL_POST['address_list'][0]['is_preferred']='';
                        $_ALL_POST['address_list'][0]['address']='';
                        $_ALL_POST['address_list'][0]['city']=1;
                        $_ALL_POST['address_list'][0]['state']=0;
                        $_ALL_POST['address_list'][0]['zipcode']='';
                        $_ALL_POST['address_list'][0]['country']=0;
                        
                    }
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'lead_id', 'value' => $lead_id);
                    //$hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    $page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
					$page["var"][] = array('variable' => 'memberTypeList', 'value' => 'memberTypeList');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
                    $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
                    $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-edit.html');
                /* }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Lead with the current Access Level.");
                } */
            }
            else {
                $messages->setErrorMessage("The Selected Lead Rival was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>