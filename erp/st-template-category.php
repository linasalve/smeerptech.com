<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
	page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
	include_once ( DIR_FS_NC ."/header.php" );
	include_once ( DIR_FS_INCLUDES .'/st-template-category.inc.php' );
	
	
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	$sString = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"]) ? $_POST["sString"] : '' );
	$sType = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder	= isset($_GET["sOrder"])? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy = isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	$rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
	$added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
	$condition_url = $condition_query = $action='';
    
	if(empty($rpp)){
		$rpp = RESULTS_PER_PAGE;
	}
	$condition_query ='';
   
	if ( empty($x) ) {
		$x              = 1;
		$next_record    = 0 ;
	}
	else {
		$next_record    = ($x-1) * $rpp;
	}
	$variables["x"]     = $x;
	$variables["rpp"]   = $rpp;
    
	if($added){
		$messages->setOkMessage("Record has been added  successfully.");
	}
    
	if ( $perm->has('nc_st_tc') ) {
        $sTypeArray     = array('Any'  =>  
			array(  'Any of following' => '-1'),
					TABLE_ST_TEMPLATE_CATEGORY   =>  array(
										'Category'  => 'category',
										'Added by Name'  => 'added_by_name',     
										'details'  => 'details'
                                    )
                                );
        
        $sOrderByArray  = array(
                                TABLE_ST_TEMPLATE_CATEGORY => array(
										'Date'   => 'do_e'
									)
                               );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
			$_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
			$_SEARCH['sOrder']  = $sOrder   = 'DESC';
			$order_by_table     = TABLE_ST_TEMPLATE_CATEGORY;
        }
	
        
        //$where_added =false ;
       $variables['status'] = StTemplateCategory::getStatus();
        //use switch case here to perform action. 
        switch ($perform) {
           
			case ('add'): {
				$action='Add';
				include (DIR_FS_NC.'/st-template-category-add.php');
					
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'st-template-category.html'); 
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
            
			case ('edit'): {
				$action='Edit';
				include (DIR_FS_NC .'/st-template-category-edit.php');
					
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'st-template-category.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}

				break;
			}
        
			case ('search'): {
					
				include(DIR_FS_NC."/st-template-category-search.php");
					
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'st-template-category.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
			   
		 
			case ('list'):
				default: {
					
				include (DIR_FS_NC .'/st-template-category-list.php');
				// CONTENT = CONTENT
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'st-template-category.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
	}
	else {
        //$messages->setErrorMessage("You donot have the Right to Access this Module.");
        
         $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
 
		$page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'st-template-category.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
	}
    
	// always assign
	$s->assign("variables", $variables);
	$s->assign("action", $action);
	$s->assign("error_messages", $messages->getErrorMessages());
	$s->assign("success_messages", $messages->getOkMessages());

	if ( isset($page["var"]) && is_array($page["var"]) ) {
		foreach ( $page["var"] as $key=>$fetch ) {
			$s->assign( $fetch["variable"], ${$fetch["value"]});
		}
	}
	if ( isset($page["section"]) && is_array($page["section"]) ) {
		$sections = count($page["section"]);
		for ( $i=0; $i<($sections-1); $i++) {
			$s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
		}
	}
	$s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
