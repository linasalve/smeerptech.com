<?php
    if ( $perm->has('nc_cb_delete') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
        // not addedd in d/b
        /*if ( $perm->has('nc_cb_delete_al') ) {
            $access_level += 1;
        }*/
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        Complainbox::delete($id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/complain-box-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Delete the entry.");
    }
?>
