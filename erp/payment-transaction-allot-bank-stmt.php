<?php    
    if ( $perm->has('nc_p_pt_bk_stmt') ) {  
		include_once ( DIR_FS_INCLUDES .'/bank-statement.inc.php');
		
		$sTypeArray1     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_BANK_STATEMENT   =>  array(  
															'Cheque No'        => 'cheque_no',
															'Description'    => 'description',
															'Debit'     => 'debit',
															'Credit'     => 'credit'
                                                        )
                            );
        
        $sOrderByArray1  = array(
                                TABLE_BANK_STATEMENT => array(       
                                                    'Transaction Date'=> 'stmt_date' ,
													 'Year'        	=> 'year' ,
                                                    'Value Date'    => 'val_date' 
                                                    ),
                            ); 
        $transaction_id = isset($_GET['transaction_id']) ? $_GET['transaction_id']:(isset($_POST['transaction_id'] )
		? $_POST['transaction_id'] :''); 
        //$exchange_rate = isset ($_GET['exchange_rate']) ? $_GET['exchange_rate'] : ( isset($_POST['exchange_rate'] ) ? $_POST['exchange_rate'] :'');
        $transaction_number = isset ($_GET['transaction_number']) ? $_GET['transaction_number'] : ( isset($_POST['transaction_number'] ) ? 
		$_POST['transaction_number'] :'');
        $party_id = isset ($_GET['party_id']) ? $_GET['party_id'] : ( isset($_POST['party_id'] ) ? $_POST['party_id'] :'');
        $vendor_bank_id = isset ($_GET['vendor_bank_id']) ? $_GET['vendor_bank_id'] : ( isset($_POST['vendor_bank_id'] ) ? $_POST['vendor_bank_id'] :'');
		$executive_id = isset ($_GET['executive_id']) ? $_GET['executive_id'] : ( isset($_POST['executive_id'] ) ? $_POST['executive_id'] :'');
        $pamount = isset ($_GET['pamount']) ? $_GET['pamount'] : ( isset($_POST['pamount'] ) ? $_POST['pamount'] :'');
        $company_id = isset ($_GET['company_id']) ? $_GET['company_id'] : ( isset($_POST['company_id'] ) ? $_POST['company_id'] :'');
        $clear_bk_stmt_id = isset ($_GET['clear_bk_stmt_id']) ? $_GET['clear_bk_stmt_id'] : ( isset($_POST['clear_bk_stmt_id'] ) ? $_POST['clear_bk_stmt_id'] :'');
        $clr_type = isset ($_GET['clr_type']) ? $_GET['clr_type'] : ( isset($_POST['clr_type'] ) ? $_POST['clr_type'] :'');
        $linking = isset ($_GET['linking']) ? $_GET['linking'] : ( isset($_POST['linking'] ) ? $_POST['linking'] :'');
		$x		= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])       ? $_POST["x"]       :'');
		$pageno = isset($_GET["pageno"])  ? $_GET["pageno"]     : ( isset($_POST["pageno"])  ? $_POST["pageno"]  :'');
		$rpp 	= isset($_GET["rpp"])     ? $_GET["rpp"]        : ( isset($_POST["rpp"])     ? $_POST["rpp"]     : 20);// Result per page 
		
		if(empty($rpp)){
			$rpp = RESULTS_PER_PAGE; 
		}	   
		$condition_query ='';
		if ( empty($x) ){
			$x              = 1;
			$pageno              = 1;
			$next_record    = 0 ;
		}else {
			$next_record    = ($x-1) * $rpp;
		}
	   	$variables["x"]     = $x;
		$variables["pageno"]     = $pageno;
		$variables["rpp"]   = $rpp;
		
		if(!empty($linking)){
			$messages->setOkMessage("Linking status done properly.");
		} 
		
		//CLEAR LINKING BOF        		
		if(isset($clear_bk_stmt_id) && $clear_bk_stmt_id>0){  
			
			$table = TABLE_PAYMENT_TRANSACTION;
			$condition2 = " WHERE ".TABLE_PAYMENT_TRANSACTION .".id= '".$transaction_id ."' " ;
			$fields1 =  TABLE_PAYMENT_TRANSACTION .'.transaction_type,' ;
			$fields1.=  TABLE_PAYMENT_TRANSACTION .'.debit_bank_stmt_id,' ;
			$fields1.=  TABLE_PAYMENT_TRANSACTION .'.credit_bank_stmt_id,' ;
			$fields1.=  TABLE_PAYMENT_TRANSACTION .'.bank_stmt_status' ;
			$arr = getRecord($table,$fields1,$condition2);
			if(!empty($arr)){
				$clr_transaction_type =  $arr['transaction_type'] ;                    
				$clr_debit_bank_stmt_id =  $arr['debit_bank_stmt_id'] ;                    
				$clr_credit_bank_stmt_id =  $arr['credit_bank_stmt_id'] ;                    
				$clr_bank_stmt_status =  $arr['bank_stmt_status'] ;                    
			}
			if($clr_transaction_type ==Paymenttransaction::PAYMENTIN){
				$clr_sql_status = " bank_stmt_status = '".Paymenttransaction::BLANK ."'," ; 
			}
			if($clr_transaction_type ==Paymenttransaction::PAYMENTOUT){
				$clr_sql_status = " bank_stmt_status = '".Paymenttransaction::BLANK ."'," ; 
			}
			if($clr_transaction_type ==Paymenttransaction::INTERNAL){
				if($clr_type=='debit' ){
					if(empty($clr_credit_bank_stmt_id)){
						$clr_sql_status = " bank_stmt_status = '".Paymenttransaction::BLANK ."'," ; 
					}else{
						$clr_sql_status = " bank_stmt_status = '".Paymenttransaction::PARTIALLINK ."'," ; 
					}
				}
				if($clr_type=='credit' ){
					if(empty($clr_debit_bank_stmt_id)){
						$clr_sql_status = " bank_stmt_status = '".Paymenttransaction::BLANK ."'," ; 
					}else{
						$clr_sql_status = " bank_stmt_status = '".Paymenttransaction::PARTIALLINK ."'," ; 
					}
				}
			}
			if($clr_type=='debit'){				
				 $sql_vh = " UPDATE ".TABLE_PAYMENT_TRANSACTION 
					." SET debit_bank_stmt_id = '',"		            
					." debit_bank_stmt_remark1 = '',"		            
					." debit_bank_stmt_dt = '',"		            
					." debit_status_updated_by = '".$my['user_id']."',"		            
					." debit_status_updated_by_name = '".$my['f_name']." ".$my['l_name']."',"	
					.$clr_sql_status						
					." debit_udt = '".date('Y-m-d')."'"		            
					."  WHERE id =".$transaction_id; 
					$db->query($sql_vh);
						
				 $sql_vh1 = " UPDATE ".TABLE_BANK_STATEMENT 
					." SET client_id = '',"		            
					." executive_id = '',"		            
					." ref_debit_pay_bank_id = '',"		            
					." ref_credit_pay_bank_id = '',"		            
					." transaction_no = '',"		            
					." transaction_id = '',"		            
					." transaction_type = '',"		//2 -out            
					." status_u = '0',"		            
					." linked_by = '".$my['user_id']."',"		            
					." linked_by_name = '".$my['f_name']." ".$my['l_name']."',"		            
					." linked_dt = '".date('Y-m-d')."',"		            
					." linked_ip = '".$_SERVER['REMOTE_ADDR']."'"		            
				."  WHERE id =".$clear_bk_stmt_id; 
				$db->query($sql_vh1); 
			}
			
			if($clr_type=='credit'){
				$sql_vh = " UPDATE ".TABLE_PAYMENT_TRANSACTION 
					." SET credit_bank_stmt_id = '',"		            
					." credit_bank_stmt_remark1 = '',"	
					." credit_bank_stmt_dt = '',"						
					." credit_status_updated_by = '".$my['user_id']."',"		            
					." credit_status_updated_by_name = '".$my['f_name']." ".$my['l_name']."',"	
					.$clr_sql_status						
					." credit_udt = '".date('Y-m-d')."'"		            
					."  WHERE id =".$transaction_id; 
				$db->query($sql_vh);
						
				$sql_vh1 = " UPDATE ".TABLE_BANK_STATEMENT 
					." SET client_id = '',"		            
					." executive_id = '',"		            
					." ref_debit_pay_bank_id = '',"		            
					." ref_credit_pay_bank_id = '',"		            
					." transaction_no = '',"		            
					." transaction_id = '',"		            
					." transaction_type = '',"		       
					." status_u = '0',"		            
					." linked_by = '".$my['user_id']."',"		            
					." linked_by_name = '".$my['f_name']." ".$my['l_name']."',"		            
					." linked_dt = '".date('Y-m-d')."',"		            
					." linked_ip = '".$_SERVER['REMOTE_ADDR']."'"		            
				."  WHERE id =".$clear_bk_stmt_id; 
				$db->query($sql_vh1);
				
			}
		}
		//CLEAR LINKING EOF
		
        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $db1 = new db_local; // database handle bank_stmt_status
		//GET TRA DETAILS BOF
		$fields = TABLE_PAYMENT_TRANSACTION .".transaction_id,".TABLE_PAYMENT_TRANSACTION.".pay_received_amt,
		".TABLE_PAYMENT_TRANSACTION.".pay_cheque_no,".TABLE_PAYMENT_TRANSACTION.".pay_bank_company,"
		.TABLE_PAYMENT_TRANSACTION.".transaction_type,".TABLE_PAYMENT_TRANSACTION.".pay_branch,"
		.TABLE_PAYMENT_TRANSACTION.".credit_pay_bank_id,".TABLE_PAYMENT_TRANSACTION.".debit_pay_bank_id,"
		.TABLE_PAYMENT_TRANSACTION.".currency_abbr,".TABLE_PAYMENT_TRANSACTION.".currency_id,"
		.TABLE_PAYMENT_TRANSACTION.".currency_name,".TABLE_PAYMENT_TRANSACTION.".currency_symbol,"
		.TABLE_PAYMENT_TRANSACTION.".currency_country,".TABLE_PAYMENT_TRANSACTION.".exchange_rate,"
		.TABLE_PAYMENT_TRANSACTION.".behalf_vendor_bank,".TABLE_PAYMENT_TRANSACTION.".behalf_client,"
		.TABLE_PAYMENT_TRANSACTION.".behalf_executive,".TABLE_PAYMENT_TRANSACTION.".behalf_company_id,"
		.TABLE_PAYMENT_TRANSACTION.".bank_stmt_status,".TABLE_PAYMENT_TRANSACTION.".debit_bank_stmt_id, "
		.TABLE_PAYMENT_TRANSACTION.".debit_bank_stmt_remark1,".TABLE_PAYMENT_TRANSACTION.".debit_status_updated_by_name," 	 
		.TABLE_PAYMENT_TRANSACTION.".debit_status_updated_by,".TABLE_PAYMENT_TRANSACTION.".debit_udt," 			 
		.TABLE_PAYMENT_TRANSACTION.".credit_bank_stmt_id,"	 
		.TABLE_PAYMENT_TRANSACTION.".credit_bank_stmt_remark1,"	 
		.TABLE_PAYMENT_TRANSACTION.".credit_status_updated_by_name," 	 
		.TABLE_PAYMENT_TRANSACTION.".credit_status_updated_by," 	 
		.TABLE_PAYMENT_TRANSACTION.".credit_udt,"
		.TABLE_PAYMENT_TRANSACTION.".id,"
		.TABLE_PAYMENT_TRANSACTION.".client_id,"
		.TABLE_PAYMENT_TRANSACTION.".executive_id,"
		.TABLE_PAYMENT_TRANSACTION.".ref_debit_pay_bank_id,"
		.TABLE_PAYMENT_TRANSACTION.".ref_credit_pay_bank_id,"
		.TABLE_PAYMENT_TRANSACTION.".number,"
		.TABLE_USER .".f_name as efname,".TABLE_USER .".l_name as elname,"
        .TABLE_CLIENTS .".f_name as cfname,".TABLE_CLIENTS .".l_name as clname,"
		.TABLE_CLIENTS .".billing_name as cbilling_name" ;
		//.TABLE_VENDORS .".f_name as fname, ". TABLE_VENDORS .".l_name as lname, ". TABLE_VENDORS .".billing_name";
        $condition_query_tra = " WHERE ".TABLE_PAYMENT_TRANSACTION.".id=".$transaction_id."";
		Paymenttransaction::getDetails( $db, $traDetails, $fields, $condition_query_tra);
		if(!empty($traDetails)){
			$traDetails=$traDetails[0];
			$currency_id = $traDetails['currency_id'];
			$exchange_rate = $traDetails['exchange_rate'];
			$behalf_vendor_bank = $traDetails['behalf_vendor_bank'];
			$behalf_client 		= $traDetails['behalf_client'];
			$behalf_executive 	= $traDetails['behalf_executive'];
			$behalf_company_id 	= $traDetails['behalf_company_id'];
			if( $traDetails['transaction_type'] ==Paymenttransaction::PAYMENTIN ){
				/*******************************************
				SELECTING DEFAULT BANK ID = CREDIT_BANK_ID  
				eg. By Default - Displaying Bank Statement List Of - DEBIT_BANK_ID IN CASE OF PAYMENTIN
				DO NOT REMOVE THIS
				********************************************/
				$pay_bank_id = $credit_pay_bank_id = $traDetails['credit_pay_bank_id']; 
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$traDetails['credit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $credit_bank_name = $bankname = $banknameArr['bank_name'] ;                    
                }
                $traDetails['transaction_typename']='Payment In';
                $traDetails['bankname']= $bankname;
                $traDetails['credit_bank_name']= $credit_bank_name;
				
            }elseif($traDetails['transaction_type'] ==Paymenttransaction::PAYMENTOUT){
				
				/*******************************************
				SELECTING DEFAULT BANK ID = DEBIT_BANK_ID  
				eg. By Default - Displaying Bank Statement List Of - DEBIT_BANK_ID IN CASE OF PAYMENTOUT
				DO NOT REMOVE THIS
				********************************************/
				$pay_bank_id = $debit_pay_bank_id = $traDetails['debit_pay_bank_id'];
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$traDetails['debit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                   $debit_bank_name = $bankname = $banknameArr['bank_name'] ;                    
                }
                $traDetails['transaction_typename']='Payment Out';
				$traDetails['bankname']= $bankname;
				$traDetails['debit_bank_name']= $debit_bank_name;
            }elseif($traDetails['transaction_type'] ==Paymenttransaction::INTERNAL){
				$credit_pay_bank_id = $traDetails['credit_pay_bank_id'];
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$traDetails['credit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $bankname1 = $banknameArr['bank_name'] ;                    
                }
                $traDetails['credit_bank_name']= $bankname1;
				
				/*******************************************
				SELECTING DEFAULT BANK ID = DEBIT_BANK_ID  
				eg. By Default - Displaying Bank Statement List Of - DEBIT_BANK_ID IN CASE OF INTERNAL
				DO NOT REMOVE THIS
				********************************************/
				$debit_pay_bank_id = $traDetails['debit_pay_bank_id'];
				$table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$traDetails['debit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $bankname2 = $banknameArr['bank_name'] ;                    
                }
				$traDetails['debit_bank_name']= $bankname2; 
                $traDetails['transaction_typename']= 'INTERNAL';
				$traDetails['bankname']= "Payment IN : ".$bankname1." Payment OUT:".$bankname2; 
				if(empty($traDetails['debit_bank_stmt_id'])){
					$pay_bank_id =$traDetails['debit_pay_bank_id']  ;
				}
				if(empty($traDetails['credit_bank_stmt_id'])){
					$pay_bank_id =$traDetails['credit_pay_bank_id']  ;
				} 
			} 
		}
	    
	if($traDetails['bank_stmt_status']!=Paymenttransaction::COMPLETEDLINK){	 
		 
        if(array_key_exists('allot_bills',$_POST)){
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST); 
			
			if(!isset($data['link_status'])){ 
				$messages->setOkMessage("Select Bank Statement to link with the transaction."); 
			}else{
				
				if( $traDetails['transaction_type'] ==Paymenttransaction::PAYMENTIN ){
					
					$table = TABLE_BANK_STATEMENT;
					$condition2 = " WHERE ".TABLE_BANK_STATEMENT .".id= '".$data['link_status'] ."' " ;
					$fields1 =  TABLE_BANK_STATEMENT .'.bank_id,
					'.TABLE_BANK_STATEMENT .'.description,
					'.TABLE_BANK_STATEMENT .'.multi_link_count_balance,
					'.TABLE_BANK_STATEMENT .'.multi_link_count,
					'.TABLE_BANK_STATEMENT .'.stmt_date' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$stmt_bank_id = $banknameArr['bank_id'] ;                    
						$description = $banknameArr['description'] ;                    
						$stmt_date = $banknameArr['stmt_date'] ;                    
						$multi_link_count_balance = $banknameArr['multi_link_count_balance'] ;                    
					} 
					if($multi_link_count_balance>0){
						if($stmt_bank_id  == $traDetails['credit_pay_bank_id']){ 
							$sql_status1 = " bank_stmt_status = '".Paymenttransaction::COMPLETEDLINK ."'," ; 
							$sql_vh = " UPDATE ".TABLE_PAYMENT_TRANSACTION 
								." SET credit_bank_stmt_id = '".$data['link_status']."',"
								." credit_bank_stmt_remark1 = '".$description."',"		            
								." credit_bank_stmt_dt = '".$stmt_date."',"		            
								." credit_status_updated_by = '".$my['user_id']."',"		            
								." credit_status_updated_by_name = '".$my['f_name']." ".$my['l_name']."',"		            
								.$sql_status1
								." credit_udt = '".date('Y-m-d')."'"		            
							."  WHERE id =".$traDetails['id']; 
							$db->query($sql_vh);  
							
							$multi_link_count_balance = $multi_link_count_balance -1 ;
							if($multi_link_count_balance==0){
								$sql_status2  = " status_u = '".BankStatement::LINKED."'," ;
							}else{
								$sql_status2  = " status_u = '".BankStatement::PARTIALLINKED."'," ;
							}
							$sql_vh1 = " UPDATE ".TABLE_BANK_STATEMENT 
							." SET "	
								." client_id = CONCAT( client_id, ',".$traDetails['client_id']."' ),"								
								." executive_id = CONCAT( executive_id, ',".$traDetails['team_id']."' ),"								
								." ref_debit_pay_bank_id = CONCAT( ref_debit_pay_bank_id, ',".$ref_debit_pay_bank_id."' ),"
								." ref_credit_pay_bank_id = CONCAT( ref_credit_pay_bank_id, ',".$ref_credit_pay_bank_id."' )," 
								." transaction_no = CONCAT( transaction_no, ',".$traDetails['number']."' )," 
								." transaction_id = CONCAT( transaction_id, ',".$traDetails['id']."' )," 
								." multi_link_count_balance = ".$multi_link_count_balance.","  
								." transaction_type = '1',"		//IN -1             
								.$sql_status2	            
								." linked_by = '".$my['user_id']."',"		            
								." linked_by_name = '".$my['f_name']." ".$my['l_name']."',"		            
								." linked_dt = '".date('Y-m-d')."',"		            
								." linked_ip = '".$_SERVER['REMOTE_ADDR']."'"		            
							."  WHERE id =".$data['link_status']; 
							$db->query($sql_vh1);
							
							//insert into one more table bof 
							$sql3	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION_BANK_STMT
								." SET ". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".transaction_id = '". $traDetails['id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".transaction_no = '".$traDetails['number'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".bank_stmt_id = '".$data['link_status'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".client_id = '".$traDetails['client_id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".executive_id = '".$traDetails['team_id'] ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ref_debit_pay_bank_id = '".$ref_debit_pay_bank_id ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ref_credit_pay_bank_id = '".$ref_credit_pay_bank_id ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ip     = '". $_SERVER['REMOTE_ADDR'] ."'"        
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".linked_by = '".$my['user_id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".linked_by_name = '".$my['f_name']." ".$my['l_name']."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".do_link   = '".date('Y-m-d H:i:s')."'";
							$db->query($sql3); 
							//insert into one more table eof 
							
							header("Location:".DIR_WS_NC."/payment-transaction.php?perform=allot_bk_stmt&transaction_id=".$data['transaction_id']."&transaction_number=".$transaction_number."&exchange_rate=".$exchange_rate."&party_id=".$party_id."&executive_id=".$executive_id."&pamount=".$pamount."&company_id=".$company_id."&linking=done"); 
						} 
					}else{
						$messages->setErrorMessage("This transaction and bank statement is linked completely.");
					}
			    }elseif($traDetails['transaction_type'] ==Paymenttransaction::PAYMENTOUT){
					$table = TABLE_BANK_STATEMENT;
					$condition2 = " WHERE ".TABLE_BANK_STATEMENT .".id= '".$data['link_status'] ."' " ;
					$fields1 =  TABLE_BANK_STATEMENT .'.bank_id,
					'.TABLE_BANK_STATEMENT .'.description,
					'.TABLE_BANK_STATEMENT .'.multi_link_count_balance,
					'.TABLE_BANK_STATEMENT .'.multi_link_count,
					'.TABLE_BANK_STATEMENT .'.stmt_date' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$stmt_bank_id = $banknameArr['bank_id'] ;                    
						$description = $banknameArr['description'] ;                    
						$stmt_date = $banknameArr['stmt_date'] ;     
						$multi_link_count_balance = $banknameArr['multi_link_count_balance'] ;  						
					}
					if($multi_link_count_balance>0){					
						if($stmt_bank_id  == $traDetails['debit_pay_bank_id']){
							
							$sql_status1 = " bank_stmt_status = '".Paymenttransaction::COMPLETEDLINK ."'," ;
							 
							$sql_vh = " UPDATE ".TABLE_PAYMENT_TRANSACTION 
							." SET debit_bank_stmt_id = '".$data['link_status']."',"		            
							." debit_bank_stmt_remark1 = '".$description."',"		            
							." debit_bank_stmt_dt = '".$stmt_date."',"		            
							." debit_status_updated_by = '".$my['user_id']."',"		            
							." debit_status_updated_by_name = '".$my['f_name']." ".$my['l_name']."',"	
							.$sql_status1						
							." debit_udt = '".date('Y-m-d')."'"		            
							."  WHERE id =".$traDetails['id']; 
							$db->query($sql_vh);
							  
							$multi_link_count_balance = $multi_link_count_balance -1 ;
							if($multi_link_count_balance==0){
								$sql_status2  = " status_u = '".BankStatement::LINKED."'," ;
							}else{
								$sql_status2  = " status_u = '".BankStatement::PARTIALLINKED."'," ;
							}
							$sql_vh1 = " UPDATE ".TABLE_BANK_STATEMENT 
							." SET "	
								." client_id = CONCAT( client_id, ',".$traDetails['client_id']."' ),"								
								." executive_id = CONCAT( executive_id, ',".$traDetails['team_id']."' ),"								
								." ref_debit_pay_bank_id = CONCAT( ref_debit_pay_bank_id, ',".$ref_debit_pay_bank_id."' ),"
								." ref_credit_pay_bank_id = CONCAT( ref_credit_pay_bank_id, ',".$ref_credit_pay_bank_id."' )," 
								." transaction_no = CONCAT( transaction_no, ',".$traDetails['number']."' )," 
								." transaction_id = CONCAT( transaction_id, ',".$traDetails['id']."' )," 
								." multi_link_count_balance = ".$multi_link_count_balance.","  
								." transaction_type = '2',"		//IN -1             
								.$sql_status2	            
								." linked_by = '".$my['user_id']."',"		            
								." linked_by_name = '".$my['f_name']." ".$my['l_name']."',"		            
								." linked_dt = '".date('Y-m-d')."',"		            
								." linked_ip = '".$_SERVER['REMOTE_ADDR']."'"		            
							."  WHERE id =".$data['link_status']; 
							$db->query($sql_vh1);
							
							//insert into one more table bof 
							$sql3	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION_BANK_STMT
								." SET ". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".transaction_id = '". $traDetails['id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".transaction_no = '".$traDetails['number'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".bank_stmt_id = '".$data['link_status'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".client_id = '".$traDetails['client_id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".executive_id = '".$traDetails['team_id'] ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ref_debit_pay_bank_id = '".$ref_debit_pay_bank_id ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ref_credit_pay_bank_id = '".$ref_credit_pay_bank_id ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ip     = '". $_SERVER['REMOTE_ADDR'] ."'"        
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".linked_by = '".$my['user_id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".linked_by_name = '".$my['f_name']." ".$my['l_name']."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".do_link   = '".date('Y-m-d H:i:s')."'";
							$db->query($sql3); 
							//insert into one more table eof  
							
							header("Location:".DIR_WS_NC."/payment-transaction.php?perform=allot_bk_stmt&transaction_id=".$data['transaction_id']."&transaction_number=".$transaction_number."&exchange_rate=".$exchange_rate."&party_id=".$party_id."&executive_id=".$executive_id."&pamount=".$pamount."&company_id=".$company_id."&linking=done"); 
						}
					}else{
						$messages->setErrorMessage("This transaction and bank statement is linked completely.");
					} 
				
				}elseif($traDetails['transaction_type'] ==Paymenttransaction::INTERNAL){ 
				
					$table = TABLE_BANK_STATEMENT;
					$condition2 = " WHERE ".TABLE_BANK_STATEMENT .".id= '".$data['link_status'] ."' " ;
					$fields1 =  TABLE_BANK_STATEMENT .'.bank_id,
					'.TABLE_BANK_STATEMENT .'.description,
					'.TABLE_BANK_STATEMENT .'.multi_link_count_balance,
					'.TABLE_BANK_STATEMENT .'.multi_link_count,
					'.TABLE_BANK_STATEMENT .'.stmt_date' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$stmt_bank_id = $banknameArr['bank_id'] ;                    
						$description = $banknameArr['description'] ;                    
						$stmt_date = $banknameArr['stmt_date'] ; 
						$multi_link_count_balance = $banknameArr['multi_link_count_balance'] ;  
					} 
					if($multi_link_count_balance>0){	
						if($stmt_bank_id  == $traDetails['credit_pay_bank_id']){ 
							if(!empty($traDetails['dedit_bank_stmt_id'])){
								$sql_status1 = " bank_stmt_status = '".Paymenttransaction::COMPLETEDLINK ."'," ;
							}else{
								$sql_status1 = " bank_stmt_status = '".Paymenttransaction::PARTIALLINK ."'," ;
							}  
							$sql_vh = " UPDATE ".TABLE_PAYMENT_TRANSACTION 
							." SET credit_bank_stmt_id = '".$data['link_status']."',"		            
							." credit_bank_stmt_remark1 = '".$description."',"		            
							." credit_bank_stmt_dt = '".$stmt_date."',"		            
							." credit_status_updated_by = '".$my['user_id']."',"		            
							." credit_status_updated_by_name = '".$my['f_name']." ".$my['l_name']."',"		            
							.$sql_status1
							." credit_udt = '".date('Y-m-d')."'"		            
							."  WHERE id =".$traDetails['id']; 
							$db->query($sql_vh);	
							
							$multi_link_count_balance = $multi_link_count_balance - 1 ;
							if($multi_link_count_balance==0){
								$sql_status2  = " status_u = '".BankStatement::LINKED."'," ;
							}else{
								$sql_status2  = " status_u = '".BankStatement::PARTIALLINKED."'," ;
							}
							$sql_vh1 = " UPDATE ".TABLE_BANK_STATEMENT 
							." SET "	
								." client_id = CONCAT( client_id, ',".$traDetails['client_id']."' ),"								
								." executive_id = CONCAT( executive_id, ',".$traDetails['team_id']."' ),"								
								." ref_debit_pay_bank_id = CONCAT( ref_debit_pay_bank_id, ',".$ref_debit_pay_bank_id."' ),"
								." ref_credit_pay_bank_id = CONCAT( ref_credit_pay_bank_id, ',".$ref_credit_pay_bank_id."' )," 
								." transaction_no = CONCAT( transaction_no, ',".$traDetails['number']."' )," 
								." transaction_id = CONCAT( transaction_id, ',".$traDetails['id']."' )," 
								." multi_link_count_balance = ".$multi_link_count_balance.","  
								." transaction_type = '1',"		//IN -1             
								.$sql_status2	            
								." linked_by = '".$my['user_id']."',"		            
								." linked_by_name = '".$my['f_name']." ".$my['l_name']."',"		            
								." linked_dt = '".date('Y-m-d')."',"		            
								." linked_ip = '".$_SERVER['REMOTE_ADDR']."'"		            
							."  WHERE id =".$data['link_status']; 
							$db->query($sql_vh1);
							
							//insert into one more table bof 
							$sql3	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION_BANK_STMT
								." SET ". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".transaction_id = '". $traDetails['id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".transaction_no = '".$traDetails['number'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".bank_stmt_id = '".$data['link_status'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".client_id = '".$traDetails['client_id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".executive_id = '".$traDetails['team_id'] ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ref_debit_pay_bank_id = '".$ref_debit_pay_bank_id ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ref_credit_pay_bank_id = '".$ref_credit_pay_bank_id ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ip     = '". $_SERVER['REMOTE_ADDR'] ."'"        
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".linked_by = '".$my['user_id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".linked_by_name = '".$my['f_name']." ".$my['l_name']."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".do_link   = '".date('Y-m-d H:i:s')."'";
							$db->query($sql3); 
							//insert into one more table eof
							
							 header("Location:".DIR_WS_NC."/payment-transaction.php?perform=allot_bk_stmt&transaction_id=".$traDetails['id']."&transaction_number=".$transaction_number."&exchange_rate=".$exchange_rate."&party_id=".$party_id."&executive_id=".$executive_id."&pamount=".$pamount."&company_id=".$company_id."&linking=done");   
							
							
						}elseif($stmt_bank_id  == $traDetails['debit_pay_bank_id']){
						
							if(!empty($traDetails['credit_bank_stmt_id'])){
								$sql_status1 = " bank_stmt_status = '".Paymenttransaction::COMPLETEDLINK ."'," ;
							}else{
								$sql_status1 = " bank_stmt_status = '".Paymenttransaction::PARTIALLINK ."'," ;
							} 
							$sql_vh = " UPDATE ".TABLE_PAYMENT_TRANSACTION 
							." SET debit_bank_stmt_id = '".$data['link_status']."',"		            
							." debit_bank_stmt_remark1 = '".$description."',"	
							." debit_bank_stmt_dt 	= '".$stmt_date."',"							
							." debit_status_updated_by = '".$my['user_id']."',"		            
							." debit_status_updated_by_name = '".$my['f_name']." ".$my['l_name']."',"	
							.$sql_status1						
							." debit_udt = '".date('Y-m-d')."'"		            
							."  WHERE id =".$traDetails['id']; 
							$db->query($sql_vh); 
							
							$multi_link_count_balance = $multi_link_count_balance -1 ;
							if($multi_link_count_balance==0){
								$sql_status2  = " status_u = '".BankStatement::LINKED."'," ;
							}else{
								$sql_status2  = " status_u = '".BankStatement::PARTIALLINKED."'," ;
							}
							$sql_vh1 = " UPDATE ".TABLE_BANK_STATEMENT 
							." SET "	
								." client_id = CONCAT( client_id, ',".$traDetails['client_id']."' ),"								
								." executive_id = CONCAT( executive_id, ',".$traDetails['team_id']."' ),"								
								." ref_debit_pay_bank_id = CONCAT( ref_debit_pay_bank_id, ',".$ref_debit_pay_bank_id."' ),"
								." ref_credit_pay_bank_id = CONCAT( ref_credit_pay_bank_id, ',".$ref_credit_pay_bank_id."' )," 
								." transaction_no = CONCAT( transaction_no, ',".$traDetails['number']."' )," 
								." transaction_id = CONCAT( transaction_id, ',".$traDetails['id']."' )," 
								." multi_link_count_balance = ".$multi_link_count_balance.","  
								." transaction_type = '2',"		//IN -1             
								.$sql_status2	            
								." linked_by = '".$my['user_id']."',"		            
								." linked_by_name = '".$my['f_name']." ".$my['l_name']."',"		            
								." linked_dt = '".date('Y-m-d')."',"		            
								." linked_ip = '".$_SERVER['REMOTE_ADDR']."'"		            
							."  WHERE id =".$data['link_status']; 
							$db->query($sql_vh1);
							
							//insert into one more table bof 
							$sql3	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION_BANK_STMT
								." SET ". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".transaction_id = '". $traDetails['id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".transaction_no = '".$traDetails['number'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".bank_stmt_id = '".$data['link_status'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".client_id = '".$traDetails['client_id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".executive_id = '".$traDetails['team_id'] ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ref_debit_pay_bank_id = '".$ref_debit_pay_bank_id ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ref_credit_pay_bank_id = '".$ref_credit_pay_bank_id ."'" 
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".ip     = '". $_SERVER['REMOTE_ADDR'] ."'"        
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".linked_by = '".$my['user_id'] ."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".linked_by_name = '".$my['f_name']." ".$my['l_name']."'"
								.",". TABLE_PAYMENT_TRANSACTION_BANK_STMT .".do_link   = '".date('Y-m-d H:i:s')."'";
							$db->query($sql3); 
							//insert into one more table eof 
							
							
							  header("Location:".DIR_WS_NC."/payment-transaction.php?perform=allot_bk_stmt&transaction_id=".$traDetails['id']."&transaction_number=".$transaction_number."&exchange_rate=".$exchange_rate."&party_id=".$party_id."&executive_id=".$executive_id."&pamount=".$pamount."&company_id=".$company_id."&linking=done");   
						}
					}else{
						$messages->setErrorMessage("This transaction and bank statement is linked completely.");
					}
				}  
			}
			    
        }
		
		 
			
		//Search BOF 
			$sString1    = isset($_GET["sString1"])   ? $_GET["sString1"]  : ( isset($_POST["sString1"])    ? 
					$_POST["sString1"] : '' );
			$sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? 
			$_POST["sType"]   : '' );
			$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : 'DESC' );
			$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: 'stmt_date' );
			$condition_query_2 = isset($_POST["condition_query"]) ? $_POST["condition_query"] : ( isset($_GET["condition_query"]) ? $_GET["condition_query"]:''); 
			$_SEARCH['pay_bank_id'] = isset($_GET["pay_bank_id"]) ? $_GET["pay_bank_id"] :( isset($_POST["pay_bank_id"]) ? $_POST["pay_bank_id"] : $pay_bank_id );
			$condition_url  = "&transaction_id=".$traDetails['id']."&transaction_number=".$transaction_number."&exchange_rate=".$exchange_rate."&party_id=".$party_id."&executive_id=".$executive_id."&pamount=".$pamount."&company_id=".$company_id;
			$where_added    = true;
		 
			$searchStr = 0;
			if ( !empty($sString1)  ) 
			{
				$searchStr = 1;
				
				 // Set the field on which to make the search.
				if ( !($search_table = findIndex($sType, $sTypeArray1 )) ) {
					$sType = "";
					$search_table = NULL ;
				}
			   
				//$where_added = true;
				$sub_query='';        
				if ( $search_table == "Any" ) 
				{
					
					if ( $where_added ) 
					{
						$condition_query_2 .= "AND ";
					}
					else 
					{
						$condition_query_2 .= " WHERE ";
						$where_added = true;
					}
				  
					foreach( $sTypeArray1 as $table => $field_arr ){
					  
						if ( $table != "Any" ) 
						{ 
							
							if($table == TABLE_BANK_STATEMENT){
								$sub_query .= " ( " ;
								foreach( $field_arr as $key => $field ){
									
									$sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString1)."%' OR " ;
									
								}
								$sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
								$sub_query .= " ) " ;
								
								if ( $where_added ){
									$sub_query .= "OR ";
								} else {
									$sub_query .= " WHERE ";
									$where_added = true;
								}
							
							} 
							
						}
					}
					$sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
					$condition_query_2 .="(".$sub_query.")" ;
					
				}else{  
						
						if ( $where_added ){
							$sub_query .= " AND ";
						}else{
							$sub_query .= " WHERE ";
							$where_added = true;
						}
						$sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE 
						'%".trim($sString) ."%' )" ; 
					   $condition_query_2 .= $sub_query  ;
				}
				
				$_SEARCH["sString1"] = $sString1;
				$_SEARCH["sType"] 	= $sType;
				$condition_url      .= "&sString1=$sString1&sType=$sType";
			}    
			//code for search EOF 2009-03-mar-21  
			 
			// Read the Date data.
			$chk_date_from  = isset($_POST["chk_date_from"]) ? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
			$date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
			$chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
			$date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
			$dt_field = isset($_POST["dt_field"])? $_POST["dt_field"] : (isset($_GET["dt_field"]) ? $_GET["dt_field"]  :''); 
		 
			// BO: From Date
			if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from) ){
				$searchStr = 1;
				if ( $where_added ) {
					$condition_query_2 .= " ". $chk_date_from;
				}
				else {
					$condition_query_2.= ' WHERE ';
					$where_added    = true;
				}
				$dfa = explode('/', $date_from);
				$dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
				$condition_query_2 .= " ". TABLE_BANK_STATEMENT .".".$dt_field." >= '". $dfa ."'";        
				$condition_url  .= "&chk_date_from=$chk_date_from&date_from=$date_from";
				$_SEARCH["chk_date_from"]   = $chk_date_from;
				$_SEARCH["date_from"]       = $date_from;
				$_SEARCH["dt_field"]       = $dt_field;
			} 
			// EO: From Date
			
			// BO: Upto Date
			if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
				$searchStr = 1;
				if ( $where_added ) {
					$condition_query_2 .= $chk_date_to;
				}
				else {
					$condition_query_2.= ' WHERE ';
					$where_added    = true;
				}
				$dta = explode('/', $date_to);
				$dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
				$condition_query_2 .= " ". TABLE_BANK_STATEMENT .".".$dt_field." <= '". $dta ."'";
				$condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
				$_SEARCH["chk_date_to"] = $chk_date_to ;
				$_SEARCH["date_to"]     = $date_to ;
				$_SEARCH["dt_field"]       = $dt_field;
			}    
			// EO: Upto Date
			if(!empty($_SEARCH['pay_bank_id'])){
				$condition_query_2 .= " AND (".TABLE_BANK_STATEMENT.".bank_id = '".$_SEARCH['pay_bank_id']."' )";
				$condition_url   .= "&pay_bank_id=".$_SEARCH['pay_bank_id'];
			}  
		//Search EOF  
		/* 
		if(empty($condition_query_2)){
			if(!empty($credit_pay_bank_id) && empty($debit_pay_bank_id)){
				$_SEARCH['pay_bank_id'] = $credit_pay_bank_id;
				$condition_query_2 = " AND (".TABLE_BANK_STATEMENT.".bank_id = '".$credit_pay_bank_id."' )";
			}elseif(!empty($debit_pay_bank_id) && empty($credit_pay_bank_id)){
				$_SEARCH['pay_bank_id'] = $debit_pay_bank_id;
				$condition_query_2 = " AND (".TABLE_BANK_STATEMENT.".bank_id = '".$debit_pay_bank_id."')";
			}elseif(!empty($credit_pay_bank_id) && !empty($debit_pay_bank_id)){
				$_SEARCH['pay_bank_id'] = $credit_pay_bank_id;
				$condition_query_2 = " AND (".TABLE_BANK_STATEMENT.".bank_id = '".$credit_pay_bank_id."' )";
			}
		}
		*/
		$order_by_table = TABLE_BANK_STATEMENT;
		$condition_query_stmt = " WHERE ".TABLE_BANK_STATEMENT.".status='1' AND ".TABLE_BANK_STATEMENT.".status_u!='1' ".$condition_query_2 ; 
		$condition_query_stmt .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
	
		$total	=	BankStatement::getList( $db, $list, '', $condition_query_stmt);
		if ( isset($condition_url) && !empty($condition_url) ) {
			$extra_url  = $condition_url;
		}
		$condition_url .="&perform=".$perform; 
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra'); 
		$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  = '&start=url'. $extra_url .'&end=url';        
		$list	= NULL;
		$fields = TABLE_BANK_STATEMENT.".* 
			,".TABLE_PAYMENT_BANK.".bank_name 
			,".TABLE_PAYMENT_BANK.".company_name " ; 
		BankStatement::getList( $db, $list, $fields, $condition_query_stmt, $next_record, $rpp); 
    }else{
		$messages->setOkMessage("This transaction is Liked Completed with Bank Statment."); 
	}  
	
	$variables['can_clear_linking'] = false;
	if ( $perm->has('nc_p_pt_bk_stmt_clr') ) {
		$variables['can_clear_linking'] = true;
	}
		$variables['completedlink']= Paymenttransaction::COMPLETEDLINK;
        $hidden[] = array('name'=> 'perform' ,'value' => 'allot_bk_stmt');
        $hidden[] = array('name'=> 'exchange_rate' , 'value' => $exchange_rate);    
        $hidden[] = array('name'=> 'transaction_id' , 'value' => $transaction_id);    
        $hidden[] = array('name'=> 'transaction_number' , 'value' => $transaction_number);    
        $hidden[] = array('name'=> 'pamount' , 'value' => $pamount);    
        $hidden[] = array('name'=> 'totalBalance' , 'value' => $totalBalance);    
        $hidden[] = array('name'=> 'party_id' , 'value' => $party_id);    
        $hidden[] = array('name'=> 'company_id' , 'value' => $company_id);    
        $hidden[] = array('name'=> 'executive_id' , 'value' => $executive_id);    
		$hidden[] = array('name'=> 'vendor_bank_id' , 'value' => $vendor_bank_id);  
		
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => 'sTypeArray1', 'value' => 'sTypeArray1');
        $page["var"][] = array('variable' => 'sOrderByArray1', 'value' => 'sOrderByArray1');
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'traDetails', 'value' => 'traDetails');
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'exchange_rate', 'value' => 'exchange_rate');
        $page["var"][] = array('variable' => 'transaction_id', 'value' => 'transaction_id');
        $page["var"][] = array('variable' => 'transaction_number', 'value' => 'transaction_number');
        $page["var"][] = array('variable' => 'pamount', 'value' => 'pamount');
        $page["var"][] = array('variable' => 'totalAmt', 'value' => 'totalAmt');
        $page["var"][] = array('variable' => 'totalBalance', 'value' => 'totalBalance');
        $page["var"][] = array('variable' => 'party_id', 'value' => 'party_id');
        $page["var"][] = array('variable' => 'executive_id', 'value' => 'executive_id');
        $page["var"][] = array('variable' => 'vendor_bank_id', 'value' => 'vendor_bank_id');
        $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-allot-bank-stmt.html');            
        
    } else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
        
    }
?>
