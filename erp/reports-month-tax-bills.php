<?php
if ( $perm->has('nc_rp_txbl') ) {
		include_once ( DIR_FS_INCLUDES .'/payment-party-bills.inc.php' );
		include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
		include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');        
		include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');        
		include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php');  
		include_once ( DIR_FS_INCLUDES .'/company.inc.php');     		
		// Read the Date data.
        
        $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]   : (isset($_GET["date_from"])    
		? $_GET["date_from"]    :'');
        $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
        $tax1_id = isset($_POST["tax1_id"])      ? $_POST["tax1_id"]         : (isset($_GET["tax1_id"])      ? $_GET["tax1_id"]      :'');
        $bill_type        = isset($_POST["bill_type"]) ? $_POST["bill_type"]  : (isset($_GET["bill_type"])      ? $_GET["bill_type"]      :'');
        $report_type  = isset($_POST["report_type"]) ? $_POST["report_type"]  : (isset($_GET["report_type"])  ? $_GET["report_type"]      :'1');  //default is purchase bills 
		
		$company_id  = isset($_POST["company_id"]) ? $_POST["company_id"]  : (isset($_GET["company_id"])  ? $_GET["company_id"]      :'0');
		
		$tax_amt  = isset($_POST["tax_amt"]) ? $_POST["tax_amt"]  : (isset($_GET["tax_amt"])  ? $_GET["tax_amt"]      :'');
		
		//total_tax_balance_amount 
		
		$condition_query2 = $condition_url ='';
        $where_added =true;  
		$totalSum=$taxname=''	;	
        $_ALL_POST 	=$_SEARCH= $_POST;
        $list=$ntlist=null;    
		 
		 //ST - 1 ;EC-3 ;SHE -4;VAT - 2 ; CST - 5
		 
		//DB TAX IDS FOR REFERENCE
		/* 
		$ST = TAX_ST_ID;
		$EC = TAX_EC_ID;
		$SHE = TAX_SHE_ID;
		$VAT = TAX_VAT_ID;
		$CST = TAX_CST_ID;
				 
		$ST_10_mFct= 10; // Multiple factor
		$ST_10_dFct= 110.30; // Division factor
		$EC_2_mFct= 0.2;
		$EC_2_dFct= 110.30;
		$SHE_1_mFct= 0.1;
		$SHE_1_dFct= 110.30;
		
		
		$VAT_5_pFct= '5%'; //Percent factor
		$VAT_5_mFct= '5';  //Multiple factor
		$VAT_5_dFct= '105'; //Division factor
		$VAT_12_5_pFct= '12.5%'; //Percent factor
		$VAT_12_5_mFct= '12.5'; // Multiple factor
		$VAT_12_5_dFct= '125'; // Division factor
		
		$CST_5_pFct= '5%'; //Percent factor
		$CST_5_mFct= '5'; // Multiple factor
		$CST_5_dFct= '105'; // Division factor
		$CST_12_5_pFct= '12.5%'; //Percent factor
		$CST_12_5_mFct= '12.5'; // Multiple factor
		$CST_12_5_dFct= '125'; // Division factor */
		
		
		$lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                   .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
		
		if ( !isset($_SEARCH) ) {
			$_SEARCH = '';
		} 
		// print_R( $_ALL_POST);
		$tax_list    = NULL;
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=0 ORDER BY tax_name ASC";
		ServiceTax::getList($db, $tax_list, 'id,tax_name', $condition_queryst);
		$show=0;
        if(array_key_exists('submit_search',$_POST)){
			 
			$show=1;
            if(empty($date_from) || empty($date_to) ){        
              $messages->setErrorMessage('Please select From Date and To Date.');
            } 
			
			if(empty($tax1_id)){        
              $messages->setErrorMessage('Please select Tax.');
            } 
			if(empty($company_id)){        
              $messages->setErrorMessage('Please select Company.');
            }
			/* 
			if(empty($bill_type)){        
              $messages->setErrorMessage('Please select GOODS or SERVICES.');
            } 
			*/
        }    
            
   
    
    //As validate both dates
    if($messages->getErrorMessageCount() <= 0 && $show==1 ){
    
        $list	= $dateArr=	NULL;   
		$where_added_1 = true;
		$where_added_2 = true;
		$condition_query_1 = " " ;
        $condition_query_2 = " " ;
		
	     	
	    /* if( $report_type ==1){		
			$where_added_1 = false;
		}elseif($report_type == 2){
			$where_added_2 = true;
		}elseif($report_type==3){
			$where_added_1 = false;
			$where_added_2 = true;
		}   */
		
		
        // BO: From Date
        if ( !empty($date_from)){
            if ( $where_added_1 ) {
                $condition_query_1 .= " AND " ;
                
            } else {
                $condition_query_1.= ' WHERE ';
                $where_added_1    = true;
            }
			if ( $where_added_2 ) {
                $condition_query_2 .= " AND " ;
                
            } else {
                $condition_query_2.= ' WHERE ';
                $where_added_2    = true;
            }
			
            $dfa = explode('/', $date_from);
            $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
			
            if( $report_type==1){
				$condition_query_1 .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_dt >= '". $dfa ."' ";
            }elseif($report_type==2){
				$condition_query_2 .= " ". TABLE_BILL_RCPT .".do_voucher >= '". $dfa ."' ";
				
				/* 
				if($tax1_id==$VAT){
					$condition_query_2 .= " ". TABLE_BILL_INV_PROFORMA .".do_i >= '". $dfa ."' ";
				}else{
					$condition_query_2 .= " ". TABLE_BILL_RCPT .".do_voucher >= '". $dfa ."' ";
				} */
			}elseif($report_type==3){
				$condition_query_1 .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_dt >= '". $dfa ."' ";
				$condition_query_2 .= " ". TABLE_BILL_RCPT .".do_voucher >= '". $dfa ."' ";
				/* 
				if($tax1_id==$VAT){
					$condition_query_2 .= " ". TABLE_BILL_INV_PROFORMA .".do_i >= '". $dfa ."' ";
				}else{
					$condition_query_2 .= " ". TABLE_BILL_RCPT .".do_voucher >= '". $dfa ."' ";
				} */
			}
			
            $condition_url   .= "&date_from=$date_from";
          
        }
        // EO: From Date
        
        // BO: Upto Date
        if ( !empty($date_to)) {
            if ( $where_added_1 ) {
                $condition_query_1 .= " AND " ;
                
            } else {
                $condition_query_1.= ' WHERE ';
                $where_added_1    = true;
            }
			if ( $where_added_2 ) {
                $condition_query_2 .= " AND " ;
                
            } else {
                $condition_query_2.= ' WHERE ';
                $where_added_2    = true;
            }
            $dta = explode('/', $date_to);
            $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
			
            if( $report_type==1){
				$condition_query_1 .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_dt <= '". $dta ."' ";
            }elseif($report_type==2 ){
				$condition_query_2 .= " ". TABLE_BILL_RCPT .".do_voucher <= '". $dta ."' ";
				/* if($tax1_id==$VAT){
					$condition_query_2 .= " ". TABLE_BILL_INV_PROFORMA .".do_i <= '". $dta ."' ";
				}else{
					$condition_query_2 .= " ". TABLE_BILL_RCPT .".do_voucher <= '". $dta ."' ";
				
				} */
				
			}elseif($report_type==3){
				$condition_query_1 .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_dt <= '". $dta ."' ";
				$condition_query_2 .= " ". TABLE_BILL_RCPT .".do_voucher <= '". $dta ."' ";
				/* if($tax1_id==$VAT){
					$condition_query_2 .= " ". TABLE_BILL_INV_PROFORMA .".do_i <= '". $dta ."' ";
				}else{
					$condition_query_2 .= " ". TABLE_BILL_RCPT .".do_voucher <= '". $dta ."' ";
				} */
			}
            $condition_url          .= "&date_to=$date_to";
         
        }
        // EO: Upto Date
		if(!empty($tax_amt)){
		
			if ( $where_added_2 ) {
                $condition_query_2 .= " AND " ;
				if($tax_amt==1){ //Tax Paid
					$condition_query_2 .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount=0";
				}elseif($tax_amt==2){ //Tax Extra Paid
					$condition_query_2 .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount<0";
				}elseif($tax_amt==3){ //Tax Not Paid
					$condition_query_2 .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount>0";
				}
                
            }else{
                $condition_query_2.= ' WHERE ';
                $where_added_2    = true;
				if($tax_amt==1){ //Tax Paid
					$condition_query_2 .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount=0";
				}elseif($tax_amt==2){ //Tax Extra Paid
					$condition_query_2 .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount<0";
				}elseif($tax_amt==3){ //Tax Not Paid
					$condition_query_2 .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount>0";
				}
            }
		}
		if ( !empty($tax1_id)) {
			
			if ( $where_added_1 ) {
                $condition_query_1 .= " AND " ;
                
            } else {
                $condition_query_1.= ' WHERE ';
                $where_added_1    = true;
            }
			if ( $where_added_2 ) {
                $condition_query_2 .= " AND " ;
                
            } else {
                $condition_query_2.= ' WHERE ';
                $where_added_2    = true;
            }
			
			if( $report_type==1){
				if($tax1_id!=-1){
					$condition_query_1 .= " ".TABLE_PAYMENT_PARTY_BILLS.".tax_ids!='' AND 
					".TABLE_PAYMENT_PARTY_BILLS.".tax_ids LIKE '%,".trim($tax1_id).",%'" ;
				}else{
					$condition_query_1 .= "  ".TABLE_PAYMENT_PARTY_BILLS.".tax_ids='' AND 
					".TABLE_PAYMENT_PARTY_BILLS.".tax_ids LIKE '%,0,%'" ;			
				}
			}elseif($report_type==2){	
				if($tax1_id!=-1){
					$condition_query_2 .= "  ".TABLE_BILL_INV_PROFORMA.".tax_ids!='' AND 
						".TABLE_BILL_INV_PROFORMA.".tax_ids LIKE '%,".trim($tax1_id).",%'" ;
				}else{
					$condition_query_2 .= "  ".TABLE_BILL_INV_PROFORMA.".tax_ids='' AND ".TABLE_BILL_INV_PROFORMA.".tax_ids =',0,'" ;
				}
			}elseif($report_type==3){
			
				if($tax1_id!=-1){
					$condition_query_1 .= "  ".TABLE_PAYMENT_PARTY_BILLS.".tax_ids!='' AND 
					".TABLE_PAYMENT_PARTY_BILLS.".tax_ids LIKE '%,".trim($tax1_id).",%'" ;
					$condition_query_2 .= "  ".TABLE_BILL_INV_PROFORMA.".tax_ids!='' AND 
					".TABLE_BILL_INV_PROFORMA.".tax_ids LIKE '%,".trim($tax1_id).",%'" ;
					
				}else{
					$condition_query_1 .= "  ".TABLE_PAYMENT_PARTY_BILLS.".tax_ids='' AND 
					".TABLE_PAYMENT_PARTY_BILLS.".tax_ids LIKE '%,0,%'" ;	

					$condition_query_2 .= "  ".TABLE_BILL_INV_PROFORMA.".tax_ids='' AND 
					".TABLE_BILL_INV_PROFORMA.".tax_ids =',0,'" ;
				}
			}
			 
		}
		$company_name='';
		if(!empty($company_id)){
			
			if ( $where_added_1 ) {
                $condition_query_1 .= " AND " ;
                
            } else {
                $condition_query_1.= ' WHERE ';
                $where_added_1    = true;
            }
			if ( $where_added_2 ) {
                $condition_query_2 .= " AND " ;
                
            } else {
                $condition_query_2.= ' WHERE ';
                $where_added_2    = true;
            }
			$condition_query_1 .= " ".TABLE_PAYMENT_PARTY_BILLS.".company_id ='".$company_id."'" ;
			$condition_query_2 .= " ".TABLE_BILL_INV_PROFORMA.".company_id = '".$company_id."'" ;
			
			$companyNameArr=array();
			$company_name='';
			$fields = TABLE_SETTINGS_COMPANY .'.id'
            .','. TABLE_SETTINGS_COMPANY .'.name';
			$condition_comp =" WHERE ".TABLE_SETTINGS_COMPANY.".id=".$company_id;
			Company::getList($db,$companyNameArr,$fields,$condition_comp);
			if(!empty($companyNameArr)){
				$companyNameArr=$companyNameArr[0];
				$company_name = $companyNameArr['name'];
			}
		}
		
		/* if ( !empty($bill_type)) {
			$condition_query2 .= " AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_type ='".$bill_type."'" ;
		} */
		
		//CSV Variables BOF
			$csv_terminated = "\n";
			$csv_separator = ",";
			$csv_enclosed = '"';
			$csv_escaped = "\\";
			$schema_insert = '';
			$schema_insert_nt = '';
			$schema_insert1 = '';
			$schema_insert2 = '';
			$grant_total1 = 0; 
		//CSV Variables EOF
		$out_1=$out_2='';
		//PURCHASE BILLS
		if($report_type==1 || $report_type==3){
				/*
				$query="SELECT ".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".party_id,"
					.TABLE_PAYMENT_PARTY_BILLS.".number,"
					.TABLE_PAYMENT_PARTY_BILLS.".amount,"
					.TABLE_PAYMENT_PARTY_BILLS.".bill_dt,".TABLE_PAYMENT_PARTY_BILLS.".bill_amount,
					".TABLE_PAYMENT_BILLS_P.".stot_amount," 
					.TABLE_PAYMENT_BILLS_P.".particulars,"	
					.TABLE_PAYMENT_BILLS_P.".p_amount,"	
					.TABLE_PAYMENT_BILLS_P.".s_quantity,"	
					.TABLE_PAYMENT_BILLS_P.".tax1_id,"	
					.TABLE_PAYMENT_BILLS_P.".tax1_name,".TABLE_PAYMENT_BILLS_P.".tax1_value,"
					.TABLE_PAYMENT_BILLS_P.".tax1_amount,"
					.TABLE_PAYMENT_BILLS_P.".tax1_sub1_name,".TABLE_PAYMENT_BILLS_P.".tax1_sub1_value,
					".TABLE_PAYMENT_BILLS_P.".tax1_sub1_amount,"
					.TABLE_PAYMENT_BILLS_P.".tax1_sub2_name,".TABLE_PAYMENT_BILLS_P.".tax1_sub2_value,
					".TABLE_PAYMENT_BILLS_P.".tax1_sub2_amount,"
					.TABLE_PAYMENT_BILLS_P.".tot_amount FROM ".TABLE_PAYMENT_PARTY_BILLS." LEFT JOIN "
					.TABLE_PAYMENT_BILLS_P." ON ".TABLE_PAYMENT_PARTY_BILLS.".id=".TABLE_PAYMENT_BILLS_P.".bill_id 
					WHERE ".TABLE_PAYMENT_PARTY_BILLS.".status IN ('".PaymentPartyBills::ACTIVE."')					
					".$condition_query_1." 
					ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".number, ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt " ;		
						*/
					$query="SELECT ".TABLE_PAYMENT_PARTY_BILLS.".bill_no,"
					.TABLE_PAYMENT_BILLS_AGAINST.".bill_against," 
					.TABLE_PAYMENT_PARTY_BILLS.".party_id,"
					.TABLE_PAYMENT_PARTY_BILLS.".number,"
					.TABLE_PAYMENT_PARTY_BILLS.".amount,"
					.TABLE_PAYMENT_PARTY_BILLS.".bill_dt,"
					.TABLE_PAYMENT_PARTY_BILLS.".bill_amount,"
					.TABLE_PAYMENT_BILLS_P.".stot_amount," 
					.TABLE_PAYMENT_BILLS_P.".particulars,"	
					.TABLE_PAYMENT_BILLS_P.".p_amount,"	
					.TABLE_PAYMENT_BILLS_P.".s_quantity,"	
					.TABLE_PAYMENT_BILLS_P.".tax1_id,"	
					.TABLE_PAYMENT_BILLS_P.".tax1_name,".TABLE_PAYMENT_BILLS_P.".tax1_value,"
					.TABLE_PAYMENT_BILLS_P.".tax1_amount,"
					.TABLE_PAYMENT_BILLS_P.".tax1_sub1_name,".TABLE_PAYMENT_BILLS_P.".tax1_sub1_value,
					".TABLE_PAYMENT_BILLS_P.".tax1_sub1_amount,"
					.TABLE_PAYMENT_BILLS_P.".tax1_sub2_name,".TABLE_PAYMENT_BILLS_P.".tax1_sub2_value,
					".TABLE_PAYMENT_BILLS_P.".tax1_sub2_amount,"
					.TABLE_PAYMENT_BILLS_P.".tot_amount FROM ".TABLE_PAYMENT_PARTY_BILLS." LEFT JOIN "
					.TABLE_PAYMENT_BILLS_P." ON ".TABLE_PAYMENT_PARTY_BILLS.".id=".TABLE_PAYMENT_BILLS_P.".bill_id "
					." LEFT JOIN ". TABLE_PAYMENT_BILLS_AGAINST
                    ." ON ". TABLE_PAYMENT_BILLS_AGAINST .".id = ". TABLE_PAYMENT_PARTY_BILLS .".bill_against_id
					 WHERE ".TABLE_PAYMENT_PARTY_BILLS.".status IN ('".PaymentPartyBills::ACTIVE."')					
					".$condition_query_1." 
					ORDER BY ".TABLE_PAYMENT_PARTY_BILLS.".number, ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt " ;		
					
					
						
						
				if ( $db->query($query) ) {
				  if ( $db->nf() > 0 ) {
						$key1=$key2=0;
						$myTData=array(0=>'key',
							  1=>'number',
							  2=>'bill_no',
							  3=>'bill_dt',
							  4=>'vendor',
							  5=>'bill_against',
							  6=>'particulars',
							  7=>'s_quantity',
							  8=>'p_amount',
							  9=>'bill_amount',
							  10=>'amount',
							  11=>'stot_amount',
							  12=>'tax1_name',
							  13=>'tax1_value',
							  14=>'tax1_amount',
							  15=>'tax1_sub1_name',
							  16=>'tax1_sub1_value',
							  17=>'tax1_sub1_amount',
							  18=>'tax1_sub2_name',
							  19=>'tax1_sub2_value',
							  20=>'tax1_sub2_amount',
							  21=>'tot_amount'
						);
					   $myNTData=array(0=>'key',
								  1=>'number',
								  2=>'bill_no',
								  3=>'bill_dt',
								  4=>'vendor',
								  5=>'bill_against',
								  6=>'particulars',								  
								  7=>'s_quantity',								  
								  8=>'p_amount',								  
								  9=>'bill_amount',	
								  10=>'amount',								  
								  11=>'tot_amount'
						);
					 
						while ($db->next_record()) {
							$bill_against='';			
							$party_id = $db->f('party_id') ;
							$vendor_bank_id = $db->f('vendor_bank_id') ;
							$bill_against = $db->f('bill_against') ;
							$tax1_id_check = $db->f('tax1_id') ;
							$bill_no=$db->f('bill_no');
							$number=$db->f('number');
							$particulars = $db->f('particulars');
							$particulars = str_replace(","," ",$particulars);
							$s_quantity = $db->f('s_quantity');
							$p_amount = $db->f('p_amount');
							$bill_amount = number_format($db->f('bill_amount'),2);
							$stot_amount = number_format($db->f('stot_amount'),2);
							$bill_dt_str='';
							$bill_dt_str = $db->f('bill_dt');
							$bill_dt = date('d M Y',strtotime($bill_dt_str));
							$tax1_name = $db->f('tax1_name');
							$tax1_value = $db->f('tax1_value');
							$tax1_amount = number_format($db->f('tax1_amount'),2);
							$tax1_sub1_name = $db->f('tax1_sub1_name');
							$tax1_sub1_value = $db->f('tax1_sub1_value');
							$tax1_sub1_amount = number_format($db->f('tax1_sub1_amount'),2);
						 	$tax1_sub2_name = $db->f('tax1_sub2_name');
							$tax1_sub2_value = $db->f('tax1_sub2_value');
							$tax1_sub2_amount = number_format($db->f('tax1_sub2_amount'),2);
							$tot_amount = number_format($db->f('tot_amount'),2);
							$amount = number_format($db->f('amount'),2);
							
							$vendor='';
							if(!empty($party_id)){
								$fields1 =  TABLE_CLIENTS .".billing_name,".TABLE_CLIENTS.".f_name,
								".TABLE_CLIENTS.".l_name ";                
								$condition2 = " WHERE ".TABLE_CLIENTS .".user_id='".$party_id."' LIMIT 0,1" ;
								
								$table = TABLE_CLIENTS ;              
								$compArr = getRecord($table,$fields1,$condition2);
								if(!empty($compArr)){
									$vendor = $compArr['billing_name']  ;   
								}							
							}elseif(!empty($vendor_bank_id) ){
								$fields1 =  TABLE_VENDORS_BANK .".billing_name,".TABLE_VENDORS_BANK.".f_name,
								".TABLE_VENDORS_BANK.".l_name ";                
								$condition2 = " WHERE ".TABLE_VENDORS_BANK .".user_id='".$vendor_bank_id."' 
								LIMIT 0,1" ;
								
								$table = TABLE_VENDORS_BANK ;              
								$compArr = getRecord($table,$fields1,$condition2);
								if(!empty($compArr)){
									$vendor = $compArr['billing_name']  ;   
								}	
							}
							if((!empty($tax1_id) && $tax1_id_check==$tax1_id) || (!empty($tax1_id_check) && $tax1_id=='-1') ){
								$key1++;
								$key=$key1;
								$list[] = array(
									'number'=> $db->f('number'),
									'bill_no'=> $db->f('bill_no'),
									'bill_dt'=> $bill_dt,
									'vendor'=> $vendor,
									'bill_against'=> $bill_against,
									'particulars'=> $particulars,
									's_quantity'=> $s_quantity,									
									'p_amount'=> $p_amount,									
									'bill_amount'=> number_format($db->f('bill_amount'),2),
									'stot_amount'=> number_format($db->f('stot_amount'),2),
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=> number_format($db->f('tax1_amount'),2),
									'tax1_sub1_name'=> $db->f('tax1_sub1_name'),
									'tax1_sub1_value'=> $db->f('tax1_sub1_value'),
									'tax1_sub1_amount'=> number_format($db->f('tax1_sub1_amount'),2),
									'tax1_sub2_name'=> $db->f('tax1_sub2_name'),
									'tax1_sub2_value'=> $db->f('tax1_sub2_value'),
									'tax1_sub2_amount'=> number_format($db->f('tax1_sub2_amount'),2),
									'tot_amount'=> number_format($db->f('tot_amount'),2),
									'amount'=> number_format($db->f('amount'),2)
									);
									
									//Sr no
									$schema_insert11='';
									foreach($myTData as $key_1 =>$val){
										$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,										stripslashes($$val)) . $csv_enclosed;
										//$schema_insert1 .= $l;
										$schema_insert11 .= $l.$csv_separator;
									}
									$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
									$schema_insert1 .= $csv_terminated;
									$grant_total1 = $grant_total1 + $tot_amount	;
									
							}else{
								$key2++;
								$key=$key2;
								$ntlist[] = array(
									'number'=> $db->f('number'),
									'bill_no'=> $db->f('bill_no'),
									'bill_dt'=> $bill_dt,
									'vendor'=> $vendor,
									'bill_against'=> $bill_against,
									'particulars'=> $particulars,
									's_quantity'=> $s_quantity,	
									'p_amount'=> $p_amount,																		
									'bill_amount'=> number_format($db->f('bill_amount'),2),
									'stot_amount'=> number_format($db->f('stot_amount'),2),
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=> number_format($db->f('tax1_amount'),2),
									'tax1_sub1_name'=> $db->f('tax1_sub1_name'),
									'tax1_sub1_value'=> $db->f('tax1_sub1_value'),
									'tax1_sub1_amount'=> number_format($db->f('tax1_sub1_amount'),2),
									'tax1_sub2_name'=> $db->f('tax1_sub2_name'),
									'tax1_sub2_value'=> $db->f('tax1_sub2_value'),
									'tax1_sub2_amount'=> $db->f('tax1_sub2_amount'),
									'tot_amount'=> number_format($db->f('tot_amount'),2),
									'amount'=> number_format($db->f('amount'),2)
									);
									$schema_insert22='';
									foreach($myNTData as $key_2 =>$val){							 
										$ll = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . 
										$csv_enclosed,stripslashes($$val)) . $csv_enclosed;
										//$schema_insert1 .= $l;
										$schema_insert22 .= $ll.$csv_separator;
									}
									$schema_insert2 .= trim(substr($schema_insert22, 0, -1));
									$schema_insert2 .= $csv_terminated;
							
							}
						}
					}
					//Write CSV BOF
				
					
					$heading1=array('Sr No','NC Ref No','Bill No','Bill Date',
					'Vendor','BillAgainst','Item','Quantity','Unit Rate',
					'Bill Amount','Paid Amount','Taxed Amount','Taxname','Tax%',
					'Tax Amount','SubTax1','Sub Tax1%', 'Sub Tax1 Amount', 
					'SubTax2','Sub Tax2%', 'Sub Tax2 Amount',
					'Total Amount');
					$fields_cnt = count($heading1);
					$taxname='';
					if(!empty($tax1_id)){
						$taxname_list=array();
						$condition_queryst= " WHERE id='".$tax1_id."'";
						ServiceTax::getList($db, $taxname_list, 'id,tax_name', $condition_queryst);
						
						
						if(!empty($taxname_list)){						
							$taxname_list = $taxname_list[0];
							$taxname = $taxname_list['tax_name'];						 
						}
					}
					
					$bill_type_name ='';
					
					//First Part Display BOF
					$taxname_str ='';
					if(!empty($taxname)){
						$taxname_str =' Tax - '.$taxname ;
					}else{						
						$taxname_str = 'With All tax';
					}
					
					$mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results for Purchae Bills - '.$company_name.' - Period  - '.date('d M Y',strtotime($dfa)) .' - '.date('d M Y',strtotime($dta)).' '.$taxname_str.' ')) . $csv_enclosed;
					
					$schema_insert .= $mHead1;
					$schema_insert .= $csv_terminated;
					$schema_insert .= $csv_separator;
					$schema_insert .= $csv_terminated;
					
					$mHead2 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes( $taxname.' Particulars of the Bills')). $csv_enclosed;
					$schema_insert .= $mHead2;
					$schema_insert .= $csv_terminated;
					$schema_insert .= $csv_separator;
					$schema_insert .= $csv_terminated;
					for ($i = 0; $i < $fields_cnt; $i++){
						$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes($heading1[$i])) . $csv_enclosed;
						$schema_insert .= $l;
						$schema_insert .= $csv_separator;
					} // end for
				 
					$out1 = trim(substr($schema_insert, 0, -1));
					$out1 .= $csv_terminated;
					$out1 .= $schema_insert1 ;
					
					//Row show the grand total bof
					//$grant_total1 = number_format($grant_total1,2);
					/*$granttotstr = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes("Grand Total is ".$grant_total1)) . $csv_enclosed;
					$granttotstr .= $csv_separator;
					$granttotstr = trim(substr($granttotstr, 0, -1));
					$out1 .=$granttotstr ;
					$out1 .= $csv_terminated;
					*/
					//Row show the grand total EOF
					
					//First Part Display EOF
					//Second Part Display BOF
					$taxname_str ='';
					if(!empty($taxname)){
						$taxname_str =' Non - '.$taxname ;
					}else{
						$taxname_str = 'Without tax';
					}
					$heading2=array('Sr No','NC Ref No','Bill No','Bill Date',
					'Vendor','BillAgainst','Item','Quantity','Unit Rate',
					'Bill Amount','Paid Amount',$taxname_str.' Amount');
					$fields_cnt2 =count($heading2);
					$mHead3 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes(' '.$taxname_str.' Particulars of the Bills')). $csv_enclosed;
					$schema_insert_nt .= $mHead3;
					$schema_insert_nt .= $csv_separator;
					$schema_insert_nt .= $csv_terminated;;
					for ($i = 0; $i < $fields_cnt2; $i++)
					{
						$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes($heading2[$i])) . $csv_enclosed;
						$schema_insert_nt .= $l;
						$schema_insert_nt .= $csv_separator;
					} // end for
				 
					$out2 = trim(substr($schema_insert_nt, 0, -1));
					$out2 .= $csv_terminated;
					$out2 .= $schema_insert2 ;
					//Second Part Display EOF
					
					
					
					$out_1 =$out1.$out2;
					$filename = date('dmY').".csv";
					/* 
					header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
					header("Content-Length: " . strlen($out));
					// Output to browser with appropriate mime type, you choose ;)
					header("Content-type: text/x-csv");
					//header("Content-type: text/csv");
					//header("Content-type: application/csv");
					header("Content-Disposition: attachment; filename=$filename");
					echo $out;
					exit; 
					*/
					//Write CSV EOF
				}
				
		}
		
		if($report_type==2 || $report_type==3){
			//SALES BILLS
				$query=$mHead1=$mHead2=$schema_insert=$schema_insert1=$schema_insert2=$out1=$out2='';
				$heading2=$schema_insert_nt=$taxname_str ='';
				$myTData=$myNTData=$list=$ntlist=array();
				
			/*  $query="SELECT ".TABLE_BILL_INV.".number,".TABLE_BILL_INV.".client,"
				.TABLE_BILL_INV.".do_i,".TABLE_BILL_INV.".amount,".TABLE_BILL_ORD_P.".stot_amount," 
				.TABLE_BILL_ORD_P.".tax1_id,"	
				.TABLE_BILL_ORD_P.".tax1_name,".TABLE_BILL_ORD_P.".tax1_value,".TABLE_BILL_ORD_P.".tax1_amount,"
				.TABLE_BILL_ORD_P.".tax1_sub1_name,".TABLE_BILL_ORD_P.".tax1_sub1_value,
				".TABLE_BILL_ORD_P.".tax1_sub1_amount,"
				.TABLE_BILL_ORD_P.".tax1_sub2_name,".TABLE_BILL_ORD_P.".tax1_sub2_value,
				".TABLE_BILL_ORD_P.".tax1_sub2_amount,"
				.TABLE_BILL_ORD_P.".tot_amount FROM 
				".TABLE_BILL_INV." LEFT JOIN ".TABLE_BILL_ORD_P." ON 
				".TABLE_BILL_INV.".or_no=".TABLE_BILL_ORD_P.".ord_no WHERE 
				".TABLE_BILL_INV.".status IN ('".Invoice::PENDING."','".Invoice::COMPLETED."')
				".$condition_query_2." ORDER BY 
				".TABLE_BILL_INV.".number, ".TABLE_BILL_INV.".do_i " ;		 
			*/
				
			/* 
			if($tax1_id==$VAT){
				
				echo $query="SELECT ".TABLE_BILL_INV_PROFORMA.".client,"
				.TABLE_BILL_ORDERS.".stotal_amount," 
				.TABLE_BILL_INV_PROFORMA.".amount,"
				.TABLE_BILL_INV_PROFORMA.".number as pinv_no," 
				.TABLE_BILL_INV_PROFORMA.".do_i as do_pi," 
				.TABLE_BILL_INV_PROFORMA.".balance as pbalance," 
				.TABLE_BILL_INV_PROFORMA.".balance_inr as pbalance_inr," 
				.TABLE_BILL_INV_PROFORMA.".billing_name ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_id ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_name ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_value ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_pvalue ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_total_amount as tax1_amount,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_id ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_name ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_value ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_total_amount as tax1_sub1_amount ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_id ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_name ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_value," 
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_total_amount as tax1_sub2_amount FROM
				".TABLE_BILL_INV_PROFORMA." LEFT JOIN ".TABLE_BILL_ORDERS." ON 
					".TABLE_BILL_INV_PROFORMA.".or_no = ".TABLE_BILL_ORDERS.".number  
				WHERE ".TABLE_BILL_INV_PROFORMA.".status IN('".InvoiceProforma::ACTIVE."',
				'".InvoiceProforma::PENDING."','".InvoiceProforma::COMPLETED."')
				 ".$condition_query_2." ORDER BY 
				".TABLE_BILL_INV_PROFORMA.".do_i " ;	
			
			
				if ( $db->query($query) ) {
					if ( $db->nf() > 0 ) {
						$myTData=array(0=>'key',
						  1=>'billing_name',
						  2=>'inv_no',
						  3=>'do_i',
						  4=>'pinv_no',
						  5=>'do_pi',
						  6=>'pbalance',
						  7=>'pbalance_inr',
						  8=>'amount',
						  9=>'rcpt_no',
						  10=>'do_r',
						  11=>'do_transaction',
						  12=>'do_voucher',
						  13=>'stotal_amount',
						  14=>'tds_amount',
						  15=>'tax1_name',
						  16=>'tax1_value',
						  17=>'tax1_amount',
						  18=>'tax1_sub1_name',
						  19=>'tax1_sub1_value',
						  20=>'tax1_sub1_amount',
						  21=>'tax1_sub2_name',
						  22=>'tax1_sub2_value',
						  23=>'tax1_sub2_amount'
						);
					 
						$myNTData=array(0=>'key',
								  1=>'inv_no',
								  2=>'do_pi',
								  3=>'amount',						 
								  4=>'stotal_amount'
						);
					 
						while ($db->next_record()) {
											
							$tax1_id_check = $db->f('tax1_id') ;
							$tax1_id_check = $db->f('tax1_id') ;
							$tax1_sub1_id=$db->f('tax1_sub1_id');
							$tax1_sub2_id=$db->f('tax1_sub2_id');
							$number=$db->f('number');
							$billing_name=$db->f('billing_name');
							$inv_no=$db->f('inv_no');
							$pinv_no=$db->f('pinv_no');
							$rcpt_no='';
							//$rcpt_no=$db->f('rcpt_no');
							$amount=number_format($db->f('amount'),2);
							$stotal_amount=number_format($db->f('stotal_amount'),2);
							$pbalance=number_format($db->f('pbalance'),2);
							$pbalance_inr=number_format($db->f('pbalance_inr'),2);
							$rcpt_amount=$tds_amount='';
							 							
							$tax1_name= $db->f('tax1_name');
							$do_i = '';
							$do_voucher ='';
							$do_transaction ='';
							
							$do_pi_str= $db->f('do_pi');
							if($do_pi_str != '0000-00-00 00:00:00'){
								$do_pi = date('d M Y',strtotime($do_pi_str));
							}else{
								$do_pi ='';
							}
							$do_r_str='';
							
							$tax1_sub1_name = $tax1_sub1_name1=$db->f('tax1_sub1_name');
							$tax1_sub1_value = $tax1_sub1_value1 = $db->f('tax1_sub1_value');
							
							$tax1_sub2_name = $tax1_sub2_name1 =$db->f('tax1_sub2_name');
							$tax1_sub2_value = $tax1_sub2_value1 =$db->f('tax1_sub2_value');
							
							$tax1_value= $db->f('tax1_value');
							$tax1_pvalue= $db->f('tax1_pvalue');
							
							//$tax1_amount=number_format($db->f('tax1_amount'),2);
							$tax1_amount = $tax1_sub1_amount=$tax1_sub2_amount=0;
							$tax1_mFctr = trim($tax1_value,"%");
							$tax1_mFctr = number_format($tax1_mFctr,2);
							$taxcal_amount =  $db->f('stotal_amount') ;							
							$tax1_amount = ($taxcal_amount * $tax1_pvalue) ;
							 
							//
							//if($tax1_id_check == $ST ){
								
								//$tax1_amount = ($taxcal_amount/$ST_10_dFct ) * $tax1_mFctr ;
								//if($tax1_sub1_id ==$EC){
									//$tax1_sub1_amount = ($taxcal_amount/$EC_2_dFct ) * $EC_2_mFct ;
									
								//}elseif($tax1_sub1_id ==$SHE){								
									//$tax1_sub2_amount = ($taxcal_amount/$SHE_1_dFct ) * $SHE_1_mFct ;
									//$tax1_sub2_name = $tax1_sub1_name1;
									//$tax1_sub2_value =$tax1_sub1_value1;
								//}		
								//if($tax1_sub2_id ==$EC){
									//$tax1_sub1_amount = ($taxcal_amount/$EC_2_dFct ) * $EC_2_mFct ;
									//$tax1_sub1_name = $tax1_sub2_name1;
									//$tax1_sub1_value =$tax1_sub2_value1;
								//}elseif($tax1_sub2_id ==$SHE){
									//$tax1_sub2_amount = ($taxcal_amount/$SHE_1_dFct ) * $SHE_1_mFct ;
								//}								
							//}else{
								//$dFct = 100 + $tax1_mFctr ;
								//$tax1_amount = ($taxcal_amount/$dFct ) * $tax1_mFctr ;							
							//}
							 
							
							
							$tax1_amount=number_format($tax1_amount,2);
							$tax1_sub1_amount=number_format($tax1_sub1_amount,2);
							$tax1_sub2_amount=number_format($tax1_sub2_amount,2);
							//$tax1_sub2_amount = number_format($db->f('tax1_sub2_amount'),2);
							//$tot_amount=number_format($db->f('tot_amount'),2);
							
							
							if(!empty($tax1_id_check) && $tax1_id_check==$tax1_id){
								$key1++;
								$key=$key1;
								$list[] = array(
									'billing_name'=> $db->f('billing_name'),
									'number'=> $db->f('number'),
									'inv_no'=> '',
									'do_i'=> $do_i,
									'pinv_no'=> $pinv_no,
									'do_pi'=> $do_pi,									 
									'pbalance'=> $pbalance,
									'pbalance_inr'=> $pbalance_inr,
									'amount'=> $amount,	
									'rcpt_no'=> $rcpt_no,
									'do_r'=> '',	
									'do_transaction'=> '',	
									'do_voucher'=> '',	
									'stotal_amount'=> number_format($db->f('stotal_amount'),2),
									'tds_amount'=> $tds_amount,
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=>$tax1_amount,	
									'tax1_sub1_name'=> $tax1_sub1_name,
									'tax1_sub1_value'=> $tax1_sub1_value,
									'tax1_sub1_amount'=> $tax1_sub1_amount,	
									'tax1_sub2_name'=> $tax1_sub2_name,
									'tax1_sub2_value'=> $tax1_sub2_value,
									'tax1_sub2_amount'=> $tax1_sub2_amount
									//'tax1_sub2_amount'=> number_format($db->f('tax1_sub2_amount'),2),
									 
									);
									
									//Sr no
									$schema_insert11='';
									foreach($myTData as $key_1 =>$val){
									 
										$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,										stripslashes($$val)) . $csv_enclosed;
										//$schema_insert1 .= $l;
										$schema_insert11 .= $l.$csv_separator;
									}
									$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
									$schema_insert1 .= $csv_terminated;
									
									
									$grant_total1 = $grant_total1 + $tot_amount	;
									
							}else{
								$key2++;
								$key=$key2;
								$ntlist[] = array(
									'number'=> $db->f('number'),
									'do_i'=> '',
									'amount'=> number_format($db->f('amount'),2),
									'stot_amount'=> number_format($db->f('stot_amount'),2),
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=> number_format($db->f('tax1_amount'),2),
									'tax1_sub1_name'=> $db->f('tax1_sub1_name'),
									'tax1_sub1_value'=> $db->f('tax1_sub1_value'),
									'tax1_sub1_amount'=> number_format($db->f('tax1_sub1_amount'),2),
									'tax1_sub2_name'=> $db->f('tax1_sub2_name'),
									'tax1_sub2_value'=> $db->f('tax1_sub2_value'),
									'tax1_sub2_amount'=> $db->f('tax1_sub2_amount'),
									'tot_amount'=> number_format($db->f('tot_amount'),2)
									);
									$schema_insert22='';
								foreach($myNTData as $key_2 =>$val){							 
								$ll = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped.$csv_enclosed,stripslashes($$val)) . $csv_enclosed;
									//$schema_insert1 .= $l;
									$schema_insert22 .= $ll.$csv_separator;
								}
									$schema_insert2 .= trim(substr($schema_insert22, 0, -1));
									$schema_insert2 .= $csv_terminated;
							
							}
						}
						
						$heading1=array('Sr No','Billing Name','Invoice No','Invoice Dt',
								'Proforma No',
								'Proforma Dt',
								'Proforma Bal',
								'Proforma Bal INR',
								'Proforma Amt',
								'Rcpt No',
								'Rcpt Dt',
								'Tran Dt',
								'Vch Dt',
								'SubTot Amt',					 
								'TDS Amt',					 
								'Taxname','Tax%','Tax Amount',
								'SubTax1','Sub Tax1%', 'Sub Tax1 Amount', 
								'SubTax2','Sub Tax2%', 'Sub Tax2 Amount',
							);
								
								
						$fields_cnt =count($heading1);
						
						$taxname_list=array();
						$taxname='';
						if(!empty($tax1_id)){
							$taxname_list=array();
							$condition_queryst= " WHERE id='".$tax1_id."'";
							ServiceTax::getList($db, $taxname_list, 'id,tax_name', $condition_queryst);
							
							
							if(!empty($taxname_list)){						
								$taxname_list = $taxname_list[0];
								$taxname = $taxname_list['tax_name'];						 
							}
						}
						 
						//$bill_type_name = ($bill_type==1)? '	Goods 	':' Services' ;
						$bill_type_name='';
						//First Part Display BOF					
						$taxname_str ='';
						if(!empty($taxname)){
							$taxname_str =' Tax - '.$taxname ;
						}else{						
							$taxname_str = 'With All tax';
						}
						
						$mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results for SALE BILLS - '.$company_name.' - Period '.date('d M Y',strtotime($dfa)) .' - '.date('d M Y',strtotime($dta)).' '.$taxname_str.' ')) . $csv_enclosed;
						 
						
						$schema_insert .= $mHead1;
						$schema_insert .= $csv_terminated;
						$schema_insert .= $csv_separator;
						$schema_insert .= $csv_terminated;
						
						$mHead2 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes( $taxname.' Particulars of the Bills')). $csv_enclosed;
						$schema_insert .= $mHead2;
						$schema_insert .= $csv_terminated;
						$schema_insert .= $csv_separator;
						$schema_insert .= $csv_terminated;;
						for ($i = 0; $i < $fields_cnt; $i++){
							$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
								stripslashes($heading1[$i])) . $csv_enclosed;
							$schema_insert .= $l;
							$schema_insert .= $csv_separator;
						} // end for
					 
						$out1 = trim(substr($schema_insert, 0, -1));
						$out1 .= $csv_terminated;
						$out1 .= $schema_insert1 ;
						
						//Row show the grand total bof
						//$grant_total1 = number_format($grant_total1,2);
						//$granttotstr = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
						//		stripslashes("Grand Total is ".$grant_total1)) . $csv_enclosed;
						//$granttotstr .= $csv_separator;
						//$granttotstr = trim(substr($granttotstr, 0, -1));
						///$out1 .=$granttotstr ;
						//$out1 .= $csv_terminated;
						 
						//Row show the grand total EOF
						
						//First Part Display EOF
						//Second Part Display BOF
						$taxname_str ='';
						if(!empty($taxname)){
							$taxname_str =' Non - '.$taxname ;
						}else{
							$taxname_str = 'Without tax';
						}					 
						$heading2=array('Sr No','Invoice No','Invoice Date','Invoice Amount',$taxname_str.' Amount');
						$fields_cnt2 =count($heading2);
						$mHead3 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes(' '.$taxname_str.' Particulars of the Bills')). $csv_enclosed;
						$schema_insert_nt .= $mHead3;
						$schema_insert_nt .= $csv_separator;
						$schema_insert_nt .= $csv_terminated;;
						for ($i = 0; $i < $fields_cnt2; $i++){
							$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
								stripslashes($heading2[$i])) . $csv_enclosed;
							$schema_insert_nt .= $l;
							$schema_insert_nt .= $csv_separator;
						} // end for
					 
						$out2 = trim(substr($schema_insert_nt, 0, -1));
						$out2 .= $csv_terminated;
						$out2 .= $schema_insert2 ;
						//Second Part Display EOF
						$out_2 =$out1.$out2;
						$filename = date('dmY').".csv";
					}
				}
			}else{ */
			
				$query="SELECT ".TABLE_BILL_INV.".number as inv_no,".TABLE_BILL_INV_PROFORMA.".client,"
				.TABLE_BILL_INV.".do_i,".TABLE_BILL_INV_PROFORMA.".amount,"
				.TABLE_BILL_INV_PROFORMA.".number as pinv_no," 
				.TABLE_BILL_INV_PROFORMA.".do_i as do_pi," 
				.TABLE_BILL_INV_PROFORMA.".balance as pbalance," 
				.TABLE_BILL_INV_PROFORMA.".balance_inr as pbalance_inr," 
				.TABLE_BILL_RCPT.".number as rcpt_no," 
				.TABLE_BILL_RCPT.".amount as rcpt_amount," 				
				.TABLE_BILL_RCPT.".tds_amount," 				
				.TABLE_BILL_RCPT.".do_r," 				
				.TABLE_BILL_RCPT.".do_transaction," 				
				.TABLE_BILL_RCPT.".do_voucher," 
				.TABLE_BILL_ORDERS .".total_tax_paid_amount,"
				.TABLE_BILL_ORDERS .".total_tax_balance_amount,"
				.TABLE_BILL_INV_PROFORMA.".billing_name ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_id ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_name ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_value ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_pvalue ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_total_amount as tax1_amount,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_id ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_name ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_value ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_pvalue ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub1_total_amount as tax1_sub1_amount ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_id ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_name ,"
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_value," 
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_pvalue," 
				.TABLE_BILL_INV_PROFORMA.".tax1_sub2_total_amount as tax1_sub2_amount FROM
				".TABLE_BILL_RCPT." LEFT JOIN ".TABLE_BILL_ORDERS." ON 
				".TABLE_BILL_RCPT.".ord_no=".TABLE_BILL_ORDERS.".number  
				LEFT JOIN ".TABLE_BILL_INV." ON ".TABLE_BILL_RCPT.".inv_no=".TABLE_BILL_INV.".number
				LEFT JOIN ".TABLE_BILL_INV_PROFORMA." ON 
				".TABLE_BILL_RCPT.".inv_profm_no=".TABLE_BILL_INV_PROFORMA.".number
				WHERE ".TABLE_BILL_RCPT.".status IN ('".Receipt::ACTIVE."')
				 ".$condition_query_2." ORDER BY 
				".TABLE_BILL_RCPT.".do_r " ;	
			
			 
				if ( $db->query($query) ) {
					if ( $db->nf() > 0 ) {
					$key1=$key2=0;
					/* $myTData=array(0=>'key',
								  1=>'number',
								  2=>'do_i',
								  3=>'amount',
								  4=>'stot_amount',
								  5=>'tax1_name',
								  6=>'tax1_value',
								  7=>'tax1_amount',
								  8=>'tax1_sub1_name',
								  9=>'tax1_sub1_value',
								  10=>'tax1_sub1_amount',
								  11=>'tax1_sub2_name',
								  12=>'tax1_sub2_value',
								  13=>'tax1_sub2_amount',
								  14=>'tot_amount'
					 );
					 $myNTData=array(0=>'key',
								  1=>'number',
								  2=>'do_i',
								  3=>'amount',						 
								  4=>'tot_amount'
					 ); 
					 */
					 $myTData=array(0=>'key',
								  1=>'billing_name',
								  2=>'inv_no',
								  3=>'do_i',
								  4=>'pinv_no',
								  5=>'do_pi',
								  6=>'pbalance',
								  7=>'pbalance_inr',
								  8=>'amount',
								  9=>'rcpt_no',
								  10=>'do_r',
								  11=>'do_transaction',
								  12=>'do_voucher',
								  13=>'rcpt_amount',
								  14=>'rcpt_amount_payable_tax',
								  15=>'total_tax_paid_amount',
								  16=>'total_tax_balance_amount',
								  17=>'tds_amount',
								  18=>'tax1_name',
								  19=>'tax1_value',
								  20=>'tax1_amount',
								  21=>'tax1_sub1_name',
								  22=>'tax1_sub1_value',
								  23=>'tax1_sub1_amount',
								  24=>'tax1_sub2_name',
								  25=>'tax1_sub2_value',
								  26=>'tax1_sub2_amount'
								  
					 );
					 
					 $myNTData=array(0=>'key',
								  1=>'inv_no',
								  2=>'do_i',
								  3=>'amount',						 
								  4=>'rcpt_amount'
					 );
					 
						while ($db->next_record()) {
											
							
							$tax1_id_check = $db->f('tax1_id') ;
							$tax1_sub1_id=$db->f('tax1_sub1_id');
							$tax1_sub2_id=$db->f('tax1_sub2_id');
							
							$tax1_pvalue = $db->f('tax1_pvalue') ;
							$tax1_sub1_pvalue=$db->f('tax1_sub1_pvalue');
							$tax1_sub2_pvalue=$db->f('tax1_sub2_pvalue');
							
							$number=$db->f('number');
							$billing_name=$db->f('billing_name');
							$inv_no=$db->f('inv_no');
							$pinv_no=$db->f('pinv_no');
							$rcpt_no=$db->f('rcpt_no');
							$amount=number_format($db->f('amount'),2);
							$pbalance=number_format($db->f('pbalance'),2);
							$pbalance_inr=number_format($db->f('pbalance_inr'),2);
							$rcpt_amount=$db->f('rcpt_amount');
							$tds_amount=$db->f('tds_amount');							
							$tax1_name= $db->f('tax1_name');
							
							$do_i_str= $db->f('do_i');
							if($do_i_str != '0000-00-00 00:00:00' && !empty($do_i_str)){
								$do_i = date('d M Y',strtotime($do_i_str));
							}else{
								$do_i ='';
							}
							$do_voucher_str= $db->f('do_voucher');
							if($do_voucher_str != '0000-00-00 00:00:00'){
								$do_voucher = date('d M Y',strtotime($do_voucher_str));
							}else{
								$do_voucher ='';
							}
							$do_transaction_str= $db->f('do_transaction');
							if($do_transaction_str != '0000-00-00 00:00:00'){
								$do_transaction = date('d M Y',strtotime($do_transaction_str));
							}else{
								$do_transaction ='';
							}
							$do_pi_str= $db->f('do_pi');
							if($do_pi_str != '0000-00-00 00:00:00'){
								$do_pi = date('d M Y',strtotime($do_pi_str));
							}else{
								$do_pi ='';
							}
							$do_r_str= $db->f('do_r');
							
							$tax1_sub1_name = $tax1_sub1_name1=$db->f('tax1_sub1_name');
							$tax1_sub1_value = $tax1_sub1_value1 = $db->f('tax1_sub1_value');
							
							$tax1_sub2_name = $tax1_sub2_name1 =$db->f('tax1_sub2_name');
							$tax1_sub2_value = $tax1_sub2_value1 =$db->f('tax1_sub2_value');
							$do_r = date('d M Y',strtotime($do_r_str));
							
							$tax1_value= $db->f('tax1_value');
							//$tax1_amount=number_format($db->f('tax1_amount'),2);
							$tax1_amount = $tax1_sub1_amount=$tax1_sub2_amount=0;
							/* $tax1_mFctr = trim($tax1_value,"%");
							 */
							$tax1_mFctr = (float) ($tax1_pvalue * 100);
							 
							$taxcal_amount = $tds_amount + $rcpt_amount ;
							$tax_dFctr =0;
							if(!empty($tax1_id_check)){
								$tax_dFctr = 100 + $tax1_mFctr ;
							}
							if(!empty($tax1_sub1_id)){
								$tax1_sub1_mFctr = (float) ($tax1_sub1_pvalue * $tax1_mFctr);
								$tax_dFctr = 100 + $tax1_mFctr +$tax1_sub1_mFctr ;
							
							}
							if(!empty($tax1_sub2_id)){
								$tax1_sub2_mFctr = (float) ($tax1_sub2_pvalue * $tax1_mFctr);
								$tax_dFctr = 100 + $tax1_mFctr +$tax1_sub1_mFctr + $tax1_sub2_mFctr;
							}
							if(!empty($tax1_id_check)){
								$tax1_amount = (float) ( ($taxcal_amount/$tax_dFctr ) * $tax1_mFctr );
							}
							if(!empty($tax1_sub1_id)){
								$tax1_sub1_amount = (float) ( ($taxcal_amount/$tax_dFctr ) * $tax1_sub1_mFctr) ;
							}
							if(!empty($tax1_sub2_id)){
								$tax1_sub2_amount = (float) ( ($taxcal_amount/$tax_dFctr ) * $tax1_sub2_mFctr );
							}
							/* if($tax1_id_check == $ST ){
								
								$tax1_amount = ($taxcal_amount/$ST_10_dFct ) * $tax1_mFctr ;
								if($tax1_sub1_id ==$EC){
									$tax1_sub1_amount = ($taxcal_amount/$EC_2_dFct ) * $EC_2_mFct ;
									
								}elseif($tax1_sub1_id ==$SHE){								
									$tax1_sub2_amount = ($taxcal_amount/$SHE_1_dFct ) * $SHE_1_mFct ;
									$tax1_sub2_name = $tax1_sub1_name1;
									$tax1_sub2_value =$tax1_sub1_value1;
								}								
								
								if($tax1_sub2_id ==$EC){
									$tax1_sub1_amount = ($taxcal_amount/$EC_2_dFct ) * $EC_2_mFct ;
									$tax1_sub1_name = $tax1_sub2_name1;
									$tax1_sub1_value =$tax1_sub2_value1;
								}elseif($tax1_sub2_id ==$SHE){
									$tax1_sub2_amount = ($taxcal_amount/$SHE_1_dFct ) * $SHE_1_mFct ;
								}
								
							}else{
								$dFct = 100 + $tax1_mFctr ;
								$tax1_amount = ($taxcal_amount/$dFct ) * $tax1_mFctr ;						
							} */
							 
							 
							 
							 
							 
							//Tax amt paid details bof
							$valt['tax1_value1'] = $db->f('tax1_value');
							$amount_numeric = $db->f('amount'); 
							
							$total_tax_paid_amount = $db->f('total_tax_paid_amount') ;
							$total_tax_balance_amount = $db->f('total_tax_balance_amount') ;
							$valt['balance'] = number_format($db->f('pbalance'),2);
							$valt['balance_inr']=number_format($db->f('pbalance_inr'),2);
							$valt['prfm_balance'] = $db->f('pbalance');
							
							$valt['rcpt_amount_all'] = $amount_numeric - $valt['prfm_balance'] ; 
							
							
							$valt['tax1_value1'] = strstr($valt['tax1_value1'], '%', true);
							$valt['tax1_sub1_value1'] = $db->f('tax1_sub1_value');  
							$valt['tax1_sub1_value1'] = strstr($valt['tax1_sub1_value1'], '%', true);
							
							$valt['tax1_sub2_value1'] = $db->f('tax1_sub2_value');  
							$valt['tax1_sub2_value1'] = strstr($valt['tax1_sub2_value1'], '%', true);
							$sub1 = (float) ($valt['tax1_sub1_value1']/$valt['tax1_value1']);
							$sub2 = (float) ($valt['tax1_sub2_value1']/$valt['tax1_value1']);
							$dFac = 100+$valt['tax1_value1'] + $sub1 + $sub2 ;
							$mFact = $valt['tax1_value1'];
							
							$m1Fact = $valt['tax1_sub1_value1']/$valt['tax1_value1'] ;
							$m2Fact = $valt['tax1_sub2_value1']/$valt['tax1_value1'] ;
							$tax= ($valt['rcpt_amount_all']/$dFac)*$mFact ;
							$subtax1= ($valt['rcpt_amount_all']/$dFac)*$m1Fact ;
							$subtax2= ($valt['rcpt_amount_all']/$dFac)*$m2Fact ;
							$valt['rcpt_amount_payable_tax'] = ($tax + $subtax1 + $subtax2);
							$rcpt_amount_payable_tax = round($valt['rcpt_amount_payable_tax'],2);			 
							
							//Tax amt paid details eof
							
							
							
							
							
							
							
							
							$rcpt_amount=number_format($db->f('rcpt_amount'),2);
							$tds_amount=number_format($db->f('tds_amount'),2);
							
							$tax1_amount=number_format($tax1_amount,2);
							$tax1_sub1_amount=number_format($tax1_sub1_amount,2);
							$tax1_sub2_amount=number_format($tax1_sub2_amount,2);
							//$tax1_sub2_amount = number_format($db->f('tax1_sub2_amount'),2);
							//$tot_amount=number_format($db->f('tot_amount'),2);
							
							
							if(!empty($tax1_id_check) && $tax1_id_check==$tax1_id){
								$key1++;
								$key=$key1;
								$list[] = array(
									'billing_name'=> $db->f('billing_name'),
									'number'=> $db->f('number'),
									'inv_no'=> $db->f('inv_no'),
									'do_i'=> $do_i,
									'pinv_no'=> $pinv_no,
									'do_pi'=> $do_pi,									 
									'pbalance'=> $pbalance,
									'pbalance_inr'=> $pbalance_inr,
									'amount'=> $amount,	
									'rcpt_no'=> $rcpt_no,
									'do_r'=> $do_r,	
									'do_transaction'=> $do_transaction,	
									'do_voucher'=> $do_voucher,	
									'rcpt_amount'=> number_format($db->f('rcpt_amount'),2),
									'rcpt_amount_payable_tax'=>$rcpt_amount_payable_tax,
									'total_tax_paid_amount'=>$total_tax_paid_amount,
								    'total_tax_balance_amount'=>$total_tax_balance_amount,
									'tds_amount'=> $tds_amount,
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=>$tax1_amount,	
									'tax1_sub1_name'=> $tax1_sub1_name,
									'tax1_sub1_value'=> $tax1_sub1_value,
									'tax1_sub1_amount'=> $tax1_sub1_amount,	
									'tax1_sub2_name'=> $tax1_sub2_name,
									'tax1_sub2_value'=> $tax1_sub2_value,
									'tax1_sub2_amount'=> $tax1_sub2_amount
									//'tax1_sub2_amount'=> number_format($db->f('tax1_sub2_amount'),2),
									 
								);
									
									//Sr no
									$schema_insert11='';
									foreach($myTData as $key_1 =>$val){
										$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,										stripslashes($$val)) . $csv_enclosed;
										//$schema_insert1 .= $l;
										$schema_insert11 .= $l.$csv_separator;
									}
									$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
									$schema_insert1 .= $csv_terminated;
									$grant_total1 = $grant_total1 + $tot_amount	;
									
							}else{
								$key2++;
								$key=$key2;
								$ntlist[] = array(
									'number'=> $db->f('number'),
									'do_i'=> $do_i,
									'amount'=> number_format($db->f('amount'),2),
									'stot_amount'=> number_format($db->f('stot_amount'),2),
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=> number_format($db->f('tax1_amount'),2),
									'tax1_sub1_name'=> $db->f('tax1_sub1_name'),
									'tax1_sub1_value'=> $db->f('tax1_sub1_value'),
									'tax1_sub1_amount'=> number_format($db->f('tax1_sub1_amount'),2),
									'tax1_sub2_name'=> $db->f('tax1_sub2_name'),
									'tax1_sub2_value'=> $db->f('tax1_sub2_value'),
									'tax1_sub2_amount'=> $db->f('tax1_sub2_amount'),
									'tot_amount'=> number_format($db->f('tot_amount'),2)
									);
									$schema_insert22='';
									foreach($myNTData as $key_2 =>$val){							 
										$ll = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped.$csv_enclosed,stripslashes($$val)) . $csv_enclosed;
										//$schema_insert1 .= $l;
										$schema_insert22 .= $ll.$csv_separator;
									}
									$schema_insert2 .= trim(substr($schema_insert22, 0, -1));
									$schema_insert2 .= $csv_terminated;
							}
						}
					}
					//Write CSV BOF
					
					$heading1=array('Sr No','Billing Name','Invoice No','Invoice Dt',
					'Proforma No',
					'Proforma Dt',
					'Proforma Bal',
					'Proforma Bal INR',
					'Proforma Amt',
					'Rcpt No',
					'Rcpt Dt',
					'Tran Dt',
					'Vch Dt',
					'Rcpt Amt',	
					'Rcpt Payable Tax.Amt',	
					'Paid Tax.Amt',	
					'Tax Bal Amt',	
					'TDS Amt',					 
					'Taxname','Tax%','Tax Amount',
					'SubTax1','Sub Tax1%', 'Sub Tax1 Amount', 
					'SubTax2','Sub Tax2%', 'Sub Tax2 Amount',
					);
					
					
					$fields_cnt =count($heading1);
					
					$taxname_list=array();
					$taxname='';
					if(!empty($tax1_id)){
						$taxname_list=array();
						$condition_queryst= " WHERE id='".$tax1_id."'";
						ServiceTax::getList($db, $taxname_list, 'id,tax_name', $condition_queryst);
						
						
						if(!empty($taxname_list)){						
							$taxname_list = $taxname_list[0];
							$taxname = $taxname_list['tax_name'];						 
						}
					}
					 
					//$bill_type_name = ($bill_type==1)? '	Goods 	':' Services' ;
					$bill_type_name='';
					//First Part Display BOF					
					$taxname_str ='';
					if(!empty($taxname)){
						$taxname_str =' Tax - '.$taxname ;
					}else{						
						$taxname_str = 'With All tax';
					}
					
					$mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results for SALE BILLS - '.$company_name.' - Period '.date('d M Y',strtotime($dfa)) .' - '.date('d M Y',strtotime($dta)).' '.$taxname_str.' ')) . $csv_enclosed;
					 
					
					$schema_insert .= $mHead1;
					$schema_insert .= $csv_terminated;
					$schema_insert .= $csv_separator;
					$schema_insert .= $csv_terminated;
					
					$mHead2 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes( $taxname.' Particulars of the Bills')). $csv_enclosed;
					$schema_insert .= $mHead2;
					$schema_insert .= $csv_terminated;
					$schema_insert .= $csv_separator;
					$schema_insert .= $csv_terminated;;
					for ($i = 0; $i < $fields_cnt; $i++)
					{
						$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes($heading1[$i])) . $csv_enclosed;
						$schema_insert .= $l;
						$schema_insert .= $csv_separator;
					} // end for
				 
					$out1 = trim(substr($schema_insert, 0, -1));
					$out1 .= $csv_terminated;
					$out1 .= $schema_insert1 ;
					
					//Row show the grand total bof
					//$grant_total1 = number_format($grant_total1,2);
					/*
					$granttotstr = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes("Grand Total is ".$grant_total1)) . $csv_enclosed;
					$granttotstr .= $csv_separator;
					$granttotstr = trim(substr($granttotstr, 0, -1));
					$out1 .=$granttotstr ;
					$out1 .= $csv_terminated;
					*/
					//Row show the grand total EOF
					
					//First Part Display EOF
					//Second Part Display BOF
					$taxname_str ='';
					if(!empty($taxname)){
						$taxname_str =' Non - '.$taxname ;
					}else{
						$taxname_str = 'Without tax';
					}					 
					$heading2=array('Sr No','Invoice No','Invoice Date','Invoice Amount',$taxname_str.' Amount');
					$fields_cnt2 =count($heading2);
					$mHead3 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes(' '.$taxname_str.' Particulars of the Bills')). $csv_enclosed;
					$schema_insert_nt .= $mHead3;
					$schema_insert_nt .= $csv_separator;
					$schema_insert_nt .= $csv_terminated;;
					for ($i = 0; $i < $fields_cnt2; $i++)
					{
						$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes($heading2[$i])) . $csv_enclosed;
						$schema_insert_nt .= $l;
						$schema_insert_nt .= $csv_separator;
					} // end for
				 
					$out2 = trim(substr($schema_insert_nt, 0, -1));
					$out2 .= $csv_terminated;
					$out2 .= $schema_insert2 ;
					//Second Part Display EOF
					$out_2 =$out1.$out2;
					$filename = date('dmY').".csv";
					
				}
			//}
		}
		
		$out = $out_1.$csv_terminated.$out_2;
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Length: " . strlen($out));
		// Output to browser with appropriate mime type, you choose ;)
		header("Content-type: text/x-csv");
		//header("Content-type: text/csv");
		//header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=$filename");
		echo $out;
		//Write CSV EOF
		exit;  
		
	}

    $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');      
    $page["var"][] = array('variable' => 'tax_list', 'value' => 'tax_list');      
    $page["var"][] = array('variable' => 'list', 'value' => 'list');     
    $page["var"][] = array('variable' => 'ntlist', 'value' => 'ntlist');
    $page["var"][] = array('variable' => 'taxname', 'value' => 'taxname');
	$page["var"][] = array('variable' => 'show', 'value' => 'show');   	
	$page["var"][] = array('variable' => 'totalSum', 'value' => 'totalSum');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
 
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-month-tax-bills.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
   
?>