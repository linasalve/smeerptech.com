<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    //include( THIS_PATH ."/header.php" );   
    include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
    $serviceIdStr = isset($_GET['serviceIdStr']) ? $_GET['serviceIdStr'] : '' ;
    $db 		= new db_local; // database handle
    $vendorOptionList =$discountList='';
    
    if (isset($serviceIdStr) && !empty($serviceIdStr))
    {
        
        $vendorSql ="SELECT * FROM ".TABLE_VENDOR ;
        $db->query( $vendorSql );
        //code to generate $vendorOptionList BOF
        if( $db->nf() > 0 )
		{
            $vendorOptionList .= "<select name='vendor[]' class='inputbox w150'>" ;
            $vendorOptionList .= "<option value=''>Chose vendor</option>";
			while($db->next_record())
			{
                 $vendorOptionList .= "<option value='".$db->f('id')."'>".$db->f('f_name')." ".$db->f('l_name')."</option>";
            }
            $vendorOptionList .="</select>" ;
        }
        //code to generate $vendorOptionList EOF
       
        $discountList .= "<select name='discount_type[]' class='inputbox w100'>" ;
        $discountList .= "<option value=''></option>";
        $discountList .= "<option value='1'>OneTime</option>";
        $discountList .= "<option value='2'>Recurring</option>";
        $discountList .="</select>" ;
        
        $servicelist = NULL;	
        $serviceField   = TABLE_SETTINGS_SERVICES.".ss_id"
                  ." ".",".TABLE_SETTINGS_SERVICES.".ss_punch_line"
                  ." ".",".TABLE_SETTINGS_SERVICES.".tax1_name"
                  ." ".",".TABLE_SETTINGS_SERVICES.".tax1_value"
                  .",".TABLE_SETTINGS_SERVICES.".ss_title";
                  
        $condition = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_id IN (".$serviceIdStr.") AND ".TABLE_SETTINGS_SERVICES.".ss_status = '".Services::ACTIVE."'" ;
        
        $query = " SELECT ".$serviceField." FROM ".TABLE_SETTINGS_SERVICES.$condition ;
        
        $serviceStr='';
        $db->query( $query );
		$total	= $db->nf();
		if( $db->nf() > 0 )
		{
			while($db->next_record())
			{
				//$servicelist .=$val['ss_punch_line']."|".$val['ss_id']."," ;
                /*$serviceStr .="<div><div class=row>
                                    ".$db->f('ss_title')." (".$db->f('ss_punch_line').")</div>
                                    <div class=row><div class=coloumn vb>                                 
                                    <textarea name=particulars[] class=inputbox cols=25 rows=5 style=height:60px; onkeypress=javascript: return increaseHeight(this, event);></textarea>
                                    </div>
                                    <div class='coloumn pl5 vb'>
                                    <input type='text' name='p_amount[]' value=''
                                        onblur='javascript: updateTotal();' 
                                        onkeypress='javascript:return hwndAddParticularField(this, event);'
                                        class='inputbox' size='7'/>
                                <input type=hidden name=p_id[] value='' />
                                <input type=hidden name=ss_title[] value='".$db->f('ss_title')."(".$db->f('ss_punch_line').")"." ' />
                                <input type=hidden name=ss_punch_line[] value='".$db->f('ss_punch_line')." ' />
                                <input type=hidden name=s_id[] value=".$db->f('ss_id')." /></div> <br/>" ;
                */
                
                $tax1_name = $db->f('tax1_name');
                $tax1_value = $db->f('tax1_value');
				$tax1_pvalue = (float) (((float ) $db->f('tax1_value') ) / 100 );
				
                if(empty($tax1_name)){
                    $tax1_name='Tax';
                    
                }
                
                $serviceStr .="<div id=".$db->f('ss_id').">
                                <div class=row >
                                    ".$db->f('ss_title')." (".$db->f('ss_punch_line').")
                                </div>
                                <div class=row>
                                    <div class=coloumn vb>  Particulars <br/>                                
                                        <textarea name=particulars[] id=part".$db->f('ss_id')." class=inputbox cols=28 rows=4 style=height:70px;></textarea>
                                    </div>
                                    <div class='coloumn pl5 vb'>Price <br/>
                                        <input type='text' name='p_amount[]' value='0' id=pamt".$db->f('ss_id')."
                                        onblur='javascript: updateTotal();' 
                                        onkeypress='javascript:return hwndAddParticularField(this, event);'
                                        class='inputbox' size='5'/>
                                        <input type=hidden name=p_id[] value='' />
                                        <input type=hidden name=s_id[] value=".$db->f('ss_id')." />
                                        <input type=hidden name=ss_title[] value='".$db->f('ss_title')."(".$db->f('ss_punch_line').")"." ' />
                                        <input type=hidden name=ss_punch_line[] value='".$db->f('ss_punch_line')." ' />                                      
                                        <input type=hidden name=tax1_name[] value=".$tax1_name." />
                                        <input type=hidden name=tax1_pvalue[] value='".$tax1_pvalue."' />
                                    </div>    
                                    <div class='coloumn pl5 vb'>Discount<br/>
                                       <input type='text' name='d_amount[]' value='0'  onblur='javascript: updateTotal();'  
                                            class='inputbox' size='5'/>
                                    </div>
                                    <div class='coloumn pl5 vb'>SubTot<br/>
                                       <input type='text' name='stot_amount[]' value='0'  onblur='javascript: updateTotal();'  
                                            class='inputbox' size='5'/>
                                    </div>
                                    <div class='coloumn pl5 vb'>".$tax1_name."-".$tax1_pvalue."<br/>
                                       <input type='text' name='tax1_value[]' value='".$tax1_value."'  onblur='javascript: updateTotal();'  
                                            class='inputbox' size='5' readonly =1/>
                                    </div>
                                    <div class='coloumn pl5 vb'>Tot<br/>
                                       <input type='text' name='tot_amount[]' value='0'  onblur='javascript: updateTotal();'  
                                            class='inputbox' size='5'/>
                                    </div>
                                    <div class='coloumn pl5 vb'>Disc. type<br/>
                                        ".$discountList."
                                    </div>
                                </div>
                                
                                <div class=row>
                                    <div class='coloumn pl5 vb'> Purchase Price <br/>
                                        <input type='text' name='pp_amount[]' value='' 
                                            class='inputbox' size='7'/>
                                    </div> 
                                    <div class='coloumn pl5 vb'>Vendor<br/>
                                        ".$vendorOptionList."
                                    </div>
                                    <div class='coloumn pl5 vb'>Purchase Particulars<br/>
                                       <textarea name=purchase_particulars[] class=inputbox cols=25 rows=5 style=height:60px; onkeypress=javascript: return increaseHeight(this, event);></textarea>
                                    </div>
                                    
                                </div>
                            </div><br/>" ;
                               
           
                
                
			}
			//$servicelist = rtrim($rang,',');
		}
        echo $serviceStr ;
    
   }