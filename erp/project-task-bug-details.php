<?php
if ( $perm->has('nc_p_tsb_details') ) {


		include_once (DIR_FS_INCLUDES .'/project-priority.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-status.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-resolution.inc.php');
        include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
        
        //This is done as a bug left : add task_id in task_details - BOF
        $cid 			= isset($_GET["cid"])         ? $_GET["cid"]        : ( isset($_POST["cid"])          ? $_POST["cid"]       :'');
	    if(!empty($cid)){
           $sql= "UPDATE ".TABLE_PROJECT_TASK_DETAILS." SET task_id='".$task_id."' WHERE id =".$cid;
           $db->query($sql);
        }
        //This is done as a bug left -EOF
    
        $posted_data = array();
        
    //Get all req array BOF           
        $statusArr	= NULL;
        $condition_querys = " WHERE status ='".ProjectTask::ACTIVE."'";
        ProjectStatus::getList( $db, $statusArr, 'id, title', $condition_querys);
       
        $priorityArr	= NULL;
        $condition_queryp = " WHERE  status ='".ProjectTask::ACTIVE."'";
        ProjectPriority::getList( $db, $priorityArr, 'id, title', $condition_queryp);
        
        $resolutionArr	= NULL;
        $condition_queryr = " WHERE status ='".ProjectTask::ACTIVE."'";
        ProjectResolution::getList( $db, $resolutionArr, 'id, title', $condition_queryr);
    //Get all req array EOF
    //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
     // hrs, min.
   
     
    $extra_url  =  $pagination='';

  if(array_key_exists('btnUpdate',$_POST)){
        $_ALL_POST 	= $_POST;
        $data		= processUserData($_ALL_POST);
        $temp = explode('/', $data['resolved_date']);                               
        $time =  mktime(0, 0, 0, $temp[1], $temp[0], $temp[2]);        
        $resolved_date = $datetime->unixTimeToDate($time); 
        
        $query = "UPDATE ".TABLE_PROJECT_TASK_DETAILS
                  ." SET "
                  ." status_id     	= '".$data['status_id']."',"	
                  ." priority_id   	= '".$data['priority_id']."',"
                  ." resolution_id  = '".$data['resolution_id']."',"
                  ." resolved_date  = '".$resolved_date."' WHERE id = ".$data['id'] ;
       
        if ( $db->query($query) && $db->affected_rows() > 0 ) {
            $messages->setOkMessage("Bug has been updated Successfully.");
        }
    }

    if ( isset($_POST['btnCreate'])  && $_POST['act'] == 'save') {
          
            $posted_data = processUserData($_POST);
            $files		= processUserData($_FILES);
           
            $extra = array( 'db' 				=> &$db,
                         	'files'          => &$files,
                         	'messages'          => &$messages
                        );
            $posted_data['allowed_file_types'] = $allowed_file_types ;
            $posted_data['max_file_size']= MAX_MULTIPLE_FILE_SIZE ;
          
             
            if ( ProjectTask::validateBugCommentAdd($posted_data, $extra) ) {
                
                $is_visibleto_client = 0;
                
                if(isset($posted_data['is_visibleto_client'])){
                    $is_visibleto_client = 1;
                }
                
                
               $i=1;
               $attachfilename='';
               if(!empty($files["attached_file"]["name"])){
                   
                    //$val["name"] = str_replace(' ','-',$val["name"]);
                    $filedata["extension"]='';
                    $filedata = pathinfo($files["attached_file"]["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    $attachfilename = mktime()."-".$i.".".$ext ;                  
                   if(!empty($files["attached_file"]["name"])){
                 
                        if (move_uploaded_file($files["attached_file"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename, 0777);
                            $attached_type = $files["attached_file"]["type"] ;
                        }
                    }
                    $i++;
                }
                $hrs1 = (int) ($posted_data['hrs'] * 3);
                $min1 = (int) ($posted_data['min'] * 3);   
                 //Mark send notify BOF                        
                $mail_client=0;
                if(isset($_POST['mail_client']) ){
                    $mail_client=1;
                }
                $mail_staff=0;
                if(isset($_POST['mail_staff']) ){
                    $mail_staff=1;
                }
                $mark_imp=0;
                if(isset($_POST['mark_imp']) ){
                    $mark_imp=1;
                }
                //Mark send notify EOF
				$current_salary = $my['current_salary'];
				$per_hour_salary = $my['current_salary'] / STD_WORKING_HRS;
				$per_hour_salary =  number_format($per_hour_salary,2);
				$worked_min = ( $posted_data['hrs']*60 ) + $posted_data['min'] ;
				$worked_hour = ($worked_min/60 ) ;
				$worked_hour =  number_format($worked_hour,2);
				$worked_hour_salary = ( $per_hour_salary * $worked_hour );
				
				$query = "INSERT INTO ".TABLE_PROJECT_TASK_DETAILS
							  ." SET "
							  ." details      	= '".$posted_data['details']."',"	
                              ." task_id	    = '".$posted_data['task_id']."',"
							  ." hrs            = '".$posted_data['hrs']."',"
							  ." min            = '".$posted_data['min']."',"
                              ." hrs1           = '".$hrs1."',"
							  ." min1           = '".$min1."',"
							  ." current_salary  = '".$current_salary."',"
							  ." per_hour_salary  = '".$per_hour_salary."',"
							  ." worked_hour_salary  = '".$worked_hour_salary."',"
							  ." last_updated 	= '".$last_updated."',"
							  ." added_by      	= '".$my['user_id']."',"
							  ." parent_id      = '".$posted_data['id']."',"
							  ." project_id     = '".$posted_data['or_id']."',"
							  ." is_bug       	= '0',"
							  ." attached_file	= '".$attachfilename."',"
                              ." attached_type	= '".$attached_type."',"   
                              ." attached_desc  = '".$posted_data['attached_desc']."',"
                              ." file_upload_path  = '".$posted_data['file_upload_path']."',"
							  ." is_visibleto_client = '".$is_visibleto_client."',"
                              ." mail_client = '".$mail_client."',"
							  ." mail_staff = '".$mail_staff."',"
							  ." mark_imp = '".$mark_imp."',"
							  ." ip  	        = '".$_SERVER['REMOTE_ADDR']."',"
							  ." do_e 	        = '".date('Y-m-d H:i:s')."'" ;
                $db->query($query);
                
                //Send notification BOF    
                $data1=array();
                    $condition_query = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".id='".$posted_data['id']."'" ;
                    $fields = TABLE_PROJECT_TASK_DETAILS.".id,".TABLE_PROJECT_TASK_DETAILS.".is_client, ".
                    TABLE_PROJECT_TASK_DETAILS.".title,".TABLE_PROJECT_TASK_DETAILS.".added_by,".TABLE_PROJECT_TASK_DETAILS.".details,
                    ".TABLE_PROJECT_TASK_DETAILS.".last_updated,".TABLE_PROJECT_TASK_MODULE.".title as module" ;
                    /*$fields .= TABLE_PROJECT_TASK_STATUS.".title as status,".TABLE_PROJECT_TASK_PRIORITY.".title as priority,".TABLE_PROJECT_TASK_TYPE.".title as type, ".TABLE_PROJECT_TASK_RESOLUTION.".title as resolution" ; */
                    ProjectTask::getList( $db, $data1, $fields, $condition_query);
                    if(!empty($data1) ){      
                        $data = $data1[0];
                        if($data['is_client']=='1'){
                            $table = TABLE_CLIENTS;
                            $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$data['added_by'] ."' " ;
                            $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                            $exeCreted= getRecord($table,$fields1,$condition2);
                            $data['af_name'] =$exeCreted['f_name'];
                            $data['al_name'] = $exeCreted['l_name'] ;
                        }else{
                            $table = TABLE_USER;
                            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['added_by'] ."' " ;
                            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                            $exeCreted= getRecord($table,$fields1,$condition2);
                            $data['af_name'] =$exeCreted['f_name'];
                            $data['al_name'] = $exeCreted['l_name'] ;
                        }
                        
                        $data['cm_doe'] = date('Y-m-d H:i:s');
                        $data['cdetails'] = $posted_data['details'];
                    }
                    $data['project_title'] =$order['order_title'];
                    $data['images_nc']=DIR_WS_IMAGES_NC;
                    // Send Email to the Client BOF                    
                    if(isset($posted_data['mail_client']) ){
                         $data['link']   = DIR_WS_MP .'/project-task-bug.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'].'&task_id='.$task_id;
                         //include ( DIR_FS_INCLUDES .'/clients.inc.php');
                         Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                         if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;
                        
                            $data['cm_by_f_name'] =$my['f_name'];
                            $data['cm_by_l_name'] = $my['l_name'] ;
                            $email = NULL;
                            $data['project_title'] =$order['order_title'];
                            if ( getParsedEmail($db, $s, 'TASKBUG_NOTE_FOR_CLIENT', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email']);
                              
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                         }                       
                    }
                    // Send Email to the Client EOF
                    
                    // Send Email to the Staff BOF                    
                    if(isset($posted_data['mail_staff']) ){
                        $data['link']   = DIR_WS_NC .'/project-task-bug.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'].'&task_id='.$posted_data['task_id'];
                        //include ( DIR_FS_INCLUDES .'/client.inc.php');
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                        if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;  
                        }
                        $data['cm_by_f_name'] =$my['f_name'];
                        $data['cm_by_l_name'] = $my['l_name'] ;
                        if(!empty($team_members)){
                            foreach ( $team_members as $user ) {
                                $data['uf_name']    = $user['f_name'];
                                $data['ul_name']    = $user['l_name'];
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'TASKBUG_NOTE_FOR_STAFF', $data, $email) ) {
                                    $to     = '';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                    
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                }
                            }                             
                        }
                    }
                    
                    // Send Email to the Senior BOF     
                    //$users = getExecutivesWith($db, array(  array('right'=>'nc_p_tsb_notify', 'access_level'=>'')  ) );                       
                    if(isset($posted_data['mark_imp']) ){
                        $data['link']   = DIR_WS_NC .'/project-task-bug.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'].'&task_id='.$posted_data['task_id'];
                        //include ( DIR_FS_INCLUDES .'/client.inc.php');
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                        if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;   
                        }
                        $data['cm_by_f_name'] =$my['f_name'];
                        $data['cm_by_l_name'] = $my['l_name'] ;
                        if ( getParsedEmail($db, $s, 'TASKBUG_NOTE_FOR_STAFF_IMP', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);                                  
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                           
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        /*
                        if(!empty($users)){
                            foreach ( $users as $user ) {
                                $data['uf_name']    = $user['f_name'];
                                $data['ul_name']    = $user['l_name'];
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'TASKBUG_NOTE_FOR_STAFF_IMP', $data, $email) ) {
                                    $to     = '';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                    
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                }
                            }                             
                        }*/
                        
                    }else{                    
                        $data['link']   = DIR_WS_NC .'/project-task-bug.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'].'&task_id='.$posted_data['task_id'];
                        //include ( DIR_FS_INCLUDES .'/client.inc.php');
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                        if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] = $client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;  
                        }
                        $data['cm_by_f_name'] =$my['f_name'];
                        $data['cm_by_l_name'] = $my['l_name'] ;
                        if ( getParsedEmail($db, $s, 'TASKBUG_NOTE_FOR_STAFF', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);   
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        /*
                        if(!empty($users)){
                            foreach ( $users as $user ) {
                                $data['uf_name']    = $user['f_name'];
                                $data['ul_name']    = $user['l_name'];
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'TASKBUG_NOTE_FOR_STAFF', $data, $email) ) {
                                    $to     = '';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                    
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                }
                            }
                        }*/
                    }
                    // Send Email to the Staff BOF  
                    
                    header("Location:".DIR_WS_NC."/project-task-bug.php?perform=details&added=1&id=".$posted_data['id']."&or_id=".$posted_data['or_id'].'&task_id='.$posted_data['task_id']."&actual_time=".$posted_data['actual_time']);
                
            }

    }
    //Get list of sub threads BOF
    $list	= 	NULL;
    $condition_query_th = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id='".$id."'";
    $total	=	ProjectTask::getList($db, $list, TABLE_PROJECT_TASK_DETAILS.'.id', $condition_query_th);
   
    $condition_url = "&perform=details&id=".$id."&or_id=".$or_id."&task_id=".$task_id."&actual_time=".$actual_time;
    // $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    $extra_url  = '';
    
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url ;
    }
   // $extra_url  .= "xd=$xd&rpp=$rpp";
    $extra_url  = '&start=url'.$extra_url.'&end=url';
    //$pagination = showMovePagination($total, $xd, $rpp,$extra_url,'xd', 'movePage');
    //$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    //$list_th	= 	NULL;
    $condition_query_th = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id='".$id."' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug='0' ";
    $fields_th =  TABLE_PROJECT_TASK_DETAILS.'.id,'.TABLE_PROJECT_TASK_DETAILS.'.task_id,'.TABLE_PROJECT_TASK_DETAILS.'.details,'.TABLE_PROJECT_TASK_DETAILS.'.added_by,';
    $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.file_upload_path,'.TABLE_PROJECT_TASK_DETAILS.'.hrs,'.TABLE_PROJECT_TASK_DETAILS.'.min, '.TABLE_PROJECT_TASK_DETAILS.'.is_client,' ;
    $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.hrs1,'.TABLE_PROJECT_TASK_DETAILS.'.min1,';
    $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file,'.TABLE_PROJECT_TASK_DETAILS.'.attached_desc,';
    $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.do_e';

  	//ProjectTask::getList($db, $list_th,$fields_th, $condition_query_th,$next_recordd, $rpp);
  	ProjectTask::getList($db, $list_th,$fields_th, $condition_query_th);
    $flist_th = array();
    if(!empty($list_th)){
        foreach($list_th as $key=>$val){
            
            if($val['is_client']=='1'){
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                $val['is_client']='1';
            }else{
                $table = TABLE_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                 $val['is_client']='0';
            }
            if($actual_time){
                $val['hrs'] = strlen($val['hrs']) ==1 ? '0'.$val['hrs'] : $val['hrs'] ;
                $val['min'] = strlen($val['min']) ==1 ? '0'.$val['min'] : $val['min'] ;
            }else{
                 // show atual total time (timesheet * 3 ) total cost
                $val['tothrs'] = $val['hrs1'] ;
                $val['totmin'] = $val['min1'] ;
                $extraHr = (int) ($val['min1'] / 60 ) ;
                $min1 =  ($val['min1'] % 60 ) ;
                $val['tothrs'] =  $val['tothrs'] + $extraHr ;
                $val['totmin'] = $min1 ;
                
                $val['hrs'] = strlen($val['tothrs']) ==1 ? '0'.$val['tothrs'] : $val['tothrs'] ;
                $val['min'] = strlen($val['totmin']) ==1 ? '0'.$val['totmin'] : $val['totmin'] ;
            }            
            $flist_th[$key]=$val;
        }
    } 
    //Get list of sub threads EOF


    $condition_query = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".id=".$id;
    $details	= NULL;
    $fields = TABLE_PROJECT_TASK_DETAILS.".*, ".TABLE_PROJECT_TASK_MODULE.".title as module_title" ;
   /*  $fields .= TABLE_PROJECT_TASK_STATUS.".title as status_title,".TABLE_PROJECT_TASK_PRIORITY.".title as priority_title, ".TABLE_PROJECT_TASK_TYPE.".title as type_title,".TABLE_PROJECT_TASK_RESOLUTION.".title as rs_title" ; */
    ProjectTask::getList( $db, $details, $fields, $condition_query);
    if(!empty($details) ){      
        $details = $details[0];
       
        if($details['is_client']=='1'){
        
             $table = TABLE_CLIENTS;
           
            $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$details['added_by']."' " ;
            $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS .'.number,'.TABLE_CLIENTS .'.user_id';
            $exeCreted = getRecord($table,$fields1,$condition2);        
            $details['added_by_name'] = $exeCreted['f_name']." ".$exeCreted['l_name']."" ;
            
        }else{
        
            $table = TABLE_USER;
            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$details['added_by']."' " ;
            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER .'.number,'.TABLE_USER .'.user_id';
            $exeCreted = getRecord($table,$fields1,$condition2);        
            $details['added_by_name'] = $exeCreted['f_name']." ".$exeCreted['l_name']."" ;
        }
        
         //Calculate bugwise hrs spent           
            if($actual_time){
                    $fields_bws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr '
                    .', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin' ;
            }else{
                // show atual total time (timesheet * 3 ) total cost  
                 $fields_bws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '
                    .', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) % 60 ) as totMin' ;
            
            }
             $condition_query_bwh = " WHERE (".TABLE_PROJECT_TASK_DETAILS.".id ='".$details['id']."' AND 
                                    ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' 
                                    AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0' ) OR 
                                    ".TABLE_PROJECT_TASK_DETAILS.".parent_id IN ('".$details['id']."'
                                        )";
            $list_bws	= 	NULL;
          
          	ProjectTask::getList( $db, $list_bws, $fields_bws, $condition_query_bwh); 
          
            if(!empty($list_bws[0]['totHr']) || !empty($list_bws[0]['totMin'])){
                $list_bws =$list_bws[0];
              
                $details['tot_bhrs'] = strlen($list_bws['totHr']) ==1 ? '0'.$list_bws['totHr'] : $list_bws['totHr'] ; 
                $details['tot_bmin'] = strlen($list_bws['totMin']) ==1 ? '0'.$list_bws['totMin'] : $list_bws['totMin'] ; 
                
            }else{
                $details['tot_bhrs'] = '00';
                $details['tot_bmin'] = '00' ;
            }
        
        
        
        
        if(empty($details['resolved_date']) || $details['resolved_date']=='0000-00-00 00:00:00'){
            $details['resolved_date']='';
          
            $details['resolved_date']  = date('d') .'/'. date('m').'/'.date('Y');
        }else{
            $details['resolved_date']  = explode(' ', $details['resolved_date']);
            $temp               = explode('-', $details['resolved_date'][0]);
            $details['resolved_date']  = NULL;
            $details['resolved_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
        
        }
        //Get submodule bof
        $details['sub_module_title'] ='';
        $table = TABLE_PROJECT_TASK_MODULE;
        $conditionm = " WHERE ".TABLE_PROJECT_TASK_MODULE .".id= '".$details['sub_module_id']."' " ;
        $fieldsm =  TABLE_PROJECT_TASK_MODULE .'.title';
        $moduleDetails = getRecord($table,$fieldsm,$conditionm);    
        if(!empty( $moduleDetails)){
            
            $details['sub_module_title'] =$moduleDetails['title'];
        }        
        
        //Get submodule eof
        
        /*
        $hrs=$min='-';
        if(!empty($details['hrs'])){
            $hrs=$details['hrs']." Hrs";
        }
        if(!empty($details['min'])){
            $min=$details['min']." Min";
        }
        $details['est_hrs']  = $hrs." ".$min." ";
        */
        
        $hrs=$min='00';
        if($actual_time){
                $hrs = strlen($details['hrs']) ==1 ? '0'.$details['hrs'] : $details['hrs'] ;
                $min = strlen($details['min']) ==1 ? '0'.$details['min'] : $details['min'] ;
        }else{
             // show atual total time (timesheet * 3 ) total cost
            $details['tothrs'] = $details['hrs1'] ;
            $details['totmin'] = $details['min1'] ;
            $extraHr = (int) ($details['min1'] / 60 ) ;
            $min1 =  ($details['min1'] % 60 ) ;
            $details['tothrs'] =  $details['tothrs'] + $extraHr ;
            $details['totmin'] = $min1 ;
            
            $hrs = strlen($details['tothrs']) ==1 ? '0'.$details['tothrs'] : $details['tothrs'] ;
            $min = strlen($details['totmin']) ==1 ? '0'.$details['totmin'] : $details['totmin'] ;
        }
        $details['est_hrs']  = $hrs.":".$min." ";
        $_ALL_POST =$details;
    }
    
   
    if ( $perm->has('nc_p_ts_edit') ) {
        $variables['can_edit'] = true;
    }
   
    if ( $perm->has('nc_p_ts_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_p_ts_add') ) {
        $variables['can_add'] = true;
    }
  
  
   /*
    if ( $perm->has('nc_p_ts_details') ) {
        $variables['can_view_details'] = true;
    }*/
    $variables['can_view_details'] = true;
    // Set the Permissions.
    $hidden[] = array('name'=> 'perform' ,'value' => 'details');
    $hidden[] = array('name'=> 'act' ,'value' => 'save');
    $hidden[] = array('name'=> 'or_id' , 'value' => $or_id);     
    $hidden[] = array('name'=> 'task_id' , 'value' => $task_id);     
    $hidden[] = array('name'=> 'actual_time' , 'value' => $actual_time);   
    $hidden[] = array('name'=> 'id' , 'value' => $id);     
    
    $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
    $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
    $page["var"][] = array('variable' => 'posted_data', 'value' => 'posted_data');
    $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
    $page["var"][] = array('variable' => 'priorityArr', 'value' => 'priorityArr');
    $page["var"][] = array('variable' => 'statusArr', 'value' => 'statusArr');
    $page["var"][] = array('variable' => 'resolutionArr', 'value' => 'resolutionArr');
    $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => 'flist_th', 'value' => 'flist_th');
   
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task-bug-details.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
