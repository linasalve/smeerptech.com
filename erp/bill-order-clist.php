<?php
    if ( $perm->has('nc_bl_or_list') || $perm->has('nc_bl_or_search') ) {
		include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
		include_once (DIR_FS_INCLUDES .'/services.inc.php');
		include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
		include_once ( DIR_FS_INCLUDES .'/user.inc.php');
		
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id='0' ORDER BY id ASC ";
		ServiceTax::getList($db, $taxArr, 'id,tax_name', $condition_queryst);
    
        $lst_service    = NULL;
		$condition_querys= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC"; 
		Services::getList($db, $lst_service, 'ss_id,ss_title', $condition_querys);
		$variables['serviceArr'] =$lst_service;
		
		$lst_iah = null;
		Paymenttransaction::getIAccountHead($db,$lst_iah);
		// Status
		$variables["blocked"]           = Order::BLOCKED;
		$variables["active"]            = Order::ACTIVE;
		$variables["pending"]           = Order::PENDING;
		$variables["deleted"]           = Order::DELETED;
		$variables["completed"]         = Order::COMPLETED;
		$variables["processed"]         = Order::PROCESSED;
        $variables["followup_type"]     = FOLLOWUP::ORDER;
        
	   
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ''; 			 
            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(Order::COMPLETED); 
            $condition_query = " WHERE "." ". TABLE_BILL_ORDERS .".status IN('".Order::COMPLETED."'".")";

        }
     
        if ( $perm->has('nc_bl_or_list_all') ) { 
		 
		 
		}else{
		
			// If the Order is created by the my.
			$access_level   = $my['access_level'];
			if ( $perm->has('nc_bl_or_list_al') ) {
				$access_level += 1;
			}
			$condition_query .= " AND ( ";
			
			// If my has created this Order.
			$condition_query .= " (". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
									." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
			
			// If my is present in the Team.
			$condition_query .= " OR ( ( "
											. TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
											. TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
											. TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
											. TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
										. " ) "
										." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
			
			// If my is the Client Manager
			$condition_query .= " OR ( ". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
								." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
			
			// Check if the User has the Right to view Orders created by other Users.
			if ( $perm->has('nc_bl_or_list_ot') ) {
				$access_level_o   = $my['access_level'];
				if ( $perm->has('nc_bl_or_list_ot_al') ) {
					$access_level_o += 1;
				}
				$condition_query .= " OR ( ". TABLE_BILL_ORDERS. ".created_by != '". $my['user_id'] ."' "
									." AND ". TABLE_BILL_ORDERS .".access_level < $access_level_o ) ";
			}
			$condition_query .= " )";      
		}


		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
		
      	
		$totalAmount=$total='';
		$query_tot=" SELECT SUM(".TABLE_BILL_ORDERS.".amount) as totalAmount FROM ".TABLE_BILL_ORDERS." 
			WHERE ".TABLE_BILL_ORDERS.".id IN( 
				SELECT DISTINCT(".TABLE_BILL_ORDERS.".id) FROM ". TABLE_BILL_ORDERS."
				LEFT JOIN ".TABLE_BILL_ORD_P." ON ".TABLE_BILL_ORDERS.".number = ".TABLE_BILL_ORD_P.".ord_no 
				INNER JOIN ". TABLE_USER." ON ". TABLE_USER .".user_id = ". TABLE_BILL_ORDERS .".created_by 
				INNER JOIN ". TABLE_CLIENTS
				." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client ".$condition_query."
			)";
		$db->query($query_tot);  
		if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
				$totalAmount=$db->f('totalAmount');
			}
		}
		/* $fields=" SUM(".TABLE_BILL_ORDERS.".amount) as totalAmount ";
		Order::getDetails(  $db, $totalAmt, $fields, $condition_query);
		if(!empty($totalAmt)){			
			$totalAmount = number_format($totalAmt[0]['totalAmount'],2);
		} */
		
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
        //By default search in On
        $_SEARCH["searched"]    = true ;
        
        if($searchStr==1){
          
            $fields = TABLE_BILL_ORDERS.'.id ' ;
            $total	=	Order::getDetails( $db, $list, '', $condition_query);
            $extra_url  = '';
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            $list	= NULL;
            $fields = TABLE_BILL_ORDERS .'.id'
                    .','. TABLE_BILL_ORDERS .'.number'
					.','. TABLE_BILL_ORDERS .'.order_of'
                    .','. TABLE_BILL_ORDERS .'.order_title'
                    .','. TABLE_BILL_ORDERS .'.access_level'
					.','. TABLE_BILL_ORDERS .'.iaccount_head_id'
                    .','. TABLE_BILL_ORDERS .'.amount'
					.','. TABLE_BILL_ORDERS .'.balance'
                    .','. TABLE_BILL_ORDERS .'.team'
                    .','. TABLE_BILL_ORDERS .'.details'
                    .','. TABLE_BILL_ORDERS .'.do_e'
                    .','. TABLE_BILL_ORDERS .'.do_fe'
                    .','. TABLE_BILL_ORDERS .'.do_d'
                    .','. TABLE_BILL_ORDERS .'.do_c'
                    .','. TABLE_BILL_ORDERS .'.do_o'
                    .','. TABLE_BILL_ORDERS .'.status'
                    .','. TABLE_BILL_ORDERS .'.followup_status'
                    .','. TABLE_BILL_ORDERS .'.npa_status'
					.','. TABLE_BILL_ORDERS .'.inproduction_status'
                    .','. TABLE_BILL_ORDERS .'.cron_created '
                    .','. TABLE_BILL_ORDERS .'.is_invoiceprofm_create '
                    .','. TABLE_BILL_ORDERS .'.is_invoice_create '
                    .','. TABLE_BILL_ORDERS .'.new_ref_ord_id'
                    .','. TABLE_BILL_ORDERS .'.new_ref_ord_no'
                    .','. TABLE_BILL_ORDERS .'.order_against_id'
                    .','. TABLE_BILL_ORDERS .'.order_against_no'
                    .','. TABLE_BILL_ORDERS .'.invoice_against_id'
                    .','. TABLE_BILL_ORDERS .'.invoice_against_no'
                    .','. TABLE_BILL_ORDERS .'.is_old' //For Old Migrated data 
                    .','. TABLE_BILL_ORDERS .'.old_updated' //For Old Migrated data
                    .','. TABLE_BILL_ORDERS .'.old_particulars'
					.','. TABLE_BILL_ORDERS .'.wp_support_team_name'
					.','. TABLE_BILL_ORDERS .'.discount_explanation'
					.','. TABLE_BILL_ORDERS .'.special'
					.','. TABLE_BILL_ORDERS .'.daily_update_status'
					.','.TABLE_BILL_ORDERS.'.ord_referred_status'
					.','.TABLE_BILL_ORDERS.'.ac_ed_date'
					.','.TABLE_BILL_ORDERS.'.ac_ed_mark_by_name'
					.','.TABLE_BILL_ORDERS.'.number as order_no'
					.','.TABLE_BILL_ORDERS.'.ord_referred_used_against_no'
					.','.TABLE_BILL_ORDERS.'.ord_referred_by_name'
					.','.TABLE_BILL_ORDERS.'.ord_referred_amount'
					.','. TABLE_BILL_ORDERS .'.financial_yr'	
					.','. TABLE_BILL_ORDERS .'.sent_for_approval'	
					.','. TABLE_BILL_ORDERS .'.sent_by_name'	
					.','. TABLE_BILL_ORDERS .'.do_sent'	
                    //.','. TABLE_BILL_INV .'.number as inv_no'
                    .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                    .','. TABLE_CLIENTS .'.number AS c_number'
                    .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                    .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                    .','. TABLE_CLIENTS .'.billing_name'
                    .','. TABLE_CLIENTS .'.email AS c_email'
                    .','. TABLE_CLIENTS .'.email_1 AS c_email_1'
                    .','. TABLE_CLIENTS .'.email_2 AS c_email_2'
                    .','. TABLE_CLIENTS .'.mobile1 AS c_mobile1'
                    .','. TABLE_CLIENTS .'.mobile2 AS c_mobile2'
                    .','. TABLE_CLIENTS .'.status AS c_status';
             Order::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        
        $oarry = Order::getTypeArray();
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               
               $is_invoice_created = 0;
               
			   
                if($val['is_old']){
                   $sql= " SELECT COUNT(*) as count FROM ".TABLE_BILL_INV." WHERE 
				   ".TABLE_BILL_INV.".or_no='".$val['number']."' 
                   AND ".TABLE_BILL_INV.".status !='".Invoice::DELETED."' AND 
				   ".TABLE_BILL_INV.".old_updated ='1'"; 
                   //TABLE_BILL_INV.".old_updated ='1'"   Means Invoice template generated or not if 
				   //1 then invoice template generated
                    $db->query($sql);               
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) {
                            $count = $db->f('count') ;
                            if( $count > 0){
                                $is_invoice_created = 1;
                            }
                        }
                    }
               
                }else{
               
                   $sql= " SELECT COUNT(*) as count FROM ".TABLE_BILL_INV." WHERE ".TABLE_BILL_INV.".or_no='".$val['number']."' AND ".TABLE_BILL_INV.".status !='".Invoice::DELETED."' LIMIT 0,1";
                   $db->query($sql);               
                   if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) {
                            $count = $db->f('count') ;
                            if( $count > 0){
                                $is_invoice_created = 1;
                            }
                        }
                    }
                }
				$order_of = trim($val['order_of'],",") ;
				if(!empty($order_of)){
					$oArr = explode(",",$order_of);
					$oof_str='';
					foreach($oArr as $ky=>$vl){
						$oof_str.= $oarry[$vl].", ";
					}
					$val['order_of_name'] = trim($oof_str,",");
				}
				
				$iaccount_head_id = $val['iaccount_head_id'] ;
				$iaccount_head_name='';
				if(!empty($iaccount_head_id)){
					$sql=" SELECT account_head_name FROM ".TABLE_PAYMENT_IACCOUNT_HEAD." WHERE id =".$iaccount_head_id ;
					$db->query($sql);               
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) {
							$iaccount_head_name= $db->f('account_head_name');
							 
						}
					} 
				} 
				$val['iaccount_head_name']=$iaccount_head_name ; 
				 
				
				//check order id followup in ST BOF
				$val['ticket_id']=0;
				$sql2 = "SELECT ticket_id FROM ".TABLE_ST_TICKETS." WHERE 
				(".TABLE_ST_TICKETS.".flw_ord_id = '".$val['id']."') AND ".TABLE_ST_TICKETS.".ticket_child =0 LIMIT 0,1";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                        }
                    }                   
                }
				//check order id followup in ST EOF
				$val['team_members']='';
				if(!empty($val['team'])){
					$val['team'] = "'".implode("','", (explode(',', $val['team']))) ."'";
					$team_members=array();
					if ( User::getList($db, $team_members, 'user_id, 
					number, f_name, l_name, email', "WHERE user_id IN (". $val['team'] .")") > 0 ) {
						$val['team_members'] = $team_members ;
						 
					}
				}
				
                $is_particulars_selected = 0;
                $sql= " SELECT id FROM ".TABLE_BILL_ORD_P." WHERE ".TABLE_BILL_ORD_P.".ord_no='".$val['number']."' LIMIT 0,1";
                  $db->query($sql);               
                   if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) {
                            $count = $db->f('id') ;
                            if( $count > 0){
                                $is_particulars_selected = 1;
                            }
                        }
                    }
				$val['is_invoice_created']=0;
				if($is_invoice_created==1 || $val['is_invoiceprofm_create'] ==1){
					$val['is_invoice_created'] = 1;
				}
                   
                $val['is_particulars_selected'] = $is_particulars_selected;                   
                $flist[$key]=$val;
            }
        }
        //print_r($flist);
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_create_invoice']= false;
        $variables['can_followup'] = false;
        $variables['can_bifurcate'] = false;
		$variables['can_view_inv'] = false;
        $variables['can_add_bid'] = false;
		$variables['can_list_bid'] = false;
        $variables['can_add_vjob'] = false;
		$variables['can_mark_flw'] = false;
		$variables['can_mark_tds'] = false;
		$variables['can_mark_npa'] = false;
		$variables['can_mark_pro'] = false;
		$variables['can_mark_dailyupdate'] = false;
	    if ( $perm->has('nc_or_bd') && $perm->has('nc_or_bd_add') ) {
            $variables['can_add_bid'] = true;
        }
        if ( $perm->has('nc_or_bd') && $perm->has('nc_or_bd_list') ) {
            $variables['can_list_bid'] = true;
        }
	 
        if ( $perm->has('nc_bl_or_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_bl_or_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_bl_or_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_bl_or_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_bl_or_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_bl_or_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_bl_inv_add') ) {
            $variables['can_create_invoice'] = true;
        }
        /*if ( $perm->has('nc_bl_or_flw') ) {
            $variables['can_followup'] = true;
        }*/
        if ( $perm->has('nc_bl_or_bifur') ) {
            $variables['can_bifurcate'] = true;
        }
		if ( $perm->has('nc_st') && $perm->has('nc_st_flw_inv') ) {
            $variables['can_followup'] = true;
        }
		if( $perm->has('nc_bl_inv') && $perm->has('nc_bl_inv_list') ) {
            $variables['can_view_inv']     = true;
        }
		if( $perm->has('nc_vjb') && $perm->has('nc_vjb_add') ) {
            $variables['can_add_vjob']     = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_flw_status') ) {
            $variables['can_mark_flw'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_tds_status') ) {
            $variables['can_mark_tds'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_npa_status') ) {
            $variables['can_mark_npa'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_pro_status') ) {
            $variables['can_mark_pro'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_dailyupdate_status') ) {
            $variables['can_mark_dailyupdate'] = true;
        }
		
		$variables['tdsflw'] = Order::FOLLOWUPTDS;
		$variables['ceoflw'] = Order::FOLLOWUPCEO;
		$variables['noneflw'] = Order::FOLLOWUPPENDING;
		$variables['activeflw'] = Order::FOLLOWUPACTIVE;
		$variables['activenpa'] = Order::NPAACTIVE;
		$variables['pendingnpa'] = Order::NPADEACTIVE;
		$variables['activepro'] = Order::PROACTIVE;
		$variables['pendingpro'] = Order::PRODEACTIVE;
		
		$page["var"][] = array('variable' => 'taxArr', 'value' => 'taxArr');
		$page["var"][] = array('variable' => 'oarry', 'value' => 'oarry');
		$page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount');
        $page["var"][] = array('variable' => 'total', 'value' => 'total');
		$page["var"][] = array('variable' => 'lst_iah', 'value' => 'lst_iah');
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-clist.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>