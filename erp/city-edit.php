<?php
if ( $perm->has('nc_city_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/state.inc.php');
        
        $lst_state = NULL;
        $required_fields ='*';
		State::getList($db,$lst_state,$required_fields);
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( City::validateUpdate($data, $extra) ) {
                
                $query  = "SELECT code FROM ".TABLE_COUNTRIES." WHERE id='".$data['country_id']."'";
                            $db->query($query);
                            if($db->nf()>0){
                                $db->next_record();
                                $country_code=$db->f('code');
                            }
                            
                $query  = " UPDATE ". TABLE_LIST_CITY
                            ." SET ". TABLE_LIST_CITY .".name = '".		$data['name'] ."'"
                                    .",". TABLE_LIST_CITY .".country_id    = '". 		$data['country_id'] ."'"                                
                                    .",". TABLE_LIST_CITY .".state_id      = '". 		$data['state_id'] ."'"                                
                                    .",". TABLE_LIST_CITY .".country_code  = '". 		$country_code ."'"    
                                   	.",". TABLE_LIST_CITY .".status = '". 		$data['status'] ."'"                               
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("City has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_LIST_CITY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_LIST_CITY .".id = '". $id ."' )";
            
            if ( City::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
             $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/city-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'city-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
