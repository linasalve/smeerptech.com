<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php'); 
    include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php'); 
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])  ? $_POST["rpp"]     : 
	( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');
    $deleted 	= isset($_GET["deleted"])   ? $_GET["deleted"]  : ( isset($_POST["deleted"])    ? $_POST["deleted"] : '0');
    $updated 	= isset($_GET["updated"])   ? $_GET["updated"]  : ( isset($_POST["updated"])    ? $_POST["updated"] : '0');
    $or_no 		= isset($_GET["or_no"])     ? $_GET["or_no"]    : ( isset($_POST["or_no"])      ? $_POST["or_no"]   : '');
    $hid 		= isset($_GET["hid"])       ? $_GET["hid"]      : ( isset($_POST["hid"])        ? $_POST["hid"]     :'');
	
	$action="Sales Daily Report" ;
    $condition_url ='';
    $condition_query = '';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    } else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
     //By default search in On
    $_SEARCH["searched"]    = true ;
     // Read the available Status 
    $variables['types'] = ProspectsOrder::getTypes();
    
     
    if ( $perm->has('nc_ps_or') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_PROSPECTS_ORDERS   =>  array(  'Lead No.'     => 'number',
                                                                'Lead Title'   => 'order_title',													 
                                                                'Details'       => 'details'
                                                        ),
								TABLE_PROSPECTS_TICKETS 	=>  array( 
                                                            'Prospect'  => 'ticket_owner-'.TABLE_PROSPECTS_TICKETS,
                                                            'Reporting Details'     => 'ticket_text-'.TABLE_PROSPECTS_TICKETS 
                                                )
                                 // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                              /*   TABLE_USER      =>  array(  'Creator Number'    => 'number-user',
                                                            'Creator Username'  => 'username-user',
                                                            'Creator Email'     => 'email-user',
                                                            'Creator First Name'=> 'f_name-user',
                                                            'Creator Last Name' => 'l_name-user',
                                                ), 
								TABLE_PROSPECTS 	=>  array( 
                                                            'Prospect Company'  => 'org-'.TABLE_PROSPECTS,
                                                            'Prospect Email'     => 'email-'.TABLE_PROSPECTS,
                                                            'Prospect First Name'=> 'f_name-'.TABLE_PROSPECTS,
                                                            'Prospect Last Name' => 'l_name-'.TABLE_PROSPECTS,
                                                ),*/
								
                            );
        
        $sOrderByArray  = array( TABLE_PROSPECTS_TICKETS => array( 'Reporting Date'     => 'do_activity',
															'Reporting Add Date'     => 'do_e'
														),
								TABLE_PROSPECTS_ORDERS => array( 'Date of Order'     => 'do_o'  
														)
                            ); 
					 
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
          
            if(!empty($hid)){
                $_SEARCH['sOrderBy']= $sOrderBy = 'id';
                $variables['hid']=$hid;
            }else{
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_activity';
            }
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PROSPECTS_TICKETS;
        }

        // Read the available Status for the Pre Order.
       $variables['status'] = ProspectsOrder::getStatus();
       $variables['flwstatus'] = ProspectsOrder::getFollowupStatus();
        
        
        //use switch case here to perform action. 
        switch ($perform) {
           
        
            
            case ('search'): {
				$searchLink=1;
                include(DIR_FS_NC."/prospects-order-activity-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-order-activity.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('list'):
            default: {
                $searchStr = 1;
                include (DIR_FS_NC .'/prospects-order-activity-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-order-activity.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-order-activity.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("action", $action);
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>