<?php
    if ( $perm->has('nc_bl_inv_add') ) {
        //include_once ( DIR_FS_INCLUDES .'/currency.inc.php');      
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
        
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $exchange_rate  = isset($_POST["exchange_rate"]) ? $_POST["exchange_rate"] : '1' ;
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_inv_add_al') ) {
            $access_level += 1;
        }
        $_ALL_POST['exchange_rate'] = $exchange_rate; 
        
        $extra = array( 'db' 				=> &$db,
                        'access_level'      => $access_level,
                         'messages'          => &$messages
                        );
                        
        $currency = NULL ;
        $required_fields = '*' ;
		Currency::getList($db,$currency,$required_fields);        
        //echo $time_start = microtime(true);      
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,                        
                            'messages'          => &$messages
                        );
            $data['do_c'] = time();
         
           
            if ( Invoice::validateAdd($data, $extra) ) {
                
                //get currency abbr from d/b bof
                $mail_client = 0;
                if(isset($data['mail_client'])){
                    $mail_client = 1;
                }
                $currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                $fields1 = "abbr ";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);
              
                if(!empty($currency1)){
                   $data['currency'] = $currency1[0]['abbr'];
                }
                //get currency abbr from d/b eof                
                if(!empty($data['do_e'])){
                    $data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                }else{
                    $data['do_e'] ='';
                }
                if(!empty($data['do_fe'])){
                    $data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }else{
                    $data['do_fe'] ='';
                }
            
                  $query	= " INSERT INTO ".TABLE_BILL_INV
                            ." SET ". TABLE_BILL_INV .".number = '".        $data['number'] ."'"
                                .",". TABLE_BILL_INV .".or_no = '".         $data['or_no'] ."'"
                                //.",". TABLE_BILL_INV .".service_id = '".    implode(',', $data['service_id']) ."'"
                                .",". TABLE_BILL_INV .".access_level = '".  $data['access_level'] ."'"
                                .",". TABLE_BILL_INV .".created_by = '".    $data['created_by'] ."'"
                                .",". TABLE_BILL_INV .".client = '".        $data['client']['user_id'] ."'"                                
                                .",". TABLE_BILL_INV .".currency = '".      $data['currency'] ."'"
                                .",". TABLE_BILL_INV .".currency_id = '".   $data['currency_id'] ."'"
                                .",". TABLE_BILL_INV .".exchange_rate = '". $data['exchange_rate'] ."'"
                                .",". TABLE_BILL_INV .".amount = '".        $data['amount'] ."'"
                                .",". TABLE_BILL_INV .".amount_inr = '".    $data['amount_inr'] ."'"
                                .",". TABLE_BILL_INV .".amount_words = '".  $data['amount_words'] ."'"
                                .",". TABLE_BILL_INV .".do_c = '".          date('Y-m-d H:i:s') ."'"
                                .",". TABLE_BILL_INV .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
                                .",". TABLE_BILL_INV .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
                                .",". TABLE_BILL_INV .".do_e = '".          $data['do_e'] ."'"
                                .",". TABLE_BILL_INV .".do_fe = '".         $data['do_fe']."'"
                                .",". TABLE_BILL_INV .".remarks = '".       $data['remarks'] ."'"
                                .",". TABLE_BILL_INV .".balance = '".       $data['balance'] ."'"
                                .",". TABLE_BILL_INV .".balance_inr = '".   $data['amount_inr'] ."'"
                                .",". TABLE_BILL_INV .".company_id = '".    $data['company_id'] ."'"
                                //.",". TABLE_BILL_INV .".billing_name =    '".  $data['billing_name'] ."'"
                                .",". TABLE_BILL_INV .".billing_address =   '". $data['billing_address'] ."'"
                                .",". TABLE_BILL_INV .".is_renewable =      '". $data['is_renewable'] ."'"
                                .",". TABLE_BILL_INV .".is_old          =   '1'"
                                .",". TABLE_BILL_INV .".ip =                '".$_SERVER['REMOTE_ADDR']."'"
                                .",". TABLE_BILL_INV .".status =            '".$data['status'] ."'" ;
             
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New Invoice has been created.");
                    //After insert, update invoice counter in 
                    $variables['hid'] = $db->last_inserted_id();
                    $or_no_arr=explode("-",$data['or_no'] );
                    $financial_yr=$or_no_arr['2']."-".$or_no_arr['3'];
                    
                    updateOldCounterOf($db,'INV',$data['company_id'],$financial_yr);
                    
                    //BOF Insert followup
                        $query="SELECT followup_no FROM ".TABLE_SALE_FOLLOWUP." WHERE followup_of = '".$or_id."' AND flollowup_type= '".FOLLOWUP::ORDER."'";
                        $db->query($query);
                        if($db->nf()>0){
                            $db->next_record();
                            $flwnumber=$db->f('followup_no');
                        }                    
                        $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                            ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".title_id 		= '". $data ['title_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".remarks 		= 'Invoice for the order has been created'"
                                //.",". TABLE_SALE_FOLLOWUP .".date    		= '". $data['date'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".time    		= '". $data ['hrs'].':'.$data ['min'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".time_type 		= '". $data ['time_type'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".lead_id    	= '". $data ['lead_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data['client']['user_id'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".assign_to 		= '". $data ['assign_to'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $variables['hid'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".table_name    	= 'billing_inv'"
                                .",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". FOLLOWUP::INVOICE ."'"
                                .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
                        $db->query($query); 
                    //EOF Insert followup
                    
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
                    Order::getParticulars($db, $temp, '*', $condition_query);
                    if(!empty($temp)){
                        foreach ( $temp as $pKey => $parti ) {
                           if($temp[$pKey]['s_type']=='1'){
                                $temp[$pKey]['s_type']='Years';
                           }elseif($temp[$pKey]['s_type']=='2'){
                                $temp[$pKey]['s_type']='Quantity';
                           }
                            $temp_p[]=array(
                                            'p_id'=>$temp[$pKey]['id'],
                                            'particulars'=>$temp[$pKey]['particulars'],
                                            'p_amount' =>$temp[$pKey]['p_amount'] ,                                   
                                            's_type' =>$temp[$pKey]['s_type'] ,                                   
                                            's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                            's_amount' =>$temp[$pKey]['s_amount'] ,                                   
                                            's_id' =>$temp[$pKey]['s_id'] ,                                   
                                            'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                            'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                            'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                            'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                            'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                            'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                            'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                            'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                            'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                            );
                        }
                    }
                    $data['particulars'] = $temp_p ;
                   
                    include_once ( DIR_FS_CLASS .'/Region.class.php');
                    $region         = new Region();
                    $region->setAddressOf(TABLE_CLIENTS,  $data['client']['user_id']);
                    $addId = $data['billing_address'] ;
                    $address_list  = $region->get($addId);
                    $data['b_address'] = $address_list ;
                    
                    // Update the Order. for time being
                     /*                         
                    if ( !Invoice::setOrderStatus($db, $or_id, Invoice::COMPLETED) ) {
                        $messages->setErrorMessage("The Order was not Marked as Completed.");
                    }
                    */
                    $or_id = NULL;
                    
                    
                    // Update the Client Services.
                    /*
                    if ( !Invoice::UpdateClientService($db, $data['client']['user_id'], $data['service_id']) ) {
                        $messages->setErrorMessage("The Clients Services were not updated.");
                    }
                    */
                    
                    // Create the Invoice PDF, HTML in file.
                    $extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';
                   
                    if ( !($attch = Invoice::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Invoice file was not created.");
                    }else{
                    
                        $file_name = DIR_FS_INV_FILES ."/". $data["number"] .".html";
                    }
                   
                    // Send the notification mails to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_inv_add', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_inv_add_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                                            
                    //Get details of Client BOF
                    /*
                    $condition = " WHERE user_id = '". $data['client']['user_id'] ."' ";
                    $clientdetails      = null;
                    Clients::getList($db, $clientdetails, 'f_name, l_name, org, email, manager ', $condition);
                    */
                    
                    
                    
                    //Get details of Client EOF
                    
                    // BO: Set up Reminder for invoice expiry, if applicable.
                    if ( !empty($data["do_e"]) ) {
                    
                        $data["invoice_no"]		= $data["number"];
                        $data["expiry_date"] 	= $data["do_e"];
                        $data["expiry_date_int"]= strtotime($data["do_e"]);
                        
                        $data["fname"] 			= $data['client']["f_name"];
                        $data["lname"] 			= $data['client']["l_name"];
                        $data["company"] 		= $data['client']["org"];
                        $data["email"]          = $data['client']["email"];
                        $data["amount"] 		= $data["amount"];
                       
                        $data["currency"] 		= $data["currency"];
                        $billing_name = $data["fname"]." ".$data["lname"] ;
                        $billing_email=$data['client']["email"];
                        /*
                        
                        if ( getParsedEmail($db, $s, 'EMAIL_REMINDER_INVOICE_EXPIRY', $data, $reminder_email) ) {
                            include (DIR_FS_LIB ."/includes/reminder.inc.php");
                            include_once ( DIR_FS_CLASS .'/Reminder.class.php');
                            $reminder = new Reminder($db, TABLE_REMINDERS, $messages, 'UNIX');
                            $query = "SELECT setting_datatype, setting_text "
                                        ." FROM ". TABLE_GLOBAL_SETTINGS 
                                        ." WHERE setting_title = 'Invoice reminder first' ";

                            if ( $db->query($query) && $db->nf()>0 && $db->next_record()) {
                                $data['repeat_for'] = $db->f("setting_text");
                            }
                            
                            // Set the dates to send reminders.
                            $data['repeat_for']	 	= explode(',', $data['repeat_for']);
                            $repeat_for_arr			= array();
                            
                            foreach ($data['repeat_for'] as $key=>$item) {
                                $item = str_replace(' ', '', $item);
                                if ( !empty($item) ) {
                                    $repeat_for_arr[]	= trim($item);
                                }
                                $item = NULL;
                            }
                            rsort($repeat_for_arr, SORT_NUMERIC);	// Sort the interval in descending order.
                
                            // Set the time when the first reminder will be sent.
                            if ( $reminder->getTimestamp() == 'MYSQL' ) {
                                $data['date_next']	= date("Y-m-d H:i:s", strtotime("-". $repeat_for_arr[0] .' day ' 
                                                                                    ." ". $data["expiry_date_int"] ));
                            }elseif ( $reminder->getTimestamp() == 'UNIX' ) {
                                $data['date_next']	= strtotime("-". $repeat_for_arr[0] .' day ', $data["expiry_date_int"]);
                            }

                            // Remove the element for which the date has been set.
                            $data['repeat_for']		= implode(',', array_slice($repeat_for_arr, 1));
                            
                            // Set the emails on which to send reminder.
                            $data['to_email'] = "$billing_name <$billing_email>";
                            $data['to_email'] .= ','. $data["fname"] .' '. $data["lname"] .' <'. $data["email"] .'>';
                            
                           
                           // if ( !empty($invoice["email2"]) ) {
                             //   $temp = explode(',', $invoice["email2"]);
                              //  foreach ( $temp as $email ) {
                                //    $data['to_email'] .= ','. $invoice["fname"] .' '. $invoice["lname"] .' <'. trim($email) .'>';
                                //}
                                //$temp = NULL;
                           // }
                            
                            $reminder->setEarlierDateAllowed(true);
                            $reminder->add(	'', 'rem_inv_exp', $reminder_email["subject"], $reminder_email["body"], 
                                                '', $data['to_email'], $data['date_next'], '', 
                                                $data['repeat_for'], 'day', strtotime($data["expiry_date"])
                                                , $reminder_email["isHTML"] );
                        }
                        
                        */
                    }                    
                    //Code for Reminder expiry EOF

 



                    // Organize the data to be sent in email.
                    /*
                    $data['link']   = DIR_WS_MP .'/bill-invoice.php?perform=view&inv_id='. $variables['hid'];

                    // Read the Client Manager information.
                    
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');
                    
                    // Send Email to the Client.
                   if( isset($mail_client) &&  $mail_client==1 ){ 
                       
                        $email = NULL;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT', $data, $email) ) {
                        
                            $to     = '';
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                           
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                            
                            
                        }
                        
                    }
                    */
                    
                    /*
                    $data['link']   = DIR_WS_NC .'/bill-invoice.php?perform=view&inv_id='. $variables['hid'];
                   
                     // Send mail to the Concerned Executives with same access level and higher access levels
                    if(isset($data['mail_exec_ac'])){
                       
                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_INV_NEW_MANAGERS', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                            }
                        }
                    }
                    // Send Email to the Client Manager.
                    if(isset($data['mail_client_mgr'])){ 
                       
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT_MANAGER', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $data['manager']['f_name'] .' '. $data['manager']['l_name'], 'email' => $data['manager']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                        }
                    }*/
                    //to flush the data.
                    $or_no_arr=explode("-",$_ALL_POST['or_no'] );
                    $financial_yr=$or_no_arr['2']."-".$or_no_arr['3'];
                    $_ALL_POST  = NULL;
                    $_ALL_POST['number'] = getOldCounterNumber($db,'INV',$data['company_id'],$financial_yr); 
                    
                    $data       = NULL;
                }
            }
        }
        else {
            // Set up the default values to be displayed.
            // $_ALL_POST['number'] = "PT". date("Y") ."-INV-". date("mdhi-s") ;
            //As invoice-number formate changed on the basis of ( fanancial yr + sr no ) 
          
        }

        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/bill-invoice.php?added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/bill-invoice.php");
        }


        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if (  isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            //include ( DIR_FS_NC .'/bill-invoice-list.php');
            header("Location:".DIR_WS_NC."/bill-invoice.php?added=1");
        }
        else {
        
            if ( !empty($or_id) ) {
                // Read the Order details.
                include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
                $order  = NULL;              
                
                $fields = TABLE_BILL_ORDERS.'.id,'.TABLE_BILL_ORDERS .'.do_o AS do_o,'.TABLE_BILL_ORDERS .'.number AS or_no,'. TABLE_BILL_ORDERS .'.access_level, '. TABLE_BILL_ORDERS .'.status, '.TABLE_BILL_ORDERS.'.details';
                $fields .= ', '.TABLE_BILL_ORDERS.'.order_title, '. TABLE_BILL_ORDERS .'.order_closed_by';
                $fields .= ', '.TABLE_BILL_ORDERS.'.do_e, '.TABLE_BILL_ORDERS .'.do_fe';
                $fields .= ', '.TABLE_BILL_ORDERS.'.currency_id, '. TABLE_BILL_ORDERS .'.company_id  ,'.TABLE_BILL_ORDERS .'.amount';
                $fields .= ', '.TABLE_CLIENTS.'.user_id, '. TABLE_CLIENTS .'.number, '.TABLE_CLIENTS.'.f_name, '.TABLE_CLIENTS.'.l_name';
                $fields .= ', '.TABLE_BILL_ORDERS.'.client, '. TABLE_USER .'.number AS u_number, '.TABLE_USER.'.f_name AS u_f_name, '.TABLE_USER.'.l_name AS u_l_name ';
                
                if ( Order::getDetails($db, $order, $fields, " WHERE ".TABLE_BILL_ORDERS.".id = '". $or_id ."' ") > 0 ) {
                    $order = $order[0];
                     
                    if ( ($order['status'] == Order::ACTIVE) || ($order['status'] == Order::PROCESSED) || ($order['status'] == Order::COMPLETED) ) {
                        if ( $access_level > $order['access_level'] ) {
                            $_ALL_POST['or_no']             = $order['or_no'];
                           // $_ALL_POST['or_particulars']    = $order['particulars'];
                            $_ALL_POST['or_details']        = $order['details'];
                            $_ALL_POST['or_user_id']        = $order['user_id'];
                            $_ALL_POST['or_number']         = $order['number'];
                            $_ALL_POST['or_f_name']         = $order['f_name'];
                            $_ALL_POST['or_l_name']         = $order['l_name'];
                            $_ALL_POST['or_access_level']   = $order['access_level'];
                         
                            $_ALL_POST['do_o']              = $order['do_o'];
                            $_ALL_POST['u_f_name']          = $order['u_f_name'];
                            $_ALL_POST['u_l_name']          = $order['u_l_name'];
                            $_ALL_POST['u_number']          = $order['u_number'];
                            $_ALL_POST['order_title']       = $order['order_title'];
                            $_ALL_POST['order_closed_by']   = $order['order_closed_by'];
                            $_ALL_POST['do_e']   = $order['do_e'];
                            $_ALL_POST['do_fe']   = $order['do_fe'];
                            
                            if($_ALL_POST['do_e'] !='0000-00-00 00:00:00'){
                                $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                                $temp               = explode('-', $_ALL_POST['do_e'][0]);
                                $_ALL_POST['do_e']  = NULL;
                                $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                            }else{
                            
                                $_ALL_POST['do_e']  ='';
                            }
                            
                            if($_ALL_POST['do_fe'] !='0000-00-00 00:00:00'){
                                $_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
                                $temp               = explode('-', $_ALL_POST['do_fe'][0]);
                                $_ALL_POST['do_fe']  = NULL;
                                $_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                            }else{
                            
                                $_ALL_POST['do_fe']  ='';
                            }
                                    
                            
                            
                            if(!empty($order['order_closed_by'])){
                                $clientfields='';
                                $condition = TABLE_USER." WHERE  user_id='".$order['order_closed_by']."'";
                                $clientfields .= ' '.TABLE_USER.'.user_id, '. TABLE_USER .'.number, '.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
                                User::getList($db, $closed_by, $clientfields, $condition) ;
                                $_ALL_POST['order_closed_by_name']   = $closed_by['0'];
                            }
                                                    
                            
                            $_ALL_POST['amount']= $order['amount'];
                            $_ALL_POST['amount_inr']= $order['amount'] * $_ALL_POST['exchange_rate'];
                            $_ALL_POST['currency_id']= $order['currency_id'];
                            $_ALL_POST['company_id']= $order['company_id'];
                           
                            /*get particulars details bof*/
                            $temp = NULL;
                            $temp_p = NULL;
                            $condition_query = "WHERE ord_no = '". $order['or_no'] ."'";
                            Order::getParticulars($db, $temp, '*', $condition_query);
                            if(!empty($temp)){
                                foreach ( $temp as $pKey => $parti ) {
                                  if($temp[$pKey]['s_type']=='1'){
                                    $temp[$pKey]['s_type']='Years';
                                  }elseif($temp[$pKey]['s_type']=='2'){
                                    $temp[$pKey]['s_type']='Quantity';
                                  }
                                    $temp_p[]=array(
                                                    'p_id'=>$temp[$pKey]['id'],
                                                    'particulars'=>$temp[$pKey]['particulars'],
                                                    'p_amount' =>$temp[$pKey]['p_amount'] ,      
                                                    's_type' =>$temp[$pKey]['s_type'] ,                                   
                                                    's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                                    's_amount' =>$temp[$pKey]['s_amount'] ,                                                              
                                                    's_id' =>$temp[$pKey]['s_id'] ,                                   
                                                    'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                                    'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                                    'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                                    'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                                    'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                                    'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                                    'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                                    'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                                    'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                                    );
                                }
                            }
                            $_ALL_POST['particulars']=$temp_p ;
                            
                            /*get particulars details eof*/
                            $order = NULL;
                            // Read the Client Addresses.
                            include_once ( DIR_FS_CLASS .'/Region.class.php');
                            $region         = new Region();
                            $region->setAddressOf(TABLE_CLIENTS, $_ALL_POST['or_user_id']);
                            $_ALL_POST['address_list']  = $region->get();
                            
                            if ( (!isset($_POST['btnCreate']) && !isset($_POST['btnReturn'])) ) {
                                $_ALL_POST['billing_name']  = $_ALL_POST['or_f_name'] .' '. $_ALL_POST['or_l_name'];
                                //$_ALL_POST['particulars']   = array('');
                                //$_ALL_POST['p_amount']      = array(0);
                            }

							$al_list = getAccessLevel($db, $access_level);
							$index = array();
							if ( isset($_ALL_POST['or_access_level']) ) {
								array_key_search('access_level', $_ALL_POST['or_access_level'], $al_list, $index);
								$_ALL_POST['or_access_level'] = $al_list[$index[0]]['title'];
							}
							
                            // As invoice-number formate changed on the basis of ( fanancial yr +  company_id + sr no ) 
                            
                            $or_no_arr=explode("-",$_ALL_POST['or_no'] );
                            $financial_yr=$or_no_arr['2']."-".$or_no_arr['3'];
                            
                            $_ALL_POST['number'] = getOldCounterNumber($db,'INV',$_ALL_POST['company_id'],$financial_yr); 
                            
            
							$hidden[] = array('name'=> 'or_id' ,'value' => $or_id);
							$hidden[] = array('name'=> 'or_no' ,'value' => $_ALL_POST['or_no']);
							$hidden[] = array('name'=> 'client' ,'value' => $_ALL_POST['or_user_id']);
							$hidden[] = array('name'=> 'company_id' ,'value' => $_ALL_POST['company_id']);
                            
							$hidden[] = array('name'=> 'perform' ,'value' => 'add_o');
							$hidden[] = array('name'=> 'act' , 'value' => 'save');
				
							$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
							$page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
							$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
							$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                            //$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
							//$page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
							$page["var"][] = array('variable' => 'currency', 'value' => 'currency');
							
							$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-add-o.html');
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create invoice for this Order.");
                            $_ALL_POST = '';
                        }
                    }
                    else {
                        $messages->setErrorMessage("The Order is not yet approved.");
                        $_ALL_POST = '';
                    }
                }
                else {
                    $messages->setErrorMessage("The Selected Order was not found.");
                }
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>