<?php   

if( $perm->has('nc_vjb') && $perm->has('nc_vjb_add') ){
    
	 
	
	
    $or_id = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"])          ? $_POST["or_id"]       :'');
    $job_id = isset($_GET["job_id"]) ? $_GET["job_id"]: ( isset($_POST["job_id"])  ? $_POST["job_id"]       :'');
    
    $scount = isset($_GET["scount"]) ? $_GET["scount"] : ( isset($_POST["scount"]) ? $_POST["scount"]  :'1');   
    
    include_once (DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/vendors-jobwork.inc.php' );
    
    if(!empty($or_id)){
        $order_list = NULL;
        $required_fields ='client'.','.'order_title,do_o,amount,number';
        $condition_query="WHERE id='".$or_id."'";
		Order::getList($db,$order_list,$required_fields,$condition_query);
		$order_details = $order_list[0];
        
    }
	
    $required_fields="*";
    $condition_query=" WHERE ord_id='".$or_id."'";
    VendorsJobwork::getList($db,$target_list,$required_fields,$condition_query);
  
    $_ALL_POST	    = NULL;
    $access_level   = $my['access_level'];
    
    //Code to delete the bifurcation row bof
    if(!empty($job_id)){
         
		$fields = TABLE_VENDORS_JOBWORK.".*,".TABLE_CLIENTS.".billing_name,".TABLE_CLIENTS.".f_name,"
		.TABLE_CLIENTS.".l_name";
		VendorsJobwork::getList( $db, $job_data, $fields, $condition_query, $next_record, $rpp);
		$_ALL_POST = $job_data[0];
		$_ALL_POST['do_job']  = explode(' ', $_ALL_POST['do_job']);
		$temp               = explode('-', $_ALL_POST['do_job'][0]);
		$_ALL_POST['do_job']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
        $_ALL_POST['client_details']  = $_ALL_POST['billing_name']." (".$_ALL_POST['f_name']." ".$_ALL_POST['l_name'].") " ;
		$_ALL_POST['job_id'] = $job_id ;
		 
    }
    //Code to delete the bifurcation row eof

    
    if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save' ) {
       
        $_ALL_POST  = $_POST;
        
        $data = processUserData($_ALL_POST);    
       
        $extra = array( 'db'        => &$db,
                'messages'          => &$messages
            );
            
		if(empty($job_id)){
		
			if ( VendorsJobwork::validateAdd($data, $extra) ) {
					
				if ( isset($data['do_job']) && !empty($data['do_job']) ) {
					$data['do_job'] = explode('/', $data['do_job']);
					$data['do_job'] = mktime(0, 0, 0, $data['do_job'][1], $data['do_job'][0], $data['do_job'][2]);
				}
				
				$sql="INSERT INTO ".TABLE_VENDORS_JOBWORK
					." SET ". TABLE_VENDORS_JOBWORK .".ord_id       = '". $or_id ."'"
					.",". TABLE_VENDORS_JOBWORK .".ord_no = '".    $order_details['number'] ."'"
					.",". TABLE_VENDORS_JOBWORK .".vendors_id = '".    $data['vendors_id'] ."'"
					.",". TABLE_VENDORS_JOBWORK .".title = '".    $data['title'] ."'"
					.",". TABLE_VENDORS_JOBWORK .".vendor_instructions 	 = '".$data['vendor_instructions'] ."'"
					.",". TABLE_VENDORS_JOBWORK .".team_instructions = '".$data['team_instructions'] ."'"
					.",". TABLE_VENDORS_JOBWORK .".do_job = '".date('Y-m-d H:i:s',$data['do_job']) ."'"
					.",". TABLE_VENDORS_JOBWORK .".added_by = '".   $my['user_id']."'"
					.",". TABLE_VENDORS_JOBWORK .".added_by_name = '".$my['f_name']." ".$my['l_name']."'"
					.",". TABLE_VENDORS_JOBWORK .".do_e = '".   date('Y-m-d H:i:s')."'" ;
				$db->query($sql);
				
				$messages->setOkMessage("Vendor Job has been added.");       
				$_ALL_POST	    = NULL;
			}
        }else{
		
			if ( VendorsJobwork::validateUpdate($data, $extra) ) {
				 
				if(!empty($job_data[0]['updated_by'])){
					$added_by=$job_data[0]['updated_by'] ;
					$added_by_name =$job_data[0]['updated_by_name'];
				    $do_e = $job_data[0]['do_update'];
				
				}else{
				
					$added_by=$job_data[0]['added_by'] ;
					$added_by_name =$job_data[0]['added_by_name'];
				    $do_e = $job_data[0]['do_e'];
				}
				
				
				
				
				$sql1="INSERT INTO ".TABLE_VENDORS_JOBWORK_HISTORY
				." SET ". TABLE_VENDORS_JOBWORK_HISTORY .".vjb_id       = '". $job_id ."'"				 
			   .",". TABLE_VENDORS_JOBWORK_HISTORY .".vendor_instructions =
			   '".$job_data[0]['vendor_instructions'] ."'"				
				.",". TABLE_VENDORS_JOBWORK_HISTORY .".added_by = '". $added_by."'"
				.",". TABLE_VENDORS_JOBWORK_HISTORY .".added_by_name = '".$added_by_name."'"
				.",". TABLE_VENDORS_JOBWORK_HISTORY .".do_e = '". $do_e."'" ;
				$db->query($sql1);
				
				
				$sql="UPDATE ".TABLE_VENDORS_JOBWORK
					." SET ". TABLE_VENDORS_JOBWORK .".vendor_instructions 	 = '".$data['vendor_instructions'] ."'"
					.",". TABLE_VENDORS_JOBWORK .".updated_by = '".   $my['user_id']."'"
					.",". TABLE_VENDORS_JOBWORK .".updated_by_name = '".$my['f_name']." ".$my['l_name']."'"
					.",". TABLE_VENDORS_JOBWORK .".do_update = '".   date('Y-m-d H:i:s')."'
					WHERE id=".$job_id ;
				$db->query($sql);
				
				$messages->setOkMessage("Vendor Instructions has been updated.");       
				$_ALL_POST	    = NULL;
			}
		
		}
    }
 
     // get list of all comments of change log BOF
    $condition_query1 = $condition_url1 =$extra_url= '';
    $condition_url1 .="&perform=".$perform."&or_id=".$or_id;
    $perform = 'followup';
    $followupList	= 	NULL;
    $fields = TABLE_VENDORS_JOBWORK.".*,".TABLE_CLIENTS.".billing_name,".TABLE_CLIENTS.".f_name,"
	.TABLE_CLIENTS.".l_name";
    $condition_query1 = " WHERE ".TABLE_VENDORS_JOBWORK.".ord_id=".$or_id;
    $condition_query1 .= " ORDER BY ".TABLE_VENDORS_JOBWORK.".do_e DESC";
    $total	=	VendorsJobwork::getList( $db, $list, $fields , $condition_query1);
   
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
	
	$list	= NULL;
    $fields = TABLE_VENDORS_JOBWORK.".*,".TABLE_CLIENTS.".billing_name,".TABLE_CLIENTS.".f_name,"
	.TABLE_CLIENTS.".l_name";
    VendorsJobwork::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    //$extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  .= "&perform=".$perform;
    $extra_url  .= "&or_id=".$or_id ;
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $followupList	= NULL;
     $condition_url .="&perform=".$perform;
    
    $variables['can_edit'] = true;
    if ( $perm->has('nc_vjb_edit') ) {
        $variables['can_edit'] = true;
    }
  
    
    $hidden[] = array('name'=> 'or_id', 'value' => $or_id);
    $hidden[] = array('name'=> 'job_id', 'value' => $job_id);
    //$hidden[] = array('name'=> 'client_id', 'value' => $client_id);
    $hidden[] = array('name'=> 'perform', 'value' => 'vendors_job');
    $hidden[] = array('name'=> 'act', 'value' => 'save');
    
    $page["var"][] = array('variable' => 'job_id', 'value' => 'job_id');
    
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'order_details', 'value' => 'order_details');
    $page["var"][] = array('variable' => 'order_title', 'value' => 'order_title');
    $page["var"][] = array('variable' => 'bifurcationList', 'value' => 'list');
    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-vendors-jobwork.html');
}else{
    $messages->setErrorMessage("You do not have the Permission to access this module.");
}   
?>