<?php
    if ( $perm->has('nc_nwl_smtp_delete') ) {
	    $smtp_id = isset($_GET["smtp_id"]) ? $_GET["smtp_id"] : ( isset($_POST["smtp_id"]) ? $_POST["smtp_id"] : '' );
        
        
                
        $extra = array( 'db'           => &$db,
                        'messages'     => &$messages
                        );
        NewsletterSmtp::delete($smtp_id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/newsletter-smtp-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this module.");
         //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
