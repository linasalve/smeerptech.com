<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/client-domains.inc.php');	   
    
    $client = isset($_GET['client']) ? $_GET['client'] : '';
	  
    $optionLink =$stringOpt= '' ;
    if(!empty($client)){
        $sString = trim($client);
    }
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if( (!empty($sString)) ){	
        header("Content-Type: application/json");
		
	    $query =" SELECT DISTINCT(".TABLE_CLIENT_DOMAINS.".id),".TABLE_CLIENT_DOMAINS.".domain FROM 
		".TABLE_CLIENT_DOMAINS." WHERE ".TABLE_CLIENT_DOMAINS.".client='".$sString."' AND ".TABLE_CLIENT_DOMAINS.".status='1'";		
		 
        $db->query($query);
        //$total	= $db->nf();	
		if( $db->nf() > 0 ){
			while($db->next_record()){
				$list .=  $db->f("domain") ."|". $db->f("id") .",";
			}
			echo rtrim($list,',');
		}else{		
			echo "No data|0";
		}	   
	}
 
include_once( DIR_FS_NC ."/flush.php");

?>
