<?php
	if ( $perm->has('nc_al_status') ) {
        $al_id	= isset($_GET["al_id"]) ? $_GET["al_id"] 	: ( isset($_POST["al_id"]) ? $_POST["al_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"]  : ( isset($_POST["status"]) ? $_POST["status"] : '' );
        
        $extra = array( 'db' 		=> &$db,
                        'messages'  => $messages
                    );
        
        AccessLevel::updateStatus($al_id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/settings-access-level-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>