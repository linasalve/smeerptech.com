<?php
    if ( $perm->has('nc_p_pt_delete') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
        /*
        if ( $perm->has('nc_ts_delete_al') ) {
            $access_level += 1;
        }
        */
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        Paymenttransaction::delete($id, $extra);
        
        // Display the list.
        
        include ( DIR_FS_NC .'/payment-transaction-search.php');
       
    }
    else {
        
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-read-permission.html');
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
