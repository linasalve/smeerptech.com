<?php

       $showall	= isset($_GET["showall"]) 	? $_GET["showall"]	: ( isset($_POST["showall"]) 	? $_POST["showall"] : '' );
       
        if ($showall != '1'){
            if ( !isset($condition_query) || $condition_query == '' ) {
                $condition_query = ' WHERE (';
            }
            else {
                $condition_query .= ' AND (';
            }
        }
        $access_level   = $my['access_level'];
        
        // If the User has the Right to View Clients of the same Access Level.
        /*if ( $perm->has('nc_ue_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " (". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
        $condition_query .= ')';*/
        
        if ($showall != '1'){
            $condition_query .= TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",'";
            $condition_query .= ')';
        }
        
		if(!empty($order_by_table))
        	$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	Order::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        Order::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_view_list']     = true;
        $variables['can_add']           = true;
        $variables['can_edit']          = true;
        $variables['can_delete']        = true;
        $variables['can_view_details']  = true;
        $variables['can_change_status'] = true;

        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'showall', 'value' => 'showall');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-choose-list.html');
  
?>