<?php

    if ( $perm->has('nc_pop_details') ) {
        $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');
        $no = isset ($_GET['no']) ? $_GET['no'] : ( isset($_POST['no'] ) ? $_POST['no'] :'');
        
        if(!empty($id)){
			$condition_query= " WHERE ". TABLE_PURCHASE_ORDER_PDF .".id = '". $id ."'"	;
        }
		if(!empty($no)){
			$condition_query= " WHERE ". TABLE_PURCHASE_ORDER_PDF .".number = '". $no."'"	;
        }
		
        $access_level   = $my['access_level'] + 1;
         
        
        $list	= NULL;
        if ( PurchaseOrderPdf::getDetails( $db, $list, TABLE_PURCHASE_ORDER_PDF .'.id, '. TABLE_PURCHASE_ORDER_PDF .'.number', $condition_query) ) {
          
            
			
            $file_path = DIR_FS_PO_FILES ."/";
            $file_name = $list[0]["number"] .".pdf";
            $content_type = 'application/pdf';
			
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-type: $content_type");
            header("Content-Disposition: attachment; filename=". $file_name );
            header("Content-Length: ".filesize($file_path.$file_name));
            readfile($file_path.$file_name);
			
		} else {
            
			$messages->setErrorMessage("The File is not found or you do not have the permission to view the file.");
		    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
          
        }
    } else {
       
		
		$messages->setErrorMessage("You do not have the permission to view the file.");
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
        
		
    }
   
?>