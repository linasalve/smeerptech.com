<?php
  if ( $perm->has('nc_sms_api_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		$lst_api = null;
        $fields = TABLE_SMS_API.'.id'
                   .','. TABLE_SMS_API .'.gateway_name'
                   .','. TABLE_SMS_API .'.name';
        SmsApi::getList($db,$lst_api,$fields);
        //print_R($lst_api);
         
		// Include the payment party class
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );               
                if ( SmsSender::validateAdd($data, $extra) ) { 
                        $query	= " INSERT INTO ".TABLE_SMS_SENDER
                            ." SET ".TABLE_SMS_SENDER .".api_id 		= '". $data['api_id'] ."'"  
                                	.",". TABLE_SMS_SENDER .".sender_name	= '". $data['sender_name'] ."'"                      
                                    .",". TABLE_SMS_SENDER .".created_by 	= '". $my['user_id'] ."'"                    
                                    .",". TABLE_SMS_SENDER .".created_by_name 	= '". $my['f_name']." ".$my['l_name'] ."'"                    
									.",". TABLE_SMS_SENDER .".status 		= '". $data['status'] ."'"                    
                                    .",". TABLE_SMS_SENDER .".ip 			= '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_SMS_SENDER .".do_e = '". 	date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sms-sender.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sms-sender.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sms-sender.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'lst_api', 'value' => 'lst_api');     
            
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-sender-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
