<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_site_sp_view') ) {
        $page_id = isset($_GET["page_id"]) ? $_GET["page_id"] : ( isset($_POST["page_id"]) ? $_POST["page_id"] : '' );
    
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        $al_list    = getAccessLevel($db, $access_level);

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list
                        );
            
            if ( Staticpages::validateUpdate($data, $extra) ) {
                $query  = " UPDATE ". TABLE_SETTINGS_STATICPAGES
                        ." SET "
							. TABLE_SETTINGS_STATICPAGES .".page_name			= '". $data["page_name"] ."'"
						.",". TABLE_SETTINGS_STATICPAGES .".page_intro			= '". $data["page_intro"] ."'"
						.",". TABLE_SETTINGS_STATICPAGES .".metakeyword			= '". $data["metakeyword"] ."'"
						.",". TABLE_SETTINGS_STATICPAGES .".metadesc			= '". $data["metadesc"] ."'"
						.",". TABLE_SETTINGS_STATICPAGES .".heading				= '". $data["heading"] ."'"
						.",". TABLE_SETTINGS_STATICPAGES .".content				= '". $data["content"] ."'"
						.",". TABLE_SETTINGS_STATICPAGES .".page_status			= '". $data["page_status"] ."'"
						.",". TABLE_SETTINGS_STATICPAGES .".last_edited_by		= '". $my['user_id'] ."'"
						.",". TABLE_SETTINGS_STATICPAGES .".access_level		= '". $data['access_level'] ."'" 
						." WHERE ". TABLE_SETTINGS_STATICPAGES .".page_id   	= '". $data['page_id'] ."'";
                if ( $db->query($query) ) {
                    $messages->setOkMessage("Clients Profile has been updated.");
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Page was not updated.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/staticpage-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE page_id = '". $page_id ."' ";
                $_ALL_POST      = NULL;
                if ( Staticpages::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                }
            }

            if ( !empty($_ALL_POST['page_id']) ) {

                // Check the Access Level.
                if ( $_ALL_POST['access_level'] < $access_level ) {

                    //print_r($_ALL_POST);
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'page_id', 'value' => $page_id);
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
        
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'staticpage-view.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Client with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected Page was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>