<?php
    if ( $perm->has('nc_pin_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];           
        
		// Include the  class.
		include_once (DIR_FS_INCLUDES .'/payment-in.inc.php');
		
         
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Paymentin::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_PAYMENT_IN
                            ." SET ". TABLE_PAYMENT_IN .".payment_from = '".            $data['payment_from'] ."'"
                                .",". TABLE_PAYMENT_IN .".amount = '".                  $data['amount'] ."'"
                                .",". TABLE_PAYMENT_IN .".access_level = '".            $my['access_level'] ."'"
                                .",". TABLE_PAYMENT_IN .".created_by = '".              $data['created_by'] ."'"
                                .",". TABLE_PAYMENT_IN .".mode = '".                    $data['mode'] ."'"
                                .",". TABLE_PAYMENT_IN .".received_by = '".             $data['received_by'] ."'"
                                .",". TABLE_PAYMENT_IN .".pay_purpose = '".             $data['pay_purpose'] ."'"
                                .",". TABLE_PAYMENT_IN .".date = '".                    $data['date'] ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/payment-in-list.php');
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-in-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>