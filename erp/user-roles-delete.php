<?php
    if ( $perm->has('nc_urol_delete') ) {
        $role_id= isset($_GET["role_id"]) ? $_GET["role_id"] : ( isset($_POST["role_id"]) ? $_POST["role_id"] : '' );
        
        $extra = array( 'db' 		=> &$db,
                        'messages' 	=> $messages
                    );
        UserRoles::delete($db, $role_id, $extra);
        
        include ( DIR_FS_NC .'/user-roles-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>