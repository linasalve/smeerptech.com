<?php

    if ( $perm->has('nc_bl_inv_rm_list') ) {
        
        if ( !isset($condition_query) || $condition_query == '' ) {

            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(Invoice::PENDING_INV_R);            
            
            $condition_query = " WHERE ( ". TABLE_INV_REMINDER .".status = '". Invoice::PENDING_INV_R."')"   ;
        }
     
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
        
            $extra_url  = $condition_url;
            
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }        
        $condition_url .="&perform=".$perform;
		
        //Code to calculate total of Amount EOF
        
        // To count total records.
        $list	= 	NULL;
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
		$db1 = new db_local; // database handle
		
        if($searchStr==1){
            $fields = TABLE_INV_REMINDER .'.id'
					.','. TABLE_INV_REMINDER .'.inv_no'
					.','. TABLE_INV_REMINDER .'.pinv_no'
					.','. TABLE_INV_REMINDER .'.status'
					.','. TABLE_INV_REMINDER .'.client_id'
					.','. TABLE_INV_REMINDER .'.do_expiry'
					.','. TABLE_INV_REMINDER .'.send_reminder_dt'
					.','. TABLE_INV_REMINDER .'.inv_id'
					.','. TABLE_INV_REMINDER .'.pinv_id'
					.','. TABLE_CLIENTS .'.number'
					.','. TABLE_CLIENTS .'.f_name'
					.','. TABLE_CLIENTS .'.l_name'  
					.','. TABLE_CLIENTS .'.billing_name'  ;
            
			  $sql ="SELECT COUNT(".TABLE_INV_REMINDER .".id) as count FROM ".TABLE_INV_REMINDER." LEFT JOIN 
			  ".TABLE_CLIENTS." ON ".TABLE_INV_REMINDER.".client_id = ".TABLE_CLIENTS.".user_id ".$condition_query;              $db->query($sql) ;
			  if ( $db->nf() > 0 ) {			  
				while ($db->next_record()){
				  $total =$db->f('count');
				}
			  }
			  //$total= $db->nf();
			  
			  $sql ="SELECT ".$fields." FROM ".TABLE_INV_REMINDER." LEFT JOIN 
			  ".TABLE_CLIENTS." ON ".TABLE_INV_REMINDER.".client_id = ".TABLE_CLIENTS.".user_id ".$condition_query;            	 $sql .= " LIMIT ". $next_record .", ". $rpp;
			  $db->query($sql) ;
			  $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra'); 
			  if ( $db->nf() > 0 ) {			  
					while ($db->next_record()){
					  $id =$db->f('id');
					  $inv_no =$db->f('inv_no');
					  $pinv_no =$db->f('pinv_no');
					  $status =$db->f('status');
					  $client_id =$db->f('client_id');
					  $client_fname =$db->f('f_name');
					  $client_lname =$db->f('l_name');
					  $client_number =$db->f('number');
					  $billing_name =$db->f('billing_name');
					  $inv_id =$db->f('inv_id');
					  $pinv_id =$db->f('pinv_id');
					  $send_reminder_dt =$db->f('send_reminder_dt');
					  $do_expiry =$db->f('do_expiry');
					    //check invoice id followup in ST BOF
						$ticket_id=0;
						/* 
						$sql2 = " SELECT ticket_id FROM ".TABLE_ST_TICKETS." WHERE 
						( ".TABLE_ST_TICKETS.".invoice_id = '".$inv_id."' OR 
						".TABLE_ST_TICKETS.".invoice_profm_id = '".$pinv_id."') 
						AND ".TABLE_ST_TICKETS.".ticket_child=0  LIMIT 0,1";
						if ( $db1->query($sql2) ) {
							if ( $db1->nf() > 0 ) {
								while ($db1->next_record()) {
								   $ticket_id= $db1->f('ticket_id');
								}
							}                   
						} 
						*/
					    //check invoice id followup in ST EOF
					
					    $list[]=array('inv_no'=>$inv_no,
									'pinv_no'=>$pinv_no,
									'id'=>$id,
									'status'=>$status,
									'billing_name'=>$billing_name,
									'client_id'=>$client_id,
									'client_fname'=>$client_fname,
									'client_lname'=>$client_lname,
									'client_number'=>$client_number,
									'inv_id'=>$inv_id,
									'pinv_id'=>$pinv_id,
									'ticket_id'=>$ticket_id,
									'do_expiry'=>$do_expiry,				  
									'send_reminder_dt'=>$send_reminder_dt				  
									);
					}
			} 

		}
        
      
        // Set the Permissions.
        $variables['can_view_list'] = false;
        $variables['can_followup']	= false;
        $variables['can_send_manually']	= false;

        if ( $perm->has('nc_bl_inv_rm_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_bl_inv_rm_add') ) {
            $variables['can_send_manually']  = true;
        } 
        
        if ( $perm->has('nc_st') && $perm->has('nc_st_flw_inv') ) {
            $variables['can_followup'] = true;
        }
        
        
        $page["var"][] = array('variable' => 'total', 'value' => 'total');
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-reminder-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>