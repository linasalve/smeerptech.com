<?php
    if ( $perm->has('nc_bl_or_add') ) {
    
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/quotation.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
       
        $po_id          = isset($_GET["po_id"]) ? $_GET["po_id"] : ( isset($_POST["po_id"]) ? $_POST["po_id"] : '' );
        $lead_id        = isset($_GET["lead_id"]) ? $_GET["lead_id"] : ( isset($_POST["lead_id"]) ? $_POST["lead_id"] : '' );
        $q_id           = isset($_GET["q_id"]) ? $_GET["q_id"] : ( isset($_POST["q_id"]) ? $_POST["q_id"] : '' );
        //$tbl_name       = isset($_GET["tbl_name"]) ? $_GET["tbl_name"] : ( isset($_POST["tbl_name"]) ? $_POST["tbl_name"] : '' );
        //echo $tbl_name;
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }
		//Get company list 
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                   .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
      
        // Read the available source
       	Leads::getSource($db,$source);
        
		// Include the Work Stage class and Work Timeline class.
		include_once (DIR_FS_INCLUDES .'/work-stage.inc.php');
		include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
		include_once (DIR_FS_INCLUDES .'/user.inc.php');
		include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
		$lst_work_stage = NULL;
		WorkStage::getWorkStages($db, $lst_work_stage);
        // list of executives to assign project manager
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields);
        
        //code to generate $vendorOptionList BOF
        $vendorSql ="SELECT * FROM ".TABLE_VENDOR ;
        $db->query( $vendorSql );
        if( $db->nf() > 0 )
		{
            $vendorList['0'] = "" ;
			while($db->next_record())
			{
                $vendorList[$db->f('id')] = $db->f('f_name')." ".$db->f('l_name') ;
            }
            
        }
        //code to generate $vendorOptionList EOF
        
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);        
        // code for particulars bof 
         $rejectedFiles=null;
        // Read the Services.
        include_once ( DIR_FS_INCLUDES .'/services.inc.php');        
        
        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC"; 
        Services::getList($db, $lst_service, 'ss_id,ss_title,ss_punch_line, tax1_name, tax1_value,is_renewable ', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
                    $ss_id= $key['ss_id'];
                    $ss_title= $key['ss_title'];
                    $ss_punch_line= $key['ss_punch_line'];
                    $tax1_name = $key['tax1_name'];
                    $tax1_value = $key['tax1_value'];
                    $is_renewable = $key['is_renewable'];
                    $tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
                    
                    $keyNew['ss_id'] = $ss_id ; 
                    $keyNew['ss_title'] = $ss_title ; 
                    $keyNew['ss_punch_line'] = $ss_punch_line ; 
                    $keyNew['tax1_name'] = $tax1_name ; 
                    $keyNew['tax1_value'] = $tax1_value ; 
                    $keyNew['tax1_pvalue'] = $tax1_pvalue ; 
                    $keyNew['is_renewable'] = $is_renewable ; 
                    $lst_service[$val] = $keyNew;
            }
        }
        // code for particulars eof
        
        /* Get quotation details bof*/
          if(!empty($lead_id)){            
            
                   //Get data of q_id
                    $condition_queryd= " WHERE ".TABLE_SALE_QUOTATION .'.id='.$q_id ;
                    $fields = '*';
                    if ( Quotation::getDetails($db, $_ALL_POST, $fields, $condition_queryd) > 0 ) {
                        $_ALL_POST = $_ALL_POST['0'];
                        $_ALL_POST['do_e']='';
                        //$lead_id = $_ALL_POST['lead_id'];
                        $_ALL_POST['quotation_no']  = $_ALL_POST['number'];
                        
                        //Get Quotation files BOF
                        if( $_ALL_POST['quotation_no']){
                            $rejectedFiles = array();
                            Quotation::allRejectedQuotation($db,$_ALL_POST['parent_q_id'],$rejectedFiles);
                        }
                       
                        //Get Quotation files EOF
                        
                        
                        //$number = $_ALL_POST['number'];
                        
                        // Read the Particulars.
                            $temp_p = NULL;
                            $condition_query_p ='';
                            $service_idArr= array();
                            $condition_query_p = "WHERE ".TABLE_SALE_QUOTATION_P.".q_no = '". $_ALL_POST['number'] ."'";
                            Quotation::getParticulars($db, $temp_p, '*', $condition_query_p);
                            if(!empty($temp_p)){
                                foreach ( $temp_p as $pKey => $parti ) {
                                    
                                    $_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];
                                    $_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
                                    $_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];
                                    $_ALL_POST['s_type'][$pKey]       = $temp_p[$pKey]['s_type'];
                                    $_ALL_POST['s_quantity'][$pKey]       = $temp_p[$pKey]['s_quantity'];
                                    $_ALL_POST['s_amount'][$pKey]       = $temp_p[$pKey]['s_amount'];
                                    $_ALL_POST['is_renewable'][$pKey]      = $temp_p[$pKey]['is_renewable'];
                                    
                                    $_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
                                    $_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
                                    $_ALL_POST['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];
                                    $_ALL_POST['ss_title'][$pKey]   = $temp_p[$pKey]['ss_title'];
                                    $_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];
                                    $_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
                                    $_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
                                    $_ALL_POST['is_renewable'][$pKey]   = $temp_p[$pKey]['is_renewable'];
                                    $_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
                                    $_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
                                    $_ALL_POST['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
                                    $_ALL_POST['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
                                    $_ALL_POST['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
                                    $_ALL_POST['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];
                                    $service_idArr[] = $temp_p[$pKey]['s_id'];
                                }
                            }
                            $_ALL_POST['service_id'] = $service_idArr;
                    }else{
                        $messages->setErrorMessage("Quotation was not found.");
                    }
                   
                    $query="SELECT user_id,f_name,l_name,number,email FROM ".TABLE_CLIENTS." WHERE lead_id='".$lead_id."'";
                    
                    if( $db->query($query) && $db->affected_rows() > 0 ){
                        while($db->next_record()){
                            $_ALL_POST['client']=$db->f('user_id');
                            $_ALL_POST['client_details']=$db->f('f_name')."&nbsp;".$db->f('l_name')."&nbsp;(".$db->f('number').")"."&nbsp;(".$db->f('email').")";
                        }
                    }
            
            //As order-number formate changed on the basis of ( fanancial yr + sr no ) 
            //$_ALL_POST['number'] = getCounterNumber($db,'ORD'); 
            $_ALL_POST['exchange_rate'] = 1;             
         
        }
         
        /* Get quotation details eof*/
        
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_MULTIPLE_FILE_SIZE     ;
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'lst_service'       => $lst_service,
							'lst_work_stage'    => $lst_work_stage,
                            'messages'          => &$messages
                        );

           
            if ( Order::validateAddOld($data, $extra) ) {
			    
             
                if(!empty($data['po_filename'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['po_filename']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "PO-".$data['number'].".".$ext ;
                    $attachfilename = "PO-".mktime().".".$ext ;
                    $data['po_filename'] = $attachfilename;
                    
                    /*if (move_uploaded_file ($files['po_filename']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                        @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,'0777');
                    }*/
                    if (move_uploaded_file ($files['po_filename']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                        @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                          
                    }
                }
                
                if(!empty($data['filename_1'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_1']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "WO1-".$data['number'].".".$ext ;
                    $attachfilename = "WO1-".mktime().".".$ext ;
                     $data['filename_1'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_1']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                       
                      @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
                
                if(!empty($data['filename_2'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_2']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "WO2-".$data['number'].".".$ext ;
                    $attachfilename = "WO2-".mktime().".".$ext ;
                     $data['filename_2'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_2']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                         @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
                
                if(!empty($data['filename_3'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_3']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "WO3-".$data['number'].".".$ext ;
                    $attachfilename = "WO3-".mktime().".".$ext ;
                     $data['filename_3'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_3']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                       @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
              
                if($data['isrenewable']){
                    
                    $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                    $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                }
                
            
                $query	= " INSERT INTO ".TABLE_BILL_ORDERS
                            ." SET ". TABLE_BILL_ORDERS .".number           = '". $data['number'] ."'"
                                //.",". TABLE_BILL_ORDERS .".po_id = '".          $data['po_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".service_id       = '". implode(',', $data['service_id']) ."'"
                                .",". TABLE_BILL_ORDERS .".access_level     = '". $data['access_level'] ."'"
                                .",". TABLE_BILL_ORDERS .".client           = '". $data['client']['user_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".created_by       = '". $data['created_by'] ."'"
                                .",". TABLE_BILL_ORDERS .".company_id	    = '". $data['company_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_title	    = '". $data['order_title'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_domain	    = '". $data['order_domain'] ."'"
                                .",". TABLE_BILL_ORDERS .".currency_id      = '". $data['currency_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".quotation_no     = '". $data['quotation_no'] ."'"
                                .",". TABLE_BILL_ORDERS .".source_to        = '". $data['source_to'] ."'"
                                .",". TABLE_BILL_ORDERS .".source_from      = '". $data['source_from'] ."'"
                                .",". TABLE_BILL_ORDERS .".po_filename      = '". $data['po_filename'] ."'"
                                .",". TABLE_BILL_ORDERS .".filecaption_1    = '". $data['filecaption_1'] ."'"
                                .",". TABLE_BILL_ORDERS .".filename_1       = '". $data['filename_1'] ."'"
                                .",". TABLE_BILL_ORDERS .".filecaption_2    = '". $data['filecaption_2'] ."'"
                                .",". TABLE_BILL_ORDERS .".filename_2       = '". $data['filename_2'] ."'"
                                .",". TABLE_BILL_ORDERS .".filecaption_3    = '". $data['filecaption_3'] ."'"
                                .",". TABLE_BILL_ORDERS .".filename_3       = '". $data['filename_3'] ."'"
                                .",". TABLE_BILL_ORDERS .".amount           = '". $data['amount'] ."'"
                                .",". TABLE_BILL_ORDERS .".existing_client	= '". $data['existing_client'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_type	    = '". $data['order_type'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_closed_by	= '". $data['order_closed_by'] ."'"
                                .",". TABLE_BILL_ORDERS .".team             = '". implode(",", $data['team']) ."'"
                                .",". TABLE_BILL_ORDERS .".old_particulars = '".  $data['old_particulars'] ."'"
                                .",". TABLE_BILL_ORDERS .".details          = '". $data['details'] ."'"
                                .",". TABLE_BILL_ORDERS .".do_o             = '". date('Y-m-d H:i:s', $data['do_o']) ."'"
                                .",". TABLE_BILL_ORDERS .".do_c             = '". date('Y-m-d H:i:s', $data['do_c']) ."'"
                                .",". TABLE_BILL_ORDERS .".do_d             = '". date('Y-m-d H:i:s', $data['do_d']) ."'"
                                .",". TABLE_BILL_ORDERS .".do_e             = '". $do_e."'"
                                .",". TABLE_BILL_ORDERS .".do_fe            = '". $do_fe."'"
                                .",". TABLE_BILL_ORDERS .".project_manager  = '". $data['project_manager']."'"
                                .",". TABLE_BILL_ORDERS .".st_date          = '". $data['st_date']."'"
                                .",". TABLE_BILL_ORDERS .".es_ed_date       = '". $data['es_ed_date']."'"
                                .",". TABLE_BILL_ORDERS .".es_hrs           = '". $data['es_hrs']."'"
                                .",". TABLE_BILL_ORDERS .".status           = '". $data['status'] ."'" 
                                .",". TABLE_BILL_ORDERS .".is_old           = '1'" 
                                //.",". TABLE_BILL_ORDERS .".is_renewable     = '". $data['is_renewable'] ."'"
                                .",". TABLE_BILL_ORDERS .".ip           = '".$_SERVER['REMOTE_ADDR']."'" ;
               
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    
                    $variables['hid'] = $db->last_inserted_id();
                    
                    $messages->setOkMessage("New Order has been created.");
                    //Mark Quotation No As Completed BOF
                      
                    if(!empty($data['quotation_no'])){
                    
                          $access_level   = $my['access_level'];
                          $extra = array( 'db'           => &$db,
                                    'access_level' => $access_level,
                                    'messages'     => &$messages,                      
                            );
                        
                        $quotstatus = Quotation::COMPLETED ;
                        Quotation::updateStatus($data['quotation_no'], $quotstatus, $extra);
                        
                    }
                    //Mark Quotation No As Completed EOF
                    
                    
                    
                    //After insert, update order counter in 
                   // updateCounterOf($db,'ORD', $data['company_id']);
                    
                    
                    
                    if(!empty($lead_id)){
                        
                        $query="SELECT followup_no FROM ".TABLE_SALE_LEADS." WHERE lead_id= '".$data['lead_id']."'";
                        $db->query($query);
                        if($db->nf()>0){
                            $db->next_record();
                            $flwnumber=$db->f('followup_no');
                        }
                    
                        $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                            ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".title_id 	= '". $data ['title_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".remarks 		= 'Order has been created through lead'"
                                //.",". TABLE_SALE_FOLLOWUP .".date    		= '". $data['date'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".time    		= '". $data ['hrs'].':'.$data ['min'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".time_type 	= '". $data ['time_type'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".lead_id    	= '". $data ['lead_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data['client']['user_id'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".assign_to 	= '". $data ['assign_to'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $variables['hid'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".table_name    	= 'billing_orders'"
								.",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
								.",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
								.",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". FOLLOWUP::ORDER ."'"
                                .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
                        $db->query($query); 
                        
                    }else{
                        
                        $query="SELECT followup_no FROM ".TABLE_SALE_FOLLOWUP." WHERE client= '".$data['client']['user_id']."'";
                        $db->query($query);
                        if($db->nf() > 0){
                            $db->next_record();
                            $flwnumber=$db->f('followup_no');
                        }else{
                            $flwnoquery="SELECT MAX(followup_no) as followup_no FROM ".TABLE_SALE_FOLLOWUP;
                            $dbnew=new db_local();
                            $dbnew->query($flwnoquery);
                                
                            if($dbnew ->nf() > 0){
                                $dbnew->next_record();
                                $max_flwnumber=$dbnew->f('followup_no');
                                if(!isset($max_flwnumber)){
                                    $flwnumber = FOLLOWUP;
                                }else{
                                    $flwnumber = $max_flwnumber + 1;
                                }
                            }
                        } 
                        
                        
                        $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                            ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".title_id 		= '". $data ['title_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".remarks 		= 'Order has been created directly'"
                                //.",". TABLE_SALE_FOLLOWUP .".date    		= '". $data['date'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".time    		= '". $data ['hrs'].':'.$data ['min'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".time_type 		= '". $data ['time_type'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".lead_id    	= '". $data ['lead_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data['client']['user_id'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".assign_to 		= '". $data ['assign_to'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $variables['hid'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".table_name    	= 'billing_orders'"
								.",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
								.",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
								.",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". FOLLOWUP::ORDER ."'"
                                .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
                        $db->query($query); 
                    }
                    
                    
                    // Insert the Particulars.
                    if ( !$db->query($data['query_p']) ) {
                        $messages->setErrorMessage("The Particulars were not Saved.");
                    }
                    
					// Add the Record to the Work Timeline.
                    /*
					$query  = " INSERT INTO ". TABLE_WORK_TL
								." SET ". TABLE_WORK_TL .".order_id		= '". $variables['hid'] ."'"
									.",". TABLE_WORK_TL .".order_no		= '". $data['number'] ."'"
									.",". TABLE_WORK_TL .".to_id		= '". $data['team'][0] ."'"
									.",". TABLE_WORK_TL .".by_id		= '". $my['user_id'] ."'"
									.",". TABLE_WORK_TL .".comments		= '". $data['work_comments'] ."'"
									.",". TABLE_WORK_TL .".do_assign	= '". date('Y-m-d H:i:s', time()) ."'"
									.",". TABLE_WORK_TL .".status   	= '". $data['work_stage'] ."'";
					if ( !$db->query($query) ) {
						$messages->setErrorMessage("The comment was not entered into Work Timeline.");
					}
                    */
					
                    // Send the notification mails to the concerned persons.
                    
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_or_add', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_or_add_al', 'access_level'=>($data['access_level']-1) ),
                                                            // array('right'=>'nc_bl_in_add', 'access_level'=>$data['access_level']),
                                                            // array('right'=>'nc_bl_in_add_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    
                    // Organize the data to be sent in email.
                    //$data['do_d']   = date('d M, Y', $data['do_d']);
                    //$data['do_c']   = date('d M, Y', $data['do_c']);
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    
                    /*
                    // Read the Client Manager information.
                    include ( DIR_FS_INCLUDES .'/clients.inc.php');
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');
                    
                    // Send mail to the Concerned Executives with same access level and higher access levels
                    if(isset($data['mail_exec_ac'])){ 
                       
                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_OR_NEW_MANAGERS', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    // Send Email to the Client.
                    if(isset($data['mail_client'])){ 
                     
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_CLIENT', $data, $email) ) {
                       
                            $to     = '';
                          
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }    
                    // Send Email to the Client Manager / creator email .
                    if(isset($data['mail_client_mgr'])){ 
                     
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_CLIENT_MANAGER', $data, $email) ) {
                            
                            $to     = '';
                            $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                    // Send Email to Team Members.
                    if(isset($data['mail_executive'])){    
                         
                        // For Speed notification is sent in one email to all the team members.
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_TEAM', $data, $email) ) {
                          
                            $to     = '';
                            foreach ( $data['team_members'] as $key=>$executive) {
                                $to[]   = array('name' => $executive['f_name'] .' '. $executive['l_name'], 'email' => $executive['email']);
                            }
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                    */
                    // Send Email to the Creator of the Order.
                    /*
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'NOT REQUIRED, SO NOT DEFINED', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    */
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                //$_ALL_POST['number'] = getCounterNumber($db,'ORD',$data['company_id']); 
                //$_ALL_POST['exchange_rate'] = 1; 
                $data		= NULL;
            }
        }
        else {           
            
          
        }
 
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/bill-order.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/bill-order.php");
        }
 
 
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            header("Location:".DIR_WS_NC."/bill-order.php?perform=list&added=1");
            //include ( DIR_FS_NC .'/bill-order-list.php');
        }
        else {
       
           /*
            if ( !empty($po_id) ) {
                // Read the Pre Order details.
                include_once ( DIR_FS_INCLUDES .'/bill-pre-order.inc.php');
                $pre_order  = NULL;
                $fields     = 'id, '. TABLE_BILL_PO .'.access_level, '.TABLE_BILL_PO .'.attach_files, '. TABLE_BILL_PO .'.status, order_details, client_details';
                $fields     .= ', user_id, number, f_name, l_name';
                if ( PreOrder::getDetails($db, $pre_order, $fields, "WHERE id = '". $po_id ."' ") > 0 ) {
                    $pre_order = $pre_order[0];
                    
                    if ( $pre_order['status'] == ACTIVE ) {
                        if ( $access_level > $pre_order['access_level'] ) {
                            $_ALL_POST['po_id']             = $pre_order['id'];
                            $_ALL_POST['po_order_details']  = $pre_order['order_details'];
                            $_ALL_POST['po_client_details'] = $pre_order['client_details'];
                            $attach_files                   = $pre_order['attach_files'];
                            $files=array();
                            $filenames='';
                            if(!empty($attach_files)){
                                $files = explode(",",$attach_files);
                                $i=1;                           
                                foreach($files as $key1=>$val1){
                                    if(!empty($val1)){
                                                                               
                                        $filenames .= "<a href=javascript:void(0); onclick=javascript:showFileAttachment('$val1') title='View file' class='file'>file ".$i."</a><br/>" ;
                                        $i++;
                                    }
                                } 
                            }
                            $_ALL_POST['files'] = $filenames ;
                            $_ALL_POST['po_user_id']        = $pre_order['user_id'];
                            $_ALL_POST['po_number']         = $pre_order['number'];
                            $_ALL_POST['po_f_name']         = $pre_order['f_name'];
                            $_ALL_POST['po_l_name']         = $pre_order['l_name'];
                            $_ALL_POST['po_access_level']   = $pre_order['access_level'];
                            
                            $pre_order = NULL;
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                            $_ALL_POST = '';
                        }
                    }
                    else {
                        $messages->setErrorMessage("The Pre Order is not yet approved.");
                        $_ALL_POST = '';
                    }
                }
                else {
                    $messages->setErrorMessage("The Selected Pre Order was not found.");
                }
            }
            */
            
            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['po_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['po_access_level'], $al_list, $index);
                $_ALL_POST['po_access_level'] = $al_list[$index[0]]['title'];
            }
            
            $hidden[] = array('name'=> 'po_id' ,'value' => $po_id);
            $hidden[] = array('name'=> 'lead_id' ,'value' => $lead_id);
            $hidden[] = array('name'=> 'q_id' ,'value' => $q_id);
            
            //$hidden[] = array('name'=> 'tbl_name' ,'value' => $tbl_name);
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lead_id', 'value' => 'lead_id');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'lst_work_stage', 'value' => 'lst_work_stage');
			$page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
			$page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'rejectedFiles', 'value' => 'rejectedFiles');
          
            $page["var"][] = array('variable' => 'source', 'value' => 'source');
			$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-add-old.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>