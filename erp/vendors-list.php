<?php

    if ( $perm->has('nc_uv_list') || $perm->has('nc_uv_search')  ) {
        
        include_once ( DIR_FS_CLASS .'/PhoneVendor.class.php');
        include_once ( DIR_FS_CLASS .'/RegionVendor.class.php');
        
        $region         = new RegionVendor();
        $phone          = new PhoneVendor(TABLE_VENDORS);
        
        $variables['send_mail_yes'] = Vendors::SENDMAILYES;
        $variables['send_mail_no'] = Vendors::SENDMAILNO;
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
        }
        else {
            $condition_query .= ' AND (';
        }
        
        $access_level   = $my['access_level'];        
        // If the User has the Right to View Vendors of the same Access Level.
        /*
        if ( $perm->has('nc_uc_list_al') ) {
            $access_level += 1;
        }
        // If my is the Manager of this Client.
        $condition_query .= " (". TABLE_VENDORS .".manager = '". $my['user_id'] ."' "
                                ." AND ". TABLE_VENDORS .".access_level < $access_level ) ";
       
        // Check if the User has the Right to view Vendors of other Executives.
        if ( $perm->has('nc_uc_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_uc_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_VENDORS .".access_level < $access_level_o ) ";
        }
        */
         //show the list of Vendors of same access levels
        if ( $perm->has('nc_uv_list_all') ) { 
            $condition_query .= "  parent_id = '' )"; 
        }else{      
            $condition_query .= "".TABLE_VENDORS .".access_level <= $access_level ";    
            $condition_query .= ')';
            $condition_query .= " AND parent_id = '' ";            
        }
        
       
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
         //By default search in On
        $_SEARCH["searched"]    = true ;
		$list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
		
        if($searchStr==1){
			// To count total records.
			$list	= 	NULL;
			$total	=	Vendors::getList( $db, $list, 'user_id', $condition_query);
			$extra_url  = '';
			if ( isset($condition_url) && !empty($condition_url) ) {
				$extra_url  = $condition_url;
			}
			$condition_url .="&perform=".$perform; 
			$extra_url  .= "&x=$x&rpp=$rpp";
			$extra_url  = '&start=url'. $extra_url .'&end=url';
			$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
			$list	= NULL;
			Vendors::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
        }
        /*$phone  = new Phone(TABLE_VENDORS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_VENDORS, $list[$i]['user_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}*/
        
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                $client=array();
                $string = str_replace(",","','", $val['alloted_clients']);
                $fields1 =  TABLE_VENDORS .'.f_name'.','. TABLE_VENDORS .'.l_name';
                $condition1 = " WHERE ".TABLE_VENDORS .".user_id IN('".$string."') " ;
                Vendors::getList($db,$client,$fields1,$condition1);
                $clientname='';              
                foreach($client as $key1=>$val1){
                    $clientname .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
                } 
                $val['alloted_clients'] = $clientname ;
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_VENDORS, $val['user_id']);
                $val['phone'] = $phone->get($db);
                // Read the Addresses.
                $region->setAddressOf(TABLE_VENDORS, $val['user_id']);
                $val['address_list'] = $region->get();   
				$val['sublist_list']=array();
                if($searchLink && $perm->has('nc_uv_su_list')){
                    $parent_id = $val['user_id'];
                    $sublist	= 	NULL;
                    $condition_query=" WHERE parent_id='".$parent_id."'" ; 
                    Vendors::getList( $db, $sublist, 'user_id,status,f_name,l_name,mobile1,mobile2,email,email_1,email_2', $condition_query);
                    $val['sublist_list']=$sublist;
                
                }			   
                $fList[$key]=$val;
            }
        }
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_invite']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_view_sub_user'] = false;
        $variables['can_send_password'] = false;
        $variables['can_up_mail_status'] = false;
        $variables['can_edit_su'] = false;
		 if ( $perm->has('nc_v_su_edit') ) {
            $variables['can_edit_su'] = true;
        }
		
        if ( $perm->has('nc_uv_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_uv_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_uv_invite') ) {
            $variables['can_invite'] = true;
        }
        if ( $perm->has('nc_uv_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_uv_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_uv_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_uv_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_uv_su_list') ) {
            $variables['can_view_sub_user'] = true;
        }
        if ( $perm->has('nc_uv_send_pwd') ) {
            $variables['can_send_password'] = true;
        }
        if ( $perm->has('nc_uv_up_mst') ) {
            $variables['can_up_mail_status'] = true;
        }
        $variables['searchLink']=$searchLink ;
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>