<?php

    if ( $perm->has('nc_cld_or_list') || $perm->has('nc_cld_or_search') ) {
		include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
      
		// Status
		$variables["assign"]           = LeadsOrder::CRM_ASSIGN;
		$variables["positive"]            = LeadsOrder::CRM_POSITIVE;
		$variables["negative"]           = LeadsOrder::CRM_NEGATIVE; 
        
				
		
	   
		if($perm->has('nc_cld_or_all_list')){
			$condition_query = " WHERE ("
                                   ." ". TABLE_LEADS_ORDERS .".status IN( 
							   '". LeadsOrder::PENDING ."','". LeadsOrder::ACTIVE ."','". LeadsOrder::ASSIGN ."' )"
                            .") ".$condition_query;
	 
		}else{
			$condition_query = " WHERE ("
                                   ." ". TABLE_LEADS_ORDERS .".status IN( '". LeadsOrder::PENDING ."' )"
                                .") ".$condition_query;
	 
		
		}
		/*     
		if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';			 
            $condition_query = " WHERE ("
                                   ." ". TABLE_LEADS_ORDERS .".status IN( '". LeadsOrder::PENDING ."')"
                                .")";
			$_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(LeadsOrder::PENDING);
        }  
		if ( $perm->has('nc_ld_or_list_all') ) { 
		 
		 
		}else{
		
			// If the Order is created by the my.
			$access_level   = $my['access_level'];
			if ( $perm->has('nc_bl_or_list_al') ) {
				$access_level += 1;
			}
			$condition_query .= " AND ( ";
			
			// If my has created this Order.
			$condition_query .= " (". TABLE_LEADS_ORDERS .".created_by = '". $my['user_id'] ."' "
									." AND ". TABLE_LEADS_ORDERS .".access_level < $access_level ) ";
			
			// If my is present in the Team.
			$condition_query .= " OR ( ( "
											. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
										. " ) "
										." AND ". TABLE_LEADS_ORDERS .".access_level < $access_level ) ";
			
			// If my is the Client Manager
			$condition_query .= " OR ( ". TABLE_SALE_LEADS .".manager = '". $my['user_id'] ."'"
								." AND ". TABLE_LEADS_ORDERS .".access_level < $access_level ) ";
			
			// Check if the User has the Right to view Orders created by other Users.
			 
			$condition_query .= " )";      
		} */


		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
        //By default search in On
        $_SEARCH["searched"]    = true ;
        $regionlead     = new RegionLead('91');
        $phonelead      = new PhoneLead(TABLE_SALE_LEADS);
       
        if($searchStr==1){
       
		 
            $fields = TABLE_LEADS_ORDERS.'.id ' ;
            $total	= LeadsOrder::getDetails( $db, $list, '', $condition_query);
            $extra_url  = '';
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            $list	= NULL;
            $fields = TABLE_LEADS_ORDERS .'.id'
                    .','. TABLE_LEADS_ORDERS .'.number'
                    .','. TABLE_LEADS_ORDERS .'.order_title'
                    .','. TABLE_LEADS_ORDERS .'.access_level'
                    .','. TABLE_LEADS_ORDERS .'.team'
                    .','. TABLE_LEADS_ORDERS .'.details'
                    .','. TABLE_LEADS_ORDERS .'.do_d'
                    .','. TABLE_LEADS_ORDERS .'.do_c'
                    .','. TABLE_LEADS_ORDERS .'.do_o'
                    .','. TABLE_LEADS_ORDERS .'.status'
                    .','. TABLE_LEADS_ORDERS .'.crm_status'
                    .','. TABLE_LEADS_ORDERS .'.followup_status'
                    .','. TABLE_LEADS_ORDERS .'.cron_created '
                    .','. TABLE_LEADS_ORDERS .'.new_ref_ord_id'
                    .','. TABLE_LEADS_ORDERS .'.new_ref_ord_no'
                    .','. TABLE_LEADS_ORDERS .'.order_against_id'
                    .','. TABLE_LEADS_ORDERS .'.order_against_no'
                    .','. TABLE_LEADS_ORDERS .'.invoice_against_id'
                    .','. TABLE_LEADS_ORDERS .'.invoice_against_no'
                    .','. TABLE_LEADS_ORDERS .'.is_old' //For Old Migrated data 
                    .','. TABLE_LEADS_ORDERS .'.old_updated' //For Old Migrated data
                    .','. TABLE_LEADS_ORDERS .'.old_particulars'
                    //.','. TABLE_LEADS_QUOTATION .'.number as inv_no'
                    .','. TABLE_SALE_LEADS .'.lead_id AS c_user_id'
                    .','. TABLE_SALE_LEADS .'.number AS c_number'
                    .','. TABLE_SALE_LEADS .'.f_name AS c_f_name'
                    .','. TABLE_SALE_LEADS .'.l_name AS c_l_name'
                    .','. TABLE_SALE_LEADS .'.billing_name'
					.','. TABLE_SALE_LEADS .'.email AS c_email'
                    .','. TABLE_SALE_LEADS .'.email_1 AS email_1'
                    .','. TABLE_SALE_LEADS .'.email_2 AS email_2'
                    .','. TABLE_SALE_LEADS .'.mobile1 AS mobile1'
                    .','. TABLE_SALE_LEADS .'.mobile2 AS mobile2'
                    .','. TABLE_SALE_LEADS .'.mobile3 AS mobile3'       
                    .','. TABLE_SALE_LEADS .'.status AS c_status';
             LeadsOrder::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }	
        
        
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               
               $is_invoice_created = 0;			   
			   $sql= " SELECT COUNT(*) as count FROM ".TABLE_LEADS_QUOTATION." WHERE 
			   ".TABLE_LEADS_QUOTATION.".or_no='".$val['number']."' AND 
			   ".TABLE_LEADS_QUOTATION.".status !='".LeadsQuotation::DELETED."' LIMIT 0,1";
			   $db->query($sql);               
			   if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$count = $db->f('count') ;
						if( $count > 0){
							$is_invoice_created = 1;
						}
					}
				}
                
				//check order id followup in ST BOF
				$val['ticket_id']=0;
				/* $sql2 = "SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
				(".TABLE_LD_TICKETS.".flw_ord_id = '".$val['id']."') LIMIT 0,1"; */
				
				$val['ticket_id']=0;
				$sql2 = "SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
				(".TABLE_LD_TICKETS.".flw_ord_id = '".$val['id']."' AND ".TABLE_LD_TICKETS.".ticket_child=0)
				 ORDER BY ticket_id ASC LIMIT 0,1";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                        }
                    }                   
                }
				$phonelead->setPhoneOf(TABLE_SALE_LEADS, $val['c_user_id']);
				$val['phone'] = $phonelead->get($db);
				
				// Read the Addresses.
				$regionlead->setAddressOf(TABLE_SALE_LEADS, $val['c_user_id']);
				$val['address_list'] = $regionlead->get();
			
				//check order id followup in ST EOF
				/* 
				if(!empty($val['team'])){
				
					$val['team'] = trim($val['team'],",") ;
					$val['team'] = explode(',', $val['team']);
					$temp       = "'". implode("','", $val['team'])."'";
					$team_members  = '';
					$team_members  = array();
					User::getList($db, $team_members, 'user_id,number,f_name,l_name', 
					" WHERE user_id IN (". $temp .")");
					$val['team'] = array();
					foreach ( $team_members as $key1=>$members) {
						$val['team'][] = $members['user_id'];
						$val['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
					}
				}   
				*/
                $is_particulars_selected = 0; 
                $sql= " SELECT id FROM ".TABLE_LEADS_ORD_P." WHERE 
				".TABLE_LEADS_ORD_P.".ord_no='".$val['number']."' LIMIT 0,1";
                $db->query($sql);               
                if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$count = $db->f('id') ;
						if( $count > 0){
							$is_particulars_selected = 1;
						}
					}
                }                
                $val['is_invoice_created'] = $is_invoice_created;   
                $val['is_particulars_selected'] = $is_particulars_selected;                   
                $flist[$key]=$val;
            }
        }
        
        //print_R($flist);
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_create_invoice']= false;
        $variables['can_followup'] = false;
        $variables['can_bifurcate'] = false;
		$variables['can_view_inv'] = false;
        
        if ( $perm->has('nc_cld_or_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_cld_or_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_cld_or_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_cld_or_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_cld_or_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_cld_or_status') ) {
            $variables['can_change_status'] = true;
        }
        		
		if ( $perm->has('nc_ldt') && $perm->has('nc_ldt_flw_ord') ) {
            $variables['can_followup'] = true;
        }
		if( $perm->has('nc_ld_quot') && $perm->has('nc_ld_quot_list') ) {
            $variables['can_view_inv']     = true;
        }
		
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'crm-leads-order-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>