<?php
    if ( $perm->has('nc_bk_chq_csv') ){ //
        $_ALL_POST  = NULL;
        $data       = NULL;
        $messages_ask 	= new Messager;	// create the object for the Message class.
		
		
        $CSV = BankCheque::getRestrictions();
        
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);          
			$files       = processUserData($_FILES);
    		$ask_added      = 0;
			$ask_duplicate  = 0;
			$ask_empty      = 0;
			print_r($files);
            $extra = array( 'file'      => &$files,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );
			 
			 
            if ( BankCheque::validateUpload($data, $extra) ) {
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $invalid  = 0;
                $empty      = 0;
                $index      = array();
                $handle     = fopen($_FILES['file_csv']['tmp_name'],"rb");
                $message = '';
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
					
					
                    if ( $row == 1 ) {
                        $num = count($data);
                        for ($i=0; $i < $num; $i++) {
                            $case = trim($data[$i]);
                            switch ($case) {
                                case ('bank_id'): {
                                    $index['bank_id'] = $i;
                                    break;
                                }                        
								case ('cheque_no'): {
                                    $index['cheque_no'] = $i;
                                    break;
                                }									
								case ('cheque_book_no'): {
                                    $index['cheque_book_no'] = $i;
                                    break;
                                }	
								
                            }
                        }
                    }
                    else {                 
                          $index= array (
							"bank_id"=>0,
							"cheque_no"=>1,
							"cheque_book_no"=>2
                           );
						   
						    $dt=$data[$index['tax_paid_dt']];
						    $dtArr = explode('/', $dt);
						    $tax_paid_dt = $dtArr[2]."-".$dtArr[1]."-".$dtArr[0]." 00:00:00";
							 
							
							
							if( $data[$index['bank_id']] != '' && $data[$index['cheque_no']] != '' ) {
								 
							    $table = TABLE_BANK_CHEQUE;
							    $cond =" WHERE ".TABLE_BANK_CHEQUE.".cheque_no = '".$data[$index['cheque_no']]."'
									AND ".TABLE_BANK_CHEQUE.".bank_id = '".$data[$index['bank_id']]."'";
									
                                if ( !BankCheque::duplicateFieldValue($db, $table, 'id', $cond) ) {
									$company_id = 0;
									$banknameArr=array();
									$table = TABLE_PAYMENT_BANK;
									$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= 
									'".$data[$index['bank_id']]."' " ;
									$fields1 =  TABLE_PAYMENT_BANK .'.company_id' ;
									$banknameArr = getRecord($table,$fields1,$condition2);
									if(!empty($banknameArr)){
										$company_id =  $banknameArr['company_id'] ;                    
									}
									
									
									$sqlu= " INSERT INTO ".TABLE_BANK_CHEQUE
									." SET cheque_no = '".$data[$index['cheque_no']]."',"
									." bank_id = '".$data[$index['bank_id']]."'," 
									." company_id = '".$company_id."'," 
									." cheque_book_no = '". $data[$index['cheque_book_no']]."'," 
									." do_e = '". date('Y-m-d h:i:s')."'," 
									." added_by = '". $my['user_id']."'," 
									." added_by_name = '". $my['f_name']." ".$my['l_name']."' " 	 ;		
								
									$db->query($sqlu);
									$added += 1;
									$messages->setOkMessage('Added Cheque No. -'.$data[$index['cheque_no']]);
								}else{
									$messages->setOkMessage('Duplicate Cheque no. for selected bank -'.$data[$index['cheque_no']]);
								}
								
							}else{
								$empty += 1;
								$messages->setErrorMessage('Cheque No. -'.$data[$index['cheque_no']]." not found.");
							}							
							 
                      
                        
                        // The Row is not the First row.
                        
                    }                    
                }
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been added.');
                if ($notfound)
                    $messages->setOkMessage($notfound .' out of '. $row .' records were not found and neglected.');
                
				if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
        }
        
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$condition ='';
            include ( DIR_FS_NC .'/bank-cheque-list.php');
        }else {
			
			 
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'csv_upload');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bank-cheque-csv.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>