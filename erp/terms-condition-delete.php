<?php
	if ( $perm->has('nc_tc_delete') ) {
		$id	= isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'messages' 		=> $messages
					);
		TermsCondition::delete($id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/terms-condition-list.php');
	}
	else {
		$messages->setErrorMessage("You do not have the Right to Access this module.");
	}
?>