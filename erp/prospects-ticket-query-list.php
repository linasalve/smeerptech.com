<?php

    if ( $perm->has('nc_pst_list') ) {
        
        include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
        
		
			
        
		
        /*  
		$condition_query = " LEFT JOIN ".TABLE_PROSPECTS." ON ".TABLE_PROSPECTS.".user_id = ".TABLE_PROSPECTS_TICKETS.".ticket_owner_uid 
                WHERE ".TABLE_PROSPECTS_TICKETS.".ticket_child='0' ".$condition_query    ; 
		*/
		$condition_query = " WHERE ".TABLE_PROSPECTS_TICKETS.".ticket_child='0' AND 
		 ".TABLE_PROSPECTS_TICKETS.".ticket_type='".ProspectsTicket::TYP_QUERY."' ".$condition_query    ;  
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
          //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        
        $total	=	ProspectsTicket::getList( $db, $list, '', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
       
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $list	= NULL;
        //$fields = TABLE_PROSPECTS_TICKETS.".*,".TABLE_ST_STATUS.".status_name,";
        $fields = TABLE_PROSPECTS_TICKETS.".*"; 
       /*  $fields .= TABLE_PROSPECTS.".f_name,".TABLE_PROSPECTS.".l_name,".TABLE_PROSPECTS.".billing_name,".TABLE_PROSPECTS.".org,".TABLE_PROSPECTS.".email,".TABLE_PROSPECTS.".mobile1,".TABLE_PROSPECTS.".mobile2"; */
        ProspectsTicket::getList( $db, $list,$fields, $condition_query, $next_record, $rpp);
        
        $ticketStatusArr = ProspectsTicket::getTicketQStatus();
        $ticketStatusArr = array_flip($ticketStatusArr);
        //$ticketRatingArr = ProspectsTicket::getRating();
        //$ticketRatingArr = array_flip($ticketRatingArr);
      
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                //$val['ticket_department']    = $val['department_name'] ;     
                //$val['ticket_status']    = $val['status_name'] ;     
                //$val['ticket_priority']    = $val['priority_name'] ;   
                if(isset($val['ticket_status'])){
                    $val['ticket_status_name'] = $ticketStatusArr[$val['ticket_status']];  
                }
                if(!empty($val['ticket_rating'])){
                    $val['ticket_rating_name'] = $ticketRatingArr[$val['ticket_rating']];  
                }
                $val['ticket_date'] = date("d M Y H:i:s",$val['ticket_date']);
                $executive=array();
                $string = str_replace(",","','", $val['assign_members']);
                $fields4 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                $condition4 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                User::getList($db,$executive,$fields4,$condition4);
                $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['assign_members'] = $executivename ; 
                 
                $db2 = new db_local ;
                
                $query = "SELECT COUNT(*) AS replies FROM ". TABLE_PROSPECTS_TICKETS
                                    ." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_child ='". $val['ticket_id'] ."' " ; 
                $db2->query($query) ;
                $db2->next_record() ;
                $ticket_replies = $db2->f("replies") ;
                $val['ticket_replied']    = $ticket_replies ;                
                $fList[$key]=$val;
            }
        }
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_view_details']  = false;
        $variables['can_view_client_details']  = true;
        $variables['can_update_status'] = false;
		$variables['can_email_read'] = false;
        
        if ( $perm->has('nc_pst_add') ) {
            $variables['can_add'] = true;
        }
        
        if ( $perm->has('nc_pst_details') ) {
            $variables['can_view_details'] = true;
        }
         
        if ( $perm->has('nc_pst_list') ) {
            $variables['can_view_list'] = true;
        }
        
        if ( $perm->has('nc_pst_update_status') ) {
            $variables['can_update_status'] = true;
        }
        if( $perm->has('nc_pst_email_read') ){
            $variables['can_email_read'] = true;
        }
		/*
		$mailBox = imap_open("{imap.emailsrvr.com:143/novalidate-cert}INBOX", "sales@smeerptech.com", "LDFJ#@$$%3487dwld")
		or die("can't connect: " . imap_last_error());
		$check = imap_mailboxmsginfo($mailBox);
		$totalNumOfMsg = $check->Nmsgs;
		 $variables['totalNumOfMsg'] = $check->Nmsgs;
		 */
		 
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-ticket-query-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>