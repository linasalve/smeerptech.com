<?php
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	 
	
	$id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');
	
	 
        

	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
   // $db_new 		= new db_local; // database handle
	
if ( $perm->has('nc_st_tc_delete') ) {

	if (isset($id) && $id > 0 ){
	
		$extra = array( 'db'           => &$db ,
						'messages'     => &$messages		
                        );
		$execute= StTemplateCategory::delete($id, $extra);		 
		if($execute){
			$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
			<tr>
				<td class=\"message_header_ok\">
					<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
					Success
				</td>
			</tr>
			<tr>
				<td class=\"message_ok\" align=\"left\" valign=\"top\">
					<ul class=\"message_ok_2\">
						<li class=\"message_ok\">Record deleted Successfully </li>
					</ul>
				</td>
			</tr>
			</table>";
			
			$message .= "|1";
								
		}else{
			$message = " <table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
				<tr>
					<td class=\"message_error_header\" height=\"10px\">
						<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\"  border=\"0\">&nbsp;
						Not Success
					</td>
				</tr>
				<tr>
					<td class=\"message_error\" align=\"left\" valign=\"top\">
						<ul class=\"message_error\">				
							<li class=\"message_error\">Record not deleted Successfully</li>				
						</ul>
					</td>
				</tr>
			</table>";
			$message .= "|0";
		}
		
	}
}else{

	$message = " <table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
				<tr>
					<td class=\"message_error_header\" height=\"10px\">
						<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\"  border=\"0\">&nbsp;
						Not Success
					</td>
				</tr>
				<tr>
					<td class=\"message_error\" align=\"left\" valign=\"top\">
						<ul class=\"message_error\">				
							<li class=\"message_error\">You donot have the Permisson to Access this module.</li>				
						</ul>
					</td>
				</tr>
			</table>";
			$message .= "|0";

}
	echo $message ;

    exit;
?>