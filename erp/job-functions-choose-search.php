<?php

	$condition_query= '';
    $condition_url  = '';
    $where_added = false;
    $searchStr = 1;
    if ( $sString != "" ) {
	    $searchStr = 1;
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
		if ( $search_table != "Any" && $search_table != NULL) {
            if($where_added){
                 $condition_query .= " AND  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            }else{
                $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
                $where_added = true;
            }
           
			$condition_url .= "&sString=". $sString;
			$condition_url .= "&sType=". $sType;
			
		}
		elseif ( $search_table == "Any" ) {
           
            if($where_added){
               
               $condition_query .= " AND (";
               
            }else{                
                $condition_query .= " WHERE (";
            }
            
			foreach( $sTypeArray as $table => $field_arr ) {
				if ( $table != "Any" ) {
					foreach( $field_arr as $key => $field ) {
                        $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
						$where_added = true;
					}
				}
			}
			$condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
			$condition_query .= ") ";
		}
		$condition_url .= "&sString=". $sString;
		$condition_url .= "&sType=". $sType;
			
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}
    
     // BO: Status data.
    $sStatusStr = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR')  && isset($sStatus) && $sStatus!=''){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
        }        
        
        $condition_query .= " ". TABLE_TJ_FUNCTIONS .".status IN ('". $sStatusStr ."') ";        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
        
    }
    // Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
    
    
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR' ) && !empty($date_from) ){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_TJ_FUNCTIONS .".".$dt_field." >= '". $dfa ."'";
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;      
    }  
    // EO: From Date
    
    // BO: Upto Date
	
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR')  && !empty($date_to)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_TJ_FUNCTIONS .".".$dt_field." <= '". $dta ."'";
        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }
    
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
	$_SEARCH["sString"] 	= $sString ;
	$_SEARCH["sType"] 		= $sType ;
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/job-functions-choose-list.php');
?>