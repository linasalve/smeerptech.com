<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
	
    $sString = isset($_GET["sString"]) ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType  = isset($_GET["sType"]) ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder	= isset($_GET["sOrder"])? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"])? $_POST["sOrderBy"]: '' );
	$perform= isset($_GET["perform"])? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x = isset($_GET["x"]) ? $_GET["x"] : ( isset($_POST["x"]) ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"]) ? $_GET["rpp"]  : ( isset($_POST["rpp"]) ? $_POST["rpp"] : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $condition_url='';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if ( $perm->has('nc_ue') ) {

        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_USER   =>  array(     'ID'            => 'user_id',
                                                            'Number'        => 'number',
                                                            'User Name'     => 'username',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Middle Name'   => 'm_name',
                                                            'Last Name'     => 'l_name',
                                                            'Pet Name'      => 'p_name',
                                                            'Designation'   => 'desig',
                                                            'Organization'  => 'org',
                                                            'Domain'        => 'domain'
                                                        )
                            );
        
        $sOrderByArray  = array(
                                TABLE_USER => array('Number'        => 'number',
                                                    'User Name'     => 'username',
                                                    'E-mail'        => 'email',
                                                    'First Name'    => 'f_name',
                                                    'Last Name'     => 'l_name',
                                                    'Date of Birth' => 'do_birth',
                                                    'Date of Regis.'=> 'do_reg',
                                                    'Date of Login' => 'do_login',
                                                    'Status'        => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_reg';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_USER;
        }
        $variables['status'] = User::getStatus();
        $variables['title_type'] = User::getTitleType();
        //use switch case here to perform action. 
        switch ($perform) {
            case ('add_user'):
            case ('add'): {
            
                include (DIR_FS_NC.'/user-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_user'):
            case ('edit'): {
                include (DIR_FS_NC .'/user-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_user'):
            case ('view'): {
                include (DIR_FS_NC .'/user-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('download_file'): {
				include (DIR_FS_NC .'/user-download.php');
				break;
			} 
			case ('test-upload'): {
                include (DIR_FS_NC .'/user-test-upload.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('change_status'): {
                 $searchStr=1;
                include ( DIR_FS_NC .'/user-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/user-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('delete_user'):
            case ('delete'): {
                include ( DIR_FS_NC .'/user-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list_user'):{
                 $searchStr = 1;
                //include (DIR_FS_NC .'/user-list.php');
                include (DIR_FS_NC .'/user-select-search.php');
                // CONTENT = CONTENT
               
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-select.html');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user-select-search.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
                break;
            }
            case ('list'):
            default: {
				
                $searchStr = 1;
                include (DIR_FS_NC .'/user-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	echo 'flush 2: ' . number_format(memory_get_usage(), 0, '.', ',') . " bytes\n";
	include_once( DIR_FS_NC ."/flush.php");
	
?>