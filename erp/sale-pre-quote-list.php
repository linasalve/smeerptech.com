<?php

    if ( $perm->has('nc_sl_pq_list') ) {
		// Status
		$variables["rejected"]          = PreQuote::REJECTED;
		$variables["active"]            = PreQuote::ACTIVE;
		$variables["approved"]          = PreQuote::APPROVED;
		//$variables["deleted"]           = PreQuote::DELETED;
		//$variables["completed"]         = PreQuote::COMPLETED;
		//$variables["processed"]         = PreQuote::PROCESSED;
	
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';
            $time   = time();
            $l_mon  = strtotime('+3 month', $time);
            $from   = date('Y-m-d 00:00:00', $time );
            $to     = date('Y-m-d 23:59:59', $l_mon);
            
            $condition_query = " WHERE ". TABLE_SALE_PRE_QUOTE .".do_e <= '". $to ."' "
                                ." AND ("
                                    . TABLE_SALE_PRE_QUOTE .".status = '". PreQuote::REJECTED ."'"
                                    ." OR ". TABLE_SALE_PRE_QUOTE .".status = '". PreQuote::ACTIVE ."'"
                                    ." OR ". TABLE_SALE_PRE_QUOTE .".status = '". PreQuote::APPROVED ."'"
                                .")";
            
            $_SEARCH["date_from"]   = date('d/m/Y', $time);
            $_SEARCH["chk_date_to"] = 'AND';
            $_SEARCH["date_to"]     = date('d/m/Y', $l_mon);
            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(PreQuote::REJECTED, PreQuote::ACTIVE,PreQuote::APPROVED);
        }
        // If the PreQuote is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_sl_pq_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " AND ( ";

        // If my has created this PreQuote.
        $condition_query .= " (". TABLE_SALE_PRE_QUOTE .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_SALE_PRE_QUOTE .".access_level < $access_level ) ";
        
        // If my is present in the Team.
        /*$condition_query .= " OR ( ( "
                                        . TABLE_SALE_PRE_QUOTE .".team REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_SALE_PRE_QUOTE .".team REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_SALE_PRE_QUOTE .".team REGEXP ',". $my['user_id'] .",' OR "
                                        . TABLE_SALE_PRE_QUOTE .".team REGEXP '^". $my['user_id'] .",' "
                                    . " ) "
                                    ." AND ". TABLE_SALE_PRE_QUOTE .".access_level < $access_level ) ";
        
        // If my is the Client Manager
        $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                            ." AND ". TABLE_SALE_PRE_QUOTE .".access_level < $access_level ) ";
        */
        // Check if the User has the Right to view Orders created by other Users.
        if ( $perm->has('nc_sl_pq_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_Sl_pq_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_SALE_PRE_QUOTE. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_SALE_PRE_QUOTE .".access_level < $access_level_o ) ";
        }
        $condition_query .= " )";
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
       
      
        // To count total records.
        $list	= 	NULL;
        $fields = TABLE_SALE_PRE_QUOTE.'.id ' ;
        $total	=	PreQuote::getDetails( $db, $list, '', $condition_query);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
       
       
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields = TABLE_SALE_PRE_QUOTE .'.id'
                    .','. TABLE_SALE_PRE_QUOTE .'.number'
					.','. TABLE_SALE_PRE_QUOTE .'.subject'
                    .','. TABLE_SALE_PRE_QUOTE .'.access_level'
                    .','. TABLE_SALE_PRE_QUOTE .'.do_e'
                    .','. TABLE_SALE_PRE_QUOTE .'.status'
                    /*.','. TABLE_BILL_INV .'.number as inv_no'
                    .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                    .','. TABLE_CLIENTS .'.number AS c_number'
                    .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                    .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                    .','. TABLE_CLIENTS .'.org '
                    .','. TABLE_CLIENTS .'.email AS c_email'
                    .','. TABLE_CLIENTS .'.status AS c_status'*/;
        PreQuote::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        //$variables['can_create_invoice']= false;
        
        if ( $perm->has('nc_sl_pq_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_sl_pq_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_sl_pq_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_sl_pq_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_sl_pq_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_sl_pq_status') ) {
            $variables['can_change_status'] = true;
        }
        /*if ( $perm->has('nc_sl_pq_add') ) {
            $variables['can_create_invoice'] = true;
        }*/
       
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-pre-quote-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>