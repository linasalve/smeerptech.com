<?php
	if ( $perm->has('nc_sl_pq_status') ) {
        $pq_id	= isset($_GET["pq_id"])  ? $_GET["pq_id"] 	: ( isset($_POST["pq_id"]) 	? $_POST["pq_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        if ( $perm->has('nc_sl_pq_status_al') ) {
            $access_level += 1;
        }
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                    );
        PreQuote::updateStatus($pq_id, $status, $extra);
        
        // Display the  list.
        //include ( DIR_FS_NC .'/bill-order-list.php');
        
        $perform = 'search';
        // Display the  list.
        include ( DIR_FS_NC .'/sale-pre-quote-search.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>