<?php
if ( $perm->has('nc_pu_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    include_once (DIR_FS_INCLUDES .'/user.inc.php');
	
    $lst_hrs = $lst_min = $lst_typ= NULL;
	$lst_hrs[0] = 'Hr';
	for($i=1; $i<=20;$i++){
		if(strlen($i)==1) $i='0'.$i ;
		$lst_hrs[$i] = $i;
	}
	$lst_min[0] = 'Min';
	for($j=1; $j<=59 ;$j++){
		if(strlen($j)==1) $j='0'.$j ;
		$lst_min[$j] = $j;
	} 
	include_once (DIR_FS_INCLUDES .'/project-module.inc.php');
	$moduleArr	= NULL;
	/*  
		As we changed the logic behind order based modules to Global Modules
		We are fetching the new modules ie module id > 871; Till 871 all modules are Order based thats why we are 
		implementing this logic
	*/
	//$condition_querym = " WHERE parent_id='0' AND status ='".ProjectTask::ACTIVE."' AND order_id='".$or_id."'";
	$condition_querym = " WHERE parent_id='0' AND ".TABLE_PROJECT_TASK_MODULE.".id > 871 
	AND ".TABLE_PROJECT_TASK_MODULE.".status = '".ProjectTask::ACTIVE."' ";
	ProjectModule::getList( $db, $moduleArr, 'id, title', $condition_querym);	 
    
	if ( isset($_POST['btnCreate']) ){
        $_ALL_POST 	= $_POST;
        $data		= processUserData($_ALL_POST);
        $files		= processUserData($_FILES); 
		$extra = array( 'db' 				=> &$db,
						'files'          => &$files,
						'messages'          => &$messages
					);
		$data['allowed_file_types'] = $allowed_file_types ; 
		$data['max_file_size']= (MAX_FILE_SIZE_1MB * 10) ; 
		/*   if (empty($data["comment"]) && empty($data["internal_comment"])){
            $messages->setErrorMessage("Please enter Comment or Internal Comment");
        }
		if(empty($data['hrs']) && empty($data['min'])){
			$messages->setErrorMessage("Please select timesheet");
		} */ 
		
		if ( ProjectTask::validateCommentDailyAdd($data, $extra) ) {  
			
			$mail_client=0;
			$is_visibleto_client = 0;     
			if(isset($_POST['mail_client']) ){
				$mail_client=1;
				$is_visibleto_client = 1;
			}
			$mail_staff=0;
			if(isset($_POST['mail_staff']) ){
				$mail_staff=1;
			}
			$mark_imp=0;
			if(isset($_POST['mark_imp']) ){
				$mark_imp=1;
			}
			$mail_ceo=0;
			if(isset($_POST['mail_ceo'])){
				$mail_ceo = 1;
			}
			$send_offers=0;
			if(isset($_POST['send_offers'])){
				$send_offers = 1;
			}
			$i=1;
			$attachfilename='';
			if(!empty($files["attached_file"]["name"])){
				//$val["name"] = str_replace(' ','-',$val["name"]);
				$filedata["extension"]='';
				$filedata = pathinfo($files["attached_file"]["name"]); 
				$ext = $filedata["extension"] ; 
				//$attachfilename = $username."-".mktime().".".$ext ;
				$attachfilename = mktime()."-".$i.".".$ext ;
			   if(!empty($files["attached_file"]["name"])){                 
					if (move_uploaded_file($files["attached_file"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename, 0777);
						$attached_type = $files["attached_file"]["type"] ;
					}
				}
				$i++;
			}
			if(!empty($files["attached_file1"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file1"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename1 = mktime()."-file1".".".$ext ;
			    if(!empty($files["attached_file1"]["name"])){                 
					if (move_uploaded_file($files["attached_file1"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename1)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename1, 0777); 
					}
				} 
			}
			if(!empty($files["attached_file2"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file2"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename2 = mktime()."-file2".".".$ext ;
			    if(!empty($files["attached_file2"]["name"])){                 
					if (move_uploaded_file($files["attached_file2"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename2)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename2, 0777); 
					}
				} 
			}
			if(!empty($files["attached_file3"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file3"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename3 = mktime()."-file3".".".$ext ;
			    if(!empty($files["attached_file3"]["name"])){                 
					if (move_uploaded_file($files["attached_file3"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename3)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename3, 0777); 
					}
				} 
			}
			if(!empty($files["attached_file4"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file4"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename4 = mktime()."-file4".".".$ext ;
			    if(!empty($files["attached_file4"]["name"])){                 
					if (move_uploaded_file($files["attached_file4"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename4)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename4, 0777); 
					}
				} 
			}
			if(!empty($files["attached_file5"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file5"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename5 = mktime()."-file5".".".$ext ;
			    if(!empty($files["attached_file5"]["name"])){                 
					if (move_uploaded_file($files["attached_file5"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename5)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename5, 0777); 
					}
				} 
			}
			if(!empty($files["attached_file6"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file6"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename6 = mktime()."-file6".".".$ext ;
			    if(!empty($files["attached_file6"]["name"])){                 
					if (move_uploaded_file($files["attached_file6"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename6)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename6, 0777); 
					}
				} 
			}
			if(!empty($files["attached_file7"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file7"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename7 = mktime()."-file7".".".$ext ;
			    if(!empty($files["attached_file7"]["name"])){                 
					if (move_uploaded_file($files["attached_file7"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename7)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename7, 0777); 
					}
				} 
			}
			if(!empty($files["attached_file8"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file8"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename8 = mktime()."-file8".".".$ext ;
			    if(!empty($files["attached_file8"]["name"])){                 
					if (move_uploaded_file($files["attached_file8"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename8)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename8, 0777); 
					}
				} 
			}
			if(!empty($files["attached_file9"]["name"])){
				$filedata["extension"] = '';
				$filedata = pathinfo($files["attached_file9"]["name"]); 
				$ext = $filedata["extension"] ; 
				$attachfilename9 = mktime()."-file9".".".$ext ;
			    if(!empty($files["attached_file9"]["name"])){                 
					if (move_uploaded_file($files["attached_file9"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename9)){
						@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename9, 0777); 
					}
				} 
			}
			
			if($data['dear']==1){
				$data['details']="Dear Sir, <br/><br/>".$data['details'] ;  
			}elseif($data['dear']==2){
				$data['details']="Dear Madam, <br/><br/>".$data['details'] ; 
			}elseif($data['dear']==3){
				$data['details']="Dear Sir / Madam, <br/><br/>".$data['details'] ;
			}elseif($data['dear']==4){
				$data['details']= $data['dear_text']."<br/><br/>".$data['details'] ;
			}
			
            if(!empty($data['or_id'])){
                $order_list = NULL;
                $required_fields =TABLE_BILL_ORDERS.'.client,
				'.TABLE_BILL_ORDERS.'.order_title_mail,
				'.TABLE_BILL_ORDERS.'.order_title,
				'.TABLE_BILL_ORDERS.'.wp_support_team,
				'.TABLE_BILL_ORDERS.'.clients_su';
                $condition_c =	" WHERE id='".$data['or_id']."'";
                Order::getList($db,$order_list,$required_fields,$condition_c);
                $order = $order_list[0];
				$client_id = $order['client']; 
				$support_team='';
				if(!empty($order['wp_support_team'])){
					$wp_support_team = trim($order['wp_support_team'],",") ;
					$wp_support_team_arr = explode(',', $wp_support_team) ;
					$no = array_rand($wp_support_team_arr, 1);
					$support_team = $wp_support_team_arr[$no];
				}
				if(!empty($_POST['executive_id'])){
					$support_team = $_POST['executive_id'];
				}
				if(isset($_POST['send_via_ceo'])){
					$support_team =  CEO_USER_ID;
				}
				
				$data['project_title'] = !empty($order['order_title_mail']) ? $order['order_title_mail'] : $order['order_title'];
				$randomUser = getRandomAssociate($support_team);
				 
				$data['display_name'] = $randomUser['name'];
				$data['display_user_id'] = $randomUser['user_id'];
				$data['display_designation'] = $randomUser['desig'];
				$clients_su = $order['clients_su'] ;
				$clients_su_str='';
				$subUserDetails=null;
				if(!empty($clients_su) && !empty($data["details"]) && $mail_client=='1' ){
					$clients_su_str =  str_replace(",","','",$clients_su);
					if(!empty($clients_su_str)){
						Clients::getList($db, $subUserDetails, 'number, f_name, l_name, email, email_1,email_2,title', " WHERE status='".Clients::ACTIVE."' AND user_id IN('".$clients_su_str."') ");                   
					} 
				} 	
            }   
			
			$query = "SELECT ".TABLE_PROJECT_TASK_DETAILS.".id as project_task_id FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE 
				".TABLE_PROJECT_TASK_DETAILS.".project_id='".$data['or_id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".task_id='0' AND 
				".TABLE_PROJECT_TASK_DETAILS.".is_flw='1' LIMIT 0,1";
			if ( $db->query($query) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$project_task_id =$db->f("project_task_id") ;					
					}
				}else{ 
					$data['hrs_d'] = 0; //default hrs set to add main task
					$data['min_d'] = 5;
					$current_salary = $my['current_salary'];
					$per_hour_salary = $my['current_salary'] / STD_WORKING_HRS;
					$per_hour_salary =  number_format($per_hour_salary,2,'.', '');
					$worked_min = ( $data['hrs_d']*60 ) + $data['min_d'] ;
					$worked_hour = ($worked_min/60) ;
					$worked_hour =  number_format($worked_hour,2,'.', '');
					$worked_hour_salary = ( $per_hour_salary * $worked_hour);
					$hrs1 = (int) ($data['hrs_d'] * HRS_FACTOR);
					$min1 = (int) ($data['min_d'] * HRS_FACTOR);	
					
					$query = "INSERT INTO ".TABLE_PROJECT_TASK_DETAILS
							  ." SET "
							  ." project_id 	= '".$data['or_id']."',"
							  ." module_id	    = '".FOLLOWUP_MODULE_ID."',"   
							  //." sub_module_id	= '".$data['sub_module_id']."',"
							  ." title      	= '".FOLLOWUP_TASK_TITLE ."',"	
							 // ." details      	= '".$data['details']."',"	
							  ." status_id     	= '',"	
							  //." priority_id   	= '".$data['priority_id']."',"
							  //." resolution_id  = '".$data['resolution_id']."',"
							  //." type_id        ='".$data['type_id']."',"
							  ." hrs            ='".$data['hrs_d']."',"
							  ." min  =         '".$data['min_d']."',"
							  ." current_salary  = '".$current_salary."',"
							  ." per_hour_salary  = '".$per_hour_salary."',"
							  ." worked_hour_salary  = '".$worked_hour_salary."',"
                              ." hrs1  =         '".$hrs1."',"
							  ." min1  =         '".$min1."',"
							  ." last_updated 	= '".date('Y-m-d h:i:s')."',"
							  ." added_by      	= '".$my['user_id']."',"
							  ." added_by_name 	= '".$my['f_name']." ".$my['l_name']."',"
							  ." is_bug       	    = '0'," 
							  ." is_flw       	    = '1'," 
							  ." is_visibleto_client = '1'," 
                              ." resolved_date  = '".date('Y-m-d h:i:s')."',"
							  ." ip  	= '".$_SERVER['REMOTE_ADDR']."',"
							  ." do_e 	= '".date('Y-m-d H:i:s')."'" ;
					$db->query($query);		  
					$project_task_id = $db->last_inserted_id();   
				}
			}
			
			$data['title'] = FOLLOWUP_TASK_TITLE;
			$data['cdetails'] = $data['details']; 
			$bworked_min=0;
			if(!empty($data['behalf_executive'])){
				$table = TABLE_USER;
				$condition2 = " WHERE ".TABLE_USER .".user_id = '".$data['behalf_executive'] ."' LIMIT 0,1 " ;
				$fields1 =  TABLE_USER .'.current_salary';
				$exeCreted = getRecord($table,$fields1,$condition2);                
				$behalfCurrentSalary = $exeCreted['current_salary'] ;				
				$hrs1_b = (int) ($data['hrs'] * HRS_FACTOR);
				$min1_b = (int) ($data['min'] * HRS_FACTOR);	 
				$per_hour_salary = $behalfCurrentSalary / STD_WORKING_HRS;
				$per_hour_salary =  number_format($per_hour_salary,2,'.', '');
				$worked_min = $bworked_min = ( $data['hrs']*60 ) + $data['min'] ;
				$worked_hour = ($worked_min/60) ;
				$worked_hour =  number_format($worked_hour,2,'.', '');
				$worked_hour_salary = ( $per_hour_salary * $worked_hour);	
				$query = "INSERT INTO ".TABLE_PROJECT_TASK_DETAILS
					  ." SET "
					  ." details      	= '".$data['details']."',"	
					  ." hrs             = '".$data['hrs']."',"
					  ." min            = '".$data['min']."',"
					  ." hrs1            = '".$hrs1_b."',"
					  ." min1           = '".$min1_b."',"
					  ." current_salary  = '".$behalfCurrentSalary."',"
					  ." per_hour_salary  = '".$per_hour_salary."',"
					  ." worked_hour_salary  = '".$worked_hour_salary."',"
					  ." last_updated 		= '".$last_updated."',"
					  ." added_by      		= '".$data['behalf_executive']."',"
					  ." added_by_name     	= '".$exeCreted['f_name']." ".$exeCreted['l_name']."',"
					  ." behalf_added_by      	= '".$my['user_id']."',"
					  ." behalf_added_by_name   = '".$my['f_name']." ".$my['l_name']."',"
					  ." parent_id      = '".$project_task_id."',"
					  ." project_id      = '".$data['or_id']."',"
					  ." module_id      = '".$data['module_id']."',"
					  ." is_bug       	= '0',"
					 //." file_type      = '".$data['file_type']."',"
					 //." file_counter   = '".$data['file_counter']."',"
					 // ." attached_file	= '".$attachfilename."',"			
					//  ." attached_type	= '".$attached_type."',"                              
					//." attached_desc  = '".$data['attached_desc']."',"
					  ." display_name  = '".$data['display_name']."',"
					  ." display_user_id  = '".$data['display_user_id']."',"
					  ." display_designation  = '".$data['display_designation']."',"
					  ." file_upload_path  = '".$data['file_upload_path']."', "
					  ." is_visibleto_client = '".$is_visibleto_client."', "
					  ." mail_client = '".$mail_client."',"
					  ." mail_staff = '".$mail_staff."',"
					  ." mark_imp = '".$mark_imp."',"
					  ." mail_ceo = '".$mail_ceo."',"
					  ." ip  	        = '".$_SERVER['REMOTE_ADDR']."', "
					  ." do_e 	        = '".date('Y-m-d H:i:s')."'" ;
				$db->query($query);
				//As entry was onbehalf added by my BOF so by default one entry of 10min by my
				$data['hrs'] = 0;
				$data['min'] = 5; 
				//As entry was onbehalf added by my EOF
			}
			
			$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
			$min1 = (int) ($data['min'] * HRS_FACTOR);
			$current_salary = $my['current_salary'];
			$per_hour_salary = $my['current_salary'] / STD_WORKING_HRS;
			$per_hour_salary =  number_format($per_hour_salary,2,'.', '');
			$worked_min =  ( $data['hrs']*60 ) + $data['min'] ;
			$worked_hour = ($worked_min/60) ;
			$worked_hour =  number_format($worked_hour,2,'.', '');
			$worked_hour_salary = ( $per_hour_salary * $worked_hour);	
			$est_completion_dt1 ='';
			/* 
			if(!empty($data['est_completion_dt'])){
				$est_arr = explode("/",$data['est_completion_dt']);
				$est_completion_dt1 = $est_arr[2]."-".$est_arr[1]."-".$est_arr[0]." 00:00:00" ;
				$data['est_completion_date'] = date('d M Y',strtotime($est_completion_dt1));
			} */
			//{$production_team},{$telephonic_team},{$support_contact}
			if(!empty($wp_support_team) ){ 
				$production_team='';
				$wp_support_team_str = str_replace(",","','",$wp_support_team) ; 
				$sql= " SELECT f_name,l_name,desig FROM ".TABLE_USER." WHERE user_id IN('".$wp_support_team_str."') AND 
				".TABLE_USER.".status='".User::ACTIVE."'";
				$db->query($sql);
				if ( $db->nf()>0 ) {
					while ( $db->next_record() ) { 
						$production_team .= "<div>".$db->f('f_name')." ".$db->f('l_name')." <span style=\"font-family: arial,sans-serif; font-size: 10px; color: rgb(102, 102, 102);\">(".$db->f('desig').")</span></div>";
					} 
				}
				$data['production_team']=$production_team ;				
			}
			$daily_update_team = DAILY_WORK_UPDATE_TELEPHON_TEAM;
			if(!empty($daily_update_team)){ 
				$telephonic_team='';
				$wp_support_team_str = str_replace(",","','",$daily_update_team) ; 
				$sql= " SELECT f_name,l_name,desig FROM ".TABLE_USER." WHERE user_id 
					IN('".$wp_support_team_str."') AND ".TABLE_USER.".status='".User::ACTIVE."'";
				$db->query($sql);
				if ( $db->nf()>0 ) {
					while ( $db->next_record() ){ 
						$telephonic_team .= "<div>".$db->f('f_name')." ".$db->f('l_name')." <span style=\"font-family: arial,sans-serif; font-size: 11px;color: rgb(102, 102, 102);\"\">(".$db->f('desig').")</span></div>";  
					} 
				}
				$data['telephonic_team']=$telephonic_team ;				
			} 
			$data['support_contact']= "<span style=\"font-family: arial,sans-serif; font-size: 11px; line-height: 14px; padding-top: 15px; color: rgb(102, 102, 102);\">INDIA: +91- 9823066661, 9823066662 <br/>INDIA: +91- 9823066663, 9823066664<br/>USA & CANADA: 1-837-621-1000 <br/>EMAIL: Support@SMEERPTech.in</span>"; 			
			$query = "INSERT INTO ".TABLE_PROJECT_TASK_DETAILS
				  ." SET "
				  ." details      		= '".$data['details']."',"	
				  ." internal_comment   = '".$data['internal_comment']."',"	
				  ." current_status   	= '".$data['current_status']."',"	
				  ." pending_frm_client = '".$data['pending_frm_client']."',"	
				  ." est_completion_dt  = '".$data['est_completion_dt']."',"	
				  ." hrs             = '".$data['hrs']."',"
				  ." min            = '".$data['min']."',"
				  ." hrs1            = '".$hrs1."',"
				  ." min1           = '".$min1."',"
				  ." current_salary  = '".$current_salary."',"
				  ." per_hour_salary  = '".$per_hour_salary."',"
				  ." worked_hour_salary  = '".$worked_hour_salary."',"
				  ." last_updated 	= '".date('Y-m-d h:i:s')."',"
				  ." added_by      	= '".$my['user_id']."',"							 
				  ." added_by_name  = '".$my['f_name']." ".$my['l_name']."',"								 
				  ." parent_id      = '".$project_task_id."',"
				  ." project_id     = '".$data['or_id']."',"
				  ." module_id      = '".$data['module_id']."',"
				  ." subject      = '".$data['subject']."',"
				  ." is_bug       	= '0',"
				//." file_type      = '".$data['file_type']."',"
				//." file_counter   = '".$data['file_counter']."',"
				  ." attached_file	= '".$attachfilename."',"			
				  ." attached_file1	= '".$attachfilename1."',"			
				  ." attached_file2	= '".$attachfilename2."',"			
				  ." attached_file3	= '".$attachfilename3."',"			
				  ." attached_file4	= '".$attachfilename4."',"			
				  ." attached_file5	= '".$attachfilename5."',"			
				  ." attached_file6	= '".$attachfilename6."',"			
				  ." attached_file7	= '".$attachfilename7."',"			
				  ." attached_file8	= '".$attachfilename8."',"			
				  ." attached_file9	= '".$attachfilename9."',"			
				  ." attached_type	= '".$attached_type."',"                              
				  ." attached_desc  = '".$data['attached_desc']."'," 
				  ." template_id  = '".$data['template_id']."',"
				  ." template_file_1  = '".$data['template_file_1']."',"
				  ." template_file_2  = '".$data['template_file_2']."',"
				  ." template_file_3  = '".$data['template_file_3']."'," 
				  ." display_name  	  = '".$data['display_name']."',"
				  ." display_user_id  = '".$data['display_user_id']."',"
				  ." display_designation  = '".$data['display_designation']."'," 
				  ." is_visibleto_client = '".$is_visibleto_client."', "
				  ." mail_client = '".$mail_client."',"
				  ." mail_staff = '".$mail_staff."',"
				  ." mark_imp = '".$mark_imp."',"
				  ." mail_ceo = '".$mail_ceo."',"
				  ." ip  	        = '".$_SERVER['REMOTE_ADDR']."', "
				  ." do_e 	        = '".date('Y-m-d H:i:s')."'" ;
            //Post Comment EOF                   
            if ($db->query($query) && $db->affected_rows() > 0) {
            
                $variables['hid'] = $db->last_inserted_id() ;
                
				if(!empty($subUserDetails) && !empty($data['details']) && $mail_client=='1'){     
					$to = '';
					foreach ($subUserDetails  as $key=>$value){
						$email = NULL;
						$cc_to_sender= $cc = $bcc = Null;
						$data['c_title'] = $subUserDetails[$key]['title'];                            
						$data['cf_name'] = $subUserDetails[$key]['f_name'];
						$data['cl_name'] = $subUserDetails[$key]['l_name'] ;
						
						$mail_send_to .=",".$subUserDetails[$key]['email']; 
						$to[]   = array('name' => $subUserDetails[$key]['email'], 
						'email' => $subUserDetails[$key]['email']);
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']); 
						
						if(!empty($subUserDetails[$key]['email_1'])){
							$to[]   = array('name' => $subUserDetails[$key]['email_1'], 'email' => $subUserDetails[$key]['email_1']); 
							$mail_send_to .=",".$subUserDetails[$key]['email_1'];                                
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						}
						if(!empty($subUserDetails[$key]['email_2'])){ 
							$to[]   = array('name' => $subUserDetails[$key]['email_2'], 
							'email' => $subUserDetails[$key]['email_2']);
							$mail_send_to .=",".$subUserDetails[$key]['email_2'];                                
							$from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']); 
						} 
					}
				}
				if($send_offers==1){
					$table_off = TABLE_OFFERS;
					$fields_off ="*";
					$condition_off = '';
					$offerData = getRecord($table_off,$fields_off,$condition_off);
					if(!empty($offerData)){
						$data['offers']=$offerData['offers_text'];
					}
				}
				if(!empty($attachfilename)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename;
				}
				if(!empty($attachfilename1)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename1;
				}
				if(!empty($attachfilename2)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename2;
				}
				if(!empty($attachfilename3)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename3;
				}
				if(!empty($attachfilename4)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename4;
				}
				if(!empty($attachfilename5)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename5;
				}
				if(!empty($attachfilename6)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename6;
				}
				if(!empty($attachfilename7)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename7;
				}
				if(!empty($attachfilename8)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename8;
				}
				if(!empty($attachfilename9)){
					$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename9;
				}
				if(!empty($data['template_file_1'])){
					$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_1'];
				}
				if(!empty($data['template_file_2'])){
					$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_2'];
				}
				if(!empty($data['template_file_3'])){
					$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_3'];
				} 
				if($mail_ceo==1){
					$to[]   = array('name' =>CEO_USER_EMAIL,'email' => CEO_USER_EMAIL);
					$mail_send_to .=",".CEO_USER_EMAIL ;
				}


				
				if(!empty($to) && !empty($data['details'])){
					getParsedEmail($db, $s, 'PROJECT_DAILYWORK_UPDATE', $data, $email) ;
					//$to     = '';
					//echo  $email["subject"];
					//echo  $email["body"];
					//print_R($to);
					$bcc[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $smeerp_client_email);
					$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
					SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); 
					$messages->setOkMessage("Mail Sent Successfully on Order - ".$data['project_title']." - Client - ".$data['client_name']);   
				}				
				$sql = " UPDATE ".TABLE_PROJECT_TASK_DETAILS." SET 
						last_comment = '".$data['details']."',
						last_internal_comment = '".$data['internal_comment']."',						
                        last_comment_by  	= '".$my['user_id']."', 
                        last_comment_by_name  	= '".$my['f_name']." ".$my['l_name']."', 
                        last_comment_dt  	= '".date('Y-m-d H:i:s')."' 
                        WHERE id ='".$project_task_id."'"  ;
                $db->query($sql); 
				$update_sql='';
				if(!empty($mail_send_to) && !empty($data['details'])){
					$query_update = "UPDATE ". TABLE_PROJECT_TASK_DETAILS 
					." SET ". TABLE_PROJECT_TASK_DETAILS .".mail_send_to_su  = '".trim($mail_send_to ,",")."' 
					  WHERE ". TABLE_PROJECT_TASK_DETAILS .".id = ". $variables['hid'] ." " ;
					$db->query($query_update) ; 
					   
					$query_update = "UPDATE ". TABLE_BILL_ORDERS 
						." SET ".TABLE_BILL_ORDERS .".daily_update  = '".$data['details']."',
						". TABLE_BILL_ORDERS .".daily_update_dt  = '".date('Y-m-d h:i:s')."',
						". TABLE_BILL_ORDERS .".daily_update_by  = '".$my['user_id']."',
						". TABLE_BILL_ORDERS .".daily_update_by_name  = '".$my['f_name']." ".$my['l_name']."'						
						 WHERE ". TABLE_BILL_ORDERS .".id = ". $data['or_id'] ." " ;	
					$db->query($query_update) ; 
				}
				$sql_sub0=$sql_sub1=$sql_sub2=$sql_sub3='';
				if(!empty($data['internal_comment'])){
					$sql_sub0 = " daily_update_internal = '".$data['internal_comment']."',";
				}
				if(!empty($data['est_completion_dt'])){
					$sql_sub1= " last_est_completion_dt  = '".$data['est_completion_dt']."',";
				}
				if(!empty($data['current_status'])){
					$sql_sub2= " last_current_status  = '".$data['current_status']."',";
				}
				if(!empty($data['pending_frm_client'])){
					$sql_sub3= " last_pending_frm_client  = '".$data['pending_frm_client']."',";
				}  
				if(!empty($data['internal_comment']) || !empty($data['est_completion_dt']) || !empty($data['current_status']) 
					|| !empty($data['pending_frm_client']) ){
					$sql_sub = $sql_sub0.$sql_sub1.$sql_sub2.$sql_sub3 ;
					$sql_sub = trim($sql_sub,",");
					
					$query_update = "UPDATE ". TABLE_BILL_ORDERS 
					." SET ".$sql_sub."	WHERE ". TABLE_BILL_ORDERS .".id = ". $data['or_id'] ." " ;
					$db->query($query_update) ; 
				} 
			}   
            $messages->setOkMessage("Record updated successfully.");   
        }
    }
	
	
 
    if ( empty($condition_query) ) { 
        $_SEARCH = ''; 
		$condition_query = " AND date_format(".TABLE_BILL_ORDERS.".daily_update_dt, '%Y-%m-%d') != '".date('Y-m-d')."'"; 
    }
	$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    $_SEARCH['searched'] = 1;

    // To count total records.
	$condition_query = " LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id=".TABLE_BILL_ORDERS.".client
            WHERE ".TABLE_BILL_ORDERS.".status IN('".Order::ACTIVE."','".Order::PENDING."','".Order::COMPLETED."') AND ".TABLE_BILL_ORDERS.".daily_update_status='1' ".$condition_query; 
    $list	= 	NULL;
    $total	=	Order::getList( $db, $list, '', $condition_query);  
    //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage'); 
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    } 
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $condition_url .="&perform=".$perform;
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields =TABLE_BILL_ORDERS.".id, ".TABLE_BILL_ORDERS.".order_title,	".TABLE_BILL_ORDERS.".clients_su,
	".TABLE_BILL_ORDERS.".details,	
	".TABLE_BILL_ORDERS.".daily_update,	
	".TABLE_BILL_ORDERS.".daily_update_dt,	
	".TABLE_BILL_ORDERS.".daily_update_by_name,	
	".TABLE_BILL_ORDERS.".daily_update_internal,	
	".TABLE_BILL_ORDERS.".last_est_completion_dt,	
	".TABLE_BILL_ORDERS.".last_current_status,	
	".TABLE_BILL_ORDERS.".last_pending_frm_client,	
	".TABLE_CLIENTS.".f_name as cf_name,".TABLE_CLIENTS.".l_name as cl_name,".TABLE_CLIENTS.".billing_name";
    Order::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    
	$flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
			
			$query1 = "SELECT ".TABLE_PROJECT_TASK_DETAILS.".id as project_task_id FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE 
				".TABLE_PROJECT_TASK_DETAILS.".project_id='".$val['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".task_id='0' AND 
				".TABLE_PROJECT_TASK_DETAILS.".is_flw='1' LIMIT 0,1";
			$project_task_id = 0 ;
			if ( $db->query($query1) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
						$project_task_id = $db->f("project_task_id") ;					
					}
				}
			}
			$val['project_task_id'] = $project_task_id;
           
			//$last_followup_comment	= NULL;
            //$fields2 =TABLE_PROJECT_TASK_FOLLOWUP.".comment"; 
            //$condition2 = " WHERE ".TABLE_PROJECT_TASK_FOLLOWUP .".or_id= '".$val['id'] ."' GROUP BY do_e ORDER BY id DESC LIMIT 0,1";
            //ProjectTask::getDetailsCommLog( $db, $last_followup_comment, $fields2, $condition2); 
            //$val['comment']=$last_followup_comment['0']['comment'];  
			 
			$val['client_su_mem']=$clients_su_members1=array();
			if(!empty($val['clients_su'])){
				$val['clients_su1']= str_replace(',',"','",$val['clients_su']);
				Clients::getList($db, $clients_su_members1, 'user_id, number, f_name, l_name, email, mobile1,mobile2', " WHERE user_id IN ('". $val['clients_su1'] ."')") ;
				$val['client_su_mem']=$clients_su_members1;
			}  
			$flist[$key]=$val; 
        }
    }
	
	include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
	$stTemplates = null;
	$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
	".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::WP.",%' ORDER BY title";
	SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
	$page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
	$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
	$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
	$page["var"][] = array('variable' => 'moduleArr', 'value' => 'moduleArr');
	
    $hidden[] = array('name'=> 'perform' ,'value' => 'followup_final');
    // Set the Permissions.   
    $variables['can_view_list'] = false;
    $variables['can_send_via_ceo'] = false;
    //$variables['can_mail_to_su'] = false;
    //$variables['can_mail_on_ad'] = false;
     $variables['can_select_team']  = false;   
    if ( $perm->has('nc_pu_list') ) {
        $variables['can_view_list'] = true;
    }
	if ( $perm->has('nc_pu_send_via_ceo') ) {
        $variables['can_send_via_ceo'] = true;
    }
    /*if ( $perm->has('nc_pu_mail_su') ) {
        $variables['can_mail_to_su'] = true;
    }
    if ( $perm->has('nc_pu_mail_ad') ) {
        $variables['can_mail_on_ad'] = true;
    }*/
    if ( $perm->has('nc_st_select_team') ) { 
	   $variables['can_select_team'] = true;
	}
	
    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'list', 'value' => 'flist');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-updates-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
