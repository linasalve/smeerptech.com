<?php

if ( $perm->has('nc_hr_sal_details') ) {

	include_once ( DIR_FS_INCLUDES .'/hr-attendance.inc.php' );
	$date_from      = isset($_POST["date_from"]) ? $_POST["date_from"]   : (isset($_GET["date_from"]) 
	? $_GET["date_from"]    :'');
	$date_to = isset($_POST["date_to"]) ? $_POST["date_to"] : (isset($_GET["date_to"]) ? $_GET["date_to"]      :'');	
	$uid      = isset($_POST["uid"])    ? $_POST["uid"]       : (isset($_GET["uid"])    ? $_GET["uid"]    :'');
	
	if(!empty($date_from)){
		$dfa = explode('/', $date_from);
		$dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] ;
		$condition_query = " AND DATE_FORMAT(". TABLE_HR_ATTENDANCE .".date,'%Y-%m-%d' ) >= '". $dfa ."' AND ";
		$condition_query2 = " AND DATE_FORMAT(". TABLE_HR_ATTENDANCE_MINI .".attendance_dt,'%Y-%m-%d' ) >= '". $dfa ."' AND ";
	}
	if(!empty($date_to)){
		$dta = explode('/', $date_to);
		$dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0];
		$condition_query .= " DATE_FORMAT(".TABLE_HR_ATTENDANCE .".date,'%Y-%m-%d') <= '". $dta ."'";
		$condition_query2 .= " DATE_FORMAT(".TABLE_HR_ATTENDANCE_MINI .".attendance_dt,'%Y-%m-%d') <= '". $dta ."'";
	}
	
	//DATE_FORMAT(".TABLE_SCORE_SHEET.".date,'%m-%Y')
	// To count total records.
    $list	= 	NULL;
    $condition_query1 = " LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id = ".TABLE_HR_ATTENDANCE.".uid 
		WHERE ".TABLE_HR_ATTENDANCE .".uid = '".$uid."' ".$condition_query." ORDER BY ".TABLE_HR_ATTENDANCE.".date";
    $total	=	HrAttendance::getList( $db, $list, '', $condition_query1);
    
    
 
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    $list	= NULL;
    $fields = TABLE_HR_ATTENDANCE.'.* ,'.TABLE_USER.".f_name,".TABLE_USER.".l_name,".
	TABLE_USER.".current_salary,".TABLE_USER.".remarks";
    HrAttendance::getList( $db, $list, $fields, $condition_query1, $next_record, $rpp);
	$variables['pstatus'] = HrAttendance::getPresenty();
    $pstatusArr = array_flip($variables['pstatus']);
    $flist= $user_data = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){      
           
		  //$val['att_time_in']=synchronizeTime($datetime->dateToUnixTime($val["att_time_in"]),$my["time_offset"]);
		  //$val['att_time_out']=synchronizeTime($datetime->dateToUnixTime($val["att_time_out"]),$my["time_offset"]);
            $val['pstatus'] = $pstatusArr[$val['pstatus']];
            $flist[$key] = $val;
			
        }
    }
	
	
	$sql1 = " SELECT ".TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".current_salary,".TABLE_USER.".remarks,".TABLE_USER.".do_join FROM ".TABLE_USER." WHERE ".TABLE_USER.".user_id = '".$uid."' " ;
	$db->query($sql1) ;
	while($db->next_record()){
		if($db->nf() > 0){
			$user_data['current_salary'] =  $db->f('current_salary');
			$user_data['remarks'] =  $db->f('remarks');
			$user_data['do_join'] =  $db->f('do_join');
			$user_data['name'] =  $db->f('f_name')." ".$db->f('l_name') ;
		} 
	}
	 
	 
	 
	$sql1 = " SELECT sec_to_time( sum( extract( HOUR FROM `worked_hrs` ) *3600 + extract(MINUTE FROM `worked_hrs`) *60 ) ) AS total_mv_time FROM ".TABLE_HR_ATTENDANCE." ".$condition_query1  ;
	$db->query($sql1) ;
	while($db->next_record()){
		if($db->nf() > 0){
			$worked_tot_hrs = $db->f('total_mv_time');
		}else{
			$worked_tot_hrs = 0 ;
		}
	}
	
	$holidays=array();
	$working_days = $totalMonthDays ='';
	if(!empty($date_from) && !empty($date_to)){
		$working_days = getWorkingDays($dfa,$dta,$holidays);
		$totalMonthDays = getTotalDays($dfa,$dta);
	}
	
	
	///ATTENDANCE MINI BOF
	include_once ( DIR_FS_INCLUDES .'/hr-attendance-mini.inc.php' );
	$list2	= 	NULL;
    $condition_query2 = " LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id = 
	".TABLE_HR_ATTENDANCE_MINI.".user_id WHERE ".TABLE_HR_ATTENDANCE_MINI.".user_id = '".$uid."'
	".$condition_query2." ORDER BY ".TABLE_HR_ATTENDANCE_MINI.".attendance_dt " ; 
	 
	/*  $total	=	HrAttendanceMini::getList( $db, $list2, '', $condition_query2);    
    $extra_url2  = '';
    if ( isset($condition_url2) && !empty($condition_url2) ) {
        $extra_url2  = $condition_url2;
    }
    $extra_url2  .= "&x2=$x2&rpp2=$rpp2";
    $extra_url2  = '&start2=url2'. $extra_url2 .'&end=url';
    $condition_url2 .="&rpp2=".$rpp2."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x2, $rpp2, $condition_url2, 'changePageWithExtra'); */
    $list2	= NULL;
    $fields = TABLE_HR_ATTENDANCE_MINI.'.* ,'.TABLE_USER.".f_name,".TABLE_USER.".l_name";
    //HrAttendanceMini::getList( $db, $list2, $fields, $condition_query2, $next_record, $rpp);
    HrAttendanceMini::getList( $db, $list2, $fields, $condition_query2);
	if(!empty($list2)){
		$totalPday =0;
        foreach( $list2 as $key=>$val){ 
		    $totalPday = $totalPday  + $val['present_score'];
            $flist2[$key]=$val;
        }
    }
	// ATTENDANCE MINI EOF
	
	//BO Total Advance transaction  
	$limitDt = "2012-09-12";
	$sql3 =" SELECT SUM( pay_received_amt_inr ) AS totalAdv FROM ".TABLE_PAYMENT_TRANSACTION." 
	WHERE ".TABLE_PAYMENT_TRANSACTION.".executive_id = '".$uid."' AND ".TABLE_PAYMENT_TRANSACTION .".transaction_type='2' AND
	".TABLE_PAYMENT_TRANSACTION.".is_salary_advance='1' AND DATE_FORMAT(".TABLE_PAYMENT_TRANSACTION.".do_transaction,'%Y-%m-%d') >= '".$limitDt."' AND ".TABLE_PAYMENT_TRANSACTION.".status IN('0','1')" ;
	
	$db->query($sql3) ;
	while($db->next_record()){
		if($db->nf() > 0){
			$user_data['totalAdv'] = $db->f('totalAdv');
		}else{
			$user_data['totalAdv'] = 0 ;
		}
	}
	//EO Total Advance transaction  
	$limitDt = "2012-10-01";
	//BO Total Salary deducted
    $sql4 =" SELECT SUM( deduction_amount ) AS totalDeduc FROM ".TABLE_HR_SALARY." 
	WHERE ".TABLE_HR_SALARY.".user_id = '".$uid."' AND 
	DATE_FORMAT(".TABLE_HR_SALARY.".do_e,'%Y-%m-%d') >= '".$limitDt."' AND ".TABLE_HR_SALARY.".deduction_amount>0 "   ; 
	$db->query($sql4) ;
	while($db->next_record()){
		if($db->nf() > 0){
			$user_data['totalDeduc'] = $db->f('totalDeduc');
		}else{
			$user_data['totalDeduc'] = 0 ;
		}
	}
	$user_data['totalBalance'] = $user_data['totalAdv'] -  $user_data['totalDeduc'] ;
	//EO Total Salary deducted
	
	
	
	
	
	
	$page["var"][] = array('variable' => 'list2', 'value' => 'flist2');
	$page["var"][] = array('variable' => 'list', 'value' => 'flist');
	$page["var"][] = array('variable' => 'user_data', 'value' => 'user_data');
	$page["var"][] = array('variable' => 'worked_tot_hrs', 'value' => 'worked_tot_hrs');
	$page["var"][] = array('variable' => 'working_days', 'value' => 'working_days');
	$page["var"][] = array('variable' => 'totalPday', 'value' => 'totalPday');
	$page["var"][] = array('variable' => 'totalMonthDays', 'value' => 'totalMonthDays');
	$page["var"][] = array('variable' => 'date_from', 'value' => 'date_from');
	$page["var"][] = array('variable' => 'date_to', 'value' => 'date_to');
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-salary-get-attendence.html'); 
	
	 
	 
}
else {
	$messages->setErrorMessage("You donot have the Permisson to Access this module.");
}
?>
