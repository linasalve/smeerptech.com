<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/letters.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/common-functions.inc.php' );
	
    $do_l = isset($_GET['do_l']) ? $_GET['do_l'] : '';
    $company_id = isset($_GET['company_id']) ? $_GET['company_id'] : '';
    
   	$details = $subject=$file_1=$file_2=$file_3='' ;
	$file_1_str = $file_2_str = $file_3_str ='' ;
	
    if (isset($do_l) && !empty($company_id)){
		
		
		$data['do_l'] = explode('/', $do_l);
        $data['do_l'] = mktime(0, 0, 0, $data['do_l'][1], $data['do_l'][0], $data['do_l'][2]);
		$detailNo = getLetterNumber( $data['do_l'] ,$company_id); 				
		if(!empty($detailNo)){
			echo $data['number'] = $detailNo['number'];
			$data['counter'] = $detailNo['counter'];
		}
		 
	}else{
		echo "";
	}	
	include_once( DIR_FS_NC ."/flush.php");
?>
