<?php
if ( $perm->has('nc_sms_pr_renew') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        
        //Get gateway list 
        $lst_gateway=null;
        $fields = TABLE_SMS_GATEWAY .'.id'
                   .','. TABLE_SMS_GATEWAY .'.company';
        $condition_query= "WHERE status='".SmsGateway::ACTIVE."'";
        SmsGateway::getList($db,$lst_gateway,$fields,$condition_query);
        
        //Get api list 
        $lst_api=null;
        $fields = TABLE_SMS_API .'.id'
                   .','. TABLE_SMS_API .'.name';
        $condition_query= "WHERE status='".SmsApi::ACTIVE."'";
        SmsApi::getList($db,$lst_api,$fields,$condition_query);
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( SmsPurchase::validateUpdate($data, $extra) ) {
            
               $query = "SELECT * FROM ".TABLE_SMS_PURCHASE." WHERE id='".$id."'";
               if ( $db->query($query) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                            $history[] = processSqlData($db->result());
                        }
                    }
                }
                $history = $history['0'];
                
               $query  = " UPDATE ". TABLE_SMS_PURCHASE
                            ." SET ". TABLE_SMS_PURCHASE .".balance_sms 		= balance_sms+'".$data['allotted_sms'] ."'"
                                	.",". TABLE_SMS_PURCHASE .".allotted_sms 	= allotted_sms+'". $data['allotted_sms'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE .".rate 	        = '". $data['rate'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE .".amount 			= '". $data['amount'] ."'"                                     
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Sms Purchase entry has been renewed successfully.");
                    $variables['hid'] = $id;
                    $balance_sms = $history['balance_sms'] + $data['allotted_sms'];
                    $query	= " INSERT INTO ".TABLE_SMS_PURCHASE_HISTORY
                            ." SET ".TABLE_SMS_PURCHASE_HISTORY .".purchase_id 			= '". $history['id'] ."'"  
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".gateway_id 		= '". $history['gateway_id'] ."'"                                
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".api_id 			= '". $history['api_id'] ."'"                                
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".no_of_sms 	    = '". $data['allotted_sms'] ."'"                    
                                    .",". TABLE_SMS_PURCHASE_HISTORY .".consumed_sms 	= '". $history['consumed_sms'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".opening_sms 	= '". $history['balance_sms'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".balance_sms 	= '".$balance_sms ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".rate 	        = '". $history['rate'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".amount 			= '". $history['amount'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".created_by 		= '". $history['created_by'] ."'"                    
                                    .",". TABLE_SMS_PURCHASE_HISTORY .".ip 			    = '". $history['ip'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".do_e            = '". $history['do_e']."'"
                                    .",". TABLE_SMS_PURCHASE_HISTORY .".ip_h 			= '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".do_he           = '". date('Y-m-d H:i:s')."'"
                                    .",". TABLE_SMS_PURCHASE_HISTORY .".history_created_by = '". $my['user_id'] ."'";   
                    $db->query($query);                 
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SMS_PURCHASE .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SMS_PURCHASE .".id = '". $id ."' )";
            
            if ( SmsPurchase::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                $id         = $_ALL_POST['id'];
                $_ALL_POST['allotted_sms'] = "";
                $_ALL_POST['rate'] = "";
                $_ALL_POST['amount'] = "";
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        /*if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            include ( DIR_FS_NC .'/sms-purchase-list.php');
        }*/
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
            header("Location:".DIR_WS_NC."/sms-purchase.php?perform=renew&added=1&id=".$id);
            $variables['hid'] = $id;
            $condition_query='';
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sms-purchase.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sms-purchase.php?added=1");   
            $variables['hid'] = $id;
            $condition_query='';
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'renew');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'lst_gateway', 'value' => 'lst_gateway');                 
            $page["var"][] = array('variable' => 'lst_api', 'value' => 'lst_api');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-purchase-renew.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
