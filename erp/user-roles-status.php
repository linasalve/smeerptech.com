<?php
    if ( $perm->has('nc_urol_status') ) {
        $role_id	= isset($_GET["role_id"]) 	? $_GET["role_id"] 	: ( isset($_POST["role_id"]) 	? $_POST["role_id"] : '' );
        $status	    = isset($_GET["status"])    ? $_GET["status"] 	: ( isset($_POST["status"])     ? $_POST["status"]  : '' );
        
        $extra = array( 'db' 			=> &$db,
                        'messages' 		=> $messages
                    );
        UserRoles::updateStatus($role_id, $status, $extra);
    
        include ( DIR_FS_NC .'/user-roles-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>