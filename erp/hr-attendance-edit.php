<?php
    if ( $perm->has('nc_hr_att_edit') ) {
    
        $att_id    = isset($_GET["att_id"]) ? $_GET["att_id"] : ( isset($_POST["att_id"]) ? $_POST["att_id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $lst_hr = HrAttendance::getHrs();
		$lst_min = HrAttendance::getMin();
		$lst_sec = HrAttendance::getSec();
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,                          
                            'messages'          => &$messages
                        );
          
            if ( HrAttendance::validateUpdate($data, $extra) ) {
				
                if($data['pstatus']==HrAttendance::PRESENT || $data['pstatus']==HrAttendance::HALFDAY){
                
                    $worked_hrs ='';
                    //Calculate Worked hrs between intime and outtime bof
                   /*  $strIn   = $datetime->unixTimeToDate(desynchronizeTime($data['fromdate'], $my["time_offset"]));
                    $strOut   = $datetime->unixTimeToDate(desynchronizeTime($data['todate'], $my["time_offset"])); 
					
                    echo $sql= " SELECT TIMEDIFF('".$strOut."','".$strIn."') as work_hrs";
					*/
					$sql= " SELECT TIMEDIFF('".$data['todate']."','".$data['fromdate']."') as work_hrs";
                    $db->query($sql);	
                    while ( $db->next_record() ) {
                        $worked_hrs = $db->f('work_hrs');
                    }
                    //Calculate Worked hrs between intime and outtime eof
                   
                  /*  echo $query	= " UPDATE  ".TABLE_HR_ATTENDANCE
					." SET ".TABLE_HR_ATTENDANCE .".att_time_in 
					= '". $datetime->unixTimeToDate(desynchronizeTime($data['fromdate'], $my["time_offset"])) ."'"  
					.",". TABLE_HR_ATTENDANCE .".att_time_out 
					= '". $datetime->unixTimeToDate(desynchronizeTime($data['todate'], $my["time_offset"])) ."'"
					.",". TABLE_HR_ATTENDANCE .".att_place = '".OFFICE_PLACE ."'"
					.",". TABLE_HR_ATTENDANCE .".pstatus = '". $data['pstatus'] ."'"
					.",". TABLE_HR_ATTENDANCE .".att_reason = '". $data['att_reason'] ."'"    
					.",". TABLE_HR_ATTENDANCE .".worked_hrs = '". $worked_hrs ."'"    
					.",". TABLE_HR_ATTENDANCE .".date = '". date('Y-m-d H:i:s', $data['date']) ."'"    
					.",". TABLE_HR_ATTENDANCE .".uid = '".$data['executive'] ."'"    
					.",". TABLE_HR_ATTENDANCE .".created_by = '".$my['uid'] ."'"    
					.",". TABLE_HR_ATTENDANCE .".ip       = '". $_SERVER['REMOTE_ADDR'] ."'"                    
					.",". TABLE_HR_ATTENDANCE .".do_e = '". date('Y-m-d H:i:s')."'
					WHERE ".TABLE_HR_ATTENDANCE.".att_id ='".$att_id."'"; */
					
					$query	= " UPDATE  ".TABLE_HR_ATTENDANCE
					." SET ".TABLE_HR_ATTENDANCE .".att_time_in = '".$data['fromdate'] ."'"  
					.",". TABLE_HR_ATTENDANCE .".att_time_out = '". $data['todate'] ."'"
					.",". TABLE_HR_ATTENDANCE .".att_place = '".OFFICE_PLACE ."'"
					.",". TABLE_HR_ATTENDANCE .".pstatus = '". $data['pstatus'] ."'"
					.",". TABLE_HR_ATTENDANCE .".att_reason = '". $data['att_reason'] ."'"    
					.",". TABLE_HR_ATTENDANCE .".worked_hrs = '". $worked_hrs ."'"    
					.",". TABLE_HR_ATTENDANCE .".date = '". date('Y-m-d H:i:s', $data['date']) ."'"    
					.",". TABLE_HR_ATTENDANCE .".uid = '".$data['executive'] ."'"    
					.",". TABLE_HR_ATTENDANCE .".created_by = '".$my['uid'] ."'"    
					.",". TABLE_HR_ATTENDANCE .".ip       = '". $_SERVER['REMOTE_ADDR'] ."'"                    
					.",". TABLE_HR_ATTENDANCE .".do_e = '". date('Y-m-d H:i:s')."'
					WHERE ".TABLE_HR_ATTENDANCE.".att_id ='".$att_id."'";
           
                    
                }else{
                
                    $query	= " UPDATE ".TABLE_HR_ATTENDANCE." SET "
                                    . TABLE_HR_ATTENDANCE .".att_place = '".OFFICE_PLACE ."'"
									.",". TABLE_HR_ATTENDANCE .".att_time_in = '0000-00-00 00:00:00'"
									.",". TABLE_HR_ATTENDANCE .".att_time_out = '0000-00-00 00:00:00'"
									.",". TABLE_HR_ATTENDANCE .".pstatus = '". $data['pstatus'] ."'"
                                    .",". TABLE_HR_ATTENDANCE .".att_reason = '". $data['att_reason'] ."'"    
                                    .",". TABLE_HR_ATTENDANCE .".date = '". date('Y-m-d H:i:s', $data['date']) ."'"    
                                    .",". TABLE_HR_ATTENDANCE .".uid = '".$data['executive'] ."'"    
                                    .",". TABLE_HR_ATTENDANCE .".created_by = '".$my['uid'] ."'"    
                                    .",". TABLE_HR_ATTENDANCE .".ip       = '". $_SERVER['REMOTE_ADDR'] ."'"                                      
                                	.",". TABLE_HR_ATTENDANCE .".do_e = '". date('Y-m-d H:i:s')."'
                                     WHERE ".TABLE_HR_ATTENDANCE.".att_id ='".$att_id."'";
                
                
                }
                            
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    $variables['hid'] = $att_id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_HR_ATTENDANCE .'.*'  ;            
            $condition_query = " WHERE (". TABLE_HR_ATTENDANCE .".att_id = '". $att_id ."' )";
           
            if ( HrAttendance::getList($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                 
                
                
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    $att_id         = $_ALL_POST['att_id'];
                    
                    $condition_querye=" WHERE "." user_id='".$_ALL_POST['uid']."'";
                    User::getList( $db, $edetails, 'f_name,l_name,number,email', $condition_querye);
                    $edetails = $edetails[0];
                    $_ALL_POST['executive_details'] = $edetails['f_name']." ".$edetails['l_name']." (".$edetails['number'].") (".$edetails['email'].")" ;
                    $_ALL_POST['executive'] = $_ALL_POST['uid'];
                    
                   
                  
                   
                    $att_time_in  = $datetime->unixTimeToDate(synchronizeTime($datetime->dateToUnixTime($_ALL_POST["att_time_in"]), $my["time_offset"] ));
                   
                    $att_time_out = $datetime->unixTimeToDate(synchronizeTime($datetime->dateToUnixTime($_ALL_POST["att_time_out"]), $my["time_offset"] ));
                    $inArr = explode(" ", $att_time_in ); 
                    $inArr1 = explode(":", $inArr[1] ); 
                    
                    $_ALL_POST['cmbtimeinHour'] =$inArr1[0];
                    $_ALL_POST['cmbtimeinMin']  =$inArr1[1];
                    $_ALL_POST['cmbtimeinSec']  =$inArr1[2];
                   
                    $outArr = explode(" ", $att_time_out ); 
                    $outArr1 = explode(":", $outArr[1] ); 
                    $_ALL_POST['cmbtimeoutHour']    =$outArr1[0];
                    $_ALL_POST['cmbtimeoutMin']     =$outArr1[1];
                    $_ALL_POST['cmbtimeoutSec']     =$outArr1[2];
                                                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $att_id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/hr-attendance-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'att_id', 'value' => $att_id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'lst_hr', 'value' => 'lst_hr');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'lst_sec', 'value' => 'lst_sec');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-attendance-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>