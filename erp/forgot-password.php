<?php
if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    /*
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    */
    //include_once ( DIR_FS_NC ."/header.php" );
    //include_once ( DIR_FS_NC ."/header.php" );
    
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    
    $db 		= new db_local; // database handle
	$s 			= new smarty; 	//smarty template handle
	$messages 	= new Messager;	// create the object for the Message class.
    
    
    $variables["title"] 			= TITLE ;
	$variables["domain"] 			= THIS_DOMAIN;
    $variables["lib"]               = DIR_WS_LIB;
    $variables["nc"] 				= DIR_WS_NC;
    $variables["mp"] 				= DIR_WS_MP;
    $variables["nc_images"]		    = DIR_WS_IMAGES_NC;
    $variables["images"]            = DIR_WS_IMAGES;
    $variables["css"]               = DIR_WS_CSS;
    $variables["scripts"]           = DIR_WS_SCRIPTS;
    $variables["file_save_domain"] 	= DIR_WS_ST_FILES;
    
    
    if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) ) {
         $_ALL_POST  = $_POST;
         $data       = processUserData($_ALL_POST);
         
          
          
            if ( !isset($data['number']) || empty($data['number']) ) {
                $messages->setErrorMessage("Account number can not be empty.");
            }
            if ( !isset($data['username']) || empty($data['username']) ) {
                $messages->setErrorMessage("Username can not be empty.");
            }
            
            if ( $messages->getErrorMessageCount() <= 0 ) {
				
                $condition_query = " WHERE (". TABLE_USER .".number = '". $data['number'] ."' AND ". TABLE_USER .".username = '". $data['username']."') ";
                $fields = TABLE_USER .'.user_id '
                                .','. TABLE_USER .'.number'
                                .','. TABLE_USER .'.f_name '
                                .','. TABLE_USER .'.l_name'
                                .','. TABLE_USER .'.email'
                                .','. TABLE_USER .'.username'
                                .','. TABLE_USER .'.status';
					
                if ( User::getList($db, $list, $fields, $condition_query) > 0 ) {
                    $list = $list['0'];
                    
                    if($list['status']==User::ACTIVE){
                        $new_pass = substr(md5($list["f_name"]), 2, 6) . rand(5, 125) ;
                        $maildata['newpassword'] =  $new_pass;
                        $maildata['password'] = md5($new_pass);
                        
                        $maildata['f_name']=$list['f_name'];
                        $maildata['l_name']=$list['l_name'];
                        $maildata['number']=$list['number'];
                        $maildata['email']=$list['email'];
                        $maildata['username']=$list['username'];
                        $name = $maildata['f_name']." ". $maildata['l_name'] ;
                        
                        $query  = " UPDATE ". TABLE_USER
                        ." SET password='". $maildata['password']."'"
                        ." WHERE ". TABLE_USER.".user_id   = '". $list['user_id'] ."'";
                        
                        if ( $db->query($query) ) {
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'EXECUTIVE_FORGOT_PASSWORD', $maildata, $email) ) {                                
                                $to     = '';                   
                                $to[]   = array('name' => $name, 'email' => $maildata['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                               $messages->setOkMessage("The email containing the new password is emailed to you.");
                             
                            }
                        }
                    }else{
                        $messages->setErrorMessage("Status is not valid. Please contact with the administrator.");
                    }
                      
                }else{
                    $messages->setErrorMessage("The combination of Account number and Username is not valid.");
                }
                
			}
            
            
    }
    
   
    
     //$page["section"][] = array('container'=>'CONTENT', 'page' => 'forgot-password.html');
     
     $page["section"][] = array('container'=>'CONTENT', 'page' => 'forgot-password.html');
     $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
     //$page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
     $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages()); 
    
    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
        $hidden[]   = array('name'=> $key, 'value' => $value);
    }
            
           
    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
    
?>