<?php

    if ( $perm->has('nc_sms_ca_view') ) {
		$cah_caid = isset($_GET['ca_id']) ? $_GET['ca_id'] : ( isset($_POST['ca_id'] ) ? $_POST['ca_id'] :'');
		if(empty($condition_query))
			$condition_query = " WHERE cah_caid ='".$cah_caid."' ";
		else
			$condition_query = " AND cah_caid ='".$cah_caid."' ";

        //$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	ClientAccount::viewList( $db, $list, 'cah_caid', $condition_query);
    	//$variables['hid'] = $cah_caid;
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        ClientAccount::viewList( $db, $list, '*', $condition_query, $next_record, $rpp);

        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'client-account-view.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>