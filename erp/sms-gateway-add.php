<?php
  if ( $perm->has('nc_sms_gtw_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the payment party class
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( SmsGateway::validateAdd($data, $extra) ) { 
                            $query	= " INSERT INTO ".TABLE_SMS_GATEWAY
                            ." SET ".TABLE_SMS_GATEWAY .".company 				= '". $data['company'] ."'"  
                                	.",". TABLE_SMS_GATEWAY .".address 			= '". $data['address'] ."'"                                
                                	.",". TABLE_SMS_GATEWAY .".contact_name1 	= '". $data['contact_name1'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_dept1 			= '". $data['contact_dept1'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_email1 	= '". $data['contact_email1'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_phone1 			= '". $data['contact_phone1'] ."'"                    
                                    .",". TABLE_SMS_GATEWAY .".contact_name2 	= '". $data['contact_name2'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_dept2 			= '". $data['contact_dept2'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_email2 	= '". $data['contact_email2'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_phone2 			= '". $data['contact_phone2'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".order				= '". $data['order'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".details 	= '". $data['details'] ."'"                     	
                                	.",". TABLE_SMS_GATEWAY .".created_by 			= '". $my['user_id'] ."'"                    
									.",". TABLE_SMS_GATEWAY .".status 			= '". $data['status'] ."'"                    
                                    .",". TABLE_SMS_GATEWAY .".ip 			= '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".do_e = '". 		date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New sms gateway entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sms-gateway.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sms-gateway.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sms-gateway.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-gateway-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
