<?php
	if ( $perm->has('nc_bl_po_edit') ) {
        $po_id	= isset($_GET["po_id"]) ? $_GET["po_id"] 	: ( isset($_POST["po_id"]) ? $_POST["po_id"] : '' );

        $data           = "";
        $_ALL_POST      = "";
        $access_level   = $my['access_level'];
        /*if ( $perm->has('nc_bl_po_add_al') ) {
            $access_level += 1;
        }*/
        $al_list        = getAccessLevel($db, $access_level);
        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 				=> &$db,
                            'data' 				=> $data,
                            'messages' 			=> $messages,
                        );
            $data['do_po'] = time();
            if ( PreOrder::validateUpdate($data, $messages, $extra, $po_id) ) {
                
                $query	= " UPDATE ".TABLE_BILL_PO
                            ." SET ". TABLE_BILL_PO .".q_no             = '". $data['q_no'] ."'"
                                .",". TABLE_BILL_PO .".access_level     = '". $data['access_level'] ."'"                                
                                .",". TABLE_BILL_PO .".order_details    = '". $data['order_details'] ."'"
                                .",". TABLE_BILL_PO .".client_details   = '". $data['client_details'] ."'"
                                .",". TABLE_BILL_PO .".payment_details   = '". $data['payment_details'] ."'"
                                .",". TABLE_BILL_PO .".remark   		= '". $data['remark'] ."'"                                
                                .",". TABLE_BILL_PO .".status           = '". $data['status'] ."'" 
                            ." WHERE id = '". $po_id ."' ";
                if ( $db->query($query) ) {
                    if ( $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("The Pre-Order has been Updated Successfully.");

                        // Send the notification mails to the concerned persons.
                        include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                        $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_po_app', 'access_level'=>$data['access_level']),
                                                                array('right'=>'nc_bl_po_app_al', 'access_level'=>($data['access_level']-1) )
                                                            )
                                                );

                        include ( DIR_FS_INCLUDES .'/user.inc.php');
                        $data['id']     = $po_id;
                        $data['link']   = DIR_WS_NC .'/bill-pre-order.php?perform=view&po_id='. $po_id;
                        $data['date']   = date('d M, Y (H:i)', $data['do_po']);
                        $data['creator']= '';
                        User::getList($db, $data['creator'], 'username,number,email,f_name,l_name', "WHERE user_id = '". $data['created_by'] ."'");
                        $data['creator']= $data['creator'][0];
                        $data['updater']= array('f_name'=> $my['f_name'],'l_name'=> $my['l_name'],'number'=> $my['number']);

                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_PO_UPDATE_MANAGERS', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }

                        // Send Email to the Creator of the Pre-Order.
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_PO_UPDATE', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                    else {
                        $messages->setOkMessage("There were no changes in the Pre-Order.");
                    }
                }
                // Clear all the data.
                $_ALL_POST	= "";
                $data		= "";
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
                || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $po_id;
            $perform='list';
            $condition_query='';   
            
            if ( isset($_POST['action']) && $_POST['action'] == 'search' ) {
                include ( DIR_FS_NC .'/bill-pre-order-search.php');
            }
            else {
                include ( DIR_FS_NC .'/bill-pre-order-list.php');
            }
        }
        else {

            $condition_query= " WHERE id = '". $po_id ."' ";
            $_ALL_POST      = NULL;
            if ( PreOrder::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                $_ALL_POST  = $_ALL_POST['0'];

                // These parameters will be used when returning the control back to the List page.
                foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                    $hidden[]   = array('name'=> $key, 'value' => $value);
                }
                $hidden[]   = array('name'=> 'po_id' , 'value' => $po_id);
                    
                // Check if the Pre Order has been Approved?
                if ( $_ALL_POST['status'] != PreOrder::COMPLETED ) {

                    $hidden[]   = array('name'=> 'perform' , 'value' => 'edit');
                    $hidden[]   = array('name'=> 'act' , 'value' => 'save');
                    $hidden[]   = array('name'=> 'created_by' , 'value' => $_ALL_POST['created_by']);
                }
                else {
                    // Empty the Pre Order details fetched from the Database.
                    $_ALL_POST = NULL;
                    $messages->setErrorMessage("Cannot Edit Pre Order: It has been Approved.");
                }
            }

            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');

            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-pre-order-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
