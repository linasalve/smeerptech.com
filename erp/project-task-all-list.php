<?php
if ( $perm->has('nc_p_ts_list') ) {

    
	if ( !isset($condition_query) || $condition_query == '' ) {
		$condition_query = '';
		$_SEARCH["chk_status"]  = 'AND';
		$_SEARCH["sStatus"]     = array(ProjectTask::STS_ACTIVE); 
		$condition_query = " AND "." ". TABLE_PROJECT_TASK_DETAILS .".status_id IN('".ProjectTask::STS_ACTIVE ."')";			
	}
	$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;    
    
	if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
	
	
    $_SEARCH['searched'] = 1; 
    // To count total records.
    $condition_query = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_flw='0' ".$condition_query;
    $list	= 	NULL;
    $total	=	ProjectTask::getListAll( $db, $list, '', $condition_query); 
    //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage'); 
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $condition_url .="&perform=".$perform."&actual_time=".$actual_time;
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    //$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','project-task.php','frmSearch');
    $list	= NULL;
    $fields = TABLE_PROJECT_TASK_DETAILS.".id,".TABLE_PROJECT_TASK_DETAILS.".title as task_title, ".TABLE_PROJECT_TASK_DETAILS.".details,".TABLE_PROJECT_TASK_DETAILS.".last_updated, ".TABLE_PROJECT_TASK_MODULE.".title as module_title," ;
	//".TABLE_PROJECT_TASK_STATUS.".title as status_title,TABLE_PROJECT_TASK_PRIORITY.".title as priority_title, "..TABLE_PROJECT_TASK_RESOLUTION.".title as rs_title"
    $fields .=TABLE_PROJECT_TASK_DETAILS.".resolved_date,".TABLE_PROJECT_TASK_DETAILS.".is_client,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".hrs,".TABLE_PROJECT_TASK_DETAILS.".min,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".hrs1,".TABLE_PROJECT_TASK_DETAILS.".min1,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".do_e,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".status_id,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".added_by,";
	$fields .=TABLE_PROJECT_TASK_DETAILS.".added_by_name," ;
	$fields .=TABLE_PROJECT_TASK_DETAILS.".project_id," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".internal_comment," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".files_link," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".files_link2," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".last_comment," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".last_comment_by_name," ;
    $fields .= TABLE_PROJECT_TASK_TYPE.".title as type_title," ;
    $fields .= TABLE_PROJECT_TASK_DETAILS.".attached_file,".TABLE_PROJECT_TASK_DETAILS.".attached_file1," ;
    $fields .= TABLE_PROJECT_TASK_DETAILS.".attached_file2,".TABLE_PROJECT_TASK_DETAILS.".attached_file3," ;
    $fields .= TABLE_PROJECT_TASK_DETAILS.".attached_file4,".TABLE_PROJECT_TASK_DETAILS.".attached_file5," ;
    $fields .= TABLE_PROJECT_TASK_DETAILS.".attached_file6,".TABLE_PROJECT_TASK_DETAILS.".attached_file7," ;
    $fields .= TABLE_PROJECT_TASK_DETAILS.".attached_file8,".TABLE_PROJECT_TASK_DETAILS.".attached_file9," ;
    $fields .= TABLE_BILL_ORDERS.".order_title, ".TABLE_BILL_ORDERS.".number as or_number" ;
    ProjectTask::getListAll( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    $flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){   			 
			
			if($val['status_id']>0){
				$val['status_title'] = $statusArr[$val['status_id']];
			}
            if($val['is_client']=='1'){
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                $val['is_client'] = '1';
            }else{
                $table = TABLE_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                 $val['is_client']='0';
            }
            
            if($actual_time){
                $val['tothrs'] = strlen($val['hrs']) ==1 ? '0'.$val['hrs'] : $val['hrs'] ;
                $val['totmin'] = strlen($val['min']) ==1 ? '0'.$val['min'] : $val['min'] ;
            }else{
                  // show atual total time (timesheet * 3 ) total cost
                $val['tothrs'] = $val['hrs1'] ;
                $val['totmin'] = $val['min1'] ;
                $extraHr = (int) ($val['min1'] / 60 ) ;
                $min1 =  ($val['min1'] % 60 ) ;
                $val['tothrs'] =  $val['tothrs'] + $extraHr ;
                $val['totmin'] = $min1 ;
                
                $val['tothrs'] = strlen($val['tothrs']) ==1 ? '0'.$val['tothrs'] : $val['tothrs'] ;
                $val['totmin'] = strlen($val['totmin']) ==1 ? '0'.$val['totmin'] : $val['totmin'] ;
            }
            //Calculate total bugs
            $condition_query_bug = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PROJECT_TASK_DETAILS.".task_id = '".$val['id']."'";
            $listbug	= 	NULL;
            $totalbug	=	ProjectTask::getList( $db, $listbug, TABLE_PROJECT_TASK_DETAILS.'.id', $condition_query_bug);
            $val['totalbugs']= $totalbug;
            //Calculate total pending bugs 
            $condition_query_bugp =" WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' AND "
			  .TABLE_PROJECT_TASK_DETAILS.".task_id = '".$val['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".status_id ='".ProjectTask::STS_ACTIVE ."'";
            $listpbug	= 	NULL;
            $totalpendingbugs	=	ProjectTask::getList( $db, $listpbug, TABLE_PROJECT_TASK_DETAILS.'.id', $condition_query_bugp);
            $val['totalpendingbugs']= $totalpendingbugs;
             //Calculate taskwise hrs spent
            if($actual_time){
                $fields_tws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin, 
				( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost' ;
            }else{
                // show atual total time (timesheet * 3 ) total cost
                  $fields_tws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) % 60 ) as totMin,
				  ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost' ;
            }
            $condition_query_twh = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0'";
            $list_tws	= 	NULL;
          	ProjectTask::getList( $db, $list_tws, $fields_tws, $condition_query_twh);           
         
            $val['tot_bhrs'] = '00';
            $val['tot_bmin'] = '00' ;
            $val['worked_task_cost'] = '0' ;
			
            if(!empty($list_tws[0]['totHr']) || !empty($list_tws[0]['totMin'])){
                $list_tws =$list_tws[0];
                $val['tot_thrs'] = strlen($list_tws['totHr']) ==1 ? '0'.$list_tws['totHr'] : $list_tws['totHr'] ; 
                $val['tot_tmin'] = strlen($list_tws['totMin']) ==1 ? '0'.$list_tws['totMin'] : $list_tws['totMin'] ; 
                $val['tot_tcost'] = $list_tws['tot_tcost'] ;
            }else{
                $val['tot_thrs'] = '00';
                $val['tot_tmin'] = '00' ;
				$val['tot_tcost'] = 0 ;
            }
            
            //Calculate bugwise hrs spent
            if($actual_time){
                $fields_bws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin, 
				( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_bcost ' ;
            }else{
                // show atual total time (timesheet * 3 ) total cost
                $fields_bws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) % 60 ) as totMin,
				( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_bcost' ; 
            }
            $condition_query_bwh = " WHERE (".TABLE_PROJECT_TASK_DETAILS.".task_id ='".$val['id']."' AND 
                                    ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' 
                                    AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0' ) OR 
                                    ".TABLE_PROJECT_TASK_DETAILS.".parent_id IN (
                                            SELECT ".TABLE_PROJECT_TASK_DETAILS.".id FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE 
                                            ".TABLE_PROJECT_TASK_DETAILS.".task_id ='".$val['id']."' AND 
                                            ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' 
                                            AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0' 
                                        )";
            $list_bws	= 	NULL;
          	ProjectTask::getList( $db, $list_bws, $fields_bws, $condition_query_bwh);           
            if(!empty($list_bws[0]['totHr']) || !empty($list_bws[0]['totMin'])){
                $list_bws =$list_bws[0];
                $val['tot_bhrs'] = strlen($list_bws['totHr']) ==1 ? '0'.$list_bws['totHr'] : $list_bws['totHr'] ; 
                $val['tot_bmin'] = strlen($list_bws['totMin']) ==1 ? '0'.$list_bws['totMin'] : $list_bws['totMin'] ; 
                $val['tot_bcost']= $list_bws['tot_bcost'];
            }else{
                $val['tot_bhrs'] = '00';
                $val['tot_bmin'] = '00' ;
				$val['tot_bcost']=0;
            }
			$val['worked_task_cost']= $val['tot_tcost'] + $val['tot_bcost'] ;
			$val['worked_task_cost']= ceil($val['worked_task_cost']);
            //Get Last comment bof
            $val['last_comment']='';
            //$condition_query_tlc = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0' ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".id DESC Limit 0,1";
            $condition_query_tlc = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".id DESC Limit 0,1";
            $list_tlc	= 	NULL;
            $fields_tlc = TABLE_PROJECT_TASK_DETAILS.".details";
          	ProjectTask::getList( $db, $list_tlc, $fields_tlc, $condition_query_tlc);           
            if(!empty($list_tlc)){
                $list_tlc=$list_tlc[0];
                $val['last_comment'] =  $list_tlc['details'] ;
            } 
            //Get Last comment eof
            $tlist[$key]=$val;
        }
    }
	 
	$page["var"][] = array('variable' => 'tlist', 'value' => 'tlist');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
	
	include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
   
    $lst_hrs = $lst_min = $lst_typ= NULL;
	$lst_hrs[0] = 'Hr';
	for($i=1; $i<=20;$i++){
		if(strlen($i)==1) $i='0'.$i ;
		$lst_hrs[$i] = $i;
	}
	$lst_min[0] = 'Min';
	for($j=1; $j<=59 ;$j++){
		if(strlen($j)==1) $j='0'.$j ;
		$lst_min[$j] = $j;
	} 
	
	include_once (DIR_FS_INCLUDES .'/project-module.inc.php');
	$moduleArr	= NULL;
	/* As we changed the logic behind order based modules to Global Modules
		We are fetching the new modules ie module id > 871; Till 871 all modules are Order based thats why we are 
		implementing this logic
	*/
	//$condition_querym = " WHERE parent_id='0' AND status ='".ProjectTask::ACTIVE."' AND order_id='".$or_id."'";
	$condition_querym = " WHERE parent_id='0' AND ".TABLE_PROJECT_TASK_MODULE.".id > 871 AND ".TABLE_PROJECT_TASK_MODULE.".status = '".ProjectTask::ACTIVE."' ";
	ProjectModule::getList( $db, $moduleArr, 'id, title', $condition_querym);
	
	 
	
	 
	/* 
	include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
	$stTemplates = null;
	$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
	".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::WP.",%' ORDER BY title";
	SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'.TABLE_ST_TEMPLATE.'.title', $condition_query_st); 
	*/
	$page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
	$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
	$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
	$page["var"][] = array('variable' => 'moduleArr', 'value' => 'moduleArr'); 
	
	$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'list', 'value' => 'flist');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
	
    // Set the Permissions. 
	$variables['can_view_cost'] = false;	
    if ( $perm->has('nc_p_ts_cost') ) {
        $variables['can_view_cost'] = true;
    }	
   
	
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task-all-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
