<?php
     if ($perm->has(EDIT)){    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $output = "";
        Webaccess::getAllWebAccessGroups();
        $all_groups = $output ;
        
        include_once (DIR_FS_INCLUDES .'/web-access.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'messages'          => &$messages
                        );
          
            if ( Webaccess::validateUpdate($data, $extra) ) {
        
                $data['from_ip']=$data['fIp1'].".".$data['fIp2'].".".$data['fIp3'].".".$data['fIp4'] ;
                $data['to_ip']=$data['tIp1'].".".$data['tIp2'].".".$data['tIp3'].".".$data['tIp4'] ;
                $query	= " UPDATE ".TABLE_WEB_ACCESS_LIST
                            ." SET ". TABLE_WEB_ACCESS_LIST .".group_id = '".             $data['group_id'] ."'"
                                .",". TABLE_WEB_ACCESS_LIST .".from_ip = '".              $data['from_ip'] ."'"
                               .",". TABLE_WEB_ACCESS_LIST .".to_ip = '".                 $data['to_ip'] ."'"
                               .",". TABLE_WEB_ACCESS_LIST .".collected_ip = '".         ",".trim($data['collected_ip'])."," ."'" 
                               ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_WEB_ACCESS_LIST .'.*'  ;            
            $condition_query = " WHERE (". TABLE_WEB_ACCESS_LIST .".id = '". $id ."' )";
            
            
           
            if ( Webaccess::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                if(!empty($_ALL_POST['from_ip'])){
                    $fromArr = explode('.', $_ALL_POST['from_ip'] );
                    $_ALL_POST['fIp1'] = $fromArr[0] ;
                    $_ALL_POST['fIp2'] = $fromArr[1] ;
                    $_ALL_POST['fIp3'] = $fromArr[2] ;
                    $_ALL_POST['fIp4']  = $fromArr[3] ;
                    
                }
                
                if(!empty($_ALL_POST['to_ip'])){
                    $toArr   =  explode('.',$_ALL_POST['to_ip']) ;                     
                    $_ALL_POST['tIp1'] = $toArr[0] ;
                    $_ALL_POST['tIp2'] = $toArr[1] ;
                    $_ALL_POST['tIp3'] = $toArr[2] ;
                    $_ALL_POST['tIp4']  = $toArr[3] ;
                    
                }
                
                $id        = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            include ( DIR_FS_ADMIN .'/web-access-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
             $page["var"][] = array('variable' => 'all_groups', 'value' => 'all_groups');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'web-access-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
          $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-edit-permission.html');
    }
?>