<?php

  if ($perm->has(ADD)){
      
        $_ALL_POST      = NULL;
        $data           = NULL;
        //$access_level   = $my['access_level'];
        
		
        
		// Include the hardware book class.
		include_once (DIR_FS_INCLUDES .'/web-access.inc.php');
		$output = "";
        Webaccess::getAllWebAccessGroups();
        $all_groups = $output ;
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
           
           
            $extra = array( 'db' 				=> &$db,
            				'messages'          => &$messages
                        );
                
            if ( Webaccess::validateAdd($data, $extra) ) { 
                $data['from_ip']=$data['fIp1'].".".$data['fIp2'].".".$data['fIp3'].".".$data['fIp4'] ;
                $data['to_ip']=$data['tIp1'].".".$data['tIp2'].".".$data['tIp3'].".".$data['tIp4'] ;
                $query	= " INSERT INTO ".TABLE_WEB_ACCESS_LIST
                            ." SET ". TABLE_WEB_ACCESS_LIST .".group_id = '".             $data['group_id'] ."'"
                                .",". TABLE_WEB_ACCESS_LIST .".from_ip = '".              $data['from_ip'] ."'"
                               .",". TABLE_WEB_ACCESS_LIST .".to_ip = '".                 $data['to_ip'] ."'"
                               .",". TABLE_WEB_ACCESS_LIST .".collected_ip = '".         ",".trim($data['collected_ip'])."," ."'" ;
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
     
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_ADMIN .'/web-access-list.php');
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'all_groups', 'value' => 'all_groups');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'web-access-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-add-permission.html');
        
    }
?>