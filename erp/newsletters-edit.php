<?php
    if ( $perm->has('nc_nwl_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        include_once ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST); 
			
            $extra = array( 'db'                => &$db,
                            'messages'          => &$messages
                        );
          
            if ( Newsletters::validateUpdate($data, $extra) ){
				
				$send_newsletter=0;
				$added=1;
				$status = Newsletters::PENDING;
				$condition = $message=''; 
				$total_members=0;					
				$per_batch = PER_BATCH_NWS;
				$total_batches=0;
				$number = $data['number'];
				//$number  =  Newsletters::getNewNumber($db);
				$attachfilename=$attachment_path='';
				if(!empty($data['attachment'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['attachment']["name"]); 
					$ext = $filedata["extension"] ;  
					$attachfilename = $number."-".mktime().".".$ext ;
					$data['attachment'] = $attachfilename;                        
					$attachment_path = DIR_WS_NWS_FILES;                        
					if (move_uploaded_file ($files['attachment']['tmp_name'], DIR_FS_NWS_FILES."/".$attachfilename)){
						@chmod(DIR_FS_NWS_FILES."/".$attachfilename, 0777);
					}
					$sql_attachment = "', ".TABLE_NEWSLETTERS .".attachment = '". $attachfilename."'"  ;
				}
			   
				if(isset($data['send_newsletter']) ){
					$added=2;
					$send_newsletter=1;
					$status = Newsletters::PENDING;
					$message = ' and added in a queue.';						
					//$sqlCount = " SELECT MAX(id) as total FROM ".TABLE_NEWSLETTERS_MEMBERS ;
					$sqlCount =  "SELECT COUNT(".TABLE_NEWSLETTERS_MEMBERS.".id) as total FROM 
						".TABLE_NEWSLETTERS_MEMBERS." WHERE 
						( 	".TABLE_NEWSLETTERS_MEMBERS.".email_bounce_status!='".NewslettersMembers::EMAILBOUNCE."'	AND 	
							".TABLE_NEWSLETTERS_MEMBERS.".unsubscribed_status!='".NewslettersMembers::UNSUBSCRIBED."' AND
							".TABLE_NEWSLETTERS_MEMBERS.".nws_category_id IN ('".$data['nws_category_id']."') )" ;
					$db->query($sqlCount);
					if ( $db->nf() > 0 ) {
						while ($db->next_record()){
							$total_members	= $db->f('total');
						}
					}						
				}else{
					$status = Newsletters::DRAFT ;
				}               
					
				if($total_members > 0){
					$total_batches =  ceil($total_members/$per_batch)  ;	
				}else{
					if($send_newsletter==1){
						$added=3;
						$send_newsletter=0;
						$status=Newsletters::DRAFT;
						$message=' and Save as a Draft <br/> as No members Found in NDB';
					}else{
						$added=4;
						$message=' and Save as a Draft.';
					}
				}
				$sql='';
				if($send_newsletter == 1){
					$sql =  ", ".TABLE_NEWSLETTERS .".send_on_date     = '".date('Y-m-d H:i:s')."'"
						    .", ".TABLE_NEWSLETTERS .".send_newsletter = '".$send_newsletter."'"
							.", ".TABLE_NEWSLETTERS .".total_members   = '".$total_members."' "
							.", ".TABLE_NEWSLETTERS .".per_batch        = '".$per_batch."' "
							.", ".TABLE_NEWSLETTERS .".total_batches    = '".$total_batches."' "
							.", ".TABLE_NEWSLETTERS .".status        = '".$status."' " ;
				}
				
                 $query  = " UPDATE ". TABLE_NEWSLETTERS
                            ." SET ". TABLE_NEWSLETTERS .".subject 	= '". $data['subject']."'"
							.",".TABLE_NEWSLETTERS .".gmail_subject = '". $data['gmail_subject'] ."'"
							.",".TABLE_NEWSLETTERS .".text       	= '". $data['text']."'"
							.",".TABLE_NEWSLETTERS .".identity_no 	= '". $data['identity_no'] ."'"
							.",".TABLE_NEWSLETTERS .".eml_template  = '". $data['eml_template']."'"
							.",".TABLE_NEWSLETTERS .".sendmethod    = '". $data['sendmethod']."'"
							.",".TABLE_NEWSLETTERS .".sockethost    = '". $data['sockethost']."'"
							.",".TABLE_NEWSLETTERS .".smtpauth      = '". $data['smtpauth']."'"
							.",".TABLE_NEWSLETTERS .".smtpauthuser  = '". $data['smtpauthuser']."'"
							.",".TABLE_NEWSLETTERS .".smtpauthpass  = '". $data['smtpauthpass']."'"
							.",".TABLE_NEWSLETTERS .".smtpport      = '". $data['smtpport']."'"
							.",".TABLE_NEWSLETTERS .".from_name     = '". $data['from_name']."'"
							.",".TABLE_NEWSLETTERS .".from_email    = '". $data['from_email']."'"
							.",".TABLE_NEWSLETTERS .".replyto_name  = '". $data['replyto_name']."'"
							.",".TABLE_NEWSLETTERS .".replyto_email = '". $data['replyto_email']."'"
							.",".TABLE_NEWSLETTERS .".nws_category_id 	= '". $data['nws_category_id']."'"
							.",".TABLE_NEWSLETTERS .".client			= '". $data['client'] ."' "
							.",".TABLE_NEWSLETTERS .".domain_name		= '". $data['domain_name'] ."' "
							.",".TABLE_NEWSLETTERS .".domain_mailgun_key = '". $data['domain_mailgun_key'] ."' "
							.",".TABLE_NEWSLETTERS .".newsletters_smtp_id  = '". $data['newsletters_smtp_id'] ."' "
							.",".TABLE_NEWSLETTERS .".test_email       = '". $data['test_email']."'"
							.",".TABLE_NEWSLETTERS .".remarks       	= '". $data['remarks']."'"
							.$sql_attachment
							.$sql
                            ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    $variables['hid'] = $id;
					$auth_details['sendmethod']=  $data['sendmethod'];
					$auth_details['sockethost']= $data['sockethost'] ;
					$auth_details['smtpauth']=  $data['smtpauth'] ; 
					$auth_details['smtpauthuser']= $data['smtpauthuser'];
					$auth_details['smtpauthpass'] = $data['smtpauthpass'];
					$auth_details['socketfrom'] = $data['socketfrom'];
					$auth_details['socketfromname'] = $data['socketfromname'];
					$auth_details['smtpport'] =  $data['smtpport'];
					/*
					$auth_details['sendmethod']= 'smtp'; 
					$auth_details['sockethost']='mail.emailsrvr.com';
					$auth_details['smtpauth']='TRUE';
					$auth_details['smtpauthuser']='demo@smeerpemail.net';
					$auth_details['smtpauthpass'] = 'hongkong'; 
					$auth_details['socketfrom'] = 'demo@smeerpemail.net';
					$auth_details['socketfromname'] = 'Administrator';
					$auth_details['smtpport'] = '587'; 
					$auth_details['socketreply'] = '';
					$auth_details['socketreplyname']=''; */
					
					if($send_newsletter==1){
						$exist = 0; 
						
						$query1 = "INSERT INTO "	
							.TABLE_NEWSLETTERS_BATCHES ." SET "
							.TABLE_NEWSLETTERS_BATCHES .".newsletters_id         = '". $variables['hid']."', "
							.TABLE_NEWSLETTERS_BATCHES .".number    = '". $data['number'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".subject    = '". $data['subject'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".gmail_subject     = '". $data['gmail_subject'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".identity_no     = '". $data['identity_no'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".text       = '". $data['text'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".eml_template  = '". $data['eml_template'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".sendmethod    = '". $data['sendmethod'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".sockethost    = '". $data['sockethost'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".smtpauth      = '". $data['smtpauth'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".smtpauthuser  = '". $data['smtpauthuser'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".smtpauthpass  = '". $data['smtpauthpass'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".smtpport       = '". $data['smtpport'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".from_name       = '". $data['from_name'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".from_email       = '". $data['from_email'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".replyto_name       = '". $data['replyto_name'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".replyto_email       = '". $data['replyto_email'] ."', "							
							.TABLE_NEWSLETTERS_BATCHES .".nws_category_id       = '". $data['nws_category_id'] ."', " 
							.TABLE_NEWSLETTERS_BATCHES .".client	= '". $data['client'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".domain_name	= '". $data['domain_name'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".domain_mailgun_key	= '". $data['domain_mailgun_key'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".newsletters_smtp_id  = '". $data['newsletters_smtp_id'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".attachment = '". $attachfilename ."', "
							.TABLE_NEWSLETTERS_BATCHES .".total_batches    = '".$total_batches."', "
							.TABLE_NEWSLETTERS_BATCHES .".pending_batches    = '".$total_batches."', "
							.TABLE_NEWSLETTERS_BATCHES .".total_members        = '".$total_members."', "
							.TABLE_NEWSLETTERS_BATCHES .".per_batch        = '".$per_batch."', "                      
							.TABLE_NEWSLETTERS_BATCHES .".send_on_date    = '".date('Y-m-d H:i:s')."', "    
							.TABLE_NEWSLETTERS_BATCHES .".do_e    = '".date('Y-m-d H:i:s')."' "; 
						$db->query($query1);
					}
					/*
					if(!empty($data['test_email'])){
						$smeerp_support_email = $data['test_email'];
					} */
					$toArr = explode(",", $data['test_email']);
					$unsubid = base64_encode($smeerp_support_email);
					$nws_id = base64_encode($id);
					$data['unsubscribed'] = THIS_DOMAIN.'/unsubscribe.php?id='.$unsubid   ;
					$data['html_link'] = THIS_DOMAIN.'/newsletters.php?id='.$nws_id."&email=".$unsubid   ;
					$data['attachment']   =   $attachfilename ;
					$data['counter']   =   1 ;
					$data['subject']   =   $_ALL_POST['subject'] ;
					$data['gmail_subject']   =   $_ALL_POST['gmail_subject'] ;
					$data['text']   =   $_ALL_POST['text'] ;
					$data['nws_number']   =    $data['number']."-v".$_ALL_POST['identity_no']."-".$data['nws_category_id'] ;					
					$file_name = DIR_FS_NWS_FILES ."/". $attachfilename;
					 //Send a copy to check the mail for clients
					$sendStatus='';
					$to_email ='';
					$eml_template = $data['eml_template'];
					if ( getParsedEmail($db, $s, $eml_template, $data, $email) ) { 
						$to     = '';
						$reply_to=$cc_to_sender=$cc=$bcc='';
						//$to[]   = array('name' => $smeerp_support_name, 'email' => $smeerp_support_email);
						if(!empty($toArr)){
							foreach($toArr as $key1=>$val1){
							  $to[]   = array('name' => $val1, 'email' => $val1);
							}
						}
						/* if(!empty($data['test_email'])){
							$smeerp_support_email=$data['test_email'];
						} */							    
						if(!empty($to)){
							
							$from= array('name' => $data["from_name"],'email' => $data['from_email']);
							$reply_to = array('name' => $data["replyto_name"],'email' => $data['replyto_email']);
						    //echo $email["body"];
							//print_R($to);
						    SendMailAuth($to, $from, $reply_to, $email["subject"], $email["body"],$email["isHTML"], $cc_to_sender,$cc,$bcc,$file_name,$auth_details); 
						   //SendMail($to, $from, $reply_to, $email["subject"], $email["body"],$email["isHTML"], $cc_to_sender,$cc,$bcc,$file_name); 
						}                                
					}  
					
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_NEWSLETTERS .'.*'  ;            
            $condition_query = " WHERE (". TABLE_NEWSLETTERS .".id = '". $id ."' )";
          
            if ( Newsletters::getList($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];                   
                $id         = $_ALL_POST['id'];
                $number         = $_ALL_POST['number'];
				//Get client data BOF
				if(!empty($_ALL_POST['client'])){
					$table = TABLE_CLIENTS;
					$condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$_ALL_POST['client'] ."' " ;
					$fields1 =  TABLE_CLIENTS .'.f_name,'.TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['client_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." (".$detailsArr['billing_name'].")" ;   
					}				 
				}
				//Get client data EOF
				
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission 
				to access this Module.");
            }
        }
		
		$fields1 = "id,category";
		$condition_query1 = " WHERE ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY.".status='1' ";
		NewslettersMembers::getCategory($db, $category_list,$fields1, $condition_query1);
		
		$smtp_list = array();
		if(!empty($_ALL_POST['client'])){
			include_once ( DIR_FS_INCLUDES .'/newsletter-smtp.inc.php');
			$fields1 =TABLE_NEWSLETTER_SMTP.".smtp_id,".TABLE_NEWSLETTER_SMTP.".domain_name,".TABLE_NEWSLETTER_SMTP.".from_email";
			$condition_query1 = " WHERE ".TABLE_NEWSLETTER_SMTP.".status='1' AND ".TABLE_NEWSLETTER_SMTP.".client='".$_ALL_POST['client']."'";
			NewsletterSmtp::getList($db, $smtp_list,$fields1, $condition_query1);
		}
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            include ( DIR_FS_NC .'/newsletters-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'number', 'value' => $number);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'smtp_list', 'value' => 'smtp_list');
            $page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletters-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>