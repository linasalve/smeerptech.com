<?php
    if ( $perm->has('nc_bk_stmt_csv') ){ //
        $_ALL_POST  = NULL;
        $data       = NULL;
        $messages_ask 	= new Messager;	// create the object for the Message class.
		
		
        $CSV = BankStatement::getRestrictions();
        
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);          
			$files       = processUserData($_FILES);
    		$ask_added      = 0;
			$ask_duplicate  = 0;
			$ask_empty      = 0;
			 print_R($files );
            $extra = array( 'file'      => &$files,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );
			 
			 
            if ( BankStatement::validateUpload($data, $extra) ) {
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $invalid  = 0;
                $empty      = 0;
                $index      = array();
                $handle     = fopen($_FILES['file_csv']['tmp_name'],"rb");
                $message = '';
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
                    if ( $row == 1 ) {
                        $num = count($data);
                        for ($i=0; $i < $num; $i++) {
                            $case = trim($data[$i]);
                            switch ($case) {
                                                      
								case ('date'): {
                                    $index['date'] = $i;
                                    break;
                                }									
								case ('description'): {
                                    $index['description'] = $i;
                                    break;
                                }
								case ('cheque_no'): {
                                    $index['cheque_no'] = $i;
                                    break;
                                }
								case ('debit'): {
                                    $index['debit'] = $i;
                                    break;
                                }
								case ('credit'): {
                                    $index['credit'] = $i;
                                    break;
                                }
								case ('balance'): {
                                    $index['description'] = $i;
                                    break;
                                }
								case ('val_date'): {
                                    $index['val_date'] = $i;
                                    break;
                                }  
								 
								case ('bank'): {
                                    $index['bank'] = $i;
                                    break;
                                } 	
								case ('month'): {
                                    $index['month'] = $i;
                                    break;
                                }	
								case ('month_nm'): {
                                    $index['month_nm'] = $i;
                                    break;
                                }
								case ('year'): {
                                    $index['year'] = $i;
                                    break;
                                }
								
                            }
                        }
                    }
                    else {                 
                          $index= array ( 
							"date"=>0,
							"description"=>1, 
							"cheque_no"=>2, 
							"debit"=>3, 
							"credit"=>4, 
							"balance"=>5, 
							"val_date"=>6,  
							"bank"=>7, 
							"month"=>8, 
							"month_nm"=>9, 
							"year"=>10
                           );
							$company_id=0;
							$table = TABLE_PAYMENT_BANK;
							$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$data[$index['bank']]."' " ;
							$fields1 =  TABLE_PAYMENT_BANK .'.company_id' ;
							$banknameArr = getRecord($table,$fields1,$condition2);
							if(!empty($banknameArr)){
								$company_id =  $banknameArr['company_id'] ;                    
							} 
							$dtArr=$dtArr2=array();
							$date_stmt=$val_date1='';
						    //dd-mm-yyyy
						    $dt1 = trim($data[$index['date']]);
						    $dtArr = explode('-', $dt1);
						    $date_stmt = $dtArr[2]."-".$dtArr[1]."-".$dtArr[0]." 00:00:00";
							
							$dt2 = trim($data[$index['val_date']]);
						    $dtArr2 = explode('-', $dt2);
						    $val_date1 = $dtArr2[2]."-".$dtArr2[1]."-".$dtArr2[0]." 00:00:00";
							
							if( $data[$index['bank']] != '' && $company_id>0 ){ 
								$do_stmt = $data[$index['year']]."-".$data[$index['month']]."-01";
								 
								$sqlu = " INSERT INTO ".TABLE_BANK_STATEMENT
										." SET stmt_date = '".$date_stmt."',"
										." bank_id = '".$data[$index['bank']]."'," 
										." company_id = '".$company_id."'," 
										." description = '".$data[$index['description']]."'," 
										." cheque_no = '".$data[$index['cheque_no']]."'," 
										." debit = '".$data[$index['debit']]."'," 
										." credit = '".$data[$index['credit']]."'," 
										." balance = '".$data[$index['balance']]."'," 
										." value_date = '".$val_date1."'," 
										." year = '".$data[$index['year']]."'," 
										." month = '".$data[$index['month']]."'," 
										." month_name = '".$data[$index['month_nm']]."'," 
										." do_stmt = '".$do_stmt."'," 
										." status = '1'," 
										." do_e = '". date('Y-m-d h:i:s')."'," 
										." added_by = '". $my['user_id']."'," 
										." added_by_name = '". $my['f_name']." ".$my['l_name']."' " ;		
									
								 
								$db->query($sqlu);   
								$added += 1;
								$messages->setOkMessage('Records Added '.$data[$index['description']]);
							}else{
								$empty += 1;
							
							}
							/*
							$dt = $data[$index['tax_paid_dt']];
						    $dtArr = explode('/', $dt);
						    $tax_paid_dt = $dtArr[2]."-".$dtArr[1]."-".$dtArr[0]." 00:00:00"; 
							if( $data[$index['bank_id']] != '' && $data[$index['cheque_no']] != '' ) { 
								$table = TABLE_BANK_STATEMENT;
							    $cond =" WHERE ".TABLE_BANK_STATEMENT.".cheque_no = '".$data[$index['cheque_no']]."'
									AND ".TABLE_BANK_STATEMENT.".bank_id = '".$data[$index['bank_id']]."'";
								if ( !BankCheque::duplicateFieldValue($db, $table, 'id', $cond) ) {
									$company_id = 0;
									$banknameArr=array();
									$table = TABLE_PAYMENT_BANK;
									$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= 
									'".$data[$index['bank_id']]."' " ;
									$fields1 =  TABLE_PAYMENT_BANK .'.company_id' ;
									$banknameArr = getRecord($table,$fields1,$condition2);
									if(!empty($banknameArr)){
										$company_id =  $banknameArr['company_id'] ;                    
									} 
									$sqlu= " INSERT INTO ".TABLE_BANK_STATEMENT
									." SET cheque_no = '".$data[$index['cheque_no']]."',"
									." bank_id = '".$data[$index['bank_id']]."'," 
									." company_id = '".$company_id."'," 
									." cheque_book_no = '". $data[$index['cheque_book_no']]."'," 
									." do_e = '". date('Y-m-d h:i:s')."'," 
									." added_by = '". $my['user_id']."'," 
									." added_by_name = '". $my['f_name']." ".$my['l_name']."' " 	 ;		
								
									$db->query($sqlu);
									$added += 1;
									$messages->setOkMessage('Added Cheque No. -'.$data[$index['cheque_no']]);
								}else{
									$messages->setOkMessage('Duplicate Cheque no. for selected bank -'.$data[$index['cheque_no']]);
								} 
							}else{
								$empty += 1;
								$messages->setErrorMessage('Cheque No. -'.$data[$index['cheque_no']]." not found.");
							}
							*/							
							 
                      
                        
                        // The Row is not the First row.
                        
                    }                    
                }
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been added.');
                if ($notfound)
                    $messages->setOkMessage($notfound .' out of '. $row .' records were not found and neglected.');
                
				if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
        }
        
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$condition ='';
			$searchStr='1';
			$perform='list';
            include ( DIR_FS_NC .'/bank-statement-list.php');
        }else {
			
			 
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'csv_upload');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bank-statement-csv.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>