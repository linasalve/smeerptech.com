<?php
    if ( $perm->has('nc_ld_qt_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/leads-quotation.inc.php');
		include_once ( DIR_FS_INCLUDES .'/leads-order.inc.php');
		
        $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $or_no          = isset($_GET["or_no"]) ? $_GET["or_no"] : ( isset($_POST["or_no"]) ? $_POST["or_no"] : '' );

        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level'] + 1;
        
        
        $currency = NULL;
        $required_fields ='*';
        Currency::getList($db,$currency,$required_fields);
        
        //show invoice bof
         // Read the Invoice which is to be Updated.
        $fields = TABLE_LEADS_QUOTATION .'.*'
                    .','. TABLE_LEADS_ORDERS .'.old_particulars'
                    .','. TABLE_SALE_LEADS .'.lead_id AS c_user_id'
                    .','. TABLE_SALE_LEADS .'.number AS c_number'
                    .','. TABLE_SALE_LEADS .'.f_name AS c_f_name'
                    .','. TABLE_SALE_LEADS .'.l_name AS c_l_name'
                    .','. TABLE_SALE_LEADS .'.email AS c_email'
                    .','. TABLE_SALE_LEADS .'.status AS c_status' ;
        
        $condition_query = " WHERE (". TABLE_LEADS_QUOTATION .".id = '". $inv_id ."' "
                            ." OR ". TABLE_LEADS_QUOTATION .".number = '". $inv_id ."')";
		
		
        if ( LeadsQuotation::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];
            
           
            
            if ( $_ALL_POST['access_level'] < $access_level ) {
           
                // Set up the Client Details field.
                $_ALL_POST['client_details']= $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
                                                .' ('. $_ALL_POST['c_number'] .')'
                                                .' ('. $_ALL_POST['c_email'] .')';
                
                // Set up the dates.
                $_ALL_POST['do_i']  = explode(' ', $_ALL_POST['do_i']);
                $temp               = explode('-', $_ALL_POST['do_i'][0]);
                $_ALL_POST['do_i']  = NULL;
                $_ALL_POST['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                
                $_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
                $temp               = explode('-', $_ALL_POST['do_d'][0]);
                $_ALL_POST['do_d']  = NULL;
                $_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                
                if($_ALL_POST['do_e'] !='0000-00-00 00:00:00'){
                    $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                    $temp               = explode('-', $_ALL_POST['do_e'][0]);
                    $_ALL_POST['do_e']  = NULL;
                    $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                }else{
                    $_ALL_POST['do_e']  ='';
                }
                
                if($_ALL_POST['do_fe'] !='0000-00-00 00:00:00'){
                    $_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
                    $temp               = explode('-', $_ALL_POST['do_fe'][0]);
                    $_ALL_POST['do_fe']  = NULL;
                    $_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                }else{
                
                    $_ALL_POST['do_fe']  ='';
                }
                // Set up the Services.
                //$_ALL_POST['service_id'] = explode(',', $_ALL_POST['service_id']);                  
                $condition_query = '';
                $or_no              = $_ALL_POST['or_no'];
				
            } else {
          
                $messages->setErrorMessage("You do not have the Permission to edit the Proposal with the current Access Level.");
            }
        }
        else {
           
            $messages->setErrorMessage("The Proposal was not found or you do not have the Permission to access this Proposal.");
        }
        //show invoice eof
        
      
      
      
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])  || isset($_POST['btnReturnRcp']) ) 
		&& $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);

            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                           // 'lst_service'       => $lst_service,
                            'messages'          => &$messages
                        );

            if ( LeadsQuotation::validateUpdate($data, $extra) ) {
                
                $invDetails =null;
                $condition_query1 = " WHERE ".TABLE_LEADS_QUOTATION.".number ='".$data['number']."'";
                $fields1 = TABLE_LEADS_QUOTATION.".currency_abbr,".TABLE_LEADS_QUOTATION.".currency_name,
				".TABLE_LEADS_QUOTATION.".currency_symbol,".TABLE_LEADS_QUOTATION.".currency_country,
				".TABLE_LEADS_QUOTATION.".round_off_op,".TABLE_LEADS_QUOTATION.".round_off ,
				".TABLE_LEADS_QUOTATION.".dont_show_total,	
				".TABLE_LEADS_QUOTATION.".company_name,".TABLE_LEADS_QUOTATION.".tin_no, 
				".TABLE_LEADS_QUOTATION.".cst_no,".TABLE_LEADS_QUOTATION.".vat_no,".TABLE_LEADS_QUOTATION.".do_tax,
				".TABLE_LEADS_QUOTATION.".service_tax_regn,".TABLE_LEADS_QUOTATION.".do_st,
				".TABLE_LEADS_QUOTATION.".pan_no,".TABLE_LEADS_QUOTATION.".delivery_at,
				".TABLE_LEADS_QUOTATION.".order_closed_by,".TABLE_LEADS_QUOTATION.".tax1_declaration,
				".TABLE_LEADS_QUOTATION.".other_details,
				".TABLE_LEADS_QUOTATION.".sub_total_amount,".TABLE_LEADS_QUOTATION.".tax1_id,
				".TABLE_LEADS_QUOTATION.".tax1_number,".TABLE_LEADS_QUOTATION.".tax1_name,
				".TABLE_LEADS_QUOTATION.".tax1_value,".TABLE_LEADS_QUOTATION.".tax1_pvalue,
				".TABLE_LEADS_QUOTATION.".tax1_total_amount,
				".TABLE_LEADS_QUOTATION.".tax1_sub1_id,".TABLE_LEADS_QUOTATION.".tax1_sub1_number,
				".TABLE_LEADS_QUOTATION.".tax1_sub1_name,".TABLE_LEADS_QUOTATION.".tax1_sub1_value,
				".TABLE_LEADS_QUOTATION.".tax1_sub1_pvalue,".TABLE_LEADS_QUOTATION.".tax1_sub1_total_amount,				
				".TABLE_LEADS_QUOTATION.".tax1_sub2_id,".TABLE_LEADS_QUOTATION.".tax1_sub2_number,
				".TABLE_LEADS_QUOTATION.".tax1_sub2_name,".TABLE_LEADS_QUOTATION.".tax1_sub2_value,
				".TABLE_LEADS_QUOTATION.".tax1_sub2_pvalue,".TABLE_LEADS_QUOTATION.".tax1_sub2_total_amount	";
                
				LeadsQuotation::getList( $db, $invDetails, $fields1, $condition_query1 );
                $data['currency_abbr'] = '';
                $data['currency_name'] = '';
                $data['currency_symbol'] = '';
                $data['currency_country'] = '';
                $data['do_tax']=0;			 
                if(!empty($invDetails)){
                    $invDetails = $invDetails[0] ;
					//$data['other_details'] = $invDetails['other_details'];
                    $data['order_closed_by'] = $invDetails['order_closed_by'];
                    $data['currency_abbr'] = $invDetails['currency_abbr'];
                    $data['currency_name'] = $invDetails['currency_name'];
                    $data['currency_symbol'] = $invDetails['currency_symbol'];
                    $data['currency_country'] = $invDetails['currency_country'];
					$data['round_off_op'] =  $invDetails['round_off_op'];
                    $data['round_off'] 	  =  $invDetails['round_off'];
					$data['dont_show_total'] 	  =  $invDetails['dont_show_total'];
                    $data['company_name'] =  $invDetails['company_name'];
                    $data['tax1_declaration'] =  $invDetails['tax1_declaration'];
					
                    $data['sub_total_amount'] = number_format($invDetails['sub_total_amount'],2);
                    $data['tax1_id'] 	 =  $invDetails['tax1_id'];
                    $data['tax1_number'] =  $invDetails['tax1_number'];
                    $data['tax1_name']   =  $invDetails['tax1_name'];
                    $data['tax1_name']   =  $invDetails['tax1_name'];
                    $data['tax1_value']  =  $invDetails['tax1_value'];
                    $data['tax1_pvalue'] =  $invDetails['tax1_pvalue'];
                    $data['tax1_total_amount']  = number_format($invDetails['tax1_total_amount'],2);
					
					$data['tax1_sub1_id'] =  $invDetails['tax1_sub1_id'];
                    $data['tax1_sub1_number'] =  $invDetails['tax1_sub1_number'];
                    $data['tax1_sub1_name'] =  $invDetails['tax1_sub1_name'];
                    $data['tax1_sub1_value'] =  $invDetails['tax1_sub1_value'];
                    $data['tax1_sub1_pvalue'] =  $invDetails['tax1_sub1_pvalue'];
                    $data['tax1_sub1_total_amount'] = number_format($invDetails['tax1_sub1_total_amount'],2);
					
					$data['tax1_sub2_id'] =  $invDetails['tax1_sub2_id'];
                    $data['tax1_sub2_number'] =  $invDetails['tax1_sub2_number'];
                    $data['tax1_sub2_name'] =  $invDetails['tax1_sub2_name'];
                    $data['tax1_sub2_value'] =  $invDetails['tax1_sub2_value'];
                    $data['tax1_sub2_pvalue'] =  $invDetails['tax1_sub2_pvalue'];
                    $data['tax1_sub2_total_amount'] = number_format($invDetails['tax1_sub2_total_amount'],2);
					
					$data['delivery_at'] =  $invDetails['delivery_at'];
					$data['pan_no'] =  $invDetails['pan_no'];
					
                  
                    $data['do_tax'] =  $invDetails['do_tax'];	
				    $data['do_st'] =  $invDetails['do_st'];	
				    if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
						$data['do_tax']= strtotime($data['do_tax']);				
					}
					if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){					    
					   $data['tin_no'] =  $invDetails['tin_no'];
					   $data['cst_no'] =  $invDetails['cst_no'];
					   $data['vat_no'] =  $invDetails['vat_no'];
					}
					$data['service_tax_regn'] =  '';
					if( $data['do_st']!=0 &&  $data['do_st']!='0000:00:00 00:00:00'){
						$data['do_st']= strtotime($data['do_st']);				
					}
					if($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00'){					    
					   
					   $data['service_tax_regn'] =  $invDetails['service_tax_regn'];
					  
					}
                }
				$data['invoice_title']=' PROPOSAL ';
                //Get billing address BOF
                 include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                $region         = new RegionLead();
                $region->setAddressOf(TABLE_SALE_LEADS, $data['client']['user_id']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
                //$data['b_address'] = $address_list ;
                //Get billing address EOF
                
                $data['do_i_chk'] = $data['do_i'];
				$data['do_rs_symbol'] = strtotime('2010-07-20');
                $data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
                $data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
                if(!empty($data['do_e'])){
                    $data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                }
                if(!empty($data['do_fe'])){
                    $data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }                       
                
                $query  = " UPDATE ".TABLE_LEADS_QUOTATION
                    ." SET "
					."". TABLE_LEADS_QUOTATION .".remarks = '".$data['remarks'] ."'"    
					.",". TABLE_LEADS_QUOTATION .".quotation_subject = '".  $data['quotation_subject'] ."'"					
					.",". TABLE_LEADS_QUOTATION .".other_details = '".  $data['other_details'] ."'"					
					.",". TABLE_LEADS_QUOTATION .".exchange_rate = '". $data['exchange_rate'] ."'"
					.",". TABLE_LEADS_QUOTATION .".amount = '".        $data['amount'] ."'"
					.",". TABLE_LEADS_QUOTATION .".amount_inr = '".    $data['amount_inr'] ."'"
					.",". TABLE_LEADS_QUOTATION .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
					.",". TABLE_LEADS_QUOTATION .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
					.",". TABLE_LEADS_QUOTATION .".do_e = '".          $data['do_e'] ."'" //date of expiry
					.",". TABLE_LEADS_QUOTATION .".do_fe = '".         $data['do_fe']."'" //date of expiry from
					//.",". TABLE_LEADS_QUOTATION .".access_level = '".  $data['access_level'] ."'"
					.",". TABLE_LEADS_QUOTATION .".remarks = '".       $data['remarks'] ."'"
					.",". TABLE_LEADS_QUOTATION .".balance = '".       $data['balance'] ."'"
					.",". TABLE_LEADS_QUOTATION .".balance_inr = '".   $data['amount_inr'] ."'"
					.",". TABLE_LEADS_QUOTATION .".amount_words = '".   $data['amount_words'] ."'"
					.",". TABLE_LEADS_QUOTATION .".billing_name = '".processUserData($data['billing_name']) ."'"
					.",". TABLE_LEADS_QUOTATION .".billing_address='". processUserData($data['billing_address']) ."'"
					.",". TABLE_LEADS_QUOTATION .".b_addr_company_name = '".processUserData($data['b_addr_company_name'])."'"
					.",". TABLE_LEADS_QUOTATION .".b_addr          = '".processUserData($data['b_addr']) ."'"
					.",". TABLE_LEADS_QUOTATION .".b_addr_city     = '".processUserData($data['b_addr_city'] )."'"
					.",". TABLE_LEADS_QUOTATION .".b_addr_state    = '".processUserData( $data['b_addr_state']) ."'"
					.",". TABLE_LEADS_QUOTATION .".b_addr_country  = '".processUserData($data['b_addr_country']) ."'"
					.",". TABLE_LEADS_QUOTATION .".b_addr_zip      = '". $data['b_addr_zip'] ."'"
					.",". TABLE_LEADS_QUOTATION .".status ='". $data['status'] ."'"
					." WHERE ".TABLE_LEADS_QUOTATION.".number ='".$data['number']."'";
               
               if ( $db->query($query) ) {
                    $messages->setOkMessage('The Proposal "'. $data['number'] .'" has been Updated.');
                    
                    $data['amount']= number_format($data['amount'],2);
                    $data['amount_inr']= number_format($data['amount_inr'],2);
                    $data['balance']= number_format($data['balance'],2);
                    $data['amount_words']=processSqlData($data['amount_words']);
                    
					$data['other_details'] = processSqlData($data['other_details']);
					
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_LEADS_ORD_P.".ord_no = '". $data['or_no'] ."'";
                    LeadsOrder::getParticulars($db, $temp, '*', $condition_query);
                    $data['show_discount']=0;
                    $data['show_nos']=0;
                    $data['colsSpanNo1']=5;
                    $data['colsSpanNo2']=6; 
                    if(!empty($temp)){
                        /*
						$data['wd1']=10;
                        $data['wdinvp']=217;
                        $data['wdpri']=70;
                        $data['wdnos']=70;
                        $data['wdamt']=70;
                        $data['wddisc']=50;
                        $data['wdst']=70;
                        //$data['wdtx']=40; as three columns are removed so manage its width in other cols
                       // $data['wdtxp']=50;
                        //$data['wdtotam']=75;
                        $data['wd2']=9;*/
						
						$data['wd1']=10;
                        $data['wdinvp']=245;//217+28
                        $data['wdpri']=98;//70+28
                        $data['wdnos']=98;//70+28
                        $data['wdamt']=97;//70+27
                        $data['wddisc']=77; //50+27
                        $data['wdst']=97;   //70+27                     
                        $data['wd2']=9;
                    
                        foreach ( $temp as $pKey => $parti ) {
                            /*
                               if($temp[$pKey]['s_type']=='1'){
                                    $temp[$pKey]['s_type']='Years';
                               }elseif($temp[$pKey]['s_type']=='2'){
                                    $temp[$pKey]['s_type']='Quantity';
                               }
                           */
                            if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
                                $data['show_discount']=1;
                                $show_discountNo=1;
                            }
                            if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
                                $data['show_nos']=1;
                                $show_nos=1;
                            }
                            $temp_p[]=array(
                                'p_id'=>$temp[$pKey]['id'],
                                'particulars'=>$temp[$pKey]['particulars'],
                                'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
                                's_type' =>$temp[$pKey]['s_type'] ,                                   
                                's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
                                's_id' =>$temp[$pKey]['s_id'] ,                                   
                                'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
                                'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,   
								'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,                                   
                                'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                   
                                'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
                                'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,                                   
                                'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,
								'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,                                   
                                'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                   
                                'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
                                'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,                                   
                                'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,                           
								'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
                                'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
                                'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
                                'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                                );
                        }
                         if($data['show_discount'] ==0){ 
                            // disc : 50 & subtot : 70
                           /* $data['wdinvp']= $data['wdinvp']+20 + 40;
                            $data['wdpri'] = $data['wdpri'] + 5 + 6;
                            $data['wdamt'] = $data['wdamt'] + 5+ 6;                            
                            $data['wdtx']  = $data['wdtx'] + 5+ 6;
                            $data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            $data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							*/
							// disc : 77 & subtot : 97
							$data['wdinvp']= $data['wdinvp']+50 + 70;
                            $data['wdpri'] = $data['wdpri'] +10 + 10;
                            $data['wdamt'] = $data['wdamt'] + 17+ 17;                            
                            //$data['wdtx']  = $data['wdtx'] + 5+ 6;
                            //$data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            //$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
                            
                        }
                         if($data['show_nos'] ==0){
                            // disc : 70
                           /* $data['wdinvp']= $data['wdinvp']+30;
                            $data['wdpri'] = $data['wdpri'] + 5;
                            $data['wdamt'] = $data['wdamt'] + 5;
                            $data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;
							*/
							  // disc :98
							$data['wdinvp']= $data['wdinvp']+80;
                            $data['wdpri'] = $data['wdpri'] + 8;
                            $data['wdamt'] = $data['wdamt'] + 10;
                            /*
							$data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;*/
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;
                            
                        }       
                    }
					
					$data['mkex_fname']   = '';
					$data['mkex_lname']   = '';
					$data['mkex_designation'] ='';
					$data['mkex_mobile1']   ='';					
					$data['mkex_email']   ='';					
                    if(!empty($data['order_closed_by'])){
						$clientfields='';
						$condition = TABLE_USER." WHERE  user_id='".$data['order_closed_by']."'";
						$clientfields .= ' '.TABLE_USER.'.marketing_contact, '. TABLE_USER .'.desig,  '. TABLE_USER .'.email,
						'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
						User::getList($db, $closed_by, $clientfields, $condition) ;
						$data['mkex_fname']   	= $closed_by['0']['f_name'];
						$data['mkex_lname']   	= $closed_by['0']['l_name'];
						$data['mkex_designation']   = $closed_by['0']['desig'];
						$data['mkex_mobile1']   = $closed_by['0']['marketing_contact'];
						$data['mkex_email']   = $closed_by['0']['email'];
					}
					
                    $data['particulars']=$temp_p ;
                    //get Proposal details bof
                    
                    // Create the Proposal PDF, HTML in file.
                    $extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch =$attch1= '';
                    /* 
					if ( !($attch1 = LeadsQuotation::createFile($data, 'HTMLPRINT', $extra)) ) {
                        $messages->setErrorMessage("The Proposal print html  file was not created.");
                    } 
					*/
                    if ( !($attch = LeadsQuotation::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Proposal file was not created.");
                    }
                    LeadsQuotation::createPdfFile($data); 
                    $file_name = DIR_FS_QUOTATIONS_PDF ."/". $data["number"] .".pdf";
					
                }
            }
        }
        else {
      
           
        }
        
        if(isset($_POST['btnReturnRcp']) && $messages->getErrorMessageCount() <= 0 ){
        
           //header("Location:".DIR_WS_NC."/bill-receipt.php?perform=add&inv_id=".$inv_id."&inv_no=".$data['number']);           
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $inv_id;
			$condition_query='';
			$searchStr=1;
            include ( DIR_FS_NC .'/leads-quotation-list.php');
        }
        else {
        
            if ( !empty($or_no) ) {
         
                // Read the Order details.
                include_once ( DIR_FS_INCLUDES .'/leads-order.inc.php');
                $order  = NULL;
                 
                $fields = TABLE_LEADS_ORDERS.'.id,'.TABLE_LEADS_ORDERS .'.do_o AS do_o,'.TABLE_LEADS_ORDERS .'.number AS or_no,'.
				TABLE_LEADS_ORDERS .'.access_level, '. TABLE_LEADS_ORDERS .'.status, '.TABLE_LEADS_ORDERS.'.details';
                $fields .= ', '.TABLE_LEADS_ORDERS.'.order_title, '. TABLE_LEADS_ORDERS .'.order_closed_by';
                $fields .= ', '.TABLE_LEADS_ORDERS.'.is_renewable ';
                $fields .= ', '.TABLE_LEADS_ORDERS.'.currency_id, '. TABLE_LEADS_ORDERS .'.company_id  
				,'.TABLE_LEADS_ORDERS .'.amount' ;
                $fields .= ', '.TABLE_SALE_LEADS.'.lead_id as user_id, '. TABLE_SALE_LEADS .'.number, 
				'.TABLE_SALE_LEADS.'.f_name, '.TABLE_SALE_LEADS.'.l_name';
                $fields .= ', '.TABLE_LEADS_ORDERS.'.client, '. TABLE_USER .'.number AS u_number, 
				'.TABLE_USER.'.f_name AS u_f_name, '.TABLE_USER.'.l_name AS u_l_name ' ;
                
				if ( LeadsOrder::getDetails($db, $order, $fields, 
					" WHERE ".TABLE_LEADS_ORDERS .".number = '". $or_no ."' ") > 0 ) {
                    $order = $order[0];
                        if ( $access_level >= $order['access_level'] ) {
                            /*
							$_ALL_POST['or_no']             = $order['or_no'];                            
                            $_ALL_POST['or_details']        = $order['details'];
                            $_ALL_POST['or_user_id']        = $order['user_id'];
                            $_ALL_POST['or_number']         = $order['number'];
                            $_ALL_POST['or_f_name']         = $order['f_name'];
                            $_ALL_POST['or_l_name']         = $order['l_name'];
                            $_ALL_POST['or_access_level']   = $order['access_level'];
							*/                            
							$_ALL_POST['or_no']             = $order['or_no'];
                           // $_ALL_POST['or_particulars']    = $order['particulars'];
                            $_ALL_POST['or_details']        = $order['details'];
                            $_ALL_POST['or_user_id']        = $order['user_id'];
                            $_ALL_POST['or_number']         = $order['number'];
                            $_ALL_POST['or_f_name']         = $order['f_name'];
                            $_ALL_POST['or_l_name']         = $order['l_name'];
                            $_ALL_POST['or_access_level']   = $order['access_level'];
                            $_ALL_POST['do_o']              = $order['do_o'];
                            $_ALL_POST['u_f_name']          = $order['u_f_name'];
                            $_ALL_POST['u_l_name']          = $order['u_l_name'];
                            $_ALL_POST['u_number']          = $order['u_number'];
                            $_ALL_POST['order_title']       = $order['order_title'];
                            $_ALL_POST['order_closed_by']   = $order['order_closed_by'];
                            $_ALL_POST['is_renewable']   = $order['is_renewable'];
                            
                            if(!empty($order['order_closed_by'])){
                                
                                $condition = TABLE_USER." WHERE  user_id='".$order['order_closed_by']."'";
                                $clientfields = ' '.TABLE_USER.'.user_id, '. TABLE_USER .'.number, 
								'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
                                User::getList($db, $closed_by, $clientfields, $condition) ;
                                $_ALL_POST['order_closed_by_name']   = $closed_by['0'];
                            }
                            $_ALL_POST['amount']= $order['amount'];
                            $_ALL_POST['gramount']= number_format($order['amount'],2);
                            $_ALL_POST['amount_inr']= $order['amount'] * $_ALL_POST['exchange_rate'];
                            $_ALL_POST['currency_id']= $order['currency_id'];
                            $_ALL_POST['company_id']= $order['company_id'];
                           
                           /*get particulars details bof*/
                            $temp = NULL;
                            $temp_p = NULL;
                            $condition_query = "WHERE ord_no = '". $order['or_no'] ."'";
                            LeadsOrder::getParticulars($db, $temp, '*', $condition_query);
                            if(!empty($temp)){
                                foreach ( $temp as $pKey => $parti ) {
									/*
                                    if($temp[$pKey]['s_type']=='1'){
                                        $temp[$pKey]['s_type']='Years';
                                    }elseif($temp[$pKey]['s_type']=='2'){
                                        $temp[$pKey]['s_type']='Quantity';
                                    }*/
									$temp_p[]=array(
									'p_id'=>$temp[$pKey]['id'],
									'particulars'=>$temp[$pKey]['particulars'],
									'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
									's_id' =>$temp[$pKey]['s_id'] ,                                   
									's_type' =>$temp[$pKey]['s_type'] ,                                   
									's_quantity' =>$temp[$pKey]['s_quantity'] , 
									's_amount' =>number_format($temp[$pKey]['s_amount'],2),
									'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
									'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
									'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
									'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
									'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
									'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,    
									'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,
									'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,                                   
									'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                   
									'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
									'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,    
									'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,   
									'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,                                   
									'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                   
									'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
									'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,    
									'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,
									'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
									'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                
									'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                 
									'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
									'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)
									);
                                    
                                }
                            }
                            $_ALL_POST['particulars']=$temp_p ;
                           
                            $order = NULL;
                            // Read the Client Addresses.
                            include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                            $region         = new RegionLead();
                            $region->setAddressOf(TABLE_SALE_LEADS, $_ALL_POST['or_user_id']);
                            $_ALL_POST['address_list']  = $region->get();                           
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to edit proposal for this Lead Order.");
                            $_ALL_POST = '';
                        }
                        
                 
                }
                else {
                    $messages->setErrorMessage("The Selected Lead Order was not found.");
                }
            }

            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['or_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['or_access_level'], $al_list, $index);
                $_ALL_POST['or_access_level'] = $al_list[$index[0]]['title'];
            }
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'inv_id', 'value' => $inv_id);
            $hidden[] = array('name'=> 'or_no', 'value' => $or_no);
            $hidden[] = array('name'=> 'is_renewable', 'value' => $_ALL_POST['is_renewable']);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
            
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            //$page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            //$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-quotation-edit.html');
			
        }
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>