<?php

    if ( $perm->has('nc_sl_ld_qe_list') ) {
	
		include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
		//include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
        $and_added = false ;
        if ( !isset($condition_query) || $condition_query == '' ) {
         // $condition_query = ' AND (';
          // $where_added = true;
        }else{
            //$condition_query .= ' AND (';
            //$and_added = true;
        }
        
        $access_level   = $my['access_level'];       
        /*        
        // If the User has the Right to View Leads of the same Access Level.
        if ( $perm->has('nc_sl_ld_qe_list_al') ) {
            $access_level += 1;
        }
        // If my is the Manager of this Client.
        //$condition_query .= " ("." AND ". TABLE_CLIENTS .".access_level < $access_level ) ";
        */
        
        // Check if the User has the Right to view Leads of other Executive.
        if ( $perm->has('nc_sl_ld_qe_list_ot') ) {            
            $access_level_o   = $my['access_level'];
            $access_level_o += 1;
            $condition_query .= " AND " ;
            if ( $perm->has('nc_sl_ld_qe_list_my') ) { 
                 $condition_query .= " ( ";
            }

            $condition_query .= "  ( ". TABLE_SALE_LEADS .".access_level < $access_level_o ) ";
        }
        
        
        
        if ( $perm->has('nc_sl_ld_qe_list_my') ) {
              if ( $perm->has('nc_sl_ld_qe_list_ot') ) { 
                 $condition_query .= " OR ";
              }else{
                 $condition_query .= " AND ";
              }

            $condition_query .= "  ( ". TABLE_SALE_LEADS .".created_by = '". $my['user_id'] ."' "." ) ";
            if ( $perm->has('nc_sl_ld_qe_list_ot') ) { 
                  $condition_query .= ' ) ';
            }
        }
       
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        $condition_query = " LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id=".TABLE_SALE_LEADS.".created_by
        WHERE lead_status= '".Quick::NEWONE."' AND lead_assign_to='' AND lead_assign_to_mr='' ".$condition_query;
        
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	Quick::getList( $db, $list, '', $condition_query);
    
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','sale-quick.php','frmSearch');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
		$fields ='';
        $fields = TABLE_SALE_LEADS.".lead_id, ".TABLE_SALE_LEADS.".company_name,".TABLE_SALE_LEADS.".f_name, 
		".TABLE_SALE_LEADS.".m_name,".TABLE_SALE_LEADS.".l_name," ;
        $fields .= TABLE_SALE_LEADS.".status,";
		 $fields .= TABLE_SALE_LEADS.".email, ". TABLE_SALE_LEADS.".email_1, ".TABLE_SALE_LEADS.".email_2, "
		.TABLE_SALE_LEADS.".mobile1,". TABLE_SALE_LEADS.".mobile2, ".TABLE_SALE_LEADS.".mobile3,".TABLE_SALE_LEADS.".lead_status, 
		".TABLE_SALE_LEADS.".created_by, " ;
        $fields .= TABLE_USER.".f_name as added_by_fname, ". TABLE_USER.".l_name as added_by_lname ";
        Quick::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
		
		/*$phonelead  = new PhoneLead(TABLE_SALE_LEADS);
		for ( $i=0; $i<$total; $i++ ) {
			$phonelead->setPhoneOf(TABLE_SALE_LEADS, $list[$i]['lead_id']);
        	$list[$i]['phone'] = $phonelead->get($db);    	
		}*/
		$regionlead     = new RegionLead('91');
        $phonelead      = new PhoneLead(TABLE_SALE_LEADS);
       
		$leadStatusArr =Quick::getLeadStatus();
    	$leadStatusArr  = array_flip($leadStatusArr );
  
		$fList=array();
		if(!empty($list)){
			foreach( $list as $key=>$val){      
				
				/*$exeCreted=$exeCreatedname='';
				$table = TABLE_AUTH_USER;
				$condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by'] ."' " ;
				$fields1 =  TABLE_USER .'.f_name'.','. TABLE_AUTH_USER .'.l_name';
				$exeCreted= getRecord($table,$fields1,$condition2);
			
				if(!empty($exeCreted)){
				   
				   $exeCreatedname = $exeCreted['f_name']." ".$exeCreted['l_name']."<br/>" ;
				}
				$val['created_by_name']    = $exeCreatedname ;     */
				$phonelead->setPhoneOf(TABLE_SALE_LEADS, $val['lead_id']);
				$val['phone'] = $phonelead->get($db);
				
				// Read the Addresses.
				$regionlead->setAddressOf(TABLE_SALE_LEADS, $val['lead_id']);
				$val['address_list'] = $regionlead->get();
				$leadstatus = $val['lead_status'];
				$val['lead_status_name']    = $leadStatusArr[$leadstatus] ;     
			   
			   $fList[$key]=$val;
			}
		}

        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_sl_ld_qe_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_sl_ld_qe_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_sl_ld_qe_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_sl_ld_qe_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_sl_ld_qe_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_sl_ld_qe_status') ) {
            $variables['can_change_status']     = true;
        }
        
        $variables['lead_status']=Quick::NEWONE;
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-quick-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>