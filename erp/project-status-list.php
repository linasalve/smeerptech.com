<?php
if ( $perm->has('nc_pts_sta_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    
   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	ProjectStatus::getDetails( $db, $list, '', $condition_query);

   
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_PROJECT_TASK_STATUS.'.*' ;
    ProjectStatus::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    // Set the Permissions.
   
    if ( $perm->has('nc_pts_sta_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_pts_sta_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_pts_sta_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_pts_sta_delete') ) {
        $variables['can_delete'] = true;
    }
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-status-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
