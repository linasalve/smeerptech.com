<?php

    if ( $perm->has('nc_st_uc_list') ) {
        
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        
        $region         = new Region();
        $phone          = new Phone(TABLE_CLIENTS);
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
        }
        else {
            $condition_query .= ' AND (';
        }
        
        $access_level   = $my['access_level'];  
        /*        
        // If the User has the Right to View Clients of the same Access Level.
        if ( $perm->has('nc_uc_list_al') ) {
            $access_level += 1;
        }
        // If my is the Manager of this Client.
        $condition_query .= " (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."' "
                                ." AND ". TABLE_CLIENTS .".access_level < $access_level ) ";
        
        // Check if the User has the Right to view Clients of other Executives.
        if ( $perm->has('nc_uc_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_uc_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_CLIENTS. ".manager != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_CLIENTS .".access_level < $access_level_o ) ";
        }
        $condition_query .= ')';

        $condition_query .= " AND parent_id = '' AND service_id LIKE '%". Clients::ST ."%'";
        */
         //show the list of clients of same access levels
        if ( $perm->has('nc_uc_list_all') ) { 
            $condition_query .= "  parent_id = '' )"; 
        }else{      
            $condition_query .= "".TABLE_CLIENTS .".access_level <= $access_level ";    
            $condition_query .= ')';
            $condition_query .= " AND parent_id = '' ";            
        }
        //$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        $condition_query .= " ORDER BY do_reg DESC ";
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;        
        $pagination   = '';
        $extra_url  = '';
        if($searchStr==1){
            $total	=	Clients::getList( $db, $list, '', $condition_query);    
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
          
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
             }
            $condition_url .="&perform=".$perform;
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
    
            $list	= NULL;
            Clients::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
        }
        /*$phone  = new Phone(TABLE_CLIENTS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_CLIENTS, $list[$i]['user_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}*/
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               $client=array();
               $string = str_replace(",","','", $val['alloted_clients']);
               $clientname='';
               if(!empty($string)){
                   $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name';
                   $condition1 = " WHERE ".TABLE_CLIENTS .".user_id IN('".$string."') " ;
                   Clients::getList($db,$client,$fields1,$condition1);                   
                   if(!empty($client)){
                       foreach($client as $key1=>$val1){
                            $clientname .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
                       } 
                   }
               }
               $val['alloted_clients'] = $clientname ;
               
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_CLIENTS, $val['user_id']);
                $val['phone'] = $phone->get($db);
                
                // Read the Addresses.
                $region->setAddressOf(TABLE_CLIENTS, $val['user_id']);
                $val['address_list'] = $region->get();
                
                
               $fList[$key]=$val;
            }
        }
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_view_sub_user'] = false;
        $variables['can_view_client_details'] = false;
        if ( $perm->has('nc_uc_contact_details') ) {           
            $variables['can_view_client_details'] = true;
        }
        if ( $perm->has('nc_uc_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_uc_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_uc_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_uc_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_uc_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_uc_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_uc_su_list') ) {
            $variables['can_view_sub_user'] = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-st-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>