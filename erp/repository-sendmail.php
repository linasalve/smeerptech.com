<?php
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	include_once ( DIR_FS_INCLUDES .'/repository.inc.php' );
	include ( DIR_FS_INCLUDES .'/support-functions.inc.php'); 
	
	$id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :''); 
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
   
	
if ( $perm->has('nc_rps_sendmail') ) {

	if (isset($id) && $id > 0 ){
	
		$_ALL_POST 	= $_POST;
		$data		= processUserData($_ALL_POST);
		$extra = array( 'db'           => &$db ,
						'messages'     => &$messages		
                        );
						
						
		if ( Repository::validateSendmail($data, $extra) ) {
			$fields = "*";
			$condition_query = "" ;
			$condition_query = " WHERE (". TABLE_REPOSITORY .".id = '". $id ."')";
			if ( Repository::getDetails($db, $_ALL_Data, $fields, $condition_query) > 0 ) {
				$_ALL_Data =  $_ALL_Data['0'];
				$data['subject']=$_ALL_Data['subject'];
				$data['text']=$_ALL_Data['text'];
				$file_name=array();
				if($data['type']=='email'){
					if(!empty($_ALL_Data["file_1"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["file_1"];
					}
					if(!empty($_ALL_Data["file_2"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["file_2"];
					}
					if(!empty($_ALL_Data["file_3"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["file_3"];
					}
					if(!empty($_ALL_Data["file_4"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["file_4"];
					}
					if(!empty($_ALL_Data["file_5"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["file_5"];
					}
                }
				if($data['type']=='print'){
					if(!empty($_ALL_Data["print_file_1"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["print_file_1"];
					}
					if(!empty($_ALL_Data["print_file_2"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["print_file_2"];
					}
					if(!empty($_ALL_Data["print_file_3"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["print_file_3"];
					}
					if(!empty($_ALL_Data["print_file_4"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["print_file_4"];
					}
					if(!empty($_ALL_Data["print_file_5"])){
						$file_name[] = DIR_FS_REPOSITORY_FILES ."/". $_ALL_Data["print_file_5"];
					}
                }
				$data['count_file'] = count($file_name);
				$data['display_name']=$data['display_user_id']=$data['display_designation']='';
				$randomUser = getRandomAssociate($data['executive_id']);
				$data['display_name'] = $randomUser['name'];
				$data['display_user_id'] = $randomUser['user_id'];
				$data['display_designation'] = $randomUser['desig'];		
                if ( getParsedEmail($db, $s, 'EMAIL_REPOSITORY', $data, $email) ) {        
                    $to =$cc_to_sender=$cc=$bcc= '';
                    $to[]   = array('name' => $data['email'], 'email' => $data['email']);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                   
					$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
							'email' => $smeerp_client_email);      
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
					//echo $email["body"];
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Mail send Successfully on email ".$data['email']."</li>
							</ul>
						</td>
					</tr>
					</table>";
					
					$message .= "|".$id;
										
				}else{
					$message = " <table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
						<tr>
							<td class=\"message_error_header\" height=\"10px\">
								<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\"  border=\"0\">&nbsp;
								Not Success
							</td>
						</tr>
						<tr>
							<td class=\"message_error\" align=\"left\" valign=\"top\">
								<ul class=\"message_error\">				
									<li class=\"message_error\">Mail not send Successfully. </li>				
								</ul>
							</td>
						</tr>
					</table>";
					$message .= "|".$id;
				}
			}
		}else{
			$error_messages=array();
			$error_messages=$messages->getErrorMessages();
			if(!empty($error_messages)){

			    $message = " <table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
						<tr>
							<td class=\"message_error_header\" height=\"10px\">
								<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\"  border=\"0\">&nbsp;
								Not Success
							</td>
						</tr> 
						<tr>
							<td class=\"message_error\" align=\"left\" valign=\"top\">
								<ul class=\"message_error\">" ;
						$errmessage='';
						foreach($error_messages as $item=>$error){
							$errmessage.="<li class=\"message_error\">".$error."</li>";
						}						
				$message = $message.$errmessage."</ul>
							</td>
						</tr>
					</table>";
			}
					$message .= "|".$id;
		
		
		}
	}
}else{

	$message = " <table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
				<tr>
					<td class=\"message_error_header\" height=\"10px\">
						<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\"  border=\"0\">&nbsp;
						Not Success
					</td>
				</tr>
				<tr>
					<td class=\"message_error\" align=\"left\" valign=\"top\">
						<ul class=\"message_error\">				
							<li class=\"message_error\">You donot have the Permisson to Access this module.</li>				
						</ul>
					</td>
				</tr>
			</table>";
			$message .= "|0";

}
echo $message ;

    exit;
?>