<?php
	$_ALL_POST      = NULL;
	$data           = NULL; 
	
	if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST 	= $_POST;
		$data		= processUserData($_ALL_POST);
				   
		$extra = array( 'db' 				=> &$db,
						'access_level'      => $access_level,
						'messages'          => &$messages
					);
			 
		   
		if ( NewslettersCategory::validateAdd($data, $extra) ) { 
				 
				$query	= " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY
					." SET ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".category = '".$data['category'] ."'"  
						                           
						.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".client = '".$data['client'] ."'"                            
						.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".client_details = '".$data['client_details'] ."'"   
						.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".status = '".$data['status'] ."'"                          
						//.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".created_by = '".	$my['uid'] ."'"                                
						//.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".created_by_name = '".$my['f_name']." ".$my['l_name']."'"                                
						//.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".ip = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
						.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".do_e = '". 		date('Y-m-d')."'";
			
			if ( $db->query($query) && $db->affected_rows() > 0 ) {
				$messages->setOkMessage("New record has been done.");
				$variables['hid'] = $db->last_inserted_id();              
			}
			//to flush the data.
			$_ALL_POST	= NULL;
			$data		= NULL;
		}
	}               
	
	
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
		 header("Location:".DIR_WS_NC."/newsletters-category.php?perform=add&added=1");
	}
	if(isset($_POST['btnCancel'])){
		 header("Location:".DIR_WS_NC."/newsletters-category.php");
	}
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
		header("Location:".DIR_WS_NC."/newsletters-category.php?added=1");   
	}
	else {
	
		$hidden[] = array('name'=> 'perform' ,'value' => 'add');
		$hidden[] = array('name'=> 'act' , 'value' => 'save');           
		
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletters-category-add.html');
	}
?>