<?php
if ( $perm->has('nc_pr_ts_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        		        
		// Include the 
		include_once (DIR_FS_INCLUDES .'/purchase-task.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-module.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-priority.inc.php');
		
		include_once (DIR_FS_INCLUDES .'/project-resolution.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-type.inc.php');
		include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
        
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=150;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        }   
       
		$_ALL_POST 			=  ( isset($_POST)    ? processUserData($_POST)       :'');
       
        //Get all req array BOF     
      
        $moduleArr	= NULL;
        /* As we changed the logic behind order based modules to Global Modules
			We are fetching the new modules ie module id > 871; Till 871 all modules are Order based thats why we are 
			implementing this logic
		*/
		//$condition_querym = " WHERE parent_id='0' AND status ='".PurchaseTask::ACTIVE."' AND order_id='".$or_id."'";
        $condition_querym = " WHERE parent_id='0' AND ".TABLE_PROJECT_TASK_MODULE.".id > 871 
		AND ".TABLE_PROJECT_TASK_MODULE.".status = '".PurchaseTask::ACTIVE."' ";
        ProjectModule::getList( $db, $moduleArr, 'id, title', $condition_querym);
      
        $priorityArr	= NULL;
        $condition_queryp = " WHERE  status ='".PurchaseTask::ACTIVE."'";
        ProjectPriority::getList( $db, $priorityArr, 'id, title', $condition_queryp);
        
        $resolutionArr	= NULL;
        $condition_queryr = " WHERE status ='".PurchaseTask::ACTIVE."'";
        ProjectResolution::getList( $db, $resolutionArr, 'id, title', $condition_queryr);
        
        $typeArr	= NULL;
        $condition_queryt = " WHERE status ='".PurchaseTask::ACTIVE."'";
        ProjectType::getList( $db, $typeArr, 'id, title', $condition_queryt);
        //Get all req array EOF
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files		= processUserData($_FILES);
          
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'files'          => &$files,
							'messages'          => &$messages
                        );
            if(!empty($data['module_id'])){        
                $lst_submodule=null;
                $fields = TABLE_PROJECT_TASK_MODULE .'.id'
                           .','. TABLE_PROJECT_TASK_MODULE .'.title';
                $condition_query= " WHERE status='".ProjectModule::ACTIVE."' AND parent_id='".$data['module_id']."' ORDER BY ".TABLE_PROJECT_TASK_MODULE.".id ASC";
                ProjectModule::getList($db,$lst_submodule,$fields,$condition_query);        
            }
                
            $data['allowed_file_types'] = $allowed_file_types ;
            //$data['max_file_size']= MAX_MULTIPLE_FILE_SIZE ;       
            $data['max_file_size']= MAX_FILE_SIZE ;       
            
            if ( PurchaseTask::validateAdd($data, $extra) ) {
                $is_visibleto_client = 0;
                
                if(isset($data['is_visibleto_client'])){
                    $is_visibleto_client = 1;
                }
              
                $i=1;
               
                $attachfilename='';
                if(!empty($files["attached_file"]["name"])){
                   
                    //$val["name"] = str_replace(' ','-',$val["name"]);
                    $filedata["extension"]='';
                    $filedata = pathinfo($files["attached_file"]["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;                   
                    $attachfilename = mktime()."-".$i.".".$ext ;
                  
                   if(!empty($files["attached_file"]["name"])){                 
                        if (move_uploaded_file($files["attached_file"]['tmp_name'], DIR_FS_PRATTACHMENTS_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_PRATTACHMENTS_FILES."/".$attachfilename, 0777);
                            $attached_type = $files["attached_file"]["type"] ;
                            
                        }
                    }
                    $i++;
                }
                
               $hrs1 = (int) ($data['hrs'] * 3);
               $min1 = (int) ($data['min'] * 3);
               $last_updated = $datetime->unixTimeToDate(time()); 
               $resolved_date='';
               if(!empty($data['resolved_date'])){
                    $temp = explode('/', $data['resolved_date']);                               
                    $time =  mktime(0, 0, 0, $temp[1], $temp[0], $temp[2]);        
                    $resolved_date = $datetime->unixTimeToDate($time); 
               }
               //Mark send notify BOF                        
                $mail_client=0;
                if(isset($_POST['mail_client']) ){
                    $mail_client=1;
                }
                $mail_staff=0;
                if(isset($_POST['mail_staff']) ){
                    $mail_staff=1;
                }
                $mark_imp=0;
                if(isset($_POST['mark_imp']) ){
                    $mark_imp=1;
                }
                //Mark send notify EOF
                $current_salary = $my['current_salary'];
				$per_hour_salary = $my['current_salary'] / STD_WORKING_HRS;
				$per_hour_salary =  number_format($per_hour_salary,2);
				$worked_min = ( $data['hrs']*60 ) + $data['min'] ;
				$worked_hour = ($worked_min/60 ) ;
				$worked_hour =  number_format($worked_hour,2);
				$worked_hour_salary = ( $per_hour_salary * $worked_hour );
				
				$query = "INSERT INTO ".TABLE_PURCHASE_TASK_DETAILS
						  ." SET "
						  ." project_id 	= '".$data['or_id']."',"
						  ." module_id	    = '".$data['module_id']."',"
						  ." sub_module_id	= '".$data['sub_module_id']."',"
						  ." title      	= '".$data['title']."',"	
						  ." details      	= '".$data['details']."',"	
						  ." status_id     	= '".$data['status_id']."',"	
						  ." priority_id   	= '".$data['priority_id']."',"
						  ." resolution_id  = '".$data['resolution_id']."',"
						  ." type_id        ='".$data['type_id']."',"
						  ." hrs            ='".$data['hrs']."',"
						  ." min  =         '".$data['min']."',"
						  ." current_salary  = '".$current_salary."',"
						  ." per_hour_salary  = '".$per_hour_salary."',"
						  ." worked_hour_salary  = '".$worked_hour_salary."',"
						  ." hrs1  =         '".$hrs1."',"
						  ." min1  =         '".$min1."',"
						  ." last_updated 	= '".$last_updated."',"
						  ." added_by      	= '".$my['user_id']."',"
						  ." added_by_name 	= '".$my['f_name']." ".$my['l_name']."',"
						  ." is_bug       	    = '0',"
						  ." attached_file	     = '".$attachfilename."',"
						  ." attached_type	= '".$attached_type."',"
						  ." attached_desc       = '".$data['attached_desc']."',"
						  ." is_visibleto_client = '".$is_visibleto_client."',"
						  ." mail_client = '".$mail_client."',"
						  ." mail_staff = '".$mail_staff."',"
						  ." mark_imp = '".$mark_imp."',"
						  ." resolved_date  = '".$resolved_date."',"
						  ." ip  	= '".$_SERVER['REMOTE_ADDR']."',"
						  ." do_e 	= '".date('Y-m-d H:i:s')."'" ;
                                          
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New Project Task entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();   
                    
                    //Send notification BOF    
                    $condition_query = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".id='".$variables['hid']."'" ;
                    $fields = TABLE_PURCHASE_TASK_DETAILS.".id, ".TABLE_PURCHASE_TASK_DETAILS.".title,".TABLE_PURCHASE_TASK_DETAILS.".added_by,".TABLE_PURCHASE_TASK_DETAILS.".details,".TABLE_PURCHASE_TASK_DETAILS.".last_updated,".TABLE_PROJECT_TASK_MODULE.".title as module,".TABLE_PROJECT_TASK_STATUS.".title as status," ;
                    $fields .= TABLE_PROJECT_TASK_PRIORITY.".title as priority, ".TABLE_PROJECT_TASK_TYPE.".title as type,".TABLE_PROJECT_TASK_RESOLUTION.".title as resolution" ;
                    PurchaseTask::getList( $db, $data, $fields, $condition_query);
                    if(!empty($data) ){      
                        $data = $data[0];                        
                  
                    }
                    $data['project_title'] =$order['order_title'];
                    $data['images_nc']=DIR_WS_IMAGES_NC;
                    $file_name = DIR_FS_PRATTACHMENTS_FILES ."/". $attachfilename;
                    $cc_to_sender= $cc = $bcc = Null;
					//get the subclients allotted to the order bof
					$ord_details=null;
					$fields = TABLE_PURCHASE_ORDER.'.id, '.TABLE_PURCHASE_ORDER.'.clients_su ' ;
					$condition_query_ord = " WHERE ".TABLE_PURCHASE_ORDER.".id = ".$or_id ;
					$total	=	PurchaseOrder::getDetails( $db, $ord_details, $fields, $condition_query_ord);
					$order_details =  $ord_details[0];
					$clients_su = $order_details['clients_su'] ;
					$clients_su_str='';
					if(!empty($clients_su)){
						$clients_su_str =  str_replace(",","','",$clients_su);
					}
					//get the subclients allotted to the order eof
					
					
                    // Send Email to the Client BOF                    
                    if(isset($_POST['mail_client']) ){
						$mail_send_to_su='';
                        $data['link'] = DIR_WS_MP .'/purchase-task.php?perform=details&id='.$variables['hid'].'&or_id='.$or_id;
                        
                        
                        $subUserDetails=null;
						if(!empty($clients_su_str)){
							//Clients::getList($db, $subUserDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE parent_id = '".$order['client']."' AND status='".Clients::ACTIVE."' AND user_id IN('".$clients_su_str."') ");                   
							Clients::getList($db, $subUserDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE status='".Clients::ACTIVE."' AND user_id IN('".$clients_su_str."') ");                   
                        }
    				    Clients::getList($db, $clientDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE user_id = '".$order['client']."'");
                        if(!empty($clientDetails)){
                            $client = $clientDetails['0'];            
                            $data['c_title'] =$client['title'];                            
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;
                        
                            $data['uf_name'] =$my['f_name'];
                            $data['ul_name'] = $my['l_name'] ;
                            $data['af_name'] =$my['f_name'];
                            $data['al_name'] = $my['l_name'] ;
                            $email = NULL;
                            if(isset($_POST['mark_imp']) ){
                                if ( getParsedEmail($db, $s, 'TASK_REPORT_CLIENT_IMP', $data, $email) ) {
                                    $to     = '';
                                    $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    $mail_send_to_su .= ",".$client['email'] ;
                                    if(!empty($client['email_1'])){
										$mail_send_to_su .=",".$client['email_1']; 
                                        $to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_1']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                    if(!empty($client['email_2'])){
										$mail_send_to_su .=",".$client['email_2']; 
                                        $to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_2']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                  
                                    //Send a copy to check the mail for clients
                                    if(!empty($smeerp_client_email)){
                                        $to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $smeerp_client_email);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }                                     
                                }                                
                                // send mail to sub users bof
                                if(!empty($subUserDetails)){                                        
                                    foreach ($subUserDetails  as $key=>$value){
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        $data['c_title'] =$subUserDetails[$key]['title'];                            
                                        $data['cf_name'] =$subUserDetails[$key]['f_name'];
                                        $data['cl_name'] = $subUserDetails[$key]['l_name'] ;                                    
                                        if ( getParsedEmail($db, $s, 'TASK_REPORT_CLIENT_IMP', $data, $email) ) {
                                            $mail_send_to_su .=",".$subUserDetails[$key]['email'];
											$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                        
										    if(!empty($subUserDetails[$key]['email_1'])){
												$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_1']);
                                            
												$mail_send_to_su .=",".$subUserDetails[$key]['email_1'];                                
												 $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
												SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name);
											}
											if(!empty($subUserDetails[$key]['email_2'])){
												$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_2']);
                                            	$mail_send_to_su .=",".$subUserDetails[$key]['email_2'];                                
												 $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
												SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name);
											}
										}
                                    }
                                }
                                // send mail to sub users bof
                            }else{
                                if ( getParsedEmail($db, $s, 'TASK_REPORT_CLIENT', $data, $email) ) {
                                    $mail_send_to_su .=",".$client['email']; 
									$to     = '';
                                    $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    
									if(!empty($client['email_1'])){
                                         $mail_send_to_su .=",".$client['email_1']; 
										$to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_1']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                    if(!empty($client['email_2'])){
                                        $mail_send_to_su .=",".$client['email_2']; 
										$to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_2']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                    //Send a copy to check the mail for clients
                                    if(!empty($smeerp_client_email)){
                                        $to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $smeerp_client_email);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }    
                                }
                                
                                // send mail to sub users bof
                                if(!empty($subUserDetails)){                                        
                                    foreach ($subUserDetails  as $key=>$value){
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        $data['c_title'] = $subUserDetails[$key]['title'];                            
                                        $data['cf_name'] = $subUserDetails[$key]['f_name'];
                                        $data['cl_name'] = $subUserDetails[$key]['l_name'];                                    
                                        if ( getParsedEmail($db, $s, 'TASK_REPORT_CLIENT', $data, $email) ) {
                                            $mail_send_to_su .=",".$subUserDetails[$key]['email'];
											$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                        }
										if(!empty($subUserDetails[$key]['email_1'])){
											$to = '';
											$to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_1']);
										
											$mail_send_to_su .=",".$subUserDetails[$key]['email_1'];                                
											 $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
											SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
											$cc_to_sender,$cc,$bcc,$file_name);
										}
										if(!empty($subUserDetails[$key]['email_2'])){
											$to = '';
											$to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_2']);
											$mail_send_to_su .=",".$subUserDetails[$key]['email_2'];                                
											 $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
											SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
											$cc_to_sender,$cc,$bcc,$file_name);
										}
                                    }
                                }
                                // send mail to sub users bof                                
                            }
                        }                       
                    }
                    // Send Email to the Client EOF         
					//Set the list subuser's email-id to whom the mails are sent bof   
					if(!empty($mail_send_to_su)){
						$query_update = "UPDATE ". TABLE_PURCHASE_TASK_DETAILS 
                                ." SET ". TABLE_PURCHASE_TASK_DETAILS .".mail_send_to_su  = '".trim($mail_send_to_su ,",")."' 
                                WHERE ". TABLE_PURCHASE_TASK_DETAILS .".id = ". $variables['hid'] ." " ;
						$db->query($query_update) ;
					}
										
                    // Send Email to the Staff BOF                    
                       if(isset($_POST['mail_staff']) ){
                            $data['link']   = DIR_WS_NC .'/purchase-task.php?perform=details&id='. $variables['hid'].'&or_id='.$or_id;
                            //include ( DIR_FS_INCLUDES .'/client.inc.php');
                            Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                            if(!empty($clientDetails)){
                                $client = $clientDetails['0'];                       
                                $data['cf_name'] =$client['f_name'];
                                $data['cl_name'] = $client['l_name'] ;     
                            }
                            $data['af_name'] =$my['f_name'];
                            $data['al_name'] = $my['l_name'] ;  
                            if(!empty($team_members)){
                                foreach ( $team_members as $user ) {
                                    $data['uf_name']    = $user['f_name'];
                                    $data['ul_name']    = $user['l_name'];
                                    $email = NULL;
                                    if ( getParsedEmail($db, $s, 'TASK_REPORT_STAFF', $data, $email) ) {
                                        $to     = '';
                                        $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        /*echo "subject=>".$email["subject"];
                                        echo "<br/>";
                                        echo $email["body"];
                                        echo "To<br/>";
                                        print_r($to);
                                        echo "<br/>";
                                        echo "From<br/>";
                                        print_r($from);*/
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                }                             
                            }
                        }
                    // Send Email to the Staff EOF   
                  
                     // Send Email to the Senior If IMP BOF                      
                       //$users = getExecutivesWith($db, array(  array('right'=>'nc_p_ts_notify', 'access_level'=>'')  ) );                        
                       if(isset($_POST['mark_imp']) ){
                            $data['link']   = DIR_WS_NC .'/purchase-task.php?perform=details&id='. $variables['hid'].'&or_id='.$or_id;
                            //include ( DIR_FS_INCLUDES .'/client.inc.php');
                            Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                            if(!empty($clientDetails)){
                                $client = $clientDetails['0'];                       
                                $data['cf_name'] =$client['f_name'];
                                $data['cl_name'] = $client['l_name'] ;                                
                               
                            }
                            $data['af_name'] =$my['f_name'];
                            $data['al_name'] = $my['l_name'] ;
                            if ( getParsedEmail($db, $s, 'TASK_REPORT_STAFF_IMP', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);                                  
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                /*echo "subject=>".$email["subject"];
                                echo "<br/>";
                                echo $email["body"];
                                echo "To<br/>";
                                print_r($to);
                                echo "<br/>"; 
                                echo "From<br/>";
                                print_r($from);*/
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                            }                            
                            /*
                            if(!empty($users )){
                                foreach ( $users  as $user ) {
                                    $data['uf_name']    = $user['f_name'];
                                    $data['ul_name']    = $user['l_name'];
                                    $email = NULL;
                                    if ( getParsedEmail($db, $s, 'TASK_REPORT_STAFF_IMP', $data, $email) ) {
                                        $to     = '';
                                        $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                      
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                    }
                                }
                             
                            }*/
                            
                        }else{
                       
                            $data['link']   = DIR_WS_NC .'/purchase-task.php?perform=details&id='. $variables['hid'].'&or_id='.$or_id;
                            //include ( DIR_FS_INCLUDES .'/client.inc.php');
                            Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                            if(!empty($clientDetails)){
                                $client = $clientDetails['0'];                       
                                $data['cf_name'] =$client['f_name'];
                                $data['cl_name'] = $client['l_name'] ; 
                            }   
                            $data['af_name'] =$my['f_name'];
                            $data['al_name'] = $my['l_name'] ;
                            $email = NULL;                            
                            if ( getParsedEmail($db, $s, 'TASK_REPORT_STAFF', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                /*
                                echo "subject=>".$email["subject"];
                                echo "<br/>";
                                echo $email["body"];
                                echo "To<br/>";
                                print_r($to);
                                echo "<br/>";
                                echo "From<br/>";
                                print_r($from);*/
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                            }
                            /*
                            if(!empty($users )){
                                foreach ( $users  as $user ) {
                                    $data['uf_name']    = $user['f_name'];
                                    $data['ul_name']    = $user['l_name'];
                                    $email = NULL;
                                    if ( getParsedEmail($db, $s, 'TASK_REPORT_STAFF', $data, $email) ) {
                                        $to     = '';
                                        $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                      
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                    }
                                }                             
                            }*/
                        }
                    // Send Email to the Staff EOF   
                }     
              
               // Send Email to the Staff BOF
               //to flush the data.
               $_ALL_POST	= NULL;
               $data		= NULL;
               
            }
        }
       
        
            $count =  $temp ='';
            /*
            if(isset($_SESSION['attachment'])){
                $count = sizeof($_SESSION['attachment']);
	            $temp = $_SESSION['attachment'];
            }*/
               
            
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $hidden[] = array('name'=> 'or_id' , 'value' => $or_id);           
            $hidden[] = array('name'=> 'ajx','value' => $ajx);
          
            $page["var"][] = array('variable' => 'moduleArr', 'value' => 'moduleArr');
            $page["var"][] = array('variable' => 'priorityArr', 'value' => 'priorityArr');
            $page["var"][] = array('variable' => 'statusArr', 'value' => 'statusArr');
            $page["var"][] = array('variable' => 'resolutionArr', 'value' => 'resolutionArr');
            $page["var"][] = array('variable' => 'typeArr', 'value' => 'typeArr');
            $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            
            $page["var"][] = array('variable' => 'lst_submodule', 'value' => 'lst_submodule');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'count', 'value' => 'count');
            $page["var"][] = array('variable' => 'or_id', 'value' => 'or_id');  
            $page["var"][] = array('variable' => 'temp', 'value' => 'temp');
            
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-task-add.html');
        
    }
    else {
        $messages->setErrorMessage("You do not have the Permisson to Access this module.");
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-add-permission.html');
        
    }
?>
