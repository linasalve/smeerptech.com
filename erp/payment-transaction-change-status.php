<?php
if ( $perm->has('nc_p_pt_edit') ) {  

    $_ALL_POST =null;
    if(array_key_exists('btnGo',$_POST)){
        $data       = processUserData($_POST);
         $transaction_id =$data['transaction_id'];
         $status =$data['status'];
         $do_tr_k = 'do_transaction'.$transaction_id ;
         $do_transaction =$data[$do_tr_k];
         
        $data['do_transaction'] = explode('/', $do_transaction);
        $data['do_transaction'] = mktime(0, 0, 0, $data['do_transaction'][1], $data['do_transaction'][0], $data['do_transaction'][2]);
        $data['do_transaction'] = date('Y-m-d H:i:s', $data['do_transaction']);
        
        $fields_r = TABLE_PAYMENT_TRANSACTION .'.*'  ;            
        $condition_query_r = " WHERE (". TABLE_PAYMENT_TRANSACTION .".id = '". $transaction_id ."' )";
        Paymenttransaction::getDetails($db, $recData, $fields_r, $condition_query_r);
        if(!empty($recData)){
            $recData =$recData['0'];
        }
        
        $tr_db_bank_amt=$tr_cr_bank_amt=$tr_cr_sql=$tr_db_sql='';                
        // FOR Paymenttransaction::PAYMENTIN
        if(isset($recData["transaction_type"]) && $recData['transaction_type'] ==  Paymenttransaction::PAYMENTIN ){
            if($recData['status'] != $data['status']){
                if($data['status'] == Paymenttransaction::COMPLETED){                
                    $dbnew=new db_local();                    	
                    $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                       ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$recData['credit_pay_amt']." WHERE id='".$recData['credit_pay_bank_id']."'";
                    $executed = $dbnew->query($update_query);
                    //Get balance amount after transaction BOF
                    $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$recData['credit_pay_bank_id'];
                    $db->query($sql);
                    if($db->nf()>0 )
                    {
                        while ($db->next_record()) {
                            $tr_cr_bank_amt = $db->f('balance_amt');
                            $tr_cr_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_cr_bank_amt = '".$tr_cr_bank_amt."'" ;
                        }
                    }
                    //Get balance amount after transaction EOF
                    
                    if($executed){
                        $messages->setOkMessage("Your Amount Details has been updated.");
                    }else{
                        $messages->setErrorMessage("Sorry! Amount Details can not be updated.");
                    }
                }            
            }        
        }                
         // FOR Paymenttransaction::PAYMENTOUT BOF
         
        if(isset($recData["transaction_type"]) && $recData['transaction_type'] ==  Paymenttransaction::PAYMENTOUT ){
        
            if($recData['status'] != $data['status']){
                if($data['status'] == Paymenttransaction::COMPLETED){
                                            
                    $dbnew=new db_local();                    	
                    $retrieve_query= "SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id='".$recData['debit_pay_bank_id']."'";
                    $dbnew->query($retrieve_query);
                    $executed = 0 ;
                    $balance = 0 ;
                    if($dbnew->nf()>0 ){
                        while ($dbnew->next_record()) {
                            $balance =$dbnew->f('balance_amt');
                        }
                    }                    
                    if($balance < $recData['debit_pay_amt']){
                        $messages->setErrorMessage("Your balance amount is less than the amount you want to debit.");
                    }
                    else{
                        $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                        ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$recData['debit_pay_amt']." WHERE id='".$recData['debit_pay_bank_id']."'";
                        $executed = $dbnew->query($update_query);
                    }
                    //Get balance amount after transaction BOF
                    
                    
                    $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$recData['debit_pay_bank_id'];
                    $db->query($sql);
                    if($db->nf()>0 )
                    {
                        while ($db->next_record()) {
                            $tr_db_bank_amt = $db->f('balance_amt');
                            $tr_db_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_db_bank_amt = '".$tr_db_bank_amt."'"  ;
                        }
                    }
                    //Get balance amount after transaction EOF
                    if($executed){
                        $messages->setOkMessage("Your Amount Details has been updated.");
                    }else{
                        $messages->setErrorMessage("Sorry! Amount Details can not be updated.");
                    }                        
                }            
            }        
        }
        // FOR Paymenttransaction::PAYMENTOUT EOF
        
        // FOR Paymenttransaction::INTERNAL BOF
        if(isset($recData["transaction_type"]) && $recData['transaction_type'] ==  Paymenttransaction::INTERNAL ){
        
            if($recData['status'] != $data['status']){
                if($data['status'] == Paymenttransaction::COMPLETED){
                        
                        $dbnew=new db_local();                    	
                        $retrieve_query= "SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id='".$recData['debit_pay_bank_id']."'";
                        $dbnew->query($retrieve_query);
                        $executed = 0;
                        $balance = 0;
                        if($dbnew->nf()>0 ){
                            while ($dbnew->next_record()) {
                                $balance =$dbnew->f('balance_amt');
                            }
                        }
                        
                        if($balance < $recData['debit_pay_amt']){
                            $messages->setErrorMessage("Your balance amount is less than the amount you want to debit.");
                        }
                        else{
                            $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$recData['debit_pay_amt']." WHERE id='".$recData['debit_pay_bank_id']."'";
                            $executed = $dbnew->query($update_query);                                   
                            
                                                
                            $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                               ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$recData['credit_pay_amt']." WHERE id='".$recData['credit_pay_bank_id']."'";
                            $executed = $dbnew->query($update_query);
                        }
                        //Get balance amount after transaction BOF
                        $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$recData['credit_pay_bank_id'];
                        $db->query($sql);
                        if($db->nf()>0 )
                        {
                            while ($db->next_record()) {
                                $tr_cr_bank_amt = $db->f('balance_amt');
                                $tr_cr_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_cr_bank_amt = '".$tr_cr_bank_amt."'" ;
                            }
                        }
                        
                        $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$recData['debit_pay_bank_id'];
                        $db->query($sql);
                        if($db->nf()>0 )
                        {
                            while ($db->next_record()) {
                                $tr_db_bank_amt = $db->f('balance_amt');
                                $tr_db_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_db_bank_amt = '".$tr_db_bank_amt."'"  ;
                            }
                        }
                        //Get balance amount after transaction EOF
                        
                        if($executed){
                            $messages->setOkMessage("Your Amount Details has been updated.");
                        }else{
                            $messages->setErrorMessage("Sorry! Amount Details can not be updated for ".$recData["transaction_id"]);
                        }
                }                    
            }                
        }                
        // FOR Paymenttransaction::INTERNAL EOF  
         if( $messages->getErrorMessageCount() <= 0){
                $tr_st_sql='';
                if( $data['status'] != $recData['status']){
                    $tr_st_sql = ",". TABLE_PAYMENT_TRANSACTION .".do_status_update = '".date("Y-m-d")."'
                                ,". TABLE_PAYMENT_TRANSACTION .".status_update_by = '".$my['uid']."'
                                ,". TABLE_PAYMENT_TRANSACTION .".status_update_ip = '".$_SERVER['REMOTE_ADDR']."'" ;
                }
               $query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION
                                 ." SET ". TABLE_PAYMENT_TRANSACTION .".status = '". $data['status'] ."'"   
                                    .",". TABLE_PAYMENT_TRANSACTION .".do_transaction = '".$data['do_transaction'] ."'"      
                                    .$tr_st_sql
                                    .$tr_cr_sql    
                                    .$tr_db_sql                                    
                                    ." WHERE id = '". $transaction_id ."'";
                $db->query($query);
                
                if($data['status'] == Paymenttransaction::COMPLETED){
                    $url=DIR_WS_NC."/payment-transaction.php?perform=print&id=".$transaction_id ;                       
                ?>
                   
                     <script language="javascript" type="text/javascript"> 
                        url= "<?php echo $url; ?>";                       
                        vwidth=600; 
                        vheight=500; 
                        var win_param;	
                        LeftPosition = (screen.width) ? (screen.width-vwidth)/2 : 0;
                        TopPosition = (screen.height) ? (screen.height-vheight)/2 : 0;       
                        win_param="top="+TopPosition+",left="+LeftPosition+",width="+vwidth+",height="+vheight+"channelmode=0,dependent=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,toolbar=0,screenX=0,left=75,top=75,screenY=0";
                        window.open(url,"SmallWindow",win_param);
                       
                   </script>   
                <?
                }
         }        
    }
  
  
  
    $variables['hid'] = $transaction_id;
    $perform='list';
    $condition_query='';
    include ( DIR_FS_NC .'/payment-transaction-list.php');
}else {
    $messages->setErrorMessage("You donot have the Permisson to Access this module.");
}
  
?>