<?php
  if ( $perm->has('nc_ds_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
	    
	    
	    include_once (DIR_FS_INCLUDES .'/deposite-slips.inc.php');	
         
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                 
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
            );
            
                if ( DepositeSlips::validateUpdate($data, $extra) ) {
                    
                     
                    
                    $query	= " UPDATE ".TABLE_DEPOSITE_SLIPS
                            ." SET "   
                            ." ". TABLE_DEPOSITE_SLIPS .".description = '". $data['description'] ."'
							WHERE ".TABLE_DEPOSITE_SLIPS.".id= ".$data['id']  ;
                    
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Record has been updated successfully.");
                                  
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    //$data		= NULL;
		        }
        }else{
		   // Read the record which is to be Updated.
            $fields = TABLE_DEPOSITE_SLIPS .'.*'  ;            
            $condition_query = " WHERE (". TABLE_DEPOSITE_SLIPS .".id = '". $id ."' )";
            
            if ( DepositeSlips::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                    // Setup the date of delivery. 
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
		
		}               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/deposite-slips.php?perform=edit&added=1&id=".$data['id']);
        }
		/*
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/deposite-slips.php");
        }
       */
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
       /*
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/deposite-slips.php?updated=1&number=".$data['number']);   
        }*/
		
		if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/deposite-slips-list.php');
        }
        else {
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit'); 
            $hidden[] = array('name'=> 'act' , 'value' => 'save');         
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');                     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'deposite-slips-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
