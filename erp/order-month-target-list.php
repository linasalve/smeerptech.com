<?php
	
    if ( $perm->has('nc_ordm_t_list') ) {
	
		 
        
        $access_level   = $my['access_level'];   
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
      
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	OrdMonthTarget::getList( $db, $list, '', $condition_query);
    
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','order-month-target.php','frmSearch');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
		$fields ='*';
        //$fields .= TABLE_USER.".f_name as added_by_fname, ". TABLE_USER.".l_name as added_by_lname ";
        OrdMonthTarget::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
		
		 
		
		
		 

        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_ordm_t_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_ordm_t_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_ordm_t_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_ordm_t_delete') ) {
            $variables['can_delete']     = true;
        }
         
        
 
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'order-month-target-list.html');
       
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>