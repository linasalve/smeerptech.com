<?php
    if ( $perm->has('nc_st_add') || $perm->has('nc_st_flw_inv')) {
		 
        $subUserDetails=$additionalEmailArr= null;
		//$user_id = isset ($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
        $step1 = isset ($_GET['step1']) ? $_GET['step1'] : ( isset($_POST['step1'] ) ? $_POST['step1'] : '1');
        $step2 = isset ($_GET['step2']) ? $_GET['step2'] : ( isset($_POST['step2'] ) ? $_POST['step2'] : '0');
        $client_inv = isset ($_GET['client_inv']) ? $_GET['client_inv'] : ( isset($_POST['client_inv'] ) ? 
		$_POST['client_inv'] : '');
		$invoice_id = isset ($_GET['invoice_id']) ? $_GET['invoice_id'] : ( isset($_POST['invoice_id'] ) 
						?	$_POST['invoice_id'] : ''); 
		$invoice_profm_id = isset($_GET['invoice_profm_id']) ? $_GET['invoice_profm_id'] : 
			( isset($_POST['invoice_profm_id'] ) ?	$_POST['invoice_profm_id'] : ''); 
		$flw_ord_id = isset ($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : ( isset($_POST['flw_ord_id'] ) 
						?	$_POST['flw_ord_id'] : '');	
		$flw_tds = isset ($_GET['flw_tds']) ? $_GET['flw_tds'] : ( isset($_POST['flw_tds'] ) ? $_POST['flw_tds'] : 0); 
		$template_category = isset ($_GET['template_category']) ? $_GET['template_category'] : 
		( isset($_POST['template_category'] ) ? $_POST['template_category'] :'');

		$_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates=$ticketThreads = null;
		$_ALL_POST = isset ($_POST['perform']) ? $_POST : ( isset($_GET['perform'] ) ? $_GET : NULL);
        $pagination = '';
	        
        //BOF read the available departments
        //SupportTicket::getDepartments($db,$department);
        //EOF read the available departments
        $domains = array();
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        
		
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        //$lst_min[0] = 'Min';
        for($j=12; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        }
		$clientAuth = Clients::getAuthorization();
		$clientAuth = array_flip($clientAuth);
		// hrs, min.
        if ( (isset($_ALL_POST['btnCreate']) || isset($_ALL_POST['btnReturn'])) && $_ALL_POST['act'] == 'save') {
            //$_ALL_POST 	= $_POST;
            $data	= processUserData($_ALL_POST);
            $files  = processUserData($_FILES); 
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ; 
            $data['max_file_size_mb'] =$variables['max_file_size_in_mb'] ; 
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
						); 
            {
		 
                if ( SupportTicket::validateAdd($data, $extra) ) {
                  
                    //$ticket_no  =  "PT". date("ymdhi-s");
                    $ticket_no  =  SupportTicket::getNewNumber($db);
                    $attachfilename=$ticket_attachment_path='';
                    if(!empty($data['ticket_attachment'])){
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['ticket_attachment']["name"]);
                        $ext = $filedata["extension"] ; 
                        //$attachfilename = $username."-".mktime().".".$ext ;
                        $attachfilename = $ticket_no.".".$ext ;
                        $data['ticket_attachment'] = $attachfilename;
                        
                        $ticket_attachment_path = DIR_WS_ST_FILES;
                        
                        if(move_uploaded_file($files['ticket_attachment']['tmp_name'], 
							DIR_FS_ST_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_ST_FILES."/".$attachfilename, 0777);
                        }
                    }
                    $mail_to_all_su=0;
                    if(isset($data['mail_to_all_su']) ){
                        $mail_to_all_su=1;
                    }
                    $mail_to_additional_email=0;
                    if(isset($data['mail_to_additional_email']) ){
                        $mail_to_additional_email=1;
                    }
                    $mail_client=0;
                    if(isset($data['mail_client']) ){
                        $mail_client=1;
                    }
					$mail_ceo=0;
					if(isset($data['mail_ceo'])){
						$mail_ceo = 1;
					}
				 
					//disply random name BOF 
					$data['display_name']=$data['display_user_id']=$data['display_designation']='';
					$randomUser = getRandomAssociate($data['executive_id']);
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];					
					//disply random name EOF		
					
                    if(!isset($data['ticket_status']) ){                    
                        $ticket_status = SupportTicket::PENDINGWITHCLIENTS;
                    }else{
                        $ticket_status = $data['ticket_status'];
                    }
                    $hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
					$min1 = (int) ($data['min'] * HRS_FACTOR);  
					$sent_inv_id_str = $sent_inv_no_str ='';
					if(!empty($data['sent_inv_id'])){	
						foreach($data['sent_inv_id'] as $key1=>$val1){
							$sent_inv_id_str .= $key1."," ;
							$sent_inv_no_str .= $val1."," ;
						}		
						$sent_inv_id_str = ",".$sent_inv_id_str ;
						$sent_inv_no_str = ",".$sent_inv_no_str ;
					}
					$sent_pinv_id_str = $sent_pinv_no_str ='';
					if(!empty($data['sent_pinv_id'])){	
						foreach($data['sent_pinv_id'] as $key2=>$val2){
							$sent_pinv_id_str .= $key2."," ;
							$sent_pinv_no_str .= $val2."," ;
						}
						$sent_pinv_id_str = ",".$sent_pinv_id_str ;					
						$sent_pinv_no_str = ",".$sent_pinv_no_str ;
					}
				
					if(empty($data['ss_id'])){		
						$data['ss_title']='';
						$data['ss_file_1']='';
						$data['ss_file_2']='';
						$data['ss_file_3']='';
					}
					
					
                    $query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
                        . TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
                        . TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
                        . TABLE_ST_TICKETS .".invoice_profm_id    = '". $invoice_profm_id ."', "
                        . TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
                        . TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
                        . TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
                        . TABLE_ST_TICKETS .".on_behalf        = '". $data['on_behalf'] ."', "
                        . TABLE_ST_TICKETS .".template_id      = '". $data['template_id'] ."', "
						. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
                        . TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
                        . TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_ST_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
						. TABLE_ST_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
						. TABLE_ST_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
                        . TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
                        . TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
						. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
						. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
						. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
						. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
						. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
						. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
                        . TABLE_ST_TICKETS .".ticket_creator_uid    = '". $my['uid'] ."', "
                        . TABLE_ST_TICKETS .".ticket_creator         = '". $my['f_name']." ". $my['l_name'] ."', "
                        . TABLE_ST_TICKETS .".ticket_subject    	 = '". $data['ticket_subject'] ."', "
                        . TABLE_ST_TICKETS .".ticket_mail_subject    = '". $data['ticket_subject'] ."', "
                        . TABLE_ST_TICKETS .".ticket_text         = '". $data['ticket_text'] ."', "
                        . TABLE_ST_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
						. TABLE_ST_TICKETS.".last_comment_by 	  = '".$my['uid'] ."', "
						. TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
                        //. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
                        //. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
                        //. TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
						. TABLE_ST_TICKETS .".mail_ceo        = '".$mail_ceo."', "
                        . TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
                        . TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
                        . TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
                        . TABLE_ST_TICKETS .".ticket_child      = '0', "
						. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_ST_TICKETS .".min = '". $data['min']."', "
                        . TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
                        . TABLE_ST_TICKETS .".ticket_response_time = '0', "
                        . TABLE_ST_TICKETS .".ticket_replied        = '0', "
                        . TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
                        . TABLE_ST_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
                        . TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
                        . TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
                        . TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
                        . TABLE_ST_TICKETS .".is_private           = '". $data['is_private'] ."', "
                        . TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
                        . TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ;
                    
                    //unset($GLOBALS["flag"]);
                    //unset($GLOBALS["submit"]);
                                
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        
                        $variables['hid'] = $db->last_inserted_id() ;
                        
                        /**************************************************************************
                        * Send the notification to the ticket owner and the Administrators
                        * of the new Ticket being created
                        **************************************************************************/
                        $data['ticket_no']    =   $ticket_no ;
                        /*foreach( $status as $key=>$val){                           
                            if ($data['ticket_status'] == $val['status_id'])
                            {
                                $data['status']=$val['status_name'];
                            }
                        }*/
                        
                        $data['domain']   = THIS_DOMAIN;                        
                        //$data['replied_by']   =   $data['ticket_owner'] ;
                        $data['mticket_no']   =   $ticket_no ;
                        $data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
                       	$data['subject']    =     $_ALL_POST['ticket_subject'] ;
                        $data['text']    =   $_ALL_POST['ticket_text'] ;
                        $data['attachment']   =   $attachfilename ;
                        $data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $variables['hid'];
                        $file_name='';
						if(!empty($attachfilename)){
							$file_name[] = DIR_FS_ST_FILES ."/". $attachfilename;
						}
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
						
						if(!empty($data['ss_file_1'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_1'];
						}
						if(!empty($data['ss_file_2'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_2'];
						}
						if(!empty($data['ss_file_3'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_3'];
						}
						if(!empty($data['sent_inv_id'])){	
							foreach($data['sent_inv_id'] as $key1=>$val1){
								$file_name[]  = DIR_FS_INV_PDF_FILES ."/".$val1 .".pdf";
							}				
						}
						if(!empty($data['sent_pinv_id'])){	
							foreach($data['sent_pinv_id'] as $key2=>$val2){
								$file_name[] = DIR_FS_INVPROFM_PDF_FILES ."/".$val2 .".pdf";
							}				
						}
						
                        // Send Email to the ticket owner BOF  
                        $main_client_name = '';
                        $mail_send_to_su  = '';
                        $billing_name = '';
                        //for sms
                        $sms_to_number = $sms_to =array();
                        $mobile1 = $mobile2='';
                        $counter=0;
                        $smsLength = strlen($data['text']); 
                        Clients::getList($db, $userDetails, 'number, f_name, l_name, email,email_1,email_2, email_3,email_4,additional_email,title as ctitle, billing_name,  mobile1, mobile2 ', " WHERE user_id = '".$data['ticket_owner_uid'] ."'");
                        if(!empty($userDetails)){
                            $userDetails=$userDetails['0'];
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    =   $userDetails['ctitle'];
                            $main_client_name  =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $billing_name    =   $userDetails['billing_name'];
                            $email = NULL;
						}
						 
                        // Send Email to the ticket owner EOF
                        $subsmsUserDetails=array();
                        // Send Email to all the subuser BOF
                        if ($data['is_private'] != 1){
                            if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
                                if(isset($_POST['mail_to_all_su'])){
									$followupSql= "";
									if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
										$followupSql = " AND ( authorization_id  LIKE '%,".Clients::ACCOUNTS.",%' OR 
										authorization_id  LIKE '%,".Clients::MANAGEMENT.",%' OR
										authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%' ) " ;
									}
                                    Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, 
									email,email_1,email_2, email_3,email_4', " 
                                    WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."')
                                    AND status='".Clients::ACTIVE."' AND check_email='0' ".$followupSql."");
                                }elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
                                    $mail_to_su = implode("','",array_unique($_POST['mail_to_su']));  
                                    Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " WHERE user_id IN('".$mail_to_su."') AND status='".Clients::ACTIVE."' "); 
                                }                                
                                if(!empty($subUserDetails)){ 
                                    foreach ($subUserDetails  as $key=>$value){
                                        $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                        $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email'], 
											'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ; 
                                            if(!empty($subUserDetails[$key]['email_1'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_1'], 
												'email' => $subUserDetails[$key]['email_1']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ; 
                                            } 
                                            if(!empty($subUserDetails[$key]['email_2'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_2'], 
												'email' => $subUserDetails[$key]['email_2']);
                                                $from  = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ; 
                                            }
                                            if(!empty($subUserDetails[$key]['email_3'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_3'], 
												'email' => $subUserDetails[$key]['email_3']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ; 
                                            }
                                            if(!empty($subUserDetails[$key]['email_4'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_4'], 
												'email' => $subUserDetails[$key]['email_4']);
                                                $from   = $reply_to = array('name' => $email["from_name"],
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ; 
                                            }
                                        }                      
                                    }
                                }
                            } 
							if($mail_to_additional_email==1){ 
								if(!empty($userDetails['additional_email'])){
									$arrAddEmail = explode(",",$userDetails['additional_email']); 
									foreach($arrAddEmail as $key=>$val ){
										if(!empty($val)){
											$mail_send_to_su  .= ",".$val;
											//$to = '';
											$to[]   = array('name' =>$val, 'email' => $val);  
											 
										}
									}
								}
							}else{ 
								if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
									$arrAddEmail = $data['mail_additional_email'];
									foreach($arrAddEmail as $key=>$val ){
										if(!empty($val)){
											$mail_send_to_su  .= ",".$val;
											//$to = '';
											$to[]   = array('name' =>$val, 'email' => $val);  
										 
										}
									}
								}
							}	
                            if(isset($_POST['mobile1_to_su']) ){                                
                               $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
                                Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."') AND user_id IN('".$mobile1_to_su."') AND status='".Clients::ACTIVE."' ");
                                if(!empty($subsmsUserDetails)){                                                                   
                                    foreach ($subsmsUserDetails  as $key=>$value){
                                        if(isset($value['mobile1']) && $smsLength<=130){
                                            $sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
                                            $sms_to_number[] =$value['mobile1'];
                                            $mobile1 = "91".$value['mobile1'];
                                            $client_mobile .= ','.$mobile1;
                                            ProjectTask::sendSms($data['text'],$mobile1);
                                            $counter = $counter+1; 
                                        }
                                    }
                                } 
                            }
                            $subsmsUserDetails=array();
                                     
                        }
                        // Send Email to all the subuser EOF
                        //Mail to CEO BOF
                        if($mail_ceo==1){
							$to[]   = array('name' =>CEO_USER_EMAIL,'email' => CEO_USER_EMAIL);
							$mail_send_to_su .=",".CEO_USER_EMAIL ;
						}
						//Mail to CEO EOF
                        
						//Send one copy on clients BOF
						if(!empty($to)){
							getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
							//$to     = '';
							$cc_to_sender=$cc=$bcc='';
							$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
							'email' => $smeerp_client_email); 
							$from= $reply_to= array('name' => $email["from_name"], 'email' => $email['from_email']);
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name);
						}    
						//Send one copy on clients EOF
						 
                        // Send Email to the admin BOF
                        $email = NULL;
                        $data['link']   = DIR_WS_NC .'/support-ticket.php?perform=view&ticket_id='. $variables['hid'];
                        $data['quicklink']   = DIR_WS_SHORTCUT .'/support-ticket.php?perform=view&ticket_id='. $variables['hid'];      
                        $data['main_client_name']   = $main_client_name;
                        $data['billing_name']   = $billing_name;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'EMAIL_ST_ADMIN', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $support_name , 'email' => $support_email);                           
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);							 
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                            $cc_to_sender,$cc,$bcc,$file_name);
                        }                        
                        // Send Email to the admin EOF
                        $messages->setOkMessage("New Support Ticket has been created.And email Notification has been sent.");
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
                } 
            }
        }
		
		 
        
		if(isset($_POST['btnCreate']) && $data['step1']!='1' && $messages->getErrorMessageCount() <= 0 ){
			
			if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
				header("Location:".DIR_WS_NC."/support-ticket.php?perform=invflw_view&added=1&ticket_id=".$variables['hid']."&invoice_profm_id=".$invoice_profm_id."&invoice_id=".$invoice_id."&flw_tds=".$flw_tds."&flw_ord_id=".$flw_ord_id."&client_inv=".$client_inv);
			}else{
				header("Location:".DIR_WS_NC."/support-ticket.php?perform=add&added=1");
			}
        } 
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        
		/* 
		if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/support-ticket-list.php');
        }
        else { */
        
            $variables['can_view_client_details']  = false;
			$variables['can_select_team']  = false;         
            if ( $perm->has('nc_uc_contact_details') ) {
                $variables['can_view_client_details'] = true;
            }
			if ( $perm->has('nc_st_select_team') ) {
                $variables['can_select_team'] = true;
            }	
            $variables['client_sendmail'] = Clients::SENDMAILNO;
			
			if(!isset($_ALL_POST['executive_id'])){
				$randomUser = getRandomAssociate();
				$_ALL_POST['executive_details']= $randomUser['name']." ( ".$randomUser['number']." )";
				$_ALL_POST['executive_id'] = $randomUser['user_id'] ;
				
			}
			
			
            /*
			$condition_query= " WHERE user_id = '". $user_id ."' ";
            $clients      = NULL;
            if ( Clients::getList($db, $clients, '*', $condition_query) > 0 ) {
                //$_ALL_POST = $_ALL_POST[0];
                $_ALL_POST['name']=$clients[0]['f_name']." ".$clients[0]['l_name'];
                $_ALL_POST['email']=$clients[0]['email'];
            }*/            
            /*
			$condition_query= " WHERE service_id LIKE '%". Clients::ST ."%'";            
            $clients      = NULL;
            Clients::getList($db, $clients, '*', $condition_query);
			*/
            
            // These parameters will be used when returning the control back to the List page.
            /*
			foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ){
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
			*/
			
            $hidden[] = array('name'=> 'perform','value' => $perform);
            $hidden[] = array('name'=> 'invoice_id','value' => $invoice_id);
            $hidden[] = array('name'=> 'invoice_profm_id','value' => $invoice_profm_id);
            $hidden[] = array('name'=> 'flw_ord_id','value' => $flw_ord_id);
            $hidden[] = array('name'=> 'client_inv','value' => $client_inv);
            $hidden[] = array('name'=> 'x','value' => $x);
			$hidden[] = array('name'=> 'ajx','value' => $ajx);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //$hidden[] = array('name'=> 'user_id', 'value' => $user_id);
           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
            $page["var"][] = array('variable' => 'additionalEmailArr', 'value' => 'additionalEmailArr');
            $page["var"][] = array('variable' => 'subUserDetailsAccts', 'value' => 'subUserDetailsAccts');
            $page["var"][] = array('variable' => 'subUserDetailsMgmts', 'value' => 'subUserDetailsMgmts');
            $page["var"][] = array('variable' => 'subUserDetailsAuth', 'value' => 'subUserDetailsAuth');
            $page["var"][] = array('variable' => 'subUserDetailsUsr', 'value' => 'subUserDetailsUsr');
            $page["var"][] = array('variable' => 'subUserDetailsDnd', 'value' => 'subUserDetailsDnd');
            $page["var"][] = array('variable' => 'subUserDetailsDir', 'value' => 'subUserDetailsDir');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
            $page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
            $page["var"][] = array('variable' => 'ticketThreads', 'value' => 'ticketThreads');
			
			$page["var"][] = array('variable' => 'flw_tds', 'value' => 'flw_tds');
			$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
			$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'invoice_id', 'value' => 'invoice_id');
            $page["var"][] = array('variable' => 'invoice_profm_id', 'value' => 'invoice_profm_id');
            $page["var"][] = array('variable' => 'flw_ord_id', 'value' => 'flw_ord_id');
            $page["var"][] = array('variable' => 'orderArr', 'value' => 'orderArr');
            $page["var"][] = array('variable' => 'pendingInvlist', 'value' => 'fpendingInvlist');
			$page["var"][] = array('variable' => 'fpendingPInvlist', 'value' => 'fpendingPInvlist');
			$page["var"][] = array('variable' => 'totalBalanceAmount', 'value' => 'totalBalanceAmount');
			$page["var"][] = array('variable' => 'totalBalanceAmountP', 'value' => 'totalBalanceAmountP');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            //$page["var"][] = array('variable' => 'status', 'value' => 'status');
            //$page["var"][] = array('variable' => 'priority', 'value' => 'priority');
            $page["var"][] = array('variable' => 'domains', 'value' => 'domains');
            //$page["var"][] = array('variable' => 'clients', 'value' => 'clients');
            $page["var"][] = array('variable' => 'step1', 'value' => 'step1');
            $page["var"][] = array('variable' => 'step2', 'value' => 'step2');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'client-doc-upload-add.html');
        //}
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>