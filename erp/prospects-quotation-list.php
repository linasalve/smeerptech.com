<?php

    if ( $perm->has('nc_ps_qt_list') ) {
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            
            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(ProspectsQuotation::COMPLETED,ProspectsQuotation::PENDING);            
            
            $condition_query = " WHERE (". TABLE_PROSPECTS_QUOTATION .".status = '". ProspectsQuotation::COMPLETED ."' OR ". TABLE_PROSPECTS_QUOTATION .".status = '". ProspectsQuotation::PENDING."')"   ;
        }
        
        // If the ProspectsQuotation is created by the my.
        if ( $perm->has('nc_ps_qt_list_all') ) { 
		 
		 
		}else{ 
		
			if(empty($_SEARCH["search_executive_id"]) && empty($_SEARCH["user_id"]) ){
				$condition_query .= " AND ( ";
				
					$condition_query .= " (". TABLE_PROSPECTS_ORDERS .".order_closed_by = '". $my['user_id'] ."' "."  ) "; 
					$condition_query .= " OR ( "
											. TABLE_PROSPECTS_ORDERS .".team LIKE '%,". $my['user_id'] .",%' "
										. "  "
										."  ) "; 
				// Check if the User has the Right to view Orders created by other Users.
				$condition_query .= " )";   				
				$condition_url  .= "&user_id=$user_id";
				$_SEARCH["user_id"]  = $user_id;
			}    
		}
     
         $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
        
            $extra_url  = $condition_url;
            
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }        
        $condition_url .="&perform=".$perform;
        
		$totalAmt =array();
		$totalAmount='';
		$fields=" SUM(".TABLE_PROSPECTS_QUOTATION.".amount_inr) as totalAmount ";
		ProspectsQuotation::getDetails(  $db, $totalAmt, $fields, $condition_query);
		if(!empty($totalAmt)){
			$totalAmount=$totalAmt[0]['totalAmount'];
		}
		
        //Code to calculate total of Amount EOF
        
        // To count total records.
        $list	= 	NULL;
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
        if($searchStr==1){
           $fields = TABLE_PROSPECTS_QUOTATION .'.id'
                        .','. TABLE_PROSPECTS_QUOTATION .'.number'
                        .','. TABLE_PROSPECTS_QUOTATION .'.or_no'
                        .','. TABLE_PROSPECTS_QUOTATION .'.access_level'
                        .','. TABLE_PROSPECTS_QUOTATION .'.do_e'
                        .','. TABLE_PROSPECTS_QUOTATION .'.do_c'
                        .','. TABLE_PROSPECTS_QUOTATION .'.do_i'
                        .','. TABLE_PROSPECTS_QUOTATION .'.currency_abbr'
                        .','. TABLE_PROSPECTS_QUOTATION .'.amount'
                        .','. TABLE_PROSPECTS_QUOTATION .'.amount_inr'
                        .','. TABLE_PROSPECTS_QUOTATION .'.balance'
                        .','. TABLE_PROSPECTS_QUOTATION .'.balance_inr'
                        .','. TABLE_PROSPECTS_QUOTATION .'.status'
                        .','. TABLE_PROSPECTS_QUOTATION .'.is_old'
                        .','. TABLE_PROSPECTS_QUOTATION .'.old_updated'
                        .','. TABLE_PROSPECTS_QUOTATION .'.reason_of_delete'						
                        .','. TABLE_PROSPECTS_QUOTATION .'.attach_file1'
                        .','. TABLE_PROSPECTS_QUOTATION .'.attach_file2'
                        .','. TABLE_PROSPECTS_QUOTATION .'.attach_file3'
                        .','. TABLE_PROSPECTS_QUOTATION .'.attach_file4'
                        .','. TABLE_PROSPECTS_QUOTATION .'.attach_file5'
                        .','. TABLE_PROSPECTS_QUOTATION .'.pdf_created'
                        .','. TABLE_PROSPECTS_QUOTATION .'.quotation_status'
                        .','. TABLE_PROSPECTS_ORDERS .'.order_title'
                        .','. TABLE_PROSPECTS_ORDERS .'.id as order_id'
                        .','. TABLE_PROSPECTS_ORDERS .'.team'
                        .','. TABLE_PROSPECTS .'.user_id AS c_user_id'
                        .','. TABLE_PROSPECTS .'.number AS c_number'
                        .','. TABLE_PROSPECTS .'.org'
                        .','. TABLE_PROSPECTS .'.f_name AS c_f_name'
                        .','. TABLE_PROSPECTS .'.l_name AS c_l_name'
                        .','. TABLE_PROSPECTS .'.email AS c_email'
                        .','. TABLE_PROSPECTS .'.mobile1'
                        .','. TABLE_PROSPECTS .'.mobile2'
                        .','. TABLE_PROSPECTS .'.billing_name AS billing_name'
                        .','. TABLE_PROSPECTS .'.status AS c_status';
            $total	= ProspectsQuotation::getDetails( $db, $list, '', $condition_query);        
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');            
            $list	= NULL;
            
            ProspectsQuotation::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){               
               $val['is_rcpt_created'] = false;
               $val['is_paid_fully'] = false;
                 
                // check whether rcpt created or not EOF
                if($val['balance'] <= 0){
                    $val['is_paid_fully'] = true;
                }
				//check invoice id followup in ST BOF
				$val['ticket_id']='';
				$sql2 = "SELECT ticket_id FROM ".TABLE_PROSPECTS_TICKETS." WHERE 
				(".TABLE_PROSPECTS_TICKETS.".flw_ord_id = '".$val['order_id']."') LIMIT 0,1";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                        }
                    }                   
                }
				//check invoice id followup in ST EOF
				$val['team']=trim($val['team'],",");
                $team = str_replace(',',"','",$val['team']);
				$sql= " SELECT f_name,l_name FROM ".TABLE_USER." WHERE ".TABLE_USER.".user_id IN ('".$team."') ";
                $db->query($sql);               
				$team_members ='';
                if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$team_members .= $db->f('f_name')." ". $db->f('l_name').", <br/>" ; 
					}
                }   
				$team_members =trim($team_members,",");
				$val['team_members']=$team_members ;
                $val['amount']=number_format($val['amount'],2);
                $val['amount_inr']=number_format($val['amount_inr'],2);
                $val['balance']=number_format($val['balance'],2);
                $val['balance_inr']=number_format($val['balance_inr'],2);
               
                $flist[$key]=$val;
                
            }
        }
       
       
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_send_invoice'] = false;
        $variables['can_followup']= false;
        $variables['can_verify_proposal']= false; 
		$variables['can_view_list_all']     = false;
		$variables['can_print_lbl'] = true;
		
        if ( $perm->has('nc_ps_qt_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_ps_qt_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_ps_qt_edit') ) {
           $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_ps_qt_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_ps_qt_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_ps_qt_status') ) {
            $variables['can_change_status']     = true;
        }
        if ( $perm->has('nc_ps_qt_send') ) {
            $variables['can_send_invoice']     = true;
        }
        if ( $perm->has('nc_pst') && $perm->has('nc_pst_flw_ord') ) {
            $variables['can_followup'] = true;
        }
        if ($perm->has('nc_pr') && $perm->has('nc_pr_lblprn') ) {
            $variables['can_print_lbl'] = true;
        }
		if ( $perm->has('nc_ps_qt_verify') ) {
            $variables['can_verify_proposal']     = true;
        }
		if ( $perm->has('nc_ps_qt_list_all') ) { 
		    $variables['can_view_list_all'] = true;
		}
		$variables["none"] = ProspectsQuotation::NONEAPPROVAL; 
        $variables["send_for_approval"] = ProspectsQuotation::SEND_FOR_APPROVAL;
		$variables["approved"]  = ProspectsQuotation::APPROVED;
		$variables["disapproved"] = ProspectsQuotation::DISAPPROVED;
		//Get list of team bof
		if ( $perm->has('nc_ps_qt_list_all') ) {   
			
		}else{ 
			$rteam=trim($my['my_reporting_members'],",");
            $team = str_replace(',',"','",$rteam);
			$ulist=array();
			$ulist[]= array('user_id'=>$my['user_id'],
						'number'=>$my['number'],
						'name'=>$my['f_name'].' '.$my['l_name']
					);
			$sql= " SELECT f_name,l_name,number,user_id FROM ".TABLE_USER." WHERE ".TABLE_USER.".user_id IN ('".$team."') ";
			$db->query($sql);   
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ) {
					$user_id=$db->f('user_id');
					$number = $db->f('number');
					$name= $db->f('f_name')." ". $db->f('l_name') ; 
					$ulist[]= array('user_id'=>$user_id,
						'number'=>$number,
						'name'=>$name
					) ; 
				}
			}  
		}
	 
        $page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount');
        $page["var"][] = array('variable' => 'total', 'value' => 'total');
        $page["var"][] = array('variable' => 'ulist', 'value' => 'ulist');
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-quotation-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>