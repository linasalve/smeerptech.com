<?php

// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/old-nc-db.inc.php');
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/common-functions.inc.php');
    include_once ( DIR_FS_CLASS .'/Region.class.php');
    include_once ( DIR_FS_CLASS .'/Phone.class.php');
    include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
  	include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
    //$region     = new Region('91');
    //$phone      = new Phone(TABLE_CLIENTS);
    $reminder   = new UserReminder(TABLE_CLIENTS);
    ini_set("max_execution_time",0);// TO EXECUTE A SCRIPT WITH UN LIMITED TIME
    
    // define tables of old nc BOF
    define("TABLE_USER_OLD","auth_user");
    define("TABLE_MEMBER_DETAILS_OLD","member_details");
	define("TABLE_BILL_INVOICE_OLD","bills_invoice");
	define("TABLE_BILL_ORDERS_OLD","bills_orders");
	//define("TABLE_BILL_MISC","bills_misc");
	//define("TABLE_BILL_USER","bills_member_account");
	define("TABLE_BILL_RECEIPT_OLD","bills_receipt");
	define("TABLE_MIGRATING_CLIENT","migrating_client_info");
    // define tables of old nc EOF



    $db1 		= new db_local; // database handle
    // connect with old nc d/b
    $con = DB::makeConnection();
   // $con1 = DB::makeConnection();
   /*
 
    $query = "SELECT ".TABLE_USER_OLD.".uid,".TABLE_USER_OLD.".username,".TABLE_USER_OLD.".password, "
            .TABLE_USER_OLD.".fname, ".TABLE_USER_OLD.".lname, ".TABLE_USER_OLD.".email, ".TABLE_USER_OLD.".position, "
            .TABLE_USER_OLD.".website, ".TABLE_USER_OLD.".company, ".TABLE_USER_OLD.".user_status, ".TABLE_USER_OLD.".date_added, "
            .TABLE_USER_OLD.".regDate "." FROM ".TABLE_USER_OLD." WHERE ".TABLE_USER_OLD.".perms='USER'" ;
     
	 $result = mysql_query($query,$con);
     
     $clientExistContent=$clientEnterdContent='';
     $listUid =$listUDone=array();
	if( mysql_num_rows($result) > 0){
        while ($row = mysql_fetch_array($result)){
            $error=array();
            $email2 = '';
            $email3 = '';
            $uid = $row['uid'];
            $username = $row['username'];
            //$username ='';
            $password = $row['password'];
            $fname = $row['fname'];
            $lname = $row['lname'];
            $email = $row['email'];
            $position = $row['position'];
            $website = $row['website'];
            $company = $row['company'];
            $user_status = $row['user_status'];
            $date_added = $row['date_added'];
            $regDate = $row['regDate'];
            $email2 = $row['email2'];
            $email3 = $row['email3'];
            //$details = $row['details'];
            $username = strtolower($row['fname']).".".strtolower($row['lname']);
            
            
            $details ='';
            $user_status = $row['user_status'];
            $date_added = $row['date_added'];            
            $regDate = $row['regDate'];
            $do_add  = date("Y-m-d",$date_added );
            $do_reg =  date("Y-m-d",$regDate );
           
            
            $title=$m_name=$p_name=$gender=$do_birth=$do_aniv='';
            // access level 65535 
            $access_level = "65535";
            // Default role 'Client Genaral'
            $roles ="13";
            //Any uid who will be a client's  manager
            $manager='e68adc58f2062f58802e4cdcfec0af2d';
            //comma seperated team uids
            $teamStr ='e68adc58f2062f58802e4cdcfec0af2d';
            
            $number = Clients::getNewAccNumber($db);
           
            $user_list  = NULL;
            $condition  = " WHERE (user_id = '".$uid."') " ;
                           // ." OR ( username = '". $data['username'] ."' "
                           // ." AND number = '". $data['number'] ."' AND number IS NOT NULL ) ";
             
            echo $query = "SELECT * FROM ". TABLE_CLIENTS
                        ." WHERE user_id = '".$uid."'";
            if ( $db->query($query) && $db->nf() > 0 ) {
                //$error[] = 'already exist';
                
                $clientExistContent .= " Client Already exist in database. client id is '$uid' "."\n";
                $clientExist[] = $uid;
                continue;
                
            }
            echo "<br>";
 
    
           //if ( $messages->getErrorMessageCount() <= 0 ) {           
            if ( empty($error) ) {     
                $consql = DB::makeConnection();
                echo $sql_addr = "SELECT * FROM ".TABLE_MEMBER_DETAILS_OLD." WHERE uid ='".$uid."'" ;
                $resultsqladr = mysql_query($sql_addr,$consql);
                $primary_address ='';
                if( mysql_num_rows($resultsqladr) ){
                    while ($rowdata = mysql_fetch_array($resultsqladr)){
                     
                        $title=processUserData($rowdata['title']);
                        $phone_h=processUserData($rowdata['phone_h']);
                        $phone_o=processUserData($rowdata['phone_o']);
                        $fax_h=processUserData($rowdata['fax_h']);
                        $fax_o=processUserData($rowdata['fax_o']);
                        $mobile=processUserData($rowdata['mobile']);
                        
                        $add1_h=processUserData($rowdata['add1_h']);
                        $add2_h=processUserData($rowdata['add2_h']);
                        $city_h=processUserData($rowdata['city_h']);
                        $state_h=processUserData($rowdata['state_h']);
                        $country_h=processUserData($rowdata['country_h']);
                        $zip_h=processUserData($rowdata['zip_h']);
                        $add1_o=processUserData($rowdata['add1_o']);
                        $add2_o=processUserData($rowdata['add2_o']);
                        $city_o=processUserData($rowdata['city_o']);
                        $state_o=processUserData($rowdata['state_o']);
                        $country_o=processUserData($rowdata['country_o']);
                        $zip_o=processUserData($rowdata['zip_o']);
                        $primary_address=processUserData($rowdata['primary_address']);
                    }
                }                  
                DB::closeConnection($consql);
            
                $db 		= new db_local; // database handle  
                $phone1      = new Phone(TABLE_CLIENTS);
                $phone2      = new Phone(TABLE_CLIENTS);
                $phone3      = new Phone(TABLE_CLIENTS);
                $phone4      = new Phone(TABLE_CLIENTS);
                $phone5      = new Phone(TABLE_CLIENTS);
                $region1     = new Region('91');
                $region2     = new Region('91');
                $region3     = new Region('91');
            
            
            
                $service_idstr = ",".Clients::BILLING.",".Clients::ST."," ;
            
                echo "<br>";
                echo $sql1	= " INSERT INTO ". TABLE_CLIENTS
                ." SET ". TABLE_CLIENTS .".user_id     	= '".$uid."'"
                            .",". TABLE_CLIENTS .".number      	= '". processUserData($number) ."'"
							.",". TABLE_CLIENTS .".manager 	   	= '". processUserData($manager) ."'"
							.",". TABLE_CLIENTS .".team			= '". processUserData($teamStr)."'"
                            .",". TABLE_CLIENTS .".username    	= '". processUserData($username) ."'"
                            .",". TABLE_CLIENTS .".password    	= '". processUserData($password) ."'"
                            .",". TABLE_CLIENTS .".access_level	= '". processUserData($access_level)."'"
                            .",". TABLE_CLIENTS .".service_id	= '". processUserData($service_idstr)."'"
                            .",". TABLE_CLIENTS .".roles       	= '". processUserData($roles )."'"
                            .",". TABLE_CLIENTS .".email       	= '". processUserData($email )."'"
                            .",". TABLE_CLIENTS .".email_1     	= '". processUserData($email2 )."'"
                            .",". TABLE_CLIENTS .".email_2     	= '". processUserData($email3 )."'"
                            .",". TABLE_CLIENTS .".title       	= '". processUserData($title )."'"
                            .",". TABLE_CLIENTS .".f_name      	= '". processUserData($fname )."'"
                            .",". TABLE_CLIENTS .".m_name      	= '". processUserData($m_name )."'"
                            .",". TABLE_CLIENTS .".l_name      	= '". processUserData($lname )."'"
                            .",". TABLE_CLIENTS .".p_name      	= '". processUserData($p_name )."'"
                            .",". TABLE_CLIENTS .".desig       	= '". processUserData($position )."'"
                            .",". TABLE_CLIENTS .".org         	= '". processUserData($company )."'"
                            .",". TABLE_CLIENTS .".domain      	= '". processUserData($website )."'"
                            .",". TABLE_CLIENTS .".gender      	= '". processUserData($gender )."'"
                            .",". TABLE_CLIENTS .".do_birth    	= '". processUserData($do_birth )."'"
                            .",". TABLE_CLIENTS .".do_aniv     	= '". processUserData($do_aniv )."'"
                            .",". TABLE_CLIENTS .".remarks     	= '". processUserData($details )."'"
                            .",". TABLE_CLIENTS .".do_add      	= '". processUserData($do_add )."'"
                            .",". TABLE_CLIENTS .".do_reg      	= '". processUserData($do_reg )."'"
                            .",". TABLE_CLIENTS .".status      	= '". processUserData($user_status )."'" ;
                $db->query($sql1);
                // get address and insert into address table BOF
                $address= $city= $state= $country= $zipcode='';
                $phone = 0 ;                
                    $address = '' ;
                    $city = '' ;
                    $state = '' ;
                    $country ='' ;
                    $zip = '' ;
                    $phone = '' ;
                    
                    $company_name = processUserData($fname )." ".processUserData($lname);
                    
                    
                    if( !empty($add1_h) || !empty($add2_h) || !empty($city_h) || !empty($state_h)){
                        // Insert the address.
                        $address = $add1_h."\n".$add2_h ;
                        $city = $city_h;
                        $state = $state_h;
                        $country = '91';
                        $zip = $zip_h;                    
                        $phone = $phone_h;                                              
                        //default country h
                        $country = 91;
                        $address_arr =array();
                        $address_arr = array('address_type' => 'h',
                                                        'company_name'  => $company_name,
                                                        'address'       => $address,                                                
                                                        'city'          => $city,
                                                        'state'         => $state,
                                                        'country'       => $country,
                                                        'zipcode'       => $zip,
                                                        'is_preferred'  => '0',
                                                        'is_verified'   => '0',
                                                        'id'=>''
                                                        );
                                    
                        if ( !$region1->update($uid, TABLE_CLIENTS, $address_arr) ) {
                            foreach ( $region1->getError() as $errors) {
                                $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                            }
                        }

                    }
                    
                    if( !empty($add1_o) || !empty($add2_o) || !empty($city_o) || !empty($state_o)){
                         // Insert the address.
                       $address = $add1_o."\n".$add2_o ;
                       $city = $city_o;
                       $state = $state_o;
                       $country = '91';
                       $zip = $zip_o;   
                       $phone = $phone_o;   
                       
                       //default country o
                        $country = 91;
                        $address_arr =array();
                        $address_arr = array('address_type' => 'w',
                                                        'company_name'  => $company_name,
                                                        'address'       => $address,                                                
                                                        'city'          => $city,
                                                        'state'         => $state,
                                                        'country'       => $country,
                                                        'zipcode'       => $zip,
                                                        'is_preferred'  => '0',
                                                        'is_verified'   => '0',
                                                        'id'=>''
                                            );
                                    
                        if ( !$region2->update($uid, TABLE_CLIENTS, $address_arr) ) {
                            foreach ( $region2->getError() as $errors) {
                                $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                            }
                        }                       
                    }
                    
                    if(empty($primary_address)){
                    
                       if(!empty($add1_h)){
                          $address = $add1_h."\n".$add2_h ;
                       
                       }elseif(!empty($add2_h)){
                       
                          $address =  $add2_h ;
                          
                       }elseif(!empty($add1_o)){
                       
                            $address = $add1_o."\n".$add2_o ;
                            
                       }elseif(!empty($add2_o)){
                       
                            $address =  $add2_o ;
                       }
                       
                       if(!empty($city_h)){
                          $city = $city_h;
                       
                       }elseif(!empty($city_o)){                       
                          $city =  $city_o ;                          
                       }
                       
                      if(!empty($state_h)){
                          $state = $state_h;
                       
                       }elseif(!empty($state_o)){                       
                          $state =  $state_o ;                          
                       }
                       
                       if(!empty($zip_h)){
                          $zip = $zip_h;
                       
                       }elseif(!empty($zip_o)){                       
                          $zip = $zip_o;                           
                       }
                       if(!empty($phone_h)){
                          $phone = $phone_h;
                       
                       }elseif(!empty($phone_o)){                       
                          $phone = $phone_o;                         
                       }
                        
                        //default country o
                        $country = 91;
                        $address_arr =array();
                        $address_arr = array('address_type' => 'o',
                                            'company_name'  => $company_name,
                                            'address'       => $address,                                                
                                            'city'          => $city,
                                            'state'         => $state,
                                            'country'       => $country,
                                            'zipcode'       => $zip,
                                            'is_preferred'  => '0',
                                            'is_verified'   => '0',
                                            'id'=>''
                                            );
                                    
                        if ( !$region3->update($uid, TABLE_CLIENTS, $address_arr) ) {
                            foreach ( $region3->getError() as $errors) {
                                $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                            }
                        }
                    }                     
                  
                
                
              
                if($country=='India'){
                    $country =91;
                   
                }
               
                // get address and insert into address table BOF
                        
                if(!empty($phone_h)){                
                     $p_number = $phone_h ;
                     $phone_arr=array();
                     $phone_arr = array( 'p_type'        => 'h',
                                        'cc'            => '',
                                        'ac'            => '',
                                        'p_number'      => $p_number,
                                        'p_is_preferred'=> '0',
                                        'p_is_verified' => '0'
                                    );
                
                    if ( ! $phone1->update($db, $uid, TABLE_CLIENTS, $phone_arr) ) {
                        foreach ( $phone1->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
                if(!empty($phone_o)){                
                     $p_number = $phone_o ;
                     $phone_arr=array();
                     $phone_arr = array('p_type'        => 'w',
                                        'cc'            => '',
                                        'ac'            => '',
                                        'p_number'      => $p_number,
                                        'p_is_preferred'=> '0',
                                        'p_is_verified' => '0'
                                    );
                
                                if ( ! $phone2->update($db, $uid, TABLE_CLIENTS, $phone_arr) ) {
                                    foreach ( $phone2->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                }
                
                if(!empty($mobile)){                
                     $p_number = $mobile ;
                     $phone_arr=array();
                     $phone_arr = array( 'p_type'        => 'm',
                                        'cc'            => '',
                                        'ac'            => '',
                                        'p_number'      => $p_number,
                                        'p_is_preferred'=> '0',
                                        'p_is_verified' => '0'
                                    );
                
                                if ( ! $phone3->update($db, $uid, TABLE_CLIENTS, $phone_arr) ) {
                                    foreach ( $phone3->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                }          
                if(!empty($fax_h)){                
                     $p_number = $fax_h ;
                     $phone_arr=array();
                     $phone_arr = array( 'p_type'        => 'f',
                                        'cc'            => '',
                                        'ac'            => '',
                                        'p_number'      => $p_number,
                                        'p_is_preferred'=> '0',
                                        'p_is_verified' => '0'
                                    );
                
                    if ( ! $phone4->update($db, $uid, TABLE_CLIENTS, $phone_arr) ) {
                        foreach ( $phone4->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                }
                
                if(!empty($fax_o)){                
                     $p_number = $fax_o ;
                     $phone_arr=array();
                     $phone_arr = array( 'p_type'        => 'f',
                                        'cc'            => '',
                                        'ac'            => '',
                                        'p_number'      => $p_number,
                                        'p_is_preferred'=> '0',
                                        'p_is_verified' => '0'
                                    );
                
                    if ( ! $phone5->update($db, $uid, TABLE_CLIENTS, $phone_arr) ) {
                        foreach ( $phone5->getError() as $errors) {
                            $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                        }
                    }
                } 
                
                echo  $sql2 = "INSERT INTO ".TABLE_MIGRATING_CLIENT
                       ." SET ". TABLE_MIGRATING_CLIENT .".new_number     = '". $number ."'"
                       .",". TABLE_MIGRATING_CLIENT .".old_uid      = '". $uid ."'" ;
                $db->query($sql2);
                echo "<br>";
                $listUDone[]= $fname." ".$lname."(".$uid.")" ;  
                $clientEnterdContent.= " Client enterd now ( Current script )"." uid ".$uid." and number ".$number." \n";
                
                
            }else{
                $listUid[]= $fname." ".$lname."(".$uid.")" ;                            
            }
            
        }
    }
    // already entered users list
    if(!empty( $listUid)){
        echo "following users are created previously";
        echo "<br/>";
        print_r($listUid);
    }
    // already entered users list
    if(!empty( $listUDone)){
        echo "following users are created now";
        echo "<br/>";
        print_r($listUDone);
    }
    
    //write the errors in file BOF 
    $file='migrate-client.txt';
    $fp = fopen($file, 'a+');
    $content = " ----------- Script date ".date('Y-m-d')." ----------- \n" ;
	$content .= "------------ Client exist Content ------------"."\n" ;
	$content .= $clientExistContent ;
    $content .= "------------ Script entered client (Currently entered client) ------------"."\n" ;
    echo $content .= $clientEnterdContent;
    
    if (fwrite($fp, $content) === FALSE) {
        echo "Cannot write to file ($file)";
        exit;
    }
    fclose($fp);
    //write the errors in file EOF 
*/
 
?>