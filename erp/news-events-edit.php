<?php
if ( $perm->has('nc_ne_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        
        $hour = array();
		$min = array();
		$sec = array();
		for($i=0;$i < 60 ; $i++)
		{
			$min[$i] = "$i";
			$sec[$i] = "$i";
			if($i >= 0 && $i <=24)
			{
				$hour[$i] = "$i";
			} 
		} 
		
        include_once (DIR_FS_INCLUDES .'/news-events.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( NewsEvents::validateUpdate($data, $extra) ) {
				if ( !empty($data['events_date']) ) {
					$temp = explode('/', $data['events_date']);                
					$data['events_date'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
				}
				if ( !empty($data['events_exp_date']) ) {
					$temp = explode('/', $data['events_exp_date']);                
					$data['events_exp_date'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
				}
			
                $query  = " UPDATE ". TABLE_EVENTS
                           ." SET ".TABLE_EVENTS .".events_title = '". $data['events_title'] ."'" 
                                    .",". TABLE_EVENTS .".events_date = '". $data['events_date'] ."'" 
                                    .",". TABLE_EVENTS .".events_exp_date = '". $data['events_exp_date'] ."'" 
                                    .",". TABLE_EVENTS .".event_time  = '". $data['event_time'] ."'" 
                                    .",". TABLE_EVENTS .".events_source = '". $data['events_source'] ."'"  
                                    .",". TABLE_EVENTS .".events_introtext = '". $data['events_introtext'] ."'"  
                                    .",". TABLE_EVENTS .".event_type = '". 		$data['event_type'] ."'"  
                                     .",". TABLE_EVENTS .".events_content = '". $data['events_content'] ."'" 
                                   	.",". TABLE_EVENTS .".status = '". 		$data['status'] ."'"                               
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_EVENTS .'.*'  ;            
            $condition_query = " WHERE (". TABLE_EVENTS .".id = '". $id ."' )";
            
            if ( NewsEvents::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                	
                	$time = array();
                	 
                	$time= explode(':', $_ALL_POST['event_time']);
       
                	$_ALL_POST['hr']=$time[0];
                	$_ALL_POST['min']=$time[1];
                	$_ALL_POST['sec']=$time[2];
                    // Setup the date of delivery.
                   /* $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
					*/
					if ( !empty($_ALL_POST['events_date']) && $_ALL_POST['events_date']!='0000-00-00 00:00:00') {
						 $_ALL_POST['events_date']  = explode(' ', $_ALL_POST['events_date']);
						$temp               = explode('-', $_ALL_POST['events_date'][0]);
						$_ALL_POST['events_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						
					}else{
					$_ALL_POST['events_date']='';
					}
					
					if ( !empty($_ALL_POST['events_exp_date']) && $_ALL_POST['events_exp_date']!='0000-00-00 00:00:00') {
						$_ALL_POST['events_exp_date']  = explode(' ', $_ALL_POST['events_exp_date']);
						$temp               = explode('-', $_ALL_POST['events_exp_date'][0]);
						$_ALL_POST['events_exp_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						
					}else{
						$_ALL_POST['events_exp_date']='';
					}
					
                    $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/news-events-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'news-events-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
    
    $s->assign("hour",$hour);
	$s->assign("min",$min);
	$s->assign("sec",$sec);
?>
