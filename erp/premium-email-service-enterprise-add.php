<?php
	$_ALL_POST      = NULL;
	$data           = NULL; 
	include_once ( DIR_FS_INCLUDES .'/services.inc.php'); 
	
	$lstMonthDuration=array();
	$y=12;
	$monthLimit = 120;
	for($y;$y<=$monthLimit;$y=$y+12){
		$lstMonthDuration[$y] = $y.' Months';
	}
	
	
	if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST 	= $_POST;
		$data		= processUserData($_ALL_POST);
			   
		$extra = array( 'db' 				=> &$db,
						'access_level'      => $access_level,
						'messages'          => &$messages
					);
			
		if ( PremiumEmailService::validateAdd($data, $extra) ) { 
			
			if(!empty($data['fr_dt'])){
				$fr_dt1 = explode('/', $data['fr_dt']);            
                $fr_dt2 = mktime(0, 0, 0, $fr_dt1[1], $fr_dt1[0], $fr_dt1[2]);
				$fr_dt  = date('Y-m-d H:i:s', $fr_dt2) ;
				
				$to_dt_months = $fr_dt1[1] + $data['period_in_months'];
				$to_dt2 = mktime(0, 0, 0, $to_dt_months, $fr_dt1[0], $fr_dt1[2]);
				$to_dt  = date('Y-m-d H:i:s', $to_dt2) ;				
			}			 	 
			
			$query	= " INSERT INTO ".TABLE_PREMIUM_EMAIL_SERVICE
				." SET ".TABLE_PREMIUM_EMAIL_SERVICE .".domain_id = '".$data['domain_id'] ."'"  
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".client = '".$data['client'] ."'"                            
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".service_id = '".ENTERPRISE_EMAIL_SERVICE_ID ."'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".total_allotment = '".$data['total_allotment'] ."'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".ss_type = '". Services::ENTERPRISE_EMAIL_SERVICE ."'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".fr_dt = '".$fr_dt."'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".to_dt = '".$to_dt."'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".period_in_months = '".$data['period_in_months'] ."'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".price_per_month = '".$data['price_per_month'] ."'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".discount = '".$data['discount'] ."'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".status = '1'"                          
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".created_by = '".	$my['uid'] ."'"                                
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".created_by_name = '".$my['f_name']." ".$my['l_name']."'"                                
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".ip = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
				.",". TABLE_PREMIUM_EMAIL_SERVICE .".do_e = '". date('Y-m-d')."'";
			
			if ( $db->query($query) && $db->affected_rows() > 0 ) {
				$messages->setOkMessage("New record has been done.");
				$variables['hid'] = $db->last_inserted_id();              
			}
			//to flush the data.
			$_ALL_POST	= NULL;
			$data		= NULL;
		}
	}               
	
	
	$lst_service    = NULL;
	$condition_query1= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 AND ss_id='".ENTERPRISE_EMAIL_SERVICE_ID."' ORDER BY sequence_services ASC"; 
	Services::getList($db, $lst_service, 'ss_division, ss_id, ss_title, ss_punch_line,tax1_id, tax1_name, tax1_value, is_renewable', $condition_query1);
	if(!empty($lst_service)){
		$lst_service = $lst_service[0];
	}
	 
	/* 
	$condition_query = '';
	if(!empty($lst_service)){
		foreach($lst_service as $val =>$key ){
				$ss_id= $key['ss_id'];
				$ss_div_id= $key['ss_division'];
				$ss_title= $key['ss_title'];
				$ss_punch_line= $key['ss_punch_line'];
				$tax1_id = $key['tax1_id'];
				$tax1_name = $key['tax1_name'];
				$tax1_value = $key['tax1_value'];
				$is_renewable = $key['is_renewable'];
				$tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
				$keyNew['ss_div_id'] = $ss_div_id ; 
				$keyNew['ss_id'] = $ss_id ; 
				$keyNew['ss_title'] = $ss_title ; 
				$keyNew['ss_punch_line'] = $ss_punch_line ; 
				$keyNew['tax1_id'] = $tax1_id ; 
				$keyNew['tax1_name'] = $tax1_name ; 
				$keyNew['tax1_value'] = $tax1_value ; 
				$keyNew['tax1_pvalue'] = $tax1_pvalue ; 
				$keyNew['is_renewable'] = $is_renewable ; 
				$lst_service[$val] = $keyNew;
		}
	} */
	$domainList = array();
	if(!empty($data['client'])){
		$query =" SELECT DISTINCT(".TABLE_CLIENT_DOMAINS.".id),".TABLE_CLIENT_DOMAINS.".domain FROM 
		".TABLE_CLIENT_DOMAINS." WHERE ".TABLE_CLIENT_DOMAINS.".client='".$data['client']."' AND ".TABLE_CLIENT_DOMAINS.".status='1'";		
		$db->query($query);
		if( $db->nf() > 0 ){
			while($db->next_record()){
				$did	=	$db->f("id");
				$domain	=	$db->f("domain");
				$domainList[$did] = $domain;
			}
		}
	}
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
		 header("Location:".DIR_WS_NC."/premium-email-service.php?perform=add&added=1");
	}
	if(isset($_POST['btnCancel'])){
		 header("Location:".DIR_WS_NC."/premium-email-service.php");
	}
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
		header("Location:".DIR_WS_NC."/premium-email-service.php?added=1");   
	}else{	
		$hidden[] = array('name'=> 'perform' ,'value' => 'enterprise_add');
		$hidden[] = array('name'=> 'act' , 'value' => 'save');           
		
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');  
		$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');		
		$page["var"][] = array('variable' => 'lstMonthDuration', 'value' => 'lstMonthDuration');		
		$page["var"][] = array('variable' => 'domainList', 'value' => 'domainList');		
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'premium-email-service-enterprise-add.html');
	}
?>