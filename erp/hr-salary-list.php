<?php
if ( $perm->has('nc_hr_sal_list') ) {

    
    
    
	
	if(empty($condition_query)){
		 
		if ( $where_added ) {
            $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$_SEARCH["chk_status"]  = "AND";
		 
		$_SEARCH["sStatus"]   = array(HrSalary::DEACTIVE);     
		$condition_query .= " ".TABLE_HR_SALARY.".status IN('".HrSalary::DEACTIVE."') ";
	}
        
	 //$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    $condition_query1 = " LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id=".TABLE_HR_SALARY.".user_id ".$condition_query;
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
	
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	HrSalary::getDetails( $db, $list, '', $condition_query1);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_HR_SALARY.'.*'.','.TABLE_USER.'.f_name'.','.TABLE_USER.'.l_name';
    HrSalary::getDetails( $db, $list, $fields, $condition_query1, $next_record, $rpp);
    
    // Set the Permissions.
   
    if ( $perm->has('nc_hr_sal_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_hr_sal_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_hr_sal_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_hr_sal_delete') ) {
        $variables['can_delete'] = true;
    }
    if ( $perm->has('nc_hr_sal_details') ) {
        $variables['can_view_details'] = true;
    }
    
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-salary-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
