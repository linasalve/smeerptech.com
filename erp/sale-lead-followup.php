<?php   
if( $perm->has('nc_ld_flw')){
    $lead_id = isset ($_GET['lead_id']) ? $_GET['lead_id'] : ( isset($_POST['lead_id'] ) ? $_POST['lead_id'] :'');
    //$tbl_name = isset ($_GET['tbl_name']) ? $_GET['tbl_name'] : ( isset($_POST['tbl_name'] ) ? $_POST['tbl_name'] :'');
    $followup_type = isset ($_GET['followup_type']) ? $_GET['followup_type'] : ( isset($_POST['followup_type'] ) ? $_POST['followup_type'] :'');
    
    //include ( DIR_FS_INCLUDES .'/followup.inc.php');
    $_ALL_POST	    = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
    /*
    if ( $perm->has('nc_ld_flw') ) {
        $access_level += 1;
    }
    */
    // Read the record details
    $fields = TABLE_SALE_LEADS .'.*'  ;            
    $condition_query = " WHERE (". TABLE_SALE_LEADS .".lead_id = '". $lead_id ."' )";
    /*
    $condition_query .= " AND ( ";
    // If my has created this Order.
    $condition_query .= " (". TABLE_TASK_REMINDER .".created_by = '". $my['user_id'] ."' "
                            ." AND ". TABLE_TASK_REMINDER .".access_level < $access_level ) ";              
    // Check if the User has the Right to Edit Orders created by other Users.
   
    if ( $perm->has('nc_ts_edit_ot') ) {
        $access_level_o   = $my['access_level'];
        if ( $perm->has('nc_ts_edit_ot_al') ) {
            $access_level_o += 1;
        }
        $condition_query .= " OR ( ". TABLE_TASK_REMINDER. ".created_by != '". $my['user_id'] ."' "
                            ." AND ". TABLE_TASK_REMINDER .".access_level < $access_level_o ) ";
    }
    $condition_query .= " )";
    */
    
 	if ( Leads::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ){
        
        $_ALL_POST = $_ALL_POST['0'];
                
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=12;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        }        
        $_ALL_POST['date'] = date('d/m/Y');
        
        $lst_typ =array('1'=>'AM','2'=>'PM');
        
		$followup_no = $_ALL_POST['followup_no'];
        $created_by = $_ALL_POST['created_by'];
        $lead_assign_to = $_ALL_POST['lead_assign_to'];
        $access_level = $_ALL_POST['access_level'];
        
        //$allottedtoArr = explode(",", $allotted_to) ;
        //array_push($allottedtoArr,$created_by) ;
        // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null;
        //$allotted_to_str= str_replace(',',"','",$allotted_to);
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        $my_condition_query = " ";
        User::getList($db,$lst_executive,$fields,$my_condition_query);
        // EO:  Read the Team members list ie member list in dropdown box  EOF 
		
		// Read the available titles
       	Leads::getFollowupTitle($db,$titles);
		
            if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
			
                  $_ALL_POST  = $_POST;
				  
                  $data       = processUserData($_ALL_POST);
            
                    $extra = array( 'db'        => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
                if ( Followup::validateFollowupAdd($data, $extra) ) {
                    // insert into followup of lead
                    if ( isset($data['date']) && !empty($data['date']) ) {
                         $data['date'] = explode('/', $data['date']);
                         $data['date'] =$data['date'][2]."-".$data['date'][1]."-".$data['date'][0] ;
                    }
                    
                    $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                            ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $followup_no ."'"
                                .",". TABLE_SALE_FOLLOWUP .".title_id 		= '". $data ['title_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".remarks 		= '". $data ['remarks'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".date    		= '". $data['date'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".time    		= '". $data ['hrs'].':'.$data ['min'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".time_type 		= '". $data ['time_type'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".lead_id    	= '". $data ['lead_id'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data ['lead_id'] ."'"
                                //.",". TABLE_SALE_FOLLOWUP .".assign_to 		= '". $data ['assign_to'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $data ['lead_id'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".table_name    	= 'sale_leads'"
								.",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
								.",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
								.",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                                .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". $followup_type ."'"
                                .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
                    $db->query($query); 
					/*
					$query	= " INSERT INTO ".TABLE_SALE_ASSIGN_HISTORY
                            			." SET ".TABLE_SALE_ASSIGN_HISTORY .".lead_id = '". $data['lead_id'] ."'"  
                                		.",". TABLE_SALE_ASSIGN_HISTORY .".assign_to  = '".	$data['assign_to'] ."'"
										.",". TABLE_SALE_ASSIGN_HISTORY .".created_by = '".	$my['uid'] ."'"
										.",". TABLE_SALE_ASSIGN_HISTORY .".do_assign  = '".	date('Y-m-d')."'";
                	
					$db->query($query);                      
                    */
                }
   			}
             // get list of all comments of change log BOF
            $condition_query1 = $condition_url1 =$extra_url= '';
            $condition_url1 .="&perform=".$perform."&lead_id=".$lead_id;
            $perform = 'followup';
            $followupLog	= 	NULL;
            $fields = TABLE_SALE_FOLLOWUP.".id " ;
            $condition_query1 = " WHERE ".TABLE_SALE_FOLLOWUP.".followup_no = '".$followup_no."' AND lead_id='".$lead_id."'" ;
            $condition_query1 .= " ORDER BY ".TABLE_SALE_FOLLOWUP.".do_e DESC";
            $total	=	Followup::getDetailsCommLog( $db, $followupLog, $fields , $condition_query1);
           
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
            //$extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  .= "&perform=".$perform;
            $extra_url  .= "&lead_id=".$lead_id ;
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            $followupLog	= NULL;
            $fields = TABLE_SALE_FOLLOWUP .'.*, '.TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name'.','.TABLE_SALE_FOLLOWUP_TITLE .'.title';   
            Followup::getDetailsCommLog( $db, $followupLog, $fields, $condition_query1, $next_record, $rpp);
            $condition_url .="&perform=".$perform;
            
            $fList=array();
            if(!empty($followupLog)){
                foreach( $followupLog as $key=>$val){  
                
                   $executive=array();
                   $string = str_replace(",","','", $val['assign_to']);
                   $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                   $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                   User::getList($db,$executive,$fields1,$condition1);
                   $executivename='';
                  
                   foreach($executive as $key1=>$val1){
                        $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
                   } 
                   $val['to_id'] = $executivename ;
                   if(!empty($val['time_type'])){
                       $val['time_type_name'] = $lst_typ[$val['time_type']] ;
                   }
                   $fList[$key]=$val;
                }
            }
            
            $hidden[] = array('name'=> 'lead_id', 'value' => $lead_id);
            $hidden[] = array('name'=> 'followup_type', 'value' => $followup_type);
            $hidden[] = array('name'=> 'perform', 'value' => 'followup');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'followupLog', 'value' => 'fList');
            $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'lst_typ', 'value' => 'lst_typ');
			
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive'); 
			$page["var"][] = array('variable' => 'titles', 'value' => 'titles'); 
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-followup.html');
        
        /*}else{
        
           $messages->setErrorMessage("You do not have the Permission to view the details.");*/
    }
   	else{
        $messages->setErrorMessage("Records not found.");
    }
    
}else{
    $messages->setErrorMessage("You do not have the Permission to access this module.");
}   
   
        
        
    
?>