<?php

    if ( $perm->has('nc_ld_qt_list') ) {
        include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
		
        if ( !isset($condition_query) || $condition_query == '' ) {
            /*
            $condition_query = '';
            $time   = time();
            $l_mon  = strtotime('-1 month', $time);
            $from   = date('Y-m-d 00:00:00', $l_mon );
            $to     = date('Y-m-d 23:59:59', $time);
            
            $condition_query = " WHERE ". TABLE_LEADS_QUOTATION .".do_i <= '". $to ."' "
                                ." AND ("
                                    . TABLE_LEADS_QUOTATION .".status = '". LeadsQuotation::BLOCKED ."'"
                                    ." OR ". TABLE_LEADS_QUOTATION .".status = '". LeadsQuotation::ACTIVE ."'"
                                    ." OR ". TABLE_LEADS_QUOTATION .".status = '". LeadsQuotation::PENDING ."'"
                                    ." OR ". TABLE_LEADS_QUOTATION .".status = '". LeadsQuotation::DELETED ."'"
                                .")";
            
            $_SEARCH["date_from"]   = date('d/m/Y', $l_mon);
            $_SEARCH["chk_date_to"] = 'AND';
            $_SEARCH["date_to"]     = date('d/m/Y', $time);*/
            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(LeadsQuotation::COMPLETED,LeadsQuotation::PENDING);            
            
            $condition_query = " WHERE (". TABLE_LEADS_QUOTATION .".status = '". LeadsQuotation::COMPLETED ."' OR ". TABLE_LEADS_QUOTATION .".status = '". LeadsQuotation::PENDING."')"   ;
        }
        
        // If the LeadsQuotation is created by the my.
         $access_level   = $my['access_level'] +1;
		 
       
        
        if ( $perm->has('nc_ld_qt_list_all') ) {
       
            $condition_query .='';
             
        }else{
			
			// If my has created this Invoice.
			/*  $condition_query .= " ( ". TABLE_LEADS_QUOTATION .".created_by = '". $my['user_id'] ."' "
									." AND ". TABLE_LEADS_QUOTATION .".access_level < $access_level ) ";
			$condition_query .= " OR ( ( "
										. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
										. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
										. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
										. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
									. " ) "
									." AND ". TABLE_LEADS_ORDERS .".access_level < $access_level ) "; 
			*/
			if(empty($_SEARCH["search_executive_id"]) || empty($_SEARCH["user_id"]) ){
				$condition_query .= " AND ( ";
				$condition_query .= " ( ". TABLE_LEADS_QUOTATION .".created_by = '". $my['user_id'] ."' )";
				$condition_query .= " OR ( ".TABLE_LEADS_ORDERS .".team LIKE '%,". $my['user_id'] .",%' "."  ) ";
				
				/*
				$condition_query .= " OR (  "
											. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
										. " ) "
										."  ";
				*/
				
				// Check if the User has the Right to view Invoices created by other Users.
				/*  
				if ( $perm->has('nc_bl_inv_list_ot') ) {
					$access_level_o   = $my['access_level'];
					if ( $perm->has('nc_bl_inv_list_ot_al') ) {
						$access_level_o += 1;
					}
					$condition_query .= " OR ( ". TABLE_LEADS_QUOTATION. ".created_by != '". $my['user_id'] ."' "
										." AND ". TABLE_LEADS_QUOTATION .".access_level < $access_level_o ) ";
				} */
				$condition_query .= " ) ";
			}
        }
     
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
        
            $extra_url  = $condition_url;
            
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }        
        $condition_url .="&perform=".$perform;
        
		$totalAmt =array();
		$totalAmount='';
		$fields=" SUM(".TABLE_LEADS_QUOTATION.".amount_inr) as totalAmount ";
		LeadsQuotation::getDetails(  $db, $totalAmt, $fields, $condition_query);
		if(!empty($totalAmt)){
			$totalAmount=$totalAmt[0]['totalAmount'];
		}
		
        //Code to calculate total of Amount EOF
        
        // To count total records.
        $list	= 	NULL;
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
        if($searchStr==1){
           $fields = TABLE_LEADS_QUOTATION .'.id'
                        .','. TABLE_LEADS_QUOTATION .'.number'
                        .','. TABLE_LEADS_QUOTATION .'.or_no'
                        .','. TABLE_LEADS_QUOTATION .'.access_level'
                        .','. TABLE_LEADS_QUOTATION .'.do_e'
                        .','. TABLE_LEADS_QUOTATION .'.do_c'
                        .','. TABLE_LEADS_QUOTATION .'.do_i'
                        .','. TABLE_LEADS_QUOTATION .'.currency_abbr'
                        .','. TABLE_LEADS_QUOTATION .'.amount'
                        .','. TABLE_LEADS_QUOTATION .'.amount_inr'
                        .','. TABLE_LEADS_QUOTATION .'.balance'
                        .','. TABLE_LEADS_QUOTATION .'.balance_inr'
                        .','. TABLE_LEADS_QUOTATION .'.status'
                        .','. TABLE_LEADS_QUOTATION .'.is_old'
                        .','. TABLE_LEADS_QUOTATION .'.old_updated'
                        .','. TABLE_LEADS_QUOTATION .'.reason_of_delete'
						.','. TABLE_LEADS_QUOTATION .'.attach_file1'
                        .','. TABLE_LEADS_QUOTATION .'.attach_file2'
                        .','. TABLE_LEADS_QUOTATION .'.attach_file3'
                        .','. TABLE_LEADS_QUOTATION .'.attach_file4'
                        .','. TABLE_LEADS_QUOTATION .'.attach_file5'
                        .','. TABLE_LEADS_ORDERS .'.order_title'
                        .','. TABLE_LEADS_ORDERS .'.id as order_id'
						.','. TABLE_LEADS_ORDERS .'.team'
                        .','. TABLE_SALE_LEADS .'.lead_id AS c_user_id'
                        .','. TABLE_SALE_LEADS .'.number AS c_number'
                        .','. TABLE_SALE_LEADS .'.org'
                        .','. TABLE_SALE_LEADS .'.f_name AS c_f_name'
                        .','. TABLE_SALE_LEADS .'.l_name AS c_l_name'
                        .','. TABLE_SALE_LEADS .'.email AS c_email'
						.','. TABLE_SALE_LEADS .'.email_1'
						.','. TABLE_SALE_LEADS .'.email_2'
						.','. TABLE_SALE_LEADS .'.mobile1'
						.','. TABLE_SALE_LEADS .'.mobile2'
						.','. TABLE_SALE_LEADS .'.mobile3'       
                        .','. TABLE_SALE_LEADS .'.billing_name AS billing_name'
                        .','. TABLE_SALE_LEADS .'.status AS c_status';
            $total	= LeadsQuotation::getDetails( $db, $list, '', $condition_query);        
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');            
            $list	= NULL;
            
            LeadsQuotation::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        $regionlead     = new RegionLead('91');
        $phonelead      = new PhoneLead(TABLE_SALE_LEADS);
         
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){               
               $val['is_rcpt_created'] = false;
               $val['is_paid_fully'] = false;
                 
                // check whether rcpt created or not EOF
                if($val['balance'] <= 0){
                    $val['is_paid_fully'] = true;
                }
				//check invoice id followup in ST BOF
				$val['ticket_id']='';
				$sql2 = "SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
				(".TABLE_LD_TICKETS.".flw_ord_id = '".$val['order_id']."') LIMIT 0,1";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                        }
                    }                   
                }
				//check invoice id followup in ST EOF
				$phonelead->setPhoneOf(TABLE_SALE_LEADS, $val['c_user_id']);
				$val['phone'] = $phonelead->get($db);
				
				// Read the Addresses.
				$regionlead->setAddressOf(TABLE_SALE_LEADS, $val['c_user_id']);
				$val['address_list'] = $regionlead->get();
                $val['amount']=number_format($val['amount'],2);
                $val['amount_inr']=number_format($val['amount_inr'],2);
                $val['balance']=number_format($val['balance'],2);
                $val['balance_inr']=number_format($val['balance_inr'],2);
                $val['team'] = trim($val['team'],",");
                $team = str_replace(',',"','",$val['team']);
				$sql= " SELECT f_name,l_name FROM ".TABLE_USER." WHERE ".TABLE_USER.".user_id IN ('".$team."') ";
                $db->query($sql);               
				$team_members ='';
                if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$team_members .= $db->f('f_name')." ". $db->f('l_name')." <br/>" ;
					}
                }     
				$team_members =trim($team_members,",");
				$val['team_members']=$team_members ;
                $flist[$key]=$val;
                
            }
        }
       
       
        // Set the Permissions.
        $variables['can_view_list']     = false;
		$variables['can_view_list_all'] = false;
        $variables['can_add']           = false;
        
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_send_invoice'] = false;
        $variables['can_followup']= false;

        if ( $perm->has('nc_ld_qt_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_ld_qt_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_ld_qt_edit') ) {
           $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_ld_qt_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_ld_qt_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_ld_qt_status') ) {
            $variables['can_change_status']     = true;
        }
        if ( $perm->has('nc_ld_qt_send') ) {
            $variables['can_send_invoice']     = true;
        }
        
        if ( $perm->has('nc_ldt') && $perm->has('nc_ldt_flw_ord') ) {
            $variables['can_followup'] = true;
        }
        if ( $perm->has('nc_ld_qt_list_all') ) {
			$variables['can_view_list_all'] = true;
		}
        $page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount');
        $page["var"][] = array('variable' => 'total', 'value' => 'total');
        
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-quotation-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>