<?php
    if ( $perm->has(DELETE) ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
       
        
        $extra = array( 'db'           => &$db,
                        'messages'     => &$messages
                        );
        Webaccess::delete($id, $extra);
        
        // Display the list.
        include ( DIR_FS_ADMIN .'/web-access-list.php');
    }
    else {
        //$messages->setErrorMessage("You donot have the Right to Delete the entry.");
         $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>