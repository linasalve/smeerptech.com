<?php

    if ( $perm->has('nc_sl_ld_q_add') ) {
        
        $q_id        = isset($_GET["q_id"]) ? $_GET["q_id"] : ( isset($_POST["q_id"]) ? $_POST["q_id"] : '' );
        //$tbl_name    = isset($_GET["tbl_name"]) ? $_GET["tbl_name"] : ( isset($_POST["tbl_name"]) ? $_POST["tbl_name"] : '' );
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
	    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
        //include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/terms-condition.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/quotation-subject.inc.php' );
        
        $_ALL_POST	= NULL;
        $data       = NULL;
       
        
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);
        
        $terms_condition = NULL;
        $required_fields =  '*';
        $condition_query=   'ORDER BY sequence';
		TermsCondition::getList($db,$terms_condition,$required_fields,$condition_query);
        
        //arrange array of validity 
        $validity = Quotation::getValidity() ;
        $validityType = Quotation::getValidityType() ;
      
        /*
        if ( $perm->has('nc_sl_ld_qt_add_al') ) {
            $al_list    = getAccessLevel($db, $my['access_level']);
            //$role_list  = Lead::getRoles($db, $my['access_level']);
        }
        else {
            $al_list    = getAccessLevel($db, ($my['access_level']-1));
            //$role_list  = Lead::getRoles($db, ($my['access_level']-1));
        }
        */
        
        // code for particulars bof 
        // Read the Services.
        include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
        include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');

        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC";
        Services::getList($db, $lst_service, 'ss_id,ss_title,ss_punch_line, tax1_name, tax1_value ,is_renewable', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
                    $ss_id= $key['ss_id'];
                    $ss_title= $key['ss_title'];
                    $ss_punch_line= $key['ss_punch_line'];
                    $tax1_name = $key['tax1_name'];
                    $tax1_value = $key['tax1_value'];
                    $is_renewable = $key['is_renewable'];
                    $tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );                    
                    $keyNew['ss_id'] = $ss_id ; 
                    $keyNew['ss_title'] = $ss_title ; 
                    $keyNew['ss_punch_line'] = $ss_punch_line ; 
                    $keyNew['tax1_name'] = $tax1_name ; 
                    $keyNew['tax1_value'] = $tax1_value ; 
                    $keyNew['tax1_pvalue'] = $tax1_pvalue ; 
                    $keyNew['is_renewable'] = $is_renewable ; 
                    $lst_service[$val] = $keyNew;
            }
        }
        // code for particulars eof
        
        $terms_selected_arr = array();
        
        $condition_query = '';
        Company::getList($db, $lst_company, '*', $condition_query);
        
        $condition_qs_query    = " WHERE ".TABLE_SETTINGS_QUOT_SUBJECT.".status='".QuotationSubject::ACTIVE."'";
        //Read the available quotation subject
        QuotationSubject::getList($db, $lst_quot_subject, '*',$condition_qs_query);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                           'lst_service'       => $lst_service,
                         //lst_work_stage'    => $lst_work_stage,
                        );

            if ( Quotation::validateAdd($data, $extra) ) {
                 if(isset($_POST['terms'])){
                        $terms=$_POST['terms'];                
                        $termsstr=implode(",",$terms);
                        $terms_selected=$termsstr;   
                        $terms_selected_arr=explode(",",$terms_selected);
                    }
                
                //Generate Followup &lead no bof
                //$flwnumber = getCounterNumber($db,'FLW'); 
                 
                $currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                $fields1 = "abbr ";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);
              
                if(!empty($currency1)){
                   $data['currency'] = $currency1[0]['abbr'];
                }
                $data['validity'] =  $data['validity_no']." ".$data['validity_type'];

                $query="SELECT followup_no FROM ".TABLE_SALE_LEADS." WHERE lead_id= '".$data['lead_id']."'";
                $db->query($query);
                    if($db->nf()>0){
                        $db->next_record();
                        $flwnumber=$db->f('followup_no');
                    }
            
                //insert into  database 
                  $query	= " INSERT INTO ".TABLE_SALE_QUOTATION
                            ." SET ". TABLE_SALE_QUOTATION .".number = '".         	$data['number'] ."'"
                                .",". TABLE_SALE_QUOTATION .".company_id = '".     	$data['company_id'] ."'"
                                .",". TABLE_SALE_QUOTATION .".lead_id = '".     	$data['lead_id'] ."'"
                                .",". TABLE_SALE_QUOTATION .".parent_q_id = '".    	$data['q_id'] ."'"
							    .",". TABLE_SALE_QUOTATION .".closed_by = '".     	$my['uid'] ."'"
                                .",". TABLE_SALE_QUOTATION .".subject = '".     	$data['subject'] ."'"                               
                                .",". TABLE_SALE_QUOTATION .".currency_id = '".     $data['currency_id'] ."'"                               
                                .",". TABLE_SALE_QUOTATION .".currency = '".     	$data['currency'] ."'"                               
                                .",". TABLE_SALE_QUOTATION .".amount = '".     	    $data['amount'] ."'"                               
                                .",". TABLE_SALE_QUOTATION .".amount_words = '".    $data['amount_words'] ."'"                               
                                .",". TABLE_SALE_QUOTATION .".validity = '".        $data['validity'] ."'"   
                                .",". TABLE_SALE_QUOTATION .".terms_condition = ',". $termsstr .",'"                                                               
                                .",". TABLE_SALE_QUOTATION .".access_level = '".    $my['access_level'] ."'"
                                .",". TABLE_SALE_QUOTATION .".created_by = '".     	$my['uid'] ."'"
                                .",". TABLE_SALE_QUOTATION .".status = '0' "
	                            .",". TABLE_SALE_QUOTATION .".do_e = '".         	date('Y-m-d H:i:s') ."'" ;              
            
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $variables['hid'] = $db->last_inserted_id();
                    //After insert, update lead counter in 
                    
                    updateCounterOf($db,'QT',$data['company_id']);
                    
                    $followup_query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                                    ." SET ".TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"  
                                    //.",". TABLE_SALE_FOLLOWUP .".assign_to  		= '".	$_POST['lead_assign_to'][$value]  ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".remarks 			= 'A copy of quotation is created' "
                                    .",". TABLE_SALE_FOLLOWUP .".ip         	    = '". $_SERVER['REMOTE_ADDR'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".lead_id    	    = '". $data['lead_id'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".followup_of        = '". $variables['hid'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".table_name    	    = 'sale_quotation'"
                                    .",". TABLE_SALE_FOLLOWUP .".created_by 		= '". $my['uid'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".access_level 		= '". $my['access_level'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".created_by_dept 	= '". $my['department'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".flollowup_type     = '". FOLLOWUP::LEADQUOT ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".do_e  				= '". date('Y-m-d')."'";
                    $db_followup = new db_local();
                    $db_followup->query($followup_query);
                    
                     // Insert the Particulars.
                    if ( !$db->query($data['query_p']) ) {
                        $messages->setErrorMessage("The Particulars were not Saved.");
                    }
                   
                    //Create Quotation BOF 
                    $condition_query = " WHERE user_id = '". $my['uid'] ."' ";
                    $userDetails      = NULL;
                    User::getList($db, $userDetails, 'f_name,l_name,desig', $condition_query) ;
                    $data['user']= $userDetails[0];
                    $data['do_e']= date('Y-m-d H:i:s');
                    
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_SALE_QUOTATION_P.".q_no = '". $data['number'] ."'";
                    Quotation::getParticulars($db, $temp, '*', $condition_query);
                    if(!empty($temp)){
                        foreach ( $temp as $pKey => $parti ) {
                          
                            $temp_p[]=array(
                                            'p_id'=>$temp[$pKey]['id'],
                                            'particulars'=>$temp[$pKey]['particulars'],
                                            'p_amount' =>$temp[$pKey]['p_amount'] ,                                   
                                            's_id' =>$temp[$pKey]['s_id'] ,  
                                            'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                            'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                            's_type' =>$temp[$pKey]['s_type'] ,                                                                
                                            's_quantity' =>$temp[$pKey]['s_quantity'] ,                                                                
                                            's_amount' =>$temp[$pKey]['s_amount'] ,                                                                
                                            'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                            'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                            'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                            'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                            'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                            'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                            'discount_type' =>$temp[$pKey]['discount_type'] ,    
                                            'is_renewable' =>$temp[$pKey]['is_renewable'] ,                                               
                                            'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                            );
                        }
                    }
                    $data['particulars']=$temp_p ;
                    include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                    $region         = new RegionLead();
                    $region->setAddressOf(TABLE_SALE_LEADS,  $data['lead_id']);
                    $_ALL_POST['address_list']  = $region->get();
                    //$address_list  = $region->get($addId);
                    $data['b_address'] = $_ALL_POST['address_list'][0] ;
                     //Read the available quotation subject
                     $condition_qs_query    = " WHERE ".TABLE_SETTINGS_QUOT_SUBJECT.".id='".$data['subject']."'";
                    QuotationSubject::getList($db, $lstsubject, 'title as subject_title',$condition_qs_query);
                    if(!empty($lstsubject)){
                        $lstsubject = $lstsubject['0'] ;
                        $data['subject_title'] =$lstsubject['subject_title'];
                    }
                    //get content of term& condition BOF
                    $specified_terms = NULL;
                    $required_fields ='title, condition_content';
                    $condition_query=" WHERE id IN(".$termsstr.")";
                    TermsCondition::getList($db,$specified_terms,$required_fields, $condition_query);
                    
                    //$data['terms'] = '';
                    $data['terms'] = $specified_terms;
                    //get content of term & condition EOF
                    
                    
                     // Create the Invoice PDF, HTML in file.
                    $extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';                   
                    if ( !($attch = Quotation::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Quotation file was not created.");
                    }
                    //Create Quotation EOF            
                    $data['q_no'] =$data['number'];
                    //Create Quotation in PDF BOF                        
                        $data['content'] = getQuotationContent($s, $db, $data, $extra);
                        createQuotationPDF( $data['q_no'], $data );                        
                    //Create Quotation in PDF EOF
                    
                   
                    $messages->setOkMessage("The New Quotation Entry has been done."); 
                    $condition_query='';
                    $_ALL_POST = null;
                    $data =null;
                    //$_ALL_POST['number'] = getCounterNumber($db,'QT',$data['company_id']); 
                    $_ALL_POST['exchange_rate'] = 1; 
                  
                }
            }      
        }
        else{
           
            //Get data of q_id
            $condition_queryd= " WHERE ".TABLE_SALE_QUOTATION .'.id='.$q_id ;
            $fields ='*';
            if ( Quotation::getDetails($db, $_ALL_POST, $fields, $condition_queryd) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                $number = $_ALL_POST['number'];
                $lead_id = $_ALL_POST['lead_id'];
               
                // Read the Particulars.
                    $temp_p = NULL;
                    $condition_query_p ='';
                    $service_idArr= array();
                    $condition_query_p = "WHERE ".TABLE_SALE_QUOTATION_P.".q_no = '". $_ALL_POST['number'] ."'";
                    Quotation::getParticulars($db, $temp_p, '*', $condition_query_p);
                    if(!empty($temp_p)){
                        foreach ( $temp_p as $pKey => $parti ) {
                            
                            $_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];
                            $_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
                            $_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];
                            $_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
                            $_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
                            $_ALL_POST['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];
                            $_ALL_POST['s_amount'][$pKey]   = $temp_p[$pKey]['s_amount'];
                            $_ALL_POST['ss_title'][$pKey]   = $temp_p[$pKey]['ss_title'];
                            $_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];
                            $_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
                            $_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
                            $_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
                            $_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
                            $_ALL_POST['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
                            $_ALL_POST['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
                            $_ALL_POST['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
                            $_ALL_POST['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];
                            $service_idArr[] = $temp_p[$pKey]['s_id'];
                        }
                    }
                    $_ALL_POST['service_id'] = $service_idArr;
                    //$_ALL_POST['number'] = getCounterNumber($db,'QT'); 
                    $_ALL_POST['exchange_rate'] = 1; 
                    $terms_selected_arr=explode(",",$_ALL_POST['terms_condition']);
                
            }else{
                $messages->setErrorMessage("Quotation was not found.");
            
            }
            
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/sale-quotation-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'copy');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'q_id', 'value' => $q_id);
            $hidden[] = array('name'=> 'lead_id', 'value' => $lead_id);
            //$hidden[] = array('name'=> 'tbl_name', 'value' => $tbl_name);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
            $page["var"][] = array('variable' => 'validity', 'value' => 'validity');
            $page["var"][] = array('variable' => 'validityType', 'value' => 'validityType');
            $page["var"][] = array('variable' => 'terms_condition', 'value' => 'terms_condition');
            $page["var"][] = array('variable' => 'terms_selected_arr', 'value' => 'terms_selected_arr');
            $page["var"][] = array('variable' => 'lst_quot_subject', 'value' => 'lst_quot_subject');
            //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
            //$page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
			$page["var"][] = array('variable' => 'my', 'value' => 'my');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-quotation-copy.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>