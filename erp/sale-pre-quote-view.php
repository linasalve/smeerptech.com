<?php

    if ( $perm->has('nc_sl_pq_details') ) {
        $pq_id = isset($_GET['pq_id']) ? $_GET['pq_id'] : ( isset($_POST['pq_id'] ) ? $_POST['pq_id'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_sl_pq_details_al') ) {
            $access_level += 1;
        }
		
		$query="SELECT ".TABLE_SALE_PRE_QUOTE_P.".* , ".TABLE_SALE_PRE_QUOTE.". number , ".TABLE_SALE_PRE_QUOTE.". subject , ".TABLE_SALE_PRE_QUOTE.".do_e
				FROM ".	TABLE_SALE_PRE_QUOTE_P.
                " LEFT JOIN ". TABLE_SALE_PRE_QUOTE." ON ".TABLE_SALE_PRE_QUOTE_P.".pq_no= ".TABLE_SALE_PRE_QUOTE.".number
                 WHERE ".TABLE_SALE_PRE_QUOTE.".id='".$pq_id."'";
							
        if ( ($db->query($query)) > 0 ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$list[] = processSqlData($db->result());
				}
			}		
/*        if ( Lead::getList($db, $list, '*', " WHERE user_id = '$user_id'") > 0 ) {*/
            $_ALL_POST = $list;

            if ( $_ALL_POST['access_level'] < $access_level ) {
                //include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                //include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
                //include_once ( DIR_FS_CLASS .'/UserReminder.class.php');

                //$regionlead         = new RegionLead();
                //$phonelead          = new PhoneLead(TABLE_SALE_LEADS);
                //$reminder       = new UserReminder(TABLE_LEADS);

                //$_ALL_POST['manager'] = Lead::getManager($db, '', $_ALL_POST['manager']);

                // Read the available Access Level and User Roles.
				
				$_ALL_POST['access_level'];
                $_ALL_POST['access_level']  = PreQuote::getAccessLevel($db, $access_level, $_ALL_POST['access_level']);
                //$_ALL_POST['roles']         = Lead::getRoles($db, $access_level, $_ALL_POST['roles']);
                //print_r($role_list);
                // Read the Contact Numbers.
                //$phonelead->setPhoneOf(TABLE_SALE_LEADS, $lead_id);
                //$_ALL_POST['phone'] = $phonelead->get($db);
                
                // Read the Addresses.
                //$regionlead->setAddressOf(TABLE_SALE_LEADS, $lead_id);
                //$_ALL_POST['address_list'] = $regionlead->get();
                
                // Read the Reminders.
                //$reminder->setReminderOf(TABLE_SALE_LEADS, $user_id);
                //$_ALL_POST['reminder_list'] = $reminder->get($db);
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    			//$page["var"][] = array('variable' => 'source_title_from', 'value' => 'source_title_from');
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-pre-quote-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Pre Quote with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("The Pre Quote details were not found. ");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>