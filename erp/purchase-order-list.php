<?php
if ( $perm->has('nc_po_list') ) {

	
	if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
	if(empty($condition_query)){
		/* 
		$_SEARCH["chk_fstatus"]  = 'AND';
		$_SEARCH["fStatus"]    = array(PurchaseOrder::FORCEDEACTIVE);
		$_SEARCH["chk_bstatus"]  = 'AND';
		$_SEARCH["bStatus"] = array(PurchaseOrder::BILL_STATUS_UNPAID,PurchaseOrder::BILL_STATUS_PARTIALPAID);
        
		$condition_query = " WHERE ".TABLE_PURCHASE_ORDER .".f_status='". PurchaseOrder::FORCEDEACTIVE ."'
			AND ".TABLE_PURCHASE_ORDER .".bill_status 
			IN('".PurchaseOrder::BILL_STATUS_UNPAID ."','".PurchaseOrder::BILL_STATUS_PARTIALPAID ."') ";
		$perform = 'search';
        $condition_url   = "&chk_fstatus=".$_SEARCH["chk_fstatus"]."&fStatus=".PurchaseOrder::FORCEDEACTIVE ;   
		*/ 
    
	}
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
	
    // To count total records.
    $list	= 	NULL;
    $total	=	PurchaseOrder::getList( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    //$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    $pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','purchase-order.php','frmSearch');
    $list	= NULL;
    $fields = TABLE_PURCHASE_ORDER.'.*,'.TABLE_CLIENTS.'.f_name as mfname,'.TABLE_CLIENTS.'.l_name as mlname,
	'.TABLE_CLIENTS.'.billing_name as mbilling_name, ';
	$fields .= TABLE_VENDORS_BANK.".f_name as vfname,".TABLE_VENDORS_BANK.".l_name as vlname,
	".TABLE_VENDORS_BANK.'.billing_name as vbilling_name, ' ;
	$fields .= "team_table.f_name as tfname,team_table.l_name as tlname," ;
	$fields .= TABLE_USER.".f_name as ufname,".TABLE_USER.".l_name as ulname," ;
    $fields .= TABLE_PAYMENT_BILLS_AGAINST.".bill_against" ;
    PurchaseOrder::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    $flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
            $fields1 =  TABLE_SETTINGS_COMPANY .'.name as company_name';                
            $condition2 = " WHERE ".TABLE_SETTINGS_COMPANY .".id= ".$val['company_id']." " ;
            $company_name='';
            $table =TABLE_SETTINGS_COMPANY ;              
            $compArr = getRecord($table,$fields1,$condition2);
            if(!empty($compArr)){
                $company_name = $compArr['company_name']  ;   
            }
			$val['company_name']=$company_name ;
			
			$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';                
            $condition2 = " WHERE ".TABLE_USER .".user_id IN('".$val['created_by']."') " ;
           
            $table =TABLE_USER ;              
            $createdArr = getRecord($table,$fields1,$condition2);
            if(!empty($createdArr)){
                $createdBy = $createdArr['f_name']." ".$createdArr['l_name'] ;   
            }
            $val['added_by']=$createdBy ;
			$val['attachment_imap_arr'] = array();
			if(!empty($val['attachment_imap'])){
				$val['attachment_imap_arr']=explode(",",$val['attachment_imap']);
			}
			$is_particulars_selected = 0;
			$sql= " SELECT id FROM ".TABLE_PURCHASE_ORDER_P." WHERE ".TABLE_PURCHASE_ORDER_P.".bill_id =
			'".$val['id']."' LIMIT 0,1";
			$db->query($sql);               
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ) {
					$count = $db->f('id') ;
					if( $count > 0){
						$is_particulars_selected = 1;
					}
				}
			}
			$val['is_particulars_selected'] = $is_particulars_selected;
            $flist[$key] = $val;
            
        }
    }
 
    
    // Set the Permissions.
   
    $variables['can_view_list'] = false;
	$variables['can_add'] = false;
	$variables['can_edit'] = false;
	$variables['can_edit'] = false;
	$variables['can_view'] = false;
	$variables['can_change_status'] = false;
	$variables['can_create_pdf'] = false;
	$variables['can_view_pdf'] = false;
	
	
    if ( $perm->has('nc_po_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_po_add') ) {
        $variables['can_add'] = true;
		
    }
    if ( $perm->has('nc_po_edit') ) {
        $variables['can_edit'] = true;
    }
	if ( $perm->has('nc_po_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_po_view') ) {
        $variables['can_view'] = true;
    }
    
    if ( $perm->has('nc_po_status') ) {
        $variables['can_change_status'] = true;
    }
	
	if ( $perm->has('nc_pop') && $perm->has('nc_pop_add')) {
        $variables['can_create_pdf'] = true;
    }
	if ( $perm->has('nc_pop') && $perm->has('nc_pop_details')) {
        $variables['can_view_pdf'] = true;
    }
	//can view list of payment party bills transactions
	 
	 
    $variables['bills_path'] = DIR_WS_PO_FILES;
    $page["var"][] = array('variable' => 'list', 'value' => 'flist');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
	
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
