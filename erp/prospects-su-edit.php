<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_pr_su_edit') ) {
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
        $client_id	= isset($_GET["client_id"]) ? $_GET["client_id"] : ( isset($_POST["client_id"]) ? $_POST["client_id"] : '' );
        
        include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
        //include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
        //include ( DIR_FS_INCLUDES .'/sale-industry.inc.php');
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new RegionProspects();
        $phone          = new PhoneProspects(TABLE_PROSPECTS);
        //$reminder       = new UserReminder(TABLE_PROSPECTS);
        $access_level   = $my['access_level'];
        $access_level += 1;
        
        
        
        $al_list    = getAccessLevel($db, $access_level);
        
        
        //BOF read the available services
		$authorization_list  = Prospects::getAuthorization();
        $services_list  = Prospects::getServices();
        //EOF read the available services
        
        //BOF read the available industries
        /*$industry_list = NULL;
        $required_fields ='*';
        $condition_query="WHERE status='".Industry::ACTIVE."'";
		Industry::getList($db,$industry_list,$required_fields,$condition_query);*/
        //EOF read the available industries
        
        //BOF read the available industries
		Prospects::getCountryCode($db,$country_code);
        //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder
                        );
            
            if ( Prospects::validateSubUserUpdate($data, $extra) ) {
            
                $service_ids = $data['service_id'];
                $service_idstr = implode(",",$service_ids);
                
                if(!empty($service_idstr)){
                    $service_idstr=",".$service_idstr.",";	
                }
                $authorization_idstr='';
                if(isset($_POST['authorization_id']) && !empty($_POST['authorization_id'])){
                    $authorization_ids=$_POST['authorization_id'];
                    $authorization_idstr=implode(",",$authorization_ids);
                    $authorization_idstr = ",".$authorization_idstr.",";	
                }
				
				if( isset($data['check_email'])){
					$data['check_email']=1;
				}else{
					$data['check_email']=0;
				}
				
				
                $query  = " UPDATE ". TABLE_PROSPECTS
                        ." SET "
                        ."". TABLE_PROSPECTS .".service_id   = '". $service_idstr ."'"
						.",". TABLE_PROSPECTS .".authorization_id   = '". $authorization_idstr ."'"
                           /* .",". TABLE_PROSPECTS .".number      = '". $data['number'] ."'"
                            .",". TABLE_PROSPECTS .".manager     = '". $data['manager'] ."'"*/
						   //.",". TABLE_PROSPECTS .".team			= '". implode(',', $data['team']) ."'"
                           // .",". TABLE_PROSPECTS .".username    = '". $data['username'] ."'"
                            . $data['password']
                            //.",". TABLE_PROSPECTS .".access_level= '". $data['access_level'] ."'"
                            .",". TABLE_PROSPECTS .".email       = '". $data['email'] ."'"
							.",". TABLE_PROSPECTS .".check_email = '". $data['check_email'] ."'"
                            .",". TABLE_PROSPECTS .".email_1     = '". $data['email_1'] ."'"
                            .",". TABLE_PROSPECTS .".email_2     = '". $data['email_2'] ."'"
                            .",". TABLE_PROSPECTS .".title       = '". $data['title'] ."'"
                            .",". TABLE_PROSPECTS .".f_name      = '". $data['f_name'] ."'"
                            .",". TABLE_PROSPECTS .".m_name      = '". $data['m_name'] ."'"
                            .",". TABLE_PROSPECTS .".l_name      = '". $data['l_name'] ."'"
                            .",". TABLE_PROSPECTS .".p_name      = '". $data['p_name'] ."'"                            
                            //.",". TABLE_PROSPECTS .".grade      	= '". $data['grade'] ."'"
                            //.",". TABLE_PROSPECTS .".credit_limit = '". $data['credit_limit'] ."'"
                            .",". TABLE_PROSPECTS .".desig       = '". $data['desig'] ."'"
                            .",". TABLE_PROSPECTS .".org         = '". $data['org'] ."'"
                            .",". TABLE_PROSPECTS .".domain      = '". $data['domain'] ."'"
                            .",". TABLE_PROSPECTS .".gender      = '". $data['gender'] ."'"
                            .",". TABLE_PROSPECTS .".do_birth    = '". $data['do_birth'] ."'"
                            .",". TABLE_PROSPECTS .".do_aniv     = '". $data['do_aniv'] ."'"
                            .",". TABLE_PROSPECTS .".remarks     = '". $data['remarks'] ."'"
                            .",". TABLE_PROSPECTS .".mobile1      = '". $data['mobile1'] ."'"
                            .",". TABLE_PROSPECTS .".mobile2      = '". $data['mobile2'] ."'"
                            .",". TABLE_PROSPECTS .".allow_ip   = '". $data['allow_ip'] ."'"
                            .",". TABLE_PROSPECTS .".valid_ip    = '". $data['valid_ip'] ."'"
                            .",". TABLE_PROSPECTS .".status      = '". $data['status'] ."'" 
                        ." WHERE ". TABLE_PROSPECTS .".user_id   = '". $data['user_id'] ."'";
                
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['user_id'];
					//Push email id into Newsletters Table BOF
					$table = TABLE_NEWSLETTERS_MEMBERS;
					include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
					if(!empty($data['email'])){
						$domain1 = strtolower(strstr($data['email'], '@'));
						$condi1= " WHERE email='".strtolower(trim($data['email']))."'";
						if( $domain1!= 'smeerptech.com' && 
							!NewslettersMembers::duplicateFieldValue($db, $table, 'email',$condi1) ) {
							$sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
							SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email']))."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_1'])){
						$domain2 = strtolower(strstr($data['email_1'], '@'));
						$condi2= " WHERE email='".strtolower(trim($data['email_1']))."'";
						if( $domain2!='smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email',$condi2) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS." SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_1']))."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_2'])){
						$domain3 = strtolower(strstr($data['email_2'], '@'));
						$condi3= " WHERE email='".strtolower(trim($data['email_2']))."'";
						if($domain3!= 'smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi3) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_2']))."'" ;
						  $db->query($sql1);
						}
					}
					//Push email id into Newsletters Table EOF
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_PROSPECTS) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $phone_arr = array( 'id' => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
                                                    'p_type'        => $data['p_type'][$i],
                                                    'cc'            => $data['cc'][$i],
                                                    'ac'            => $data['ac'][$i],
                                                    'p_number'      => $data['p_number'][$i],
                                                    'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
                                                    'p_is_verified' => isset($data['p_is_verified'][$i]) ? $data['p_is_verified'][$i] : '0'
                                                );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_PROSPECTS, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                       $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    } 
                    $messages->setOkMessage("Prospects Profile has been updated.");
					/*SEND SERVICE PDF BOF*/
					include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
					include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
					include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
					$id=39;
					$fields = TABLE_ST_TEMPLATE .'.*'  ;            
					$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
					SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1);
					
					if(!empty($details)){
						$data['ticket_owner_uid']=$client_id;
						$table = TABLE_PROSPECTS;
						$fields1 =  TABLE_PROSPECTS.'.billing_name,'.TABLE_PROSPECTS.'.f_name,
						'.TABLE_PROSPECTS.'.l_name ';
						$condition1 = " WHERE ".TABLE_PROSPECTS .".user_id = '".$client_id."' " ;
						$arr = getRecord($table,$fields1,$condition1);
						if(!empty($arr)){
							$data['ticket_owner'] = $arr['f_name']." ".$arr['l_name'];
						}						 
						$details = $details[0];
						$data['template_id'] = $details['id'];
						$data['template_file_1'] = $details['file_1'];
						$data['template_file_2'] = $details['file_2'];
						$data['template_file_3'] = $details['file_3'];
						$data['subject'] = processUserData($details['subject']);
						$data['text'] = processUserData($details['details']);
						$ticket_no  =  ProspectsTicket::getNewNumber($db);
						$ticket_status = ProspectsTicket::PENDINGWITHCLIENTS;
						$data['display_name']=$data['display_user_id']= $data['display_designation']='';				
						$randomUser = getCeoDetails();
						$data['display_name'] = $randomUser['name'];
						$data['display_user_id'] = $randomUser['user_id'];
						$data['display_designation'] = $randomUser['desig'];
						
						/* $randomUser = getCeoDetails();
						$data['display_name'] = $randomUser['name'];
						$data['display_user_id'] = $randomUser['user_id'];
						$data['display_designation'] = $randomUser['desig']; */
						$data['display_name'] = $my['f_name']." ".$my['l_name'];
						$data['display_user_id'] = $my['user_id'];
						$data['display_designation'] = $my['desig']; 
						if($my['department'] == ID_MARKETING){
							//This is the marketing person identity
							$data['tck_owner_member_id'] = $my['user_id'];
							$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
							$data['tck_owner_member_email'] = $my['email'];
							$data['marketing_email'] = $my['email'];
							$data['marketing_contact'] = $my['marketing_contact'];
							$data['marketing'] = 1;
						}else{ 
							$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
							$data['tck_owner_member_name']= SALES_MEMBER_USER_NAME;
							$data['tck_owner_member_email'] = SALES_MEMBER_USER_EMAIL;
							$data['marketing_email'] = SALES_MEMBER_USER_EMAIL;
							$data['marketing_contact'] = '';
							$data['marketing'] = 0;
						}
						
												
						//Check that this user's ticket is exist or not
						$ticket_id = 0;
						$sql = " SELECT ticket_id,ticket_no FROM ".TABLE_PROSPECTS_TICKETS." WHERE 
							".TABLE_PROSPECTS_TICKETS.".ticket_owner_uid = '".$data['user_id']."' 
							AND ".TABLE_PROSPECTS_TICKETS.".service_pdf_tkt ='1' AND 
							".TABLE_PROSPECTS_TICKETS.".ticket_child=0 LIMIT 0,1";
						$db->query($sql) ;
						$mticket_no='';
						if($db->nf() > 0){
							$db->next_record() ;
							$ticket_id = $db->f("ticket_id") ; 
							$mticket_no = $db->f("ticket_no") ;
						}
						
						if($ticket_id>0){
							
							$query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
							. TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_PROSPECTS_TICKETS .".template_id       = '". $data['template_id'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_creator  = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_subject  = '". $data['subject'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_text     = '". $data['text'] ."', "
							. TABLE_PROSPECTS_TICKETS .".mail_to_all_su  = '".$mail_to_all_su."', "
							. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_status   = '".$ticket_status ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_child    = '".$ticket_id."', "
							. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
							. TABLE_PROSPECTS_TICKETS .".service_pdf_tkt = '1', "
							. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
							. TABLE_PROSPECTS_TICKETS .".ticket_replied  = '0', "
							. TABLE_PROSPECTS_TICKETS .".from_admin_panel = '".ProspectsTicket::ADMIN_PANEL."', "
							. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
							. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
							. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
							. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
							. TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name']."', "
							. TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
							
						
						}else{
							$mticket_no	=$ticket_no;
							$query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
							. TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_PROSPECTS_TICKETS .".template_id       = '". $data['template_id'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid = '". $my['uid'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_creator  	= '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_subject  	= '". $data['subject'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_text     	= '". $data['text'] ."', "
							. TABLE_PROSPECTS_TICKETS .".mail_to_all_su  	= '".$mail_to_all_su."', "
							. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_status   	= '".$ticket_status ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_child    	= '0', "
							. TABLE_PROSPECTS_TICKETS .".hrs1 				= '". $hrs1 ."', "
							. TABLE_PROSPECTS_TICKETS .".min1 				= '". $min1 ."', "
							. TABLE_PROSPECTS_TICKETS .".hrs 				= '". $data['hrs'] ."', "
							. TABLE_PROSPECTS_TICKETS .".min 				= '". $data['min']."', "
							. TABLE_PROSPECTS_TICKETS .".service_pdf_tkt 	= '1', "
							. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
							. TABLE_PROSPECTS_TICKETS .".ticket_replied  = '0', "
							. TABLE_PROSPECTS_TICKETS .".from_admin_panel = '".ProspectsTicket::ADMIN_PANEL."', "
							. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
							. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
							. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
							. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
							. TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name']."', "
							. TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
						}
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
						
						/* $query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
						. TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
						. TABLE_PROSPECTS_TICKETS .".template_id       = '". $data['template_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['subject'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['text'] ."', "
						. TABLE_PROSPECTS_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
						. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_status     = '".$ticket_status ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_child      = '0', "
						. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
						. TABLE_PROSPECTS_TICKETS .".service_pdf_tkt = '1', "
						. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
						. TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
						. TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
						. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
						. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
						
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						} */
						if(!empty($data['email']) || !empty($data['email_1']) || !empty($data['email_2'])){
							$db->query($query) ;
							$ticket_id = $db->last_inserted_id() ;
							$mail_send_to_su='';
							$data['mticket_no']   =   $ticket_no ;
							if ( getParsedEmail($db, $s, 'EMAIL_PST_MEMBER', $data, $email) ){
								if(!empty($data['tck_owner_member_email'])){
									$email["from_name"] =   $email["from_name"]." - ".$data['tck_owner_member_name'];
									$email["from_email"] = $data['tck_owner_member_email'];
								}
								if(!empty($data['email'])){
									//$to = '';
									$to[]   = array('name' => $data['email'] , 'email' => $data['email']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email']; 
								}
								if(!empty($data['email_1'])){
									//$to = '';
									$to[]   = array('name' => $data['email_1'] , 'email' => $data['email_1']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email_1']; 
								}
								if(!empty($data['email_2'])){
									//$to = '';
									$to[]   = array('name' => $data['email_2'] , 'email' => $data['email_2']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email_2']; 
								}
							 
							 	if(!empty($smeerp_client_email)){
									//$to = '';
									$bcc[]  = array('name' => $data['ticket_owner'] , 'email' => $smeerp_client_email);     
								}
								if(!empty($to)){
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,	$cc,$bcc,$file_name);
								}
							}
							
							if(!empty($mail_send_to_su)){
								$query_update = "UPDATE ". TABLE_PROSPECTS_TICKETS 
                                    ." SET ". TABLE_PROSPECTS_TICKETS .".mail_send_to_su
									= '".trim($mail_send_to_su,",")."'"                                   
                                    ." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_id = '". $ticket_id ."' " ;
								$db->query($query_update) ;
							}
						}
					}
					/*SEND SERVICE PDF EOF*/ 
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Prospects Profile was not updated.');
                }
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            //$client_id=$client_id
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/prospects-su-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'  => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( Prospects::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                    if ( empty($_ALL_POST['country']) ) {
                            $_ALL_POST['country'] = '91';
                    }
                    $_ALL_POST['password']='';
                    
                    if(!empty($_ALL_POST['service_id'])){
                        $_ALL_POST['service_id'] = trim($_ALL_POST['service_id'],",");
                        $_ALL_POST['service_id']    = explode(",", $_ALL_POST['service_id']);
                    }
                    if(!empty($_ALL_POST['authorization_id'])){
                        $_ALL_POST['authorization_id'] = trim($_ALL_POST['authorization_id'],",");
                        $_ALL_POST['authorization_id'] = explode(",", $_ALL_POST['authorization_id']);
                    }
                    // Check the Access Level.
                    if ( $_ALL_POST['access_level'] < $access_level ) {
                        // Read the Contact Numbers.
                        $phone->setPhoneOf(TABLE_PROSPECTS, $user_id);
                        $_ALL_POST['phone'] = $phone->get($db);
                        
                        // Read the Addresses.
                        $region->setAddressOf(TABLE_PROSPECTS, $user_id);
                        $_ALL_POST['address_list'] = $region->get();
                        
                        // Read the Reminders.
                        /*$reminder->setReminderOf(TABLE_PROSPECTS, $user_id);
                        $_ALL_POST['reminder_list'] = $reminder->get($db);*/
                    }
                    
                }
            }
 
            if ( !empty($_ALL_POST['user_id']) ) {

                // Check the Access Level.
                if ( $_ALL_POST['access_level'] < $access_level ) {

                    $phone_types    = $phone->getTypeList();
                    
                    $condition_query = " WHERE user_id = '". $client_id ."' ";
                    if ( Prospects::getList($db, $_ALL_POST['main_client_details'], 'service_id', $condition_query) > 0 ) {
                        $_ALL_POST['main_client_details'] = $_ALL_POST['main_client_details'][0];
                        $_ALL_POST['main_client_details']['service_id'] = trim($_ALL_POST['main_client_details']['service_id'],",");
                        $my_services_list = explode(",",$_ALL_POST['main_client_details']['service_id']);
                    }
                    //$address_types  = $region->getTypeList();
                    //$lst_country    = $region->getCountryList();
                    //$lst_state      = $region->getStateList();
                    //$lst_city       = $region->getCityList();
                    
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    
              
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    $hidden[] = array('name'=> 'client_id', 'value' => $client_id);
                    //$hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    //$page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
                    //$page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
                    //$page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    //$page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
                    //$page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
                    $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
					$page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
                    $page["var"][] = array('variable' => 'my_services_list', 'value' => 'my_services_list');
                    //$page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
                    $page["var"][] = array('variable' => 'allowipArr', 'value' => 'allowipArr');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-su-edit.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Client with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected Client was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>