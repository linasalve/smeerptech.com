<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
  
	   

    $order_details = isset($_GET['order_details']) ? $_GET['order_details'] : '';
    $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : '';
     
    $optionLink =$stringOpt= '' ;
    if(!empty($order_details)){
        $sString = $order_details;
    }
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if (isset($sString) && !empty($sString)){
        header("Content-Type: application/json");      
	   /* $query =" SELECT DISTINCT(".TABLE_BILL_ORDERS.".id),".TABLE_BILL_ORDERS.".order_title,".TABLE_BILL_ORDERS.".number FROM ".TABLE_BILL_ORDERS." WHERE 
		".TABLE_BILL_ORDERS.".clients_su LIKE '%,".$client_id.",%' AND
        ( ".TABLE_BILL_ORDERS.".order_title LIKE '%".$sString."%' OR ".TABLE_BILL_ORDERS.".number LIKE '%".$sString."%')  ";
        */
		
		/* $query =" SELECT DISTINCT(".TABLE_BILL_ORDERS.".id),".TABLE_BILL_ORDERS.".order_title,".TABLE_BILL_ORDERS.".number FROM ".TABLE_BILL_ORDERS." WHERE 
		(".TABLE_BILL_ORDERS.".client = '".$client_id."' OR ".TABLE_BILL_ORDERS.".clients_su LIKE '%,".$client_id.",%' ) OR 
        ( ".TABLE_BILL_ORDERS.".order_title LIKE '%".$sString."%' OR ".TABLE_BILL_ORDERS.".number LIKE '%".$sString."%')  "; */
		$query =" SELECT DISTINCT(".TABLE_BILL_ORDERS.".id),".TABLE_BILL_ORDERS.".order_title,".TABLE_BILL_ORDERS.".number FROM ".TABLE_BILL_ORDERS." WHERE ( ".TABLE_BILL_ORDERS.".order_title LIKE '%".$sString."%' OR ".TABLE_BILL_ORDERS.".number LIKE '%".$sString."%')  ";
		
		$db->query($query);
        echo "{\"results\": [";
        if ( $db->nf()>0 ) {
          
            $arr = array();
            while ($db->next_record()){
                 
                $arr[] = "{\"id\": \"".$db->f('id')."\", \"value\": \"".processSQLData($db->f('order_title'))." (".processSQLData($db->f('number')).")".""."\", \"info\": \"\"}";
            }
            echo implode(", ", $arr);
           
        }
         echo "]}";	   
	}
include_once( DIR_FS_NC ."/flush.php");


?>
