<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");

    include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/service-tax-price.inc.php' );
	
    $tax_id = isset($_GET['tax_id']) ? $_GET['tax_id'] : '';
    
   	$taxDetails = '' ;
    if (isset($tax_id) && !empty($tax_id))
    {
		//Get tax value list BOF
		$tax_vlist    = NULL;
		$condition_queryvt= " WHERE status = '". ACTIVE ."' ORDER BY tax_price ASC";
		ServiceTaxPrice::getList($db, $tax_vlist, 'id,tax_price,tax_percent', $condition_queryvt);
		//Get tax value list EOF
		$taxpOpt=$taxpOptEc =$taxpOptShe ='';
		if(!empty($tax_vlist)){
			$taxpOpt="<option value=''> </option>";
			foreach($tax_vlist as $key=>$val){
				$taxpOpt.= "<option value=".$val['tax_percent'].">".$val['tax_percent']."</option>";
				if($val['tax_percent']=="2%"){
					$taxpOptEc = "<option value=".$val['tax_percent'].">".$val['tax_percent']."</option>";
					$taxpOptEc .= "<option value=\"0.0\">0%</option>";
				}
				if($val['tax_percent']=="1%"){
					$taxpOptShe = "<option value=".$val['tax_percent'].">".$val['tax_percent']."</option>";
					$taxpOptShe .= "<option value=\"0.0\">0%</option>";
				}
			}
		}
        
		//Get tax list BOF
		$tax_list    = NULL;
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." ORDER BY tax_name ASC";
		ServiceTax::getList($db, $tax_list, 'id,tax_name', $condition_queryst);
        $taxnOpt = $taxnOptEc = $taxnOptShe = '';
		if(!empty($tax_list)){
			$taxnOpt="<option value=''> </option>";
			foreach($tax_list as $key1=>$val1){
				$tax_name =$val1['tax_name'];
				$id =$val1['id'];
				$mergeTxt = $tax_name."#".$id ;
				$taxnOpt.= "<option value=\"".$mergeTxt."\">".$tax_name."</option>";
				if($id==TAX_EC_ID){
					$taxnOptEc = "<option value=\"".$mergeTxt."\" selected=\"selected\">".$tax_name."</option>";
				}
				if($id==TAX_SHE_ID){
					$taxnOptShe = "<option value=\"".$mergeTxt."\" selected=\"selected\">".$tax_name."</option>";
				}
				
			}
			
		}
		
		$count = count($tax_list);
		
		$text="<table>
				<tr>
					<td><input type=\"hidden\" name=\"tax1_sub1_id[]\" id=\"tax1_sub1_id\"/></td>
					<td><input type=\"hidden\" name=\"tax1_sub1_value[]\"  id=\"tax1_sub1_value\"/></td>
					<td><input type=\"hidden\" name=\"tax1_sub1_amount[]\"  id=\"tax1_sub1_amount\"/></td>
				</tr>
				<tr>
					<td><input type=\"hidden\" name=\"tax1_sub2_id[]\" id=\"tax1_sub2_id\"/></td>
					<td><input type=\"hidden\" name=\"tax1_sub2_value[]\" id=\"tax1_sub2_value\"/></td>
					<td><input type=\"hidden\" name=\"tax1_sub2_amount[]\" id=\"tax1_sub2_amount\"/></td>
				</tr>
		</table>";
		if(!empty($tax_list)){
			$text="<table>";
			foreach($tax_list as $key2=>$val2){
				$i=$key2+1;
				
				if($tax_id == TAX_ST_ID){
					if($i==1){
						$taxnOpt = $taxnOptEc ;
						$taxpOpt = $taxpOptEc;
					}elseif($i==2){
						$taxnOpt = $taxnOptShe ;
						$taxpOpt = $taxpOptShe;
					}
				}
				$text.="<tr>";
				$text.="<td><select name=\"tax1_sub".$i."_id[]\" id=\"tax1_sub".$i."_id\" class=\"inputbox\" style=\"width:50px;height:25px\"
				onchange=\"updateTotal()\">".$taxnOpt."</select> </td>";
				$text.="<td><select name=\"tax1_sub".$i."_value[]\" id=\"tax1_sub".$i."_value\" class=\"inputbox\" style=\"width:50px;height:25px\"
				onchange=\"updateTotal()\">".$taxpOpt."</select> </td>";
				$text.="<td> <input type=\"text\" name=\"tax1_sub".$i."_amount[]\" id=\"tax1_sub".$i."_amount\" value=\"\" class=\"inputbox\" maxlength=\"255\" style=\"width:50px\"/>	 
						</td>";
				$text.="</tr>";
				
			}
			$text.="<table>";
		}
   	   echo $text;
	}	
	include_once( DIR_FS_NC ."/flush.php");

?>
