<?php
  if ( $perm->has('nc_dp_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the payment party class
		include_once (DIR_FS_INCLUDES .'/department.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( Department::validateAdd($data, $extra) ) { 
                            $query	= " INSERT INTO ".TABLE_SETTINGS_DEPARTMENT
                            ." SET ".TABLE_SETTINGS_DEPARTMENT .".department_name = '". $data['department_name'] ."'"  
									.",". TABLE_SETTINGS_DEPARTMENT .".department_value = '". $data['department_value'] ."'"                     
                                    .",". TABLE_SETTINGS_DEPARTMENT .".show_in_st = '". $data['show_in_st'] ."'"                     
                                	.",". TABLE_SETTINGS_DEPARTMENT .".status = '". 		$data['status'] ."'"                                
                                	.",". TABLE_SETTINGS_DEPARTMENT .".date = '". 		date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New department entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate'])){
             header("Location:".DIR_WS_NC."/department.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/department.php");
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            //include ( DIR_FS_NC .'/department-list.php');
             header("Location:".DIR_WS_NC."/department.php?added=1");
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'department-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
