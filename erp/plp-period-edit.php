<?php
    if ( $perm->has('nc_p_pr_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
      
        include_once (DIR_FS_INCLUDES .'/plp-period.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                          
                            'messages'          => &$messages
                        );
          
            if ( PlpPeriod::validateUpdate($data, $extra) ) {
              $query  = " UPDATE ". TABLE_PLP_PERIOD
                            ." SET ". TABLE_PLP_PERIOD .".plp_period_start = '".     $data['plp_period_start'] ."'"
                                .",". TABLE_PLP_PERIOD .".plp_period_end = '".       $data['plp_period_end'] ."'"
                                .",". TABLE_PLP_PERIOD .".plp_period_title = '".     $data['plp_period_title'] ."'"
                                .",". TABLE_PLP_PERIOD .".plp_period_status = '".    $data['plp_period_status'] ."'"
                            ." WHERE plp_period_id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
               // $data       = NULL;
            }
            $_ALL_POST = $data;
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_PLP_PERIOD .'.*'  ;            
            $condition_query = " WHERE (". TABLE_PLP_PERIOD .".plp_period_id = '". $id ."' )";
            
          
            if ( PlpPeriod::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                
                    // Setup the date of delivery.
                    /*$_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];*/
                    $id         = $_ALL_POST['plp_period_id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/plp-period-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'plp-period-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
