<?php

    if ( $perm->has('nc_sms_api_list') ) {
    
		// Status
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';  
        }
     
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
       
        /* $condition_query = " LEFT JOIN ".TABLE_SMS_GATEWAY." ON ".TABLE_SMS_GATEWAY.".id=".TABLE_SMS_API.".gateway_id 
        LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id=".TABLE_SMS_API.".created_by ".$condition_query; */
        // To count total r ecords.
        $list	= 	NULL;
        $fields = TABLE_SMS_API.'.id ' ;
        $total	=	SmsApi::getDetails( $db, $list, '', $condition_query);
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
        $list	= NULL;
        $fields = TABLE_SMS_API .'.*'           ;
                           
        SmsApi::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
 
        if ( $perm->has('nc_sms_api_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_sms_api_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_sms_api_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_sms_api_delete') ) {
            $variables['can_delete'] = true;
        }
        
        if ( $perm->has('nc_sms_api_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_sms_api_status') ) {
            $variables['can_change_status'] = true;
        }
       
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-api-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>