<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
  
    $searchStr = 0;
    if ( $sString != "" ){
        $searchStr = 1;
        
         // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
       
        //$where_added = true;
        $sub_query='';        
        if ( $search_table == "Any" ) 
        {
            
            if ( $where_added ) 
            {
                $condition_query .= "OR ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ) 
            {
              
                if ( $table != "Any" ) 
                { 
                    
					if($table == TABLE_NEWSLETTERS_MEMBERS){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    } 
                    
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= " AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE 
				'%".trim($sString) ."%' )" ;
                
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
    //code for search EOF 2009-03-mar-21 
     
    // EO: chk_bounce   
    $chk_bounce = isset($_POST["chk_bounce"])   ? $_POST["chk_bounce"]  : (isset($_GET["chk_bounce"])   ? $_GET["chk_bounce"]   :'');
   
    if ( ( $chk_bounce == 'AND')){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_bounce;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		
		$condition_query .= " ". TABLE_NEWSLETTERS_MEMBERS .".email_bounce_status ='1' ";        
		
       
        $condition_url          .= "&chk_bounce=$chk_bounce";
        $_SEARCH["chk_bounce"]  = $chk_bounce;
             
    }    
    // EO: chk_bounce
	// EO: chk_unsubscribed   
	$chk_unsubscribed  = isset($_POST["chk_unsubscribed"]) ? $_POST["chk_unsubscribed"]  : (isset($_GET["chk_unsubscribed"])   ? $_GET["chk_unsubscribed"]   :'');
   
    if ( ( $chk_unsubscribed == 'AND')){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_unsubscribed;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		
		$condition_query .= " ". TABLE_NEWSLETTERS_MEMBERS .".unsubscribed_status ='1' ";        
		
       
        $condition_url          .= "&chk_unsubscribed=$chk_unsubscribed";
        $_SEARCH["chk_unsubscribed"]  = $chk_unsubscribed;
             
    }    
    // EO: chk_unsubscribed
	$s_category_id  = isset($_POST["s_category_id"]) ? $_POST["s_category_id"]  : (isset($_GET["s_category_id"])   ? $_GET["s_category_id"]   :'');
	if ( ( $s_category_id > 0 )){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		
		$condition_query .= " ". TABLE_NEWSLETTERS_MEMBERS .".nws_category_id IN ('".$s_category_id."') ";    
        $condition_url          .= "&s_category_id=$s_category_id";
        $_SEARCH["s_category_id"]  = $s_category_id; 
    }    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"]) ? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
	$dt_field = isset($_POST["dt_field"])? $_POST["dt_field"] : (isset($_GET["dt_field"]) ? $_GET["dt_field"]  :'');
 
	
	// BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from) ){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_NEWSLETTERS_MEMBERS .".".$dt_field." >= '". $dfa ."'";        
        $condition_url  .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;
    }
    
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_NEWSLETTERS_MEMBERS .".".$dt_field." <= '". $dta ."'";
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }    
    // EO: Upto Date
     
    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/newsletters-members-list.php');
?>