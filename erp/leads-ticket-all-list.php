<?php

    if ( $perm->has('nc_ldt_all_list') ) {
        
     
		if ( !isset($condition_query) || $condition_query == '' ) {
            
			$_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"] = array( LeadsTicket::STALL_INPROCESS, 
										LeadsTicket::STALL_PENDING);
			
			$condition_query = '';
			$condition_query = " WHERE ( "
            .TABLE_LEADS_TCKT_ALL .".status IN('". LeadsTicket::STALL_INPROCESS ."',". "'".LeadsTicket::STALL_PENDING."') )";
		
		}
		
		if($perm->has('nc_ldt_all_my_list')){
		
			$condition_query.= " AND ".TABLE_LEADS_TCKT_ALL.".created_by = '".$my['uid']."'";
		}
		
		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
          //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        
        $total	=	LeadsTicket::getStAllList( $db, $list, '', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
       
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $list	= NULL;
         
        $fields = TABLE_LEADS_TCKT_ALL.".*";
        LeadsTicket::getStAllList( $db, $list,$fields, $condition_query, $next_record, $rpp);
        
       
       
      
        
        if($perm->has('nc_ldt_all_add')){
            $variables['can_add'] = true;
        }
        if($perm->has('nc_ldt_all_edit')){
            $variables['can_edit'] = true;
        }
        if( $perm->has('nc_ldt_all_list') ) {
            $variables['can_view_list'] = true;
        }
        
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-ticket-all-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>