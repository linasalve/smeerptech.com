<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    
                    //$table = TABLE_ADDRESS_BOOK ;
                    foreach( $field_arr as $key => $field ) {
                            $condition_query .= " ". $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                        }
                    
                }
            }
            $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
   
	

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_ADMIN .'/web-access-list.php');
?>