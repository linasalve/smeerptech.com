<?php

if ( $perm->has('nc_p_pt_list') ) {  
	
	 
    $access_level   = $my['access_level'];
    
	 
    
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;

    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    $extra_url  = '';    
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }        
    $condition_url .="&perform=".$perform."&show=1&rpp=$rpp";
   
    $fList =$plist	= $list	= array();
	$total=$total_pending=0;
	$pagination='';
	
	 $searchStr=1;
	
	
	if($searchStr==1){
	
		 
		// To count total records.
		
		$total	=	OrderBid::getDetails( $db, $list, '', $condition_query);  
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
		   
		$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  = '&start=url'. $extra_url .'&end=url';

		$list	= NULL;
		$fields = TABLE_ORDER_BID .".*,". TABLE_BILL_ORDERS .".order_title, ".TABLE_BILL_ORDERS .".number as order_no "  ;	 
		OrderBid::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);		 
		
		if(!empty($list)){
			foreach( $list as $key=>$val){

				if( $key%2==0 ){
					$val['class'] = 'odd';
				}else{
					$val['class'] = 'even';
				}
				
				$temp_p = NULL;			 
				$condition_query_p = " WHERE ".TABLE_ORDER_BID_DETAILS.".ord_bid_id = '".$val['id'] ."'";
				OrderBid::getParticulars($db, $temp_p, '*', $condition_query_p);
				/* 
				if(!empty($temp_p)){					 
					foreach ( $temp_p as $pKey => $parti ) {                            
						$_ALL_POST_DETAILS['hrs'][$pKey]        		= $temp_p[$pKey]['hrs'];					
						$_ALL_POST_DETAILS['projected_hour_salary'][$pKey]		= $temp_p[$pKey]['projected_hour_salary'];
						$_ALL_POST_DETAILS['team_member'][$pKey]		= $temp_p[$pKey]['team_member'];
						$_ALL_POST_DETAILS['team_member_name'][$pKey]   = $temp_p[$pKey]['team_member_name'];
					}
				} */
				$val['bid_details']=$temp_p;				
			    $fList[$key]=$val;
			}
		}
	}
	print_R($fList);
    // Set the Permissions.
 
    $variables['can_approved'] = false;    
    $variables['can_view_amt'] = false;    
 
     
	
    if ( $perm->has('nc_or_bd_appr') ) {
        $variables['can_approved'] = true;
    }
	if ( $perm->has('nc_or_bd_view_amt') ) {
            $variables['can_view_amt'] = true;
    }
 
    $page["var"][] = array('variable' => 'total_records', 'value' => 'total');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'fList', 'value' => 'fList');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'order-bid-list.html');
    
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
   
}
?>
