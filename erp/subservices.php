<?php
    if ( $perm->has('nc_sr_mark') ) {
        $condition_query = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = ".$parent_id." AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
          $condition_query .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = ".$currency_id." 
		AND ".TABLE_SETTINGS_SERVICES.".ss_status='1'" ;
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
             
        // To count total records.
        $list	= 	NULL;
        $fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price";
        
        $sql = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." ".$condition_query ; 
        if ( $db->query($sql) ) {
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
                        $list[] = processSqlData($db->result());
					}
				}
        }                
        $currency = $fields= $condition_query='';
        $condition_query = " WHERE id= ".$currency_id;
        $fields = "currency_name ";
        Currency::getList( $db, $currency, $fields, $condition_query);
      
        if(!empty($currency)){
            $currencyName = $currency[0]['currency_name'];
        }
        $page["var"][] = array('variable' => 'currencyName', 'value' => 'currencyName');
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'parent_id', 'value' => 'parent_id');
        
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'subservices.html');
    }
    else{
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>