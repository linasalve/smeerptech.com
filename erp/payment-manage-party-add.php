<?php
  if ( $perm->has('nc_p_mp_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the payment party class
		include_once (DIR_FS_INCLUDES .'/payment-party.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Paymentparty::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_PAYMENT_PARTY
                            ." SET ". TABLE_PAYMENT_PARTY .".fname = '". 		$data['fname'] ."'"                                                              
                                	.",". TABLE_PAYMENT_PARTY .".lname = '". 		$data['lname'] ."'"  
                                	.",". TABLE_PAYMENT_PARTY .".company_name = '". $data['company_name'] ."'"  
                                	.",". TABLE_PAYMENT_PARTY .".billing_name = '". $data['billing_name'] ."'"  
                                	.",". TABLE_PAYMENT_PARTY .".address      = '". $data['address'] ."'"  
                                	.",". TABLE_PAYMENT_PARTY .".contact_no   = '". $data['contact_no'] ."'"  
                                	.",". TABLE_PAYMENT_PARTY .".created_by   = '". $my['uid'] ."'"  
                                    .",". TABLE_PAYMENT_PARTY .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"   
                                	.",". TABLE_PAYMENT_PARTY .".status = '". 		$data['status'] ."'"                                
                                	.",". TABLE_PAYMENT_PARTY .".date = '". 		date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New party entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/payment-manage-party.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-manage-party.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/payment-manage-party.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-manage-party-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
