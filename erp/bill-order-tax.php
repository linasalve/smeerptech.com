<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
            ));
    
    include_once ( DIR_FS_NC ."/header.php");

    include_once ( DIR_FS_INCLUDES .'/bill-order-tax.inc.php');
    include_once ( DIR_FS_INCLUDES .'/common-functions.inc.php');
    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? 
	$_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? 
	$_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');    
    $added_all 	= isset($_GET["added_all"]) ? $_GET["added_all"]: ( isset($_POST["added_all"])  ? $_POST["added_all"]:'0');    
    $i 	        = isset($_GET["i"])         ? $_GET["i"]        : ( isset($_POST["i"])          ? $_POST["i"]   :     '0');    
    $j 	        = isset($_GET["j"])         ? $_GET["j"]        : ( isset($_POST["j"])          ? $_POST["j"]   :     '0');    
    $rpp = isset($_GET["rpp"])  ? $_GET["rpp"] : ( isset($_POST["rpp"]) ? $_POST["rpp"] : 
	(defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20)); // Result per page
    $condition_query = '';
    $condition_url = '';
	
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    
     
    
    if($added_all){
        $messages->setOkMessage("Record has been created successfully for ".$i ."/". $j .".");
    }
    
		$sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_BILL_ORD_TAX   =>  array(  
															'Order No'        => 'or_no',
															'Proforma No'    => 'inv_profm_no',
															'Rcpt No'     => 'rcpt_no '
                                                        )
                            );
        
        $sOrderByArray  = array(
                                TABLE_BILL_ORD_TAX => array(                                                     
                                                    'Order No'        => 'or_no',
													'Proforma No'    => 'inv_profm_no',
													'Rcpt no'     => 'rcpt_no'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_BILL_ORD_TAX;
        }
	$action='Tax Paid Details';
	
    if ( $perm->has('nc_bill_ord_tax') ) { 
         
        //use switch case here to perform action. 
        switch ($perform) {
			
			case ('csv_upload'):{
                include (DIR_FS_NC.'/bill-order-tax-csv.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-tax.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('search'): {
                include(DIR_FS_NC."/bill-order-tax-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-tax.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list'):
            default: {
				$action ='Tax Paid List Receipts';
				$searchStr=1;
                include (DIR_FS_NC.'/bill-order-tax-list.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-tax.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    
	}else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-tax.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("action", $action);
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
           $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>