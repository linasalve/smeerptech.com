<?php

    if ( $perm->has('nc_sms_schd_list') ) {
        @$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	ScheduledSmsList::getList( $db, $list, '', $condition_query);    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        ScheduledSmsList::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_delete']        = false;

        if ( $perm->has('nc_sms_schd_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_sms_schd_delete') ) {
            $variables['can_delete']     = true;
        }
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'scheduled-sms-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>