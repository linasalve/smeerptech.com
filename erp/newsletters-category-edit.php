<?php

	$id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
	$_ALL_POST      = NULL;
	$data           = NULL;
	$condition_query= NULL;
   
	if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST  = $_POST;
		$data       = processUserData($_ALL_POST);
		
		
		$extra = array( 'db'                => &$db,
						'access_level'      => $access_level,
						'messages'          => &$messages
					);
	  
		if ( NewslettersCategory::validateUpdate($data, $extra) ) { 
			 
			$query  = " UPDATE ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY." SET 
						".TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".category = '".$data['category'] ."'"  		
						.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".client = '".$data['client'] ."'"                            
						.",". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".client_details = '".$data['client_details'] ."'"                              
						." WHERE id = '". $id ."'";
			
			if ( $db->query($query) && $db->affected_rows() > 0 ) {
				$messages->setOkMessage("Record updated successfully.");
				$variables['hid'] = $id;
			}
			$data       = NULL;
		}
	}
	else {
		// Read the record which is to be Updated.
		$fields = TABLE_NEWSLETTERS_MEMBERS_CATEGORY.'.*'  ;            
		$condition_query = " WHERE (". TABLE_NEWSLETTERS_MEMBERS_CATEGORY .".id = '". $id ."' )"; 
		if ( NewslettersCategory::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
			$_ALL_POST = $_ALL_POST['0'];
			$id         = $_ALL_POST['id'];
			
		}
		else { 
			$messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
		}
	}

	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	if ( isset($_POST['btnCancel'])
		|| (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
		$variables['hid'] = $id;
		$perform='list';
		$condition_query='';
		include ( DIR_FS_NC .'/newsletters-category-list.php');
	}
	else {
		// These parameters will be used when returning the control back to the List page.
		foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
			$hidden[]   = array('name'=> $key, 'value' => $value);
		}
		
		$hidden[] = array('name'=> 'id', 'value' => $id);
		$hidden[] = array('name'=> 'perform', 'value' => 'edit');
		$hidden[] = array('name'=> 'act', 'value' => 'save');
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletters-category-edit.html');
	}

?>
