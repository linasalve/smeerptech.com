<?php
// report of score sheet ss
if ( $perm->has('nc_rp_ss') ) {
         //to view report member id should be compulsary
        $member_id	= isset($_GET["member_id"]) 	? $_GET["member_id"]	: ( isset($_POST["member_id"]) 	? $_POST["member_id"] : '' );
        $_SEARCH["member_id"] 	= $member_id;       
        $where_added    = true;
        $allData = $_GET 	? $_GET	: ($_POST 	? $_POST : '' );
        $extra_url=$condition_query=$condition_url=$pagination ='';
        // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields);
        // BO:  Read the Team members list ie member list in dropdown box  EOF
              
        if(array_key_exists('submit_search',$allData)){
        
            if(!empty($member_id)){        
                // Read the Date data.
                $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
                $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
                $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
                $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
                
                // BO: From Date
                if ( $chk_date_from == 'AND' || $chk_date_from == 'OR') {
                    if ( $where_added ) {
                        $condition_query .= " ". $chk_date_from;
                    }
                    else {
                        $condition_query.= ' WHERE ';
                        $where_added    = true;
                    }
                    $dfa = explode('/', $date_from);
                    //$dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
                    $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0];
                    $condition_query .= " ". TABLE_SCORE_SHEET .".date >= '". $dfa ."'";
                }
                $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
                $_SEARCH["chk_date_from"]   = $chk_date_from;
                $_SEARCH["date_from"]       = $date_from;
                // EO: From Date
                
                // BO: Upto Date
                if ( $chk_date_to == 'AND' || $chk_date_to == 'OR') {
                    if ( $where_added ) {
                        $condition_query .= $chk_date_to;
                    }
                    else {
                        $condition_query.= ' WHERE ';
                        $where_added    = true;
                    }
                    $dta = explode('/', $date_to);
                    //$dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
                    $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0];
                    $condition_query .= " ". TABLE_SCORE_SHEET .".date <= '". $dta ."'";
                }
                $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
                $_SEARCH["chk_date_to"] = $chk_date_to ;
                $_SEARCH["date_to"]     = $date_to ;
                // EO: Upto Date
                
            
            
                 // fields req to in query
                /*$fields = ' ( SUM('.TABLE_USER_TIMESHEET .'.hrs) + ( SUM('. TABLE_USER_TIMESHEET .'.min) DIV 60 ))  as totHr  '
                    .', ( SUM('. TABLE_USER_TIMESHEET .'.min) % 60 ) as totMin'
                    .', '. TABLE_USER_TIMESHEET .'.date'        
                    .', '. TABLE_USER_TIMESHEET .'.order_id' ;
                */
                $fields = TABLE_SCORE_SHEET.".date,".TABLE_SCORE_SHEET.".particulars, ".TABLE_SCORE_SHEET.".score, ".TABLE_USER.".f_name ,".TABLE_USER.".l_name " ;
                $query = "SELECT  ".$fields;            
                $query .= " FROM ". TABLE_SCORE_SHEET.','.TABLE_USER ;
                $query .= " WHERE ".TABLE_SCORE_SHEET.".comment_to ='".$member_id."' AND ".TABLE_SCORE_SHEET.".created_by = ".TABLE_USER.".user_id";
                $condition_query .= "ORDER BY ".TABLE_SCORE_SHEET.".date ";
                $query .= " ". $condition_query;   
                $db->query($query) ;
                if($db->nf() > 0){
                    $total = $db->nf() ;
                }
                else{
                    $total = 0 ;
                }
                $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
                $query .= " LIMIT ". $next_record .", ". $rpp ;
                $db1 		= new db_local;
               if ( $db->query($query) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {   
                            
                            $list[] = array(
                                        'particulars'=>nl2br($db->f('particulars')),
                                        'score'=>$db->f('score'),
                                        'f_name'=>$db->f('f_name'),
                                        'l_name'=>$db->f('l_name'),
                                        'date'=>$db->f('date'),
                                        );
                            
                                                     
                        }
                    }
                }
               
                $extra_url  .= "&x=$x&rpp=$rpp&member_id=$member_id&submit_search=1";
                $extra_url  = '&start=url'. $extra_url .'&end=url';
            }else{
                $messages->setErrorMessage('Please select Member.');
            }
        }   
            
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    
   
     
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');        
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-executive-score.html');
}
else {
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>