<?php

    if ( $perm->has('nc_sl_ld_list') ) {
	    
        $variables["followup_type"]     = FOLLOWUP::LEADTM;
        $memberTypeList  = Leads::getMemberType();
		
		include_once ( DIR_FS_CLASS .'/Phone.class.php');
		
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
            $where_added = true;
        }else {
            $condition_query .= ' AND (';
        }
        
        $access_level   = $my['access_level'];
        
        // If Lead is alloted to my
        $condition_query1 = " ( "." ". TABLE_SALE_LEADS .".lead_assign_to ='".$my['user_id']."' ) AND 
			( ".TABLE_SALE_LEADS .".lead_status ='".LEADS::POSITIVE."' ) "; 
        
       
        if ( $perm->has('nc_sl_ld_list_ot') ) {            
            $access_level_o   = $my['access_level'];
            $access_level_o += 1;
            $condition_query .= " ( ". TABLE_SALE_LEADS .".access_level < $access_level_o ) ";
        }
        
        if ( $perm->has('nc_sl_ld_list_my') ) {
            if ( $perm->has('nc_sl_ld_list_ot') ) { 
               $condition_query .= " OR ";
            }
            $condition_query .= " ( ". TABLE_SALE_LEADS .".created_by = '". $my['user_id'] ."' "." ) ";
        }
        
        if($perm->has('nc_sl_ld_list_my') || $perm->has('nc_sl_ld_list_ot')){
            $condition_query.= " OR ".$condition_query1 ;
        }else{
            $condition_query.= $condition_query1 ;
        }
        
        
        $condition_query .= ')';
        
        $condition_query .= " AND lead_status!= '".Leads::CONVERTED."' AND lead_assign_to!='' AND lead_assign_to_mr='' ";
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	Leads::getList( $db, $list, 'lead_id', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields ='';
       $fields = TABLE_SALE_LEADS.".lead_id, ".TABLE_SALE_LEADS.".company_name,".TABLE_SALE_LEADS.".f_name,
		".TABLE_SALE_LEADS.".m_name,".TABLE_SALE_LEADS.".l_name,  
		".TABLE_SALE_LEADS.".member_type,"  ;
        $fields .= TABLE_SALE_LEADS.".email, ". TABLE_SALE_LEADS.".lead_status, ".TABLE_SALE_LEADS.".created_by, " ;
        $fields .= TABLE_SALE_LEADS.".lead_assign_to, ".TABLE_SALE_LEADS.".status " ;
        Leads::getList( $db, $list,$fields , $condition_query, $next_record, $rpp);
        
        $leadStatusArr =Leads::getLeadStatus();
        $leadStatusArr  = array_flip($leadStatusArr );
          
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                
                $exeCreted=$exeCreatedname='';
                $table = TABLE_AUTH_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_AUTH_USER .'.l_name';
                $exeCreted= getRecord($table,$fields1,$condition2);
                if(!empty($exeCreted)){                   
                   $exeCreatedname = $exeCreted['f_name']." ".$exeCreted['l_name']."<br/>" ;
                }
                $val['created_by_name']    = $exeCreatedname ;     
                $leadstatus = $val['lead_status'];
                $val['lead_status_name']    = $leadStatusArr[$leadstatus] ;     
                $member_type = trim($val['member_type'],",") ; 
				$val['member_type_name'] = '';
				$mArr = explode(",",trim($val['member_type'],","));
				if(!empty($mArr)){
					foreach($mArr as $key1=>$val1){
						$val['member_type_name'].= $memberTypeList[$val1];
					}
				}
                $fList[$key]=$val;
            }
        }
        
        
    	/*
		$phone          = new Phone(TABLE_SALE_LEADS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_SALE_LEADS, $list[$i]['lead_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}*/
		
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        
		$variables['can_view_tck']		= false;
		
		
        if ( $perm->has('nc_sl_ld_list') ) {
            $variables['can_view_list']     = true;
        }
        /*
        if ( $perm->has('nc_sl_ld_add') ) {
            $variables['can_add']     = true;
        }*/
        if ( $perm->has('nc_sl_ld_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_sl_ld_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_sl_ld_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_sl_ld_status') ) {
            $variables['can_change_status']     = true;
        }
		if ( $perm->has('nc_ldt') && $perm->has('nc_ldt_details') ) {
			$variables['can_view_tck'] = true;
		}
        
        //$variables['lead_status']=Leads::NEWONE;
        $variables['positive']=Leads::POSITIVE;
        $variables['negative']=Leads::NEGATIVE;
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
		$page["var"][] = array('variable' => 'memberTypeList', 'value' => 'memberTypeList');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>