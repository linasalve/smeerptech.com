<?php   

if( $perm->has('nc_bl_or_bifur')){
    
    $or_id 			= isset($_GET["or_id"]) ? $_GET["or_id"]        : ( isset($_POST["or_id"])          ? $_POST["or_id"]       :'');
    $bifc_id 			= isset($_GET["bifc_id"]) ? $_GET["bifc_id"]        : ( isset($_POST["bifc_id"])          ? $_POST["bifc_id"]       :'');
    
    $scount = isset($_GET["scount"]) ? $_GET["scount"] : ( isset($_POST["scount"]) ? $_POST["scount"]   :'1');   
    include_once (DIR_FS_INCLUDES .'/department.inc.php');
    include_once (DIR_FS_INCLUDES .'/clients.inc.php');
    include_once (DIR_FS_INCLUDES .'/project-task.inc.php');
    include_once (DIR_FS_INCLUDES .'/order-bifurcation-services.inc.php');
    
    if(!empty($or_id)){
        $order_list = NULL;
        $required_fields ='client'.','.'order_title';
        $condition_query="WHERE id='".$or_id."'";
		Order::getList($db,$order_list,$required_fields,$condition_query);
        $client_id = $order_list[0]['client'];
        $order_title = $order_list[0]['order_title'];
    }
    $required_fields="title,id";
    $condition_query=" WHERE status='".OrderBifurcationServices::ACTIVE."'";
    OrderBifurcationServices::getList($db,$bifc_serv_list,$required_fields,$condition_query);
  
    $_ALL_POST	    = NULL;
    $access_level   = $my['access_level'];
    
    //Code to delete the bifurcation row bof
    if(!empty($bifc_id)){
         $extra = array( 'db'        => &$db,
                'access_level'      => $access_level,
                'files'          => &$files,
                'messages'          => &$messages
            );
         Order::deleteBifurcation($bifc_id, $extra);
        
    }
    //Code to delete the bifurcation row eof

    
    if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
       
        $_ALL_POST  = $_POST;
        
        $data = processUserData($_ALL_POST);    
       
        $extra = array( 'db'        => &$db,
                'access_level'      => $access_level,
                'files'          => &$files,
                'messages'          => &$messages
            );
            
    
        if ( Order::validateBifurcationAdd($data, $extra) ) {
     
            foreach ( $data['service'] as $key=>$service ) {
                if(!empty($data['service'][$key]) && !empty($data['price'][$key])){
                    $sql="INSERT INTO ".TABLE_ORD_BIFURCATION
                            ." SET ". TABLE_ORD_BIFURCATION .".ord_id           = '". $or_id ."'"
                            .",". TABLE_ORD_BIFURCATION .".ord_bifc_service = '".    $data['service'][$key] ."'"
                            .",". TABLE_ORD_BIFURCATION .".details = '".    $data['details'][$key] ."'"
                            .",". TABLE_ORD_BIFURCATION .".price = '".    $data['price'][$key] ."'"
                            .",". TABLE_ORD_BIFURCATION .".created_by = '".   $my['user_id']."'"
                            .",". TABLE_ORD_BIFURCATION .".created_by_name = '".$my['f_name']." ".$my['l_name']."'"
                            .",". TABLE_ORD_BIFURCATION .".do_e = '".   date('Y-m-d H:i:s')."'"
                            .",". TABLE_ORD_BIFURCATION .".ip = '".   $_SERVER['REMOTE_ADDR']."'";
                    $db->query($sql);
                }
                    
            }
            $messages->setOkMessage("Order Bifurcation has been added.");       
            $_ALL_POST	    = NULL;
        }
        
    }

     // get list of all comments of change log BOF
    $condition_query1 = $condition_url1 =$extra_url= '';
    $condition_url1 .="&perform=".$perform."&or_id=".$or_id;
    $perform = 'followup';
    $followupList	= 	NULL;
    $fields = TABLE_ORD_BIFURCATION.".id,". TABLE_ORD_BIFURCATION.".ord_id,".TABLE_ORD_BIFURCATION_SERVICES.".title,".TABLE_ORD_BIFURCATION.".price,".TABLE_ORD_BIFURCATION.".details,
    ".TABLE_ORD_BIFURCATION.".created_by_name,
    ".TABLE_ORD_BIFURCATION.".do_e" ;
    $condition_query1 = " LEFT JOIN ".TABLE_ORD_BIFURCATION_SERVICES." ON ".TABLE_ORD_BIFURCATION_SERVICES.".id= ".TABLE_ORD_BIFURCATION.".ord_bifc_service";
    $condition_query1 .= " ORDER BY ".TABLE_ORD_BIFURCATION.".do_e DESC";
    $total	=	Order::getDetailsBifurcation( $db, $list, $fields , $condition_query1);
   
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
    //$extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  .= "&perform=".$perform;
    $extra_url  .= "&or_id=".$or_id ;
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $followupList	= NULL;
    //$fields = TABLE_PROJECT_TASK_FOLLOWUP .".*,".TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_SETTINGS_DEPARTMENT.".department_name";   
   // Order::getDetailsBifurcation( $db, $followupList, $fields, $condition_query1, $next_record, $rpp);
    $condition_url .="&perform=".$perform;
    
    $variables['can_bifur_delete'] = false;
   if ( $perm->has('nc_bl_or_bifur_delete') ) {
        $variables['can_bifur_delete'] = true;
    }
  
    
    $hidden[] = array('name'=> 'or_id', 'value' => $or_id);
    $hidden[] = array('name'=> 'client_id', 'value' => $client_id);
    $hidden[] = array('name'=> 'perform', 'value' => 'order_bifurcation');
    $hidden[] = array('name'=> 'act', 'value' => 'save');
    
    $page["var"][] = array('variable' => 'scount', 'value' => 'scount');
    $page["var"][] = array('variable' => 'bifc_serv_list', 'value' => 'bifc_serv_list');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'order_title', 'value' => 'order_title');
    $page["var"][] = array('variable' => 'bifurcationList', 'value' => 'list');
    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-bifurcation.html');
}else{
    $messages->setErrorMessage("You do not have the Permission to access this module.");
}   
?>