<?php
	if ( $perm->has('nc_urht_details') ) {
        $right_id = isset ($_GET['right_id']) ? $_GET['right_id'] : ( isset($_POST['right_id'] ) ? $_POST['right_id'] :'');
    
        $list	=	NULL;
        $condition_query	=	NULL;
            
        if (!isset($condition_query) || $condition_query=='') {
            $condition_query	=" WHERE id='".$right_id."' ";
        }
        
        if (UserRights::getList($db, $list, '*', $condition_query) > 0 ) {
            $data	=	$list['0'];
        }
        else {
            $messages->setErrorMessage("Records not found to view. ");
        }
        
        $page["var"][] = array('variable' => 'data', 'value' => 'data');
    
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-rights-view.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>