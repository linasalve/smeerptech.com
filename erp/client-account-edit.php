<?php
    if ( $perm->has('nc_sms_ca_edit') ) {
		$ca_id = isset($_GET["ca_id"]) ? $_GET["ca_id"] : ( isset($_POST["ca_id"]) ? $_POST["ca_id"] : '' );
        $_ALL_POST	= NULL;
        $data       = NULL;

		//get client list for the dropdown
		ClientAccount::showUsers($clientlist);
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            if ( ClientAccount::validateUpdate($data, $extra) ) {
				$sql = "UPDATE ".TABLE_SMS_CLIENT_ACC." SET 
						ca_username 	= '".$data['ca_username']."'";

				if(!empty($data['ca_password']))
					$sql .= ",ca_password 	= '".md5($data['ca_password'])."',";
				else
					$sql .= ",";

				$sql .= "ca_displayname 	= '".$data['ca_displayname']."',
						ca_mobileno 	= '".$data['ca_mobileno']."',
						ca_mobileno 	= '".$data['ca_mobileno']."',
						ca_posturl 		= '".$data['ca_posturl']."' 
						WHERE ca_id 	='".$ca_id."'";
				if($db->query($sql))
					$messages->setOkMessage("The Package has been updated.");
				else
					$messages->setErrorMessage('Problem adding Package. Try later');
				//to flush the data.
				$_ALL_POST	= NULL;
				$data		= NULL;
			}
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$variables['hid'] = $ca_id;
            include ( DIR_FS_NC .'/client-account-list.php');
        }
        else {
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
				$_ALL_POST = $_POST;
            }
            else {

                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE ca_id = '". $ca_id ."' ";
                $_ALL_POST      = NULL;
                if ( ClientAccount::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) 
                    $_ALL_POST = $_ALL_POST[0];
            }

			if ( !empty($_ALL_POST['ca_id']) ) {

            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
				$hidden[] = array('name'=> 'perform','value' => 'edit');
				$hidden[] = array('name'=> 'act', 'value' => 'save');
				$hidden[] = array('name'=> 'ca_id', 'value' => $ca_id);
		
				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["var"][] = array('variable' => 'data', 'value' => 'data');
	
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'client-account-edit.html');
            }
            else {
                $messages->setErrorMessage("The Selected Account was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You do not have the Permisson to Access this module.");
    }
?>