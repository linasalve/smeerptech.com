<?php
    if ( $perm->has('nc_bl_rcpt_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/payment-mode.inc.php');
        include_once ( DIR_FS_INCLUDES .'/payment-bank.inc.php');
        
        $rcpt_id 		= isset($_GET["rcpt_id"]) ? $_GET["rcpt_id"] : ( isset($_POST["rcpt_id"]) ? $_POST["rcpt_id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_rcpt_edit_al') ) {
            $access_level += 1;
        }
		
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);        
           
       
        
          // Read the available payment mode 
        $payment_mode	= NULL;
        $fieldspm= TABLE_PAYMENT_MODE.'.*' ;
        $condition_querypm= " WHERE ".TABLE_PAYMENT_MODE.".status='1'";
        Paymentmode::getDetails($db,$payment_mode, $fieldspm, $condition_querypm);
       
        
        $inhouseBanklst	= NULL;
        $condition_queryib = " WHERE ".TABLE_PAYMENT_BANK.".type = '1' AND ".TABLE_PAYMENT_BANK.".status='1'";
        $fieldsi = TABLE_PAYMENT_BANK.'.*' ;
        Paymentbank::getDetails($db, $inhouseBanklst, $fieldsi, $condition_queryib);
        
        $globalBanklst	= NULL;
        $fieldsg = TABLE_PAYMENT_BANK.'.*' ;
        $condition_querygb = " WHERE ".TABLE_PAYMENT_BANK.".type = '0' AND ".TABLE_PAYMENT_BANK.".status='1'";
        Paymentbank::getDetails($db, $globalBanklst, $fieldsg, $condition_querygb);
        
        
        
        
		// Read the Receipt which is to be Updated.
		$fields = TABLE_BILL_RCPT .'.*'
					.','. TABLE_CLIENTS .'.user_id AS c_user_id'
					.','. TABLE_CLIENTS .'.number AS c_number'
					.','. TABLE_CLIENTS .'.f_name AS c_f_name'
					.','. TABLE_CLIENTS .'.l_name AS c_l_name'
					.','. TABLE_CLIENTS .'.email AS c_email'
					.','. TABLE_CLIENTS .'.status AS c_status';
		
		$condition_query = " WHERE ". TABLE_BILL_RCPT .".id = '". $rcpt_id ."'";
		$condition_query .= " AND ( ";
        // If my has created this Invoice.
        $condition_query .= " (". TABLE_BILL_RCPT .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_RCPT .".access_level < $access_level ) ";
        
        // Check if the User has the Right to Edit receipts created by other Users.
        if ( $perm->has('nc_bl_rcpt_edit_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_bl_rcpt_edit_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_BILL_RCPT. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_RCPT .".access_level < $access_level_o ) ";
        }
        $condition_query .= " )";
		
		// Read the Receipt.
		
		$receipt = NULL;
        
        
        
		if ( Receipt::getDetails($db, $receipt, $fields, $condition_query) > 0 ) {
			$receipt = $receipt[0];
			
			$extra = array( 'db' 				=> &$db,
							'access_level'      => $access_level,
							'messages'          => &$messages
						);	
           
			// Read the Invoice details.
			$invoice = NULL;
            
            
			if ( Receipt::getInvoice($receipt['inv_no'], $invoice, $extra) ) {
				// Retrieve the Receipts for the invoice.
				$receipt_list = array();
				Receipt::getInvoiceReceipts($invoice['number'], $receipt_list, $extra);
				
                $_ALL_POST['particulars']= $invoice['particular_details'];
                $or_no = $invoice['or_no'];                
			}
           
			if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
				$_ALL_POST 	= $_POST;
				$data		= processUserData($_ALL_POST);

				// Set the Creator.
				if ( !isset($data['created_by']) || empty($data['created_by']) ) {
					$data['created_by'] = $my['user_id'];
				}
				// Set the Client.
				$data['client'] = $invoice['c_user_id'];
	
				
              	
        		if ( Receipt::validateUpdate($data, $extra) ) {
                    
                     
                     $query  = " UPDATE ".TABLE_BILL_RCPT
                                ." SET "
                                ."". TABLE_BILL_RCPT .".remarks = '".$data['remarks'] ."'"
                                .",". TABLE_BILL_RCPT .".do_r = '".          date('Y-m-d H:i:s', $data['do_r']) ."'"
                                .",". TABLE_BILL_RCPT .".amount_words = '".   $data['amount_words'] ."'"
                                .",". TABLE_BILL_RCPT .".billing_address ='". $data['billing_address'] ."'"
                                ." WHERE ".TABLE_BILL_RCPT.".number ='".$data['number']."'";
                    
                    
					
					if ( $db->query($query) && $db->affected_rows() > 0 ) {
						$messages->setOkMessage("New Receipt has been Updated.");
						$variables['hid'] = $rcpt_id;
                        
                        $temp = NULL;
                        $temp_p = NULL;
                        $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
                        Order::getParticulars($db, $temp, '*', $condition_query);
                        if(!empty($temp)){
                            foreach ( $temp as $pKey => $parti ) {
                              
                                $temp_p[]=array(
                                                'p_id'=>$temp[$pKey]['id'],
                                                'particulars'=>$temp[$pKey]['particulars'],
                                                'p_amount' =>$temp[$pKey]['p_amount'] ,                                   
                                                's_id' =>$temp[$pKey]['s_id'] ,                                   
                                                'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                                'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                                'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                                'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                                'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                                'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                                'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                                'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                                'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                                );
                            }
                        }
                        
                        $data['particulars']=$temp_p ;
                        include_once ( DIR_FS_CLASS .'/Region.class.php');
                        $region         = new Region();
                        $region->setAddressOf(TABLE_CLIENTS, $data['client']);
                        $addId = $data['billing_address'] ;
                        $address_list  = $region->get($addId);
                        $data['b_address'] = $address_list ;
                        
                        //get invoice details bof
						
                        $data['inv_amount'] = $invoice['amount'];
                        $data['do_i'] = $invoice['do_i'];
                        // code to show history of all receipts againce invoice BOF
                        $data['historyrcp'] = $receipt_list;
                        // commented now $data['invbalance'] = $balance['amount'];
                        $data['invbalance'] = $balance['balance'];
                        						
                        
						// Create the Invoice PDF, HTML in file.
                        /*
						$extra = array( 's'         => &$s,
										'messages'  => &$messages
									  );
						$attch = '';
						if ( !($attch = Receipt::createFile($data, 'HTML', $extra)) ) {
							$messages->setErrorMessage("The Receipt file was not created.");
						}
                        */
						
                        
                        
                        
                        
						// Send the notification mails to the concerned persons.
						include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
						$users = getExecutivesWith($db, array(  array('right'=>'nc_bl_rcpt_add', 'access_level'=>$data['access_level']),
																array('right'=>'nc_bl_rcpt_add_al', 'access_level'=>($data['access_level']-1) )
															)
												);
	
						// Organize the data to be sent in email.
						$data['link']   = DIR_WS_MP .'/bill-receipt.php?perform=view&rcpt_id='. $variables['hid'];
	
						// Read the Client Manager information.
						include ( DIR_FS_INCLUDES .'/clients.inc.php');
						$data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');

						// Send Email to the Client.
						$email = NULL;
						if ( getParsedEmail($db, $s, 'BILL_RCPT_NEW_CLIENT', $data, $email) ) {
							$to     = '';
							$to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
						}
						
						$data['link']   = DIR_WS_NC .'/bill-receipt.php?perform=view&rcpt_id='. $variables['hid'];
						// Send mail to the Concerned Executives.
						foreach ( $users as $user ) {
							$data['myFname']    = $user['f_name'];
							$data['myLname']    = $user['l_name'];
							$email = NULL;
							if ( getParsedEmail($db, $s, 'BILL_RCPT_NEW_MANAGERS', $data, $email) ) {
								$to     = '';
								$to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
								$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
							}
						}
						
						// Send Email to the Client Manager.
						$email = NULL;
						if ( getParsedEmail($db, $s, 'BILL_RCPT_NEW_CLIENT_MANAGER', $data, $email) ) {
							$to     = '';
							$to[]   = array('name' => $data['manager']['f_name'] .' '. $data['manager']['l_name'], 'email' => $data['manager']['email']);
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
						}
						
						//to flush the data.
						$_ALL_POST  = NULL;
						$data       = NULL;
					}
				}
			}
			else {
				$_ALL_POST = $receipt;
				
				// Set up the Client Details field.
				$_ALL_POST['client_details']= $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
												.' ('. $_ALL_POST['c_number'] .')'
												.' ('. $_ALL_POST['c_email'] .')';
				
				// Set up the dates.
				$_ALL_POST['do_r']  = explode(' ', $_ALL_POST['do_r']);
				$temp               = explode('-', $_ALL_POST['do_r'][0]);
				$_ALL_POST['do_r']  = NULL;
				$_ALL_POST['do_r']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];

				$_ALL_POST['do_pay_received']  = explode(' ', $_ALL_POST['do_pay_received']);
				$temp               = explode('-', $_ALL_POST['do_pay_received'][0]);
				$_ALL_POST['do_pay_received']  = NULL;
				$_ALL_POST['do_pay_received']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];

			}
           

			// Check if the Form to Edit is to be displayed or the control is to be sent to the List page.
			if ( isset($_POST['btnCancel'])
					|| ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $messages->getErrorMessageCount() <= 0 )) {
				$variables['hid'] = $rcpt_id;
				include_once ( DIR_FS_NC .'/bill-receipt-list.php');
			}
			else {
				$_ALL_POST['invoice']	= $invoice;
				 $_ALL_POST['currency_id']= $invoice['currency_id'];
                $_ALL_POST['particulars']= $invoice['particular_details'];
                
				// Read the Client Addresses.
				include_once ( DIR_FS_CLASS .'/Region.class.php');
				$region         = new Region();
				$region->setAddressOf(TABLE_CLIENTS, $receipt['client']);
                $addId = $invoice['billing_address'] ;
				$address_list  = $region->get($addId);
                $_ALL_POST['address_list'][0] = $address_list ;
				//$_ALL_POST['address_list']  = $region->get();

				$al_list = getAccessLevel($db, $access_level);
				$index = array();
				if ( isset($_ALL_POST['invoice']['access_level']) ) {
					array_key_search('access_level', $invoice['access_level'], $al_list, $index);
					$_ALL_POST['invoice']['access_level'] = $al_list[$index[0]]['title'];
				}

				// These parameters will be used when returning the control back to the List page.
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}
				
				$hidden[] = array('name'=> 'inv_id' ,'value' => $invoice['id']);
				// added new 2008-12-16
                $hidden[] = array('name'=> 'rcpt_id' ,'value' => $rcpt_id);
				$hidden[] = array('name'=> 'inv_no' ,'value' => $invoice['number']);
				$hidden[] = array('name'=> 'perform' ,'value' => 'edit');
				$hidden[] = array('name'=> 'or_no' ,'value' => $or_no);
				$hidden[] = array('name'=> 'act' , 'value' => 'save');

				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
				$page["var"][] = array('variable' => 'receipt_list', 'value' => 'receipt_list');
				$page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
				$page["var"][] = array('variable' => 'payment_mode', 'value' => 'payment_mode');
                $page["var"][] = array('variable' => 'inhouseBanklst', 'value' => 'inhouseBanklst');
				$page["var"][] = array('variable' => 'globalBanklst', 'value' => 'globalBanklst');
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
				
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-receipt-edit.html');
			}

		}
		else {
			$messages->setErrorMessage("The Receipt was not found or you do not have the Permission to access this Receipt.");
		}
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>