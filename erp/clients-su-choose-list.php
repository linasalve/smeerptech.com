<?php
        /* Get if anyone want to add executive in diff div ie other than 'add_clients_su_info' then pass div name here 
            By default div name is add_clients_su_info
        */
        $div    = isset($_GET["div"])   ? $_GET["div"]  : ( isset($_POST["div"])    ? $_POST["div"] : 'add_clients_su_info' );
		$fieldname    = isset($_GET["fieldname"])   ? $_GET["fieldname"]  : ( isset($_POST["fieldname"])    ? $_POST["fieldname"] : 'clients_su' );
		$parent_id = isset($_GET["parent_id"]) ? $_GET["parent_id"] : ( isset($_POST["parent_id"]) ? $_POST["parent_id"] : '' );
		
		/*
		if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = " WHERE parent_id='".$parent_id."'";			 
        }else {
            $condition_query .= " AND parent_id='".$parent_id."'";;
        }*/
       	$condition_query .= " AND parent_id =''";		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	=$fList= 	NULL;
		$total	=0;
		$pagination='';		
		$extra_url  = '';
		
		
		if($searchStr ==1 ){
		
			$total	=	Clients::getList( $db, $list, '', $condition_query);    
			$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
			
			if ( isset($condition_url) && !empty($condition_url) ) {
				$extra_url  = $condition_url;
			}
			$extra_url  .= "&x=$x&rpp=$rpp";
			$extra_url  = '&start=url'. $extra_url .'&end=url';
		
			$list	= NULL;
			Clients::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
			$fList=array();
			$parentclientname ='';
			if(!empty($list)){
				foreach( $list as $key=>$val){      
				   /* $client=array();
				   $parent_id =  $val['parent_id'];
				   $parentclientname='';
				    if(!empty($parent_id)){
					   $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name';
					   $condition1 = " WHERE ".TABLE_CLIENTS .".user_id IN('".$parent_id."') " ;
					   Clients::getList($db,$client,$fields1,$condition1);                   
					    if(!empty($client)){
						   foreach($client as $key1=>$val1){
								$parentclientname .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
						   } 
					    }
				    }
				    $val['parentname'] = $parentclientname ;               
					// Read the Contact Numbers.
					$phone->setPhoneOf(TABLE_CLIENTS, $val['user_id']);
					$val['phone'] = $phone->get($db);					
					// Read the Addresses.
					$region->setAddressOf(TABLE_CLIENTS, $val['user_id']);
					$val['address_list'] = $region->get();
					*/
					$val['sublist_list']=array();					
					$parent_id = $val['user_id'];
					$sublist	= 	NULL;
					$condition_query=" WHERE parent_id='".$parent_id."'" ; 
					Clients::getList( $db, $sublist, 'user_id,status,f_name,l_name,mobile1,mobile2,email,email_1,email_2', $condition_query);
					$val['sublist_list']=$sublist;
					
				    $fList[$key]=$val;
				}
			}
        			
		}
		 
        // Set the Permissions.
        $variables['can_view_list']     = true;
        $variables['can_add']           = true;
        $variables['can_edit']          = true;
        $variables['can_delete']        = true;
        $variables['can_view_details']  = true;
        $variables['can_change_status'] = true;
  
        
		$hidden[] = array('name'=> 'div' , 'value' => $div);         
        $hidden[] = array('name'=> 'fieldname' , 'value' => $fieldname);                
        $hidden[] = array('name'=> 'parent_id' , 'value' => $parent_id);         
        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');        
        $page["var"][] = array('variable' => 'div', 'value' => 'div');
        $page["var"][] = array('variable' => 'fieldname', 'value' => 'fieldname');
		
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-su-choose-list.html');
  
?>