<?php
    if ( $perm->has('nc_nwl_add') ) {
        $_ALL_POST =array();
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $extra = array( 'db' 				=> &$db,
                          	'messages'          => &$messages
                        );
            if ( Newsletter::validateAdd($data, $extra) ) { 
                
                    $to=$data["to_email"];
                    $cc  = $data["cc_email"] ;
                    $bcc = $data["bcc_email"] ;
                       
                if($data["from"]!=0){
                    $smtpDetails=array();
                    $fields1 = "*";
                    $condition1 = " WHERE status='".NewsletterSmtp::ACTIVE."' AND smtp_id = '". $data["from"]."'" ;
                    NewsletterSmtp::getList( $db, $smtpDetails , $fields1, $condition1);
                  
                    if(!empty($smtpDetails)){
                        $smtpDetails    = $smtpDetails[0];
                        $socketfromname = $smtpDetails["smtp_name_display"];
                        $socketfrom 	= $smtpDetails["smtp_email_display"];
                        
                        $smtpauthuser	= $smtpDetails["smtp_email"];
                        $smtpauthpass	= $smtpDetails["smtp_password"];
                        $sockethost		= $smtpDetails["smtp_host"];
                        $smtpport		= $smtpDetails["smtp_port"];
                        $smtp_id		= $smtpDetails["smtp_id"]; 
                        $smtpauth		= true;
                    }
                }else{              
                    $smtp_id = 0;               
                }    
                    $from 			= "";
                    $from["name"] 	= $socketfromname;
                    $from["email"] 	= $socketfrom; 
                    
                    $reply_to = $from ;
        
                    $subject = $data["subject"];
                    $message = $data["html_content"];
                    
                    // Check if the mail is to be sent as HTML or PLAIN.
                    if ($data["sendAs"] == '0') {
                        $sendAs = false ;
                    }
                    else {
                        $sendAs = true ;
                    }
                    
                    // Set the time when the email will be sent.
                    $send_time_array = array();
                    if ( @$data["direct_send"] != "1" ) {
                            
                            
                         $dateArr= explode("/",$data['send_on_date']);
                         $send_on_date = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
                         $send_time_array[] = date($send_on_date." H:i:s", $send_time);                        
                         
                         
                        /*
                        $send_time = mktime($data["timeHour"], $data["timeMinute"], $data["timeSecond"], 
                                            $data["dateMonth"], $data["dateDay"], $data["dateYear"]);
                        $send_time -= $dates["time"]["offset"];
                        $send_time_array[] = date("Y-m-d H:i:s", $send_time);
                        */
                    }
                    else {
                        $send_time_array[] = date("Y-m-d H:i:s", time());
                    }
                    //If Only Save 
                    if ( $data["save_newsletter"] == '2' ){
				
                        $temp_from 	= $from["name"] ." <" . $from["email"] .">" ;
                        $temp_reply = $reply_to["name"] ." <" . $reply_to["email"] .">" ;
                        
                        $query = "INSERT INTO " . TABLE_NEWSLETTER . " SET "
                                                . TABLE_NEWSLETTER .".smtp_id = '"		. $smtp_id ."', "
                                                . TABLE_NEWSLETTER .".n_title = '"		. $data["save_newsletter_title"] ."', "
                                                . TABLE_NEWSLETTER .".subject = '"		. $subject ."', "
                                                . TABLE_NEWSLETTER .".message = '"		. $message ."', "
                                                . TABLE_NEWSLETTER .".to = '"			. $data["to"] ."', "
                                                . TABLE_NEWSLETTER .".from = '"		    . $temp_from ."', "
                                                . TABLE_NEWSLETTER .".reply_to = '"	    . $temp_reply ."', "
                                                . TABLE_NEWSLETTER .".cc = '"			. $data["cc"] ."', "
                                                . TABLE_NEWSLETTER .".bcc = '"			. $data["bcc"] ."', "
                                                . TABLE_NEWSLETTER .".is_html = '"		. $data["sendAs"] ."', "
                                                . TABLE_NEWSLETTER .".batch = '"		. $data["batch"] ."', "
                                                . TABLE_NEWSLETTER .".direct_send = '"	. $data["direct_send"]  ."', "
                                                . TABLE_NEWSLETTER .".newsletter = '"	. $data["save_newsletter"]  ."', "
                                            //	. TABLE_NEWSLETTER .".newsletter = '"	. $newsletter_str ."', "
                                                . TABLE_NEWSLETTER .".send_on_date = '". $send_time_array[0] ."', "
                                                . TABLE_NEWSLETTER .".sent_date = '"	. "0" ."' " ;			
                        $db->query($query);
                        $messages->setOkMessage("Newsletter has been saved.");
                        $newsletter_id = $db->last_inserted_id() ;
                        
                        if ( !empty($_POST["direct_send"]) && $_POST["direct_send"] == "1" ) {
                            $attach = NULL;
                             //Code to send email to each email id BOF
                             if(!empty($to)){
                                foreach($to as $key=>$sendto){
                                   
                                    $sendcc=null;
                                    $sendbcc =null;
                                    $sendStatus = SendMail($sendto, $from, $reply_to, $subject, $message, $sendAs, false, $sendcc, $sendbcc, $attach);
                                    $pos    =   strpos($sendStatus,'Email Sent');
                                    
                                    if($pos == false){
                                        //mail not sent 
                                        $email= $sendto['name']." <".$sendto['email'].">";
                                        $query = "INSERT INTO " . TABLE_NEWSLETTER_SENT_EMAIL . " SET "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".n_id = '"	. $newsletter_id ."', "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".email = '"	. $email ."', "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".status = '0', "                                                 
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".do_sent = '"	.date("Y-m-d") ."' " ;
                                        $db->query($query);
                                    }else{
                                        //mail sent successufully
                                        $email= $sendto['name']." <".$sendto['email'].">";
                                        $query = "INSERT INTO " . TABLE_NEWSLETTER_SENT_EMAIL . " SET "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".n_id = '". $newsletter_id ."', "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".email = '". $email ."', "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".status = '1',"                                                 
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".do_sent = '"	.date("Y-m-d") ."' " ;
                                        $db->query($query);
                                        
                                    }
                                 
                                }
                                //
                                $query = "UPDATE " . TABLE_NEWSLETTER ." SET "
                                                       . TABLE_NEWSLETTER .".sent_date = '". time() ."' " 
                                             ." WHERE ". TABLE_NEWSLETTER .".n_id = '". $newsletter_id ."' " ;
                                $db->query($query);
                            }
                             //Code to send email to each email id EOF                            
                            
                            /*
                            $mailresult = SendMail($to, $from, $reply_to, $subject, $message, $sendAs, false, $cc, $bcc, $attach);
                            if ( $mailresult) {
                                $messages->setOkMessage("Newsletter sent Successfully.");
                                if ( $newsletter_id != "" ) {
                                    $query = "UPDATE " . TABLE_NEWSLETTER ." SET "
                                                       . TABLE_NEWSLETTER .".sent_date = '". time() ."' " 
                                             ." WHERE ". TABLE_NEWSLETTER .".n_id = '". $newsletter_id ."' " ;
                                    $db->query($query);
                                }				
                            }
                            else {
                                $messages->setErrorMessage("Sorry! Mail cannot be sent. Please try again latter.");
                                //$variables["message"] = "Mail cannot be sent.<br/>Reason: ". $message;
                            }
                            */
                        }
                        
                    }else{
                    
                    
                        //If Save and Add to queue/Send
                        if ( $data["save_newsletter"] == '1' ){
					
                            $temp_from 	= $from["name"] ." <" . $from["email"] .">" ;
                            $temp_reply = $reply_to["name"] ." <" . $reply_to["email"] .">" ;
                            
                            $query = "INSERT INTO " . TABLE_NEWSLETTER . " SET "
                                                    . TABLE_NEWSLETTER .".smtp_id = '"		. $smtp_id ."', "
                                                    . TABLE_NEWSLETTER .".n_title = '"		. $data["save_newsletter_title"] ."', "
                                                    . TABLE_NEWSLETTER .".subject = '"		. $subject ."', "
                                                    . TABLE_NEWSLETTER .".message = '"		. $message ."', "
                                                    . TABLE_NEWSLETTER .".to = '"			. $data["to"] ."', "
                                                    . TABLE_NEWSLETTER .".from = '"		    . $temp_from ."', "
                                                    . TABLE_NEWSLETTER .".reply_to = '"	    . $temp_reply ."', "
                                                    . TABLE_NEWSLETTER .".cc = '"			. $data["cc"] ."', "
                                                    . TABLE_NEWSLETTER .".bcc = '"			. $data["bcc"] ."', "
                                                    . TABLE_NEWSLETTER .".is_html = '"		. $data["sendAs"] ."', "
                                                    . TABLE_NEWSLETTER .".batch = '"		. $data["batch"] ."', "
                                                    . TABLE_NEWSLETTER .".direct_send = '"	. $data["direct_send"]  ."', "
                                                    . TABLE_NEWSLETTER .".newsletter = '"	. $data["save_newsletter"]  ."', "
                                                //	. TABLE_NEWSLETTER .".newsletter = '"	. $newsletter_str ."', "
                                                    . TABLE_NEWSLETTER .".send_on_date = '". $send_time_array[0] ."', "
                                                    . TABLE_NEWSLETTER .".sent_date = '"	. "0" ."' " ;
                            $db->query($query);
                            $newsletter_id = $db->last_inserted_id() ;
				        }   
				     
                        
                        if ( !empty($_POST["direct_send"]) && $_POST["direct_send"] == "1" ) {
                            $attach = NULL;
                             //Code to send email to each email id BOF
                             if(!empty($to)){
                                foreach($to as $key=>$sendto){
                                   
                                    $sendcc=null;
                                    $sendbcc =null;
                                    $sendStatus = SendMail($sendto, $from, $reply_to, $subject, $message, $sendAs, false, $sendcc, $sendbcc, $attach);
                                    $pos    =   strpos($sendStatus,'Email Sent');
                                    
                                    if($pos == false){
                                        //mail not sent 
                                        $email= $sendto['name']." <".$sendto['email'].">";
                                        $query = "INSERT INTO " . TABLE_NEWSLETTER_SENT_EMAIL . " SET "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".n_id = '"	. $newsletter_id ."', "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".email = '"	. $email ."', "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".status = '0', "                                                 
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".do_sent = '"	.date("Y-m-d") ."' " ;
                                        $db->query($query);
                                    }else{
                                        //mail sent successufully
                                        $email= $sendto['name']." <".$sendto['email'].">";
                                        $query = "INSERT INTO " . TABLE_NEWSLETTER_SENT_EMAIL . " SET "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".n_id = '". $newsletter_id ."', "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".email = '". $email ."', "
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".status = '1',"                                                 
                                                    . TABLE_NEWSLETTER_SENT_EMAIL .".do_sent = '"	.date("Y-m-d") ."' " ;
                                        $db->query($query);
                                        
                                    }
                                 
                                }
                                
                                $query = "UPDATE " . TABLE_NEWSLETTER ." SET "
                                                       . TABLE_NEWSLETTER .".sent_date = '". date("Y-m-d") ."' " 
                                             ." WHERE ". TABLE_NEWSLETTER .".n_id = '". $newsletter_id ."' " ;
                                $db->query($query);
                                
                             }
                             //Code to send email to each email id EOF                            
                            
                            /*
                            $mailresult = SendMail($to, $from, $reply_to, $subject, $message, $sendAs, false, $cc, $bcc, $attach);
                            if ( $mailresult) {
                                $messages->setOkMessage("Newsletter sent Successfully.");
                                if ( $newsletter_id != "" ) {
                                    $query = "UPDATE " . TABLE_NEWSLETTER ." SET "
                                                       . TABLE_NEWSLETTER .".sent_date = '". time() ."' " 
                                             ." WHERE ". TABLE_NEWSLETTER .".n_id = '". $newsletter_id ."' " ;
                                    $db->query($query);
                                }				
                            }
                            else {
                                $messages->setErrorMessage("Sorry! Mail cannot be sent. Please try again latter.");
                                //$variables["message"] = "Mail cannot be sent.<br/>Reason: ". $message;
                            }
                            */
                        }
                        
                        //Code to insert email into queue
                        /**
                        * Insert newsletter and email id's into temp table for mail sent queue.
                        **/
                          
                            // Calculate how many batches will be created.
                            if( empty($data["batch"]) || $data["batch"] == '0' ){
                                $data["batch"] = NEWSLETTER_BATCH;
                            }
                            
                            $total_batches = count($to) + count($cc) + count($bcc);
                            if ( ($total_batches%$data["batch"]) > 0 ) {
                                $total_batches = (int)($total_batches/$data["batch"]) + 1;
                            }
                            else {
                                $total_batches = (int)($total_batches/$data["batch"]);
                            }
                
                            $k 			= 0;
                            $field 		= "to";
                            $batches 	= "";
                            //echo '['. $total_batches .']';
                            for ( $i=0; $i<$total_batches; $i++ ) {
                                for ( $j=0; $j<$data["batch"]; $j++ ) {
                                    //echo ("\n". 'loop : ('. (($i*$collection["batch"]) + $j) .')');
                                    //echo "\n$field($k) i=$i j=$j k=$k";
                                    
                                    if ( !isset(${$field}[$k]["email"]) ) {
                                        $k = 0; // Set the pointer to '0' for reading the 1st email from the queue.
                                        $j-=1;	// No entry was added to the list, so reduce the count so that proper number of emails are sent in each batch.
                                        if ($field == "to") {
                                            $field = "cc";
                                            continue;
                                        }
                                        elseif ($field == "cc") {
                                            $field = "bcc";
                                            continue;
                                        }
                                        elseif ($field == "bcc") {
                                            break;
                                        }
                                    }
                                    
                                    if ( (!isset($batches[$i]["to"])) || (empty($batches[$i]["to"])) ) {
                                        $batches[$i]['to'] = ${$field}[$k]["name"] ."<". ${$field}[$k]["email"] .">,";
                                    }
                                    else {
                                        $batches[$i][$field] .= ${$field}[$k]["name"] ."<". ${$field}[$k]["email"] .">,";
                                        //echo "\n$field($k) added in batches i=$i j=$j k=$k";echo " $batches";
                                    }
                                    $k+=1; // increment the pointer to read the next email from the queue.
                                    //print_r($batches);echo "\n\n";
                                }
                                $batches[$i]["to"] 	= substr($batches[$i]["to"], 0, strlen($batches[$i]["to"])-1);
                                $batches[$i]["cc"] 	= substr($batches[$i]["cc"], 0, strlen($batches[$i]["cc"])-1);
                                $batches[$i]["bcc"] = substr($batches[$i]["bcc"], 0, strlen($batches[$i]["bcc"])-1);
                            }
                                                
                            $batch_id = array();
                            foreach ($batches as $key=>$batch) {
            
                                $query 	= " INSERT INTO " . TABLE_NEWSLETTER_TEMP_EMAILS 
                                                    . " SET "
                                                    //. TABLE_NEWSLETTER_TEMP_EMAILS .".temp_news_id = '" . $newsletter_id ."',"
                                                    . TABLE_NEWSLETTER_TEMP_EMAILS .".n_id = '" . $newsletter_id ."',"
                                                    . TABLE_NEWSLETTER_TEMP_EMAILS .".to = '" . $batch["to"] ."',"
                                                    . TABLE_NEWSLETTER_TEMP_EMAILS .".cc = '" . $batch["cc"] ."',"
                                                    . TABLE_NEWSLETTER_TEMP_EMAILS .".bcc = '". $batch["bcc"] ."'";
                            
                                if ( $db->query($query) ) {
                                    $batch_id[] = $db->last_inserted_id();
                                }
                                
                            }
                            $batch_id = implode(",", $batch_id);
                            
                            foreach ( $send_time_array as $send_time ) {
                                $query 	=  " INSERT INTO " 	. TABLE_NEWSLETTER_TEMP ." SET "
                                                            . TABLE_NEWSLETTER_TEMP .".n_id		= '" . $newsletter_id ."',"
                                                            . TABLE_NEWSLETTER_TEMP .".smtp_id		= '" . $smtp_id ."',"
                                                            . TABLE_NEWSLETTER_TEMP .".subject		= '" . $subject ."',"
                                                            . TABLE_NEWSLETTER_TEMP .".message		= '" . $message ."',"
                                                            . TABLE_NEWSLETTER_TEMP .".sendAs		= '" . $sendAs ."',"
                                                            . TABLE_NEWSLETTER_TEMP .".send_on_date = '" . $send_time ."',"
                                                            . TABLE_NEWSLETTER_TEMP .".batches_remaining = '" . $batch_id ."'";
                                
                                $db->query($query);
                            }
                            $messages->setOkMessage("Newsletter has been added to the Message queue.");
                           
                        
                    }
            } 
        }
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/newsletter.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/newsletter.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/newsletter.php?added=1");   
        }
        else {
            //Get smtp list 
             $smtp  =array();
             $fields =  TABLE_NEWSLETTER_SMTP .".smtp_id" 
						.",". TABLE_NEWSLETTER_SMTP .".smtp_name_display"
						.",". TABLE_NEWSLETTER_SMTP .".smtp_email_display"
						.",". TABLE_NEWSLETTER_SMTP .".smtp_email";
             $condition = " WHERE status='".NewsletterSmtp::ACTIVE."'";
             NewsletterSmtp::getList( $db, $smtp , $fields, $condition_query);
            
       
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'smtp', 'value' => 'smtp');     
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletter-add.html');
        }
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>