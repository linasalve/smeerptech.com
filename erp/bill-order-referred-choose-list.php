<?php

      $showall	= isset($_GET["showall"]) 	? $_GET["showall"]	: ( isset($_POST["showall"]) 	? $_POST["showall"] : '' );
      $or_id	= isset($_GET["or_id"]) 	? $_GET["or_id"]	: ( isset($_POST["or_id"]) 	? $_POST["or_id"] : '' );
      $ord_discount_to	= isset($_GET["ord_discount_to"]) 	? $_GET["ord_discount_to"]	: ( isset($_POST["ord_discount_to"]) 	? $_POST["ord_discount_to"] : '' );
       
        if ( !isset($condition_query) || $condition_query == '' ) {
			$where_added=true;
			$_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(Order::ACTIVE,Order::PENDING); 
			
			$_SEARCH["chk_rstatus"]  = 'AND';
            $_SEARCH["rStatus"]     = array(Order::RACTIVE); 
            $condition_query = " WHERE "
				." ". TABLE_BILL_ORDERS .".status IN('".Order::ACTIVE ."','".Order::PENDING."'".") AND 
				". TABLE_BILL_ORDERS .".ord_referred_status ='".Order::RACTIVE."'";

 
     
		}
		else {
			$condition_query .= ' AND ';
		}
        $access_level   = $my['access_level'];
        
        // If the User has the Right to View Clients of the same Access Level.
        /*if ( $perm->has('nc_ue_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " (". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
        $condition_query .= ')';*/
        
        
       
		
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
          
        // To count total records.
		if(!empty($ord_discount_to)){
			if($where_added){
				$condition_query .= " AND " ;
			}else{
				$condition_query .= ' WHERE ';			
			}
			$condition_query .= " (".TABLE_BILL_ORDERS.".ord_referred_by='".$ord_discount_to."')";
			
			
			if(!empty($or_id)){
				if($where_added){
					$condition_query .= " AND ".TABLE_BILL_ORDERS.".id!=".$or_id ;
				}else{
					$condition_query .= ' WHERE ';
					$condition_query .= " ".TABLE_BILL_ORDERS.".id!=".$or_id ;
				}
			}
			
			if(!empty($order_by_table))
        	$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
			
			$list	= 	NULL;
			$total	=	Order::getDetails( $db, $list, TABLE_BILL_ORDERS.'.id ' , $condition_query);
			$condition_url.="&ord_discount_to=".$ord_discount_to ;
			
			$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
			$extra_url  = '';
			if ( isset($condition_url) && !empty($condition_url) ) {
				$extra_url  = $condition_url;
			}
			$extra_url  .= "&x=$x&rpp=$rpp";
			$extra_url  = '&start=url'. $extra_url .'&end=url';
			
			$flds = TABLE_BILL_ORDERS.".order_title,
			".TABLE_BILL_ORDERS.".status,
			".TABLE_BILL_ORDERS.".ord_referred_status,
			".TABLE_BILL_ORDERS.".number as order_no,
			".TABLE_BILL_ORDERS.".ord_referred_used_against_no,
			".TABLE_BILL_ORDERS.".ord_referred_by_name,
			".TABLE_BILL_ORDERS.".ord_referred_amount,
			".TABLE_BILL_ORDERS.".id as order_id,".TABLE_BILL_ORDERS.".number as ord_number,".TABLE_CLIENTS .'.f_name AS c_f_name'.','. TABLE_CLIENTS .'.l_name AS c_l_name'.','. TABLE_CLIENTS .'.billing_name' ;
			$list	= NULL;
			Order::getDetails( $db, $list, $flds, $condition_query, $next_record, $rpp);
			
		}else{
		
			$messages->setErrorMessage("Select the Person to whome you are giving the DISCOUNT.");
		}
        // Set the Permissions.
		$variables['status'] = Order::getStatus();
		$variables['rstatus'] = Order::getRStatus();
		
        $variables['can_view_list']     = true;
        $variables['can_add']           = true;
        $variables['can_edit']          = true;
        $variables['can_delete']        = true;
        $variables['can_view_details']  = true;
        $variables['can_change_status'] = true;
		
		$hidden[] = array('name'=> 'or_id' ,'value' => $or_id);
		$hidden[] = array('name'=> 'ord_discount_to' ,'value' => $ord_discount_to);
         
        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');

        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'showall', 'value' => 'showall');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-referred-choose-list.html');
  
?>