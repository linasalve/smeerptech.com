<?php
    if ( $perm->has('nc_vjb_edit') ) {
        $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
    
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages 
                        );
            
            if ( VendorsJobwork::validateUpdate($data, $extra) ) {                
				$query  = " UPDATE ". TABLE_VENDORS_JOBWORK
                        ." SET ". TABLE_VENDORS_JOBWORK .".title   		= '". $data['title'] ."'"
						.", ". TABLE_VENDORS_JOBWORK .".vendors_id   		= '". $data['vendors_id'] ."'"
						.", ". TABLE_VENDORS_JOBWORK .".description     	= '". $data['description'] ."'"
                        ." WHERE ". TABLE_VENDORS_JOBWORK .".id   		= '". $data['id'] ."'";

                if ( $db->query($query) ) {
                    $variables['hid'] = $data['id'];					
                    $messages->setOkMessage("Record has been updated.");
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Record was not updated.');
                }
            }
        }

        if ( $messages->getErrorMessageCount() <= 0 ) {
		 
			$condition_query= " WHERE ".TABLE_VENDORS_JOBWORK.".id = '". $id ."' ";
			$_ALL_POST      = NULL;
			$fields = TABLE_VENDORS_JOBWORK.".*,".TABLE_VENDORS.".billing_name,
			".TABLE_VENDORS.".f_name,".TABLE_VENDORS.".l_name" ;      
			if ( VendorsJobwork::getList($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
				$_ALL_POST = $_ALL_POST[0];
				$_ALL_POST['vendors_details']=$_ALL_POST['billing_name']." ".$_ALL_POST['f_name']." ".$_ALL_POST['l_name'] ;
			}
			
		}
       

		if ( !empty($id) ) {
		
		
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}
				$hidden[] = array('name'=> 'perform','value' => 'edit');
				$hidden[] = array('name'=> 'act', 'value' => 'save');
				$hidden[] = array('name'=> 'id', 'value' => $id);
				$hidden[] = array('name'=> 'ajx','value' => $ajx);				 
				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden'); 
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
		 
	
			   $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-jobwork-edit.html');
				
			
		}
		else {
			$messages->setErrorMessage("The Selected Record was not found.");
		}
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
 ?>