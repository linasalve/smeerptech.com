<?php
    if ( $perm->has('nc_ps_or_list') ) {
        $order_id      = isset($_GET["order_id"]) ? $_GET["order_id"] : ( isset($_POST["order_id"]) ? $_POST["order_id"] : '' );
       include_once (DIR_FS_INCLUDES .'/user.inc.php');  
       include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
       
        $fields = TABLE_PROSPECTS_ORDERS .'.*'
                        .','. TABLE_PROSPECTS .'.user_id AS c_user_id'
                        .','. TABLE_PROSPECTS .'.number AS c_number'
                        .','. TABLE_PROSPECTS .'.f_name AS c_f_name'
                        .','. TABLE_PROSPECTS .'.l_name AS c_l_name'
                        .','. TABLE_PROSPECTS .'.billing_name AS billing_name'
                        .','. TABLE_PROSPECTS .'.email AS c_email'
                        .','. TABLE_PROSPECTS .'.status AS c_status';
            
        $condition_query = " WHERE (". TABLE_PROSPECTS_ORDERS .".id = '". $order_id ."' "." )";
            
        if ( ProspectsOrder::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
			// Send the notification mails to the concerned persons.
			$data = $_ALL_POST[0];
			
			$temp = NULL;
			$temp_p = NULL; 
			
			$condition_query_p = " WHERE ".TABLE_PROSPECTS_ORD_P.".ord_no = '". $data['number'] ."'";
            ProspectsOrder::getParticulars($db, $temp, '*', $condition_query_p);
					
			$particulars = "" ;
			if(!empty($temp)){
				$particulars .="<table cellpadding='0' cellspacing='0' border='1' width='100%'>" ;
				foreach ( $temp as $pKey => $parti ){
					$sr = $pKey + 1;
					$service_type = $temp[$pKey]['is_renewable']  ? 'Renewable' : 'Non-Renewable' ;
					$temp_p[] = array(
									'particulars'=>$temp[$pKey]['particulars'],
									'ss_punch_line' =>$temp[$pKey]['ss_punch_line']  
									);
							
					$ss_title =  $temp[$pKey]['ss_title'] ;
					$ss_punch_line =  $temp[$pKey]['ss_punch_line'] ;
					$p_amount =  $temp[$pKey]['p_amount'] ;
					$s_type =  $temp[$pKey]['s_type'] ;
					$s_quantity =  $temp[$pKey]['s_quantity'] ;
					$samount =  $temp[$pKey]['samount'] ;
					$discount_type =  $temp[$pKey]['discount_type'] ; 
					$stot_amount =  $temp[$pKey]['stot_amount'] ; 
					$tax1_name =  $temp[$pKey]['tax1_name'] ; 
					$tax1_value =  $temp[$pKey]['tax1_value'] ; 
					$tax1_amount =  $temp[$pKey]['tax1_amount'] ; 
					$tax1_sub1_name =  $temp[$pKey]['tax1_sub1_name'] ; 
					$tax1_sub1_value =  $temp[$pKey]['tax1_sub1_value'] ; 
					$tax1_sub1_amount =  $temp[$pKey]['tax1_sub1_amount'] ; 
					$tax1_sub2_name =  $temp[$pKey]['tax1_sub2_name'] ; 
					$tax1_sub2_value =  $temp[$pKey]['tax1_sub2_value'] ; 
					$tax1_sub2_amount =  $temp[$pKey]['tax1_sub2_amount'] ; 
					$d_amount =  $temp[$pKey]['d_amount'] ; 
					$stot_amount =  $temp[$pKey]['stot_amount'] ; 
					$tot_amount =  $temp[$pKey]['tot_amount'] ; 
					$discount_type =  $temp[$pKey]['discount_type'] ; 
					$particulars .="
					<tr>
						<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'><b>".$sr."</b> Punchline : ".$temp[$pKey]['ss_punch_line'] ." - Service type  : ".$service_type  ."</td/>
					</tr>
					<tr>
						<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Services : ".$ss_title ."</td/>
					</tr>
					<tr>
						<td> 
							<table border='0'>
								<tr>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Price</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Type</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Qty</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Amt</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Discount</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>DiscType</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Sub Tot</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Taxname</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tax%</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tax Amt</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tot Amt</td>
								</tr>
								<tr>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$p_amount."</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$s_type."</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$s_quantity."</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$samount."</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$d_amount."</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$discount_type."</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$stot_amount."</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_name."<br/>".$tax1_sub1_name."<br/>".$tax1_sub2_name."
									</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_value."<br/>".$tax1_sub1_value."<br/>".$tax1_sub2_value."
									</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_amount."<br/>".$tax1_sub1_amount."<br/>".$tax1_sub2_amount."
									
									</td>
									<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tot_amount."</td>
								</tr>
							</table>
						</td/>
					</tr>
					" ;
				}
				$particulars .= "</table>";
				
			}
			 
			include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
			$file_name='';
			// Send Email to the Client.
			
			$data['af_name'] =$my['f_name'];
			$data['al_name'] = $my['l_name'] ;
			//$data['particulars']=$temp_p;
			 
			//Send mail after create invoice to admin bof/
		 
			$sub_clients_members =''; 
			if( !empty($clients_su_str)){
				$clients_su_str = trim($clients_su_str,",");
				$clients_su_str = str_replace(",","','",$clients_su_str);
				
				Prospects::getList($db, $data['clients_su_members'], 'user_id,number,
				f_name,l_name', "WHERE user_id IN ('". $clients_su_str ."')");
			
				if(!empty($data['clients_su_members'])){
					foreach ( $data['clients_su_members'] as $key=>$members) {							 
						$sub_clients_members .= $members['f_name'] .' '. $members['l_name'] .'';
					}
				}
			}
			$team_members='';
			$team_str = trim($data['team'],",") ;
			if(!empty($team_str )){
				$team_str = trim($team_str,",");
				$team_str = str_replace(",","','",$team_str);
				User::getList($db, $data['team_members'], 'user_id,number,f_name,l_name', " WHERE user_id IN 
				('". $team_str ."')");
				foreach ( $data['team_members'] as $key=>$members) {				
					$team_members .= $members['f_name'] .' '. $members['l_name']."," ;
				}
			}
			 
			
			if($data['do_o']!='0000-00-00 00:00:00'){
				$data['do_o']= date("d F Y",strtotime($data['do_o'])); 
			}			
			/* if($data['do_fe']!='0000-00-00 00:00:00'){
				$data['do_fe']= date("d F Y",strtotime($data['do_fe'])); 
			}
			if($data['do_e']!='0000-00-00 00:00:00'){
				$data['do_e']= date("d F Y",strtotime($data['do_e'])); 
			}
			if($data['st_date']!='0000-00-00 00:00:00'){
				$data['st_date']= date("d F Y",strtotime($data['st_date'])); 
			}
			if($data['es_ed_date']!='0000-00-00 00:00:00'){
				$data['es_ed_date']= date("d F Y",strtotime($data['es_ed_date'])); 
			} */
			$table = TABLE_USER;
            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['created_by'] ."' " ;
            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
            $exeCreted= getRecord($table,$fields1,$condition2);
            $data['added_by'] = $exeCreted['f_name']." ".$exeCreted['l_name']."<br/>" ;
			
			$data['client_name']= $data['c_f_name'] .' '. $data['c_l_name']." ".$data['billing_name']; 
			$data['send_by'] = $my['f_name'].' '.$my['l_name']; 
			$data['sub_clients_members'] = $sub_clients_members; 
			$data['team_members'] = $team_members; 
			$data['particulars1'] = $particulars; 
			$data['currency'] = $data['currency_name']; 
			//$data['link']   = DIR_WS_NC .'/prospects-order.php?perform=view&or_id='.$order_id;
			$data['link2']   = DIR_WS_NC .'/prospects-order.php?perform=edit&or_id='.$order_id;
			$email = NULL;
			$cc_to_sender= $cc = $bcc = Null;
			if ( getParsedEmail($db, $s, 'PROSPECT_ORD_CREATE_TO_ADMIN', $data, $email) ) {
				$to     = '';
				$to[]   = array('name' => $ceo_lead_approval_name , 'email' => $ceo_lead_approval_email);  
				$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);  
				echo  $email["body"];
				SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);  
			    $messages->setOkMessage("Prospect Lead Order details of ".$data['number']." sent for approval.");
			}
			// Display the  list.
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
		}
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Change Status.");
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
        
    }
?>
