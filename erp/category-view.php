<?php
    if ( $perm->has('nc_cat_view') ) {	
		$cat_id = isset ($_GET['cat_id']) ? $_GET['cat_id'] : ( isset($_POST['cat_id'] ) ? $_POST['cat_id'] :'');
	
		$list	            = NULL;
		$condition_query	= NULL;
	
		// Get list of category and subcategory.
		Categories::getComboMenus();
		$option   = null;
		// fetch parent id from list.
		$query ="SELECT sc_parent_id FROM ".TABLE_SETTINGS_CATEGORY. " WHERE sc_cat_id='".$cat_id."'";
		$db->query($query);
		if ($db->nf() > 0){
			$db->next_record();
			$parent_id = $db->f('sc_parent_id');
		}
		
		foreach ($output as $key => $val) {
	
				if ($parent_id==$key) { 
					$select = "selected";
				}
				else {
					$select ='';
				}
			$option.="<option value='$key' $select disabled >$val</option>";
		}
			
		if ( !isset($condition_query) || $condition_query=='' ) {
			$condition_query	=" WHERE sc_cat_id = '". $cat_id."' ";
		}
		
		if ( Categories::getList($db, $list, '*', $condition_query) > 0 ) {
			$data	=	$list['0'];
		}
		else {
			$messages->setErrorMessage("The category list was not found. ");
		}
		
		$page["var"][] = array('variable' => 'data', 'value' => 'data');
		$page["var"][] = array('variable' => 'option', 'value' => 'option');
	
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'category-view.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
