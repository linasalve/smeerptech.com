<?php
  if ( $perm->has('nc_city_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		// Include the payment party class
		//include_once (DIR_FS_INCLUDES .'/city.inc.php');
        include_once (DIR_FS_INCLUDES .'/state.inc.php');
		
        $lst_state = NULL;
        $required_fields ='*';
		State::getList($db,$lst_state,$required_fields);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( City::validateAdd($data, $extra) ) { 
                            $query  = "SELECT code FROM ".TABLE_COUNTRIES." WHERE id='".$data['country_id']."'";
                            $db->query($query);
                            if($db->nf()>0){
                                $db->next_record();
                                $country_code=$db->f('code');
                            }
            
                            $query	= " INSERT INTO ".TABLE_LIST_CITY
                            ." SET ".TABLE_LIST_CITY .".name = '". $data['name'] ."'"  
                                    .",". TABLE_LIST_CITY .".country_id    = '". 		$data['country_id'] ."'"                                
                                    .",". TABLE_LIST_CITY .".state_id      = '". 		$data['state_id'] ."'"                                
                                    .",". TABLE_LIST_CITY .".country_code  = '". 		$country_code ."'"                                
                                	.",". TABLE_LIST_CITY .".status        = '". 		$data['status'] ."'"                                
                                    .",". TABLE_LIST_CITY .".ip            = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                                	.",". TABLE_LIST_CITY .".date          = '". 		date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New city entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/city.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/city.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/city.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'city-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
