<?php
    if ( $perm->has('nc_bk_stmt_list') ) {     
	
		
        if ( (!isset($condition_query) || $condition_query == '' ) && $searchStr==1) {
			
			$curr_m = date('m');
			$curr_y = date('Y');
			
			if($curr_m=='04'|| $curr_m=='03' || $curr_m=='02' || $curr_m=='01'){
				$yr= $curr_y -1;
				$yr1= $curr_y ;
			}else{
				$yr= $curr_y ;
				$yr1= $curr_y +1;
			}
			$dfa =$yr."-04-01 00:00:00" ;
			$dta=$yr1."-03-31 00:00:00" ;
			
            $_SEARCH["dt_field"]  = $dt_field='stmt_date'; 
			$_SEARCH["chk_date_from"]   = 'AND';
			$_SEARCH["date_from"]   = "01/04/".$yr; 
			$_SEARCH["date_to"]     ="31/03/".$yr1 ;
			$_SEARCH["chk_date_to"] = 'AND';
			
			if ( $where_added ) {
				$condition_query .= " AND " ;
			}else {
				$condition_query.= ' WHERE ';
				$where_added    = true;
			} 			 
            $condition_query.="(".TABLE_BANK_STATEMENT .".".$dt_field." <= '". $dta ."' AND ".TABLE_BANK_STATEMENT .".".$dt_field." >= '". $dfa ."')"; 
			
        }  
		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
         //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = ''; 
       
			$fields_tot = " SUM(".TABLE_BANK_STATEMENT.".debit) as totdebit, SUM(".TABLE_BANK_STATEMENT.".credit) as totcredit";
		    $total1	=	BankStatement::getList( $db, $list_tot, $fields_tot, $condition_query); 
            $total	=	BankStatement::getList( $db, $list, '', $condition_query);
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');           
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';        
            $list	= NULL;
			$fields = TABLE_BANK_STATEMENT.".* 
				,".TABLE_PAYMENT_BANK.".bank_name 
				,".TABLE_PAYMENT_BANK.".company_name " ;
            BankStatement::getList( $db, $list, $fields, $condition_query, $next_record, $rpp); 
			if(!empty($list)){
				foreach( $list as $key=>$val){   
					 
					$tran_nos = trim($val['transaction_no'],","); 
					if(!empty($tran_nos)){
						$tran_nos1 = str_replace(",","','",$tran_nos);
						$sql1="SELECT id,number FROM ".TABLE_PAYMENT_TRANSACTION." WHERE number IN('".$tran_nos1."')" ;
						$db->query($sql1); 
						$link_str='';
						if ( $db->nf()>0 ) {
							while ( $db->next_record() ) { 
								$trans_id = $db->f('id');
								$trans_no = $db->f('number');
								
								$link_str.="<a href=\"javascript:void(0);\" 
								onclick=\"openSmallWindow('".DIR_WS_NC."/payment-transaction.php?perform=print&id=".$trans_id."',700,600);\"
								class=\"action\" title=\"\">".$trans_no."</a><br/>";
							}
							$val['tran_link'] = $link_str;
						}
					}
					$flist[$key]=$val;
				}
			}
     
        // Set the Permissions.
        $variables['can_add']     = false;
        $variables['can_edit']     = false;
        $variables['can_delete']     = false;
        $variables['can_view_list']     = false;
        $variables['can_status']     = false;
        $variables['can_private']     = false; 
        if ( $perm->has('nc_bk_stmt_list') ) {
			$variables['can_view_list']     = true;
		}
		if ( $perm->has('nc_bk_stmt_add') ) {
			$variables['can_add']     = true;
		}
		if ( $perm->has('nc_bk_stmt_status') ) {
			$variables['can_status']     = true;
		}
		if ( $perm->has('nc_bk_stmt_multistatus') ) {
			$variables['can_status']     = true;
		}
		
		if ( $perm->has('nc_bk_stmt_edit') ) {
			$variables['can_edit']     = true;
		}
		if ( $perm->has('nc_bk_stmt_delete') ) {
			$variables['can_delete'] = true;
		}
		if ( $perm->has('nc_bk_stmt_private') ) {
			$variables['can_private'] = true;
		}
		
		$banklst	= NULL;
        $fieldsi = TABLE_PAYMENT_BANK.'.*' ;
        $condition_queryib = " WHERE ".TABLE_PAYMENT_BANK.".type = '1' AND ".TABLE_PAYMENT_BANK.".status='1' ORDER BY bank_name";
        Paymentbank::getDetails($db, $banklst, $fieldsi, $condition_queryib);
		 
		$variables['status'] = BankStatement::getStatus();
		$variables['lstatus'] = BankStatement::getLStatus();
		
		include_once ( DIR_FS_INCLUDES .'/company.inc.php');
		$condition_query1 = "ORDER BY name ";
		$fields = TABLE_SETTINGS_COMPANY.'.id,'.TABLE_SETTINGS_COMPANY.".name" ;
		Company::getDetails( $db, $company_list, $fields, $condition_query1);
		 
        //$variables['searchLink']=$searchLink ;
        $page["var"][] = array('variable' => 'banklst', 'value' => 'banklst');
        $page["var"][] = array('variable' => 'list_tot', 'value' => 'list_tot');
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'company_list', 'value' => 'company_list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bank-statement-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>