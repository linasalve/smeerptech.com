<?php

    if ( $perm->has('nc_bill_ord_tax_list') ) {        
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
         //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
		$totTaxPaidAmt = 0;
        if($searchStr==1){
			
			$fld = "SUM(".TABLE_BILL_ORD_TAX.".total_tax_paid_amount) as totTaxPaidAmt" ;
            $totalArr	=	BillOrderTax::getList( $db, $list1, $fld, $condition_query);
			 
			if(!empty($list1)){
				$totalArr1 = $list1[0];
				$totTaxPaidAmt = number_format($totalArr1['totTaxPaidAmt'],2);
			}
			
            $total	=	BillOrderTax::getList( $db, $list, '', $condition_query);
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');           
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';        
            $list	= NULL;
			$fields = TABLE_BILL_ORD_TAX.".* 
				,".TABLE_BILL_ORDERS.".order_title 
				,".TABLE_BILL_ORDERS.".total_tax_amount
				,".TABLE_BILL_ORDERS.".total_tax_paid_amount
				,".TABLE_BILL_ORDERS.".total_tax_balance_amount " ;
				
            BillOrderTax::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        
		
		
		
		
        // Set the Permissions.
        $variables['can_add']     = false;
        $variables['can_view_list']     = false;
        if ( $perm->has('nc_bill_ord_tax_list') ) {
			$variables['can_view_list']     = true;
		}
		if ( $perm->has('nc_bill_ord_tax_csv') ) {
			$variables['can_add']     = true;
		}
		
       
        //$variables['searchLink']=$searchLink ;
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'totTaxPaidAmt', 'value' => 'totTaxPaidAmt');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-tax-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>