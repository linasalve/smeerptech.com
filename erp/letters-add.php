<?php
  if ( $perm->has('nc_letr_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
	   
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
			.','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
		
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
			$files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_REPOSITORY_FILE_SIZE     ;
                 
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
						);
            
          
               
				if( Letters::validateAdd($data, $extra) ) {  
					
					$company1 =null;
					$condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
					$fields_c1 = "name,prefix";
					Company::getList( $db, $company1, $fields_c1, $condition_query_c1); 
					if(!empty($company1)){                  
						$data['company_name'] = $company1[0]['name'];
						$data['company_prefix'] = $company1[0]['prefix'];
					}
					if(!empty($data['file_1'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_1']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-1".".".$ext ;
						$data['file_1'] = $attachfilename;
						
						if(move_uploaded_file($files['file_1']['tmp_name'], 
							DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   
							@chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						}
					}
					if(!empty($data['file_2'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_2']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-2".".".$ext ;
						$data['file_2'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_2']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						}
					}
					if(!empty($data['file_3'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_3']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-3".".".$ext ;
						$data['file_3'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_3']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						}
					}				
					if(!empty($data['file_4'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_4']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-4".".".$ext ;
						$data['file_4'] = $attachfilename;                    
						if(move_uploaded_file ($files['file_4']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						}
					}
					if(!empty($data['file_5'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_5']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-5".".".$ext ;
						$data['file_5'] = $attachfilename;                    
						if(move_uploaded_file($files['file_5']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						}
					}	
					
                   $query	= " INSERT INTO ".TABLE_LETTERS
                            ." SET ".TABLE_LETTERS .".filename = '". $data['filename'] ."'"                            
							.",". TABLE_LETTERS .".number 	= '". $data['number'] ."'"
							.",". TABLE_LETTERS .".company_id 	= '". $data['company_id'] ."'"
							.",". TABLE_LETTERS .".company_name 	= '". $data['company_name'] ."'"
							.",". TABLE_LETTERS .".company_prefix  	= '". $data['company_prefix'] ."'"
							.",". TABLE_LETTERS .".counter 	= '". $data['counter'] ."'"
							.",". TABLE_LETTERS .".do_l 	= '".date("Y-m-d h:i:s",$data['do_l']) ."'"
							.",". TABLE_LETTERS .".vendor_bank_id 	= '". $data['vendor_bank_id'] ."'"
							.",". TABLE_LETTERS .".vendor_bank_details 	= '". $data['vendor_bank_details'] ."'"
							.",". TABLE_LETTERS .".client_id 	= '". $data['client_id'] ."'"
							.",". TABLE_LETTERS .".client_details 	= '". $data['client_details'] ."'"
							.",". TABLE_LETTERS .".executive_id 	= '". $data['executive_id'] ."'"
							.",". TABLE_LETTERS .".executive_details = '". $data['executive_details'] ."'"
							.",". TABLE_LETTERS .".subject 	= '". $data['subject'] ."'"
							.",". TABLE_LETTERS .".type 	= '". $data['type'] ."'"
							.",". TABLE_LETTERS .".doc_of 	= '". $data['doc_of'] ."'"
                            .",". TABLE_LETTERS .".text   = '". $data['text'] ."'"
                            .",". TABLE_LETTERS .".description = '". $data['description'] ."'"
                            .",". TABLE_LETTERS .".file_1 = '".    $data['file_1'] ."'"
                            .",". TABLE_LETTERS .".file_2 = '".    $data['file_2'] ."'"
                            .",". TABLE_LETTERS .".file_3 = '".    $data['file_3'] ."'"
                            .",". TABLE_LETTERS .".file_4 = '".    $data['file_4'] ."'"
                            .",". TABLE_LETTERS .".file_5 = '".    $data['file_5'] ."'"
                            .",". TABLE_LETTERS .".ip     = '".   $_SERVER['REMOTE_ADDR'] ."'"                            .",". TABLE_LETTERS .".created_by = 	  '".$my['user_id']."'"
                            .",". TABLE_LETTERS .".created_by_name = '".$my['f_name']." ".$my['l_name'] ."'"
                            .",". TABLE_LETTERS .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                    
					
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Record has been added successfully.");
                        $variables['hid'] = $db->last_inserted_id();              
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    //$data		= NULL;
		        }
        }               
        
        
			$hidden[] = array('name'=> 'ajx','value' => $ajx);
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');     
             
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'letters-add.html');
         
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
