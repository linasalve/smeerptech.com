<?php
if ( $perm->has('nc_sms_sl_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    
    $variables['active']=SmsSaleClient::ACTIVE;
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    $condition_query = " LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id=".TABLE_SMS_SALE_CLIENT.".client_id 
                         LEFT JOIN ".TABLE_SMS_CLIENT_ACCOUNT." ON ".TABLE_SMS_CLIENT_ACCOUNT.".id=".TABLE_SMS_SALE_CLIENT.".client_acc_id ".$condition_query;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $fields = TABLE_SMS_SALE_CLIENT.'.id ' ;
    $total	=	SmsSaleClient::getDetails( $db, $list,'', $condition_query);

    $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
    // $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';

    $list	= NULL;
    //$fields = TABLE_SMS_SALE_CLIENT.'.*' ;
    $fields = TABLE_SMS_SALE_CLIENT .'.*'
                    .','. TABLE_CLIENTS.'.f_name as c_fname '.','. TABLE_CLIENTS.'.l_name as c_lname, '. TABLE_SMS_CLIENT_ACCOUNT.'.name as account_name';
    SmsSaleClient::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    // Set the Permissions.
   
    $variables['can_view_list'] = false;
    $variables['can_add'] = false;
    //$variables['can_edit'] = false;
    $variables['can_renew'] = false;
    $variables['can_delete'] = false;
    $variables['can_view_h'] = false;
    
    if ( $perm->has('nc_sms_sl_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_sms_sl_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_sms_sl_renew') ) {
        $variables['can_renew'] = true;
    }
    if ( $perm->has('nc_sms_sl_delete') ) {
        $variables['can_delete'] = true;
    }
     if ( $perm->has('nc_sms_sl_view_h') ) {
        $variables['can_view_h'] = true;
    }
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-sale-client-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
