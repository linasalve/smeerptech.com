<?php

// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	
	include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
	include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
	 
	$totalAmountInrOnlyPrfm1=$totalAmountInrOnlyPrfm2=0;
	$totalAmountInrOnlyPrfmInv1=$totalAmountInrOnlyPrfmInv2=0;
	$totalAmountInrOnlyInv1=$totalAmountInrOnlyInv2=0;
	
	//Only profm 1 Apr 09 - 31 mar 2010 
	$condition_query1=" WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' 
	AND billing_orders.is_invoice_create ='0' AND billing_inv_proforma.do_i >= '2009-04-01 00:00:00' AND 
	billing_inv_proforma.do_i <= '2010-03-31 23:59:59' ";
	
	$totalAmt =array();
	$totalAmount='';
	$fields1=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, SUM(".TABLE_BILL_INV_PROFORMA.".balance) as 
	totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields1, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfm1=$totalAmt[0]['totalAmountInr'];
		//$totalAmount1=number_format($totalAmt[0]['totalAmount'],2);
		//$totalBalanceInr1 = number_format($totalAmt[0]['totalBalanceInr'],2);
		//$totalBalance1 = number_format($totalAmt[0]['totalBalance'],2);
	}
	
	
	$condition_query1=" WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' AND billing_orders.is_invoice_create ='1' AND billing_inv_proforma.do_i >= '2009-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2010-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields2=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV_PROFORMA.".balance) as totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields2, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmInv1=$totalAmt[0]['totalAmountInr'];
		//$totalAmount1=number_format($totalAmt[0]['totalAmount'],2);
		//$totalBalanceInr1 = number_format($totalAmt[0]['totalBalanceInr'],2);
		//$totalBalance1 = number_format($totalAmt[0]['totalBalance'],2);
	}
	
	
	
	$condition_query1=" where billing_inv.status IN ('2','4') AND billing_orders.is_invoiceprofm_create ='0' AND 	
	billing_orders.is_invoice_create ='1' AND billing_inv.do_i >= '2009-04-01 00:00:00' AND billing_inv.do_i <= '2010-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields3=" SUM(".TABLE_BILL_INV.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV.".balance) as totalBalance ";
	Invoice::getDetails(  $db, $totalAmt, $fields3, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyInv1=$totalAmt[0]['totalAmountInr'];
		//$totalAmount1=number_format($totalAmt[0]['totalAmount'],2);
		//$totalBalanceInr1 = number_format($totalAmt[0]['totalBalanceInr'],2);
		//$totalBalance1 = number_format($totalAmt[0]['totalBalance'],2);
	}
	
	

	
	
	
	
	
	//Only profm 1Apr 10- 31 mar 2011
	//------------------------------------------ST  1
	
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' AND 
	billing_orders.is_invoice_create = '0' AND billing_inv_proforma.tax_ids LIKE '%,1,%'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields4=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, SUM(".TABLE_BILL_INV_PROFORMA.".balance) as 
	totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields4, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmST2=$totalAmt[0]['totalAmountInr'];
	}
	
	//VAT 2
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' AND 
	billing_orders.is_invoice_create = '0' AND billing_inv_proforma.tax_ids LIKE '%,2,%'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields4=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, SUM(".TABLE_BILL_INV_PROFORMA.".balance) as 
	totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields4, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmVAT2=$totalAmt[0]['totalAmountInr'];
	}
	
	//CST 5	
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' AND 
	billing_orders.is_invoice_create = '0' AND billing_inv_proforma.tax_ids LIKE '%,5,%'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields4=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, SUM(".TABLE_BILL_INV_PROFORMA.".balance) as 
	totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields4, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmCST2=$totalAmt[0]['totalAmountInr'];
	}
	
	//NOTAX with Hardware ord
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' AND 
	billing_orders.is_invoice_create = '0' AND (billing_inv_proforma.tax_ids = '' OR billing_inv_proforma.tax_ids = ',0,')
	AND is_hardware_ord ='1'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields4=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, SUM(".TABLE_BILL_INV_PROFORMA.".balance) as 
	totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields4, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmNTHD2=$totalAmt[0]['totalAmountInr'];
	}
	
	//NOTAX without Hardware ord
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' AND 
	billing_orders.is_invoice_create = '0' AND (billing_inv_proforma.tax_ids = '' OR billing_inv_proforma.tax_ids = ',0,')
	AND is_hardware_ord ='0'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields4=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, SUM(".TABLE_BILL_INV_PROFORMA.".balance) as 
	totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields4, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmNTWHD2=$totalAmt[0]['totalAmountInr'];
	}
	
	
	/*
	ST  1
	VAT 2
	CST 5	
	NOTax tax_ids = '' OR tax_ids= ',0,'
	LIKE '%". $sString ."%'
	*/
	
	//	PROFM + INV - ST
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' 
	AND billing_orders.is_invoice_create ='1' AND billing_inv_proforma.tax_ids LIKE '%,1,%'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59' ";
	
	$totalAmt = array();
	$totalAmount = '';
	$fields=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV_PROFORMA.".balance) as totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmInvST2=$totalAmt[0]['totalAmountInr'];
 
	}
	
	//	PROFM + INV - VAT
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' 
	AND billing_orders.is_invoice_create ='1' AND billing_inv_proforma.tax_ids LIKE '%,2,%'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59' ";
	
	$totalAmt = array();
	$totalAmount = '';
	$fields=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV_PROFORMA.".balance) as totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmInvVAT2=$totalAmt[0]['totalAmountInr'];
		 
	}
	
	//	PROFM + INV - CST
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' 
	AND billing_orders.is_invoice_create ='1' AND billing_inv_proforma.tax_ids LIKE '%,5,%'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59' ";
	
	$totalAmt = array();
	$totalAmount = '';
	$fields=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV_PROFORMA.".balance) as totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmInvCST2=$totalAmt[0]['totalAmountInr'];
	}
	 
	//	PROFM + INV - NOTAX with Hardware
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' 
	AND billing_orders.is_invoice_create ='1' AND (billing_inv_proforma.tax_ids = '' OR billing_inv_proforma.tax_ids = ',0,')
	AND is_hardware_ord ='1'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59' ";
	
	$totalAmt = array();
	$totalAmount = '';
	$fields=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV_PROFORMA.".balance) as totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmInvNTHD2=$totalAmt[0]['totalAmountInr'];
	}
	//	PROFM + INV - NOTAX without Hardware
	$condition_query1 = " WHERE billing_inv_proforma.status IN ('1','2','4') AND billing_orders.is_invoiceprofm_create ='1' 
	AND billing_orders.is_invoice_create ='1' AND (billing_inv_proforma.tax_ids = '' OR billing_inv_proforma.tax_ids = ',0,')
	AND is_hardware_ord ='0'
	AND billing_inv_proforma.do_i >= '2010-04-01 00:00:00' AND billing_inv_proforma.do_i <= '2011-03-31 23:59:59' ";
	
	$totalAmt = array();
	$totalAmount = '';
	$fields=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV_PROFORMA.".balance) as totalBalance ";
	InvoiceProforma::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyPrfmInvNTWHD2=$totalAmt[0]['totalAmountInr'];
	}
		
	// INV - ST 
	$condition_query1=" where billing_inv.status IN ('2','4') AND billing_orders.is_invoiceprofm_create ='0' 
	AND billing_orders.is_invoice_create ='1' AND billing_inv.tax_ids LIKE '%,1,%'
	AND billing_inv.do_i >= '2010-04-01 00:00:00'AND billing_inv.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields=" SUM(".TABLE_BILL_INV.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV.".balance) as totalBalance ";
	Invoice::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyInvST2= $totalAmt[0]['totalAmountInr'] ;
		
	}
	
	// INV - VAT 
	$condition_query1=" where billing_inv.status IN ('2','4') AND billing_orders.is_invoiceprofm_create ='0' 
	AND billing_orders.is_invoice_create ='1' AND billing_inv.tax_ids LIKE '%,2,%'
	AND billing_inv.do_i >= '2010-04-01 00:00:00'AND billing_inv.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields=" SUM(".TABLE_BILL_INV.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV.".balance) as totalBalance ";
	Invoice::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyInvVAT2= $totalAmt[0]['totalAmountInr'] ;
		
	}

	// INV - CST 
	$condition_query1=" where billing_inv.status IN ('2','4') AND billing_orders.is_invoiceprofm_create ='0' 
	AND billing_orders.is_invoice_create ='1' AND billing_inv.tax_ids LIKE '%,5,%'
	AND billing_inv.do_i >= '2010-04-01 00:00:00'AND billing_inv.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields=" SUM(".TABLE_BILL_INV.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV.".balance) as totalBalance ";
	Invoice::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyInvCST2= $totalAmt[0]['totalAmountInr'] ;
	}
	
	// INV - NO TAX With Hardware
	$condition_query1=" where billing_inv.status IN ('2','4') AND billing_orders.is_invoiceprofm_create ='0' 
	AND billing_orders.is_invoice_create ='1' AND (billing_inv.tax_ids = '' OR billing_inv.tax_ids = ',0,')
	AND billing_orders.is_hardware_ord = '1'
	AND billing_inv.do_i >= '2010-04-01 00:00:00'AND billing_inv.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields=" SUM(".TABLE_BILL_INV.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV.".balance) as totalBalance ";
	Invoice::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyInvNTHD2= $totalAmt[0]['totalAmountInr'] ;
	}
	
	//INV - NO TAX Without Hardware
	$condition_query1=" where billing_inv.status IN ('2','4') AND billing_orders.is_invoiceprofm_create ='0' 
	AND billing_orders.is_invoice_create ='1' AND (billing_inv.tax_ids = '' OR billing_inv.tax_ids = ',0,')
	AND billing_orders.is_hardware_ord = '0'
	AND billing_inv.do_i >= '2010-04-01 00:00:00'AND billing_inv.do_i <= '2011-03-31 23:59:59'";
	
	$totalAmt =array();
	$totalAmount='';
	$fields=" SUM(".TABLE_BILL_INV.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV.".balance_inr) 
	as totalBalanceInr, SUM(".TABLE_BILL_INV.".amount) as totalAmount, 
	SUM(".TABLE_BILL_INV.".balance) as totalBalance ";
	Invoice::getDetails(  $db, $totalAmt, $fields, $condition_query1);
	if(!empty($totalAmt)){
		$totalAmountInrOnlyInvNTWHD2= $totalAmt[0]['totalAmountInr'] ;
	}
	
	
	$total1 = $totalAmountInrOnlyPrfm1 + $totalAmountInrOnlyPrfmInv1 + $totalAmountInrOnlyInv1 ;
	
	$totalST2 = $totalAmountInrOnlyPrfmST2 + $totalAmountInrOnlyPrfmInvST2 + $totalAmountInrOnlyInvST2 ;
	$totalVAT2 = $totalAmountInrOnlyPrfmVAT2 + $totalAmountInrOnlyPrfmInvVAT2 + $totalAmountInrOnlyInvVAT2 ;
	$totalCST2 = $totalAmountInrOnlyPrfmCST2 + $totalAmountInrOnlyPrfmInvCST2 + $totalAmountInrOnlyInvCST2 ;
	$totalNTHD2 = $totalAmountInrOnlyPrfmNTHD2 + $totalAmountInrOnlyPrfmInvNTHD2 + $totalAmountInrOnlyInvNTHD2 ;
	$totalNTWHD2 = $totalAmountInrOnlyPrfmNTWHD2 + $totalAmountInrOnlyPrfmInvNTWHD2 + $totalAmountInrOnlyInvNTWHD2 ;
	
	$total2=$totalST2+$totalVAT2+$totalCST2 +$totalNTHD2 + $totalNTWHD2;
	
	echo "<br/>For 1 Apr 2009- 31 Mar 2010-----------------------------------------";
	echo "<br/> Total of Proforma + Not Invoice = ".number_format($totalAmountInrOnlyPrfm1,2) ;
	echo "<br/> Total of Proforma + Invoice = ".number_format($totalAmountInrOnlyPrfmInv1,2) ;
	echo "<br/> Total of Not Proforma + Invoice = ".number_format($totalAmountInrOnlyInv1,2) ;
	echo "<br/> <b>TOTAL</b> = ".number_format($total1,2) ;
	
	echo "<br/><br/>For 1 Apr 2010- 31 Mar 2011 -----------------------------------------";
	echo "<br/> ST " ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(nc)  = ".$totalAmountInrOnlyPrfmST2 ;
	echo "<br/>&nbsp;&nbsp;&nbsp; P(nc) + I(c)  = ".$totalAmountInrOnlyInvST2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(c)  = ".$totalAmountInrOnlyPrfmInvST2 ;
	echo "<br/>&nbsp;&nbsp;&nbsp; = ".number_format($totalST2,2) ;
	
	echo "<br/> VAT " ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(nc)  = ".$totalAmountInrOnlyPrfmVAT2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(nc) + I(c)  = ".$totalAmountInrOnlyInvVAT2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(c)  = ".$totalAmountInrOnlyPrfmInvVAT2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;  = ".number_format($totalVAT2,2) ;
	
	echo "<br/> CST " ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(nc)  = ".$totalAmountInrOnlyPrfmCST2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(nc) + I(c)  = ".$totalAmountInrOnlyInvCST2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(c)  = ".$totalAmountInrOnlyPrfmInvCST2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;  = ".number_format($totalCST2,2) ;
	
	echo "<br/> NOTAX (Hardware Ord) " ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(nc)  = ".$totalAmountInrOnlyPrfmNTHD2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(nc) + I(c)  = ".$totalAmountInrOnlyInvNTHD2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(c)  = ".$totalAmountInrOnlyPrfmInvNTHD2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;  = ".number_format($totalNTHD2,2) ;
	
	echo "<br/> NOTAX (Without Hardware Ord)" ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(nc)  = ".$totalAmountInrOnlyPrfmNTWHD2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(nc) + I(c)  = ".$totalAmountInrOnlyInvNTWHD2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;P(c) + I(c)  = ".$totalAmountInrOnlyPrfmInvNTWHD2 ;
	echo "<br/> &nbsp;&nbsp;&nbsp;  = ".number_format($totalNTWHD2,2) ;
	
	echo "<br/><br/> <b>TOTAL</b>= ".number_format($total2,2) ;

	 
?>