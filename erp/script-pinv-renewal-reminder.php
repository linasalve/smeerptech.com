<?php

// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
	 
	 
	
	$sql= "SELECT ".TABLE_BILL_ORDERS.".id as or_id,".TABLE_BILL_ORDERS.".number as ord_no,
     ".TABLE_BILL_ORDERS.".is_invoice_create, ".TABLE_BILL_ORDERS.".is_invoiceprofm_create,
	 ".TABLE_BILL_INV_PROFORMA.".id as pinv_id, ".TABLE_BILL_INV_PROFORMA.".number as pinv_no,
	 ".TABLE_BILL_INV_PROFORMA.".client,".TABLE_BILL_INV_PROFORMA.".do_e
	 FROM ".TABLE_BILL_ORDERS." LEFT JOIN ".TABLE_BILL_INV_PROFORMA." ON ".TABLE_BILL_ORDERS.".id = 
	 ".TABLE_BILL_INV_PROFORMA.".or_id WHERE ".TABLE_BILL_ORDERS.".is_renewal_ord='1' AND 
	 ".TABLE_BILL_ORDERS.".is_invoice_create='0' AND 
	 ".TABLE_BILL_ORDERS.".is_invoiceprofm_create='1' AND 
	 ".TABLE_BILL_INV_PROFORMA.".status IN('".InvoiceProforma::ACTIVE."','".InvoiceProforma::PENDING."')";

	
	$db->query($sql);
     $i=0;
    $db1 		= new db_local;
    if ( $db->nf()>0 ) {
        while ( $db->next_record() ) { 
               
			
			$data["do_e"] =$db->f('do_e');
			$data["pinv_id"] = $db->f('pinv_id');
			echo $data["pinv_no"] = $db->f('pinv_no');
			echo "<br/>";
			$data["client"] = $db->f('client');
			// BO: Set up Reminder for invoice expiry, if applicable.
			if ( !empty($data["do_e"]) && $data["do_e"] != "0000-00-00 00:00:00" ) {
				echo $i++."---<br/>";	 
				$dayArr = InvoiceProforma::expirySetDays();
				foreach($dayArr as $key =>$val){
					 
					$temp = explode(' ',$data["do_e"]);
					$temp1 =explode('-',$temp[0]);
					$dd=$temp1[2] - $val ;
					$mm=$temp1[1];
					$yy=$temp1[0];
					$sendDt = mktime(0,0,0,$mm,$dd,$yy);                            
					$send_reminder_dt=date('Y-m-d',$sendDt);
					echo $query_rm1	= " INSERT INTO ".TABLE_INV_REMINDER
					." SET ". TABLE_INV_REMINDER .".client_id = '".$data['client']."'"
					.",". TABLE_INV_REMINDER .".pinv_id = '".$data["pinv_id"] ."'"
					.",". TABLE_INV_REMINDER .".pinv_no = '".$data["pinv_no"] ."'"
					.",". TABLE_INV_REMINDER .".do_expiry = '".$data["do_e"] ."'"
					.",". TABLE_INV_REMINDER .".date = '".date('Y-m-d h:i:s') ."'"
					.",". TABLE_INV_REMINDER .".days_before_expiry = '". $val ."'"
					.",". TABLE_INV_REMINDER .".send_reminder_dt = '".$send_reminder_dt ."'"  
					.",". TABLE_INV_REMINDER .".created_by = '".     $my['user_id'] ."'"
					.",". TABLE_INV_REMINDER .".ip                  = '".$_SERVER['REMOTE_ADDR']."'" ;
					echo $db1->query($query_rm1)   ;
				    echo "<br/>";
					echo $query_rm2	= " INSERT INTO ".TABLE_INV_REMINDER_CRON
					." SET ". TABLE_INV_REMINDER_CRON .".client_id = '".$data['client']."'"
					.",". TABLE_INV_REMINDER_CRON .".pinv_id = '".$data["pinv_id"] ."'"
					.",". TABLE_INV_REMINDER_CRON .".pinv_no = '". $data["pinv_no"] ."'"
					.",". TABLE_INV_REMINDER_CRON .".do_expiry = '". $data["do_e"] ."'"
					.",". TABLE_INV_REMINDER_CRON .".date = '".date('Y-m-d h:i:s') ."'"
					.",". TABLE_INV_REMINDER_CRON .".days_before_expiry = '". $val ."'"
					.",". TABLE_INV_REMINDER_CRON .".send_reminder_dt = '".$send_reminder_dt ."'" 
					.",". TABLE_INV_REMINDER_CRON .".created_by = '".     $my['user_id'] ."'"
					.",". TABLE_INV_REMINDER_CRON .".ip = '".$_SERVER['REMOTE_ADDR']."'" ;
					echo $db1->query($query_rm2)   ;
				}                                
			} 
			  
		}
	}
	
	
?>  		 