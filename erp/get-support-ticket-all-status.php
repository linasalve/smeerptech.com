<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    
	include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');   
	
   
    
	$ticket_id =  isset($_GET["ticket_id"])   ? $_GET["ticket_id"]  : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
	$status = isset($_GET["status"])   ? $_GET["status"]  : ( isset($_POST["status"])    ? $_POST["status"] : '' );
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new 		= new db_local; // database handle
	
    if (isset($status) && $status!=''){
	
        if( $status!=''){
			$sql= " UPDATE ".TABLE_TICKET_ALL." SET 
			 ".TABLE_TICKET_ALL.".status='".$status."', 
			 ".TABLE_TICKET_ALL .".ip_u  = '".$_SERVER['REMOTE_ADDR']."',  
			 ".TABLE_TICKET_ALL .".do_u = '".date('Y-m-d',time())."',
			 ".TABLE_TICKET_ALL.".updated_by='".$my['user_id']."',
             ".TABLE_TICKET_ALL.".updated_by_name='".$my['f_name']." ".$my['l_name']."' 
			 WHERE ".TABLE_TICKET_ALL.".ticket_id ='".$ticket_id."'";
			
			$execute = $db_new->query($sql);
			$sql= " UPDATE ".TABLE_TICKET_ALL_BATCHES." SET 
			 ".TABLE_TICKET_ALL_BATCHES.".status='".SupportTicket::STALL_BATCH_CANCELLED."'			 
			 WHERE ".TABLE_TICKET_ALL_BATCHES.".ticket_id ='".$ticket_id."'";
			$execute = $db_new->query($sql);
			
			if($execute){
				$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				<tr>
					<td class=\"message_header_ok\">
						<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
						Success
					</td>
				</tr>
				<tr>
					<td class=\"message_ok\" align=\"left\" valign=\"top\">
						<ul class=\"message_ok_2\">
							<li class=\"message_ok\">Status marked to completed successfully </li>
						</ul>
					</td>
				</tr>
				</table>";
				$message .= "| Cancelled ";
				$message .= "| Cancelled ";
				$message .= "| <img src=\"".addslashes($variables['nc_images'])."/edit-off.gif\" border=\"0\" 
							title=\"Edit\" name=\"Edit\" alt=\"Edit\"/> ";
				$message .= "| Cancelled "; 
			}
        } 	   
	}	

echo $message."|".$ticket_id ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
