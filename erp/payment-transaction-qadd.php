<?php
   if ( $perm->has('nc_p_pt_qk_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level']; 
        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
           
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages,
							'_ALL_POST'          => &$_ALL_POST
                        );
            
            
            
            if ( Paymenttransaction::validateAddShort($data, $extra) ) {
			 
                if( $messages->getErrorMessageCount() <= 0){
             
                    $query	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION
                                ." SET ". TABLE_PAYMENT_TRANSACTION .".number = '".$data['number'] ."'"   
									.",". TABLE_PAYMENT_TRANSACTION .".date = '".date("Y-m-d h:i:s")."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".user_id = '".$my['uid'] ."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".remarks = '".$data['remarks'] ."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".ip = '".$_SERVER['REMOTE_ADDR']."'"
                                    .",". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::PENDING ."'";
                
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                         $variables['hid'] = $db->last_inserted_id(); 
                        //After insert, update transaction counter in 
                        //updateCounterOf($db,CONTR_VCH,$data['company_id']);
                        // updateOldCounterOf($db,CONTR_VCH, $data['company_id'],$data['financialYr']);  
						
                        $messages->setOkMessage("Payment transaction entry has been done.");
                       
                        $dataDetails=array();
						 
						
                        //Send mail to admin after adding record bof
                        $fields = TABLE_PAYMENT_TRANSACTION .'.* , '.TABLE_USER .'.f_name as efname, '.TABLE_USER.'.l_name as elname, '.TABLE_PAYMENT_ACCOUNT_HEAD .'.account_head_name, ' . TABLE_PAYMENT_MODE .'.payment_mode ,'. TABLE_VENDORS .'.f_name as fname, '
                        . TABLE_VENDORS .'.l_name as lname, '. TABLE_VENDORS .'.billing_name as ppcompany_name, '.TABLE_SETTINGS_COMPANY.'.name AS company_name';
                        $condition_query=" WHERE ".TABLE_PAYMENT_TRANSACTION.".id= ".$variables['hid'] ;
                        Paymenttransaction::getDetails($db, $dataDetails, $fields, $condition_query) ;
                       
                        $dataDetails = $dataDetails[0] ;
                        $email = NULL;    
                        $statusArr = Paymenttransaction::getStatusAll();
                        $statusArr = array_flip($statusArr);
                        if(!empty($dataDetails)){
                               $dataDetails['will_receipt_generate_name']='No';
                               $dataDetails['is_pdc_name']='No';
                               $dataDetails['is_bill_receive_name'] = 'No';
                               $dataDetails['creator']= $my['f_name']." ".$my['l_name'] ;
                               $dataDetails['status_name'] = $statusArr[$dataDetails['status']];
                               //get bank name on the basis of type IN/OUT
                               if( $dataDetails['transaction_type'] ==Paymenttransaction::PAYMENTIN ){
                               
                                    $table = TABLE_PAYMENT_BANK;
                                    $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$dataDetails['credit_pay_bank_id'] ."' " ;
                                    $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                                    $banknameArr = getRecord($table,$fields1,$condition2);
                                    if(!empty($banknameArr)){
                                        $bankname = $banknameArr['bank_name'] ;                    
                                    }
                                    $dataDetails['transaction_typename']='Payment In';
                                    
                                    if($dataDetails['will_receipt_generate']){
                                        $dataDetails['will_receipt_generate_name']='Yes';
                                    }
                                    if($dataDetails['is_pdc']){
                                        $dataDetails['is_pdc_name']='Yes';
                                    }
                                    $dataDetails['creditbankname'] = $bankname ; 
                               }elseif($dataDetails['transaction_type'] ==Paymenttransaction::PAYMENTOUT){
                               
                                    $table = TABLE_PAYMENT_BANK;
                                    $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$dataDetails['debit_pay_bank_id'] ."' " ;
                                    $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                                    $banknameArr = getRecord($table,$fields1,$condition2);
                                    if(!empty($banknameArr)){
                                        $bankname = $banknameArr['bank_name'] ;                                           
                                    }
                                    $dataDetails['transaction_typename']='Payment Out';
                                    if($dataDetails['is_bill_receive'] ){           
                                        $dataDetails['is_bill_receive_name'] = "Yes";
                                    }
                                     $dataDetails['debitbankname'] = $bankname ;   
                               }elseif($dataDetails['transaction_type'] ==Paymenttransaction::INTERNAL){
                               
                                    $table = TABLE_PAYMENT_BANK;
                                    $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$dataDetails['debit_pay_bank_id'] ."' " ;
                                    $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                                    $banknameArr = getRecord($table,$fields1,$condition2);
                                    if(!empty($banknameArr)){
                                        $debitbankname = $banknameArr['bank_name'] ;
                                    }                
                                    $table = TABLE_PAYMENT_BANK;
                                    $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$dataDetails['credit_pay_bank_id'] ."' " ;
                                    $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                                    $banknameArr = getRecord($table,$fields1,$condition2);
                                    if(!empty($banknameArr)){
                                        $creditbankname = $banknameArr['bank_name'] ;   
                                    }
                                    $dataDetails['transaction_typename']='Internal';
                                    //$val['rowspan']=3;
                                    if($dataDetails['will_receipt_generate']){
                                        $dataDetails['will_receipt_generate_name']='Yes';
                                    }
                                    if($dataDetails['is_pdc']){
                                        $dataDetails['is_pdc_name']='Yes';
                                    }
                                    if($dataDetails['is_bill_receive'] ){           
                                        $dataDetails['is_bill_receive_name'] = "Yes";
                                    }
                                }
                                if($dataDetails['transaction_type'] ==Paymenttransaction::INTERNAL){
                                    $dataDetails['debitbankname'] = $debitbankname ;           
                                    $dataDetails['creditbankname'] = $creditbankname ;  
                                }
                                else{
                                    $dataDetails['bankname'] = $bankname ;           
                                }
                            $dataDetails['link']= DIR_WS_NC .'/payment-transaction.php?perform=view&id='.$variables['hid'] ;
                        }

                        if ( getParsedEmail($db, $s, 'PAYMENT_TRANSACTION_ADD', $dataDetails, $email) ) {
                            
							$to     = '';
                            $to[]   = array('name' => 'Admin', 'email' => $email_transaction_updates);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        } 
						 
                        //Send mail to admin after adding record eof
                                    
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                }
                 
                
            }
            
        }
		
      
        if(isset($_POST['btnCreate'])  && $messages->getErrorMessageCount() <= 0){
             header("Location:".DIR_WS_NC."/payment-transaction.php?perform=qadd&added=1&number=".$data['number']);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-transaction.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {           
            //include ( DIR_FS_NC .'/payment-transaction-list.php');
            header("Location:".DIR_WS_NC."/payment-transaction.php?added=1&number=".$data['number']);            
        }
        else {           
            $hidden[] = array('name'=> 'perform' ,'value' => 'qadd');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden'); 
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-qadd.html');
        }
    }
    else {
         $messages->setErrorMessage("You donot have the Permisson to Access this module.");
      //  $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-add-permission.html');
    }
?>
