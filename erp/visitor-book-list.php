<?php

    if ( $perm->has('nc_vb_list') ) {
		
	
         
        if ( !isset($condition_query) || $condition_query == '' ) {
            /* $condition_query = '';
            $time   = time();
            $l_mon  = strtotime('+3 month', $time);
            $from   = date('Y-m-d 00:00:00', $time );
            $to     = date('Y-m-d 23:59:59', $l_mon); 
            
            $condition_query = " WHERE ". TABLE_VISITOR_BOOK .".do_d <= '". $to ."' "
                                ." AND ("
                                    . TABLE_VISITOR_BOOK .".status = '". Order::BLOCKED ."'"
                                    ." OR ". TABLE_BILL_ORDERS .".status = '". Order::ACTIVE ."'"
                                    ." OR ". TABLE_BILL_ORDERS .".status = '". Order::PENDING ."'"
                                    ." OR ". TABLE_BILL_ORDERS .".status = '". Order::DELETED ."'"
                                .")";
           
            $_SEARCH["date_from"]   = date('d/m/Y', $time);
            $_SEARCH["chk_date_to"] = 'AND';
            $_SEARCH["date_to"]     = date('d/m/Y', $l_mon);
            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(Order::BLOCKED, Order::ACTIVE,Order::PENDING,Order::DELETED); 
			*/
			
			$condition_query = " WHERE  ("
                                    . TABLE_VISITOR_BOOK .".status = '". Visitor::PENDING ."'" 
                                .")";
			$_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = Visitor::PENDING ;
        } 
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
		 $_SEARCH['searched'] = 1;
        // To count total records.
        $list	= 	NULL;
        $total	=	Visitor::getDetails( $db, $list, '', $condition_query);
            
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    	$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
        $list	= NULL;
        $fields = TABLE_VISITOR_BOOK .'.id'
                    .','. TABLE_VISITOR_BOOK .'.name'
                    .','. TABLE_VISITOR_BOOK .'.company_name'
                    .','. TABLE_VISITOR_BOOK .'.contact_no'
                    .','. TABLE_VISITOR_BOOK .'.important'
                    .','. TABLE_VISITOR_BOOK .'.address'
                    .','. TABLE_VISITOR_BOOK .'.purpose'
                    .','. TABLE_VISITOR_BOOK .'.in_time'
                    .','. TABLE_VISITOR_BOOK .'.out_time'
                    .','. TABLE_VISITOR_BOOK .'.last_comment'
                    .','. TABLE_VISITOR_BOOK .'.last_comment_by_name'
                    .','. TABLE_VISITOR_BOOK .'.attend_by' 
                    .','. TABLE_VISITOR_BOOK .'.status' 
                    .','. TABLE_VISITOR_BOOK .'.date' 
                    .','. TABLE_VISITOR_BOOK .'.do_e' ;
                    
        Visitor::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
       
        if ( $perm->has('nc_vb_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_vb_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_vb_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_vb_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_vb_details') ) {
            $variables['can_view_details'] = true;
        }
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'visitor-book-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>
