<?php
if ( !defined('THIS_DOMAIN') ) {
		require_once("../lib/config.php");
	}
	$ajx	= isset($_GET["ajx"])? $_GET["ajx"]	: ( isset($_POST["ajx"])? $_POST["ajx"]	:'');
    if($ajx==1){
		page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
		include_once ( DIR_FS_NC ."/header.php");
	}
	
if ( $perm->has('nc_dashboard_orders') ) {
    
		include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');

		 
		// For lead list
		$x	= isset($_GET["x"])? $_GET["x"]	: ( isset($_POST["x"])? $_POST["x"]	:'');
		//$rpp= isset($_GET["rpp"])	? $_GET["rpp"]	: ( isset($_POST["rpp"])? $_POST["rpp"]	: ( 			defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
		
        $rpp=10;
        $sString = isset($_GET["sString"]) ? $_GET["sString"]: ( isset($_POST["sString"]) ? $_POST["sString"] : '' );
        $sType= isset($_GET["sType"])? $_GET["sType"]    : ( isset($_POST["sType"]) ? $_POST["sType"]   : '' );
	    $sOrder = isset($_GET["sOrder"])? $_GET["sOrder"]: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	    $sOrderBy = isset($_GET["sOrderBy"])? $_GET["sOrderBy"]	:( isset($_POST["sOrderBy"])?$_POST["sOrderBy"]: '' );
	    $perform= isset($_GET["perform"])? $_GET["perform"]:( isset($_POST["perform"])? $_POST["perform"] : '' );
	
        $condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : 
		(isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
        $condition_url  = '';
        $condition_query1='';
        $extra='';
		
		if ( empty($x) ) {
			$x            = 1;
			$next_record	= 0 ;
		}
		else {
			$next_record	= ($x-1) * $rpp;
		}
        
		
		
		$sTypeArray  = array('Any'           =>  array(  '' => '-1'),
                                TABLE_BILL_ORDERS   =>  array('Order no'            => 'number',
                                                              'Order title'            => 'order_title',
                                                            ),
                               );

		
        $variables['can_view_list'] = true;
        $_SEARCH['searched']    =   true;
       
        
        $where_added    = true;
		// BO: Read the Team Orders.
		     
		// If the Order is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_or_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " AND ( ";

        // If my has created this Order.
        $condition_query .= " ( ". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
        
         
        $condition_query .= " OR (  "
                                        . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
                                    . "   ) ";
     
        
        
        // If my is the Client Manager
        $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                            ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
        
        
        $condition_query .= " )";
        
        // CODE FOR SEARCH BOF        
        
        if ( $sString != "" ) {
            $condition_query1 = '';
            // Set the field on which to make the search.
            if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
                $sType = "";
                $search_table = NULL ;
            }
            
    
            if ( $search_table == "Any" ) {
            
                if ( $where_added ) {
                    $condition_query1 .= ' ';
                }
                else {
                    $condition_query1.= ' WHERE (';
                    $where_added    = true;
                }
                //$condition_query .= " WHERE (";
                
                
                foreach( $sTypeArray as $table => $field_arr ) {
              
                    if ( $table != "Any" ) {
                        
                        foreach( $field_arr as $key => $field ) {                         
                            $condition_query1 .= "  ". $table .".". clearSearchString($field) ." LIKE '%". 
							$sString ."%' OR " ;                          
                            $where_added = true;
                        }
                        
                    }
                  
                }
                $len = strlen( $condition_query1 ) - 3 ;
                $condition_query1 .= substr($condition_query1, 0,$len);
                //$condition_query1 .= ") ";
               
                
            }
            elseif ( $search_table != "Any" && $search_table != NULL) {
                
                if ( $where_added ) {
                    $condition_query1 .= ' ';
                }
                else {
                    $condition_query1.= ' WHERE (';
                    $where_added    = true;
                }
                $condition_query1 .= "  (". $search_table .".".clearSearchString($sType)." LIKE '%".$sString."%' )" ;
                $where_added = true;
            }
            $condition_query1 = ' AND ('.$condition_query1.")" ;
            $_SEARCH["sString"] = $sString;
            $_SEARCH["sType"] 	= $sType;
        }
        
        $condition_query .= $condition_query1 ;
        
        $condition_url      .= "&sString=$sString&sType=$sType";
        $_SEARCH["sString"] = $sString ;
        $_SEARCH["sType"]   = $sType ;
        $condition_url .= "&rpp=$rpp&action=search";
        $_SEARCH["searched"]    = true     ;
        
        // Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    
    //default from date befor 1 week of current date;
    
   
    // BO: From Date
    if ( $chk_date_from == 'AND' || $chk_date_from == 'OR') {
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_BILL_ORDERS.".do_e >= '". $dfa ."'";
    }
    $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
    $_SEARCH["chk_date_from"]   = $chk_date_from;
    $_SEARCH["date_from"]       = $date_from;
    // EO: From Date
    
    // BO: Upto Date
    if ( $chk_date_to == 'AND' || $chk_date_to == 'OR') {
    
    
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_BILL_ORDERS .".do_e <= '". $dta ."'";
    }
    $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
    $_SEARCH["chk_date_to"] = $chk_date_to ;
    $_SEARCH["date_to"]     = $date_to ;
    
   
    // EO: Upto Date        
   
    
    //$condition_query .= " ORDER BY ". TABLE_BILL_ORDERS .".do_d DESC ";
    
    $fields = TABLE_BILL_ORDERS .'.id'
                .','. TABLE_BILL_ORDERS .'.number'
                .','. TABLE_BILL_ORDERS .'.team'
                .','. TABLE_BILL_ORDERS .'.do_d'
                .','. TABLE_BILL_ORDERS .'.do_c'
                .','. TABLE_BILL_ORDERS .'.order_title'                
                .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                .','. TABLE_CLIENTS .'.number AS c_number' 
                .','. TABLE_CLIENTS .'.f_name'
                .','. TABLE_CLIENTS .'.l_name' ;
				
    /* if(empty($condition_query)){
		$query = " SELECT ".$fields." FROM ".TABLE_BILL_ORDERS." USE INDEX ( or_no)" ;
		$query .= " INNER JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client " ;
		$query .= " WHERE ". TABLE_BILL_ORDERS .".status = '". Order::ACTIVE ."'";   
	}else{
		$query = " SELECT ".$fields." FROM ".TABLE_BILL_ORDERS." USE INDEX ( or_no)" ;
		$query .= " INNER JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client " ;
		$query .= " WHERE ( ". TABLE_BILL_ORDERS .".status = '". Order::ACTIVE ."')";   
		$query	.= $condition_query ;
	} */
    
	if(empty($condition_query)){
		$query = " SELECT ".$fields." FROM ".TABLE_BILL_ORDERS." USE INDEX (or_no)" ;
		$query .= " INNER JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client " ;
		$query .= " WHERE ". TABLE_BILL_ORDERS .".status IN('". Order::ACTIVE ."','".Order::COMPLETED."')";   
	}else{
		$query = " SELECT ".$fields." FROM ".TABLE_BILL_ORDERS." USE INDEX ( or_no)" ;
		$query .= " INNER JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client " ;
		$query .= " WHERE ( ". TABLE_BILL_ORDERS .".status IN('". Order::ACTIVE ."','".Order::COMPLETED."'))"; 
		$query	.= $condition_query ;
	}
    $db 		= new db_local; // database handle
    $order_by='';
    
    $order_by = " ORDER BY ". TABLE_BILL_ORDERS .".do_d DESC ";
    $query  .=  $order_by ;
    $db->query($query);  
    $total = $db->nf();    
	$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','dashboard-ord.php','frmSearch');
    $query .= " LIMIT ". $next_record .", ". $rpp;
    $db->query($query);
    $db1 		= new db_local;
    $r_list	= 	NULL;
    
    if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
                $teamStr =$db->f('team');
                $by_id =$db->f('by_id');
                $status =$db->f('status');
                $id =$db->f('id');
                $number=$db->f('number');
                $access_level=$db->f('access_level');
                $do_d=$db->f('do_d');
                $order_title =$db->f('order_title');
                $status =$db->f('status');
                $c_user_id =$db->f('c_user_id');
                $c_number=$db->f('c_number');
                $comments=$db->f('comments');
                $work_tl_id=$db->f('work_tl_id');
                $to_id=$db->f('to_id');
                $do_assign=$db->f('do_assign');
                $f_name=$db->f('f_name');
                $l_name=$db->f('l_name');
                
                $team_members =$work_by=$work=null;
                /*
                $teamStr= "'". implode("','", (explode(',', $teamStr))) ."'";
                User::getList($db1, $team_members, 'user_id, number, f_name, l_name, email', " 
				WHERE user_id IN (". $teamStr .")");             
                User::getList($db1, $work_by, 'user_id, number, f_name, l_name, email', " 
				WHERE user_id = '". $by_id ."'");             
                $timelineStatus = WorkStage::getWorkStageTitle($db1, $status);                
                $work = array(
                            'id'=>$work_tl_id, 
                            'to_id'=>$to_id, 
                            'by_id'=>$db->f('by_id'), 
                            'do_assign'=>$db->f('do_assign'), 
                            'status'=>$timelineStatus, 
                            'comments'=>$comments, 
                            'by'=>$work_by
                        );
                */
                $r_list[]	= array(
                                'id'=>$id,
                                'number'=>$number,
                                'access_level'=>$access_level,
                                'team'=>$teamStr,
                                'do_d'=>$do_d,
                                'order_title'=>$order_title,
                                'status'=>$status,
                                'c_user_id'=>$c_user_id,
                                'c_number'=>$c_number,
                                'team_members'=>$team_members,
                                'work'=>$work,
                                'f_name'=>$f_name,
                                'l_name'=>$l_name,
                                'comments'=>$comments
                            );
                   
            }
    }
	$db->close();
	$db1->close();
    $variables['can_view_order'] = false;
    if ( $perm->has('nc_bl_or_details') ) {
        $variables['can_view_order'] = true;
    }
    $variables["x"]	= $x;
	$variables["rpp"]   = $rpp;
    $variables['can_view_cdetails'] =false;
    
    if ( $perm->has('nc_uc') && $perm->has('nc_uc_details')) {
        $variables['can_view_cdetails'] = true;
    }
	$variables['can_add_bid'] = false;
	$variables['can_list_bid'] = false;
	
	
	if ( $perm->has('nc_or_bd') && $perm->has('nc_or_bd_add') ) {
		$variables['can_add_bid'] = true;
	}
	if ( $perm->has('nc_or_bd') && $perm->has('nc_or_bd_list') ) {
		$variables['can_list_bid'] = true;
	}
	
	$page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
	$page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => 'r_list', 'value' => 'r_list');
	$page["section"][] = array('container'=>'TEAM_ORDERS', 'page' => 'dashboard-ord.html');
	if($ajx==1){
		
		$s->assign("variables", $variables);
		$s->assign("error_messages", $messages->getErrorMessages());
		$s->assign("success_messages", $messages->getOkMessages());
	  
		if ( isset($page["var"]) && is_array($page["var"]) ) {
			foreach ( $page["var"] as $key=>$fetch ) {
				$s->assign( $fetch["variable"], ${$fetch["value"]});
			}
		}
		
		if ( is_array($page["section"]) ) {
			$sections = count($page["section"]);
			for ( $i=0; $i<($sections-1); $i++) {
				
				$s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
			}
		}
		
		$s->display($page["section"][$sections-1]["page"]);
	}

	
}else{

	$page["section"][] = array('container'=>'TEAM_ORDERS', 'page' => 'popup.html');
}		
?>