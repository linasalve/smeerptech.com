<?php

    if ( $perm->has('nc_sugg_list') ) {
		
		$variables["completed"]         = Suggestions::COMPLETED;
		$variables["pending"]           = Suggestions::PENDING;
	
        // If the task is created by the my.
        $access_level   = $my['access_level'] + 1;
        
        //$condition_query .= " AND ( ";
         $chk_status_ma = isset($_POST["chk_status_ma"])   ? $_POST["chk_status_ma"]  : (isset($_GET["chk_status_ma"])   ? $_GET["chk_status_ma"]   :'');
         $chk_status_mr = isset($_POST["chk_status_mr"])   ? $_POST["chk_status_mr"]  : (isset($_GET["chk_status_mr"])   ? $_GET["chk_status_mr"]   :'');
      
          
        //***************EOF**********
       
        
        //***************BOF**********
        
        
        
         //By default search in On
        $_SEARCH["searched"]    = true ;
    
        if ( $perm->has('nc_sugg_list_all') ) {

            if ( !isset($condition_query) || $condition_query == ''   ) {
                $condition_query = '';
             
                $chk_status = "AND" ;
                $condition_query= " AND ".TABLE_SUGGESTIONS.".status='".Suggestions::PENDING."'" ;
                $_SEARCH['sStatus'] = Suggestions::PENDING;  
                $_SEARCH['chk_status'] = $chk_status;  
                $_SEARCH['alloted_by'] = Suggestions::SHOW_ALL; 
                $condition_url .= "&alloted_by=".$_SEARCH['alloted_by']."&sStatus=".$_SEARCH['sStatus']."&chk_status=".$chk_status ;
            }        

            $condition_query = " WHERE (". TABLE_SUGGESTIONS .".comment != '' ) ".$condition_query ;    
        }else{
        
            if ( !isset($condition_query) || $condition_query == ''   ) {
                $condition_query = '';
                $chk_status = "AND" ;
                
                $condition_query= " AND ".TABLE_SUGGESTIONS.".status='".Suggestions::PENDING."'" ;
                
                 $_SEARCH['chk_status'] = $chk_status;  
                $_SEARCH['sStatus'] = Suggestions::PENDING;  
                $condition_url .= "&sStatus=".$_SEARCH['sStatus']."&chk_status=".$chk_status ;
            }
            
            $condition_query = " WHERE ( (".TABLE_SUGGESTIONS .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_SUGGESTIONS .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_SUGGESTIONS .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
                                        . TABLE_SUGGESTIONS .".allotted_to REGEXP '^". $my['user_id'] .",' 
                                        ) OR (". TABLE_SUGGESTIONS .".created_by REGEXP '". 
											$my['user_id']."') 
									)".
										$condition_query ;
        }
        
      
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        $lst_type=array_flip(Suggestions::getTypes());
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        $extra_url  = '';    
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }        
        $condition_url .="&perform=".$perform;
        //echo $condition_query;
        // To count total records.
        $list	= 	NULL;
        $total	=	Suggestions::getDetails( $db, $list, '', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        //$extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        //echo $condition_query;
    
        $list	= NULL;
        $fields = TABLE_SUGGESTIONS .'.*'    ;
        Suggestions::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        $fList=array();
        $statusArr = Suggestions::getStatus();
        $statusArr = array_flip($statusArr);
        $priorityArr = Suggestions::getPriority();
        $priorityArr = array_flip($priorityArr);
		$groups_list  = User::getGroups();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
				$groupsName ='';
								
				/* if(!empty($val['groups'])){				
					$groups1 = explode(",",$val['groups']);
					
					foreach($groups1 as $key1=>$val1){
						$groupsName .=$groups_list[$val1].",";						 	
					}
					$groupsName = trim($groupsName,',');					
				}
				$val['groupsName'] = $groupsName ;
				 */
				if(empty($val['allotted_to_name'])){
				   $executive=array();
				   $string = str_replace(",","','", $val['allotted_to']);
				   $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
				   $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
				   User::getList($db,$executive,$fields1,$condition1);
				   $executivename='';              
				   foreach($executive as $key1=>$val1){
						$executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
				   } 
				   $val['allotted_to_name'] = $executivename ;  
				   
			    }else{
				   $val['allotted_to_name'] =  wordwrap(trim($val['allotted_to_name'],","),30);
			    } 
            
			
				/* $val['allotted_to_client_name']=  wordwrap(trim($val['allotted_to_client_name'],","),30);
				$val['allotted_to_lead_name']=  wordwrap(trim($val['allotted_to_lead_name'],","),30);
				$val['allotted_to_addr_name']=  wordwrap(trim($val['allotted_to_addr_name'],","),30);
				$val['allotted_to_vendor_name']=  wordwrap(trim($val['allotted_to_vendor_name'],","),30);
				$val['status_name'] = $statusArr[$val['status']];
				if(!empty($val['priority'])){
					$val['priority'] = $priorityArr[$val['priority']];
				}
				$val['typename'] ='';
				if($val['type']!=0){               
					$val['typename'] =$lst_type[$val['type']];
                }
                */
                $exeCreted=array();
                $exeCreatedname='';
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                User::getList($db,$exeCreted,$fields1,$condition2);
              
                foreach($exeCreted as $key2=>$val2){
                    $exeCreatedname .= $val2['f_name']." ".$val2['l_name']."<br/>" ;
                }
               
                $val['created_by_name'] = $exeCreatedname;  
               
                //BOF to retrieve last two comments
                /*$lastComments = array();
                $last_comment_desc='';
                $fields2 =  TABLE_SUGGESTIONS_LOG .'.comment';
                $condition4 = " WHERE ".TABLE_SUGGESTIONS_LOG .".task_id= '".$val['id']."' 
				ORDER BY do_a DESC LIMIT 0,2" ;
                Suggestions::getDetailsCommLog($db,$lastComments,$fields2,$condition4);
              
                foreach($lastComments as $key4=>$val4){
                    $last_comment_desc .= $val4['comment']."<br/><br/>" ;
                }                 
                $val['last_comment_desc'] = $last_comment_desc; */
               //EOF to retrieve last two comments
              
               //BOF to calculate the delay
                /*
                $do_r_history = array();
                $first_do_deadline = '';
                $fields3 =  TABLE_SUGGESTIONS_HISTORY .'.do_r';
                $condition5 = " WHERE ".TABLE_SUGGESTIONS_HISTORY .".task_id= '".$val['id']."' 
				ORDER BY do_r LIMIT 0,1" ;
                Suggestions::getHistoryList($db,$do_r_history,$fields3,$condition5);
                
                if(empty($do_r_history)){
                    $first_do_deadline = $val['do_r'];
                }else{
                    $first_do_deadline = $do_r_history['0']['do_r'];
                }
                */
                
                 
               
                
                
                //EOF to calculate the delay
                /* $val['can_editable'] = false;
                $val['can_deletable'] = false;
                if($val['created_by']==$my['user_id']){                    
                    if ( $perm->has('nc_ts_delete') ) {
                        $variables['can_deletable'] = true;
                        $val['can_deletable'] = true;
                    }
                    if ( $perm->has('nc_ts_edit') ) {            
                        $variables['can_editable'] = true;                        
                        $val['can_editable'] = true;
                    }
                } */
                $fList[$key]=$val;
            }
        }
     
    if(!isset($_SEARCH['alloted_by'])){
        $_SEARCH['alloted_by'] = Suggestions::ALLOTED_BY_ME ; //By default Alloted by me
    }     
		
		$variables['alloted_by_me'] = Suggestions::ALLOTED_BY_ME ;
		$variables['show_all'] = Suggestions::SHOW_ALL ;
		// Set the Permissions.
		$variables['can_view_list']     = false;
		$variables['can_add']           = false;
		$variables['can_change_status']          = false;
		$variables['can_delete']        = false;
		$variables['can_delete_all']        = false;
		$variables['can_view_details']  = false;
		$variables['can_search']  = false;
		$variables['can_view_list_other'] = false;
		$variables['can_add_impt'] = false;
	
		if ( $perm->has('nc_sugg_list_all') ) {
			$variables['can_view_list_other'] = true;
		}
		if ( $perm->has('nc_sugg_list') ) {
			$variables['can_view_list'] = true;
			$variables['can_search'] = true;
		}
		if ( $perm->has('nc_sugg_add') ) {
			$variables['can_add'] = true;
		}    
		if ( $perm->has('nc_sugg_status') ) {
			$variables['can_change_status'] = true;
		} 
   
    
    
  
       
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'suggestions-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>