<?php

    //if ( $perm->has('nc_ts_list') ) {  
		$variables["completed"]  = Taskreminder::COMPLETED;
		$variables["pending"]    = Taskreminder::PENDING;
		
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';
            $time   = time();
            $l_mon   = time();
            // $l_mon  = strtotime('+3 month', $time);
            $from   = date('Y-m-d 00:00:00', $time );
            $to   = date('Y-m-d 00:00:00', $time );
            // $to     = date('Y-m-d 23:59:59', $l_mon);
            
            $condition_query = " WHERE ". TABLE_TASK_REMINDER .".do_r >= '". $to ."' "
                                ." AND ("
                                    //. TABLE_TASK_REMINDER .".status = '". Taskreminder::COMPLETED ."'"
                                    //." OR ". TABLE_TASK_REMINDER .".status = '". Taskreminder::PENDING ."'"
                                    ." ". TABLE_TASK_REMINDER .".status = '". Taskreminder::PENDING ."'"
                                    
                                .")";
            
            $_SEARCH["date_from"]   = date('d/m/Y', $time);
            $_SEARCH["chk_date_to"] = 'AND';
            $_SEARCH["date_to"]     = date('d/m/Y', $l_mon);
            $_SEARCH["chk_status"]  = 'AND';
            //$_SEARCH["sStatus"]    = array(Taskreminder::COMPLETED,Taskreminder::PENDING);
            $_SEARCH["sStatus"]    = array(Taskreminder::PENDING);
        }
       
       
         // If the task is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_ts_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " AND ( ";

        // If my has created this task.
        $condition_query .= " (". TABLE_TASK_REMINDER .".created_by = '". $my['user_id'] ."') ";
        
        // If my is present in the Team.
        /*       
       $condition_query .= " OR ( ( "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' "
                                    . " ) "
                                    ." AND ". TABLE_TASK_REMINDER .".access_level < $access_level ) ";
        */
        $condition_query .= " AND  ( "
                                        . TABLE_TASK_REMINDER .".allotted_to ='". $my['user_id'] ."' "
                                    . " ) " ;
        // Check if the User has the Right to view task created by other Users.
        /*
        if ( $perm->has('nc_ts_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_ts_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_TASK_REMINDER. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_TASK_REMINDER .".access_level < $access_level_o ) ";
        }
        */
        $condition_query .= " )";
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
     
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }

        // To count total records.
        $list	= 	NULL;
        $total	=	Taskreminder::getDetails( $db, $list, 'id', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields = TABLE_TASK_REMINDER .'.*'    ;
                    
        Taskreminder::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               $executive=array();
               $string = str_replace(",","','", $val['allotted_to']);
               $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
               User::getList($db,$executive,$fields1,$condition1);
               $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['allotted_to'] = $executivename ;
               $statusArr = Taskreminder::getStatus();
               $statusArr = array_flip($statusArr);
               $val['status'] = $statusArr[$val['status']];
               $priorityArr = Taskreminder::getPriority();
               $priorityArr = array_flip($priorityArr);
               $val['priority'] = $priorityArr[$val['priority']];
               $exeCreted=$exeCreatedname='';
               $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
               User::getList($db,$exeCreted,$fields1,$condition2);
              
               foreach($exeCreted as $key2=>$val2){
                    $exeCreatedname .= $val2['f_name']." ".$val2['l_name']."<br/>" ;
               } 
               $val['created_by'] = $exeCreatedname;
               
               
               $fList[$key]=$val;
            }
        }
      
        // Set the Permissions.
        //As task is created by my so he can edit, delete and view it
        $variables['can_view_list']     = true;
        $variables['can_add']           = true;
        $variables['can_edit']          = true;
        $variables['can_delete']        = true;
        $variables['can_view_details']  = true;
        
        /*
        if ( $perm->has('nc_ts_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_ts_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_ts_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_ts_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_ts_details') ) {
            $variables['can_view_details'] = true;
        }
        */
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'task-reminder-myself.html');
    //}
    //else {
       // $messages->setErrorMessage("You do not have the permission to view the list.");
    //}
?>