<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
	page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
	include_once ( DIR_FS_NC ."/header.php" );
	include_once ( DIR_FS_INCLUDES .'/payment-party-bills.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
	
	
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	$sString = isset($_GET["sString"])? $_GET["sString"]  : ( isset($_POST["sString"]) ? $_POST["sString"] : '' );
	$sType = isset($_GET["sType"]) ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder	= isset($_GET["sOrder"])? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy= isset($_GET["sOrderBy"]) ? $_GET["sOrderBy"] : ( isset($_POST["sOrderBy"]) ? $_POST["sOrderBy"]: '' );
	$perform = isset($_GET["perform"]) ? $_GET["perform"]: ( isset($_POST["perform"]) ? $_POST["perform"] : '' );
	$x 	= isset($_GET["x"])   ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	$rpp = isset($_GET["rpp"])   ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
	$added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
	$edited 		= isset($_GET["edited"])     ? $_GET["edited"]    : ( isset($_POST["edited"])      ? $_POST["edited"]   :'0');
	$number 		= isset($_GET["number"])     ? $_GET["number"]    : ( isset($_POST["number"])      ? $_POST["number"]   :'0');
	$condition_url = $condition_query = '';
    
	if(empty($rpp)){
		$rpp = RESULTS_PER_PAGE;
	}
	$condition_query ='';
   
	if ( empty($x) ) {
		$x              = 1;
		$next_record    = 0 ;
	}
	else {
		$next_record    = ($x-1) * $rpp;
	}
	$variables["x"]     = $x;
	$variables["rpp"]   = $rpp;
    
	if($added){
		$messages->setOkMessage("Record of number ".$number." has been added  successfully.");
	}
	if($edited){
		$messages->setOkMessage("Record of number ".$number." has been edited  successfully.");
	}
    
	if ( $perm->has('nc_p_pb') ) {
        $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
					TABLE_PAYMENT_PARTY_BILLS   =>  array(
                                                          'Bill Details'  => 'bill_details',
                                                          'Number'  => 'number',
                                                          'Bill No'  => 'bill_no',
                                                          'Proforma Bill No'  => 'profm_bill_no',
                                                          'Transaction No'  => 'transaction_number',
                                                          'Bill Paid Amount'  => 'amount',
                                                          'Bill Amount'  => 'bill_amount',
                                                          'Late Amount'  => 'late_amount',
                                                          'Added by'     => 'created_by_name',
                                                        ),
                    TABLE_PAYMENT_BILLS_AGAINST   =>  array(
                                                          'Bill Against'  => 'bill_against'
                                                        ),
					TABLE_CLIENTS   =>    array(
                                                      'Members First Name'  => 'f_name-'.TABLE_CLIENTS,
                                                      'Members Last Name'  => 'l_name-'.TABLE_CLIENTS,
                                                      'Members Billing Name'  => 'billing_name-'.TABLE_CLIENTS
                                                    ),
                    TABLE_VENDORS_BANK   =>    array(
                                                      'Vendor Accounts First Name'  => 'f_name-'.TABLE_VENDORS_BANK,
                                                      'Vendor Accounts Last Name'  => 'l_name-'.TABLE_VENDORS_BANK,
                                                      'Vendor Accounts Billing Name'  => 'billing_name-'.TABLE_VENDORS_BANK
                                                    ),
                    TABLE_USER     => array(
                                                'Executive First Name' => 'f_name-'.TABLE_USER,
                                                'Executive Last Name' => 'l_name-'.TABLE_USER                   
                                            ), 
                                );
        
        $sOrderByArray  = array(
                                TABLE_PAYMENT_PARTY_BILLS => array(  
														  'Number'   => 'number',
														  'Bill Date'   => 'bill_dt',
														  'Date'   => 'do_e'
                                                        ),
                                );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
			$_SEARCH['sOrderBy']= $sOrderBy = 'bill_dt';
			$_SEARCH['sOrder']  = $sOrder   = 'DESC';
			$order_by_table     = TABLE_PAYMENT_PARTY_BILLS;
        }
	
        $variables['status'] = PaymentPartyBills::getStatus();
        $variables['fstatus'] = PaymentPartyBills::getFStatus();
        $variables['bstatus'] = PaymentPartyBills::getBillStatus();
        $variables['active'] = PaymentPartyBills::ACTIVE;
        $variables['deactive'] = PaymentPartyBills::DEACTIVE;
		$variables['gltype'] = PaymentPartyBills::getGLType();
		//Get company list 
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
			.','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
		
        //$where_added =false ;
       
        //use switch case here to perform action. 
        switch ($perform) {
           
			case ('add'): {
				include (DIR_FS_NC.'/payment-party-bills-add.php');
					
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-party-bills.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
			case ('csv_upload'):{
                
                include (DIR_FS_NC.'/payment-party-bills-tax-csv.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
				$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('edit_bill'): {
                include (DIR_FS_NC.'/payment-party-bills-edit-bill.php');
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-party-bills.html');
				if($ajx==0){					
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
			case ('edit'): {
				include (DIR_FS_NC .'/payment-party-bills-edit.php'); 
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-party-bills.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
			case ('download_file'): {
					include (DIR_FS_NC .'/payment-party-bills-download.php');
					break;
			} 
			case ('view'): {
					
				include (DIR_FS_NC .'/payment-party-bills-view.php');
				
				$page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
				break;
			}     
			case ('view_pbt'): {
					
				include (DIR_FS_NC .'/payment-party-bills-transaction.php');
				
				$page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
				break;
				}              
			case ('search'): {
					
				include(DIR_FS_NC."/payment-party-bills-search.php");
					
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-party-bills.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
			   
			case ('delete'): {
				$perform='list';
				include ( DIR_FS_NC .'/payment-party-bills-delete.php');
					
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-party-bills.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
					 
			case ('change_status'): {
				$perform='list';
				include ( DIR_FS_NC .'/payment-party-bills-status.php');
					
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-party-bills.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
				
			case ('list'):
			default: {
					
				include (DIR_FS_NC .'/payment-party-bills-list.php');
				// CONTENT = CONTENT
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-party-bills.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				break;
			}
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
	}
	else {
        //$messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-party-bills.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
	}
    $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
	// always assign
	$s->assign("variables", $variables);
	$s->assign("error_messages", $messages->getErrorMessages());
	$s->assign("success_messages", $messages->getOkMessages());

	if ( isset($page["var"]) && is_array($page["var"]) ) {
		foreach ( $page["var"] as $key=>$fetch ) {
		$s->assign( $fetch["variable"], ${$fetch["value"]});
		}
	}
	if ( isset($page["section"]) && is_array($page["section"]) ) {
		$sections = count($page["section"]);
		for ( $i=0; $i<($sections-1); $i++) {
		$s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
		}
	}
	$s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
