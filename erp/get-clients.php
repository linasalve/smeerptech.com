<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	include_once ( DIR_FS_INCLUDES .'/clients.inc.php');  
	   

    $client_details = isset($_GET['client_details']) ? $_GET['client_details'] : '';
    $client_comm 	= isset($_GET['client_comm']) ? $_GET['client_comm'] : '0';
    $tran_purchase 	= isset($_GET['tran_purchase']) ? $_GET['tran_purchase'] : '0';
    
	//$is_classified = isset($_GET['is_classified']) ? $_GET['is_classified'] : Clients::CLASSIFIED_NO; // 0 is not classified; 1 for classified
    
	if(empty($client_details)){
		$client_details = isset($_GET['behalf_client_name']) ? $_GET['behalf_client_name'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['search_client_details']) ? $_GET['search_client_details'] : '';	
    }
	if(empty($client_details)){
		$client_details = isset($_GET['client_details_to']) ? $_GET['client_details_to'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['client_details_fi']) ? $_GET['client_details_fi'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['ord_referred_by_name']) ? $_GET['ord_referred_by_name'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['ord_discount_to_name']) ? $_GET['ord_discount_to_name'] : '';
    }
	
    $optionLink =$stringOpt= '' ;
    if(!empty($client_details)){
        $sString = $client_details;
    } 
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0  
    
    if (isset($sString) && !empty($sString)){
        header("Content-Type: application/json");   
		if($client_comm==1){
			if ( $perm->has('nc_uc_classified') ) {
				//Right to Access classified that means user can access all Classified and Non-Classified clients 
			}else{			
				$sql_classified = " AND ".TABLE_CLIENTS.".is_classified='".Clients::CLASSIFIED_NO."'";
			}
		}
		if($tran_purchase==1 ){
			if ( $perm->has('nc_uc_tran_purchase_classified') ) {
				 
			}else{			
				$sql_classified1 = " AND ".TABLE_CLIENTS.".is_tran_purchase_classified='".Clients::CLASSIFIED_NO."'";
			}
		}
	
	    $query =" SELECT DISTINCT(".TABLE_CLIENTS.".user_id),".TABLE_CLIENTS.".f_name,".TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name FROM ".TABLE_CLIENTS." WHERE ".TABLE_CLIENTS.".parent_id='' AND
			( ".TABLE_CLIENTS.".member_type LIKE '%,".Clients::MEMBER_CLIENTS.",%' 
				OR ".TABLE_CLIENTS.".member_type LIKE '%,".Clients::MEMBER_VENDORS.",%'
				OR ".TABLE_CLIENTS.".member_type LIKE '%,".Clients::MEMBER_THIRDPARTY.",%'
				OR ".TABLE_CLIENTS.".member_type LIKE '%,".Clients::MEMBER_MARKETING.",%'
				OR ".TABLE_CLIENTS.".member_type LIKE '%,".Clients::MEMBER_VENDORACCOUNT.",%'
			) AND ( ".TABLE_CLIENTS.".f_name LIKE '%".$sString."%' OR 
				".TABLE_CLIENTS.".l_name LIKE '%".$sString."%' OR 
				".TABLE_CLIENTS.".billing_name LIKE '%".$sString."%' 
			) AND ".TABLE_CLIENTS.".status='1' ".$sql_classified.$sql_classified1 ;
		
			 
		
        $db->query($query);
        echo "{\"results\": [";
        if ( $db->nf()>0 ) {
          
            $arr = array();
            while ($db->next_record()){
                 $billing_name =processSQLData($db->f('billing_name'));
                 $billing_name =str_replace("\r\n"," ",$billing_name);
                 //$arr[] = "{\"id\": \"".$db->f('product_id')."\", \"value\": \"".htmlspecialchars($db->f('title')).""."\", \"info\": \"\"}";
                 $arr[] = "{\"id\": \"".$db->f('user_id')."\", \"value\": \"".processSQLData($db->f('f_name'))." ".processSQLData($db->f('l_name'))." (".$billing_name.")".""."\", \"info\": \"\"}";
            }
            echo implode(", ", $arr);
           
        }
         echo "]}";	   
	}
 
include_once( DIR_FS_NC ."/flush.php");

?>
