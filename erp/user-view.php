<?php

    if ( $perm->has('nc_ue_details') ) {
        $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_ue_details_al') ) {
            $access_level += 1;
        }

        if ( User::getList($db, $list, '*', " WHERE user_id = '$user_id'") > 0 ) {
            $_ALL_POST = $list['0'];
            
            if ( $_ALL_POST['access_level'] < $access_level ) {
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                include_once ( DIR_FS_CLASS .'/Phone.class.php');
                include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
                include ( DIR_FS_INCLUDES .'/company.inc.php');
                
                $region         = new Region();
                $phone          = new Phone(TABLE_USER);
                $reminder       = new UserReminder(TABLE_USER);
               
                $_ALL_POST['company_name']= '';
                Company::getList($db, $_ALL_POST['company_name'], 'id,name', "WHERE id = '". $_ALL_POST['company_id'] ."'");
                $_ALL_POST['company_name'] = $_ALL_POST['company_name'][0];
                
                $executive=array();
                $string = str_replace(",","','", $_ALL_POST['report_to']);
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                User::getList($db,$executive,$fields1,$condition1);
                $executivename='';
              
                foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
                } 
                
                $_ALL_POST['report_to'] = $executivename ;
                
                // Read the available Access Level and User Roles.
                $_ALL_POST['access_level']  = User::getAccessLevel($db, $access_level, $_ALL_POST['access_level']);
                $_ALL_POST['roles']         = User::getRoles($db, $access_level, $_ALL_POST['roles']);
                //print_r($role_list);
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_USER, $user_id);
                $_ALL_POST['phone'] = $phone->get($db);
                
                // Read the Addresses.
                $region->setAddressOf(TABLE_USER, $user_id);
                $_ALL_POST['address_list'] = $region->get();
                
                // Read the Reminders.
                $reminder->setReminderOf(TABLE_USER, $user_id);
                $_ALL_POST['reminder_list'] = $reminder->get($db);
                
                //print_r($_ALL_POST);
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Executives with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("The Executives details were not found. ");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>