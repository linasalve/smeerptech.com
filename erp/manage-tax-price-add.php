<?php
    if ( $perm->has('nc_mtp_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the Service Tax class.
		include_once (DIR_FS_INCLUDES .'/service-tax-price.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( ServiceTaxPrice::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_SERVICE_TAX_PRICE
                            ." SET ". TABLE_SERVICE_TAX_PRICE .".tax_percent = '". $data['tax_percent'] ."'"
                                .",". TABLE_SERVICE_TAX_PRICE .".tax_price = '". $data['tax_price'] ."'"
                                .",". TABLE_SERVICE_TAX_PRICE .".status = '". $data['status'] ."'"
                                .",". TABLE_SERVICE_TAX_PRICE .".access_level = '".         $access_level."'"
                                .",". TABLE_SERVICE_TAX_PRICE .".created_by = '".           $my['uid'] ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Tax Price entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/manage-tax-price.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/manage-tax-price.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/manage-tax-price.php?added=1");   
            
        }    
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'manage-tax-price-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>