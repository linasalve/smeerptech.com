<?php
  if ( $perm->has('nc_hr_sal_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        // Include the Calendar class
        include_once (DIR_FS_INCLUDES .'/calendar.inc.php');
        
		$lst_calendar = NULL;
        $required_fields ='id,title';
        $condition =" where status='".Calendar::ACTIVE."'";
		Calendar::getList($db,$lst_calendar,$required_fields,$condition);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( HrSalary::validateAdd($data, $extra) ) { 
				
					$absent_days = $data['working_days'] - $data['present_days'] ;
					
                    $query	= " INSERT INTO ".TABLE_HR_SALARY
					." SET ".TABLE_HR_SALARY .".user_id = '". $data['user_id'] ."'"  
					//.",". TABLE_HR_SALARY .".calendar_id = '". 		$data['calendar_id'] ."'"   
					.",". TABLE_HR_SALARY .".from_date = '". date('Y-m-d',$data['from_date']) ."'"  
					.",". TABLE_HR_SALARY .".to_date = '". 		date('Y-m-d',$data['to_date']) ."'"   
					.",". TABLE_HR_SALARY .".total_days = '". 		$data['total_days'] ."'"   
					.",". TABLE_HR_SALARY .".working_days = '". $data['working_days'] ."'"   
					.",". TABLE_HR_SALARY .".present_days = '". $data['present_days'] ."'"
					.",". TABLE_HR_SALARY .".absent_days = '". 	$absent_days ."'"  					
					.",". TABLE_HR_SALARY .".present_days_desc = '".$data['present_days_desc'] ."'"   
					.",". TABLE_HR_SALARY .".promised_payment = '". $data['promised_payment']."'"   
					.",". TABLE_HR_SALARY .".30_per_appr_pay = '".$data['30_per_appr_pay'] ."'"   
					.",". TABLE_HR_SALARY .".no_projects = '". 		$data['no_projects'] ."'"   
					.",". TABLE_HR_SALARY .".no_projects_desc = '". 		$data['no_projects_desc'] ."'"   
					.",". TABLE_HR_SALARY .".worked_hrs = '". 		$data['worked_hrs'] ."'"   
					.",". TABLE_HR_SALARY .".calculation_type = '". 		$data['calculation_type'] ."'"   
					.",". TABLE_HR_SALARY .".per_day = '". 		$data['per_day'] ."'"   
					.",". TABLE_HR_SALARY .".per_hour = '". 		$data['per_hour'] ."'"   
					.",". TABLE_HR_SALARY .".incentive_amount = '". 		$data['incentive_amount'] ."'" 
					.",". TABLE_HR_SALARY .".earned_payment = '". 		$data['earned_payment'] ."'"   
					.",". TABLE_HR_SALARY .".30_perof_earned_pay = '". 	$data['30_perof_earned_pay'] ."'"   
					.",". TABLE_HR_SALARY .".30_appraisal_score = '". 	$data['30_appraisal_score'] ."'"   
					.",". TABLE_HR_SALARY .".30_appraisal_score_desc = '". 	$data['30_appraisal_score_desc'] ."'"   
					.",". TABLE_HR_SALARY .".earned_30_per_appr_pay = '". 	$data['earned_30_per_appr_pay'] ."'"   
				    .",". TABLE_HR_SALARY.".earned_30_per_appr_pay_desc='".$data['earned_30_per_appr_pay_desc']."'"   
					.",". TABLE_HR_SALARY .".cafeteria_deduction = '". 		$data['cafeteria_deduction'] ."'"   
					.",". TABLE_HR_SALARY .".phone_deduction = '". 		$data['phone_deduction'] ."'"   
					.",". TABLE_HR_SALARY .".pt_deduction = '". 		$data['pt_deduction'] ."'"      
					.",". TABLE_HR_SALARY .".pf_deduction = '". 		$data['pf_deduction'] ."'"                    .",". TABLE_HR_SALARY .".esi_deduction = '". 		$data['esi_deduction'] ."'"                    
					.",". TABLE_HR_SALARY .".uniform_deposite = '". $data['uniform_deposite'] ."'"                    .",". TABLE_HR_SALARY .".adv = '". 		$data['adv'] ."'"                              
					.",". TABLE_HR_SALARY .".deduction_amount = '".$data['deduction_amount']."'" 
					.",". TABLE_HR_SALARY .".deduction_amount_desc = '".$data['deduction_amount_desc'] ."'" 
					.",". TABLE_HR_SALARY .".total_earned_payment = '".$data['total_earned_payment'] ."'"  
					.",". TABLE_HR_SALARY .".paid_salary = '". 		$data['paid_salary'] ."'"   
					.",". TABLE_HR_SALARY .".note = '". 		$data['note'] ."'"   
					.",". TABLE_HR_SALARY .".created_by = '". 		$my['user_id'] ."'"    
					.",". TABLE_HR_SALARY .".created_by_name = '". 		$my['f_name']." ".$my['l_name']."'"    
					.",". TABLE_HR_SALARY .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                    
					.",". TABLE_HR_SALARY .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
				$_ALL_POST['from_date']=$data['from_date'];
				$_ALL_POST['to_date']=$data['to_date'];
                $data		= NULL; 
            }
        }else{
			$_ALL_POST['to_date']=date("15/m/Y",strtotime("-1 month"));
			$_ALL_POST['from_date']= date("16/m/Y", strtotime("-2 month") );  
		}               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/hr-salary.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/hr-salary.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/hr-salary.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'lst_calendar', 'value' => 'lst_calendar');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-salary-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
