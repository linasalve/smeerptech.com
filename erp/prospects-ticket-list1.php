<?php

    if ( $perm->has('nc_pst_list') ) {
        
        include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
        
		
		if(empty($condition_query)){
		
			$_SEARCH["chk_t_status"]  = 'AND';
			$_SEARCH["tStatus"]   = array(ProspectsTicket::PENDINGWITHSMEERP,ProspectsTicket::PENDINGWITHSMEERPCOZCLIENT);            
			$condition_query = " AND ticket_status 
			IN('".ProspectsTicket::PENDINGWITHSMEERP."','".ProspectsTicket::PENDINGWITHSMEERPCOZCLIENT."') ";
		}
        if ( $perm->has('nc_pst_list_all') ) { 
		
		 
		}else{ 
			if(empty($_SEARCH["search_executive_id"]) || empty($_SEARCH["user_id"]) ){
				$condition_query .= " AND ";
				$condition_query .= " (". TABLE_PROSPECTS_TICKETS .".tck_owner_member_id = '". $my['user_id'] ."' "."  ) "; 
					
				// Check if the User has the Right to view Orders created by other Users.
				$condition_url  .= "&user_id=".$my['user_id'];
				$_SEARCH["user_id"]  = $my['user_id'];
			}    
		}
		
		
		
        $condition_query = " LEFT JOIN ".TABLE_PROSPECTS." ON ".TABLE_PROSPECTS.".user_id = ".TABLE_PROSPECTS_TICKETS.".ticket_owner_uid 
						LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON 
							".TABLE_PROSPECTS_ORDERS.".id =".TABLE_PROSPECTS_TICKETS.".flw_ord_id                             
						WHERE ".TABLE_PROSPECTS_TICKETS.".ticket_child='0' AND 
						".TABLE_PROSPECTS_TICKETS.".ticket_type= '".ProspectsTicket::TYP_COMM."' ".$condition_query    ;
                             
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
          //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        
        $total	=	ProspectsTicket::getList( $db, $list, '', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
       
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $list	= NULL;
        //$fields = TABLE_PROSPECTS_TICKETS.".*,".TABLE_ST_STATUS.".status_name,";
        $fields = TABLE_PROSPECTS_TICKETS.".*,";
        $fields .= TABLE_PROSPECTS_ORDERS.".order_title,";
        //$fields .= TABLE_ST_PRIORITY.".priority_name,";
        $fields .= TABLE_PROSPECTS.".f_name,".TABLE_PROSPECTS.".l_name,".TABLE_PROSPECTS.".billing_name,".TABLE_PROSPECTS.".org,".TABLE_PROSPECTS.".email,".TABLE_PROSPECTS.".mobile1,".TABLE_PROSPECTS.".mobile2";
        ProspectsTicket::getList( $db, $list,$fields, $condition_query, $next_record, $rpp);
        
        $ticketStatusArr = ProspectsTicket::getTicketStatus();
        $ticketStatusArr = array_flip($ticketStatusArr);
        $ticketRatingArr = ProspectsTicket::getRating();
        $ticketRatingArr = array_flip($ticketRatingArr);
      
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                //$val['ticket_department']    = $val['department_name'] ;     
                //$val['ticket_status']    = $val['status_name'] ;     
                //$val['ticket_priority']    = $val['priority_name'] ;   
                if(isset($val['ticket_status'])){
                    $val['ticket_status_name'] = $ticketStatusArr[$val['ticket_status']];  
                }
                if(!empty($val['ticket_rating'])){
                    $val['ticket_rating_name'] = $ticketRatingArr[$val['ticket_rating']];  
                }
                $val['ticket_date'] = date("d M Y H:i:s",$val['ticket_date']);
                $executive=array();
                $string = str_replace(",","','", $val['assign_members']);
                $fields4 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                $condition4 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                User::getList($db,$executive,$fields4,$condition4);
                $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['assign_members'] = $executivename ; 
                 
                $db2 = new db_local ;
                
                $query = "SELECT COUNT(*) AS replies FROM ". TABLE_PROSPECTS_TICKETS
                                    ." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_child ='". $val['ticket_id'] ."' " ; 
                $db2->query($query) ;
                $db2->next_record() ;
                $ticket_replies = $db2->f("replies") ;
                $val['ticket_replied']    = $ticket_replies ;                
                $fList[$key]=$val;
            }
        }
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_view_details']  = false;
        $variables['can_view_client_details']  = true;
        $variables['can_update_status'] = false;
		$variables['can_email_read'] = false;
        $variables['can_view_list_all'] = false;
        if ( $perm->has('nc_pst_add') ) {
            $variables['can_add'] = true;
        }
        
        if ( $perm->has('nc_pst_details') ) {
            $variables['can_view_details'] = true;
        }
         
        if ( $perm->has('nc_pst_list') ) {
            $variables['can_view_list'] = true;
        }
        
        if ( $perm->has('nc_pst_update_status') ) {
            $variables['can_update_status'] = true;
        }
        if( $perm->has('nc_pst_email_read') ){
            $variables['can_email_read'] = true;
        }
		
		//Get list of team bof
		if ( $perm->has('nc_pst_list_all') ) {   
			
		}else{ 
			$rteam=trim($my['my_reporting_members'],",");
            $team = str_replace(',',"','",$rteam);
			$ulist=array();
			$ulist[]= array('user_id'=>$my['user_id'],
						'number'=>$my['number'],
						'name'=>$my['f_name'].' '.$my['l_name']
					) ;
			$sql= " SELECT f_name,l_name,number,user_id FROM ".TABLE_USER." WHERE ".TABLE_USER.".user_id IN ('".$team."') ";
			$db->query($sql);   
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ) {
					$user_id=$db->f('user_id');
					$number = $db->f('number');
					$name= $db->f('f_name')." ". $db->f('l_name') ; 
					$ulist[]= array('user_id'=>$user_id,
						'number'=>$number,
						'name'=>$name
					) ; 
				}
			}  
		}	 
		if ( $perm->has('nc_pst_list_all') ) { 
		    $variables['can_view_list_all'] = true;
		}
		if($variables['can_view_list_all']){
			echo $mkt['email'] 		   = SALES_EMAIL;
			$mkt['email_password'] = SALES_EMAIL_PWD;
		}else{
			$mkt['email'] = $my['email'];
			$mkt['email_password'] = $my['email_password'];
		}
		
		if(!empty($mkt['email']) && !empty($mkt['email_password']) ){
			$mailBox = imap_open("{imap.emailsrvr.com:143/novalidate-cert}INBOX","'".$mkt['email']."'","'".$mkt['email_password']."'")
			or die("can't connect: " . imap_last_error());
			$check = imap_mailboxmsginfo($mailBox);
			$totalNumOfMsg = $check->Nmsgs;
			$variables['totalNumOfMsg'] = $check->Nmsgs; 
		}
		 
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => 'ulist', 'value' => 'ulist');
		$page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');		
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-ticket-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>