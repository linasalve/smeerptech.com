<?php
  if ( $perm->has('nc_anc_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		 include_once (DIR_FS_INCLUDES .'/announcement.inc.php');	
        
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( Announcement::validateAdd($data, $extra) ) { 
                            $query	= " INSERT INTO ".TABLE_ANNOUNCEMENT
                                    ." SET ".TABLE_ANNOUNCEMENT .".title = '". $data['title'] ."'" 
                                    .",". TABLE_ANNOUNCEMENT .".description = '". 		$data['description'] ."'"
                                    .",". TABLE_ANNOUNCEMENT .".do_announcement = '". 		$data['do_announcement'] ."'" 
                                	.",". TABLE_ANNOUNCEMENT .".status = '". 		$data['status'] ."'"                                
                                    .",". TABLE_ANNOUNCEMENT .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                                    .",". TABLE_ANNOUNCEMENT .".created_by = '".           $my['user_id'] ."'"
                                    .",". TABLE_ANNOUNCEMENT .".access_level = '".         $my['access_level'] ."'"
                                	.",". TABLE_ANNOUNCEMENT .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/announcement.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/announcement.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/announcement.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'announcement-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
