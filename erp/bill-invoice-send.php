<?php
 if ( $perm->has('nc_bl_inv_send') ) {
    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
    $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );
	$flw_ord_id = isset($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : (isset($_POST['flw_ord_id']) ?	$_POST['flw_ord_id'] : '');

    $_ALL_POST	    = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
   
    /*$condition_query = " WHERE (". TABLE_BILL_INV .".id = '". $inv_id ."' "
                            ." OR ". TABLE_BILL_INV .".number = '". $inv_id ."')";*/
    $condition_query = " WHERE (". TABLE_BILL_INV .".id = '". $inv_id ."')";

    $fields = TABLE_BILL_INV .'.number'
                .','. TABLE_BILL_INV .'.or_no'
                .','. TABLE_BILL_INV .'.amount'
                .','. TABLE_BILL_INV .'.currency_abbr'
                .','. TABLE_BILL_INV .'.do_i'
                .','. TABLE_BILL_INV .'.do_d'
                .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                .','. TABLE_CLIENTS .'.number AS c_number'
                .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                .','. TABLE_CLIENTS .'.billing_name'
				.','. TABLE_CLIENTS .'.authorization_id'
				.','. TABLE_CLIENTS .'.send_mail'
				.','. TABLE_CLIENTS .'.check_email'
				.','. TABLE_CLIENTS .'.email AS c_email'
                .','. TABLE_CLIENTS .'.email_1 AS c_email_1'
				.','. TABLE_CLIENTS .'.email_2 AS c_email_2'
				.','. TABLE_CLIENTS .'.email_3 AS c_email_3';
                
    if ( Invoice::getDetails($db, $_ALL_Data, $fields, $condition_query) > 0 ) {
        $_ALL_Data =  $_ALL_Data['0'];
      
        //$_ALL_POST['to']= $_ALL_Data['c_email'];
        $inv_no =  $_ALL_Data['number'];
        $_ALL_Data['amount'] =  number_format($_ALL_Data['amount'],2);
        
		
		$_ALL_Data['authorization_id'] = $_ALL_Data['authorization_id'];
		$_ALL_Data['accountValid'] = 0;
		
		
		$clientAuth = Clients::getAuthorization();
		$clientAuth = array_flip($clientAuth);
		$authorizationName='';
		$authorization_id = trim($_ALL_Data['authorization_id'],",");
		if(!empty($authorization_id)){
			$authorization_id = trim($authorization_id,",");
			$authorizationArr = explode(",",$authorization_id);
			if(!empty($authorizationArr)){
				foreach($authorizationArr as $key2=>$val2){
					$authorizationName.= $clientAuth[$val2].", ";
				}
			}					
		}
		$_ALL_Data['authorizationName'] =  $authorizationName;
		$isValid1 = in_array(Clients::ACCOUNTS,$authorizationArr);
		$isValid2 = in_array(Clients::MANAGEMENT,$authorizationArr);
		if($isValid1==1 || $isValid2==1){
			$isValid = 1;
		}
		$_ALL_Data['accountValid'] = $isValid ;
		
		
		
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            //print_r($_ALL_POST);    
            $data		= processUserData($_ALL_POST);   
            $extra = array( 'db' 		=> &$db,
                            'messages'          => &$messages
                          );
                          
            if(Invoice::validateSendInvoice($data, $extra)){
            
                $email = NULL;
                $cc_to_sender= $cc = $bcc = Null;
                
                $data['client'] = array("f_name" => $_ALL_Data['c_f_name'],
										"l_name" => $_ALL_Data['c_l_name']
                                    );
                $data['number'] = $_ALL_Data['number'];
				$data['billing_name'] = $_ALL_Data['billing_name'];
                $data['amount'] = $_ALL_Data['amount'];
                $data['currency_name'] = $_ALL_Data['currency_name'];
                $data['do_i'] = $_ALL_Data['do_i'];
                $data['do_d'] = $_ALL_Data['do_d'];
                
                $temp = NULL;
                $temp_p = NULL;
                $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $_ALL_Data['or_no'] ."'";
                Order::getParticulars($db, $temp, 'particulars,ss_title,ss_punch_line,ord_no', $condition_query);
                if(!empty($temp)){
                    foreach ( $temp as $pKey => $parti ) {
                        $temp_p[] = array(
										'particulars'=>$temp[$pKey]['particulars'],
										'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
										'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] 
									);
                    }
                }                    
                $data['particulars'] = $temp_p ;     
                $file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";
                
				$mail_client = $mail_to_all_su = 0;
				if(isset($data['mail_client'])){
					$mail_client = 1 ;
				}				
				if(isset($data['mail_to_all_su'])){
					$mail_to_all_su = 1 ;
				}	
                if ( getParsedEmail($db, $s, 'INVOICE_CREATE_TO_CLIENT', $data, $email) ) {        
                    $to = '';
                    $to[]   = array('name' => $_ALL_POST['to'], 'email' => $_ALL_POST['to']);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                    $mail_send_to_su=$_ALL_POST['to'];
					/*
					echo $email["body"];
                    echo $email["subject"];
                    print_r($to);
                    print_r($from);
					*/
                    //SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                }
				
				
				/* if(!empty($smeerp_client_email)){
					$to     = '';
					$cc_to_sender=$cc=$bcc='';
					$to[]   = array('name' => $_ALL_Data['c_f_name'] .' '. $_ALL_Data['c_l_name'], 
					'email' => $smeerp_client_email);
					$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
					 
					SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
					$cc_to_sender,$cc,$bcc,$file_name);
				} */
				
				
				if($mail_client){
					
					if(!empty($_ALL_Data['c_email'])){						
						$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
						$mail_send_to_su .= ",".$_ALL_Data['c_email'];
						//$to = '';
						$to[]   = array('name' => $_ALL_Data['c_email'], 'email' => $_ALL_Data['c_email'] );
						$from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);						  /*                     	
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
						*/					
					}
					if(!empty($_ALL_Data['c_email_1'])){						
					
						$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
						$mail_send_to_su .= ",".$_ALL_Data['c_email_1'];
						//$to = '';
						$to[]   = array('name' => $_ALL_Data['c_email_1'], 'email' => $_ALL_Data['c_email_1'] );
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    	
						/*
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
						*/
					}						
					if(!empty($_ALL_Data['c_email_2'])){						
					
						$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
						$mail_send_to_su .= ",".$_ALL_Data['c_email_2'];
						//$to = '';
						$to[]   = array('name' => $_ALL_Data['c_email_2'], 'email' => $_ALL_Data['c_email_2'] );
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    	/*
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);*/
					}
					if(!empty($_ALL_Data['c_email_3'])){
						$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
						$mail_send_to_su .= ",".$_ALL_Data['c_email_3'];
						//$to = '';
						$to[]   = array('name' => $_ALL_Data['c_email_3'], 'email' => $_ALL_Data['c_email_3'] );
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    	/*SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);*/
					}					
				}
				
				if( $mail_to_all_su || !empty($data['mail_to_su']) ){
					
					if($mail_to_all_su){
						$followupSql = " AND (authorization_id LIKE '%,".Clients::ACCOUNTS.",%' OR authorization_id LIKE '%,".Clients::MANAGEMENT.",%')" ;
						Clients::getList($db, $subUDetails, ' user_id, f_name, l_name,
						title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', 
						" WHERE parent_id = '".$_ALL_Data['c_user_id']."' AND 
						status='".Clients::ACTIVE."' ".$followupSql." ORDER BY f_name");
					
					}elseif(!empty($data['mail_to_su'])){
						$mail_to_su= implode("','",$data['mail_to_su']);							
						Clients::getList($db, $subUDetails, 'user_id, f_name, l_name,
						title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', 
						" WHERE parent_id = '".$_ALL_Data['c_user_id']."' AND status='".Clients::ACTIVE."' 
						AND user_id IN('".$mail_to_su."')
						ORDER BY f_name");
					}						
					if(!empty($subUDetails)){

						foreach ($subUDetails  as $key=>$value){
						
							if(!empty($subUDetails[$key]['email'])){
								$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
								$mail_send_to_su .= ",".$subUDetails[$key]['email'];
								
								//$to = '';
								$to[]   = array('name' => $subUDetails[$key]['email'], 
								'email' =>$subUDetails[$key]['email'] );
								$from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);                    				
								/*
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
								$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
								*/
							}
							if(!empty($subUDetails[$key]['email_1'])){
								$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
								$mail_send_to_su .= ",".$subUDetails[$key]['email_1'];
								
								//$to = '';
								$to[]   = array('name' => $subUDetails[$key]['email_1'], 
								'email' =>$subUDetails[$key]['email_1'] );
								$from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);                     
								/*
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
								$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
								*/
							}
							if(!empty($subUDetails[$key]['email_2'])){
								$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
								$mail_send_to_su .= ",".$subUDetails[$key]['email_2'];
								
								//$to = '';
								$to[]   = array('name' => $subUDetails[$key]['email_2'], 
								'email' =>$subUDetails[$key]['email_2'] );
								$from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);                    				
								/*
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
								$email["isHTML"],
								$cc_to_sender,$cc,$bcc,$file_name);*/
							}
						}
					}
				}
				if(!empty($to)){
					 
					$cc_to_sender=$cc=$bcc='';
					$bcc[]   = array('name' => $smeerp_client_email, 'email' => $smeerp_client_email);  
				 
					$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
					 
					SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
					$cc_to_sender,$cc,$bcc,$file_name);
				} 
				
				$data['ticket_owner_uid'] = $_ALL_Data['c_user_id'];
				$data['ticket_owner'] = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name'];
				if(!empty($flw_ord_id)){					
					$sql2 = " SELECT ticket_id FROM ".TABLE_ST_TICKETS." WHERE 
					".TABLE_ST_TICKETS.".flw_ord_id = '".$flw_ord_id."' AND ".TABLE_ST_TICKETS.".ticket_child = 0  LIMIT 0,1";
					if ( $db->query($sql2) ) {
						$ticket_id=0;
						if ( $db->nf() > 0 ) {
							while ($db->next_record()) {
							   $ticket_id= $db->f('ticket_id');
							}
						}
					}				 
				}
				$ticket_no  =  SupportTicket::getNewNumber($db);				
				$data['display_name']=$data['display_user_id']=$data['display_designation']='';
				$randomUser = getRandomAssociate();
				$data['display_name'] = $randomUser['name'];
				$data['display_user_id'] = $randomUser['user_id'];
				$data['display_designation'] = $randomUser['desig'];
				
				
				$data['ticket_subject'] = $email["subject"];
				$data['ticket_text'] = $email["body"];
				$data['hrs'] =0;
				$data['min'] =10;
				$hrs1 = (int) ($data['hrs'] * 6);
				$min1 = (int) ($data['min'] * 6);  
				
				//disply random name EOF
				if($ticket_id>0){
				
					$query= "SELECT "	. TABLE_ST_TICKETS .".ticket_date, ".TABLE_ST_TICKETS.".status"
							." FROM ". TABLE_ST_TICKETS 
							." WHERE ". TABLE_ST_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
							." AND  (". TABLE_ST_TICKETS .".ticket_child = '". $ticket_id ."' " 
										." OR "
										. TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " 
									.") "
							." ORDER BY ". TABLE_ST_TICKETS .".ticket_date DESC LIMIT 0,1";				
						
					$db->query($query) ;
					if($db->nf() > 0){
						$db->next_record() ;
						$last_time = $db->f("ticket_date") ;
						$ticket_response_time = time() - $last_time ;
						$status = $db->f("status") ;
					}else{
						$ticket_response_time = 0 ;
					}
					$ticket_date 	= time() ;
					$ticket_replied = '0' ;
					
					$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
					. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
					. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
					. TABLE_ST_TICKETS .".sent_inv_id         = '". $inv_id ."', "
                    . TABLE_ST_TICKETS .".sent_inv_no         = '". $inv_no ."', "
					. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
					. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
					. TABLE_ST_TICKETS .".ticket_creator_uid = '". $my['uid'] ."', "
					. TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
					. TABLE_ST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
					. TABLE_ST_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
					. TABLE_ST_TICKETS .".ticket_status     = '".SupportTicket::PENDINGWITHCLIENTS."', "
					. TABLE_ST_TICKETS .".ticket_child      = '".$ticket_id."', "
					. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
					. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
					. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
					. TABLE_ST_TICKETS .".min = '". $data['min']."', "
					. TABLE_ST_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
					. TABLE_ST_TICKETS .".ticket_replied    = '".$ticket_replied."', "
					. TABLE_ST_TICKETS .".from_admin_panel  = '".SupportTicket::ADMIN_PANEL."', "
					. TABLE_ST_TICKETS .".mail_client    = '".$mail_client."', "
					. TABLE_ST_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
					. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
					. TABLE_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', "
					. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
					. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
					. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
					. TABLE_ST_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
					. TABLE_ST_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
					. TABLE_ST_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "
					. TABLE_ST_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
					
					$db->query($query) ;
					$variables['hid'] =  $db->last_inserted_id() ;

					$query_update = "UPDATE ". TABLE_ST_TICKETS 
					." SET ". TABLE_ST_TICKETS .".ticket_status = '".SupportTicket::PENDINGWITHCLIENTS."'"  
					.",".TABLE_ST_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
					.",".TABLE_ST_TICKETS.".last_comment_from = '".SupportTicket::ADMIN_COMMENT."'"
					.",".TABLE_ST_TICKETS.".last_comment = '".$data['ticket_text']."'"
					.",".TABLE_ST_TICKETS.".last_comment_by = '".$my['uid'] ."'"
					.",".TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
					.",".TABLE_ST_TICKETS.".status='".SupportTicket::ACTIVE."'"
					." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				    $db->query($query_update) ;
				
				}else{
					//Create New Ticket 
					$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
                        . TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
                        . TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
                        . TABLE_ST_TICKETS .".sent_inv_id         = '". $inv_id ."', "
                        . TABLE_ST_TICKETS .".sent_inv_no         = '". $inv_no ."', "
                        . TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
                        . TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
                        . TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
                        . TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
                        . TABLE_ST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
                        . TABLE_ST_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
                        . TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
                        . TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
                        . TABLE_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', "
                        . TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
						. TABLE_ST_TICKETS .".status='".SupportTicket::ACTIVE."',"
                        . TABLE_ST_TICKETS .".ticket_child      = '0', "
						. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_ST_TICKETS .".min = '". $data['min']."', "                        
                        . TABLE_ST_TICKETS .".ticket_response_time = '0', "
                        . TABLE_ST_TICKETS .".ticket_replied        = '0', "
                        . TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
                        . TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
                        . TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
                        . TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
                        . TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
                        . TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ;
					$db->query($query) ;
					$variables['hid'] =  $db->last_inserted_id() ;
				}
				
				
                $messages->setOkMessage("Invoice has been sent successfully on given email id.");  
				
				
				
                /*
                    $_ALL_POST['email']= $_ALL_POST['to'];
                 
                    $file_name = DIR_FS_INV_FILES ."/". $data["inv_no"] .".html";
                    $handle = fopen($file_name, "r");
                    $contents = fread($handle, filesize($file_name));
                    //To check 
                    //$contents = htmlspecialchars($contents) ;
                    fclose($handle);
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT', $data, $email) ) {
                      
                        $cc_to_sender= $cc = $bcc = Null;
                        $to     = '';
                        $email["isHTML"] = true;
                        $to[]   = array('name' => $_ALL_Data['c_f_name'] .' '. $_ALL_Data['c_f_name'], 'email' => $_ALL_POST['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        
                        //SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                        SendMail($to, $from, $reply_to, $email["subject"], $contents, $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                        $messages->setOkMessage("Invoice has been sent successfully on given email id.");  
                    }
                
                */
            }
                
        } 
		$variables['can_view_client_details']=1;
       
		$followupSql = " AND (authorization_id LIKE '%,".Clients::ACCOUNTS.",%' OR authorization_id LIKE '%,".Clients::MANAGEMENT.",%')" ;
		$suCondition =" WHERE parent_id = '".$_ALL_Data['c_user_id']."' 
		AND status='".Clients::ACTIVE."' ".$followupSql." 
		ORDER BY f_name ";
		Clients::getList($db, $subUserDetails_su, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail, 
		email,check_email,email_1,email_2, email_3,email_4,mobile1,mobile2,authorization_id', $suCondition );
		$authorizationNameSu='';
		if(!empty($subUserDetails_su)){
			foreach($subUserDetails_su as $key_su => $val_su){
				$authorizationNameSu='';
				$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
				$authorizationArr1 = explode(",",$val_su['authorization_id']);
				if(!empty($authorizationArr1)){
					foreach($authorizationArr1 as $keys=>$vals){
						$authorizationNameSu.= $clientAuth[$vals].", ";
					}
				}							
				$val_su['authorizationNameSu']=$authorizationNameSu ;
				$subUserDetails[$key_su]= $val_su;
			}
		}
		
		$variables['client_sendmail'] = Clients::SENDMAILNO;
		
        $hidden[] = array('name'=> 'inv_id' ,'value' => $inv_id);
        $hidden[] = array('name'=> 'inv_no' ,'value' => $inv_no);
		$hidden[] = array('name'=> 'flw_ord_id' ,'value' => $flw_ord_id);
        $hidden[] = array('name'=> 'perform' ,'value' => 'send');
        $hidden[] = array('name'=> 'act' , 'value' => 'save');

        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
		$page["var"][] = array('variable' => 'variables', 'value' => 'variables');
        $page["var"][] = array('variable' => '_ALL_Data', 'value' => '_ALL_Data');
        $page["var"][] = array('variable' => 'inv_no', 'value' => 'inv_no');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-send.html');        
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
}else{
 
    $messages->setErrorMessage("You donot have the Right to Send Invoice.");
}
?>