<?php
if ( $perm->has('nc_pst_t_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		 include_once (DIR_FS_INCLUDES .'/prospects-ticket-template.inc.php');	
        
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
             $files       = processUserData($_FILES);   

			$data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_REPOSITORY_FILE_SIZE     ;             
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
            if ( ProspectsTicketTemplate::validateAdd($data, $extra) ) {
			
				if(!empty($data['file_1'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_1']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = mktime()."-1".".".$ext ;
					$data['file_1'] = $attachfilename;
					
					if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_PST_TEMPLATES_FILES."/".$attachfilename)){
					   
					  @chmod(DIR_FS_PST_TEMPLATES_FILES."/".$attachfilename,0777);
					}
				}
				if(!empty($data['file_2'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_2']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = mktime()."-2".".".$ext ;
					$data['file_2'] = $attachfilename;                    
					if (move_uploaded_file ($files['file_2']['tmp_name'], DIR_FS_PST_TEMPLATES_FILES."/".$attachfilename)){
					   @chmod(DIR_FS_PST_TEMPLATES_FILES."/".$attachfilename,0777);
					}
				}
				if(!empty($data['file_3'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_3']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = mktime()."-3".".".$ext ;
					$data['file_3'] = $attachfilename;                    
					if (move_uploaded_file ($files['file_3']['tmp_name'], DIR_FS_PST_TEMPLATES_FILES."/".$attachfilename)){
					   @chmod(DIR_FS_PST_TEMPLATES_FILES."/".$attachfilename,0777);
					}
				}
			
				$query	= " INSERT INTO ".TABLE_PROSPECTS_TICKET_TEMPLATES
						." SET ".TABLE_PROSPECTS_TICKET_TEMPLATES .".title = '". 	$data['title'] ."'" 
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".subject = '". 	$data['subject'] ."'"
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".details = '". 	$data['details'] ."'"
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".file_1 = '".    $data['file_1'] ."'"
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".file_2 = '".    $data['file_2'] ."'"
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".file_3 = '".    $data['file_3'] ."'"
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".status = '". 		$data['status'] ."'"                        
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".access_level = '".   $my['access_level'] ."'"
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                        .",". TABLE_PROSPECTS_TICKET_TEMPLATES .".created_by = '".     $my['user_id'] ."'"
						.",". TABLE_PROSPECTS_TICKET_TEMPLATES .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/prospects-ticket-template.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/prospects-ticket-template.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/prospects-ticket-template.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-ticket-template-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
