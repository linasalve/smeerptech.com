<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php');
    include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
    include_once ( DIR_FS_INCLUDES .'/leads-ticket.inc.php');
    //include_once ( DIR_FS_INCLUDES .'/leads-ticket-template.inc.php');
	include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');    
    $added_all 	= isset($_GET["added_all"]) ? $_GET["added_all"]: ( isset($_POST["added_all"])  ? $_POST["added_all"]:'0');    
    $i 	        = isset($_GET["i"])         ? $_GET["i"]        : ( isset($_POST["i"])          ? $_POST["i"]   :     '0');    
    $j 	        = isset($_GET["j"])         ? $_GET["j"]        : ( isset($_POST["j"])          ? $_POST["j"]   :     '0');    
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : 
	( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
	
	
    $condition_query = '';
    $condition_url = '';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    //echo "added=>".$added;
     
    if($added){
        $messages->setOkMessage("Ticket has been created successfully. And email Notification has been sent.");
    }
    
    if($added_all){
        $messages->setOkMessage("Ticket has been created successfully for ".$i ."/". $j .". And email Notification has been sent.");
    }
    
    if ( $perm->has('nc_ldt') ) {
    
        if($perform !='list_st_clients'){
            $sTypeArray     = array('Any'  =>  array(  'Any of following' => '-1'),
                                    TABLE_LD_TICKETS  => array(  'Ticket no.'        => 'ticket_no',
                                                                'Subject'       => 'ticket_subject',
                                                                'Order Details'       => 'order_details',
                                                                'Ticket Creator'       => 'ticket_creator',
															    'Last Comment By'      => 'last_comment_by_name',
                                                                'Last Comment'      => 'last_comment'
                                                                //'Priority'      => 'ticket_priority',
                                                                //'Department'    => 'ticket_department'
                                                                //'Status'        => 'ticket_status'
                                                            ),
                                    TABLE_SALE_LEADS    =>  array(  'Client First Name'        => 'f_name',
                                                                  'Client Last Name'       => 'l_name',
                                                                 // 'Client Account No.'     => 'number',
                                                                  'Client Billing Name'    => 'billing_name',
                                                                  'Client Company Name'    => 'company_name',
                                                                  'Client Email'       => 'email',
                                                                  //'Client Username.'       => 'username'
                                                                )
                                );
            
            $sOrderByArray  = array(
                                    TABLE_LD_TICKETS    => array('Comment Date'  => 'do_comment',
                                                        'Ticket no'               => 'ticket_no',
                                                        'Subject'               => 'ticket_subject',
                                                        'Ticket Create Date'    => 'ticket_date'                                                        )
                                );
        
            // Set the sorting order of the user list.
            if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_comment';
                $_SEARCH['sOrder']  = $sOrder   = 'DESC';
                $order_by_table     = TABLE_LD_TICKETS;
            }
    
            $variables['active']  = LeadsTicket::ACTIVE;
            $variables['deactive']  = LeadsTicket::DEACTIVE;
            $variables['closed']  = LeadsTicket::CLOSED;
            
           //BOF read the available status
            $variables['status']=LeadsTicket::getStatus();
            $variables['ticket_status']=LeadsTicket::getTicketStatus();
            $variables['PENDINGWITHSMEERP']=LeadsTicket::PENDINGWITHSMEERP;
            $variables['PENDINGWITHSMEERPCOZCLIENT']=LeadsTicket::PENDINGWITHSMEERPCOZCLIENT;
            //EOF read the available status
       }else{
            $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                        TABLE_SALE_LEADS   =>  array(  'Relationship Number' => 'number',
                                                                    'Billing Name'  => 'billing_name',
                                                                    'Company Name'  => 'company_name',
                                                                    'User Name'     => 'username',
                                                                    'E-mail'        => 'email',
                                                                    'First Name'    => 'f_name',
                                                                    'Middle Name'   => 'm_name',
                                                                    'Last Name'     => 'l_name',
                                                                    'Pet Name'      => 'p_name',
                                                                    'Designation'   => 'desig',
                                                                    'Organization'  => 'org',
                                                                    'Domain'        => 'domain'
                                                                )
                                    );
                
                $sOrderByArray  = array(
                                        TABLE_SALE_LEADS => array('Relationship Number'   => 'number',
                                                            'User Name'     => 'username',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Last Name'     => 'l_name',
                                                            'Date of Birth' => 'do_birth',
                                                            'Date of Regis.'=> 'do_reg',
                                                            'Date of Login' => 'do_login',
                                                            'Status'        => 'status'
                                                            ),
                                    );
            
                // Set the sorting order of the user list.
                if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
                    $_SEARCH['sOrderBy']= $sOrderBy = 'do_add';
                    $_SEARCH['sOrder']  = $sOrder   = 'DESC';
                    $order_by_table     = TABLE_SALE_LEADS;
                }
            
                $variables['status'] = Leads::getStatus();
        
       }
        //BOF read the available priority
        //LeadsTicket::getPriority($db,$priority);
        //EOF read the available priority
        
        //use switch case here to perform action. 
        switch ($perform) {
        
            case ('add'): {
            
                include (DIR_FS_NC.'/leads-ticket-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('ordflw_add'): {			
                include (DIR_FS_NC .'/leads-ticket-add.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('ordflw_view'): {			
                include (DIR_FS_NC .'/leads-ticket-view.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			 case ('ldflw_add'): {			
                include (DIR_FS_NC .'/leads-ticket-add.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('ldflw_view'): {			
                include (DIR_FS_NC .'/leads-ticket-view.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('download_file'): {
				include (DIR_FS_NC .'/leads-ticket-template-download.php');
				break;
			} 
            case ('add_all'): {
            
                include (DIR_FS_NC.'/leads-ticket-add-all.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            /*case ('edit_user'):
            case ('edit'): {
                include (DIR_FS_MP .'/user-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            /*
            case ('select_member'): {
                include (DIR_FS_NC .'/leads-ticket-select-member.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/

            case ('view'): {
                include (DIR_FS_NC.'/leads-ticket-view.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_header'): {
                include (DIR_FS_NC.'/leads-ticket-header.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*
            case ('change_status'): {
                include ( DIR_FS_MP .'/user-status.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            case ('search'): {
                include(DIR_FS_NC."/leads-ticket-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('assign_st'): {
                include(DIR_FS_NC."/leads-ticket-assign-st.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('active'):{
                $perform='list';
            	include ( DIR_FS_NC .'/leads-ticket-active.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
			}
            case ('deactive'): {
                $perform='list';
                include ( DIR_FS_NC .'/leads-ticket-deactive.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            /*
            case ('delete_user'):
            case ('delete'): {
                include ( DIR_FS_MP .'/user-delete.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
           
            case ('list_st_clients'):{
                $searchStr=0;
                //include (DIR_FS_NC .'/clients-st-list.php');
                include (DIR_FS_NC .'/clients-st-select-search.php');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-st-select.html');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'clients-st-select-search.html');
                
                $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
                break;
            }            
            case ('list'):
            default: {
                include (DIR_FS_NC .'/leads-ticket-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-ticket.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    //$s->assign("status", $status);
    //$s->assign("priority", $priority);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
           $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>