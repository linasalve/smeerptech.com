<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_pr_su_edit') ) {
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
        $parent_id = isset($_GET["parent_id"]) ? $_GET["parent_id"] : ( isset($_POST["parent_id"]) ? $_POST["parent_id"] : '' );
        
        include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
        include_once ( DIR_FS_CLASS .'/ReminderProspects.class.php');
    
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new RegionProspects();
        $phone          = new PhoneProspects(TABLE_PROSPECTS);
        $reminder       = new ReminderProspects(TABLE_PROSPECTS);
        $access_level   = $my['access_level'] +1 ;
        
        
            
        
        //$al_list    = getAccessLevel($db, $access_level);
        //$role_list  = Prospects::getRoles($db, $access_level);

        //$grade_list  = Prospects::getGrades();
        
        //BOF read the available services
        $services_list  = Prospects::getServices();
        //EOF read the available services
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            //'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            //'reminder'  => $reminder
                        );
            
            if ( Prospects::validateUpdateSubUser($data, $extra) ) {
            
                $service_ids=$_POST['service_id'];
                $service_idstr=implode(",",$service_ids);
                
                if(!empty($service_idstr)){
                    $service_idstr=",".$service_idstr.",";	
                }
                
                $query  = " UPDATE ". TABLE_PROSPECTS
                        ." SET ". TABLE_PROSPECTS .".user_id     = '". $data['user_id'] ."'"
                            .",". TABLE_PROSPECTS .".service_id   = '". $service_idstr ."'"
                            //.",". TABLE_PROSPECTS .".number      = '". $data['number'] ."'"
                            //.",". TABLE_PROSPECTS .".manager     = '". $data['manager'] ."'"
							//.",". TABLE_PROSPECTS .".team			= '". implode(',', $data['team']) ."'"
                            .",". TABLE_PROSPECTS .".username    = '". $data['username'] ."'"
                            . $data['password']
                            //.",". TABLE_PROSPECTS .".access_level= '". $data['access_level'] ."'"
                            .",". TABLE_PROSPECTS .".created_by	= '". $my['uid'] ."'"
                            //.",". TABLE_PROSPECTS .".roles       = '". $data['roles'] ."'"
                            .",". TABLE_PROSPECTS .".email       = '". $data['email'] ."'"
                            .",". TABLE_PROSPECTS .".email_1     = '". $data['email_1'] ."'"
                            .",". TABLE_PROSPECTS .".email_2     = '". $data['email_2'] ."'"
                            .",". TABLE_PROSPECTS .".title       = '". $data['title'] ."'"
                            .",". TABLE_PROSPECTS .".f_name      = '". $data['f_name'] ."'"
                            .",". TABLE_PROSPECTS .".m_name      = '". $data['m_name'] ."'"
                            .",". TABLE_PROSPECTS .".l_name      = '". $data['l_name'] ."'"
                            .",". TABLE_PROSPECTS .".p_name      = '". $data['p_name'] ."'"                            
                            //.",". TABLE_PROSPECTS .".grade      	= '". $data['grade'] ."'"
                            //.",". TABLE_PROSPECTS .".credit_limit = '". $data['credit_limit'] ."'"
                            .",". TABLE_PROSPECTS .".desig       = '". $data['desig'] ."'"
                            .",". TABLE_PROSPECTS .".org         = '". $data['org'] ."'"
                            .",". TABLE_PROSPECTS .".domain      = '". $data['domain'] ."'"
                            .",". TABLE_PROSPECTS .".gender      = '". $data['gender'] ."'"
                            .",". TABLE_PROSPECTS .".do_birth    = '". $data['do_birth'] ."'"
                            .",". TABLE_PROSPECTS .".do_aniv     = '". $data['do_aniv'] ."'"
                            .",". TABLE_PROSPECTS .".remarks     = '". $data['remarks'] ."'"
                            .",". TABLE_PROSPECTS .".allow_ip       = '". $data['allow_ip'] ."'"
                            .",". TABLE_PROSPECTS .".valid_ip       = '". $data['valid_ip'] ."'"
                            .",". TABLE_PROSPECTS .".status      = '". $data['status'] ."'" 
                        ." WHERE ". TABLE_PROSPECTS .".user_id   = '". $data['user_id'] ."'";
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['user_id'];

                    // Update the address.
                    /*for ( $i=0; $i<$data['address_count']; $i++ ) {
                        if ( isset($data['a_remove'][$i]) && $data['a_remove'][$i] == '1') {
                            if ( ! $region->delete($db, $data['address_id'][$i], $variables['hid'], TABLE_PROSPECTS) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setOkMessage($errors['description']);
                                }
                            }
                        }
                        else {
                            $address_arr = array(   'id'            => isset($data['address_id'][$i])? $data['address_id'][$i] : '0',
                                                    'address_type'  => $data['address_type'][$i],
                                                    'company_name'  => $data['company_name'][$i],
                                                    'address'       => $data['address'][$i],
                                                    'city'          => $data['city'][$i],
                                                    'state'         => $data['state'][$i],
                                                    'country'       => $data['country'][$i],
                                                    'zipcode'       => $data['zipcode'][$i],
                                                    'is_preferred'  => isset($data['is_preferred'][$i])? $data['is_preferred'][$i] : '0',
                                                    'is_verified'   => isset($data['is_verified'][$i])? $data['is_verified'][$i] : '0'
                                                );
                            
                            if ( !$region->update($variables['hid'], TABLE_PROSPECTS, $address_arr) ) {
                                foreach ( $region->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }*/
                    
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_PROSPECTS) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $phone_arr = array( 'id'            => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
                                                    'p_type'        => $data['p_type'][$i],
                                                    'cc'            => $data['cc'][$i],
                                                    'ac'            => $data['ac'][$i],
                                                    'p_number'      => $data['p_number'][$i],
                                                    'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
                                                    'p_is_verified' => isset($data['p_is_verified'][$i]) ? $data['p_is_verified'][$i] : '0'
                                                );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_PROSPECTS, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Update the Reminders.
                    /*for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['remind_date_'.$i]) ) {
                            if ( isset($data['r_remove_'.$i]) && $data['r_remove_'.$i] == '1') {
                                if ( ! $reminder->delete($db, $data['remind_id_'.$i], $variables['hid'], TABLE_PROSPECTS) ) {
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $reminder_arr = array(  'id'            => (isset($data['remind_id_'.$i])? $data['remind_id_'.$i] : ''),
                                                        'do_reminder'   => $data['remind_date_'.$i],
                                                        'description'   => $data['remind_text_'.$i]
                                                    );
                                if ( ! $reminder->update($db, $variables['hid'], TABLE_PROSPECTS, $reminder_arr) ) {
                                    $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }*/
                    
                    // Send The Email to the concerned persons.
                    /*include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_uc_add_notif', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_uc_add_notif_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    $manager = Prospects::getManager($db, '', $data['manager'], 'f_name,l_name,email');
                    $data['manager_name']   = $manager['f_name'] .' '. $manager['l_name'];
                    $data['manager_email']  = $manager['email'];
                    $data['manager_phone']  = '';
                    
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link']       = DIR_WS_NC .'/clients.php?perform=view_details&user_id='. $data['user_id'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'CLIENT_INFO_UPDATE_MANAGERS', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        
                    }
                    
                    // Send Email to the Client whose infromation was updated.
                    $data['link']       = DIR_WS_NC;
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'CLIENT_INFO_UPDATE', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['f_name'] .' '. $data['l_name'], 'email' => $data['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }*/
                    $messages->setOkMessage("Sub Users Profile has been updated.");
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Sub Users Profile was not updated.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/clients-sub-user-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'            => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
                
                // Preserve the Addresses.
               /* $count = count($_ALL_POST['city']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['address_list'][] = array(   'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'company_name'   => isset($_ALL_POST['company_name'][$i])? $_ALL_POST['company_name'][$i]:'',
                                                                'is_verified'   => isset($_ALL_POST['is_verified'][$i])? $_ALL_POST['is_verified'][$i]:'',
                                                                'is_preferred'  => isset($_ALL_POST['is_preferred'][$i])? $_ALL_POST['is_preferred'][$i]:'',
                                                                'address_type'  => isset($_ALL_POST['address_type'][$i])? $_ALL_POST['address_type'][$i]:'',
                                                                'address'       => isset($_ALL_POST['address'][$i])? $_ALL_POST['address'][$i]:'',
                                                                'city'          => isset($_ALL_POST['city'][$i])? $_ALL_POST['city'][$i]:'',
                                                                'state'         => isset($_ALL_POST['state'][$i])? $_ALL_POST['state'][$i]:'',
                                                                'zipcode'       => isset($_ALL_POST['zipcode'][$i])? $_ALL_POST['zipcode'][$i]:'',
                                                                'country'       => isset($_ALL_POST['country'][$i])? $_ALL_POST['country'][$i]:'',
                                                     );
//                    }
                }*/

                
                // Preserve the Reminders.
                /*$count = $_ALL_POST['reminder_count'];
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['remind_date_'.$i) && !empty($_ALL_POST['remind_date_'.$i) ) {
                        $_ALL_POST['reminder_list'][] = array(  'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'do_reminder'   => isset($_ALL_POST['remind_date_'.$i])? $_ALL_POST['remind_date_'.$i]:'',
                                                                'description'   => isset($_ALL_POST['remind_text_'.$i])? $_ALL_POST['remind_text_'.$i]:'',
                                                     );
//                    }
                }*/
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( Prospects::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                    
                    $condition_query= " WHERE user_id = '". $_ALL_POST['parent_id'] ."' ";
                    $my_services_list  = NULL;
                    if ( Prospects::getList($db, $my_services_list, 'service_id', $condition_query) > 0 ) {
                    
                        $my_services_list = $my_services_list[0]['service_id'];
                        
                        $my_services_list   = explode(",", $my_services_list);
                        
                        //print_r($my_services_list['service_id']);
                    }
                    
                            
                    //$_ALL_POST['manager'] 	= Prospects::getManager($db, '', $_ALL_POST['manager']);
					//$_ALL_POST['roles'] 	= explode(',', $_ALL_POST['roles']);
					
                    $_ALL_POST['service_id']    = explode(",", $_ALL_POST['service_id']);
					// BO: Read the Team Members Information.
					/*if ( !empty($_ALL_POST['team']) && !is_array($_ALL_POST['team']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
						$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
						$_ALL_POST['team_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['team'] = array();
						foreach ( $_ALL_POST['team_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['team'][] = $members['user_id'];
							$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}*/
					// EO: Read the Team Members Information.
					
                    // Check the Access Level.
                    if ( $_ALL_POST['access_level'] < $access_level ) {
                        // Read the Contact Numbers.
                        $phone->setPhoneOf(TABLE_PROSPECTS, $user_id);
                        $_ALL_POST['phone'] = $phone->get($db);
                        
                        // Read the Addresses.
                        $region->setAddressOf(TABLE_PROSPECTS, $user_id);
                        $_ALL_POST['address_list'] = $region->get();
                        
                        // Read the Reminders.
                        /*$reminder->setReminderOf(TABLE_PROSPECTS, $user_id);
                        $_ALL_POST['reminder_list'] = $reminder->get($db);*/
                    }
                    
                }
            }

            if ( !empty($_ALL_POST['user_id']) ) {

                // Check the Access Level.
                if ( $_ALL_POST['access_level'] < $access_level ) {

                    $phone_types    = $phone->getTypeList();
                    $address_types  = $region->getTypeList();
                    /*$lst_country    = $region->getCountryList();
                    $lst_state      = $region->getStateList();
                    $lst_city       = $region->getCityList();*/
                    
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    
                    //print_r($_ALL_POST);
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit_sub_user');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    //$hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    $page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    //$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
                    /*$page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');*/
                    //$page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
                    $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
                    $page["var"][] = array('variable' => 'my_services_list', 'value' => 'my_services_list');
                    $page["var"][] = array('variable' => 'allowipArr', 'value' => 'allowipArr');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-sub-user-edit.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Sub User with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected Sub User was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>