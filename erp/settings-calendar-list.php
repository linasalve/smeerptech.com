<?php

    if ( $perm->has('nc_cal_list') ) {
		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched']=1;
        
        //echo $condition_query;
        // To count total records.
        $list	= 	NULL;
        $total	=	Calendar::getDetails( $db, $list, '', $condition_query);
    
       // $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $list	= NULL;
        $fields = TABLE_SETTINGS_CALENDAR .'.*'     ;
        Calendar::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                
               $exeCreted=$exeCreatedname='';
               $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               $condition1 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
               User::getList($db,$exeCreted,$fields1,$condition1);
              
               foreach($exeCreted as $key1=>$val1){
                    $exeCreatedname .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['created_by'] = $exeCreatedname;             
               
               $fList[$key]=$val;
            }
        }
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
       
        if ( $perm->has('nc_cal_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_cal_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_cal_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_cal_delete') ) {
            $variables['can_delete'] = true;
        }
   
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-calendar-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>
