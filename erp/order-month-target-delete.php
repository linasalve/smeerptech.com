<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_ordm_t_delete') ) {
        $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
		$extra = array( 'db'           => &$db,
                        'messages'     => &$messages
                        );
        
		OrdMonthTarget::delete($id, $extra);
       $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'order-month-target-delete.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>