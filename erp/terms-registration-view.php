<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_tcr_details') ) {
        $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
    
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        //$al_list    = getAccessLevel($db, $access_level);

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $id;
            include ( DIR_FS_NC .'/terms-condition-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE page_id = '". $page_id ."' ";
                $_ALL_POST      = NULL;
                if ( Staticpages::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                }
            }

            if ( !empty($_ALL_POST['page_id']) ) {

                // Check the Access Level.
                //if ( $_ALL_POST['access_level'] < $access_level ) {

                    //print_r($_ALL_POST);
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'page_id', 'value' => $page_id);
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
        
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'staticpage-view.html');
             /*   }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Client with the current Access Level.");
                }*/
            }
            else {
                $messages->setErrorMessage("The Selected Page was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>