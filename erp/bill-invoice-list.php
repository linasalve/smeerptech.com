<?php

    if ( $perm->has('nc_bl_inv_list') ) {
         include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php');
		  include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
	
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id='0' ORDER BY id ASC ";
		ServiceTax::getList($db, $taxArr, 'id,tax_name', $condition_queryst);
    
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            /*
            $condition_query = '';
            $time   = time();
            $l_mon  = strtotime('-1 month', $time);
            $from   = date('Y-m-d 00:00:00', $l_mon );
            $to     = date('Y-m-d 23:59:59', $time);
            
            $condition_query = " WHERE ". TABLE_BILL_INV .".do_i <= '". $to ."' "
                                ." AND ("
                                    . TABLE_BILL_INV .".status = '". Invoice::BLOCKED ."'"
                                    ." OR ". TABLE_BILL_INV .".status = '". Invoice::ACTIVE ."'"
                                    ." OR ". TABLE_BILL_INV .".status = '". Invoice::PENDING ."'"
                                    ." OR ". TABLE_BILL_INV .".status = '". Invoice::DELETED ."'"
                                .")";
            
            $_SEARCH["date_from"]   = date('d/m/Y', $l_mon);
            $_SEARCH["chk_date_to"] = 'AND';
            $_SEARCH["date_to"]     = date('d/m/Y', $time);*/
            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(Invoice::COMPLETED,Invoice::PENDING);            
            $_SEARCH["chk_paid"]  = 'AND';
            $_SEARCH["paidStatus"] = InvoiceProforma::UN_PAID;  
			
            $condition_query = " WHERE (". TABLE_BILL_INV .".status = '". Invoice::COMPLETED ."' OR ". TABLE_BILL_INV .".status = '". Invoice::PENDING."')  AND ".TABLE_BILL_INV_PROFORMA.".paid_status='".InvoiceProforma::UN_PAID."'"   ;
        }
        
        // If the Invoice is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_inv_list_al') ) {
            $access_level += 1;
        }
       
        
        if ( $perm->has('nc_bl_inv_list_all') ) {
        
             $condition_query .='';
             
        }else{
                $condition_query .= " AND ( ";
            
                // If my has created this Invoice.
                $condition_query .= " ( ". TABLE_BILL_INV .".created_by = '". $my['user_id'] ."' "
                                        ." AND ". TABLE_BILL_INV .".access_level < $access_level ) ";
                
                // If my is the Client Manager
                $condition_query .= " OR ( ". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                                    ." AND ". TABLE_BILL_INV .".access_level < $access_level ) ";
                
                // Check if the User has the Right to view Invoices created by other Users.
                if ( $perm->has('nc_bl_inv_list_ot') ) {
                    $access_level_o   = $my['access_level'];
                    if ( $perm->has('nc_bl_inv_list_ot_al') ) {
                        $access_level_o += 1;
                    }
                    $condition_query .= " OR ( ". TABLE_BILL_INV. ".created_by != '". $my['user_id'] ."' "
                                        ." AND ". TABLE_BILL_INV .".access_level < $access_level_o ) ";
                }
           $condition_query .= " ) ";
        }
		$condition_query_tot = $condition_query;
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
        
            $extra_url  = $condition_url;
            
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }        
        $condition_url .="&perform=".$perform;
        
        //Code to calculate total of Amount BOF
        /*
		$totalAmt =array();
		$totalAmount='';
		$fields= " SUM(".TABLE_BILL_INV.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_INV_PROFORMA.".balance_inr) as totalBalanceInr, SUM(".TABLE_BILL_INV.".amount) as totalAmount, 
		SUM(".TABLE_BILL_INV_PROFORMA.".balance) as totalBalance ";
		//$condition_query_tot .=" GROUP BY ".TABLE_BILL_INV.".number "; 
		Invoice::getDetails(  $db, $totalAmt, $fields, $condition_query_tot);
		if(!empty($totalAmt)){
			$totalAmountInr=number_format($totalAmt[0]['totalAmountInr'],2);
			$totalAmount=number_format($totalAmt[0]['totalAmount'],2);
			$totalBalanceInr = number_format($totalAmt[0]['totalBalanceInr'],2);
			$totalBalance = number_format($totalAmt[0]['totalBalance'],2);
		}
		*/
		
		//Get total Amount BOF
		/*
		$sql = " SELECT SUM( c.amount_inr ) AS totalAmountInr,
				 SUM( c.balance_inr ) AS totalBalanceInr,
				 SUM( c.amount ) AS totalAmount,
				 SUM( c.balance ) AS totalBalance
			FROM ( 
				SELECT DISTINCT(".TABLE_BILL_INV.".id),".TABLE_BILL_INV.".amount_inr,
				".TABLE_BILL_INV.".balance_inr,
				".TABLE_BILL_INV.".amount,
				".TABLE_BILL_INV.".balance
				FROM ". TABLE_BILL_INV. " LEFT JOIN ". TABLE_BILL_ORDERS
                ." ON ". TABLE_BILL_ORDERS .".number = ". TABLE_BILL_INV .".or_no 
				LEFT JOIN ". TABLE_BILL_INV_PROFORMA." ON ( ".TABLE_BILL_INV .".inv_profm_no = ". TABLE_BILL_INV_PROFORMA .".number AND
				".TABLE_BILL_INV_PROFORMA.".status = '".InvoiceProforma::COMPLETED."' ) LEFT JOIN ". TABLE_BILL_ORD_P
                ." ON ". TABLE_BILL_ORD_P .".ord_no = ". TABLE_BILL_INV .".or_no LEFT JOIN ". TABLE_USER
                ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_INV .".created_by  LEFT JOIN ". TABLE_CLIENTS
                ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_INV .".client ";
		 $sql .= " ". $condition_query." ) as c "; 
		*/
		//* Leena 2014-09-sep-01
		
		$sql = " SELECT SUM( c.amount_inr ) AS totalAmountInr,
				 SUM( c.balance_inr ) AS totalBalanceInr,
				 SUM( c.amount ) AS totalAmount,
				 SUM( c.balance ) AS totalBalance
			FROM ( 
				SELECT DISTINCT(".TABLE_BILL_INV.".id),".TABLE_BILL_INV_PROFORMA.".amount_inr,
				".TABLE_BILL_INV_PROFORMA.".balance_inr,
				".TABLE_BILL_INV_PROFORMA.".amount,
				".TABLE_BILL_INV_PROFORMA.".balance
				FROM ". TABLE_BILL_INV. " LEFT JOIN ". TABLE_BILL_ORDERS
                ." ON ". TABLE_BILL_ORDERS .".number = ". TABLE_BILL_INV .".or_no 
				LEFT JOIN ". TABLE_BILL_INV_PROFORMA." ON ( ".TABLE_BILL_INV .".inv_profm_no = ". TABLE_BILL_INV_PROFORMA .".number AND
				".TABLE_BILL_INV_PROFORMA.".status = '".InvoiceProforma::COMPLETED."' ) LEFT JOIN ". TABLE_BILL_ORD_P
                ." ON ". TABLE_BILL_ORD_P .".ord_no = ". TABLE_BILL_INV .".or_no LEFT JOIN ". TABLE_USER
                ." ON ". TABLE_USER .".user_id = ". TABLE_BILL_INV .".created_by  LEFT JOIN ". TABLE_CLIENTS
                ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_INV .".client ";
		 $sql .= " ". $condition_query." ) as c "; 
		if ( $db->query($sql) ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$totalAmountInr =$db->f("totalAmountInr") ;
					$totalAmount =$db->f("totalAmount") ;
					$totalBalanceInr =$db->f("totalBalanceInr") ;
					$totalBalance =$db->f("totalBalance") ;
				}
			}
		}
		 
		
		
        //Code to calculate total of Amount EOF
        
        // To count total records.
        $list	= 	NULL;
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
        if($searchStr==1){
		//$bgselect = "SET SQL_BIG_SELECTS=1" ;
		//$db->query($bgselect);
		//$db->query("SET MAX_JOIN_SIZE=3");
			
           $fields = "DISTINCT(".TABLE_BILL_INV.".id)"
                        .','. TABLE_BILL_INV .'.number'
                        .','. TABLE_BILL_INV .'.inv_profm_id'
                        .','. TABLE_BILL_INV .'.inv_profm_no'
                        .','. TABLE_BILL_INV .'.or_no'
                        .','. TABLE_BILL_INV .'.access_level'
                        .','. TABLE_BILL_INV .'.do_e'
                        .','. TABLE_BILL_ORDERS .'.do_fe'
                        .','. TABLE_BILL_INV .'.do_c'
                        .','. TABLE_BILL_INV .'.do_i'
                        .','. TABLE_BILL_INV .'.do_d'
                        .','. TABLE_BILL_INV .'.currency_abbr'
                        .','. TABLE_BILL_INV .'.tax1_name'
                        .','. TABLE_BILL_INV .'.tax1_value'
                        .','. TABLE_BILL_INV .'.tax1_total_amount'
                        .','. TABLE_BILL_INV .'.tax1_sub1_name'
                        .','. TABLE_BILL_INV .'.tax1_sub1_value'
                        .','. TABLE_BILL_INV .'.tax1_sub1_total_amount'
                        .','. TABLE_BILL_INV .'.tax1_sub2_name'
                        .','. TABLE_BILL_INV .'.tax1_sub2_value'
                        .','. TABLE_BILL_INV .'.tax1_sub2_total_amount'
                        .','. TABLE_BILL_INV .'.sub_total_amount'
                        .','. TABLE_BILL_INV .'.round_off_op'
                        .','. TABLE_BILL_INV .'.round_off'  
                        .','. TABLE_BILL_INV .'.amount'
                        .','. TABLE_BILL_INV .'.amount_inr'
                        .','. TABLE_BILL_INV .'.balance'
                        .','. TABLE_BILL_INV .'.balance_inr'
						.','. TABLE_BILL_INV_PROFORMA .'.id as prfm_id'
						.','. TABLE_BILL_INV_PROFORMA .'.do_i as do_profm_inv'
						.','. TABLE_BILL_INV_PROFORMA .'.balance as prfm_balance'
                        .','. TABLE_BILL_INV_PROFORMA .'.balance_inr as prfm_balance_inr'
						.','. TABLE_BILL_INV_PROFORMA .'.show_period as prfm_show_period'
                        .','. TABLE_BILL_INV .'.status'
                        .','. TABLE_BILL_INV .'.is_old'
                        .','. TABLE_BILL_INV .'.old_updated'
                        .','. TABLE_BILL_INV .'.reason_of_delete'
						.','. TABLE_BILL_INV .'.show_period'
                        .','. TABLE_BILL_ORDERS .'.order_title'
                        .','. TABLE_BILL_ORDERS .'.id as order_id'
						.','. TABLE_BILL_ORDERS .'.followup_status'
						.','. TABLE_BILL_ORDERS .'.do_o'
						.','. TABLE_BILL_ORDERS .'.is_hardware_ord'
						.','. TABLE_BILL_ORDERS .'.special'
						.','. TABLE_BILL_ORDERS .'.discount_explanation'
						.','. TABLE_BILL_ORDERS .'.npa_status'
						.','. TABLE_BILL_ORDERS .'.financial_yr'	
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.org'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.mobile1'
                        .','. TABLE_CLIENTS .'.mobile2'
                        .','. TABLE_CLIENTS .'.billing_name AS billing_name'
                        .','. TABLE_CLIENTS .'.status AS c_status';
           echo $total	= Invoice::getDetails( $db, $list, '', $condition_query);  
			 
			 	 
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');            
            $list	= NULL;            
            Invoice::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }  
        
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){               
               $val['is_rcpt_created'] = false;
               $val['is_paid_fully'] = false;
                // check whether rcpt created or not BOF
                $sql = " SELECT id FROM ".TABLE_BILL_RCPT." WHERE ".TABLE_BILL_RCPT.".inv_no = 
				'".$val['number']."' AND status !='".Receipt::DELETED."' LIMIT 0,1";
                if ( $db->query($sql) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()){
                           $rcpt_id = $db->f('id');
                           if($rcpt_id  > 0 ){
                                $val['is_rcpt_created']= true ;
                           }
                        }
                    }                   
                }
                // check whether rcpt created or not EOF
                if($val['balance'] <= 0){
                    $val['is_paid_fully'] = true;
                }
				//check invoice id followup in ST BOF
				$val['ticket_id']='';
				$sql2 = "SELECT ticket_id FROM ".TABLE_ST_TICKETS." WHERE 
				(".TABLE_ST_TICKETS.".flw_ord_id = '".$val['order_id']."') AND 
				".TABLE_ST_TICKETS.".ticket_child = 0 LIMIT 0,1";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                        }
                    }                   
                }
				//check invoice id followup in ST EOF	
				$val['tax_amount'] = $val['tax1_total_amount'] + $val['tax1_sub1_total_amount'] + $val['tax1_sub2_total_amount'] ;
				$val['sub_total_amount']=number_format($val['sub_total_amount'],2);
				$val['tax_amount']=number_format($val['tax_amount'],2);
                //$val['sub_total_amount']=number_format($val['sub_total_amount'],2);
				
                $val['amount']=number_format($val['amount'],2);
                $val['amount_inr']=number_format($val['amount_inr'],2);
                $val['balance']=number_format($val['balance'],2);
                $val['balance_inr']=number_format($val['balance_inr'],2);
                $val['prfm_balance']=number_format($val['prfm_balance'],2);
                $val['prfm_balance_inr']=number_format($val['prfm_balance_inr'],2);
                $is_rcpt_created = 0 ;
				if($val['inv_profm_id']>0){
					$sql= " SELECT id as rcpt_id FROM ".TABLE_BILL_RCPT." WHERE ".TABLE_BILL_RCPT.".inv_profm_id
					='".$val['inv_profm_id']."' AND ".TABLE_BILL_RCPT.".status ='".Receipt::ACTIVE."' LIMIT 0,1";
					$db->query($sql);   
									
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) {
							$rcpt_id = $db->f('rcpt_id') ;
							if( $rcpt_id > 0){
								$is_rcpt_created = 1;
							}
						}
					}
				}
			    $val['is_rcpt_created'] = $is_rcpt_created;
                $flist[$key]=$val;
            }
        }
		
        Invoice::getCompany($db,$company);
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_send_invoice'] = false;
        $variables['can_followup']= false;
		$variables['can_add_ord'] = false;
		$variables['can_print_lbl'] = false;
		$variables['can_mark_flw'] = false;
		$variables['can_mark_tds'] = false;
		$variables['can_mark_npa'] = false;
        if ( $perm->has('nc_bl_inv_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_bl_inv_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_bl_inv_edit') ) {
           $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_bl_inv_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_bl_inv_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_bl_inv_status') ) {
            $variables['can_change_status']     = true;
        }
        if ( $perm->has('nc_bl_inv_send') ) {
            $variables['can_send_invoice']     = true;
        }
        if ( $perm->has('nc_st') && $perm->has('nc_st_flw_inv') ) {
            $variables['can_followup'] = true;
        }
		
        if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_add') ) {
            $variables['can_add_ord'] = true;
        }
		if ($perm->has('nc_uc') && $perm->has('nc_uc_lblprn') ) {
            $variables['can_print_lbl'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_flw_status') ) {
            $variables['can_mark_flw'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_tds_status') ) {
            $variables['can_mark_tds'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_npa_status') ) {
            $variables['can_mark_npa'] = true;
        }
		$variables['tdsflw'] = Order::FOLLOWUPTDS;
		$variables['ceoflw'] = Order::FOLLOWUPCEO;
		$variables['noneflw'] = Order::FOLLOWUPPENDING;
		$variables['activeflw'] = Order::FOLLOWUPACTIVE;
		$variables['activenpa'] = Order::NPAACTIVE;
		$variables['pendingnpa'] = Order::NPADEACTIVE;
		$variables['inv_html'] = DIR_WS_INV_HTML_FILES;
		$variables['inv_htmlp'] = DIR_WS_INV_HTMLPRINT_FILES; 
		$variables['inv_pdf'] = DIR_WS_INV_PDF_FILES; 
        $page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount');
        $page["var"][] = array('variable' => 'totalAmountInr', 'value' => 'totalAmountInr');
        $page["var"][] = array('variable' => 'totalBalance', 'value' => 'totalBalance');
        $page["var"][] = array('variable' => 'totalBalanceInr', 'value' => 'totalBalanceInr');
        $page["var"][] = array('variable' => 'total', 'value' => 'total');
        $page["var"][] = array('variable' => 'taxArr', 'value' => 'taxArr');
		$page["var"][] = array('variable' => 'company', 'value' => 'company');
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>