<?php
    if ( $perm->has('nc_cal_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*echo $sql = "SELECT DATEDIFF('2009-09-23','2009-08-22') as datediff";
        $db->query($sql);
        $db->next_record();
        echo $date_diff=$db->f('datediff');*/
                
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
           
            $extra = array( 'db' 				=> &$db,
							'messages'          => &$messages
                        );
                
            if ( Calendar::validateAdd($data, $extra) ) { 
                
                $actual_working_days = $data['total_days'] - $data['holidays'];
                $query	= " INSERT INTO ".TABLE_SETTINGS_CALENDAR
                            ." SET ". TABLE_SETTINGS_CALENDAR .".start_date         = '". $data['start_date'] ."'"
                                .",". TABLE_SETTINGS_CALENDAR .".end_date           = '". $data['end_date'] ."'"
                                .",". TABLE_SETTINGS_CALENDAR .".title              = '". $data['title'] ."'"
                                .",". TABLE_SETTINGS_CALENDAR .".total_days         = '". $data['total_days']."'"
                                .",". TABLE_SETTINGS_CALENDAR .".holidays           = '". $data['holidays'] ."'"
                                .",". TABLE_SETTINGS_CALENDAR .".actual_working_days= '". $actual_working_days ."'"
                                .",". TABLE_SETTINGS_CALENDAR .".status             = '". $data['status'] ."'"
                              	.",". TABLE_SETTINGS_CALENDAR .".ip                 = '". $_SERVER['REMOTE_ADDR'] ."'"  
                              	.",". TABLE_SETTINGS_CALENDAR .".created_by         = '". $data['created_by'] ."'"                              
                                .",". TABLE_SETTINGS_CALENDAR .".do_e               = '". date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/settings-calendar.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/settings-calendar.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/settings-calendar.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-calendar-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
