<?php
if ( $perm->has('nc_sms_gtw_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( SmsGateway::validateUpdate($data, $extra) ) {
               $query  = " UPDATE ". TABLE_SMS_GATEWAY
                            ." SET ". TABLE_SMS_GATEWAY .".company 				= '".		$data['company'] ."'"
                                   	.",". TABLE_SMS_GATEWAY .".address 			= '". $data['address'] ."'"                                
                                	.",". TABLE_SMS_GATEWAY .".contact_name1 	= '". $data['contact_name1'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_dept1 			= '". $data['contact_dept1'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_email1 	= '". $data['contact_email1'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_phone1 			= '". $data['contact_phone1'] ."'"                    
                                    .",". TABLE_SMS_GATEWAY .".contact_name2 	= '". $data['contact_name2'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_dept2 			= '". $data['contact_dept2'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_email2 	= '". $data['contact_email2'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".contact_phone2 			= '". $data['contact_phone2'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".order				= '". $data['order'] ."'"                    
                                	.",". TABLE_SMS_GATEWAY .".details 	= '". $data['details'] ."'"                     	
									.",". TABLE_SMS_GATEWAY .".status 			= '". $data['status'] ."'"                    
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Sms Gateway has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SMS_GATEWAY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SMS_GATEWAY .".id = '". $id ."' )";
            
            if ( SmsGateway::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                    // Setup the date of delivery.
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            include ( DIR_FS_NC .'/sms-gateway-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-gateway-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
