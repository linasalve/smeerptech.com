<?php
    if ( $perm->has('nc_urol_edit') ) {
        $data		= NULL;
        $chk_output	= NULL;
        $_ALL_POST  = NULL;
        $chk_rights	= NULL;
        $rights_array = NULL;
        $al_list    = getAccessLevel($db, ($my['access_level']+1));
        
        $role_id = isset ($_GET['role_id']) ? $_GET['role_id'] : ( isset($_POST['role_id'] ) ? $_POST['role_id'] :'');
        $x 	= isset($_GET["x"])       ? $_GET["x"]      : ( isset($_POST["x"])        ? $_POST["x"]     : 0  );// Result per page
		$rpp1 = isset($_GET["rpp1"])       ? $_GET["rpp1"]      : ( isset($_POST["rpp1"])        ? $_POST["rpp1"]     : 5  );// Result per page
        if ( empty($x) ) {
			$x              = 1;
			$next_record1    = 0 ;
		}else {
			$next_record1    = ($x-1) * $rpp1;
		}
		
		$condition_query    = " WHERE id = '". $role_id ."' ";
		$data               = NULL;
		if ( UserRoles::getList($db, $data, '*', $condition_query) > 0 ) {
			$data                  =   $data['0']; 
			$data['chk_rights']  = $db_chk_rights = explode(",", $data['rights']); 
		} 
        // update form.
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST	=	$_POST;
            $data		=  processUserData($_ALL_POST); 
			
            //insert comma(,) when select multipule rights. 
            $rights_array1 = array();
			if ( isset($data['chk_rights']) &&  $data['chk_rights']!='' ){
                $chk_output[]   = $data['chk_rights']; 
                /*   
				foreach( $chk_output as $key=>$val ){
                    $rights_array	=  $val;
                }  */
				$rights_array1 = array_merge($data['chk_rights'], $db_chk_rights);
				$rights_array1 = array_unique($rights_array1);
                //$chk_rights    = implode(",", $rights_array1);
            }
			
			
			//insert comma(,) when select multipule rights. 
			
            if ( isset($data['chkd_rights']) &&  $data['chkd_rights']!='' ){
                $chkd_output[]   = $data['chkd_rights'];
                 
				if(!empty($rights_array1)){
					$rights_array1 = array_diff($rights_array1,$data['chkd_rights']);
				}else{
					$rights_array1 = array_diff($db_chk_rights,$data['chkd_rights']);
				}
				 
            }			
			 
			$rights_array1 = array_unique($rights_array1 );
			$fchk_rights    = implode(",", $rights_array1);
            
			$extra = array( 'db' 				=> &$db,
                            'data' 				=> $data,
                            'messages' 			=> $messages,
                            'fchk_rights' 		=> $fchk_rights,
                    );

            //check errors before updating the User roles.
            if(UserRoles::validateUpdate($data, $messages, $extra, $role_id)){
				$rights_values =$value = '';
				$sql = " SELECT value FROM ".TABLE_USER_RIGHTS." WHERE id IN(".$fchk_rights.") " ; 
				$db->query($sql);
				if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$value.=$db->f('value').",";
					}
					$rights_values = trim($value,",");
				}
                $query	= " UPDATE  ".TABLE_USER_ROLES
							. " SET "
							. TABLE_USER_ROLES .".title 			='". $data['title'] ."'"
							.",". TABLE_USER_ROLES .".rights		='". $fchk_rights ."'"
							.",". TABLE_USER_ROLES .".rights_values			='". $rights_values ."'"
							.",". TABLE_USER_ROLES .".description	='". $data['description'] ."'"
							.",". TABLE_USER_ROLES .".access_level   ='". $data['access_level'] ."'"
						." WHERE ".TABLE_USER_ROLES. ".id = '". $role_id ."'";

                if ( $db->query($query) ){
                    if ( $db->affected_rows() > 0 ){
                        $messages->setOkMessage("Users Role Updated Successfully.");
                    }else{
                        $messages->setOkMessage("No changes were made to the User Role.");
                    }
                }
            }
        }
        
		
		if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/user-roles-ajx.php?perform=edit&role_id=".$role_id."&x=".$x);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/user-roles-ajx.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/user-roles-ajx.php?perform=edit&role_id=".$role_id."&x=".$x);   
        }else{
			
		
            $hidden[]   = array('name'=> 'perform' , 'value' => 'edit');
            $hidden[]   = array('name'=> 'act' , 'value' => 'save');
            $hidden[]   = array('name'=> 'role_id' , 'value' => $role_id);
			include_once('user-roles-ajx-rights-list.php');
			
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'rightList', 'value' => 'rightList');
            $page["var"][] = array('variable' => 'extra', 'value' => 'extra');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            
    
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-roles-ajx-edit.html');
        } 
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
		 
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>