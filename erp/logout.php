<?php

	if(!@constant("THIS_DOMAIN")){
		require_once("../lib/config.php"); 
	} 

    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Default_Auth"
                   ));
    	
	if ( isset($auth) ) {
        $auth->logout();
        $auth = NULL;
    }
    
    page_close();
    header("Location: index.php");
    exit;
?>
