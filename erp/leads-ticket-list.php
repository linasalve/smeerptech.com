<?php

    if ( $perm->has('nc_ldt_list') || $perm->has('nc_ldt_list_all')) {
        
        include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
        //Get my team members bof
		$my_team_list=array();		 
		$condition_query_user = " WHERE report_to LIKE '%".trim($my['user_id'])."%'";
		User::getList( $db, $my_team_list, 'user_id, f_name,l_name,number', $condition_query_user);
		$count= count($my_team_list) + 1;
		$my_team_list[$count] = array('user_id'=>$my['user_id'],
									'f_name'=>$my['f_name'],
									'l_name'=>$my['l_name'],
									'number'=>$my['number']
								);
						 
		//Get my team members eof
		
        if(empty($condition_query)){
			$_SEARCH["chk_t_status"]  = 'AND';
			$_SEARCH["tStatus"]   = array(LeadsTicket::PENDINGWITHSMEERP,LeadsTicket::PENDINGWITHSMEERPCOZCLIENT);   
            $condition_query = " AND ticket_status IN('".LeadsTicket::PENDINGWITHSMEERP."','"
			.LeadsTicket::PENDINGWITHSMEERPCOZCLIENT."') ";
        }
 		$subSql='';
		if($perm->has('nc_ldt_list_all')){
		
		
			
		}else{
			if(!isset($_POST["sTeam"]) || empty($_POST["sTeam"]) ){
				if(!empty($my_team_list)){
					foreach($my_team_list as $key=>$val ){
					 
						$subSql .= " ".TABLE_LD_TICKETS.".tck_owner_member_id ='".$val['user_id']."' OR " ;
					}
					$subSql = substr( $subSql, 0, strlen( $subSql ) - 3 );
					$condition_query .= " AND ( ".$subSql." ) " ;
					
				}
			}
		}
		
        $condition_query = " LEFT JOIN ".TABLE_SALE_LEADS." ON 
		".TABLE_SALE_LEADS.".lead_id = ".TABLE_LD_TICKETS.".ticket_owner_uid  
		WHERE ".TABLE_LD_TICKETS.".ticket_child='0' ".$condition_query    ;

        echo $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;

        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        $total	=	LeadsTicket::getList( $db, $list, '', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
       
        $extra_url  = '';
        if( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $list	= NULL;
        //$fields = TABLE_LD_TICKETS.".*,".TABLE_ST_STATUS.".status_name,";
        $fields = TABLE_LD_TICKETS.".*,";
        //$fields .= TABLE_SETTINGS_DEPARTMENT.".department_name,";
        //$fields .= TABLE_ST_PRIORITY.".priority_name,";
        $fields .= TABLE_SALE_LEADS.".f_name,".TABLE_SALE_LEADS.".l_name,".TABLE_SALE_LEADS.".company_name,".TABLE_SALE_LEADS.".org,".TABLE_SALE_LEADS.".email,".TABLE_SALE_LEADS.".mobile1,".TABLE_SALE_LEADS.".mobile2";
        LeadsTicket::getList( $db, $list,$fields, $condition_query, $next_record, $rpp);
        
        $ticketStatusArr = LeadsTicket::getTicketStatus();
        $ticketStatusArr = array_flip($ticketStatusArr);
        $ticketRatingArr = LeadsTicket::getRating();
        $ticketRatingArr = array_flip($ticketRatingArr);
      
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                //$val['ticket_department']    = $val['department_name'] ;     
                //$val['ticket_status']    = $val['status_name'] ;     
                //$val['ticket_priority']    = $val['priority_name'] ;   
                if(isset($val['ticket_status'])){
                    $val['ticket_status_name'] = $ticketStatusArr[$val['ticket_status']];  
                }
                if(!empty($val['ticket_rating'])){
                    $val['ticket_rating_name'] = $ticketRatingArr[$val['ticket_rating']];  
                }
                $val['ticket_date'] = date("d M Y H:i:s",$val['ticket_date']);
                $executive=array();
                $string = str_replace(",","','", $val['assign_members']);
                $fields4 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                $condition4 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                User::getList($db,$executive,$fields4,$condition4);
                $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['assign_members'] = $executivename ; 
                 
                $db2 = new db_local ;
                
                $query = "SELECT COUNT(*) AS replies FROM ". TABLE_LD_TICKETS
                                    ." WHERE ". TABLE_LD_TICKETS .".ticket_child ='". $val['ticket_id'] ."' " ; 
                $db2->query($query) ;
                $db2->next_record() ;
                $ticket_replies = $db2->f("replies") ;
                $val['ticket_replied']    = $ticket_replies ;                
                $fList[$key]=$val;
            }
        }
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_view_details']  = false;
        $variables['can_view_client_details']  = true;
        $variables['can_update_status'] = false;
        
        if ( $perm->has('nc_ldt_add') ) {
            $variables['can_add'] = true;
        }
        
        if ( $perm->has('nc_ldt_details') ) {
            $variables['can_view_details'] = true;
        }
        /*
		if ( $perm->has('nc_uc_contact_details') ) {
            $variables['can_view_client_details'] = true;
        }*/
        if ( $perm->has('nc_ldt_list') ) {
            $variables['can_view_list'] = true;
        }
        
        if ( $perm->has('nc_ldt_update_status') ) {
            $variables['can_update_status'] = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        $page["var"][] = array('variable' => 'my_team_list', 'value' => 'my_team_list');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-ticket-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>