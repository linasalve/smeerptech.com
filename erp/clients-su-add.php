<?php
	if ( $perm->has('nc_c_su_add') ) {    
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/DomainAccess.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
    	include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
        include ( DIR_FS_INCLUDES .'/sms-client-account.inc.php');
        include_once ( DIR_FS_INCLUDES .'/client-domains.inc.php');
		
		$condition_query = " WHERE ".TABLE_CLIENT_DOMAINS.".client='".$client_id."'";
		$fields1 = "*";
		ClientDomains::getDetails( $db, $domainlist1, $fields1, $condition_query);

        $_ALL_POST	= NULL;
        $data       = NULL;
		//$region   = new Region('91');
        $region     = new Region('91','21','231');
        $phone      = new Phone(TABLE_CLIENTS);
        $domain_acc = new DomainAccess(TABLE_CLIENTS);
        $reminder   = new UserReminder(TABLE_CLIENTS);
        
		$lst_sms_acc = null;
        $fields = TABLE_SMS_CLIENT_ACCOUNT.'.name as account_name'
				.','.TABLE_SMS_CLIENT_ACCOUNT .'.id as account_id'
				.','.TABLE_CLIENTS .'.user_id'.','.TABLE_CLIENTS .'.f_name'.','.TABLE_CLIENTS .'.l_name';
        $condition_query1 = " LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id = ".TABLE_SMS_CLIENT_ACCOUNT.".client_id 
						WHERE ".TABLE_SMS_CLIENT_ACCOUNT.".client_id = '".$client_id."'";
        SmsClientAccount::getList($db,$lst_sms_acc,$fields,$condition_query1);
		 
		
		
        if( $perm->has('nc_uc_add_al') ){
            $al_list    = getAccessLevel($db, $my['access_level']);
            $role_list  = Clients::getRoles($db, $my['access_level']);
        }else{
            $al_list    = getAccessLevel($db, ($my['access_level']-1));
            $role_list  = Clients::getRoles($db, ($my['access_level']-1));
        }
        //grade_list  = Clients::getGrades();
        $memberTypeList  = Clients::getMemberType();
         //For Clients list allotment
        if ( $perm->has('nc_uc') && $perm->has('nc_uc_list')) {
            $variables['can_view_clist']   = true;
        }
        
        //BOF read the available services
		$authorization_list  = Clients::getAuthorization();
        $services_list  = Clients::getServices();
        //EOF read the available services
        
        //BOF read the available industries
        /*
		$industry_list = NULL;
        $required_fields ='*';
        $condition_query = " WHERE status='".Industry::ACTIVE."'";
		Industry::getList($db,$industry_list,$required_fields,$condition_query);
		*/
        //EOF read the available industries
		
        //BOF read the available industries
		Clients::getCountryCode($db,$country_code);
        //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
			 
            $extra = array( 'db' 			=> &$db,
                            'messages'  	=> $messages,
                            'al_list'   	=> $al_list,
                            //'role_list' 	=> $role_list,
                            'region'   	 	=> $region,
                            'phone'     	=> $phone,
                            'domain_acc'    => $domain_acc,
                            'reminder'  => $reminder
                        );
            
            if ( Clients::validateSubUserAdd($data, $extra) ) {
				$sms_accounts ='';
				if(!empty($data['sms_accounts'])){  
					$sms_accounts = implode(',',$data['sms_accounts']);
					$sms_accounts = ','.$sms_accounts.',';
				}
                $service_ids = $data['service_id'];
                $service_idstr = implode(",",$service_ids);
                
                if(!empty($service_idstr)){
                    $service_idstr=",".$service_idstr.",";	
                }
                $authorization_ids=$_POST['authorization_id'];
                $authorization_idstr=implode(",",$authorization_ids);                
                if(!empty($authorization_idstr)){
                    $authorization_idstr=",".$authorization_idstr.",";	
                }
				$membertype_ids = $_POST['member_type'];
                $membertypestr = implode(",",$membertype_ids);
                if(!empty($membertypestr)){
                    $membertypestr = ",".$membertypestr.",";
                }
                /*
				$industry_ids = $_POST['industry'];
                $industry_idstr = implode(",",$industry_ids); 
                if(!empty($industry_idstr)){
                    $industry_idstr=",".$industry_idstr.",";
                }
                $alloted_clientsstr='';
                $alloted_clients = $_POST['alloted_clients'];
                $alloted_clientsstr = implode(",",$alloted_clients);
               
                if(!empty($alloted_clientsstr)){
                    $alloted_clientsstr=",".$alloted_clientsstr.",";
                }
				*/
                //Default account manager aryan sir
                //$data['team']='e68adc58f2062f58802e4cdcfec0af2d';
                if( isset($data['check_email'])){
					$data['check_email'] = 1;
				}else{
					$data['check_email'] = 0;
				}
                
                 $query	= " INSERT INTO ". TABLE_CLIENTS
                    ." SET ". TABLE_CLIENTS .".user_id     	= '". $data['user_id'] ."'"
					.",". TABLE_CLIENTS .".parent_id    = '". $client_id ."'"
					.",". TABLE_CLIENTS .".service_id   = '". $service_idstr ."'"
					.",". TABLE_CLIENTS .".authorization_id   = '". $authorization_idstr ."'"
					.",". TABLE_CLIENTS .".sms_accounts   = '". $sms_accounts ."'"
					.",". TABLE_CLIENTS .".member_type   = '". $membertypestr ."'"
					.",". TABLE_CLIENTS .".enable_login  = '". $data['enable_login'] ."'"
					.",". TABLE_CLIENTS .".number      	 = '". $data['number'] ."'"
					//.",". TABLE_CLIENTS .".manager 	   	= '". $data['manager'] ."'"
					//.",". TABLE_CLIENTS .".team			= '". implode(',', $data['team']) ."'"
					.",". TABLE_CLIENTS .".username    	= '". $data['username'] ."'"
					.",". TABLE_CLIENTS .".password    	= '". encParam($data['password']) ."'"
					//.",". TABLE_CLIENTS .".access_level	= '". $data['access_level'] ."'"
					.",". TABLE_CLIENTS .".created_by	= '". $my['uid'] ."'"
					//.",". TABLE_CLIENTS .".roles       	= '". $data['roles'] ."'"
					.",". TABLE_CLIENTS .".email       	= '". $data['email'] ."'"
					.",". TABLE_CLIENTS .".check_email = '". $data['check_email'] ."'"
					.",". TABLE_CLIENTS .".email_1     	= '". $data['email_1'] ."'"
					.",". TABLE_CLIENTS .".email_2     	= '". $data['email_2'] ."'"
					.",". TABLE_CLIENTS .".title       	= '". $data['title'] ."'"
					.",". TABLE_CLIENTS .".f_name      	= '". $data['f_name'] ."'"
					.",". TABLE_CLIENTS .".m_name      	= '". $data['m_name'] ."'"
					.",". TABLE_CLIENTS .".l_name      	= '". $data['l_name'] ."'"
					.",". TABLE_CLIENTS .".p_name      	= '". $data['p_name'] ."'"
					//.",". TABLE_CLIENTS .".grade      	= '". $data['grade'] ."'"
					//.",". TABLE_CLIENTS .".credit_limit = '". $data['credit_limit'] ."'"
					.",". TABLE_CLIENTS .".desig       	= '". $data['desig'] ."'"
					.",". TABLE_CLIENTS .".org         	= '". $data['org'] ."'"
					.",". TABLE_CLIENTS .".domain      	= '". $data['domain'] ."'"
					.",". TABLE_CLIENTS .".gender      	= '". $data['gender'] ."'"
					.",". TABLE_CLIENTS .".do_birth    	= '". $data['do_birth'] ."'"
					.",". TABLE_CLIENTS .".do_aniv     	= '". $data['do_aniv'] ."'"
					.",". TABLE_CLIENTS .".remarks     	= '". $data['remarks'] ."'"
					.",". TABLE_CLIENTS .".special_remarks 	= '". $data['special_remarks'] ."'"
					.",". TABLE_CLIENTS .".payment_remarks 	= '". $data['payment_remarks'] ."'"
					.",". TABLE_CLIENTS .".search_keywords 	 = '". $data['search_keywords'] ."'"
					.",". TABLE_CLIENTS .".company_details 	 = '". $data['company_details'] ."'"
					.",". TABLE_CLIENTS .".connected_via 	 = '". $data['connected_via'] ."'"
					.",". TABLE_CLIENTS .".mobile1      = '". $data['mobile1'] ."'"
					.",". TABLE_CLIENTS .".mobile2      = '". $data['mobile2'] ."'"
					.",". TABLE_CLIENTS .".allow_ip       = '". $data['allow_ip'] ."'"
					.",". TABLE_CLIENTS .".valid_ip       = '". $data['valid_ip'] ."'"
					.",". TABLE_CLIENTS .".do_add      	= '". date('Y-m-d H:i:s', time()) ."'"
					.",". TABLE_CLIENTS .".status      	= '". $data['status'] ."'" ;
                            
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $variables['hid'] = $data['user_id'];
					
					//Push email id into Newsletters Table BOF
					$table = TABLE_NEWSLETTERS_MEMBERS;
					include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
					if(!empty($data['email'])){
						$domain1 = strtolower(strstr($data['email'], '@'));
						$condi1= " WHERE email='".strtolower(trim($data['email']))."'";
						if( $domain1!= 'smeerptech.com' && 
							!NewslettersMembers::duplicateFieldValue($db, $table, 'email',$condi1) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email']))."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_1'])){
						$domain2 = strtolower(strstr($data['email_1'], '@'));
						$condi2= " WHERE email='".strtolower(trim($data['email_1']))."'";
						if( $domain2!='smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email',$condi2) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_1']))."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_2'])){
						$domain3 = strtolower(strstr($data['email_2'], '@'));
						$condi3= " WHERE email='".strtolower(trim($data['email_2']))."'";
						if($domain3!= 'smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi3) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_2']))."'" ;
						  $db->query($sql1);
						}
					}
					//Push email id into Newsletters Table EOF
					
                    // Insert the address.
                    for( $i=0; $i<$data['address_count']; $i++ ){ 
                        $address_arr = array('address_type' => $data['address_type'][$i],
                                            'company_name'  => '',
                                            'address'       => $data['address'][$i],
                                            'city'          => $data['city'][$i],
                                            'state'         => $data['state'][$i],
                                            'country'       => $data['country'][$i],
                                            'zipcode'       => $data['zipcode'][$i],
                                            'is_preferred'  => $data['is_preferred'][$i],
                                            'is_verified'   => $data['is_verified'][$i]
                                            );
                        
                        if ( !$region->update($variables['hid'], TABLE_CLIENTS, $address_arr) ) {
                            foreach ( $region->getError() as $errors) {
                                $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                            }
                        }
                    }
                    
					// Insert the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {
                            $phone_arr = array( 'p_type'        => $data['p_type'][$i],
                                                'cc'            => $data['cc'][$i],
                                                'ac'            => $data['ac'][$i],
                                                'p_number'      => $data['p_number'][$i],
                                                'p_is_preferred'=> $data['p_is_preferred'][$i],
                                                'p_is_verified' => $data['p_is_verified'][$i]
                                                );
                            if ( !$phone->update($db, $variables['hid'],  $phone_arr) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
					
                    // Insert the Domain
                    for ( $i=0; $i<$data['domain_count']; $i++ ) {
                        if ( !empty($data['domain_id'][$i]) ) {
							$dmn_arr = array();
                            $dmn_arr = array( 'domain_id'        	=> $data['domain_id'][$i],
                                              'excluded_emails'    => $data['excluded_emails'][$i],
                                              'cda_id'           	=> $data['cda_id'][$i] 
                                            );
                            if ( ! $domain_acc->update($db, $variables['hid'], $dmn_arr) ) {
                                foreach ( $domain_acc->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    if($_ALL_POST['send_service_mail']=='1'){
						/*SEND SERVICE PDF BOF*/
						include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
						include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
						include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
						
						$id=39; 
						$fields = TABLE_ST_TEMPLATE .'.*'  ;            
						$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
						SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1);
						
						if(!empty($details)){
							$data['ticket_owner_uid']=$client_id;
							$table = TABLE_CLIENTS;
							$fields1 =  TABLE_CLIENTS .'.billing_name,'.TABLE_CLIENTS.'.f_name,
							'.TABLE_CLIENTS.'.l_name ';
							$condition1 = " WHERE ".TABLE_CLIENTS .".user_id = '".$client_id."' " ;
							$arr = getRecord($table,$fields1,$condition1);
							if(!empty($arr)){
								$data['ticket_owner'] = $arr['f_name']." ".$arr['l_name'];
							}						 
							$details = $details[0];
							$data['template_id'] = $details['id'];
							$data['template_file_1'] = $details['file_1'];
							$data['template_file_2'] = $details['file_2'];
							$data['template_file_3'] = $details['file_3'];
							$data['subject'] = processUserData($details['subject']);
							$data['text'] = processUserData($details['details']);
							
							$data['display_name']=$data['display_user_id']= $data['display_designation']='';				
							$randomUser = getCeoDetails();
							$data['display_name'] = $randomUser['name'];
							$data['display_user_id'] = $randomUser['user_id'];
							$data['display_designation'] = $randomUser['desig'];
							
							$ticket_no  =  SupportTicket::getNewNumber($db);
							$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
							
							$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
							. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_ST_TICKETS .".template_id       = '". $data['template_id'] ."', "
							. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_ST_TICKETS .".ticket_subject    = '". $data['subject'] ."', "
							. TABLE_ST_TICKETS .".ticket_text       = '". $data['text'] ."', "
							. TABLE_ST_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
							. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
							. TABLE_ST_TICKETS .".ticket_child      = '0', "
							. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_ST_TICKETS .".min = '". $data['min']."', "
							. TABLE_ST_TICKETS .".service_pdf_tkt = '1', "
							. TABLE_ST_TICKETS .".ticket_response_time = '0', "
							. TABLE_ST_TICKETS .".ticket_replied        = '0', "
							. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
							. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
							. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
							. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
							. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
							. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ;
							
							if(!empty($data['template_file_1'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
							}
							if(!empty($data['template_file_2'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
							}
							if(!empty($data['template_file_3'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
							}
							if(!empty($data['email']) || !empty($data['email_1']) || !empty($data['email_2'])){
								$db->query($query) ;
								$ticket_id = $db->last_inserted_id() ;
								$mail_send_to_su='';
								$data['mticket_no']   =   $ticket_no ;
								if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ){
									$to = '';
									if(!empty($data['email'])){ 
										$to[]   = array('name' => $data['email'] , 'email' => 
										$data['email']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email'];
										/* SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
									if(!empty($data['email_1'])){
										//$to = '';
										$to[]   = array('name' => $data['email_1'] , 'email' => 
										$data['email_1']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email_1'];
										/* SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
									if(!empty($data['email_2'])){
										//$to = '';
										$to[]   = array('name' => $data['email_2'] , 'email' => 
										$data['email_2']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email_2'];
										/* SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
								 
									if(!empty($to)){
										//$to = '';
										$bcc[]  = array('name' => $data['ticket_owner'] , 'email' => 
										$smeerp_client_email);        
										SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
										//echo  $email["body"];
									}
								}
								
								if(!empty($mail_send_to_su)){
									$query_update = "UPDATE ". TABLE_ST_TICKETS 
										." SET ". TABLE_ST_TICKETS .".mail_send_to_su
										= '".trim($mail_send_to_su,",")."'"                                   
										." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
									$db->query($query_update) ;
								}
							}
							
						}
						/*SEND SERVICE PDF EOF*/
                    }
                    $messages->setOkMessage("The New Client has been added.");
                    
                }
				
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
         
        }else{
			$_ALL_POST['p_number'] = array('0'=>'');
			$_ALL_POST['domain']   = array('0'=>'');
            $_ALL_POST['address'] = array('0'=>'');
            $_ALL_POST['reminder_list'] = array(0=>'',1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'');
			 
            $_ALL_POST['cc'][0]=91;
            $_ALL_POST['cc'][1]=91;
            $_ALL_POST['cc'][2]=91;
            $_ALL_POST['service_id'][0]=Clients::ST;
			$_ALL_POST['send_service_mail']=1;
        }
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/clients-su.php?perform=add&added=1&client_id=".$client_id);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/clients-su.php?client_id=".$client_id);
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        /*
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/clients-list.php');
        }*/
        if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            header("Location:".DIR_WS_NC."/clients-su.php?perform=list&added=1&client_id=".$client_id);
            
        }
        else {
            //$_ALL_POST['number'] = Clients::getNewAccNumber($db);
             $_ALL_POST['reminder_list']=array(0=>'',1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'');
			
			if ( empty($data['country']) ) {
                $data['country'] = '91';
            }
            if(empty($data['state'])){
                $data['state']='';
            }
            
            if(empty($data['city'])){
                $data['city']='';
            }
            $member_type='';
            $condition_query = " WHERE user_id = '". $client_id ."' ";
            if ( Clients::getList($db, $_ALL_POST['main_client_details'], 'number,service_id,member_type', $condition_query) > 0 ) {
                $_ALL_POST['main_client_details'] = $_ALL_POST['main_client_details'][0];
                $_ALL_POST['number'] = $_ALL_POST['main_client_details']['number'];
                $member_type = $_ALL_POST['main_client_details']['member_type'];
                //$_ALL_POST['service_id'] = $_ALL_POST['main_client_details'][0]['service_id'];
                $_ALL_POST['main_client_details']['service_id'] = trim($_ALL_POST['main_client_details']['service_id'],",");
                $my_services_list = explode(",",$_ALL_POST['main_client_details']['service_id']);
            }
              
			
			
            $lst_country=$lst_state= $lst_city=array();
			$phone_types    = $phone->getTypeList();
            $address_types  = $region->getTypeList();
            $lst_country    = $region->getCountryList($data['country']);
            $lst_state      = $region->getStateList($data['state']);
            $lst_city       = $region->getCityList($data['city']);
             
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            //print_r($_ALL_POST);
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'reminder_count', 'value' => '6');
            $hidden[] = array('name'=> 'client_id', 'value' => $client_id);
            $hidden[] = array('name'=> 'member_type', 'value' => $member_type);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			
			$page["var"][] = array('variable' => 'domainlist1', 'value' => 'domainlist1');
			$page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
			$page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
            $page["var"][] = array('variable' => 'memberTypeList', 'value' => 'memberTypeList');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            // $page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
            //$page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
            $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
			$page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
            $page["var"][] = array('variable' => 'my_services_list', 'value' => 'my_services_list');
            $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
            $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
            $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
            $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
            $page["var"][] = array('variable' => 'allowipArr', 'value' => 'allowipArr');
			$page["var"][] = array('variable' => 'lst_sms_acc', 'value' => 'lst_sms_acc');
            //$page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-su-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>