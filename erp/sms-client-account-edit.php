<?php
if ( $perm->has('nc_sms_ca_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $lst_api = $lst_sender = array();
        
       /*  $fields = TABLE_SMS_API .'.id'
                   .','. TABLE_SMS_API .'.name'
                   .','. TABLE_SMS_API .'.gateway_name'
                   .','. TABLE_SMS_API .'.is_scrubbed';
        $condition_query= "WHERE status='".SmsApi::ACTIVE."'";
        SmsApi::getList($db,$lst_api,$fields,$condition_query); */
		
		
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
            if(!empty($data['api_id'])){   
                
				$lst_api=null;
                $fields = TABLE_SMS_API .'.id'
                   .','. TABLE_SMS_API .'.name'
                   .','. TABLE_SMS_API .'.gateway_id'
                   .','. TABLE_SMS_API .'.gateway_name'
                   .','. TABLE_SMS_API .'.is_scrubbed';
                $condition_query0 = " WHERE status='".SmsApi::ACTIVE."' AND id='".$data['api_id']."'";
                SmsApi::getList($db,$lst_api,$fields,$condition_query0); 
				if(!empty($lst_api)){
					$gateway_id= $lst_api[0]['gateway_id'];
				}
				 					
				$fields0 = TABLE_SMS_SENDER .'.id'.','. TABLE_SMS_SENDER .'.sender_name' ;
				$condition_query0 = " WHERE status='".SmsSender::ACTIVE."' AND api_id='".$data['api_id']."'";
				SmsSender::getList($db,$lst_sender,$fields0,$condition_query0);  
			}
			if(!empty($data['sender_id'])){  
				$sender_id = implode(',',$data['sender_id']);
				$sender_id = ','.$sender_id.',';
			}
			
			
            if ( SmsClientAccount::validateUpdate($data, $extra) ) {
				//get details of current record
				
				$fields = TABLE_SMS_CLIENT_ACCOUNT .'.*'  ;            
				$condition_query = " WHERE (". TABLE_SMS_CLIENT_ACCOUNT .".id = '". $id ."' )"; 
				if ( SmsClientAccount::getDetails($db, $details, $fields, $condition_query) > 0 ) {
					$details = $details['0'];
				}
				if(!empty($data['credit_sms'])){
					$old_unit_price = $details['unit_price'] ;
					$balance_sms = $details['balance_sms'] + $data['credit_sms'];
					$credit_sms = $details['credit_sms'] + $data['credit_sms'];
					$sql1=  ",". TABLE_SMS_CLIENT_ACCOUNT .".credit_sms =  '". $credit_sms ."'
							,". TABLE_SMS_CLIENT_ACCOUNT .".balance_sms =  '". $balance_sms ."'					
						" ;
				} 
				
                $query  = " UPDATE ". TABLE_SMS_CLIENT_ACCOUNT
                            ." SET "."". TABLE_SMS_CLIENT_ACCOUNT .".name 	        = '". $data['name'] ."'" 
							.",". TABLE_SMS_CLIENT_ACCOUNT .".sender_id 		= '". $sender_id ."'"  
							//.",". TABLE_SMS_CLIENT_ACCOUNT .".unit_price 	= '". $data['unit_price'] ."'"                   
							.$sql1
							//.",". TABLE_SMS_CLIENT_ACCOUNT .".display_name 	= '". $data['display_name'] ."'"                    
							//.",". TABLE_SMS_CLIENT_ACCOUNT .".mobile_no 	    = '". $data['mobile_no'] ."'"                    
							//.",". TABLE_SMS_CLIENT_ACCOUNT .".do_expiry 		= '". date('Y-m-d H:i:s',$data['do_expiry'])."'"                    
							." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Sms Account has been updated successfully.");
                    $variables['hid'] = $id;					
					//Update in clients account. Deduct credit from client's balance sms
					if(!empty($data['credit_sms'])){ 
					
						$query = " INSERT INTO ".TABLE_SMS_CLIENT_ACCOUNT_HISTORY
						." SET ".TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".sms_count 	= '".$data['credit_sms']."'"  
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".credit_account_id 	= '". $id."'"  
						//.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".credit_do_expiry 	= '". $allot_acct['do_expiry'] ."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".credit_total 	= '". $credit_sms ."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".credit_balance 	= '". $balance_sms ."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".deduct_account_id 	= ''" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".deduct_do_expiry 	= ''" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".deduct_total 	= ''" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".deduct_balance 	= ''" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".created_by		= '". $my['user_id']."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".created_by_name = '". $my['f_name']." ".$my['l_name'] ."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".ip 	= '". $_SERVER['REMOTE_ADDR'] ."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".do_e      = '". date('Y-m-d H:i:s')."'";      
						$db->query($query);			
						
						
						$sql1 = " UPDATE ". TABLE_CLIENTS ." SET
						". TABLE_CLIENTS .".balance_sms = (".TABLE_CLIENTS .".balance_sms - ". $data['credit_sms'] ."),
						". TABLE_CLIENTS .".consumed_sms = (".TABLE_CLIENTS .".consumed_sms + ". $data['credit_sms'] .") 							
						WHERE ".TABLE_CLIENTS.".user_id = '".$_ALL_POST['client_id']."'" ;
						$db->query($sql1);
					} 
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SMS_CLIENT_ACCOUNT .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SMS_CLIENT_ACCOUNT .".id = '". $id ."' )";
            
            if ( SmsClientAccount::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
				$_ALL_POST['client_id'] = $_ALL_POST['client_id'];
                $condition_querye=" WHERE "." user_id='".$_ALL_POST['client_id']."'";
                Clients::getList( $db, $edetails, 'f_name,l_name,number,email,balance_sms,do_expiry', $condition_querye);
                $edetails = $edetails[0];                
                $_ALL_POST['client_details'] = $edetails['f_name']." ".$edetails['l_name']." (".$edetails['number'].") Bal. Sms - ".$edetails['balance_sms']." Dt of expiry - ".$edetails['do_expiry'] ;
				
				$lst_api=null;
                $fields = TABLE_SMS_API .'.id'
                   .','. TABLE_SMS_API .'.name'
                   .','. TABLE_SMS_API .'.gateway_name'
                   .','. TABLE_SMS_API .'.is_scrubbed';
                $condition_query = " WHERE status='".SmsApi::ACTIVE."' AND id='".$_ALL_POST['api_id']."'";
                SmsApi::getList($db,$lst_api,$fields,$condition_query);  
				$_ALL_POST['sender_id'] = explode(',',trim($_ALL_POST['sender_id'],',')) ;
				$_ALL_POST['credit_sms'] = '';  
				$fields0 = TABLE_SMS_SENDER .'.id'.','. TABLE_SMS_SENDER .'.sender_name' ;
				$condition_query0 = " WHERE status='".SmsSender::ACTIVE."' AND api_id='".$_ALL_POST['api_id']."'";
				SmsSender::getList($db,$lst_sender,$fields0,$condition_query0);  
                // Setup the date of delivery.
                $id         = $_ALL_POST['id'];
                $_ALL_POST['do_expiry']  = explode(' ', $_ALL_POST['do_expiry']);
                $temp               = explode('-', $_ALL_POST['do_expiry'][0]);
                $_ALL_POST['do_expiry']  = NULL;
                $_ALL_POST['do_expiry']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0]; 
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
		if ( isset($_POST['btnCancel']) && $messages->getErrorMessageCount() <= 0 ){
			header("Location:".DIR_WS_NC."/sms-client-account.php?perform=list");
		}
        if((isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $messages->getErrorMessageCount() <= 0 ){
            $variables['hid'] = $id;
            $condition_query='';
            header("Location:".DIR_WS_NC."/sms-client-account.php?perform=edit&updated=1&id=".$id);
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'lst_api', 'value' => 'lst_api');     
            $page["var"][] = array('variable' => 'lst_sender', 'value' => 'lst_sender');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-client-account-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
