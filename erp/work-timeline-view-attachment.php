<?php
    $file_name = isset ($_GET['file_name']) ? $_GET['file_name'] : ( isset($_POST['file_name'] ) ? $_POST['file_name'] :'');
   
    
    $filedata = pathinfo($file_name);                       
    $type = $filedata["extension"] ;
   
  
    if(array_key_exists($type,$mime_type)){
        $content_type = $mime_type[$type];
        $file_path = DIR_FS_ATTACHMENTS_FILES ."/";
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); // required for certain browsers 
        header("Content-type: $content_type");
        header("Content-Disposition: attachment; filename=". $file_name );
        header("Content-Length: ".filesize($file_path.$file_name));
        readfile($file_path.$file_name);
    
    }else{
       echo('<script language="javascript" type="text/javascript">alert("Invalid File specified");</script>');
    }
      
    exit;
?>