<?php

	$condition_query= '';
    $condition_url  = '';
    $where_added = false;
    $searchStr = 0;
    if ( $sString != "" ) {
	    $searchStr = 1;
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
		if ( $search_table != "Any" && $search_table != NULL) {
            if($where_added){
                 $condition_query .= " AND  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            }else{
                $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
                $where_added = true;
            }
           
			$condition_url .= "&sString=". $sString;
			$condition_url .= "&sType=". $sType;
			
		}
		elseif ( $search_table == "Any" ) {
           
            if($where_added){
               
               $condition_query .= " AND (";
               
            }else{                
                $condition_query .= " WHERE (";
            }
            
			foreach( $sTypeArray as $table => $field_arr ) {
				if ( $table != "Any" ) {
					foreach( $field_arr as $key => $field ) {
                        $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
						$where_added = true;
					}
				}
			}
			$condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
			$condition_query .= ") ";
		}
		$condition_url .= "&sString=". $sString;
		$condition_url .= "&sType=". $sType;
			
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}
    // BO: Member type data.
    $mTypeStr = '';
    $chk_mtype = isset($_POST["chk_mtype"])  ? $_POST["chk_mtype"]  : (isset($_GET["chk_mtype"])   ? $_GET["chk_mtype"]   :'');
    $mType    = isset($_POST["mType"])      ? $_POST["mType"]     : (isset($_GET["mType"])      ? $_GET["mType"]      :'');
    
	if ( ($chk_mtype == 'AND' || $chk_mtype == 'OR')  && isset($mType)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_mtype;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        if(is_array($mType)){
			$str = '';
			foreach($mType as $key=>$val ){
				$str.= TABLE_CLIENTS .".member_type LIKE '%,".$val.",%' AND " ;
			}
			$str = substr( $str, 0, strlen( $str ) - 4 );
            $mTypeStr1 = implode(",", $mType) ;
             
        }else{
            $mTypeStr1 = $mType ;
            $mType = explode(",",$mType) ;
			$str = '';
			foreach($mType as $key=>$val ){
				$str.= TABLE_CLIENTS .".member_type LIKE '%,".$val.",%' AND " ;
			}
			$str = substr( $str, 0, strlen( $str ) - 4 );
        }
        $condition_query .= " ( ".$str." ) ";
        $condition_url          .= "&chk_mtype=$chk_mtype&mType=$mTypeStr1";
        $_SEARCH["chk_mtype"]  = $chk_mtype;
        $_SEARCH["mType"]     = $mType;        
    }
		
    // BO: Status data.
    $sStatusStr = '';
    $chk_status = isset($_POST["chk_status"]) ? $_POST["chk_status"]:(isset($_GET["chk_status"])? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])  ? $_POST["sStatus"]: (isset($_GET["sStatus"]) ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR')  && isset($sStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }        
        
        $condition_query .= " ". TABLE_CLIENTS .".status IN ('". $sStatusStr ."') ";        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
        
    }
	// BO: Grade data.
    $sGradeStr = '';
    $chk_grade = isset($_POST["chk_grade"]) ? $_POST["chk_grade"]:(isset($_GET["chk_grade"])? $_GET["chk_grade"]   :'');
    $sGrade    = isset($_POST["sGrade"])  ? $_POST["sGrade"]: (isset($_GET["sGrade"]) ? $_GET["sGrade"]      :'');
    if ( ($chk_grade == 'AND' || $chk_grade == 'OR')  && isset($sGrade)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_grade;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        if(is_array($sGrade)){
             $sGradeStr = implode("','", $sGrade) ;
             $sGradeStr1 = implode(",", $sGrade) ;
             
        }else{
            $sGradeStr = str_replace(',',"','",$sGrade);
            $sGradeStr1 = $sStatus ;
            $sGrade = explode(",",$sGrade) ;
            
        }        
        
        $condition_query .= " ". TABLE_CLIENTS .".grade IN ('". $sGradeStr ."') ";        
        $condition_url          .= "&chk_grade=$chk_grade&sGrade=$sGradeStr1";
        $_SEARCH["chk_grade"]  = $chk_grade;
        $_SEARCH["sGrade"]     = $sGrade;
    }
	
	//Client serach bof
	$search_client = isset($_POST["search_client"])   ? $_POST["search_client"]  : (isset($_GET["search_client"])   ? $_GET["search_client"]   :'');
	$search_client_details = isset($_POST["search_client_details"])   ? $_POST["search_client_details"]  : (isset($_GET["search_client_details"])   ? $_GET["search_client_details"]   :'');
	if(!empty($search_client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " (". TABLE_CLIENTS .".user_id ='". $search_client ."' OR ". TABLE_CLIENTS .".parent_id ='". $search_client ."')";        
        $condition_url          .= "&search_client_details=$search_client_details&search_client=$search_client";
        $_SEARCH["search_client_details"]  = $search_client_details;
        $_SEARCH["search_client"]     = $search_client;   
	}
	//Client serach eof
	
	$city = isset($_POST["city"])   ? $_POST["city"]  : (isset($_GET["city"])   ? $_GET["city"]   :'');
	 
	if(!empty($city)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " (". TABLE_ADDRESS .".city ='". $city ."' )";        
        $condition_url          .= "&city=$city";
        $_SEARCH["city"]  = $city; 
	}
	
	$state = isset($_POST["state"])   ? $_POST["state"]  : (isset($_GET["state"])   ? $_GET["state"]   :'');
	 
	if(!empty($state)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " (". TABLE_ADDRESS .".state ='". $state ."' )";        
        $condition_url          .= "&state=$state";
        $_SEARCH["state"]  = $state; 
	}
	$country = isset($_POST["country"])   ? $_POST["country"]  : (isset($_GET["country"])   ? $_GET["country"]   :'');
	 
	if(!empty($country)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " (". TABLE_ADDRESS .".country ='". $country ."' )";        
        $condition_url          .= "&country=$country";
        $_SEARCH["country"]  = $country; 
	}
	$check_email = isset($_POST["check_email"])   ? $_POST["check_email"]  : (isset($_GET["check_email"])   ? $_GET["check_email"]   :'');
	if(!empty($check_email)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " (". TABLE_CLIENTS .".check_email ='1' )";        
        $condition_url          .= "&check_email=$check_email";
        $_SEARCH["check_email"]  = $check_email; 
	}
    // Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
    
    
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR' ) && !empty($date_from) ){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_CLIENTS .".".$dt_field." >= '". $dfa ."'";
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;      
    }  
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR')  && !empty($date_to)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_CLIENTS .".".$dt_field." <= '". $dta ."'";
        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }
    

    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
	$_SEARCH["sString"] 	= $sString ;
	$_SEARCH["sType"] 		= $sType ;
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/clients-all-list.php');
?>