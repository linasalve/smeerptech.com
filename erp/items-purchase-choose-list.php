<?php
        /* Get if anyone want to add executive in diff div ie other than 'add_executive_info' then pass div name here 
            By default div name is add_executive_info
        */
        $div	= isset($_GET["div"]) 	? $_GET["div"]	: 'item_info';
   
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
		if ( !isset($_SEARCH) ) {
			$_SEARCH = '';
		}
		$_SEARCH['searched'] = 1;
		
		//echo $condition_query;
		// To count total records.
		$list	= 	NULL;
		$total	=	ItemsPurchase::getDetails( $db, $list, '', $condition_query);
		
		$extra_url  = '';
		if ( isset($condition_url) && !empty($condition_url) ) {
			$extra_url  = $condition_url;
		}
		$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  = '&start=url'. $extra_url .'&end=url';
		
		$condition_url .="&rpp=".$rpp."&perform=".$perform;
		
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
		
		$list	= NULL;
		$fields = TABLE_ITEMS_PURCHASE.'.*';
		ItemsPurchase::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
		
		// Set the Permissions.
		$variables['INUSED'] = ItemsPurchase::INUSED ;
		$variables['can_view_list']     = false;
		$variables['can_add']           = false;
		$variables['can_edit']          = false;
		$variables['can_delete']        = false;
		$variables['can_change_status']  = false;
 
 
 
        $page["var"][] = array('variable' => 'div', 'value' => 'div');
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'items-purchase-choose-list.html');
  
?>