<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
     
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
    $condition_query='';
    $condition_url='';
    
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    //echo "added=>".$added;
    if($added){
         $messages->setOkMessage("Ticket has been created successfully. And email Notification has been sent.");
    }
    
    if ( $perm->has('nc_st_as') ) {

        $sTypeArray     = array('Any'        =>  array(  'Any of following' => '-1'),
                                 TABLE_ST_TICKETS    =>  array(  'Ticket no.'        => 'ticket_no',
                                                            'Subject'       => 'ticket_subject',
                                                            //'Priority'      => 'ticket_priority',
                                                            //'Department'    => 'ticket_department'
                                                            //'Status'        => 'ticket_status'
                                                        ),
                                 TABLE_CLIENTS    =>  array(  'Client First Name'      => 'f_name',
                                                              'Client Last Name'       => 'l_name',
                                                              'Client Account No.'     => 'number',
                                                              'Client Username.'       => 'username',
															  'Billing Name'       => 'billing_name'
                                                            )
                            );
        
        $sOrderByArray  = array(
                                TABLE_ST_TICKETS     => array('Ticket no.'  => 'ticket_no',
                                                     'Subject'       => 'ticket_subject',
                                                    //'Priority'      => 'ticket_priority',
                                                    //'Department'    => 'ticket_department'
                                                    //'Status'        => 'ticket_status'
                                                    )
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            //$_SEARCH['sOrderBy']= $sOrderBy = 'ticket_date';
            $_SEARCH['sOrderBy']= $sOrderBy = 'ticket_no';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_ST_TICKETS;
        }
        $variables['active']  = SupportTicket::ACTIVE;
        $variables['deactive']  = SupportTicket::DEACTIVE;
        $variables['closed']  = SupportTicket::CLOSED;
        
       //BOF read the available status
        $variables['status']=SupportTicket::getStatus();
        $variables['ticket_status']=SupportTicket::getTicketStatus();
        $variables['PENDINGWITHSMEERP']=SupportTicket::PENDINGWITHSMEERP;
        $variables['PENDINGWITHSMEERPCOZCLIENT']=SupportTicket::PENDINGWITHSMEERPCOZCLIENT;
       //BOF read the available status
        //SupportTicket::getStatus($db,$status);
        //EOF read the available status
        
        //BOF read the available priority
        //SupportTicket::getPriority($db,$priority);
        //EOF read the available priority
        
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('view'): {
                include (DIR_FS_NC.'/support-ticket-assign-view.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'support-ticket-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('search'): {
                include(DIR_FS_NC."/support-ticket-assign-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'support-ticket-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('mark-ct'): {
               
                include(DIR_FS_NC .'/support-ticket-assign-mark-ct.php');
				$page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
				break;
            }
           
            case ('list'):
            default: {
                include (DIR_FS_NC .'/support-ticket-assign-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'support-ticket-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'support-ticket-assign.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    //$s->assign("status", $status);
    //$s->assign("priority", $priority);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>