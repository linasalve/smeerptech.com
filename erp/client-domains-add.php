<?php
	$_ALL_POST      = NULL;
	$data           = NULL; 
	
	if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST 	= $_POST;
		$data		= processUserData($_ALL_POST);
				   
		$extra = array( 'db' 				=> &$db,
						'access_level'      => $access_level,
						'messages'          => &$messages
					);
			print_R($data);
		   
		if ( ClientDomains::validateAdd($data, $extra) ) { 
				 
				$query	= " INSERT INTO ".TABLE_CLIENT_DOMAINS
					." SET ".TABLE_CLIENT_DOMAINS .".domain = '".$data['domain'] ."'"  
						.",". TABLE_CLIENT_DOMAINS .".registered_with = '".$data['registered_with'] ."'"                            
						.",". TABLE_CLIENT_DOMAINS .".client = '".$data['client'] ."'"                            
						.",". TABLE_CLIENT_DOMAINS .".client_details = '".$data['client_details'] ."'"   
						.",". TABLE_CLIENT_DOMAINS .".status = '".$data['status'] ."'"                          
						.",". TABLE_CLIENT_DOMAINS .".created_by = '".	$my['uid'] ."'"                                
						.",". TABLE_CLIENT_DOMAINS .".created_by_name = '".$my['f_name']." ".$my['l_name']."'"                                
						.",". TABLE_CLIENT_DOMAINS .".ip = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
						.",". TABLE_CLIENT_DOMAINS .".do_e = '". 		date('Y-m-d')."'";
			
			if ( $db->query($query) && $db->affected_rows() > 0 ) {
				$messages->setOkMessage("New record has been done.");
				$variables['hid'] = $db->last_inserted_id();              
			}
			//to flush the data.
			$_ALL_POST	= NULL;
			$data		= NULL;
		}
	}               
	
	
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
		 header("Location:".DIR_WS_NC."/client-domains.php?perform=add&added=1");
	}
	if(isset($_POST['btnCancel'])){
		 header("Location:".DIR_WS_NC."/client-domains.php");
	}
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
		header("Location:".DIR_WS_NC."/client-domains.php?added=1");   
	}
	else {
	
		$hidden[] = array('name'=> 'perform' ,'value' => 'add');
		$hidden[] = array('name'=> 'act' , 'value' => 'save');           
		
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'client-domains-add.html');
	}
?>