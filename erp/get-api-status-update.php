<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
	include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
    
	$mail_to_su = isset($_GET['mail_to_su']) ? $_GET['mail_to_su'] : '';
    $mail_additional_email = isset($_GET['mail_additional_email']) ? $_GET['mail_additional_email'] : '';
    $mail_to_additional_email = isset($_GET['mail_to_additional_email']) ? $_GET['mail_to_additional_email'] : '0';
    $mail_to_all_su = isset($_GET['mail_to_all_su']) ? $_GET['mail_to_all_su'] : '';
    $form_type = isset($_GET['form_type']) ? $_GET['form_type'] : '';
    $data['email_domain'] = isset($_GET['email_domain']) ? $_GET['email_domain'] : '';
    $exchangeMaxNumMailboxes = isset($_GET['exchangeMaxNumMailboxes']) ? $_GET['exchangeMaxNumMailboxes'] : '';
    $exchangeUsedStorage = isset($_GET['exchangeUsedStorage']) ? $_GET['exchangeUsedStorage'] : '';
    $rsEmailMaxNumberMailboxes = isset($_GET['rsEmailMaxNumberMailboxes']) ? $_GET['rsEmailMaxNumberMailboxes'] : '';
    $rsEmailUsedStorage = isset($_GET['rsEmailUsedStorage']) ? $_GET['rsEmailUsedStorage'] : '';
    $data['ticket_owner_uid'] = isset($_GET['ticket_owner_uid']) ? $_GET['ticket_owner_uid'] : '';
    $data['ticket_owner'] = isset($_GET['ticket_owner']) ? $_GET['ticket_owner'] : '';
    $data['user_email1'] = isset($_GET['user_email1']) ? $_GET['user_email1'] : '';
    $user_email2 = isset($_GET['user_email2']) ? $_GET['user_email2'] : '';
    $quota_email = isset($_GET['quota_email']) ? $_GET['quota_email'] : '';
    
	
	
	$no1 = rand(10,99);
	$no2 = rand(10,99);
	$spec1 = randomString(1,'+#-=!$%@^&<*_>');
	$str1 = randomString(3);
	$str2 = randomString(3);
	$email_password = $str1.$no1.$spec1.$str2.$no2;
	$format = 'application/json';
	
   	$apiDetails = '' ;
    if (isset($form_type) && !empty($form_type)){
		
		if($form_type=='change_pwd'){ 
					
				$err_msg=$succ_msg=array();	
				if ( empty($data['user_email1']) ) {
					
					$err_msg[]='Please enter email';
				}
				if ( empty($data['email_domain']) ) {
					
					$err_msg[]='Please enter email domain';
				}
				$msg_count =count($msg);
				if ( $msg_count <= 0 ) {
					if(!empty($data['user_email1']) && !empty($data['email_domain'])){				
						$url_string="/customers/all/domains/".$data['email_domain']."/rs/mailboxes/".$data['user_email1'];
						$url = 'http://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
						$headers = array();
						$headers = array("Accept: $format");
						$curl_session = curl_init();
						curl_setopt($curl_session, CURLOPT_URL, $url);
						$time_stamp = date('YmdHis'); 
						$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
						$signature = base64_encode(sha1($data_to_sign, true));

						$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
						$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature"; 
						$fields =  array( 
							'password' => $email_password 
						);
						curl_setopt($curl_session, CURLOPT_CUSTOMREQUEST, 'PUT');
						curl_setopt($curl_session, CURLOPT_POSTFIELDS, $fields);
						curl_setopt($curl_session, CURLOPT_HEADER, true);
						curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);

						$httpResponse = curl_exec($curl_session); 	
						//$httpResponse = 'df fsdf sdfsfsdfdsf HTTP/1.1 200 OK';
						if(strpos($httpResponse, 'HTTP/1.1 200 OK')!==false){
							$ticket_no  =  SupportTicket::getNewNumber($db);
							$data['ticket_subject']	='Password Changed - '.$data['user_email1'].'@'.$data['email_domain'];
							$data['ticket_text']= "<div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:18px;\">Dear Sir/Madam, <br/> Please check new password as below:</div><div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:15px;\">
							Email: ".$data['user_email1']."@".$data['email_domain']." 
							New Password: ".$email_password."
							</div>";
							 
							$attachfilename=$ticket_attachment_path='';
							/*
							$mail_to_all_su=0;
							if(isset($data['mail_to_all_su']) ){
								$mail_to_all_su=1;
							}
							$mail_to_additional_email=0;
							if(isset($data['mail_to_additional_email']) ){
								$mail_to_additional_email=1;
							}
							$mail_client=0;
							if(isset($data['mail_client']) ){
								$mail_client=1;
							}
							$mail_ceo=0;
							if(isset($data['mail_ceo'])){
								$mail_ceo = 1;
							}
							*/
							//disply random name BOF 
							$data['display_name']=$data['display_user_id']=$data['display_designation']='';
							$randomUser = getRandomAssociate($data['executive_id']);
							$data['display_name'] = $randomUser['name'];
							$data['display_user_id'] = $randomUser['user_id'];
							$data['display_designation'] = $randomUser['desig'];					
							//disply random name EOF		
							/* 
							if(!isset($data['ticket_status']) ){                    
								$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
							}else{
								$ticket_status = $data['ticket_status'];
							} */
							$data['min']=5;
							$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
							$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
							$min1 = (int) ($data['min'] * HRS_FACTOR);  
							$sent_inv_id_str = $sent_inv_no_str ='';
							if(!empty($data['sent_inv_id'])){	
								foreach($data['sent_inv_id'] as $key1=>$val1){
									$sent_inv_id_str .= $key1."," ;
									$sent_inv_no_str .= $val1."," ;
								}		
								$sent_inv_id_str = ",".$sent_inv_id_str ;
								$sent_inv_no_str = ",".$sent_inv_no_str ;
							}
							$sent_pinv_id_str = $sent_pinv_no_str ='';
							if(!empty($data['sent_pinv_id'])){	
								foreach($data['sent_pinv_id'] as $key2=>$val2){
									$sent_pinv_id_str .= $key2."," ;
									$sent_pinv_no_str .= $val2."," ;
								}
								$sent_pinv_id_str = ",".$sent_pinv_id_str ;					
								$sent_pinv_no_str = ",".$sent_pinv_no_str ;
							}
							if(empty($data['ss_id'])){		
								$data['ss_title']='';
								$data['ss_file_1']='';
								$data['ss_file_2']='';
								$data['ss_file_3']='';
							}
						
							$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
							. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
							. TABLE_ST_TICKETS .".invoice_profm_id    = '". $invoice_profm_id ."', "
							. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
							. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_ST_TICKETS .".on_behalf        = '". $data['on_behalf'] ."', "
							. TABLE_ST_TICKETS .".template_id      = '". $data['template_id'] ."', "
							. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_ST_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
							. TABLE_ST_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
							. TABLE_ST_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
							. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
							. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
							. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
							. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
							. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
							. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
							. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
							. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
							. TABLE_ST_TICKETS .".ticket_creator_uid    = '". $my['uid'] ."', "
							. TABLE_ST_TICKETS .".ticket_creator         = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_ST_TICKETS .".ticket_subject    	 = '". $data['ticket_subject'] ."', "
							. TABLE_ST_TICKETS .".ticket_mail_subject    = '". $data['ticket_subject'] ."', "
							. TABLE_ST_TICKETS .".ticket_text         = '". $data['ticket_text'] ."', "
							. TABLE_ST_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
							. TABLE_ST_TICKETS.".last_comment_by 	  = '".$my['uid'] ."', "
							. TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
							//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
							//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
							//. TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
							. TABLE_ST_TICKETS .".mail_ceo        = '".$mail_ceo."', "
							. TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
							. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
							. TABLE_ST_TICKETS .".ticket_child      = '0', "
							. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
							. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_ST_TICKETS .".min = '". $data['min']."', "
							. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
							. TABLE_ST_TICKETS .".ticket_response_time = '0', "
							. TABLE_ST_TICKETS .".ticket_replied        = '0', "
							. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
							//. TABLE_ST_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
							. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
							. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
							. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
							. TABLE_ST_TICKETS .".is_private           = '". $data['is_private'] ."', "
							. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
							. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ; 
							//unset($GLOBALS["flag"]);
							//unset($GLOBALS["submit"]); 
							if ($db->query($query) && $db->affected_rows() > 0) { 
								$variables['hid'] = $db->last_inserted_id() ;
								$data['ticket_no']    =   $ticket_no ;
								$data['domain']   = THIS_DOMAIN;                        
								//$data['replied_by']   =   $data['ticket_owner'] ;
								$data['mticket_no']   =   $ticket_no ;
								$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
								$data['subject']    =     $data['ticket_subject'] ;
								$data['text']    =   $data['ticket_text'] ;
								$data['attachment']   =   $attachfilename ;
								$data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $variables['hid'];
								if ($data['is_private'] != 1){
									if(isset($mail_to_all_su) || isset($mail_to_su)){
										if(isset($mail_to_all_su)){
											$followupSql= "";
											if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
												$followupSql = " AND ( authorization_id  LIKE '%,".Clients::ACCOUNTS.",%' OR 
												authorization_id  LIKE '%,".Clients::MANAGEMENT.",%' OR
												authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%' ) " ;
											}
											Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, 
											email,email_1,email_2, email_3,email_4', " 
											WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."')
											AND status='".Clients::ACTIVE."' AND check_email='0' ".$followupSql."");
										}elseif( isset($mail_to_su) && !empty($mail_to_su) ){
											$mail_to_su = implode("','",array_unique($mail_to_su));  
											Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " WHERE user_id IN('".$mail_to_su."') AND status='".Clients::ACTIVE."' "); 
										}                                
										if(!empty($subUserDetails)){ 
											foreach ($subUserDetails  as $key=>$value){
												$data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
												$data['ctitle']    =   $subUserDetails[$key]['ctitle'];
												$email = NULL;
												$cc_to_sender= $cc = $bcc = Null;
												if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
													//$to = '';
													$to[]   = array('name' => $subUserDetails[$key]['email'], 
													'email' => $subUserDetails[$key]['email']);
													$from   = $reply_to = array('name' => $email["from_name"], 
													'email' => $email['from_email']);
													$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ; 
													if(!empty($subUserDetails[$key]['email_1'])){                                       
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email_1'], 
														'email' => $subUserDetails[$key]['email_1']);
														$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ; 
													} 
													if(!empty($subUserDetails[$key]['email_2'])){                                       
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email_2'], 
														'email' => $subUserDetails[$key]['email_2']);
														$from  = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ; 
													}
													if(!empty($subUserDetails[$key]['email_3'])){                                       
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email_3'], 
														'email' => $subUserDetails[$key]['email_3']);
														$from   = $reply_to = array('name' => $email["from_name"], 
														'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ; 
													}
													if(!empty($subUserDetails[$key]['email_4'])){                                       
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email_4'], 
														'email' => $subUserDetails[$key]['email_4']);
														$from   = $reply_to = array('name' => $email["from_name"],
														'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ; 
													}
												}                      
											}
										}
									} 
									if($mail_to_additional_email==1){ 
										if(!empty($userDetails['additional_email'])){
											$arrAddEmail = explode(",",$userDetails['additional_email']); 
											foreach($arrAddEmail as $key=>$val ){
												if(!empty($val)){
													$mail_send_to_su  .= ",".$val;
													//$to = '';
													$to[]   = array('name' =>$val, 'email' => $val);  
													 
												}
											}
										}
									}else{ 
										if(isset($mail_additional_email) && !empty($mail_additional_email)){
											$arrAddEmail = $mail_additional_email;
											foreach($arrAddEmail as $key=>$val ){
												if(!empty($val)){
													$mail_send_to_su  .= ",".$val;
													//$to = '';
													$to[]   = array('name' =>$val, 'email' => $val);  
												 
												}
											}
										}
									}	
									if(isset($_POST['mobile1_to_su']) ){                                
									   $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
										Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."') AND user_id IN('".$mobile1_to_su."') AND status='".Clients::ACTIVE."' ");
										if(!empty($subsmsUserDetails)){                                                                   
											foreach ($subsmsUserDetails  as $key=>$value){
												if(isset($value['mobile1']) && $smsLength<=130){
													$sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
													$sms_to_number[] =$value['mobile1'];
													$mobile1 = "91".$value['mobile1'];
													$client_mobile .= ','.$mobile1;
													ProjectTask::sendSms($data['text'],$mobile1);
													$counter = $counter+1; 
												}
											}
										} 
									}
									$subsmsUserDetails=array();
									if(isset($_POST['mobile2_to_su']) ){                                
									   $mobile2_to_su = implode("','",$_POST['mobile2_to_su']);                                  
										Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile2', " 
										WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."') AND user_id IN('".$mobile2_to_su."') AND status='".Clients::ACTIVE."' ");
										if(!empty($subsmsUserDetails)){                                                                   
											foreach ($subsmsUserDetails  as $key=>$value){
												if(isset($value['mobile2']) && $smsLength<=130){
													$sms_to[]  = $value['f_name'] .' '. $value['l_name'];
													$sms_to_number[] =$value['mobile2'];
													$mobile2 = "91".$value['mobile2'];
													$client_mobile .= ','.$mobile2;
													ProjectTask::sendSms($data['text'],$mobile2);
													$counter = $counter+1;
												}
											}
										} 
									}         
								}
								//Mail to CEO BOF
								if($mail_ceo==1){
									$to[]   = array('name' =>CEO_user_email1,'email' => CEO_user_email1);
									$mail_send_to_su .=",".CEO_user_email1 ;
								}
								//Mail to CEO EOF
								//Set the list subuser's email-id to whom the mails are sent bof
								$query_update = "UPDATE ". TABLE_ST_TICKETS 
											." SET ". TABLE_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', sms_to_number='".implode(",",array_unique($sms_to_number))."',
												sms_to ='".implode(",",array_unique($sms_to))."' "                                    
											." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
								$db->query($query_update) ;
								//Set the list subuser's email-id to whom the mails are sent eof                        
								//update sms counter
								if($counter >0){
									$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
									$db->query($sql); 
								} 
								//Send one copy on clients BOF
								if(!empty($to)){
									getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
									//$to     = '';
									$cc_to_sender=$cc=$bcc='';
									$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
									'email' => $smeerp_client_email); 
									$from= $reply_to= array('name' => $email["from_name"], 'email' => $email['from_email']); 
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
									$cc_to_sender,$cc,$bcc,$file_name);
								}    
								//Send one copy on clients EOF
							} 
							$succ_msg[]="Password change successufully. Check ticket no.".$ticket_no;
							
						}else{ 
							$err_msg[]= "Sorry password can not changed. Please try again.<br>Check your email-id".$httpResponse;
						}
					}	 
				} 
				
				if(!empty($err_msg)){
					foreach($err_msg as $key=>$val){
						$err_msg_text.="<li class=\"message_error\">".$val." </li>";
					}
					
					$err_message = "<table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_error_header\">
							<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\" border=\"0\" />&nbsp;
							Message
						</td>
					</tr>
					<tr>
						<td class=\"message_error\" align=\"left\" valign=\"top\">
							<ul class=\"message_error\">
								".$err_msg_text."
							</ul>
						</td>
					</tr>
					</table>";
				}
				if(!empty($succ_msg)){
					foreach($succ_msg as $key=>$val){
						$succ_msg_text.="<li class=\"message_ok\">".$val." </li>";
					}
					$succ_message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								".$succ_msg_text."
							</ul>
						</td>
					</tr>
					</table>";
				}
				 
		}
		
		if($form_type=='create_email'){ 
		 
		}
   	    if($form_type=='allot_quota'){ 
		
		
		 
		}
	}
echo 	$err_message.$succ_message."|".'';

	include_once( DIR_FS_NC ."/flush.php");

?>
