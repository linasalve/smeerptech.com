<?php
  if ( $perm->has('nc_ne_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		$hour = array();
		$min = array();
		$sec = array();
		for($i=0;$i < 60 ; $i++)
		{
			$min[$i] = "$i";
			$sec[$i] = "$i";
			if($i >= 0 && $i <=24)
			{
				$hour[$i] = "$i";
			} 
		} 
        
		// Include the payment party class
		include_once (DIR_FS_INCLUDES .'/news-events.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
               
                if ( NewsEvents::validateAdd($data, $extra) ) { 
				 	if ( !empty($data['events_date']) ) {
						$temp = explode('/', $data['events_date']);                
						$data['events_date'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
					}
					if ( !empty($data['events_exp_date']) ) {
						$temp = explode('/', $data['events_exp_date']);                
						$data['events_exp_date'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
					}
				
                          $query	= " INSERT INTO ".TABLE_EVENTS
                                    ." SET ".TABLE_EVENTS .".events_title = '". $data['events_title'] ."'" 
                                    .",". TABLE_EVENTS .".events_date = '". 		$data['events_date'] ."'" 
									.",". TABLE_EVENTS .".events_exp_date = '". $data['events_exp_date'] ."'" 
                                    .",". TABLE_EVENTS .".event_time  = '". 		$data['event_time'] ."'" 
                                    .",". TABLE_EVENTS .".events_source = '". 		$data['events_source'] ."'"  
                                    .",". TABLE_EVENTS .".events_introtext = '". 		$data['events_introtext'] ."'"  
                                    .",". TABLE_EVENTS .".event_type = '". 		$data['event_type'] ."'"  
                                     .",". TABLE_EVENTS .".events_content = '". 		$data['events_content'] ."'" 
                                	.",". TABLE_EVENTS .".status = '". 		$data['status'] ."'"  
                                	.",". TABLE_EVENTS .".created_by = '". $my['user_id'] ."'"                                                            
                                    .",". TABLE_EVENTS .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                                	.",". TABLE_EVENTS .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                              	
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/news-events.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/news-events.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/news-events.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'news-events-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
    
    $s->assign("hour",$hour);
	$s->assign("min",$min);
	$s->assign("sec",$sec);
?>
