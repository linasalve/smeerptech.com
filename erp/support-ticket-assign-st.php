<?php

    if ( $perm->has('nc_st_details') ) {
        include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
        $ticket_id	= isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
	    
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        //BOF read the available departments
        //SupportTicket::getDepartments($db,$department);
        //EOF read the available departments
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
            
            if ( SupportTicket::validateAssign($data, $extra) ) {
                
				/* 
					$assign_members_str = implode(",",$data['assign_members']);
					$assign_members_str=",".$assign_members_str.",";
					$assign_members_details_str = implode(",",$data['assign_members_details']);
					$assign_members_details_str=",".$assign_members_details_str.",";
            
					$query = "UPDATE ". TABLE_ST_TICKETS 
                    ." SET ". TABLE_ST_TICKETS .".assign_members = '".$assign_members_str."', "
					. TABLE_ST_TICKETS .".assign_members_details = '".$assign_members_details_str."' "
                    ." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				 */				  
					$query = "UPDATE ". TABLE_ST_TICKETS 
                    ." SET ". TABLE_ST_TICKETS .".order_id = '".$data['order_id']."', "
					. TABLE_ST_TICKETS .".order_details = '".$data['order_details']."', "
					. TABLE_ST_TICKETS .".flw_ord_id = '".$data['order_id']."', "
					. TABLE_ST_TICKETS .".ticket_type = '".SupportTicket::TYP_PROJECT."' "
                    ." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				 
				//unset($GLOBALS["flag"]);
				//unset($GLOBALS["submit"]);
                            
                if ($db->query($query) && $db->affected_rows() > 0) { 
                    $variables['hid'] = $db->last_inserted_id() ;
                    $messages->setOkMessage("Order assigned successfully.");
					//$messages->setOkMessage("Communication assigned to memebers successfully.");
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
		/*    
		if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/support-ticket-list.php');
        }else {*/
		
            $condition_query= " WHERE ticket_id = '". $ticket_id ."' ";
            $_ALL_POST      = NULL;
            
            if ( SupportTicket::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST[0];
                $ticketStatusArr = SupportTicket::getTicketStatus();
                $ticketStatusArr = array_flip($ticketStatusArr);
                $_ALL_POST['ticket_status_name'] = $ticketStatusArr[$_ALL_POST['ticket_status']];   
            }
			
            /*        
            $table = TABLE_SETTINGS_DEPARTMENT;
            $condition2 = " WHERE ".TABLE_SETTINGS_DEPARTMENT .".id= '".$_ALL_POST['ticket_department'] ."' " ;
            $fields1 =  TABLE_SETTINGS_DEPARTMENT .'.department_name';
            $ticket_department= getRecord($table,$fields1,$condition2);            
            $_ALL_POST['department_name']    = $ticket_department['department_name'] ;     
            */
            $condition_query= " WHERE user_id = '". $_ALL_POST['ticket_owner_uid'] ."' ";
            $member      = NULL;
            if ( Clients::getList($db, $member, 'f_name,l_name,org,mobile1,mobile2', $condition_query) > 0 ) {
                $_ALL_POST['client'] = $member[0];
                $_ALL_POST['client']['name'] = $member[0]['f_name']." ".$member[0]['l_name'];
            }
            $fields= TABLE_BILL_ORDERS .'.number,'.TABLE_BILL_ORDERS.'.id,'.TABLE_BILL_ORDERS.'.order_title' ;
			$condition_query1= TABLE_BILL_ORDERS.".client='".$_ALL_POST['ticket_owner_uid']."' 
			AND ".TABLE_BILL_ORDERS.".daily_update_status='1'";
            Order::getDetails( $db, $olist, $fields, $condition_query1);
			
            // BO: Read the Team Members Information.
            if ( !empty($_ALL_POST['assign_members']) && !is_array($_ALL_POST['assign_members']) ) {
                //include ( DIR_FS_INCLUDES .'/user.inc.php');
                
                $_ALL_POST['assign_members']          = explode(',', $_ALL_POST['assign_members']);
                $temp                       = "'". implode("','", $_ALL_POST['assign_members']) ."'";
                $_ALL_POST['assign_members_st']  = '';
                $_ALL_POST['assign_members_details']  = array();
                User::getList($db, $_ALL_POST['assign_members_st'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
                $_ALL_POST['assign_members'] = array();
                foreach ( $_ALL_POST['assign_members_st'] as $key=>$members) {
                //for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
                    $_ALL_POST['assign_members'][] = $members['user_id'];
                    $_ALL_POST['assign_members_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
                }
            }
            // EO: Read the Team Members Information.
     
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'assign_st');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'ticket_id', 'value' => $ticket_id);
            $hidden[] = array('name'=> 'ajx', 'value' => $ajx);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            //$page["var"][] = array('variable' => 'status', 'value' => 'status');
            //$page["var"][] = array('variable' => 'priority', 'value' => 'priority');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            //print_r($_ALL_POST);
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'support-ticket-assign-st.html');
       // }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>