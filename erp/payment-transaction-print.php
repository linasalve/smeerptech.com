<?php
    
    if ( $perm->has('nc_p_pt_print') ) {
		include_once (DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
		include_once (DIR_FS_INCLUDES .'/bill-invoice.inc.php');
		
        $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :''); 
        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $db1 		= new db_local; // database handle
        
        /*$access_level   = $my['access_level'];       
        if ( $perm->has('nc_ab_details_al') ) {
            $access_level += 1;
        }*/

        // Read the record details
        $fields = TABLE_PAYMENT_TRANSACTION .'.*'  ;            
        $condition_query = " WHERE (". TABLE_PAYMENT_TRANSACTION .".id = '". $id ."' )";
		        
		/* $fields = TABLE_PAYMENT_TRANSACTION .'.* , ' . TABLE_USER .".f_name as efname,".TABLE_USER .".l_name as elname,"
		. TABLE_PAYMENT_ACCOUNT_HEAD .'.account_head_name, ' . TABLE_PAYMENT_MODE .'.payment_mode ,'
		. TABLE_VENDORS.'.f_name as fname, '. TABLE_VENDORS .'.l_name as lname, '. TABLE_VENDORS .'.billing_name 
		as ppcompany_name,'
		.TABLE_VENDORS.'.bank_account_no, '.TABLE_SETTINGS_COMPANY.'.name AS scompany_name'; 
		*/
		
		$fields = TABLE_PAYMENT_TRANSACTION .'.*,'. TABLE_USER .".f_name as efname,".TABLE_USER .".l_name as elname,"
                . TABLE_PAYMENT_ACCOUNT_HEAD .'.account_head_name,'.TABLE_PAYMENT_MODE .'.payment_mode,'
				. TABLE_SETTINGS_COMPANY.'.name AS scompany_name,'.TABLE_SETTINGS_COMPANY.'.prefix AS scompany_prefix';
				 
				 
        if ( Paymenttransaction::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];
			$_ALL_POST['debit_stmt_remark']=$_ALL_POST['credit_stmt_remark']='';
			//Display Desc. of Bank Stmt BOF
			if(!empty($_ALL_POST['debit_bank_stmt_id'])){
				$_ALL_POST['debit_stmt_remark'] = $_ALL_POST['debit_bank_stmt_remark1']; 
			}
			if(!empty($_ALL_POST['credit_bank_stmt_id'])){
				$_ALL_POST['credit_stmt_remark'] = $_ALL_POST['credit_bank_stmt_remark1'];      
			}
			//Display Desc. of Bank Stmt EOF
			$pinvArr = array();
			if(!empty($_ALL_POST['proforma_invoice_no'])){
			  $proforma_invoice_no1 = trim($_ALL_POST['proforma_invoice_no'],",");
			  $proforma_invoice_no_str = str_replace(",","','",$proforma_invoice_no1);
			  $sql_pf = " SELECT ".TABLE_BILL_INV_PROFORMA.".number as profm_inv, ".TABLE_BILL_INV.".number as inv_no
			  FROM ".TABLE_BILL_INV_PROFORMA." LEFT JOIN ".TABLE_BILL_INV." ON 
			  ".TABLE_BILL_INV_PROFORMA.".number = ".TABLE_BILL_INV.".inv_profm_no
			  WHERE ".TABLE_BILL_INV_PROFORMA.".number IN('".$proforma_invoice_no_str."') AND
			  ".TABLE_BILL_INV_PROFORMA.".status IN('".InvoiceProforma::COMPLETED."','".InvoiceProforma::ACTIVE."','".InvoiceProforma::PENDING."')
				AND ".TABLE_BILL_INV.".status IN('".Invoice::COMPLETED."','".Invoice::ACTIVE."','".Invoice::PENDING."')";
			
				if ( $db->query($sql_pf) ){
					if ( $db->nf() > 0 ){
						while ($db->next_record()){
						   $profm_inv = $db->f('profm_inv');
						   $inv_no = $db->f('inv_no');
						   $pinvArr[] = array('profm_inv'=>$profm_inv,
												'inv_no'=>$inv_no);
						  
						}
					}                   
				}
			}
			$_ALL_POST['invDetails'] = $pinvArr ;
            $exeCreted=$exeCreatedname='';
            
            $table = TABLE_AUTH_USER;
            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$_ALL_POST['user_id'] ."' " ;
            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
            $exeCreted= getRecord($table,$fields1,$condition2);
            
            $_ALL_POST['creator'] = $exeCreted['f_name']." ".$exeCreted['l_name']."<br/>" ;
            $statusArr = Paymenttransaction::getStatusall();
			$_ALL_POST['pending'] = Paymenttransaction::PENDING ;
            $statusArr = array_flip($statusArr);
            $_ALL_POST['status_name'] = $statusArr[$_ALL_POST['status']];
            if($_ALL_POST['transaction_type'] == Paymenttransaction::PAYMENTIN){
                $_ALL_POST['transaction_type_name'] = 'Receipt';
            }elseif($_ALL_POST['transaction_type'] == Paymenttransaction::PAYMENTOUT){
                $_ALL_POST['transaction_type_name'] = 'Payment';
            }elseif($_ALL_POST['transaction_type'] == Paymenttransaction::INTERNAL){
                $_ALL_POST['transaction_type_name'] = 'Contra';
            }
			
			if(!empty($_ALL_POST['ref_debit_pay_bank_id'])){
				$_ALL_POST['ref_bank_id'] = $_ALL_POST['ref_debit_pay_bank_id'] ;
			}
			if(!empty($_ALL_POST['ref_credit_pay_bank_id'])){
				$_ALL_POST['ref_bank_id'] = $_ALL_POST['ref_credit_pay_bank_id'] ;
			}
			
            if(!empty($_ALL_POST['ref_bank_id'])){
				$table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$_ALL_POST['ref_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr1 = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr1)){
                    $_ALL_POST['ref_bank'] = $banknameArr1['bank_name'] ;
                }
			}
			
            $_ALL_POST['cfname'] ='';
            $_ALL_POST['clname'] ='';
            $_ALL_POST['org'] = '';
            
            if(empty($_ALL_POST['company_name'])){            
                $_ALL_POST['company_name'] = $_ALL_POST['scompany_name'];
                $_ALL_POST['company_prefix'] = $_ALL_POST['scompany_prefix'];
                
            }
			$_ALL_POST['company_logo'] = strtolower($_ALL_POST['company_prefix'])."-tran.jpg";
            $creditbankname= $debitbankname =$pay_bank_companyname='';  
            if(!empty($_ALL_POST['pay_bank_company'])){               
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$_ALL_POST['pay_bank_company'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $pay_bank_companyname = $banknameArr['bank_name'] ;
                }
            }
            
            
            //Bank name BOF
            if( $_ALL_POST['transaction_type'] ==Paymenttransaction::PAYMENTIN ){
           
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$_ALL_POST['credit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $creditbankname = $banknameArr['bank_name'] ;
                    
                }
            }elseif($_ALL_POST['transaction_type'] ==Paymenttransaction::PAYMENTOUT){
           
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$_ALL_POST['debit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $debitbankname = $banknameArr['bank_name'] ;
                    
                }
            }elseif($_ALL_POST['transaction_type'] ==Paymenttransaction::INTERNAL){
           
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$_ALL_POST['debit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $debitbankname = $banknameArr['bank_name'] ;
                }                
                $table = TABLE_PAYMENT_BANK;
                $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$_ALL_POST['credit_pay_bank_id'] ."' " ;
                $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                $banknameArr = getRecord($table,$fields1,$condition2);
                if(!empty($banknameArr)){
                    $creditbankname = $banknameArr['bank_name'] ;   
                }
            }
            $_ALL_POST['pay_received_amt']=number_format($_ALL_POST['pay_received_amt'],2);
            $_ALL_POST['debitbankname'] = $debitbankname ;           
            $_ALL_POST['creditbankname'] = $creditbankname ;           
            $_ALL_POST['pay_bank_companyname'] = $pay_bank_companyname ;
            //Bank name EOF
            
            
            if(!empty($_ALL_POST['client_id'])){
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$_ALL_POST['client_id'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name,l_name,billing_name,bank_account_no' ;
                $clientDetails = getRecord($table,$fields1,$condition2);
                if(!empty($clientDetails['f_name']) ){
                
                         $_ALL_POST['cfname'] = $clientDetails['f_name'] ;      
                         $_ALL_POST['clname'] = $clientDetails['l_name'] ;      
                         $_ALL_POST['billing_name'] = $clientDetails['billing_name'] ;      
                         $_ALL_POST['bank_account_no'] = $clientDetails['bank_account_no'] ;      
                }
            }    
			
			/* if(!empty($_ALL_POST['vendor_bank_id'])){
                $table = TABLE_VENDORS_BANK;
                $condition2 = " WHERE ".TABLE_VENDORS_BANK .".user_id= '".$_ALL_POST['vendor_bank_id'] ."' " ;
                $fields1 =  TABLE_VENDORS_BANK .'.f_name,l_name,billing_name,bank_account_no' ;
                $vendorBankDetails = getRecord($table,$fields1,$condition2);
                
				if(!empty($vendorBankDetails['f_name']) ){
                
                         $_ALL_POST['cfname'] = $vendorBankDetails['f_name'] ;      
                         $_ALL_POST['clname'] = $vendorBankDetails['l_name'] ;      
                         $_ALL_POST['billing_name'] = $vendorBankDetails['billing_name'] ;      
                         $_ALL_POST['bank_account_no'] = $vendorBankDetails['bank_account_no'] ;      
                }
            }    */ 

			if(!empty($_ALL_POST['behalf_client'])){
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$_ALL_POST['behalf_client'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name,l_name,billing_name' ;
                $bclientDetails = getRecord($table,$fields1,$condition2);
                if(!empty($bclientDetails['f_name']) ){
                       
                    $_ALL_POST['behalf_name'] = $bclientDetails['billing_name'] ;      
                }
            }    
			
			if(!empty($_ALL_POST['behalf_vendor_bank'])){
                $table = TABLE_VENDORS_BANK;
                $condition2 = " WHERE ".TABLE_VENDORS_BANK .".user_id= '".$_ALL_POST['behalf_vendor_bank'] ."' " ;
                $fields1 =  TABLE_VENDORS_BANK .'.f_name,l_name,billing_name' ;
                $bvendorBankDetails = getRecord($table,$fields1,$condition2);
                
				if(!empty($bvendorBankDetails['f_name']) ){                            
                    $_ALL_POST['behalf_name'] = $bvendorBankDetails['billing_name'] ;      
                }
            }    
			if(!empty($_ALL_POST['behalf_executive'])){
                $table = TABLE_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$_ALL_POST['behalf_executive'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name,l_name' ;
                $bexecutiveDetails = getRecord($table,$fields1,$condition2);
                
				if(!empty($bexecutiveDetails['f_name']) ){                            
                    $_ALL_POST['behalf_name'] = $bexecutiveDetails['f_name']." ".$bexecutiveDetails['l_name'] ;      
                }
            } 
			
            if(isset($_ALL_POST['pdc']) && $_ALL_POST['pdc'] == 0){
                $_ALL_POST['pdc'] = 'No';
            }else{
                $_ALL_POST['pdc'] = 'Yes';
            }
         
            if($_ALL_POST['transaction_type'] ==Paymenttransaction::PAYMENTOUT  ){				
				 
				/*
				if( $_ALL_POST['pay_received_amt_rcpt'] > 0){
					$_ALL_POST=array();
					$messages->setErrorMessage("All Bills are not alloted to this transaction.");
				}*/					 
				$_ALL_POST['voucher_remarks'] = $voucher_remarks = array();						
				$sql2= "SELECT  ".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".number,
				".TABLE_PAYMENT_PARTY_BILLS.".gl_type_name,
				".TABLE_PAYMENT_TRANSACTION_BILLS.".amount, ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt,
				".TABLE_PAYMENT_PARTY_BILLS.".bill_against_id
				FROM ".TABLE_PAYMENT_PARTY_BILLS." LEFT JOIN ".TABLE_PAYMENT_TRANSACTION_BILLS." ON 
				".TABLE_PAYMENT_PARTY_BILLS.".id = ".TABLE_PAYMENT_TRANSACTION_BILLS.".bill_id WHERE 
				".TABLE_PAYMENT_TRANSACTION_BILLS.".transaction_id =".$id ;
				$voucher_remarks='';
				$bill_against_id_arr=array();
				if ( $db->query($sql2) ) {
					if ( $db->nf() > 0 ) {
						while ($db->next_record()) {
							$bl_no = $db->f('bill_no') ;
							$gl_type_name = $db->f('gl_type_name') ;
							$nc_number = $db->f('number') ;
							$bill_dt  = $db->f('bill_dt') ;
							$bill_against_id  = $db->f('bill_against_id') ;
							$bill_against_no ='';
							if(!empty($bill_against_id )){
								$bill_against_id_arr[]=$bill_against_id ;
								
								$sql3="SELECT bill_against FROM ".TABLE_PAYMENT_BILLS_AGAINST." 
								WHERE id IN (".$bill_against_id.")";
								
								if ( $db1->query($sql3) ) {
									if ( $db1->nf() > 0 ) {
										while ($db1->next_record()) {
											$bill_against_no = $db1->f('bill_against') ;
										}
									}
								}								
							}
							$paid_amount = number_format($db->f('amount'),2) ;
							$voucher_remarks[] = array(
									'bill_no'=>$bl_no,
									'gl_type_name'=>$gl_type_name,
									'nc_number'=>$nc_number,
									'paid_amount'=>$paid_amount,
									'bill_dt'=>$bill_dt,
									'bill_against_no'=>$bill_against_no
								);
						}
						$_ALL_POST['voucher_remarks'] = $voucher_remarks ;
					}
				}
				 /*
				if(!empty($bill_against_id_arr)){
					$bill_agn_str = implode(",",$bill_against_id_arr);
					$sql3="SELECT bill_against FROM ".TABLE_PAYMENT_BILLS_AGAINST." WHERE id IN (".$bill_agn_str.")";
					$bill_against = array();
					if ( $db->query($sql3) ) {
						if ( $db->nf() > 0 ) {
							while ($db->next_record()) {
								$bill_against[] = $db->f('bill_against') ;
							}
						}
					}
					$_ALL_POST['bill_against']=$bill_against;
				}  */
			}
			if ( $messages->getErrorMessageCount() <= 0 ) {
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["var"][] = array('variable' => 'variables', 'value' => 'variables');
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-print.html'); 
			}
        }
        else {
            $messages->setErrorMessage("Records not found to view.");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
        
    }
?>
