<?php

    if ( $perm->has('nc_sms_ma_edit') ) {
		$ma_id = isset($_GET["ma_id"]) ? $_GET["ma_id"] : ( isset($_POST["ma_id"]) ? $_POST["ma_id"] : '' );

        $_ALL_POST	= NULL;
        $data       = NULL;

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            if ( MasterAccount::validateUpdate($data, $extra) ) {
				$sql = "UPDATE ".TABLE_SMS_MASTER_ACC." SET 
						ma_userid 	= '".$data['ma_userid']."',
						ma_username = '".$data['ma_username']."',
						ma_password = '".$data['ma_password']."',
						ma_posturl 	= '".$data['ma_posturl']."',
						ma_status 	= '".$data['ma_status']."' WHERE ma_id='".$ma_id."'";

                if ($db->query($sql))
					$messages->setOkMessage("The Account has been update.");
				else
					$messages->setErrorMessage('Problem updating Account. Try later');
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$variables['hid'] = $ma_id;
            include ( DIR_FS_NC .'/master-account-list.php');
        }
        else {
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
				$_ALL_POST = $_POST;
            }
            else {

                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE ma_id = '". $ma_id ."' ";
                $_ALL_POST      = NULL;
                if ( MasterAccount::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) 
                    $_ALL_POST = $_ALL_POST[0];
            }

			if ( !empty($_ALL_POST['ma_id']) ) {
            	// These parameters will be used when returning the control back to the List page.
            	foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                	$hidden[]   = array('name'=> $key, 'value' => $value);
            	}
				$hidden[] = array('name'=> 'perform','value' => 'edit');
				$hidden[] = array('name'=> 'act', 'value' => 'save');
				$hidden[] = array('name'=> 'ma_id', 'value' => $ma_id);
	
				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["var"][] = array('variable' => 'data', 'value' => 'data');
			
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'master-account-edit.html');
            }
            else {
                $messages->setErrorMessage("The Selected Account was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You do not have the Permisson to Access this module.");
    }
?>