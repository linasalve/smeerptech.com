<?php

    if ( $perm->has('nc_ld_qt_details') ) {
        $inv_id = isset ($_GET['inv_id']) ? $_GET['inv_id'] : ( isset($_POST['inv_id'] ) ? $_POST['inv_id'] :'');
        $type   = isset ($_GET['type']) ? $_GET['type'] : ( isset($_POST['type'] ) ? $_POST['type'] :'');
		$filename   = isset ($_GET['filename']) ? $_GET['filename'] : ( isset($_POST['filename'] ) ? $_POST['filename'] :'');
        
        $condition_query= " WHERE ". TABLE_LEADS_QUOTATION .".id = '". $inv_id ."'"
							." OR ". TABLE_LEADS_QUOTATION .".number = '". $inv_id ."'";
        
 

        $list	= NULL;
        if ( LeadsQuotation::getDetails( $db, $list, TABLE_LEADS_QUOTATION .'.id, '. TABLE_LEADS_QUOTATION .'.number,'. TABLE_LEADS_QUOTATION .'.attach_file1,'. TABLE_LEADS_QUOTATION .'.attach_file2,'. TABLE_LEADS_QUOTATION .'.attach_file3,'. TABLE_LEADS_QUOTATION .'.attach_file4,'. TABLE_LEADS_QUOTATION .'.attach_file5', $condition_query) ) {
          
            if ( $type == 'html' ) {
                $file_path = DIR_FS_QUOTATIONS_HTML ."/";
                $file_name = $list[0]["number"] .".html";
                $content_type = 'text/html';
            }elseif ( $type == 'htmlprint' ) {
                $file_path = DIR_FS_QUOTATIONS_HTMLPRINT ."/";
                $file_name = $list[0]["number"] .".html";
                $content_type = 'text/html';
            }elseif ( $type == 'pdf' ) {
                $file_path = DIR_FS_QUOTATIONS_PDF ."/";
                $file_name = $list[0]["number"] .".pdf";
                $content_type = 'application/pdf';
            }elseif ( $type == 'attach_file1' ){
                $file_path = DIR_FS_QUOTATIONS."/";
                $file_name = $filename;
                $content_type = '';
            }
			elseif ( $type == 'attach_file2' ) {
                $file_path = DIR_FS_QUOTATIONS."/";
               $file_name = $filename;//mime_content_type
                $content_type = '';
            }
			elseif ( $type == 'attach_file3' ) {
                $file_path = DIR_FS_QUOTATIONS."/";
				$file_name = $filename;
                $content_type = '';
            }
			elseif ( $type == 'attach_file4' ) {
                $file_path = DIR_FS_QUOTATIONS."/";
                $file_name = $filename;
                $content_type = '';
            }elseif ( $type == 'attach_file5' ) {
                $file_path = DIR_FS_QUOTATIONS."/";
                $file_name = $filename;
                $content_type = '';
            }else {
                echo('<script language="javascript" type="text/javascript">alert("Invalid File specified");</script>');
            }
            
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-type: $content_type");
            header("Content-Disposition: attachment; filename=". $file_name );
            header("Content-Length: ".filesize($file_path.$file_name));
            readfile($file_path.$file_name);
        }
        else {
            echo('<script language="javascript" type="text/javascript">alert("The File is not found or you do not have the permission to view the file.");</script>');
        }
    }
    else {
        echo('<script language="javascript" type="text/javascript">alert("You do not have the permission to view the list.");</script>');
    }
    exit;
?>