<?php

    if ( $perm->has('nc_bk_chq_list') ) {        
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
         //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
		
        if($searchStr==1){
            $total	=	BankCheque::getList( $db, $list, '', $condition_query);
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');           
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';        
            $list	= NULL;
			$fields = TABLE_BANK_CHEQUE.".* 
				,".TABLE_PAYMENT_BANK.".bank_name 
				,".TABLE_PAYMENT_BANK.".company_name " ;
            BankCheque::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        // Set the Permissions.
        $variables['can_add']     = false;
        $variables['can_view_list']     = false;
        if ( $perm->has('nc_bill_ord_tax_list') ) {
			$variables['can_view_list']     = true;
		}
		if ( $perm->has('nc_bill_ord_tax_csv') ) {
			$variables['can_add']     = true;
		}
		
        //$variables['searchLink']=$searchLink ;
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bank-cheque-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>