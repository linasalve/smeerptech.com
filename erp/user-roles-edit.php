<?php
    if ( $perm->has('nc_urol_edit') ) {
        $data		= NULL;
        $chk_output	= NULL;
        $_ALL_POST  = NULL;
        $chk_rights	= NULL;
        $rights_array = NULL;
        $al_list    = getAccessLevel($db, ($my['access_level']+1));
        
        $role_id = isset ($_GET['role_id']) ? $_GET['role_id'] : ( isset($_POST['role_id'] ) ? $_POST['role_id'] :'');
        
        // update form.
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST	=	$_POST;
            $data		=  processUserData($_ALL_POST); 
            //insert comma(,) when select multipule rights. 
            if ( isset($data['chk_rights']) &&  $data['chk_rights']!='' ){
                $chk_output[]   = $data['chk_rights'];
                
                foreach( $chk_output as $key=>$val ) {
                    $rights_array	=  $val;
                }
                $chk_rights 	=	implode(",", $rights_array);
            }
    
            $extra = array( 'db' 				=> &$db,
                            'data' 				=> $data,
                            'messages' 			=> $messages,
                            'chk_rights' 		=> $chk_rights,
                    );

            //check errors before updating the User roles.
            if (UserRoles::validateUpdate($data, $messages, $extra, $role_id)) {
				$rights_values =$value = '';
				$sql = " SELECT value FROM ".TABLE_USER_RIGHTS." WHERE id IN(".$chk_rights.") " ; 
				$db->query($sql);
				if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$value.=$db->f('value').",";
					}
					$rights_values = trim($value,",");
				}
                $query	= " UPDATE  ".TABLE_USER_ROLES
                                . " SET "
                                        . TABLE_USER_ROLES .".title 			='". $data['title'] ."'"
                                    .",". TABLE_USER_ROLES .".rights		='". $chk_rights ."'"
									.",". TABLE_USER_ROLES .".rights_values			='". $rights_values ."'"
                                    .",". TABLE_USER_ROLES .".description	='". $data['description'] ."'"
                                    .",". TABLE_USER_ROLES .".access_level   ='". $data['access_level'] ."'"
                            ." WHERE ".TABLE_USER_ROLES. ".id = '". $role_id ."'";

                if ( $db->query($query) ) {
                    if ( $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Users Role Updated Successfully.");
                    }
                    else {
                        $messages->setOkMessage("No changes were made to the User Role.");
                    }
                }
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
                || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $role_id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/user-roles-list.php');
            /*
            if ( isset($_POST['action']) && $_POST['action'] == 'search' ) {
                include ( DIR_FS_NC .'/user-roles-search.php');
            }
            else {
                include ( DIR_FS_NC .'/user-roles-list.php');
            }
            */
            
        }
        else {
            // Read all the User Rights.
            /*
            $userrights_list        = NULL;
            UserRoles::getUserRights( $db, $userrights_list, 'id, parent_id, title', 'ORDER BY value ASC');
            */
            
            //Get Userrts BOF
            $userrightsParent_list		= NULL;
            $condition = " WHERE parent_id = 0 ";
            UserRoles::getUserRights( $db, $userrightsParent_list, 'id, parent_id, title', $condition);
            $rightList=array();
            if(!empty($userrightsParent_list)){
                foreach( $userrightsParent_list as $key=>$val){
                    $userRtlist=null;
                    $id = $val['id'];
                    $parent_id = $val['parent_id'];
                    $title= $val['title'];
                    $condition = " WHERE parent_id = '".$id."'";
                    UserRoles::getUserRights( $db, $userRtlist, 'id, parent_id, title', $condition);
                    $rightList[]= array('id'=>$id,
                                        'parent_id'=>$parent_id,
                                        'title'=>$title,
                                        'child'=>$userRtlist
                                        );
                }
            }
            //Get Userrts EOF
            
            
            $condition_query    = " WHERE id = '". $role_id ."' ";
            $data               = NULL;
            if ( UserRoles::getList($db, $data, '*', $condition_query) > 0 ) {
                $data                  =   $data['0'];
                $data['chk_rights']    = explode(",", $data['rights']);
    
                // These parameters will be used when returning the control back to the List page.
                foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                    $hidden[]   = array('name'=> $key, 'value' => $value);
                }
                $hidden[]   = array('name'=> 'perform' , 'value' => 'edit');
                $hidden[]   = array('name'=> 'act' , 'value' => 'save');
                $hidden[]   = array('name'=> 'role_id' , 'value' => $role_id);
            }
            else {
                $messages->setErrorMessage('The selected User Role was not found.');
            }
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'rightList', 'value' => 'rightList');
            
    
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-roles-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>