<?php
    if ( $perm->has('nc_al_list') ) {
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';
        }
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched'] = 1 ;
        // To count total records.
        $list	= 	NULL;
        $total	=	AccessLevel::getList($db, $list, '', $condition_query);
    
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');

        $list	= NULL;
        AccessLevel::getList($db, $list, '*', $condition_query, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_add']            = false;
        $variables['can_edit']           = false;
        $variables['can_delete']         = false;
        $variables['can_view_details']   = false;
        $variables['can_status']  = false;
    
        if ( $perm->has('nc_al_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_al_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_al_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_al_status') ) {
            $variables['can_status'] = true;
        }
        if ( $perm->has('nc_al_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_al_view') ) {
            $variables['can_view_details'] = true;
        }
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-access-level-list.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>