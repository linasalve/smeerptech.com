<?php
  if ( $perm->has('nc_fy_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		include_once (DIR_FS_INCLUDES .'/financial-year.inc.php');	
       	include_once (DIR_FS_INCLUDES .'/company.inc.php');	
       	$lst_company = NULL;
        $required_fields ='*';
		Company::getList($db,$lst_company,$required_fields);
       
       	
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST 	= $_POST;
		$data		= processUserData($_ALL_POST);
                
		$extra = array( 'db' 				=> &$db,
						'access_level'      => $access_level,
						'messages'          => &$messages
		);
        
         if ( FinancialYear::validateAdd($data, $extra) ) { 
			
			$db_prefix 	= new db_local();
			
			$prefix_query="SELECT prefix FROM ".TABLE_SETTINGS_COMPANY." WHERE id = '".$data['company_id']."'";
			
			$db_prefix->query( $prefix_query );
			$db_prefix->next_record();
			$prefix=$db_prefix->f("prefix");
			
			$query	= " INSERT INTO ".TABLE_FINANCIAL_YEAR
						." SET ".TABLE_FINANCIAL_YEAR .".year_range = '". $data['year_range'] ."'"                                     
						.",". TABLE_FINANCIAL_YEAR .".company_id = '". 		$data['company_id'] ."'"
						.",". TABLE_FINANCIAL_YEAR .".company_prefix = '". 		$prefix ."'"						
						.",". TABLE_FINANCIAL_YEAR .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"						
						.",". TABLE_FINANCIAL_YEAR .".date   = '". 		date('Y-m-d H:i:s')."'";
                
				if ( $db->query($query) && $db->affected_rows() > 0 ) {
					$messages->setOkMessage("Record has been added successfully.");
					$variables['hid'] = $db->last_inserted_id();              
				}
			
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
		}
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/financial-year.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/financial-year.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/financial-year.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');               
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'financial-year-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
