<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
    //include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php');
    include_once ( DIR_FS_INCLUDES .'/followup.inc.php');
    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');
    $deleted 	= isset($_GET["deleted"])   ? $_GET["deleted"]  : ( isset($_POST["deleted"])    ? $_POST["deleted"] : '0');
    $updated 	= isset($_GET["updated"])   ? $_GET["updated"]  : ( isset($_POST["updated"])    ? $_POST["updated"] : '0');
    $or_no 		= isset($_GET["or_no"])     ? $_GET["or_no"]    : ( isset($_POST["or_no"])      ? $_POST["or_no"]   : '');
    $hid 		= isset($_GET["hid"])         ? $_GET["hid"]        : ( isset($_POST["hid"])          ? $_POST["hid"]       :'');
    $action='Order';
    $condition_url ='';
    $condition_query = '';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
     //By default search in On
    $_SEARCH["searched"]    = true ;
     // Read the available Status 
    $variables['types'] = Order::getTypes();
    
    if($added){
    
        $messages->setOkMessage("New Order has been created.");
    }
    if($updated){
    
        $messages->setOkMessage("Order has been updated.");
    }
    if($deleted){    
        $messages->setOkMessage("Order $or_no has been deleted.");
    }
    if ( $perm->has('nc_bl_or') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_BILL_ORDERS   =>  array(  'Order No.'     => 'number',
																'Financial Year'   => 'financial_yr',
                                                                'Order Title'   => 'order_title',
																'Amount'     => 'amount',
                                                               //'Particulars'   => 'particulars',
                                                                'Details'       => 'details'
                                                        ),
								TABLE_BILL_ORD_P   =>  array(  'Order Particulars'     => 'particulars',                                                        ),
                                 // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                TABLE_USER      =>  array(  'Creator Number'    => 'number-user',
                                                            'Creator Username'  => 'username-user',
                                                            'Creator Email'     => 'email-user',
                                                            'Creator First Name'=> 'f_name-user',
                                                            'Creator Last Name' => 'l_name-user',
                                                ),
								TABLE_CLIENTS 	=>  array(  'Client Number'    => 'number-clients',
                                                            'Billing Name'  => 'billing_name-clients',
                                                            'Client Company'  => 'org-clients',
                                                            'Client Username'  => 'username-clients',
                                                            'Client Email'     => 'email-clients',
                                                            'Client First Name'=> 'f_name-clients',
                                                            'Client Last Name' => 'l_name-clients',
                                                ),
								
                            );
        
        $sOrderByArray  = array(
                                TABLE_BILL_ORDERS => array( 'Date of Order'     => 'do_o',
                                                            'Date of Delivery'  => 'do_d',
                                                            'Date of Completion'  => 'ac_ed_date',
                                                            'Date of Expiry'  => 'do_e',
															'Date of Creation'  => 'do_c',
                                                            'Status'            => 'status'
														),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
          
            if(!empty($hid)){
                $_SEARCH['sOrderBy']= $sOrderBy = 'id';
                $variables['hid']=$hid;
            }else{
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            }
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_BILL_ORDERS;
        }

        // Read the available Status for the Pre Order.
        $variables['status'] = Order::getStatus();
        $variables['flwstatus'] = Order::getFollowupStatus();
        $variables['rstatus'] = Order::getRStatus();
        
        //use switch case here to perform action. 
        switch ($perform) {
           
        
            case ('add'): {
				$action.=' Add';
                include (DIR_FS_NC.'/bill-order-add.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('approval'): {
				$action.=' Add';
                include (DIR_FS_NC.'/bill-order-approval.php');                 
                break;
            }
            case ('renew'): 
            case ('nadd'): {
				$action.=' Renew';
                include (DIR_FS_NC.'/bill-order-nadd.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			
			case ('view_dm'): {
                include (DIR_FS_NC .'/bill-order-view-dm.php');
                break;
            }
			case ('add_dm'):{            
                include (DIR_FS_NC.'/bill-order-add-dm.php');         
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('add_o'): {
            
                include (DIR_FS_NC.'/bill-order-add-old.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('edit_pre_order'):
            case ('edit'): {	
				$action.=' Edit';
                include (DIR_FS_NC .'/bill-order-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('nedit'): {	
				$action.=' Edit';
                include (DIR_FS_NC .'/bill-order-nedit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_o'): {
                include (DIR_FS_NC .'/bill-order-edit-old.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_pre_order'):
            case ('view'): {
                include (DIR_FS_NC .'/bill-order-view.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('change_status'): {
                $searchStr=1; 
                include ( DIR_FS_NC .'/bill-order-status.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('prod-list'): {
				$searchLink=1;
				$searchStr = 1;
				
				$_SEARCH["chk_status"]  = 'AND';
				$_SEARCH["sStatus"]     = array(Order::ACTIVE,Order::PENDING); 
				$condition_query = " WHERE "." ". TABLE_BILL_ORDERS .".status IN('".Order::ACTIVE ."','".Order::PENDING."'".")";
				
				$inproduction_status = 1;
				$chk_inprod = 'AND';				
				$condition_query .= " AND ". TABLE_BILL_ORDERS .".inproduction_status ='".$inproduction_status."' ";  
				$condition_url          .= "&chk_inprod=$chk_inprod&inproduction_status=$inproduction_status";
				$_SEARCH["inproduction_status"]  = $inproduction_status;
				$_SEARCH["chk_inprod"]     = $chk_inprod;   
				
                include(DIR_FS_NC."/bill-order-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('csearch'): { 
				$action.=' Completed List';
				$searchLink=1;
                include(DIR_FS_NC."/bill-order-search.php");  
				include (DIR_FS_NC .'/bill-order-clist.php');				
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			//Order completed list
			case ('clist'): {
				$action.=' Completed List';
                $searchStr = 1; 
 				$_SEARCH['sOrderBy']= $sOrderBy = 'ac_ed_date';
                $_SEARCH['sOrder']  = $sOrder   = 'DESC';
				
                include (DIR_FS_NC .'/bill-order-clist.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): { 
				$searchLink=1;
                include(DIR_FS_NC."/bill-order-search.php");       
				include (DIR_FS_NC .'/bill-order-list.php');				
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('rp_list'): { 
				//renewal pending orders				
				$action.=' - CRON Created Renewal List';
                $searchStr = 1;
				$_SEARCH["chk_status"]  = 'AND';
				$_SEARCH["sStatus"]     = array(Order::ACTIVE);
				$_SEARCH["chk_pinv"]  = 'AND';
				$_SEARCH["pInv"]     = 0;    
				$_SEARCH["chk_inv"]  = 'AND';
				$_SEARCH["sInv"]     = 0;    
				$_SEARCH["chk_cron_created"]     = 1;    
				
				$condition_query = " WHERE ".TABLE_BILL_ORDERS.".status='".Order::ACTIVE."'
				AND ".TABLE_BILL_ORDERS .".cron_created = '".$_SEARCH["chk_cron_created"]."'
				AND ".TABLE_BILL_ORDERS.".is_proforma_invoice='".$_SEARCH["pInv"]."' 
				AND ".TABLE_BILL_ORDERS.".is_invoiceprofm_create='".$_SEARCH["sInv"]."'";
				$condition_url  = "&chk_cron_created=".$_SEARCH["chk_cron_created"]
				."&chk_inv=AND&sInv=0&chk_pinv=AND&pInv=0&chk_status=AND&sStatus=".Order::ACTIVE ;
                
				include (DIR_FS_NC .'/bill-order-list.php');				
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			
			case ('renewal_list'): { 
				//renewal orders				
				$action.= ' - Renewal List';
                $searchStr = 1;
				        
				$_SEARCH["chk_status"]  = 'AND';
				$_SEARCH["sStatus"]     = array(Order::ACTIVE);
				$_SEARCH["chk_pinv"]  = 'AND';
				$_SEARCH["pInv"]     = 0;    
				$_SEARCH["chk_inv"]  = 'AND';
				$_SEARCH["sInv"]     = 0;    
				$_SEARCH["dt_field"]      = 'do_e';    
				$_SEARCH["chk_date_from"] = 'AND';
				$_SEARCH["date_from"]     = date('d')."/".(date('m')-1)."/".date('Y');
				$_SEARCH["chk_date_to"]   =	'AND';
				$_SEARCH["date_to"]       = date('d')."/".(date('m')+1)."/".date('Y'); 
				 
				$dfa = date('Y') .'-'. (date('m')-1) .'-'. date('d').' 00:00:00';				 
				$dta = date('Y') .'-'.(date('m')+1).'-'. date('d').' 23:59:59';
				
				$condition_query = " WHERE ".TABLE_BILL_ORDERS.".status IN ('".Order::ACTIVE."')
				AND ".TABLE_BILL_ORDERS.".is_invoice_create='".$_SEARCH["sInv"]."' 
				AND ".TABLE_BILL_ORDERS.".is_invoiceprofm_create='".$_SEARCH["pInv"]."'
				AND ".TABLE_BILL_ORDERS .".".$_SEARCH["dt_field"] ." >= '". $dfa ."'
				AND ".TABLE_BILL_ORDERS .".".$_SEARCH["dt_field"] ." <= '". $dta ."'";
				
				$condition_url  = "&chk_inv=AND&sInv=0&chk_pinv=AND&pInv=0&chk_status=AND&sStatus=".Order::ACTIVE ;
				$condition_url  .= "&chk_date_from=$chk_date_from&date_from=$date_from"; 
                $condition_url  .= "&chk_date_to=$chk_date_to&date_to=$date_to";
				
				//include (DIR_FS_NC .'/bill-order-list.php');	
				include(DIR_FS_NC."/bill-order-search.php");  
				
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('delete_pre_order'):
            case ('delete'): {
                include ( DIR_FS_NC .'/bill-order-delete.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('order_bifurcation'): {
                include (DIR_FS_NC .'/bill-order-bifurcation.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('ordih_target'): {
                include (DIR_FS_NC .'/order-inhand-target.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('vendors_job'): {
                include (DIR_FS_NC .'/bill-order-vendors-jobwork.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*BOF added on 28-april-2009
            case ('followup'): {
                include (DIR_FS_NC .'/bill-order-followup.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }*/
            case ('old-list'): {
                include (DIR_FS_NC .'/bill-order-old-list.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			
            case ('view_attachment'): {             
                include (DIR_FS_NC .'/bill-order-view-attachment.php');
                break;
            }     
            /*EOF added on 28-april-2009*/
            case ('list_pre_order'):
            case ('list'):
            default: {
				$action.=' List';
                $searchStr = 1;
				if(!empty($or_no)){
					$condition_query = " WHERE ".TABLE_BILL_ORDERS.".number='".$or_no."'";
				}
                include (DIR_FS_NC .'/bill-order-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
	$s->assign("action", $action);
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>