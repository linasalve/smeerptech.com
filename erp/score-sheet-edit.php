<?php
    if ( $perm->has('nc_ss_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_ss_edit_al') ) {
            $access_level += 1;
        }*/
       // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
                   
        $condition1 = " WHERE status='".User::ACTIVE."' AND user_id NOT IN('50a9c7dbf0fa09e8969978317dca12e8',
        'e68adc58f2062f58802e4cdcfec0af2d','febc8f8ac083f5fc27e032c81e7b536a') ORDER BY f_name";
        User::getList($db,$lst_executive,$fields, $condition1);
        // BO:  Read the Team members list ie member list in dropdown box  EOF 
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
            
            if(empty($data['hide_my_name'])){
            	$data['hide_my_name']=0;
            }	
            
            if(empty($data['hide_my_comment'])){
            	$data['hide_my_comment']=0;
            }
            
            if ( Scoresheet::validateUpdate($data, $extra) ) {  
			
				//$comment_to = implode(",",$data['comment_to']);
				//$comment_to_user = str_replace(",","','",$comment_to);
				
				$comment_to_user = $comment_to = $data['comment_to'];
                $condition_ct = " status='".User::ACTIVE."' AND user_id IN('".$comment_to_user."') ORDER BY f_name";
                $ct_field = " CONCAT_WS(' ', f_name, l_name) AS name " ;
                $sql= " SELECT ".$ct_field." FROM ".TABLE_USER." WHERE ".$condition_ct;

                if ( $db->query($sql) ) {
					if ( $db->nf() > 0 ) {
						while ($db->next_record()) {
							$comment_to_name .= $db->f('name').",";
						}
						$comment_to_name = trim($comment_to_name);
					}
				}
								
                $query  = " UPDATE ". TABLE_SCORE_SHEET
				    ." SET ". TABLE_SCORE_SHEET .".comment_to 		= ',".$comment_to.",'"
					.",". TABLE_SCORE_SHEET .".comment_to_name = '".      $comment_to_name ."'"
					.",". TABLE_SCORE_SHEET .".particulars = '".          $data['particulars'] ."'"
					.",". TABLE_SCORE_SHEET .".reason_id = '".            $data['reason_id'] ."'"
					.",". TABLE_SCORE_SHEET .".hide_my_name = '".         $data['hide_my_name'] ."'"
					.",". TABLE_SCORE_SHEET .".hide_my_comment = '".      $data['hide_my_comment'] ."'"
					.",". TABLE_SCORE_SHEET .".score = '".                $data['score'] ."'"
                    ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Score entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SCORE_SHEET .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SCORE_SHEET .".id = '". $id ."' )";
            /*$condition_query .= " AND ( ";
            // If my has created this record.
            $condition_query .= " (". TABLE_SCORE_SHEET .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_SCORE_SHEET .".access_level < $access_level ) ";  
                                    
            // Check if the User has the Right to Edit scoresheet created by other Users.
            /*
            if ( $perm->has('nc_ss_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_ss_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_SCORE_SHEET. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_SCORE_SHEET .".access_level < $access_level_o ) ";
            }
            $condition_query .= " ) ";*/
            
         
            if ( Scoresheet::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                $_ALL_POST['comment_to']=explode(",",trim($_ALL_POST['comment_to'],","));
                /*if ( $_ALL_POST['access_level'] < $access_level ) {
                    
                }
                else {
                    $messages->setErrorMessage("You do not have the Permission to view the Record with the current Access Level.");
                    $_ALL_POST=null;
                }*/
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
                
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/score-sheet-list.php');
        }
        else {
            if ( $messages->getErrorMessageCount() <= 0 ) {
               // These parameters will be used when returning the control back to the List page.
                foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                    $hidden[]   = array('name'=> $key, 'value' => $value);
                }
                
                $hidden[] = array('name'=> 'id', 'value' => $id);
                $hidden[] = array('name'=> 'perform', 'value' => 'edit');
                $hidden[] = array('name'=> 'act', 'value' => 'save');
                
                $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            }
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');     
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'score-sheet-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>