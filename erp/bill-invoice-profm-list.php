<?php

    if ( $perm->has('nc_bl_invprfm_list') ) {
        include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php');
        include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
	
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id='0' ORDER BY id ASC ";
		ServiceTax::getList($db, $taxArr, 'id,tax_name', $condition_queryst);
    
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            /*
            $condition_query = '';
            $time   = time();
            $l_mon  = strtotime('-1 month', $time);
            $from   = date('Y-m-d 00:00:00', $l_mon );
            $to     = date('Y-m-d 23:59:59', $time); 
            $condition_query = " WHERE ". TABLE_BILL_INV_PROFORMA .".do_i <= '". $to ."' "
                                ." AND ("
                                    . TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::BLOCKED ."'"
                                    ." OR ". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::ACTIVE ."'"
                                    ." OR ". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::PENDING ."'"
                                    ." OR ". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::DELETED ."'"
                                .")"; 
            $_SEARCH["date_from"]   = date('d/m/Y', $l_mon);
            $_SEARCH["chk_date_to"] = 'AND';
            $_SEARCH["date_to"]     = date('d/m/Y', $time); */
            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"] = array(InvoiceProforma::COMPLETED,InvoiceProforma::PENDING,InvoiceProforma::ACTIVE);            
            $_SEARCH["chk_paid"]  = 'AND';
            $_SEARCH["paidStatus"] = InvoiceProforma::UN_PAID;           
            $condition_query = " WHERE (". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::COMPLETED ."' OR ". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::PENDING."' OR ". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::ACTIVE."') AND ".TABLE_BILL_INV_PROFORMA.".paid_status='".InvoiceProforma::UN_PAID."'"   ;
        }
        
        // If the InvoiceProforma is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_inv_list_al') ) {
            $access_level += 1;
        }
       
    
        if ( $perm->has('nc_bl_invprfm_list_all') ) {
        
             $condition_query .='';
             
        }else{
                $condition_query .= " AND ( ";
            
                // If my has created this InvoiceProforma.
                $condition_query .= " ( ". TABLE_BILL_INV_PROFORMA .".created_by = '". $my['user_id'] ."' "
                                        ." AND ". TABLE_BILL_INV_PROFORMA .".access_level < $access_level ) ";
                
                // If my is the Client Manager
                $condition_query .= " OR ( ". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                                    ." AND ". TABLE_BILL_INV_PROFORMA .".access_level < $access_level ) ";
                
                // Check if the User has the Right to view InvoiceProformas created by other Users.
                if ( $perm->has('nc_bl_inv_list_ot') ) {
                    $access_level_o   = $my['access_level'];
                    if ( $perm->has('nc_bl_inv_list_ot_al') ) {
                        $access_level_o += 1;
                    }
                    $condition_query .= " OR ( ". TABLE_BILL_INV_PROFORMA. ".created_by != '". $my['user_id'] ."' "
                                        ." AND ". TABLE_BILL_INV_PROFORMA .".access_level < $access_level_o ) ";
                }
           $condition_query .= " ) ";
        }     
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
		
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {        
            $extra_url  = $condition_url;            
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }        
        $condition_url .="&perform=".$perform;
        
        //Code to calculate total of Amount BOF
		/* 
		$totalAmt =array();
		$totalAmount=$totalAmountInr='';
		$fields=" SUM(".TABLE_BILL_INV_PROFORMA.".amount_inr) as totalAmountInr, 
		SUM(".TABLE_BILL_INV_PROFORMA.".amount) as totalAmount ";
		InvoiceProforma::getDetails(  $db, $totalAmt, $fields, $condition_query);
		if(!empty($totalAmt)){
			$totalAmountInr=number_format($totalAmt[0]['totalAmountInr'],2);
			$totalAmount=number_format($totalAmt[0]['totalAmount'],2);
		} 
		*/
		$totalAmt =array();
		$totalAmount=$totalAmountInr='';
		//Get total Amount BOF
		$sql = " SELECT SUM( c.amount_inr ) AS totalAmountInr,
				SUM( c.balance_inr ) AS totalBalanceInr,
				SUM( c.amount ) AS totalAmount,
				SUM( c.balance ) AS totalBalance
			FROM ( 
				SELECT DISTINCT(".TABLE_BILL_INV_PROFORMA.".id), ".TABLE_BILL_INV_PROFORMA.".amount_inr,
				".TABLE_BILL_INV_PROFORMA.".balance_inr,
				".TABLE_BILL_INV_PROFORMA.".amount,
				".TABLE_BILL_INV_PROFORMA.".balance 
			FROM ". TABLE_BILL_INV_PROFORMA."
			LEFT JOIN ". TABLE_BILL_ORDERS." ON ". TABLE_BILL_ORDERS .".number = ". TABLE_BILL_INV_PROFORMA .".or_no 
			LEFT JOIN ". TABLE_BILL_ORD_P ." ON ". TABLE_BILL_ORD_P .".ord_no = ". TABLE_BILL_INV_PROFORMA .".or_no 
			LEFT JOIN ". TABLE_BILL_INV." ON ( ". TABLE_BILL_INV .".inv_profm_no = ". TABLE_BILL_INV_PROFORMA .".number AND
				".TABLE_BILL_INV.".status != '".Invoice::DELETED."' ) 
			LEFT JOIN ". TABLE_USER." ON ". TABLE_USER .".user_id = ". TABLE_BILL_INV_PROFORMA .".created_by 
			LEFT JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_INV_PROFORMA .".client ";			 
		$sql .= " ". $condition_query." ) as c "; 
		if ( $db->query($sql) ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$totalAmountInr = $db->f("totalAmountInr") ;
					$totalAmount = $db->f("totalAmount") ;
					$totalBalanceInr = $db->f("totalBalanceInr") ;
					$totalBalance = $db->f("totalBalance") ;
				}
			}
		}
        //Code to calculate total of Amount EOF
        
        // To count total records.
        $list	= 	NULL;
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
        if($searchStr==1){
           $fields = 	" DISTINCT(".TABLE_BILL_INV_PROFORMA.".id)"
                        .','. TABLE_BILL_INV_PROFORMA .'.number'
                        .','. TABLE_BILL_INV_PROFORMA .'.or_no'
                        .','. TABLE_BILL_INV_PROFORMA .'.or_id'
                        .','. TABLE_BILL_INV_PROFORMA .'.new_ref_ord_id '
                        .','. TABLE_BILL_INV_PROFORMA .'.new_ref_ord_no '
                        .','. TABLE_BILL_INV_PROFORMA .'.access_level'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_fe'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_e'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_c'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_d'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_i'
						.','. TABLE_BILL_INV_PROFORMA .'.paid_status'
                        .','. TABLE_BILL_INV_PROFORMA .'.currency_abbr'
						.','. TABLE_BILL_INV_PROFORMA .'.tax1_name'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_value'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_total_amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub1_name'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub1_value'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub1_total_amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub2_name'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub2_value'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub2_total_amount'
						.','. TABLE_BILL_INV_PROFORMA .'.sub_total_amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.round_off_op'
						.','. TABLE_BILL_INV_PROFORMA .'.round_off'  
                        .','. TABLE_BILL_INV_PROFORMA .'.amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.amount_inr'
                        .','. TABLE_BILL_INV_PROFORMA .'.balance as prfm_balance'
                        .','. TABLE_BILL_INV_PROFORMA .'.balance_inr as prfm_balance_inr'
						.','. TABLE_BILL_INV .'.number as inv_no'
						.','. TABLE_BILL_INV .'.do_i as inv_dt'
						.','. TABLE_BILL_INV .'.balance'
                        .','. TABLE_BILL_INV .'.balance_inr'
                        .','. TABLE_BILL_INV_PROFORMA .'.status'
                        .','. TABLE_BILL_INV_PROFORMA .'.is_old'
                        .','. TABLE_BILL_INV_PROFORMA .'.old_updated'
                        .','. TABLE_BILL_INV_PROFORMA .'.reason_of_delete'
                        .','. TABLE_BILL_ORDERS .'.order_title'
                        .','. TABLE_BILL_ORDERS .'.followup_status'
						.','. TABLE_BILL_ORDERS .'.tds_status'
						.','. TABLE_BILL_ORDERS .'.id as order_id'
						.','. TABLE_BILL_ORDERS .'.do_o'
						.','. TABLE_BILL_ORDERS .'.is_hardware_ord'
						.','. TABLE_BILL_ORDERS .'.special'
						.','. TABLE_BILL_ORDERS .'.discount_explanation'
						.','. TABLE_BILL_ORDERS .'.npa_status'
                        .','. TABLE_BILL_ORDERS .'.total_tax_paid_amount'
						.','. TABLE_BILL_ORDERS .'.total_tax_balance_amount'
						.','. TABLE_BILL_ORDERS .'.financial_yr'						
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.org'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
						.','. TABLE_CLIENTS .'.email_1 AS c_email_1'
						.','. TABLE_CLIENTS .'.email_2 AS c_email_2'
                        .','. TABLE_CLIENTS .'.mobile1'
                        .','. TABLE_CLIENTS .'.mobile2'
                        .','. TABLE_CLIENTS .'.billing_name AS billing_name'
                        .','. TABLE_CLIENTS .'.status AS c_status';
            $total	= InvoiceProforma::getDetails( $db, $list, '', $condition_query);        
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');            
            $list	= NULL;
            InvoiceProforma::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){               
               $val['is_rcpt_created'] = false;
               $val['is_paid_fully'] = false;
               
				//check invoice id followup in ST BOF
				$val['ticket_id']='';
				$sql2 = "SELECT ticket_id FROM ".TABLE_ST_TICKETS." WHERE 
				(".TABLE_ST_TICKETS.".flw_ord_id = '".$val['order_id']."') AND 
				".TABLE_ST_TICKETS.".ticket_child =0  LIMIT 0,1";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                       while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                       }
                    }                   
                }
				//check invoice id followup in ST EOF
				$is_invoice_created=0;
				$sql= " SELECT COUNT(*) as count FROM ".TABLE_BILL_INV." WHERE ".TABLE_BILL_INV.".or_no
				='".$val['or_no']."' AND 
				".TABLE_BILL_INV.".status !='".Invoice::DELETED."' LIMIT 0,1";
                $db->query($sql);               
                if ( $db->nf()>0 ) {
                    while ( $db->next_record() ) {
                        $count = $db->f('count') ;
                        if( $count > 0){
                            $is_invoice_created = 1;
                        }
                    }
                }
				$val['is_invoice_created'] = $is_invoice_created ;
				$val['tax_amount'] = $val['tax1_total_amount'] + $val['tax1_sub1_total_amount'] + $val['tax1_sub2_total_amount'] ;
				$val['tax_amount'] = number_format($val['tax_amount'],2);
                $val['sub_total_amount']=number_format($val['sub_total_amount'],2);
				$val['amount_numeric'] = $val['amount'];
                $val['amount']=number_format($val['amount'],2);
                $val['amount_inr']=number_format($val['amount_inr'],2);
				/* if(!empty($val['inv_no'])){
					$val['balance']=number_format($val['balance'],2);
					$val['balance_inr']=number_format($val['balance_inr'],2);
				}else{					   
					$val['balance']=number_format($val['prfm_balance'],2);
					$val['balance_inr']=number_format($val['prfm_balance_inr'],2);
				} */
                $val['balance']=number_format($val['prfm_balance'],2);
				$val['balance_inr']=number_format($val['prfm_balance_inr'],2);
				$val['rcpt_amount'] = $val['amount_numeric'] - $val['prfm_balance'] ; 
				
				$val['tax1_value1'] = trim($val['tax1_value']);
				$val['tax1_value1'] = strstr($val['tax1_value1'], '%', true);
				
				$val['tax1_sub1_value1'] = trim($val['tax1_sub1_value']);
				 $val['tax1_sub1_value1'] = strstr($val['tax1_sub1_value1'], '%', true);
				
				$val['tax1_sub2_value1'] = trim($val['tax1_sub2_value']);
				$val['tax1_sub2_value1'] = strstr($val['tax1_sub2_value1'], '%', true);
				$sub1 = (float) ($val['tax1_sub1_value1']/$val['tax1_value1']);
				$sub2 = (float) ($val['tax1_sub2_value1']/$val['tax1_value1']);
				$dFac=100+$val['tax1_value1'] + $sub1 + $sub2 ;
				$mFact = $val['tax1_value1'];
				$m1Fact = $val['tax1_sub1_value1']/$val['tax1_value1'] ;
				$m2Fact = $val['tax1_sub2_value1']/$val['tax1_value1'] ;
				$tax= ($val['rcpt_amount']/$dFac)*$mFact ;
				$subtax1= ($val['rcpt_amount']/$dFac)*$m1Fact ;
				$subtax2= ($val['rcpt_amount']/$dFac)*$m2Fact ;
				$val['rcpt_amount_payable_tax'] = ($tax + $subtax1 + $subtax2);
				$val['rcpt_amount_payable_tax'] = round($val['rcpt_amount_payable_tax'],2);
                $sql= " SELECT id as rcpt_id FROM ".TABLE_BILL_RCPT." WHERE ".TABLE_BILL_RCPT.".inv_profm_id
				='".$val['id']."' AND ".TABLE_BILL_RCPT.".status ='".Receipt::ACTIVE."' LIMIT 0,1";
                $db->query($sql);   
				$is_rcpt_created = 0;				
                if ( $db->nf()>0 ) {
                    while ( $db->next_record() ) {
                        $rcpt_id = $db->f('rcpt_id') ;
                        if( $rcpt_id > 0){
                            $is_rcpt_created = 1;
                        }
                    }
                }
			    $val['is_rcpt_created'] = $is_rcpt_created;
                $flist[$key]=$val;
            }
        }
        //BOF DOWNLOAD CSV
	 
	    
	   
        InvoiceProforma::getCompany($db,$company);
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_send_invoice'] = false;
        $variables['can_followup']= false;
        $variables['can_create_inv']= false;
		
		$variables['can_add_ord'] = false;
		$variables['can_print_lbl'] = false;
		$variables['can_mark_flw'] = false;
		$variables['can_mark_tds'] = false;
		$variables['can_mark_npa'] = false;
        if ( $perm->has('nc_bl_invprfm_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_bl_invprfm_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_bl_invprfm_edit') ) {
           $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_bl_invprfm_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_bl_invprfm_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_bl_invprfm_status') ) {
            $variables['can_change_status']     = true;
        }
        if ( $perm->has('nc_bl_invprfm_send') ) {
            $variables['can_send_invoice']     = true;
        }
		if ( $perm->has('nc_bl_invprfm_taxinv') ) {
            $variables['can_create_inv']     = true;
        }
		 
        if ( $perm->has('nc_st') && $perm->has('nc_st_flw_inv') ) {
            $variables['can_followup'] = true;
        }
        if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_add') ) {
            $variables['can_add_ord'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_flw_status') ) {
            $variables['can_mark_flw'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_tds_status') ) {
            $variables['can_mark_tds'] = true;
        }
		if ($perm->has('nc_uc') && $perm->has('nc_uc_lblprn') ) {
            $variables['can_print_lbl'] = true;
        }
		if ($perm->has('nc_bl_or') && $perm->has('nc_bl_or_npa_status') ) {
            $variables['can_mark_npa'] = true;
        }
		$variables['tdsflw'] = Order::FOLLOWUPTDS;
		$variables['ceoflw'] = Order::FOLLOWUPCEO;
		$variables['noneflw'] = Order::FOLLOWUPPENDING;
		$variables['activeflw'] = Order::FOLLOWUPACTIVE;
		
		$variables['blocked'] = InvoiceProforma::BLOCKED;
		$variables['active'] = InvoiceProforma::ACTIVE;
		$variables['pending'] = InvoiceProforma::PENDING;
		$variables['deleted'] = InvoiceProforma::DELETED;
		$variables['completed'] = InvoiceProforma::COMPLETED;
		$variables['activenpa'] = Order::NPAACTIVE;
		$variables['pendingnpa'] = Order::NPADEACTIVE;
		$variables['x'] = $x; 
		$variables['proinv_html'] = DIR_WS_INVPROFM_HTML_FILES;
		$variables['proinv_pdf'] = DIR_WS_INVPROFM_PDF_FILES; 
        $page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount');
        $page["var"][] = array('variable' => 'totalAmountInr', 'value' => 'totalAmountInr');		
        $page["var"][] = array('variable' => 'totalBalance', 'value' => 'totalBalance');
        $page["var"][] = array('variable' => 'totalBalanceInr', 'value' => 'totalBalanceInr');
        $page["var"][] = array('variable' => 'total', 'value' => 'total');
        $page["var"][] = array('variable' => 'taxArr', 'value' => 'taxArr');
		$page["var"][] = array('variable' => 'company', 'value' => 'company');
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-profm-list.html');
		 
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>