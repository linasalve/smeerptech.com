<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");

    include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/service-tax-price.inc.php' );
	
    $tax_id = isset($_GET['tax_id']) ? $_GET['tax_id'] : '';
    
   	$taxDetails = '' ;
    if (isset($tax_id) && !empty($tax_id))
    {
		//Get tax value list BOF
		$tax_vlist    = NULL;
		$condition_queryvt= " WHERE status = '". ACTIVE ."' ORDER BY tax_price ASC";
		ServiceTaxPrice::getList($db, $tax_vlist, 'id,tax_price,tax_percent', $condition_queryvt);
		//Get tax value list EOF
		$taxpOpt='';
		if(!empty($tax_vlist)){
			$taxpOpt="<option value=''> </option>";
			foreach($tax_vlist as $key=>$val){
				$taxpOpt.= "<option value=".$val['tax_percent'].">".$val['tax_percent']."</option>";
			}
		}
        
		 //Get tax list BOF
		$tax_list    = NULL;
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." ORDER BY tax_name ASC";
		ServiceTax::getList($db, $tax_list, 'id,tax_name', $condition_queryst);
        $taxnOpt='';
		if(!empty($tax_list)){
			$taxnOpt="<option value=''> </option>";
			foreach($tax_list as $key1=>$val1){
				$tax_name =$val1['tax_name'];
				$id =$val1['id'];
				$mergeTxt = $tax_name."#".$id ;
				$taxnOpt.= "<option value=\"".$mergeTxt."\">".$tax_name."</option>";
				
				
			}
			
		}
		
		$count =count($tax_list);
		
		$text='';
		if(!empty($tax_list)){
			$text="<table>";
			foreach($tax_list as $key2=>$val2){
				$i=$key2+1;
				$text.="<tr>";
				$text.="<td><select name=\"tax1_sub".$i."_name\" class=\"inputbox\">".$taxnOpt."</select> </td>";
				$text.="<td><select name=\"tax1_sub".$i."_value\" class=\"inputbox\">".$taxpOpt."</select> </td>";
				$text.="</tr>";
			}
			$text.="<table>";
		}
   	   echo $text;
	}	
include_once( DIR_FS_NC ."/flush.php");

?>
