<?php
if ( $perm->has('nc_bl_rcpt_add') ) {
        
    $rcpt_id = isset($_GET["rcpt_id"]) ? $_GET["rcpt_id"] : ( isset($_POST["rcpt_id"]) ? $_POST["rcpt_id"] : '' );
    $inv_id = isset($_GET["inv_id"]) ? $_GET["inv_id"] : ( isset($_POST["inv_id"]) ? $_POST["inv_id"] : '' );
	$inv_profm_id = isset($_GET["inv_profm_id"]) ? $_GET["inv_profm_id"] : ( isset($_POST["inv_profm_id"]) ? 
	$_POST["inv_profm_id"] : '' );
    $access_level = $my['access_level'];
       
	include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
	include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	$extra = array( 'db' 		    => &$db,
                    'access_level'  => $access_level,
                    'messages' 	    => $messages
                   );
        
     $condition_query =  " LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_RCPT .".client ". 
    $condition_query .= " WHERE (". TABLE_BILL_RCPT.".id = '". $rcpt_id ."')";

		$fields = TABLE_BILL_RCPT .'.*'
		.','. TABLE_CLIENTS .'.user_id AS c_user_id'
		.','. TABLE_CLIENTS .'.number AS c_number'
		.','. TABLE_CLIENTS .'.f_name AS c_f_name'
		.','. TABLE_CLIENTS .'.l_name AS c_l_name'
		.','. TABLE_CLIENTS .'.billing_name'
		.','. TABLE_CLIENTS .'.email AS c_email'
		.','. TABLE_CLIENTS .'.status AS c_status';
                
    if ( Receipt::getList($db, $rcptDetails, $fields, $condition_query) > 0 ) {
		$data =  $rcptDetails[0];
	
		$data['client'] = array('number'=>$data['c_number']);
		$data['voucher_total_amount'] = number_format($data['voucher_total_amount'],2);
		$invoice_id = 0;
		if(!empty($data['inv_profm_id'])){
			
			$sql = " SELECT ".TABLE_BILL_INV.".id,".TABLE_BILL_INV.".number, ".TABLE_BILL_INV.".amount,"
			.TABLE_BILL_INV.".do_i,".TABLE_BILL_INV.".inv_profm_no
			FROM ".TABLE_BILL_INV." WHERE 
			".TABLE_BILL_INV.".inv_profm_id = '".$data['inv_profm_id']."' AND status NOT IN 
			('".Invoice::DELETED."','".Invoice::BLOCKED."' ) ORDER BY id DESC LIMIT 1";
			if ( $db->query($sql) ) {
				if ( $db->nf() > 0 ) {
					while($db->next_record()){
					   $invoice_id = $db->f('id');
					   $data['inv_no']= $inv_number = $db->f('number');
					   $data['do_i']= $db->f('do_i');
					   $data['inv_amount']= $db->f('amount');
					   $data['inv_profm_no']= $db->f('inv_profm_no');
					   $data['inv_amount'] = number_format($data['inv_amount'],2);
					   
					}
				}
			}
		}
		if($invoice_id>0 && empty($data['inv_id'])){
			
			$extra = array( 's'         => &$s,
							'messages'  => &$messages
						);
			$attch = '';
			if(!empty($data['inv_no'])){
			
						 
				$data['amount'] = $data['amount'];			 
				$data['do_pay_received'] = $data['do_pay_received'];			 
				$data['do_rs_symbol'] = strtotime('2010-07-20');
				$data['amount_words']=processSqlData($data['amount_words']);
                $data['pay_branch']=processSqlData($data['pay_branch']);
                $data['p_details']=processSqlData($data['p_details']);
                $data['pay_cheque_no']=processSqlData($data['pay_cheque_no']);
				$temp = explode(' ', $data['do_r']);
				$temp1 = explode('-', $temp[0]);
                $data['do_r'] = mktime(0, 0, 0, $temp1[1], $temp1[2], $temp1[0]);
				$data['do_r_chk'] = $data['do_r'];		 
				if($data['do_pay_received'] =='0000-00-00'){
                    $data['do_pay_received'] ='';
                }
				//mdy y-m-d
				//$balance['balance']  = $data['profm_invoice_closing_balance'] ;
                //$balance['balance_inr'] =    $data['profm_invoice_closing_balance_inr'];
				if($data['p_mode'] == '2' ){
					//ie p_mode is cash
					 $data['pay_bank_company_name'] ='';
				}
				$receipt_list = array();
                $required_fields = TABLE_BILL_RCPT .".id"
							.",". TABLE_BILL_RCPT .".number"
							.",". TABLE_BILL_RCPT .".amount"
							.",". TABLE_BILL_RCPT .".amount_inr"
							.",". TABLE_BILL_RCPT .".discount"
							.",". TABLE_BILL_RCPT .".do_r";
			
				$condition_query = " WHERE inv_profm_id = '". $data['inv_profm_id'] ."' 
				AND status!='".Receipt::DELETED."'"
								." AND id <= ".$rcpt_id." ORDER BY do_r ASC ";
				Receipt::getList($db, $receipt_list, $required_fields, $condition_query);
                $rptList=array();
				if(!empty($receipt_list)){
					foreach($receipt_list as $keyRpt=>$valRpt){
						$valRpt2['id'] = $valRpt['id'];
						$valRpt2['number'] = $valRpt['number'];
						$valRpt2['amount'] = number_format($valRpt['amount'],2);
						$valRpt2['amount_inr'] = number_format($valRpt['amount_inr'],2);
						$valRpt2['do_r'] = $valRpt['do_r'];
						$rptList[$keyRpt] = $valRpt2;
					}
				}
			   
			    $statusSql='';
			    $balance['balance']  = $data['profm_invoice_closing_balance'] ;
			    $balance['balance_inr'] =    $data['profm_invoice_closing_balance_inr'];           
				
				// code to show history of all receipts againce invoice BOF
				$data['historyrcp'] = $rptList;
				
				//commented now $data['invbalance'] = $balance['amount'];
				$data['invbalance'] = number_format($balance['balance'],2);
				// code to show history of all receipts againce invoice EOF  
				// Format the amount
				$data['amount'] = number_format($data['amount'],2);
				
				if ( ($attch = Receipt::createFile($data, 'HTML', $extra)) ) {
					
					
					$query	= " UPDATE ".TABLE_BILL_RCPT
					." SET ". TABLE_BILL_RCPT .".inv_no = '". $data['inv_no'] ."'"
					.",". TABLE_BILL_RCPT .".inv_id = '".$invoice_id ."'
					WHERE id='".$rcpt_id."'" ;
					
					$db->query($query);
					//In Local PDF can not generate bof
					Receipt::createRptPdfFile($data);
					//In Local PDF can not generate eof
					$messages->setOkMessage("The Receipt PDF is created.");
				}
				
			}
			$file_name = DIR_FS_RPT_PDF_FILES ."/". $data["number"] .".pdf";
			// Send the notification mails to the concerned persons.
			
		}else{
			if($invoice_id==0){
				$messages->setErrorMessage("The Invoice is not created.");
			}
			if(!empty($data['inv_id'])){
				$messages->setErrorMessage("The Receipt is created.");
			}
		} 
	}else{
		$messages->setErrorMessage("The Receipt was not found.");
	}    	
			
    // Display the  list.
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-receipt-pdf-create.html');
	
}else {
	$messages->setErrorMessage("You do not have the Right to Change Status.");
}
?>
