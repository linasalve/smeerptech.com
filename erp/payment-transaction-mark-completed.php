<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    

if ( $perm->has('nc_p_pt_edit') ) {  
	include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
    $_ALL_POST =null;
    if(array_key_exists('btnGo',$_GET)){
        $data       = processUserData($_GET);
		 
         $transaction_id =$data['transaction_id'];
         $status =$data['status'];
         //$do_tr_k = 'do_transaction'.$transaction_id ;
        // $do_tr_k = 'do_transaction'.$transaction_id ;
        $do_transaction =$data['do_transaction'];
        if(!empty($do_transaction)){ 
			$data['do_transaction'] = explode('/', $do_transaction);
			$data['do_transaction'] = mktime(0, 0, 0, $data['do_transaction'][1], $data['do_transaction'][0], $data['do_transaction'][2]);
			$data['do_transaction'] = date('Y-m-d H:i:s', $data['do_transaction']);
        }
		
		$do_voucher =$data['do_voucher'];
        if(!empty($do_voucher)){ 
			$data['do_voucher'] = explode('/', $do_voucher);
			$data['do_voucher'] = mktime(0, 0, 0, $data['do_voucher'][1], $data['do_voucher'][0], $data['do_voucher'][2]);
			$data['do_voucher'] = date('Y-m-d H:i:s', $data['do_voucher']);
        }		
		
        $fields_r = TABLE_PAYMENT_TRANSACTION .'.*'  ;            
        $condition_query_r = " WHERE (". TABLE_PAYMENT_TRANSACTION .".id = '". $transaction_id ."' )";
        Paymenttransaction::getDetails($db, $recData, $fields_r, $condition_query_r);
        if(!empty($recData)){
            $recData =$recData['0'];
        }
        
        $tr_db_bank_amt=$tr_cr_bank_amt=$tr_cr_sql=$tr_db_sql='';                
        // FOR Paymenttransaction::PAYMENTIN
        if(isset($recData["transaction_type"]) && $recData['transaction_type'] ==  Paymenttransaction::PAYMENTIN ){
            if( ($recData['status'] != $data['status']) && !empty($data['credit_pay_bank_id'])){
                if($data['status'] == Paymenttransaction::COMPLETED){
					
                    $dbnew=new db_local();  					   
					 $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                       ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$recData['credit_pay_amt']." 
					   WHERE id='".$data['credit_pay_bank_id']."'";    
					   
                    $executed = $dbnew->query($update_query);
                    //Get balance amount after transaction BOF
                     $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['credit_pay_bank_id'];
                    $db->query($sql);
                    if($db->nf()>0 )
                    {
                        while ($db->next_record()) {
                            $tr_cr_bank_amt = $db->f('balance_amt');
                            $tr_cr_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_cr_bank_amt = '".$tr_cr_bank_amt."',
							". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id = '".$data['credit_pay_bank_id']."'" ;
                        }
                    }
                    //Get balance amount after transaction EOF
                    
                    if($executed){
                        $messages->setOkMessage("Your Amount Details has been updated.");
                    }else{
                        $messages->setErrorMessage("Sorry! Amount Details can not be updated.");
                    }
                }            
            }        
        }                
         // FOR Paymenttransaction::PAYMENTOUT BOF
         
        if(isset($recData["transaction_type"]) && $recData['transaction_type'] ==  Paymenttransaction::PAYMENTOUT ){
        
            if( ($recData['status'] != $data['status']) && !empty($data['debit_pay_bank_id'])){
                if($data['status'] == Paymenttransaction::COMPLETED){
                                            
                    /* -ve amt allowed
					$dbnew=new db_local();                    	
                    $retrieve_query= "SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id='".$recData['debit_pay_bank_id']."'";
                    $dbnew->query($retrieve_query);
                    $executed = 0 ;
                    $balance = 0 ;
                    if($dbnew->nf()>0 ){
                        while ($dbnew->next_record()) {
                            $balance =$dbnew->f('balance_amt');
                        }
                    }                    
                    if($balance < $recData['debit_pay_amt']){
                        $messages->setErrorMessage("Your balance amount is less than the amount you want to debit.");
                    }
                    else{
                        $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                        ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$recData['debit_pay_amt']." WHERE id='".$recData['debit_pay_bank_id']."'";
                        $executed = $dbnew->query($update_query);
                    } 
					*/
                    //Get balance amount after transaction BOF
                    $dbnew=new db_local();
					$update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                        ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$recData['debit_pay_amt']." 
						WHERE id='".$data['debit_pay_bank_id']."'";
                        $executed = $dbnew->query($update_query);
                    
                    $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['debit_pay_bank_id'];
                    $db->query($sql);
                    if($db->nf()>0 ){
                        while($db->next_record()){
                            $tr_db_bank_amt = $db->f('balance_amt');
                            $tr_db_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_db_bank_amt = '".$tr_db_bank_amt."',
							". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = '".$data['debit_pay_bank_id']."'"   ;
                        } 
                    }
                    //Get balance amount after transaction EOF
                    if($executed){
                        $messages->setOkMessage("Your Amount Details has been updated.");
                    }else{
                        $messages->setErrorMessage("Sorry! Amount Details can not be updated.");
                    }                        
                }            
            }        
        }
        // FOR Paymenttransaction::PAYMENTOUT EOF
        
        // FOR Paymenttransaction::INTERNAL BOF
        if(isset($recData["transaction_type"]) && $recData['transaction_type'] ==  Paymenttransaction::INTERNAL ){
        
            if(($recData['status'] != $data['status']) && !empty($data['debit_pay_bank_id']) && 
			!empty($data['credit_pay_bank_id']) ){
                if($data['status'] == Paymenttransaction::COMPLETED){
                        
                       /*  
					    $dbnew=new db_local();                    	
                        $retrieve_query= "SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE 
						id='".$recData['debit_pay_bank_id']."'";
                        $dbnew->query($retrieve_query);
                        $executed = 0;
                        $balance = 0;
                        if($dbnew->nf()>0 ){
                            while ($dbnew->next_record()) {
                                $balance =$dbnew->f('balance_amt');
                            }
                        }
                        
                        if($balance < $recData['debit_pay_amt']){
                            $messages->setErrorMessage("Your balance amount is less than the amount you want to debit.");
                        }
                        else{
                            $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$recData['debit_pay_amt']." WHERE id='".$recData['debit_pay_bank_id']."'";
                            $executed = $dbnew->query($update_query);                                   
                            
                                                
                            $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                               ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$recData['credit_pay_amt']." WHERE id='".$recData['credit_pay_bank_id']."'";
                            $executed = $dbnew->query($update_query);
                        }
						*/						
						
						$dbnew=new db_local();  
						$update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                        ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$recData['debit_pay_amt']."
							WHERE id='".$data['debit_pay_bank_id']."'";
                        $executed = $dbnew->query($update_query);                                   
                            
                                                
                        $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$recData['credit_pay_amt']." 
							WHERE id='".$data['credit_pay_bank_id']."'";
                            $executed = $dbnew->query($update_query);

						
                        //Get balance amount after transaction BOF
                        $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['credit_pay_bank_id'];
                        $db->query($sql);
                        if($db->nf()>0 ){
                            while ($db->next_record()) {
                                $tr_cr_bank_amt = $db->f('balance_amt');
                                $tr_cr_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_cr_bank_amt = '".$tr_cr_bank_amt."' ,
								". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id = '".$data['credit_pay_bank_id']."'"   ;
                            }
                        }
                        
                        $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['debit_pay_bank_id'];
                        $db->query($sql);
                        if($db->nf()>0 )
                        {
                            while ($db->next_record()) {
                                $tr_db_bank_amt = $db->f('balance_amt');
                                $tr_db_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_db_bank_amt = '".$tr_db_bank_amt."',
								". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = '".$data['debit_pay_bank_id']."'"   ;
								
                            }
                        }
                        //Get balance amount after transaction EOF
                        
                        if($executed){
                            $messages->setOkMessage("Your Amount Details has been updated.");
                        }else{
                            $messages->setErrorMessage("Sorry! Amount Details can not be updated for ".$recData["transaction_id"]);
                        }
                }                    
            }                
        }                
        // FOR Paymenttransaction::INTERNAL EOF  
         if( $messages->getErrorMessageCount() <= 0){
                $tr_st_sql='';
                if( $data['status'] != $recData['status']){
                    $tr_st_sql = ",". TABLE_PAYMENT_TRANSACTION .".do_status_update = '".date("Y-m-d")."'
					,". TABLE_PAYMENT_TRANSACTION .".status_update_by = '".$my['uid']."'
					,". TABLE_PAYMENT_TRANSACTION .".status_update_ip = '".$_SERVER['REMOTE_ADDR']."'" ;
                }
                $query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION
                        ." SET ". TABLE_PAYMENT_TRANSACTION .".status = '". $data['status'] ."'"   
						.",". TABLE_PAYMENT_TRANSACTION .".do_transaction = '".$data['do_transaction'] ."'"      
						//.",". TABLE_PAYMENT_TRANSACTION .".do_voucher = '".$data['do_voucher'] ."'"      
						.$tr_st_sql
						.$tr_cr_sql    
						.$tr_db_sql                                    
                        ." WHERE id = '". $transaction_id ."'";
               
			   $db->query($query);
                $statusName =$goBtn=$editBtn= '';
				if($data['status'] == Paymenttransaction::COMPLETED){
					$statusName = 'COMPLETED';
					$goBtn = "";
					$editBtn .= "<img src=\"".addslashes($variables['nc_images'])."/edit-off.gif\" border=\"0\" 
                                title=\"Edit\" name=\"Edit\" alt=\"Edit\"/> ";
								
				}elseif($data['status'] == Paymenttransaction::PENDING){
					$statusName = 'PENDING';
					$goBtn = "<a href=\"#\" onClick=\"updateStatus(".$transaction_id.")\">GO</a> ";
					$goBtn = '';
					$editBtn .= "<img src=\"".addslashes($variables['nc_images'])."/edit-off.gif\" border=\"0\" 
                                title=\"Edit\" name=\"Edit\" alt=\"Edit\"/> ";
								
				}elseif($data['status'] == Paymenttransaction::CANCELLED_P){				
					$statusName = 'CANCELLED_P';
					$goBtn = " ";
					$editBtn .= "<img src=\"".addslashes($variables['nc_images'])."/edit-off.gif\" border=\"0\" 
                                title=\"Edit\" name=\"Edit\" alt=\"Edit\"/> ";
				}
				
                $message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Status updated successfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					$message .= "|".$statusName;
					$message .= "|".$do_transaction;
					$message .= "|".$do_voucher;
					$message .= "|".$goBtn;
					$message .= "|".$editBtn;
					$message .= "|".$tr_db_bank_amt;
					$message .= "|".$tr_cr_bank_amt;
					
					
					echo $message;
					 
					/*
					$message .= "| <img src=\"".addslashes($variables['nc_images'])."/completed-on.gif\" border=\"0\" 
                                title=\"Completed\" name=\"Completed\" alt=\"Completed\"/> ";
					$message .= "| ".date('j M Y')."<b> Completion Mark By </b>".$my['f_name']." ".$my['l_name'] ;
					$message .= "| <img src=\"".addslashes($variables['nc_images'])."/edit-off.gif\" border=\"0\" 
                                title=\"Edit\" name=\"Edit\" alt=\"Edit\"/> ";
					$message .= "| <img src=\"".addslashes($variables['nc_images'])."/comment-off.gif\" border=\"0\" 
                                title=\"Comment\" name=\"Comment\" alt=\"Comment\"/> ";
					$message .= "| <img src=\"".addslashes($variables['nc_images'])."/delete-off.gif\" border=\"0\" 
                                title=\"Delete\" name=\"Delete\" alt=\"Delete\"/> ";*/
					 
         }        
    }  
   
 
}else {
    //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
	 
	
	
	
	
	
	
}
  
?>