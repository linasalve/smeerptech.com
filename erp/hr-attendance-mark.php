<?php
    if ( $perm->has('nc_hr_att_mark') ) {
	    $att_status= isset($_GET["att_status"]) ? $_GET["att_status"] : ( isset($_POST["att_status"]) ? $_POST["att_status"] : '' );
	    $att_mark= isset($_GET["att_mark"]) ? $_GET["att_mark"] : ( isset($_POST["att_mark"]) ? $_POST["att_mark"] : '' );


        if($my["att_status"] == "0" && $att_mark != "1"){
		    $messages->setErrorMessage("You have not marked your attendence for Today.");	
	    }	
       
        if ( in_array( $att_mark, array( "1", "2", "3" ) ) ) {
            // user has submitted some operation
            /************************************************************
                att_mark					Meaning
                    1				Mark the attendence for today.
                    2				Enter the In Time again. 
                                    (Possible only if the user has entered Time Out)
                    3				Enter the Out Time for the Time In.
            ************************************************************/
    
            //$desynTime = desynchronizeTime(time(), $my["time_offset"]);    
            //$str = DateTime::unixTimeToDateTime($desynTime);
            
            //$str = $datetime->unixTimeToDate( time() );
			$str = date('Y-m-d H:i:s');
			
            if ( $my["att_status"] == "0" && $att_mark == "1" ) {
                        
                // mark users attendence for today.            
                 $query = "INSERT INTO ". TABLE_HR_ATTENDANCE ." SET " 
                            . TABLE_HR_ATTENDANCE.".uid = '". $my["uid"] ."' "
                            .",". TABLE_HR_ATTENDANCE.".att_time_in = '". $str ."' "
                            .",". TABLE_HR_ATTENDANCE.".att_time_out = 0 "
                            .",". TABLE_HR_ATTENDANCE.".status = 1 "
                            .",". TABLE_HR_ATTENDANCE.".created_by = '".$my["uid"]."' "
                            .",". TABLE_HR_ATTENDANCE.".date = '".date('Y-m-d')."' "                            
                            .",". TABLE_HR_ATTENDANCE.".ip = '". $_SERVER['REMOTE_ADDR'] ."'"              
                            .",". TABLE_HR_ATTENDANCE.".att_place = '". OFFICE_PLACE ."' ";
                            
                if (!$db->query( $query ) ) {
                
                    $messages->setErrorMessage("Internal Error: Cannot mark your Attendence."
                                ."<br/>Please try again.");			
                }
                else {
                    $my["att_status"] = "1";
                 
                    $messages->setOkMessage("Your Attendence is marked for today");	
                
                }
            }
            elseif ( $my["att_status"] != "0" && $att_mark == "1" ) {
                // user has already marked his/her attendence.           
                $messages->setOkMessage("You have already marked your Attendence for today");	
            }
            elseif ( $my["att_status"] == "0" && ($att_mark == "2" || $att_mark == "3") ) {
                // users hasn't marked attendence for today
                // and wants to perform other operations           
                            
                $messages->setErrorMessage("Cannot enter your In Time / Out Time as "
                            ."you have not marked your Attendence for today");				
            }
            elseif ( $my["att_status"] == "1" && $att_mark == "3" ) {
                // enter users Out Time.
                //Calculate Worked hrs between intime and outtime bof
                $sql="SELECT att_time_in FROM ".TABLE_HR_ATTENDANCE." WHERE att_id = '". $my["att_id"] ."'";
                $db->query($sql);	
                while ( $db->next_record() ) {
                  $strIn = $db->f('att_time_in');
                }
                $sql= " SELECT TIMEDIFF('".$str."','".$strIn."') as work_hrs";
                $db->query($sql);	
                while ( $db->next_record() ) {
                    $work_hrs = $db->f('work_hrs');
                }
                //Calculate Worked hrs between intime and outtime eof
                
                $query = "UPDATE ". TABLE_HR_ATTENDANCE ." SET "
                            .TABLE_HR_ATTENDANCE.".att_time_out = '". $str ."',"
                            .TABLE_HR_ATTENDANCE.".worked_hrs = '". $work_hrs ."'"
                            ." WHERE att_id = '". $my["att_id"] ."'";
                            
                            
               
                if ( !$db->query( $query ) ) {                  
                    $messages->setErrorMessage("Internal Error: Cannot enter your Out Time."
                                ."<br/>Please try again.");				
                }
                else {                 
                    $my["att_status"] = "2";
                    $messages->setOkMessage("Your Out Time has been entered.");			
                   
                }
            }
            elseif ( $my["att_status"] != "1" && $att_mark == "3" ) {
                // user has not marked attendence for today and 
                // wants to enter the Out Time.
                $messages->setErrorMessage("Cannot enter Out Time."
                            ."<br/>You have not marked you Attendence for Today.");			
            }
            elseif ( $my["att_status"] == "1" && ($att_mark == "1" || $att_mark == "2") ) {
                // users has marked attendence for today and wants 
                // to make another entry again without entering Out Time.
                $messages->setErrorMessage("Cannot mark Attendence / In Time."
                            ." You have not entered Out Time and trying to make new entry.");				
            }
            elseif ( $my["att_status"] == "2" && $att_mark == "2" ) {
                // enter users In Time again for today.
                $query = "INSERT INTO ". TABLE_HR_ATTENDANCE ." SET " 
                            . TABLE_HR_ATTENDANCE.".uid = '". $my["uid"] ."' "
                            .",". TABLE_HR_ATTENDANCE.".att_time_in = '". $str ."' "
                            .",". TABLE_HR_ATTENDANCE.".att_time_out = 0 "
                            .",". TABLE_HR_ATTENDANCE.".status = 1 "
                            .",". TABLE_HR_ATTENDANCE.".created_by = '".$my["uid"]."' "
                            .",". TABLE_HR_ATTENDANCE.".date = '".date('Y-m-d')."' "                            
                            .",". TABLE_HR_ATTENDANCE.".ip = '". $_SERVER['REMOTE_ADDR'] ."'"  
                            .",". TABLE_HR_ATTENDANCE .".do_e = '". date('Y-m-d H:i:s')."'"                            
                            .",". TABLE_HR_ATTENDANCE.".att_place = '". OFFICE_PLACE ."' ";
                            
                if ( !$db->query( $query ) ) {                 
                    $messages->setErrorMessage("Internal Error: Cannot enter your In Time."
                                ."<br/>Please try again.");				
                }
                else {
                    $my["att_status"] = "1";                   
                    $messages->setOkMessage("Your In Time has been entered.");	
                   
                }
            }
            elseif ( $my["att_status"] != "2" && $att_mark == "2" ) {
                // user has not entered Out Time and wants to 
                // enter In Time again withour enterin Out Time.
                $messages->setErrorMessage("Cannot enter In Time."
                            ." You have not entered Out Time and trying to make new entry.");				
            }
            elseif ( $my["att_status"] == "2" && ($att_mark == "1" || $att_mark == "3") ) {
                // user has entered Out Time for today and wants 
                // to mark attendence for today or to enter Out Time.              
                $messages->setErrorMessage("Cannot mark Attendence / enter Out Time."
                            ." You have already entered Out Time and trying to enter again.");				
            }
        }
       
        //Get My Today's attendance List BOF
        $query = "SELECT * "
	              ." FROM ". TABLE_HR_ATTENDANCE 
				  ." WHERE uid = '". $my["uid"] ."'"
				  ." AND DATE_FORMAT(date,'%Y-%m-%d') = '".date('Y-m-d')."'"
				  ." ORDER BY date DESC";
        $worked_hr='';
        $db->query($query);
        $list 	= NULL;
        $data 		= NULL;
        $total_hr  =0;
        $total_min =0;
        $total_sec =0;
        
        while ( $db->next_record() ) {
            $data 	= NULL;
            $data 	= processSqlData( $db->Record );
            
            if($data['att_time_out'] == "0000-00-00 00:00:00"){
                $att_time_out = "0000-00-00 00:00:00";
            }else{
              //$att_time_out =synchronizeTime($datetime->dateToUnixTime($data["att_time_out"]), $my["time_offset"] );
			  $att_time_out = $data["att_time_out"];
            }
            if($datetime->dateToUnixTime($data['att_time_out']) >=$datetime->dateToUnixTime($data['att_time_in'])){
              $diff=$datetime->dateToUnixTime($data['att_time_out'])-$datetime->dateToUnixTime($data['att_time_in']);
                    if( $hours=intval((floor($diff/3600))) )
                        $diff = $diff % 3600;
                    if( $minutes=intval((floor($diff/60))) )
                        $diff = $diff % 60;
                    $diff        =    intval( $diff );
                     
                    $total_hr   = $total_hr   + $hours; 
                    $total_min  = $total_min  + $minutes; 
                    $total_sec  = $total_sec  + $diff; 
                    if($total_sec >= 60){
                         $minutes=intval((floor($total_sec/60)));
                         $total_min = $total_min + $minutes;
                         $total_sec = $total_sec % 60;
                    }
                    if($total_min >= 60){
						 $minutes=intval((floor($total_min/60)));
						 $total_hr = $total_hr + $minutes;
						 $total_min = $total_min % 60;
						 
					}
            }	
            if(!empty($total_hr)){
                if(!empty($total_min)){
                    if($total_min < 10){
                       $worked_hr = $total_hr.":"."0".$total_min;
                    }
                    else{
                        $worked_hr = $total_hr.":".$total_min;
                    }
                }
            }
            $list[] = array("att_id" 	   => $data["att_id"],
							"uid"		   => $data["uid"],
							"date"		   => $data["date"],
						//  "att_time_in"  => synchronizeTime($datetime->dateToUnixTime($data["att_time_in"]), $my["time_offset"] ),
							"att_time_in" => $data["att_time_in"],
							"att_time_out" => $att_time_out,
						   // "worked_hr"    => $worked_hr,
							"worked_hrs"    =>  $data["worked_hrs"],
							"att_place"    => $data["att_place"]
							);
                            
        }
        //Get My Today's attendance List EOF
        
         
        
        
        
         $page["var"][] = array('variable' => 'my', 'value' => 'my');
         $page["var"][] = array('variable' => 'list', 'value' => 'list');
        //$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        //$page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        //$page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-attendance-mark.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this module.");
         //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
