<?php
  if ( $perm->has('nc_pba_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
	    
	    include_once (DIR_FS_INCLUDES .'/user.inc.php');	
	    
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                 
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
            );
                
               
                if ( PaymentBillsAgainst::validateAdd($data, $extra) ) {
                    
                    $query	= " INSERT INTO ".TABLE_PAYMENT_BILLS_AGAINST
                            ." SET ".TABLE_PAYMENT_BILLS_AGAINST .".bill_against = '". $data['bill_against'] ."'"                            .",". TABLE_PAYMENT_BILLS_AGAINST .".vendor_id = '". $data['vendor_id'] ."'"
                            .",". TABLE_PAYMENT_BILLS_AGAINST .".status = '". 		$data['status'] ."'"                            .",". TABLE_PAYMENT_BILLS_AGAINST .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                            .",". TABLE_PAYMENT_BILLS_AGAINST .".created_by = '".           $my['user_id'] ."'"
                            .",". TABLE_PAYMENT_BILLS_AGAINST .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                    
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Record has been added successfully.");
                        $variables['hid'] = $db->last_inserted_id();              
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
		        }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/payment-bills-against.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-bills-against.php");
        }
      
     
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/payment-bills-against.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            //$page["var"][] = array('variable' => 'executivelist', 'value' => 'executivelist');     
            //$page["var"][] = array('variable' => 'party', 'value' => 'party');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-bills-against-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
