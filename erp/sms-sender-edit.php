<?php
if ( $perm->has('nc_sms_api_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $lst_api = null;
        $fields = TABLE_SMS_API.'.id'
                   .','. TABLE_SMS_API .'.gateway_name'
                   .','. TABLE_SMS_API .'.name';
        SmsApi::getList($db,$lst_api,$fields);
		
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( SmsSender::validateUpdate($data, $extra) ) {
               $query  = " UPDATE ". TABLE_SMS_SENDER
                              ." SET ".TABLE_SMS_SENDER .".api_id 		= '". $data['api_id'] ."'"   
                                	.",". TABLE_SMS_SENDER .".sender_name	= '". $data['sender_name'] ."'"     
									.",". TABLE_SMS_SENDER .".status 		= '". $data['status'] ."'"                    
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SMS_SENDER .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SMS_SENDER .".id = '". $id ."' )";
            
            if ( SmsSender::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];  
                $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/sms-sender-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'lst_api', 'value' => 'lst_api');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-sender-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
