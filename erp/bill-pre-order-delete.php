<?php
    if ( $perm->has('nc_bl_po_delete') ) {
	   $po_id   = isset($_GET["po_id"])  ? $_GET["po_id"]   : ( isset($_POST["po_id"])  ? $_POST["po_id"] : '' );
        
        $access_level = $my['access_level'];
        /*if ( $perm->has('nc_bl_po_delete_al') ) {
            $access_level += 1;
        }*/
    
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        PreOrder::delete($po_id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/bill-pre-order-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the Pre Order.");
    }
?>
