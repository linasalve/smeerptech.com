<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_uv_edit') ) {
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
    
        include_once ( DIR_FS_CLASS .'/RegionVendor.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneVendor.class.php');
        include_once ( DIR_FS_CLASS .'/ReminderVendor.class.php');
        //include ( DIR_FS_INCLUDES .'/sale-industry.inc.php');
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new RegionVendor();
        $phone          = new PhoneVendor(TABLE_VENDORS);
        $reminder       = new ReminderVendor(TABLE_VENDORS);
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_uv_edit_al') ) {
            $access_level += 1;
        }
        //For Vendors list allotment
        if ( $perm->has('nc_uv') && $perm->has('nc_uv_list')) {
            $variables['can_view_clist']     = true;
        }
        
        
        $al_list    = getAccessLevel($db, $access_level);
        //$role_list  = Vendors::getRoles($db, $access_level);

        $grade_list  = Vendors::getGrades();
        
        //BOF read the available services
		$authorization_list  = Vendors::getAuthorization();
        $services_list  = Vendors::getServices();
		$message_list  = Vendors::getMessages();
        //EOF read the available services
        
        //BOF read the available industries
        $industry_list = NULL;
        $required_fields ='*';
        $condition_query="WHERE status='".Industry::ACTIVE."'";
		Industry::getList($db,$industry_list,$required_fields,$condition_query);
        //EOF read the available industries
        
        //BOF read the available industries
		Vendors::getCountryCode($db,$country_code);
        //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder
                        );
            
            if ( Vendors::validateUpdate($data, $extra) ) {
                $service_idstr='';
                if(isset($_POST['service_id']) && !empty($_POST['service_id'])){
                    $service_ids=$_POST['service_id'];
                    $service_idstr=implode(",",$service_ids);
                    $service_idstr = ",".$service_idstr.",";	
                }
                //Default entry remove me 
                /*                
                $service_idstr = Vendors::BILLING.",".Vendors::ST ;    
                if(!empty($service_idstr)){
                    $service_idstr = ",".$service_idstr.",";	
                }*/
                $authorization_idstr='';
                if(isset($_POST['authorization_id']) && !empty($_POST['authorization_id'])){
                    $authorization_ids=$_POST['authorization_id'];
                    $authorization_idstr=implode(",",$authorization_ids);
                    $authorization_idstr = ",".$authorization_idstr.",";	
                }
                $industry_idstr='';
                if(isset($_POST['industry']) && !empty($_POST['industry'])){
                    $industry_ids=$_POST['industry'];
                    $industry_idstr= implode(",",$industry_ids);
                }
                if(!empty($industry_idstr)){
                    $industry_idstr=",".$industry_idstr.",";	
                }
                $alloted_clientsstr='';
                if(isset($_POST['alloted_clients']) && !empty($_POST['alloted_clients'])){
                    $alloted_clients=$_POST['alloted_clients'];
                    $alloted_clientsstr=implode(",",$alloted_clients);
                }
                if(!empty($alloted_clientsstr)){
                    $alloted_clientsstr=",".$alloted_clientsstr.",";
                }
                
               $query  = " UPDATE ". TABLE_VENDORS
							." SET "
                            ."". TABLE_VENDORS .".service_id   = '". $service_idstr ."'"
							.",". TABLE_VENDORS .".authorization_id   = '". $authorization_idstr ."'"
                            /* .",". TABLE_VENDORS .".number      = '". $data['number'] ."'"
                            .",". TABLE_VENDORS .".manager     = '". $data['manager'] ."'"*/
							.",". TABLE_VENDORS .".team			= '". implode(',', $data['team']) ."'"
                            .",". TABLE_VENDORS .".username    = '". $data['username'] ."'"
                            . $data['password']
                            .",". TABLE_VENDORS .".access_level= '". $data['access_level'] ."'"
                            .",". TABLE_VENDORS .".created_by	= '". $my['uid'] ."'"
                            .",". TABLE_VENDORS .".email       = '". $data['email'] ."'"
							.",". TABLE_VENDORS .".message_id 	   	= '". $data['message_id'] ."'"
                            .",". TABLE_VENDORS .".email_1     = '". $data['email_1'] ."'"
                            .",". TABLE_VENDORS .".email_2     = '". $data['email_2'] ."'"
                            .",". TABLE_VENDORS .".email_3     	= '". $data['email_3'] ."'"
                            .",". TABLE_VENDORS .".email_4     	= '". $data['email_4'] ."'"
                            .",". TABLE_VENDORS .".additional_email     = '". $data['additional_email'] ."'"
                            .",". TABLE_VENDORS .".title       = '". $data['title'] ."'"
                            .",". TABLE_VENDORS .".f_name      = '". $data['f_name'] ."'"
                            .",". TABLE_VENDORS .".m_name      = '". $data['m_name'] ."'"
                            .",". TABLE_VENDORS .".l_name      = '". $data['l_name'] ."'"
                            .",". TABLE_VENDORS .".p_name      = '". $data['p_name'] ."'"     
							.",". TABLE_VENDORS .".bank_account_no     = '". $data['bank_account_no'] ."'"							  .",". TABLE_VENDORS .".grade      	= '". $data['grade'] ."'"
                            .",". TABLE_VENDORS .".credit_limit = '". $data['credit_limit'] ."'"
                            .",". TABLE_VENDORS .".desig       = '". $data['desig'] ."'"
							.",". TABLE_VENDORS .".cst_no       = '". $data['cst_no'] ."'"
                            .",". TABLE_VENDORS .".vat_no       = '". $data['vat_no'] ."'"
                            .",". TABLE_VENDORS .".service_tax_no  = '". $data['service_tax_no'] ."'"
                            .",". TABLE_VENDORS .".pan_no  		= '". $data['pan_no'] ."'"	
                            .",". TABLE_VENDORS .".org         = '". $data['org'] ."'"
                            .",". TABLE_VENDORS .".domain      = '". $data['domain'] ."'"
                            .",". TABLE_VENDORS .".gender      = '". $data['gender'] ."'"
                            .",". TABLE_VENDORS .".do_birth    = '". $data['do_birth'] ."'"
                            .",". TABLE_VENDORS .".do_aniv     = '". $data['do_aniv'] ."'"
                            .",". TABLE_VENDORS .".do_reg     = '". $data['do_reg'] ."'"
                            .",". TABLE_VENDORS .".remarks     = '". $data['remarks'] ."'"
                            .",". TABLE_VENDORS .".industry     = '". $industry_idstr ."'"
                            .",". TABLE_VENDORS .".wt_you_do    = '". $data['wt_you_do'] ."'"
                            .",". TABLE_VENDORS .".mobile1      = '". $data['mobile1'] ."'"
                            .",". TABLE_VENDORS .".mobile2      = '". $data['mobile2'] ."'"
                            .",". TABLE_VENDORS .".alloted_clients   = '". $alloted_clientsstr ."'"
                            .",". TABLE_VENDORS .".billing_name   = '". $data['billing_name'] ."'"
                           .",". TABLE_VENDORS .".spouse_dob   = '". $data['spouse_dob'] ."'"
                            .",". TABLE_VENDORS .".status      = '". $data['status'] ."'" 
                        ." WHERE ". TABLE_VENDORS .".user_id   = '". $data['user_id'] ."'";
                
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['user_id'];

                    // Update the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        if ( isset($data['a_remove'][$i]) && $data['a_remove'][$i] == '1') {
                            if ( ! $region->delete($db, $data['address_id'][$i], $variables['hid'], TABLE_VENDORS) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setOkMessage($errors['description']);
                                }
                            }
                        }
                        else {
                            $address_arr = array(   'id'            => isset($data['address_id'][$i])? $data['address_id'][$i] : '0',
                                                    'address_type'  => $data['address_type'][$i],
                                                    'company_name'  => '',
                                                    'address'       => $data['address'][$i],
                                                    'city'          => $data['city'][$i],
                                                    'state'         => $data['state'][$i],
                                                    'country'       => $data['country'][$i],
                                                    'zipcode'       => $data['zipcode'][$i],
                                                    'is_preferred'  => isset($data['is_preferred'][$i])? $data['is_preferred'][$i] : '0',
                                                    'is_verified'   => isset($data['is_verified'][$i])? $data['is_verified'][$i] : '0'
                                                );
                            
                            if ( !$region->update($variables['hid'], TABLE_VENDORS, $address_arr) ) {
                                foreach ( $region->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_VENDORS) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $phone_arr = array( 'id'            => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
                                                    'p_type'        => $data['p_type'][$i],
                                                    'cc'            => $data['cc'][$i],
                                                    'ac'            => $data['ac'][$i],
                                                    'p_number'      => $data['p_number'][$i],
                                                    'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
                                                    'p_is_verified' => isset($data['p_is_verified'][$i]) ? $data['p_is_verified'][$i] : '0'
                                                );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_VENDORS, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Update the Reminders.
                    for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['remind_date_'.$i]) ) {
                            if ( isset($data['r_remove_'.$i]) && $data['r_remove_'.$i] == '1') {
                                if ( ! $reminder->delete($db, $data['remind_id_'.$i], $variables['hid'], TABLE_VENDORS) ) {
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $reminder_arr = array(  'id'            => (isset($data['remind_id_'.$i])? $data['remind_id_'.$i] : ''),
                                                        'do_reminder'   => $data['remind_date_'.$i],
                                                        'description'   => $data['remind_text_'.$i]
                                                    );
                                if ( ! $reminder->update($db, $variables['hid'], TABLE_VENDORS, $reminder_arr) ) {
                                    $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Send The Email to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_uv_add_notif', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_uv_add_notif_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    /*$manager = Vendors::getManager($db, '', $data['manager'], 'f_name,l_name,email');
                    $data['manager_name']   = $manager['f_name'] .' '. $manager['l_name'];
                    $data['manager_email']  = $manager['email'];
                    $data['manager_phone']  = '';
                    */
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link']       = DIR_WS_NC .'/vendors.php?perform=view_details&user_id='. $data['user_id'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'VENDORS_INFO_UPDATE_MANAGERS', $data, $email) ) {
                            $to     = '';
                            //echo " subject : ".$email['subject'];
                            //echo "<br/> body : ".$email['body'];
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }                        
                    }
                    
                    // Send Email to the Client whose infromation was updated.
                    /*
                    $data['link']       = DIR_WS_NC;
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'CLIENT_INFO_UPDATE', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['f_name'] .' '. $data['l_name'], 'email' => $data['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                   
                    */
                    $messages->setOkMessage("Vendors Profile has been updated.");
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Vendors Profile was not updated.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/vendors-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'            => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
                
                // Preserve the Addresses.
                $count = count($_ALL_POST['city']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['address_list'][] = array(   'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                //'company_name'   => isset($_ALL_POST['company_name'][$i])? $_ALL_POST['company_name'][$i]:'',
                                                                'is_verified'   => isset($_ALL_POST['is_verified'][$i])? $_ALL_POST['is_verified'][$i]:'',
                                                                'is_preferred'  => isset($_ALL_POST['is_preferred'][$i])? $_ALL_POST['is_preferred'][$i]:'',
                                                                'address_type'  => isset($_ALL_POST['address_type'][$i])? $_ALL_POST['address_type'][$i]:'',
                                                                'address'       => isset($_ALL_POST['address'][$i])? $_ALL_POST['address'][$i]:'',
                                                                'city'          => isset($_ALL_POST['city'][$i])? $_ALL_POST['city'][$i]:'',
                                                                'state'         => isset($_ALL_POST['state'][$i])? $_ALL_POST['state'][$i]:'',
                                                                'zipcode'       => isset($_ALL_POST['zipcode'][$i])? $_ALL_POST['zipcode'][$i]:'',
                                                                'country'       => isset($_ALL_POST['country'][$i])? $_ALL_POST['country'][$i]:'',
                                                     );
//                    }
                }

                
                // Preserve the Reminders.
                $count = $_ALL_POST['reminder_count'];
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['remind_date_'.$i) && !empty($_ALL_POST['remind_date_'.$i) ) {
                        $_ALL_POST['reminder_list'][] = array(  'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'do_reminder'   => isset($_ALL_POST['remind_date_'.$i])? $_ALL_POST['remind_date_'.$i]:'',
                                                                'description'   => isset($_ALL_POST['remind_text_'.$i])? $_ALL_POST['remind_text_'.$i]:'',
                                                     );
//                    }
                }
                
                if ( !empty($_ALL_POST['spouse_dob']) ) {
                    $temp = explode('/', $_ALL_POST['spouse_dob']);
                    $_ALL_POST['spouse_dob'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                }
                
                if ( !empty($_ALL_POST['do_reg']) ) {
                    $temp = explode('/', $_ALL_POST['do_reg']);
                    $_ALL_POST['do_reg'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                }
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( Vendors::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                    if ( empty($_ALL_POST['country']) ) {
                            $_ALL_POST['country'] = '91';
                    }
                    $_ALL_POST['password'] = '';
                    $_ALL_POST['manager'] 	= Vendors::getManager($db, '', $_ALL_POST['manager']);
					//$_ALL_POST['roles'] 	= explode(',', $_ALL_POST['roles']);
					
                    if(!empty($_ALL_POST['service_id'])){
                        $_ALL_POST['service_id'] = trim($_ALL_POST['service_id']);
                        $_ALL_POST['service_id']    = explode(",", $_ALL_POST['service_id']);
                    }
                    if(!empty($_ALL_POST['authorization_id'])){
                        $_ALL_POST['authorization_id'] = trim($_ALL_POST['authorization_id'],",");
                        $_ALL_POST['authorization_id'] = explode(",", $_ALL_POST['authorization_id']);
                    }
                    $_ALL_POST['industry']    = explode(",", $_ALL_POST['industry']);
					// BO: Read the Team Members Information.
					if ( !empty($_ALL_POST['team']) && !is_array($_ALL_POST['team']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
                        $_ALL_POST['team'] = trim($_ALL_POST['team'],',');
						$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
						$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
						$_ALL_POST['team_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['team'] = array();
						foreach ( $_ALL_POST['team_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
                       
							$_ALL_POST['team'][] = $members['user_id'];
							$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}
                
					// EO: Read the Team Members Information.
					 // BO: Read the Team Members Information.
					if ( !empty($_ALL_POST['alloted_clients']) && !is_array($_ALL_POST['alloted_clients']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
                        $_ALL_POST['alloted_clients'] = trim($_ALL_POST['alloted_clients'],',');
						$_ALL_POST['alloted_clients']          = explode(',', $_ALL_POST['alloted_clients']);
						$temp                       = "'". implode("','", $_ALL_POST['alloted_clients']) ."'";
						$_ALL_POST['allotted_to_clients']  = '';
						$_ALL_POST['client_details']  = array();
						Vendors::getList($db, $_ALL_POST['allotted_to_clients'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['alloted_clients'] = array();
						foreach ( $_ALL_POST['allotted_to_clients'] as $key1=>$members1) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['alloted_clients'][] = $members1['user_id'];
							$_ALL_POST['client_details'][] = $members1['f_name'] .' '. $members1['l_name'] .' ('. $members1['number'] .')';
						}
					}
                    //Uncomment me after editing Vendors list
                    if(!empty($_ALL_POST['username'])){
                        if ( !preg_match('/^[a-zA-Z]+[a-zA-Z0-9_.]{5,80}$/', $_ALL_POST['username']) ) {
                            $_ALL_POST['username']='smeerp';
                        }  
                    }
                    
					// EO: Read the Team Members Information.
                    // Check the Access Level.
                    if ( $_ALL_POST['access_level'] < $access_level ) {
                        // Read the Contact Numbers.
                        $phone->setPhoneOf(TABLE_VENDORS, $user_id);
                        $_ALL_POST['phone'] = $phone->get($db);
                        
                        // Read the Addresses.
                        $region->setAddressOf(TABLE_VENDORS, $user_id);
                        $_ALL_POST['address_list'] = $region->get();
                        
                        // Read the Reminders.
                        $reminder->setReminderOf(TABLE_VENDORS, $user_id);
                        $_ALL_POST['reminder_list'] = $reminder->get($db);
                    }
                    
                }
            }
 
            if ( !empty($_ALL_POST['user_id']) ) {

                // Check the Access Level.
                if ( $_ALL_POST['access_level'] < $access_level ) {

                    $phone_types    = $phone->getTypeList();
                    $address_types  = $region->getTypeList();
                    $lst_country    = $region->getCountryList();
                    $lst_state      = $region->getStateList();
                    $lst_city       = $region->getCityList();
                    
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    //print_r($_ALL_POST);
              
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    $hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    $page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
                    $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
                    $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
                    $page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
					$page["var"][] = array('variable' => 'message_list', 'value' => 'message_list');
                    $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
					$page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
                    $page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-edit.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Client with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected Client was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>