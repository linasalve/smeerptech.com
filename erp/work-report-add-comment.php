<?php   

    $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');
    $curz     = isset($_GET["curz"]) ? $_GET["curz"] : ( isset($_POST["curz"]) ? $_POST["curz"] : '' );
    $curno     = isset($_GET["curno"]) ? $_GET["curno"] : ( isset($_POST["curno"]) ? $_POST["curno"] : '' );
    $curdate    = isset($_GET["curdate"]) ? $_GET["curdate"] : ( isset($_POST["curdate"]) ? $_POST["curdate"] : '' );
    $frm    = isset($_GET["frm"]) ? $_GET["frm"] : ( isset($_POST["frm"]) ? $_POST["frm"] : '' );
    
    $_ALL_POSTDATA	   =$_ALL_POST = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
   
    

    // Read the record details
    $fields = TABLE_WORK_REPORT .'.*,'.TABLE_USER.'.f_name,'.TABLE_USER.'.l_name';            
    $condition_query = " LEFT JOIN ". TABLE_USER ." ON ".TABLE_USER.".user_id =".TABLE_WORK_REPORT.".created_by
                                WHERE (". TABLE_WORK_REPORT .".id = '". $id ."' )";
    
   
    
    if ( WorkReport::getDetails($db, $_ALL_POSTDATA, $fields, $condition_query) > 0 ){
        
        $_ALL_POSTDATA = $_ALL_POSTDATA['0'];
        $created_by = $_ALL_POSTDATA['created_by'];
        $allotted_to = $_ALL_POSTDATA['allotted_to'];
        $on_behalf_id = $_ALL_POSTDATA['on_behalf_id'];
        $job_id = $_ALL_POSTDATA['job_id'];
        $order_id = $_ALL_POSTDATA['order_id'];
        $task_no = $_ALL_POSTDATA['task_no'];
        $access_level = $_ALL_POSTDATA['access_level'];
        $status = $_ALL_POSTDATA['status'];
        $priority = $_ALL_POSTDATA['priority'];
        $do_r = $_ALL_POSTDATA['do_r'];
        $main_task = $_ALL_POSTDATA['comment'];
        $task_title = $_ALL_POSTDATA['task'];
        $is_imp = $_ALL_POSTDATA['is_imp'];
        $type = $_ALL_POSTDATA['type'];
        $task_af_name = $_ALL_POSTDATA['f_name'];
        $task_al_name = $_ALL_POSTDATA['l_name'];
        $allottedtoArr = explode(",", $allotted_to) ;
        //array_push($allottedtoArr,$created_by) ;
        
        // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null;
        $allotted_to_str= str_replace(',',"','",$allotted_to);
        $fields = "  DISTINCT(".TABLE_USER .'.user_id )'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name'
                    .','. TABLE_USER .'.email';
        $my_condition_query = " WHERE ".TABLE_USER .".user_id 
		IN('".$allotted_to_str."','".$created_by."','".$on_behalf_id."') ";
        User::getList($db,$lst_executive,$fields,$my_condition_query);
        // BO:  Read the Team members list ie member list in dropdown box  EOF 

        if(in_array($my['user_id'],$allottedtoArr) || $perm->has('nc_ts_viewallcm') || ($my['user_id']==$created_by)){
        
        
            if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
                  $_ALL_POST  = $_POST;
                  $data       = processUserData($_ALL_POST);
				  $files  = processUserData($_FILES);
            
                    $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
					 $data['files'] = $files ;
					$data['allowed_file_types'] = $allowed_file_types   ;
					$data['max_file_size'] = MAX_FILE_SIZE ;
          
                if ( WorkReport::validateCommentAdd($data, $extra) ) {
                    
					if(!empty($data['file_1'])){
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_1']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-1".".".$ext ;
						$data['file_1'] = $attachfilename;
						
						if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_TASKSCHEDULER_FILES."/".$attachfilename)){
						   
						  @chmod(DIR_FS_TASKSCHEDULER_FILES."/".$attachfilename,0777);
						}
					}
					
					
					
					// insert into log of task reminder
                    $toidStr=$toidStr1='';
                    if(!empty($data['to_id']) && isset($data['to_id'])){
                        $toidStr=implode(',',$data['to_id']) ;
                        
                    }
                    $query	= " INSERT INTO ".TABLE_WORK_REPORT_LOG
                            ." SET ". TABLE_WORK_REPORT_LOG .".task_id = '". $id ."'"
                                .",". TABLE_WORK_REPORT_LOG .".task_no = '".$task_no ."'"
                                .",". TABLE_WORK_REPORT_LOG .".file_1 = '".$data['file_1'] ."'"
                                .",". TABLE_WORK_REPORT_LOG .".by_id = '". $my['user_id'] ."'"
                                .",". TABLE_WORK_REPORT_LOG .".to_id = '". $toidStr ."'"
                                .",". TABLE_WORK_REPORT_LOG .".comment = '". processUserData($data['comment']) ."'"
                                .",". TABLE_WORK_REPORT_LOG .".do_a = '".                 date('Y-m-d h:i:s') ."'";
                    $db->query($query);    
                    $variables['hid'] = $db->last_inserted_id();        
                     
                    if(isset($data['to_id']) && !empty($data['to_id'])) {
                        $string1 = implode("','",$data['to_id']);
                        $string2 = $my['user_id'];
                        $string = $string1."','".$string2;
                    }else{
                        $string =$my['user_id'];
                    }
                    
                    $statusArr = WorkReport::getStatus();
                    $statusArr = array_flip($statusArr);
                    $priorityArr = WorkReport::getPriority();
                    $priorityArr = array_flip($priorityArr);
                    $data['task_title']       =   processSqlData($task_title) ;
                    $data['task']       =   processSqlData($main_task) ;
					$data['comment']	=	$_ALL_POST['comment'];
                    $data['task_no']       =   $task_no;
                    $data['do_r']       =   date("d M Y",strtotime($do_r));				
                    $data['af_name']    =   $my['f_name'] ;
                    $data['al_name']    =   $my['l_name'] ;
                    $data['status']     =   $statusArr[$status] ;
                    $data['priority']     =   $priorityArr[$priority] ;
                    $data['task_af_name']     =   $task_af_name;
                    $data['task_al_name']     =   $task_al_name;
                    $data['link']   = DIR_WS_NC .'/task-reminder.php?perform=acomm&id='. $id ;
                    $send_to='';
					$file_name='';
					/* 
					if(!empty($attachfilename)){
						$file_name[] = DIR_FS_TASKSCHEDULER_FILES ."/". $attachfilename;
					}  
                    if ( !empty($job_id) ) {
                        Jobs::getList($db, $_ALL_POST['job_details'], 'title,description', " WHERE id ='". $job_id ."'");   
                        //$_ALL_POST['job_details'] = $_ALL_POST['job_details']['0'];
                        $data['job_title'] = $_ALL_POST['job_details']['0']['title'];
                    }
                    
                    if ( !empty($order_id) ) {
                        Order::getList($db, $_ALL_POST['order_title'], 'order_title', " WHERE id ='". $order_id ."'");  
                        $data['order_title'] = $_ALL_POST['order_title']['0'];
                    }
					$data['imp']='-Team-Cmmt ';
					if($is_imp){
						$data['imp']='-IMP-Cmmt ';
					}
					if($type==WorkReport::CEOTASK){
						$data['imp']='-CEO-Cmmt ';
					}
					*/				
                    $send_to='';
                    $data['link']   = DIR_WS_NC .'/task-reminder.php?perform=acomm&id='. $id;
                    $data['mail_exec']=0; 
                    if(isset($data['mail_exec']) && $data['mail_exec']=='1'){
                        //User::getList($db, $userDetails, 'number, f_name, l_name, email', " WHERE user_id IN('".$string."')" );
                        //Send mail to the all task members bof
                        if(!empty($lst_executive)){
                            foreach ( $lst_executive as $user ) {
                                $data['uf_name']    =   $user['f_name'];
                                $data['ul_name']    =   $user['l_name'] ;
                                $email = NULL;
                                
                                if ( getParsedEmail($db, $s, 'WORK_REPORT_COMMENT', $data, $email) ) {
                                    $to = $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);  
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $send_to .= $user['email'].",";
                                   
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], $cc_to_sender,$cc,$bcc,$file_name);
                                }
                            }
                        }
                        //Send mail to the all task members eof
                    }else{
						 
						if(in_array('e68adc58f2062f58802e4cdcfec0af2d',$allottedtoArr)){
							//Aryan Sir alloted in task then send mail
							$data['uf_name']    =   'Aryan';
                            $data['ul_name']    =  'Salve' ;
							$user['email'] 		= 'aryan@smeerptech.com';
                            $email = NULL;                            
                            if ( getParsedEmail($db, $s, 'WORK_REPORT_COMMENT', $data, $email) ) {
                                $to = $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $user['uf_name'] .' '. $user['ul_name'], 'email' => $user['email']);                          	   
                                $send_to .=$user['email'].",";
                                
								$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
							}
						}
					
					}
                    //Set the list email-id to whom the mails are sent bof
                    if(!empty($send_to)){
                        $query_update = "UPDATE ". TABLE_WORK_REPORT_LOG 
                                ." SET ". TABLE_WORK_REPORT_LOG .".mail_send_to = '".trim($send_to,",")."'"." WHERE ". TABLE_WORK_REPORT_LOG .".id = '". $variables['hid'] ."' " ;
                        $db->query($query_update) ;
                    }
                    //Set the list subuser's email-id to whom the mails are sent eof 
                    
                    // Send Email to the admin BOF  
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'WORK_REPORT_COMMENT', $data, $email) ) { //WORK_REPORT_COMMENT_ADMIN
                        $to = $cc_to_sender=$cc=$bcc='';
                        $to[]   = array('name' => $workreport_name , 'email' => $workreport_email);                                                  
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                       echo $email["body"];
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
                    }
                    // Send Email to the admin EOF  
                    
                   //Set the list subuser's to whom the task comment alloted are sent eof 
                    $executivename_to=$to='';
                    if(isset($data['to_id']) && !empty($data['to_id'])){
                        $executive_to = array();
                        $to=implode(',',$data['to_id']);
                        $string_to = str_replace(",","','",$to);
                       
                        $fields_last =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                        $condition_last = " WHERE ".TABLE_USER .".user_id IN('".$string_to."') " ;
                        User::getList($db,$executive_to,$fields_last,$condition_last);
                        $executivename_to='';
                      
                        foreach($executive_to as $key=>$val){
                            $executivename_to .= $val['f_name']." ".$val['l_name']."," ;
                        } 
                    }
                    
                    $query_last_comment  = " UPDATE ". TABLE_WORK_REPORT
                        ." SET ". TABLE_WORK_REPORT .".last_comment = '".processUserData($data['comment']) ."'"
                        .",". TABLE_WORK_REPORT .".last_comment_by = '".      $my['user_id'] ."'"
                        .",". TABLE_WORK_REPORT .".last_comment_by_name = '". $my['f_name']." ".$my['l_name'] ."'"
                        .",". TABLE_WORK_REPORT .".last_comment_to = '".      $toidStr ."'"
                        .",". TABLE_WORK_REPORT .".last_comment_to_name = '". $executivename_to ."'"
                        .",". TABLE_WORK_REPORT .".last_comment_dt = '".      date('Y-m-d h:i:s') ."'"
                        ." WHERE id = '". $id ."'";
                	$db->query($query_last_comment);
                }
                
            }
            
            if (  (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && !empty($frm) && $messages->getErrorMessageCount() <= 0 ) {
                if(!empty($curdate)){
                
                    if( $curz =='x' ){
                        $url = $variables['nc']."/work-report.php?perform=view_calendar&nextdate=".$curdate."&x=".$curno ;
                    }elseif( $curz =='y'){
                        $url = $variables['nc']."/work-report.php?perform=view_calendar&previousdate=".$curdate."&y=".$curno ;
                    
                    }                        
                }else{
                
                      $url = $variables['nc']."/work-report.php?perform=view_calendar";
                
                }
                ?>
                        <script language="javascript" type="text/javascript">        
                         window.close();
                         window.opener.location.href="<?php echo $url ;    ?>"
                      </script>   
                
                <?php
                
            }
        
             // get list of all comments of change log BOF
            $condition_query1 = $condition_url1 =$extra_url= '';
            $condition_url1 .="&perform=".$perform."&id=".$id;
            $perform = 'acomm';
            $listLog	= 	NULL;
            $fields = TABLE_WORK_REPORT_LOG.".id " ;
            $condition_query1 = " WHERE ".TABLE_WORK_REPORT_LOG.".task_id = '".$id."' " ;
            $condition_query1 .= " ORDER BY ".TABLE_WORK_REPORT_LOG.".id DESC";
            $total	=	WorkReport::getDetailsCommLog( $db, $listLog, $fields , $condition_query1);
           
            
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
            //$extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  .= "&perform=".$perform;
            $extra_url  .= "&id=".$id ;
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            $listLog	= NULL;
            $fields = TABLE_WORK_REPORT_LOG .'.*, '.TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';   
            WorkReport::getDetailsCommLog( $db, $listLog, $fields, $condition_query1, $next_record, $rpp);
            $condition_url .="&perform=".$perform;
            
            $fList=array();
            if(!empty($listLog)){
                foreach( $listLog as $key=>$val){  
                
                   $executive=array();
                   $string = str_replace(",","','", $val['to_id']);
                   $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                   $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                   User::getList($db,$executive,$fields1,$condition1);
                   $executivename='';
                  
                   foreach($executive as $key1=>$val1){
                        $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
                   } 
                   $val['to_id'] = $executivename ;
                   $fList[$key]=$val;
                }
            }
          
            $hidden[] = array('name'=> 'curz', 'value' => $curz);
            $hidden[] = array('name'=> 'curdate', 'value' => $curdate);
            $hidden[] = array('name'=> 'curno', 'value' => $curno);
            $hidden[] = array('name'=> 'frm', 'value' => $frm);
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'acomm');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'listLog', 'value' => 'fList');
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive'); 
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'work-report-add-comment.html');
        
        }else{
        
           $messages->setErrorMessage("You do not have the Permission to view the details.");
        }
    
    }else{
        $messages->setErrorMessage("Records not found.");
    }
   
?>