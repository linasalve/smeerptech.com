<?php

    if ( $perm->has('nc_tcr_add') ) {
    
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        $al_list    = getAccessLevel($db, $my['access_level']);

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list
                        );

            if ( TermsRegistration::validateAdd($data, $extra) ) {
	
                if ( $messages->getErrorMessageCount() <= 0 ) {
                    
                            $query = "INSERT INTO ". TABLE_TERMS_REGISTRATION
                                ." SET "
                                    . TABLE_TERMS_REGISTRATION.".title			    = '". $data["title"] ."'"
                                    .",". TABLE_TERMS_REGISTRATION.".sequence         = '". $data["sequence"] ."'"
                                    .",". TABLE_TERMS_REGISTRATION.".condition_content= '". $data["condition_content"] ."'"
                                    .",". TABLE_TERMS_REGISTRATION.".status			= '". $data["status"] ."'"
                                    .",". TABLE_TERMS_REGISTRATION.".ip		        = '". $_SERVER['REMOTE_ADDR'] ."'"
                                    .",". TABLE_TERMS_REGISTRATION.".date		        = '". date('Y-m-d H:i:s') ."'"
                                    .",". TABLE_TERMS_REGISTRATION.".created_by		= '". $my['uid'] ."'";
        
                        if ($db->query($query) && $db->affected_rows() > 0) {
                            $messages->setOkMessage("Terms and condition added successfully.");
                        }
                        //to flush the data.
                        $_ALL_POST	= NULL;
                        $data		= NULL;                    
                }
            }
        }
        

	  if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/terms-registration.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/terms-registration.php");
        }
        

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
		header("Location:".DIR_WS_NC."/terms-registration.php?added=1");   

        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');

    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'terms-registration-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>