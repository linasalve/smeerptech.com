<?php
    if ( $perm->has('nc_ts_list') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*
        if ( $perm->has('nc_ts_add_al') ) {
            $access_level += 1;
        }
        */
       
		//$previousdate = isset($_GET['previousdate']) ?  $_GET['previousdate'] : date("Y-m-d") ;
        //$nextdate = isset($_GET['nextdate']) ?  $_GET['nextdate'] : date("Y-m-d") ;
        
        $previousdate    = isset($_GET["previousdate"])   ? $_GET["previousdate"]  : ( isset($_POST["previousdate"])    ? $_POST["previousdate"] : date("Y-m-d") );
        $nextdate    = isset($_GET["nextdate"])   ? $_GET["nextdate"]  : ( isset($_POST["nextdate"])    ? $_POST["nextdate"] : date("Y-m-d") );
        $act    = isset($_GET["act"])   ? $_GET["act"]  : ( isset($_POST["act"])    ? $_POST["act"] : '' );
        
        $previousurl = $nexturl = $extraUrl = '';
        /**************************************	
        **	Pagination page number
        **************************************/
        $x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
        $y 			= isset($_GET["y"])         ? $_GET["y"]        : ( isset($_POST["y"])          ? $_POST["y"]       :'');
        $week = 7 ;
        
        if (empty($x)) {
            $from=0;
            $nox = 2;
        }else{   
            $from = ($x-1) * RESULT_PER_CALENDAR;
            $nox = $x+1;    
        }
        
        if(empty($y)) {    
            $fromy=0;
            $noy = 2;
        }else{    
          
            $fromy = ($y-1) * RESULT_PER_CALENDAR;
            $noy = $y+1;
        }
       
        if(empty($x) && empty($y)){
            $dateArr = array();
            for($i=0;$i<$week;$i++ ){               
                $dateArr[]= date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")+$i, date("Y"))) ;                
            }
            $curz='';
            $curno = '';
            $curdate='';
            
            
            $previousurl = "previousdate=".$dateArr[0]."&y=".$noy;
            $nexturl =  "nextdate=".$dateArr[6]."&x=".$nox;
            
        }elseif(!empty($x) && empty($y) ){
            $arr=explode('-',$nextdate) ;
            $mm  =  $arr[1] ;
            $dd =$arr[2] ;
            $yy = $arr[0] ;
            for($i=1;$i<=$week;$i++ ){
               
                $dateArr[]= date('Y-m-d',mktime(0, 0, 0, $mm  , $dd+$i, $yy) ) ;
                
            }
            
             $curz='x';
             $curno = $nox-1;
             $curdate=$nextdate;
             
             $previousurl = 'previousdate='.$dateArr[0].'&y='.$noy;
             $nexturl =  'nextdate='.$dateArr[6].'&x='.$nox;
        }elseif(empty($x) && !empty($y)){
        
            $arr=explode('-',$previousdate) ;
            $mm  =  $arr[1] ;
            $dd =$arr[2] ;
            $yy = $arr[0] ;
            for($i=1;$i<=$week;$i++ ){
               
                $dateArr[]= date('Y-m-d',mktime(0, 0, 0, $mm  , $dd-$i, $yy) ) ;
                
            }
            $curz='y';
            $curno = $noy-1;
            $curdate=$previousdate;
            $previousurl = 'previousdate='.$dateArr[6].'&y='.$noy;
            $nexturl =  'nextdate='.$dateArr[0].'&x='.$nox;
            
        }
        $search_other=0;
       
        if (isset($act) && !empty($act)) {
             
            $_ALL_POST    = (isset($_GET) && !empty($_GET) )  ? $_GET  : ( ( isset($_POST) && !empty($_POST))    ? $_POST : array() );
            $data		= processUserData($_POST);
           
         
             
            if(!empty($_ALL_POST['executive'])){ 
                $edetails	= 	NULL;
                $condition_querye=" WHERE "." user_id='".$_ALL_POST['executive']."'";
                User::getList( $db, $edetails, 'f_name,l_name,number,email', $condition_querye);
                $edetails = $edetails[0];
                
                $_ALL_POST['executive_details'] = $edetails['f_name']." ".$edetails['l_name']." (".$edetails['number'].") (".$edetails['email'].")" ;
                if(isset($_ALL_POST['alloted_by'])){
                    $search_other=1;
                    if( $_ALL_POST['alloted_by'] == Taskreminder::ALLOTED_BY_ME){
                    
                       
                        $condition_query = " WHERE ( "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $_ALL_POST['executive'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $_ALL_POST['executive'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',".$_ALL_POST['executive'].",' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^".$_ALL_POST['executive'].",' "
                                    . " ) AND ". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."'" ;
                                    
                        $previousurl .= "&executive=".$_ALL_POST['executive']."&alloted_by=".$_ALL_POST['alloted_by']."&act=".$act;
                        $nexturl .= "&executive=".$_ALL_POST['executive']."&alloted_by=".$_ALL_POST['alloted_by']."&act=".$act;
                       
                        
                    }elseif( $_ALL_POST['alloted_by'] == Taskreminder::SHOW_ALL ){
                        //$subcondition = " AND ". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."'" ;
                      
                         $condition_query = " WHERE (( "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $_ALL_POST['executive'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',".$_ALL_POST['executive'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',".$_ALL_POST['executive'].",' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^".$_ALL_POST['executive'].",' "
                                    . " )  OR (". TABLE_TASK_REMINDER. ".created_by = '".$_ALL_POST['executive']."') ) "      ;
                       
                        $previousurl .= "&executive=".$_ALL_POST['executive']."&alloted_by=".$_ALL_POST['alloted_by']."&act=".$act;
                        $nexturl .= "&executive=".$_ALL_POST['executive']."&alloted_by=".$_ALL_POST['alloted_by']."&act=".$act;
                       
                    }
                }
                
            }else{
                 //$messages->setErrorMessage("Please select the executive.");
                
                if(isset($_ALL_POST['alloted_by'])){
                    $search_other=1;
                    if( $_ALL_POST['alloted_by'] == Taskreminder::ALLOTED_BY_ME){
                    
                       
                        $condition_query = " WHERE "
                                        .  "  ". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."'"
                                        . " "     ;
                                        
                        $previousurl .= "&alloted_by=".$_ALL_POST['alloted_by']."&act=".$act;
                        $nexturl .= "&alloted_by=".$_ALL_POST['alloted_by']."&act=".$act;
                       

                    }elseif( $_ALL_POST['alloted_by'] == Taskreminder::SHOW_ALL ){
                      
                        $condition_query = " WHERE ( ( "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',".$my['user_id'].",' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^".$my['user_id'].",' "
                                    . " ) OR (". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."') )"      ;
                        
                        $previousurl .= "&alloted_by=".$_ALL_POST['alloted_by']."&act=".$act;
                        $nexturl .= "&alloted_by=".$_ALL_POST['alloted_by']."&act=".$act;
                    }
                }
                 
            }
             
        }else{
            if(!isset($_ALL_POST['alloted_by'])){
                $_ALL_POST['alloted_by'] =Taskreminder::SHOW_ALL ; //By default show all
            }
        }
        $cond1=$condition_query;
        if(!empty($dateArr)){
            foreach($dateArr as $key1=>$val1){
              
                $arr=explode('-',$val1) ;
                $mm  =  $arr[1] ;
                $dd =$arr[2] ;
                $yy = $arr[0] ;
                $time  = mktime(0,0,0,$mm,$dd,$yy);
                $day = date('D',$time);
                $day = date('l',$time);
             
                //$date = $val1." 00:00:00";
                $date = $val1;
                $reminderlist=array();
                // $condition_query = " WHERE   (". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."' AND ".TABLE_TASK_REMINDER.".do_r='".$date."')";
                if($search_other==1){
                   
                     $subcondition_query='';
                     $condition_query='';
                     //$subcondition_query = " AND ".TABLE_TASK_REMINDER.".do_r='".$date."' "    ;
                     $subcondition_query = " AND 
					 date_format(".TABLE_TASK_REMINDER.".do_r,'%Y-%m-%d')= '".$date."' "    ;
                     $condition_query = $cond1.$subcondition_query ;
                }else{
                    $condition_query='';
                    $condition_query = " WHERE ( ( "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' "
                                    . " ) OR (". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."') ) AND date_format(".TABLE_TASK_REMINDER.".do_r,'%Y-%m-%d')='".$date."' "       ;
                    //$condition_query = " WHERE   (". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."' ) AND ".TABLE_TASK_REMINDER.".do_r='".$date."')";
                }
                
                $condition_query.= " AND ".TABLE_TASK_REMINDER.".status='".Taskreminder::PENDING."'";
                
                $total	= Taskreminder::getDetails( $db, $reminderlist, '*', $condition_query);
                $fList=array();
              
                if(!empty($reminderlist)){
                
                    foreach( $reminderlist as $key=>$val){
                       /*          
                        $executive=array();
                        $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                      
                           $string_a = str_replace(",","','", $val['allotted_to']);                      
                           $conditiona = " WHERE ".TABLE_USER .".user_id IN('".$string_a."') " ;
                           User::getList($db,$executive,$fields1,$conditiona);
                           $executivename='';                      
                           foreach($executive as $keya=>$vala){
                                $executivename .= $vala['f_name']." ".$vala['l_name']."<br/>" ;
                           } 
                           $val['allotted_to'] = $executivename ;
                        */
                        
                        $fields1 =  TABLE_USER.'.user_id,'.TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                        $executive=array();                      
                        if(empty($val['allotted_to_name'])){
                         
                           $string = str_replace(",","','", $val['allotted_to']);
                          
                           $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                           User::getList($db,$executive,$fields1,$condition1);
                           $executivename='';              
                           foreach($executive as $key1=>$val1){
                                $executivename .= $val1['f_name']." ".$val1['l_name']."," ;
                           } 
                           $val['allotted_to'] = wordwrap(trim($executivename,","),30) ;  
                           
                        }else{
                           $val['allotted_to'] =  wordwrap(trim($val['allotted_to_name'],","),30);
                        } 
                    
                     
                     
                     
                       $statusArr = Taskreminder::getStatus();
                       $statusArr = array_flip($statusArr);
                       $val['status_n'] = $statusArr[$val['status']];
                       $priorityArr = Taskreminder::getPriority();
                       $priorityArr = array_flip($priorityArr);
                       $val['priority_n'] = $priorityArr[$val['priority']];
                       
                       $exeCreted = array();
                       $exeCreatedname ='';
                       $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
                       User::getList($db,$exeCreted,$fields1,$condition2);
                      
                       foreach($exeCreted as $key3=>$val3){
                            $exeCreatedname .= $val3['f_name']." ".$val3['l_name']."<br/>" ;
                       } 
                       $val['created_by_name'] = $exeCreatedname;     
                       $val['can_editable'] = false;
                       $val['can_deletable'] = false;                 
                        if($val['created_by']==$my['user_id']){
                          
                            if ( $perm->has('nc_ts_delete') ) {
                                $variables['can_deletable'] = true;
                                $val['can_deletable'] = true;
                            }
                            if ( $perm->has('nc_ts_edit') ) {            
                                $variables['can_editable'] = true;
                                $val['can_editable'] = true;
                            }
                        }
                        //Get recent comment on the assign task bof
                         $val['recent_comment']='';
                        if(!empty($val['last_comment'])){
                             $val['recent_comment'] = $val['last_comment'];
                        }
                        /*
                         $listLog = array();
                         $val['recent_comment'] = '';
                         $fieldsComment = " by_id,comment ";
                         $condition_querycomm = " WHERE task_id='".$val['id']."' ORDER BY do_a DESC LIMIT 0,1 ";
                         Taskreminder::getDetailsCommLog( $db, $listLog, $fieldsComment, $condition_querycomm);
                         
                         if(!empty($listLog)){
                            $listLog=$listLog[0];
                            $val['recent_comment'] =$listLog['comment'];
                         }
                         */
                        //Get recent comment on the assign task eof
                        
                        $fList[$key]=$val;
                    }
                }
            
                /*
                if(!empty($reminderlist)){
                    $reminderlist=$reminderlist[0];
                }*/
              
                $list[] = array(
                            'date' =>$val1,
                            'day'  =>$day,
                            'reminder_list'=>$fList
                        );
            }
        }
       
        
      
       // Set the Permissions.
        $variables['alloted_by_me'] = Taskreminder::ALLOTED_BY_ME ;
        $variables['show_all'] = Taskreminder::SHOW_ALL ;
       
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit_all']          = false;
        $variables['can_delete_all']        = false;
        //$variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_search_other']  = false;
        $variables['can_search']  = false;
        $variables['can_view_history']  = false;
       
        if ( $perm->has('nc_ts_list') ) {
            $variables['can_view_list'] = true;
            $variables['can_search'] = false;
        }
        if ( $perm->has('nc_ts_add') ) {
            $variables['can_add'] = true;
        }    
        if ( $perm->has('nc_ts_edit_all') ) {            
            $variables['can_edit_all'] = true;
        }
        
        if ( $perm->has('nc_ts_delete_all') ) {
            $variables['can_delete_all'] = true;
        }
        /*if ( $perm->has('nc_ts_edit') ) {            
            $variables['can_edit'] = true;
        }*/
        
        /*if ( $perm->has('nc_ts_delete') ) {
            $variables['can_delete'] = true;
        }*/
        
        if ( $perm->has('nc_ts_list_other') ) {
            $variables['can_view_list_other'] = true;
            
        }
        if ( $perm->has('nc_ts_details') ) {
            $variables['can_view_details'] = true;
        }
        
        if ( $perm->has('nc_ts_view_h') ) {
            $variables['can_view_history'] = true;
        }
        
        
        $hidden[] = array('name'=> 'perform' ,'value' => 'view_calendar');
        $hidden[] = array('name'=> 'act' , 'value' => 'search');           
        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
         
        $page["var"][] = array('variable' => 'curz', 'value' => 'curz');     
        $page["var"][] = array('variable' => 'curno', 'value' => 'curno');     
        $page["var"][] = array('variable' => 'curdate', 'value' => 'curdate');     
            
        
          
        $page["var"][] = array('variable' => 'previousurl', 'value' => 'previousurl');     
        $page["var"][] = array('variable' => 'nexturl', 'value' => 'nexturl');     
        $page["var"][] = array('variable' => 'list', 'value' => 'list');     
        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'task-reminder-calendar.html');
      
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>