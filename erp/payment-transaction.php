<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
    include_once ( DIR_FS_INCLUDES .'/payment-bank.inc.php');
    include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    include_once ( DIR_FS_INCLUDES .'/financial-year.inc.php');
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
	
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform= isset($_GET["perform"])? $_GET["perform"]	: ( isset($_POST["perform"])? $_POST["perform"] : '' );
	$x		= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	$pageno = isset($_GET["pageno"])  ? $_GET["pageno"]        : ( isset($_POST["pageno"])          ? $_POST["pageno"]       :'');
	
	$number 		= isset($_GET["number"])     ? $_GET["number"]    : ( isset($_POST["number"])  ? $_POST["number"]       :'0');
	$added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])  ? $_POST["added"]       :'0');
	$transaction_id = isset($_GET["transaction_id"])  ? $_GET["transaction_id"]  : ( isset($_POST["transaction_id"]) ? $_POST["transaction_id"]       : '');
	
	$de_act 	= isset($_GET["de_act"])  ? $_GET["de_act"]  : ( isset($_POST["de_act"]) ? $_POST["de_act"]       : '');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
  
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
   
    $condition_query ='';
    if( empty($x) ){
        $x              = 1;
		$pageno              = 1;
        $next_record    = 0 ;
    }else{
        $next_record    = ($x-1) * $rpp;
    }
   
    $variables["x"] = $x;
	$variables["pageno"] = $pageno;
    $variables["rpp"]  =  $rpp;
    $condition_url = '';
     
    if($added){
        $messages->setOkMessage("Payment transaction of number ".$number." entry has been done.");
    }
    if(!empty($transaction_id)){
      
		if($de_act==1){
		 
          $messages->setOkMessage("Record has been deactivated for transaction id ".$transaction_id);
        }
    } 
  	 
     
    if ( $perm->has('nc_p_pt') ) {
        
	    $sTypeArray     = array('Any'  =>  
		array(  'Any of following'  => '-1'),
				TABLE_PAYMENT_TRANSACTION =>  array(                                                                
							'Number' => 'number',                              
							'Amount' => 'pay_received_amt',                                                           
							'Cheque No' => 'pay_cheque_no',                                                           
							'Amount Issue to' => 'off_exp_issue_to_name',
							'Amount Issue by' => 'off_exp_issue_by_name',
							'Remarks' => 'remarks',                                                               
							'Particulars' => 'particulars',   
							'Invoice Nos' => 'invoice_no',                            
							'Bill NC Nos' => 'bill_nc_no',     
							'Receipt Nos' => 'receipt_no',    
							'Behalf of Accounts' => 'behalf_vendor_bank',
							'Behalf of Client/Vendor' => 'behalf_client_name',
							'Behalf of Executive' => 'behalf_executive_name',
							'Behalf of Company Name' => 'behalf_company_name'          
						),
				TABLE_PAYMENT_ACCOUNT_HEAD  => array(                                              
							'Account Head' => 'account_head_name'                                                                
						), 
				TABLE_VENDORS_BANK => array(
							'Bank/CAs/Directors First Name' => 'f_name-'.TABLE_VENDORS_BANK,
							'Bank/CAs/Directors Last Name' => 'l_name-'.TABLE_VENDORS_BANK,
							'Bank/CAs/Directors Billing Name'=> 
				'billing_name-'. TABLE_VENDORS_BANK                                                                
								
						),
				TABLE_PAYMENT_BANK  => array(
							'Bank Name' => 'bank_name',                              
						),
				TABLE_CLIENTS  => array(
				'Client First Name' => 'f_name',   
				'Client Last Name' => 'l_name',                        
				'Client Billing Name' =>'billing_name-'.TABLE_CLIENTS                                                               
				),  
            TABLE_USER  => array(
					'Executive First Name' => 'f_name-'.TABLE_USER,	
					'Executive Last Name' => 'l_name-'.TABLE_USER                   
            ),  
        );
        
        $sOrderByArray  = array(
			TABLE_PAYMENT_TRANSACTION => array(
				'Number' => 'number',                        
				'Cheque No' => 'pay_cheque_no',                         
				'Date Of Entry'  => 'date' ,    
				'Date of Transaction'  => 'do_transaction'                                                          
			),
         );
    
        // Set the sorting order of the user list.
        if(!($order_by_table = findIndex($sOrderBy, $sOrderByArray))){
            //$_SEARCH['sOrderBy']= $sOrderBy = 'do_transaction';
            $_SEARCH['sOrderBy']= $sOrderBy = 'number';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PAYMENT_TRANSACTION;
        }
                                     
        $variables['date'] = date("Y-m-d H:i:s");
        //$variables['transaction_id'] = sprintf(TRANSACTION_ID_FORMAT, date('Y'), date('s'));
        //$variables['transaction_id'] = getCounterNumber($db,'TRA'); 
       
       // Read the available Status 
       $variables['statusall'] = Paymenttransaction::getStatusAll();
	   $variables['fstatus'] = Paymenttransaction::getFStatus();
       $variables['lstatus'] = Paymenttransaction::getLinkingStatus();
       $variables['status'] = Paymenttransaction::getStatus();
	   $variables['gltype'] = Paymenttransaction::getGLType();
       $variables['transaction_type'] = Paymenttransaction::getTransactionType();
       Paymenttransaction::getAccountHead($db,$account_head);
       $variables['account_head'] = $account_head;
	   Paymenttransaction::getIAccountHead($db,$iaccount_head);
       $variables['iaccount_head'] = $iaccount_head;
       $variables['transaction_in'] = Paymenttransaction::PAYMENTIN;
       $variables['transaction_internal'] = Paymenttransaction::INTERNAL;
       $variables['transaction_out'] = Paymenttransaction::PAYMENTOUT;
	   
       $type='';
       // Read the available account head 
       // Paymenttransaction::getAccountHead($db,$account_head,$type);      
       // Read the available party 
       // Paymenttransaction::getParty($db,$party);       
       // Read the available payment mode 
       Paymenttransaction::getPaymentMode($db,$payment_mode);       
       // Read the available bank 
       // Paymenttransaction::getBank($db,$bank);
        $inhouseBanklst	= NULL;
        $fieldsi = TABLE_PAYMENT_BANK.'.*' ;
        $condition_queryib = " WHERE ".TABLE_PAYMENT_BANK.".type = '1' AND ".TABLE_PAYMENT_BANK.".status='1' 
		ORDER BY bank_name";
        Paymentbank::getDetails($db, $inhouseBanklst, $fieldsi, $condition_queryib);
       
        $globalBanklst	= NULL;
        $fieldsg = TABLE_PAYMENT_BANK.'.*' ;
        $condition_querygb = " WHERE ".TABLE_PAYMENT_BANK.".type = '0' AND ".TABLE_PAYMENT_BANK.".status='1' 
		ORDER BY bank_name";
        Paymentbank::getDetails($db, $globalBanklst, $fieldsg, $condition_querygb);
        // Read the available company 
        Paymenttransaction::getCompany($db,$company);
        //getfinancial yr bof
        $lst_fyr = null;
        $lst_fyr = FinancialYear::yearRange();
        
        
       //use switch case here to perform action. 
        switch ($perform) {           
			case ('quick-add'):{            
                include (DIR_FS_NC.'/payment-transaction-quick-add.php');                
               if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('add'): {            
                //include (DIR_FS_NC.'/payment-transaction-add.php');
                include (DIR_FS_NC.'/payment-transaction-nadd.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
			case ('nadd'): {            
                include (DIR_FS_NC.'/payment-transaction-nadd.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
			case ('nedit'): {
            
                include (DIR_FS_NC.'/payment-transaction-nedit.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            } 
			case ('edit_chq'): {
               include (DIR_FS_NC.'/payment-transaction-edit-chq.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
			case ('copy'):{            
                //include (DIR_FS_NC.'/payment-transaction-add.php');
                include (DIR_FS_NC.'/payment-transaction-nadd.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
			case ('qadd'):{            
                include (DIR_FS_NC.'/payment-transaction-qadd.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('edit'): {
                //include (DIR_FS_NC .'/payment-transaction-edit.php');
                include (DIR_FS_NC .'/payment-transaction-nedit.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('deactivate'): {
                include (DIR_FS_NC .'/payment-transaction-deactivate.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('change_status'): {
				$searchStr=1;
                include (DIR_FS_NC .'/payment-transaction-change-status.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('acomm'): {
                include (DIR_FS_NC .'/payment-transaction-add-comment.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('view'): {
                include (DIR_FS_NC .'/payment-transaction-view.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('print'): {
                include (DIR_FS_NC .'/payment-transaction-print.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('download_file'): {
				include (DIR_FS_NC .'/payment-transaction-download.php');
				break;
			}
            case ('allot_bills'): {
                include (DIR_FS_NC .'/payment-transaction-allot-bills.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('allot_bk_stmt'): {
                include (DIR_FS_NC .'/payment-transaction-allot-bank-stmt.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('pcomm'): {
                include (DIR_FS_NC .'/payment-transaction-post-comment.php'); 
				if($ajx==0){				
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/payment-transaction-search.php");
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('delete'): {
                include ( DIR_FS_NC .'/payment-transaction-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('myself'):{
                include ( DIR_FS_NC .'/payment-transaction-myself.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list'):
            default: {
				$searchStr = 1;
                include (DIR_FS_NC .'/payment-transaction-list.php');
                // CONTENT = CONTENT
				if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'payment-transaction.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
		
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
 
    // always assign
    $s->assign("variables", $variables);
    $s->assign("account_head", $account_head);
    $s->assign("lst_fyr", $lst_fyr);
   // $s->assign("party", $party);
    $s->assign("payment_mode", $payment_mode);
    $s->assign("inhouseBanklst", $inhouseBanklst);
    $s->assign("globalBanklst", $globalBanklst);
    $s->assign("company", $company);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
