<?php

	$condition_query= '';
    $condition_url  = '';
    $where_added=false;
    $searchStr = 0;
    if ( $sString != "" ) {
	    $searchStr = 1;
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
		if ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
			$condition_url .= "&sString=". $sString;
			$condition_url .= "&sType=". $sType;
			$where_added = true;
		}
		elseif ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
			foreach( $sTypeArray as $table => $field_arr ) {
				if ( $table != "Any" ) {
					foreach( $field_arr as $key => $field ) {
                        $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
						$where_added = true;
					}
				}
			}
			$condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
			$condition_query .= ") ";
		}
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}
    // BO: Status data.
    $sStatusStr = '';
    $sStatusStrPass = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    
    if ( ($chk_status == 'AND' || $chk_status == 'OR') && isset($sStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }        
     
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_USER .".status IN ('". $sStatusStr ."') ";
        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
    }
	
	$sGroupsStr = '';
    $sGroupsStrPass = '';
    $chk_groups = isset($_POST["chk_groups"])   ? $_POST["chk_groups"]  : (isset($_GET["chk_groups"])   ? $_GET["chk_groups"]   :'');
    $sGroups    = isset($_POST["sGroups"])      ? $_POST["sGroups"]     : (isset($_GET["sGroups"])      ? $_GET["sGroups"]      :'');
    
    if ( ($chk_groups == 'AND' || $chk_groups == 'OR') && isset($sGroups)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_groups;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }        
     
        if(is_array($sGroups)){
             $sStatusStr = implode("','", $sGroups) ;
             $sStatusStr1 = implode(",", $sGroups) ;
             
        }else{
            $sGroupsStr = str_replace(',',"','",$sGroups);
            $sGroupsStr1 = $sGroups ;
            $sGroups = explode(",",$sGroups) ;
            
        }
        $group_like_q='';
		foreach($sGroups as $key1=>$val1){
		
			$group_like_q .= TABLE_USER.".groups LIKE '%,".trim($val1).",%' OR " ;
		}
		
		$group_like_q = substr( $group_like_q, 0, strlen( $group_like_q ) - 3 );
		
		
		
        $condition_query .= " ".$group_like_q;
        
        $condition_url          .= "&chk_groups=$chk_groups&sGroups=$sGroupsStr1";
        $_SEARCH["chk_groups"]  = $chk_groups;
        $_SEARCH["sGroups"]     = $sGroups;
    }
	
	
    $chk_current = isset($_POST["chk_current"])   ? $_POST["chk_current"]  : (isset($_GET["chk_current"])   ? $_GET["chk_current"]   :'');
	$current_staff = isset($_POST["current_staff"])   ? $_POST["current_staff"]  : (isset($_GET["current_staff"])   ? $_GET["current_staff"]   :'');
	
	
	if ( isset($chk_current) && $chk_current=='AND' && isset($current_staff) ){
	    $searchStr = 1;
		 if ( $where_added ) {
            $condition_query .= " ".$chk_current;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }        
     
		$condition_query .= " ". TABLE_USER .".current_staff ='".$current_staff."' ";        
        $condition_url  .= "&current_staff=".$current_staff."&chk_current=".$chk_current;
		$_SEARCH["chk_current"]  = $chk_current;
        $_SEARCH["current_staff"]     = $current_staff;
	}
	
	 $chk_smail = isset($_POST["chk_smail"])   ? $_POST["chk_smail"]  : (isset($_GET["chk_smail"])   ? $_GET["chk_smail"]   :'');
	$send_mail = isset($_POST["send_mail"])   ? $_POST["send_mail"]  : (isset($_GET["send_mail"])   ? $_GET["send_mail"]   :'');
	
	
	if ( isset($chk_smail) && $chk_smail=='AND' && isset($send_mail) ){
	    $searchStr = 1;
		 if ( $where_added ) {
            $condition_query .= " ".$chk_smail;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }        
     
		$condition_query .= " ". TABLE_USER .".send_mail ='".$send_mail."' ";        
        $condition_url  .= "&send_mail=".$send_mail."&chk_smail=".$chk_smail;
		$_SEARCH["chk_smail"]  = $chk_smail;
        $_SEARCH["send_mail"]     = $send_mail;
	}
	
    $chk_random = isset($_POST["chk_random"])   ? $_POST["chk_random"]  : (isset($_GET["chk_random"]) ? 
	$_GET["chk_random"]   :'');
	$random_select = isset($_POST["random_select"])   ? $_POST["random_select"]  : (isset($_GET["random_select"])   ? $_GET["random_select"]   :'');
	
	
	if ( isset($chk_random) && $chk_random=='AND' && isset($random_select) ){
	    $searchStr = 1;
		 if ( $where_added ) {
            $condition_query .= " ".$chk_random;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }        
     
		$condition_query .= " ". TABLE_USER .".random_select ='".$random_select."' ";        
        $condition_url  .= "&random_select=".$random_select."&chk_random=".$chk_random;
		$_SEARCH["chk_random"]  = $chk_random;
        $_SEARCH["random_select"]     = $random_select;
	}
	
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"]) ? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
 
	
	// BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from) ){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
		$dfa_dm=$dfa[1] .'-'. $dfa[0] ;
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
		
        if($dt_field=='do_birth'){
		
			$condition_query .= " date_format(".TABLE_USER.".do_birth,'%m-%d') >= '".$dfa_dm."'";
		
		}else{
			$condition_query .= " ". TABLE_USER .".".$dt_field." >= '". $dfa ."'"; 
		}
				
        $condition_url  .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;
    }
	
	// BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta_dm=$dta[1] .'-'. $dta[0] ;
		$dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
       
		
		if($dt_field=='do_birth'){
		
		
			$condition_query .= " date_format(".TABLE_USER.".do_birth,'%m-%d') <= '".$dta_dm."'";
		
		}else{
			 $condition_query .= " ". TABLE_USER .".".$dt_field." <= '". $dta ."'";
		}
		
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }    
    
    // EO: From Date
	//echo $condition_query;
	$condition_url = "&rpp=$rpp"
					."&sString=$sString"
					."&sType=$sType"
					."&sOrderBy=$sOrderBy"
					."&sOrder=$sOrder"
					."&action=search";
			
	$_SEARCH["sString"] 	= $sString ;
	$_SEARCH["sType"] 		= $sType ;
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/user-list.php');
?>