<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
  
	   

    $client_details = isset($_GET['client_details']) ? $_GET['client_details'] : '';
   
   /*  $count = isset($_GET['count']) ? $_GET['count'] : '';
	if(isset($count)){
		$var = 'team_name_'.$count;
		
		 $client_details = isset($_GET[$var]) ? $_GET[$var] : '';
	} */
	
	if(empty($client_details)){
		$client_details = isset($_GET['team_name']) ? $_GET['team_name'] : '';
    }
    if(empty($client_details)){
		$client_details = isset($_GET['executive_details']) ? $_GET['executive_details'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['team_details']) ? $_GET['team_details'] : '';
    } 
	if(empty($client_details)){
		$client_details = isset($_GET['off_exp_issue_by_name']) ? $_GET['off_exp_issue_by_name'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['off_exp_issue_to_name']) ? $_GET['off_exp_issue_to_name'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['behalf_executive_name']) ? $_GET['behalf_executive_name'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['search_executive_details']) ? $_GET['search_executive_details'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['executive_details_to']) ? $_GET['executive_details_to'] : '';
    }
	if(empty($client_details)){
		$client_details = isset($_GET['executive_details_fi']) ? $_GET['executive_details_fi'] : '';
    }
    $optionLink =$stringOpt= '' ;
	
	
	if(!empty($_GET) && empty($client_details)){
		foreach($_GET as $key=>$val){
			$client_details = $val;
		}
	}
    if(!empty($client_details)){
        $sString = $client_details;
    }
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if (isset($sString) && !empty($sString)){
        header("Content-Type: application/json");      
	    $query =" SELECT DISTINCT(".TABLE_USER.".user_id),".TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".number FROM ".TABLE_USER." WHERE 
        ( ".TABLE_USER.".f_name LIKE '%".$sString."%' OR ".TABLE_USER.".l_name LIKE '%".$sString."%' ) 
       ";
	   // AND ".TABLE_USER.".status='1' 
        $db->query($query);
        echo "{\"results\": [";
        if ( $db->nf()>0 ) {          
            $arr = array();
            while ($db->next_record()){
                 
                 //$arr[] = "{\"id\": \"".$db->f('product_id')."\", \"value\": \"".htmlspecialchars($db->f('title')).""."\", \"info\": \"\"}";
                 $arr[] = "{\"id\": \"".$db->f('user_id')."\", \"value\": \"".processSQLData($db->f('f_name'))." ".processSQLData($db->f('l_name'))." (".processSQLData($db->f('number')).")".""."\", \"info\": \"\"}";
            }
            echo implode(", ", $arr);
           
        }
         echo "]}";	   
	}
include_once( DIR_FS_NC ."/flush.php");


?>
