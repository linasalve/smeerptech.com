<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    
                    $table = TABLE_PAYMENT_BANK ;
                    foreach( $field_arr as $key => $field ) {
                            $condition_query .= " ". $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                        }
                    
                }
            }
            $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
    /*$sStatusStr='';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' ) && isset($sStatus)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
       $condition_query .= " ". TABLE_SETTINGS_QUOT_SUBJECT .".status IN ('". $sStatusStr ."') ";
       $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";   
       $_SEARCH["chk_status"]  = $chk_status;
       $_SEARCH["sStatus"]     = $sStatus;
    }*/
    // EO: Status data.
    //BO : Company
    $chk_cmp = isset($_POST["chk_cmp"])   ? $_POST["chk_cmp"]  : (isset($_GET["chk_cmp"])   ? $_GET["chk_cmp"]   :'');
    $company_id    = isset($_POST["company_id"])      ? $_POST["company_id"]     : (isset($_GET["company_id"])      ?$_GET["company_id"]     :'');
    if ( ($chk_cmp == 'AND' || $chk_cmp == 'OR' ) && !empty($company_id)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_cmp;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       $condition_query .= " ". TABLE_PAYMENT_BANK .".company_id  IN ('". $company_id ."') ";
       $condition_url          .= "&chk_cmp=$chk_cmp&company_id=$company_id";   
       $_SEARCH["chk_cmp"]  = $chk_cmp;
       $_SEARCH["company_id"]     = $company_id;
    } 
    //EO : Company
	
	//BO : type
    $chk_typ = isset($_POST["chk_typ"])   ? $_POST["chk_typ"]  : (isset($_GET["chk_typ"])   ? $_GET["chk_typ"]   :'');
    $type    = isset($_POST["type"])      ? $_POST["type"]     : (isset($_GET["type"])      ?$_GET["type"]     :'');
    if ( ($chk_typ == 'AND' || $chk_typ == 'OR' ) && !empty($type)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_typ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       $condition_query .= " ". TABLE_PAYMENT_BANK .".type  IN ('". $type ."') ";
       $condition_url          .= "&chk_typ=$chk_typ&type=$type";   
       $_SEARCH["chk_typ"]  = $chk_typ;
       $_SEARCH["type"]     = $type;
    } 
    //EO : type
	
	//BO : status
    $chk_typ = isset($_POST["chk_typ"])   ? $_POST["chk_typ"]  : (isset($_GET["chk_typ"])   ? $_GET["chk_typ"]   :'');
    $type    = isset($_POST["type"])      ? $_POST["type"]     : (isset($_GET["type"])      ?$_GET["type"]     :'');
    if ( ($chk_typ == 'AND' || $chk_typ == 'OR' ) && !empty($type)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_typ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       $condition_query .= " ". TABLE_PAYMENT_BANK .".type  IN ('". $type ."') ";
       $condition_url          .= "&chk_typ=$chk_typ&type=$type";   
       $_SEARCH["chk_typ"]  = $chk_typ;
       $_SEARCH["type"]     = $type;
    } 
    //EO : //BO : type
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $status    = isset($_POST["status"])      ? $_POST["status"]     : (isset($_GET["status"])      ?$_GET["status"]     :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' ) && isset($status)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       $condition_query .= " ". TABLE_PAYMENT_BANK .".status  IN ('". $status ."') ";
       $condition_url          .= "&chk_status=$chk_status&status=$status";   
       $_SEARCH["chk_status"]  = $chk_status;
       $_SEARCH["status"]     = $status;
    } 
    //EO : type
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    
    // BO: From Date
    if (( $chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_PAYMENT_BANK .".date >= '". $dfa ."'";
        
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
    }
    
    // EO: From Date
    
    // BO: Upto Date
    if (( $chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)){
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_PAYMENT_BANK .".date <= '". $dta ."'";
        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
    }
    
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/payment-manage-bank-list.php');
?>
