<?php

	$id	= isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
	
	$extra = array( 'db' 			=> &$db,
					'access_level' 	=> $my['access_level'],
					'messages' 		=> $messages
				);
	Policies::delete($id, $extra);
	
	// Display the list.
	$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'policies-delete.html');
	
?>