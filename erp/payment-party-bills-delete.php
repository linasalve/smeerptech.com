<?php
      if ( $perm->has('nc_p_pb_delete') ) {
		$id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
		$access_level = $my['access_level'];       
        
		$extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
		PaymentPartyBills::delete($id, $extra);
        
		// Display the list.
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-party-bills-delete.html');
	}
	else {
		$messages->setErrorMessage("You donot have the Right to Delete the entry.");
		// $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
	}
?>
