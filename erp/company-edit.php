<?php
if ( $perm->has('nc_cmp_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/company.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Company::validateUpdate($data, $extra) ) {
			
				$do_implement ='';
				if($data['do_implement']){                    
					$do_implement  = date('Y-m-d H:i:s', $data['do_implement']) ;
					 
				}
				$do_implement_st ='';
				if($data['do_implement_st']){                    
					$do_implement_st  = date('Y-m-d H:i:s', $data['do_implement_st']) ;
					 
				}
				$do_iso ='';
				if($data['do_iso']){                    
					$do_iso  = date('Y-m-d', $data['do_iso']) ; 
				}
                $query  = " UPDATE ". TABLE_SETTINGS_COMPANY
                            ." SET ". TABLE_SETTINGS_COMPANY .".name = '".		$data['name'] ."'"
							//.",". TABLE_SETTINGS_COMPANY .".prefix = '". 		$data['prefix'] ."'"                               
							.",". TABLE_SETTINGS_COMPANY .".quot_prefix = '". 		$data['quot_prefix'] ."'"
							.",". TABLE_SETTINGS_COMPANY .".status = '". 		$data['status'] ."'" 
							.",". TABLE_SETTINGS_COMPANY .".tin_no = '". 			$data['tin_no'] ."'"                            
							.",". TABLE_SETTINGS_COMPANY .".service_tax_regn = '". 	$data['service_tax_regn'] ."'"                            
							.",". TABLE_SETTINGS_COMPANY .".pan_no = '". 			$data['pan_no'] ."'"                            
							.",". TABLE_SETTINGS_COMPANY .".cst_no = '". 			$data['cst_no'] ."'"                            
							.",". TABLE_SETTINGS_COMPANY .".vat_no = '". 			$data['vat_no'] ."'"                            
							.",". TABLE_SETTINGS_COMPANY .".do_implement = '". 		$do_implement ."'"
							.",". TABLE_SETTINGS_COMPANY .".do_implement_st = '". 	$do_implement_st ."'"                            
							.",". TABLE_SETTINGS_COMPANY .".do_iso = '". 			$do_iso ."'"                            
							." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Company has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SETTINGS_COMPANY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SETTINGS_COMPANY .".id = '". $id ."' )";
            
            if ( Company::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
					if($_ALL_POST['do_implement']=='0000-00-00 00:00:00'){
						$_ALL_POST['do_implement']='';
					}else{
						$_ALL_POST['do_implement']  = explode(' ', $_ALL_POST['do_implement']);
						$temp1               = explode('-', $_ALL_POST['do_implement'][0]);
						$_ALL_POST['do_implement']  = NULL;
						$_ALL_POST['do_implement']  = $temp1[2] .'/'. $temp1[1] .'/'. $temp1[0];
					}
					if($_ALL_POST['do_implement_st']=='0000-00-00 00:00:00'){
						$_ALL_POST['do_implement_st']='';
					}else{
						$_ALL_POST['do_implement_st']  = explode(' ', $_ALL_POST['do_implement_st']);
						$temp1               = explode('-', $_ALL_POST['do_implement_st'][0]);
						$_ALL_POST['do_implement_st']  = NULL;
						$_ALL_POST['do_implement_st']  = $temp1[2] .'/'. $temp1[1] .'/'. $temp1[0];
					}
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
             $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/company-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'company-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
