<?php
     if ($perm->has(EDIT)){


        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $output = "";
        Webaccess::getAllWebAccessGroups();
        $all_groups = $output ;
        
        include_once (DIR_FS_INCLUDES .'/web-access.inc.php');
        $table = TABLE_WEB_ACCESS_SETTING;
        $fields = '*';
        $condition ='';
        $_ALL_POST = getRecord($table,$fields,$condition);  
        if(!empty($_ALL_POST)){
            
            $_ALL_POST['group_id'] = trim($_ALL_POST['group_id'],',') ;
            $_ALL_POST['group_id']= explode(',',$_ALL_POST['group_id']) ;
            
        }
       
       
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'messages'          => &$messages
                        );
          
            if ( Webaccess::validateWebaccess($data, $extra) ) {
        
                
                $table = TABLE_WEB_ACCESS_SETTING;
                $fields = '*';
                $condition ='';
                $recordData = getRecord($table,$fields,$condition);
                $exe = false;
                $groupidStr = implode(",",$data['group_id']); 
                $groupidStr =",".$groupidStr."," ;
                
                if($data['access_type']=='N'){          
                 
                    $groupidStr='';
                }
             
                if(!empty($recordData)) {                    
                    $query	= " UPDATE ".TABLE_WEB_ACCESS_SETTING
                            ." SET ". TABLE_WEB_ACCESS_SETTING .".group_id = '".$groupidStr ."'"
                                .",". TABLE_WEB_ACCESS_SETTING .".access_type = '".$data['access_type'] ."'"   ; 
                    $exe =$db->query($query);
                    
                    $table =  $fields = $condition =$recordData='';                    
                    $table = TABLE_SITE_SETTINGS;
                    $fields = '*';
                    $condition ='';
                    $recordCheck = getRecord($table,$fields,$condition);  
                    if(!empty($recordCheck )){
                            $update_site_settings = "UPDATE ". TABLE_SITE_SETTINGS." SET " 
                              ."web_access = '".$data['access_type']."'  " ;    
                            $db->query($update_site_settings);
                    }else{
                    
                            $update_site_settings = " INSERT ". TABLE_SITE_SETTINGS." SET " 
                              ."web_access = '".$data['access_type']."'  " ;    
                            $db->query($update_site_settings);
                    }
                
                }else{
                
                    $query	= " INSERT ".TABLE_WEB_ACCESS_SETTING
                            ." SET ". TABLE_WEB_ACCESS_SETTING .".group_id = '".$groupidStr ."'"
                                .",". TABLE_WEB_ACCESS_SETTING .".access_type = '".$data['access_type'] ."'"   ; 
                    $exe = $db->query($query); 
                    
                    $table =  $fields = $condition =$recordData='';                    
                    $table = TABLE_SITE_SETTINGS;
                    $fields = '*';
                    $condition ='';
                    $recordCheck = getRecord($table,$fields,$condition);  
                    if(!empty($recordCheck )){
                    
                        $update_site_settings = "UPDATE ". TABLE_SITE_SETTINGS." SET " 
                            ."web_access = '".$data['access_type']."'  " ;    
                        $db->query($update_site_settings);
                    
                    }else{
                        $update_site_settings = " INSERT ". TABLE_SITE_SETTINGS." SET " 
                              ."web_access = '".$data['access_type']."'  " ;    
                        $db->query($update_site_settings);
                    }
                }
                
                
                
                if ( $exe ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
   
     
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            include ( DIR_FS_ADMIN .'/web-access-list.php');
        }
        else {
                
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'mark');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
             $page["var"][] = array('variable' => 'all_groups', 'value' => 'all_groups');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'web-access-mark.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
          $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-edit-permission.html');
    }
?>