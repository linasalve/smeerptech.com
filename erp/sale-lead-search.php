<?php

	$condition_query= '';
    $condition_url  = '';
    
    if ( $sString != "" ) {
	
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
		if ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
			$condition_url .= "&sString=". $sString;
			$condition_url .= "&sType=". $sType;
			$where_added = true;
		}
		elseif ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
			foreach( $sTypeArray as $table => $field_arr ) {
				if ( $table != "Any" ) {
					foreach( $field_arr as $key => $field ) {
                        $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
						$where_added = true;
					}
				}
			}
			$condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
			$condition_query .= ") ";
		}
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}
	
	// BO: Member type data.
    $mTypeStr = '';
    $chk_mtype = isset($_POST["chk_mtype"])  ? $_POST["chk_mtype"]  : (isset($_GET["chk_mtype"])   ? $_GET["chk_mtype"]   :'');
    $mType  = isset($_POST["mType"]) ? $_POST["mType"] : (isset($_GET["mType"]) ? $_GET["mType"] :'');
	
	if ( ($chk_mtype == 'AND' || $chk_mtype == 'OR')  && isset($mType)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_mtype;
        }else{
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        if(is_array($mType)){
			$str = '';
			foreach($mType as $key=>$val ){
				$str.= TABLE_SALE_LEADS .".member_type LIKE '%,".$val.",%' AND " ;
			}
			$str = substr( $str, 0, strlen( $str ) - 4 );
            $mTypeStr1 = implode(",", $mType) ;
             
        }else{
            $mTypeStr1 = $mType ;
            $mType = explode(",",$mType) ;
			$str = '';
			foreach($mType as $key=>$val ){
				$str.= TABLE_SALE_LEADS .".member_type LIKE '%,".$val.",%' AND " ;
			}
			$str = substr( $str, 0, strlen( $str ) - 4 );
        }
        $condition_query .= " ( ".$str." ) ";
        $condition_url          .= "&chk_mtype=$chk_mtype&mType=$mTypeStr1";
        $_SEARCH["chk_mtype"]  = $chk_mtype;
        $_SEARCH["mType"]     = $mType;        
    }
	//EO: Member type data.
	
	//echo $condition_query;
	$condition_url = "&rpp=$rpp"
                        ."&sString=$sString"
                        ."&sType=$sType"
                        ."&sOrderBy=$sOrderBy"
                        ."&sOrder=$sOrder"
                        ."&action=search";
			
	$_SEARCH["sString"] 	= $sString ;
	$_SEARCH["sType"] 		= $sType ;
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/sale-lead-list.php');
?>