<?php

    if ( $perm->has('nc_uc_su_list') ) {
    
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
        $parent_id = isset($_GET["parent_id"]) ? $_GET["parent_id"] : ( isset($_POST["parent_id"]) ? $_POST["parent_id"] : '' );
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
        }
        else {
            $condition_query .= ' AND (';
        }
        $access_level   = $my['access_level'];
        
        // If the User has the Right to View Clients of the same Access Level.
        if ( $perm->has('nc_uc_su_list_al') ) {
            $access_level += 1;
        }
        // If my is the Manager of this Client.
        $condition_query .= " (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."' "
                                ." AND ". TABLE_CLIENTS .".access_level < $access_level ) ";
        
        // Check if the User has the Right to view Clients of other Executives.
        if ( $perm->has('nc_uc_su_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_uc_su_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_CLIENTS. ".manager != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_CLIENTS .".access_level < $access_level_o ) ";
        }
        $condition_query .= ')';
        if(!empty($parent_id)){
            $condition_query .= " AND parent_id='".$parent_id."'";
        }else{
            $condition_query .= " AND parent_id='".$user_id."'";
            
        }
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	Clients::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        Clients::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
        
        /*$phone  = new Phone(TABLE_CLIENTS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_CLIENTS, $list[$i]['user_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}*/
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_uc_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_uc_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_uc_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_uc_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_uc_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_uc_status') ) {
            $variables['can_change_status']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-sub-user-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>