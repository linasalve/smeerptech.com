<?php
if ( $perm->has('nc_itm_p_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    $action='';
    
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	ItemsPurchase::getDetails( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_ITEMS_PURCHASE.'.*';
    ItemsPurchase::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
	$db_new 		= new db_local;
	$fList = array();
	if(!empty($list)){
		foreach( $list as $key=>$val){
		
			$val['item_type_name']='';
			 $val['vendor_name']='';
		    if(!empty($val['item_type_id'])){
				$table = TABLE_ITEMS_TYPE;
                $condition2 = " WHERE ".TABLE_ITEMS_TYPE .".id= '".$val['item_type_id'] ."' " ;
                $fields1 =  TABLE_ITEMS_TYPE.'.name' ;
                $typeArr = getRecord($table,$fields1,$condition2);
                if(!empty($typeArr)){
                    $typename = $typeArr['name'] ;                    
                }
                $val['item_type_name']= $typename ;
		    }
			if(!empty($val['vendor_id'])){
				$table = TABLE_VENDORS;
                $condition2 = " WHERE ".TABLE_VENDORS .".user_id= '".$val['vendor_id'] ."' " ;
                $fields1 =  TABLE_VENDORS.'.f_name,'.TABLE_VENDORS.'.l_name,'.TABLE_VENDORS.'.billing_name' ;
                $vendorArr = getRecord($table,$fields1,$condition2);
                if(!empty($vendorArr)){
                    $vendor_name = $vendorArr['f_name']." ". $vendorArr['l_name']." ( ". $vendorArr['billing_name']." )";                    
                }
                $val['vendor_name']= $vendor_name ;
		    }
			if(!empty($val['family_linking'])){
				$family_link='';
				$str_fl=trim(",",$val['family_linking']);
				$family_link_str = str_replace(",","','",$val['family_linking']);
				$fields = TABLE_ITEMS_PURCHASE.".item_name,".TABLE_ITEMS_PURCHASE.".code";
				$condition = " WHERE ".TABLE_ITEMS_PURCHASE.".id IN (".$family_link_str.")" ;
				ItemsPurchase::getDetails( $db, $family_link, $fields, $condition_query);
				$val['familylink']=$family_link ;
				
			}
			 $fList[$key]=$val;
		}
	}
	
	
    // Set the Permissions.
    $variables['INUSED'] = ItemsPurchase::INUSED ;
    $variables['can_view_list']     = false;
	$variables['can_add']           = false;
	$variables['can_edit']          = false;
	$variables['can_delete']        = false;
	$variables['can_change_status']  = false;
        
    if ( $perm->has('nc_itm_p_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_itm_p_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_itm_p_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_itm_p_delete') ) {
        $variables['can_delete'] = true;
    }
  
    if ( $perm->has('nc_itm_p_status') ) {
        $variables['can_change_status']     = true;
     }
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'items-purchase-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
