<?php

    if ( $perm->has('nc_pr_su_list') ) {
        
        include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
        include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
        
        $region         = new RegionProspects();
        $phone          = new PhoneProspects(TABLE_PROSPECTS);
        
        $variables['send_mail_yes'] = Prospects::SENDMAILYES;
        $variables['send_mail_no'] = Prospects::SENDMAILNO;
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
        }
        else {
            $condition_query .= ' AND (';
        }
        
        $access_level   = $my['access_level'] +1 ;        
         
         //show the list of clients of same access levels
        $condition_query .= "".TABLE_PROSPECTS .".access_level <= $access_level ";       
        $condition_query .= ')';
        $condition_query .= " AND parent_id = '".$client_id."' ";
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
         //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        $total	=	Prospects::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        Prospects::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
        
        /*$phone  = new Phone(TABLE_PROSPECTS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_PROSPECTS, $list[$i]['user_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}*/
        
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               /*$client=array();
               $string = str_replace(",","','", $val['alloted_clients']);
               $fields1 =  TABLE_PROSPECTS .'.f_name'.','. TABLE_PROSPECTS .'.l_name';
               $condition1 = " WHERE ".TABLE_PROSPECTS .".user_id IN('".$string."') " ;
               Prospects::getList($db,$client,$fields1,$condition1);
               $clientname='';
              
               foreach($client as $key1=>$val1){
                    $clientname .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['alloted_clients'] = $clientname ;*/
               
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_PROSPECTS, $val['user_id']);
                $val['phone'] = $phone->get($db);
                
                // Read the Addresses.
                $region->setAddressOf(TABLE_PROSPECTS, $val['user_id']);
                $val['address_list'] = $region->get();
                
                
               $fList[$key]=$val;
            }
        }
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_up_mail_status'] = false;
        //$variables['can_send_password'] = false;
        
        if ( $perm->has('nc_pr_su_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_pr_su_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_pr_su_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_pr_su_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_pr_su_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_pr_su_status') ) {
            $variables['can_change_status'] = true;
        }
       
        if ( $perm->has('nc_pr_su_up_mst') ) {
            $variables['can_up_mail_status'] = true;
        }
        
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-su-list.html');   
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>