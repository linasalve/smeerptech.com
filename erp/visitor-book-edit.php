<?php
    if ( $perm->has('nc_vb_edit') ) {
    
        $visitor_id     = isset($_GET["visitor_id"]) ? $_GET["visitor_id"] : ( isset($_POST["visitor_id"]) ? $_POST["visitor_id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/visitor-book.inc.php');
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=12;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        }        
        $lst_typ =array('1'=>'AM','2'=>'PM');
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            $data['in_time'] =$data['intime_hrs'].":".$data['intime_min']." ".$data['intime_type'];
            $data['out_time'] =$data['outtime_hrs'].":".$data['outtime_min']." ".$data['outtime_type'];
         
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Visitor::validateUpdate($data, $extra) ) {
			
				$important = 0;
				if(isset($data['important'])){
					$important=1;
				}
				
                $query  = " UPDATE ". TABLE_VISITOR_BOOK
                           ." SET ". TABLE_VISITOR_BOOK .".name = '".             $data['name'] ."'"
							.",". TABLE_VISITOR_BOOK .".company_name = '".        $data['company_name'] ."'"
							.",". TABLE_VISITOR_BOOK .".contact_no = '".          $data['contact_no'] ."'"
							.",". TABLE_VISITOR_BOOK .".address = '".             $data['address'] ."'"
							.",". TABLE_VISITOR_BOOK .".in_time = '".             $data['in_time'] ."'"
							.",". TABLE_VISITOR_BOOK .".out_time = '".            $data['out_time'] ."'"
							.",". TABLE_VISITOR_BOOK .".attend_by = '".           $data['attend_by'] ."'"
							.",". TABLE_VISITOR_BOOK .".important = '".           $important ."'"
							.",". TABLE_VISITOR_BOOK .".purpose_id = '".          $data['purpose_id'] ."'"
							.",". TABLE_VISITOR_BOOK .".purpose = '".             $data['purpose'] ."'"
                            .",". TABLE_VISITOR_BOOK .".date = '".                $data['date'] ."'"
                            ." WHERE id = '". $visitor_id ."'";
              
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Visitor has been updated successfully.");
                    $variables['hid'] = $visitor_id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the visitor which is to be Updated.
            $fields = TABLE_VISITOR_BOOK .'.*'  ;            
            $condition_query = " WHERE (". TABLE_VISITOR_BOOK .".id = '". $visitor_id ."' )";
            
            if ( Visitor::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                //if ( $_ALL_POST['access_level'] < $access_level ) {
                
                    $in_timeArr  = explode(' ', $_ALL_POST['in_time']);                   
                    $in_time = explode(':',$in_timeArr[0]);
                    $_ALL_POST['intime_hrs'] =  $in_time[0];
                    $_ALL_POST['intime_min'] =  $in_time[1];
                    $_ALL_POST['intime_type'] = $in_timeArr[1];
                    $out_timeArr  = explode(' ', $_ALL_POST['out_time']);
                    $out_time = explode(':',$out_timeArr[0]);
                    $_ALL_POST['outtime_hrs'] =  $out_time[0];
                    $_ALL_POST['outtime_min'] =  $out_time[1];
                    $_ALL_POST['outtime_type'] = $out_timeArr[1];
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    $visitor_id         = $_ALL_POST['id'];
                //}
                //else {
                     //$messages->setErrorMessage("You do not have the Permission to view the Record with the current Access Level.");
                //}
            }
            else {
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {            
            $variables['hid'] = $visitor_id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/visitor-book-list.php');
        } else {
           
			$variables['purposelist'] = Visitor::getPurposeList();	
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'visitor_id', 'value' => $visitor_id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'lst_typ', 'value' => 'lst_typ');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'visitor-book-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
