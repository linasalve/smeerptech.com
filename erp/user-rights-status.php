<?php
	if ( $perm->has('nc_urht_status') ) {
        $right_id	= isset($_GET["right_id"]) 	? $_GET["right_id"] 	: ( isset($_POST["right_id"]) 	? $_POST["right_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
        
        $extra = array( 'db' 			=> &$db,
                        'messages' 		=> $messages
                    );
        
        UserRights::updateStatus($right_id, $status, $messages, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/user-rights-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>