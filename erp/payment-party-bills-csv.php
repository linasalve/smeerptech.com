<?php

 
		// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	
	include_once ( DIR_FS_INCLUDES .'/payment-party-bills.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/company.inc.php' );

 $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x_csv 			= isset($_GET["x_csv"])         ? $_GET["x_csv"]        : ( isset($_POST["x_csv"])          ? $_POST["x_csv"]       : '');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');
    $deleted 	= isset($_GET["deleted"])   ? $_GET["deleted"]  : ( isset($_POST["deleted"])    ? $_POST["deleted"] : '0');
    $inv_no 	= isset($_GET["inv_no"])    ? $_GET["inv_no"]   : ( isset($_POST["inv_no"])     ? $_POST["inv_no"]  : '');

    $hid 		= isset($_GET["hid"])         ? $_GET["hid"]        : ( isset($_POST["hid"])          ? $_POST["hid"]       :'');
	$action='Proforma Invoice';
	
	$sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
					TABLE_PAYMENT_PARTY_BILLS   =>  array(
                                                          'Bill Details'  => 'bill_details',
                                                          'Number'  => 'number',
                                                          'Bill No'  => 'bill_no',
                                                          'Proforma Bill No'  => 'profm_bill_no',
                                                          'Transaction No'  => 'transaction_number',
                                                          'Bill Paid Amount'  => 'amount',
                                                          'Bill Amount'  => 'bill_amount',
                                                          'Late Amount'  => 'late_amount',
                                                          'Added by'     => 'created_by_name',
                                                        ),
                    TABLE_PAYMENT_BILLS_AGAINST   =>  array(
                                                          'Bill Against'  => 'bill_against'
                                                        ),
					TABLE_CLIENTS   =>    array(
                                                      'Members First Name'  => 'f_name-'.TABLE_CLIENTS,
                                                      'Members Last Name'  => 'l_name-'.TABLE_CLIENTS,
                                                      'Members Billing Name'  => 'billing_name-'.TABLE_CLIENTS
                                                    ),
                     
                    TABLE_USER     => array(
                                                'Executive First Name' => 'f_name-'.TABLE_USER,
                                                'Executive Last Name' => 'l_name-'.TABLE_USER                   
                                            ), 
                                );
        
        $sOrderByArray  = array(
                                TABLE_PAYMENT_PARTY_BILLS => array(  
														  'Number'   => 'number',
														  'Bill Date'   => 'bill_dt',
														  'Date'   => 'do_e'
                                                        ),
                                );
	
	
    if ( empty($x_csv) ) {
        $x_csv              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x_csv-1) * $rpp;
    }
    $variables["x_csv"]     = $x_csv;
    $variables["rpp"]   = $rpp;
	
	 if(!empty($hid)){
		$_SEARCH['sOrderBy']= $sOrderBy = 'id';
		$variables['hid']=$hid;
	}else{
		$_SEARCH['sOrderBy']= $sOrderBy = 'bill_dt';
	}
	$_SEARCH['sOrder']  = $sOrder   = 'DESC';
	$order_by_table     = TABLE_PAYMENT_PARTY_BILLS;

	
	
	include ( DIR_FS_NC .'/payment-party-bills-csv-search.php');
	 
if($perform=='search'){

		if ( !isset($_SEARCH) ) {
			$_SEARCH = '';
		}
		$_SEARCH['searched'] = 1;
		
		if(empty($condition_query)){
			$_SEARCH["chk_status"]  = 'AND';
			$_SEARCH["sStatus"]    = array(PaymentPartyBills::ACTIVE);
			$_SEARCH["chk_fstatus"]  = 'AND';
			$_SEARCH["fStatus"]    = array(PaymentPartyBills::FORCEDEACTIVE);
			$_SEARCH["chk_bstatus"]  = 'AND';
			$_SEARCH["bStatus"] = array(PaymentPartyBills::BILL_STATUS_UNPAID,PaymentPartyBills::BILL_STATUS_PARTIALPAID); 
			$condition_query = " WHERE ".TABLE_PAYMENT_PARTY_BILLS .".f_status='". PaymentPartyBills::FORCEDEACTIVE ."'
			AND ".TABLE_PAYMENT_PARTY_BILLS .".bill_status IN('".PaymentPartyBills::BILL_STATUS_UNPAID ."',
					'".PaymentPartyBills::BILL_STATUS_PARTIALPAID ."') 
			AND ".TABLE_PAYMENT_PARTY_BILLS .".status IN('".PaymentPartyBills::ACTIVE ."')";
			$perform = 'search';
			$condition_url   = "&chk_fstatus=".$_SEARCH["chk_fstatus"]."&fStatus=".PaymentPartyBills::FORCEDEACTIVE."&
			chk_bstatus=".$_SEARCH["chk_bstatus"]."&bStatus=".PaymentPartyBills::BILL_STATUS_UNPAID.","
			.PaymentPartyBills::BILL_STATUS_PARTIALPAID."&chk_status=".$_SEARCH["chk_status"]."&
			sStatus=".PaymentPartyBills::ACTIVE;   
		}
		$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
		
	   
		//echo $condition_query;
		$totalAmt =array();
		$totalAmount=$totalAmountPerbill=$totalBalanceAmount='';
		$fields=" SUM(".TABLE_PAYMENT_PARTY_BILLS.".bill_amount) as totalAmount,
		SUM(".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill) as totalAmountPerbill,
		SUM(".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs) as totalAmountAdjsbill,
		SUM(".TABLE_PAYMENT_PARTY_BILLS.".balance_inr) as totalBalanceAmount ";
		PaymentPartyBills::getList($db, $totalAmt, $fields, $condition_query);
		if(!empty($totalAmt)){
			$totalAmount = $totalAmt[0]['totalAmount'];
			$totalAmountPerbill = $totalAmt[0]['totalAmountPerbill'];
			$totalBalanceAmount = $totalAmt[0]['totalBalanceAmount'];
			$totalAmountAdjsbill = $totalAmt[0]['totalAmountAdjsbill']; 
		}
		
		//Totla of Bill Amount/Current Charges = bill_amount,Bill Amount ( As per the Bill)=bill_amount_perbill,Bill Amount (Approved by SMEERP )=bill_amount_adjs
		
		
		// To count total records.
		
		$extra_url  = '';
		if ( isset($condition_url) && !empty($condition_url) ) {
			$extra_url  = $condition_url;
		}
		$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  = '&start=url'. $extra_url .'&end=url';
		
		$condition_url .="&rpp=".$rpp."&perform=".$perform;
		
		
		
        //Code to calculate total of Amount EOF
        
		//CSV Variables BOF
		$csv_terminated = "\n";
		$csv_separator = ",";
		$csv_enclosed = '"';
		$csv_escaped = "\\";
		$schema_insert = '';
		$schema_insert_nt = '';
		$schema_insert1 = '';
		$schema_insert2 = '';
		$grant_total1 = 0; 
		//CSV Variables EOF
		$out_1=$out_2='';
		
        // To count total records.
        $list	= 	NULL;
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
		$searchStr=1;
        if($searchStr==1){
           $list	= 	NULL;
			$total	=	PaymentPartyBills::getList( $db, $list, '', $condition_query);
		
			$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','payment-party-bills.php','frmSearch');
			$list	= NULL;
			$fields = TABLE_PAYMENT_PARTY_BILLS.'.*,'.TABLE_CLIENTS.'.f_name as mfname,'.TABLE_CLIENTS.'.l_name as mlname,
			'.TABLE_CLIENTS.'.billing_name as mbilling_name, ';
			$fields .= "team_table.f_name as tfname,team_table.l_name as tlname," ;
			$fields .= TABLE_USER.".f_name as ufname,".TABLE_USER.".l_name as ulname," ;
			$fields .= TABLE_PAYMENT_BILLS_AGAINST.".bill_against" ;
			PaymentPartyBills::getList( $db, $list, $fields, $condition_query, $next_record, $rpp); 
        }
        $key2=0;
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){  
			    $key2++; 
				 
				$fields1 =  TABLE_SETTINGS_COMPANY .'.name as company_name';                
				$condition2 = " WHERE ".TABLE_SETTINGS_COMPANY .".id=".$val['company_id']." " ;
				$company_name='';
				$table =TABLE_SETTINGS_COMPANY ;              
				$compArr = getRecord($table,$fields1,$condition2);
				if(!empty($compArr)){
					$company_name = $compArr['company_name']  ;   
				}  
				$number =$val['number'] ; 
				$billing_name =$val['mbilling_name'] ;
				$team_name =$val['tfname']." ".$val['tlname']  ;
				$transaction_number = $val['transaction_number'];
				if($val['bill_status']== PaymentPartyBills::BILL_STATUS_UNPAID){
					$bill_status_name='Unpaid';
				}elseif($val['bill_status']== PaymentPartyBills::BILL_STATUS_PAID){
					$bill_status_name='Paid';			
				}elseif($val['bill_status']== PaymentPartyBills::BILL_STATUS_PARTIALPAID){
					$bill_status_name='Partly Paid';
				}
				$bill_dt = date('d M Y',strtotime($val['bill_dt'])) ;
				$bill_against = $val['bill_against'] ;
				$bill_amount_adjs = $val['bill_amount_adjs'] ;
				$balance_inr = $val['balance_inr'] ; 				
				$myTData=array(
					0=>'key2',
					1=>'number',					 
					2=>'company_name',
					3=>'billing_name',
					4=>'team_name',
					5=>'transaction_number',
					6=>'bill_status_name',
					7=>'bill_dt',
					8=>'bill_against',
					9=>'bill_amount_adjs',
					10=>'balance_inr'
				);				
				$schema_insert11='';
				foreach($myTData as $key_1 =>$val3){
					$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes($$val3)) . $csv_enclosed;
					$schema_insert11 .= $l.$csv_separator;
				}
				$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
				$schema_insert1 .= $csv_terminated; 
            }
        } 
	$heading1=array('Sr No','Number','Company Name','Vendor Name','Team Name',
	'TransactionNo','Bill Status','Bill Date','Bill Against','BillAmt(as per SMEERP)','Bal Amt');
	
	/*number, inv_no,financial_yr,or_no,order_title,do_o,do_c,do_fe,do_e,do_i,billing_name,sub_total_amount,tax1_name,tax_amount,round_off_op - round_off, amount,amount_inr,{if $invoice.prfm_balance > 0}  prfm_balance {/if}, prfm_balance_inr,rcpt_amount,rcpt_amount_payable_tax,
	total_tax_paid_amount,total_tax_balance_amount
	*/
	
	
	$fields_cnt = count($heading1);
	/* $mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results for Purchae Bills - '.$company_name.' - Period  - '.date('d M Y',strtotime($dfa)) .' - '.date('d M Y',strtotime($dta)).' '.$taxname_str.' ')) . $csv_enclosed;
	 */
	$mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results found - ')) . $csv_enclosed;
			
	$schema_insert .= $mHead1;
	$schema_insert .= $csv_terminated;
	$schema_insert .= $csv_separator;
	$schema_insert .= $csv_terminated;
	$mHead2 = '';
	/* $mHead2 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes( $taxname.' Particulars of the Bills')). $csv_enclosed; */
	$schema_insert .= $mHead2;
	$schema_insert .= $csv_terminated;
	$schema_insert .= $csv_separator;
	$schema_insert .= $csv_terminated;
	for ($i = 0; $i < $fields_cnt; $i++){
		$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($heading1[$i])) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
	}// end for
 
	$out1 = trim(substr($schema_insert, 0, -1));
	$out1 .= $csv_terminated;
	$out1 .= $schema_insert1 ;
	$filename = date('dmY').".csv";
	//$out = $out_1.$csv_terminated.$out_2;
	/* 
	$myTData=array(0=>'key',
			  1=>'billing_name',
			  2=>'inv_no',
			  3=>'do_i',
			  4=>'pinv_no',
			  5=>'do_pi',
			  6=>'pbalance',
			  7=>'pbalance_inr',
			  8=>'amount',
			  9=>'rcpt_no',
			  10=>'do_r',
			  11=>'do_transaction',
			  12=>'do_voucher',
			  13=>'rcpt_amount',
			  14=>'rcpt_amount_payable_tax',
			  15=>'total_tax_paid_amount',
			  16=>'total_tax_balance_amount',
			  17=>'tds_amount',
			  18=>'tax1_name',
			  19=>'tax1_value',
			  20=>'tax1_amount',
			  21=>'tax1_sub1_name',
			  22=>'tax1_sub1_value',
			  23=>'tax1_sub1_amount',
			  24=>'tax1_sub2_name',
			  25=>'tax1_sub2_value',
			  26=>'tax1_sub2_amount'
						  
	);
	$schema_insert11='';
	foreach($myTData as $key_1 =>$val){
		$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes($$val)) . $csv_enclosed;
		//$schema_insert1 .= $l;
		$schema_insert11 .= $l.$csv_separator;
	}
	
	
	
	$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
	$schema_insert1 .= $csv_terminated;*/
	//$out1 = trim(substr($schema_insert1, 0, -1));
	//$out1 .= $csv_terminated;
	
	
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Length: " . strlen($out1));
	// Output to browser with appropriate mime type, you choose ;)
	header("Content-type: text/x-csv");
	//header("Content-type: text/csv");
	//header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=$filename");
	echo $out1;
	//Write CSV EOF
		exit;
	//EOF DOWNLOAD CSV


}
	
	
?>