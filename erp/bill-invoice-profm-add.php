<?php
    if ( $perm->has('nc_bl_invprfm_add') ) {
        //include_once ( DIR_FS_INCLUDES .'/currency.inc.php');      
       include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
       include_once ( DIR_FS_INCLUDES .'/financial-year.inc.php');
       include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php' );
	   include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
        
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $exchange_rate          = isset($_POST["exchange_rate"]) ? $_POST["exchange_rate"] : '1' ;
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_invprfm_add_al') ) {
            $access_level += 1;
        }
        $_ALL_POST['exchange_rate'] = $exchange_rate; 
        
        $extra = array( 'db' 				=> &$db,
                        'access_level'      => $access_level,
                        'messages'          => &$messages
                        );
                        
        $currency = NULL ;
        $required_fields = '*' ;
		Currency::getList($db,$currency,$required_fields);        
        
		//Get company list 
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                   .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
		
        //getfinancial yr bof
        $lst_fyr = null;
        $lst_fyr = FinancialYear::yearRange();
        //getfinancial yr eof        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn']) || isset($_POST['btnInvReturn']) || isset($_POST['btnInvFlwReturn']) ) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,                        
                            'messages'          => &$messages
                        );
            $data['do_c'] = time();     
            if ( InvoiceProforma::validateAdd($data, $extra) ) {
                //get currency abbr from d/b bof
                $mail_client = 0;
                if(isset($data['mail_client'])){
                    $mail_client = 1;
                }
                $currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                $fields1 = "*";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);              
                if(!empty($currency1)){
                   $data['currency_abbr'] = $currency1[0]['abbr'];
                   $data['currency_name'] = $currency1[0]['currency_name'];
                   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
                   $data['currency_country'] = $currency1[0]['country_name'];
                }
                //get currency abbr from d/b eof                      
                $company1 =null;
                $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                $fields_c1 = "name,prefix,quot_prefix, 
				tin_no,service_tax_regn,pan_no,cst_no,vat_no,do_implement,do_implement_st,do_iso";
                Company::getList( $db, $company1, $fields_c1, $condition_query_c1); 
				$data['do_tax']=$data['do_st']=0;
				$do_tax=$do_st='';
                if(!empty($company1)){                  
                   $data['company_name'] = $company1[0]['name'];
                   $data['company_prefix'] = $company1[0]['prefix'];
				   $data['company_quot_prefix'] = $company1[0]['quot_prefix'];
				   $data['do_tax']     = $company1[0]['do_implement'];	
				   $data['do_st']     = $company1[0]['do_implement_st'];
				   $data['do_iso']     = $company1[0]['do_iso'];
				   
				   $data['tin_no'] 			 = '';
				   $data['service_tax_regn'] 	 = '';
				   $data['pan_no'] 			 = $company1[0]['pan_no'];
				   $data['cst_no'] 			 = '';
				   $data['vat_no'] 			 = '';
				   
				    if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
						$do_tax=$data['do_tax'];
						$data['do_tax']= strtotime($data['do_tax']);				
					}					
					if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){
					   $data['tin_no'] 			 = $company1[0]['tin_no'];
					   $data['cst_no'] 			 = $company1[0]['cst_no'];
					   $data['vat_no'] 			 = $company1[0]['vat_no'];
					}
					if( $data['do_st']!=0 &&  $data['do_st']!='0000:00:00 00:00:00'){
						$do_st = $data['do_st'];
						$data['do_st'] = strtotime($data['do_st']);				
					}
					if($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00'){
					   $data['service_tax_regn'] = $company1[0]['service_tax_regn'];
					}
                }
                $data['invoice_title']='PROFORMA INVOICE';
				$data['profm']='Proforma';
                //Get billing address BOF
                 include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS,  $data['client']['user_id']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $address_list  =  $address_list;
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
                //$data['b_address'] = $address_list ;
                //Get billing address EOF
				$show_period=$data['show_period_chk']=0;
				if(isset($data['show_period'])){
					$data['show_period_chk'] = $data['show_period'];
				}
                

                if(!empty($data['do_e'])){
                    $data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                    $data['is_renewable']=1;
                }else{
                    $data['do_e'] ='';
                    $data['is_renewable']=0;
                }
                if(!empty($data['do_fe'])){
                    $data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }else{
                    $data['do_fe'] ='';
                }
				              
                $sub_str='';
				
                $query	= " INSERT INTO ".TABLE_BILL_INV_PROFORMA
						." SET ". TABLE_BILL_INV_PROFORMA .".number = '".        $data['number'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".inv_counter = '".$data['inv_counter'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".show_period = '". $data['show_period_chk']."'"
						.",". TABLE_BILL_INV_PROFORMA .".or_id = '".         $data['or_id'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".or_no = '".         $data['or_no'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".service_id = '". ",".trim($data['service_id'],",").","."'"
						.",". TABLE_BILL_INV_PROFORMA .".tax_ids = '".         $data['tax_ids'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".delivery_at = '".         $data['delivery_at'] ."'"						
						.",". TABLE_BILL_INV_PROFORMA .".access_level = '".  $data['access_level'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".created_by = '".     $my['user_id'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".client = '".        $data['client']['user_id'] ."'"                    
						.",". TABLE_BILL_INV_PROFORMA .".currency_abbr='". processUserData($data['currency_abbr']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".currency_id = '".   $data['currency_id'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".currency_name = '".processUserData($data['currency_name']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".currency_symbol = '".processUserData($data['currency_symbol']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".currency_country ='".processUserData($data['currency_country']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".exchange_rate = '". $data['exchange_rate'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".amount = '".        $data['amount'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".round_off = '".     $data['round_off'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".round_off_op = '".  $data['round_off_op'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".amount_inr = '".    $data['amount_inr'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".amount_words = '".  $data['amount_words'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".do_c = '".          date('Y-m-d H:i:s', $data['do_c']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".do_e = '".          $data['do_e'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".do_fe = '".         $data['do_fe']."'"
						.",". TABLE_BILL_INV_PROFORMA .".remarks = '".       $data['remarks'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".balance = '".       $data['balance'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".balance_inr = '".   $data['amount_inr'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".company_id = '".    $data['company_id'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".company_name = '". processUserData($data['company_name']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".tin_no   		   = '". $data['tin_no'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".service_tax_regn   = '". $data['service_tax_regn'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".do_st   		  = '". $do_st ."'"
						.",". TABLE_BILL_INV_PROFORMA .".pan_no  		   = '". $data['pan_no'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".cst_no  		   = '". $data['cst_no'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".vat_no  		   = '". $data['vat_no'] ."'"						
						.",". TABLE_BILL_INV_PROFORMA .".do_tax   		  = '". $do_tax ."'"
						.",". TABLE_BILL_INV_PROFORMA .".company_prefix = '".processUserData($data['company_prefix']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".company_quot_prefix= '".processUserData($data['company_quot_prefix']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".billing_name      = '".processUserData($data['billing_name']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".billing_address     = '".processUserData($data['billing_address']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".b_addr_company_name = '".processUserData($data['b_addr_company_name']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".b_addr              = '".processUserData($data['b_addr'])."'"
						.",". TABLE_BILL_INV_PROFORMA .".b_addr_city         = '".processUserData($data['b_addr_city'] )."'"
						.",". TABLE_BILL_INV_PROFORMA .".b_addr_state        = '".processUserData($data['b_addr_state']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".b_addr_country      = '".processUserData($data['b_addr_country']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".b_addr_zip          = '".processUserData($data['b_addr_zip']) ."'"
						.",". TABLE_BILL_INV_PROFORMA .".is_renewable        = '". $data['is_renewable'] ."'"
						.",". TABLE_BILL_INV_PROFORMA .".ip                  = '".$_SERVER['REMOTE_ADDR']."'"
						.",". TABLE_BILL_INV_PROFORMA .".status              = '".$data['status'] ."'" ;
             
                    $sub_str= ",". TABLE_BILL_INV .".or_id = '".         $data['or_id'] ."'"
						.",". TABLE_BILL_INV .".or_no = '".         $data['or_no'] ."'"
						.",". TABLE_BILL_INV .".show_period = '". $data['show_period_chk']."'"
						.",". TABLE_BILL_INV .".delivery_at = '".         $data['delivery_at'] ."'"
						.",". TABLE_BILL_INV .".service_id = '".         $data['service_id'] ."'"
						.",". TABLE_BILL_INV .".tax_ids = '".         $data['tax_ids'] ."'"
						.",". TABLE_BILL_INV .".access_level = '".  $data['access_level'] ."'"
						.",". TABLE_BILL_INV .".created_by = '".    $my['user_id'] ."'"
						.",". TABLE_BILL_INV .".client = '".        $data['client']['user_id'] ."'"                        
						.",". TABLE_BILL_INV .".currency_abbr = '". processUserData($data['currency_abbr']) ."'"
						.",". TABLE_BILL_INV .".currency_id = '".   $data['currency_id'] ."'"
						.",". TABLE_BILL_INV .".currency_name    = '". processUserData($data['currency_name']) ."'"
						.",". TABLE_BILL_INV .".currency_symbol  = '". processUserData($data['currency_symbol']) ."'"
                        .",". TABLE_BILL_INV .".currency_country = '". processUserData($data['currency_country']) ."'"
						.",". TABLE_BILL_INV .".exchange_rate = '". $data['exchange_rate'] ."'"
						.",". TABLE_BILL_INV .".amount = '".        $data['amount'] ."'"
						.",". TABLE_BILL_INV .".round_off = '".     $data['round_off'] ."'"
						.",". TABLE_BILL_INV .".round_off_op = '".  $data['round_off_op'] ."'"
						.",". TABLE_BILL_INV .".amount_inr = '".    $data['amount_inr'] ."'"
						.",". TABLE_BILL_INV .".amount_words = '".  $data['amount_words'] ."'"
						.",". TABLE_BILL_INV .".do_c = '".          date('Y-m-d H:i:s', $data['do_c']) ."'"
						.",". TABLE_BILL_INV .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
						.",". TABLE_BILL_INV .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
						.",". TABLE_BILL_INV .".do_e = '".          $data['do_e'] ."'"
						.",". TABLE_BILL_INV .".do_fe = '".         $data['do_fe']."'"
						.",". TABLE_BILL_INV .".remarks = '".       $data['remarks'] ."'"
						.",". TABLE_BILL_INV .".balance = '".       $data['balance'] ."'"
						.",". TABLE_BILL_INV .".balance_inr = '".   $data['amount_inr'] ."'"
						.",". TABLE_BILL_INV .".company_id = '".    $data['company_id'] ."'"
						.",". TABLE_BILL_INV .".company_name = '". processUserData($data['company_name']) ."'"
						.",". TABLE_BILL_INV .".tin_no   		   = '". $data['tin_no'] ."'"
						.",". TABLE_BILL_INV .".service_tax_regn   = '". $data['service_tax_regn'] ."'"
						.",". TABLE_BILL_INV .".do_st   		  = '". $do_st ."'"
						.",". TABLE_BILL_INV .".pan_no  		   = '". $data['pan_no'] ."'"
						.",". TABLE_BILL_INV .".cst_no  		   = '". $data['cst_no'] ."'"
						.",". TABLE_BILL_INV .".vat_no  		   = '". $data['vat_no'] ."'"						
						.",". TABLE_BILL_INV .".do_tax   		  = '". $do_tax ."'"
						.",". TABLE_BILL_INV .".company_prefix = '".processUserData($data['company_prefix']) ."'"
						.",". TABLE_BILL_INV .".company_quot_prefix='".processUserData($data['company_quot_prefix'])."'"
						.",". TABLE_BILL_INV .".billing_name      = '".processUserData($data['billing_name']) ."'"
						.",". TABLE_BILL_INV .".billing_address     = '".processUserData($data['billing_address']) ."'"
						.",". TABLE_BILL_INV .".b_addr_company_name = '".processUserData($data['b_addr_company_name']) ."'"
						.",". TABLE_BILL_INV .".b_addr              = '".processUserData($data['b_addr'])."'"
						.",". TABLE_BILL_INV .".b_addr_city         = '".processUserData($data['b_addr_city'] )."'"
						.",". TABLE_BILL_INV .".b_addr_state        = '".processUserData($data['b_addr_state']) ."'"
						.",". TABLE_BILL_INV .".b_addr_country      = '".processUserData($data['b_addr_country']) ."'"
						.",". TABLE_BILL_INV .".b_addr_zip          = '".processUserData($data['b_addr_zip']) ."'"
						.",". TABLE_BILL_INV .".is_renewable        = '". $data['is_renewable'] ."'"
						.",". TABLE_BILL_INV .".ip                  = '".$_SERVER['REMOTE_ADDR']."'"
						.",". TABLE_BILL_INV .".status              = '".$data['status'] ."'" ;
						
                $data['amount'] = number_format($data['amount'],2);
                $data['amount_inr'] = number_format($data['amount_inr'],2);
                $data['balance'] = number_format($data['balance'],2);
                $data['amount_words'] = processSqlData($data['amount_words']);
                $data['do_i_chk'] = $data['do_i'];			 
			    $data['do_rs_symbol'] = strtotime('2010-07-20');
                $data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
				if($data['do_iso'] !='0000-00-00'){
					$data['do_iso'] = strtotime($data['do_iso']); //date of ISO CERTIFIED
				}else{
					$data['do_iso'] = '';
				}
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
					
                    $variables['hid'] = $db->last_inserted_id(); 
					
                    //Update in order, proforma invoice_created =1                       
                    $query1 = "UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".is_invoiceprofm_create='1' 
					WHERE ".TABLE_BILL_ORDERS.".number = '".$data['or_no']."'";
                    $db->query($query1);     
					
                    $messages->setOkMessage("New Proforma Invoice has been created."); 
					
                    //After insert, update invoice counter in               
                    //updateCounterOf($db,'INV',$data['company_id']);
                    //updateOldCounterOf($db,'INV',$data['company_id'],$data['financialYr']);
                    
                    $data['sub_total_amount'] = 0;
					$data['tax1_total_amount']=$data['tax1_sub1_total_amount'] = $data['tax1_sub2_total_amount'] = 0;
					
                    $temp = NULL;
                    $temp_p = NULL; 
                    $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
                    $data['show_discount']=0;
                    $data['show_nos']=0;
                    $data['colsSpanNo1']=5;
                    $data['colsSpanNo2']=6;
                    Order::getParticulars($db, $temp, '*', $condition_query);
                    if(!empty($temp)){                    
                        /*
						$data['wd1']=10;
                        $data['wdinvp']=217;
                        $data['wdpri']=70;
                        $data['wdnos']=70;
                        $data['wdamt']=70;
                        $data['wddisc']=50;
                        $data['wdst']=70;
                        //$data['wdtx']=40; as three columns are removed so manage its width in other cols
                       // $data['wdtxp']=50;
                        //$data['wdtotam']=75;
                        $data['wd2']=9;*/
						
						/* $data['wd1']=10;
                        $data['wdinvp']=245;//217+28
                        $data['wdpri']=98;//70+28
                        $data['wdnos']=98;//70+28
                        $data['wdamt']=97;//70+27
                        $data['wddisc']=77; //50+27
                        $data['wdst']=97;   //70+27                     
                        $data['wd2']=9; */
                        
						
						$data['wd1']=10;
                        $data['wdinvp']=265;//217+28+20
                        $data['wdpri']=88;//70+28=98-10
                        $data['wdnos']=88;//70+28=98-10
                        $data['wdamt']=97;//70+27=97
                        $data['wddisc']=77; //50+27
                        $data['wdst']=97;   //70+27                     
                        $data['wd2']=9;
						
                        foreach ( $temp as $pKey => $parti ) {                        
                            /*
                               if($temp[$pKey]['s_type']=='1'){
                                    $temp[$pKey]['s_type']='Years';
                               }elseif($temp[$pKey]['s_type']=='2'){
                                    $temp[$pKey]['s_type']='Quantity';
                               }
                            */     
                            if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
                                $data['show_discount'] = 1; 
                            }
                            if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
                                $data['show_nos']=1;     
                            }                            
                            $temp_p[]=array(
									'p_id'=>$temp[$pKey]['id'],
									'particulars'=>$temp[$pKey]['particulars'],
									'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
									's_type' =>$temp[$pKey]['s_type'] ,                                   
									's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
									's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
									's_id' =>$temp[$pKey]['s_id'] ,                                   
									'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
									'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
									'tax1_name' =>$temp[$pKey]['tax1_name'] ,    
									'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                                     
									'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
									'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,     
									'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2),									
									'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,    
									'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                                     
									'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
									'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,     
									'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,									
									'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,    
									'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                                     
									'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
									'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,     
									'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,									
									'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
									'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
									'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
									'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
									'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                                );
								
							if(($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00')
								|| ($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00')
							){
							
								$data['sub_total_amount'] = $data['sub_total_amount'] + $temp[$pKey]['stot_amount'] ; 
								
								$data['tax1_id'] =   $temp[$pKey]['tax1_id'] ; 
								$data['tax1_number'] =   $temp[$pKey]['tax1_number'] ; 
								$data['tax1_name'] =   $temp[$pKey]['tax1_name'] ; 
								$data['tax1_value'] =   $temp[$pKey]['tax1_value'] ; 
								$data['tax1_pvalue'] =   $temp[$pKey]['tax1_pvalue'] ; 
								$data['tax1_total_amount'] =  $data['tax1_total_amount'] + $temp[$pKey]['tax1_amount'] ; 
								
								
								$data['tax1_sub1_id'] =   $temp[$pKey]['tax1_sub1_id'] ; 
								$data['tax1_sub1_number'] =   $temp[$pKey]['tax1_sub1_number'] ; 
								$data['tax1_sub1_name'] =   $temp[$pKey]['tax1_sub1_name'] ; 
								$data['tax1_sub1_value'] =   $temp[$pKey]['tax1_sub1_value'] ; 
								$data['tax1_sub1_pvalue'] =   $temp[$pKey]['tax1_sub1_pvalue'] ; 
								$data['tax1_sub1_total_amount'] =  $data['tax1_sub1_total_amount'] + $temp[$pKey]['tax1_sub1_amount'] ; 
								
								$data['tax1_sub2_id'] =   $temp[$pKey]['tax1_sub2_id'] ; 
								$data['tax1_sub2_number'] =   $temp[$pKey]['tax1_sub2_number'] ; 
								$data['tax1_sub2_name'] =   $temp[$pKey]['tax1_sub2_name'] ; 
								$data['tax1_sub2_value'] =   $temp[$pKey]['tax1_sub2_value'] ; 
								$data['tax1_sub2_pvalue'] =   $temp[$pKey]['tax1_sub2_pvalue'] ; 
								$data['tax1_sub2_total_amount'] =  $data['tax1_sub2_total_amount'] + $temp[$pKey]['tax1_sub2_amount'] ; 
							}
							
                        }
                        if($data['show_discount'] ==0){ 
                            // disc : 50 & subtot : 70
                           /* $data['wdinvp']= $data['wdinvp']+20 + 40;
                            $data['wdpri'] = $data['wdpri'] + 5 + 6;
                            $data['wdamt'] = $data['wdamt'] + 5+ 6;                            
                            $data['wdtx']  = $data['wdtx'] + 5+ 6;
                            $data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            $data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							*/
							 // disc : 77 & subtot : 97
							$data['wdinvp']= $data['wdinvp']+50 + 70;
                            $data['wdpri'] = $data['wdpri'] +10 + 10;
                            $data['wdamt'] = $data['wdamt'] + 17+ 17;                            
                            //$data['wdtx']  = $data['wdtx'] + 5+ 6;
                            //$data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            //$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
                            
                        }
                        if($data['show_nos'] ==0){
                            // disc : 70
                           /* $data['wdinvp']= $data['wdinvp']+30;
                            $data['wdpri'] = $data['wdpri'] + 5;
                            $data['wdamt'] = $data['wdamt'] + 5;
                            $data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;
							*/
						    // disc :98
							$data['wdinvp']= $data['wdinvp']+80;
                            $data['wdpri'] = $data['wdpri'] + 8;
                            $data['wdamt'] = $data['wdamt'] + 10;
                            /*
							$data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;*/
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 1; 
                        }                        
                    }
					$sub_tax_str='';
                    if(!empty($data['tax1_name'])){
					
						//get the tax's declaration bof
						$condition1=" WHERE id= ".$data['tax1_id']." LIMIT 0,1" ;
						$required_fields='declaration';
						$parentTaxDetails=array();
						$data['tax1_declaration']='';
						ServiceTax::getDetails( $db, $parentTaxDetails, $required_fields, $condition1);
						if(!empty($parentTaxDetails)){
							$arr = $parentTaxDetails[0];
							$data['tax1_declaration'] = $arr['declaration'];
						}
						
						//get the tax's declaration eof
					
					
						$sqlu  = " UPDATE ".TABLE_BILL_INV_PROFORMA
                            ." SET "
							."". TABLE_BILL_INV_PROFORMA .".sub_total_amount = '".$data['sub_total_amount'] ."'"                            .",". TABLE_BILL_INV_PROFORMA .".tax1_id = '". $data['tax1_id'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_number = '". $data['tax1_number'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_name = '". $data['tax1_name'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_declaration = '". $data['tax1_declaration'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_value = '". $data['tax1_value'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_pvalue = '". $data['tax1_pvalue'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_total_amount = '". $data['tax1_total_amount'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub1_id = '". $data['tax1_sub1_id'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub1_number = '". $data['tax1_sub1_number'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub1_name = '". $data['tax1_sub1_name'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub1_value = '". $data['tax1_sub1_value'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub1_pvalue = '". $data['tax1_sub1_pvalue'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub1_total_amount = '".$data['tax1_sub1_total_amount'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub2_id = '". $data['tax1_sub2_id'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub2_number = '". $data['tax1_sub2_number'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub2_name = '". $data['tax1_sub2_name'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub2_value = '". $data['tax1_sub2_value'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub2_pvalue = '". $data['tax1_sub2_pvalue'] ."'"
							.",". TABLE_BILL_INV_PROFORMA .".tax1_sub2_total_amount = '". $data['tax1_sub2_total_amount'] ."'"
							." WHERE ".TABLE_BILL_INV_PROFORMA.".id ='".$variables['hid'] ."'";	
							
							$sub_tax_str = ",".TABLE_BILL_INV .".sub_total_amount = '".$data['sub_total_amount'] ."'"
							.",". TABLE_BILL_INV .".tax1_id = '". $data['tax1_id'] ."'"
							.",". TABLE_BILL_INV .".tax1_number = '". $data['tax1_number'] ."'"
							.",". TABLE_BILL_INV .".tax1_name = '". $data['tax1_name'] ."'"
							.",". TABLE_BILL_INV .".tax1_declaration = '". $data['tax1_declaration'] ."'"
							.",". TABLE_BILL_INV .".tax1_value = '". $data['tax1_value'] ."'"
							.",". TABLE_BILL_INV .".tax1_pvalue = '". $data['tax1_pvalue'] ."'"
							.",". TABLE_BILL_INV .".tax1_total_amount = '". $data['tax1_total_amount'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub1_id = '". $data['tax1_sub1_id'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub1_number = '". $data['tax1_sub1_number'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub1_name = '". $data['tax1_sub1_name'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub1_value = '". $data['tax1_sub1_value'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub1_pvalue = '". $data['tax1_sub1_pvalue'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub1_total_amount = '". $data['tax1_sub1_total_amount'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub2_id = '". $data['tax1_sub2_id'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub2_number = '". $data['tax1_sub2_number'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub2_name = '". $data['tax1_sub2_name'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub2_value = '". $data['tax1_sub2_value'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub2_pvalue = '". $data['tax1_sub2_pvalue'] ."'"
							.",". TABLE_BILL_INV .".tax1_sub2_total_amount = '". $data['tax1_sub2_total_amount'] ."'"		;
							
							
						$db->query($sqlu);
						
						$data['sub_total_amount']=number_format($data['sub_total_amount'],2);
						$data['tax1_total_amount']=number_format($data['tax1_total_amount'],2);
						$data['tax1_sub1_total_amount']=number_format($data['tax1_sub1_total_amount'],2);
						$data['tax1_sub2_total_amount']=number_format($data['tax1_sub2_total_amount'],2);
					}
					
                    $data['particulars'] = $temp_p ;                    
                    // Update the Order. for time being
                                    
                    $or_id = NULL;
                    //Get details of PAN,ST,VAT,CST BOF
					$table = TABLE_CLIENTS;
					$fields1 =  TABLE_CLIENTS .'.cst_no, '.TABLE_CLIENTS .'.vat_no, 
					'.TABLE_CLIENTS .'. service_tax_no,'.TABLE_CLIENTS .'. pan_no' ;
					$condition1 = " WHERE ".TABLE_CLIENTS .".user_id ='".$data['client']['user_id']."' " ;
					$clientArr1 = getRecord($table,$fields1,$condition1);
					if(!empty($clientArr1)){
						$data['client_cst_no'] = $clientArr1['cst_no'];
						$data['client_vat_no'] = $clientArr1['vat_no'];
						$data['client_st_no'] = $clientArr1['service_tax_no'];
						$data['client_pan_no'] = $clientArr1['pan_no'];
					}
					//Get details of PAN,ST,VAT,CST EOF
                    // Create the Invoice PDF, HTML in file.                    
					$extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';     
					// needs tobe removed as this comment 
                    if ( !($attch1 = InvoiceProforma::createFile($data, 'HTMLPRINT', $extra)) ) {
                        $messages->setErrorMessage("The Invoice print html  file was not created.");
                    }                    
                    if ( !($attch = InvoiceProforma::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Invoice html file was not created.");
                    }
					
					InvoiceProforma::createPdfFile($data);
					//InvoiceProforma::createPdfFileSJM($data);
                    $file_name = DIR_FS_INVPROFM_PDF_FILES ."/". $data["number"] .".pdf";
					
					// BO: Set up Reminder for invoice expiry, if applicable.
                    if ( !empty($data["do_e"]) && $data["do_e"] != "0000-00-00 00:00:00"  && $data['special'] ==0) {
                        $dayArr = InvoiceProforma::expirySetDays();
                        foreach($dayArr as $key =>$val){
                            $temp = explode(' ',$data["do_e"]);
                            $temp1 =explode('-',$temp[0]);
                            $dd=$temp1[2] - $val ;
                            $mm=$temp1[1];
                            $yy=$temp1[0];
                            $sendDt = mktime(0,0,0,$mm,$dd,$yy);                            
                            $send_reminder_dt=date('Y-m-d',$sendDt);
                             $query_rm1	= " INSERT INTO ".TABLE_INV_REMINDER
                             ." SET ". TABLE_INV_REMINDER .".client_id = '".$data['client']['user_id']."'"
                                .",". TABLE_INV_REMINDER .".pinv_id = '".         $variables['hid'] ."'"
                                .",". TABLE_INV_REMINDER .".pinv_no = '".         $data["number"] ."'"
                                .",". TABLE_INV_REMINDER .".do_expiry = '".       $data["do_e"] ."'"
                                .",". TABLE_INV_REMINDER .".date = '".date('Y-m-d h:i:s') ."'"
                                .",". TABLE_INV_REMINDER .".days_before_expiry = '". $val ."'"
                                .",". TABLE_INV_REMINDER .".send_reminder_dt = '".$send_reminder_dt ."'"  
								.",". TABLE_INV_REMINDER .".created_by = '".     $my['user_id'] ."'"
                                .",". TABLE_INV_REMINDER .".ip                  = '".$_SERVER['REMOTE_ADDR']."'" ;
                            $db->query($query_rm1)   ;
                           
                            $query_rm2	= " INSERT INTO ".TABLE_INV_REMINDER_CRON
                             ." SET ". TABLE_INV_REMINDER_CRON .".client_id = '".$data['client']['user_id']."'"
                                .",". TABLE_INV_REMINDER_CRON .".pinv_id = '".         $variables['hid'] ."'"
                                .",". TABLE_INV_REMINDER_CRON .".pinv_no = '".         $data["number"] ."'"
                                .",". TABLE_INV_REMINDER_CRON .".do_expiry = '".       $data["do_e"] ."'"
                                .",". TABLE_INV_REMINDER_CRON .".date = '".date('Y-m-d h:i:s') ."'"
                                .",". TABLE_INV_REMINDER_CRON .".days_before_expiry = '". $val ."'"
                                .",". TABLE_INV_REMINDER_CRON .".send_reminder_dt = '".$send_reminder_dt ."'"                                .",". TABLE_INV_REMINDER_CRON .".created_by = '".     $my['user_id'] ."'"
                                  .",". TABLE_INV_REMINDER_CRON .".ip = '".$_SERVER['REMOTE_ADDR']."'" ;
                            $db->query($query_rm2)   ;
                        }                                
                    }       
					 
                    //Code for Reminder expiry EOF
                    // Send the notification mails to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    
					//CREATE TAX INVOICE BOF
                    if( isset($_POST['btnInvReturn']) || isset($_POST['btnInvFlwReturn']) ){ 
 						$data['invoice_title']='INVOICE';
						if(!empty($data['tax1_name'])){
							$data['invoice_title']= 'TAX INVOICE';
						}
						
						$data['inv_profm_id'] = $variables['hid'];
						$data['inv_profm_no'] = $data["number"];
						$detailInvNo = getInvoiceNumber($data['do_i'],$data['company_id']); 				
						if(!empty($detailInvNo)){
							$data['number'] = $tinv_no = $detailInvNo['number'];
							$data['inv_counter'] = $detailInvNo['inv_counter'];
						} 
						
						$query1	= " INSERT INTO ".TABLE_BILL_INV
						." SET ". TABLE_BILL_INV .".number = '".    $data['number'] ."'"
						.",". TABLE_BILL_INV .".inv_counter = '".   $data['inv_counter'] ."'"
						.",". TABLE_BILL_INV .".inv_profm_id = '".  $data['inv_profm_id'] ."'"
						.",". TABLE_BILL_INV .".inv_profm_no = '".  $data['inv_profm_no'] ."'"
						.$sub_tax_str
						.$sub_str	 ;
						
						$db->query($query1)   ;					
						$variables['hid2'] = $db->last_inserted_id(); 
						$flwSql="";
						if(isset($_POST['btnInvFlwReturn'])){
							$flwSql= ",".TABLE_BILL_ORDERS.".followup_status='1'";
						}
						
						//Update in order, invoice_created =1                       
						$query1 = "UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".is_invoice_create='1' ".$flwSql." WHERE 
							".TABLE_BILL_ORDERS.".number = '".$data['or_no']."'";
						$db->query($query1);
						
						//Update in profm, invoice_created =1                       
						$query1 = "UPDATE ".TABLE_BILL_INV_PROFORMA." SET ".TABLE_BILL_INV_PROFORMA.".status='".InvoiceProforma::COMPLETED."' WHERE 
							".TABLE_BILL_INV_PROFORMA.".id = '". $data['inv_profm_id']."'";
						$db->query($query1);
						
						
						// Create the Invoice PDF, HTML in file.                    
						$extra = array( 's'         => &$s,
										'messages'  => &$messages
									  );
						$attch = '';     
						// needs tobe removed as this comment 
						if ( !($attch2 = Invoice::createFile($data, 'HTMLPRINT', $extra)) ) {
							$messages->setErrorMessage("The Invoice print html  file was not created.");
						}                    
						if ( !($attch3 = Invoice::createFile($data, 'HTML', $extra)) ) {
							$messages->setErrorMessage("The Invoice html file was not created.");
						}
						Invoice::createPdfFile($data);
						$file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";
						
						
						// BO: Set up Reminder for invoice expiry, if applicable.						 
						/*
						if ( !empty($data["do_e"]) && $data["do_e"] != "0000-00-00 00:00:00" ) {
							$dayArr = Invoice::expirySetDays();
							foreach($dayArr as $key =>$val){
								$temp = explode(' ',$data["do_e"]);
								$temp1 =explode('-',$temp[0]);
								$dd=$temp1[2] - $val ;
								$mm=$temp1[1];
								$yy=$temp1[0];
								$sendDt = mktime(0,0,0,$mm,$dd,$yy);                            
								$send_reminder_dt=date('Y-m-d',$sendDt);
								$query_rm1	= " INSERT INTO ".TABLE_INV_REMINDER
								." SET ". TABLE_INV_REMINDER .".client_id = '".$data['client']['user_id']."'"
									.",". TABLE_INV_REMINDER .".inv_id = '".         $variables['hid2'] ."'"
									.",". TABLE_INV_REMINDER .".inv_no = '".         $data["number"] ."'"
									.",". TABLE_INV_REMINDER .".do_expiry = '".         $data["do_e"] ."'"
									.",". TABLE_INV_REMINDER .".date = '".date('Y-m-d h:i:s') ."'"
									.",". TABLE_INV_REMINDER .".days_before_expiry = '". $val ."'"
									.",". TABLE_INV_REMINDER .".send_reminder_dt = '".$send_reminder_dt ."'"  
									.",". TABLE_INV_REMINDER .".created_by = '".     $my['user_id'] ."'"
									.",". TABLE_INV_REMINDER .".ip                  = '".$_SERVER['REMOTE_ADDR']."'" ;
								$db->query($query_rm1)   ;
							   
							   $query_rm2	= " INSERT INTO ".TABLE_INV_REMINDER_CRON
								." SET ". TABLE_INV_REMINDER_CRON .".client_id = '".$data['client']['user_id']."'"
									.",". TABLE_INV_REMINDER_CRON .".inv_id = '".         $variables['hid2'] ."'"
									.",". TABLE_INV_REMINDER_CRON .".inv_no = '".         $data["number"] ."'"
									.",". TABLE_INV_REMINDER_CRON .".do_expiry = '".         $data["do_e"] ."'"
									.",". TABLE_INV_REMINDER_CRON .".date = '".date('Y-m-d h:i:s') ."'"
									.",". TABLE_INV_REMINDER_CRON .".days_before_expiry = '". $val ."'"
									.",". TABLE_INV_REMINDER_CRON .".send_reminder_dt = '".$send_reminder_dt ."'"                                
									.",". TABLE_INV_REMINDER_CRON .".created_by = '".     $my['user_id'] ."'"
									  .",". TABLE_INV_REMINDER_CRON .".ip                  = '".$_SERVER['REMOTE_ADDR']."'" ;
								$db->query($query_rm2)   ;
							}                                
						} */                  
						//Code for Reminder expiry EOF
				    }
					//CREATE TAX INVOICE EOF
					
                    //Get details of Client EOF                  
                    
                   
                    // Organize the data to be sent in email.
                    $data['link']   = DIR_WS_MP .'/bill-invoice-profm.php?perform=view&inv_id='. $variables['hid'];
                    // Read the Client Manager information.
                  
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');
                    
                    // Send Email to the Client.
                   if( isset($mail_client) &&  $mail_client==1 ){
                        $email = NULL;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'PROFM_CREATE_TO_CLIENT', $data, $email) ) {
                            $to     = '';
                            //echo " subject ".$email["subject"] ;
                            //echo "<br/>";
                            //echo " body ".$email["body"];
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                        }
                    }
                    
                    //Send mail after create invoice to admin bof
                     $data['link']   = DIR_WS_NC .'/bill-invoice-profm.php?perform=view&inv_id='. $variables['hid'];    
                     $email = NULL;
                     $cc_to_sender= $cc = $bcc = Null;
                     if ( getParsedEmail($db, $s, 'PROFM_INVOICE_CREATE_TO_ADMIN', $data, $email) ) {
                        $to     = '';
                        //echo " subject ".$email["subject"] ;
                        //echo "<br/>";
                        //echo " body ".$email["body"];
                        $to[]   = array('name' => $bill_inv_name , 'email' => $bill_inv_email);                           
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                     }
                    //Send mail after create invoice to admin eof
                    
                    /*
                    $data['link']   = DIR_WS_NC .'/bill-invoice.php?perform=view&inv_id='. $variables['hid'];                   
                    // Send mail to the Concerned Executives with same access level and higher access levels
                    if(isset($data['mail_exec_ac'])){
                       
                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_INV_NEW_MANAGERS', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                            }
                        }
                    }
                    // Send Email to the Client Manager.
                    if(isset($data['mail_client_mgr'])){ 
                       
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT_MANAGER', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $data['manager']['f_name'] .' '. $data['manager']['l_name'], 'email' => $data['manager']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                        }
                    }
                    */
                    //to flush the data.
                    
                    $_ALL_POST  = NULL;
                    //$_ALL_POST['number'] = getCounterNumber($db,'INV',$data['company_id']); 
                    $inv_no =$data['number'];
                    $data       = NULL;
                }
            }
            
        }
        else {
            // Set up the default values to be displayed.
            // $_ALL_POST['number'] = "PT". date("Y") ."-INV-". date("mdhi-s") ;
            //As invoice-number formate changed on the basis of ( fanancial yr + sr no )           
        }
    
             
		
		if( ( isset($_POST['btnInvReturn']) || isset($_POST['btnInvFlwReturn']) ) && $messages->getErrorMessageCount() <= 0 ){
        
           header("Location:".DIR_WS_NC."/bill-invoice.php?added=1&hid=".$variables['hid2']."&inv_no=".$tinv_no);           
        }
		if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
        
           header("Location:".DIR_WS_NC."/bill-invoice-profm.php?added=1&hid=".$variables['hid']."&inv_no=".$inv_no);           
        }
        if(isset($_POST['btnReturnRcp']) && $messages->getErrorMessageCount() <= 0 ){
        
           header("Location:".DIR_WS_NC."/bill-receipt.php?perform=add&inv_id=".$variables['hid']."&inv_no=".$inv_no);           
        }
        
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/bill-invoice-profm.php");
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
             header("Location:".DIR_WS_NC."/bill-invoice-profm.php?added=1&hid=".$variables['hid']."&inv_no=".$inv_no);
            // include ( DIR_FS_NC .'/bill-invoice-list.php');
        }
        else {
        
            if ( !empty($or_id) ) {
                // Read the Order details.
                include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
                $order  = NULL;              
                
                $fields = TABLE_BILL_ORDERS.'.id,'.TABLE_BILL_ORDERS .'.do_o AS do_o,
				'.TABLE_BILL_ORDERS .'.number AS or_no,'. TABLE_BILL_ORDERS .'.access_level, 
				'. TABLE_BILL_ORDERS .'.status, '.TABLE_BILL_ORDERS.'.details';
                $fields .= ', '.TABLE_BILL_ORDERS.'.order_title, '. TABLE_BILL_ORDERS .'.order_closed_by';
                $fields .= ', '.TABLE_BILL_ORDERS.'.do_e, '.TABLE_BILL_ORDERS .'.do_fe';
                $fields .= ', '.TABLE_BILL_ORDERS.'.currency_id, '. TABLE_BILL_ORDERS .'.company_id ,
				'.TABLE_BILL_ORDERS .'.amount, '. TABLE_BILL_ORDERS .'.round_off_op,'.TABLE_BILL_ORDERS .'.round_off';                $fields .= ', '.TABLE_CLIENTS.'.user_id, '. TABLE_CLIENTS .'.number, '.TABLE_CLIENTS.'.f_name, 
				'.TABLE_CLIENTS.'.l_name';
				$fields .= ', '.TABLE_CLIENTS.'.billing_name' ;
				$fields .= ', '.TABLE_BILL_ORDERS.'.delivery_at' ;
				$fields .= ', '.TABLE_BILL_ORDERS.'.tax_ids' ;
				$fields .= ', '.TABLE_BILL_ORDERS.'.service_id' ;
				$fields .= ', '.TABLE_BILL_ORDERS.'.special' ;
				$fields .= ', '.TABLE_BILL_ORDERS.'.show_period' ;
				$fields .= ', '.TABLE_BILL_ORDERS.'.is_invoiceprofm_create' ;
                $fields .= ', '.TABLE_BILL_ORDERS.'.client, '. TABLE_USER .'.number AS u_number, 
				'.TABLE_USER.'.f_name AS u_f_name, '.TABLE_USER.'.l_name AS u_l_name ';
                
                if ( Order::getDetails($db, $order, $fields, " WHERE ".TABLE_BILL_ORDERS.".id = '". $or_id ."' ") > 0 ) {
                    $order = $order[0];
					
                    if ( !$order['is_invoiceprofm_create'] ){
					
						if ( ($order['status'] == Order::ACTIVE) || ($order['status'] == Order::PROCESSED) || 
							($order['status'] == Order::COMPLETED) ) {
							if ( $access_level > $order['access_level'] ) {
								$_ALL_POST['or_no']             = $order['or_no'];
								$_ALL_POST['show_period']             = $order['show_period'];
								$_ALL_POST['special']           = $order['special'];
								$_ALL_POST['delivery_at']       = $order['delivery_at'];
							   // $_ALL_POST['or_particulars']  = $order['particulars'];
								$_ALL_POST['or_details']        = $order['details'];
								$_ALL_POST['or_user_id']        = $order['user_id'];
								$_ALL_POST['or_number']         = $order['number'];
								$_ALL_POST['or_f_name']         = $order['f_name'];
								$_ALL_POST['or_l_name']         = $order['l_name'];
								$_ALL_POST['billing_name']         = $order['billing_name'];
								$_ALL_POST['or_access_level']   = $order['access_level'];
							 
								$_ALL_POST['do_o']              = $order['do_o'];
								$_ALL_POST['u_f_name']          = $order['u_f_name'];
								$_ALL_POST['u_l_name']          = $order['u_l_name'];
								$_ALL_POST['u_number']          = $order['u_number'];
								$_ALL_POST['order_title']       = $order['order_title'];
								$_ALL_POST['order_closed_by']   = $order['order_closed_by'];
								$_ALL_POST['do_e']   = $order['do_e'];
								$_ALL_POST['do_fe']   = $order['do_fe'];
							  	$_ALL_POST['tax_ids']   = $order['tax_ids'];
								$_ALL_POST['service_id']   = $order['service_id'];
								
								if($_ALL_POST['do_e'] !='0000-00-00 00:00:00'){
									$_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
									$temp               = explode('-', $_ALL_POST['do_e'][0]);
									$_ALL_POST['do_e']  = NULL;
									$_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
								}else{                            
									$_ALL_POST['do_e']  ='';
								}
								
								if($_ALL_POST['do_fe'] !='0000-00-00 00:00:00'){
									$_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
									$temp               = explode('-', $_ALL_POST['do_fe'][0]);
									$_ALL_POST['do_fe']  = NULL;
									$_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
								}else{                            
									$_ALL_POST['do_fe']  ='';
								}
										
								/* $_ALL_POST['show_period']  =0;	
								if(!empty($_ALL_POST['do_e']) && !empty($_ALL_POST['do_fe'])){
									$_ALL_POST['show_period']  =1;
								} */
								
								if(!empty($order['order_closed_by'])){
									$clientfields='';
									$condition = TABLE_USER." WHERE  user_id='".$order['order_closed_by']."'";
									$clientfields .= ' '.TABLE_USER.'.user_id, '. TABLE_USER .'.number, '.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
									User::getList($db, $closed_by, $clientfields, $condition) ;
									$_ALL_POST['order_closed_by_name']   = $closed_by['0'];
								}
														
								
								$_ALL_POST['amount']= $order['amount'];
								$_ALL_POST['round_off_op']= $order['round_off_op'];
								$_ALL_POST['round_off']= $order['round_off'];
								$_ALL_POST['gramount']= number_format($order['amount'],2);
								$_ALL_POST['amount_inr']= $order['amount'] * $_ALL_POST['exchange_rate'];
								$_ALL_POST['currency_id']= $order['currency_id'];
								//$_ALL_POST['company_id']= $order['company_id'];
								
							   
								/*get particulars details bof*/
								$temp = NULL;
								$temp_p = NULL;
								$condition_query = " WHERE ord_no = '". $order['or_no'] ."' ";
								Order::getParticulars($db, $temp, '*', $condition_query);
								if(!empty($temp)){
									foreach ( $temp as $pKey => $parti ) {
									 /*if($temp[$pKey]['s_type']=='1'){
										$temp[$pKey]['s_type']='Yrs';
									  }elseif($temp[$pKey]['s_type']=='2'){
										$temp[$pKey]['s_type']='Qty';
									  }*/
										$temp_p[]=array(
														'p_id'=>$temp[$pKey]['id'],
														'particulars'=>$temp[$pKey]['particulars'],
														'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,      
														's_type' =>$temp[$pKey]['s_type'] ,                                   
														's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
														's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                                              
														's_id' =>$temp[$pKey]['s_id'] ,                                   
														'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
														'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
														'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
														'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
														'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
														'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
														'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) , 
														'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,                                   
														'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                   
														'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
														'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,                                   
														'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) , 
														'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,                                   
														'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                   
														'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
														'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,                                   
														'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,                                                        'd_amount' =>number_format($temp[$pKey]['d_amount'],2),                                   
														'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
														'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
														'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
														'pp_amount' =>number_format($temp[$pKey]['pp_amount'] ,2)                                   
														);
									}
								}
								$_ALL_POST['particulars']=$temp_p ;
								
								/*get particulars details eof*/
								$order = NULL;
								// Read the Client Addresses.
								include_once ( DIR_FS_CLASS .'/Region.class.php');
								$region         = new Region();
								$region->setAddressOf(TABLE_CLIENTS, $_ALL_POST['or_user_id']);
								$_ALL_POST['address_list']  = $region->get();
							  
								if ( (!isset($_POST['btnCreate']) && !isset($_POST['btnReturn'])) ) {
									//$_ALL_POST['billing_name']  = $_ALL_POST['or_f_name'] .' '. $_ALL_POST['or_l_name'];
									$_ALL_POST['billing_name']  = $_ALL_POST['billing_name'] ;
									//$_ALL_POST['particulars']   = array('');
									//$_ALL_POST['p_amount']      = array(0);
								}

								$al_list = getAccessLevel($db, $access_level);
								$index = array();
								if ( isset($_ALL_POST['or_access_level']) ) {
									array_key_search('access_level', $_ALL_POST['or_access_level'], $al_list, $index);
									$_ALL_POST['or_access_level'] = $al_list[$index[0]]['title'];
								}
								
								// As invoice-number formate changed on the basis of ( fanancial yr +  company_id + sr no ) 
								//$_ALL_POST['number'] = getCounterNumber($db,'INV',$_ALL_POST['company_id']); comment as require dropdown to select financial yr
								
				
								$hidden[] = array('name'=> 'or_id' ,'value' => $or_id);
								$hidden[] = array('name'=> 'special' ,'value' => $_ALL_POST['special']);
								$hidden[] = array('name'=> 'or_no' ,'value' => $_ALL_POST['or_no']);
								$hidden[] = array('name'=> 'tax_ids' ,'value' => $_ALL_POST['tax_ids']);
								$hidden[] = array('name'=> 'service_id' ,'value' => $_ALL_POST['service_id']);
								$hidden[] = array('name'=> 'delivery_at' ,'value' => $_ALL_POST['delivery_at']);
								$hidden[] = array('name'=> 'client' ,'value' => $_ALL_POST['or_user_id']);
								$hidden[] = array('name'=> 'round_off' ,'value' => $_ALL_POST['round_off']);
								$hidden[] = array('name'=> 'round_off_op' ,'value' => $_ALL_POST['round_off_op']);
								//$hidden[] = array('name'=> 'company_id' ,'value' => $_ALL_POST['company_id']);
								
								$hidden[] = array('name'=> 'perform' ,'value' => 'add');
								$hidden[] = array('name'=> 'act' , 'value' => 'save');
					
								$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
								$page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
								$page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
								$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
								$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
								$page["var"][] = array('variable' => 'lst_fyr', 'value' => 'lst_fyr');
								//$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
								//$page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
								$page["var"][] = array('variable' => 'currency', 'value' => 'currency');
								
								$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-profm-add.html');
							}
							else {
								$messages->setErrorMessage("You donot have the Right to create invoice for this Order.");
								$_ALL_POST = '';
							}
						}
						else {
							$messages->setErrorMessage("The Order is not yet approved.");
							$_ALL_POST = '';
						}
					}else{
						$messages->setErrorMessage("The Proforma Invoice is created for this Order.");
					}
                }
                else {
                    $messages->setErrorMessage("The Selected Order was not found.");
                }
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>