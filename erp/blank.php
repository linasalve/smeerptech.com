<html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<link rel="shortcut icon" href="{$variables.domain}/media/images/favicon.ico" type="image/x-icon" />
	<title><?php echo $variables['title']; ?>: Nerve Center - World No. 1 SME CRM</title>
 
    <script language="javascript" type="text/javascript" src="<?php echo $variables['scripts']."/ajax.js";?>"></script>
    <script language="javascript" type="text/javascript" src="<?php echo $variables['scripts']."/common-functions.js";?>"></script>
    <script language="javascript">
    
        var current_menu;
        var time_handle;
       // var last_menu = {/literal}{$menu_count}{literal};
        
        function revert() {
            for ( i=0; i<last_menu; i++ ) {
                element = document.getElementById("sublink_"+i);
                if ( i == current_menu) {
                    element.className = "visible";
                }
                else {
                    element.className = "hidden";
                }
            }
        }
        
        function showSubLink(id) { 
            for ( i=0; i<last_menu; i++ ) {
                element = document.getElementById("sublink_"+i);
                element.className = "hidden";
            }
            element = document.getElementById("sublink_"+id);
            element.className = "visible";
        }
    </script>
    <style media="all" type="text/css">
        @import url(<?php echo $variables['css']."/nc/en_IN.css";?>); 
        @import url(<?php echo $variables['css']."/window/window-red.css";?>);
    </style>
 
</head>
<body>
 <?php 
					 
 include (DIR_FS_NC .'/'.$main_file_name);
?>
</body>
</html>