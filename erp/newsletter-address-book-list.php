<?php

    //if ( $perm->has('nc_ab_list') ) {
		
	    
         $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
                                TABLE_ADDRESS_BOOK   =>  array('ID'            => 'id',
                                                          'Name'               => 'name',
                                                          'Company'             => 'company',
                                                          'Url'                 => 'url',
                                                          'Mobile'              => 'mobile',
                                                          'Email'               => 'email',                                                           
                                                          'Contact title'       => 'contact_title'                                                               
                                                        ),
                                );
        
        $sOrderByArray  = array(
                                TABLE_ADDRESS_BOOK => array( 'ID'               => 'id',
                                                              'Date'              => 'date',
                                                              'Name'              => 'name',
                                                              'Contact title'     => 'contact_title'
                                                             ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'date';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_ADDRESS_BOOK;
        }
        
        
        // If the entry is created by the my.
         /*$access_level   = $my['access_level'];
        if ( $perm->has('nc_ab_list_al') ) {
            $access_level += 1;
        }*/

        if ( !isset($condition_query1) || $condition_query1 == '' ) {
            $condition_query1 = ' WHERE ';
        }
        else {
            $condition_query1 .= ' AND ';
        }
    
        /*
        $condition_query .= "  ( ";
        // If my has created this entry.
        $condition_query .= " ( ". TABLE_ADDRESS_BOOK .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_ADDRESS_BOOK .".access_level < $access_level ) ";
       
        
         // Check if the User has the Right to view Orders created by other Users.
        if ( $perm->has('nc_ab_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_ab_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_ADDRESS_BOOK. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_ADDRESS_BOOK .".access_level < $access_level_o ) ";
        }
        $condition_query .= ")";
        */
         $condition_query1 .= " email !=''";
        $condition_query1 .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched']=1;
        
        //echo $condition_query;
        // To count total records.
        $list	= 	NULL;
        $total	=	Addressbook::getDetails( $db, $list, '', $condition_query1);
    
         $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields = TABLE_ADDRESS_BOOK .'.*'     ;
        Addressbook::getDetails( $db, $list, $fields, $condition_query1, $next_record, $rpp);
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
       
        if ( $perm->has('nc_ab_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_ab_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_ab_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_ab_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_ab_details') ) {
            $variables['can_view_details'] = true;
        }
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletter-address-book-list.html');
  
  /* }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }*/
?>