<?php
if ( $perm->has('nc_p_pt_edit') ) {
    $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');
	include_once ( DIR_FS_INCLUDES .'/payment-party-bills.inc.php');
        $_ALL_POST =null;        
        $fields_r = TABLE_PAYMENT_TRANSACTION .'.*'  ;            
        $condition_query_r = " WHERE ( ". TABLE_PAYMENT_TRANSACTION .".id = '". $id ."' )";
        Paymenttransaction::getDetails($db, $recData, $fields_r, $condition_query_r);
        if(!empty($recData)){
            $recData =$recData['0'];
        }
        $tr_cr_sql ='';
        $tr_db_sql ='';  
        $check = 0;
        $de_act = -1;
        $variables['reasonsList'] = Paymenttransaction::getReasonsOfDelete();
    
    if($recData['status'] == Paymenttransaction::COMPLETED){
        
        if((isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save'){
            $_ALL_POST		= processUserData($_POST);
			if(empty($_POST['reason'])){
                $check=1;
                        
                $messages->setErrorMessage("Please Select Reason To Delete The Transaction ".$recData['number']);
            }
            if($messages->getErrorMessageCount() <= 0){
				
                if($recData['transaction_type'] ==  Paymenttransaction::PAYMENTIN){
                    
                    $data['debit_pay_amt']      = $recData['credit_pay_amt'] ;
                    $data['debit_pay_bank_id']  = $recData['credit_pay_bank_id'];   
                    $data['do_debit_trans'] =   $recData['do_credit_trans'] ;            
                    $data['transaction_type']   = Paymenttransaction::PAYMENTOUT;
                    $data['reference_transaction_id'] = $recData['id'] ;
                    $data['pay_received_amt'] = $recData['pay_received_amt'] ;
                    $data['pay_received_amt_words'] =   $recData['pay_received_amt_words'] ;
                    $data['company_id'] =   $recData['company_id'] ;            
                    $data['company_name'] =   $recData['company_name'] ;            
                    $data['company_prefix'] =   $recData['company_prefix'] ;            
                    $data['do_transaction'] =   $recData['do_transaction'] ;
                    $data['account_head_id'] =   $recData['account_head_id'] ;
                    $data['party_id'] =   $recData['party_id'] ;
                    $data['client_id'] =   $recData['client_id'] ;
                    $data['vendor_bank_id'] =   $recData['vendor_bank_id'] ;
                    $data['executive_id'] =   $recData['executive_id'] ;
                    $data['mode_id'] =   $recData['mode_id'] ;           
                    $data['pay_cheque_no'] =   $recData['pay_cheque_no'] ;
                    $data['do_pay_received'] =   $recData['do_pay_received'] ;
                    $data['pay_bank_company'] =   $recData['pay_bank_company'] ;
                    $data['pay_branch'] =   $recData['pay_branch'] ;               
                    $data['will_receipt_generate'] =   $recData['will_receipt_generate'] ;
                    $data['is_pdc'] =   $recData['is_pdc'] ;
                    $data['do_pdc_chq'] =   $recData['do_pdc_chq'] ;
                    $data['pdc_chq_details'] =   $recData['pdc_chq_details'] ;
                    $data['pay_received_amt_rcpt'] =   $recData['pay_received_amt_rcpt'] ;
                    $data['pay_received_amt_inr'] =   $recData['pay_received_amt_inr'] ;
                    $data['pay_received_amt_rcpt_inr'] =   $recData['pay_received_amt_rcpt_inr'] ;
                    $data['exchange_rate'] =   $recData['exchange_rate'] ;
                    $data['currency_abbr'] =   $recData['currency_abbr'] ;
                    $data['currency_id'] =   $recData['currency_id'] ;
                    $data['currency_name'] =   $recData['currency_name'] ;
                    $data['currency_symbol'] =   $recData['currency_symbol'] ;
                    $data['currency_country'] =   $recData['currency_country'] ;
                    $data['ref_credit_pay_bank_id'] = $recData['ref_debit_pay_bank_id'] ;
                    $data['ref_debit_pay_bank_id'] = $recData['ref_credit_pay_bank_id'] ;
                    $data['do_voucher'] = $recData['do_voucher'] ;
                    $data['iaccount_head_id'] = $recData['iaccount_head_id'] ;
                    
                    // check amt to withdraw frm credit bank bof 
                    $dbnew=new db_local(); 
					$update_query	= " UPDATE ".TABLE_PAYMENT_BANK
					." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$data['debit_pay_amt']." 
					WHERE id='".$data['debit_pay_bank_id']."'";
					$executed = $dbnew->query($update_query);			  
                  
                    //Get balance amount after transaction BOF
                    $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['debit_pay_bank_id'];
                    $db->query($sql);
                    if($db->nf()>0 )
                    {
                        while ($db->next_record()) {
                           $tr_db_bank_amt = $db->f('balance_amt');
                           $tr_db_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_db_bank_amt = '".$tr_db_bank_amt."'"  ;
                        }
                    }            
					if($executed){
                        $messages->setOkMessage("Your Amount Details has been updated for ".$recData["number"]);
                    }else{
                        $messages->setErrorMessage("Sorry! Amount Details can not be updated ".$recData["number"]);
                    }
                    // check amt to withdraw frm credit bank eof           
                    if( $messages->getErrorMessageCount() <= 0){
                        $query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION                   
						 ." SET ". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::CANCELLED_C ."'"   
						 .",". TABLE_PAYMENT_TRANSACTION .".reason = '".$_ALL_POST['reason']."'"      
						 .",". TABLE_PAYMENT_TRANSACTION .".reason_of_deactivate = 
							'".$_ALL_POST['reason_of_deactivate']."'"      
						 .",". TABLE_PAYMENT_TRANSACTION.".status_deactivate = 
							'".Paymenttransaction::DEACTIVATE_COMPLETED."'"
						 .",". TABLE_PAYMENT_TRANSACTION.".do_status_update = '".date("Y-m-d h:i:s")."'"
						 .",". TABLE_PAYMENT_TRANSACTION .".status_update_by = '".$my['uid']."'"
						 .",". TABLE_PAYMENT_TRANSACTION .".status_update_ip = '".$_SERVER['REMOTE_ADDR']."'" 
						 ." WHERE id = '". $recData['id']."'";
                          
                            $db->query($query);
                            //Insert automatic record which shows Out entry 
                            $data['number']= Paymenttransaction::getNewNumber($db);                            
                            //insert automatic entry bof
                        if(!empty($data['number'])){
                                $autoRemark = "Auto reversal against transaction no. ".$recData['number']."." ;
                                $insertquery = " INSERT INTO ".TABLE_PAYMENT_TRANSACTION
                            ." SET ". TABLE_PAYMENT_TRANSACTION .".number = '".$data['number'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".company_id = '".$data['company_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".company_name = '".$data['company_name'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".company_prefix = '".$data['company_prefix'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".account_head_id = '".$data['account_head_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".party_id = '".$data['party_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".client_id = '".$data['client_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id = '".$data['vendor_bank_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".executive_id = '".$data['executive_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".mode_id = '".$data['mode_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_pdc_chq = '".$data['do_pdc_chq'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pdc_chq_details = '".$data['pdc_chq_details'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_cheque_no = '".$data['pay_cheque_no'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_pay_received = '".$data['do_pay_received'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_bank_company = '".$data['pay_bank_company'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".transaction_type = '".$data['transaction_type'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt = '".$data['pay_received_amt']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt 
								= '".$data['pay_received_amt_rcpt']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_inr 
								= '".$data['pay_received_amt_inr']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt_inr 
								='".$data['pay_received_amt_rcpt_inr']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".exchange_rate = '".$data['exchange_rate']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_abbr 
								= '". processUserData($data['currency_abbr']) ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_id = '".   $data['currency_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_name 
								= '". processUserData($data['currency_name'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_symbol 
								= '".processUserData($data['currency_symbol'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_country 
								='".processUserData($data['currency_country'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_words 
								= '".$data['pay_received_amt_words']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_amt = '".$data['debit_pay_amt']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_debit_trans = '".$data['do_debit_trans']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = '".$data['debit_pay_bank_id']."'"                      .",". TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id='".$data['ref_credit_pay_bank_id']."'"
					  .",". TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id='".$data['ref_debit_pay_bank_id']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".date = '".date("Y-m-d h:i:s")."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".do_transaction = '".$data['do_transaction']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_voucher = '".$data['do_voucher']."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".iaccount_head_id = '".$data['iaccount_head_id']."'" 							  .",". TABLE_PAYMENT_TRANSACTION .".is_auto_entry = '1'" 
							.",". TABLE_PAYMENT_TRANSACTION .".remarks = '".$autoRemark."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".particulars = '".$autoRemark."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".user_id = '".$my['uid']."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".ip = '".$_SERVER['REMOTE_ADDR']."'" 
							.$tr_cr_sql    
							.$tr_db_sql      
							.",". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::AUTOREVERSAL."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".reference_transaction_id = '".$recData['id']."'" ;
                               
 							    $db->query($insertquery);
                               
                                $data['trid'] = $db->last_inserted_id(); 
                                $query2  = " UPDATE ". TABLE_PAYMENT_TRANSACTION                   
                                ." SET ". TABLE_PAYMENT_TRANSACTION .".reference_transaction_id='".$data['trid']."'"   							    ." WHERE id = '". $recData['id']."'";
                          
                                $db->query($query2);
								
								 
                        }
                           
                        $de_act=1;							
							
							
							
							
                    }else{
                        $query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION
						." SET ". TABLE_PAYMENT_TRANSACTION .".status_deactivate = 
							'".Paymenttransaction::DEACTIVATE_PENDING."'"
						.",". TABLE_PAYMENT_TRANSACTION .".reason = '".$_ALL_POST['reason']."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".reason_of_deactivate 
							= '".$_ALL_POST['reason_of_deactivate']."'"  
						.",". TABLE_PAYMENT_TRANSACTION .".do_status_update = '".date("Y-m-d h:i:s")."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_by = '".$my['uid']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_ip = '".$_SERVER['REMOTE_ADDR']."'"                                     
						." WHERE id = '". $recData['id']."'";
                        $db->query($query);  
                           $de_act=0;                
                    }
                    
                }elseif($recData['transaction_type'] ==  Paymenttransaction::PAYMENTOUT){
                
                    
                    $data['credit_pay_amt']     = $recData['debit_pay_amt'] ;         
                    $data['credit_pay_bank_id'] = $recData['debit_pay_bank_id'] ;
                    $data['transaction_type']   = Paymenttransaction::PAYMENTIN ;
                    $data['reference_transaction_id']   =    $recData['id'] ;
                    $data['pay_received_amt'] =$recData['pay_received_amt'] ;
                    $data['pay_received_amt_words'] =   $recData['pay_received_amt_words'] ;
                    $data['company_id'] =$recData['company_id'] ;
                    $data['company_name'] =   $recData['company_name'] ;            
                    $data['company_prefix'] =   $recData['company_prefix'] ;
                    $data['do_transaction']=$recData['do_transaction'] ;            
                    $data['account_head_id'] =   $recData['account_head_id'] ;
                    $data['party_id'] =   $recData['party_id'] ;
                    $data['client_id'] =   $recData['client_id'] ;
                    $data['vendor_bank_id'] =   $recData['vendor_bank_id'] ;
                    $data['executive_id'] =   $recData['executive_id'] ;
                    $data['mode_id'] =   $recData['mode_id'] ;           
                    $data['pay_cheque_no'] =   $recData['pay_cheque_no'] ;
                    $data['do_pay_received'] =   $recData['do_pay_received'] ;
                    $data['pay_bank_company'] =   $recData['pay_bank_company'] ;
                    $data['pay_branch'] =   $recData['pay_branch'] ;               
                    $data['will_receipt_generate'] =   $recData['will_receipt_generate'] ;
                    $data['is_pdc'] =   $recData['is_pdc'] ;
                    $data['do_pdc_chq'] =   $recData['do_pdc_chq'] ;
                    $data['pdc_chq_details'] =   $recData['pdc_chq_details'] ; 
					$data['pay_received_amt_rcpt'] =   $recData['pay_received_amt_rcpt'] ;
                    $data['pay_received_amt_inr'] =   $recData['pay_received_amt_inr'] ;
                    $data['pay_received_amt_rcpt_inr'] =   $recData['pay_received_amt_rcpt_inr'] ;
                    $data['exchange_rate'] =   $recData['exchange_rate'] ;
                    $data['currency_abbr'] =   $recData['currency_abbr'] ;
                    $data['currency_id'] =   $recData['currency_id'] ;
                    $data['currency_name'] =   $recData['currency_name'] ;
                    $data['currency_symbol'] =   $recData['currency_symbol'] ;
                    $data['currency_country'] =   $recData['currency_country'] ;
					$data['ref_credit_pay_bank_id'] = $recData['ref_debit_pay_bank_id'] ;
                    $data['ref_debit_pay_bank_id'] = $recData['ref_credit_pay_bank_id'] ;
                    $data['do_voucher'] = $recData['do_voucher'] ;
                    $data['iaccount_head_id'] = $recData['iaccount_head_id'] ;
                    
                    $dbnew=new db_local();                    	
                    $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                       ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$data['credit_pay_amt']." 
					   WHERE id='".$data['credit_pay_bank_id']."'";
                    $executed = $dbnew->query($update_query);
                    //Get balance amount after transaction BOF
                    $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['credit_pay_bank_id'];
                    $db->query($sql);
                    if($db->nf()>0 ){
                        while ($db->next_record()){
                            $tr_cr_bank_amt = $db->f('balance_amt');
                            $tr_cr_sql= ",". TABLE_PAYMENT_TRANSACTION.".tr_cr_bank_amt = '".$tr_cr_bank_amt."'" ;
                        }
                    }
                    //Get balance amount after transaction EOF
                    
                    if($executed){
                        $messages->setOkMessage("Your Amount Details has been updated for ".$recData["number"]);
                    }else{
                        $messages->setErrorMessage("Sorry! Amount Details can not be updated ".$recData["number"]);
                    }
                    if( $messages->getErrorMessageCount() <= 0){
                       $query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION
						." SET ". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::CANCELLED_C ."'"   
						.",". TABLE_PAYMENT_TRANSACTION .".reason = '".$_ALL_POST['reason']."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".reason_of_deactivate 
							= '".$_ALL_POST['reason_of_deactivate']."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".status_deactivate 
							= '".Paymenttransaction::DEACTIVATE_COMPLETED ."'"         			   
						.",". TABLE_PAYMENT_TRANSACTION .".do_status_update = '".date("Y-m-d h:i:s")."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_by = '".$my['uid']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_ip = '".$_SERVER['REMOTE_ADDR']."'"   
						." WHERE id = '". $recData['id']."'";
                        $db->query($query);
                        //Insert automatic record which shows Out entry 
                            
                        $data['number']= Paymenttransaction::getNewNumber($db);                             
                        //insert automatic entry bof
                        if(!empty($data['number'])){
                            $autoRemark = "Auto reversal against transaction no. ".$recData['number']."." ;
                            $insertquery = " INSERT INTO ".TABLE_PAYMENT_TRANSACTION
                            ." SET ". TABLE_PAYMENT_TRANSACTION .".number = '".$data['number'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".company_id = '".$data['company_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".company_name = '".$data['company_name'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".company_prefix = '".$data['company_prefix'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".account_head_id = '".$data['account_head_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".party_id = '".$data['party_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".client_id = '".$data['client_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id = '".$data['vendor_bank_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".executive_id = '".$data['executive_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".mode_id = '".$data['mode_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_pdc_chq = '".$data['do_pdc_chq'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pdc_chq_details = '".$data['pdc_chq_details'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_cheque_no = '".$data['pay_cheque_no'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_pay_received = '".$data['do_pay_received'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_bank_company = '".$data['pay_bank_company'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".transaction_type = '".$data['transaction_type'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt = '".$data['pay_received_amt']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt 
								= '".$data['pay_received_amt_rcpt']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_inr 
								= '".$data['pay_received_amt_inr']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt_inr 
								='".$data['pay_received_amt_rcpt_inr']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".exchange_rate = '".$data['exchange_rate']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_abbr
								='". processUserData($data['currency_abbr']) ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_id = '".   $data['currency_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_name 
								= '". processUserData($data['currency_name'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_symbol 
								= '".processUserData($data['currency_symbol'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_country 
								='".processUserData($data['currency_country'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_words 
								= '".$data['pay_received_amt_words']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_amt = '".$data['credit_pay_amt']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id='".$data['credit_pay_bank_id']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id
								='".$data['ref_credit_pay_bank_id']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id 
								='".$data['ref_debit_pay_bank_id']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".date = '".date("Y-m-d h:i:s")."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".do_transaction = '".$data['do_transaction']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_voucher = '".$data['do_voucher']."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".iaccount_head_id = '".$data['iaccount_head_id']."'" 							  .",". TABLE_PAYMENT_TRANSACTION .".is_auto_entry = '1'" 
							.",". TABLE_PAYMENT_TRANSACTION .".remarks = '".$autoRemark."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".particulars = '".$autoRemark."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".user_id = '".$my['uid']."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".ip = '".$_SERVER['REMOTE_ADDR']."'" 
							.$tr_cr_sql    
							.$tr_db_sql             
							.",". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::AUTOREVERSAL."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".reference_transaction_id = '".$recData['id']."'" ;
                            $db->query($insertquery);
                            $data['trid'] = $db->last_inserted_id(); 
                            $data['number']= Paymenttransaction::getNewNumber($db);    
                            
                            $de_act=1;
                            $query2  = " UPDATE ". TABLE_PAYMENT_TRANSACTION                   
									." SET ". TABLE_PAYMENT_TRANSACTION .".reference_transaction_id 
										= '".$data['trid']."'"   
									." WHERE id = '". $recData['id']."'";
                            $db->query($query2);
							
							//If any bills alloted in this transaction do BOF
							/* 
							$dbnew=new db_local();       							
							$sqlbl="SELECT DISTINCT(".TABLE_PAYMENT_TRANSACTION_BILLS.".bill_id) 
							FROM ".TABLE_PAYMENT_TRANSACTION_BILLS." WHERE transaction_id =".$recData['id'] ;
							$db->query($sqlbl);
							if($db->nf()>0 ){
								while ($db->next_record()) {
									$bbill_id = $db->f('bill_id');									
									$billsql = "UPDATE ".TABLE_PAYMENT_PARTY_BILLS." SET balance =".TABLE_PAYMENT_PARTY_BILLS.".amount,bill_status = '".PaymentPartyBills::BILL_STATUS_UNPAID."' 
										WHERE id=".$bbill_id ;	
									$dbnew->query($billsql);
								}
								//After updating the all bill's balance amt & status delete the alloted bills from transaction bill
								$sqldel = "DELETE FROM ".TABLE_PAYMENT_TRANSACTION_BILLS." WHERE transaction_id =".$recData['id'];
								$dbnew->query($sqldel);
							} 
							*/							
							//If any bills alloted in this transaction do EOF
							
                        }
                        
                    }else{
                        $query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION
                        ." SET ". TABLE_PAYMENT_TRANSACTION .".status_deactivate 
							= '".Paymenttransaction::DEACTIVATE_PENDING."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".reason = '".$_ALL_POST['reason']."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".reason_of_deactivate 
							= '".$_ALL_POST['reason_of_deactivate']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".do_status_update = '".date("Y-m-d h:i:s")."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_by = '".$my['uid']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_ip = '".$_SERVER['REMOTE_ADDR']."'"   
                        ." WHERE id = '". $recData['id']."'";
                        $db->query($query);     
                        $de_act=0;                        
                    }
                
                }elseif($recData['transaction_type'] ==  Paymenttransaction::INTERNAL){
                 
					$data['debit_pay_amt']      = $recData['credit_pay_amt'] ;
                    $data['debit_pay_bank_id']  = $recData['credit_pay_bank_id'];  
                    $data['credit_pay_amt']     = $recData['debit_pay_amt'] ;         
                    $data['credit_pay_bank_id'] = $recData['debit_pay_bank_id'] ;					
                    $data['transaction_type']   = Paymenttransaction::INTERNAL;
                    $data['reference_transaction_id'] =$recData['id'] ;
                    $data['company_id'] =$recData['company_id'] ;
					$data['company_name'] =   $recData['company_name'] ;            
                    $data['company_prefix'] =   $recData['company_prefix'] ;  
                    $data['pay_received_amt'] =$recData['pay_received_amt'] ;
                    $data['do_transaction']=$recData['do_transaction'] ;
					$data['account_head_id'] =   $recData['account_head_id'] ;
					$data['party_id'] =   $recData['party_id'] ;
                    $data['client_id'] =   $recData['client_id'] ;
                    $data['vendor_bank_id'] =   $recData['vendor_bank_id'] ;
                    $data['executive_id'] =   $recData['executive_id'] ;
                    $data['mode_id'] =   $recData['mode_id'] ;  
					$data['pay_cheque_no'] =   $recData['pay_cheque_no'] ;
                    $data['do_pay_received'] =   $recData['do_pay_received'] ;
                    $data['pay_bank_company'] =   $recData['pay_bank_company'] ;
                    $data['pay_branch'] =   $recData['pay_branch'] ;               
                    $data['will_receipt_generate'] =   $recData['will_receipt_generate'] ;
                    $data['is_pdc'] =   $recData['is_pdc'] ;
                    $data['do_pdc_chq'] =   $recData['do_pdc_chq'] ;
                    $data['pdc_chq_details'] =   $recData['pdc_chq_details'] ;
                    $data['pay_received_amt_words'] =   $recData['pay_received_amt_words'] ;
					$data['pay_received_amt_rcpt'] =   $recData['pay_received_amt_rcpt'] ;
                    $data['pay_received_amt_inr'] =   $recData['pay_received_amt_inr'] ;
                    $data['pay_received_amt_rcpt_inr'] =   $recData['pay_received_amt_rcpt_inr'] ;
                    $data['exchange_rate'] =   $recData['exchange_rate'] ;
                    $data['currency_abbr'] =   $recData['currency_abbr'] ;
                    $data['currency_id'] =   $recData['currency_id'] ;
                    $data['currency_name'] =   $recData['currency_name'] ;
                    $data['currency_symbol'] =   $recData['currency_symbol'] ;
                    $data['currency_country'] =   $recData['currency_country'] ;
					$data['ref_credit_pay_bank_id'] = $recData['ref_debit_pay_bank_id'] ;
                    $data['ref_debit_pay_bank_id'] = $recData['ref_credit_pay_bank_id'] ;
                    $data['do_voucher'] = $recData['do_voucher'] ;
                    $data['iaccount_head_id'] = $recData['iaccount_head_id'] ;
                    
                    $dbnew=new db_local();                    	
                    
					$update_query	= " UPDATE ".TABLE_PAYMENT_BANK
					." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$data['debit_pay_amt']." 
						WHERE id='".$data['debit_pay_bank_id']."'";
					$executed = $dbnew->query($update_query);   
					
					$update_query	= " UPDATE ".TABLE_PAYMENT_BANK
					   ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$data['credit_pay_amt']." 
					   WHERE id='".$data['credit_pay_bank_id']."'";
					$executed = $dbnew->query($update_query);
                  
                    //Get balance amount after transaction BOF
                    $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['credit_pay_bank_id'];
                    $db->query($sql);
                    if($db->nf()>0 )
                    {
                        while ($db->next_record()) {
                            $tr_cr_bank_amt = $db->f('balance_amt');
                            $tr_cr_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_cr_bank_amt = '".$tr_cr_bank_amt."'" ;
                        }
                    }
                    $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['debit_pay_bank_id'];
                    $db->query($sql);
                    if($db->nf()>0 )
                    {
                        while ($db->next_record()) {
                            $tr_db_bank_amt = $db->f('balance_amt');
                            $tr_db_sql= ",". TABLE_PAYMENT_TRANSACTION .".tr_db_bank_amt = '".$tr_db_bank_amt."'"  ;
                        }
                    }
                    //Get balance amount after transaction EOF            
                  
                    
                    if( $messages->getErrorMessageCount() <= 0){
                    
                       $query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION
						." SET ". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::CANCELLED_C ."'"   
						.",". TABLE_PAYMENT_TRANSACTION .".reason = '".$_ALL_POST['reason']."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".reason_of_deactivate 
							= '".$_ALL_POST['reason_of_deactivate']."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".status_deactivate 
							= '".Paymenttransaction::DEACTIVATE_COMPLETED ."'"      				   
						.",". TABLE_PAYMENT_TRANSACTION .".do_status_update = '".date("Y-m-d")."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_by = '".$my['uid']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_ip = '".$_SERVER['REMOTE_ADDR']."'"   
						." WHERE  id = '". $recData['id']."'";
                        $db->query($query);
                        //Insert automatic record which shows Out entry 
                       $data['number']= Paymenttransaction::getNewNumber($db);           
                        //insert automatic entry bof
                        if(!empty($data['number'])){
                            $autoRemark = "Auto reversal against voucher no. ".$recData['number']."." ;
                            $insertquery = " INSERT INTO ".TABLE_PAYMENT_TRANSACTION
                            ." SET ". TABLE_PAYMENT_TRANSACTION .".number = '".$data['number'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".company_id = '".$data['company_id'] ."'"                            .",". TABLE_PAYMENT_TRANSACTION .".company_name = '".$data['company_name'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".company_prefix = '".$data['company_prefix'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".account_head_id = '".$data['account_head_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".party_id = '".$data['party_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".client_id = '".$data['client_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id = '".$data['vendor_bank_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".executive_id = '".$data['executive_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".mode_id = '".$data['mode_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_pdc_chq = '".$data['do_pdc_chq'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pdc_chq_details = '".$data['pdc_chq_details'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_cheque_no = '".$data['pay_cheque_no'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_pay_received = '".$data['do_pay_received'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_bank_company = '".$data['pay_bank_company'] ."'"
                            .",". TABLE_PAYMENT_TRANSACTION .".transaction_type = '".$data['transaction_type']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt = '".$data['pay_received_amt']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt 
								= '".$data['pay_received_amt_rcpt']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_inr 
								= '".$data['pay_received_amt_inr']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt_inr 
								='".$data['pay_received_amt_rcpt_inr']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".exchange_rate = '".$data['exchange_rate']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_abbr 
								= '". processUserData($data['currency_abbr']) ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_id = '".   $data['currency_id'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_name 
								= '". processUserData($data['currency_name'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_symbol 
								= '".processUserData($data['currency_symbol'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_country 
								='".processUserData($data['currency_country'])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_words 
								= '".$data['pay_received_amt_words']  ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_amt = '".$data['credit_pay_amt']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id = '".$data['credit_pay_bank_id']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_amt = '".$data['debit_pay_amt']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = '".$data['debit_pay_bank_id']."'"
							 .",". TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id
								='".$data['ref_credit_pay_bank_id']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id
								='".$data['ref_debit_pay_bank_id']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".date = '".date("Y-m-d h:i:s")."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".do_transaction = '".$data['do_transaction']."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".do_voucher = '".$data['do_voucher']."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".iaccount_head_id = '".$data['iaccount_head_id']."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".is_auto_entry = '1'" 
							.",". TABLE_PAYMENT_TRANSACTION .".remarks = '".$autoRemark."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".particulars = '".$autoRemark."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".user_id = '".$my['uid']."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".ip = '".$_SERVER['REMOTE_ADDR']."'" 
							.$tr_cr_sql    
							.$tr_db_sql             
							.",". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::AUTOREVERSAL."'" 
							.",". TABLE_PAYMENT_TRANSACTION .".reference_transaction_id = '".$recData['id']."'" ;
                            $db->query($insertquery);
							
                            $data['trid'] = $db->last_inserted_id(); 
                            $de_act=1;
                            $query2  = " UPDATE ". TABLE_PAYMENT_TRANSACTION                   
							." SET ". TABLE_PAYMENT_TRANSACTION .".reference_transaction_id = '".$data['trid']."'"   
							." WHERE id = '". $recData['id']."'";
                            $db->query($query2);
                        }
                        
                    }else{
                        $query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION
                        ." SET ". TABLE_PAYMENT_TRANSACTION .".status_deactivate 
							= '".Paymenttransaction::DEACTIVATE_PENDING."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".reason = '".$_ALL_POST['reason']."'"      
						.",". TABLE_PAYMENT_TRANSACTION .".reason_of_deactivate 
							= '".$_ALL_POST['reason_of_deactivate']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".do_status_update = '".date("Y-m-d h:i:s")."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_by = '".$my['uid']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".status_update_ip = '".$_SERVER['REMOTE_ADDR']."'"   
						." WHERE id = '". $recData['id']."'";
                        $db->query($query);         
                         $de_act=0;
                    }
                }
                if($de_act==1){               
                    header("Location:".DIR_WS_NC."/payment-transaction.php?transaction_id=".$recData['number']."&de_act=1&sString=".$recData['number']."&chk_fstatus=AND&fStatus=0&sType=-1&perform=search");
                }                
            }else{
                if($check==1){
                    //$hidden[] = array('name'=> 'id' ,'value' => $recData['transaction_id']);
                    $hidden[] = array('name'=> 'id' ,'value' => $recData['id']);
                    $hidden[] = array('name'=> 'perform' ,'value' => $perform);
                    $hidden[] = array('name'=> 'act' , 'value' => 'save');
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-deactivate.html'); 
                }
            }
        }else{
        
            //$hidden[] = array('name'=> 'transaction_id' ,'value' => $recData['transaction_id']);
            $hidden[] = array('name'=> 'id' ,'value' => $recData['id']);
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-deactivate.html');   

        }
            
    }else{
        $messages->setOkMessage("Record is not completed mark, so can not mark it as Deactivate.");
    }    
  
    if( ($messages->getErrorMessageCount() > 0 && $check==0) || $de_act==0 ){
        $perform='list';
        $condition_query='';
        $variables['hid'] = $recData['id'];
        include ( DIR_FS_NC .'/payment-transaction-list.php');    
    }
    
    
}else{
    $messages->setErrorMessage("You donot have the Permisson to Access this module.");
}
  
?>