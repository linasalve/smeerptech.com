<?php
    if ( $perm->has('nc_sms_ma_status') ) {
	$ma_id	= isset($_GET["ma_id"]) 	? $_GET["ma_id"] 	: ( isset($_POST["ma_id"]) 	? $_POST["ma_id"] : '' );
	$status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
	
	$extra = array( 'db' 		=> &$db,
					'messages' 	=> $messages
				);
	MasterAccount::updateStatus($ma_id, $status, $extra);
	
	// Display the Webpage list.
	include ( DIR_FS_NC .'/master-account-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to change the status.");
    }
?>