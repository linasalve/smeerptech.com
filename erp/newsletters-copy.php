<?php

    if ( $perm->has('nc_nwl_copy') ) {
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
		$_ALL_POST	= NULL;
        $data       = NULL;
       
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
         include_once ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');
       
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
          
                if ( Newsletters::validateAdd($data, $extra) ) { 
                   
                    $number  =  Newsletters::getNewNumber($db);
                    $attachfilename=$attachment_path='';
                    if(!empty($data['attachment'])){
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['attachment']["name"]); 
                        $ext = $filedata["extension"] ;  
                        $attachfilename =  $number."-".mktime().".".$ext ;
						$data['attachment'] = $attachfilename;                        
                        $attachment_path = DIR_WS_NWS_FILES;                        
                        if (move_uploaded_file ($files['attachment']['tmp_name'], 
							DIR_FS_NWS_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_NWS_FILES."/".$attachfilename, 0777);
                        }
                    }else{
						$attachfilename =$data['old_attachment'];
					
					}
                   
                    $send_newsletter=0;
					$added=1;
					$status = Newsletters::PENDING;
					$condition =$message=''; 
					$total_members=0;					
                    $per_batch = PER_BATCH;
					$total_batches=0;
					
                    if(isset($data['send_newsletter']) ){
						$added=2;
                        $send_newsletter=1;
						$status = Newsletters::PENDING;
						$message = ' and added in a queue.';						
						$sqlCount =  "SELECT COUNT(".TABLE_NEWSLETTERS_MEMBERS.".id) as total FROM 
						".TABLE_NEWSLETTERS_MEMBERS." WHERE 
						( ".TABLE_NEWSLETTERS_MEMBERS.".email_bounce_status!='".NewslettersMembers::EMAILBOUNCE."'	AND 	
						".TABLE_NEWSLETTERS_MEMBERS.".unsubscribed_status!='".NewslettersMembers::UNSUBSCRIBED."' )" ;
						
						$db->query($sqlCount);
						if ( $db->nf() > 0 ) {
							while ($db->next_record()) {
								$total_members	= $db->f('total');
							}
						}
						
                    }else{
						$status = Newsletters::DRAFT;
					}               
										
					if($total_members > 0){
						$total_batches =  ceil($total_members/$per_batch)  ;	
					}else{
						if($send_newsletter==1){
							$added=3;
							$send_newsletter=0;
							$status=Newsletters::DRAFT;
							$message=' and Save as a Draft <br/> as No members Found in NDB';
						}else{
							$added=4;
							$message=' and Save as a Draft.';
						}
					}
					
					
                    $query = "INSERT INTO "	. TABLE_NEWSLETTERS ." SET "
							. TABLE_NEWSLETTERS .".number      = '". $number   ."', "
							. TABLE_NEWSLETTERS .".test_email    = '". $data['test_email'] ."', "
							. TABLE_NEWSLETTERS .".subject    = '". $data['subject'] ."', "
							. TABLE_NEWSLETTERS .".text       = '". $data['text'] ."', "
							. TABLE_NEWSLETTERS .".attachment = '". $attachfilename ."', "
							. TABLE_NEWSLETTERS .".send_newsletter        = '".$send_newsletter."', "
							. TABLE_NEWSLETTERS .".total_members        = '".$total_members."', "
							. TABLE_NEWSLETTERS .".per_batch        = '".$per_batch."', "
							. TABLE_NEWSLETTERS .".total_batches    = '".$total_batches."', "
							. TABLE_NEWSLETTERS .".status        = '".$status."', "
							. TABLE_NEWSLETTERS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_NEWSLETTERS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_NEWSLETTERS .".send_on_date    = '".date('Y-m-d H:i:s')."', "							 
							. TABLE_NEWSLETTERS .".created_by= '". $my['uid'] ."', "
							. TABLE_NEWSLETTERS .".created_by_name    = '". $my['f_name']." ". $my['l_name'] ."'" ;
                    
                 
                                
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        
                        $variables['hid'] = $db->last_inserted_id() ;						
					  				
						if($send_newsletter==1){
							$query1 = "INSERT INTO "	
							.TABLE_NEWSLETTERS_BATCHES ." SET "
							.TABLE_NEWSLETTERS_BATCHES .".newsletters_id         = '". $variables['hid']."', "
							.TABLE_NEWSLETTERS_BATCHES .".subject    = '". $data['subject'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".text       = '". $data['text'] ."', "
							.TABLE_NEWSLETTERS_BATCHES .".attachment = '". $attachfilename ."', "
							.TABLE_NEWSLETTERS_BATCHES .".total_batches    = '".$total_batches."', "
							.TABLE_NEWSLETTERS_BATCHES .".pending_batches    = '".$total_batches."', "
							.TABLE_NEWSLETTERS_BATCHES .".total_members        = '".$total_members."', "
							.TABLE_NEWSLETTERS_BATCHES .".per_batch        = '".$per_batch."', "                      
							.TABLE_NEWSLETTERS_BATCHES .".send_on_date    = '".date('Y-m-d H:i:s')."', "                            .TABLE_NEWSLETTERS_BATCHES .".do_e    = '".date('Y-m-d H:i:s')."' "; 
							$db->query($query1);
						}
						
                         //Send a copy to check the mail for clients
						 
						if(!empty($data['test_email'])){
							$smeerp_support_email=$data['test_email'];
						}
						$unsubid = base64_encode($smeerp_support_email);
						$nws_id = base64_encode($variables['hid']);
						$data['unsubscribed'] = THIS_DOMAIN.'/unsubscribe.php?id='.$unsubid   ;
						$data['html_link'] = THIS_DOMAIN.'/newsletters.php?id='.$nws_id."&email=".$unsubid   ;
						$data['attachment']   =   $attachfilename ;
						$data['counter']   =   1 ;
						$data['subject']   =   $_ALL_POST['subject'] ;
						$data['text']   =   $_ALL_POST['text'] ;
						$file_name = DIR_FS_NWS_FILES ."/". $attachfilename;
						 //Send a copy to check the mail for clients
						 $sendStatus='';
						 $to_email ='';
						 
                        if( getParsedEmail($db, $s, 'EMAIL_NEWSLETTERS', $data, $email) ){ 
							
							if(!empty($data['test_email'])){
								$smeerp_support_email=$data['test_email'];
							}							    
							if(!empty($smeerp_support_email)){
                                $to     = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $smeerp_support_name, 
                                'email' => $smeerp_support_email);
							 $from=$reply_to = array('name' => $email["from_name"],'email' => $email['from_email']);
                               
                            }
							echo $email["body"];
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 	
								$email["isHTML"],
                                $cc_to_sender,$cc,$bcc,$file_name); 							
                        } 
						
                        $messages->setOkMessage("Newsletter has been created ".$message);
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
                }
        }else{
		
			$fields = TABLE_NEWSLETTERS .'.*'  ;            
            $condition_query = " WHERE (". TABLE_NEWSLETTERS .".id = '". $id ."' )";
          
            if ( Newsletters::getList($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];                   
                $id         = $_ALL_POST['id'];
                $old_attachment         = $_ALL_POST['attachment'];
             
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission 
				to access this Module.");
            }
		}
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
            header("Location:".DIR_WS_NC."/newsletters.php?perform=add&added=".$added);
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
             
			header("Location:".DIR_WS_NC."/newsletters.php?perform=list");
        }
        else {
        
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
			$hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'old_attachment','value' => $old_attachment);
            $hidden[] = array('name'=> 'perform','value' => 'copy');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');			 
            
           // $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletters-copy.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>