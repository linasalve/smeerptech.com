<?php 
    ob_start();
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("lib/config.php");
	}    
   	page_open(array("sess" => "MP_Session",
                    "auth" => "MP_Auth",
                    "perm" => "MP_Perm"
                ));

    include( DIR_FS_MP ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    include_once ( DIR_FS_INCLUDES .'/send-sms.inc.php'); 
    include_once ( DIR_FS_INCLUDES .'/sms-client-account.inc.php'); 
    include_once ( DIR_FS_INCLUDES .'/sms-template.inc.php'); 
    //require( DIR_FS_CLASS ."/sms.class.php");
    include_once ( DIR_FS_CLASS .'/Validation.class.php');
	
    
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : 'add' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page

    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
     
    
    
	if($variables['has_sms']){

        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_SMS_HISTORY   =>  array(   
                                                            'Sender Name'     => 'sender_name',
                                                            'Message'        => 'message' 
                                                        )
                            );
        
        $sOrderByArray  = array(
                                TABLE_SMS_HISTORY => array( 'Sender Name'     => 'sender_name',
                                                            'Message'        => 'message' 
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SMS_HISTORY;
        }
    
        
        //use switch case here to perform action. 
        switch ($perform) {
			case ('search'):{ 
                include (DIR_FS_MP.'/send-sms-search.php'); 
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'send-sms.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }  
            case ('list'): { 
                include (DIR_FS_MP.'/send-sms-list.php'); 
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'send-sms.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }            
             
            case ('add'):
            default: {
                include (DIR_FS_MP .'/send-sms-add.php');
                
				// CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'send-sms.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'clients.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>