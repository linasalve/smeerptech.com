<?php
if ( $perm->has('nc_rp_txbl') ) {
		include_once ( DIR_FS_INCLUDES .'/payment-party-bills.inc.php' );
		include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
		include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');        
		// Read the Date data.
        
        $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]   : (isset($_GET["date_from"])    
		? $_GET["date_from"]    :'');
        $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
        $tax1_id = isset($_POST["tax1_id"])      ? $_POST["tax1_id"]         : (isset($_GET["tax1_id"])      ? $_GET["tax1_id"]      :'');
        $bill_type        = isset($_POST["bill_type"]) ? $_POST["bill_type"]  : (isset($_GET["bill_type"])      ? $_GET["bill_type"]      :'');
        $report_type  = isset($_POST["report_type"]) ? $_POST["report_type"]  : (isset($_GET["report_type"])  ? $_GET["report_type"]      :'1');  //default is purchase bills 
		
		$condition_query2 = $condition_url ='';
        $where_added =true;  
		$totalSum=$taxname=''	;	
        $_ALL_POST 	=$_SEARCH= $_POST;
        $list=$ntlist=null;    
		 
		if ( !isset($_SEARCH) ) {
			$_SEARCH = '';
		} 
		// print_R( $_ALL_POST);
		$tax_list    = NULL;
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=0 ORDER BY tax_name ASC";
		ServiceTax::getList($db, $tax_list, 'id,tax_name', $condition_queryst);
		
		$show=0;
        if(array_key_exists('submit_search',$_POST)){
			 
			$show=1;
            if(empty($date_from) || empty($date_to) ){        
              $messages->setErrorMessage('Please select From Date and To Date.');
            } 
			/* if(empty($tax1_id)){        
              $messages->setErrorMessage('Please select Tax.');
            } 
			
			if(empty($bill_type)){        
              $messages->setErrorMessage('Please select GOODS or SERVICES.');
            } 
			*/
        }    
            
   
    
    //As validate both dates
    if($messages->getErrorMessageCount() <= 0 && $show==1 ){
    
        $list	= $dateArr=	NULL;   
		if( $report_type ==1 ){		
			$where_added = false;
		}elseif($report_type == 2){
			$where_added = true;
		}
        // BO: From Date
        if ( !empty($date_from)){
            if ( $where_added ) {
                $condition_query2 .= " AND " ;
            } else {
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            $dfa = explode('/', $date_from);
            $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
			
            if( $report_type==1){
				$condition_query2 .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_dt >= '". $dfa ."' ";
            }elseif($report_type==2){
				$condition_query2 .= " ". TABLE_BILL_INV .".do_i >= '". $dfa ."' ";
			}
			
            $condition_url   .= "&date_from=$date_from";
          
        }
        // EO: From Date
        
        // BO: Upto Date
        if ( !empty($date_to)) {
            if ( $where_added ) {
                $condition_query2 .= " AND " ;
            }
            else {
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            $dta = explode('/', $date_to);
            $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
			
            if( $report_type==1){
				$condition_query2 .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_dt <= '". $dta ."' ";
            }elseif($report_type==2){
				$condition_query2 .= " ". TABLE_BILL_INV .".do_i <= '". $dta ."' ";
			}
            $condition_url          .= "&date_to=$date_to";
         
        }
        // EO: Upto Date
		
		if ( !empty($tax1_id)) {
			 
			if( $report_type==1){
				if($tax1_id!=-1){
					$condition_query2 .= " AND ".TABLE_PAYMENT_PARTY_BILLS.".tax_ids!='' AND 
					".TABLE_PAYMENT_PARTY_BILLS.".tax_ids LIKE '%,".trim($tax1_id).",%'" ;
				}else{
					$condition_query2 .= " AND ".TABLE_PAYMENT_PARTY_BILLS.".tax_ids='' AND 
					".TABLE_PAYMENT_PARTY_BILLS.".tax_ids LIKE '%,0,%'" ;			
				}
			}elseif($report_type==2){	
				if($tax1_id!=-1){
					$condition_query2 .= " AND ".TABLE_BILL_INV.".tax_ids!='' AND 
						".TABLE_BILL_INV.".tax_ids LIKE '%,".trim($tax1_id).",%'" ;
				}else{
					$condition_query2 .= " AND ".TABLE_BILL_INV.".tax_ids='' AND ".TABLE_BILL_INV.".tax_ids =',0,'" ;
				}
			}
			 
		}
		/* if ( !empty($bill_type)) {
			$condition_query2 .= " AND ".TABLE_PAYMENT_PARTY_BILLS.".bill_type ='".$bill_type."'" ;
		} */
		
		//CSV Variables BOF
			$csv_terminated = "\n";
			$csv_separator = ",";
			$csv_enclosed = '"';
			$csv_escaped = "\\";
			$schema_insert = '';
			$schema_insert_nt = '';
			$schema_insert1 = '';
			$schema_insert2 = '';
			$grant_total1 = 0; 
		//CSV Variables EOF
		
		//PURCHASE BILLS
		if($report_type==1){
				$query="SELECT ".TABLE_PAYMENT_PARTY_BILLS.".bill_no,".TABLE_PAYMENT_PARTY_BILLS.".party_id,"
					.TABLE_PAYMENT_PARTY_BILLS.".number,"
					.TABLE_PAYMENT_PARTY_BILLS.".amount,"
					.TABLE_PAYMENT_PARTY_BILLS.".bill_dt,".TABLE_PAYMENT_PARTY_BILLS.".bill_amount,
					".TABLE_PAYMENT_BILLS_P.".stot_amount," 
					.TABLE_PAYMENT_BILLS_P.".particulars,"	
					.TABLE_PAYMENT_BILLS_P.".p_amount,"	
					.TABLE_PAYMENT_BILLS_P.".s_quantity,"	
					.TABLE_PAYMENT_BILLS_P.".tax1_id,"	
					.TABLE_PAYMENT_BILLS_P.".tax1_name,".TABLE_PAYMENT_BILLS_P.".tax1_value,".TABLE_PAYMENT_BILLS_P.".tax1_amount,"
					.TABLE_PAYMENT_BILLS_P.".tax1_sub1_name,".TABLE_PAYMENT_BILLS_P.".tax1_sub1_value,
					".TABLE_PAYMENT_BILLS_P.".tax1_sub1_amount,"
					.TABLE_PAYMENT_BILLS_P.".tax1_sub2_name,".TABLE_PAYMENT_BILLS_P.".tax1_sub2_value,
					".TABLE_PAYMENT_BILLS_P.".tax1_sub2_amount,"
					.TABLE_PAYMENT_BILLS_P.".tot_amount FROM ".TABLE_PAYMENT_PARTY_BILLS." LEFT JOIN ".TABLE_PAYMENT_BILLS_P." ON 
					".TABLE_PAYMENT_PARTY_BILLS.".id=".TABLE_PAYMENT_BILLS_P.".bill_id 
					 ".$condition_query2." ORDER BY 
					".TABLE_PAYMENT_PARTY_BILLS.".number, ".TABLE_PAYMENT_PARTY_BILLS.".bill_dt " ;		
						
				if ( $db->query($query) ) {
					if ( $db->nf() > 0 ) {
					$key1=$key2=0;
					$myTData=array(0=>'key',
								  1=>'number',
								  2=>'bill_no',
								  3=>'bill_dt',
								  4=>'vendor',
								  5=>'particulars',
								  6=>'s_quantity',
								  7=>'p_amount',
								  8=>'bill_amount',
								  9=>'amount',
								  10=>'stot_amount',
								  11=>'tax1_name',
								  12=>'tax1_value',
								  13=>'tax1_amount',
								  14=>'tax1_sub1_name',
								  15=>'tax1_sub1_value',
								  16=>'tax1_sub1_amount',
								  17=>'tax1_sub2_name',
								  18=>'tax1_sub2_value',
								  19=>'tax1_sub2_amount',
								  20=>'tot_amount'
					 );
					   $myNTData=array(0=>'key',
								  1=>'number',
								  2=>'bill_no',
								  3=>'bill_dt',
								  4=>'vendor',
								  5=>'particulars',								  
								  6=>'s_quantity',								  
								  7=>'p_amount',								  
								  8=>'bill_amount',	
								  9=>'amount',								  
								  10=>'tot_amount'
						);
					 
						while ($db->next_record()) {
											
							$party_id = $db->f('party_id') ;
							$vendor_bank_id = $db->f('vendor_bank_id') ;
							$tax1_id_check = $db->f('tax1_id') ;
							$bill_no=$db->f('bill_no');
							$number=$db->f('number');
							$particulars = $db->f('particulars');
							$s_quantity = $db->f('s_quantity');
							$p_amount = $db->f('p_amount');
							$bill_amount = number_format($db->f('bill_amount'),2);
							$stot_amount = number_format($db->f('stot_amount'),2);
							$bill_dt_str='';
							$bill_dt_str = $db->f('bill_dt');
							$bill_dt = date('d M Y',strtotime($bill_dt_str));
							$tax1_name = $db->f('tax1_name');
							$tax1_value = $db->f('tax1_value');
							$tax1_amount = number_format($db->f('tax1_amount'),2);
							$tax1_sub1_name = $db->f('tax1_sub1_name');
							$tax1_sub1_value = $db->f('tax1_sub1_value');
							$tax1_sub1_amount = number_format($db->f('tax1_sub1_amount'),2);
						 	$tax1_sub2_name = $db->f('tax1_sub2_name');
							$tax1_sub2_value = $db->f('tax1_sub2_value');
							$tax1_sub2_amount = number_format($db->f('tax1_sub2_amount'),2);
							$tot_amount = number_format($db->f('tot_amount'),2);
							$amount = number_format($db->f('amount'),2);
							
							if(!empty($party_id)){
								$fields1 =  TABLE_CLIENTS .".billing_name,".TABLE_CLIENTS.".f_name,
								".TABLE_CLIENTS.".l_name ";                
								$condition2 = " WHERE ".TABLE_CLIENTS .".user_id='".$party_id."' LIMIT 0,1" ;
								$vendor='';
								$table = TABLE_CLIENTS ;              
								$compArr = getRecord($table,$fields1,$condition2);
								if(!empty($compArr)){
									$vendor = $compArr['billing_name']  ;   
								}							
							}elseif(!empty($vendor_bank_id) ){
								$fields1 =  TABLE_VENDORS_BANK .".billing_name,".TABLE_VENDORS_BANK.".f_name,
								".TABLE_VENDORS_BANK.".l_name ";                
								$condition2 = " WHERE ".TABLE_VENDORS_BANK .".user_id='".$vendor_bank_id."' LIMIT 0,1" ;
								$vendor='';
								$table = TABLE_VENDORS_BANK ;              
								$compArr = getRecord($table,$fields1,$condition2);
								if(!empty($compArr)){
									$vendor = $compArr['billing_name']  ;   
								}	
							}
							if((!empty($tax1_id) && $tax1_id_check==$tax1_id) || (!empty($tax1_id_check)  && $tax1_id=='-1') ){
								$key1++;
								$key=$key1;
								$list[] = array(
									'number'=> $db->f('number'),
									'bill_no'=> $db->f('bill_no'),
									'bill_dt'=> $bill_dt,
									'vendor'=> $vendor,
									'particulars'=> $particulars,
									's_quantity'=> $s_quantity,									
									'p_amount'=> $p_amount,									
									'bill_amount'=> number_format($db->f('bill_amount'),2),
									'stot_amount'=> number_format($db->f('stot_amount'),2),
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=> number_format($db->f('tax1_amount'),2),
									'tax1_sub1_name'=> $db->f('tax1_sub1_name'),
									'tax1_sub1_value'=> $db->f('tax1_sub1_value'),
									'tax1_sub1_amount'=> number_format($db->f('tax1_sub1_amount'),2),
									'tax1_sub2_name'=> $db->f('tax1_sub2_name'),
									'tax1_sub2_value'=> $db->f('tax1_sub2_value'),
									'tax1_sub2_amount'=> number_format($db->f('tax1_sub2_amount'),2),
									'tot_amount'=> number_format($db->f('tot_amount'),2),
									'amount'=> number_format($db->f('amount'),2)
									);
									
									//Sr no
									$schema_insert11='';
									foreach($myTData as $key_1 =>$val){
										$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,										stripslashes($$val)) . $csv_enclosed;
										//$schema_insert1 .= $l;
										$schema_insert11 .= $l.$csv_separator;
									}
									$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
									$schema_insert1 .= $csv_terminated;
									$grant_total1 = $grant_total1 + $tot_amount	;
									
							}else{
								$key2++;
								$key=$key2;
								$ntlist[] = array(
									'number'=> $db->f('number'),
									'bill_no'=> $db->f('bill_no'),
									'bill_dt'=> $bill_dt,
									'vendor'=> $vendor,
									'particulars'=> $particulars,
									's_quantity'=> $s_quantity,	
									'p_amount'=> $p_amount,																		
									'bill_amount'=> number_format($db->f('bill_amount'),2),
									'stot_amount'=> number_format($db->f('stot_amount'),2),
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=> number_format($db->f('tax1_amount'),2),
									'tax1_sub1_name'=> $db->f('tax1_sub1_name'),
									'tax1_sub1_value'=> $db->f('tax1_sub1_value'),
									'tax1_sub1_amount'=> number_format($db->f('tax1_sub1_amount'),2),
									'tax1_sub2_name'=> $db->f('tax1_sub2_name'),
									'tax1_sub2_value'=> $db->f('tax1_sub2_value'),
									'tax1_sub2_amount'=> $db->f('tax1_sub2_amount'),
									'tot_amount'=> number_format($db->f('tot_amount'),2),
									'amount'=> number_format($db->f('amount'),2)
									);
									$schema_insert22='';
									foreach($myNTData as $key_2 =>$val){							 
										$ll = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . 
										$csv_enclosed,stripslashes($$val)) . $csv_enclosed;
										//$schema_insert1 .= $l;
										$schema_insert22 .= $ll.$csv_separator;
									}
									$schema_insert2 .= trim(substr($schema_insert22, 0, -1));
									$schema_insert2 .= $csv_terminated;
							
							}
						}
					}
					//Write CSV BOF
				
					
					$heading1=array('Sr No','NC Ref No','Bill No','Bill Date',
					'Vendor','Item','Quantity','Unit Rate',
					'Bill Amount','Paid Amount','Taxed Amount','Taxname','Tax%',
					'Tax Amount','SubTax1','Sub Tax1%', 'Sub Tax1 Amount', 
					'SubTax2','Sub Tax2%', 'Sub Tax2 Amount',
					'Total Amount');
					$fields_cnt = count($heading1);
					
					if(!empty($tax1_id)){
						$taxname_list=array();
						$condition_queryst= " WHERE id='".$tax1_id."'";
						ServiceTax::getList($db, $taxname_list, 'id,tax_name', $condition_queryst);
						
						$taxname='';
						if(!empty($taxname_list)){						
							$taxname_list = $taxname_list[0];
							$taxname = $taxname_list['tax_name'];						 
						}
					}
					
					$bill_type_name ='';
					
					//First Part Display BOF
					$taxname_str ='';
					if(!empty($taxname)){
						$taxname_str =' Tax - '.$taxname ;
					}else{						
						$taxname_str = 'With All tax';
					}
					
					$mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results for Period '.date('d M Y',strtotime($dfa)) .' - '.date('d M Y',strtotime($dta)).' '.$taxname_str.' ')) . $csv_enclosed;
					
					$schema_insert .= $mHead1;
					$schema_insert .= $csv_separator;
					$schema_insert .= $csv_terminated;
					
					$mHead2 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes( $taxname.' Particulars of the Bills')). $csv_enclosed;
					$schema_insert .= $mHead2;
					$schema_insert .= $csv_separator;
					$schema_insert .= $csv_terminated;;
					for ($i = 0; $i < $fields_cnt; $i++){
						$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes($heading1[$i])) . $csv_enclosed;
						$schema_insert .= $l;
						$schema_insert .= $csv_separator;
					} // end for
				 
					$out1 = trim(substr($schema_insert, 0, -1));
					$out1 .= $csv_terminated;
					$out1 .= $schema_insert1 ;
					
					//Row show the grand total bof
					//$grant_total1 = number_format($grant_total1,2);
					/*$granttotstr = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes("Grand Total is ".$grant_total1)) . $csv_enclosed;
					$granttotstr .= $csv_separator;
					$granttotstr = trim(substr($granttotstr, 0, -1));
					$out1 .=$granttotstr ;
					$out1 .= $csv_terminated;
					*/
					//Row show the grand total EOF
					
					//First Part Display EOF
					//Second Part Display BOF
					$taxname_str ='';
					if(!empty($taxname)){
						$taxname_str =' Non - '.$taxname ;
					}else{
						$taxname_str = 'Without tax';
					}
					$heading2=array('Sr No','NC Ref No','Bill No','Bill Date',
					'Vendor','Item','Quantity','Unit Rate',
					'Bill Amount','Paid Amount',$taxname_str.' Amount');
					$fields_cnt2 =count($heading2);
					$mHead3 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes(' '.$taxname_str.' Particulars of the Bills')). $csv_enclosed;
					$schema_insert_nt .= $mHead3;
					$schema_insert_nt .= $csv_separator;
					$schema_insert_nt .= $csv_terminated;;
					for ($i = 0; $i < $fields_cnt2; $i++)
					{
						$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes($heading2[$i])) . $csv_enclosed;
						$schema_insert_nt .= $l;
						$schema_insert_nt .= $csv_separator;
					} // end for
				 
					$out2 = trim(substr($schema_insert_nt, 0, -1));
					$out2 .= $csv_terminated;
					$out2 .= $schema_insert2 ;
					//Second Part Display EOF
					
					
					
					$out=$out1.$out2;
					$filename = date('dmY').".csv";
					header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
					header("Content-Length: " . strlen($out));
					// Output to browser with appropriate mime type, you choose ;)
					header("Content-type: text/x-csv");
					//header("Content-type: text/csv");
					//header("Content-type: application/csv");
					header("Content-Disposition: attachment; filename=$filename");
					echo $out;
					exit;
					//Write CSV EOF
				}
				
		}elseif($report_type==2){
			//SALES BILLS
			
				 $query="SELECT ".TABLE_BILL_INV.".number,".TABLE_BILL_INV.".client,"
				.TABLE_BILL_INV.".do_i,".TABLE_BILL_INV.".amount,".TABLE_BILL_ORD_P.".stot_amount," 
				.TABLE_BILL_ORD_P.".tax1_id,"	
				.TABLE_BILL_ORD_P.".tax1_name,".TABLE_BILL_ORD_P.".tax1_value,".TABLE_BILL_ORD_P.".tax1_amount,"
				.TABLE_BILL_ORD_P.".tax1_sub1_name,".TABLE_BILL_ORD_P.".tax1_sub1_value,".TABLE_BILL_ORD_P.".tax1_sub1_amount,"
				.TABLE_BILL_ORD_P.".tax1_sub2_name,".TABLE_BILL_ORD_P.".tax1_sub2_value,".TABLE_BILL_ORD_P.".tax1_sub2_amount,"
				.TABLE_BILL_ORD_P.".tot_amount FROM ".TABLE_BILL_INV." LEFT JOIN ".TABLE_BILL_ORD_P." ON 
				".TABLE_BILL_INV.".or_no=".TABLE_BILL_ORD_P.".ord_no WHERE 
				".TABLE_BILL_INV.".status IN ('".Invoice::PENDING."','".Invoice::COMPLETED."')
				 ".$condition_query2." ORDER BY 
				".TABLE_BILL_INV.".number, ".TABLE_BILL_INV.".do_i " ;		
						
				if ( $db->query($query) ) {
					if ( $db->nf() > 0 ) {
					$key1=$key2=0;
					$myTData=array(0=>'key',
								  1=>'number',
								  2=>'do_i',
								  3=>'amount',
								  4=>'stot_amount',
								  5=>'tax1_name',
								  6=>'tax1_value',
								  7=>'tax1_amount',
								  8=>'tax1_sub1_name',
								  9=>'tax1_sub1_value',
								  10=>'tax1_sub1_amount',
								  11=>'tax1_sub2_name',
								  12=>'tax1_sub2_value',
								  13=>'tax1_sub2_amount',
								  14=>'tot_amount'
					 );
					 $myNTData=array(0=>'key',
								  1=>'number',
								  2=>'do_i',
								  3=>'amount',						 
								  4=>'tot_amount'
					 );
					 
						while ($db->next_record()) {
											
							$tax1_id_check = $db->f('tax1_id') ;
							$number=$db->f('number');
							$amount=number_format($db->f('amount'),2);
							$stot_amount=number_format($db->f('stot_amount'),2);
							$tax1_name= $db->f('tax1_name');
							$do_i_str= $db->f('do_i');
							$do_i = date('d M Y',strtotime($do_i_str));
							$tax1_value= $db->f('tax1_value');
							$tax1_amount=number_format($db->f('tax1_amount'),2);
							
							$tax1_sub1_name = $db->f('tax1_sub1_name');
							$tax1_sub1_value = $db->f('tax1_sub1_value');
							$tax1_sub1_amount = number_format($db->f('tax1_sub1_amount'),2);
						 	$tax1_sub2_name = $db->f('tax1_sub2_name');
							$tax1_sub2_value = $db->f('tax1_sub2_value');
							$tax1_sub2_amount = number_format($db->f('tax1_sub2_amount'),2);
							$tot_amount=number_format($db->f('tot_amount'),2);
							if(!empty($tax1_id_check) && $tax1_id_check==$tax1_id){
								$key1++;
								$key=$key1;
								$list[] = array(
									'number'=> $db->f('number'),
									'do_i'=> $do_i,
									'amount'=> number_format($db->f('amount'),2),
									'stot_amount'=> number_format($db->f('stot_amount'),2),
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=> number_format($db->f('tax1_amount'),2),
									'tax1_sub1_name'=> $db->f('tax1_sub1_name'),
									'tax1_sub1_value'=> $db->f('tax1_sub1_value'),
									'tax1_sub1_amount'=> number_format($db->f('tax1_sub1_amount'),2),
									'tax1_sub2_name'=> $db->f('tax1_sub2_name'),
									'tax1_sub2_value'=> $db->f('tax1_sub2_value'),
									'tax1_sub2_amount'=> number_format($db->f('tax1_sub2_amount'),2),
									'tot_amount'=> number_format($db->f('tot_amount'),2)
									);
									
									//Sr no
									$schema_insert11='';
									foreach($myTData as $key_1 =>$val){
									 
										$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,										stripslashes($$val)) . $csv_enclosed;
										//$schema_insert1 .= $l;
										$schema_insert11 .= $l.$csv_separator;
									}
									$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
									$schema_insert1 .= $csv_terminated;
									
									
									$grant_total1 = $grant_total1 + $tot_amount	;
									
							}else{
								$key2++;
								$key=$key2;
								$ntlist[] = array(
									'number'=> $db->f('number'),
									'do_i'=> $do_i,
									'amount'=> number_format($db->f('amount'),2),
									'stot_amount'=> number_format($db->f('stot_amount'),2),
									'tax1_name'=> $db->f('tax1_name'),
									'tax1_value'=> $db->f('tax1_value'),
									'tax1_amount'=> number_format($db->f('tax1_amount'),2),
									'tax1_sub1_name'=> $db->f('tax1_sub1_name'),
									'tax1_sub1_value'=> $db->f('tax1_sub1_value'),
									'tax1_sub1_amount'=> number_format($db->f('tax1_sub1_amount'),2),
									'tax1_sub2_name'=> $db->f('tax1_sub2_name'),
									'tax1_sub2_value'=> $db->f('tax1_sub2_value'),
									'tax1_sub2_amount'=> $db->f('tax1_sub2_amount'),
									'tot_amount'=> number_format($db->f('tot_amount'),2)
									);
									$schema_insert22='';
								foreach($myNTData as $key_2 =>$val){							 
								$ll = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped.$csv_enclosed,stripslashes($$val)) . $csv_enclosed;
									//$schema_insert1 .= $l;
									$schema_insert22 .= $ll.$csv_separator;
								}
									$schema_insert2 .= trim(substr($schema_insert22, 0, -1));
									$schema_insert2 .= $csv_terminated;
							
							}
						}
					}
					//Write CSV BOF
				
					
					$heading1=array('Sr No','Invoice No','Invoice Date','Invoice Amount','Taxed Amount',
					'Taxname','Tax%','Tax Amount',
					'SubTax1','Sub Tax1%', 'Sub Tax1 Amount', 
					'SubTax2','Sub Tax2%', 'Sub Tax2 Amount',
					'Total Amount');
					$fields_cnt =count($heading1);
					
					$taxname_list=array();
					$condition_queryst= " WHERE id='".$tax1_id."'";
					ServiceTax::getList($db, $taxname_list, 'id,tax_name', $condition_queryst);
					
					$taxname='';
					if(!empty($taxname_list)){
						$taxname_list = $taxname_list[0];
						$taxname = $taxname_list['tax_name'];
					}
					 
					//$bill_type_name = ($bill_type==1)? '	Goods 	':' Services' ;
					$bill_type_name='';
					//First Part Display BOF
					$taxname_str ='';
					if(!empty($taxname)){
						$taxname_str =' Tax - '.$taxname ;
					}else{
						$taxname_str = 'Without any tax';
					}
					$mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results for Period '.date('d M Y',strtotime($dfa)) .' - '.date('d M Y',strtotime($dta)).' '.$taxname_str.' ')) . $csv_enclosed;
					 
					$schema_insert .= $mHead1;
					$schema_insert .= $csv_separator;
					$schema_insert .= $csv_terminated;
					
					$mHead2 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes( $taxname.' Particulars of the Bills')). $csv_enclosed;
					$schema_insert .= $mHead2;
					$schema_insert .= $csv_separator;
					$schema_insert .= $csv_terminated;;
					for ($i = 0; $i < $fields_cnt; $i++)
					{
						$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes($heading1[$i])) . $csv_enclosed;
						$schema_insert .= $l;
						$schema_insert .= $csv_separator;
					} // end for
				 
					$out1 = trim(substr($schema_insert, 0, -1));
					$out1 .= $csv_terminated;
					$out1 .= $schema_insert1 ;
					
					//Row show the grand total bof
					//$grant_total1 = number_format($grant_total1,2);
					/*$granttotstr = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes("Grand Total is ".$grant_total1)) . $csv_enclosed;
					$granttotstr .= $csv_separator;
					$granttotstr = trim(substr($granttotstr, 0, -1));
					$out1 .=$granttotstr ;
					$out1 .= $csv_terminated;
					*/
					//Row show the grand total EOF
					
					//First Part Display EOF
					//Second Part Display BOF
					$taxname_str ='';
					if(!empty($taxname)){
						$taxname_str =' Non - '.$taxname ;
					}else{
						$taxname_str = 'Without tax';
					}					 
					$heading2=array('Sr No','Invoice No','Invoice Date','Invoice Amount',$taxname_str.' Amount');
					$fields_cnt2 =count($heading2);
					$mHead3 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes(' '.$taxname_str.' Particulars of the Bills')). $csv_enclosed;
					$schema_insert_nt .= $mHead3;
					$schema_insert_nt .= $csv_separator;
					$schema_insert_nt .= $csv_terminated;;
					for ($i = 0; $i < $fields_cnt2; $i++)
					{
						$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
							stripslashes($heading2[$i])) . $csv_enclosed;
						$schema_insert_nt .= $l;
						$schema_insert_nt .= $csv_separator;
					} // end for
				 
					$out2 = trim(substr($schema_insert_nt, 0, -1));
					$out2 .= $csv_terminated;
					$out2 .= $schema_insert2 ;
					//Second Part Display EOF
					$out=$out1.$out2;
					$filename = date('dmY').".csv";
					header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
					header("Content-Length: " . strlen($out));
					// Output to browser with appropriate mime type, you choose ;)
					header("Content-type: text/x-csv");
					//header("Content-type: text/csv");
					//header("Content-type: application/csv");
					header("Content-Disposition: attachment; filename=$filename");
					echo $out;
					//Write CSV EOF
					exit;
				}
			
		}
	}

    $page["var"][] = array('variable' => 'tax_list', 'value' => 'tax_list');      
    $page["var"][] = array('variable' => 'list', 'value' => 'list');     
    $page["var"][] = array('variable' => 'ntlist', 'value' => 'ntlist');
    $page["var"][] = array('variable' => 'taxname', 'value' => 'taxname');
	$page["var"][] = array('variable' => 'show', 'value' => 'show');   	
	$page["var"][] = array('variable' => 'totalSum', 'value' => 'totalSum');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
 
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-tax-bills.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
   
?>