<?php
    if ( $perm->has('nc_site_menu_add') ) {
		$site_id	= isset($_GET["site_id"]) 	? $_GET["site_id"]	: ( isset($_POST["site_id"]) 	? $_POST["site_id"] : SITE_ID );
	
		//Menus can be added.
		//include_once ( FS_CLASS ."/Validation.class.php");
		$_ALL_POST	=	NULL;	
		if ( isset($_GET) && !empty($_GET) ) {
			$_ALL_POST = $_GET;
		}
		
		//read Websites
		$list_sites		= NULL;
		getAllSites( $db, $list_sites);
	
		$al_list    = getAccessLevel($db, $my['access_level']);
		
		//read static pages
		$list_static_pages		= NULL;
		getStaticPages( $db, $list_static_pages, $site_id);
		$condition = NULL;
	
		// Fetch all the parent menu
		$menu_placement 		= MenuSetting::getPlacements('title');
		$menu_type 				= MenuSetting::getType();
		$menu_status['key'] 	= MenuSetting::getStatus();
		$menu_status['value'] 	= MenuSetting::getDisplayStatus();
		$menu_existing			= NULL;
		$menu_array				= NULL;
	
		//MenuSetting::getAllMenus($db, $menu_existing, $site_id);
		/*********BOF commented on 07-july-2009 because sub menu is not getting visible*************/
        //MenuSetting::getMenus($db, $menu_existing, $site_id);
        /*********EOF commented on 07-july-2009 because sub menu is not getting visible*************/
        MenuSetting::getMenus($db, $menu_existing);
		if ( is_array($menu_existing) && count($menu_existing)>0 ) {
			foreach ( $menu_placement as $key=>$placement ) {
				if ( array_key_exists($key, $menu_existing) ) {
					array_traverse($menu_array[$placement], $menu_existing[$key]);
				}
				else {
					$menu_array[$placement] = NULL;
				}
			}
		}
		
		
		if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
			$_ALL_POST		= $_POST;
			$menu_detail	= processUserData($_ALL_POST);
			
			$extra = array( 'db' 				=> &$db,
							'messages'  		=> $messages,
							'al_list'   		=> $al_list,
							'menu_existing' 	=> $menu_existing,
							'list_sites' 		=> $list_sites,
							'list_static_pages' => $list_static_pages,
							'menu_placement'	=> $menu_placement,
							'menu_status'		=> $menu_status,
						);
	
	
			if( isset($menu_detail["group_id"]) && $menu_detail["group_id"] != "0" ){
				// If the Parent Menu is selected, then retrieve the Placement 
				// of the Parent Menu to be applied to the Current Menu.
				$condition = " WHERE id = '". $menu_detail["group_id"] ."'";
								//." AND id != '". $m_id ."'";
				$parent = NULL;
				if ( MenuSetting::getList( $db, $parent, 'placement', $condition) ) {
					$menu_detail["placement"] = $parent[0]['placement'];
				}
				else {
					// Parent is not valid.
					$messages->setErrorMessage('The selected Parent Menu is invalid.');
				}
			}
	
			if (MenuSetting::validateAdd($menu_detail, $messages, $extra)) {
				
				if( $menu_detail["type"] == "S" ){
					$menu_detail["page"] = $menu_detail["page_s"];
				}
				elseif( $menu_detail["type"] == "D" ){
					$menu_detail["page"] = $menu_detail["page_d"];
				}
				elseif( $menu_detail["type"] == "E" ){
					$menu_detail["page"] = $menu_detail["page_e"];
				}
				
				$query	= " INSERT INTO ".TABLE_MENU
										." SET " 		.TABLE_MENU .".group_id		= '". $menu_detail["group_id"] ."'"
													.",". TABLE_MENU .".site_id		= '". $menu_detail["site_id"] . "'"
													.",". TABLE_MENU .".title		= '". $menu_detail["title"] ."'" 
													.",". TABLE_MENU .".placement	= '". $menu_detail["placement"] ."'"
													.",". TABLE_MENU.".type			= '". $menu_detail["type"] ."'"
													.",". TABLE_MENU .".page		= '". $menu_detail["page"] ."'"
													.",". TABLE_MENU .".menu_order	= '". $menu_detail["order"] ."'"
													.",". TABLE_MENU .".status		= '". $menu_detail["status"] ."'"
													.",". TABLE_MENU .".access_level= '". $menu_detail["access_level"] ."'"
													.",". TABLE_MENU .".date		= '". date("Y-m-d H:i:s", time())."'";
												
				if ($db->query($query) && $db->affected_rows() > 0) {
					$messages->setOkMessage('The Menu has been Created.');
					$_ALL_POST		= NULL;
					$menu_data		= NULL;
					$menu_existing	= NULL;
					$menu_array		= NULL;
					MenuSetting::getMenus($db, $menu_existing, $site_id);
					if ( is_array($menu_existing) && count($menu_existing)>0 ) {
						foreach ( $menu_placement as $key=>$placement ) {
							if ( array_key_exists($key, $menu_existing) ) {
								array_traverse($menu_array[$placement], $menu_existing[$key]);
							}
							else {
								$menu_array[$placement] = NULL;
							}
						}
					}
					
				}
				else {
					$messages->setErrorMessage('The Menu was not added, please try again.');
				}
			}
		}
		
	
		// Check if the Form to add is to be displayed or the control is to be sent to the List page.
		if ( isset($_POST['btnCancel'])
			|| (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$_GET = $_POST = '';
			include ( DIR_FS_NC .'/menu-settings-list.php');
		}
		else {
			$_ALL_POST['site_id'] = $site_id;
			
			$hidden[] = array('name'=> 'perform' , 'value' => 'add');
			$hidden[] = array('name'=> 'act' , 'value' => 'save');
			$hidden[] = array('name'=> 'x' , 'value' => $x);
			$hidden[] = array('name'=> 'rpp' , 'value' => $rpp);
			
		
			$page["var"][] = array('variable' => 'site_id', 'value' => 'site_id');
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
			$page["var"][] = array('variable' => 'list_sites', 'value' => 'list_sites');
			$page["var"][] = array('variable' => 'list_static_pages', 'value' => 'list_static_pages');
			$page["var"][] = array('variable' => 'menu_array', 'value' => 'menu_array');
			$page["var"][] = array('variable' => 'menu_status', 'value' => 'menu_status');
			$page["var"][] = array('variable' => 'menu_type', 'value' => 'menu_type');
			$page["var"][] = array('variable' => 'menu_placement', 'value' => 'menu_placement');
			$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'menu-settings-add.html');
		}
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }	
?>