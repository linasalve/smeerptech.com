<?php
      if ( $perm->has('nc_sms_api_delete') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );        
        $access_level = $my['access_level'];
        
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        SmsSender::delete($id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/sms-sender-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the entry.");
        
    }
?>
