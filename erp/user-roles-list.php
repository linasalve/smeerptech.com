<?php
    if ( $perm->has('nc_urol_list') ) {
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';
        }
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched'] = 1;
        // To count total records.
        $list	= 	NULL;
        $total	=	UserRoles::getList( $db, $list, '', $condition_query);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');

        $list   = NULL;
        UserRoles::getList( $db, $list, 'id, title, description, access_level, status', $condition_query, $next_record, $rpp);
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-roles-list.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>