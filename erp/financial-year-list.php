<?php
if ( $perm->has('nc_fy_list') ) {

   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	FinancialYear::getDetails( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_FINANCIAL_YEAR.'.*';
    FinancialYear::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    $fList=array();
		if(!empty($list)){
			foreach( $list as $key=>$val){      
				
				$company=$company_name='';
				$company=$company_prefix='';
				$table = TABLE_SETTINGS_COMPANY;
				$condition1 = " WHERE ".TABLE_SETTINGS_COMPANY .".id= '".$val['company_id'] ."' " ;
				$fields1 =  TABLE_SETTINGS_COMPANY .'.name ,'.TABLE_SETTINGS_COMPANY .'.prefix';
				$company= getRecord($table,$fields1,$condition1);
			
				if(!empty($company)){
				   
				  $company_name = $company['name'];
				  $company_prefix = $company['prefix'];
				}
				$val['company']    = $company_name ;  
				$val['company_prefix']    = $company_prefix ;    
			   
			   
			   $fList[$key]=$val;
			   
			}
		}
   
    // Set the Permissions.
   
    if ( $perm->has('nc_fy_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_fy_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_fy_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_fy_delete') ) {
        $variables['can_delete'] = true;
    }    
    if ( $perm->has('nc_fy_status') ) {
        $variables['can_change_status']     = true;
     }
     
     
     
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'financial-year-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
