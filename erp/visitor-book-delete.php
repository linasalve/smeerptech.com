<?php
    if ( $perm->has('nc_vb_delete') ) {
	    $visitor_id = isset($_GET["visitor_id"]) ? $_GET["visitor_id"] : ( isset($_POST["visitor_id"]) ? $_POST["visitor_id"] : '' );
       
        $access_level = $my['access_level'];
        /*if ( $perm->has('nc_bl_or_delete_al') ) {
            $access_level += 1;
        }
        */
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        Visitor::delete($visitor_id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/visitor-book-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the Visitor.");
    }
?>
