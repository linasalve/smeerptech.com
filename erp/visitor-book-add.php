<?php
    if ( $perm->has('nc_vb_add') ) {
        //$po_id          = isset($_GET["po_id"]) ? $_GET["po_id"] : ( isset($_POST["po_id"]) ? $_POST["po_id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }*/
		 //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs['00'] = 'Hr';
        for($i=1; $i<=12;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min['00'] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        }        
        $lst_typ =array('1'=>'AM','2'=>'PM');
		$_ALL_POST['date']= date('d/m/Y');
        //echo  $date1 =date('Y-m-d',time());
		// Include the visitor book class.
		include_once (DIR_FS_INCLUDES .'/visitor-book.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            $data['in_time'] =$data['intime_hrs'].":".$data['intime_min']." ".$data['intime_type'];
            $data['out_time'] =$data['outtime_hrs'].":".$data['outtime_min']." ".$data['outtime_type'];
           
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Visitor::validateAdd($data, $extra) ) {
				
				$important = 0;
				if(isset($data['important'])){
					$important=1;
				}
                $query	= " INSERT INTO ".TABLE_VISITOR_BOOK
						." SET ". TABLE_VISITOR_BOOK .".name = '".            $data['name'] ."'"
						.",". TABLE_VISITOR_BOOK .".company_name = '".        $data['company_name'] ."'"
						.",". TABLE_VISITOR_BOOK .".contact_no = '".          $data['contact_no'] ."'"
						.",". TABLE_VISITOR_BOOK .".access_level = '".        $my['access_level'] ."'"
						.",". TABLE_VISITOR_BOOK .".address = '".             $data['address'] ."'"
						.",". TABLE_VISITOR_BOOK .".created_by = '".          $data['created_by'] ."'"                                
						.",". TABLE_VISITOR_BOOK .".important = '".           $important ."'"
						.",". TABLE_VISITOR_BOOK .".purpose_id = '".          $data['purpose_id'] ."'"
						.",". TABLE_VISITOR_BOOK .".purpose = '".             $data['purpose'] ."'"
						.",". TABLE_VISITOR_BOOK .".in_time = '".             $data['in_time'] ."'"
						.",". TABLE_VISITOR_BOOK .".out_time = '".            $data['out_time'] ."'"
						.",". TABLE_VISITOR_BOOK .".attend_by = '".           $data['attend_by'] ."'"
						.",". TABLE_VISITOR_BOOK .".date = '".                $data['date'] ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Visitor entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/visitor-book.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/visitor-book.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/visitor-book.php?added=1");   
        }
        else {
			$variables['purposelist'] = Visitor::getPurposeList();
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'lst_typ', 'value' => 'lst_typ');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'visitor-book-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
                
?>
