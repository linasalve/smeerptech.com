<?php

    if ( $perm->has('nc_ld_or_list') || $perm->has('nc_ld_or_search') ) {
		include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
       
		// Status
		$variables["negative"]           = LeadsOrder::NEGATIVE;
		$variables["active"]            = LeadsOrder::ACTIVE;
		$variables["pending"]           = LeadsOrder::PENDING;
		$variables["assign"]           = LeadsOrder::ASSIGN;
		$variables["completed"]         = LeadsOrder::COMPLETED;
		$variables["processed"]         = LeadsOrder::PROCESSED;
        ///$variables["followup_type"]     = FOLLOWUP::ORDER;
        $access_level = $my['access_level'] + 1;
	   
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';			 
            $condition_query = " WHERE ("
                                   ." ". TABLE_LEADS_ORDERS .".status IN( '". LeadsOrder::ACTIVE ."',
								   '".LeadsOrder::ASSIGN."')"
                                .")";
			$_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = array(LeadsOrder::ACTIVE,LeadsOrder::ASSIGN);
        }
     
        if ( $perm->has('nc_ld_or_list_all') ) { 
		 
		 
		}else{
		
			// If the Order is created by the my. 
			// If my has created this Order.
			/* 
			if ( $perm->has('nc_bl_or_list_al') ) {
				$access_level += 1;
			}
			 
			$condition_query .= " (". TABLE_LEADS_ORDERS .".created_by = '". $my['user_id'] ."' "
									." AND ". TABLE_LEADS_ORDERS .".access_level < $access_level ) ";
			
			// If my is present in the Team.
			$condition_query .= " OR ( ( "
											. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
										. " ) "
										." AND ". TABLE_LEADS_ORDERS .".access_level < $access_level ) "; 
			
			// If my is the Client Manager
			$condition_query .= " OR ( ". TABLE_SALE_LEADS .".manager = '". $my['user_id'] ."'"
								." AND ". TABLE_LEADS_ORDERS .".access_level < $access_level ) ";
								
			*/
			
			if(empty($_SEARCH["search_executive_id"]) || empty($_SEARCH["user_id"]) ){
				$condition_query .= " AND ( ";
				
					$condition_query .= " (". TABLE_LEADS_ORDERS .".created_by = '". $my['user_id'] ."' "."  ) ";
					// If my is present in the Team.
					/* $condition_query .= " OR ( "
											. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
											. TABLE_LEADS_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
										. "  "
										."  ) ";  */
										
					$condition_query .= " OR ( "
											. TABLE_LEADS_ORDERS .".team LIKE '%,". $my['user_id'] .",%' "
										. "  "
										."  ) "; 
				// Check if the User has the Right to view Orders created by other Users.
				$condition_query .= " )";   				
				$condition_url  .= "&user_id=$user_id";
				$_SEARCH["user_id"]  = $user_id;
			}    
		}


		if( $sOrderBy=='delay'){
			$condition_query .= " ORDER BY  DATEDIFF( CURDATE() , ".TABLE_LEADS_ORDERS.".do_c )  ". $sOrder ;        
		}else{
			$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;        
		}
		
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
        //By default search in On
        $_SEARCH["searched"]    = true ;
        $regionlead     = new RegionLead('91');
        $phonelead      = new PhoneLead(TABLE_SALE_LEADS);
       
        if($searchStr==1){
          
            $fields = TABLE_LEADS_ORDERS.'.id ' ;
            $total	=	LeadsOrder::getDetails( $db, $list, '', $condition_query);
            $extra_url  = '';
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            $list	= NULL;
            $fields = TABLE_LEADS_ORDERS .'.id'
                    .','. TABLE_LEADS_ORDERS .'.number'
                    .','. TABLE_LEADS_ORDERS .'.order_title'
                    .','. TABLE_LEADS_ORDERS .'.access_level'
                    .','. TABLE_LEADS_ORDERS .'.team'
                    .','. TABLE_LEADS_ORDERS .'.details'
                    .','. TABLE_LEADS_ORDERS .'.do_d'
                    .','. TABLE_LEADS_ORDERS .'.do_c'
                    .','. TABLE_LEADS_ORDERS .'.do_o'
                    .','. TABLE_LEADS_ORDERS .'.status'
                    .','. TABLE_LEADS_ORDERS .'.followup_status'
                    .','. TABLE_LEADS_ORDERS .'.cron_created '
                    .','. TABLE_LEADS_ORDERS .'.new_ref_ord_id'
                    .','. TABLE_LEADS_ORDERS .'.new_ref_ord_no'
                    .','. TABLE_LEADS_ORDERS .'.order_against_id'
                    .','. TABLE_LEADS_ORDERS .'.order_against_no'
                    .','. TABLE_LEADS_ORDERS .'.invoice_against_id'
                    .','. TABLE_LEADS_ORDERS .'.invoice_against_no'
                    .','. TABLE_LEADS_ORDERS .'.is_old' //For Old Migrated data 
                    .','. TABLE_LEADS_ORDERS .'.old_updated' //For Old Migrated data
                    .','. TABLE_LEADS_ORDERS .'.old_particulars'
                    .',	  DATEDIFF( CURDATE() , '.TABLE_LEADS_ORDERS.'.do_c ) AS delay'
                    //.','. TABLE_LEADS_QUOTATION .'.number as inv_no'
                    .','. TABLE_SALE_LEADS .'.lead_id AS c_user_id'
                    .','. TABLE_SALE_LEADS .'.number AS c_number'
                    .','. TABLE_SALE_LEADS .'.f_name AS c_f_name'
                    .','. TABLE_SALE_LEADS .'.l_name AS c_l_name'
					.','. TABLE_SALE_LEADS .'.billing_name'
                    .','. TABLE_SALE_LEADS .'.email AS c_email'
                    .','. TABLE_SALE_LEADS .'.email_1 AS email_1'
                    .','. TABLE_SALE_LEADS .'.email_2 AS email_2'
                    .','. TABLE_SALE_LEADS .'.mobile1 AS mobile1'
                    .','. TABLE_SALE_LEADS .'.mobile2 AS mobile2'
                    .','. TABLE_SALE_LEADS .'.mobile3 AS mobile3'       
                    .','. TABLE_SALE_LEADS .'.status AS c_status';
             LeadsOrder::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        
        
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               $val['team']=trim($val['team'],",");
                $team = str_replace(',',"','",$val['team']);
               $is_invoice_created = 0;			   
			   $sql= " SELECT COUNT(*) as count FROM ".TABLE_LEADS_QUOTATION." WHERE 
			   ".TABLE_LEADS_QUOTATION.".or_no='".$val['number']."' AND 
			   ".TABLE_LEADS_QUOTATION.".status !='".LeadsQuotation::DELETED."' LIMIT 0,1";
			   $db->query($sql);               
			   if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$count = $db->f('count') ;
						if( $count > 0){
							$is_invoice_created = 1;
						}
					}
				}
				
				$sql= " SELECT f_name,l_name FROM ".TABLE_USER." WHERE 
				".TABLE_USER.".user_id IN ('".$team."') ";
                $db->query($sql);               
				$team_members ='';
                if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$team_members .= $db->f('f_name')." ". $db->f('l_name').", <br/>" ;
						 
					}
                }   
				$team_members =trim($team_members,",");
				$val['team_members']=$team_members ;
				
                $phonelead->setPhoneOf(TABLE_SALE_LEADS, $val['c_user_id']);
				$val['phone'] = $phonelead->get($db);
				
				// Read the Addresses.
				$regionlead->setAddressOf(TABLE_SALE_LEADS, $val['c_user_id']);
				$val['address_list'] = $regionlead->get();
			
				//check order id followup in ST BOF
				$val['ticket_id']=0;
				$sql2 = "SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
				(".TABLE_LD_TICKETS.".flw_ord_id = '".$val['id']."') AND ".TABLE_LD_TICKETS.".ticket_child =0  LIMIT 0,1";
				
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                        }
                    }                   
                }
				//check order id followup in ST EOF
				
				
                $is_particulars_selected = 0; 
                $sql= " SELECT id FROM ".TABLE_LEADS_ORD_P." WHERE 
				".TABLE_LEADS_ORD_P.".ord_no='".$val['number']."' LIMIT 0,1";
                $db->query($sql);               
                if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$count = $db->f('id') ;
						if( $count > 0){
							$is_particulars_selected = 1;
						}
					}
                }                
                $val['is_invoice_created'] = $is_invoice_created;   
                $val['is_particulars_selected'] = $is_particulars_selected;                   
                $flist[$key]=$val;
            }
        }
        
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
		$variables['can_view_list_all'] = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_create_invoice']= false;
        $variables['can_followup'] = false;
        $variables['can_bifurcate'] = false;
		$variables['can_view_inv'] = false;
        
        if ( $perm->has('nc_ld_or_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_ld_or_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_ld_or_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_ld_or_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_ld_or_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_ld_or_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_ld_qt_add') ) {
            $variables['can_create_invoice'] = true;
        }
        		
		if ( $perm->has('nc_ldt') && $perm->has('nc_ldt_flw_ord') ) {
            $variables['can_followup'] = true;
        }
		if( $perm->has('nc_ld_qt') && $perm->has('nc_ld_qt_list') ) {
            $variables['can_view_inv']     = true;
        }
		if ( $perm->has('nc_ld_or_list_all') ) {
			$variables['can_view_list_all'] = true;
		}
		
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-order-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>