<?php

    if ( $perm->has('nc_nwl_qlist') ) {
    
		// Status
        
        
       
        
       
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;
        $condition = " WHERE ". TABLE_NEWSLETTER_TEMP .".batches_remaining != ''"
					." AND ". TABLE_NEWSLETTER_TEMP .".failed = '0'".$condition_query;
                    
                    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched'] = 1;
        $_SEARCH['search'] = 'qsearch' ;
        
        // To count total records.
        $list	= 	NULL;
        $fields = TABLE_NEWSLETTER_TEMP.'.temp_news_id ' ;
        $total	=	Newsletter::getListQueue( $db, $list,'', $condition);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
              
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
       
       
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields = TABLE_NEWSLETTER_TEMP .'.n_id'
                    .','. TABLE_NEWSLETTER_TEMP .'.temp_news_id'
                    .','. TABLE_NEWSLETTER_TEMP .'.message'
                    .','. TABLE_NEWSLETTER_TEMP .'.subject'
                    .','. TABLE_NEWSLETTER_TEMP .'.sendAs'      ;
        Newsletter::getListQueue( $db, $list, $fields, $condition, $next_record, $rpp);
        
        /*
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){     
                
                $flist[$key]=$val;
            }
        }
        */
        
        
        // Set the Permissions.
        $variables['can_view_list']      = false;
        $variables['can_view_qlist']     = false;     
        $variables['can_qdelete']         = false;
        $variables['can_add']           = false;
        
        if ( $perm->has('nc_nwl_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_nwl_qlist') ) {
            $variables['can_view_qlist'] = true;
        }
       if ( $perm->has('nc_nwl_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_nwl_qdelete') ) {
            $variables['can_qdelete'] = true;
        }
       
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletter-queue-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>