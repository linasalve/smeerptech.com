<?php
    if ( $perm->has('nc_bl_or_up_delete') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        /*$access_level = $my['access_level'];
        if ( $perm->has('nc_e_delete_al') ) {
            $access_level += 1;
        }*/
        
        $extra = array( 'db'           => &$db,
                        'messages'     => &$messages
                        );
        BillOrderUpload::delete($id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/bill-order-upload-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the entry.");
    }
?>
