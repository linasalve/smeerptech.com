<?php
    if ( $perm->has('nc_bl_or_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/company.inc.php');
        include_once ( DIR_FS_INCLUDES .'/quotation.inc.php');
        include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php');
        include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
        
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $po_id          = isset($_GET["po_id"]) ? $_GET["po_id"] : ( isset($_POST["po_id"]) ? $_POST["po_id"] : '' );
        $is_inv_cr          = isset($_GET["is_inv_cr"]) ? $_GET["is_inv_cr"] : ( isset($_POST["is_inv_cr"]) ? $_POST["is_inv_cr"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }
        include_once (DIR_FS_INCLUDES .'/user.inc.php');
        // list of executives to assign project manager
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields);
        
        //Get company list 
        $lst_company = null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
        
        // Read the available source
       	Leads::getSource($db,$source);
        
        //code to generate $vendorOptionList BOF
        $vendorSql ="SELECT * FROM ".TABLE_VENDOR ;
        $db->query( $vendorSql );
        $vendorList['0'] = "" ;
        if( $db->nf() > 0 )
		{
            while($db->next_record())
			{
                $vendorList[$db->f('id')] = $db->f('f_name')." ".$db->f('l_name') ;
            }
        }
        //code to generate $vendorOptionList EOF
        
        
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);
        //Read Order dEtails BOF  
         if($messages->getErrorMessageCount() <= 0){
       
            // Read the Order which is to be Updated.
            $fields = TABLE_BILL_ORDERS .'.*'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.status AS c_status';
            
            $condition_query = " WHERE (". TABLE_BILL_ORDERS .".id = '". $or_id ."' "
                                    ." OR ". TABLE_BILL_ORDERS .".number = '". $or_id ."')";

            /*$condition_query .= " AND ( ";
    
            
            $condition_query .= " (". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // If my is present in the Team (Remove if Team Member cannot edit the Order).
            $condition_query .= " OR ( ( "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
                                        . " ) "
                                        ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // If my is the Client Manager (Remove if Client manager cannot edit the Order)
            $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // Check if the User has the Right to Edit Orders created by other Users.
            if ( $perm->has('nc_bl_or_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_bl_or_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_BILL_ORDERS. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level_o ) ";
            }
            $condition_query .= " )";
            */
            
            /*
            $condition_query .= " AND ( ". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";

            // Check if the User has the Right to edit Orders created by other Users.
            if ( $perm->has('nc_bl_or_edit_ot') ) {
                $access_level   = $my['access_level'];
                if ( $perm->has('nc_bl_or_edit_ot_al') ) {
                    $access_level += 1;
                }
                $condition_query .= " OR ( ". TABLE_BILL_ORDERS. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            }
            */
            
          
            
            if ( Order::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                
                $_ALL_POST = $_ALL_POST['0'];
              
                
               
                //Check whether is_invoice_created created or not
                
                
                //if ( $_ALL_POST['access_level'] < $access_level ) {
                    // Set up the Client Details field.
                    $_ALL_POST['client_details'] = $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
                                                    .' ('. $_ALL_POST['c_number'] .')'
                                                    .' ('. $_ALL_POST['c_email'] .')';
                    
                    
                    //include ( DIR_FS_INCLUDES .'/user.inc.php');
                    // Read the Order Creator's Information. No need to show Creator on Edit Form.
                   //$_ALL_POST['creator']= '';
                   //User::getList($db, $_ALL_POST['creator'], 'number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                   //$_ALL_POST['creator'] = $_ALL_POST['creator'][0];
                
                    // Read the Team Members Information.
                    $_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
                    $temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
                    $_ALL_POST['team_members']  = '';
                    $_ALL_POST['team_details']  = array();
                    User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
                    $_ALL_POST['team'] = array();
                    foreach ( $_ALL_POST['team_members'] as $key=>$members) {
                    //for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
                        $_ALL_POST['team'][] = $members['user_id'];
                        $_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
                    }
                    //date of order
                    $_ALL_POST['do_o']  = explode(' ', $_ALL_POST['do_o']);
                    $temp               = explode('-', $_ALL_POST['do_o'][0]);
                    $_ALL_POST['do_o']  = NULL;
                    $_ALL_POST['do_o']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    // Setup the date of delivery.
                    $_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
                    $temp               = explode('-', $_ALL_POST['do_d'][0]);
                    $_ALL_POST['do_d']  = NULL;
                    $_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    $po_id              = $_ALL_POST['po_id'];
                    
                    // Setup the start date and est date
                    if( $_ALL_POST['st_date'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['st_date']  ='';
                    }else{
                        $_ALL_POST['st_date']  = explode(' ', $_ALL_POST['st_date']);
                        $temp               = explode('-', $_ALL_POST['st_date'][0]);
                        $_ALL_POST['st_date']  = NULL;
                        $_ALL_POST['st_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    // Setup the start date and est date
                    
                    if( $_ALL_POST['es_ed_date'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['es_ed_date']  ='';
                    }else{
                        $_ALL_POST['es_ed_date']  = explode(' ', $_ALL_POST['es_ed_date']);
                        $temp               = explode('-', $_ALL_POST['es_ed_date'][0]);
                        $_ALL_POST['es_ed_date']  = NULL;
                        $_ALL_POST['es_ed_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    if( $_ALL_POST['do_e'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['do_e']  ='';
                    }else{
                        $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                        $temp               = explode('-', $_ALL_POST['do_e'][0]);
                        $_ALL_POST['do_e']  = NULL;
                        $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    if( $_ALL_POST['do_fe'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['do_fe']  ='';
                    }else{
                        $_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
                        $temp               = explode('-', $_ALL_POST['do_fe'][0]);
                        $_ALL_POST['do_fe']  = NULL;
                        $_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    //Get Quotation files BOF            
                     if( !empty($_ALL_POST['quotation_no'])){
                        $rejectedFiles = array();                       
                        Quotation::allRejectedQuotation($db,$_ALL_POST['parent_q_id'],$rejectedFiles);
                    }                     
                    
                    
                    //Get Quotation files EOF
                    
                    //if order_closed_by is empty 
                    
                    if(empty($_ALL_POST['order_type'])){
                        $_ALL_POST['order_type'] = Order::TARGETED ;
                    }
                    if(empty($_ALL_POST['order_closed_by'])){
                        $_ALL_POST['order_closed_by'] = 'e68adc58f2062f58802e4cdcfec0af2d' ;
                    }
                    
                     // Set up the Services.
                    //$_ALL_POST['service_id'] = explode(',', $_ALL_POST['service_id']);
                   
                    // Read the Particulars.
                    $temp_p = NULL;
                    $condition_query_p ='';
                    $service_idArr= array();
                    $condition_query_p = " WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $_ALL_POST['number'] ."'";
                    Order::getParticulars($db, $temp_p, '*', $condition_query_p);
                    foreach ( $temp_p as $pKey => $parti ) {
                        
                        $_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];
                        $_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
                        $_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];
                        $_ALL_POST['s_type'][$pKey]       = $temp_p[$pKey]['s_type'];
                        $_ALL_POST['s_quantity'][$pKey]       = $temp_p[$pKey]['s_quantity'];
                        $_ALL_POST['s_amount'][$pKey]       = $temp_p[$pKey]['s_amount'];
                        $_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
                        $_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
                        $_ALL_POST['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];
                        $_ALL_POST['ss_title'][$pKey]   = $temp_p[$pKey]['ss_title'];
                        $_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];
                        $_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
                        $_ALL_POST['tax1_value'][$pKey]   = $temp_p[$pKey]['tax1_value'];
                        $_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
                        $_ALL_POST['is_renewable'][$pKey]   = $temp_p[$pKey]['is_renewable'];
                        $_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
                        $_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
                        $_ALL_POST['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
                        $_ALL_POST['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
                        $_ALL_POST['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
                        $_ALL_POST['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];
                        $service_idArr[] = $temp_p[$pKey]['s_id'];
                    }
                    $_ALL_POST['service_id'] = $service_idArr;
                   
                    $_ALL_POST['po_filename']=$_ALL_POST['po_filename'];
                    $_ALL_POST['filename_1']=$_ALL_POST['filename_1'];
                    $_ALL_POST['filename_2']=$_ALL_POST['filename_2'];
                    $_ALL_POST['filename_3']=$_ALL_POST['filename_3']; 
                    
                  
                //}
                //else {
                    //$messages->setErrorMessage("You do not have the Permission to view the Order with the current Access Level.");
               // }
            }
            else {
                $messages->setErrorMessage("The Order was not found or you do not have the Permission to access this Order.");
            }
            //get order details eof
        
        }        
         // Read the Services.on the basis of order renewable or not
        include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC";
        Services::getList($db, $lst_service, 'ss_id,ss_title,ss_punch_line, tax1_name, tax1_value,is_renewable ', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
                    $ss_id= $key['ss_id'];
                    $ss_title= $key['ss_title'];
                    $ss_punch_line= $key['ss_punch_line'];
                    $tax1_name = $key['tax1_name'];
                    $tax1_value = $key['tax1_value'];
                    $tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
                    $is_renewable = $key['is_renewable'];
                    $keyNew['ss_id'] = $ss_id ; 
                    $keyNew['ss_title'] = $ss_title ; 
                    $keyNew['ss_punch_line'] = $ss_punch_line ; 
                    $keyNew['tax1_name'] = $tax1_name ; 
                    $keyNew['tax1_value'] = $tax1_value ; 
                    $keyNew['tax1_pvalue'] = $tax1_pvalue ; 
                    $keyNew['is_renewable'] = $is_renewable ; 
                    $lst_service[$val] = $keyNew;
            }
        }
        //Read Order dEtails EOF  
      
       
       
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            $files       = processUserData($_FILES);
            
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_MULTIPLE_FILE_SIZE     ;
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'lst_service'       => $lst_service,
                            'messages'          => &$messages
                        );
            $data['do_c'] = time();
            if ( Order::validateUpdate($data, $extra) ) {
                
                $po_file_sql="";
                $file1_sql="";
                $file2_sql="";
                $file3_sql="";
                  
                if(!empty($data['po_filename'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['po_filename']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "PO-".$data['number'].".".$ext ;
                    $attachfilename = "PO-".mktime().".".$ext ;
                    $data['po_filename'] = $attachfilename;
                    $_ALL_POST['po_filename'] = $attachfilename;
                    if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_po_filename']))
                    {
                        @unlink( DIR_FS_ORDER_FILES."/".$data['old_po_filename']);
                    }
                    if (move_uploaded_file ($files['po_filename']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                        @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                    $po_file_sql = ",". TABLE_BILL_ORDERS .".po_filename = '". $attachfilename ."'";
                    $_ALL_POST['po_filename']=$attachfilename;
                }else{
                    $_ALL_POST['po_filename']=$_ALL_POST['old_po_filename'];
                }
                
                if(!empty($data['filename_1'])){
                    $filedata["extension"]=$attachfilename='';
                    $filedata = pathinfo($files['filename_1']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                   // $attachfilename = "WO1-".$data['number'].".".$ext ;
                    $attachfilename = "WO1-".mktime().".".$ext ;
                    $data['filename_1'] = $attachfilename;
                    if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_1'])){
                        @unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_1']);
                    }
                    if (move_uploaded_file ($files['filename_1']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                           @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                    $file1_sql=",". TABLE_BILL_ORDERS .".filename_1      = '". $attachfilename ."'" ;
                    $file1_sql.=",". TABLE_BILL_ORDERS .".filecaption_1      = '". $data['filecaption_1'] ."'";
                    $_ALL_POST['filename_1']=$attachfilename;
                }elseif (!empty($data['old_filename_1'])){
                    $file1_sql=",". TABLE_BILL_ORDERS .".filecaption_1      = '". $data['filecaption_1'] ."'";
                    $_ALL_POST['filename_1']=$_ALL_POST['old_filename_1'];
                }
                
                if(!empty($data['filename_2'])){
                    $filedata["extension"]=$attachfilename='';
                    $filedata = pathinfo($files['filename_2']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "WO2-".$data['number'].".".$ext ;
                    $attachfilename = "WO2-".mktime().".".$ext ;
                    $data['filename_2'] = $attachfilename;
                    if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_2']))
                    {
                         @unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_2']);
                    }
                    
                    if (move_uploaded_file ($files['filename_2']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                          @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                    $file2_sql=",". TABLE_BILL_ORDERS .".filename_2      = '". $attachfilename ."'";
                    $file2_sql.=",". TABLE_BILL_ORDERS .".filecaption_2      = '". $data['filecaption_2'] ."'";
                    $_ALL_POST['filename_2']=$attachfilename;
                }elseif (!empty($data['old_filename_2'])){
                    $file2_sql=",". TABLE_BILL_ORDERS .".filecaption_2      = '". $data['filecaption_2'] ."'";
                    $_ALL_POST['filename_2']=$_ALL_POST['old_filename_2'];
                }
                
                if(!empty($data['filename_3'])){
                    $filedata["extension"] = $attachfilename='';
                    $filedata = pathinfo($files['filename_3']["name"]);                   
                    $ext = $filedata["extension"] ;                    
                    //$attachfilename = "WO3-".$data['number'].".".$ext ;
                    $attachfilename = "WO3-".mktime().".".$ext ;
                    $data['filename_3'] = $attachfilename;
                    if(file_exists(DIR_FS_ORDER_FILES."/".$data['old_filename_3']))
                    {
                        @unlink( DIR_FS_ORDER_FILES."/".$data['old_filename_3']);
                    }
                    
                    if (move_uploaded_file ($files['filename_3']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                          @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                    $file3_sql = ",". TABLE_BILL_ORDERS .".filename_3      = '". $data['filename_3'] ."'";
                    $file3_sql.= ",". TABLE_BILL_ORDERS .".filecaption_3      = '". $data['filecaption_3'] ."'";
                    $_ALL_POST['filename_3']=$attachfilename;
                }elseif (!empty($data['old_filename_3'])){
                    $file3_sql=",". TABLE_BILL_ORDERS .".filecaption_3      = '". $data['filecaption_3'] ."'";
                    $_ALL_POST['filename_3']=$_ALL_POST['old_filename_3'];
                }
                
                if($data['isrenewable']){
                
                  $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                  $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                }
                
                
                $query  = " UPDATE ". TABLE_BILL_ORDERS
                            ." SET ". TABLE_BILL_ORDERS .".number           = '". $data['number'] ."'"
                                .",". TABLE_BILL_ORDERS .".po_id            = '". $data['po_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".access_level     = '". $data['access_level'] ."'"
                                .",". TABLE_BILL_ORDERS .".client           = '". $data['client']['user_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".created_by       = '". $data['created_by'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_title	    = '". $data['order_title'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_domain	    = '". $data['order_domain'] ."'"
                                .",". TABLE_BILL_ORDERS .".currency_id      = '". $data['currency_id'] ."'"
                                .$po_file_sql
                                .$file1_sql
                                .$file2_sql
                                .$file3_sql
                                .",". TABLE_BILL_ORDERS .".amount           = '". $data['amount'] ."'"
                                .",". TABLE_BILL_ORDERS .".existing_client	= '". $data['existing_client'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_type	    = '". $data['order_type'] ."'"
                                .",". TABLE_BILL_ORDERS .".source_to        = '". $data['source_to'] ."'"
                                .",". TABLE_BILL_ORDERS .".source_from      = '". $data['source_from'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_closed_by	= '". $data['order_closed_by'] ."'"
                                .",". TABLE_BILL_ORDERS .".team             = '". implode(",", $data['team']) ."'"
                                .",". TABLE_BILL_ORDERS .".old_particulars  = '". $data['old_particulars'] ."'"
                                .",". TABLE_BILL_ORDERS .".old_updated  = '". $data['old_updated'] ."'"
                                .",". TABLE_BILL_ORDERS .".details          = '". $data['details'] ."'"
                                //.",". TABLE_BILL_ORDERS .".do_c             = '". date('Y-m-d H:i:s', $data['do_c']) ."'"
                                .",". TABLE_BILL_ORDERS .".do_d             = '". date('Y-m-d H:i:s', $data['do_d']) ."'"
                                .",". TABLE_BILL_ORDERS .".do_e             = '". $do_e."'"
                                .",". TABLE_BILL_ORDERS .".do_fe            = '". $do_fe."'"
                                .",". TABLE_BILL_ORDERS .".project_manager  = '". $data['project_manager']."'"
                                .",". TABLE_BILL_ORDERS .".st_date          = '". $data['st_date']."'"
                                .",". TABLE_BILL_ORDERS .".es_ed_date       = '". $data['es_ed_date']."'"
                                .",". TABLE_BILL_ORDERS .".es_hrs           = '". $data['es_hrs']."'"
                               // .",". TABLE_BILL_ORDERS .".status = '".         $data['status'] ."'" 
                            ." WHERE id = '". $or_id ."'";
                
                
                if ( $db->query($query)  ) {
                    $messages->setOkMessage("Order has been updated.");
                    $variables['hid'] = $or_id;
                    //Particulars edit bof 
                   
                    if(isset($data['p_id'])){
                        foreach ( $data['p_id'] as $key=>$pid ) {
                            if(!empty($data['p_id'][$key])){
                                if(!isset($data['particulars'][$key]) || empty($data['particulars'][$key]) ){
                                   //delete pid
                                   //Get amount from TABLE_BILL_ORD_P and update in                                    
                                    $query1 = "DELETE FROM ". TABLE_BILL_ORD_P
                                                ." WHERE id = '".$data['p_id'][$key]."'";
                                    $db->query($query1);
                                
                                }else{
                                
                                    $do_e=$do_fe ='';
                                    if($data['is_renewable'][$key] =='1'){
                                              $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                                              $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                                              $isrenewable = true;
                                    }
                                    $data['particulars'][$key] = trim($data['particulars'][$key],',');
                                   
                                    //Get vendor name BOF                                     
                                        $data['vendor_name'][$key]='';
                                        if(!empty($data['vendor'][$key])){
                                            $vendorSql ="SELECT f_name,l_name FROM ".TABLE_VENDOR." WHERE id='".$data['vendor'][$key]."'" ;
                                            $db->query( $vendorSql );
                                            if( $db->nf() > 0 )
                                            {
                                                while($db->next_record()){			
                                                    $data['vendor_name'][$key] = $db->f('f_name')." ".$db->f('l_name')  ;
                                                }
                                            }
                                        }
                                    //Get vendor name EOF
                                   
                                    $query2  = " UPDATE ". TABLE_BILL_ORD_P
                                    ." SET ". TABLE_BILL_ORD_P .".particulars = '".   processUserData($data['particulars'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".p_amount = '".        $data['p_amount'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_id = '".        $data['s_id'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".sub_s_id = '".        $data['sub_s_id'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_type = '".        $data['s_type'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_quantity = '".        $data['s_quantity'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_amount = '".        $data['s_amount'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".ss_title = '".     processUserData($data['ss_title'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".ss_punch_line	= '".  processUserData($data['ss_punch_line'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_name	= '".   processUserData($data['tax1_name'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_value	= '".   processUserData($data['tax1_value'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_pvalue	= '". processUserData($data['tax1_pvalue'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".d_amount	= '".    processUserData($data['d_amount'][$key]) ."'"                                
                                    .",". TABLE_BILL_ORD_P .".stot_amount = '".   processUserData($data['stot_amount'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".tot_amount = '".   processUserData($data['tot_amount'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".discount_type = '". processUserData($data['discount_type'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".pp_amount = '". processUserData($data['pp_amount'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".vendor = '". processUserData($data['vendor'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".vendor_name = '". processUserData($data['vendor_name'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".purchase_particulars = '".processUserData($data['purchase_particulars'][$key]) ."'"                            
                                    .",". TABLE_BILL_ORD_P .".is_renewable = '".$data['is_renewable'][$key]."'"                            
                                    .",". TABLE_BILL_ORD_P .".do_e = '".$do_e."'"                            
                                    .",". TABLE_BILL_ORD_P .".do_fe = '".$do_fe."'"                            
                                    ." WHERE id = '". $data['p_id'][$key] ."'" ;
                                    
                                    $db->query($query2);
                                }
                              
                            }else{
                                //insert
                                if(!empty($data['particulars'][$key])){
                                    $do_e=$do_fe ='';
                                    if($data['is_renewable'][$key] =='1'){
                                              $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                                              $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                                              $isrenewable = true;
                                    }
                                
                                    $data['particulars'][$key] = trim($data['particulars'][$key],',');
                                   //Get vendor name BOF                                     
                                        $data['vendor_name'][$key]='';
                                        if(!empty($data['vendor'][$key])){
                                            $vendorSql ="SELECT f_name,l_name FROM ".TABLE_VENDOR." WHERE id='".$data['vendor'][$key]."'" ;
                                            $db->query( $vendorSql );
                                            if( $db->nf() > 0 )
                                            {
                                                while($db->next_record()){			
                                                    $data['vendor_name'][$key] = $db->f('f_name')." ".$db->f('l_name')  ;
                                                }
                                            }
                                        }
                                    //Get vendor name EOF
                                    
                                     $query3  = " INSERT INTO ". TABLE_BILL_ORD_P
                                    ." SET ". TABLE_BILL_ORD_P .".particulars = '".   processUserData($data['particulars'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".ord_no = '".        $data['number']  ."'"
                                    .",". TABLE_BILL_ORD_P .".p_amount = '".        $data['p_amount'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_id = '".        $data['s_id'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".sub_s_id = '".        $data['sub_s_id'][$key]  ."'"
                                     .",". TABLE_BILL_ORD_P .".s_type = '".        $data['s_type'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_quantity = '".        $data['s_quantity'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".s_amount = '".        $data['s_amount'][$key]  ."'"
                                    .",". TABLE_BILL_ORD_P .".ss_title = '".     processUserData($data['ss_title'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".ss_punch_line	= '".  processUserData($data['ss_punch_line'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_name	= '".   processUserData($data['tax1_name'][$key])."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_value	= '".   processUserData($data['tax1_value'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".tax1_pvalue	= '". processUserData($data['tax1_pvalue'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".d_amount	= '".    processUserData($data['d_amount'][$key]) ."'"                                
                                    .",". TABLE_BILL_ORD_P .".stot_amount	= '".    processUserData($data['stot_amount'][$key]) ."'"                                
                                    .",". TABLE_BILL_ORD_P .".tot_amount = '".   processUserData($data['tot_amount'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".discount_type = '". processUserData($data['discount_type'][$key])  ."'"
                                    .",". TABLE_BILL_ORD_P .".pp_amount = '". processUserData($data['pp_amount'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".vendor = '". processUserData($data['vendor'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".vendor_name = '". processUserData($data['vendor_name'][$key]) ."'"
                                    .",". TABLE_BILL_ORD_P .".purchase_particulars = '".processUserData($data['purchase_particulars'][$key]) ."'"                            
                                    .",". TABLE_BILL_ORD_P .".is_renewable = '".$data['is_renewable'][$key]."'"                            
                                    .",". TABLE_BILL_ORD_P .".do_e = '".$do_e."'"                            
                                    .",". TABLE_BILL_ORD_P .".do_fe = '".$do_fe."'"     ;
                                   $db->query($query3);  
                                }                                   
                            }
                        }
                    }
               
                    //Particulars edit eof 
                    
                    // Mark the Pre Order as PROCESSED.
                    /*
                    if ( !(Order::setPreOrderStatus($db, $data['po_id'], Order::COMPLETED)) ) {
                        $messages->setErrorMessage("Pre Order was not marked as Processed.");
                    }
                    $po_id = NULL;
                    */
              
                    // Send the notification mails to the concerned persons.
                    /*
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_or_edit', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_or_edit_al', 'access_level'=>($data['access_level']-1) ),
                                                            //array('right'=>'nc_bl_in_add', 'access_level'=>$data['access_level']),
                                                            //array('right'=>'nc_bl_in_add_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );

                    // Organize the data to be sent in email.
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    $data['updater']= array('f_name'=> $my['f_name'],
                                            'l_name'=> $my['l_name'],
                                            'number'=> $my['number']);
                                            
                    // Read the Client Manager information.
                    include ( DIR_FS_INCLUDES .'/clients.inc.php');
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');

                    
                    // Send mail to the Concerned Executives with same access level and higher access levels
                    if($data['mail_exec_ac']){ 
                  
                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_MANAGERS', $data, $email) ) {
                                $to     = '';
                              
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }
                    // Send Email to the Client.
                      
                    if($data['mail_client']){
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_CLIENT', $data, $email) ) {
                            $to     = '';
                           
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                    
                    // Send Email to the Client Manager / creator email .
                    if($data['mail_client_mgr']){ 
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_CLIENT_MANAGER', $data, $email) ) {
                            $to     = '';
                           
                            $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                                     
                    // Send Email to Team Members.
                    if($data['mail_executive']){    
                        // For Speed notification is sent in one email to all the team members.
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_TEAM', $data, $email) ) {
                            $to     = '';
                            foreach ( $data['team_members'] as $key=>$executive) {
                                $to[]   = array('name' => $executive['f_name'] .' '. $executive['l_name'], 'email' => $executive['email']);
                            }
                           
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        // Send Email to the Creator of the Order.
                    }
                    */
                    /*
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'NOT REQUIRED, SO NOT DEFINED', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    */
                    
                   
                    
                    
                }
               
            }
        }
       
       
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $or_id;
            
            include ( DIR_FS_NC .'/bill-order-list.php');
        }
        else {
            
           

            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['po_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['po_access_level'], $al_list, $index);
                $_ALL_POST['po_access_level'] = $al_list[$index[0]]['title'];
            }
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'or_id', 'value' => $or_id);
            $hidden[] = array('name'=> 'po_id', 'value' => $po_id);
            $hidden[] = array('name'=> 'is_inv_cr', 'value' => $is_inv_cr);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit_o');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //print_r($_ALL_POST);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'is_inv_cr', 'value' => 'is_inv_cr');
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
            $page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["var"][] = array('variable' => 'source', 'value' => 'source');
            $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-edit-old.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>