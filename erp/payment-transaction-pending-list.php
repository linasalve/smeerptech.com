<?php

if ( $perm->has('nc_p_pt_pending_list') || $perm->has('nc_p_pt_pending_search')) {  
    
 
    // If the task is created by the my.
    $access_level   = $my['access_level'];
    
    $show	= isset($_GET["show"])         ? $_GET["show"]        : ( isset($_POST["show"])          ? $_POST["show"]       :''); 
    
    //***************EOF**********
	
    if ( empty($show )) {
        $where_added=true;
        $condition_query = " WHERE ".TABLE_PAYMENT_TRANSACTION .".status = '". Paymenttransaction::PENDING ."'
			AND ".TABLE_PAYMENT_TRANSACTION .".f_status = '". Paymenttransaction::FORCEOPEN ."'" ;
        
        
        $_SEARCH["chk_status"]  = 'AND';
        $_SEARCH["sStatus"]    = array(Paymenttransaction::PENDING);
        $_SEARCH["chk_fstatus"]  = 'AND';
		$_SEARCH["fStatus"]    = array(Paymenttransaction::FORCEOPEN);
		
        $perform = 'search';
        $condition_url   = "&chk_status=".$_SEARCH["chk_status"]."&sStatus=".Paymenttransaction::PENDING."
			&chk_fstatus=".$_SEARCH["chk_fstatus"]."&fStatus=".Paymenttransaction::FORCEOPEN ;   
    }
    //***************BOF**********
    
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;

    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    $extra_url  = '';    
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }        
    $condition_url .="&perform=".$perform."&show=1&rpp=$rpp";
    //echo $condition_query;
    $fList =$plist	= $list	= array();
	$total=$total_pending=0;
	$pagination='';
	
	if($searchStr==1){
		//count total Pending records bof
		$plist	= 	NULL;    
		$condition_query_pending = " WHERE ".TABLE_PAYMENT_TRANSACTION.".status='".Paymenttransaction::PENDING."'" ;
		$total_pending	=	Paymenttransaction::getDetails( $db, $plist, '', $condition_query_pending);
		//count total Pending records eof
			
		// To count total records.
		$list	= 	NULL;    
		$total	=	Paymenttransaction::getDetails( $db, $list, '', $condition_query);  
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
		   
		$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  = '&start=url'. $extra_url .'&end=url';

		$list	= NULL;
		$fields = TABLE_PAYMENT_TRANSACTION .".*,"
		. TABLE_PAYMENT_ACCOUNT_HEAD .".account_head_name, " . TABLE_PAYMENT_MODE .".payment_mode ,"
		. TABLE_USER .".f_name as efname,".TABLE_USER .".l_name as elname,"
		.TABLE_CLIENTS .".f_name as cfname,".TABLE_CLIENTS .".l_name as clname,".TABLE_CLIENTS .".billing_name as cbilling_name ,"
		//.TABLE_VENDORS .".f_name as fname, ". TABLE_VENDORS .".l_name as lname, ". TABLE_VENDORS .".billing_name,"
		.TABLE_VENDORS_BANK .".f_name as vfname, ". TABLE_VENDORS_BANK .".l_name as vlname, ".TABLE_VENDORS_BANK .".billing_name 
		as vbilling_name";		
		Paymenttransaction::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
		$fList=array();
		$statusArr = Paymenttransaction::getStatusAll();
		$statusArr = array_flip($statusArr);
		
		if(!empty($list)){
			foreach( $list as $key=>$val){        
		   
			   //$string = str_replace(",","','", $val['allotted_to']);
			   $fields1 =  TABLE_AUTH_USER .'.first_name'.','. TABLE_AUTH_USER .'.last_name';
			   if(!empty($string)){
				$condition1 = " WHERE ".TABLE_AUTH_USER .".user_id IN('".$string."') " ;
			   }          
			   $val['rowspan']=$bankname ='';
			   $val['status_name'] = $statusArr[$val['status']];
			   if(!empty($val['ref_credit_pay_bank_id'])){
					$banknameArr=array();
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['ref_credit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$val['ref_credit_pay_bank_name'] =  $banknameArr['bank_name'] ;                    
					}
					
			    }
			    if(!empty($val['ref_debit_pay_bank_id'])){
					$banknameArr=array();
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['ref_debit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$val['ref_debit_pay_bank_name'] =  $banknameArr['bank_name'] ;                    
					}
					
			    }
			   //get bank name on the basis of type IN/OUT
				if( $val['transaction_type'] ==Paymenttransaction::PAYMENTIN ){
			   
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['credit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$bankname = $banknameArr['bank_name'] ;                    
					}
					$val['transaction_typename']='Payment In';
				}elseif($val['transaction_type'] ==Paymenttransaction::PAYMENTOUT){
			   
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['debit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$bankname = $banknameArr['bank_name'] ;                    
					}
					 $val['transaction_typename']='Payment Out';
				}elseif($val['transaction_type'] ==Paymenttransaction::INTERNAL){
			   
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['debit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$debitbankname = $banknameArr['bank_name'] ;
					}                
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['credit_pay_bank_id'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr)){
						$creditbankname = $banknameArr['bank_name'] ;   
					}
					$val['transaction_typename']='Internal';
					//$val['rowspan']=3;
				}
				$val['default_trdt']='';
				/*
				if ($val['do_transaction']=='0000-00-00 00:00:00' && $val['status']== Paymenttransaction::PENDING){
					$val['do_transaction']= date("Y-m-d h:i:s");
					$val['default_trdt']='Default Date';
				}*/
				
				if($val['transaction_type'] == Paymenttransaction::INTERNAL){
					$val['debitbankname'] = $debitbankname ;           
					$val['creditbankname'] = $creditbankname ;  
				}else{
					$val['bankname'] = $bankname ;           
				}

				$val['pay_bank_company_name']='';
				if(!empty($val['pay_bank_company'])){
					
					$table = TABLE_PAYMENT_BANK;
					$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$val['pay_bank_company'] ."' " ;
					$fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
					$banknameArr1 = getRecord($table,$fields1,$condition2);
					if(!empty($banknameArr1)){
						$val['pay_bank_company_name'] = $banknameArr1['bank_name'] ;                    
					}
				}
				$val['auto_entry']='';       
				if($val['is_auto_entry']=='1'){
					$val['auto_entry']='Auto Reversal';               
				}
				//check transaction id followup in VST BOF FOR (VENDORS)
					$val['vticket_id']='';
					$sql2 = " SELECT ticket_id FROM ".TABLE_VENDORS_ST_TICKETS." WHERE 
					".TABLE_VENDORS_ST_TICKETS.".transaction_id = '".$val['id']."' LIMIT 0,1";
					if ( $db->query($sql2) ) {
						if ( $db->nf() > 0 ) {
							while ($db->next_record()) {
							   $val['vticket_id']= $db->f('ticket_id');
							}
						}                   
					}
				//check invoice id followup in ST EOF
				$val['attachment_imap_arr'] = array();
				if(!empty($val['attachment_imap'])){
					$val['attachment_imap_arr']=explode(",",$val['attachment_imap']);
				}
			    $val['pay_received_amount']=round($val['pay_received_amt'],2);
			    $val['pay_received_amt'] = number_format($val['pay_received_amt'],2);
			    $val['pay_received_amt_inr'] = number_format($val['pay_received_amt_inr'],2);
			    $val['tr_cr_bank_amt'] = number_format($val['tr_cr_bank_amt'],2);
			    $val['tr_db_bank_amt'] = number_format($val['tr_db_bank_amt'],2);
				if( $key%2==0 ){
					$val['class']= 'odd';
				}else{
					$val['class']= 'even';
				}
				$fList[$key]=$val;
			}
		}
	}
    // Set the Permissions.
    $variables['can_view_list'] = false;    
    $variables['can_add'] = false;
    $variables['can_edit'] = false; 
    $variables['can_allot_bills'] =false;
 
    $variables['can_print'] = false;
    
     
    if ( $perm->has('nc_p_pt_pending_list') ) {
        $variables['can_view_list'] = true;
    } 
     if ( $perm->has('nc_p_pt') && $perm->has('nc_p_pt_print') ) {
        $variables['can_print'] = true;
    } 
    if ( $perm->has('nc_p_pt_pending_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_p_pt_pending_edit') ) {
        $variables['can_edit'] = true;
    } 
    if ( $perm->has('nc_p_pt_bl') ) {
        $variables['can_allot_bills'] = true;
    }
	
	
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');         
    $page["var"][] = array('variable' => 'total_pending', 'value' => 'total_pending');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-pending-list.html');
    
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
   
}
?>
