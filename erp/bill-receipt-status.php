<?php
	if ( $perm->has('nc_bl_rcpt_status') ) {
        $rcpt_id	= isset($_GET["rcpt_id"]) ? $_GET["rcpt_id"] 	: ( isset($_POST["rcpt_id"]) ? $_POST["rcpt_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        if ( $perm->has('nc_bl_rcpt_status_al') ) {
            $access_level += 1;
        }
		
		$access_level_ot = 0;
        if ( $perm->has('nc_bl_rcpt_status_ot') ) {
            $access_level_ot = $my['access_level'];
			if ( $perm->has('nc_bl_rcpt_status_ot_al') ) {
				$access_level_ot += 1;
			}
        }
        
        $extra = array( 'db'           		=> &$db,
                        'my' 				=> $my,
						'access_level' 		=> $access_level,
						'access_level_ot' 	=> $access_level_ot,
                        'messages'     		=> &$messages
                    );
        Receipt::updateStatus($rcpt_id, $status, $extra);
        
		$variables['hid'] = $rcpt_id;
		$perform = 'search';
        $searchStr = 1;
        $action = "Receipt List";		 
		if(!empty($rcpt_id)){
			$condition_query = " WHERE ".TABLE_BILL_RCPT.".id='".$rcpt_id."'";
		}
        // Display the  list.
        include ( DIR_FS_NC .'/bill-receipt-list.php');
        //This is changed b'coz it giving error if we change the status 2009-03-mar-27 BOF
        //$perform = 'search';
        //include ( DIR_FS_NC .'/bill-receipt-search.php');
        //This is changed b'coz it giving error if we change the status 2009-03-mar-27 EOF
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>