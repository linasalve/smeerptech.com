<?php
    if ( $perm->has('nc_pin_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_pin_edit_al') ) {
            $access_level += 1;
        }
        
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Paymentin::validateUpdate($data, $extra) ) {
                $query  = " UPDATE ". TABLE_PAYMENT_IN
                             ." SET ". TABLE_PAYMENT_IN .".payment_from = '".            $data['payment_from'] ."'"
                                .",". TABLE_PAYMENT_IN .".amount = '".                  $data['amount'] ."'"
                                .",". TABLE_PAYMENT_IN .".mode = '".                    $data['mode'] ."'"
                                .",". TABLE_PAYMENT_IN .".received_by = '".             $data['received_by'] ."'"
                                .",". TABLE_PAYMENT_IN .".pay_purpose = '".             $data['pay_purpose'] ."'"
                                .",". TABLE_PAYMENT_IN .".date = '".                     $data['date'] ."'"
                            ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_PAYMENT_IN .'.*'  ;            
            $condition_query = " WHERE (". TABLE_PAYMENT_IN .".id = '". $id ."' )";
            $condition_query .= " AND ( ";
            // If my has created this record.
            $condition_query .= " ( ". TABLE_PAYMENT_IN .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_PAYMENT_IN .".access_level <= $access_level ) ";              
            
            // Check if the User has the Right to Edit created by other Users.
           /*
            if ( $perm->has('nc_ab1_edit_osst') ) {          
                       
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_ab1_edit_ot_al') ) {
                         
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_COMPLAIN_BOX. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_COMPLAIN_BOX .".access_level < $access_level_o ) ";
            }*/
            $condition_query .= " ) ";
          
            if ( Paymentin::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
               
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
             
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            include ( DIR_FS_NC .'/payment-in-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-in-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>