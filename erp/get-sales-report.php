<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    error_reporting(0);
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php');     
	include_once ( DIR_FS_INCLUDES .'/prospects-quotation.inc.php');	
	include_once ( DIR_FS_INCLUDES .'/prospects.inc.php');	
	include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
    include_once ( DIR_FS_NC ."/header.php"); 


    $casetxt = isset($_GET['casetxt']) ? $_GET['casetxt'] : '';
    $executive = isset($_GET['executive']) ? $_GET['executive'] : '';
    $date_from = isset($_GET['fdt']) ? $_GET['fdt'] : '';
    $date_to = isset($_GET['tdt']) ? $_GET['tdt'] : '';
     
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
   
    if ( !empty($executive) && !empty($date_from) && !empty($date_to) ){
        header("Content-Type: application/json");      
	    $dfa = explode('/', $date_from); 
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] ;
		$dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0];     
		$dateArr = getDateArray($dfa,$dta,"d M Y D",'wos'); 
		//$dateArr['dates'] $dateArr['total_sundays']
		//print_R($dateArr['dates']);
		
		if($casetxt=='prospects'){
			$sql = " SELECT COUNT(user_id) as  prospects, date_format(".TABLE_PROSPECTS.".do_add,'%Y-%m-%d') as do_add  FROM ".TABLE_PROSPECTS." 
				WHERE ".TABLE_PROSPECTS.".created_by='".$executive."' AND ".TABLE_PROSPECTS.".parent_id='' AND
				date_format(".TABLE_PROSPECTS.".do_add,'%Y-%m-%d') >= '".$dfa."' AND 
				date_format(".TABLE_PROSPECTS.".do_add,'%Y-%m-%d') <= '".$dta."' GROUP BY date_format(".TABLE_PROSPECTS.".do_add,'%Y-%m-%d')";  
			$db->query($sql); 
			$total_prospects=0;
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>Added date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ){ 
					$prospects =$db->f('prospects');
					$total_prospects= $total_prospects + $prospects  ;
					$do_add = $db->f('do_add');			
					$do_add0 = date('d M Y D',strtotime($do_add));	
					$do_add1 = date('d M Y D',strtotime($do_add));	
					$w = date('w',strtotime($do_add));	
					$style ='';
					if($w==0){
						$style ="style='background-color:#cecece;'";
					}
					$pdates[] = $do_add0 ;
					echo "<tr><td ".$style.">".$do_add1."</td><td>".$prospects."</td></tr>";
				}
				$arr = array_diff($dateArr['dates'],$pdates); //get dates WithOutSunday
				 
				$dates_not_worked = implode(", ",$arr);
				$total_working_days = count($dateArr['dates']);
				if(!empty($dates_not_worked)){
					echo "<tr><td colspan='2'><b>Not Performed dates</b></td></tr>";
					echo "<tr><td colspan='2'>".$dates_not_worked."</td></tr>";
				}
				$avg=0;
				$avg = ($total_prospects/$total_working_days);
				if($avg>0){
					echo "<tr><td colspan='2'><b>Average Prospects (".$avg.")</b> = Total prospects (".$total_prospects.") / total working days (".$total_working_days.")</td></tr>"; 
				}
				echo "</table>";
			}
		}
		if($casetxt == 'total_activity'){ 
			$sql = " SELECT COUNT(ticket_id) as total_activity, date_format(".TABLE_PROSPECTS_TICKETS.".do_e,'%Y-%m-%d') as do_c 
					FROM ".TABLE_PROSPECTS_TICKETS." WHERE ".TABLE_PROSPECTS_TICKETS.".ticket_creator_uid = '".$executive."' 
					AND ".TABLE_PROSPECTS_TICKETS.".ticket_type ='".ProspectsTicket::TYP_REPORTING."'
					AND date_format(".TABLE_PROSPECTS_TICKETS.".do_e,'%Y-%m-%d') >= '".$dfa."'
					AND date_format(".TABLE_PROSPECTS_TICKETS.".do_e,'%Y-%m-%d') <= '".$dta."'
					GROUP BY date_format(".TABLE_PROSPECTS_TICKETS.".do_e,'%Y-%m-%d')";  
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('total_activity');			
					$do_c =$db->f('do_c');		
					$do_c = date('d M Y D',strtotime($do_c));						
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		} 
		if($casetxt=='self_leads'){  
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$executive."') AND
						date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."' GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt=='shared_leads'){
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND 
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND
						date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."' 
					GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')";  
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt=='self_hot_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$executive."')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::HOTLSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt=='shared_hot_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::HOTLSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt=='self_warm_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$executive."')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::WARMLSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'shared_warm_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::WARMLSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'self_cold_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::COLDLSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'shared_cold_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::COLDLSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'self_negative_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::NEGATIVELSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads = $db->f('leads');			
					$do_c = $db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'shared_negative_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::NEGATIVELSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'self_negative_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::NEGATIVELSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads = $db->f('leads');			
					$do_c = $db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'shared_negative_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::NEGATIVELSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'self_win_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::WINLSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads = $db->f('leads');			
					$do_c = $db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			} 
		}
		if($casetxt == 'shared_win_leads'){ 
			$sql = " SELECT COUNT(id) as leads,date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$executive."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::WINLSTATUS."'
						GROUP BY date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'total_self_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c 	FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$executive."')
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
						('".ProspectsQuotation::PENDING."','".ProspectsQuotation::BLOCKED."','".ProspectsQuotation::DELETED."')
						GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'total_shared_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$executive."'  AND
			".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'	AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
			('".ProspectsQuotation::PENDING."','".ProspectsQuotation::BLOCKED."','".ProspectsQuotation::DELETED."')
			GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'total_self_block_proposals'){   
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$executive."') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."' AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::BLOCKED."') 
			GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'total_shared_block_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$executive."'  AND
			".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'	AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
			('".ProspectsQuotation::BLOCKED."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'total_self_cancelled_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c 	FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$executive."') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."' AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
			AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::DELETED."') 
			GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'total_shared_cancelled_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$executive."'  AND
			".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'	AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
			('".ProspectsQuotation::DELETED."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		 
		
		if($casetxt == 'pending_self_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c 	FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$executive."') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."' AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
			AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."') 
			GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'pending_shared_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$executive."'  AND
			".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'	AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
			('".ProspectsQuotation::PENDING."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ) {
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		if($casetxt == 'approved_self_proposals'){   
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c 	FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$executive."') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."' AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
			AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."') 
			AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::APPROVED."')
			GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'approved_shared_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$executive."'  AND
			".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'	AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
			('".ProspectsQuotation::PENDING."') AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::APPROVED."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'self_pending_for_approvals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c 	FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$executive."') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."' AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
			AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."') AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::SEND_FOR_APPROVAL."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'shared_pending_for_approvals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$executive."'  AND
			".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'	AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
			('".ProspectsQuotation::PENDING."') AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::SEND_FOR_APPROVAL."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		//NotSendForApproval
		if($casetxt == 'nsfa_self_proposals'){  
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c 	FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$executive."') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."' AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
			AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."') AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::NONEAPPROVAL."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'nsfa_shared_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$executive."'  AND
			".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'	AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
			('".ProspectsQuotation::PENDING."') AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::NONEAPPROVAL."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		 
		if($casetxt == 'disapproved_self_proposals'){  
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c 	FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$executive."') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."' AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
			AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."') AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::DISAPPROVED."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		}
		
		if($casetxt == 'disapproved_shared_proposals'){ 
			$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as leads, date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%d %M %Y') as do_c FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id	WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$executive."'  AND
			".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$executive.",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'	AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."' AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
			('".ProspectsQuotation::PENDING."') AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::DISAPPROVED."') GROUP BY date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d')"; 
			$db->query($sql); 
			if ( $db->nf()>0 ){
				echo "<table cellpadding='5' cellspacing='0' border='0' width='100%' 
						style='border-collapse:collapse; border:1px solid #d8d8d8;'>";
				echo "<tr><td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>date</td>
				<td style='font-family: arial,sans-serif; font-size: 12px; line-height: 12px; font-weight: normal; border-collapse:collapse; border:1px solid #d8d8d8;'>count</td></tr>";
				while ( $db->next_record() ) { 
					$leads =$db->f('leads');			
					$do_c =$db->f('do_c');			
					echo "<tr><td>".$do_c."</td><td>".$leads."</td></tr>";
				}
				echo "</table>";
			}
		} 
		
		
		 
 
		 
		
	}
include_once( DIR_FS_NC ."/flush.php");


?>
