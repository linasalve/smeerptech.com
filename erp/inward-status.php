<?php
	if ( $perm->has('nc_inward_status') ) {
        $id	= isset($_GET["id"]) ? $_GET["id"] 	: ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

             
        $extra = array( 'db'           		=> &$db,
                       'messages'     		=> &$messages
                    );
        Inward::updateStatus($id, $status, $extra);        
        
        $perform = 'list';
        include ( DIR_FS_NC .'/inward-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>