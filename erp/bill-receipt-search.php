<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    $searchStr =0;
    
    if ( $sString != "" ) 
    {
        $searchStr = 1;
        // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
       
        //$where_added = true;
        $sub_query='';        
        if ( $search_table == "Any" ) 
        {
            
            if ( $where_added ) 
            {
                $condition_query .= "OR ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ) 
            {
              
                if ( $table != "Any" ) 
                { 
                    
                    if ($table == TABLE_BILL_RCPT) 
                    {
                        $sub_query .= "( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if(clearSearchString($field,"-") !='number' && clearSearchString($field,"-") !='inv_no' && clearSearchString($field,"-") !='id' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                            
                    }elseif($table == TABLE_BILL_ORDERS){
					
						$sub_query .= "( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            //if($field !='number' && $field !='or_no' && $field !='id' ){
                              //  $sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                            //}else{
                              //    $sub_query .= $table .".". clearSearchString($field) ." = '".trim($sString)."' OR " ;
                            //}
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
					
					
					}elseif($table == TABLE_BILL_RCPT_P){
                        $sub_query .= "( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            //if($field !='number' && $field !='or_no' && $field !='id' ){
                              //  $sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                            //}else{
                              //    $sub_query .= $table .".". clearSearchString($field) ." = '".trim($sString)."' OR " ;
                            //}
                            $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    }elseif($table == TABLE_USER){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                             $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            /*
                            if( clearSearchString($field,"-") !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_CLIENTS){
                    
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                           $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if( clearSearchString($field,"-") !='number'){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                           
                           
                            //$sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }
                    
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= " AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                 $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                /*
                if(clearSearchString($sType,"-") !='number' && clearSearchString($sType,"-") !='inv_no' && clearSearchString($sType,"-") !='id' ){
                    $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                }else{
                     $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ."='".trim($sString) ."' )" ;
                }*/
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}  
    //Client serach bof
	$search_client = isset($_POST["search_client"])   ? $_POST["search_client"]  : (isset($_GET["search_client"])   ? $_GET["search_client"]   :'');
	$search_client_details = isset($_POST["search_client_details"])   ? $_POST["search_client_details"]  : (isset($_GET["search_client_details"])   ? $_GET["search_client_details"]   :'');
	
	if(!empty($search_client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_BILL_RCPT .".client ='". $search_client ."' ";        
        $condition_url          .= "&search_client_details=$search_client_details&search_client=$search_client";
        $_SEARCH["search_client_details"]  = $search_client_details;
        $_SEARCH["search_client"]     = $search_client;       
	
	}
	//Client serach eof
	//BO : Company
    $sCompIdStr='';
    $chk_cmp = isset($_POST["chk_cmp"])   ? $_POST["chk_cmp"]  : (isset($_GET["chk_cmp"])   ? $_GET["chk_cmp"]   :'');
    $sCompId    = isset($_POST["sCompId"])      ? $_POST["sCompId"]     : (isset($_GET["sCompId"])      ?$_GET["sCompId"]     :'');
    if ( ($chk_cmp == 'AND' || $chk_cmp == 'OR' ) && !empty($sCompId)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_cmp;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
              
        if(is_array($sCompId)){
             $sCompIdStr = implode("','", $sCompId) ;
             $sCompIdStr1 = implode(",", $sCompId) ;
             
        }else{
            $sCompIdStr = str_replace(',',"','",$sCompId);
            $sCompIdStr1 = $sCompId ;
            $sCompId = explode(",",$sCompId) ;
            
        }
       
       $condition_query .= " ". TABLE_BILL_RCPT .".company_id  IN ('". $sCompIdStr ."') ";
       $condition_url          .= "&chk_cmp=$chk_cmp&sCompId=$sCompIdStr1";   
       $_SEARCH["chk_cmp"]  = $chk_cmp;
       $_SEARCH["sCompId"]  = $sCompId;
    } 
    //EO : Company
    // BO: Status data.
    $sStatusStr = '';
    $sStatusStrPass = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if (( $chk_status == 'AND' || $chk_status == 'OR') && isset($sStatus)){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
       if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStrPass = implode(",", $sStatus) ;
        }else{
            $sStatusStrPass =$sStatus;
            $sStatusStr = str_replace(",","','",$sStatus) ;
            $sStatus = explode(",", $sStatus) ;
        }
        
        
        //$condition_query .= " ". TABLE_BILL_RCPT .".status IN ('". implode("','", $sStatus) ."') ";
        $condition_query .= " ". TABLE_BILL_RCPT .".status IN ('".$sStatusStr."') ";
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStrPass";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
        
    }
    
    // EO: Status data.
    
	$chk_pstatus  = isset($_POST["chk_pstatus"])? $_POST["chk_pstatus"]   : (isset($_GET["chk_pstatus"])? $_GET["chk_pstatus"]:'');
	$pRcpt  = isset($_POST["pRcpt"])? $_POST["pRcpt"]   : (isset($_GET["pRcpt"])? $_GET["pRcpt"]:'');
	
	if($chk_pstatus == 'AND' && $pRcpt!='' ){
		$searchStr = 1;
		if ( $where_added ) {
            $condition_query .= " ". $chk_pstatus;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		if($pRcpt==0){
			$condition_query .= " ". TABLE_BILL_RCPT.".inv_id = '0'";	
		}
		
		if($pRcpt==1){
			$condition_query .= " ". TABLE_BILL_RCPT.".inv_id > 0";
		}
		
        $condition_url              .= "&chk_pstatus=$chk_pstatus&pRcpt=$pRcpt";
        $_SEARCH["chk_pstatus"]   = $chk_pstatus;
        $_SEARCH["pRcpt"]       = $pRcpt;
        
	}
	
	
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
 
  
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from) ){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_BILL_RCPT.".".$dt_field." >= '". $dfa ."'";
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;
    }
   
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to) ){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_BILL_RCPT.".".$dt_field." <= '". $dta ."'";
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }
   
    // EO: Upto Date
    $chk_old_update        = isset($_POST["chk_old_update"])      ? $_POST["chk_old_update"]         : (isset($_GET["chk_old_update"])      ? $_GET["chk_old_update"]      :'');
    if(isset($chk_old_update ) && $chk_old_update!=''){
        $searchStr = 1;        
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= " ". TABLE_BILL_RCPT .".old_updated = '". $chk_old_update ."' AND ".TABLE_BILL_RCPT.".is_old='1'";
         
        $condition_url          .= "&chk_old_update=$chk_old_update";
        $_SEARCH["chk_old_update"] = $chk_old_update ;
     
    }

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/bill-receipt-list.php');
?>