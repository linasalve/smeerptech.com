<?php
    if ( $perm->has('nc_bk_stmt_status') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
                
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        BankStatement::deactive($id, $extra);
        $searchStr=1;
        // Display the list.
        include ( DIR_FS_NC .'/bank-statement-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Access this module.");
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
