<?php
if ( $perm->has('nc_fq_pr_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( FqPriority::validateUpdate($data, $extra) ) {
               $query  = " UPDATE ". TABLE_FQ_PRIORITY
                            ." SET ". TABLE_FQ_PRIORITY .".priority_name            = '". $data['priority_name'] ."'"
                                    .",". TABLE_FQ_PRIORITY .".priority_color       = '". $data['priority_color'] ."'"                               
                                    .",". TABLE_FQ_PRIORITY .".priority_font_color  = '". $data['priority_font_color'] ."'"                               
                                    .",". TABLE_FQ_PRIORITY .".priority_order       = '". $data['priority_order'] ."'"                               
                                   	.",". TABLE_FQ_PRIORITY .".priority_status      = '". $data['priority_status'] ."'"                               
                            		." WHERE priority_id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Priority has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_FQ_PRIORITY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_FQ_PRIORITY .".priority_id = '". $id ."' )";
            
            if ( FqPriority::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform = 'list';
            $condition_query='';
            include ( DIR_FS_NC .'/fq-priority-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'fq-priority-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
