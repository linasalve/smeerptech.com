<?php
    if ( $perm->has('nc_bkt_add') || $perm->has('nc_bkt_flw_tra') ) {
        include_once ( DIR_FS_INCLUDES .'/st-template-category.inc.php');
        $subUserDetails=$additionalEmailArr= null;
        //$user_id = isset ($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
        $step1 = isset ($_GET['step1']) ? $_GET['step1'] : ( isset($_POST['step1'] ) ? $_POST['step1'] : '1');
        $step2 = isset ($_GET['step2']) ? $_GET['step2'] : ( isset($_POST['step2'] ) ? $_POST['step2'] : '0');
        $vendor_tra = isset ($_GET['vendor_tra']) ? $_GET['vendor_tra'] : ( isset($_POST['vendor_tra'] ) ? $_POST['vendor_tra'] : '');
		$transaction_id = isset ($_GET['transaction_id']) ? $_GET['transaction_id'] : ( isset($_POST['transaction_id'] ) 
						?	$_POST['transaction_id'] : '');
						
        $template_category = isset ($_GET['template_category']) ? $_GET['template_category'] : ( isset($_POST['template_category'] ) ? $_POST['template_category'] :'');
		
        $_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates=null;
		$_ALL_POST = isset ($_POST['perform']) ? $_POST : ( isset($_GET['perform'] ) ? $_GET : NULL);
		
		
		if(!empty($transaction_id)){
			//check if invoice id already in ST then redirect on view ST BOF
			$sql2 = " SELECT ticket_id FROM ".TABLE_VENDORS_BANK_TICKETS." WHERE 
			".TABLE_VENDORS_BANK_TICKETS.".transaction_id = '".$transaction_id."' LIMIT 0,1";
				if ( $db->query($sql2) ) {
					$ticket_id='';
					if ( $db->nf() > 0 ) {
						while ($db->next_record()) {
						   $ticket_id= $db->f('ticket_id');
						}
					}
					if($ticket_id >0){
						header("Location:".DIR_WS_NC."/bank-ticket.php?perform=traflw_view&ticket_id=".$ticket_id."&transaction_id=".$transaction_id."&vendor_tra=".$vendor_tra);
					
					}
				}
			//check if invoice id already in ST then redirect on view ST EOF
			$_ALL_POST['client']=$vendor_tra;
		}
		
		
		
		$category_list	= 	NULL;
		$condition_query1 = " WHERE ".TABLE_ST_TEMPLATE_CATEGORY.".status = '".StTemplateCategory::ACTIVE."'
		ORDER BY ".TABLE_ST_TEMPLATE_CATEGORY.".category" ;
		StTemplateCategory::getList( $db, $category_list, TABLE_ST_TEMPLATE_CATEGORY.".category,".TABLE_ST_TEMPLATE_CATEGORY.".id", $condition_query1);
        
		$stTemplates = null;
		if(!empty($template_category)){
			$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
			".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::ST.",%' AND 
			".TABLE_ST_TEMPLATE.".template_category  LIKE '%,".$template_category.",%' ORDER BY title";
			SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'
			.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
		}
    
        //BOF read the available departments
        //SupportTicket::getDepartments($db,$department);
        //EOF read the available departments
        $domains = array();
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["status"] = BankTicket::getTicketStatusList();
        $clientAuth = Clients::getAuthorization();
		$clientAuth = array_flip($clientAuth);
        if ( (isset($_ALL_POST['btnCreate']) || isset($_ALL_POST['btnReturn'])) && $_ALL_POST['act'] == 'save') {
            //$_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES); 
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ; 
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        ); 
            if($data['step1']=='1'){
                if ( BankTicket::validateMemberSelect($data, $extra) ) {
                    $step2=1;
                    $step1=0;
                    $condition_query= " WHERE user_id = '". $data['client'] ."' ";
                    $_ALL_POST = NULL;
                    if (Clients::getList($db, $_ALL_POST, 'user_id, f_name,l_name,send_mail, org,billing_name, 					additional_email,email,email_1,email_2,email_3,mobile1, 
                    mobile2,authorization_id', $condition_query) > 0 ) {
                        $_ALL_POST = $_ALL_POST[0];
                        $_ALL_POST['user_id']=$_ALL_POST['user_id'];
                        $_ALL_POST['name']=$_ALL_POST['f_name']." ".$_ALL_POST['l_name'];
                        //$_ALL_POST['email']=$_ALL_POST['email'];
                        $_ALL_POST['org']=$_ALL_POST['org'];
                        $_ALL_POST['billing_name']=$_ALL_POST['billing_name'];
                        $_ALL_POST['mobile1']=$_ALL_POST['mobile1'];
                        $_ALL_POST['mobile2']=$_ALL_POST['mobile2'];
                        $_ALL_POST['send_mail']=$_ALL_POST['send_mail'];
						 $all_additional_email = trim($_ALL_POST['additional_email'],",");
						$additionalEmailArr = explode(",",$all_additional_email);
                        $_ALL_POST['authorization_id'] = $_ALL_POST['authorization_id'];
                        $_ALL_POST['mail_client'] = '0';
                        //$_ALL_POST['mail_to_all_su'] = '1';
						$_ALL_POST['accountValid'] = 0;
						$authorization_id = trim($_ALL_POST['authorization_id'],",");
						$clientAuth = Clients::getAuthorization();
						$clientAuth = array_flip($clientAuth);
						$authorizationName='';
						if(!empty($authorization_id)){
							$authorization_id = trim($authorization_id,",");
							$authorizationArr = explode(",",$authorization_id);
							if(!empty($authorizationArr)){
								foreach($authorizationArr as $key2=>$val2){
									$authorizationName.= $clientAuth[$val2].", ";
								}
							}
						}
						$_ALL_POST['authorizationName'] =  $authorizationName;
                        if(!empty($transaction_id)){
							$isValid = in_array(Clients::ACCOUNTS,$authorizationArr);
							$_ALL_POST['accountValid'] = $isValid ;
						}
                    }
                    /*
					$followupSql= "";
					if(!empty($transaction_id)){
						$followupSql=" AND (authorization_id LIKE '%,".Clients::ACCOUNTS.",%' OR authorization_id LIKE '%,".Clients::MANAGEMENT.",%')" ;
					}					
                    Clients::getList($db, $subUserDetails, 'number,user_id, f_name, l_name,title as ctitle,send_mail, email,email_1,email_2, email_3,email_4,mobile1,mobile2,authorization_id', " WHERE parent_id = '".$data['client']."' AND service_id LIKE '%,".Clients::ST.",%' AND status='".Clients::ACTIVE."' ".$followupSql." ORDER BY f_name ");
                    if(!empty($subUserDetails_su)){
						foreach($subUserDetails_su as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetails[$key_su]= $val_su;
						}
					}               
                    //BOF read the available domains
                    BankTicket::getDomains($db,$domains,$data['client']);
                    //EOF read the available domains
					*/
					$subUserDetailsAccts=$subUserDetailsMgmts=$subUserDetailsAuth=$subUserDetailsUsr=$subUserDetailsDnd=$subUserDetailsDir=array();
					$followupSql= ""; 
					//Directors BOF
					$followupSql4 = " AND authorization_id  LIKE '%,".Clients::DIRECTOR.",%'" ;
					$suCondition4 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."' ) AND status='".Clients::ACTIVE."'  
					".$followupSql4." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsDir1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition4); 
					if(!empty($subUserDetailsDir1)){
						foreach($subUserDetailsDir1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']); 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsDir[$key_su] = $val_su; 
						}
					} 
					//Directors EOF
					//Accounts BOF
					$followupSql = " AND authorization_id  LIKE '%,".Clients::ACCOUNTS.",%'" ;
					$suCondition = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."') AND status='".Clients::ACTIVE."'  
					".$followupSql." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsAccts1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition);  
					if(!empty($subUserDetailsAccts1)){
						foreach($subUserDetailsAccts1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsAccts[$key_su] = $val_su; 
						}
					} 
					//Accounts EOF
					//MANAGEMENT BOF
					$followupSql1 = " AND authorization_id  LIKE '%,".Clients::MANAGEMENT.",%'" ;
					$suCondition1 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql1." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsMgmts1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1,mobile2,authorization_id', $suCondition1); 
					if(!empty($subUserDetailsMgmts1)){
						foreach($subUserDetailsMgmts1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']); 
							if(!empty($authorizationArr1)){ 
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsMgmts[$key_su] = $val_su; 
						}
					} 
					//MANAGEMENT EOF
					//Authorised BOF
					$followupSql2 = " AND authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%'" ;
					$suCondition2 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql2." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsAuth1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition2); 
					if(!empty($subUserDetailsAuth1)){
						foreach($subUserDetailsAuth1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsAuth[$key_su] = $val_su; 
						}
					} 
					
					//Authorised EOF
					//USER BOF
					$followupSql3 = " AND authorization_id  LIKE '%,".Clients::USER.",%'" ;
					$suCondition3 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."') 
					AND status='".Clients::ACTIVE."'  
					".$followupSql3." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsUsr1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition3); 
					if(!empty($subUserDetailsUsr1)){
						foreach($subUserDetailsUsr1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsUsr[$key_su] = $val_su; 
						}
					} 
					//USER EOF
					//DND BOF
					$followupSql4 = " AND authorization_id  LIKE '%,".Clients::DND.",%'" ;
					$suCondition4 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql4." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsDnd1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition4); 
					if(!empty($subUserDetailsDnd1)){
						foreach($subUserDetailsDnd1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsDnd[$key_su] = $val_su; 
						}
					} 
					//DND EOF
                }
            }else{
                if ( BankTicket::validateAdd($data, $extra) ) {
                    
                    //$ticket_no  =  "PT". date("ymdhi-s");
                    $ticket_no  =  BankTicket::getNewNumber($db);
                    $attachfilename=$ticket_attachment_path='';
                    if(!empty($data['ticket_attachment'])){
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['ticket_attachment']["name"]);
                       
                        $ext = $filedata["extension"] ; 
                        //$attachfilename = $username."-".mktime().".".$ext ;
                       
                        $attachfilename = $ticket_no.".".$ext ;
                        $data['ticket_attachment'] = $attachfilename;
                        
                        $ticket_attachment_path = DIR_WS_BT_FILES;
                        
                        if (move_uploaded_file ($files['ticket_attachment']['tmp_name'], DIR_FS_BT_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_BT_FILES."/".$attachfilename, 0777);
                        }
                    }
                    
                    $mail_to_all_su=0;
                    if(isset($data['mail_to_all_su']) ){
                        $mail_to_all_su=1;
                    }
                    $mail_to_additional_email=0;
                    if(isset($data['mail_to_additional_email']) ){
                        $mail_to_additional_email=1;
                    }
                    $mail_client=0;
                    if(isset($data['mail_client']) ){
                        $mail_client=1;
                    }
                    if(!isset($data['ticket_status']) ){
                        $ticket_status = BankTicket::PENDINGWITHCLIENTS;
                    }else{
                        $ticket_status = $data['ticket_status'];
                    }
                  if(empty($data['ss_id'])){		
						$data['ss_title']='';
						$data['ss_file_1']='';
						$data['ss_file_2']='';
						$data['ss_file_3']='';
					}
                    $query = "INSERT INTO "	. TABLE_VENDORS_BANK_TICKETS ." SET "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_VENDORS_BANK_TICKETS .".transaction_id         = '". $transaction_id ."', "
							. TABLE_VENDORS_BANK_TICKETS .".template_id      = '". $data['template_id'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_owner_uid  	= '". $data['ticket_owner_uid'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_owner      	= '". $data['ticket_owner'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".on_behalf      		= '". $data['on_behalf'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_creator_uid	= '". $my['uid'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_creator    	= '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_subject      = '". $data['ticket_subject'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_mail_subject = '". $data['ticket_subject'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_text       	= '". $data['ticket_text'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".last_comment_by 	  = '".$my['uid'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
							//. TABLE_VENDORS_BANK_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
							//. TABLE_VENDORS_BANK_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
							//. TABLE_VENDORS_BANK_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
							. TABLE_VENDORS_BANK_TICKETS .".mail_to_all_su      = '".$mail_to_all_su."', "
							. TABLE_VENDORS_BANK_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_status     = '".$ticket_status ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_child      = '0', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_attachment = '". $attachfilename ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_response_time = '0', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_replied        = '0', "
							. TABLE_VENDORS_BANK_TICKETS .".from_admin_panel        = '".BankTicket::ADMIN_PANEL."', "
							. TABLE_VENDORS_BANK_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".is_private           = '". $data['is_private'] ."', "
							. TABLE_VENDORS_BANK_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_VENDORS_BANK_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_VENDORS_BANK_TICKETS .".do_comment    = '".date('Y-m-d H:i:s')."', "
							. TABLE_VENDORS_BANK_TICKETS .".last_comment_from    = '".BankTicket::ADMIN_COMMENT."', "
							. TABLE_VENDORS_BANK_TICKETS .".ticket_date       = '". time() ."' " ;
                    
                    //unset($GLOBALS["flag"]);
                    //unset($GLOBALS["submit"]);
                                
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        
                        $variables['hid'] = $db->last_inserted_id() ;
                        
                        /**************************************************************************
                        * Send the notification to the ticket owner and the Administrators
                        * of the new Ticket being created
                        **************************************************************************/
                        $data['ticket_no']    =   $ticket_no ;
                        /*foreach( $status as $key=>$val){                           
                            if ($data['ticket_status'] == $val['status_id'])
                            {
                                $data['status']=$val['status_name'];
                            }
                        }*/
                        
                        $data['domain']   = THIS_DOMAIN;                        
                        //$data['replied_by']   =   $data['ticket_owner'] ;
                        $data['mticket_no']   =   $ticket_no ;
                        $data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
                        //$data['subject']    =   $data['ticket_subject'] ;
                        //$data['text']    =   $data['ticket_text'] ;
						$data['subject']    =     $_ALL_POST['ticket_subject'] ;
					    $data['text']    =   $_ALL_POST['ticket_text'] ;
                        $data['attachment']   =   $attachfilename ;
                        $data['link']   = DIR_WS_MP .'/bank-ticket.php?perform=view&ticket_id='. $variables['hid'];
                        $file_name ='';
						
						if(!empty($attachfilename)){
							$file_name[] = DIR_FS_BT_FILES ."/". $attachfilename;
						}
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
						
						if(!empty($data['ss_file_1'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_1'];
						}
						if(!empty($data['ss_file_2'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_2'];
						}
						if(!empty($data['ss_file_3'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_3'];
						}
						
                        // Send Email to the ticket owner BOF  
                        $main_client_name = '';
                        $mail_send_to_su  = '';
                        $billing_name = '';
                        //for sms
                        $sms_to_number = $sms_to =array();
                        $mobile1 = $mobile2='';
                        $counter=0;
                        $smsLength = strlen($data['text']); 
						Clients::getList($db, $userDetails, 'number, f_name, l_name, email,email_1,email_2, email_3,email_4,
							additional_email,title as ctitle, billing_name,  mobile1, mobile2', " WHERE user_id = '".$data['ticket_owner_uid'] ."'");
                        if(!empty($userDetails)){
                            $userDetails=$userDetails['0'];
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    =   $userDetails['ctitle'];
                            $main_client_name    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $billing_name    =   $userDetails['billing_name'];
                            $email = NULL;
						}	
						/* 
                            $cc_to_sender= $cc = $bcc = Null;
                            if ($data['is_private'] == 1){
                                if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_CONFIDENTIAL', $data, $email) ) {
                                    //$to = '';
                                    $to[]   = array('name' =>  $userDetails['email'], 
                                    'email' => $userDetails['email']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email'];
                                     
                                    
                                    if($userDetails['email_1']){                                   
                                        //$to = '';
                                        $to[]   = array('name' => $userDetails['email_1'], 
                                        'email' => $userDetails['email_1']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_1'];
                                                                       
                                    }
                                    if($userDetails['email_2']){                                   
                                        //$to = '';
                                        $to[]   = array('name' => $userDetails['email_2'], 
                                        'email' => $userDetails['email_2']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_2'];
                                                                         
                                    }
                                    
                                    if($userDetails['email_3']){                                   
                                        //$to = '';
                                        $to[]   = array('name' => $userDetails['email_3'],
                                        'email' => $userDetails['email_3']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_3'];
                                                                  
                                    }
                                    if($userDetails['email_4']){                                   
                                        //$to = '';
                                        $to[]   = array('name' => $userDetails['email_4'], 
                                        'email' => $userDetails['email_4']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_4'];
                                                                   
                                    }
                                }
                                //Send SMS
                                if(isset($data['mobile1_client']) && $smsLength<=130){
                                    //$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
                                    $sms_to_number[] =$userDetails['mobile1'];
                                    $mobile1 = "91".$userDetails['mobile1'];
                                    $client_mobile .= ','.$mobile1;
                                    ProjectTask::sendSms($data['text'],$mobile1);
                                    $counter = $counter+1;
                                        
                                }
                                if(isset($data['mobile2_client']) && $smsLength<=130){
                                    //$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
                                    $sms_to_number[] =$userDetails['mobile2'] ;
                                    $mobile2 = "91".$userDetails['mobile2'];
                                    $client_mobile .= ','.$mobile2;
                                    ProjectTask::sendSms($data['text'],$mobile2);
                                    $counter = $counter+1;                                        
                                }
                                if(!empty($mobile1) || !empty($mobile2)){
                                     $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
                                }                                
                                
                            }else{
								if ($data['mail_client'] == 1 ){  
									if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_MEMBER', $data, $email) ) {                                    
										//$to = '';
										$to[]   = array('name' => $userDetails['email'], 'email' => 
										$userDetails['email']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email'];
										 
										
										if(!empty($userDetails['email_1'])){                                       
											//$to = '';
											$to[]   = array('name' => $userDetails['email_1'], 
											'email' =>	$userDetails['email_1']);                                   
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_1'];
											                                         
										}
										if(!empty($userDetails['email_2'])){                                       
											//$to = '';
											$to[]   = array('name' => $userDetails['email_2'], 'email' =>
											$userDetails['email_2']);                                   
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_2'];
											 
										                             
										} 
										if(!empty($userDetails['email_3'])){                                       
											//$to = '';
											$to[]   = array('name' => $userDetails['email_3'], 
											'email' => $userDetails['email_3']);                                   
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_3'];
											 
										   
										} 
										if(!empty($userDetails['email_4'])){                                       
											//$to = '';
											$to[]   = array('name' =>$userDetails['email_4'], 
											'email' => $userDetails['email_4']);                                   
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_4'];                                        
											                                     
										}
									   
									}
								}
								
								if($mail_to_additional_email==1){
									getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_MEMBER', $data, $email);
									if(!empty($userDetails['additional_email'])){
										$arrAddEmail = explode(",",$userDetails['additional_email']); 
										foreach($arrAddEmail as $key=>$val ){
											if(!empty($val)){
												$mail_send_to_su  .= ",".$val;
												//$to = '';
												$to[]   = array('name' =>$val, 'email' => $val);  
												$from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												 
											}
										}
									}
                                }else{
									getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_MEMBER', $data, $email);
									if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
										$arrAddEmail = $data['mail_additional_email'];
										foreach($arrAddEmail as $key=>$val ){
											if(!empty($val)){
												$mail_send_to_su  .= ",".$val;
												//$to = '';
												$to[]   = array('name' =>$val, 'email' => $val);  
												$from   = $reply_to = array('name' => $email["from_name"],
												'email' => $email['from_email']);
												 
											}
										}
									} 
								} 
                            }							
							//Send SMS
							if(isset($data['mobile1_client']) && $smsLength<=130){
								//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
								$sms_to_number[] =$userDetails['mobile1'];
								$mobile1 = "91".$userDetails['mobile1'];
								$client_mobile .= ','.$mobile1;
								ProjectTask::sendSms($data['text'],$mobile1);
								$counter = $counter+1;
									
							}
							if(isset($data['mobile2_client']) && $smsLength<=130){
								//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
								$sms_to_number[]=$userDetails['mobile2'];
								$mobile2 = "91".$userDetails['mobile2'];
								$client_mobile .= ','.$mobile2;
								ProjectTask::sendSms($data['text'],$mobile2);
								$counter = $counter+1;                                        
							}
							if(!empty($mobile1)|| !empty($mobile2)){
								 $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
							}
                            //Send a copy to check the mail for clients                                                        
                        }*/
                        // Send Email to the ticket owner EOF
                        $subsmsUserDetails=array();
                        // Send Email to all the subuser BOF
                        if ($data['is_private'] != 1){
                            if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
                                if(isset($_POST['mail_to_all_su'])){
									$followupSql= "";
									if(!empty($transaction_id)){ 
										$followupSql = " AND ( authorization_id  LIKE '%,".Clients::ACCOUNTS.",%' OR 
										authorization_id  LIKE '%,".Clients::MANAGEMENT.",%' OR
										authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%' ) " ;
									}
                                    Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."')
                                    AND status='".Clients::ACTIVE."' AND check_email='0' ".$followupSql."");
                                
								}elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
                                   $mail_to_su = implode("','",array_unique($_POST['mail_to_su']));                               
                                   Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " WHERE  user_id IN('".$mail_to_su."') AND status='".Clients::ACTIVE."' "); 
                                }                                
                                if(!empty($subUserDetails)){
                                    foreach ($subUserDetails  as $key=>$value){
                                        $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                        $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_MEMBER', $data, $email) ) {
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email'], 
											'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
											 
                                            if(!empty($subUserDetails[$key]['email_1'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_1'], 
												'email' => $subUserDetails[$key]['email_1']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ; 
                                            } 
                                            if(!empty($subUserDetails[$key]['email_2'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_2'], 
												'email' => $subUserDetails[$key]['email_2']);
                                                $from   = $reply_to = array('name' => $email["from_name"],
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                                
                                            }
                                            if(!empty($subUserDetails[$key]['email_3'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_3'], 
												'email' => $subUserDetails[$key]['email_3']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                               
                                            }
                                            if(!empty($subUserDetails[$key]['email_4'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_4'], 
												'email' => $subUserDetails[$key]['email_4']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ; 
                                            }
                                        }                      
                                    }
                                }
                            }
							if($mail_to_additional_email==1){
								if(!empty($userDetails['additional_email'])){
									$arrAddEmail = explode(",",$userDetails['additional_email']); 
									foreach($arrAddEmail as $key=>$val ){
										if(!empty($val)){
											$mail_send_to_su  .= ",".$val;
											//$to = '';
											$to[]   = array('name' =>$val, 'email' => $val);  
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											 
										}
									}
								}
							}else{
								if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
									$arrAddEmail = $data['mail_additional_email'];
									foreach($arrAddEmail as $key=>$val ){
										if(!empty($val)){
											$mail_send_to_su  .= ",".$val;
											//$to = '';
											$to[]   = array('name' =>$val, 'email' => $val);  
											$from   = $reply_to = array('name' => $email["from_name"],
											'email' => $email['from_email']);
											 
										}
									}
								} 
							} 
                            if(isset($_POST['mobile1_to_su'])){                                
                               $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
                               Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " 
                                WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."') AND 
								user_id IN('".$mobile1_to_su."') AND status='".Clients::ACTIVE."' ");
                                if(!empty($subsmsUserDetails)){                                                                   
                                    foreach ($subsmsUserDetails  as $key=>$value){
                                        if(isset($value['mobile1']) && $smsLength<=130){
                                            $sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
                                            $sms_to_number[] =$value['mobile1'];
                                            $mobile1 = "91".$value['mobile1'];
                                            $client_mobile .= ','.$mobile1;
                                            ProjectTask::sendSms($data['text'],$mobile1);
                                            $counter = $counter+1; 
                                        }
                                    }
                                } 
                            }
                            $subsmsUserDetails=array();
                            if(isset($_POST['mobile2_to_su']) ){                                
                               $mobile2_to_su = implode("','",$_POST['mobile2_to_su']);                                  
                               Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile2', " 
                                WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."')
								AND user_id IN('".$mobile2_to_su."')  AND status='".Clients::ACTIVE."' ");
                                if(!empty($subsmsUserDetails)){                                                                   
                                    foreach ($subsmsUserDetails  as $key=>$value){
                                        if(isset($value['mobile2']) && $smsLength<=130){
                                            $sms_to[]  = $value['f_name'] .' '. $value['l_name'];
                                            $sms_to_number[] =$value['mobile2'];
                                            $mobile2 = "91".$value['mobile2'];
                                            $client_mobile .= ','.$mobile2;
                                            ProjectTask::sendSms($data['text'],$mobile2);
                                            $counter = $counter+1;
                                        }
                                    }
                                }
                            }  
                        }
                        // Send Email to all the subuser EOF
                        //Set the list subuser's email-id to whom the mails are sent bof
                        $query_update = "UPDATE ". TABLE_VENDORS_BANK_TICKETS 
                                    ." SET ". TABLE_VENDORS_BANK_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', 
									sms_to_number='".implode(",",array_unique($sms_to_number))."',
                                        sms_to ='".implode(",",array_unique($sms_to))."' "                                    
                                    ." WHERE ". TABLE_VENDORS_BANK_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
                        $db->query($query_update) ;
                        //Set the list subuser's email-id to whom the mails are sent eof                        
                        //update sms counter
                        if($counter >0){
                            $sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
                            $db->query($sql); 
                        } 
						//Send a copy to check the mail for clients
						if(!empty($to)){
							getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_MEMBER', $data, $email);
							//$to     = '';
							//print_R($to);
							//echo $email["body"];
							$cc_to_sender=$cc=$bcc=''; 
							$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
							'email' => $smeerp_client_email);
							$from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);
							SendMailAccounts($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,
							$cc,$bcc,$file_name);
						} 
						
                        // Send Email to the admin BOF 
                        $email = NULL;
                        $data['link']   = DIR_WS_NC .'/bank-ticket.php?perform=view&ticket_id='. $variables['hid'];
                        $data['quicklink']   = DIR_WS_SHORTCUT .'/bank-ticket.php?perform=view&ticket_id='. $variables['hid'];      
                        $data['main_client_name']   = $main_client_name;
                        $data['billing_name']   = $billing_name;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_BKT_ADMIN', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $bank_purchase_name , 'email' => $bank_purchase_email);
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							
                            SendMailAccounts($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                            $cc_to_sender,$cc,$bcc,$file_name);
                        }                        
                        // Send Email to the admin EOF
                        $messages->setOkMessage("New Support Ticket has been created.And email Notification has been sent.");
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
                }else{
                    $condition_query1 = " WHERE user_id = '". $data['ticket_owner_uid'] ."' ";
                    if (Clients::getList($db, $_ALL_POST['client_details'], 'user_id,  f_name,l_name,
						additional_email,status,send_mail,org,billing_name,
                         email,email_1,email_2, email_3,email_4, mobile1, mobile2', $condition_query1) > 0 ) {
                        $_ALL_POST['user_id']=$_ALL_POST['client_details']['0']['user_id'];
                        $_ALL_POST['name']=$_ALL_POST['client_details']['0']['f_name']." ".$_ALL_POST['client_details']['0']['l_name'];
                        //$_ALL_POST['email']=$_ALL_POST['email'];
                        $_ALL_POST['billing_name'] = $_ALL_POST['client_details']['0']['billing_name'];
                        $_ALL_POST['additional_email'] = $_ALL_POST['client_details']['0']['additional_email'];
                        $_ALL_POST['mobile1']  = $_ALL_POST['client_details']['0']['mobile1'];
                        $_ALL_POST['mobile2']  = $_ALL_POST['client_details']['0']['mobile2'];
                        $_ALL_POST['send_mail'] = $_ALL_POST['client_details']['0']['send_mail'];
                        $_ALL_POST['email']    = $_ALL_POST['client_details']['0']['email'];
                        $_ALL_POST['email_1']=$_ALL_POST['client_details']['0']['email_1'];
                        $_ALL_POST['email_2']=$_ALL_POST['client_details']['0']['email_2'];
                        $_ALL_POST['email_3']=$_ALL_POST['client_details']['0']['email_3'];
                        $_ALL_POST['email_4']=$_ALL_POST['client_details']['0']['email_4'];
						$all_additional_email = trim($_ALL_POST['additional_email'],",");
						$additionalEmailArr = explode(",",$all_additional_email);
						$_ALL_POST['accountValid'] = 0;
						if(!empty($transaction_id)){
							$authorization_id = trim($_ALL_POST['client_details']['0']['authorization_id'],",");
							$authorizationArr = explode(",",$authorization_id);
							$isValid = in_array(Clients::ACCOUNTS,$authorizationArr);
							$_ALL_POST['accountValid'] = $isValid ;
						}
                    }
                    if(isset($data['mail_to_all_su'])){
                        $_ALL_POST['mail_to_all_su']=$data['mail_to_all_su'];
                    }
                    if(isset($data['mail_to_su'])){
                        $_ALL_POST['mail_to_su']=$data['mail_to_su'];
                    }
                    //BOF read the available domains
                    //BankTicket::getDomains($db,$domains,$data['ticket_owner_uid']);
                    //EOF read the available domains 
					//EOF read the available domains
                    $subUserDetails=null;  
					$subUserDetailsAccts=$subUserDetailsMgmts=$subUserDetailsAuth=$subUserDetailsUsr=$subUserDetailsDnd=$subUserDetailsDir=array();
					$followupSql= ""; 
					//Director BOF
					$followupSql = " AND authorization_id  LIKE '%,".Clients::DIRECTOR.",%'" ;
					$suCondition = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."') AND status='".Clients::ACTIVE."' ".$followupSql." ORDER BY f_name "; 
					
					Clients::getList($db, $subUserDetailsDir1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition);  
					if(!empty($subUserDetailsDir1)){
						foreach($subUserDetailsDir1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsDir[$key_su] = $val_su; 
						}
					} 
					//Director EOF
					//Accounts BOF
					$followupSql = " AND authorization_id  LIKE '%,".Clients::ACCOUNTS.",%'" ;
					echo $suCondition = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."') AND status='".Clients::ACTIVE."'  
					".$followupSql." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsAccts1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition);  
					if(!empty($subUserDetailsAccts1)){
						foreach($subUserDetailsAccts1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsAccts[$key_su] = $val_su; 
						}
					} 
					//Accounts EOF
					//MANAGEMENT BOF
					$followupSql1 = " AND authorization_id  LIKE '%,".Clients::MANAGEMENT.",%'" ;
					$suCondition1 = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql1." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsMgmts1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1,mobile2,authorization_id', $suCondition1); 
					if(!empty($subUserDetailsMgmts1)){
						foreach($subUserDetailsMgmts1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsMgmts[$key_su] = $val_su; 
						}
					} 
					//MANAGEMENT EOF
					//Authorised BOF
					$followupSql2 = " AND authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%'" ;
					$suCondition2 = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql2." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsAuth1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition2); 
					if(!empty($subUserDetailsAuth1)){
						foreach($subUserDetailsAuth1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsAuth[$key_su] = $val_su; 
						}
					} 
					
					//Authorised EOF
					//USER BOF
					$followupSql3 = " AND authorization_id  LIKE '%,".Clients::USER.",%'" ;
					$suCondition3 = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."') 
					AND status='".Clients::ACTIVE."'  
					".$followupSql3." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsUsr1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition3); 
					if(!empty($subUserDetailsUsr1)){
						foreach($subUserDetailsUsr1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsUsr[$key_su] = $val_su; 
						}
					} 
					//USER EOF
					//DND BOF
					$followupSql4 = " AND authorization_id  LIKE '%,".Clients::DND.",%'" ;
					$suCondition4 = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql4." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsDnd1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition4); 
					if(!empty($subUserDetailsDnd1)){
						foreach($subUserDetailsDnd1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsDnd[$key_su] = $val_su; 
						}
					} 
					//DND EOF
				}  
            }
        }
		
        if(!isset($_ALL_POST['ticket_text'])){
			$_ALL_POST['ticket_text'] ='Dear....';
		}
        
		if(isset($_POST['btnCreate']) && $data['step1']!='1' && $messages->getErrorMessageCount() <= 0 ){
			if(!empty($transaction_id)){
				header("Location:".DIR_WS_NC."/bank-ticket.php?perform=traflw_view&added=1&ticket_id=".$variables['hid']."&transaction_id=".$transaction_id."&vendor_tra=".$vendor_tra);
			}else{
				header("Location:".DIR_WS_NC."/bank-ticket.php?perform=add&added=1");
			}
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/bank-ticket-list.php');
        }
        else {
        
            $variables['can_view_client_details']  = false;
            if ( $perm->has('nc_uc_contact_details') ) {
                $variables['can_view_client_details'] = true;
            }
            $variables['client_sendmail'] =Clients::SENDMAILNO;
			$variables['type_bst'] = SupportTicketTemplate::BST;
			
            /*$condition_query= " WHERE user_id = '". $user_id ."' ";
            $clients      = NULL;
            if ( Clients::getList($db, $clients, '*', $condition_query) > 0 ) {
                //$_ALL_POST = $_ALL_POST[0];
                $_ALL_POST['name']=$clients[0]['f_name']." ".$clients[0]['l_name'];
                $_ALL_POST['email']=$clients[0]['email'];
            }*/            
            /*$condition_query= " WHERE service_id LIKE '%". Clients::ST ."%'";            
            $clients      = NULL;
            Clients::getList($db, $clients, '*', $condition_query);*/
            
            // These parameters will be used when returning the control back to the List page.
            /*foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            print_R($subUserDetailsUsr); */
			
            $hidden[] = array('name'=> 'perform','value' => $perform);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //$hidden[] = array('name'=> 'user_id', 'value' => $user_id);
			$hidden[] = array('name'=> 'transaction_id','value' => $transaction_id);
            $hidden[] = array('name'=> 'vendor_tra','value' => $vendor_tra);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            //$page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
			$page["var"][] = array('variable' => 'transaction_id', 'value' => 'transaction_id');
			$page["var"][] = array('variable' => 'category_list', 'value' => 'category_list'); 
			$page["var"][] = array('variable' => 'additionalEmailArr', 'value' => 'additionalEmailArr');
		    $page["var"][] = array('variable' => 'subUserDetailsAccts', 'value' => 'subUserDetailsAccts');
            $page["var"][] = array('variable' => 'subUserDetailsMgmts', 'value' => 'subUserDetailsMgmts');
            $page["var"][] = array('variable' => 'subUserDetailsAuth', 'value' => 'subUserDetailsAuth');
            $page["var"][] = array('variable' => 'subUserDetailsUsr', 'value' => 'subUserDetailsUsr');
            $page["var"][] = array('variable' => 'subUserDetailsDnd', 'value' => 'subUserDetailsDnd');
            $page["var"][] = array('variable' => 'subUserDetailsDir', 'value' => 'subUserDetailsDir');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            //$page["var"][] = array('variable' => 'status', 'value' => 'status');
            //$page["var"][] = array('variable' => 'priority', 'value' => 'priority');
            $page["var"][] = array('variable' => 'domains', 'value' => 'domains');
            //$page["var"][] = array('variable' => 'clients', 'value' => 'clients');
            $page["var"][] = array('variable' => 'step1', 'value' => 'step1');
            $page["var"][] = array('variable' => 'step2', 'value' => 'step2');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bank-ticket-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>