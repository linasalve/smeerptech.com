<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    $pq_id          = isset($_GET["pq_id"]) ? $_GET["pq_id"] : ( isset($_POST["pq_id"]) ? $_POST["pq_id"] : '' );
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/quotation.inc.php');
    include ( DIR_FS_INCLUDES .'/followup.inc.php');
    //include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');    
    $condition_url = $condition_query = '';
    
    if($added){
         $messages->setOkMessage("Record has been added successfully.");
    }
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    //By default search in On
    $_SEARCH["searched"]    = true ;
    if ( $perm->has('nc_sl_ld_q') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_SALE_QUOTATION   =>  array( 
                                                            'Quote No.'        => 'number',                                                            
                                                        ),
                                TABLE_SALE_LEADS     =>  array(  'Lead Number'   => 'lead_number',
                                                                'First Name'    => 'f_name',
                                                                'Last Name'     => 'l_name',
                                                                'Email'         => 'email'
                                                ),
								
                               
                            );
        
        $sOrderByArray  = array(
                                TABLE_SALE_QUOTATION => array(
                                                        'Quote No.'        => 'number',      
                                                        'Date of Add'=> 'do_e',
                                                        'Status'        => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SALE_QUOTATION;
        }

       
      
        $variables['status'] = Quotation::getStatus();
        $variables['search']        = 'search';
        $variables['list']        = 'list';
        $variables['pending']        = Quotation::PENDING;
        $variables['active']        = Quotation::ACTIVE;
        $variables['rejected']        = Quotation::REJECTED;
        $variables['approved']        = Quotation::APPROVED;
        $variables['completed']        = Quotation::COMPLETED;
        
        
        
        
        //use switch case here to perform action. 
        switch ($perform) {
        
            case ('add_quot'):
            case ('add'): {            
                include (DIR_FS_NC.'/sale-quotation-add.php');
                
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('mark_ac'):{            
                include (DIR_FS_NC.'/sale-quotation-ac.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }           
            case ('search_ac'): {
                include(DIR_FS_NC."/sale-quotation-ac-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
			case ('search_quot'):
            case ('search'): {
                include(DIR_FS_NC."/sale-quotation-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('change_status'): {
                include ( DIR_FS_NC .'/sale-quotation-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('copy'): {
                include ( DIR_FS_NC .'/sale-quotation-copy.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_file'): {
                include (DIR_FS_NC .'/sale-quotation-view-file.php');
                break;
            }
            case ('send'): {
                
                include (DIR_FS_NC .'/bill-quotation-send.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('create_order'): {
                include ( DIR_FS_NC .'/sale-quotation-create-order.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            /*
            case ('delete_quick'):
            case ('delete'): {
                include ( DIR_FS_NC .'/sale-quick-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quick.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            case ('view_quot'):
            case ('view'): {
                include (DIR_FS_NC .'/sale-quotation-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
			case ('list_quot'):
            case ('list'):
            default: {
                include (DIR_FS_NC .'/sale-quotation-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quotation.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>