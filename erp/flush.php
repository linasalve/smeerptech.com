<?php
//Unset the value variables
gc_collect_cycles();
//echo '1 End: ' . number_format(memory_get_usage(), 0, '.', ',') . " bytes\n";
	if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            unset(${$fetch["value"]});
        }
    }
	ob_end_flush();
	if(is_object($db)){
		$db->close();
	}
	$db=null;
	$s =null;
	$messages=null; 
	$datetime =null;
	page_close();
//echo 'Peak: ' . number_format(memory_get_peak_usage(), 0, '.', ',') . " bytes\n";
//echo 'End 2: ' . number_format(memory_get_usage(), 0, '.', ',') . " bytes\n";
?>