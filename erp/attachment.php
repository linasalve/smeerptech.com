<?php	
	ob_start() ;
    
 
    
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );    
    $prjTaskAdd    = isset($_GET["prjTaskAdd"])   ? $_GET["prjTaskAdd"]  : ( isset($_POST["prjTaskAdd"])    ? $_POST["prjTaskAdd"] : '' );
    $prjbugadd    = isset($_GET["prjbugadd"])   ? $_GET["prjbugadd"]  : ( isset($_POST["prjbugadd"])    ? $_POST["prjbugadd"] : '' );
    $or_id    = isset($_GET["or_id"])   ? $_GET["or_id"]  : ( isset($_POST["or_id"])    ? $_POST["or_id"] : '' );
    $submit ='';
	
	
    

    
	$target_path   = DIR_FS_ATTACHMENTS_FILES."/";
	if(isset($_POST['Uploadfile'])){
			if($_FILES['bug_attached_file']['name']==""){
				
				$messages->setErrorMessage("The File name cannot be empty.");
			} 
			if(!empty($_FILES['bug_attached_file']['name'])){
				if($_POST['bug_attached_desc'] == ""){
				   $messages->setErrorMessage("The File Description cannot be empty.");
				}
			}
			
			if ( $messages->getErrorMessageCount() <= 0 ) 
			{
				$posted_data = processUserData($_POST);
				$tpath   = 	$target_path.$_FILES['bug_attached_file']['name'];
				/*$query = "INSERT INTO temp_upload "
							." SET  temp_name = '".$_FILES['bug_attached_file']['name']."',"
							." temp_desc      = '".$posted_data['bug_attached_desc']."'";
				$db->query($query);
				$temp_id = mysql_insert_id();
                */
				$temp_file[] = array(
									 "temp_name" => $_FILES['bug_attached_file']['name'],
									 "temp_desc" => $posted_data['bug_attached_desc']	
									);
				if(empty($_SESSION['attachment']))
				{
				    $_SESSION['attachment'][1] = $temp_file;
				}
				else{
				   $size =  sizeof($_SESSION['attachment']);
				   $_SESSION['attachment'][$size+1] = $temp_file;
				}	
                /*
                if(empty($fileAttach[$or_id])){
                
                    $fileAttach[$or_id][1] = $temp_file;
                }else{
                
                    $size =  sizeof($fileAttach[$or_id]);
				    $fileAttach[$or_id][$size+1] = $temp_file;
                }
                */
				move_uploaded_file($_FILES['bug_attached_file']['tmp_name'],$tpath);
				@chmod($target_path,0777);
				$tpath = "";
				$messages->setOkMessage("The File successfully uploaded.");	
				$submit = 1;
			}
	}
 
	$s->assign("submit", $submit);
	$s->assign("prjbugadd", $prjbugadd);
	$s->assign("prjTaskAdd", $prjTaskAdd);
	$s->assign("variables", $variables);
	//$s->assign("pagefrom", $pagefrom);
	//$s->assign("CONTENT", $s->fetch("attachment.html"));
	$s->assign("error_messages", $messages->getErrorMessages());
	$s->assign("success_messages",$messages->getOkMessages());
	//$s->assign("pagination", $pagination);
	//$s->assign("pv_array", $pv_array);
	//$s->assign("lang_arr", $lang_arr);
	$s->display('attachment.html');
	include_once( DIR_FS_NC ."/flush.php");
   ob_end_flush();
?>