<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/sms-client-account.inc.php' ); 
    include_once ( DIR_FS_INCLUDES .'/sms-api.inc.php');
    include_once ( DIR_FS_INCLUDES .'/sms-sender.inc.php');
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $updated 		= isset($_GET["updated"])         ? $_GET["updated"]        : ( isset($_POST["updated"])          ? $_POST["updated"]       :'0');

    $condition_url ='';
    $condition_query = '';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
     //By default search in On
    $_SEARCH["searched"]    = true ;
    
     // Read the available Status 
    
  
    
    if($added){    
        $messages->setOkMessage("New record has been added.");
    }
    if($updated){    
        $messages->setOkMessage("Record has been updated.");
    }
	
    if ( $perm->has('nc_sms_ca') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                 TABLE_SMS_CLIENT_ACCOUNT =>  array(  'Client Account Name'     => 'name'),
                                 TABLE_SMS_API   =>  array(  ' API Name'     => 'name',
                                                            'Username'   => 'username',
                                                            'Post Url'      => 'post_url'
                                                        ),
                                TABLE_CLIENTS   =>  array(  ' Client First Name'     => 'f_name-clients',
                                                           ' Client Last Name'     => 'l_name-clients'
                                                        ),
                                 TABLE_SMS_GATEWAY   =>  array(  'Gateway Company'     => 'company'                                                           
                                                        ),
                                 // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                TABLE_USER      =>  array(  'Creator Number'    => 'number-user',
                                                            'Creator Username'  => 'username-user',
                                                            'Creator Email'     => 'email-user',
                                                            'Creator First Name'=> 'f_name-user',
                                                            'Creator Last Name' => 'l_name-user',
                                                )
								
								
                            );
        
        $sOrderByArray  = array(
                                TABLE_SMS_API => array( 'Gateway'        => 'gateway_id',
                                                        'Date of Entry'  => 'do_e',
                                                        'Status'         => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SMS_API;
        }

        // Read the available Status for the Pre Order.
      
        
        
        //use switch case here to perform action. 
        switch ($perform) {
           
        
            case ('add'): {
            
                include (DIR_FS_NC.'/sms-client-account-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-client-account.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }           
          
        
            case ('edit'): {
                include (DIR_FS_NC .'/sms-client-account-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-client-account.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
          
            case ('view'): {
                include (DIR_FS_NC .'/sms-client-account-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
          
            case ('search'): {
                include(DIR_FS_NC."/sms-client-account-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-client-account.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
          
            case ('delete'): {
                include ( DIR_FS_NC .'/sms-client-account-delete.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-client-account.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
          
            case ('list'):
            default: {
                include (DIR_FS_NC .'/sms-client-account-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-client-account.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'sms-client-account.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>