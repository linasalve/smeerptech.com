<?php

    //if ( $perm->has('nc_bl_po_list') ) {
		
		if ( !isset($_SEARCH) ) {
            $_SEARCH['searched'] = true;
        }
		
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';
            $time   = time();
            $l_mon  = strtotime('-1 month', $time);
            $from   = date('Y-m-d 00:00:00', $l_mon );
            $to     = date('Y-m-d 23:59:59', $time);
            
            $condition_query = " WHERE date >= '". $from ."' AND date <= '". $to ."'";
            
            $_SEARCH["date_from"]   = date('d/m/Y', $l_mon);
            $_SEARCH["date_to"]     = date('d/m/Y', $time);
        }
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;

       

        // To count total records.
        $list	= 	NULL;
        $total	=	PreQuickOrder::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $list	= NULL;
        PreQuickOrder::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_bl_po_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_bl_po_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_bl_po_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_bl_po_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_bl_po_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_bl_po_status') ) {
            $variables['can_change_status']     = true;
        }
		if ( $perm->has('nc_bl_or_add') ) {
            $variables['can_add_order']     = true;
        }
		 
        //print_r($list);
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-pre-quick-order-list.html');
   /*
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
    */
?>