<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
		
        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    
                    //$table = TABLE_ACTIVITY_SCORE ;
                    foreach( $field_arr as $key => $field ) {
                        $condition_query .= " ". $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                        $where_added = true;
                    }                 
                }
            }
            $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
    /*
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( $chk_status == 'AND' || $chk_status == 'OR') {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".status IN ('". implode("','", $sStatus) ."') ";
    }
    $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatus";
    $_SEARCH["chk_status"]  = $chk_status;
    $_SEARCH["sStatus"]     = $sStatus;
    // EO: Status data.
    */
    
    // BO: Reason data.
    $tReasonStr = '';
    $chk_reason = isset($_POST["chk_reason"])   ? $_POST["chk_reason"]  : (isset($_GET["chk_reason"])   ? $_GET["chk_reason"]   :'');
    $rReason    = isset($_POST["rReason"])      ? $_POST["rReason"]     : (isset($_GET["rReason"])      ? $_GET["rReason"]      :'');
    if ( ($chk_reason == 'AND' || $chk_reason == 'OR' )  && isset($rReason)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_reason;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if(is_array($rReason)){
             $tReasonStr = implode("','", $rReason) ;
             $tReasonStr1 = implode(",", $rReason) ;
             
        }else{
            $tReasonStr = str_replace(',',"','",$rReason);
            $tReasonStr1 = $rReason ;
            $rReason = explode(",",$rReason) ;
        }
        
        $condition_query .= " ". TABLE_ACTIVITY_SCORE .".reason_id IN ('". $tReasonStr ."') ";
        
        $condition_url          .= "&chk_reason=$chk_reason&rReason=$tReasonStr1";
        $_SEARCH["chk_reason"]  = $chk_reason;
        $_SEARCH["rReason"]     = $rReason;        
    }
    // EO: Reason data.
    
    $executive_by        = isset($_POST["executive_by"])      ? $_POST["executive_by"]         : (isset($_GET["executive_by"])      ? $_GET["executive_by"]      :'');
	$off_exp_issue_by_name  = isset($_POST["off_exp_issue_by_name"])      ? $_POST["off_exp_issue_by_name"]         : (isset($_GET["off_exp_issue_by_name"])      ? $_GET["off_exp_issue_by_name"]      :'');
    
	if(!empty($executive_by)){
		if ( $where_added ) {
            $condition_query .= " AND ";        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
	
		$condition_query .= "  ". TABLE_ACTIVITY_SCORE .".created_by ='". $executive_by ."'  ";
        $condition_url          .= "&executive_by=$executive_by&off_exp_issue_by_name=".$off_exp_issue_by_name;
        $_SEARCH["executive_by"]  = $executive_by;
		$_SEARCH["off_exp_issue_by_name"]  = $off_exp_issue_by_name;
    }
   
    $executive_to        = isset($_POST["executive_to"])      ? $_POST["executive_to"]         : (isset($_GET["executive_to"])      ? $_GET["executive_to"]      :'');
	$off_exp_issue_to_name  = isset($_POST["off_exp_issue_to_name"])      ? $_POST["off_exp_issue_to_name"]         : (isset($_GET["off_exp_issue_to_name"])      ? $_GET["off_exp_issue_to_name"]      :'');
	if(!empty($executive_to)){
		if ( $where_added ) {
            $condition_query .= " AND ";        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= "  ".TABLE_ACTIVITY_SCORE .".comment_to LIKE '%,". $executive_to .",%'  ";
        $condition_url          .= "&executive_to=$executive_to&off_exp_issue_to_name=".$off_exp_issue_to_name;
        $_SEARCH["executive_to"]  = $executive_to;
        $_SEARCH["off_exp_issue_to_name"]  = $off_exp_issue_to_name;
	}
	
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $search_by        = isset($_POST["search_by"])      ? $_POST["search_by"]         : (isset($_GET["search_by"])      ? $_GET["search_by"]      :'');
    
    // BO: From Date
    if (( $chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_ACTIVITY_SCORE .".date >= '". $dfa ."'";
        
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
    }
    
    // EO: From Date
    
    // BO: Upto Date
        
        if (( $chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)){
            if ( $where_added ) {
                $condition_query .= $chk_date_to;
            }
            else {
                $condition_query.= ' WHERE ';
                $where_added    = true;
            }
            $dta = explode('/', $date_to);
            $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
            $condition_query .= " ". TABLE_ACTIVITY_SCORE .".date <= '". $dta ."'";
            
            $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
            $_SEARCH["chk_date_to"] = $chk_date_to ;
            $_SEARCH["date_to"]     = $date_to ;
        }
        
    // EO: Upto Date

    $_SEARCH['search_by']=$search_by;

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search&search_by=".$search_by;
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/activity-score-list.php');
?>