<?php
    if ( $perm->has('nc_cst_update_status') ) {
	    $ticket_id = isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
        
        $access_level = $my['access_level'];
                
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        CeoTicket::deactive($ticket_id, $extra);
        
		 
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this module.");
         //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
