<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");  
	include_once ( DIR_FS_INCLUDES .'/task-reminder.inc.php');
	$id =  isset($_GET["id"])   ? $_GET["id"]  : ( isset($_POST["id"])    ? $_POST["id"] : '' );
	$task_cid =  isset($_GET["task_cid"]) ? $_GET["task_cid"]  : ( isset($_POST["task_cid"]) ?  $_POST["task_cid"] : '' );
	$status = isset($_GET["status"])   ? $_GET["status"]  : ( isset($_POST["status"])    ? $_POST["status"] : '' );
	$action = isset($_GET["action"])   ? $_GET["action"]  : ( isset($_POST["action"])    ? $_POST["action"] : '' );
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new = new db_local; // database handle 
	 
	 $fields = TABLE_TASK_REMINDER.".comment,".TABLE_TASK_REMINDER.".id";
	 $condition_query = " WHERE ( ".TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
	. TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
	. TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR " 
	. TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' ) AND ".TABLE_TASK_REMINDER.".status='".Taskreminder::PENDING."' 
		AND ( ".TABLE_TASK_REMINDER.".acknowledge_by NOT LIKE '%,".$my['user_id']."%' OR ".TABLE_TASK_REMINDER.".last_comment_acknowledge_by NOT LIKE '%,".$my['user_id']."%' ) ORDER BY ".TABLE_TASK_REMINDER.".id DESC LIMIT 0,5";
	
	Taskreminder::getDetails( $db, $list, $fields, $condition_query);
     $message = "<div class=\"jGrowl-notification\"></div>
	  <div class=\"jGrowl-notification highlight ui-corner-all default\" style=\"display: block;\" id=\"ts_1\">
				<div class=\"jGrowl-close\" onclick=\"javascript:closeNotification('ts_1');\">×</div><div class=\"jGrowl-header\"><b>Task Acknowlegment Notification</b></div>";
				
		    
	if(!empty($list)){
		foreach( $list as $key=>$val){  
			$sr= $key +1;
			$message .="<div class=\"jGrowl-message\">".$sr.".  <a class='notification-alert' href=\"javascript:void(0);\" onclick=\"javascript: return loadAjxPage('standalone-form','modalIframeId','".$variables['nc']."/task-reminder.php?perform=acomm&id=".$val['id']."',750,600,'add comment')\" > ".$val['comment']."</a></div>";		 
		}
			
	}else{
		$message .="<div class=\"jGrowl-message\">No records found.</div>";		 
	}  
	$message .="</div>";
	
   
		

echo $message  ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
