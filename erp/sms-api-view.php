<?php
    
    if ( $perm->has('nc_sms_api_details') ) {
        $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');

        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
       
        // Read the record details
        $fields = TABLE_SMS_API .'.*'  ;            
        $condition_query = " WHERE (". TABLE_SMS_API .".id = '". $id ."' )";        
         $condition_query = " LEFT JOIN ".TABLE_SMS_GATEWAY." ON ".TABLE_SMS_GATEWAY.".id=".TABLE_SMS_API.".gateway_id 
                             LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id=".TABLE_SMS_API.".created_by ".$condition_query;
 

        $fields = TABLE_SMS_API .'.id'.','. TABLE_SMS_GATEWAY.'.company as gateway_name,';
        $fields .= TABLE_SMS_API .'.gateway_id'.','. TABLE_SMS_API.'.name,';
        $fields .= TABLE_SMS_API .'.is_scrubbed'.','. TABLE_SMS_API.'.username,';
        $fields .= TABLE_SMS_API .'.password'.','. TABLE_SMS_API.'.post_url,';
        $fields .= TABLE_SMS_API .'.details'.','. TABLE_SMS_API.'.do_e,';
        $fields .= TABLE_SMS_API .'.status'.','. TABLE_SMS_API.'.created_by,';
        $fields .= TABLE_SMS_API .'.ip'.','.TABLE_SMS_API .'.host'.',';
        $fields .= TABLE_USER .'.f_name'.','. TABLE_USER.'.l_name,';
        $fields .= TABLE_USER .'.user_id'.','. TABLE_USER.'.number';
       
        if ( SmsApi::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
         
            $_ALL_POST = $_ALL_POST['0'];          
                          
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-api-view.html');
           
        }
        else {
            $messages->setErrorMessage("Records not found to view.");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>