<?php

    if ( $perm->has('nc_cst_list') ) {
        
        //include_once ( DIR_FS_CLASS .'/Phone.class.php');        
		
		if(empty($condition_query)){		
			$_SEARCH["chk_t_status"]  = 'AND';
			$_SEARCH["tStatus"]   = array(CeoTicket::PENDINGWITHSMEERP, CeoTicket::PENDINGWITHSMEERPCOZCLIENT);            
			$condition_query = " AND ticket_status 
			IN('".CeoTicket::PENDINGWITHSMEERP."','".CeoTicket::PENDINGWITHSMEERPCOZCLIENT."') ";
		}
        
		
        $condition_query = " LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id = ".TABLE_CST_TICKETS.".ticket_owner_uid WHERE ".TABLE_CST_TICKETS.".ticket_child='0' ".$condition_query    ;
                             
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
          //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        
        $total	=	CeoTicket::getList($db, $list, '', $condition_query);
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
       
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        
        $pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','ceo-ticket.php','frmSearch');
   
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $list	= NULL;
        //$fields = TABLE_CST_TICKETS.".*,".TABLE_ST_STATUS.".status_name,";
        $fields = TABLE_CST_TICKETS.".*,";
        //$fields .= TABLE_SETTINGS_DEPARTMENT.".department_name,";
        //$fields .= TABLE_ST_PRIORITY.".priority_name,";
        $fields .= TABLE_CLIENTS.".f_name,".TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name,".TABLE_CLIENTS.".org,".TABLE_CLIENTS.".email,".TABLE_CLIENTS.".mobile1,".TABLE_CLIENTS.".mobile2";
        CeoTicket::getList( $db, $list,$fields, $condition_query, $next_record, $rpp);
        
        $ticketStatusArr = CeoTicket::getTicketStatus();
        $ticketStatusArr = array_flip($ticketStatusArr);
        $ticketRatingArr = CeoTicket::getRating();
        $ticketRatingArr = array_flip($ticketRatingArr);
       
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                //$val['ticket_department']    = $val['department_name'] ;     
                //$val['ticket_status']    = $val['status_name'] ;     
                //$val['ticket_priority']    = $val['priority_name'] ;   
                if(isset($val['ticket_status'])){
                    $val['ticket_status_name'] = $ticketStatusArr[$val['ticket_status']];  
                }
                if(!empty($val['ticket_rating'])){
                    $val['ticket_rating_name'] = $ticketRatingArr[$val['ticket_rating']];  
                }
                $val['ticket_date'] = date("d M Y H:i:s",$val['ticket_date']);
                $executive=array();
                $string = str_replace(",","','", $val['assign_members']);
                $fields4 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                $condition4 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                User::getList($db,$executive,$fields4,$condition4);
                $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['assign_members'] = $executivename ; 
                 
                $db2 = new db_local ;
                
                $query = "SELECT COUNT(*) AS replies FROM ". TABLE_CST_TICKETS
                         ." WHERE ". TABLE_CST_TICKETS .".ticket_child ='". $val['ticket_id'] ."' " ; 
                $db2->query($query) ;
                $db2->next_record() ;
                $ticket_replies = $db2->f("replies") ;
				$db2->close();     
                $val['ticket_replied']    = $ticket_replies ;                
                $fList[$key]=$val;
            }
        }
    $list=$condition_query=$query=$key=$val=$ticket_replies=$executive=$ticketStatusArr=$ticketRatingArr=$fields=null;

        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_view_details']  = false;
        $variables['can_view_client_details']  = false;
        $variables['can_update_status'] = false;
        
        if ( $perm->has('nc_cst_add') ) {
            $variables['can_add'] = true;
        }
        
        if ( $perm->has('nc_cst_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_uc_contact_details') ) {
            $variables['can_view_client_details'] = true;
        }
        if ( $perm->has('nc_cst_list') ) {
            $variables['can_view_list'] = true;
        }
        
        if ( $perm->has('nc_cst_update_status') ) {
            $variables['can_update_status'] = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'ceo-ticket-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>