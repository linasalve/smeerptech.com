	<?php
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/currency.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/followup.inc.php');
	include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/financial-year.inc.php' );
	include_once ( DIR_FS_CLASS .'/Region.class.php');
	
	
/*	
	$db1 		= new db_local; // database handle
	Invoice::getList($db, $list, TABLE_BILL_INV.'.*', " WHERE ".TABLE_BILL_INV.".number = '' AND ".TABLE_BILL_INV.".company_id!='' ORDER BY do_i") ;
   $no=0;
   
if(!empty($list)){
	foreach($list as $key=>$val){
		$data=array();
		$data=$val;
				//Get billing address BOF
                $data['number']=$data['inv_counter'] ='';
				$doi['do_i']  = explode(' ', $data['do_i']);
				$temp               = explode('-', $doi['do_i'][0]);
				
				$do_i = mktime(0, 0, 0, $temp[1], $temp[2], $temp[0]);
				echo $data['company_id']."<br/>";
				
				if(!empty($data['company_id']) && !empty($do_i)){				
					$detailInvNo = getInvoiceNumber($do_i,$data['company_id']); 				
					if(!empty($detailInvNo)){
						$data['number'] = $detailInvNo['number'];
						$data['inv_counter'] = $detailInvNo['inv_counter'];
					}
				}
				
				echo $sql1  = " UPDATE ". TABLE_BILL_INV
                            ." SET ". TABLE_BILL_INV .".number    = '". $data['number'] ."'"
                            .",". TABLE_BILL_INV .".inv_counter  =  ". $data['inv_counter'] ." "
				     		." WHERE id = ". $data['id'] ." ";                
				echo $db1->query($sql1);
				
				$no=$no+1;
				echo "No ".$no."<br/>";
				
				 if(!empty($data['do_e'])){
                    $data['is_renewable']=1;
                }else{
                    $data['is_renewable']=0;
                }
                 
				 
				//get client number
				$clientDetails1 =array();
                $condition_query1 = " WHERE user_id= '".$data['client']."'";
                $fields1 = "number";
                Clients::getList( $db, $clientDetails1, $fields1, $condition_query1);  

                if(!empty($clientDetails1)){
					$clientNo=$clientDetails1[0];
					 
                  $data['client']  = array('number'=>$clientNo['number']); ;
 				}
			 
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS,  $data['client']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $address_list  =  $address_list;
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
	
				$data['amount']= number_format($data['amount'],2);
                $data['amount_inr']= number_format($data['amount_inr'],2);
                $data['balance']= number_format($data['balance'],2);
                $data['amount_words']=processSqlData($data['amount_words']);
                $data['do_i_chk'] = $data['do_i'];			 
			    $data['do_rs_symbol'] = strtotime('2010-07-20');
				
				
				
				
				    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
                    $data['show_discount']=0;
                    $data['show_nos']=0;
                    $data['colsSpanNo1']=8;
                    $data['colsSpanNo2']=9;
                    Order::getParticulars($db, $temp, '*', $condition_query);
                    if(!empty($temp)){                    
                        $data['wd1']=10;
                        $data['wdinvp']=217;
                        $data['wdpri']=70;
                        $data['wdnos']=70;
                        $data['wdamt']=70;
                        $data['wddisc']=50;
                        $data['wdst']=70;
                        $data['wdtx']=40;
                        $data['wdtxp']=50;
                        $data['wdtotam']=75;
                        $data['wd2']=9;
                        
                        foreach ( $temp as $pKey => $parti ) {                        
                              
                            if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
                                $data['show_discount'] = 1; 
                            }
                            if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
                                $data['show_nos']=1;     
                            }                            
                            $temp_p[]=array(
                                            'p_id'=>$temp[$pKey]['id'],
                                            'particulars'=>$temp[$pKey]['particulars'],
                                            'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
                                            's_type' =>$temp[$pKey]['s_type'] ,                                   
                                            's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                            's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
                                            's_id' =>$temp[$pKey]['s_id'] ,                                   
                                            'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                            'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                            'tax1_name' =>$temp[$pKey]['tax1_name'] ,    
                                            'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                                     
                                            'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                            'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,     
                                            'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,                                                  
                                            'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
                                            'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
                                            'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
                                            'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                            'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                                            );
                        }
                        if($data['show_discount'] ==0){ 
                            // disc : 60 & subtot : 70
                            $data['wdinvp']= $data['wdinvp']+20 + 40;
                            $data['wdpri'] = $data['wdpri'] + 5 + 6;
                            $data['wdamt'] = $data['wdamt'] + 5+ 6;                            
                            $data['wdtx']  = $data['wdtx'] + 5+ 6;
                            $data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            $data['wdtotam'] =$data['wdtotam'] + 5+ 6;
                            
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
                        }
                        if($data['show_nos'] ==0){
                            // disc : 70
                            $data['wdinvp']= $data['wdinvp']+30;
                            $data['wdpri'] = $data['wdpri'] + 5;
                            $data['wdamt'] = $data['wdamt'] + 5;
                            $data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;
                        }                        
                    }
                   
                    $data['particulars'] = $temp_p ;                    
                    // Update the Order. for time being
                                    
                    $or_id = NULL;
                    
                    // Create the Invoice PDF, HTML in file.
                    
					$extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';     
					// needs tobe removed as this comment 
                    if ( !($attch1 = Invoice::createFile($data, 'HTMLPRINT', $extra)) ) {
                        $messages->setErrorMessage("The Invoice print html  file was not created.");
                    }                    
                    if ( !($attch = Invoice::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Invoice html file was not created.");
                    }
					 Invoice::createPdfFile($data);
                    $file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";
					
                 
	}

}	 
     */       
	
?>