<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
	
	
    $category_id = isset($_GET['category_id']) ? $_GET['category_id'] : '';
    $type = isset($_GET['type_txt']) ? $_GET['type_txt'] : '';
    	
	if(!empty($category_id) && !empty($type)){
		$subCondition ='';
		if($type==SupportTicketTemplate::ST){
			$subCondition = " AND ".TABLE_ST_TEMPLATE.".show_in LIKE '%,".$type.",%'";
		}elseif($type==SupportTicketTemplate::BST){
			$subCondition = " AND ".TABLE_ST_TEMPLATE.".show_in LIKE '%,".$type.",%'";
		}elseif($type==SupportTicketTemplate::LST){
			$subCondition = " AND ".TABLE_ST_TEMPLATE.".show_in LIKE '%,".$type.",%'";
		}elseif($type==SupportTicketTemplate::PST){
			$subCondition = " AND ".TABLE_ST_TEMPLATE.".show_in LIKE '%,".$type.",%'";
		}elseif($type==SupportTicketTemplate::WP){
			$subCondition = " AND ".TABLE_ST_TEMPLATE.".show_in LIKE '%,".$type.",%'";
		}elseif($type==SupportTicketTemplate::CST){
			$subCondition = " AND ".TABLE_ST_TEMPLATE.".show_in LIKE '%,".$type.",%'";
		}
	 
		$subServiceList='';		 
		$condition_query = " WHERE ".TABLE_ST_TEMPLATE.".template_category LIKE '%,".$category_id.",%' 
		AND ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' ".$subCondition  ; 
		
		$fields = TABLE_ST_TEMPLATE.".id,".TABLE_ST_TEMPLATE.".title " ;        
		$query = " SELECT ".$fields." FROM ".TABLE_ST_TEMPLATE." ".$condition_query ; 
		$query	.= " ORDER BY ".TABLE_ST_TEMPLATE.".title ASC";
		
		$db->query( $query );
		$total	= $db->nf();	
		if( $db->nf() > 0 ){
			while($db->next_record()){
				$subServiceList .=  trim($db->f("title"))."|".$db->f("id")."~";
			}
			$subServiceList =  rtrim($subServiceList,'~');
		}  
	}
	echo $subServiceList ;
	include_once( DIR_FS_NC ."/flush.php");
?>
