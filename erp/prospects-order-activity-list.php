<?php

    if ( $perm->has('nc_ps_or_list') || $perm->has('nc_ps_or_search') ) {
		 
		
        if ( !isset($condition_query) || $condition_query == '' ) {
            $_SEARCH["chk_status"]  = 'AND';
			$_SEARCH["sStatus"]   = array(ProspectsOrder::ACTIVE, ProspectsOrder::COMPLETED,ProspectsOrder::PENDING,ProspectsOrder::PROCESSED);  
			//$_SEARCH["chk_lstatus"]  = 'AND';
			//$_SEARCH["lStatus"]   = array(ProspectsOrder::HOTLSTATUS);  
            			 
            $condition_query = " AND (". TABLE_PROSPECTS_ORDERS .".status IN('".ProspectsOrder::ACTIVE."','".ProspectsOrder::COMPLETED."','".ProspectsOrder::PENDING."','".ProspectsOrder::PROCESSED."'".")  )"; 
			
        }  
		
		 
		if ( $perm->has('nc_ps_or_list_all') ) { 
		
		 
		}else{ 
			if(empty($_SEARCH["search_executive_id"]) || empty($_SEARCH["user_id"]) ){
				/* 
				$condition_query .= " AND ( ";
				$condition_query .= " (". TABLE_PROSPECTS_ORDERS .".order_closed_by = '". $my['user_id'] ."' "."  ) "; 
				$condition_query .= " OR ( "
											. TABLE_PROSPECTS_ORDERS .".team LIKE '%,". $my['user_id'] .",%' "
										. "  "
										."  ) ";  
				$condition_query .= " ) ";
				*/ 
				$condition_query .= " AND ( ";
				$condition_query .= " ". TABLE_PROSPECTS_TICKETS .".ticket_creator_uid = '". $my['user_id'] ."' "."  "; 
				$condition_query .= " )";   				
				$condition_url  .= "&user_id=$user_id";
				$_SEARCH["user_id"]  = $user_id;
			}    
		}
		
		
		if( $sOrderBy=='delay'){
			$condition_query .= " ORDER BY DATEDIFF( CURDATE() , ".TABLE_PROSPECTS_ORDERS.".do_c )  ". $sOrder ;        
		}else{
			$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;        
		}
		
		$condition_query = " WHERE ".TABLE_PROSPECTS_TICKETS.".ticket_type='".ProspectsTicket::TYP_REPORTING."' ".$condition_query;
        if( !isset($_SEARCH)){
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
        //By default search in On
        $_SEARCH["searched"]    = true ;
        
        if($searchStr==1){
          
            $fields = TABLE_PROSPECTS_ORDERS.'.id ' ;
            $total	=	ProspectsTicket::getActivityList( $db, $list, '', $condition_query);
            $extra_url  = '';
            if ( isset($condition_url) && !empty($condition_url) ){
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            $list	= NULL;
            $fields = TABLE_PROSPECTS_ORDERS .'.id'
                    .','. TABLE_PROSPECTS_ORDERS .'.number'
                    .','. TABLE_PROSPECTS_ORDERS .'.order_title' 
                    .','. TABLE_PROSPECTS_ORDERS .'.team'
                    .','. TABLE_PROSPECTS_ORDERS .'.details'
                    .','. TABLE_PROSPECTS_ORDERS .'.do_d'
                    .','. TABLE_PROSPECTS_ORDERS .'.do_c'
                    .','. TABLE_PROSPECTS_ORDERS .'.do_o'
                    .','. TABLE_PROSPECTS_ORDERS .'.status'
                    .','. TABLE_PROSPECTS_ORDERS .'.followup_status'  
					.','. TABLE_PROSPECTS_ORDERS .'.lead_status'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by'
					.','. TABLE_PROSPECTS_TICKETS .'.ticket_type'
					.','. TABLE_PROSPECTS_TICKETS .'.do_activity'
					.','. TABLE_PROSPECTS_TICKETS .'.do_e'
					.','. TABLE_PROSPECTS_TICKETS .'.medium_comm'
					.','. TABLE_PROSPECTS_TICKETS .'.ticket_text'
					.','. TABLE_PROSPECTS_TICKETS .'.ticket_creator'
					.',	  DATEDIFF(CURDATE() ,'.TABLE_PROSPECTS_ORDERS.'.do_c ) AS delay' ;
                    /* 
                    .','. TABLE_PROSPECTS .'.user_id AS c_user_id'
                    .','. TABLE_PROSPECTS .'.number AS c_number'
                    .','. TABLE_PROSPECTS .'.f_name AS c_f_name'
                    .','. TABLE_PROSPECTS .'.l_name AS c_l_name'
                    .','. TABLE_PROSPECTS .'.billing_name'
					.','. TABLE_PROSPECTS .'.site_url'
                    .','. TABLE_PROSPECTS .'.email AS c_email' 
                    .','. TABLE_PROSPECTS .'.status AS c_status';	*/
            ProspectsTicket::getActivityList( $db, $list, $fields, $condition_query, $next_record, $rpp);
        } 
        
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){   
                  
				$val['can_edit_lead'] = 0;   				
				$val['can_delete_lead'] = 0;   			 
				$val['can_create_invoice'] = 0;   				
				if( ( ( $val['order_closed_by']==$my['user_id'] ) || $perm->has('nc_ps_or_list_all')) && $perm->has('nc_ps_or_edit')  ){
					$val['can_edit_lead'] = 1;   					
				}
				if( ( ( $val['order_closed_by']==$my['user_id'] ) || $perm->has('nc_ps_or_list_all')) && $perm->has('nc_ps_or_delete')  ){
					$val['can_delete_lead'] = 1;   					
				}
				if( ( ( $val['order_closed_by']==$my['user_id'] ) || $perm->has('nc_ps_qt_list_all')) && $perm->has('nc_ps_qt_add')  ){
					$val['can_create_invoice'] = 1;   					
				}
                $val['is_invoice_created'] = $is_invoice_created;   
                $val['is_particulars_selected'] = $is_particulars_selected;       
				$val['medium_comm_text']='';
				if($val['medium_comm']==ProspectsTicket::MEDIUM_PHONE){
					$val['medium_comm_text']='Phone';
				}else{
					$val['medium_comm_text']='Visit';
				}
				 
                $flist[$key]=$val;
            }
        }
        $variables['rstatus']=array(
			'PHONE'=>ProspectsTicket::MEDIUM_PHONE,
			'VISIT'=>ProspectsTicket::MEDIUM_VISIT
		);
        
		//GET LIST Of team bof
		if ( $perm->has('nc_ps_or_list_all') ) {   
			
		}else{ 
			$rteam=trim($my['my_reporting_members'],",");
            $team = str_replace(',',"','",$rteam);
			$ulist=array();
			$ulist[]= array('user_id'=>$my['user_id'],
						'number'=>$my['number'],
						'name'=>$my['f_name'].' '.$my['l_name']
					) ;
			$sql= " SELECT f_name,l_name,number,user_id FROM ".TABLE_USER." WHERE ".TABLE_USER.".user_id IN ('".$team."') ";
			$db->query($sql);   
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ) {
					$user_id=$db->f('user_id');
					$number = $db->f('number');
					$name= $db->f('f_name')." ". $db->f('l_name') ; 
					$ulist[]= array('user_id'=>$user_id,
						'number'=>$number,
						'name'=>$name
					) ; 
				}
			}  
		} 
		//Get list of team eof
		
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_view_list_all']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_create_invoice']= false;
        $variables['can_followup'] = false;
        $variables['can_bifurcate'] = false;
		$variables['can_view_inv'] = false;
        
        if ( $perm->has('nc_ps_or_list') ) {
            $variables['can_view_list'] = true;
        }
		if ( $perm->has('nc_ps_or_list_all') ) { 
		    $variables['can_view_list_all'] = true;
		}
        if ( $perm->has('nc_ps_or_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_ps_or_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_ps_or_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_ps_or_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_ps_or_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_ps_qt_add') ) {
            $variables['can_create_invoice'] = true;
        }
        if ( $perm->has('nc_pst') && $perm->has('nc_pst_flw_ord') ) {
            $variables['can_followup'] = true;
        }
		if( $perm->has('nc_ps_qt') && $perm->has('nc_ps_qt_list') ) {
            $variables['can_view_inv']     = true;
        }
		
		//$variables['lstatus'] = ProspectsOrder::getLStatus(); 
		
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'ulist', 'value' => 'ulist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-order-activity-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>