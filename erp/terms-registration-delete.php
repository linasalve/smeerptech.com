<?php
	if ( $perm->has('nc_tcr_delete') ) {
		$id	= isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'access_level' 	=> $my['access_level'],
						'messages' 		=> $messages
					);
		TermsRegistration::delete($id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/terms-registration-list.php');
	}
	else {
		$messages->setErrorMessage("You do not have the Right to Access this module.");
	}
?>