<?php



	$id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );


	$_ALL_POST      = NULL;
	$data           = NULL;


	if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST  = $_POST;
		$data       = processUserData($_ALL_POST);

		$extra = array( 'db'        => &$db,
						'messages'  => $messages 
					);
		
		if ( Policies::validateUpdate($data, $extra) ) {
			 
			$query  = " UPDATE ". TABLE_POLICIES
					." SET ". TABLE_POLICIES .".title     	= '". $data['title'] ."'"
					.",". TABLE_POLICIES .".description   = '". $data['description']."'"
					.",". TABLE_POLICIES .".ip_u	= '".$_SERVER['REMOTE_ADDR'] ."'"
					.",". TABLE_POLICIES .".updated_by	= '". $my['uid'] ."'"
					.",". TABLE_POLICIES .".updated_by_name	= '". $my['f_name']." ".$my['l_name'] ."'"
					.",". TABLE_POLICIES .".do_update    = '".date("Y-m-d h:i:s") ."'"
					.",". TABLE_POLICIES .".status      = '". $data['status'] ."'" 
					." WHERE ". TABLE_POLICIES .".id   = '". $data['id'] ."'";
			
			if ( $db->query($query) ) {
				$variables['hid'] = $data['id'];
				$messages->setOkMessage("Record has been updated.");
				$count1 = count($data['jtitle']);
				$count2 = count($data['p_id']);
				$checkArr =array();
				if($count1 > $count2){
					$checkArr =$data['jtitle'] ;					
				}elseif($count1 < $count2){
					$checkArr =$data['p_id'] ;
				}elseif($count1 == $count2){					
					$checkArr =$data['jtitle'] ;
				}
				
				
				
			    if(isset($data['jtitle'])){
					foreach ( $checkArr as $key=>$pid ) {
						 
						if(!empty($data['p_id'][$key])){
							if(!isset($data['jtitle'][$key]) || empty($data['jtitle'][$key]) ){
							
								$query1 = "DELETE FROM ".TABLE_POLICIES_P
										  ." WHERE id = '".$data['p_id'][$key]."'";
								$db->query($query1);
							
							}else{						
								   
								$query2  = " UPDATE ". TABLE_POLICIES_P
								." SET ".TABLE_POLICIES_P .".jtitle = 
								'".processUserData($data['jtitle'][$key] )."'"
								.",".TABLE_POLICIES_P.".jdescription 
								='".processUserData($data['jdescription'][$key])."'"
								." WHERE id = '". $data['p_id'][$key] ."'" ;
								
								$db->query($query2);
							}
						  
						}else{
							//insert
							if(!empty($data['jtitle'][$key])){                                   
							
								$data['jdescription'][$key] = trim($data['jdescription'][$key]);
								$query3  = " INSERT INTO ". TABLE_POLICIES_P
								." SET ".TABLE_POLICIES_P .".jdescription 
								= '".processUserData($data['jdescription'][$key])."'"	
								.",". TABLE_POLICIES_P .".jtitle= '".processUserData($data['jtitle'][$key])."'"
								.",". TABLE_POLICIES_P .".tjf_id = '".  $id  ."'"					;
							   $db->query($query3);  
							   $data['p_id'][$key] =  $db->last_inserted_id();   
							}                                   
						}  
					}
				}
			   
				//to flush the data.
			   // $_ALL_POST  = NULL;
				//$data       = NULL;
			}
			else {
				$messages->setErrorMessage('Record was not updated.');
			}
		}
	}


	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	if ( isset($_POST['btnCancel'])
		|| (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
		
		$variables['hid'] = $id;
		include ( DIR_FS_NC .'/policies-list.php');
	}
	else {
		
		// Check if the Edit form had been submitted and if error is generated.                

		if ( !empty($id) ) {
			
			$fields = TABLE_POLICIES .'.*'  ;            
			$condition_query = " WHERE (". TABLE_POLICIES .".id = '". $id ."' )";
		
			if ( Policies::getList($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
				$_ALL_POST = $_ALL_POST['0'];
			}
			
			$temp_p = NULL;			 
			$condition_query_p = " WHERE ".TABLE_POLICIES_P.".tjf_id = '". $id ."'";
			Policies::getParticulars($db, $temp_p, '*', $condition_query_p);
			if(!empty($temp_p)){
				foreach ( $temp_p as $pKey => $parti ) {                            
					$_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];					
					$_ALL_POST['jtitle'][$pKey]       = $temp_p[$pKey]['jtitle'];
					$_ALL_POST['jdescription'][$pKey] = $temp_p[$pKey]['jdescription'];				
				}
			}else{
				$_ALL_POST['jtitle']=array('0'=>'');
			}
			
			
			// Check the Access Level.
			$hidden[] = array('name'=> 'perform','value' => 'edit');
			$hidden[] = array('name'=> 'act', 'value' => 'save');
			$hidden[] = array('name'=> 'id', 'value' => $id);
			
			$hidden[] = array('name'=> 'ajx','value' => $ajx);
			$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden'); 
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			
			
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'policies-edit.html');
			 
		} else {
			$messages->setErrorMessage("The Selected Record was not found.");
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
		}
	}

	?>