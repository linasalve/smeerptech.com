<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
	   
	 
   
    
	$id =  isset($_GET["id"])   ? $_GET["id"]  : ( isset($_POST["id"])    ? $_POST["id"] : '' );
	$status = isset($_GET["status"])   ? $_GET["status"]  : ( isset($_POST["status"])    ? $_POST["status"] : '' );
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new = new db_local; // database handle
	
    if (isset($status) && $status!=''){      
        if( $status == Order::NPADEACTIVE){
			
			$str="<br/> NPA Status - DEACTIVE ; Updated by ".$my['f_name']." ".$my['l_name']." (".$my['number'].") ; Date ".date('d M Y');
			$sql= " UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".npa_status='".$status."', 
			".TABLE_BILL_ORDERS.".npa_status_remarks = concat(".TABLE_BILL_ORDERS.".npa_status_remarks,'".$str."') WHERE ".TABLE_BILL_ORDERS.".id ='".$id."'";
			
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Order NPA status marked as De-Active  </li>
							</ul>
						</td>
					</tr>
					</table>";
				 
								
					$message .= "|<a href=\"#\" title=\"Mark to Active\" onClick=\"updateNPAStatus(".$id.",".Order::NPAACTIVE.")\">
									De-Active NPA 
									</a>"; 

					
								
			}
        }elseif($status==Order::NPAACTIVE){
		
			 
			$str="<br/> NPA Status - ACTIVE ; Updated by ".$my['f_name']." ".$my['l_name']." (".$my['number'].") ; Date ".date('d M Y');
			$sql= " UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".npa_status='".$status."', 
			".TABLE_BILL_ORDERS.".npa_status_remarks = concat(".TABLE_BILL_ORDERS.".npa_status_remarks,'".$str."') WHERE ".TABLE_BILL_ORDERS.".id ='".$id."'";
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Order NPA status marked as Active  </li>
							</ul>
						</td>
					</tr>
					</table>";
								
					$message .= "| <a href=\"#\" title=\"Mark to De-Active\" onClick=\"updateNPAStatus(".$id.",".Order::NPADEACTIVE.")\">
									Active NPA 
									</a>"; 
					 
			}
								
		} 
	}	

echo $message."|".$id ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
