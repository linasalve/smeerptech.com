<?php

    if ( $perm->has('nc_pts_mod_add') ) {
        
        $or_id = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
    
        $_ALL_POST	= NULL;
        $data       = NULL;

        $modules = NULL;
		ProjectModule::getParent($db,$modules,$or_id);
        
        $_ALL_POST['parent_id']=0;
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
        
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            
            if ( ProjectModule::validateAdd($data, $extra) ) {
                $query	= " INSERT INTO ". TABLE_PROJECT_TASK_MODULE
                        ." SET ". TABLE_PROJECT_TASK_MODULE .".title     	= '". $data['title'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".parent_id    = '". $data['parent_id'] ."'"
                            //.",". TABLE_PROJECT_TASK_MODULE .".order_id     = '". $data['or_id'] ."'"
                            //.",". TABLE_PROJECT_TASK_MODULE .".order        = '". $data['order'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".details      = '". $data['details'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".created_by   = '". $my['user_id'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".ip           = '". $_SERVER['REMOTE_ADDR'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".status    	= '". $data['status'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".do_e   		= '". date('Y-m-d H:i:s') ."'";
                            
                if ($db->query($query) && $db->affected_rows() > 0){
                
					$messages->setOkMessage("The New Module has been added.");
				    
                }else{
					$messages->setErrorMessage('Problem saving record. Try later');
                }
                
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/project-module.php?perform=add&or_id=".$or_id."&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/project-module.php?or_id=".$or_id);
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/project-module.php?or_id=".$or_id."&added=1");   
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'or_id', 'value' => $or_id );
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'modules', 'value' => 'modules');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-module-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>