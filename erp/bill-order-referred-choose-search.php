<?php

	$condition_query= '';
    $condition_url  = '';
    
    if ( $sString != "" ) {
	
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
		if ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
			$condition_url .= "&sString=". $sString;
			$condition_url .= "&sType=". $sType;
			$where_added = true;
		}elseif ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
			foreach( $sTypeArray as $table => $field_arr ) {
				if ( $table != "Any" ) {
					foreach( $field_arr as $key => $field ) {
                        $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
						$where_added = true;
					}
				}
			}
			$condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
			$condition_query .= ") ";
		}
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}
	
	
	 // BO: Status data.
    $sStatusStr = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ( $chk_status == 'AND' || $chk_status == 'OR') &&  isset($sStatus)){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".status IN ('". $sStatusStr ."') ";        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;        
    }    
    // EO: Status data.
	// BO: Referral Status data BOF
    $rStatusStr = '';
    $chk_rstatus = isset($_POST["chk_rstatus"])   ? $_POST["chk_rstatus"]  : (isset($_GET["chk_rstatus"])   ? $_GET["chk_rstatus"]   :'');
    $rStatus    = isset($_POST["rStatus"])      ? $_POST["rStatus"]     : (isset($_GET["rStatus"])      ? $_GET["rStatus"]      :'');
    if ( ( $chk_rstatus == 'AND' || $chk_rstatus == 'OR') &&  isset($rStatus)){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_rstatus;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($rStatus)){
             $rStatusStr = implode("','", $rStatus) ;
             $rStatusStr1 = implode(",", $rStatus) ;
             
        }else{
            $rStatusStr = str_replace(',',"','",$rStatus);
            $rStatusStr1 = $rStatus ;
            $rStatus = explode(",",$rStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".ord_referred_status IN ('". $rStatusStr ."') ";        
        $condition_url   .= "&chk_rstatus=$chk_rstatus&rStatus=$rStatusStr1";
        $_SEARCH["chk_rstatus"]  = $chk_rstatus;
        $_SEARCH["rStatus"]     = $rStatus;        
    }    
    //EO: Referral Status data EOF
	
	
	
	//echo $condition_query;
	$condition_url .= "&rpp=$rpp"
                        ."&sString=$sString"
                        ."&sType=$sType"
                        ."&sOrderBy=$sOrderBy"
                        ."&sOrder=$sOrder"
                        ."&action=search";
			
	$_SEARCH["sString"] 	= $sString ;
	$_SEARCH["sType"] 		= $sType ;
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/bill-order-referred-choose-list.php');
?>