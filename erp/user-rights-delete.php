<?php
    if ( $perm->has('nc_urht_delete') ) {
        $right_id	= isset($_GET["right_id"]) 	? $_GET["right_id"] 	: ( isset($_POST["right_id"]) 	? $_POST["right_id"] : '' );
        
        $extra = array( 'db' 			=> &$db,
                     //   'userrights' 	=> $userrights,
                        'messages' 		=> $messages
                    );
        UserRights::delete($right_id, $messages, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/user-rights-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>