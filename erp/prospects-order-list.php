<?php

    if ( $perm->has('nc_ps_or_list') || $perm->has('nc_ps_or_search') ) {
		include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
		
		// Status
		$variables["blocked"]           = ProspectsOrder::BLOCKED;
		$variables["active"]            = ProspectsOrder::ACTIVE;
		$variables["suspended"]           = ProspectsOrder::SUSPENDED;
		$variables["deleted"]           = ProspectsOrder::DELETED;
		$variables["completed"]         = ProspectsOrder::COMPLETED;
		$variables["processed"]         = ProspectsOrder::PROCESSED;
        //$variables["followup_type"]  	= FOLLOWUP::ORDER;
        
	   
        if ( !isset($condition_query) || $condition_query == '' ) {
            $_SEARCH["chk_status"]  = 'AND';
			$_SEARCH["sStatus"]   = array(ProspectsOrder::ACTIVE, ProspectsOrder::COMPLETED,ProspectsOrder::SUSPENDED,ProspectsOrder::PROCESSED);  
			//$_SEARCH["chk_lstatus"]  = 'AND';
			//$_SEARCH["lStatus"]   = array(ProspectsOrder::HOTLSTATUS);  
            $condition_query = '';			 
            $condition_query = " WHERE (". TABLE_PROSPECTS_ORDERS .".status IN('".ProspectsOrder::ACTIVE."','".ProspectsOrder::COMPLETED."','".ProspectsOrder::SUSPENDED."','".ProspectsOrder::PROCESSED."'".")  )"; 
			//AND ".TABLE_PROSPECTS_ORDERS .".lead_status IN('". ProspectsOrder::ACTIVE ."')
        }
		
		 
		if ( $perm->has('nc_ps_or_list_all') ) { 
		
		 
		}else{ 
			if(empty($_SEARCH["search_executive_id"]) || empty($_SEARCH["user_id"]) ){
				$condition_query .= " AND ( ";
				$condition_query .= " (". TABLE_PROSPECTS_ORDERS .".order_closed_by = '". $my['user_id'] ."' "."  ) "; 
				$condition_query .= " OR ( "
											. TABLE_PROSPECTS_ORDERS .".team LIKE '%,". $my['user_id'] .",%' "
										. "  "
										."  ) "; 
				// Check if the User has the Right to view Orders created by other Users.
				$condition_query .= " )";   				
				$condition_url  .= "&user_id=$user_id";
				$_SEARCH["user_id"]  = $user_id;
			}    
		}
		
		
		if( $sOrderBy=='delay'){
			$condition_query .= " ORDER BY DATEDIFF( CURDATE() , ".TABLE_PROSPECTS_ORDERS.".do_c )  ". $sOrder ;        
		}else{
			$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;        
		}
 
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
        //By default search in On
        $_SEARCH["searched"]    = true ;
        
        if($searchStr==1){
          
            $fields = TABLE_PROSPECTS_ORDERS.'.id ' ;
            $total	=	ProspectsOrder::getDetails( $db, $list, '', $condition_query);
            $extra_url  = '';
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            $list	= NULL;
            $fields = TABLE_PROSPECTS_ORDERS .'.id'
                    .','. TABLE_PROSPECTS_ORDERS .'.number'
                    .','. TABLE_PROSPECTS_ORDERS .'.order_title'
                    .','. TABLE_PROSPECTS_ORDERS .'.access_level'
                    .','. TABLE_PROSPECTS_ORDERS .'.team'
                    .','. TABLE_PROSPECTS_ORDERS .'.details'
                    .','. TABLE_PROSPECTS_ORDERS .'.do_d'
                    .','. TABLE_PROSPECTS_ORDERS .'.do_c'
                    .','. TABLE_PROSPECTS_ORDERS .'.do_o'
                    .','. TABLE_PROSPECTS_ORDERS .'.status'
                    .','. TABLE_PROSPECTS_ORDERS .'.followup_status'
                    .','. TABLE_PROSPECTS_ORDERS .'.daily_update_status'
                    .','. TABLE_PROSPECTS_ORDERS .'.cron_created '
                    .','. TABLE_PROSPECTS_ORDERS .'.new_ref_ord_id'
                    .','. TABLE_PROSPECTS_ORDERS .'.new_ref_ord_no'
                    .','. TABLE_PROSPECTS_ORDERS .'.order_against_id'
                    .','. TABLE_PROSPECTS_ORDERS .'.order_against_no'
                    .','. TABLE_PROSPECTS_ORDERS .'.invoice_against_id'
                    .','. TABLE_PROSPECTS_ORDERS .'.invoice_against_no'
                    .','. TABLE_PROSPECTS_ORDERS .'.is_old' //For Old Migrated data 
                    .','. TABLE_PROSPECTS_ORDERS .'.old_updated' //For Old Migrated data
                    .','. TABLE_PROSPECTS_ORDERS .'.order_of'
                    .','. TABLE_PROSPECTS_ORDERS .'.old_particulars'
					.','. TABLE_PROSPECTS_ORDERS .'.lead_status'
					.','. TABLE_PROSPECTS_ORDERS .'.lead_closing_dt'
					.','. TABLE_PROSPECTS_ORDERS .'.lead_suspend_count'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by2'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by2_name'
					.','. TABLE_PROSPECTS_ORDERS .'.site_url'
					.',	  DATEDIFF(CURDATE() ,'.TABLE_PROSPECTS_ORDERS.'.do_c ) AS delay'
                    //.','. TABLE_PROSPECTS_QUOTATION .'.number as inv_no'
                    .','. TABLE_PROSPECTS .'.user_id AS c_user_id'
                    .','. TABLE_PROSPECTS .'.number AS c_number'
                    .','. TABLE_PROSPECTS .'.f_name AS c_f_name'
                    .','. TABLE_PROSPECTS .'.l_name AS c_l_name'
                    .','. TABLE_PROSPECTS .'.billing_name'
					.','. TABLE_PROSPECTS .'.site_url as site_urlp'
                    .','. TABLE_PROSPECTS .'.email AS c_email' 
                    .','. TABLE_PROSPECTS .'.status AS c_status';
            ProspectsOrder::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        } 
        
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){   
                $is_invoice_created = 0;
                if($val['is_old']){
                   $sql= " SELECT COUNT(*) as count FROM ".TABLE_PROSPECTS_QUOTATION." WHERE 
				   ".TABLE_PROSPECTS_QUOTATION.".or_no='".$val['number']."' 
                   AND ".TABLE_PROSPECTS_QUOTATION.".status !='".ProspectsQuotation::DELETED."' AND 
				   ".TABLE_PROSPECTS_QUOTATION.".old_updated ='1'"; 
                   //TABLE_BILL_INV.".old_updated ='1'"   Means Invoice template generated or not if 
				   //1 then invoice template generated
                   $db->query($sql);               
                   if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) {
                            $count = $db->f('count') ;
                            if( $count > 0){
                                $is_invoice_created = 1;
                            }
                        }
                    } 
                }else{ 
                   $sql= " SELECT COUNT(*) as count FROM ".TABLE_PROSPECTS_QUOTATION." WHERE 
				   ".TABLE_PROSPECTS_QUOTATION.".or_no='".$val['number']."' AND 
				   ".TABLE_PROSPECTS_QUOTATION.".status !='".ProspectsQuotation::DELETED."' LIMIT 0,1";
                   $db->query($sql);               
					if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) {
                            $count = $db->f('count') ;
                            if( $count > 0){
                                $is_invoice_created = 1;
                            }
                        }
                    }
                }
				//check order id followup in ST BOF
				$val['ticket_id']=0;
				$sql2 = "SELECT ticket_id FROM ".TABLE_PROSPECTS_TICKETS." WHERE 
				(".TABLE_PROSPECTS_TICKETS.".ticket_child=0 AND ".TABLE_PROSPECTS_TICKETS.".flw_ord_id = '".$val['id']."' AND ".TABLE_PROSPECTS_TICKETS.".ticket_type = '".ProspectsTicket::TYP_COMM."') LIMIT 0,1";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                        }
                    }                   
                }
				//check order id followup in ST EOF
				$val['team']=trim($val['team'],",");
				if(!empty($val['order_closed_by'])){
					$val['team']= $val['team'].",".$val['order_closed_by'] ;
				}
                $team = str_replace(',',"','",$val['team']);
				$sql= " SELECT f_name,l_name,user_id FROM ".TABLE_USER." WHERE ".TABLE_USER.".user_id IN ('".$team."') ";
                $db->query($sql);               
				$team_members ='';
                if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {	
						$tuser_id = $db->f('user_id');
						if($tuser_id==$val['order_closed_by']){
							$val['order_closed_by_name'] = $db->f('f_name')." ". $db->f('l_name') ;
						}else{
							$team_members .= $db->f('f_name')." ". $db->f('l_name').", <br/>" ;
						}
					}
                }   
				$team_members =trim($team_members,",");
				$val['team_members']=$team_members ;
				
                $is_particulars_selected = 0; 
                $sql= " SELECT id FROM ".TABLE_PROSPECTS_ORD_P." WHERE 
				".TABLE_PROSPECTS_ORD_P.".ord_no='".$val['number']."' LIMIT 0,1";
                $db->query($sql);               
                if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$count = $db->f('id') ;
						if( $count > 0){
							$is_particulars_selected = 1;
						}
					}
                }  
				$val['can_edit_lead'] = 0;   				
				$val['can_delete_lead'] = 0;   			 
				$val['can_create_invoice'] = 0;   				
				if( ( ( $val['order_closed_by']==$my['user_id'] ) || $perm->has('nc_ps_or_list_all')) && $perm->has('nc_ps_or_edit')  ){
					$val['can_edit_lead'] = 1;   					
				}
				if( ( ( $val['order_closed_by']==$my['user_id'] ) || $perm->has('nc_ps_or_list_all')) && $perm->has('nc_ps_or_delete')  ){
					$val['can_delete_lead'] = 1;   					
				}
				if( ( ( $val['order_closed_by']==$my['user_id'] ) || $perm->has('nc_ps_qt_list_all')) && $perm->has('nc_ps_qt_add')  ){
					$val['can_create_invoice'] = 1;   					
				}
                $val['is_invoice_created'] = $is_invoice_created;   
                $val['is_particulars_selected'] = $is_particulars_selected;                   
                $flist[$key]=$val;
            }
        }
        $variables["none"]       = ProspectsOrder::NONELSTATUS;
        $variables["hot"]        = ProspectsOrder::HOTLSTATUS;
		$variables["warm"]       = ProspectsOrder::WARMLSTATUS;
		$variables["cold"]       = ProspectsOrder::COLDLSTATUS;
		$variables["negative"]   = ProspectsOrder::NEGATIVELSTATUS;
		$variables["win"]        = ProspectsOrder::WINLSTATUS;
		$variables["loose"]      = ProspectsOrder::LOOSELSTATUS;
        
		//Get list of team bof
		if ( $perm->has('nc_ps_or_list_all') ) {   
			
		}else{ 
			$rteam=trim($my['my_reporting_members'],",");
            $team = str_replace(',',"','",$rteam);
			$ulist=array();
			$ulist[]= array('user_id'=>$my['user_id'],
						'number'=>$my['number'],
						'name'=>$my['f_name'].' '.$my['l_name']
					) ;
			$sql= " SELECT f_name,l_name,number,user_id FROM ".TABLE_USER." WHERE ".TABLE_USER.".user_id IN ('".$team."') ";
			$db->query($sql);   
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ) {
					$user_id=$db->f('user_id');
					$number = $db->f('number');
					$name= $db->f('f_name')." ". $db->f('l_name') ; 
					$ulist[]= array('user_id'=>$user_id,
						'number'=>$number,
						'name'=>$name
					) ; 
				}
			}  
		} 
		//Get list of team eof
		
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_view_list_all']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_create_invoice']= false;
        $variables['can_followup'] = false;
        $variables['can_bifurcate'] = false;
		$variables['can_view_inv'] = false;
        
        if ( $perm->has('nc_ps_or_list') ) {
            $variables['can_view_list'] = true;
        }
		if ( $perm->has('nc_ps_or_list_all') ) { 
		    $variables['can_view_list_all'] = true;
		}
        if ( $perm->has('nc_ps_or_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_ps_or_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_ps_or_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_ps_or_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_ps_or_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_ps_qt_add') ) {
            $variables['can_create_invoice'] = true;
        }
        if ( $perm->has('nc_pst') && $perm->has('nc_pst_flw_ord') ) {
            $variables['can_followup'] = true;
        }
		if( $perm->has('nc_ps_qt') && $perm->has('nc_ps_qt_list') ) {
            $variables['can_view_inv']     = true;
        }
		
		$variables['lstatus'] = ProspectsOrder::getLStatus(); 
		
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'ulist', 'value' => 'ulist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-order-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>