<?php
if ( $perm->has('nc_hr_cal_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        $month = HrCalendar::getMonth();
        $year = HrCalendar::getYear();
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( HrCalendar::validateUpdate($data, $extra) ) {
                $total_days=date("t",mktime(0,0,0,$data['month'],1,$data['year'])); 
                $query  = " UPDATE ". TABLE_HR_CALENDAR
                            ." SET ". TABLE_HR_CALENDAR .".month = '".		$data['month'] ."'"
                                    .",". TABLE_HR_CALENDAR .".month_name = '". $month[ $data['month'] ] ."'" 
                                   	.",". TABLE_HR_CALENDAR .".year = '". 		$data['year'] ."'"                               
                                   	.",". TABLE_HR_CALENDAR .".total_days = '".  $total_days ."'"                                
                                    .",". TABLE_HR_CALENDAR .".holidays = '". 		$data['holidays'] ."'"
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("HR Calendar entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_HR_CALENDAR .'.*'  ;            
            $condition_query = " WHERE (". TABLE_HR_CALENDAR .".id = '". $id ."' )";
            
            if ( HrCalendar::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                $id         = $_ALL_POST['id'];
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/hr-calendar-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'month', 'value' => 'month');     
            $page["var"][] = array('variable' => 'year', 'value' => 'year');   
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-calendar-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
