<?php
	if ( $perm->has('nc_vbk_delete') ) {
		$user_id	= isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'access_level' 	=> $my['access_level'],
						'messages' 		=> $messages
					);
		VendorsBank::delete($user_id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/vendors-bank-list.php');
	}
	else {
		$messages->setErrorMessage("You donot have the Right to Access this module.");
	}
?>