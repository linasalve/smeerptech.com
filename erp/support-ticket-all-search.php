<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
    if ( $sString != "" ) 
    {
		 // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
        //$where_added = true;
        $sub_query='';        
        if ( $search_table == "Any" ) 
        {
            
            if ( $where_added ) 
            {
                //$condition_query .= "OR ";
                $condition_query .= " AND ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ) 
            {
              
                if ( $table != "Any" ) 
                { 
                    
                    if ($table == TABLE_TICKET_ALL) 
                    {
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                          
                           if($field !='number' && $field !='or_no' && $field !='ticket_no' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }
                            
                            //$sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                        }
                        
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                            
                    }elseif($table == TABLE_USER){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            if($field !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }
                            
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_CLIENTS){
                    
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            if($field !='number'){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }
                           
                           
                            //$sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }
                    
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= "AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                if($sType !='number' && $sType !='or_no' && $sType !='ticket_no' ){
                    $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                }else{
                     $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ."='".trim($sString) ."' )" ;
                }
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
    
    
    // BO: Status data.
    $sStatusStr = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' )  && isset($sStatus)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_TICKET_ALL .".status IN ('". $sStatusStr ."') ";
        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;        
    }
    // EO: Status data.
    
    // BO: Ticket Status data.
    $tStatusStr = '';
    $chk_t_status = isset($_POST["chk_t_status"])   ? $_POST["chk_t_status"]  : (isset($_GET["chk_t_status"])   ? $_GET["chk_t_status"]   :'');
    $tStatus    = isset($_POST["tStatus"])      ? $_POST["tStatus"]     : (isset($_GET["tStatus"])      ? $_GET["tStatus"]      :'');
    if ( ($chk_t_status == 'AND' || $chk_t_status == 'OR' )  && isset($tStatus)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_t_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if(is_array($tStatus)){
             $tStatusStr = implode("','", $tStatus) ;
             $tStatusStr1 = implode(",", $tStatus) ;
             
        }else{
            $tStatusStr = str_replace(',',"','",$tStatus);
            $tStatusStr1 = $tStatus ;
            $tStatus = explode(",",$tStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_TICKET_ALL .".ticket_status IN ('". $tStatusStr ."') ";
        
        $condition_url          .= "&chk_t_status=$chk_t_status&tStatus=$tStatusStr1";
        $_SEARCH["chk_t_status"]  = $chk_t_status;
        $_SEARCH["tStatus"]     = $tStatus;        
    }
    // EO: Ticket Status data.
    
    
   
    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR' ) && !empty($date_from)) {
        
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        $dfa = explode('/', $date_from);       
       // $mktimefrm = mktime(0,0,0,$dfa[1],$dfa[0],$dfa[2]);
        
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
         
        $condition_query .= " ". TABLE_TICKET_ALL .".do_e >= '". $dfa ."'";
        
        //$condition_query .= " ". TABLE_TICKET_ALL .".ticket_date >= '". $mktimefrm ."'";        
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
    }  
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        //$mktimeto = mktime(60,0,0,$dta[1],$dta[0],$dta[2]);
        
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        
        $condition_query .= " ". TABLE_TICKET_ALL .".do_e <= '". $dta ."'";
        //$condition_query .= " ". TABLE_TICKET_ALL .".ticket_date <= '". $mktimeto ."'";
     
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
    }
   
    //exit;
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
      
    include ( DIR_FS_NC .'/support-ticket-all-list.php');
?>