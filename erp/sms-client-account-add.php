<?php
  if ( $perm->has('nc_sms_ca_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL; 
        $lst_api = $lst_sender = array();
        
        $fields = TABLE_SMS_API .'.id'
                   .','. TABLE_SMS_API .'.name'
                   .','. TABLE_SMS_API .'.gateway_name'
                   .','. TABLE_SMS_API .'.is_scrubbed';
        $condition_query= "WHERE status='".SmsApi::ACTIVE."'";
        SmsApi::getList($db,$lst_api,$fields,$condition_query);
		
		$fields0 = TABLE_SMS_SENDER .'.id'.','. TABLE_SMS_SENDER .'.sender_name' ;
		$condition_query0 = " WHERE status='".SmsSender::ACTIVE."' ";
		SmsSender::getList($db,$lst_sender,$fields0,$condition_query0);  
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                if(!empty($data['api_id'])){ 
					$fields1 = TABLE_SMS_API .'.id'.','. TABLE_SMS_API .'.name'.','. TABLE_SMS_API .'.gateway_id'
								.','. TABLE_SMS_API .'.is_scrubbed';
                    $condition_query1= " WHERE id='".$data['api_id']."'";
                    SmsApi::getList($db,$gateway,$fields1,$condition_query1);  
					if(!empty($gateway)){
						$gateway_id= $gateway[0]['gateway_id'];
						$is_scrubbed= $gateway[0]['is_scrubbed'];
					}
					// 					
					$fields0 = TABLE_SMS_SENDER .'.id'.','. TABLE_SMS_SENDER .'.sender_name' ;
                    $condition_query0 = " WHERE status='".SmsSender::ACTIVE."' AND api_id='".$data['api_id']."'";
                    SmsSender::getList($db,$lst_sender,$fields0,$condition_query0);  
                }
				if(!empty($data['sender_id'])){  
					$sender_id = implode(',',$data['sender_id']);
					$sender_id = ','.$sender_id.',';
				} 
                if ( SmsClientAccount::validateAdd($data, $extra) ){ 
                    $query	= " INSERT INTO ".TABLE_SMS_CLIENT_ACCOUNT
						." SET ".TABLE_SMS_CLIENT_ACCOUNT .".gateway_id = '". $gateway_id ."'"  
						.",". TABLE_SMS_CLIENT_ACCOUNT .".api_id 		= '". $data['api_id'] ."'"                                
						.",". TABLE_SMS_CLIENT_ACCOUNT .".is_scrubbed	= '". $is_scrubbed ."'"                                
						.",". TABLE_SMS_CLIENT_ACCOUNT .".client_id		= '". $data['client_id'] ."'"                                
						.",". TABLE_SMS_CLIENT_ACCOUNT .".name 	        = '". $data['name'] ."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT .".credit_sms    = '". $data['credit_sms'] ."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT .".balance_sms 	= '". $data['credit_sms'] ."'"                    
						//.",". TABLE_SMS_CLIENT_ACCOUNT .".unit_price 	= '". $data['unit_price'] ."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT .".sender_id 		= '". $sender_id ."'"                    
						//.",". TABLE_SMS_CLIENT_ACCOUNT .".do_expiry 		= '". date('Y-m-d H:i:s',$data['do_expiry'])."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT .".created_by 		= '". $my['user_id'] ."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT .".created_by_name 	= '". $my['f_name']." ".$my['l_name'] ."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT .".ip 			    = '". $_SERVER['REMOTE_ADDR'] ."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT .".do_e            = '". date('Y-m-d H:i:s')."'";
                    $db->query($query); 
					$sms_client_acc_id = $db->last_inserted_id(); 
					 
					// History details 
					 	$query	= " INSERT INTO ".TABLE_SMS_CLIENT_ACCOUNT_HISTORY
						." SET ".TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".sms_count 	= '".$data['credit_sms']."'"  
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".credit_account_id 	= '". $sms_client_acc_id ."'"  
						//.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".credit_do_expiry 	= '". $allot_acct['do_expiry'] ."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".credit_total 	= '". $data['credit_sms'] ."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".credit_balance 	= '". $data['credit_sms'] ."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".deduct_account_id 	= ''" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".deduct_do_expiry 	= ''" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".deduct_total 	= ''" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".deduct_balance 	= ''" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".created_by		= '". $my['user_id']."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".created_by_name = '". $my['f_name']." ".$my['l_name'] ."'" 
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".ip 	= '". $_SERVER['REMOTE_ADDR'] ."'"                    
						.",". TABLE_SMS_CLIENT_ACCOUNT_HISTORY .".do_e      = '". date('Y-m-d H:i:s')."'";      
						$db->query($query); 
					 
					  $messages->setOkMessage("Sms Client account has been done.");
					 
					if(!empty($data['credit_sms'])){ 
						$sql1=  " UPDATE ". TABLE_CLIENTS ." SET
							". TABLE_CLIENTS .".balance_sms = (".TABLE_CLIENTS .".balance_sms - ". $data['credit_sms'] ."), 
							". TABLE_CLIENTS .".consumed_sms = (".TABLE_CLIENTS .".consumed_sms + ". $data['credit_sms'] .") 
							WHERE ".TABLE_CLIENTS.".user_id = '".$_ALL_POST['client_id']."'" ;
						$db->query($sql1);
					}
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
               }
        }               
       
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sms-client-account.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sms-client-account.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sms-client-account.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');    
            $page["var"][] = array('variable' => 'lst_gateway', 'value' => 'lst_gateway');                 
            $page["var"][] = array('variable' => 'lst_api', 'value' => 'lst_api');     
            $page["var"][] = array('variable' => 'lst_sender', 'value' => 'lst_sender');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-client-account-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
