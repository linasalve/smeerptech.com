<?php
    if ( $perm->has('nc_site_menu_status') ) {
	$m_id	= isset($_GET["m_id"]) 	? $_GET["m_id"] 	: ( isset($_POST["m_id"]) 	? $_POST["m_id"] : '' );
	$status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
	
	$extra = array( 'db' 		=> &$db,
					'messages' 	=> $messages
				);
	MenuSetting::updateStatus($m_id, $status, $extra);
	
	// Display the Webpage list.
	include ( DIR_FS_NC .'/menu-settings-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to change the status.");
    }
?>