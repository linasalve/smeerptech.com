<?php

function daysDifference($endDate, $beginDate)
{
   //explode the date by "-" and storing to array
   $date_parts1=explode("-", $beginDate);
   $date_parts2=explode("-", $endDate);
   //gregoriantojd() Converts a Gregorian date to Julian Day Count
   $start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
   $end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
   return $end_date - $start_date;
}
//echo " aug days ".daysDifference('2010-08-31','2010-08-01');

//

//date("Y-m-d", mktime(0,0,0,$month,$day-1,$year);
$date = strtotime('2010-08-01 next sunday');
//$date = strtotime('2010-08-01');
$dateMax = strtotime('2010-08-31');

function calTotalDays($date,$dateMax){
	$nbr = 0;
	while ($date < $dateMax) {
	  $dateArr[] = date('Y-m-d', $date);
	  $nbr++;
	  $date += 7 * 24 * 3600;
	}
	$data['dates']=$dateArr;
	$data['totalDays']=$nbr;
}
//var_dump($nbr);
//



if ( $perm->has('nc_rp_ex_m_att') ) {   

    
    include_once ( DIR_FS_INCLUDES .'/hr-attendance.inc.php' );
    
    $date_from	= isset($_GET["date_from"]) 	? $_GET["date_from"]	: ( isset($_POST["date_from"]) 	? $_POST["date_from"] : '' );
    $date_to= isset($_GET["date_to"]) 	? $_GET["date_to"]	: ( isset($_POST["date_to"]) 	? $_POST["date_to"] : '' );
    $member_id= isset($_GET["member_id"]) 	? $_GET["member_id"]	: ( isset($_POST["member_id"]) 	? $_POST["member_id"] : '' );
    $_SEARCH["date_from"] 	= $date_from;
    $_SEARCH["date_to"] 	= $date_to;
    $_SEARCH["member_id"] 	= $member_id;
    
	
	
    $_ALL_POST = $_GET 	? $_GET	: ($_POST 	? $_POST : '' );
    $extra_url=$pagination ='';
    $reportList=array();
    $db1 		= new db_local;
    $dateList=array();
   // BO: Read the Team members list ie member list in dropdown box BOF
    $lst_executive = Null;
	$condition = "WHERE status='1'";
    $fields = TABLE_USER .'.user_id'
               .','. TABLE_USER .'.f_name'
               .','. TABLE_USER .'.l_name';
    User::getList($db,$lst_executive,$fields,$condition);
    // BO:  Read the Team members list ie member list in dropdown box  EOF
     
     
    //to view report date should be compulsary
    if(array_key_exists('submit_search',$_ALL_POST)){
    
        
         $data		= processUserData($_ALL_POST);
         
         $extra = array( 'db' 				=> &$db,
                        'messages'          => &$messages
                    );          
                    
        if (Reports::validateExecutiveMonthAttAdd($data, $extra) ) {
		
			$temp = explode('/', $date_from);
			$df = $temp[2] .'-'. $temp[1] .'-'. $temp[0] ;
			$fy=$temp[2];
			$fm=$temp[1];
			$fd=$temp[0];
			$temp = explode('/', $date_to);
			$td = $temp[2] .'-'. $temp[1] .'-'. $temp[0] ;
			$ty=$temp[2];
			$tm=$temp[1];
			$td=$temp[0];
			//total days
			$totalDays= daysDifference($df,$td)  + 1;
			//total sundays 
			$df1= date("Y-m-d", mktime(0,0,0,$fm,$fd-1,$fy));
			$dateMin = strtotime($df1.' next sunday');
			$dateMax = strtotime($dt);
			$daysArr1=calTotalDays($dateMax,$dateMin);
            
            $condition_url='&perform=exec-month-att&submit_search=1&member_id='.$member_id."&date_from=".$date_from."&date_to=".$date_to;
            
			//Total Present Days Bof
			$sql1 = " SELECT COUNT(".TABLE_HR_ATTENDANCE.".att_id) as presentDays FROM 
			".TABLE_HR_ATTENDANCE." LEFT JOIN ".TABLE_USER." 
			ON ".TABLE_USER.".user_id = ".TABLE_HR_ATTENDANCE.".uid					
			WHERE ".TABLE_HR_ATTENDANCE.".uid='".$data['member_id']."' AND 
			(DATE_FORMAT(".TABLE_HR_ATTENDANCE.".date,'%Y-%m-%d') >='".date('Y-m-d',$data['date_from'])."' AND	
			DATE_FORMAT(".TABLE_HR_ATTENDANCE.".date,'%Y-%m-%d') <='".date('Y-m-d',$data['date_to'])."') AND ".
			TABLE_HR_ATTENDANCE.".pstatus='1' ";
			$db->query($sql1) ;
            if($db->nf() > 0){
				while($db->next_record()){
					$presentDays = $db->f('presentDays') ;
				}                
            }
			
			$sql1 = " SELECT COUNT(".TABLE_HR_ATTENDANCE.".att_id) as halfDays FROM 
			".TABLE_HR_ATTENDANCE." LEFT JOIN ".TABLE_USER." 
			ON ".TABLE_USER.".user_id = ".TABLE_HR_ATTENDANCE.".uid					
			WHERE ".TABLE_HR_ATTENDANCE.".uid='".$data['member_id']."' AND 
			(DATE_FORMAT(".TABLE_HR_ATTENDANCE.".date,'%Y-%m-%d') >='".date('Y-m-d',$data['date_from'])."' AND	
			DATE_FORMAT(".TABLE_HR_ATTENDANCE.".date,'%Y-%m-%d') <='".date('Y-m-d',$data['date_to'])."') AND ".
			TABLE_HR_ATTENDANCE.".worked_hrs<='05:00:00' ";
			$db->query($sql1) ;
            if($db->nf() > 0){
				while($db->next_record()){
					$halfDays = $db->f('halfDays') ;
				}                
            }
			
			//Total Present Days Eof
              
					
			 $sql =" SELECT ".TABLE_HR_ATTENDANCE.".*,".TABLE_USER.".f_name,".TABLE_USER.".l_name
                FROM ".TABLE_HR_ATTENDANCE." LEFT JOIN ".TABLE_USER." ON 
				".TABLE_USER.".user_id =".TABLE_HR_ATTENDANCE.".uid WHERE uid='".$data['member_id']."' AND  
				(DATE_FORMAT(".TABLE_HR_ATTENDANCE.".date,'%Y-%m-%d') >='".date('Y-m-d',$data['date_from'])."' 
				AND	DATE_FORMAT(".TABLE_HR_ATTENDANCE.".date,'%Y-%m-%d') <='".date('Y-m-d',$data['date_to'])."') 	
				ORDER BY ".TABLE_HR_ATTENDANCE.'.date' ;
            
            
			/*echo $sql =" SELECT ".TABLE_HR_ATTENDANCE.".*,".TABLE_USER.".f_name,".TABLE_USER.".l_name
                    FROM ".TABLE_HR_ATTENDANCE." LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id =".TABLE_HR_ATTENDANCE.".uid					
					WHERE ".TABLE_HR_ATTENDANCE.".uid='".$data['member_id']."' AND  
					( ".TABLE_HR_ATTENDANCE.".date >='".$data['date_from']."' AND ".TABLE_HR_ATTENDANCE.".date  <='". $data['date_to']."')" ;
			*/
            $db->query($sql) ;
            if($db->nf() > 0){
                $total = $db->nf() ;
            }
            else{
                $total = 0 ;
            }
            //Get total Hrs work done bof			
			$sql1 =" SELECT sec_to_time( sum( extract( HOUR FROM `worked_hrs` ) *3600 + extract(MINUTE FROM `worked_hrs` ) *60 ) )
			AS total_mv_time FROM ".TABLE_HR_ATTENDANCE." LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id =".TABLE_HR_ATTENDANCE.".uid WHERE uid='".$data['member_id']."' AND 
			(DATE_FORMAT(".TABLE_HR_ATTENDANCE.".date,'%Y-%m-%d') >='".date('Y-m-d',$data['date_from'])."' AND	
			DATE_FORMAT(".TABLE_HR_ATTENDANCE.".date,'%Y-%m-%d') <='".date('Y-m-d',$data['date_to'])."')" ;
			$db->query($sql1) ;
			while($db->next_record()){
				if($db->nf() > 0){
				    $worked_tot_hrs = $db->f('total_mv_time');
				}else{
					$worked_tot_hrs = 0 ;
				}
			}
            //Get total Hrs work done eof
			
			
			
			
			
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
			
            $query .= " LIMIT ". $next_record .", ". $rpp ;

            $db->query($sql) ;
            
            while($db->next_record()){
              $name=$db->f('f_name')." ".$db->f('l_name');
              
              $date=$db->f('date');
              $att_time_in= $db->f('att_time_in');
			  $att_time_in1 = substr($att_time_in, 0, 5); 
              $att_time_out=$db->f('att_time_out');
			  $att_time_out1 = substr($att_time_out, 0, 5); 
              $worked_hrs=$db->f('worked_hrs');
              $pstatus=$db->f('pstatus');
			  $pstatus_name='ABSENT';
			  if($pstatus ==1){
				$pstatus_name = 'PRESENT';
			  }elseif($pstatus ==2){
			    $pstatus_name = 'ABSENT';
			  }elseif($pstatus ==3){
			    $pstatus_name = 'PAIDLEAVE';
			  }elseif($pstatus ==4){
				$pstatus_name = 'NON PAIDLEAVE';
			  }
               if(empty($att_time_in) && !empty($att_time_out)){
					$pstatus_name = 'ABSENT';
			   }
                $list[] = array(
                                'att_time_in'=>synchronizeTime($datetime->dateToUnixTime($att_time_in1), $my["time_offset"] ),
                                'att_time_out'=>$att_time_out1,
                                'name'=>$name,
                                'pstatus_name'=>$pstatus_name,
                                'worked_hrs'=>$worked_hrs,
                                'date'=>$date
                                );
            }
         
          
            
            
           
        }
    }   

	$workingDays = 0 ;
	if(!empty($data['date_from']) && !empty($data['date_to'])){
	
		echo $dfa=date('Y-m-d',$data['date_from']) ;
		echo $dta=date('Y-m-d',$data['date_to']) ;

		$holidays=array();
		$workingDays = getWorkingDays($dfa,$dta,$holidays);
		$absentDays = $workingDays - $presentDays ;
	}
	
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
   
        
 
 
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'dateList', 'value' => 'dateList');
    $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
    $page["var"][] = array('variable' => 'worked_tot_hrs', 'value' => 'worked_tot_hrs');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'presentDays', 'value' => 'presentDays');
    $page["var"][] = array('variable' => 'halfDays', 'value' => 'halfDays');
    $page["var"][] = array('variable' => 'workingDays', 'value' => 'workingDays');
    $page["var"][] = array('variable' => 'absentDays', 'value' => 'absentDays');
    
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-executive-months-att.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>