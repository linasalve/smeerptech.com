<?php   

if( $perm->has('nc_bl_inv_flw')){
    
    $inv_id = isset ($_GET['inv_id']) ? $_GET['inv_id'] : ( isset($_POST['inv_id'] ) ? $_POST['inv_id'] :'');
    $client_id = isset ($_GET['client_id']) ? $_GET['client_id'] : ( isset($_POST['client_id'] ) ? $_POST['client_id'] :'');
    
    include ( DIR_FS_INCLUDES .'/sale-followup-title.inc.php');
    $_ALL_POST	    = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
    /*
    if ( $perm->has('nc_ld_flw') ) {
        $access_level += 1;
    }
    */
    
    $flwnumber="";
    
    if(!empty($client_id)){    
     
        $query="SELECT followup_no FROM ".TABLE_SALE_FOLLOWUP." WHERE client='".$client_id."' AND followup_of='".$inv_id."'";
        $db->query($query);
        
        if($db->nf()>0){
            $db->next_record();
            $flwnumber=$db->f('followup_no');
        }
    }
    
 	if (!empty($flwnumber) ){
                
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=12;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        }        
        $_ALL_POST['date'] = date('d/m/Y');
        
        $lst_typ =array('1'=>'AM','2'=>'PM');
    
    	// Read the available titles
        $titles = NULL;
        $required_fields ='*';
		FollowupTitle::getList($db,$titles,$required_fields); 
    	
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
        
          $_ALL_POST  = $_POST;
          
          $data = processUserData($_ALL_POST);
    
            $extra = array( 'db'        => &$db,
                    'access_level'      => $access_level,
                    'messages'          => &$messages
                );
  
            if ( Followup::validateFollowupAdd($data, $extra) ) {
                // insert into followup of lead
                if ( isset($data['date']) && !empty($data['date']) ) {
                     $data['date'] = explode('/', $data['date']);
                     $data['date'] =$data['date'][2]."-".$data['date'][1]."-".$data['date'][0] ;
                }
                
                $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                        ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"
                            .",". TABLE_SALE_FOLLOWUP .".title_id 		= '". $data ['title_id'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".remarks 		= '". $data ['remarks'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".date    		= '". $data['date'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".time    		= '". $data ['hrs'].':'.$data ['min'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".time_type 		= '". $data ['time_type'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                            //.",". TABLE_SALE_FOLLOWUP .".lead_id    	= '". $data ['lead_id'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data ['client_id'] ."'"
                            //.",". TABLE_SALE_FOLLOWUP .".assign_to 		= '". $data ['assign_to'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $inv_id ."'"
                            .",". TABLE_SALE_FOLLOWUP .".table_name    	= 'billing_inv'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". FOLLOWUP::INVOICE ."'"
                            .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
                $db->query($query); 
            }
        }
         // get list of all comments of change log BOF
        $condition_query1 = $condition_url1 =$extra_url= '';
        $condition_url1 .="&perform=".$perform."&client_id=".$client_id;
        $perform = 'followup';
        $followupLog	= 	NULL;
        $fields = TABLE_SALE_FOLLOWUP.".id " ;
        $condition_query1 = " WHERE ".TABLE_SALE_FOLLOWUP.".followup_no = '".$flwnumber."' AND followup_of = '".$inv_id."' AND table_name = 'billing_inv'";
        //$condition_query1 = " WHERE ".TABLE_SALE_FOLLOWUP.".followup_no = '".$flwnumber."' " ;
        $condition_query1 .= " ORDER BY ".TABLE_SALE_FOLLOWUP.".do_e DESC";
        $total	=	Followup::getDetailsCommLog( $db, $followupLog, $fields , $condition_query1);
       
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
        //$extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  .= "&perform=".$perform;
        $extra_url  .= "&client_id=".$client_id ;
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $followupLog	= NULL;
        $fields = TABLE_SALE_FOLLOWUP .'.*, '.TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name'.','.TABLE_SALE_FOLLOWUP_TITLE .'.title';   
        Followup::getDetailsCommLog( $db, $followupLog, $fields, $condition_query1, $next_record, $rpp);
        $condition_url .="&perform=".$perform;
        
        $fList=array();
        if(!empty($followupLog)){
            foreach( $followupLog as $key=>$val){  
               $fList[$key]=$val;
            }
        }
        
        $hidden[] = array('name'=> 'client_id', 'value' => $client_id);
        $hidden[] = array('name'=> 'inv_id', 'value' => $inv_id);
        $hidden[] = array('name'=> 'perform', 'value' => 'followup');
        $hidden[] = array('name'=> 'act', 'value' => 'save');
        
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        $page["var"][] = array('variable' => 'followupLog', 'value' => 'fList');
        $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
        $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
        $page["var"][] = array('variable' => 'lst_typ', 'value' => 'lst_typ');
        $page["var"][] = array('variable' => 'titles', 'value' => 'titles'); 
        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-followup.html');
    }
   	else{
        $messages->setErrorMessage("Records not found.");
    }
    
}else{
    $messages->setErrorMessage("You do not have the Permission to access this module.");
}   
   
        
        
    
?>