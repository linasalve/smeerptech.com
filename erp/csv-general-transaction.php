<?php
    if ( $perm->has('nc_nwlm_csv_upload') ) {
        $_ALL_POST  = NULL;
        $data       = NULL;
      
		include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
		
        $CSV = CsvGeneral::getRestrictions();
      
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST); 			
            $extra = array( 'file'      => $_FILES,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );
			
            if ( CsvGeneral::validateUpload($data, $extra) ) {
				
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $invalid  = 0;
                $empty      = 0;
                $index      = array();
                $handle = fopen($_FILES['file_csv']['tmp_name'],"rb");
			    
                $message = '';
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
					
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
                    if ( $row == 1 ) {
                          $num = count($data); 
                        for ($i=0; $i < $num; $i++) {
                            $case = trim($data[$i]);
                            switch ($case) {
                                case ('executive_id'):{
                                    $index['executive_id'] = $i;
                                    break;
                                }
								case ('do_transaction'): {
                                    $index['do_transaction'] = $i;
                                    break;
                                }
								case ('debit_pay_amt'): {
                                    $index['debit_pay_amt'] = $i;
                                    break;
                                }
								case ('pay_received_amt_word'): {
                                    $index['pay_received_amt_word'] = $i;
                                    break;
                                }                               
                                case ('particulars'): {
                                    $index['particulars'] = $i;
                                    break;
                                }
                                                
                            }
                        }
                    }
                    else {                 
                          $index= array (
							"executive_id"    =>"0",
							"do_transaction"    =>"1",
                            "debit_pay_amt" => "2",
                            "pay_received_amt_word" =>"3",                           
                            "particulars" =>"4" 
                           );
						   
                        if(!empty($data[$index['executive_id']]) && !empty($data[$index['debit_pay_amt']]) ){
                             
                            $dt= $data[$index['do_transaction']];
							$temp = explode('/', $dt);
							$data['do_transaction'] = $temp[2]."-".$temp[0]."-".$temp[1];
							
							//Insert Newsletter Members BOF						 
							$number = Paymenttransaction::getNewNumber($db);
							$user_id = '4df83558e5b6d3592d8998e9b552d37a';
							
				echo	$query	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION
					." SET "
			.TABLE_PAYMENT_TRANSACTION.".executive_id='". processUserData($data[$index['executive_id']])."'" 
			.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_words 
			= '".processUserData($data[$index['pay_received_amt_word']])."'"  
			.",". TABLE_PAYMENT_TRANSACTION .".particulars = '".processUserData($data[$index['particulars']])."'"  
			.",". TABLE_PAYMENT_TRANSACTION .".user_id = '". $user_id ."'"
			.",". TABLE_PAYMENT_TRANSACTION .".is_auto_entry = '1'"
			.",". TABLE_PAYMENT_TRANSACTION .".account_head_id = '93'"
			.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = '67'"
			.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_amt='".processUserData($data[$index['debit_pay_amt']])."'"
			.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt='".processUserData($data[$index['debit_pay_amt']])."'"
	.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt='".processUserData($data[$index['debit_pay_amt']])."'"
	.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_inr='".processUserData($data[$index['debit_pay_amt']])."'"
	.",".TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt_inr='".processUserData($data[$index['debit_pay_amt']])."'"
			.",". TABLE_PAYMENT_TRANSACTION .".do_voucher='".processUserData($data['do_transaction'])."'"
			.",". TABLE_PAYMENT_TRANSACTION .".do_transaction='".processUserData($data['do_transaction'])."'"
			.",". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::PENDING."'" //cash
			.",". TABLE_PAYMENT_TRANSACTION .".mode_id = '2'" //cash
			.",". TABLE_PAYMENT_TRANSACTION .".currency_abbr = 'INR'"
			.",". TABLE_PAYMENT_TRANSACTION .".currency_id = '1'"
			.",". TABLE_PAYMENT_TRANSACTION .".currency_name = 'Rupees'"
			.",". TABLE_PAYMENT_TRANSACTION .".currency_symbol = 'Rs'"
			.",". TABLE_PAYMENT_TRANSACTION .".currency_country  = 'India'"
			.",". TABLE_PAYMENT_TRANSACTION .".exchange_rate  = '1'"
			.",". TABLE_PAYMENT_TRANSACTION .".company_id = '1'"
			.",". TABLE_PAYMENT_TRANSACTION .".company_name = 'SMEERP E-Technologies Pvt. Ltd.'"
			.",". TABLE_PAYMENT_TRANSACTION .".company_prefix = 'PET'"
			.",". TABLE_PAYMENT_TRANSACTION .".transaction_type = '".Paymenttransaction::PAYMENTOUT."'"
			.",". TABLE_PAYMENT_TRANSACTION .".ip    = '". $_SERVER['REMOTE_ADDR'] ."'"
			.",". TABLE_PAYMENT_TRANSACTION .".number = '". $number ."'"
			.",". TABLE_PAYMENT_TRANSACTION .".date= '". 	date('Y-m-d H:i:s')."'";
							
							$db->query($query); 
							$added += 1;
							//Insert Newsletter Members EOF 
                        }else {
                            $empty += 1;
                        }    
                         
                    }                    
                }
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been added.');
                if ($duplicate)
                    $messages->setOkMessage($duplicate .' out of '. $row .' records were duplicate and neglected.');
                if ($invalid)
                    $messages->setOkMessage($invalid .' out of '. $row .' email invalid records found. <br/>'.$message);
				if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
        }
        
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
        /* if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$condition ='';
            include ( DIR_FS_NC .'/newsletters-members-csv.php');
        }
        else { */
			
			 
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'tran-csv');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            //$page["var"][] = array('variable' => 'table_list', 'value' => 'table_list');
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'csv-general-transaction.html');
        //}
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>