<?php
if ( $perm->has('nc_st_t_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		include_once (DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
		
		$displayList = SupportTicketTemplate::getDisplayList();
		
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);   
			
			$data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_REPOSITORY_FILE_SIZE     ;
			
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if( SupportTicketTemplate::validateAdd($data, $extra)) {
				
				$data['number'] = SupportTicketTemplate::getNewAccNumber($db);
				$template_categoryStr=$template_categoryStrName ='';
				if(!empty($data['template_category'])) {
                    $template_category1=implode(",",$data['template_category']);
					$template_category2= str_replace(",","','",$template_category1);
                    $template_categoryStr = ",".$template_category1.",";
					
					$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE_CATEGORY.".id IN ('".$template_category2."')";
					StTemplateCategory::getList( $db, $list_st, TABLE_ST_TEMPLATE_CATEGORY.'.category', 
					$condition_query_st);
					 
					if(!empty($list_st)){
						foreach($list_st as $key=>$val){
							$template_categoryStrName.= $val['category'].",";
						}
						$template_categoryStrName = trim($template_categoryStrName,",");
					}
					
				}
				 
				
				if(!empty($data['file_1'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_1']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-1".".".$ext ;
					$data['file_1'] = $attachfilename;					
					if(move_uploaded_file($files['file_1']['tmp_name'], DIR_FS_ST_TEMPLATES_FILES."/".$attachfilename)){
					   
					  @chmod(DIR_FS_ST_TEMPLATES_FILES."/".$attachfilename,0777);
					}
				}
				if(!empty($data['file_2'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_2']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-2".".".$ext ;
					$data['file_2'] = $attachfilename;                    
					if (move_uploaded_file ($files['file_2']['tmp_name'], DIR_FS_ST_TEMPLATES_FILES."/".$attachfilename)){
					   @chmod(DIR_FS_ST_TEMPLATES_FILES."/".$attachfilename,0777);
					}
				}
				if(!empty($data['file_3'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_3']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-3".".".$ext ;
					$data['file_3'] = $attachfilename;                    
					if (move_uploaded_file ($files['file_3']['tmp_name'], DIR_FS_ST_TEMPLATES_FILES."/".$attachfilename)){
					   @chmod(DIR_FS_ST_TEMPLATES_FILES."/".$attachfilename,0777);
					}
				}
				
  			    $show_instr='';
                if(isset($_POST['show_in']) && !empty($_POST['show_in'])){
                    $show_in_ids=$_POST['show_in'];
                    $show_in_idstr=implode(",",$show_in_ids);
                    $show_instr = ",".$show_in_idstr.",";	
                } 
				
				$query	= " INSERT INTO ".TABLE_ST_TEMPLATE
						." SET ".TABLE_ST_TEMPLATE .".title = '".  $data['title'] ."'" 
						.",". TABLE_ST_TEMPLATE .".number = '".   $data['number'] ."'"
						.",". TABLE_ST_TEMPLATE .".template_category = '". $template_categoryStr ."'"
						.",". TABLE_ST_TEMPLATE .".template_category_name = '". $template_categoryStrName ."'"
						.",". TABLE_ST_TEMPLATE .".subject = '".   $data['subject'] ."'"
						.",". TABLE_ST_TEMPLATE .".details = '".   $data['details'] ."'"
						.",". TABLE_ST_TEMPLATE .".show_in = '".   $show_instr."'"
						.",". TABLE_ST_TEMPLATE .".file_1 = '".    $data['file_1'] ."'"
						.",". TABLE_ST_TEMPLATE .".file_2 = '".    $data['file_2'] ."'"
						.",". TABLE_ST_TEMPLATE .".file_3 = '".    $data['file_3'] ."'"
						.",". TABLE_ST_TEMPLATE .".status = '". 		$data['status'] ."'"                         
						.",". TABLE_ST_TEMPLATE .".access_level = '".   $my['access_level'] ."'"
						.",". TABLE_ST_TEMPLATE .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"
						.",". TABLE_ST_TEMPLATE .".created_by = '".     $my['user_id'] ."'"
						.",". TABLE_ST_TEMPLATE .".do_e   = '". 		date('Y-m-d H:i:s')."'";
		
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/support-ticket-template.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/support-ticket-template.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/support-ticket-template.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'displayList', 'value' => 'displayList');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'support-ticket-template-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
