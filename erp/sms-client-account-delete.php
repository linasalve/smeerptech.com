<?php
      if ( $perm->has('nc_sms_ca_edit') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
        /*
        if ( $perm->has('nc_ab_delete_al') ) {
            $access_level += 1;
        }*/
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        SmsClientAccount::delete($id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/sms-client-account-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the entry.");
        // $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
