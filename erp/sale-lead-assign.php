<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page

    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
     $condition_query='';
    //By default search in On
    $_SEARCH["searched"]    = true ;
    if ( $perm->has('nc_sl_ld_asig') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_SALE_LEADS   =>  array( 
                                                            'Company Name'  => 'company_name',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Middle Name'   => 'm_name',
                                                            'Last Name'     => 'l_name',
                                                            'Pet Name'      => 'p_name',
                                                            'Designation'   => 'desig',
                                                            'Organization'  => 'org',
                                                            
                                                        ),
                               /* TABLE_USER     =>  array(  'Creator Number'    => 'number-user',
                                                            'Creator Username'  => 'username-user',
                                                            'Creator Email'     => 'email-user',
                                                            'Creator First Name'=> 'f_name-user',
                                                            'Creator Last Name' => 'l_name-user',
                                                ),*/
								
                               
                            );
        
       $sOrderByArray  = array(
                                TABLE_SALE_LEADS => array(
                                                    'E-mail'        => 'email',
                                                    'First Name'    => 'f_name',
                                                    'Last Name'     => 'l_name',
                                                    'Date of Birth' => 'do_birth',
                                                    'Date of Add'=> 'do_add',
                                                    'Status'        => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_add';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SALE_LEADS;
        }

        // Read the available Status for the Pre Order.
       // $variables['status']        = Lead::getStatus();
        $variables['status']        = Leads::getLeadStatus();
        $variables['lead_assigned_status']  = Leads::getLeadSearchStatus();
        $variables['leadrating']    = Leads::getRatings();
		
		// Read the available tele - marketing members         
        $mkcondition = " LEFT JOIN ".TABLE_SETTINGS_DEPARTMENT." ON ".TABLE_SETTINGS_DEPARTMENT.".id =  ".TABLE_USER.".department WHERE ( ".TABLE_SETTINGS_DEPARTMENT.".department_value ='nc_dept_marketing' OR ".TABLE_SETTINGS_DEPARTMENT.".department_value ='nc_dept_telemarketing' )" ;
       	Leads::getUsers($db,$users,$mkcondition);
		
		
		// Read the available source
       	Leads::getSource($db,$source);
		
		// Read the available industry
       	Leads::getIndustry($db,$industry);
		
		// Read the available campaign
       	Leads::getCampaign($db,$campaign);
		
		// Read the available rivals
       	Leads::getRivals($db,$rivals);
		
        //use switch case here to perform action. 
        switch ($perform) {
        
            /*case ('add_lead'):
            case ('add'): {
            
                include (DIR_FS_NC.'/sale-lead-assign-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
             /*
            case ('edit_lead'):
            case ('edit'): {
                include (DIR_FS_NC .'/sale-lead-assign-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			
			case ('assign_lead'):
            case ('assign'): {
                include (DIR_FS_NC .'/sale-lead-assign-list.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			
			case ('followup_lead'):
            case ('followup'): {
                include (DIR_FS_NC .'/sale-lead-assign-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('add_pre_order'):
            case ('add'): {
            
                include (DIR_FS_NC.'/bill-invoice-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_pre_order'):
            case ('edit'): {
                include (DIR_FS_NC .'/bill-invoice-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            case ('view_lead'):
            case ('view'): {
                include (DIR_FS_NC .'/sale-lead-assign-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*case ('view_file'): {
                include (DIR_FS_NC .'/bill-invoice-view-file.php');
                break;
            }*/
			case ('change_status_lead'):
            case ('change_status'): {
                include ( DIR_FS_NC .'/sale-lead-assign-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('search_lead'):
            case ('search'): {
                include(DIR_FS_NC."/sale-lead-assign-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            /*
            case ('delete_lead'):
            case ('delete'): {
                include ( DIR_FS_NC .'/sale-lead-assign-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            */
			case ('list_lead'):
            case ('list'):
            default: {
                include (DIR_FS_NC .'/sale-lead-assign-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead-assign.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-lead-assign.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
	$s->assign("source", $source);
	$s->assign("industry", $industry);
	$s->assign("campaign", $campaign);
	$s->assign("rivals", $rivals);
	$s->assign("users", $users);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>