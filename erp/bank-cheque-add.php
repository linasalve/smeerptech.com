<?php
  if ( $perm->has('nc_bk_chq_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        
		
        
		 
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST); 
			           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
              
            if(BankCheque::validateAdd($data, $extra) ) { 
				$company_id = 0;
				$banknameArr=array();
				$table = TABLE_PAYMENT_BANK;
				$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id = '".$data['bank_id']."' " ;
				$fields1 =  TABLE_PAYMENT_BANK .'.company_id,'.TABLE_PAYMENT_BANK .'.bank_name' ;
				$banknameArr = getRecord($table,$fields1,$condition2);
				if(!empty($banknameArr)){
					$company_id =  $banknameArr['company_id'] ;                    
					$bank_name =  $banknameArr['bank_name'] ;                    
				}
				$chqArr = explode(",",$data['cheque_book']);
				
				if(!empty($chqArr)){
					foreach($chqArr as $key1=>$val1){
						$val1=trim($val1); 
						$table = TABLE_BANK_CHEQUE;
						$cond =" WHERE ".TABLE_BANK_CHEQUE.".cheque_no = '".$val1."'
							AND ".TABLE_BANK_CHEQUE.".bank_id = '".$data['bank_id']."'";
									
                        if ( !BankCheque::duplicateFieldValue($db, $table, 'id', $cond) ) {
						
							$query	= " INSERT INTO ".TABLE_BANK_CHEQUE
							." SET ".TABLE_BANK_CHEQUE .".cheque_no = '".$val1."'"  
							.",". TABLE_BANK_CHEQUE .".bank_id = '".$data['bank_id'] ."'"
							.",". TABLE_BANK_CHEQUE .".company_id = '". 	$company_id ."'"
							.",". TABLE_BANK_CHEQUE .".cheque_book_no = '".$data['cheque_book_no'] ."'"
							.",". TABLE_BANK_CHEQUE .".added_by = '". $my['user_id'] ."'"       
							.",". TABLE_BANK_CHEQUE .".added_by_name = '". $my['f_name']." ".$my['l_name'] ."'"       
							.",". TABLE_BANK_CHEQUE .".do_e = '". date('Y-m-d h:i:s')."'";
                
							if( $db->query($query) && $db->affected_rows() > 0 ){
								$messages->setOkMessage("Cheque No - ".$val1." added.");
								$variables['hid'] = $db->last_inserted_id();              
							}
						}else{
						 $messages->setErrorMessage("Duplicate Cheque no. ".$val1." for selected bank - ".$bank_name);
						}
					}
				}
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
		
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
            header("Location:".DIR_WS_NC."/bank-cheque.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
            header("Location:".DIR_WS_NC."/bank-cheque.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ){
            header("Location:".DIR_WS_NC."/bank-cheque.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bank-cheque-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
