<?php

    if ( $perm->has('nc_ldt_details ') || $perm->has('nc_ldt_flw_ord') ) {
        include_once ( DIR_FS_INCLUDES .'/st-template-category.inc.php');
		
        $ticket_id	= isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
        $main_client_name	= isset($_GET["main_client_name"]) ? $_GET["main_client_name"] : 	( isset($_POST["main_client_name"]) ? 
		$_POST["main_client_name"] : '' );
        $billing_name	= isset($_GET["billing_name"]) ? $_GET["billing_name"] : ( isset($_POST["billing_name"]) ? $_POST["billing_name"] : '' );		
		$leads_followup = isset ($_GET['leads_followup']) ? $_GET['leads_followup'] : ( isset($_POST['leads_followup'] ) ? 
		$_POST['leads_followup'] : '0');		
		$lead_ord = isset($_GET['lead_ord']) ? $_GET['lead_ord'] : ( isset($_POST['lead_ord'] ) ? $_POST['lead_ord'] : '');
		$invoice_id =isset($_GET['invoice_id'])? $_GET['invoice_id'] : ( isset($_POST['invoice_id'] ) ?	$_POST['invoice_id'] : '');
        $flw_ord_id = isset($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : (isset($_POST['flw_ord_id']) ?	$_POST['flw_ord_id'] : '');
		$template_category = isset ($_GET['template_category']) ? $_GET['template_category'] : 
		( isset($_POST['template_category'] ) ? $_POST['template_category'] :'');
		
		$fpendingInvlist = array();
		$totalBalanceAmount = 0;
		
	    if(!empty($invoice_id)){
			/*
			include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
			$condition_query_inv = " WHERE ".TABLE_BILL_INV.".status='".Invoice::PENDING."' AND ".TABLE_BILL_INV.".client='".$client_inv."'" ;
			$fields_inv =  TABLE_BILL_INV .'.number'
                        .','. TABLE_BILL_INV .'.or_no'
                        .','. TABLE_BILL_INV .'.currency_abbr'
                        .','. TABLE_BILL_INV .'.amount'
                        .','. TABLE_BILL_INV .'.amount_inr'
                        .','. TABLE_BILL_INV .'.balance'
                        .','. TABLE_BILL_INV .'.balance_inr'
                        .','. TABLE_BILL_INV .'.status'
                        .','. TABLE_BILL_ORDERS .'.order_title';
			Invoice::getDetails( $db, $pendingInvlist, $fields_inv, $condition_query_inv);		
			
			
			if(!empty($pendingInvlist)){
				foreach( $pendingInvlist as $key=>$val){   
					$val['amount']=number_format($val['amount'],2);
					$val['amount_inr']=number_format($val['amount_inr'],2);
					$val['balance']=number_format($val['balance'],2);
					$totalBalanceAmount = $totalBalanceAmount+$val['balance_inr'];
					$val['balance_inr']=number_format($val['balance_inr'],2);				   
					$fpendingInvlist[$key]=$val;
					
				}
				$totalBalanceAmount = number_format($totalBalanceAmount,2);
			}
			*/
		}
		
	    //$lead_id = $my['lead_id'];
        $mail_send_to_su = "";
        $_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates = null;
        $category_list	= 	NULL;
		$condition_query1 = " WHERE ".TABLE_ST_TEMPLATE_CATEGORY.".status = '".StTemplateCategory::ACTIVE."'
		ORDER BY ".TABLE_ST_TEMPLATE_CATEGORY.".category" ;
		StTemplateCategory::getList( $db, $category_list, TABLE_ST_TEMPLATE_CATEGORY.".category,".TABLE_ST_TEMPLATE_CATEGORY.".id", $condition_query1);
		$stTemplates = null;
		if(!empty($template_category)){
			$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
			".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::ST.",%' AND 
			".TABLE_ST_TEMPLATE.".template_category LIKE '%,".$template_category.",%' ORDER BY title";
			SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'
			.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
		}
        
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["rating"] = LeadsTicket::getRating();
        $variables["status"] = LeadsTicket::getTicketStatusList();        
        
		//hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=12; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
		// hrs, min.        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $posted_data=$_POST;
            $data		= processUserData($_ALL_POST);
            $files      = processUserData($_FILES);
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE     ;
			
            $extra = array( 'db' 		=> &$db,
							'my'		=>&$my,
                            'messages'  => $messages,
                        );
			
			$data['flw_ord_id'] = $flw_ord_id;
			
            if ( LeadsTicket::validateAdd($data, $extra) ) {  
                
                $ticket_no  =  LeadsTicket::getNewNumber($db);
                $attachfilename=$ticket_attachment_path='';
                
                if(!empty($data['ticket_attachment'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['ticket_attachment']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    $attachfilename = $ticket_no.".".$ext ;
                    $data['ticket_attachment'] = $attachfilename;
                    $ticket_attachment_path = DIR_WS_LD_FILES;
                    if(move_uploaded_file($files['ticket_attachment']['tmp_name'], 
					DIR_FS_LD_FILES."/".$attachfilename)){
                        @chmod(DIR_FS_LD_FILES."/".$attachfilename, 0777);
                    }
                }
                if(!empty($lead_ord)){
					$data['ticket_owner_uid'] = $lead_ord ;
				}
                $query = "SELECT "	. TABLE_LD_TICKETS .".ticket_date, ".TABLE_LD_TICKETS.".status"
					." FROM ". TABLE_LD_TICKETS 
					." WHERE ". TABLE_LD_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
					." AND (". TABLE_LD_TICKETS .".ticket_child = '". $ticket_id ."' " 
								." OR "
								. TABLE_LD_TICKETS .".ticket_id = '". $ticket_id ."' " 
							.") "
					." ORDER BY ". TABLE_LD_TICKETS .".ticket_date DESC LIMIT 0,1";				
						
				$db->query($query) ;
				if($db->nf() > 0){
					$db->next_record() ;
					$last_time = $db->f("ticket_date") ;
					$ticket_response_time = time() - $last_time ;
					$status = $db->f("status") ;
				}
				else{
					$ticket_response_time = 0 ;
				}
				$ticket_date 	= time() ;
				$ticket_replied = '0' ;

                $mail_to_all_su=0;
                if(isset($data['mail_to_all_su']) ){
                    $mail_to_all_su=1;
                }
                $mail_to_additional_email=0;
                if(isset($data['mail_to_additional_email']) ){
                    $mail_to_additional_email=1;
                }
                $mail_client=0;
                if(isset($data['mail_client']) ){
                    $mail_client=1;
                }				
                if(!isset($data['ticket_status']) ){                    
                    $ticket_status = LeadsTicket::PENDINGWITHCLIENTS;
                }else{
                    $ticket_status = $data['ticket_status'];
                }
				$hrs1 = (int) ($data['hrs'] * 6);
                $min1 = (int) ($data['min'] * 6);   		
				if(empty($data['ss_id'])){					
					$data['ss_file_1']='';
					$data['ss_file_2']='';
					$data['ss_file_3']='';
				}                
				$query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
					. TABLE_LD_TICKETS .".ticket_no         = '".  $ticket_no ."', "
					. TABLE_LD_TICKETS .".invoice_id         = '". $invoice_id ."', "
					. TABLE_LD_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
					. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
					. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
					. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
                    . TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
                    . TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
					. TABLE_LD_TICKETS .".template_id      = '". $data['template_id'] ."', "
					. TABLE_LD_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
                    . TABLE_LD_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
                    . TABLE_LD_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
					. TABLE_LD_TICKETS .".ss_id      	    = '". $data['ss_id'] ."', "
					. TABLE_LD_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
					. TABLE_LD_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
                    . TABLE_LD_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
                    . TABLE_LD_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
					. TABLE_LD_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
					. TABLE_LD_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
					. TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
					. TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
					//. TABLE_LD_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
					//. TABLE_LD_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
					//. TABLE_LD_TICKETS .".ticket_status     = '".$data['ticket_status']."', "
					. TABLE_LD_TICKETS .".ticket_status     = '".$ticket_status."', "
					. TABLE_LD_TICKETS .".ticket_child      = '".$ticket_id."', "
					. TABLE_LD_TICKETS .".ticket_attachment = '". $attachfilename ."', "
					. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
					. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
					. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
					. TABLE_LD_TICKETS .".min = '". $data['min']."', "
					. TABLE_LD_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
					. TABLE_LD_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
					. TABLE_LD_TICKETS .".ticket_replied    = '".$ticket_replied."', "
					. TABLE_LD_TICKETS .".from_admin_panel  = '".LeadsTicket::ADMIN_PANEL."', "
					. TABLE_LD_TICKETS .".mail_client    = '".$mail_client."', "
					. TABLE_LD_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
					. TABLE_LD_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
					. TABLE_LD_TICKETS .".domain_name       = '". $data['domain_name'] ."', "
					. TABLE_LD_TICKETS .".display_name           = '". $data['display_name'] ."', "
					. TABLE_LD_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
					. TABLE_LD_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
					. TABLE_LD_TICKETS .".is_private        = '". $data['is_private'] ."', "
					. TABLE_LD_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
					. TABLE_LD_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
					. TABLE_LD_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "
					. TABLE_LD_TICKETS .".to_email           = '". $data['to'] ."', "
					. TABLE_LD_TICKETS .".cc_email           = '". $data['cc'] ."', "
					. TABLE_LD_TICKETS .".bcc_email          = '". $data['bcc'] ."', "
					. TABLE_LD_TICKETS .".to_email_1         = '". $data['to_email_1'] ."', "
					. TABLE_LD_TICKETS .".cc_email_1         = '". $data['cc_email_1'] ."', "
					. TABLE_LD_TICKETS .".bcc_email_1        = '". $data['bcc_email_1'] ."', "
					. TABLE_LD_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
				
				//unset($GLOBALS["flag"]);
				//unset($GLOBALS["submit"]);
                            
                if ($db->query($query) && $db->affected_rows() > 0) {
                    
                    $variables['hid'] = $ticket_child  = $db->last_inserted_id() ;
                    $sql = '';
                    //echo $status;
                    $sqlstatus ='';
                    if($status == LeadsTicket::DEACTIVE){
                        $sqlstatus = ",".TABLE_LD_TICKETS.".status='".LeadsTicket::ACTIVE."'";   
                    }                    
                    //Mark pending towards client bof
                    $query_update = "UPDATE ". TABLE_LD_TICKETS 
							." SET ". TABLE_LD_TICKETS .".ticket_status = '".$ticket_status."'"  
							.",".TABLE_LD_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
							.",".TABLE_LD_TICKETS.".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."'"
							.",".TABLE_LD_TICKETS.".last_comment = '".$data['ticket_text']."'"
							.",".TABLE_LD_TICKETS.".last_comment_by = '".$my['uid'] ."'"
							.",".TABLE_LD_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
							.$sqlstatus                                                                     
							." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				    $db->query($query_update) ;
					
                    //Mark pending towards client eof
					if(!empty( $flw_ord_id)){
						$sql1= "UPDATE ".TABLE_LEADS_ORDERS." SET ";
						
					}
                    //Mark pending towards client eof
                    /**************************************************************************
                    * Send the notification to the ticket owner and the Administrators
                    * of the new Ticket being created
                    **************************************************************************/
                    
                    $data['ticket_no']    =   $ticket_no ;
                    $data['mticket_no']   =   $data['mticket_no'] ;
                    /*foreach( $status as $key=>$val){                           
                        if ($data['ticket_status'] == $val['status_id'])
                        {
                            $data['status']=$val['status_name'];
                        }
                    }*/
                    
                    $data['replied_by']   =   $my['f_name']." ". $my['l_name'] ;
                    $data['subject']    =     $_ALL_POST['ticket_subject'] ;
					$data['text']    =   $_ALL_POST['ticket_text'] ;
                    $data['attachment']   =   $attachfilename ;
                    $data['link']   = DIR_WS_MP .'/leads-ticket.php?perform=view&ticket_id='. $ticket_id;
                   	$file_name='';
					if(!empty($attachfilename)){
						$file_name[] = DIR_FS_LD_FILES ."/". $attachfilename;
					}
					if(!empty($data['template_file_1'])){
						//$file_name[] = DIR_FS_LD_TEMPLATES_FILES ."/". $data['template_file_1'];
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
					}
					if(!empty($data['template_file_2'])){
						//$file_name[] = DIR_FS_LD_TEMPLATES_FILES ."/". $data['template_file_2'];
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
					}
					if(!empty($data['template_file_3'])){
						//$file_name[] = DIR_FS_LD_TEMPLATES_FILES ."/". $data['template_file_3'];
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
					}
					if(!empty($data['ss_file_1'])){
						$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_1'];
					}
					if(!empty($data['ss_file_2'])){
						$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_2'];
					}
					if(!empty($data['ss_file_3'])){
						$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_3'];
					}
                    //for sms
                    $sms_to_number = $sms_to =array();
                    $mobile1 = $mobile2='';
                    $counter=0;
                    $smsLength = strlen($data['text']); 
                    // Send Email to the ticket owner BOF                         
                    Leads::getList($db, $userDetails, 'lead_number, f_name, l_name, email,email_1,email_2,email_3,email_4,additional_email,title as ctitle,company_name,billing_name,mobile1,mobile2', " WHERE lead_id = '".$data['ticket_owner_uid']."'");
                    if(!empty($userDetails)){
						$userDetails=$userDetails['0'];
						$data['client_name']  =   $userDetails['f_name']." ".$userDetails['l_name'];
						$data['ctitle']    =   $userDetails['ctitle'];
						$data['mobile1']   =   $userDetails['mobile1'];
						$data['mobile2']   =   $userDetails['mobile2'];
						$main_client_name  =   $userDetails['f_name']." ".$userDetails['l_name'];
						$billing_name     =    $userDetails['billing_name'];                      
						$company_name     =    $userDetails['company_name'];                      
						$email = $cc_to_sender = NULL;
						/*
						if(!empty($data['cc'])){
							$cc_email = explode(",",$data['cc']);
							foreach ($cc_email  as $key=>$value){
								$cc[]   = array('name' => "", 'email' => $value);                                
							}
						}else{
							$cc = Null;
						}
						
						if(!empty($data['bcc'])){
							$bcc_email = explode(",",$data['bcc']);
							foreach ($bcc_email  as $key=>$value){
								$bcc[]   = array('name' => "", 'email' => $value);
							}
						}else{
							$bcc = Null;
						}
						if(!empty($data['to'])){
							$to_email = explode(",",$data['to']);
							foreach ($to_email  as $key=>$value){
								$to[]   = array('name' => "", 'email' => $value);
							}  
						}else{
							$to = '';
						}*/
                             
                        if ($data['is_private'] == 1 ){
							if ( getParsedEmail($db, $s, 'EMAIL_LD_CONFIDENTIAL', $data, $email) ) {
								$to = '';
								$cc_to_sender=$cc=$bcc=''; // as confidential
								if(!empty($userDetails['email'])){
									$to[]   = array('name' => $userDetails['email'], 
									'email' => $userDetails['email']);     
									$mail_send_to_su .=",".$userDetails['email'];                                
									if(!empty($data['tck_owner_member_email'])){
										$email["from_name"] =  $data['tck_owner_member_name'];
										$email["from_email"] = $data['tck_owner_member_email'];
									}
									$from = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],
									$cc_to_sender,$cc,$bcc,$file_name); */
								}
								//Send mail on alternate emails ids bof
								if(!empty($userDetails['email_1'])){                                       
									//$to = '';
									$cc_to_sender=$cc=$bcc='';
									$to[]   = array('name' => $userDetails['email_1'], 
									'email' =>$userDetails['email_1']);                                   
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$userDetails['email_1'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
								}
								if(!empty($userDetails['email_2'])){                                       
									//$to = '';
									$cc_to_sender=$cc=$bcc='';
									$to[]   = array('name' => $userDetails['email_2'], 'email' =>
									$userDetails['email_2']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									
									$mail_send_to_su  .= ",".$userDetails['email_2'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
								}
								if(!empty($userDetails['email_3'])){                                       
									//$to = '';
									$cc_to_sender=$cc=$bcc='';
									$to[]   = array('name' => $userDetails['email_3'], 'email' =>
									$userDetails['email_3']);                                   
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$userDetails['email_3'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
								}
								
								if(!empty($userDetails['email_4'])){                                       
									//$to = '';
									$cc_to_sender=$cc=$bcc='';
									$to[]   = array('name' => $userDetails['email_4'], 'email' =>
									$userDetails['email_4']);                                   
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$userDetails['email_4'];
									/* 
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                }
								//Send mail on alternate emails ids eof
							}                       
                        }else{
							if ($mail_client == 1 ){  
								if ( getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email) ) {
									if(!empty($userDetails['email'])){
										$to[]   = array('name' => $userDetails['email'], 
										'email' => $userDetails['email']);
										if(!empty($data['tck_owner_member_email'])){
											$email["from_name"] =  $data['tck_owner_member_name'];
											$email["from_email"] = $data['tck_owner_member_email'];
										}
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email'];
										/* 
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],
										$cc_to_sender,$cc,$bcc,$file_name); */
									}
									//Send mail on alternate emails ids bof
									if(!empty($userDetails['email_1'])){                                       
										//$to = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['email_1'], 'email' =>
										$userDetails['email_1']);                                   
										if(!empty($data['tck_owner_member_email'])){
											$email["from_name"] =  $data['tck_owner_member_name'];
											$email["from_email"] = $data['tck_owner_member_email'];
										}
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_1'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
									}
									if(!empty($userDetails['email_2'])){                                       
										//$to = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['email_2'], 'email' =>
										$userDetails['email_2']);                                   
										if(!empty($data['tck_owner_member_email'])){
											$email["from_name"] =  $data['tck_owner_member_name'];
											$email["from_email"] = $data['tck_owner_member_email'];
										}
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_2'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
									}
									if(!empty($userDetails['email_3'])){                                       
										//$to = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['email_3'], 'email' =>
										$userDetails['email_3']);                                   
										if(!empty($data['tck_owner_member_email'])){
											$email["from_name"] =  $data['tck_owner_member_name'];
											$email["from_email"] = $data['tck_owner_member_email'];
										}
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_3'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
									}
									if(!empty($userDetails['email_4'])){                                       
										//$to = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['email_4'], 'email' =>
										$userDetails['email_4']);                                   
										if(!empty($data['tck_owner_member_email'])){
											$email["from_name"] =  $data['tck_owner_member_name'];
											$email["from_email"] = $data['tck_owner_member_email'];
										}
										$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_4'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
									}
								}                      
							}
							//Send mail on alternate emails ids bof
								if($mail_to_additional_email==1){                        
									getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email) ;
									if(!empty($userDetails['additional_email'])){
										$cc_to_sender=$cc=$bcc='';
										$arrAddEmail = explode(",",$userDetails['additional_email']); 
										foreach($arrAddEmail as $key=>$val ){
											if(!empty($val)){
												$mail_send_to_su  .= ",".$val;
												//$to = '';
												$to[]   = array('name' =>$val, 'email' => $val);
												if(!empty($data['tck_owner_member_email'])){
													$email["from_name"] =  $data['tck_owner_member_name'];
													$email["from_email"] = $data['tck_owner_member_email'];
												}
												$from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												/* 												
												SendMail($to, $from, $reply_to, $email["subject"], 
												$email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name);*/
 											}
										}
									}									
								}else{
									if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
										$arrAddEmail = $data['mail_additional_email'];
										if ( getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email) ) {
											foreach($arrAddEmail as $key=>$val ){
												if(!empty($val)){
													$mail_send_to_su  .= ",".$val;
													//$to = '';
													$to[]   = array('name' =>$val, 'email' => $val);  
													if(!empty($data['tck_owner_member_email'])){
														$email["from_name"] =  $data['tck_owner_member_name'];
														$email["from_email"] = $data['tck_owner_member_email'];
													}
													$from   = $reply_to = array('name' => $email["from_name"], 
													'email' => $email['from_email']);
													/* SendMail($to, $from, $reply_to, $email["subject"], 
													$email["body"], 
													$email["isHTML"],
													$cc_to_sender,$cc,$bcc,$file_name); */
												}
											}
										}
									}
								}
							   //Send mail on alternate emails ids eof 
                        }						
						//Send SMS
						if(isset($data['mobile1_client']) && $smsLength<=130){
							//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
							$sms_to_number[] =$userDetails['mobile1'];
							$mobile1 = "91".$userDetails['mobile1'];
							$client_mobile .= ','.$mobile1;
							ProjectTask::sendSms($data['text'],$mobile1);
							$counter = $counter+1;								
						}
						if(isset($data['mobile2_client']) && $smsLength<=130){
							//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
							$sms_to_number[] =$userDetails['mobile2'] ;
							$mobile2 = "91".$userDetails['mobile2'];
							$client_mobile .= ','.$mobile2;
							ProjectTask::sendSms($data['text'],$mobile2);
							$counter = $counter+1;                                        
						}
						if(!empty($mobile1)|| !empty($mobile2)){
							 $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
						}
										
                    }
                    // Send Email to the ticket owner EOF
                   
                    $subsmsUserDetails=array();
                    // Send Email to all the subuser BOF
                    if ($data['is_private'] != 1){
                    
                        if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
                            $followupSql= "";
							if(!empty($invoice_id)){
								$followupSql = " AND authorization_id  LIKE '%,".Leads::ACCOUNTS.",%'" ;
							}
                            if(isset($_POST['mail_to_all_su'])){
                                Leads::getList($db, $subUserDetails, 'lead_number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' 
                                AND status='".Leads::ACTIVE."' AND send_mail='".Leads::SENDMAILYES."' ".$followupSql." ORDER BY f_name");
                            }elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
                               $mail_to_su= implode("','",$_POST['mail_to_su']);
                                Leads::getList($db, $subUserDetails, 'lead_number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND lead_id IN('".$mail_to_su."')
                                AND status='".Leads::ACTIVE."' ORDER BY f_name");
                            }
							
                            if(!empty($subUserDetails)){
                                //$subUserDetails=$subUserDetails['0'];                               
                                foreach ($subUserDetails  as $key=>$value){
                                    $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                    $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                    $email = NULL;
                                    $cc_to_sender= $cc = $bcc = Null;
                                    if ( getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email) ) {
                                        //$to = '';
                                        $to[]   = array('name' => $subUserDetails[$key]['email'], 
										'email' => $subUserDetails[$key]['email']);
                                        if(!empty($data['tck_owner_member_email'])){
											$email["from_name"] =  $data['tck_owner_member_name'];
											$email["from_email"] = $data['tck_owner_member_email'];
										}
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                        /* 
										if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
											$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                        } */
                                        if(!empty($subUserDetails[$key]['email_1'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' =>  $subUserDetails[$key]['email_1'], 
											'email' => $subUserDetails[$key]['email_1']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                            /* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                            } */
                                        } 
                                        if(!empty($subUserDetails[$key]['email_2'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_2'], 
											'email' => $subUserDetails[$key]['email_2']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
											 
                                            /* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                            } */
                                        }
                                        if(!empty($subUserDetails[$key]['email_3'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_3'],
											'email' => $subUserDetails[$key]['email_3']);
											
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                           /*  if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                            } */
                                        }
                                        if(!empty($subUserDetails[$key]['email_4'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_4'], 
											'email' => $subUserDetails[$key]['email_4']);
                                            $from   = $reply_to = array('name' => $email["from_name"],
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                            /* 
											if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                            } */
                                        }
                                        
                                    }                      
                                }
                            }
                        }else{
                           /*
						   if($data['ticket_owner_uid'] != $data['ticket_creator_uid']){
                                Clients::getList($db, $clientDetails, 'number, f_name, l_name, title as ctitle, email,email_1,email_2,email_3,email_4,
                                parent_id,mobile1,mobile2', " WHERE lead_id = '".$data['ticket_creator_uid'] ."' AND send_mail='".Clients::SENDMAILYES."'");
                                if(!empty($clientDetails)){
                                    $clientDetails=$clientDetails['0'];
                                   
                                    if(!empty($clientDetails['parent_id'])){
                                        $data['client_name']    =   $clientDetails['f_name']." ".$clientDetails['l_name'];
                                        $data['ctitle']    =   $clientDetails['ctitle'];
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        if ( getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email) ) {
                                            $to = '';
                                            $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'],
                                            'email' => $clientDetails['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                            $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$clientDetails['email'];
                                            }
                                            //Send mail on alternate emails ids bof
                                            if(!empty($clientDetails['email_1'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_1']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_1'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_2'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_2']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_2'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_3'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_3']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_3'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_4'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $userDetails['email_4']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_4'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                        } 
                                    }
                                }
                            }*/
                        }
                        
                        if(isset($_POST['mobile1_to_su']) ){                                
                           $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
                            Leads::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " 
                            WHERE parent_id = '".$data['ticket_owner_uid']."' AND lead_id IN('".$mobile1_to_su."')
                            AND status='".Leads::ACTIVE."' ");
                            if(!empty($subsmsUserDetails)){                                                                   
                                foreach ($subsmsUserDetails  as $key=>$value){
                                    if(isset($value['mobile1']) && $smsLength<=130){
                                        $sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
                                        $sms_to_number[] =$value['mobile1'];
                                        $mobile1 = "91".$value['mobile1'];
                                        $client_mobile .= ','.$mobile1;
                                        ProjectTask::sendSms($data['text'],$mobile1);
                                        $counter = $counter+1;
                                            
                                    }
                                }
                            }
                        }
                        $subsmsUserDetails=array();
                        if(isset($_POST['mobile2_to_su']) ){                                
                           $mobile2_to_su = implode("','",$_POST['mobile2_to_su']);                                  
                            Leads::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile2', " 
                            WHERE parent_id = '".$data['ticket_owner_uid']."' AND lead_id IN('".$mobile2_to_su."')
                            AND status='".Leads::ACTIVE."' ");
                            if(!empty($subsmsUserDetails)){                                                                   
                                foreach ($subsmsUserDetails  as $key=>$value){
                                    if(isset($value['mobile2']) && $smsLength<=130){
                                        $sms_to[]  = $value['f_name'] .' '. $value['l_name'];
                                        $sms_to_number[] =$value['mobile2'];
                                        $mobile2 = "91".$value['mobile2'];
                                        $client_mobile .= ','.$mobile2;
                                        ProjectTask::sendSms($data['text'],$mobile2);
                                        $counter = $counter+1;
                                            
                                    }
                                }
                            } 
                        }
                    }                    
                    // Send Email to all the subuser EOF                    
                    //Set the list subuser's email-id to whom the mails are sent bof                    
                    $query_update = "UPDATE ". TABLE_LD_TICKETS 
                                ." SET ". TABLE_LD_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."',
                                sms_to_number='".implode(",",array_unique($sms_to_number))."', sms_to ='".implode(",", array_unique($sms_to))."' "                                                                        
                                ." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
                    $db->query($query_update) ; 
                     //Set the list subuser's email-id to whom the mails are sent bof                       
                        
                    //update sms counter
                    if($counter >0){
                        $sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
                        $db->query($sql); 
                    }
                    //Set the list subuser's email-id to whom the mails are sent eof
                    //Send a copy to check the mail for clients
					 if(!empty($to)){
						getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email);
						//$to     = '';
						$cc_to_sender=$cc=$bcc='';
						$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
						'email' => $sales_email);
						if(!empty($data['tck_owner_member_email'])){
							$email["from_name"] =  $data['tck_owner_member_name'];
							$email["from_email"] = $data['tck_owner_member_email'];
						}
						//echo $email["subject"];
						//echo $email["body"];
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
						$email["isHTML"],$cc_to_sender,
						$cc,$bcc,$file_name);
					}		 

                    // Send Email to the admin BOF
                    $email = NULL;
                    $data['link']   = DIR_WS_NC .'/leads-ticket.php?perform=view&ticket_id='. $ticket_id;
                    $data['quicklink']   = DIR_WS_SHORTCUT .'/leads-ticket.php?perform=view&ticket_id='. $ticket_id;      
                    $data['main_client_name']   = $main_client_name;
                    $data['billing_name']   = $billing_name;
                    $cc_to_sender= $cc = $bcc = Null;
                    /* if ( getParsedEmail($db, $s, 'EMAIL_LD_ADMIN', $data, $email) ) {
                        $to = '';
                        $to[]   = array('name' => $sales_name , 'email' => $sales_email);			 
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,
                        $cc,$bcc,$file_name);
                    }       */                  
                    // Send Email to the admin EOF                    
                    //$messages->setOkMessage("A comment has been added to the support ticket. And email Notification has been sent.");
                    $messages->setOkMessage("Ticket has been created successfully. And email Notification has been sent.");
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
               
				if(!empty($flw_ord_id)){
					header("Location:".DIR_WS_NC."/leads-ticket.php?perform=ordflw_view&ticket_id=".$ticket_id."&flw_ord_id=".$flw_ord_id."&lead_ord=".$lead_ord);
				}else{
					 header("Location:".DIR_WS_NC."/leads-ticket.php?perform=view&added=1&ticket_id=".$ticket_id);
				}
				 
            }
        }else{
			$data['ticket_subject']='Following up on your requirements. Kindly consider. ';
		}
        
        if( isset($_POST["btnUpdate"]) && $_POST["btnUpdate"] == "Close Ticket"){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            if(empty($data['ticket_rating'])){
                $messages->setErrorMessage("Please rate the service.");
            }else{
                $query = "UPDATE ". TABLE_LD_TICKETS 
                                ." SET "//. TABLE_LD_TICKETS .".ticket_priority = '". $data["ticket_priority"] ."', "
                                        . TABLE_LD_TICKETS .".ticket_status = '". LeadsTicket::CLOSED ."', "
                                        . TABLE_LD_TICKETS .".ticket_rating = '". $data["ticket_rating"] ."', "
                                        . TABLE_LD_TICKETS .".ticket_replied = '0' "
                                ." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $ticket_id ."' " 
                                ." AND ". TABLE_LD_TICKETS .".ticket_owner_uid = '". $data["ticket_owner_uid"] ."' ";
                if($db->query($query)){
                    $messages->setOkMessage("Ticket Status Updated Successfully.");
                }
            }
		}
        
        if( isset($_POST["btnActive"])){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
           
            $query = "UPDATE ". TABLE_LD_TICKETS 
                            ." SET ". TABLE_LD_TICKETS .".status = '". LeadsTicket::ACTIVE ."'"
                            ." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $data['thread_ticket_id'] ."' " ;
            if($db->query($query)){
                $messages->setOkMessage("Ticket activated Updated Successfully.");
            }          
		}
        
        if( isset($_POST["btnDeActive"])){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $query = "UPDATE ". TABLE_LD_TICKETS 
                            ." SET ". TABLE_LD_TICKETS .".status = '". LeadsTicket::DEACTIVE ."'"
                            ." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $data['thread_ticket_id'] ."' " ;
            if($db->query($query)){
                $messages->setOkMessage("Ticket de-activated Updated Successfully.");
            }            
		}
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
			if(!empty($invoice_id)){
				header("Location:".DIR_WS_NC."/leads-ticket.php?perform=ordflw_view&ticket_id=".$ticket_id."&invoice_id=".$invoice_id);
			}else{
				include ( DIR_FS_NC .'/leads-ticket-list.php');
			}
			
        }
        else {
            $condition_query= " WHERE ticket_id = '". $ticket_id ."' ";
            $_ALL_POST      = NULL;            
            if ( LeadsTicket::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST[0];
		
                $mticket_no = $_ALL_POST['ticket_no'];
                $_ALL_POST['mail_send_to_su'] = chunk_split($_ALL_POST['mail_send_to_su']);
                $_ALL_POST['sms_to'] = chunk_split($_ALL_POST['sms_to']);
                $_ALL_POST['sms_to_number'] = chunk_split($_ALL_POST['sms_to_number']);
                $_ALL_POST['attachmentArr']='';
                if(!empty($_ALL_POST['attachment_imap'])){
                    $_ALL_POST['attachment_imap'] = trim($_ALL_POST['attachment_imap'],",") ;
                    $_ALL_POST['attachmentArr'] = explode(",",$_ALL_POST['attachment_imap']);
                }                
                $_ALL_POST['path_attachment_imap'] = DIR_WS_LD_FILES ;
            }

            $ticketStatusArr = LeadsTicket::getTicketStatus();
            $ticketStatusArr = array_flip($ticketStatusArr);
            $_ALL_POST['ticket_status_name'] = $ticketStatusArr[$_ALL_POST['ticket_status']];   
			if (!isset($data['ticket_text'])){
				$data['ticket_text'] = 'Dear ...'; 
			}   
            /*
			$table = TABLE_SETTINGS_DEPARTMENT;
            $condition2 = " WHERE ".TABLE_SETTINGS_DEPARTMENT .".id= '".$_ALL_POST['ticket_department'] ."' " ;
            $fields1 =  TABLE_SETTINGS_DEPARTMENT .'.department_name';
            $ticket_department= getRecord($table,$fields1,$condition2);            
            $_ALL_POST['department_name']    = $ticket_department['department_name'] ;     
			*/   
            if(!empty($lead_ord)){
					$_ALL_POST['ticket_owner_uid'] = $lead_ord ;
			}
            $condition_query= " WHERE lead_id = '". $_ALL_POST['ticket_owner_uid'] ."' ";
            // AND status='".Leads::ACTIVE."'
			$member      =$additionalEmailArr= NULL;            
            if ( Leads::getList($db, $member, 'f_name,l_name,billing_name,company_name,additional_email,mobile1,mobile2,status,send_mail,email,email_1,email_2,email_3,email_4
			', $condition_query) > 0 ) {
                $_ALL_POST['client'] = $member[0];
                $_ALL_POST['client']['name'] = $main_client_name = $member[0]['f_name']." ".$member[0]['l_name'];      
                $_ALL_POST['mobile1'] =$member[0]['mobile1'];
                $_ALL_POST['mobile2'] =$member[0]['mobile2'];
                $billing_name = $member[0]['billing_name'];
                $_ALL_POST['email']=$member['0']['email'];
                $_ALL_POST['email_1'] = $member['0']['email_1'];
                $_ALL_POST['email_2'] = $member['0']['email_2'];
                $_ALL_POST['email_3'] = $member['0']['email_3'];
                $_ALL_POST['email_4'] = $member['0']['email_4'];
                $_ALL_POST['additional_email'] = $member['0']['additional_email'];
				$all_additional_email = trim($_ALL_POST['additional_email'],",");
				$additionalEmailArr = explode(",",$all_additional_email);
                //$_ALL_POST['authorization_id'] = $member['0']['authorization_id'];
				$_ALL_POST['accountValid'] = 0;
				if(!empty($invoice_id)){
					$authorization_id = trim($_ALL_POST['authorization_id'],",");
					$authorizationArr = explode(",",$authorization_id);
					$isValid = in_array(Leads::ACCOUNTS,$authorizationArr);
					$_ALL_POST['accountValid'] = $isValid ;
				}
            }
           
            $_ALL_POST['mail_client']=0;            
            //$_ALL_POST['mail_to_all_su']=1;            
            $_ALL_POST['mail_to_additional_email']=1;            
            $subUserDetails=null;
            /*
            Leads::getList($db, $subUserDetails, 'lead_number,lead_id, f_name, l_name,title as ctitle,status, email,email_1,email_2, email_3,email_4', " 
                        WHERE parent_id = '".$_ALL_POST['ticket_owner_uid']."' AND service_id LIKE '%,".Leads::ST.",%' 
                        AND status='".Leads::ACTIVE."' ");
                        */
			$followupSql= "";
			if(!empty($invoice_id)){
				$followupSql = " AND authorization_id LIKE '%,".Leads::ACCOUNTS.",%'" ;
			}
			$suCondition =" WHERE parent_id = '".$_ALL_POST['ticket_owner_uid']."' 
                        AND status='".Leads::ACTIVE."' ".$followupSql." ORDER BY f_name ";
            Leads::getList($db, $subUserDetails, 'lead_number,lead_id, f_name, l_name,title as ctitle,status,send_mail, email,
			email_1,email_2, email_3,email_4,mobile1,mobile2', $suCondition );
	   
            $query = "SELECT "	. TABLE_LD_TICKETS .".ticket_no, "
								. TABLE_LD_TICKETS .".ticket_id, "
								. TABLE_LD_TICKETS .".sent_inv_id, "
								. TABLE_LD_TICKETS .".sent_inv_no, "
								. TABLE_LD_TICKETS .".template_file_1, "
								. TABLE_LD_TICKETS .".template_file_2, "
								. TABLE_LD_TICKETS .".template_file_3, "
								. TABLE_LD_TICKETS .".template_id, "
								. TABLE_LD_TICKETS .".ss_id, "
								. TABLE_LD_TICKETS .".ss_file_1, "
								. TABLE_LD_TICKETS .".ss_file_2, "
								. TABLE_LD_TICKETS .".ss_file_3, "
								. TABLE_LD_TICKETS .".to_email_1, "
								. TABLE_LD_TICKETS .".cc_email_1, "
								. TABLE_LD_TICKETS .".bcc_email_1, "
								. TABLE_LD_TICKETS .".on_behalf, "
								. TABLE_LD_TICKETS .".from_email, "
								. TABLE_LD_TICKETS .".mail_send_to_su, "
								. TABLE_LD_TICKETS .".ticket_subject, "
								. TABLE_LD_TICKETS .".ticket_text, "
								. TABLE_LD_TICKETS .".ticket_creator, "
								. TABLE_LD_TICKETS .".ticket_creator_uid, "
								. TABLE_LD_TICKETS .".ticket_attachment, "
								. TABLE_LD_TICKETS .".ticket_attachment_1, "
								. TABLE_LD_TICKETS .".ticket_attachment_2, "
								. TABLE_LD_TICKETS .".ticket_attachment_path, "
								. TABLE_LD_TICKETS .".ticket_response_time, "
								. TABLE_LD_TICKETS .".attachment_imap, "
								. TABLE_LD_TICKETS .".ticket_date, "
								. TABLE_LD_TICKETS .".status, "
								. TABLE_LD_TICKETS .".hrs1, "
								. TABLE_LD_TICKETS .".min1, "
								. TABLE_LD_TICKETS .".hrs, "
								. TABLE_LD_TICKETS .".min, "
								. TABLE_LD_TICKETS .".ip, "
								. TABLE_LD_TICKETS .".headers_imap, "
								. TABLE_LD_TICKETS .".sms_to, "
								. TABLE_LD_TICKETS .".sms_to_number, "
                                . TABLE_SALE_LEADS .".mobile1, "
                                . TABLE_SALE_LEADS .".mobile2 "
							." FROM ". TABLE_LD_TICKETS ." LEFT JOIN ".TABLE_SALE_LEADS." 
                                ON ".TABLE_SALE_LEADS.".lead_id = ".TABLE_LD_TICKETS.".ticket_creator_uid
                                WHERE ". TABLE_LD_TICKETS .".ticket_owner_uid = '". $_ALL_POST['ticket_owner_uid'] ."' " 
							    ." AND ". TABLE_LD_TICKETS .".ticket_child = '". $ticket_id ."' " 
							    ." ORDER BY ". TABLE_LD_TICKETS .".ticket_date DESC";
			
			$db->query($query) ;
			$ticketThreads = array() ;
			while($db->next_record()){
				$response["h"] = round($db->f("ticket_response_time") / 3600) ;
				$response["m"] = round(($db->f("ticket_response_time") % 3600) / 60 ) ;
				$response["s"] = round(($db->f("ticket_response_time") % 3600) % 60 ) ;
                
				$db_new = new db_local();
                $sql_phone = "SELECT ". TABLE_LEAD_PHONE .".ac, "
								. TABLE_LEAD_PHONE .".cc, "
								. TABLE_LEAD_PHONE .".p_number "
							." FROM ". TABLE_LEAD_PHONE.
                            " WHERE ". TABLE_LEAD_PHONE .".phone_of = '".$db->f("ticket_creator_uid")."' " 
							." AND ". TABLE_LEAD_PHONE .".p_is_preferred = '1' " 
							." AND ". TABLE_LEAD_PHONE .".table_name='".TABLE_SALE_LEADS."'";
                $db_new->query($sql_phone);        
                $db_new->next_record();
                $attachmentArr='';
                $attachment_imap = $db->f("attachment_imap") ; 
                if(!empty($attachment_imap)){
                    $attachment_imap = trim($attachment_imap,",");
                    $attachmentArr = explode(",",$attachment_imap);
                }
                $ticket_date_thread = date("d M Y H:i:s",$db->f("ticket_date"));
                        
                $path_attachment_imap = DIR_WS_LD_FILES ;
				$ticketThreads[] = array(	"ticket_no" 			=> $db->f("ticket_no"), 
											"ticket_id" 		=> $db->f("ticket_id"), 
											"sent_inv_id" 		=> $db->f("sent_inv_id"), 
											"sent_inv_no" 		=> $db->f("sent_inv_no"), 
											"template_file_1" 		=> $db->f("template_file_1"), 
											"template_file_2" 		=> $db->f("template_file_2"), 
											"template_file_3" 		=> $db->f("template_file_3"), 
											"template_id" 		=> $db->f("template_id"), 
											"ss_id" 	=> $db->f("ss_id"), 
											"ss_file_1" 	=> $db->f("ss_file_1"), 
											"ss_file_2" 	=> $db->f("ss_file_2"), 
											"ss_file_3" 	=> $db->f("ss_file_3"), 
											"ticket_subject" 		=> $db->f("ticket_subject"), 
                                            "on_behalf" 		=> $db->f("on_behalf"), 
											"mail_send_to_su" 		=> chunk_split($db->f("mail_send_to_su")), 
											"ticket_text" 			=> $db->f("ticket_text"), 
											"ticket_creator" 		=> $db->f("ticket_creator"), 
											"ticket_attachment" 	=> $db->f("ticket_attachment"), 
											"ticket_attachment_1" 	=> $db->f("ticket_attachment_1"), 
											"ticket_attachment_2" 	=> $db->f("ticket_attachment_2"), 
											"ticket_attachment_path"=> $db->f("ticket_attachment_path"),
											"ticket_response_time" 	=> $response["h"] ." hr(s) : "
																		. $response["m"] ." min(s) : " 
																		. $response["s"]." sec(s) ",
											"ticket_date" 	=> $ticket_date_thread,
											"mobile1" 			=> $db->f("mobile1"),
											"mobile2" 			=> $db->f("mobile2"),
                                            "sms_to" 			=> chunk_split($db->f("sms_to")),
											"sms_to_number" 	=>chunk_split( $db->f("sms_to_number")),
											"cc" 			=> $db_new->f("cc"),
											"ac" 			=> $db_new->f("ac"),
											"p_number" 		=> $db_new->f("p_number"),
											"to_email_1" 	=> $db->f("to_email_1"),
											"cc_email_1" 	=> $db->f("cc_email_1"),
											"bcc_email_1" 	=> $db->f("bcc_email_1"),
											"from_email" 	=> $db->f("from_email"),
											"status" 	=> $db->f("status"),
											"ip" 	=> $db->f("ip"),
											"hrs" 	=> $db->f("hrs"),
											"min" 	=> $db->f("min"),
											"hrs1" 	=> $db->f("hrs1"),
											"min1" 	=> $db->f("min1"),
											"ip" 	=> $db->f("ip"),
											"headers_imap" 	=> $db->f("headers_imap"),
											"path_attachment_imap" 	=> $path_attachment_imap,
											"attachmentArr" 	=> $attachmentArr
										);
			}
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $variables['client_sendmail'] = Leads::SENDMAILNO;
            $variables['ticket_closed'] = LeadsTicket::CLOSED;
            $variables['can_add_cmt_after_closed'] = false;
            $variables['can_update_status'] = false;
            $variables['can_view_headers']  = false;
            $variables['can_view_reply_email']  = false;             
			$variables['can_view_client_details']  = true;
            if ( $perm->has('nc_ldt_add_cmt_after_closed') ) {
               $variables['can_add_cmt_after_closed'] = true;
            }            
            if ( $perm->has('nc_ldt_update_status') ) {
               $variables['can_update_status'] = true;
            }
            if ( $perm->has('nc_ldt_headers') ) {
                $variables['can_view_headers'] = true;
            }
            /*if ( $perm->has('nc_uc_contact_details') ) {           
                $variables['can_view_client_details'] = true;
            }*/
            if ( $perm->has('nc_ldt_reply_email') ) {
                $variables['can_view_reply_email'] = true;
            }           
			$variables['can_send_mail'] = false;
			if ( $perm->has('nc_ldt_send_mail') ) {
                $variables['can_send_mail'] = true;
            } 
            //$hidden[] = array('name'=> 'perform','value' => 'view');
            $hidden[] = array('name'=> 'perform','value' => $perform);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'ticket_id', 'value' => $ticket_id);
            $hidden[] = array('name'=> 'invoice_id', 'value' => $invoice_id);
            $hidden[] = array('name'=> 'mticket_no', 'value' => $mticket_no);  
			$hidden[] = array('name'=> 'flw_ord_id', 'value' => $flw_ord_id);			
            $hidden[] = array('name'=> 'lead_ord', 'value' => $lead_ord);            
            $hidden[] = array('name'=> 'main_client_name', 'value' => $main_client_name);            
            $hidden[] = array('name'=> 'billing_name', 'value' => $billing_name);                   
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
			$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'flw_ord_id', 'value' => 'flw_ord_id');
            $page["var"][] = array('variable' => 'ticket_id', 'value' => 'ticket_id');
			$page["var"][] = array('variable' => 'invoice_id', 'value' => 'invoice_id');
			$page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
            $page["var"][] = array('variable' => 'pendingInvlist', 'value' => 'fpendingInvlist');
            $page["var"][] = array('variable' => 'totalBalanceAmount', 'value' => 'totalBalanceAmount');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'additionalEmailArr', 'value' => 'additionalEmailArr');
            $page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
            $page["var"][] = array('variable' => 'ticketThreads', 'value' => 'ticketThreads');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-ticket-view.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>