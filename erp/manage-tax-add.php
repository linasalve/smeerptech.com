<?php
    if ( $perm->has('nc_mt_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the Service Tax class.
		include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
		
		
		$parentTax= ServiceTax::getParentServiceTax($db);
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( ServiceTax::validateAdd($data, $extra) ) { 
				$data['parent_name']='';
				if(!empty($data['parent_id'])){
					$condition1=" WHERE id= ".$data['parent_id']." LIMIT 0,1" ;
					$required_fields='tax_name';
					ServiceTax::getDetails( $db, $parentTaxDetails, $required_fields, $condition1);
					$arr = $parentTaxDetails[0];
					$data['parent_name'] = $arr['tax_name'];
					
				}
				
                $query	= " INSERT INTO ".TABLE_SERVICE_TAX
                            ." SET ". TABLE_SERVICE_TAX .".tax_name =   '".$data['tax_name'] ."'"
                                .",". TABLE_SERVICE_TAX .".parent_id = '". $data['parent_id'] ."'"
                                .",". TABLE_SERVICE_TAX .".parent_name = '". $data['parent_name'] ."'"
                                .",". TABLE_SERVICE_TAX .".declaration = '". $data['declaration'] ."'"
                                .",". TABLE_SERVICE_TAX .".tax_number = '". $data['tax_number'] ."'"
                                .",". TABLE_SERVICE_TAX .".status =     '". $data['status'] ."'"
                                .",". TABLE_SERVICE_TAX .".do_e = '".date("Y-m-d H:i:s")."'"
                                .",". TABLE_SERVICE_TAX .".ip = '".$_SERVER['REMOTE_ADDR']."'"
                                .",". TABLE_SERVICE_TAX .".access_level = '".$access_level."'"
                                .",". TABLE_SERVICE_TAX .".created_by = '".$my['uid']."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Tax entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/manage-tax.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/manage-tax.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/manage-tax.php?added=1");   
            
        }else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'parentTax', 'value' => 'parentTax');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'manage-tax-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>