<?php
    if ( $perm->has('nc_bl_or_add') ) {
    
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/quotation.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/financial-year.inc.php');		  
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
		// Include the Work Stage class and Work Timeline class.
		include_once (DIR_FS_INCLUDES .'/work-stage.inc.php');
		include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
		include_once (DIR_FS_INCLUDES .'/user.inc.php');
		include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
       
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $po_id          = isset($_GET["po_id"]) ? $_GET["po_id"] : ( isset($_POST["po_id"]) ? $_POST["po_id"] : '' );
        $lead_id        = isset($_GET["lead_id"]) ? $_GET["lead_id"] : ( isset($_POST["lead_id"]) ? $_POST["lead_id"] : '' );
        $q_id           = isset($_GET["q_id"]) ? $_GET["q_id"] : ( isset($_POST["q_id"]) ? $_POST["q_id"] : '' );
        //$tbl_name       = isset($_GET["tbl_name"]) ? $_GET["tbl_name"] : ( isset($_POST["tbl_name"]) ? $_POST["tbl_name"] : '' );
        //echo $tbl_name;
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }
        //get financial yr bof
        $lst_fyr = null;
        $lst_fyr = FinancialYear::yearRange();
        //get financial yr eof
        
        
		//Get company list 
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
			.','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
      
        // Read the available source
       	Leads::getSource($db,$source);
        
		
		$lst_work_stage = NULL;
		WorkStage::getWorkStages($db, $lst_work_stage);
        // list of executives to assign project manager
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields);     
        
        //code to generate $vendorOptionList BOF        
        $vendorSql = "SELECT id,billing_name FROM ".TABLE_VENDORS." WHERE parent_id =0" ;
        $db->query( $vendorSql );
        if( $db->nf() > 0 ){
            $vendorList['0'] = "" ;
			while($db->next_record()){
                $vendorList[$db->f('id')] = $db->f('billing_name') ;
            }
        }
        //code to generate $vendorOptionList EOF
		
		//COPY ORDER BOF
		if(!empty($or_id)){
			$fields = TABLE_BILL_ORDERS .'.*'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.billing_name AS billing_name'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.status AS c_status';
			$condition_query=" WHERE ". TABLE_BILL_ORDERS. ".id= '". $or_id."'" ;
			if ( Order::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
					
				$_ALL_POST = $_ALL_POST['0'];		 
				// Set up the Client Details field.
				$_ALL_POST['client_details'] = $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
												.' ('. $_ALL_POST['billing_name'] .')';
												//.' ('. $_ALL_POST['c_email'] .')'; 
				// Read the Team Members Information.
				$_ALL_POST['team'] = trim($_ALL_POST['team'],",") ;
				$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
				$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
				$_ALL_POST['team_members']  = '';
				$_ALL_POST['team_details']  = array();
				User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
				$_ALL_POST['team'] = array();
				foreach ( $_ALL_POST['team_members'] as $key=>$members) {
			  
					$_ALL_POST['team'][] = $members['user_id'];
					$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
				}
				 // Read the Clients Sub Members Information.
				$_ALL_POST['clients_su']= trim($_ALL_POST['clients_su']);
				$_ALL_POST['clients_su']          = explode(',', $_ALL_POST['clients_su']);
				$temp_su                       = "'". implode("','", $_ALL_POST['clients_su']) ."'";
				$_ALL_POST['clients_su_members']  = '';
				$_ALL_POST['clients_su_details']  = array();
				Clients::getList($db, $_ALL_POST['clients_su_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN 
				(". $temp_su .")");
				$_ALL_POST['clients_su'] = array();
				if(!empty($_ALL_POST['clients_su_members'])){
					foreach ( $_ALL_POST['clients_su_members'] as $key=>$members) {
				  
						$_ALL_POST['clients_su'][] = $members['user_id'];
						//$_ALL_POST['clients_su_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						$_ALL_POST['clients_su_details'][] = $members['f_name'] .' '. $members['l_name'] .'';
					}
				}
				//date of order
				$_ALL_POST['do_o']  = explode(' ', $_ALL_POST['do_o']);
				$temp               = explode('-', $_ALL_POST['do_o'][0]);
				$_ALL_POST['do_o']  = NULL;
				$_ALL_POST['do_o']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				// Setup the date of delivery.
				$_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
				$temp               = explode('-', $_ALL_POST['do_d'][0]);
				$_ALL_POST['do_d']  = NULL;
				$_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				$po_id              = $_ALL_POST['po_id'];
				
				// Setup the start date and est date
				if( $_ALL_POST['st_date'] =='0000-00-00 00:00:00'){
					$_ALL_POST['st_date']  ='';
				}else{
					$_ALL_POST['st_date']  = explode(' ', $_ALL_POST['st_date']);
					$temp               = explode('-', $_ALL_POST['st_date'][0]);
					$_ALL_POST['st_date']  = NULL;
					$_ALL_POST['st_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}
				// Setup the start date and est date
				
				if( $_ALL_POST['es_ed_date'] =='0000-00-00 00:00:00'){
					$_ALL_POST['es_ed_date']  ='';
				}else{
					$_ALL_POST['es_ed_date']  = explode(' ', $_ALL_POST['es_ed_date']);
					$temp               = explode('-', $_ALL_POST['es_ed_date'][0]);
					$_ALL_POST['es_ed_date']  = NULL;
					$_ALL_POST['es_ed_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}
				if( $_ALL_POST['do_e'] =='0000-00-00 00:00:00'){
					$_ALL_POST['do_e']  ='';
				}else{
					$_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
					$temp               = explode('-', $_ALL_POST['do_e'][0]);
					$_ALL_POST['do_e']  = NULL;
					$_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}
				if( $_ALL_POST['do_fe'] =='0000-00-00 00:00:00'){
					$_ALL_POST['do_fe']  ='';
				}else{
					$_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
					$temp               = explode('-', $_ALL_POST['do_fe'][0]);
					$_ALL_POST['do_fe']  = NULL;
					$_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}
				//Get Quotation files BOF            
				if( !empty($_ALL_POST['quotation_no'])){
					$rejectedFiles = array();                       
					Quotation::allRejectedQuotation($db,$_ALL_POST['parent_q_id'],$rejectedFiles);
				}                     
				//Get Quotation files EOF
				
				//if order_closed_by is empty 
				/*
				if(empty($_ALL_POST['order_type'])){
					$_ALL_POST['order_type'] = Order::TARGETED ;
				}
				if(empty($_ALL_POST['order_closed_by'])){
					$_ALL_POST['order_closed_by'] = 'e68adc58f2062f58802e4cdcfec0af2d' ;
				}*/
				
				 // Set up the Services.
				//$_ALL_POST['service_id'] = explode(',', $_ALL_POST['service_id']);
			   
				// Read the Particulars.
				$temp_p = NULL;
				$condition_query_p ='';
				$service_idArr= array();
				$condition_query_p = " WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $_ALL_POST['number'] ."'";
				Order::getParticulars($db, $temp_p, '*', $condition_query_p);
				if(!empty($temp_p)){
					foreach ( $temp_p as $pKey => $parti ) {                            
						$_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];
						$_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
						$_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];
						$_ALL_POST['s_type'][$pKey]       = $temp_p[$pKey]['s_type'];
						$_ALL_POST['s_quantity'][$pKey]       = $temp_p[$pKey]['s_quantity'];
						$_ALL_POST['s_amount'][$pKey]       = $temp_p[$pKey]['s_amount'];
						$_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
						$_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
						$_ALL_POST['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];
						$_ALL_POST['ss_title'][$pKey]   = $temp_p[$pKey]['ss_title'];
						$_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];
						$_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
						$_ALL_POST['tax1_id'][$pKey]   = $temp_p[$pKey]['tax1_id'];
						$_ALL_POST['tax1_value'][$pKey]   = $temp_p[$pKey]['tax1_value'];
						$_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
						$_ALL_POST['tax1_number'][$pKey]   = $temp_p[$pKey]['tax1_number'];
						$_ALL_POST['tax1_amount'][$pKey]   = $temp_p[$pKey]['tax1_amount'];
						
						$_ALL_POST['tax1_sub1_name'][$pKey]   = $temp_p[$pKey]['tax1_sub1_name'];
						$_ALL_POST['tax1_sub1_id'][$pKey]   = $temp_p[$pKey]['tax1_sub1_id'];
						$_ALL_POST['tax1_sub1_value'][$pKey]   = $temp_p[$pKey]['tax1_sub1_value'];
						$_ALL_POST['tax1_sub1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub1_pvalue'];
						$_ALL_POST['tax1_sub1_number'][$pKey]   = $temp_p[$pKey]['tax1_sub1_number'];
						$_ALL_POST['tax1_sub1_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub1_amount'];
						
						$_ALL_POST['tax1_sub2_name'][$pKey]   = $temp_p[$pKey]['tax1_sub2_name'];
						$_ALL_POST['tax1_sub2_id'][$pKey]   = $temp_p[$pKey]['tax1_sub2_id'];
						$_ALL_POST['tax1_sub2_value'][$pKey]   = $temp_p[$pKey]['tax1_sub2_value'];
						$_ALL_POST['tax1_sub2_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub2_pvalue'];
						$_ALL_POST['tax1_sub2_number'][$pKey]   = $temp_p[$pKey]['tax1_sub2_number'];
						$_ALL_POST['tax1_sub2_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub2_amount'];
						
						$_ALL_POST['is_renewable'][$pKey]   = $temp_p[$pKey]['is_renewable'];
						$_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
						$_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
						$_ALL_POST['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
						$_ALL_POST['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
						$_ALL_POST['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
						$_ALL_POST['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];
						$service_idArr[] = $temp_p[$pKey]['s_id'];
					}
				}
				$_ALL_POST['service_id'] = $service_idArr;
			   
				$_ALL_POST['po_filename']=$_ALL_POST['po_filename'];
				$_ALL_POST['filename_1']=$_ALL_POST['filename_1'];
				$_ALL_POST['filename_2']=$_ALL_POST['filename_2'];
				$_ALL_POST['filename_3']=$_ALL_POST['filename_3']; 
			}
			else {
				$messages->setErrorMessage("The Order was not found or you do not have the Permission to access this Order.");
			}
		}
		//COPY ORDER EOF
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);        
        // code for particulars bof 
         $rejectedFiles=null;
        // Read the Services.
        include_once ( DIR_FS_INCLUDES .'/services.inc.php');        
        
        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC"; 
        Services::getList($db, $lst_service, 'ss_division,ss_id,ss_title,ss_punch_line,tax1_id, tax1_name, tax1_value,
		tax1_sub1_id, tax1_sub1_name, tax1_sub1_value,tax1_sub2_id, tax1_sub2_name, tax1_sub2_value,
		is_renewable ', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
                    $ss_id = $key['ss_id'];
                    $ss_div_id = $key['ss_division'];
                    $ss_title= $key['ss_title'];
                    $ss_punch_line= $key['ss_punch_line'];
					$is_renewable = $key['is_renewable'];
					
                    $tax1_id = $key['tax1_id'];
                    $tax1_name = $key['tax1_name'];
                    $tax1_value = $key['tax1_value'];
					$tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
					 
					$tax1_sub1_id = $key['tax1_sub1_id'];
                    $tax1_sub1_name = $key['tax1_sub1_name'];
                    $tax1_sub1_value = $key['tax1_sub1_value'];                   
                    $tax1_sub1_pvalue = (float) (((float )  $key['tax1_sub1_value'] ) / 100 );
					
					$tax1_sub2_id = $key['tax1_sub2_id'];
                    $tax1_sub2_name = $key['tax1_sub2_name'];
                    $tax1_sub2_value = $key['tax1_sub2_value'];                   
                    $tax1_sub2_pvalue = (float) (((float )  $key['tax1_sub2_value'] ) / 100 );
					
                    $keyNew['ss_div_id'] = $ss_div_id ; 
                    $keyNew['ss_id'] = $ss_id ; 
                    $keyNew['ss_title'] = $ss_title ; 
                    $keyNew['ss_punch_line'] = $ss_punch_line ; 
                    $keyNew['tax1_id'] = $tax1_id ; 
                    $keyNew['tax1_name'] = $tax1_name ; 
                    $keyNew['tax1_value'] = $tax1_value ; 
                    $keyNew['tax1_pvalue'] = $tax1_pvalue ; 
                    $keyNew['is_renewable'] = $is_renewable ; 
					
					//2010-10-oct-26 added
					 $keyNew['tax1_sub1_id'] = $tax1_sub1_id ; 
                    $keyNew['tax1_sub1_name'] = $tax1_sub1_name ; 
                    $keyNew['tax1_sub1_value'] = $tax1_sub1_value ; 
                    $keyNew['tax1_sub1_pvalue'] = $tax1_sub1_pvalue ; 
					
					$keyNew['tax1_sub2_id'] = $tax1_sub2_id ; 
                    $keyNew['tax1_sub2_name'] = $tax1_sub2_name ; 
                    $keyNew['tax1_sub2_value'] = $tax1_sub2_value ; 
                    $keyNew['tax1_sub2_pvalue'] = $tax1_sub2_pvalue ; 
					
                    $lst_service[$val] = $keyNew;
            }
        }
        
        // code for particulars eof
        
        /* Get quotation details bof*/
          if(!empty($lead_id)){            
            
                   //Get data of q_id
                    $condition_queryd = " WHERE ".TABLE_SALE_QUOTATION .'.id='.$q_id ;
                    $fields = '*';
                    if ( Quotation::getDetails($db, $_ALL_POST, $fields, $condition_queryd) > 0 ) {
                        $_ALL_POST = $_ALL_POST['0'];
                        $_ALL_POST['do_e']='';
                        //$lead_id = $_ALL_POST['lead_id'];
                        $_ALL_POST['quotation_no']  = $_ALL_POST['number'];
                        
                        //Get Quotation files BOF
                        if( $_ALL_POST['quotation_no']){
                            $rejectedFiles = array();
                            Quotation::allRejectedQuotation($db,$_ALL_POST['parent_q_id'],$rejectedFiles);
                        }
                        //Get Quotation files EOF
                        
                        
                        //$number = $_ALL_POST['number'];
                        
                        // Read the Particulars.
                            $temp_p = NULL;
                            $condition_query_p ='';
                            $service_idArr= array();
                            $condition_query_p = "WHERE ".TABLE_SALE_QUOTATION_P.".q_no = '". $_ALL_POST['number'] ."'";
                            Quotation::getParticulars($db, $temp_p, '*', $condition_query_p);
                            if(!empty($temp_p)){
                                foreach ( $temp_p as $pKey => $parti ) {
                                    
                                    $_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];
                                    $_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
                                    $_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];
                                    $_ALL_POST['s_type'][$pKey]       = $temp_p[$pKey]['s_type'];
                                    $_ALL_POST['s_quantity'][$pKey]       = $temp_p[$pKey]['s_quantity'];
                                    $_ALL_POST['s_amount'][$pKey]       = $temp_p[$pKey]['s_amount'];
                                    $_ALL_POST['is_renewable'][$pKey]      = $temp_p[$pKey]['is_renewable'];
                                    
                                    $_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
                                    $_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
                                    $_ALL_POST['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];
                                    $_ALL_POST['ss_title'][$pKey]   = $temp_p[$pKey]['ss_title'];
                                    $_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];
                                    $_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
                                    $_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
                                    $_ALL_POST['is_renewable'][$pKey]   = $temp_p[$pKey]['is_renewable'];
                                    $_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
                                    $_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
                                    $_ALL_POST['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
                                    $_ALL_POST['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
                                    $_ALL_POST['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
                                    $_ALL_POST['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];
                                    $service_idArr[] = $temp_p[$pKey]['s_id'];
                                }
                            }
                            $_ALL_POST['service_id'] = $service_idArr;
                    }else{
                        $messages->setErrorMessage("Quotation was not found.");
                    }
                   
                    $query="SELECT user_id,f_name,l_name,number,email FROM ".TABLE_CLIENTS." WHERE lead_id='".$lead_id."'";
                    
                    if( $db->query($query) && $db->affected_rows() > 0 ){
                        while($db->next_record()){
                            $_ALL_POST['client']=$db->f('user_id');
                            //$_ALL_POST['client_details']=$db->f('f_name')."&nbsp;".$db->f('l_name')."&nbsp;(".$db->f('number').")"."&nbsp;(".$db->f('email').")";
                            $_ALL_POST['client_details']=$db->f('f_name')."&nbsp;".$db->f('l_name')."&nbsp;(".$db->f('number').")";
                        }
                    }
            
            //As order-number formate changed on the basis of ( fanancial yr + sr no ) 
            //$_ALL_POST['number'] = getCounterNumber($db,'ORD'); 
            $_ALL_POST['exchange_rate'] = 1;             
         
        }         
        /* Get quotation details eof*/
        
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn']) || isset($_POST['btnReturnInv'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_MULTIPLE_FILE_SIZE     ;
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'lst_service'       => $lst_service,
							'lst_work_stage'    => $lst_work_stage,
                            'messages'          => &$messages
                        );
            	    
            //  ||  WorkTimeline::validateAdd($data, $extra)
            if ( Order::validateAdd($data, $extra)  ) {
		
                
                $currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                $fields1 = "*";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);              
                if(!empty($currency1)){
                   $data['currency_abbr'] = $currency1[0]['abbr'];
                   $data['currency_name'] = $currency1[0]['currency_name'];
                   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
                   $data['currency_country'] = $currency1[0]['country_name'];
                }
                
                
                $company1 =null;
                $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                 $fields_c1 = " name,prefix";
                Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
                if(!empty($company1)){                  
                   $data['company_name'] = $company1[0]['name'];
                   $data['company_prefix'] = $company1[0]['prefix']; 
                }
				
				
                if(!empty($data['po_filename'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['po_filename']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "PO-".$data['number'].".".$ext ;
                    $attachfilename = "PO-".mktime().".".$ext ;
                    $data['po_filename'] = $attachfilename;
                    
                    /*if (move_uploaded_file ($files['po_filename']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                        @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,'0777');
                    }*/
                    if (move_uploaded_file ($files['po_filename']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                        @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                          
                    }
                }
                
                if(!empty($data['filename_1'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_1']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                    //$attachfilename = "WO1-".$data['number'].".".$ext ;
                    $attachfilename = "WO1-".mktime().".".$ext ;
                     $data['filename_1'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_1']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                       
                      @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
                
                if(!empty($data['filename_2'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_2']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "WO2-".$data['number'].".".$ext ;
                    $attachfilename = "WO2-".mktime().".".$ext ;
                     $data['filename_2'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_2']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                         @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
                
                if(!empty($data['filename_3'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_3']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "WO3-".$data['number'].".".$ext ;
                    $attachfilename = "WO3-".mktime().".".$ext ;
                     $data['filename_3'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_3']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                       @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
              
                if($data['isrenewable']){                    
                    $do_e  =date('Y-m-d H:i:s', $data['do_e']) ;
                    $do_fe  =date('Y-m-d H:i:s', $data['do_fe']) ;
                }
                $clients_su_str='';
                if(!empty($data['clients_su'])){
					$clients_su_str = implode(",", $data['clients_su']);
					$clients_su_str = ",".$clients_su_str."," ;
				}
				
				$tax_ids_str='';
				$is_proforma_invoice=1;
				if(!empty($data['alltax_ids'])){
					$tax_ids_str = implode(",", $data['alltax_ids']);
					$tax_ids_str = ",".$tax_ids_str."," ;					 
				}				 
            
                 $query	= " INSERT INTO ".TABLE_BILL_ORDERS
                            ." SET ". TABLE_BILL_ORDERS .".number           = '". $data['number'] ."'"
                                //.",". TABLE_BILL_ORDERS .".po_id = '".          $data['po_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".service_id       = '". implode(',', $data['service_id']) ."'"
                                .",". TABLE_BILL_ORDERS .".access_level     = '". $data['access_level'] ."'"
                                .",". TABLE_BILL_ORDERS .".client           = '". $data['client']['user_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".created_by       = '". $data['created_by'] ."'"
                                .",". TABLE_BILL_ORDERS .".company_id	    = '". $data['company_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".company_name	    = '". $data['company_name'] ."'"
                                .",". TABLE_BILL_ORDERS .".company_prefix   = '". $data['company_prefix'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_title	    = '". $data['order_title'] ."'"
                                .",". TABLE_BILL_ORDERS .".delivery_at	    = '". $data['delivery_at'] ."'"
                                .",". TABLE_BILL_ORDERS .".tax_ids	    	= '". $tax_ids_str ."'"
                                .",". TABLE_BILL_ORDERS .".is_proforma_invoice  = '". $is_proforma_invoice ."'"
                                .",". TABLE_BILL_ORDERS .".order_domain	    = '". $data['order_domain'] ."'"
                                .",". TABLE_BILL_ORDERS .".currency_id      = '". $data['currency_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".currency_abbr    = '". $data['currency_abbr'] ."'"
                                 .",".TABLE_BILL_ORDERS .".currency_name    = '". $data['currency_name'] ."'"
                                .",". TABLE_BILL_ORDERS .".currency_symbol  = '". $data['currency_symbol'] ."'"
                                .",". TABLE_BILL_ORDERS .".currency_country = '". $data['currency_country'] ."'"
                                .",". TABLE_BILL_ORDERS .".quotation_no     = '". $data['quotation_no'] ."'"
                                .",". TABLE_BILL_ORDERS .".source_to        = '". $data['source_to'] ."'"
                                .",". TABLE_BILL_ORDERS .".source_from      = '". $data['source_from'] ."'"
                                .",". TABLE_BILL_ORDERS .".po_filename      = '". $data['po_filename'] ."'"
                                .",". TABLE_BILL_ORDERS .".filecaption_1    = '". $data['filecaption_1'] ."'"
                                .",". TABLE_BILL_ORDERS .".filename_1       = '". $data['filename_1'] ."'"
                                .",". TABLE_BILL_ORDERS .".filecaption_2    = '". $data['filecaption_2'] ."'"
                                .",". TABLE_BILL_ORDERS .".filename_2       = '". $data['filename_2'] ."'"
                                .",". TABLE_BILL_ORDERS .".filecaption_3    = '". $data['filecaption_3'] ."'"
                                .",". TABLE_BILL_ORDERS .".filename_3       = '". $data['filename_3'] ."'"
                                .",". TABLE_BILL_ORDERS .".amount           = '". $data['amount'] ."'"
                                .",". TABLE_BILL_ORDERS .".existing_client	= '". $data['existing_client'] ."'"
                                .",". TABLE_BILL_ORDERS .".client_s_client	= '". $data['client_s_client'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_type	    = '". $data['order_type'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_closed_by	= '". $data['order_closed_by'] ."'"
                                .",". TABLE_BILL_ORDERS .".team             = ',". implode(",", $data['team']) .",'"
								.",". TABLE_BILL_ORDERS .".clients_su       = '". $clients_su_str ."'" 
                               // .",". TABLE_BILL_ORDERS .".particulars 	= '". $data['particulars'] ."'"
                                .",". TABLE_BILL_ORDERS .".details          = '". $data['details'] ."'"
                                .",". TABLE_BILL_ORDERS .".do_o             = '". date('Y-m-d H:i:s', $data['do_o']) ."'"
                                .",". TABLE_BILL_ORDERS .".do_c             = '". date('Y-m-d H:i:s') ."'"
                                .",". TABLE_BILL_ORDERS .".do_d             = '". date('Y-m-d H:i:s', $data['do_d']) ."'"
                                .",". TABLE_BILL_ORDERS .".do_e             = '". $do_e."'"
                                .",". TABLE_BILL_ORDERS .".do_fe            = '". $do_fe."'"
                                .",". TABLE_BILL_ORDERS .".project_manager  = '". $data['project_manager']."'"
                                .",". TABLE_BILL_ORDERS .".st_date          = '". $data['st_date']."'"
                                .",". TABLE_BILL_ORDERS .".es_ed_date       = '". $data['es_ed_date']."'"
                                .",". TABLE_BILL_ORDERS .".es_hrs           = '". $data['es_hrs']."'"
                                .",". TABLE_BILL_ORDERS .".status           = '". $data['status'] ."'" 
                                .",". TABLE_BILL_ORDERS .".is_renewal_ord   = '". $data['is_renewal_ord'] ."'"
                                .",". TABLE_BILL_ORDERS .".ip           = '".$_SERVER['REMOTE_ADDR']."'" ;
               
                if ( $db->query($query) ) {
                    
                    $variables['hid'] = $db->last_inserted_id();
                    $messages->setOkMessage("New Order has been created.");
                    //Mark Quotation No As Completed BOF
                      
                    if(!empty($data['quotation_no'])){
						$access_level   = $my['access_level'];
						$extra = array( 'db'           => &$db,
								'access_level' => $access_level,
								'messages'     => &$messages,                      
								);
                        $quotstatus = Quotation::COMPLETED ;
                        Quotation::updateStatus($data['quotation_no'], $quotstatus, $extra);
                    }
                    //Mark Quotation No As Completed EOF
                    
                    
                    
                    //After insert, update order counter in 
                    //updateCounterOf($db,'ORD', $data['company_id']);                                       
                    //updateOldCounterOf($db,'ORD', $data['company_id'],$data['financialYr']);    
					updateOrderCounterNumber($db, $data['financialYr']);					
                    // Insert the Particulars.
                    if(!empty($data['query_p'])){
                        if ( !$db->query($data['query_p']) ) {
                            $messages->setErrorMessage("The Particulars were not Saved.");
                        }
                    }
                    // Send the notification mails to the concerned persons.
                   
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['number'] ."'";
                    Order::getParticulars($db, $temp, 'particulars,ss_punch_line', $condition_query);
                   if(!empty($temp)){                    
                        foreach ( $temp as $pKey => $parti ) {
                            $temp_p[] = array(
                                            'particulars'=>$temp[$pKey]['particulars'],
                                            'ss_punch_line' =>$temp[$pKey]['ss_punch_line']  
                                            );
                        }                            
                    }
                    
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $file_name='';
                    // Send Email to the Client.
                    $data['link']   = DIR_WS_MP .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    $data['af_name'] =$my['f_name'];
                    $data['al_name'] = $my['l_name'] ;
                    $data['particulars']=$temp_p;
                   if( isset($data['mail_client']) &&  $data['mail_client']==1 ){
                        $email = NULL;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'ORDER_CREATE_TO_CLIENT', $data, $email) ) {
                            $to     = '';                           
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                        }
                    }
                    
                    
                    //Send mail after create invoice to admin bof/
                    
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    $email = NULL;
                     $cc_to_sender= $cc = $bcc = Null;
                     if ( getParsedEmail($db, $s, 'ORDER_CREATE_TO_ADMIN', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $bill_ord_name , 'email' => $bill_ord_email);                           
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                        
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                       
                    }
                    
                    
                    //Send mail after create invoice to admin eof
                    /*                   
                   $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_or_add', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_or_add_al', 'access_level'=>($data['access_level']-1) ),
                                                            // array('right'=>'nc_bl_in_add', 'access_level'=>$data['access_level']),
                                                            // array('right'=>'nc_bl_in_add_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    
                    // Organize the data to be sent in email.
                  
                    // Read the Client Manager information.
                    
                    include ( DIR_FS_INCLUDES .'/clients.inc.php');
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    // Send mail to the Concerned Executives with same access level and higher access levels
                    if(isset($data['mail_exec_ac'])){
                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_OR_NEW_MANAGERS', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }
                    $data['link']   = DIR_WS_MP .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    // Send Email to the Client.
                    if(isset($data['mail_client'])){ 
                     
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_CLIENT', $data, $email) ) {                       
                            $to     = '';     
                          
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }    
                    // Send Email to the Client Manager / creator email .
                    if(isset($data['mail_client_mgr'])){
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_CLIENT_MANAGER', $data, $email) ) {
                            
                            $to     = '';
                            $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                    // Send Email to Team Members.
                    if(isset($data['mail_executive'])){    
                         
                        // For Speed notification is sent in one email to all the team members.
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_TEAM', $data, $email) ) {
                          
                            $to     = '';
                            foreach ( $data['team_members'] as $key=>$executive) {
                                $to[]   = array('name' => $executive['f_name'] .' '. $executive['l_name'], 'email' => $executive['email']);
                            }
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }*/
                    
                    // Send Email to the Creator of the Order.
                    /*
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'NOT REQUIRED, SO NOT DEFINED', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    */
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $ord_no = $data['number'];
                $_ALL_POST['number'] = getOldCounterNumber($db,'ORD',$data['company_id'],$data['financialYr']); 
                $_ALL_POST['exchange_rate'] = 1; 
                $data		= NULL;
            }
        }
        else {           
            
          
        }
 
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/bill-order.php?perform=add&added=1&hid=".$variables['hid']."&or_no=".$ord_no);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/bill-order.php");
        }
       
        if(isset($_POST['btnReturnInv']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/bill-invoice-profm.php?perform=add&or_id=".$variables['hid']);
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {            
            header("Location:".DIR_WS_NC."/bill-order.php?perform=list&added=1&hid=".$variables['hid']."&or_no=".$ord_no);
        }
        else {
				
            //Code default Aryan Salve,Mohit Salve,Leena Salve,Roshni,Stella  in team list EOF
            if(!isset($_ALL_POST['team'])){
                $temp ="e68adc58f2062f58802e4cdcfec0af2d','38570b0a7e923f3349e40a4bcc5f6e31','3752e1f118290855d8e5c122f6606a50',
				'054ef5e36ceeaa060c0d2da97ab5f932','4df83558e5b6d3592d8998e9b552d37a";
                User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN ('". $temp ."')");
                $_ALL_POST['team'] = array();
                foreach ( $_ALL_POST['team_members'] as $key=>$members){
                    $_ALL_POST['team'][] = $members['user_id'];
                    $_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
                }
            }
            //Code default aryan manke in team list BOF
            if(!isset($_ALL_POST['order_type'])){
                $_ALL_POST['order_type']= Order::TARGETED ;
            }
            if(!isset($_ALL_POST['order_closed_by'])){
                $_ALL_POST['order_closed_by']='e68adc58f2062f58802e4cdcfec0af2d' ;
            }
            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['po_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['po_access_level'], $al_list, $index);
                $_ALL_POST['po_access_level'] = $al_list[$index[0]]['title'];
            }
            
            $hidden[] = array('name'=> 'po_id' ,'value' => $po_id);
            $hidden[] = array('name'=> 'lead_id' ,'value' => $lead_id);
            $hidden[] = array('name'=> 'q_id' ,'value' => $q_id);
            
            //$hidden[] = array('name'=> 'tbl_name' ,'value' => $tbl_name);
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lead_id', 'value' => 'lead_id');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'lst_work_stage', 'value' => 'lst_work_stage');
			$page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
			$page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
			$page["var"][] = array('variable' => 'lst_fyr', 'value' => 'lst_fyr');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'rejectedFiles', 'value' => 'rejectedFiles');
          
            $page["var"][] = array('variable' => 'source', 'value' => 'source');
			$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>