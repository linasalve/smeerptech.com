<?php
if ( $perm->has('nc_pop_list') ) {

	
	if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
	if(empty($condition_query)){
 
		$_SEARCH["chk_status"]  = 'AND';
		$_SEARCH["sStatus"]    = array(PurchaseOrderPdf::ACTIVE,PurchaseOrderPdf::PENDING);
		$condition_query = " WHERE ".TABLE_PURCHASE_ORDER_PDF .".status 
		IN ('".PurchaseOrderPdf::ACTIVE ."','".PurchaseOrderPdf::PENDING."')";
		$perform = 'search';
        $condition_url   = "&chk_status=".$_SEARCH["chk_status"]."&sStatus=".PurchaseOrderPdf::ACTIVE.",".PurchaseOrderPdf::PENDING ;  
		
	}
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    
	
	
	
    // To count total records.
    $list	= 	NULL;
   
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;    
    
    $list	= NULL;
    $fields = TABLE_PURCHASE_ORDER_PDF .'.id'
		.','. TABLE_PURCHASE_ORDER_PDF .'.number'
		.','. TABLE_PURCHASE_ORDER_PDF .'.or_no'
		.','. TABLE_PURCHASE_ORDER_PDF .'.or_id'
		.','. TABLE_PURCHASE_ORDER_PDF .'.access_level'
		.','. TABLE_PURCHASE_ORDER_PDF .'.do_fe'
		.','. TABLE_PURCHASE_ORDER_PDF .'.do_e'
		.','. TABLE_PURCHASE_ORDER_PDF .'.do_c'
		.','. TABLE_PURCHASE_ORDER_PDF .'.do_d'
		.','. TABLE_PURCHASE_ORDER_PDF .'.do_i'
		.','. TABLE_PURCHASE_ORDER_PDF .'.currency_abbr'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_name'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_value'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_total_amount'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_sub1_name'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_sub1_value'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_sub1_total_amount'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_sub2_name'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_sub2_value'
		.','. TABLE_PURCHASE_ORDER_PDF .'.tax1_sub2_total_amount'
		.','. TABLE_PURCHASE_ORDER_PDF .'.sub_total_amount'
		.','. TABLE_PURCHASE_ORDER_PDF .'.round_off_op'
		.','. TABLE_PURCHASE_ORDER_PDF .'.round_off'  
		.','. TABLE_PURCHASE_ORDER_PDF .'.amount'
		.','. TABLE_PURCHASE_ORDER_PDF .'.amount_inr' 
		.','. TABLE_PURCHASE_ORDER_PDF .'.status' 
		.','. TABLE_PURCHASE_ORDER_PDF .'.reason_of_delete'
		.','. TABLE_PURCHASE_ORDER .'.order_title'
		.','. TABLE_PURCHASE_ORDER .'.id as order_id'
		.','. TABLE_PURCHASE_ORDER .'.bill_dt as do_o'
		.','. TABLE_CLIENTS .'.user_id AS c_user_id'
		.','. TABLE_CLIENTS .'.number AS c_number'
		.','. TABLE_CLIENTS .'.org'
		.','. TABLE_CLIENTS .'.f_name AS c_f_name'
		.','. TABLE_CLIENTS .'.l_name AS c_l_name'
		.','. TABLE_CLIENTS .'.email AS c_email'
		.','. TABLE_CLIENTS .'.mobile1'
		.','. TABLE_CLIENTS .'.mobile2'
		.','. TABLE_CLIENTS .'.billing_name '
		.','. TABLE_CLIENTS .'.status AS c_status';
	$total	= PurchaseOrderPdf::getDetails( $db, $list, '', $condition_query);
	$list=array();
	$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','purchase-order-pdf.php','frmSearch');
	PurchaseOrderPdf::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    $flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
           //check invoice id followup in ST BOF
			$val['ticket_id']='';
			$sql2 = "SELECT ticket_id FROM ".TABLE_ST_TICKETS." WHERE 
			(".TABLE_ST_TICKETS.".pflw_ord_id = '".$val['order_id']."') AND 
			".TABLE_ST_TICKETS.".ticket_child = 0  LIMIT 0,1";
			if ( $db->query($sql2) ) {
				if ( $db->nf() > 0 ) {
				   while ($db->next_record()) {
					   $val['ticket_id']= $db->f('ticket_id');
				   }
				}                   
			}
			//check invoice id followup in ST EOF
			$val['amount']=number_format($val['amount'],2);
			$val['amount_inr']=number_format($val['amount_inr'],2);
			$flist[$key]=$val;
        }
    }    
    // Set the Permissions. 
   
    $variables['can_view_list'] = false;
	$variables['can_add'] = false;
	$variables['can_edit'] = false;
	$variables['can_delete'] = false;
	$variables['can_view'] = false;
	$variables['can_change_status'] = false;
	$variables['can_add_ord'] = false;
	
	
	
    if ( $perm->has('nc_pop_list') ) {
        $variables['can_view_list'] = true;
    }   
    if ( $perm->has('nc_pop_edit') ) {
        $variables['can_edit'] = true;
    }	
    if ( $perm->has('nc_pop_view') ) {
        $variables['can_view'] = true;
    }    
    if ( $perm->has('nc_pop_status') ) {
        $variables['can_change_status'] = true;
    }
	if ( $perm->has('nc_pop_delete') ) {
        $variables['can_delete'] = true;
    }
	if ( $perm->has('nc_po') || $perm->has('nc_po_add') ) {
        $variables['can_add_ord'] = true;
    } 
	//can view list of payment party bills transactions
	 
	 
    $variables['bills_path'] = DIR_WS_PO_FILES;
    $page["var"][] = array('variable' => 'list', 'value' => 'flist');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
	$page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order-pdf-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
	
}
?>
