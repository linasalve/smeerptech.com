<?php
	if ( $perm->has('nc_bl_rcpt_delete') ) {
        $rcpt_id	= isset($_GET["rcpt_id"]) ? $_GET["rcpt_id"] 	: ( isset($_POST["rcpt_id"]) ? $_POST["rcpt_id"] : '' );
		include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
        $access_level = $my['access_level'];
        if ( $perm->has('nc_bl_rcpt_delete_al') ) {
            $access_level += 1;
        }
		/*
		$access_level_ot = 0;
        if ( $perm->has('nc_bl_rcpt_delete_ot') ) {
            $access_level_ot = $my['access_level'];
			if ( $perm->has('nc_bl_rcpt_delete_ot_al') ) {
				$access_level_ot += 1;
			}
        }*/
        
       
        
        //Receipt::delete($rcpt_id, $extra);
        
		$variables['hid'] = $rcpt_id;
		
        
        $receipt = NULL;
        if ( (Receipt::getList($db, $receipt, 'id, number, amount,amount_inr, inv_no, client,access_level, created_by, status', " WHERE id = '$rcpt_id'")) > 0 ) {
				$receipt = $receipt[0];
				// Check Access Level for the receipt.
				if ( $receipt['access_level'] > $access_level ) {
					$messages->setErrorMessage("You do not have the Right to Delete Receipt with the current Access Level.");
				}
				else {
					if ( $receipt['status'] == Receipt::BLOCKED ) {
                       
					}
					else {
						$messages->setErrorMessage("To delete the Receipt, change its status to blocked.");
					}
				}
        }
        else {
            $messages->setErrorMessage("The Receipt was not found.");
        }
        
        if( isset($_POST['btnCancel'])){
            header("Location:".DIR_WS_NC."/bill-receipt.php?perform=list");
        }        
        if($messages->getErrorMessageCount() > 0){
            // Display the  list.
            $perform='list';
            include ( DIR_FS_NC .'/bill-receipt-list.php');
            
         }else{
            
            if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
                $data		= processUserData($_POST);
                if(empty($data['reason_of_delete'])){
                    $messages->setErrorMessage("Please Specify Reason To Delete The Receipt ".$data['number']);
                    
                }
                if($messages->getErrorMessageCount() <= 0){                    
                    
                    $extra = array( 'db'           		=> &$db,
                                    'my' 				=> $my,
                                    'access_level' 		=> $access_level,
                                    'data' 	=> $data,
                                    'messages'     		=> &$messages
                    );
                    Receipt::delete($rcpt_id, $extra);                    
                    if($messages->getErrorMessageCount() <= 0){
                    
                        //Add Followup of order delete BOF
                        /*
                        $query = " SELECT followup_no,lead_id,client FROM ".TABLE_SALE_FOLLOWUP." WHERE 
                                ".TABLE_SALE_FOLLOWUP.".followup_of = '".$rcpt_id."' AND ".TABLE_SALE_FOLLOWUP .".table_name='".TABLE_BILL_RCPT."'" ;
                        $db->query($query);
                        if($db->nf()>0){
                            $db->next_record();
                            $flwnumber=$db->f('followup_no');
                            $lead_id=$db->f('lead_id');
                            $client=$db->f('client');
                            $remarks = " Receipt has been deleted \n "." Reason To Delete Receipt : ".$data['reason_of_delete'] ;
                        }                             
                        $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                            ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"                        
                            .",". TABLE_SALE_FOLLOWUP .".remarks 		    = '".$remarks."'"
                            .",". TABLE_SALE_FOLLOWUP .".ip         	    = '". $_SERVER['REMOTE_ADDR'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".lead_id    	    = '". $lead_id ."'"
                            .",". TABLE_SALE_FOLLOWUP .".client    	        = '". $client ."'"
                            .",". TABLE_SALE_FOLLOWUP .".followup_of        = '". $rcpt_id ."'"
                            .",". TABLE_SALE_FOLLOWUP .".table_name    	    = '".TABLE_BILL_RCPT."'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by 	    = '". $my['uid'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".access_level 	    = '". $my['access_level'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by_dept    = '". $my['department'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".flollowup_type     = '". FOLLOWUP::RECEIPT ."'"
                            .",". TABLE_SALE_FOLLOWUP .".do_e 			    = '". date('Y-m-d h:i:s') ."'";                                
                        $db->query($query);*/
                        //Add Followup of order delete EOF
                        //$messages->setOkMessage("The Receipt has been deleted.");  
                        header("Location:".DIR_WS_NC."/bill-receipt.php?perform=list&deleted=1&rcp_no=".$data['number']);
                    }else{
                        $messages->setErrorMessage("The Receipt was not deleted.");
                    }                        
                }
                else {
                    $messages->setErrorMessage("The Receipt was not deleted.");
                }                   
            }           
        }
        $hidden[] = array('name'=> 'rcpt_id' ,'value' => $rcpt_id);
        $hidden[] = array('name'=> 'perform' ,'value' => $perform);
        $hidden[] = array('name'=> 'number' ,'value' => $receipt['number']);
        $hidden[] = array('name'=> 'act' , 'value' => 'save');
        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-receipt-delete.html');          
        
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete.");
    }
?>