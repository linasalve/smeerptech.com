<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
   
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    //$table = TABLE_PAYMENT_TRANSACTION ;
                    foreach( $field_arr as $key => $field ) {                      	   
                      	$condition_query .= " ". $table .".". clearSearchString($field,'-') ." LIKE '%". $sString ."%' OR " ;
                        $where_added = true;
                    }                 
                }
            }
            $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType,'-') ." LIKE '%". $sString ."%' ) " ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
  
    $sStatusStr='';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' ) && isset($sStatus)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
       $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".status IN ('". $sStatusStr ."') ";
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";   
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
    }
 
    
    // EO: Status data.
    
     // BO: Type data.
    $sTrTypeStr='';
    $chk_type = isset($_POST["chk_type"])   ? $_POST["chk_type"]  : (isset($_GET["chk_type"])   ? $_GET["chk_type"]   :'');
    $sTrType    = isset($_POST["sTrType"])      ? $_POST["sTrType"]     : (isset($_GET["sTrType"])      ?$_GET["sTrType"]     :'');
    if ( ($chk_type == 'AND' || $chk_type == 'OR' ) && isset($sTrType)) {
        if ( $where_added ) {
           $condition_query .= " ". $chk_type;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
       
        
        if(is_array($sTrType)){
             $sTrTypeStr = implode("','", $sTrType) ;
             $sTrTypeStr1 = implode(",", $sTrType) ;
             
        }else{
            $sTrTypeStr = str_replace(',',"','",$sTrType);
            $sTrTypeStr1 = $sTrType ;
            $sTrType = explode(",",$sTrType) ;
            
        }
       
       $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".transaction_type 	 IN ('". $sTrTypeStr ."') ";
       $condition_url          .= "&chk_type=$chk_type&sTrType=$sTrTypeStr1";   
       $_SEARCH["chk_type"]  = $chk_type;
       $_SEARCH["sTrType"]     = $sTrType;
    }
 
    // EO: Type data.
   
    // BO: is_pdc data.
    $is_pdcStr='';
    $chk_is_pdc = isset($_POST["chk_is_pdc"])   ? $_POST["chk_is_pdc"]  : (isset($_GET["chk_is_pdc"])   ? $_GET["chk_is_pdc"]   :'');
    $is_pdc    = isset($_POST["is_pdc"])      ? $_POST["is_pdc"]     : (isset($_GET["is_pdc"])      ? $_GET["is_pdc"]      :'');
    if ( ($chk_is_pdc == 'AND' || $chk_is_pdc == 'OR' ) && !empty($is_pdc)) {    	
        if ( $where_added ) {
            $condition_query .= " ". $chk_is_pdc;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($is_pdc)){
             $is_pdcStr = implode(",", $is_pdc) ;
             
        }else{
            $is_pdcStr = $is_pdc ;
            $is_pdc = explode(",",$is_pdc) ;
            
        }        
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".is_pdc IN ('". $is_pdcStr ."') ";
        $condition_url          .= "&chk_is_pdc=$chk_is_pdc&is_pdc=$is_pdcStr";
   
        $_SEARCH["chk_is_pdc"]  = $chk_is_pdc;
        $_SEARCH["is_pdc"]     = $is_pdc;
    }  
    // EO: is_pdc data.
    //Bank Id BOF
    $bank_id    = isset($_POST["bank_id"])      ? $_POST["bank_id"]     : (isset($_GET["bank_id"])      ? $_GET["bank_id"]      :'');
    $bank_id_type   = isset($_POST["bank_id_type"])      ? $_POST["bank_id_type"]     : (isset($_GET["bank_id_type"])      ? $_GET["bank_id_type"]      :'');

    if (  isset($bank_id) && !empty($bank_id) && !empty($bank_id_type)) {
        if ( $where_added ) {
           $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if($bank_id_type=='in'){
            
          $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id ='". $bank_id ."' ";
        }elseif($bank_id_type=='out'){
        
         $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id ='". $bank_id ."' ";
        }
        
      
        $condition_url          .= "&bank_id=$bank_id&bank_id_type=$bank_id_type";
   
      
    }
    //Bank Id EOF
    
    
    
    
    
    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    
    // BO: From Date
    if ( ( $chk_date_from == 'AND' || $chk_date_from == 'OR' ) && !empty($date_from)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".do_transaction >= '". $dfa ."'";
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
    }
    
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".do_transaction <= '". $dta ."'";
         $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
    }
   
    // EO: Upto Date
  
    
    
    
    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/payment-transaction-choose-list.php');
?>
