<?php
if ( $perm->has('nc_plp_ap_list') ) {

    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    include_once ( DIR_FS_INCLUDES .'/calendar.inc.php' );
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	Calendar::getList( $db, $list, '', $condition_query);
    
    
   // $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    $list	= NULL;
    $fields = TABLE_SETTINGS_CALENDAR.'.id'.','.TABLE_SETTINGS_CALENDAR.'.title';
    Calendar::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
   
    // Set the Permissions.
    $variables['can_view_list'] = false;
    $variables['can_add'] = false;
    $variables['can_edit'] = false;
    $variables['can_delete'] = false;
    $variables['can_status'] = true;
    $variables['can_view_details'] = false;

    if ( $perm->has('nc_plp_ap_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_plp_ap_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_plp_ap_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_plp_ap_status') ) {
        $variables['can_status'] = true;
    }
    if ( $perm->has('nc_plp_ap_delete') ) {
        $variables['can_delete'] = true;
    }
    if ( $perm->has('nc_plp_ap_details') ) {
        $variables['can_view_details'] = true;
    }
    
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'plp-performance-appraisal-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
