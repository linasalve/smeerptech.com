<?php
// $start['time'] = time();
// $start['microtime'] = microtime();
    
    if ( $perm->has('nc_site_sp_edit') ) {
        $page_id = isset($_GET["page_id"]) ? $_GET["page_id"] : ( isset($_POST["page_id"]) ? $_POST["page_id"] : '' );
        $act	= isset($_GET["act"]) ? $_GET["act"] : (isset($_POST["act"]) ? $_POST["act"] : '');
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        $al_list    = getAccessLevel($db, $access_level);
        
        //code for remove banner BOF
        if(isset($act) && $act =='rmvbanner' && !empty($page_id)){
            $getData=array();
            $table = TABLE_SETTINGS_STATICPAGES ;
            $condition2 = " WHERE ".TABLE_SETTINGS_STATICPAGES.".page_id= '".$page_id ."' " ;
            $fields1 =  TABLE_SETTINGS_STATICPAGES.'.banner,'.TABLE_SETTINGS_STATICPAGES.'.is_default_banner' ;
            $getData= getRecord($table,$fields1,$condition2);
            if(!empty($getData['banner'])){
               $banner_name= $getData['banner'] ;
               @unlink(DIR_FS_BANNER ."/". $banner_name);            
            }
            if($getData['is_default_banner']=='0'){
                $query_update = "UPDATE ". TABLE_SETTINGS_STATICPAGES
                                        ." SET ".TABLE_SETTINGS_STATICPAGES.".banner	= '' "
                                        .",". TABLE_SETTINGS_STATICPAGES.".is_default_banner	= '0'
                                         WHERE ".TABLE_SETTINGS_STATICPAGES .".page_id		= '". $page_id ."'";
                $db->query($query_update);
                $messages->setOkMessage("Banner deleted successfully.");
            }else{
                $messages->setErrorMessage("Sorry! unable to delete the banner as it is marked as default banner.");
            }
        }
        //code for remove banner EOF

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list
                        );
            
            if ( Staticpages::validateUpdate($data, $extra) ) {
				// Code for banner BOF
                $data["banner"] = $banner_sub=$banner_name ='' ;
                if ( isset($_FILES) && @$_FILES['banner']['error'] == 0 && $_FILES['banner']['name'] != '' ) {
                    $srcSize 		= getimagesize($_FILES['banner']['tmp_name']);
                    $banner_name 	= eregi_replace(" ", "_", $_FILES['banner']['name']);
                    
                    if ( !in_array($srcSize["mime"], $BANNER['MIME']) ) {
                        $messages->setErrorMessage("The image type '". $srcSize["mime"] ."' is not supported.");
                    }
                    if ( $_FILES["banner"]["size"] > $BANNER['MAX_SIZE']['bytes'] ) {
                        $messages->setErrorMessage("The image size is more than supported of ". $BANNER['MAX_SIZE']['display'] ." .");
                    }
                    if ( $srcSize['0'] > $BANNER['MAX_DIM']['width'] || $srcSize['1'] > $BANNER['MAX_DIM']['height'] ) {
                        $messages->setErrorMessage("The image dimension is larger than supported.");
                    }
                    if ( file_exists(DIR_FS_BANNER .'/'. $banner_name) ) {
                        $messages->setErrorMessage("A file with the same name already exists. Please rename and then upload.");
                    }
                }
                if( empty($_FILES['banner']['name'])  && $data["is_default_banner"]==1){
                    $messages->setErrorMessage("Please upload the default banner.");
                }
                // Code for banner EOF
					
					
				if ( $messages->getErrorMessageCount() <= 0 ) {
						// There are no errors, copy the banner file.
                    // Make the Directory writable.
                    if(!empty($banner_name)){                    
                        // Make the Directory writable.
                        @chmod(DIR_FS_BANNER, 0777) ;
                        if ( @copy($_FILES['banner']['tmp_name'], DIR_FS_BANNER ."/". $banner_name) ) {
                            $data["banner"] = $banner_name;
                            $banner_sub = ",".TABLE_SETTINGS_STATICPAGES.".banner = '". $data["banner"] ."'"	;
                        }else{
                        $messages->setErrorMessage("The file was not uploaded, try again.");
                        }
                    }
				
                    $query  = " UPDATE ". TABLE_SETTINGS_STATICPAGES
                            ." SET "
                                . TABLE_SETTINGS_STATICPAGES .".page_name			= '". $data["page_name"] ."'"
                            .",". TABLE_SETTINGS_STATICPAGES .".page_intro			= '". $data["page_intro"] ."'"
                            .",". TABLE_SETTINGS_STATICPAGES .".metakeyword			= '". $data["metakeyword"] ."'"
                            .",". TABLE_SETTINGS_STATICPAGES .".metadesc			= '". $data["metadesc"] ."'"
                            .",". TABLE_SETTINGS_STATICPAGES .".heading				= '". $data["heading"] ."'"
                            //.",". TABLE_SETTINGS_STATICPAGES .".banner				= '". $data["banner"] ."'"	
                            .$banner_sub
                            .",". TABLE_SETTINGS_STATICPAGES .".is_default_banner	= '". $data["is_default_banner"] ."'"	
                            .",". TABLE_SETTINGS_STATICPAGES .".content				= '". $data["content"] ."'"
                            .",". TABLE_SETTINGS_STATICPAGES .".page_status			= '". $data["page_status"] ."'"
                            .",". TABLE_SETTINGS_STATICPAGES .".last_edited_by		= '". $my['user_id'] ."'"
                            .",". TABLE_SETTINGS_STATICPAGES .".access_level		= '". $data['access_level'] ."'" 
                            ." WHERE ". TABLE_SETTINGS_STATICPAGES .".page_id   	= '". $data['page_id'] ."'";
                    if ( $db->query($query) ) {
                        $messages->setOkMessage("Page has been updated.");
                        
                        //to flush the data.
                        //$_ALL_POST  = NULL;
                        $data       = NULL;
                    }
                    else {
                        $messages->setErrorMessage('Page was not updated.');
                    }
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $page_id;
            include ( DIR_FS_NC .'/staticpage-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE page_id = '". $page_id ."' ";
                $_ALL_POST      = NULL;
                if ( Staticpages::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                }
            }

            if ( !empty($_ALL_POST['page_id']) ) {

                // Check the Access Level.
               

                    //print_r($_ALL_POST);
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'page_id', 'value' => $page_id);
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
        
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'staticpage-edit.html');
                
            }
            else {
                $messages->setErrorMessage("The Selected Page was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>