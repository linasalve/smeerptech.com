<?php

    if ( $perm->has('nc_vbk_add') ) {    
        include_once ( DIR_FS_CLASS .'/RegionVendorBank.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneVendorBank.class.php');
        include_once ( DIR_FS_CLASS .'/ReminderVendorBank.class.php');
    	include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
        //include ( DIR_FS_INCLUDES .'/sale-industry.inc.php');
        
        $_ALL_POST	= NULL;
        $data       = NULL;
        //$region     = new Region('91');
        $region     = new RegionVendorBank('91','21','231');
        $phone      = new PhoneVendorBank(TABLE_VENDORS_BANK);
        $reminder   = new ReminderVendorBank(TABLE_VENDORS_BANK);
        
        
		$al_list    = getAccessLevel($db, $my['access_level']);
		$role_list  = VendorsBank::getRoles($db, $my['access_level']);
        
        $grade_list  = VendorsBank::getGrades();
        
         //For Vendors list allotment
        if ( $perm->has('nc_vbk') && $perm->has('nc_vbk_list')) {
            $variables['can_view_clist']     = true;
        }
       
        
        //BOF read the available services
		$authorization_list  = VendorsBank::getAuthorization();
        $services_list  = VendorsBank::getServices();
        $message_list  = VendorsBank::getMessages();
        //EOF read the available services
        
        //BOF read the available industries
        $industry_list = NULL;
        $required_fields ='*';
        $condition_query="WHERE status='".Industry::ACTIVE."'";
		Industry::getList($db,$industry_list,$required_fields,$condition_query);
        //EOF read the available industries
        
        //BOF read the available industries
		VendorsBank::getCountryCode($db,$country_code);
         //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
			// BO: For generating default password.
			$data['password']	= getRandom(6, '', array_merge($letters_b, $letters_s, $digits));
			$data['re_password']= $data['password'];
			// EO: For generating default password.

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder
                        );
            
            if ( VendorsBank::validateAdd($data, $extra) ) {
                
                $service_ids=$_POST['service_id'];
                $service_idstr=implode(",",$service_ids);
                
                if(!empty($service_idstr)){
                    $service_idstr=",".$service_idstr.",";	
                }
                $authorization_ids=$_POST['authorization_id'];
                $authorization_idstr=implode(",",$authorization_ids);
                
                if(!empty($authorization_idstr)){
                    $authorization_idstr=",".$authorization_idstr.",";	
                }
				
                $industry_ids=$_POST['industry'];
                $industry_idstr=implode(",",$industry_ids);
                
                if(!empty($industry_idstr)){
                    $industry_idstr=",".$industry_idstr.",";
                }
                $alloted_clientsstr='';
                $alloted_clients=$_POST['alloted_clients'];
                $alloted_clientsstr=implode(",",$alloted_clients);
               
                if(!empty($alloted_clientsstr)){
                    $alloted_clientsstr=",".$alloted_clientsstr.",";
                }
                //Default account manager aryan sir
                if(!isset( $data['team'])){
                    $team = 'e68adc58f2062f58802e4cdcfec0af2d';
                }else{
                    $team=implode(',', $data['team']);
                }
                
                 $query	= " INSERT INTO ". TABLE_VENDORS_BANK
                        ." SET ". TABLE_VENDORS_BANK .".user_id     	= '". $data['user_id'] ."'"
                            .",". TABLE_VENDORS_BANK .".service_id   = '". $service_idstr ."'"
							.",". TABLE_VENDORS_BANK .".authorization_id   = '". $authorization_idstr ."'"
                            .",". TABLE_VENDORS_BANK .".number      	= '". $data['number'] ."'"
							.",". TABLE_VENDORS_BANK .".manager 	   	= '". $data['manager'] ."'"
							.",". TABLE_VENDORS_BANK .".message_id 	= '". $data['message_id'] ."'"
							.",". TABLE_VENDORS_BANK .".team			= '". $team ."'"
                            .",". TABLE_VENDORS_BANK .".username    	= '". $data['username'] ."'"
                            .",". TABLE_VENDORS_BANK .".password    	= '". encParam($data['password']) ."'"
                            .",". TABLE_VENDORS_BANK .".access_level	= '". $data['access_level'] ."'"
                            .",". TABLE_VENDORS_BANK .".created_by	= '". $my['uid'] ."'"
                            //.",". TABLE_VENDORS_BANK .".roles       	= '". $data['roles'] ."'"
                            .",". TABLE_VENDORS_BANK .".email       	= '". $data['email'] ."'"
                            .",". TABLE_VENDORS_BANK .".email_1     	= '". $data['email_1'] ."'"
                            .",". TABLE_VENDORS_BANK .".email_2     	= '". $data['email_2'] ."'"
                            .",". TABLE_VENDORS_BANK .".email_3     	= '". $data['email_3'] ."'"
                            .",". TABLE_VENDORS_BANK .".email_4     	= '". $data['email_4'] ."'"
                            .",". TABLE_VENDORS_BANK .".additional_email     = '". $data['additional_email'] ."'"
                            .",". TABLE_VENDORS_BANK .".title       	= '". $data['title'] ."'"
                            .",". TABLE_VENDORS_BANK .".f_name      	= '". $data['f_name'] ."'"
                            .",". TABLE_VENDORS_BANK .".m_name      	= '". $data['m_name'] ."'"
                            .",". TABLE_VENDORS_BANK .".l_name      	= '". $data['l_name'] ."'"
                            .",". TABLE_VENDORS_BANK .".p_name      	= '". $data['p_name'] ."'"
                            .",". TABLE_VENDORS_BANK .".bank_account_no     = '". $data['bank_account_no'] ."'"
                            .",". TABLE_VENDORS_BANK .".grade      	= '". $data['grade'] ."'"
                            .",". TABLE_VENDORS_BANK .".credit_limit = '". $data['credit_limit'] ."'"
                            .",". TABLE_VENDORS_BANK .".desig       	= '". $data['desig'] ."'"
							.",". TABLE_VENDORS_BANK .".cst_no       = '". $data['cst_no'] ."'"
                            .",". TABLE_VENDORS_BANK .".vat_no       = '". $data['vat_no'] ."'"
                            .",". TABLE_VENDORS_BANK .".service_tax_no  = '". $data['service_tax_no'] ."'"
                            .",". TABLE_VENDORS_BANK .".pan_no  		= '". $data['pan_no'] ."'"	
                            .",". TABLE_VENDORS_BANK .".org         	= '". $data['org'] ."'"
                            .",". TABLE_VENDORS_BANK .".domain      	= '". $data['domain'] ."'"
                            .",". TABLE_VENDORS_BANK .".gender      	= '". $data['gender'] ."'"
                            .",". TABLE_VENDORS_BANK .".do_birth    	= '". $data['do_birth'] ."'"
                            .",". TABLE_VENDORS_BANK .".do_aniv     	= '". $data['do_aniv'] ."'"
                            .",". TABLE_VENDORS_BANK .".remarks     	= '". $data['remarks'] ."'"
                            .",". TABLE_VENDORS_BANK .".industry     = '". $industry_idstr ."'"
                            .",". TABLE_VENDORS_BANK .".wt_you_do    = '". $data['wt_you_do'] ."'"
                            .",". TABLE_VENDORS_BANK .".mobile1      = '". $data['mobile1'] ."'"
                            .",". TABLE_VENDORS_BANK .".mobile2      = '". $data['mobile2'] ."'"
                            .",". TABLE_VENDORS_BANK .".alloted_clients   = '". $alloted_clientsstr ."'"
                            .",". TABLE_VENDORS_BANK .".billing_name   = '". $data['billing_name'] ."'"
                            .",". TABLE_VENDORS_BANK .".spouse_dob   = '". $data['spouse_dob'] ."'"
                            .",". TABLE_VENDORS_BANK .".do_add      	= '". date('Y-m-d H:i:s', time()) ."'"
                            .",". TABLE_VENDORS_BANK .".status      	= '". $data['status'] ."'" ;
                            
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $variables['hid'] = $data['user_id'];
					//Push email id into Newsletters Table bof					
					$table = TABLE_NEWSLETTERS_MEMBERS;
					include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
					if(!empty($data['email'])){
						$domain1 = strtolower(strstr($data['email'], '@'));
					    if( $domain1!= 'smeerptech.com' && 
							!NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email']) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email']."'" ;
						  $db->query($sql1);
					    }
					}
					if(!empty($data['email_1'])){
						$domain2 = strtolower(strstr($data['email_1'], '@'));
					    if ( $domain2!='smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email_1']) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email_1']."'" ;
						  $db->query($sql1);
					    }
					}
					if(!empty($data['email_2'])){
						$domain3 = strtolower(strstr($data['email_2'], '@'));
					    if ($domain3!= 'smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email_2']) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email_2']."'" ;
						  $db->query($sql1);
					    }
					}
					//Push email id into Newsletters Table eof
					
                    // Insert the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        
                        $address_arr = array('address_type' => $data['address_type'][$i],
                                            'company_name'  => '',
                                            'address'       => $data['address'][$i],
                                            'city'          => $data['city'][$i],
                                            'state'         => $data['state'][$i],
                                            'country'       => $data['country'][$i],
                                            'zipcode'       => $data['zipcode'][$i],
                                            'is_preferred'  => $data['is_preferred'][$i],
                                            'is_verified'   => $data['is_verified'][$i]
                                            );
                        
                        if ( !$region->update($variables['hid'], TABLE_VENDORS_BANK, $address_arr) ) {
                            foreach ( $region->getError() as $errors) {
                                $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                            }
                        }
                    }
                    
                    // Insert the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {
                            $phone_arr = array( 'p_type'        => $data['p_type'][$i],
                                                'cc'            => $data['cc'][$i],
                                                'ac'            => $data['ac'][$i],
                                                'p_number'      => $data['p_number'][$i],
                                                'p_is_preferred'=> $data['p_is_preferred'][$i],
                                                'p_is_verified' => $data['p_is_verified'][$i]
                                                );
                            if ( ! $phone->update($db, $variables['hid'], TABLE_VENDORS_BANK, $phone_arr) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Insert the Reminders.
                    for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['remind_date_'.$i]) ) {
                            $reminder_arr = array(  'id'            => (isset($data['remind_id_'.$i])? $data['remind_id_'.$i] : ''),
                                                    'do_reminder'   => $data['remind_date_'.$i],
                                                    'description'   => $data['remind_text_'.$i]
                                                );
                            if ( ! $reminder->update($db, $variables['hid'], TABLE_VENDORS_BANK, $reminder_arr) ) {
                                $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                foreach ( $reminder->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Send The Email to the concerned persons.
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_vbk_add_notif', 'access_level'=>$data['access_level'])
                                                             
                                                        )
                                            );
                    $manager = VendorsBank::getManager($db, '', $data['manager'], 'f_name,l_name,email');
                    $data['manager_name']   = $manager['f_name'] .' '. $manager['l_name'];
                    $data['manager_email']  = $manager['email'];
                    $data['manager_phone']  = '';
                    
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link']       = DIR_WS_NC .'/vendors-bank.php?perform=view_details&user_id='. $data['user_id'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'NEW_VENDOR_BANK_REGISTER_MANAGERS', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        
                    }
                    
                    // Send Email to the newly added Client.
                    $data['link']       = DIR_WS_NC;
                    $email = NULL;
                    /*
                    if ( getParsedEmail($db, $s, 'NEW_CLIENT_REGISTER', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['f_name'] .' '. $data['l_name'], 'email' => $data['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }*/
                    $messages->setOkMessage("The New Vendor Bank has been added.");
                    
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }else{
            $_ALL_POST['cc'][0]=91;
            $_ALL_POST['cc'][1]=91;
            $_ALL_POST['cc'][2]=91;
            $_ALL_POST['service_id'][0]=VendorsBank::BILLING;
            $_ALL_POST['service_id'][1]=VendorsBank::ST;
        }
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/vendors-bank.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/vendors-bank.php");
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        /*
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/vendors-list.php');
        }*/
        if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            header("Location:".DIR_WS_NC."/vendors-bank.php?perform=list&added=1");
            
        }
        else {

            $_ALL_POST['number'] = VendorsBank::getNewAccNumber($db);
            if ( empty($data['country']) ) {
                $data['country'] = '91';
            }
            if(empty($data['state'])){
                $data['state']='';
            }
            
            if(empty($data['city'])){
                $data['city']='';
            }
          
            $lst_country=$lst_state= $lst_city=array();
            $lst_country    = $region->getCountryList($data['country']);
            $lst_state      = $region->getStateList($data['state']);
            $lst_city       = $region->getCityList($data['city']);
             
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
           
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'reminder_count', 'value' => '6');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            // $page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
            $page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
            $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
			$page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
			$page["var"][] = array('variable' => 'message_list', 'value' => 'message_list');
            $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
            $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
            $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
            $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
            $page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-bank-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>