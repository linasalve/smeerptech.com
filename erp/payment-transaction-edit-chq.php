<?php
  if ( $perm->has('nc_p_pt_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
		include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
       
		include_once ( DIR_FS_INCLUDES .'/task-reminder.inc.php');        
		include_once ( DIR_FS_INCLUDES .'/site-settings.inc.php');        
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');    
       
         
        include_once ( DIR_FS_INCLUDES .'/user.inc.php');         
		$variables['status'] = Paymenttransaction::getStatus();
        $variables['gltype'] = Paymenttransaction::getGLType();
		$region         = new Region('91');
		$lst_city       = $region->getCityList();
		
		$currency = NULL ;
        $required_fields = '*' ;
		Currency::getList($db,$currency,$required_fields);    
        //bof services 
        $services = NULL;
		Services::getParent($db,$services);
        //eof services 
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save' ) {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages,
							'files'          => &$files
                        );
            //$account_head=null;
			$data['files'] = $files ;
           $allowed_file_types=array( "image/jpeg",												
										"image/pjpeg",
										"application/zip",
										"application/pdf",
										"application/msword"
									);	
           
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = 300; // 1024 *300 ; 300 KB
				
            if ( Paymenttransaction::validatePUpdateChq($data, $account_head, $extra) ) { 
                
				 
                $tr_db_bank_amt=$tr_cr_bank_amt=$tr_cr_sql=$tr_db_sql='';     
                // FOR Paymenttransaction::INTERNAL EOF                
                if( $messages->getErrorMessageCount() <= 0){
				
					//Code To transfer payment details in user account EOF
					/*  $data['company_name'] = '';
					 data['company_prefix'] ='';
					if(!empty($data['company_id'])){
						$company1 =null;
						$condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
						$fields_c1 = " prefix,name ";
						Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
						if(!empty($company1)){                  
						   $data['company_name'] = $company1[0]['name'];
						   $data['company_prefix'] = $company1[0]['prefix'];
						}
					} */
					
					
					if(!empty($data['period_from'])){
						$data['period_from'] = explode('/', $data['period_from']);
						$data['period_from'] = mktime(0, 0, 0, $data['period_from'][1], $data['period_from'][0], $data['period_from'][2]);
						$data['period_from'] = date('Y-m-d H:i:s', $data['period_from']);
					}
					if(!empty($data['period_to'])){
						$data['period_to'] = explode('/', $data['period_to']);
						$data['period_to'] = mktime(0, 0, 0, $data['period_to'][1], $data['period_to'][0], $data['period_to'][2]);
						$data['period_to'] = date('Y-m-d H:i:s', $data['period_to']);
					}
					$tr_st_sql='';    
                    
					$attachfilenamechq = '';
					$chq_file_sql = '';
					if(!empty($files['cheque_attachment']["name"])){	
						
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['cheque_attachment']["name"]);
                        $ext = $filedata["extension"] ;  
                        //$attachfilenamechq = $username."-".mktime().".".$ext ;                       
                        $attachfilenamechq = $data['number']."-chq".".".$ext ;
                        $data['cheque_attachment'] = $attachfilenamechq;                        
                        $attachment_path = DIR_WS_TRANSACTIONS_FILES;
                        if(file_exists(DIR_FS_TRANSACTIONS_FILES."/".$data['old_cheque_attachment'])){
							@unlink( DIR_FS_TRANSACTIONS_FILES."/".$data['old_cheque_attachment']);
						}
                        
						if(move_uploaded_file($files['cheque_attachment']['tmp_name'], 							DIR_FS_TRANSACTIONS_FILES."/".$attachfilenamechq)){
                            @chmod(DIR_FS_TRANSACTIONS_FILES."/".$attachfilenamechq, 0777);
							 $chq_file_sql = ",". TABLE_PAYMENT_TRANSACTION .".cheque_attachment = 
							'". $attachfilenamechq ."'";	
						}else{
							$chq_file_sql = ",". TABLE_PAYMENT_TRANSACTION .".cheque_attachment = ''";
						}
                    }else{
						$data['cheque_attachment']=$data['old_cheque_attachment'];
						if(isset($data['delete_cheque_attachment'])){
							if(file_exists(DIR_FS_TRANSACTIONS_FILES."/".$data['old_cheque_attachment'])){
								@unlink( DIR_FS_TRANSACTIONS_FILES."/".$data['old_cheque_attachment']);
							}
							$chq_file_sql = ",". TABLE_PAYMENT_TRANSACTION .".cheque_attachment = ''";
							$data['cheque_attachment']='';
						}
					}

					
					 
					$attachfilename='';
					$dp_file_sql='';
				
                    if(!empty($files['deposite_slip_attachment']["name"])){	
						
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['deposite_slip_attachment']["name"]);
                        $ext = $filedata["extension"] ;  
                        //$attachfilename = $username."-".mktime().".".$ext ;                       
                        $attachfilename = $data['number']."-dp".".".$ext ;
                        $data['deposite_slip_attachment'] = $attachfilename;                        
                        $attachment_path = DIR_WS_TRANSACTIONS_FILES;
                        if(file_exists(DIR_FS_TRANSACTIONS_FILES."/".$data['old_deposite_slip_attachment'])){
							@unlink( DIR_FS_TRANSACTIONS_FILES."/".$data['old_deposite_slip_attachment']);
						}
                        
						if(move_uploaded_file($files['deposite_slip_attachment']['tmp_name'], 							DIR_FS_TRANSACTIONS_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_TRANSACTIONS_FILES."/".$attachfilename, 0777);
							 $dp_file_sql = ",". TABLE_PAYMENT_TRANSACTION .".deposite_slip_attachment = 
							'". $attachfilename ."'";	
						}else{
							$dp_file_sql = ",". TABLE_PAYMENT_TRANSACTION .".deposite_slip_attachment = ''";
						}
                    }else{
						$data['deposite_slip_attachment']=$data['old_deposite_slip_attachment'];
						if(isset($data['delete_deposite_slip_attachment'])){
							if(file_exists(DIR_FS_TRANSACTIONS_FILES."/".$data['old_deposite_slip_attachment'])){
								@unlink( DIR_FS_TRANSACTIONS_FILES."/".$data['old_deposite_slip_attachment']);
							}
							$dp_file_sql = ",". TABLE_PAYMENT_TRANSACTION .".deposite_slip_attachment = ''";
							$data['deposite_slip_attachment']='';
						}
					}
					$ref_bank_id = $data['ref_bank_id'];				
					if($data['transaction_type']==Paymenttransaction::PAYMENTIN){
						$data['ref_credit_pay_bank_id'] =$ref_bank_id ;
					}elseif($data['transaction_type']==Paymenttransaction::PAYMENTOUT){
						$data['ref_debit_pay_bank_id'] =$ref_bank_id ;
					}
					
			   if(!empty($data['vendor_bank_id']) || !empty($data['client_id']) || !empty($data['executive_id'])){
						
					/* $client_vendor_sql = ",". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id 
						= '".$data['vendor_bank_id'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".client_id = '". $data['client_id'] ."'"  
						.",". TABLE_PAYMENT_TRANSACTION .".executive_id = '". $data['executive_id'] ."'"  
						.",". TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id 
						= '".$data['ref_debit_pay_bank_id']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id
						= '".$data['ref_credit_pay_bank_id']."'" ; */
				  $client_vendor_sql = ",". TABLE_PAYMENT_TRANSACTION .".client_id = '". $data['client_id'] ."'"  
				  .",".TABLE_PAYMENT_TRANSACTION .".executive_id = '". $data['executive_id'] ."'"  
				  .",".TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id = '".$data['ref_debit_pay_bank_id']."'"
				  .",".TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id= '".$data['ref_credit_pay_bank_id']."'" ;
					
				}
					
					
					/*
                    if( ( empty($data['deposite_slip_attachment']) || empty($data['old_deposite_slip_attachment'])) 	
					&& ( $data['mode_id']==1 && $data['transaction_type']==Paymenttransaction::PAYMENTIN) )
					{
						
						//If cheque and Payment IN then assign task for Deposite slip
						$condition_query = " LIMIT 0,1";
						$fields = "transaction_task_associates,transaction_task_associates_name";
						SiteSettings::getDetails($db, $_ALL_POST, $fields, $condition_query);						
						$allotted_to_str='';
						$allotted_to_name='';
						if(!empty($_ALL_POST)){
							$_ALL_POST = $_ALL_POST[0];
							$allotted_to_str = $_ALL_POST['transaction_task_associates'];
							$allotted_to_name= $_ALL_POST['transaction_task_associates_name'];
						}else{						
							$allotted_to_str = ",".$my['user_id'].",";
							$allotted_to_name= ",".$my['f_name']." ". $my['l_name'].",";
						}
						
						$task_no  =  Taskreminder::getNewNumber($db);
						$data['comment'] = "Cheque Details updated in 
						 Transaction. Please attached the deposite slip.<br/>Transaction No.-".$number;
						$query_task	= " INSERT INTO ".TABLE_TASK_REMINDER
						." SET ".TABLE_TASK_REMINDER .".task = '".$data['task'] ."'"
						.",". TABLE_TASK_REMINDER .".task_no = '".              $task_no ."'"
						.",". TABLE_TASK_REMINDER .".comment = '".              $data['comment'] ."'"
						.",". TABLE_TASK_REMINDER .".transaction_id = '".       $variables['hid'] ."'"
						.",". TABLE_TASK_REMINDER .".access_level = '".         $my['access_level'] ."'"
						.",". TABLE_TASK_REMINDER .".headers_imap = '".         $data['headers_imap']."'"
						.",". TABLE_TASK_REMINDER .".created_by = '".           $my['user_id']  ."'"
						.",". TABLE_TASK_REMINDER .".created_by_name = '".      $my['f_name']." ". $my['l_name']."'"
						.",". TABLE_TASK_REMINDER .".do_r = '".                 date('Y-m-d h:i:s') ."'"
						.",". TABLE_TASK_REMINDER .".do_deadline = '".          date('Y-m-d h:i:s') ."'"                        .",". TABLE_TASK_REMINDER .".status = '".               Taskreminder::PENDING ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to = '".          $allotted_to_str ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_name = '".     $allotted_to_name ."'"                    	  .",". TABLE_TASK_REMINDER .".do_e = '".                 date('Y-m-d') ."'";
						
						$execute = $db->query($query_task);
						//Assign a task to fill the transaction entries eof
						$data['task_no']  =   $task_no;
						$data['task']     = $data['comment'];
						$data['do_r']     =   date('Y-m-d h:i:s');
						$data['do_deadline']=   date('Y-m-d h:i:s');
						$data['af_name']    =   $my['f_name'] ;
						$data['al_name']    =   $my['l_name'] ;
						$data['status']     =  'PENDING' ;
						$data['priority']     = '' ;
						$data['link']   = DIR_WS_NC .'/task-reminder.php?perform=acomm&id='.$variables['hid'] ;
						$data['team_members'] = trim($allotted_to_name,",") ;
						// Send Email to the admin BOF  
						$data['quicklink']   = DIR_WS_SHORTCUT .'/task-reminder.php?perform=acomm&id='. 
						$variables['hid'];
						$cc_to_sender= $cc = $bcc = Null;
						if($execute){
							if ( getParsedEmail($db, $s, 'TASK_REMINDER_ADMIN', $data, $email) ) {								  
								$to = '';
								$to[]   = array('name' => $taskadmin_name , 'email' => $taskadmin_email);                           
								$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
								$cc_to_sender,$cc,$bcc,$file_name);
							}            
						}
					}*/
					$data['off_exp_issue_by_name_str']=$data['off_exp_issue_to_name_str']='';
					if(!empty($data['off_exp_issue_by'])){
						$table = TABLE_USER;
						$condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['off_exp_issue_by'] ."' " ;
						$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
						$exeCreted= getRecord($table,$fields1,$condition2);                
						$data['off_exp_issue_by_name_str'] = $exeCreted['f_name']." ".$exeCreted['l_name']." " ;
					}
					if(!empty($data['off_exp_issue_to'])){
						$table = TABLE_USER;
						$condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['off_exp_issue_to'] ."' " ;
						$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
						$exeCreted= getRecord($table,$fields1,$condition2);                
						$data['off_exp_issue_to_name_str'] = $exeCreted['f_name']." ".$exeCreted['l_name']." " ;
					}
					$data['behalf_company_name'] ='';
					if(!empty($data['behalf_company_id'])){
						$company1 =null;
						$condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['behalf_company_id'];
						$fields_c1 = "name ";
						Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
						if(!empty($company1)){                  
						   $data['behalf_company_name'] = $company1[0]['name'];
						}
					} 
					 
					$query  = " UPDATE ". TABLE_PAYMENT_TRANSACTION
					." SET ". TABLE_PAYMENT_TRANSACTION .".account_head_id = '".$data['account_head_id']."'"      
					//.",". TABLE_PAYMENT_TRANSACTION .".company_id = '".$data['company_id'] ."'"      
					//.",". TABLE_PAYMENT_TRANSACTION .".company_name = '".$data['company_name'] ."'"      
					//.",". TABLE_PAYMENT_TRANSACTION .".company_prefix = '".$data['company_prefix'] ."'"  
					.$chq_file_sql
					.$dp_file_sql					
					.$client_vendor_sql					
					.",". TABLE_PAYMENT_TRANSACTION .".iaccount_head_id = '". $data['iaccount_head_id'] ."'"
					//.",". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id = '".  $data['vendor_bank_id'] ."'"
					//.",". TABLE_PAYMENT_TRANSACTION .".party_id = '". $data['party_id'] ."'"
					//.",". TABLE_PAYMENT_TRANSACTION .".client_id = '". $data['client_id'] ."'"  
					//.",". TABLE_PAYMENT_TRANSACTION .".executive_id = '". $data['executive_id'] ."'"  
				  //.",". TABLE_PAYMENT_TRANSACTION .".off_exp_issue_by = '".$data['off_exp_issue_by']."'"
				  //.",". TABLE_PAYMENT_TRANSACTION .".off_exp_issue_by_name='".$data['off_exp_issue_by_name_str']."'"
				  //.",". TABLE_PAYMENT_TRANSACTION .".off_exp_issue_to = '".$data['off_exp_issue_to'] ."'"
				  //.",". TABLE_PAYMENT_TRANSACTION.".off_exp_issue_to_name='".$data['off_exp_issue_to_name_str'] ."'"
				    .",". TABLE_PAYMENT_TRANSACTION .".behalf_vendor_bank  = '".$data['behalf_vendor_bank'] ."'"
				    .",". TABLE_PAYMENT_TRANSACTION .".behalf_vendor_bank_name='".$data['behalf_vendor_bank_name']."'"
				    .",". TABLE_PAYMENT_TRANSACTION .".behalf_client = '".$data['behalf_client'] ."'"
				    .",". TABLE_PAYMENT_TRANSACTION .".behalf_client_name = '".$data['behalf_client_name'] ."'"
				    .",". TABLE_PAYMENT_TRANSACTION .".behalf_executive = '".$data['behalf_executive'] ."'"
				    .",". TABLE_PAYMENT_TRANSACTION .".behalf_executive_name = '".$data['behalf_executive_name'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".behalf_company_id = '".$data['behalf_company_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".behalf_company_name='".$data['behalf_company_name']."'"					  .",". TABLE_PAYMENT_TRANSACTION .".period_from = '". $data['period_from'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".period_to = '". $data['period_to'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".particulars = '".$data['particulars']."'"
					.",". TABLE_PAYMENT_TRANSACTION .".remarks = '".$data['remarks']."'"
					." WHERE id = '". $id ."'";
                    $db->query($query);
						
                      $messages->setOkMessage("Payment transaction updated successfully.");    
                }
                    
                    
                    $fields_r = TABLE_PAYMENT_TRANSACTION .'.*'  ;            
                    $condition_query_r = " WHERE (". TABLE_PAYMENT_TRANSACTION .".id = '". $id ."' )";
                    Paymenttransaction::getDetails($db, $data, $fields_r, $condition_query_r);
                    if(!empty($data)){
                        $_ALL_POST =$data['0'];    
                        
						if(isset($_ALL_POST['is_pdc']) && $_ALL_POST['is_pdc']=='1'){
							$temp  =null;
							
						  if(isset($_ALL_POST['do_pdc_chq']) && $_ALL_POST['do_pdc_chq'] !='0000-00-00 00:00:00'){
							
								$_ALL_POST['do_pdc_chq']  = explode(' ', $_ALL_POST['do_pdc_chq']);
									$temp               = explode('-', $_ALL_POST['do_pdc_chq'][0]);
								 $_ALL_POST['do_pdc_chq']  = NULL;
								$_ALL_POST['do_pdc_chq']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
							}else{
								$_ALL_POST['do_pdc_chq']  ='';  
							}                                
						}else{
							 $_ALL_POST['do_pdc_chq']  ='';  
						}
						$exeCreted=$exeCreatedname='';
		
						$table = TABLE_USER;
						$condition2 = " WHERE ".TABLE_USER .".user_id= '".$_ALL_POST['user_id'] ."' " ;
						$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
						$exeCreted= getRecord($table,$fields1,$condition2);                
						$_ALL_POST['creator'] = $exeCreted['f_name']." ".$exeCreted['l_name']."<br/>" ;
						//Vendor Bank details bof
						$_ALL_POST['vendor_bank_details'] ='';
						if(!empty($_ALL_POST['vendor_bank_id'])){
							$table = TABLE_VENDORS_BANK;
							$condition2 = " WHERE ".TABLE_VENDORS_BANK .".user_id= '".$_ALL_POST['vendor_bank_id'] ."' " ;
							$fields1 =  TABLE_VENDORS_BANK .'.f_name,'.TABLE_VENDORS_BANK.".l_name,
							".TABLE_VENDORS_BANK.".billing_name" ;
							$detailsArr = getRecord($table,$fields1,$condition2);
							if(!empty($detailsArr)){
							$_ALL_POST['vendor_bank_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." 
							(".$detailsArr['billing_name'].")" ;   
							}
						 
						}
							
						 
						$_ALL_POST['client_details'] ='';
						//clients details bof
						if(!empty($_ALL_POST['client_id'])){
							$table = TABLE_CLIENTS;
							$condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$_ALL_POST['client_id'] ."' " ;
							$fields1 =  TABLE_CLIENTS .'.f_name,'.TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name" ;
							$detailsArr = getRecord($table,$fields1,$condition2);
							if(!empty($detailsArr)){
								$_ALL_POST['client_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." (".$detailsArr['billing_name'].")" ;   
							}
						}
						//assosiate details bof
						$_ALL_POST['executive_details'] ='';
						if(!empty($_ALL_POST['executive_id'])){
							$table = TABLE_USER;
							$condition2 = " WHERE ".TABLE_USER .".user_id= '".$_ALL_POST['executive_id'] ."' " ;
							$fields1 =  TABLE_USER .'.f_name,'.TABLE_USER.".l_name" ;
							$detailsArr = getRecord($table,$fields1,$condition2);
							if(!empty($detailsArr)){
								$_ALL_POST['executive_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']."  " ;   
							}
						 
						}
						$_ALL_POST['attachment_imap_arr'] = array();
						if(!empty($_ALL_POST['attachment_imap'])){
							$_ALL_POST['attachment_imap_arr']=explode(",",$_ALL_POST['attachment_imap']);
						}
						if(isset($_ALL_POST['period_from']) && $_ALL_POST['period_from']!='0000-00-00 00:00:00'){
							$temp  =null;
							$_ALL_POST['period_from']  = explode(' ', $_ALL_POST['period_from']);
							$temp               = explode('-', $_ALL_POST['period_from'][0]);
							$_ALL_POST['period_from']  = NULL;
							$_ALL_POST['period_from']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
							$_ALL_POST['period_from']  = '';                            
						}
						if(isset($_ALL_POST['period_to']) && $_ALL_POST['period_to']!='0000-00-00 00:00:00'){
							$temp  =null;
							$_ALL_POST['period_to']  = explode(' ', $_ALL_POST['period_to']);
							$temp               = explode('-', $_ALL_POST['period_to'][0]);
							$_ALL_POST['period_to']  = NULL;
							$_ALL_POST['period_to']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
							$_ALL_POST['period_to']  = '';                            
						}
						if(isset($_ALL_POST['do_transaction']) && $_ALL_POST['do_transaction']!='0000-00-00 00:00:00'){
							$temp  =null;
							$_ALL_POST['do_transaction']  = explode(' ', $_ALL_POST['do_transaction']);
							$temp               = explode('-', $_ALL_POST['do_transaction'][0]);
							$_ALL_POST['do_transaction']  = NULL;
							$_ALL_POST['do_transaction']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
							$_ALL_POST['do_transaction']  = '';                            
						}
							
						if(isset($_ALL_POST['do_voucher']) && $_ALL_POST['do_voucher']!='0000-00-00 00:00:00'){
							$temp  =null;
							$_ALL_POST['do_voucher']  = explode(' ', $_ALL_POST['do_voucher']);
							$temp               = explode('-', $_ALL_POST['do_voucher'][0]);
							$_ALL_POST['do_voucher']  = NULL;
							$_ALL_POST['do_voucher']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
							$_ALL_POST['do_voucher']  = '';                            
						}
                            
						if(isset($_ALL_POST['do_pay_received']) && $_ALL_POST['do_pay_received']!='0000-00-00'){
							$temp  =null;
							$_ALL_POST['do_pay_received']  = explode(' ', $_ALL_POST['do_pay_received']);
							$temp               = explode('-', $_ALL_POST['do_pay_received'][0]);
							$_ALL_POST['do_pay_received']  = NULL;
							$_ALL_POST['do_pay_received']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
						
							 $_ALL_POST['do_pay_received']  = '';
						}
						if(isset($_ALL_POST['do_credit_trans']) && $_ALL_POST['do_credit_trans']!='0000-00-00'){
							$temp  =null;
							$_ALL_POST['do_credit_trans']  = explode(' ', $_ALL_POST['do_credit_trans']);
							$temp               = explode('-', $_ALL_POST['do_credit_trans'][0]);
							$_ALL_POST['do_credit_trans']  = NULL;
							$_ALL_POST['do_credit_trans']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
							$_ALL_POST['do_credit_trans']  = '';
							
						}
                            
						if(isset($_ALL_POST['do_debit_trans']) && $_ALL_POST['do_debit_trans']!='0000-00-00'){
							$temp  =null;
							$_ALL_POST['do_debit_trans']  = explode(' ', $_ALL_POST['do_debit_trans']);
							$temp               = explode('-', $_ALL_POST['do_debit_trans'][0]);
							$_ALL_POST['do_debit_trans']  = NULL;
							$_ALL_POST['do_debit_trans']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
							 $_ALL_POST['do_debit_trans']  = '';
						}
						if(!empty($_ALL_POST['ref_credit_pay_bank_id'])){
							$_ALL_POST['ref_bank_id'] = $_ALL_POST['ref_credit_pay_bank_id'] ;
						}
						if(!empty($_ALL_POST['ref_debit_pay_bank_id'])){
							$_ALL_POST['ref_bank_id'] = $_ALL_POST['ref_debit_pay_bank_id'] ;
						}
                    }
            }
        }
        else {
     
            // Read the record which is to be Updated.
            $fields = TABLE_PAYMENT_TRANSACTION .'.*'  ;            
            $condition_query = " WHERE (". TABLE_PAYMENT_TRANSACTION .".id = '". $id ."' )";
            $where_added =1;
           
            if ( Paymenttransaction::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
			    $_ALL_POST = $_ALL_POST['0'];                
                $id         = $_ALL_POST['id'];
                $number         = $_ALL_POST['number'];
				
				if(isset($_ALL_POST['is_pdc']) && $_ALL_POST['is_pdc']==1){
					$temp  =null;
					if(isset($_ALL_POST['do_pdc_chq']) && $_ALL_POST['do_pdc_chq']!='0000-00-00 00:00:00'){
						$_ALL_POST['do_pdc_chq']  = explode(' ', $_ALL_POST['do_pdc_chq']);
							$temp               = explode('-', $_ALL_POST['do_pdc_chq'][0]);
						 $_ALL_POST['do_pdc_chq']  = NULL;
						 $_ALL_POST['do_pdc_chq']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
					}else{
						$_ALL_POST['do_pdc_chq']  ='';  
					}					
				}else{
					 $_ALL_POST['do_pdc_chq']  ='';  
				}
				$_ALL_POST['attachment_imap_arr'] = array();
				if(!empty($_ALL_POST['attachment_imap'])){
					$_ALL_POST['attachment_imap_arr']=explode(",",$_ALL_POST['attachment_imap']);
				}
				//Vendor Bank details bof
				$_ALL_POST['vendor_bank_details'] ='';
				if(!empty($_ALL_POST['vendor_bank_id'])){
					$table = TABLE_VENDORS_BANK;
					$condition2 = " WHERE ".TABLE_VENDORS_BANK .".user_id= '".$_ALL_POST['vendor_bank_id'] ."' " ;
					$fields1 =  TABLE_VENDORS_BANK .'.f_name,'.TABLE_VENDORS_BANK.".l_name,
					".TABLE_VENDORS_BANK.".billing_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['vendor_bank_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." 
						(".$detailsArr['billing_name'].")" ;   
					}
				 
				}
				if(!empty($_ALL_POST['ref_credit_pay_bank_id'])){
					$_ALL_POST['ref_bank_id'] = $_ALL_POST['ref_credit_pay_bank_id'] ;
				}
				if(!empty($_ALL_POST['ref_debit_pay_bank_id'])){
					$_ALL_POST['ref_bank_id'] = $_ALL_POST['ref_debit_pay_bank_id'] ;
				}
				//vendor details bof
				/* 
				$_ALL_POST['vendor_details'] ='';
				if(!empty($_ALL_POST['party_id'])){
					$table = TABLE_VENDORS;
					$condition2 = " WHERE ".TABLE_VENDORS .".user_id= '".$_ALL_POST['party_id'] ."' " ;
					$fields1 =  TABLE_VENDORS .'.f_name,'.TABLE_VENDORS.".l_name,".TABLE_VENDORS.".billing_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['vendor_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." (".$detailsArr['billing_name'].")" ;   
					}
				 
				} 
				*/
				$_ALL_POST['client_details'] ='';
				//clients details bof
				if(!empty($_ALL_POST['client_id'])){
					$table = TABLE_CLIENTS;
					$condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$_ALL_POST['client_id'] ."' " ;
					$fields1 =  TABLE_CLIENTS .'.f_name,'.TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['client_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." (".$detailsArr['billing_name'].")" ;   
					}				 
				}
				//assosiate details bof
				$_ALL_POST['executive_details'] ='';
				if(!empty($_ALL_POST['executive_id'])){
					$table = TABLE_USER;
					$condition2 = " WHERE ".TABLE_USER .".user_id= '".$_ALL_POST['executive_id'] ."' " ;
					$fields1 =  TABLE_USER .'.f_name,'.TABLE_USER.".l_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['executive_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." " ;   
					}				 
				}

				if(isset($_ALL_POST['do_transaction']) && $_ALL_POST['do_transaction']!='0000-00-00 00:00:00'){
					$temp  =null;
					$_ALL_POST['do_transaction']  = explode(' ', $_ALL_POST['do_transaction']);
					$temp               = explode('-', $_ALL_POST['do_transaction'][0]);
					$_ALL_POST['do_transaction']  = NULL;
					$_ALL_POST['do_transaction']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['do_transaction']  = '';                            
				}
				if(isset($_ALL_POST['do_voucher']) && $_ALL_POST['do_voucher']!='0000-00-00 00:00:00'){
                    $temp  =null;
                    $_ALL_POST['do_voucher']  = explode(' ', $_ALL_POST['do_voucher']);
                    $temp               = explode('-', $_ALL_POST['do_voucher'][0]);
                    $_ALL_POST['do_voucher']  = NULL;
                    $_ALL_POST['do_voucher']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                }else{
                    $_ALL_POST['do_voucher']  = '';                            
                }
				if(isset($_ALL_POST['period_from']) && $_ALL_POST['period_from']!='0000-00-00 00:00:00'){
					$temp  =null;
					$_ALL_POST['period_from']  = explode(' ', $_ALL_POST['period_from']);
					$temp               = explode('-', $_ALL_POST['period_from'][0]);
					$_ALL_POST['period_from']  = NULL;
					$_ALL_POST['period_from']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['period_from']  = '';                            
				}
				if(isset($_ALL_POST['period_to']) && $_ALL_POST['period_to']!='0000-00-00 00:00:00'){
					$temp  =null;
					$_ALL_POST['period_to']  = explode(' ', $_ALL_POST['period_to']);
					$temp               = explode('-', $_ALL_POST['period_to'][0]);
					$_ALL_POST['period_to']  = NULL;
					$_ALL_POST['period_to']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['period_to']  = '';                            
				}
				if(isset($_ALL_POST['do_pay_received']) && $_ALL_POST['do_pay_received']!='0000-00-00'){
					$temp  =null;
					$_ALL_POST['do_pay_received']  = explode(' ', $_ALL_POST['do_pay_received']);
					$temp               = explode('-', $_ALL_POST['do_pay_received'][0]);
					$_ALL_POST['do_pay_received']  = NULL;
					$_ALL_POST['do_pay_received']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
				
					 $_ALL_POST['do_pay_received']  = '';
				}
				if(isset($_ALL_POST['do_credit_trans']) && $_ALL_POST['do_credit_trans']!='0000-00-00'){
					$temp  =null;
					$_ALL_POST['do_credit_trans']  = explode(' ', $_ALL_POST['do_credit_trans']);
					$temp               = explode('-', $_ALL_POST['do_credit_trans'][0]);
					$_ALL_POST['do_credit_trans']  = NULL;
					$_ALL_POST['do_credit_trans']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['do_credit_trans']  = '';
					
				}
				if(isset($_ALL_POST['do_debit_trans']) && $_ALL_POST['do_debit_trans']!='0000-00-00'){
					$temp  =null;
					$_ALL_POST['do_debit_trans']  = explode(' ', $_ALL_POST['do_debit_trans']);
					$temp               = explode('-', $_ALL_POST['do_debit_trans'][0]);
					$_ALL_POST['do_debit_trans']  = NULL;
					$_ALL_POST['do_debit_trans']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					 $_ALL_POST['do_debit_trans']  = '';
				}
				
				$exeCreted=$exeCreatedname='';
				
				$table = TABLE_AUTH_USER;
				$condition2 = " WHERE ".TABLE_USER .".user_id= '".$_ALL_POST['user_id'] ."' " ;
				$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
				$exeCreted= getRecord($table,$fields1,$condition2);                
				$_ALL_POST['creator'] = $exeCreted['f_name']." ".$exeCreted['l_name']."<br/>" ;
				//Get current account head EOF
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }
        // get list of all comments of change log BOF
        $condition_query1 = $condition_url1 = '';
        // get list of all comments of change log EOF        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/payment-transaction-list.php');
        }
        else{
        
          	//check if Linking done then transaction can not be edited
			$variables['can_edit_completed'] = false;
			if ( $perm->has('nc_p_pt_edit_completed') ) {
				$variables['can_edit_completed'] = true;
			}

				// These parameters will be used when returning the control back to the List page.
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}            
				$hidden[] = array('name'=> 'id', 'value' => $id);
				$hidden[] = array('name'=> 'ajx', 'value' => $ajx);
				$hidden[] = array('name'=> 'number', 'value' => $number);
				$hidden[] = array('name'=> 'perform', 'value' => 'edit_chq');
				$hidden[] = array('name'=> 'act', 'value' => 'save');
				$page["var"][] = array('variable' => 'id', 'value' => 'id');    
				$page["var"][] = array('variable' => 'services', 'value' => 'services');   
				$page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');      
				$page["var"][] = array('variable' => 'currency', 'value' => 'currency');						
				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' =>'payment-transaction-edit-chq.html');
			 
        }
   }else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
   }
?>
