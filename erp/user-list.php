<?php

    if ( $perm->has('nc_ue_list') || $perm->has('nc_ue_select_list') ) {
    
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/Region.class.php');
       
        
		
		$groups_list  = User::getGroups();
        /*
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
        }
        else {
            $condition_query .= ' AND (';
        }*/
        
        if ( empty($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE ';
            $chk_status = 'AND';
            $sStatus=User::ACTIVE;
            $_SEARCH["chk_status"]  = $chk_status;
            $_SEARCH["sStatus"]     = $sStatus;
            
            $condition_query .= TABLE_USER.".status IN('".$sStatus."') AND (" ;
        }else {
            $condition_query .= ' AND (';
        }
        
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_ue_list_al') ) {
            $access_level += 1;
        }
        /*
        // If the User has the Right to View Clients of the same Access Level.
        if ( $perm->has('nc_ue_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " (". TABLE_USER .".access_level < $access_level ) ";
        $condition_query .= ')';        
        */
        
        if ( $perm->has('nc_ue_list_all') ) { 
            $condition_query .= TABLE_USER.".user_id != '' )"; 
        }else{      
            $condition_query .= "".TABLE_USER .".access_level <= $access_level ";    
            $condition_query .= ')';
           
        }
        
		if(!empty($order_by_table))
        	$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched'] =1;
        // To count total records.
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
      
        if($searchStr==1){
			 
            $list	= 	NULL;
            $total	=	User::getList( $db, $list, '', $condition_query);
        
            $extra_url  = '';
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            
            // $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
            $list	= NULL;
            User::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
			 
        }else{
            $_SEARCH["chk_status"]  ='';
            $_SEARCH["sStatus"]     ='';
        }
        $fList=array();
		
        if(!empty($list)){
			 
			$region         = new Region();
			$phone          = new Phone(TABLE_USER);
            foreach( $list as $key=>$val){      
               
                $val['roles_title'] = User::getRoles($db, $access_level, $val['roles']);
                
                $table = TABLE_SETTINGS_DEPARTMENT;
                $condition1 = " WHERE ".TABLE_SETTINGS_DEPARTMENT .".id= '".$val['department'] ."' " ;
                $fields1 =  TABLE_SETTINGS_DEPARTMENT .'.department_name';
                $department = getRecord($table,$fields1,$condition1);
                $val['department'] ='';
                if(!empty($department)){
                    $val['department'] = $department['department_name'];
                }
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_USER, $val['user_id']);
                $val['phone'] = $phone->get($db);
                
                // Read the Addresses.
                $region->setAddressOf(TABLE_USER, $val['user_id']);
                $val['address_list'] = $region->get();
				
				$groupsName ='';								
				if(!empty($val['groups'])){				
					$groups1 = explode(",",$val['groups']);
					
					if(!empty($groups1)){
						foreach($groups1 as $key1=>$val1){
							if(!empty($val1)){
								$groupsName .=$groups_list[$val1].",";						 	
							}

						}
					}
					$groupsName = trim($groupsName,',');					
				}
				$val['groupsName'] = $groupsName ;
				$val['jobp_details1']=array();
				if(!empty($val['jobp'])){
					$val['jobp']          = explode(',', $val['jobp']);
					$temp                       = "'". implode("','", $val['jobp']) ."'";
					$condition_query2 = " LEFT JOIN ".TABLE_TJ_FUNCTIONS." ON 
					".TABLE_TJ_FUNCTIONS_P.".tjf_id = ".TABLE_TJ_FUNCTIONS.".id 
					WHERE ".TABLE_TJ_FUNCTIONS_P.".id IN(".$temp.")";
					$jfields = TABLE_TJ_FUNCTIONS.".title,".TABLE_TJ_FUNCTIONS_P.".id," 
					.TABLE_TJ_FUNCTIONS_P.".jtitle as ptitle ";
					 
					TeamJobFunctions::getParticulars($db, $val['jobp_details1'], 
					$jfields, $condition_query2 );
				}
				
                $fList[$key]=$val;
            }
			 
			$region =$phone= $list=null;
        }
		
        //print_r($fList);
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
		$variables['cstaff'] = array(
									User::CSTAFF_YES=>'YES',
									User::CSTAFF_NO=>'NO'
								);
		$variables['rselect'] = array(
									User::RSELECT_YES=>'YES',
									User::RSELECT_NO=>'NO'
								);
		$variables['smail'] = array(
									User::SENDM_YES=>'YES',
									User::SENDM_NO=>'NO'
								);
        if ( $perm->has('nc_ue_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_ue_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_ue_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_ue_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_ue_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_ue_status') ) {
            $variables['can_change_status']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => 'groups_list', 'value' => 'groups_list');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>