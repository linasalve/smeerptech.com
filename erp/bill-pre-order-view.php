<?php
    if ( $perm->has('nc_bl_po_details') ) {
        $po_id = isset ($_GET['po_id']) ? $_GET['po_id'] : ( isset($_POST['po_id'] ) ? $_POST['po_id'] :'');

        $_ALL_POST	=	NULL;
        $condition_query	=	NULL;
        $access_level   = $my['access_level'];

        if ( $perm->has('nc_bl_po_details_al') ) {
            $access_level += 1;
        }
        
        if (!isset($condition_query) || $condition_query=='') {
            $condition_query = " WHERE id = '". $po_id ."' ";
        }
        // Check if the User has the Right to view Pre Orders created by others.
        if ( !$perm->has('nc_bl_po_list_ot') ) {
            $condition_query .= " AND created_by = '". $my['user_id'] ."'";
        }
        
        if (PreOrder::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];

            if ( $_ALL_POST['access_level'] < $access_level ) {
                include ( DIR_FS_INCLUDES .'/user.inc.php');
                $_ALL_POST['creator']= '';
                User::getList($db, $_ALL_POST['creator'], 'username,number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                $_ALL_POST['creator'] = $_ALL_POST['creator'][0];
                $_ALL_POST['access_level'] = getAccessLevel($db, $_ALL_POST['access_level'], true);
                $_ALL_POST['access_level'] = $_ALL_POST['access_level'][0];
                $attach_files              = $_ALL_POST['attach_files'];
                $files=array();
                $filenames='';
                if(!empty($attach_files)){
                    $files = explode(",",$attach_files);
                    $i=1;                           
                    foreach($files as $key1=>$val1){
                        if(!empty($val1)){
                                                                   
                            $filenames .= "<a href=javascript:void(0); onclick=javascript:showFileAttachment('$val1') title='View file' class='file'>file ".$i."</a><br/>" ;
                            $i++;
                        }
                    } 
                }
                $_ALL_POST['files'] = $filenames ;
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-pre-order-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Pre Order with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("Records not found to view. ");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>