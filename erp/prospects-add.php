<?php

    if ( $perm->has('nc_pr_add') ) {    
        include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
        include_once ( DIR_FS_CLASS .'/ReminderProspects.class.php');
    	include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
        include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
		include_once ( DIR_FS_INCLUDES .'/prospects.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
		include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
		
        
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        $region     = new RegionProspects('91','21','231');
        $phone      = new PhoneProspects(TABLE_PROSPECTS);
        $reminder   = new ReminderProspects(TABLE_PROSPECTS);
        
        $al_list    = getAccessLevel($db, $my['access_level']);
        $role_list  = Prospects::getRoles($db, $my['access_level']);
        $grade_list  = Prospects::getGrades();
        
         //For Clients list allotment
        if ( $perm->has('nc_pr') && $perm->has('nc_pr_list')){
            $variables['can_view_clist']     = true;
        }
       
        
        //BOF read the available services & authorization
        $authorization_list  = Prospects::getAuthorization();
        $services_list  = Prospects::getServices();
        //EOF read the available services
        
		// Read the available source
       	Leads::getSource($db,$source);
		
        //BOF read the available industries
        $industry_list = NULL;
        $required_fields = '*' ;
        $condition_query = " WHERE status='".Industry::ACTIVE."'" ; 
		Industry::getList($db,$industry_list,$required_fields,$condition_query);
        //EOF read the available industries
        
        //BOF read the available industries
		Prospects::getCountryCode($db,$country_code);
         //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
			// BO: For generating default password.
			$data['password']	= getRandom(6, '', array_merge($letters_b, $letters_s, $digits));
			$data['re_password']= $data['password'];
			// EO: For generating default password.
 
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder
                        );
          
            if ( Prospects::validateAdd($data, $extra) ) {
                
                $service_ids=$_POST['service_id'];
                $service_idstr=implode(",",$service_ids);
                
                if(!empty($service_idstr)){
                    $service_idstr=",".$service_idstr.",";	
                }
		
				$authorization_ids=$_POST['authorization_id'];
                $authorization_idstr=implode(",",$authorization_ids);
                
                if(!empty($authorization_idstr)){
                    $authorization_idstr=",".$authorization_idstr.",";	
                }
                
                $industry_ids=$_POST['industry'];
                $industry_idstr=implode(",",$industry_ids);
                
                if(!empty($industry_idstr)){
                    $industry_idstr=",".$industry_idstr.",";
                }
                $alloted_clientsstr='';
                $alloted_clients=$_POST['alloted_clients'];
                $alloted_clientsstr=implode(",",$alloted_clients);
               
                if(!empty($alloted_clientsstr)){
                    $alloted_clientsstr=",".$alloted_clientsstr.",";
                }
				if( isset($data['check_email'])){
					$data['check_email']=1;
				}else{
					$data['check_email']=0;
				}				 
                //Default account manager aryan sir
                //$data['team']='e68adc58f2062f58802e4cdcfec0af2d';               
                
				$query	= " INSERT INTO ". TABLE_PROSPECTS
                        ." SET ". TABLE_PROSPECTS .".user_id     	= '". $data['user_id'] ."'"
                            .",". TABLE_PROSPECTS .".service_id   = '". $service_idstr ."'"
                            .",". TABLE_PROSPECTS .".authorization_id   = '". $authorization_idstr ."'"
                            .",". TABLE_PROSPECTS .".number      	= '". $data['number'] ."'"
							.",". TABLE_PROSPECTS .".manager 	   	= '". $data['manager'] ."'"
							.",". TABLE_PROSPECTS .".team			= '". implode(',', $data['team']) ."'"
                            .",". TABLE_PROSPECTS .".username    	= '". $data['username'] ."'"
                            .",". TABLE_PROSPECTS .".password    	= '". encParam($data['password']) ."'"
							.",". TABLE_PROSPECTS .".lead_source_to    = '". $data['lead_source_to'] ."'"
                            .",". TABLE_PROSPECTS .".lead_source_from  = '". $data['lead_source_from'] ."'"
                            .",". TABLE_PROSPECTS .".access_level	= '". $my['access_level'] ."'"
                            .",". TABLE_PROSPECTS .".created_by	= '". $my['uid'] ."'"
                          //.",". TABLE_PROSPECTS .".roles       	= '". $data['roles'] ."'"
                            .",". TABLE_PROSPECTS .".email       = '". $data['email'] ."'"
                            .",". TABLE_PROSPECTS .".check_email = '". $data['check_email'] ."'"
                            .",". TABLE_PROSPECTS .".email_1     = '". $data['email_1'] ."'"						    
							.",". TABLE_PROSPECTS .".email_2     = '". $data['email_2'] ."'"						
                            .",". TABLE_PROSPECTS .".email_3     	= '". $data['email_3'] ."'"							   
							.",". TABLE_PROSPECTS .".email_4     	= '". $data['email_4'] ."'"						
                            .",". TABLE_PROSPECTS .".additional_email = '". $data['additional_email'] ."'"
                            .",". TABLE_PROSPECTS .".title       	= '". $data['title'] ."'"
                            .",". TABLE_PROSPECTS .".f_name      	= '". $data['f_name'] ."'"
                            .",". TABLE_PROSPECTS .".m_name      	= '". $data['m_name'] ."'"
                            .",". TABLE_PROSPECTS .".l_name      	= '". $data['l_name'] ."'"
                            .",". TABLE_PROSPECTS .".p_name      	= '". $data['p_name'] ."'"
                            .",". TABLE_PROSPECTS .".grade      	= '". $data['grade'] ."'"
                            .",". TABLE_PROSPECTS .".credit_limit = '". $data['credit_limit'] ."'"
                            .",". TABLE_PROSPECTS .".desig       	= '". $data['desig'] ."'"
                            .",". TABLE_PROSPECTS .".org         	= '". $data['org'] ."'"
                            .",". TABLE_PROSPECTS .".domain      	= '". $data['domain'] ."'"
                            .",". TABLE_PROSPECTS .".gender      	= '". $data['gender'] ."'"
                            .",". TABLE_PROSPECTS .".do_birth    	= '". $data['do_birth'] ."'"
                            .",". TABLE_PROSPECTS .".do_aniv     	= '". $data['do_aniv'] ."'"
                            .",". TABLE_PROSPECTS .".remarks     	= '". $data['remarks'] ."'"
                            .",". TABLE_PROSPECTS .".industry     = '". $industry_idstr ."'"
                            .",". TABLE_PROSPECTS .".wt_you_do    = '". $data['wt_you_do'] ."'"
                            .",". TABLE_PROSPECTS .".mobile1      = '". $data['mobile1'] ."'"
                            .",". TABLE_PROSPECTS .".mobile2      = '". $data['mobile2'] ."'"
                            .",". TABLE_PROSPECTS .".alloted_clients   = '". $alloted_clientsstr ."'"
                            .",". TABLE_PROSPECTS .".billing_name   = '". $data['billing_name'] ."'"
                            .",". TABLE_PROSPECTS .".spouse_dob   = '". $data['spouse_dob'] ."'"
                            .",". TABLE_PROSPECTS .".do_add      	= '". date('Y-m-d H:i:s', time()) ."'"
                            .",". TABLE_PROSPECTS .".status      	= '". $data['status'] ."'" ;
                            
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $variables['hid'] = $data['user_id'];
					
					//Push email id into Newsletters Table BOF
					$table = TABLE_NEWSLETTERS_MEMBERS;
					include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
					if(!empty($data['email'])){
						$domain1 = strtolower(strstr($data['email'], '@'));
						$condi1= " WHERE email='".strtolower(trim($data['email']))."'";
					    if( $domain1!= 'smeerptech.com' && 
							!NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi1) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email']))."'" ;
						  $db->query($sql1);
					    }
					}
					if(!empty($data['email_1'])){
						$domain2 = strtolower(strstr($data['email_1'], '@'));
						$condi2= " WHERE email='".strtolower(trim($data['email_1']))."'";
					    if ( $domain2!='smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi2) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_1']))."'" ;
						  $db->query($sql1);
					    }
					}
					if(!empty($data['email_2'])){
						$domain3 = strtolower(strstr($data['email_2'], '@'));
						$condi3= " WHERE email='".strtolower(trim($data['email_2']))."'";
					    if ($domain3!= 'smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi3) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_2']))."'" ;
						  $db->query($sql1);
					    }
					}
					//Push email id into Newsletters Table EOF
					
                    // Insert the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        
                        $address_arr = array('address_type' => $data['address_type'][$i],
                                            'company_name'  => '',
                                            'address'       => $data['address'][$i],
                                            'city'          => $data['city'][$i],
                                            'state'         => $data['state'][$i],
                                            'country'       => $data['country'][$i],
                                            'zipcode'       => $data['zipcode'][$i],
                                            'is_preferred'  => $data['is_preferred'][$i],
                                            'is_verified'   => $data['is_verified'][$i]
                                            );
                        
                        if ( !$region->update($variables['hid'], TABLE_PROSPECTS, $address_arr) ) {
                            foreach ( $region->getError() as $errors) {
                                $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                            }
                        }
                    }
                    
                    // Insert the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {
                            $phone_arr = array( 'p_type'        => $data['p_type'][$i],
                                                'cc'            => $data['cc'][$i],
                                                'ac'            => $data['ac'][$i],
                                                'p_number'      => $data['p_number'][$i],
                                                'p_is_preferred'=> $data['p_is_preferred'][$i],
                                                'p_is_verified' => $data['p_is_verified'][$i]
                                                );
                            if ( ! $phone->update($db, $variables['hid'], TABLE_PROSPECTS, $phone_arr) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Insert the Reminders.
                    for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['remind_date_'.$i]) ) {
                            $reminder_arr = array(  'id'            => (isset($data['remind_id_'.$i])? $data['remind_id_'.$i] : ''),
                                                    'do_reminder'   => $data['remind_date_'.$i],
                                                    'description'   => $data['remind_text_'.$i]
                                                );
                            if ( ! $reminder->update($db, $variables['hid'], TABLE_PROSPECTS, $reminder_arr) ) {
                                $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                foreach ( $reminder->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Send The Email to the concerned persons.
                    $users = getExecutivesWith($db, array(  
						array('right'=>'nc_pr_add_notif', 'access_level'=>$data['access_level']) 
                                                        )
                                            );
                    $manager = Prospects::getManager($db, '', $data['manager'], 'f_name,l_name,email');
                    $data['manager_name']   = $manager['f_name'] .' '. $manager['l_name'];
                    $data['manager_email']  = $manager['email'];
                    $data['manager_phone']  = '';
                    
                    foreach ( $users as $user ){
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link'] = DIR_WS_NC .'/prospects.php?perform=view_details&user_id='. $data['user_id'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'NEW_PROSPECT_REGISTER_MANAGERS', $data, $email) ) {
                            $to     = '';
                            $to[] = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                    $data['link']       = DIR_WS_NC;
                    $email = NULL;
					//Add Prospect Lead BOF
					
					$data['currency_id']=1;
					$currency1 =null;
					$condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
					$fields1 = "*";
					Currency::getList( $db, $currency1, $fields1, $condition_query1);              
					if(!empty($currency1)){
					   $data['currency_abbr'] = $currency1[0]['abbr'];
					   $data['currency_name'] = $currency1[0]['currency_name'];
					   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
					   $data['currency_country'] = $currency1[0]['country_name'];
					}
					
					$company1 =null;
					$data['company_id']=1;
					$condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
					$fields_c1 = " prefix,name ";
					Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
					if(!empty($company1)){                  
					   $data['company_name'] = $company1[0]['name'];
					   $data['company_prefix'] = $company1[0]['prefix'];
					}
					/* $data['order_closed_by']=$data['project_manager']=PROSPECT_ORDER_CLOSED_BY;
					$data['team'] = PROSPECT_ORDER_CLOSED_BY; */
					$data['order_closed_by']=$data['project_manager']=$my['uid'];
					$data['team'] = $my['uid'];
					$data['display_name']=$data['display_user_id']=$data['display_designation']='';				
					/*
						$randomUser = getCeoDetails(); 
						$data['display_name'] = $randomUser['name'];
						$data['display_user_id'] = $randomUser['user_id'];
						$data['display_designation'] = $randomUser['desig']; 
					*/	 
					$data['display_name'] = $my['f_name']." ".$my['l_name'];
					$data['display_user_id'] = $my['user_id'];
					$data['display_designation'] = $my['desig']; 
					if($my['department'] == ID_MARKETING){
						if(!empty($data['executive_id'])){
							$randomUser = getRandomAssociate($data['executive_id']);
							$data['project_manager'] = $data['order_closed_by'] = $data['tck_owner_member_id'] = $randomUser['user_id']  ;
							$data['tck_owner_member_name'] = $randomUser['name'] ; 
							$data['tck_owner_member_email'] = $randomUser['email']  ;
							$data['marketing_email'] = $randomUser['email']  ;
							$data['marketing_contact'] = $randomUser['marketing_contact'];
							$data['marketing'] = 1; 
							$data['display_name'] = $randomUser['name']  ;
							$data['display_user_id'] = $randomUser['user_id']  ;
							$data['display_designation'] = $randomUser['desig']  ; 
							$data['order_closed_by2'] = $randomUser['user_id'];
							$data['order_closed_by2_name']= $randomUser['name'];
						}else{ 
							$data['tck_owner_member_id'] = $my['user_id'];
							$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
							$data['tck_owner_member_email'] = $my['email'];
							$data['marketing_email'] = $my['email'];
							$data['marketing_contact'] = $my['marketing_contact'];
							$data['marketing'] = 1;
							$data['display_name'] = $my['f_name']." ".$my['l_name'];
							$data['display_user_id'] = $my['user_id'];
							$data['display_designation'] = $my['desig']; 
							
							if($my['user_id'] == CEO_USER_ID){									
								$randomUser = getRandomAssociate(PROSPECT_ORDER_CLOSED_BY);
								$data['tck_owner_member_id'] = $my['user_id'];
								$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
								$data['tck_owner_member_email'] = $my['email']; 
								$data['project_manager'] = $data['order_closed_by'] = $data['tck_owner_member_id'] = $my['user_id'] ; 
								$data['marketing_email'] = $randomUser['email']  ;
								$data['marketing_contact'] = $randomUser['marketing_contact'];
								$data['marketing'] = 1; 
								$data['display_name'] = $randomUser['name']  ;
								$data['display_user_id'] = $randomUser['user_id']  ;
								$data['display_designation'] = $randomUser['desig']  ; 
								$data['order_closed_by2'] = 	$randomUser['user_id'];
								$data['order_closed_by2_name'] = $randomUser['name'];									
							}
							
						}
					}else{ 
						$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
						$data['tck_owner_member_name']= $data['display_name'] =SALES_MEMBER_USER_NAME;
						$data['tck_owner_member_email'] = SALES_MEMBER_USER_EMAIL;
						$data['marketing_email'] = SALES_MEMBER_USER_EMAIL;
						$data['marketing_contact'] = '';
						$data['marketing'] = 0;
					}
					
					$do_o = mktime(0,0,0,date('m'),date('d'),date('Y')); 
					$detailOrdNo = getProspectLeadNumber($do_o); 				
					if(!empty($detailOrdNo)){
						$data['or_number'] = $detailOrdNo['number'];
						$data['ord_counter'] = $detailOrdNo['ord_counter'];
					}
					$data['order_title'] = substr($data['details'],0,50);
					$query	= " INSERT INTO ".TABLE_PROSPECTS_ORDERS
						." SET ". TABLE_PROSPECTS_ORDERS .".number       = '". $data['or_number'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".ord_counter     = '". $data['ord_counter'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".access_level     = '". $my['access_level'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".client           = '". $data['user_id'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".order_of         = '". $data['order_of'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".created_by       = '". $my['uid'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".company_id	     = '". $data['company_id'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".company_name	 = '". $data['company_name'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".company_prefix   = '". $data['company_prefix'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".order_title	     = '". $data['order_title'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".currency_id      = '". $data['currency_id'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".currency_abbr    = '". $data['currency_abbr'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".currency_name    = '". $data['currency_name'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".currency_symbol  = '". $data['currency_symbol'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".currency_country = '". $data['currency_country'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".order_type	    = '". $data['order_type'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".order_closed_by	= '". $data['order_closed_by'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".order_closed_by2	= '". $data['order_closed_by2'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".order_closed_by2_name	= '". $data['order_closed_by2_name'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".team             = ',".$data['team'].",'"
						.",". TABLE_PROSPECTS_ORDERS .".details          	= '". $data['details'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".competitor_details  = '". $data['competitor_details'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".budget_details      = '". $data['budget_details'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".your_estimation     = '". $data['your_estimation'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".queries          = '". $data['queries'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".remarks          = '". $data['remarks'] ."'"
						.",". TABLE_PROSPECTS_ORDERS .".do_o             = '". date('Y-m-d H:i:s') ."'"
						.",". TABLE_PROSPECTS_ORDERS .".do_c             = '". date('Y-m-d H:i:s') ."'"
						.",". TABLE_PROSPECTS_ORDERS .".project_manager  = '". $data['project_manager']."'"
						.",". TABLE_PROSPECTS_ORDERS .".status           = '".ProspectsOrder::ACTIVE."'" 
						.",". TABLE_PROSPECTS_ORDERS .".ip           	 = '".$_SERVER['REMOTE_ADDR']."'" ;
                    //Add Prospect Lead eof
                    $db->query($query);
                     
                     
                    // Send Email to the newly added Client.
                    $data['added_by']=$my['f_name']." ".$my['l_name'] ;
					$data['do_add']= date('d M Y');
					$data['no'] = $data['number'] ;
					$data['company_name'] = $data['billing_name'] ;
					$data['name'] = $data['f_name']." ".$data['l_name'] ;
					
					$data['email'] 	 = $data['email'] ;
					$data['email_1'] = $data['email_1'] ;
					$data['email_2'] = $data['email_2'] ;
					$data['mobile1'] = $data['mobile1'] ;
					$data['mobile2'] = $data['mobile2'] ;
					$data['mobile3'] = $data['mobile3'] ;
					$data['remarks'] = $data['remarks'];
					$data['lead_details'] = $data['details'];
					$data['or_number'] = $data['or_number'];
					
                    $email = NULL;					
					if ( getParsedEmail($db, $s, 'CEO_QPROSPECT_ADD', $data, $email) ) {
						$to     = '';
						$to[]   = array('name' => $sales_name, 'email' => $sales_email);
						//echo $email["body"];
						$from   = $reply_to = array('name' => $sales_name, 'email' => $sales_email);
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
					}
					//Add Prospect Lead EOF 
					/*SEND SERVICE PDF BOF*/
					include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
					include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');  
					$id=39;
					$fields = TABLE_ST_TEMPLATE .'.*'  ;            
					$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
					SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1);
					
					if(!empty($details)){
					
						$details = $details[0];
						$data['template_id'] = $details['id'];
						$data['template_file_1'] = $details['file_1'];
						$data['template_file_2'] = $details['file_2'];
						$data['template_file_3'] = $details['file_3'];
						$data['subject'] = processUserData($details['subject']);
						$data['text'] = processUserData($details['details']);
						$data['ticket_owner'] = $data['f_name']." ".$data['l_name'] ;
						$data['ticket_owner_uid'] = $variables['hid'] ;
						$ticket_no  =  ProspectsTicket::getNewNumber($db);
						$ticket_status = ProspectsTicket::PENDINGWITHCLIENTS;
						
						$query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
						. TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
						. TABLE_PROSPECTS_TICKETS .".template_id       = '". $data['template_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['subject'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['text'] ."', "
						. TABLE_PROSPECTS_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
						. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_status     = '".$ticket_status ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_child      = '0', "
						. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
						. TABLE_PROSPECTS_TICKETS .".service_pdf_tkt = '1', "
						. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
						. TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
						. TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
						. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
						. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
						. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
						. TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name']."', "
						. TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
						
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
						if(!empty($data['email']) || !empty($data['email_1']) || !empty($data['email_2'])){
							$db->query($query) ;
							$ticket_id = $db->last_inserted_id() ;
							$mail_send_to_su='';
							$data['mticket_no']   =   $ticket_no ;
							if ( getParsedEmail($db, $s, 'EMAIL_PST_MEMBER', $data, $email) ){ 
								if(!empty($data['marketing_email'])){
									$email["from_name"] =   $email["from_name"]." - ".$data['display_name'];
									$email["from_email"] = $data['marketing_email'];
								}
								$to = '';    
								if(!empty($data['email'])){
									$to[]   = array('name' => $data['email'] , 'email' => 
									$data['email']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,
									$cc,$bcc,$file_name); */
								}
								if(!empty($data['email_1'])){
									//$to = '';
									$to[]   = array('name' => $data['email_1'] , 'email' => 
									$data['email_1']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email_1'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,
									$cc,$bcc,$file_name); */
								}
								if(!empty($data['email_2'])){
									//$to = '';
									$to[]   = array('name' => $data['email_2'] , 'email' => 
									$data['email_2']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$data['email_2'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,
									$cc,$bcc,$file_name); */
								}
							 
							 	if(!empty($to)){
									//$to = '';
									$bcc[]  = array('name' => $data['ticket_owner'] , 'email' => 
									$smeerp_client_email);        
						 			SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,
									$cc,$bcc,$file_name); 
									//echo $email["body"];
								}
							}
							
							if(!empty($mail_send_to_su)){
								$query_update = "UPDATE ". TABLE_PROSPECTS_TICKETS 
                                    ." SET ". TABLE_PROSPECTS_TICKETS .".mail_send_to_su
									= '".trim($mail_send_to_su,",")."'"                                   
                                    ." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_id = '". $ticket_id ."' " ;
								$db->query($query_update) ;
							}
						}
						
					}
					/*SEND SERVICE PDF EOF*/  
                    /*
                    if ( getParsedEmail($db, $s, 'NEW_CLIENT_REGISTER', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['f_name'] .' '. $data['l_name'], 'email' => $data['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }*/
                    $messages->setOkMessage("The New Prospect has been added.");
                    
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }else{
			
				$region     = new RegionProspects($data['country'][0],$data['state'][0],$data['city'][0]);
			}
        }else{
            $_ALL_POST['cc'][0]=91;
            $_ALL_POST['cc'][1]=91;
            $_ALL_POST['cc'][2]=91;
            $_ALL_POST['service_id'][0]=Prospects::BILLING;
            $_ALL_POST['service_id'][1]=Prospects::ST;
			$_ALL_POST['access_level']=$my['access_level'];
        }
       
        
        if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            header("Location:".DIR_WS_NC."/prospects.php?perform=edit&added=1&ajx=1&user_id=".$variables['hid']);
            
        }
       /*  if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            header("Location:".DIR_WS_NC."/prospects.php?perform=list&added=1");
            
        }
        else { */

            $_ALL_POST['number'] = Prospects::getNewAccNumber($db);
            if ( empty($data['country']) ) {
                $data['country'] = '91';
            }
            if(empty($data['state'])){
                $data['state']='';
            }
            if(empty($data['city'])){
                $data['city']='';
            }
            $variables['can_select_mkt'] = false;
			if ( $perm->has('nc_pr_select_mkt') ) {
				$variables['can_select_mkt'] = true;
			}
			
            //Code default aryan manke in team list EOF
            if(!isset($_ALL_POST['team'])){
                $temp ='e68adc58f2062f58802e4cdcfec0af2d';
                User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN ('". $temp ."')");
                $_ALL_POST['team'] = array();
                foreach ( $_ALL_POST['team_members'] as $key=>$members) {
                
                    $_ALL_POST['team'][] = $members['user_id'];
                    $_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
                }
            }          
            $lst_country=$lst_state= $lst_city=array();
            $lst_country    = $region->getCountryList($data['country']);
            $lst_state      = $region->getStateList($data['state']);
            $lst_city       = $region->getCityList($data['city']);
             
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
           
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'reminder_count', 'value' => '6');
            $hidden[] = array('name'=> 'ajx','value' => $ajx);
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            // $page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
			
            $page["var"][] = array('variable' => 'source', 'value' => 'source');
            $page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
            $page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
            $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
            $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
            $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
            $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
            $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
            $page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-add.html');
        //}
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>