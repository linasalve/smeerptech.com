<?php

    if ( $perm->has('nc_sl_q_details') ) {
        $q_id = isset($_GET['q_id']) ? $_GET['q_id'] : ( isset($_POST['q_id'] ) ? $_POST['q_id'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_sl_q_details_al') ) {
            $access_level += 1;
        }
		
		$query="SELECT ".TABLE_SALE_QUOTATION.".* , ".TABLE_SALE_QUOTATION_P.".particulars, ".TABLE_SALE_QUOTATION_P.".p_amount, "
                .TABLE_SALE_QUOTATION_P.".ss_title, ".TABLE_SALE_QUOTATION_P.".ss_punch_line, "                
                .TABLE_SALE_QUOTATION_P.".tax1_name, ".TABLE_SALE_QUOTATION_P.".tax1_value, "                
                .TABLE_SALE_QUOTATION_P.".tax1_pvalue, ".TABLE_SALE_QUOTATION_P.".d_amount, "                
                .TABLE_SALE_QUOTATION_P.".stot_amount, ".TABLE_SALE_QUOTATION_P.".tot_amount, ".TABLE_SALE_QUOTATION_P.".discount_type                 
				FROM ".	TABLE_SALE_QUOTATION.
                " LEFT JOIN ". TABLE_SALE_QUOTATION_P." ON ".TABLE_SALE_QUOTATION.".number= ".TABLE_SALE_QUOTATION_P.".q_no
                 WHERE ".TABLE_SALE_QUOTATION.".id='".$q_id."'";
							
        if ( ($db->query($query)) > 0 ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$list[] = processSqlData($db->result());
				}
			}		
/*        if ( Lead::getList($db, $list, '*', " WHERE user_id = '$user_id'") > 0 ) {*/
            $_ALL_POST = $list;
            
            if ( $_ALL_POST['0']['access_level'] < $access_level ) {
                //include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
                //include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
                //include_once ( DIR_FS_CLASS .'/UserReminder.class.php');

                //$regionlead         = new RegionLead();
                //$phonelead          = new PhoneLead(TABLE_SALE_LEADS);
                //$reminder       = new UserReminder(TABLE_LEADS);

                //$_ALL_POST['manager'] = Lead::getManager($db, '', $_ALL_POST['manager']);

                // Read the available Access Level and User Roles.
				
				//$_ALL_POST['access_level'];
                //$_ALL_POST['access_level']  = getAccessLevel($db, $access_level, $_ALL_POST['access_level']);
                //$_ALL_POST['roles']         = Lead::getRoles($db, $access_level, $_ALL_POST['roles']);
                //print_r($role_list);
                // Read the Contact Numbers.
                //$phonelead->setPhoneOf(TABLE_SALE_LEADS, $lead_id);
                //$_ALL_POST['phone'] = $phonelead->get($db);
                
                // Read the Addresses.
                //$regionlead->setAddressOf(TABLE_SALE_LEADS, $lead_id);
                //$_ALL_POST['address_list'] = $regionlead->get();
                
                // Read the Reminders.
                //$reminder->setReminderOf(TABLE_SALE_LEADS, $user_id);
                //$_ALL_POST['reminder_list'] = $reminder->get($db);
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    			//$page["var"][] = array('variable' => 'source_title_from', 'value' => 'source_title_from');
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-quotation-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Quotation with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("The Quotation details were not found.");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>