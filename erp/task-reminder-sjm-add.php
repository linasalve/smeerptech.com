<?php
    if ( $perm->has('nc_ts_sjmadd') ) {
        
        /* $_ALL_POST      = NULL;
        $data           = NULL; */
        $access_level   = $my['access_level'];
         
		$groups_list  = User::getGroups();
        
        // BO: Read the available job list BOF
	    $lst_job = NULL;
        $required_fields ='*';
        $condition_query = "WHERE status='".Jobs::PENDING."'";
		Jobs::getList($db,$lst_job,$required_fields,$condition_query);
	    // EO: Read the job list  list in dropdown box EOF
        
        
        $lst_type=array_flip(Taskreminder::getTypes());
        $allotted_to_str1=$allotted_to_client_str1=$allotted_to_lead_str1=$allotted_to_addr_str1='';
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
             
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
             $files  = processUserData($_FILES);
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE ;
			
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
            $allotted_to_str1=$allotted_to_client_str1=$allotted_to_lead_str1=$allotted_to_addr_str1='';
            $allotted_to_name = $allotted_to_client_name = $allotted_to_lead_name=$allotted_to_addr_name ='';
            
            
            if ( !empty($data['job_id']) ) {
                Jobs::getList($db, $_ALL_POST['job_details'], 'title,description', 
				" WHERE id = '". $data['job_id'] ."'");   
                //$_ALL_POST['job_details'] = $_ALL_POST['job_details']['0'];
                $data['job_title'] = $_ALL_POST['job_details']['0']['title'];
            }
            
            if ( Taskreminder::validateAdd($data, $extra) ) {
				if(!empty($data['file_1'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_1']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = mktime()."-1".".".$ext ;
					$data['file_1'] = $attachfilename;
					
					if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_TASKSCHEDULER_FILES."/".$attachfilename)){
					   
					  @chmod(DIR_FS_TASKSCHEDULER_FILES."/".$attachfilename,0777);
					}
				}
				
                if(!empty($data['allotted_to'])) {
                    $allotted_to_str1 = implode(",",$data['allotted_to']);
                    //$allotted_to_str1.=",".$data['created_by'];
                    $allotted_to_str = ",".$allotted_to_str1.",";   
                    
                    $sql= " SELECT f_name,l_name FROM ".TABLE_USER." WHERE user_id 
					IN('".str_replace(",","','",$allotted_to_str1)."') ";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_name1 = trim($allotted_to_name,",") ;
                        $allotted_to_name = ",".$allotted_to_name1."," ;
                    }
                }
				
				/*else{
                    $group_like_q = '';
					if(!empty($data['groups'])){
					
						$groupsStr='';
						$groups1=implode(",",$data['groups']);
						$groupsStr = ",".$groups1.",";
										
						
						foreach($data['groups'] as $key=>$val){
						
							$group_like_q .= TABLE_USER.".groups LIKE '%,".trim($val).",%' OR " ;
						}
						
						$group_like_q = substr( $group_like_q, 0, strlen( $group_like_q ) - 3 );
                        
						$sql= " SELECT f_name,l_name,user_id FROM ".TABLE_USER." WHERE ".$group_like_q." ";
						$db->query($sql);
						if ( $db->nf()>0 ) {
							while ( $db->next_record() ) { 
								$allotted_to_name .= $db->f('f_name')." ".$db->f('l_name').",";
								$allotted_to_str1 .= $db->f('user_id')."," ;
								
								$userDetails[] = array(  
								);
							}
							$allotted_to_name1 = trim($allotted_to_name,",") ;
							$allotted_to_name = ",".$allotted_to_name1."," ;
							$allotted_to_str1 = trim($allotted_to_str1,",") ;
							$allotted_to_str = ",".$allotted_to_str1.","; 
						}
					} 
                }
                if(!empty($data['allotted_to_client'])) {
                    $allotted_to_client_str1=implode(",",$data['allotted_to_client']);
                    $allotted_to_client_str=",".$allotted_to_client_str1.",";
                    $sql= " SELECT f_name,l_name FROM ".TABLE_CLIENTS." WHERE user_id IN('".str_replace(",","','",$allotted_to_client_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_client_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_client_name1 = trim($allotted_to_client_name,",") ;
                        $allotted_to_client_name = ",".$allotted_to_client_name1."," ;
                    }
                    
                }
                if(!empty($data['allotted_to_vendor'])) {
                    $allotted_to_vendor_str1=implode(",",$data['allotted_to_vendor']);
                    $allotted_to_vendor_str=",".$allotted_to_vendor_str1.",";
                    $sql= " SELECT f_name,l_name FROM ".TABLE_VENDORS." WHERE user_id IN('".str_replace(",","','",$allotted_to_vendor_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_vendor_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_vendor_name1 = trim($allotted_to_vendor_name,",") ;
                         $allotted_to_vendor_name = ",".$allotted_to_vendor_name1."," ;
                    } 
                }
                if(!empty($data['allotted_to_lead'])) {
                    $allotted_to_lead_str1=implode(",",$data['allotted_to_lead']);  
                    $allotted_to_lead_str=",".$allotted_to_lead_str1.",";   
                    $sql= " SELECT f_name,l_name FROM ".TABLE_SALE_LEADS." WHERE lead_id IN('".str_replace(",","','",$allotted_to_lead_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_lead_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_lead_name1 = trim($allotted_to_lead_name,",") ;
                        $allotted_to_lead_name = ",".$allotted_to_lead_name1."," ;
                    }
                }
                
                if(!empty($data['allotted_to_addr'])){
                    $allotted_to_addr_str1=implode(",",$data['allotted_to_addr']);      
                    $allotted_to_addr_str=",".$allotted_to_addr_str1.",";     
                    $sql= " SELECT name FROM ".TABLE_ADDRESS_BOOK." WHERE lead_id IN('".str_replace(",","','",$allotted_to_addr_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_lead_name .= $db->f('name').",";
                        }
                        $allotted_to_lead_name1 = trim($allotted_to_lead_name,",") ;
                        $allotted_to_lead_name = ",".$allotted_to_lead_name1."," ;
                    }
                }
				*/
				$is_imp=0;
				$data['imp']='-CEO ';
				if(isset($data['is_imp'])){
					$is_imp=1;
					$data['imp']='-IMP ';
				}   
				
                $task_no  =  Taskreminder::getNewNumber($db);
                
                $query	= " INSERT INTO ".TABLE_TASK_REMINDER
						." SET ". TABLE_TASK_REMINDER .".task = '".             $data['task'] ."'"
						.",". TABLE_TASK_REMINDER .".task_no = '".              $task_no ."'"                        
						.",". TABLE_TASK_REMINDER .".is_imp = '". $is_imp ."'"     
						.",". TABLE_TASK_REMINDER .".is_ceo = '1'"     
						.",". TABLE_TASK_REMINDER .".file_1 = 	'".	$data['file_1'] ."'"						
						.",". TABLE_TASK_REMINDER .".comment = '".              $data['comment'] ."'"
						.",". TABLE_TASK_REMINDER .".access_level = '".         $my['access_level'] ."'"
						.",". TABLE_TASK_REMINDER .".created_by = '".           $data['created_by'] ."'"
						.",". TABLE_TASK_REMINDER .".created_by_name = '".      $my['f_name']." ". $my['l_name']."'"
						.",". TABLE_TASK_REMINDER .".do_r = '".                 $data['do_r'] ."'"
						.",". TABLE_TASK_REMINDER .".do_deadline = '".          $data['do_deadline'] ."'"
						//.",". TABLE_TASK_REMINDER .".type = '".                 Taskreminder::CEOTASK ."'"
						.",". TABLE_TASK_REMINDER .".status = '".               $data['status'] ."'"
						.",". TABLE_TASK_REMINDER .".priority = '".             $data['priority'] ."'"
						.",". TABLE_TASK_REMINDER .".send_sms = '".             $data['send_sms'] ."'"
						.",". TABLE_TASK_REMINDER .".groups = '".          		$groupsStr ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to = '".          $allotted_to_str ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_name = '".     $allotted_to_name ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_client = '".   $allotted_to_client_str ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_client_name = '".$allotted_to_client_name ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_vendor = '".   $allotted_to_vendor_str ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_vendor_name = '".$allotted_to_vendor_name ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_lead = '".     $allotted_to_lead_str ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_lead_name = '".$allotted_to_lead_name ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_addr = '".     $allotted_to_addr_str ."'"
						.",". TABLE_TASK_REMINDER .".allotted_to_addr_name = '".$allotted_to_addr_name."'"
						.",". TABLE_TASK_REMINDER .".on_behalf_id = '".         $data['on_behalf_id'] ."'"
						.",". TABLE_TASK_REMINDER .".order_id = '".             $data['order_id'] ."'"
						.",". TABLE_TASK_REMINDER .".job_id = '".               $data['job_id'] ."'"
						.",". TABLE_TASK_REMINDER .".hrs = '".                  $data['hrs'] ."'"
						.",". TABLE_TASK_REMINDER .".min = '".                  $data['min'] ."'"
						.",". TABLE_TASK_REMINDER .".time_type = '".            $data['time_type'] ."'"
						.",". TABLE_TASK_REMINDER .".do_e = '".                date('Y-m-d h:i:s') ."'";
            
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                
                    $messages->setOkMessage("Task entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                   
                    $statusArr = Taskreminder::getStatus();
                    $statusArr = array_flip($statusArr);
                    $priorityArr = Taskreminder::getPriority();
                    $priorityArr = array_flip($priorityArr);
                    $data['task_title']       =   $_ALL_POST['task'];
                    $data['task']       =   $_ALL_POST['comment'];
                    $data['task_no']       =   $task_no;
                    //$data['comment']    =   $data['comment'] ;
                    $data['do_r']       =   date("d M Y",strtotime($data['do_r']));
				 
                    $data['do_deadline']=   $data['do_deadline'];
                    $data['af_name']    =   $my['f_name'] ;
                    $data['al_name']    =   $my['l_name'] ;
                    $data['status']     =   $statusArr[$data['status']] ;
                    $data['priority']     =   $priorityArr[$data['priority']] ;
                    $send_to = $send_to_client= $send_to_vendor='';
					$file_name='';
					if(!empty($attachfilename)){
						$file_name[] = DIR_FS_TASKSCHEDULER_FILES ."/". $attachfilename;
					}
                    // Send Email to the users involved BOF  
                    //include ( DIR_FS_INCLUDES .'/user.inc.php');
                    /*
                    if(!empty( $allotted_to_str1)){                          
                        $allotted_to_str1 = trim($allotted_to_str1,",");
                        $string = str_replace(",","','", $allotted_to_str1);
                    }
                    */
                    if(!empty($data['allotted_to'])) {
                        //$string1 = implode("','",$data['allotted_to']);
                        $string1 = $allotted_to_str1;
                        //$string2 = $data['created_by'];
                        //$string = $string1."','".$string2;
                        $string = $string1;
						
                    }else{
						$string1 = $allotted_to_str1;
                        //$string2 = $data['created_by'];
                        //$string = $string1."','".$string2;
						 $string = $string1;
                    }
                  
                    User::getList($db, $userDetails, 'number, f_name, l_name, email,mobile1,mobile2', " WHERE 
					user_id IN('".$string."')" );
					
					
                    $data['mail_exec']=0;
                    
                    if(!empty($userDetails) && isset($data['mail_exec']) && $data['mail_exec']==1){
                        foreach ( $userDetails as $user ) {
                             $team_members .=$user['f_name']." ".$user['l_name']."<br/>" ;							 
                        }
						$data['team_members'] = $team_members ;
						foreach ( $userDetails as $user ) {
                            $data['uf_name']    =   $user['f_name'];
                            $data['ul_name']    =   $user['l_name'] ;
                            $email = NULL;                            
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_ADD', $data, $email) ) {
								$to = $cc_to_sender=$cc=$bcc='';
								$to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);                          
                                $send_to .=$user['email'].",";
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
                            }
                        }
                        $send_to = trim($send_to,",");
                    }else{
                        foreach ( $userDetails as $user ){
                             $team_members .=$user['f_name']." ".$user['l_name']."<br/>" ;							 
                        }
						$data['team_members'] = $team_members ;
						if(in_array('e68adc58f2062f58802e4cdcfec0af2d',$data['allotted_to'])){
							//Aryan Sir alloted in task then send mail
							$data['uf_name']    =   'Aryan';
                            $data['ul_name']    =  'Salve' ;
							$user['email'] 		= 'aryan@smeerptech.com';
							$email = NULL;                            
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_ADD', $data, $email) ) {
                                $to = $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $user['uf_name'] .' '. $user['ul_name'], 'email' => 
								$user['email']);                          	   
								//echo $email['subject']."";
                                $send_to .=$user['email'].",";
								$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
							}
						}
                    }
					$data['sms_sent_mobile']='';
					$counter=0;
					if($data['send_sms']==1){
						foreach ( $userDetails as $user ) {
							if(!empty($user['mobile1'])){
								
								$data['sms_sent_mobile'] .= $user['mobile1'].",";
								$data['text'] = substr($data['task'], 0, 130);
								
								$mobile1 = "91".$user['mobile1'] ;
								ProjectTask::sendSms($data['text'],$mobile1);
								$counter = $counter+1;
							}
							if(!empty($user['mobile2'])){
								$mobile2 = "91".$user['mobile2'];
								$data['text'] = substr($data['task'], 0, 130);
								
								ProjectTask::sendSms($data['text'],$mobile2);
								$counter = $counter+1;
								$data['sms_sent_mobile'] .= $user['mobile2'].",";
							}
                            if(!empty($data['sms_sent_mobile'])){
								
								$query_update = "UPDATE ". TABLE_TASK_REMINDER 
                                ." SET ". TABLE_TASK_REMINDER .".sms_sent_mobile 
									= '".trim($data['sms_sent_mobile'],",").  "' "
                                    ." WHERE ". TABLE_TASK_REMINDER .".id = '". $variables['hid'] ."' " ;
								$db->query($query_update) ;
							
							}
							 
						
							//update sms counter
							if($counter >0){
								$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
								$db->query($sql); 
							}
							 
                        }						
						 
					}
                    $data['team_members'] = $team_members ;
                    
                    // Send Email to the users involved EOF
                 
                    // Send Email to the clients involved BOF                                   
                    if(!empty($allotted_to_client_str1) && isset($data['mail_client'])){
                        //include ( DIR_FS_INCLUDES .'/clients.inc.php');
                        $string = str_replace(",","','", $allotted_to_client_str1);
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id IN( '".$string."' )");
                        foreach ( $clientDetails as $client ) { 
                            $data['uf_name']    =   $client['f_name'];
                            $data['ul_name']    =   $client['l_name'] ;
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_ADD', $data, $email) ) {
                                $to = $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $client['f_name'] .' '. $client['l_name'], 'email' => $client['email']);
                                $send_to_client .=$client['email'].",";
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                 
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
                            }
                        }
                        $send_to_client = trim($send_to_client,",");
                    }else{
                        $data['mail_client']=0;
                    }
                    // Send Email to the clients involved EOF
                    // Send Email to the vendors involved BOF                                   
                    if(!empty($allotted_to_vendor_str1) && isset($data['mail_vendor'])){
                        
                        //include ( DIR_FS_INCLUDES .'/clients.inc.php');                         
                       $allotted_to_str1 = trim($allotted_to_vendor_str1,",");
                       $string = str_replace(",","','", $allotted_to_str1);
                       Vendors::getList($db, $vendorDetails, 'number, f_name, l_name, email', " WHERE user_id IN('".$string."' )");
                      
                        foreach ( $vendorDetails as $vendor ) { 
                            $data['uf_name']    =   $vendor['f_name'];
                            $data['ul_name']    =   $vendor['l_name'] ;
                            $email = NULL;
                            
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_ADD', $data, $email) ) {
                                $to = $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $vendor['f_name'] .' '. $vendor['l_name'], 'email' => $vendor['email']);
                                $send_to_vendor .=$vendor['email'].",";
                                //echo $email["body"];
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                /*
								print_r($to);
                                print_r($from);
                                echo $email["body"];
                                echo $email["subject"]; 
								*/
                                 SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
                            }
                        }
                        $send_to_vendor = trim($send_to_vendor,",");
                    }else{
                        $data['mail_vendor']=0;
                    }
                    
                    // Send Email to the vendors involved EOF
                    // Send Email to the leads involved BOF  
                    if(!empty($allotted_to_lead_str1) && isset($data['mail_lead'])){
                        //include ( DIR_FS_INCLUDES .'/sale-lead.inc.php');
                        $string = str_replace(",","','", $allotted_to_lead_str1);
                        Leads::getList($db, $leadDetails, 'lead_id, f_name, l_name, email', " WHERE lead_id IN( '".$string."' )");
                        foreach ( $leadDetails as $lead ) {
                            $data['uf_name']    =   $lead['f_name'];
                            $data['ul_name']    =   $lead['l_name'] ;
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_ADD', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $lead['f_name'] .' '. $lead['l_name'], 'email' => $lead['email']);
                              
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                 SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
                            }
                        }
                    }else{
                        $data['mail_lead']=0;
                    }
                    // Send Email to the leads involved EOF
                    
                    // Send Email to the external users involved BOF  
                    if(!empty($allotted_to_addr_str1) && isset($data['mail_external'])){
                        //include ( DIR_FS_INCLUDES .'/address-book.inc.php');
                        $string = str_replace(",","','", $allotted_to_addr_str1);
                        Addressbook::getList($db, $addrDetails, 'name, email', " WHERE id IN( '".$string."' )");
                        foreach ( $addrDetails as $addr ) {
                            $data['uf_name']    =   $addr['name'];
                            $data['ul_name']    =   '' ;
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_ADD', $data, $email) ) {
                                $to = $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $addr['name'] , 'email' => $addr['email']);
                                //echo $email["body"];
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                 SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
                            }
                        }
                    }else{
                        $data['mail_external']= 0;
                    }
             
                    // Send Email to the external users involved EOF
                   
                   // Send Email to the On Behalf users involved BOF  
                    if(!empty($data['on_behalf_id']) && isset($data['mail_on_behalf'])){
                        //include ( DIR_FS_INCLUDES .'/address-book.inc.php');
                        User::getList($db, $userOnBehalfDetails, 'user_id,f_name,l_name,number email', " 
						WHERE user_id ='".$data['on_behalf_id']."'");
                        $userOnBehalfDetails = $userOnBehalfDetails['0'];
                        
                        $data['uf_name']    =   $userOnBehalfDetails['f_name'];
                        $data['ul_name']    =   $userOnBehalfDetails['l_name'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_ADD', $data, $email) ) {
                            $to = $cc_to_sender=$cc=$bcc='';
                            $to[]   = array('name' => $userOnBehalfDetails['f_name'].' '.$userOnBehalfDetails['l_name'] , 'email' => $userOnBehalfDetails['email']);
                            $send_to .=",".$userOnBehalfDetails['email'].",";
                           
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
                        }
                        $send_to = trim($send_to,",");
                    }else{
                        $data['mail_on_behalf']=0;
                    }
                    // Send Email to the On Behalf users involved EOF
                    
                    
                    //Set the list email-id to whom the mails are sent bof
               
					$query_update = "UPDATE ". TABLE_TASK_REMINDER 
                        ." SET ". TABLE_TASK_REMINDER .".last_mail_send_to = '".trim($send_to,",")."', last_mail_send_to_client='".trim($send_to_client,",")."', last_mail_send_to_vendor ='".trim($send_to_vendor,",")."' "                                    
                        ." WHERE ". TABLE_TASK_REMINDER .".id = '". $variables['hid'] ."' " ;
                    $db->query($query_update) ;
                    //Set the list subuser's email-id to whom the mails are sent eof 
                    
                    
                    // Send Email to the taskadmin BOF  
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'TASK_REMINDER_ADMIN', $data, $email) ) {
                        $to = $cc_to_sender=$cc=$bcc='';
                        $to[]   = array('name' => $taskadmin_name , 'email' => $taskadmin_email);                           
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                       
                        
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                        $cc_to_sender,$cc,$bcc,$file_name);
                    }
                    // Send Email to the admin EOF  
                }
             
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
				$_ALL_POST['allotted_to']=array('0'=>'e68adc58f2062f58802e4cdcfec0af2d');
				$_ALL_POST['team_details']=array('0'=>'Aryan Salve');		
            }
        }else{
			$_ALL_POST['allotted_to']=array('0'=>'e68adc58f2062f58802e4cdcfec0af2d');
			$_ALL_POST['team_details']=array('0'=>'Aryan Salve');	
			
		}
       
        //Check permissions to add executives
        //$variables['can_view_ulist']     = false;
        $variables['can_view_clist']     = false;
        $variables['can_view_sllist']     = false;
        $variables['can_view_exlist']     = false;
        $variables['can_add_on_behalf']     = false;
        $variables['can_add_impt']     = false;
        $variables['statuspending'] = Taskreminder::PENDING ;
        //For Users
        /*
        if ( $perm->has('nc_ue') && $perm->has('nc_ue_list')) {
            $variables['can_view_ulist']     = true;
        }*/
        //For Clients
        if ( $perm->has('nc_uc') && $perm->has('nc_uc_list')) {
            $variables['can_view_clist']     = true;
        }
        //For Vendor
        if ( $perm->has('nc_uv') && $perm->has('nc_uv_list') ) {
           $variables['can_view_vlist'] = true;
        }
        //For Lead
        if ( $perm->has('nc_sl_ld') && $perm->has('nc_sl_ld_list')) {
            $variables['can_view_sllist']     = true;
        }
        //For External Users
        if ( $perm->has('nc_ab') && $perm->has('nc_ab_list')) {
            $variables['can_view_exlist']     = true;
        }
        //To add on behalf of
        if ( $perm->has('nc_ts_behalf')) {
            $variables['can_add_on_behalf']     = true;
        }
        if( $perm->has('nc_ts_add_imp')) {
            $variables['can_add_impt']     = true;
        }
		
        /* if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/task-reminder.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/task-reminder.php?perform=list");
        } */
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
           
            //include ( DIR_FS_NC .'/payment-transaction-list.php');
            header("Location:".DIR_WS_NC."/task-reminder.php?perform=list&added=1");
            
        }else {
            $_ALL_POST['do_r']=$do_r;
            
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
			$hidden[] = array('name'=> 'ajx' ,'value' => $ajx);
			$page["var"][] = array('variable' => 'groups_list', 'value' => 'groups_list');			
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'variables', 'value' => 'variables'); 
            $page["var"][] = array('variable' => 'lst_job', 'value' => 'lst_job'); 
            $page["var"][] = array('variable' => 'lst_type', 'value' => 'lst_type');  
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'task-reminder-sjm-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>