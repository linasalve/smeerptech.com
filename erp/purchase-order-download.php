<?php
if ( $perm->has('nc_po') ) {
    
        $bill_id = isset ($_GET['bill_id']) ? $_GET['bill_id'] : ( isset($_POST['bill_id'] ) ? $_POST['bill_id'] :'');
        //$type   = isset ($_GET['type']) ? $_GET['type'] : ( isset($_POST['type'] ) ? $_POST['type'] :'');
        $file   = isset ($_GET['file']) ? $_GET['file'] : ( isset($_POST['file'] ) ? $_POST['file'] :'');
        
        $condition_query= " WHERE ". TABLE_PURCHASE_ORDER .".id = '". $bill_id ."'";
        
        $list	= NULL;
        if ( PurchaseOrder::getDetails( $db, $list, TABLE_PURCHASE_ORDER .'.id', $condition_query) ) {
          
            if ( $bill_id>0 ) {
                $file_path = DIR_FS_BILLS_FILES ."/";
                $file_name = $file  ;
                $content_type = mime_content_type(DIR_FS_BILLS_FILES ."/".$file_name);
            } 
            else {
                echo('<script language="javascript" type="text/javascript">
				alert("Invalid File specified");</script>');
            }
            
            
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-type: $content_type");
            header("Content-Disposition: attachment; filename=". $file_name );
            header("Content-Length: ".filesize($file_path.$file_name));
            readfile($file_path.$file_name);
        }
        else {
            echo('<script language="javascript" type="text/javascript">alert("The File is not found or you do not have the permission to view the file.");</script>');
        }
    }
    else {
        echo('<script language="javascript" type="text/javascript">alert("You do not have the permission to view the list.");</script>');
    }
    exit;
?>