<?php
    if ($perm->has('nc_nwlm_add') ) {
 
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'messages'          => &$messages
                        );
          
            if ( NewslettersMembers::validateAdd($data, $extra) ) {
				$query  = " INSERT INTO ". TABLE_NEWSLETTERS_MEMBERS
					." SET ". TABLE_NEWSLETTERS_MEMBERS .".email = '".  $data['email'] ."',
					". TABLE_NEWSLETTERS_MEMBERS .".nws_category_id = '".  $data['nws_category_id'] ."'
					";
				if ( $db->query($query)  ) {
                    $messages->setOkMessage("Record updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
         

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
			header("Location:".DIR_WS_NC."/newsletters-members.php?perform=list");		
			
        } else {
		
			
			$fields1= "id,category";
			$condition_query1 = " WHERE ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY.".status='1' ";
			NewslettersMembers::getCategory($db, $category_list,$fields1, $condition_query1);
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            
            $hidden[] = array('name'=> 'perform', 'value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletters-members-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>