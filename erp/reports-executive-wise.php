<?php

if ( $perm->has('nc_rp_ex') ) {
         //to view report member id should be compulsary
        $executive	= isset($_GET["executive"]) 	? $_GET["executive"]	: ( isset($_POST["executive"]) 	? $_POST["executive"] : '' );
        $executive_details	= isset($_GET["executive_details"]) 	? $_GET["executive_details"]	: ( isset($_POST["executive_details"]) 	? $_POST["executive_details"] : '' );
        $_SEARCH["executive"] 	= $executive;       
        $_SEARCH["executive_details"] 	= $executive_details;       
        $where_added    = true;
        $allData = $_GET 	? $_GET	: ($_POST 	? $_POST : '' );
        $extra_url=$condition_query=$condition_url=$pagination ='';
        // BO: Read the Team members list ie member list in dropdown box BOF
        /*$lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields);
        */
        // BO:  Read the Team members list ie member list in dropdown box  EOF
        $totalHrs=$totalMin=$totalHrsMin='00';       
        if(array_key_exists('submit_search',$allData)){
        
            if(!empty($executive)){        
                // Read the Date data.
                $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
                $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
                $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
                $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
                
                // BO: From Date
                if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from)) {
                    if ( $where_added ) {
                        $condition_query .= " ". $chk_date_from;
                    }
                    else {
                        $condition_query.= ' WHERE ';
                        $where_added    = true;
                    }
                    $dfa = explode('/', $date_from);
                    $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
                    //$dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0];
                    $condition_query .= " ". TABLE_PROJECT_TASK_DETAILS .".do_e >= '". $dfa ."' ";
                    
                    $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
                    $_SEARCH["chk_date_from"]   = $chk_date_from;
                    $_SEARCH["date_from"]       = $date_from;
                }
                
                // EO: From Date
                
                // BO: Upto Date
                if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)){
                    if ( $where_added ) {
                        $condition_query .= $chk_date_to;
                    }
                    else {
                        $condition_query.= ' WHERE ';
                        $where_added    = true;
                    }
                    $dta = explode('/', $date_to);
                    $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
                    //$dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0];
                    $condition_query .= " ". TABLE_PROJECT_TASK_DETAILS .".do_e <= '". $dta ."' ";
                    
                    $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
                    $_SEARCH["chk_date_to"] = $chk_date_to ;
                    $_SEARCH["date_to"]     = $date_to ;
                }
                
                // EO: Upto Date      
                
                // Code to get Grand total hrs and min BOF
                $fields1 = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr  '
                    .', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin' ;
                  
               
                $query1 = "SELECT  ".$fields1;            
                $query1 .= " FROM ". TABLE_PROJECT_TASK_DETAILS;
                $query1 .= " WHERE ".TABLE_PROJECT_TASK_DETAILS.".added_by ='".$executive."' AND  (  (parent_id !=0 ) OR  (task_id != 0 AND parent_id=0) )";
                $condition_query1 = $condition_query ;
                $condition_query1 .= " GROUP BY ".TABLE_PROJECT_TASK_DETAILS.".added_by";
                $query1 .= " ". $condition_query1;   
                $db->query($query1);
                if($db->nf() > 0){
                       while ($db->next_record()) {   
                           $totalHrs =$db->f('totHr');
                           $totalMin = $db->f('totMin');
                           $totalHrs = strlen($totalHrs) > 1 ? $totalHrs : '0'.$totalHrs ;
                           $totalMin = strlen($totalMin) > 1 ? $totalMin : '0'.$totalMin ;
                       }    
                }
                $totalHrsMin = $totalHrs.":".$totalMin ;
                // Code to get Grand total hrs and min EOF
                
                // fields req to in query
                $fields = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr  '
                    .', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin'
                    .', '. TABLE_PROJECT_TASK_DETAILS .'.do_e'        
                    .', '. TABLE_PROJECT_TASK_DETAILS .'.project_id' ;
                $query = "SELECT  ".$fields;            
                $query .= " FROM ". TABLE_PROJECT_TASK_DETAILS;
                $query .= " WHERE ".TABLE_PROJECT_TASK_DETAILS.".added_by ='".$executive."' AND (  (parent_id !=0 ) OR  (task_id != 0 AND parent_id=0) )";
                $condition_query .= " GROUP BY ".TABLE_PROJECT_TASK_DETAILS.".project_id, DATE_FORMAT(".TABLE_PROJECT_TASK_DETAILS.".do_e,'%Y-%m-%d') ";
                $query .= " ". $condition_query." ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".do_e ";   
                
                $db->query($query) ;
                if($db->nf() > 0){
                    $total = $db->nf() ;
                }
                else{
                    $total = 0 ;
                }
                $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
                $query .= " LIMIT ". $next_record .", ". $rpp ;
                $db1 		= new db_local;
                $db2 		= new db_local;
               if ( $db->query($query) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {   
                            $sql =" SELECT order_title FROM ".TABLE_BILL_ORDERS." WHERE id = ".$db->f('project_id');
                            $db1->query($sql);
                            if ( $db1->nf() > 0 ) {
                                while ($db1->next_record()) {
                                    $ordername =$db1->f('order_title');
                                }
                            }
                            
                            /*$taskList='';
                            $sql2 =" SELECT note FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE order_id = ".$db->f('order_id')." AND date='".$db->f('date')."' AND user_id ='".$member_id."'";
                            $db2->query($sql2);
                            $nb =1;
                            if ( $db2->nf() > 0 ) {
                                while ($db2->next_record()) {
                                    $taskList .= $nb.". ".nl2br($db2->f('note'))."<br>";
                                    $nb++;
                                }
                            }*/
                            $totHr = $db->f('totHr');
                            
                            if(strlen($totHr) ==1){
                                $totWHrs = "0".$totHr;
                            }else{
                                $totWHrs = $totHr;
                            }
                            
                            $totMin = $db->f('totMin');
                            
                            if(strlen($totMin) ==1){
                                $totWMin = "0".$totMin;
                            }else{
                                $totWMin = $totMin;
                            }
                            $dataArr1 = array(
                                            //'totHr'=>$db->f('totHr'),
                                            'totHr'=>$totWHrs,
                                            'totMin'=> $totWMin,
                                            'date' =>$db->f('do_e'),
                                            'order_id'=>$db->f('project_id')
                                            //'task'=>$taskList
                                        );
                            //$list[][$ordername]=processSqlData($db->result());
                            $list[][$ordername]=$dataArr1;
                           
                        }
                    }
                }
              
                $extra_url  .= "&x=$x&rpp=$rpp&executive=$executive&executive_details=$executive_details&submit_search=1";
                $extra_url  = '&start=url'. $extra_url .'&end=url';
            }else{
                $messages->setErrorMessage('Please select Associate.');
            }
        }   
            
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    
    
     
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'totalHrsMin', 'value' => 'totalHrsMin');
   // $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');        
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    //$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    //$page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-executive-wise.html');
}
else {
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>