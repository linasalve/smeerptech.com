<?php
  if ( $perm->has('nc_fq_at_c_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        $lst_client=null;
        $fields = TABLE_CLIENTS .'.user_id'
                   .','. TABLE_CLIENTS .'.f_name'.','.TABLE_CLIENTS .'.l_name';
        $condition_query= "WHERE service_id LIKE '%,". Clients::FIREQUERY .",%'";
        Clients::getList($db,$lst_client,$fields,$condition_query);
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
               
              /*  if(!empty($data['client_id'])){        
                    $lst_account=null;
                    $fields = TABLE_SMS_CLIENT_ACCOUNT .'.id'
                               .','. TABLE_SMS_CLIENT_ACCOUNT .'.name';
                    $condition_query= "WHERE client_id='".$data['client_id']."'";
                    SmsClientAccount::getList($db,$lst_account,$fields,$condition_query);        
                }
                */
                if ( FqAllotedClient::validateAdd($data, $extra) ) { 
                
                    $query	= " INSERT INTO ".TABLE_FQ_ALLOTED_CLIENT
                            ." SET ".TABLE_FQ_ALLOTED_CLIENT .".client_id 			= '". $data['client_id'] ."'"  
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".allotted_fq 	= '". $data['allotted_fq'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".balance_fq 	= '". $data['allotted_fq'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".rate 	        = '". $data['rate'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".amount 		= '". $data['amount'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".created_by 	= '". $my['user_id'] ."'"                    
                                    .",". TABLE_FQ_ALLOTED_CLIENT .".ip 			= '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".do_e           = '". date('Y-m-d H:i:s')."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT .".status         = '". $data['status']."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT .".do_expiry 		= '". $data['do_expiry']."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New firequery entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();   
                    
                   /* $query	= " SELECT api_id, gateway_id  FROM ".TABLE_SMS_CLIENT_ACCOUNT." WHERE id='".$data['client_acc_id']."'";   
                    
                    if ( $db->query($query) ) {
                        if ( $db->nf() > 0 ) {
                            while ($db->next_record()) {
                                $api_id = $db->f('api_id');
                                $gateway_id = $db->f('gateway_id');
                            }
                        }
                    }
                    
                    $query  = " UPDATE ". TABLE_SMS_PURCHASE
                            ." SET ". TABLE_SMS_PURCHASE .".consumed_sms = consumed_fq+'".$data['allotted_fq'] ."'"
                                	.",". TABLE_SMS_PURCHASE .".balance_sms = balance_fq-'". $data['allotted_fq'] ."'"                    
                            		." WHERE api_id='".$api_id."' AND gateway_id='".$gateway_id."'";
                    
                    $db->query($query) ;*/
                    
                     $query	= " INSERT INTO ".TABLE_FQ_ALLOTED_CLIENT_HISTORY
                            ." SET ".TABLE_FQ_ALLOTED_CLIENT_HISTORY .".alloted_client_id 		= '". $variables['hid'] ."'"  
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".client_id 		    = '". $data['client_id'] ."'"                                
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".no_of_fq 	        = '". $data['allotted_fq'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".balance_fq 	    = '". $data['allotted_fq'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".rate 	            = '". $data['rate'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".amount 			= '". $data['amount'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".created_by 		= '". $my['user_id'] ."'"                    
									//.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".status 			= '". $data['status'] ."'"                    
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".ip 			    = '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".do_e               = '". date('Y-m-d H:i:s')."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".do_expiry          = '". $data['do_expiry']."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".status             = '". $data['status']."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".ip_h 			    = '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".do_he                = '". date('Y-m-d H:i:s')."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".history_created_by   = '". $my['user_id'] ."'";   
                    $db->query($query);                 
                    
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/fq-alloted-client.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/fq-alloted-client.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/fq-alloted-client.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');    
            $page["var"][] = array('variable' => 'lst_client', 'value' => 'lst_client');                 
            //$page["var"][] = array('variable' => 'lst_account', 'value' => 'lst_account');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'fq-alloted-client-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
