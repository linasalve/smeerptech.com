<?php

    if ( $perm->has('nc_v_st_as_details') ) {
        
        $ticket_id	= isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
	    
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        $stTemplates=null;
        $condition_query_st = "WHERE status='".VendorsSupportTicketTemplate::ACTIVE."'";
        VendorsSupportTicketTemplate::getDetails( $db, $stTemplates, 'id,title', $condition_query_st);
        //BOF read the available departments
        //SupportTicket::getDepartments($db,$department);
        //EOF read the available departments
        
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["rating"] = VendorsSupportTicket::getRating();
        $variables["status"] = VendorsSupportTicket::getTicketStatusList();
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE     ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
            
            if ( VendorsSupportTicket::validateAdd($data, $extra) ) {
                
                //$ticket_no  =  "PT". date("ymdhi-s");
                $ticket_no  =  VendorsSupportTicket::getNewNumber($db);
                $attachfilename=$ticket_attachment_path='';
                
                if(!empty($data['ticket_attachment'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['ticket_attachment']["name"]);      
                    
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;     
                    
                    $attachfilename = $ticket_no.".".$ext ;
                    $data['ticket_attachment'] = $attachfilename;         
                    
                    $ticket_attachment_path = DIR_WS_ST_FILES;                    
                    
                    if (move_uploaded_file ($files['ticket_attachment']['tmp_name'], DIR_FS_ST_FILES."/".$attachfilename)){
                        
                    }
                }
                
                $query = "SELECT "	. TABLE_VENDORS_ST_TICKETS .".ticket_date, ".TABLE_VENDORS_ST_TICKETS.".status"
								." FROM ". TABLE_VENDORS_ST_TICKETS 
								." WHERE ". TABLE_VENDORS_ST_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
								." AND ("
											. TABLE_VENDORS_ST_TICKETS .".ticket_child = '". $ticket_id ."' " 
											." OR "
											. TABLE_VENDORS_ST_TICKETS .".ticket_id = '". $ticket_id ."' " 
										.") "
								." ORDER BY ". TABLE_VENDORS_ST_TICKETS .".ticket_date DESC LIMIT 0,1";				
				
				$db->query($query) ;
				if($db->nf() > 0){
					$db->next_record() ;
					$last_time = $db->f("ticket_date") ;
					$ticket_response_time = time() - $last_time ;
                    $status = $db->f("status") ;
				}
				else{
					$ticket_response_time = 0 ;
				}
				$ticket_date 	= time() ;
				$ticket_replied = '0' ;
                
                $mail_to_all_su=0;
                if(isset($data['mail_to_all_su']) ){
                    $mail_to_all_su=1;
                }
                $mail_to_additional_email=0;
                if(isset($data['mail_to_additional_email']) ){
                    $mail_to_additional_email=1;
                }
                $mail_client=0;
                if(isset($data['mail_client']) ){
                    $mail_client=1;
                }
               if(!isset($data['ticket_status']) ){
                    
                    $ticket_status = VendorsSupportTicket::PENDINGWITHCLIENTS;
                }else{
                    $ticket_status = $data['ticket_status'];
                }
                
				$query = "INSERT INTO "	. TABLE_VENDORS_ST_TICKETS ." SET "
										. TABLE_VENDORS_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
										//. TABLE_VENDORS_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
										//. TABLE_VENDORS_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
										//. TABLE_VENDORS_ST_TICKETS .".ticket_status     = '".$data['ticket_status']."', "
                                        . TABLE_VENDORS_ST_TICKETS .".ticket_status     = '".$ticket_status."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_child      = '".$ticket_id."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_replied    = '".$ticket_replied."', "
                                        . TABLE_VENDORS_ST_TICKETS .".from_admin_panel  = '".VendorsSupportTicket::ADMIN_PANEL."',"
                                        . TABLE_VENDORS_ST_TICKETS .".mail_client    = '".$mail_client."', "
                                        . TABLE_VENDORS_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
                                        . TABLE_VENDORS_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
                                        . TABLE_VENDORS_ST_TICKETS .".domain_name       = '". $data['domain_name'] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".is_private        = '". $data['is_private'] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
                                        . TABLE_VENDORS_ST_TICKETS .".do_e              = '".date('Y-m-d H:i:s', time())."', "
                                        . TABLE_VENDORS_ST_TICKETS .".do_comment        = '".date('Y-m-d H:i:s', time())."', "
                                        . TABLE_VENDORS_ST_TICKETS .".to_email           = '". $data['to'] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".cc_email           = '". $data['cc'] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".bcc_email          = '". $data['bcc'] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".to_email_1         = '". $data['to_email_1'] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".cc_email_1         = '". $data['cc_email_1'] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".bcc_email_1        = '". $data['bcc_email_1'] ."', "
										. TABLE_VENDORS_ST_TICKETS .".ticket_date       = '". $ticket_date ."' " ;
				
                if ($db->query($query) && $db->affected_rows() > 0) {
                    
                    $variables['hid'] = $ticket_child  = $db->last_inserted_id() ;
                    $sql = '';
                    $sqlstatus ='';
                    if($status == VendorsSupportTicket::DEACTIVE){
                        $sqlstatus = ",".TABLE_VENDORS_ST_TICKETS.".status='".VendorsSupportTicket::ACTIVE."'";
                    }
                    //Mark pending towards client bof
                    $query_update = "UPDATE ". TABLE_VENDORS_ST_TICKETS 
								." SET ". TABLE_VENDORS_ST_TICKETS .".ticket_status = '".$ticket_status."'"                               
                                    .",".TABLE_VENDORS_ST_TICKETS.".do_comment= '".date('Y-m-d H:i:s', time())."'"
                                    .",".TABLE_VENDORS_ST_TICKETS.".last_comment_from= '".VendorsSupportTicket::ADMIN_COMMENT."'"
                                    .",".TABLE_VENDORS_ST_TICKETS.".last_comment= '".$data['ticket_text']."'"
                                    .$sqlstatus
                                    ." WHERE ". TABLE_VENDORS_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				    $db->query($query_update) ;
                    //Mark pending towards client eof
                    /**************************************************************************
                    * Send the notification to the ticket owner and the Administrators
                    * of the new Ticket being created
                    **************************************************************************/
                    
                    $data['ticket_no']    =   $ticket_no ;
                    $data['mticket_no']   =   $data['mticket_no'] ;
                    /*foreach( $status as $key=>$val){                           
                        if ($data['ticket_status'] == $val['status_id'])
                        {
                            $data['status']=$val['status_name'];
                        }
                    }*/
                     
                    $data['replied_by']   =   $my['f_name']." ". $my['l_name'] ;
                    //$data['subject']    =   $data['ticket_subject'] ;
                    //$data['text']    =   $data['ticket_text'] ;
					$data['subject']    =     $_ALL_POST['ticket_subject'] ;
					$data['text']    =   $_ALL_POST['ticket_text'] ;
                    $data['attachment']   =   $attachfilename ;
                    $data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $ticket_id;
                    $file_name = '';
                    if(!empty($attachfilename)){
                        $file_name = DIR_FS_ST_FILES ."/". $attachfilename;
                    }
                    $main_client_name = '';
                    $billing_name = $mail_send_to_su='';
                     // Send Email to the ticket owner BOF  
                    if ($mail_client == 1 ){     
                        // Send Email to the ticket owner BOF  
                        Vendors::getList($db, $userDetails, 'number, f_name, l_name, email,email_1,email_2,email_3,email_4, 
                        additional_email, title as ctitle, billing_name
                        ', " WHERE user_id = '".$data['ticket_owner_uid']."'");
                        if(!empty($userDetails)){
                            $userDetails=$userDetails['0'];
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    =   $userDetails['ctitle'];
                            $main_client_name    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $billing_name    =   $userDetails['billing_name'];
                            
                            $email = $cc_to_sender = NULL;                        
                            if(!empty($data['cc'])){
                                $cc_email = explode(",",$data['cc']);
                                foreach ($cc_email  as $key=>$value){
                                    $cc[]   = array('name' => "", 'email' => $value);
                                }
                            }else{
                                $cc = Null;
                            }
                            
                            if(!empty($data['bcc'])){
                                $bcc_email = explode(",",$data['bcc']);
                                foreach ($bcc_email  as $key=>$value){
                                    $bcc[]   = array('name' => "", 'email' => $value);
                                }
                            }else{
                                $bcc = Null;
                            }
                            if(!empty($data['to'])){
                                $to_email = explode(",",$data['to']);
                                foreach ($to_email  as $key=>$value){
                                    $to[]   = array('name' => "", 'email' => $value);
                                }
                            }else{
                                $to = '';
                            }
                            if ($data['is_private'] == 1){
                                if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_ST_CONFIDENTIAL', $data, $email) ) {
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc ='';// as confidential
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' => $userDetails['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su .=",".$userDetails['email'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    /*print_r($to);
                                    print_r($from);
                                    echo $email["subject"];
                                    echo $email["body"];*/
                                    
                                }
                                //Send mail on alternate emails ids bof
                                if(!empty($userDetails['email_1'])){                                       
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
                                    $userDetails['email_1']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email_1'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                    $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                }
                                if(!empty($userDetails['email_2'])){                                       
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
                                    $userDetails['email_2']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email_2'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                    $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                }
                                if(!empty($userDetails['email_3'])){                                       
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
                                    $userDetails['email_3']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email_3'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                    $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                }
                                if(!empty($userDetails['email_4'])){                                       
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
                                    $userDetails['email_4']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email_4'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                    $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                }
                                //Send mail on alternate emails ids eof                            
                            }else{
                                if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_ST_MEMBER', $data, $email) ) {
                                    
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' => $userDetails['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    /*print_r($to);
                                    print_r($from);
                                    echo $email["subject"];
                                    echo $email["body"];*/
                                    
                                }
                                //Send mail on alternate emails ids bof
                                if(!empty($userDetails['email_1'])){                                       
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
                                    $userDetails['email_1']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email_1'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                    $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                }
                                if(!empty($userDetails['email_2'])){                                       
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
                                    $userDetails['email_2']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email_2'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                    $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                }
                                if(!empty($userDetails['email_3'])){                                       
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
                                    $userDetails['email_3']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email_3'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                    $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                }
                                if(!empty($userDetails['email_4'])){                                       
                                    $to = '';
                                    $cc_to_sender=$cc=$bcc='';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
                                    $userDetails['email_4']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email_4'];
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                    $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                }
                                                          
                            }
                            //Send a copy to check the mail for clients
                            if(!empty($smeerp_client_email)){
                                $to     = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 'email' => $smeerp_client_email);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                            }                            
                        }
                        // Send Email to the ticket owner EOF
                    }
                     // Send Email to all the subuser BOF
                     //Send mail on alternate emails ids bof
                    if($mail_to_additional_email==1){                        
                         Vendors::getList($db, $userAdditionalDetails, 'number, f_name, l_name, 
                            additional_email,title as ctitle,billing_name', " WHERE user_id = '".$data['ticket_owner_uid']."'");
                            if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_ST_MEMBER', $data, $email) ) {
                                if(!empty($userAdditionalDetails['additional_email'])){
                                $cc_to_sender=$cc=$bcc='';
                                $arrAddEmail = explode(",",$userAdditionalDetails['additional_email']); 
                                foreach($arrAddEmail as $key=>$val ){
                                    if(!empty($val)){
                                        $mail_send_to_su  .= ",".$val;
                                        $to = '';
                                        $to[]   = array('name' =>'', 'email' => $val);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                        $cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                }
                            }
                        }
                    }
                   //Send mail on alternate emails ids eof 
                    if ($data['is_private'] != 1){
                        if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
                            if(isset($_POST['mail_to_all_su'])){
                                Vendors::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND service_id LIKE '%,".Vendors::ST.",%' 
                                AND status='".Vendors::ACTIVE."' AND send_mail='".Vendors::SENDMAILYES."'");
                            }elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
                               $mail_to_su= implode("','",$_POST['mail_to_su']);
                                Vendors::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND user_id IN('".$mail_to_su."')
                                AND status='".Vendors::ACTIVE."'");
                            }
                           
                            if(!empty($subUserDetails)){
                                //$subUserDetails=$subUserDetails['0'];
                                
                                foreach ($subUserDetails  as $key=>$value){
                                    $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                    $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                    $email = NULL;
                                    $cc_to_sender= $cc = $bcc = Null;
                                    if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_ST_MEMBER', $data, $email) ) {
                                        $to = '';
                                        $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                        }
                                        if(!empty($subUserDetails[$key]['email_1'])){                                       
                                            $to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                            '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_1']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                            }
                                        } 
                                        if(!empty($subUserDetails[$key]['email_2'])){                                       
                                            $to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                            '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_2']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                            }
                                        }
                                        if(!empty($subUserDetails[$key]['email_3'])){                                       
                                            $to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                            '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_3']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                            }
                                        }
                                        if(!empty($subUserDetails[$key]['email_4'])){                                       
                                            $to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                            '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_4']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                            }
                                        }
                                    }                      
                                }
                            }
                        }else{
                           if($data['ticket_owner_uid'] != $data['ticket_creator_uid']){
                                Vendors::getList($db, $clientDetails, 'number, f_name, l_name, email,email_1,email_2,email_3,email_4, 
                                title as ctitle, parent_id', " WHERE user_id = '".$data['ticket_creator_uid'] ."' AND send_mail='".Vendors::SENDMAILYES."'");
                                if(!empty($clientDetails)){
                                    $clientDetails=$clientDetails['0'];
                                    
                                    if(!empty($clientDetails['parent_id'])){
                                        $data['client_name']    =   $clientDetails['f_name']." ".$clientDetails['l_name'];
                                        $data['ctitle']    =   $clientDetails['ctitle'];
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_ST_MEMBER', $data, $email) ) {
                                            $to = '';
                                            $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' => $clientDetails['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$clientDetails['email'];
                                            }
                                            //Send mail on alternate emails ids bof
                                            if(!empty($clientDetails['email_1'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_1']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_1'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_2'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_2']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_2'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_3'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_3']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_3'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_4'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $userDetails['email_4']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_4'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            //Send mail on alternate emails ids eof
                                        }                                        
                                    }
                                }
                            }
                        }
                    }                  
                    // Send Email to all the subuser EOF
                    
                    //Set the list subuser's email-id to whom the mails are sent bof
                    $query_update = "UPDATE ". TABLE_VENDORS_ST_TICKETS 
								." SET ". TABLE_VENDORS_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."'"                               
                                    ." WHERE ". TABLE_VENDORS_ST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
				    $db->query($query_update) ;
                    //Set the list subuser's email-id to whom the mails are sent eof
                    
                    // Send Email to the admin BOF
                    $email = NULL;
                    $data['link']   = DIR_WS_NC .'/vendors-support-ticket.php?perform=view&ticket_id='. $ticket_id;
                    $data['main_client_name']   = $main_client_name;
                    $data['billing_name']   = $billing_name;
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'VENDORS_EMAIL_ST_ADMIN', $data, $email) ) {
                        $to = '';
                        $to[]   = array('name' => $support_name , 'email' => $support_email);                       
                        /*print_r($to);
                        print_r($from);
                        echo $email["body"];
                        echo $email["subject"];
                        exit;*/
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                    }
                    // Send Email to the admin EOF     
                    //$messages->setOkMessage("New Support Ticket has been created.And email Notification has been sent.");
                    
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
                header("Location:".DIR_WS_NC."/vendors-support-ticket-assign.php?perform=view&added=1&ticket_id=".$ticket_id);
            }
        }
        
        if( isset($_POST["btnUpdate"]) && $_POST["btnUpdate"] == "Close Ticket"){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            if(empty($data['ticket_rating'])){
                $messages->setErrorMessage("Please rate the service.");
            }else{
                $query = "UPDATE ". TABLE_VENDORS_ST_TICKETS 
                                ." SET "//. TABLE_VENDORS_ST_TICKETS .".ticket_priority = '". $data["ticket_priority"] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".ticket_status = '". VendorsSupportTicket::CLOSED ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".ticket_rating = '". $data["ticket_rating"] ."', "
                                        . TABLE_VENDORS_ST_TICKETS .".ticket_replied = '0' "
                                ." WHERE ". TABLE_VENDORS_ST_TICKETS .".ticket_id = '". $ticket_id ."' " 
                                ." AND ". TABLE_VENDORS_ST_TICKETS .".ticket_owner_uid = '". $data["ticket_owner_uid"] ."' ";
                if($db->query($query)){
                    $messages->setOkMessage("Ticket Status Updated Successfully.");
                }
            }
		}
        
        /*if( isset($_POST["btnActive"])){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
           
            $query = "UPDATE ". TABLE_VENDORS_ST_TICKETS 
                            ." SET ". TABLE_VENDORS_ST_TICKETS .".status = '". SupportTicket::ACTIVE ."'"
                            ." WHERE ". TABLE_VENDORS_ST_TICKETS .".ticket_id = '". $data['thread_ticket_id'] ."' " ;
            if($db->query($query)){
                $messages->setOkMessage("Ticket activated Updated Successfully.");
            }          
		}
        
        if( isset($_POST["btnDeActive"])){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $query = "UPDATE ". TABLE_VENDORS_ST_TICKETS 
                            ." SET ". TABLE_VENDORS_ST_TICKETS .".status = '". SupportTicket::DEACTIVE ."'"
                            ." WHERE ". TABLE_VENDORS_ST_TICKETS .".ticket_id = '". $data['thread_ticket_id'] ."' " ;
            if($db->query($query)){
                $messages->setOkMessage("Ticket de-activated Updated Successfully.");
            }            
		}*/
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/vendors-support-ticket-assign-list.php');
        }
        else {
            $condition_query= " WHERE ticket_id = '". $ticket_id ."' ";
            $_ALL_POST      = NULL;
            
            if ( VendorsSupportTicket::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST[0];
                $mticket_no = $_ALL_POST['ticket_no'];
            }
            $ticketStatusArr = VendorsSupportTicket::getTicketStatus();
            $ticketStatusArr = array_flip($ticketStatusArr);
            $_ALL_POST['ticket_status_name'] = $ticketStatusArr[$_ALL_POST['ticket_status']];  
            /*
            $table = TABLE_SETTINGS_DEPARTMENT;
            $condition2 = " WHERE ".TABLE_SETTINGS_DEPARTMENT .".id= '".$_ALL_POST['ticket_department'] ."' " ;
            $fields1 =  TABLE_SETTINGS_DEPARTMENT .'.department_name';
            $ticket_department= getRecord($table,$fields1,$condition2);            
            $_ALL_POST['department_name']    = $ticket_department['department_name'] ;
            */
            
            $condition_query= " WHERE user_id = '". $_ALL_POST['ticket_owner_uid'] ."' ";
            $member      = NULL;
            if ( Vendors::getList($db, $member, 'f_name,l_name,billing_name,additional_email,mobile1,mobile2,send_mail', $condition_query) > 0 ) {
                $_ALL_POST['client'] = $member[0];
                $_ALL_POST['client']['name'] = $member[0]['f_name']." ".$member[0]['l_name'];
            }
            $_ALL_POST['mail_to_all_su']=1;            
            $subUserDetails=null;
            Vendors::getList($db, $subUserDetails, 'number,user_id, f_name, l_name,title as ctitle,send_mail,email,email_1,email_2, email_3,email_4', " 
                        WHERE parent_id = '".$_ALL_POST['ticket_owner_uid']."' AND service_id LIKE '%,".Vendors::ST.",%' 
                        AND status='".Vendors::ACTIVE."' ");
            
     	    $query = "SELECT "	. TABLE_VENDORS_ST_TICKETS .".ticket_no, "
                                . TABLE_VENDORS_ST_TICKETS .".ticket_id, "
                                . TABLE_VENDORS_ST_TICKETS .".to_email_1, "
								. TABLE_VENDORS_ST_TICKETS .".cc_email_1, "
                                . TABLE_VENDORS_ST_TICKETS .".on_behalf, "
								. TABLE_VENDORS_ST_TICKETS .".bcc_email_1, "
                                . TABLE_VENDORS_ST_TICKETS .".from_email, "
								. TABLE_VENDORS_ST_TICKETS .".ticket_subject, "
								. TABLE_VENDORS_ST_TICKETS .".ticket_text, "
								. TABLE_VENDORS_ST_TICKETS .".ticket_creator, "
								. TABLE_VENDORS_ST_TICKETS .".ticket_creator_uid, "
								. TABLE_VENDORS_ST_TICKETS .".ticket_attachment, "
								. TABLE_VENDORS_ST_TICKETS .".ticket_attachment_path, "
								. TABLE_VENDORS_ST_TICKETS .".ticket_response_time, "
								. TABLE_VENDORS_ST_TICKETS .".ticket_date, "
                                . TABLE_VENDORS_ST_TICKETS .".status, "
                                . TABLE_VENDORS .".mobile1, "
                                . TABLE_VENDORS .".mobile2 "
							." FROM ". TABLE_VENDORS_ST_TICKETS ." LEFT JOIN ".TABLE_VENDORS." ON ".TABLE_VENDORS.".user_id=".TABLE_VENDORS_ST_TICKETS.".ticket_creator_uid
                            WHERE ". TABLE_VENDORS_ST_TICKETS .".ticket_owner_uid = '". $_ALL_POST['ticket_owner_uid'] ."' " 
							." AND ". TABLE_VENDORS_ST_TICKETS .".ticket_child = '". $ticket_id ."' " 
							." AND ". TABLE_VENDORS_ST_TICKETS .".status = '". VendorsSupportTicket::ACTIVE ."' " 
							." ORDER BY ". TABLE_VENDORS_ST_TICKETS .".ticket_date ASC";
                            
			$db->query($query);
			$ticketThreads = array();
			while($db->next_record()){
				$response["h"] = round($db->f("ticket_response_time") / 3600) ;
				$response["m"] = round(($db->f("ticket_response_time") % 3600) / 60 ) ;
				$response["s"] = round(($db->f("ticket_response_time") % 3600) % 60 ) ;
                
                $db_new = new db_local();
                $sql_phone = "SELECT ". TABLE_VENDORS_PHONE .".ac, "
								. TABLE_VENDORS_PHONE .".cc, "
								. TABLE_VENDORS_PHONE .".p_number "
							." FROM ". TABLE_VENDORS_PHONE.
                            " WHERE ". TABLE_VENDORS_PHONE .".phone_of = '".$db->f("ticket_creator_uid")."' " 
							." AND ". TABLE_VENDORS_PHONE .".p_is_preferred = '1' " 
							." AND ". TABLE_VENDORS_PHONE .".table_name='".TABLE_VENDORS."'";
                $db_new->query($sql_phone);        
                $db_new->next_record();
				
				$ticketThreads[] = array(	"ticket_no" 			=> $db->f("ticket_no"), 
                                            "ticket_id" 		=> $db->f("ticket_id"), 
											"ticket_subject" 		=> $db->f("ticket_subject"), 
											"on_behalf" 		=> $db->f("on_behalf"), 
											"ticket_text" 			=> preg_replace("(\r\n|\r|\n)", "<br/>", $db->f("ticket_text")), 
											"ticket_creator" 		=> $db->f("ticket_creator"), 
											"ticket_attachment" 	=> $db->f("ticket_attachment"), 
											"ticket_attachment_path"=> $db->f("ticket_attachment_path"),
											"ticket_response_time" 	=> $response["h"] ." hr(s) : "
																		. $response["m"] ." min(s) :" 
																		. $response["s"]." sec(s) ",
											"ticket_date" 			=> $db->f("ticket_date"),
                                            "mobile1" 			=> $db->f("mobile1"),
											"mobile2" 			=> $db->f("mobile2"),
                                            "cc" 			=> $db_new->f("cc"),
											"ac" 			=> $db_new->f("ac"),
											"p_number" 		=> $db_new->f("p_number"),
                                            "to_email_1" 	=> $db->f("to_email_1"),
											"cc_email_1" 	=> $db->f("cc_email_1"),
											"bcc_email_1" 	=> $db->f("bcc_email_1"),
                                            "from_email" 	=> $db->f("from_email"),
											"status" 	=> $db->f("status")
										);
			}		
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $variables['client_sendmail'] = Vendors::SENDMAILNO;
            $variables['ticket_closed'] = VendorsSupportTicket::CLOSED;
            
            $variables['can_add_cmt_after_closed'] = false;
            $variables['can_update_status'] = false;
            $variables['can_view_client_details']  = false;
            $variables['can_view_replay_email']  = false;
            
            if ( $perm->has('nc_uv_contact_details') ) {           
                $variables['can_view_client_details'] = true;
            }
            if ( $perm->has('nc_v_st_add_cmt_after_closed') ) {
                $variables['can_add_cmt_after_closed'] = true;
            }
            if ( $perm->has('nc_v_st_update_status') ) {
                $variables['can_update_status'] = true;
            }
            if ( $perm->has('nc_v_st_replay_email') ) {
                $variables['can_view_replay_email'] = true;
            }
            
            $hidden[] = array('name'=> 'perform','value' => 'view');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'ticket_id', 'value' => $ticket_id);
            $hidden[] = array('name'=> 'mticket_no', 'value' => $mticket_no);
           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'ticket_id', 'value' => 'ticket_id');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
            $page["var"][] = array('variable' => 'ticketThreads', 'value' => 'ticketThreads');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-support-ticket-assign-view.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>