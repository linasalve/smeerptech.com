<?php
	ob_start();
	/**
	 * Short description for file
	 * This file manages the banner
	 * Module for the jobsapplication
	 *
	 * PHP versions All
	 *
	 * @category   Application File (Banner)
	 * @package    justjobs.in
	 * @author     
	 * @copyright  SMEERP Technologies
	 * @license    As described below
	 * @version    1.2.0
	 * @link       
	 * @see        -NA-
	 * @since      File available since Release 1.2.0 dt. Tuesday, 07 June, 2005
	 * @deprecated -NA-
	 */

	/*********************************************************
	* Licence:
	* This file is sole property of the installer.
	* Any type of copy or reproduction without the consent
	* of owner is prohibited.
	* If in any case used leave this part intact without 
	* any modification.
	* All Rights Reserved
	* Copyright 2006 Owner
	*******************************************************/
	
	if(!defined( "THIS_DOMAIN"))
	{
		require("./lib/config.php");
	}
	
	page_open(array(
							"sess"=>"local_Session",
							"auth"=>"local_Auth"
							));
	/*******************************
	**	include header file				
	*******************************/
	include_once(THIS_PATH."/jobsapplication/header.php");
	
	/**************************
	**	Selected menu
	**************************/
	$is_selected = "console-configuration.php"; // Selected value for top menu
	
	/*******************************
	**	Selected submenu
	******************************/
	$is_sub_selected = "search-settings.php"; // Selected value for top submenu	
	
	/*************************
	**	Input variables
	*************************/
	$a					= ($_GET["a"]) ? $_GET["a"] : $_POST["a"]; // For ascending/descending
			
	$x					= ($_GET["x"]) ? ($_GET["x"]) : $_POST["x"]; // page value for pagination
	$id					= ($_GET["id"]) ? ($_GET["id"]) : $_POST["id"]; // page value for pagination
	
	$key				= ($_GET["key"]) ? ($_GET["key"]) : $_POST["key"]; // Key to decide the title
	
	$perPage			= RESULTS_PER_PAGE; // Total page for pagination
	
	/*******************************
	**	Ascending/Descending order
	*******************************/
	if( $o =="d" )
	{
		$o="DESC";
	}
	else if( $o =="a" )
	{
		$o="ASC";
	}
	
	/**********************************
	* Handle add search settings
	***********************************/
	if($_POST["flag"] == "Add") 
	{
		$collection 	= 	processData($_POST);
		$collect_data 	= 	processData($_POST);
		$error="";
		
			/*if(empty($collect_data["jobcategory_title"]))
			{
			  $messages->setErrorMessage($page_variables["JOBCATEGORY_LIST_ADDED_TITLE_ERROR"]);
			}*/
			
			/*$query = "SELECT "
						. TABLE_SEARCH_SETTINGS .".* "
					." FROM ". TABLE_SEARCH_SETTINGS;
						
			$db->query( $query );
			if( $db->nf()  > 0 )
			{
				//$error["exist"] = 1;
				$messages->setErrorMessage($page_variables["JOBCATEGORY_LIST_ADDED_TITLE_ALREADY_EXIST"]);
			}		*/
			if ($messages->getErrorMessageCount() <= 0) 
			{
				$query = "INSERT INTO "
							. TABLE_SEARCH_SETTINGS 
						." SET "
							. TABLE_SEARCH_SETTINGS .".user_id 							= '". $collect_data["user_id"] . "', "
							. TABLE_SEARCH_SETTINGS .".login_type 							= '". $collect_data["login_type"] . "', "
							. TABLE_SEARCH_SETTINGS .".sh_comp_to_candidate 				= '". $collect_data["sh_comp_to_candidate"] . "', "
							. TABLE_SEARCH_SETTINGS .".sh_comp_contact_to_candidate 		= '". $collect_data["sh_comp_contact_to_candidate"] . "', "
							. TABLE_SEARCH_SETTINGS .".sh_candidate_to_comp 				= '". $collect_data["sh_candidate_to_comp"] . "', "
							. TABLE_SEARCH_SETTINGS .".sh_candidate_contact_to_comp 	= '". $collect_data["sh_candidate_contact_to_comp"] ."' ";							
														
				$db->query($query);				
				$collection 		= array();
				$variables["edit"] 	= 0;				
				$messages->setOkMessage($page_variables["JOBCATEGORY_LIST_ADDED_SUCCESSFULLY"]);
			}
			else
			{
				$variables["pos"] = 0;
			}
	}
	
	/*****************************************
	**	 Handle more job categories add edit
	******************************************/
	if($_POST["flag"] == "Edit")
	{
	     
		$collection 	= 	processData($_POST);
		$collect_data 	= 	processData($_POST);
		$error="";
		/* if(empty($collect_data["jobcategory_title"]))
		{
			$messages->setErrorMessage($page_variables["JOBCATEGORY_LIST_ADDED_TITLE_ERROR"]);
		}*/
        
		if ($messages->getErrorMessageCount() <= 0) 
		{
            $sh_comp_to_candidate=0;
            if(isset($collect_data['sh_comp_to_candidate']) ){
                $sh_comp_to_candidate=1;
            }
            
            $sh_comp_contact_to_candidate=0;
            if(isset($collect_data['sh_comp_contact_to_candidate']) ){
                $sh_comp_contact_to_candidate=1;
            }
            
            $sh_candidate_to_comp=0;
            if(isset($collect_data['sh_candidate_to_comp']) ){
                $sh_comp_to_candidate=1;
            }
            
            $sh_candidate_contact_to_comp=0;
            if(isset($collect_data['sh_candidate_contact_to_comp']) ){
                $sh_candidate_contact_to_comp=1;
            }
            
			$query = " UPDATE "
					. TABLE_SEARCH_SETTINGS 
				." SET "
					. TABLE_SEARCH_SETTINGS .".sh_comp_to_candidate 			= '". $sh_comp_to_candidate . "', "
					. TABLE_SEARCH_SETTINGS .".sh_comp_contact_to_candidate	 = '". $sh_comp_contact_to_candidate . "', "
					. TABLE_SEARCH_SETTINGS .".sh_candidate_to_comp			 = '". $sh_candidate_to_comp. "', "
					. TABLE_SEARCH_SETTINGS .".sh_candidate_contact_to_comp = '". $sh_candidate_contact_to_comp . "' "
				." WHERE "
					. TABLE_SEARCH_SETTINGS .".id = '". $collect_data["id"] ."' ";

			$db->query($query);
			$collection = array();
			$variables["edit"] = 0;
			$messages->setOkMessage($page_variables["JOBCATEGORY_LIST_EDITED_SUCCESSFULLY"]);					
		}
		else
		{
			 $variables["edit"] = 1;
		}
	}
	
	/**********************************
	* To show data while edititing
	***********************************/
	if($a=="e" and $id  > 0)
	{
		$db->query(	
			"SELECT "
				. TABLE_SEARCH_SETTINGS .".* ".",".TABLE_AUTH.".firstname".",".TABLE_AUTH.".lastname
			 FROM ".TABLE_SEARCH_SETTINGS." LEFT JOIN ".TABLE_AUTH." ON ".TABLE_AUTH.".uid= ".TABLE_SEARCH_SETTINGS.".user_id 
			 WHERE "
				. TABLE_SEARCH_SETTINGS .".id ='".$id."'
				
			");
					
			$db->next_record();
			$collection = clearData(array(			
				"id"		=>	$db->f("id"),
				"user_id"		=>	$db->f("user_id"),
				"user_name"		=>	$db->f("firstname")." ".$db->f("lastname"),
				"login_type"		=>	$db->f("login_type"),
				"sh_comp_to_candidate"	=>	$db->f("sh_comp_to_candidate"),
				"sh_comp_contact_to_candidate"	=>	$db->f("sh_comp_contact_to_candidate"),
				"sh_candidate_to_comp"	=>	$db->f("sh_candidate_to_comp"),
				"sh_candidate_contact_to_comp"	=>	$db->f("sh_candidate_contact_to_comp")
			));
		/**************************************
		**	Assign the edited value to display
		**************************************/
		$s->assign("_ALL_POST",$collection);		
        
		$variables["edit"] = 1;
	}
	
	/******************************
	* To handle deletion of record
	*******************************/
	if ($a=="d")  {	
		$db->query(
				"DELETE FROM "
					. TABLE_SEARCH_SETTINGS 
				." WHERE "
					. TABLE_SEARCH_SETTINGS .".id = '". $id ."'
		");
		$messages->setOkMessage($page_variables["JOBCATEGORY_LIST_DELETED_SUCCESSFULLY"]);		
	}
	
	/*************************************************************
	* To handle page value and total page number for pagination
	**************************************************************/
	if (empty($x))
	{
		$from=0;
	}
	else
	{
		$from = ($x-1) * $perPage;
	}
	
	/****************************************************
	**	To fetch and display the result of searching 
	*****************************************************/
	$query="SELECT count( * ) FROM "
				. TABLE_SEARCH_SETTINGS 
				." ORDER BY "
				. TABLE_SEARCH_SETTINGS .".id ";
				
	$db->query($query);
	$db->next_record();
	
	$total=$db->f(0); // Get total reciords*/
	
	$query	=	"SELECT "
					. TABLE_SEARCH_SETTINGS .".* ".",".TABLE_AUTH.".firstname".",".TABLE_AUTH.".lastname FROM "
					.TABLE_SEARCH_SETTINGS." LEFT JOIN ".TABLE_AUTH." ON ".TABLE_AUTH.".uid= ".TABLE_SEARCH_SETTINGS.".user_id" ;
					
				if($a=="s" AND $key=="title") {
				
					$query.=" ORDER BY id $o";
				  
				}
				
				if($a=="s" and $key=="show") {
				
					$query.=" ORDER BY login_type $o";
				 
				}
	
	/****************************************************
	**	To fetch and display the result of searching 
	*****************************************************/
	if($_POST["flag"]=="search")
	{
		$collection 	= 	processData($_POST);
		$collect_data 	= 	processData($_POST);
		$error="";
		
		if(empty($collect_data["sstring"]))
		{
			$error["sstring"]=1;
		}
		if(empty($collect_data["stype"]))
		{
			$error["stype"]=1;
		}
		if(empty($error))
		{
			$tquery =	"SELECT "
							. TABLE_SEARCH_SETTINGS .".* ".",".TABLE_AUTH.".firstname".",".TABLE_AUTH.".lastname"
						." FROM "
							. TABLE_SEARCH_SETTINGS 
						." LEFT JOIN ".TABLE_AUTH." ON ".TABLE_AUTH.".uid= ".TABLE_SEARCH_SETTINGS.".user_id WHERE "
							. $stype ." LIKE '%". $sstring ."%' ORDER BY login_type ";
							
			$db->query($tquery);
			
			$total=$db->nf();
			
			$sstring	 =	$collect_data["sstring"];
			$stype		 =	$collect_data["stype"];
			$order		 =	$collect_data["order"];
			$query 		.= 	" WHERE $stype LIKE '%$sstring%' "
								." ORDER BY ". $stype ." ". $order;
		}
	}
	
	if(empty($a) and empty($key) and empty($order))
	{
		 $query	.=	" ORDER By id ASC";
	}
		 $query	.=	" LIMIT $from, $perPage";
	
	$db->query($query);
	
	while($db->next_record())  {	
        $name = $db->f("firstname")." ".$db->f("lastname");
		$search_settings[] = array(			    
				"id"						=>	$db->f("id"),
				"name"					=>	$name,
				"login_type"				=>	$db->f("login_type"),
				"sh_comp_to_candidate"	=>	$db->f("sh_comp_to_candidate")
		);
	}
	
	$query_user	=	"SELECT ". TABLE_AUTH .".* "
					." FROM ". TABLE_AUTH
					." WHERE ". TABLE_AUTH .".perms	=	'STAFF' " . " OR " . "perms	=	'EMPLOYER'";
	$db->query($query_user);
	
	while( $db->next_record() )  {	
		$alluser[] =	clearData(array(
			"userid"		=>	$db->f("uid"),
			"username"	=>	$db->f("username"),
			"password"	=>	$db->f("password"),
			"firstname"	=>	$db->f("firstname"),
			"lastname"	=>	$db->f("lastname"),
			"email"		=>	$db->f("email")
		));
	}
	
	$extra	= "a=". $a ."&key=" . $key ."&o=" . $o;
	
	$pagelink=showPagination($total,$x,$perPage,"x",$extra);
	
	/************************************************
	**	Submenu list of console configuration  menu
	************************************************/
	$admin_submenu 	=	array();
	$admin_submenu	=	adminSubmenuList("Console Configuration");
	/*******************************
	* Assigning variables to Smarty	
	********************************/
	$s->assign("admin_submenu", $admin_submenu);
	$s->assign("is_selected", $is_selected);
	$s->assign("is_sub_selected", $is_sub_selected);		
	$s->assign("error",$error);
	$s->assign("x", $x);
	$s->assign("search_settings",$search_settings);
	$s->assign("pagelink",$pagelink);
	$s->assign("_ALL_POST",$collection);		
	$s->assign("alluser",$alluser);
	$s->assign("error_messages", $messages->getErrorMessages());
	$s->assign("success_messages", $messages->getOkMessages());
    
	
	/**************************
	**	Always assignment
	**************************/
	$s->assign("variables",$variables);
	$s->assign('CONTENT',$s->fetch("search-settings.html"));
	$s->display('index.html');
	
	ob_end_flush();
	
?>
