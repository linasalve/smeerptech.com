<?php
    if ( $perm->has('nc_sms_ca_status') ) {
	$ca_id	= isset($_GET["ca_id"]) 	? $_GET["ca_id"] 	: ( isset($_POST["ca_id"]) 	? $_POST["ca_id"] : '' );
	$status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
	
	$extra = array( 'db' 		=> &$db,
					'messages' 	=> $messages
				);
	ClientAccount::updateStatus($ca_id, $status, $extra);
	
	// Display the Webpage list.
	include ( DIR_FS_NC .'/client-account-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to change the status.");
    }
?>