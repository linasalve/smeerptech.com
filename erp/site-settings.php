<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
	page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
	include_once ( DIR_FS_NC ."/header.php" );
	include_once ( DIR_FS_INCLUDES .'/site-settings.inc.php');
	include_once ( DIR_FS_INCLUDES .'/user.inc.php');
	
	$sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
	$sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	$rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
	$added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
	$condition_url = $condition_query = $action='';
    
	if(empty($rpp)){
		$rpp = RESULTS_PER_PAGE;
	}
	
	$condition_query ='';
   
	if ( empty($x) ) {
		$x              = 1;
		$next_record    = 0 ;
	}else{
		$next_record    = ($x-1) * $rpp;
	}
	$variables["x"]     = $x;
	$variables["rpp"]   = $rpp;
    
	if($added){
		$messages->setOkMessage("Record has been added  successfully.");
	}
   
	if ( $perm->has('nc_st_settings') ) {
       
        //use switch case here to perform action. 
        switch ($perform) {
		 
			case ('add'):
			default: {
					
				$action='Add';
				 include (DIR_FS_NC.'/site-settings-add.php');
					
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'site-settings.html');
				$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				break;
			}
        }
        
	}
	else {
        //$messages->setErrorMessage("You donot have the Right to Access this Module.");
        
         $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'site-settings.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
	}
    
	// always assign
	$s->assign("variables", $variables);
	$s->assign("action", $action);
	$s->assign("error_messages", $messages->getErrorMessages());
	$s->assign("success_messages", $messages->getOkMessages());

	if ( isset($page["var"]) && is_array($page["var"]) ) {
		foreach ( $page["var"] as $key=>$fetch ) {
			$s->assign( $fetch["variable"], ${$fetch["value"]});
		}
	}
	if ( isset($page["section"]) && is_array($page["section"]) ) {
		$sections = count($page["section"]);
		for ( $i=0; $i<($sections-1); $i++) {
			$s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
		}
	}
	$s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
