<?php

    if ( $perm->has('nc_cst_details') || $perm->has('nc_cst_flw_inv') ) {
		include_once ( DIR_FS_INCLUDES .'/st-template-category.inc.php');
		
    $ticket_id= isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
    $main_client_name	= isset($_GET["main_client_name"]) ? $_GET["main_client_name"] : 	
	( isset($_POST["main_client_name"]) ? $_POST["main_client_name"] : '' );
    $billing_name	= isset($_GET["billing_name"]) ? $_GET["billing_name"] : ( isset($_POST["billing_name"]) ? 
	$_POST["billing_name"] : '' );
	$client_inv = isset ($_GET['client_inv']) ? $_GET['client_inv'] : ( isset($_POST['client_inv'] ) ? $_POST['client_inv'] : '');
	$invoice_id = isset ($_GET['invoice_id']) ? $_GET['invoice_id'] : ( isset($_POST['invoice_id'] ) ?	$_POST['invoice_id'] : '');
	$invoice_profm_id = isset ($_GET['invoice_profm_id']) ? $_GET['invoice_profm_id'] : 
		(isset($_POST['invoice_profm_id'] ) ?	$_POST['invoice_profm_id'] : '');	
    $flw_ord_id = isset ($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : ( isset($_POST['flw_ord_id'] ) ?	$_POST['flw_ord_id'] : '');
	$flw_tds = isset ($_GET['flw_tds']) ? $_GET['flw_tds'] : ( isset($_POST['flw_tds'] ) ?	$_POST['flw_tds'] : 0);
	$template_category = isset ($_GET['template_category']) ? $_GET['template_category'] : 
			( isset($_POST['template_category'] ) ? $_POST['template_category'] :''); 
		
		$fpendingInvlist = $fpendingPInvlist=$posted_data = $orderArr = array();
		$totalBalanceAmount = $totalBalanceAmountP=0;
	    if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
		
			if(!empty($flw_ord_id)){
				$table = TABLE_BILL_ORDERS;
				$fields1 =  TABLE_BILL_ORDERS .'.details' ;
				$condition1 = " WHERE ".TABLE_BILL_ORDERS .".id =".$flw_ord_id." " ;
				$orderArr = getRecord($table,$fields1,$condition1);
			}
		
			include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
			include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php' );
			$flw_tds_str=' ';			
			/* 
			if($flw_tds==1){
				$flw_tds_str=' TDS ';
			} 
			*/
			/* 
			$condition_query_inv = " WHERE ".TABLE_BILL_INV.".status='".Invoice::PENDING."' AND 
			".TABLE_BILL_INV.".client='".$client_inv."' AND ".TABLE_BILL_ORDERS.".tds_status = '".$flw_tds ."'" ;
			*/	
			$condition_query_inv = " WHERE ".TABLE_BILL_INV.".status='".Invoice::PENDING."' AND 
			".TABLE_BILL_INV.".client='".$client_inv."'" ;
			
					
			$fields_inv =  TABLE_BILL_INV .'.number'
				.','. TABLE_BILL_INV .'.or_no'
				.','. TABLE_BILL_INV .'.id as inv_id'
				.','. TABLE_BILL_INV .'.currency_abbr'
				.','. TABLE_BILL_INV .'.do_i'
				.','. TABLE_BILL_INV .'.do_e'
				.','. TABLE_BILL_INV .'.do_fe'
				.','. TABLE_BILL_INV .'.amount'
				.','. TABLE_BILL_INV .'.amount_inr'
				.','. TABLE_BILL_INV .'.balance'
				.','. TABLE_BILL_INV .'.balance_inr'
				.','. TABLE_BILL_INV .'.status'
				.','. TABLE_BILL_ORDERS .'.order_title';
			Invoice::getDetails( $db, $pendingInvlist, $fields_inv, $condition_query_inv);	
			$followupInvStr ='';			
			if(!empty($pendingInvlist)){
				$followupInvStr .='<table cellpadding="2" cellspacing="0" border="0" align="left" width="100%">';
				$followupInvStr .='<tr class="header">
										<td class="support-heading" colspan="6"  align="left">
										Pending'.$flw_tds_str.'Invoice Details</td>
									  </tr>
									  <tr> 
										<td class="support-sub-heading" align="left">
										  Order Title
										</td>
										<td class="support-sub-heading">                                    
											Invoice No 
										</td>
										 <td class="support-sub-heading">                                    
											Invoice Dt
										</td>
										<td class="support-sub-heading">                                    
											Service From 
										</td>
										<td class="support-sub-heading">                                    
											Service To 
										</td>
										<td class="support-sub-heading">                                    
											Balance Amount 
										</td>
									</tr>
									<tr><td colspan="6">&nbsp;</td></tr>';
				foreach( $pendingInvlist as $key=>$val){
					$val['bgcolor']='#edeee6';
					if(($key%2)==0){
						$val['bgcolor']='#e0e1d8';
					}
					
					$val['inv_id']=$val['inv_id'];
					$val['do_i']=$val['do_i'];
					$val['do_e']=$val['do_e'];
					$val['do_fe']=$val['do_fe'];	
					$do_id = $do_im = $do_iy =$do_istr='';
					if($val['do_i']!='0000-00-00 00:00:00'){
						$do_i = explode(' ',$val['do_i']);
						$do_iArr =explode('-', $do_i[0]);
						$do_id = $do_iArr[2];
						$do_im = $do_iArr[1];				
						$do_iy = $do_iArr[0];
						
						$do_istr = date('d M Y',mktime(0,0,0,$do_im,$do_id,$do_iy));
					}
					
					$do_ed =$do_em = $do_ey =$do_estr ='';
					if($val['do_e']!='0000-00-00 00:00:00'){
						$do_e = explode(' ',$val['do_e']);
						$do_eArr =explode('-', $do_e[0]);
						$do_ed = $do_eArr[2];
						$do_em = $do_eArr[1];				
						$do_ey = $do_eArr[0];
						$do_estr = date('d M Y',mktime(0,0,0,$do_em,$do_ed,$do_ey));
					}
					
					$do_fed =	$do_fem =	$do_fey =$do_festr= '';
					if($val['do_fe']!='0000-00-00 00:00:00'){
						$do_fe = explode(' ',$val['do_fe']);
						$do_feArr =explode('-', $do_fe[0]);
						$do_fed = $do_feArr[2];
						$do_fem = $do_feArr[1];				
						$do_fey = $do_feArr[0];
						$do_festr = date('d M Y',mktime(0,0,0,$do_fem,$do_fed,$do_fey));
					}					
					$val['amount']= number_format($val['amount'],2);
					$val['amount_inr']= number_format($val['amount_inr'],2);
					$val['balance']= number_format($val['balance'],2);
					$totalBalanceAmount = $totalBalanceAmount+$val['balance_inr'];
					$val['balance_inr']= number_format($val['balance_inr'],2);				   
					$fpendingInvlist[$key]=$val;
					$followupInvStr .='<tr bgcolor="'.$val['bgcolor'].'" style="line-height:22px;"> 
										<td class="support-sub-content"  align="left">'.$val['order_title'].'								
										</td>
										<td class="support-sub-content">'.$val['number'].'</td>
										<td class="support-sub-content">'.$do_istr.'</td>
										<td class="support-sub-content"> ' ;
											if($val['do_fe']!='0000-00-00 00:00:00'){
												$followupInvStr .= $do_festr   ;
											}
					$followupInvStr .= 	'</td>
										 <td class="support-sub-content"> '   ;
											if($val['do_e']!='0000-00-00 00:00:00'){
												$followupInvStr .= $do_estr;
											}
					$followupInvStr .= '</td>
										<td class="support-sub-content">                                    
											'.$val['balance_inr'].' in INR
										</td>
									</tr>' ;
				}
				$totalBalanceAmount = number_format($totalBalanceAmount,2);
				$followupInvStr .='
							<tr>	
							 <td>&nbsp;</td>
							 <td colspan="5"><hr color="#cecece"></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
								<td class="support-sub-heading">Total</td>
								<td class="support-sub-heading">'.$totalBalanceAmount.'</td>
							</tr>' ;
				$followupInvStr .='</table>';
			}
			//echo $followupInvStr;
			/* $condition_query_pinv = " WHERE ".TABLE_BILL_INV_PROFORMA.".status 
			IN('".Invoice::PENDING."','".Invoice::ACTIVE."')
			AND ".TABLE_BILL_ORDERS.".is_invoiceprofm_create='1' AND ".TABLE_BILL_ORDERS.".is_invoice_create='0'
			AND ".TABLE_BILL_ORDERS.".tds_status = '".$flw_tds ."'
			AND ".TABLE_BILL_INV_PROFORMA.".client='".$client_inv."' " ; */
			
			$condition_query_pinv = " WHERE 
			".TABLE_BILL_INV_PROFORMA.".status IN('".Invoice::PENDING."','".Invoice::ACTIVE."')
			AND ".TABLE_BILL_ORDERS.".is_invoiceprofm_create='1' AND ".TABLE_BILL_ORDERS.".is_invoice_create='0'
			AND ".TABLE_BILL_INV_PROFORMA.".client='".$client_inv."' " ;
			
			$fields_inv =     TABLE_BILL_INV_PROFORMA .'.number'
                        .','. TABLE_BILL_INV_PROFORMA .'.id as pinv_id'
                        .','. TABLE_BILL_INV_PROFORMA .'.or_no'
                        .','. TABLE_BILL_INV_PROFORMA .'.currency_abbr'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_i'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_e'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_fe'
                        .','. TABLE_BILL_INV_PROFORMA .'.amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.amount_inr'
                        .','. TABLE_BILL_INV_PROFORMA .'.balance'
                        .','. TABLE_BILL_INV_PROFORMA .'.balance_inr'
                        .','. TABLE_BILL_INV_PROFORMA .'.status'
                        .','. TABLE_BILL_ORDERS .'.order_title';
			InvoiceProforma::getDetails( $db, $pendingPInvlist, $fields_inv, $condition_query_pinv);		
			$followupPInvStr ='';			
			if(!empty($pendingPInvlist)){
				$followupPInvStr .='<table cellpadding="2" cellspacing="0" border="0" align="left" width="100%">';
				$followupPInvStr .='<tr class="header">
									<td class="support-heading" colspan="6"  align="left">Pending
									'.$flw_tds_str.'Proforma Invoice Details</td>
								  </tr>
								  <tr> 
									<td class="support-sub-heading"  align="left">
									   Order Title
									</td>
									<td class="support-sub-heading">                                    
										Proforma Invoice No 
									</td>
									 <td class="support-sub-heading">                                    
										Proforma Invoice Dt
									</td>
									<td class="support-sub-heading">                                    
										Service From 
									</td>
									<td class="support-sub-heading">                                    
										Service To 
									</td>
									<td class="support-sub-heading">                                    
										Balance Amount 
									</td>
								</tr>
								<tr><td colspan="6">&nbsp;</td></tr>';
				foreach( $pendingPInvlist as $key=>$val){
					$val['bgcolor']='#edeee6';
					if(($key%2)==0){
						$val['bgcolor']='#e0e1d8';
					}
					
					$val['pinv_id']=$val['pinv_id'];
					$val['do_i']=$val['do_i'];
					$val['do_e']=$val['do_e'];
					$val['do_fe']=$val['do_fe'];
					$do_id = $do_im = $do_iy =$do_istr='';
					
					if($val['do_i']!='0000-00-00 00:00:00'){
						$do_i = explode(' ',$val['do_i']);
						$do_iArr =explode('-', $do_i[0]);
						$do_id = $do_iArr[2];
						$do_im = $do_iArr[1];				
						$do_iy = $do_iArr[0];
						
						$do_istr = date('d M Y',mktime(0,0,0,$do_im,$do_id,$do_iy));
					}
					
					$do_ed =$do_em = $do_ey =$do_estr ='';
					if($val['do_e']!='0000-00-00 00:00:00'){
						$do_e = explode(' ',$val['do_e']);
						$do_eArr =explode('-', $do_e[0]);
						$do_ed = $do_eArr[2];
						$do_em = $do_eArr[1];				
						$do_ey = $do_eArr[0];
						$do_estr = date('d M Y',mktime(0,0,0,$do_em,$do_ed,$do_ey));
					}
					
					$do_fed =	$do_fem =	$do_fey =$do_festr= '';
					if($val['do_fe']!='0000-00-00 00:00:00'){
						$do_fe = explode(' ',$val['do_fe']);
						$do_feArr =explode('-', $do_fe[0]);
						$do_fed = $do_feArr[2];
						$do_fem = $do_feArr[1];				
						$do_fey = $do_feArr[0];
						$do_festr = date('d M Y',mktime(0,0,0,$do_fem,$do_fed,$do_fey));
					}					
					$val['amount']=number_format($val['amount'],2);
					$val['amount_inr']=number_format($val['amount_inr'],2);
					$val['balance']=number_format($val['balance'],2);
					$totalBalanceAmountP = $totalBalanceAmountP+$val['balance_inr'];
					$val['balance_inr']=number_format($val['balance_inr'],2);				   
					$fpendingPInvlist[$key]=$val;
					$followupPInvStr .='<tr bgcolor="'.$val['bgcolor'].'" style="line-height:22px;"> 
					<td class="support-sub-content" 
						align="left">'.$val['order_title'].'</td>
										<td class="support-sub-content">'.$val['number'].'</td>
										<td class="support-sub-content">'.$do_istr.'</td>
										<td class="support-sub-content"> ' ;
											if($val['do_fe']!='0000-00-00 00:00:00'){
												$followupPInvStr .= $do_festr   ;
											}
					$followupPInvStr .= '</td>
										 <td class="support-sub-content"> '   ;
											if($val['do_e']!='0000-00-00 00:00:00'){
												$followupPInvStr .= $do_estr;
											}
					$followupPInvStr .=	'</td>
										<td class="support-sub-content">                                    
											'.$val['balance_inr'].' in INR
										</td>
									</tr>' ;
				}
				$totalBalanceAmountP = number_format($totalBalanceAmountP,2);
				$followupPInvStr .='
							<tr>	
							 <td>&nbsp;</td>
							 <td colspan="5"><hr color="#cecece"></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
								<td class="support-sub-heading">Total</td>
								<td class="support-sub-heading">'.$totalBalanceAmountP.'</td>
							</tr>' ;
				$followupPInvStr .='</table>';
			}
		}
		
	    //$user_id = $my['user_id'];
        $mail_send_to_su = "";
        $_ALL_POST	= NULL;
        $data       = NULL;
		
        $category_list	= 	NULL;
		$condition_query1 = " WHERE ".TABLE_ST_TEMPLATE_CATEGORY.".status = '".StTemplateCategory::ACTIVE."' 
			ORDER BY ".TABLE_ST_TEMPLATE_CATEGORY.".category ASC " ;
		StTemplateCategory::getList( $db, $category_list, TABLE_ST_TEMPLATE_CATEGORY.".category,"
		.TABLE_ST_TEMPLATE_CATEGORY.".id", $condition_query1);
        
		$stTemplates = null;
		if(!empty($template_category)){
			$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
			".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::ST.",%' AND 
			".TABLE_ST_TEMPLATE.".template_category LIKE '%,".$template_category.",%' ORDER BY ".TABLE_ST_TEMPLATE.".title";
			SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'
			.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
		}
      
        //BOF read the available departments
        //CeoTicket::getDepartments($db,$department);
        //EOF read the available departments
        
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["rating"] = CeoTicket::getRating();
        $variables["status"] = CeoTicket::getTicketStatusList();        
        
		//hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        //$lst_min[0] = 'Min';
        for($j=12; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
		// hrs, min.        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $posted_data=$_POST;
            $data		= processUserData($_ALL_POST);
            $files      = processUserData($_FILES);
            
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE     ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
            
            if ( CeoTicket::validateAdd($data, $extra) ) {
               
                //$ticket_no  =  "PT". date("ymdhi-s");
                $ticket_no  =  CeoTicket::getNewNumber($db);
                $attachfilename=$ticket_attachment_path='';
                
                if(!empty($data['ticket_attachment'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['ticket_attachment']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    $attachfilename = $ticket_no.".".$ext ;
                    $data['ticket_attachment'] = $attachfilename;
                    
                    $ticket_attachment_path = DIR_WS_CST_FILES;
                    
                    if(move_uploaded_file ($files['ticket_attachment']['tmp_name'], DIR_FS_CST_FILES."/".$attachfilename)){
                        @chmod(DIR_FS_CST_FILES."/".$attachfilename, 0777);
                    }
                }
                
                $query = "SELECT "	. TABLE_CST_TICKETS .".ticket_date, ".TABLE_CST_TICKETS.".status"
						." FROM ". TABLE_CST_TICKETS 
						." WHERE ". TABLE_CST_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
						." AND (". TABLE_CST_TICKETS .".ticket_child = '". $ticket_id ."' " 
									." OR "
									. TABLE_CST_TICKETS .".ticket_id = '". $ticket_id ."' " 
								.") "
						." ORDER BY ". TABLE_CST_TICKETS .".ticket_date DESC LIMIT 0,1";				
						
				$db->query($query) ;
				if($db->nf() > 0){
					$db->next_record() ;
					$last_time = $db->f("ticket_date") ;
					$ticket_response_time = time() - $last_time ;
					$status = $db->f("status") ;
				}
				else{
					$ticket_response_time = 0 ;
				}
				$ticket_date 	= time() ;
				$ticket_replied = '0' ;

                $mail_to_all_su=0;
                if(isset($data['mail_to_all_su']) ){
                    $mail_to_all_su=1;
                }
                $mail_to_additional_email=0;
                if(isset($data['mail_to_additional_email']) ){
                    $mail_to_additional_email=1;
                }
                $mail_client=0;
                if(isset($data['mail_client']) ){
                    $mail_client=1;
                }
				//disply random name BOF 
				$data['display_name']=$data['display_user_id']=$data['display_designation']='';
				//if($mail_to_all_su==1 || $mail_to_additional_email==1 || $mail_client==1){
					$randomUser = getRandomAssociate($data['executive_id']);
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
				//}
				//disply random name EOF
                if(!isset($data['ticket_status']) ){                    
                    $ticket_status = CeoTicket::PENDINGWITHCLIENTS;
                }else{
                    $ticket_status = $data['ticket_status'];
                }
				$hrs1 = (int) ($data['hrs'] * 6);
                $min1 = (int) ($data['min'] * 6);  
				
				$sent_inv_id_str = $sent_inv_no_str ='';
				if(!empty($data['sent_inv_id'])){	
					foreach($data['sent_inv_id'] as $key1=>$val1){
						$sent_inv_id_str .= $key1."," ;
						$sent_inv_no_str .= $val1."," ;
					}		
					$sent_inv_id_str = ",".$sent_inv_id_str ;
					$sent_inv_no_str = ",".$sent_inv_no_str ;
				}
				$sent_pinv_id_str = $sent_pinv_no_str ='';
				if(!empty($data['sent_pinv_id'])){	
					foreach($data['sent_pinv_id'] as $key2=>$val2){
						$sent_pinv_id_str .= $key2."," ;
						$sent_pinv_no_str .= $val2."," ;
					}
					$sent_pinv_id_str = ",".$sent_pinv_id_str ;					
					$sent_pinv_no_str = ",".$sent_pinv_no_str ;
				}
				
				if(empty($data['ss_id'])){					
					$data['ss_file_1']='';
					$data['ss_file_2']='';
					$data['ss_file_3']='';
				}				
				 $query = "INSERT INTO "	. TABLE_CST_TICKETS ." SET "
					. TABLE_CST_TICKETS .".ticket_no          = '". $ticket_no ."', "
					. TABLE_CST_TICKETS .".invoice_id         = '". $invoice_id ."', "
					. TABLE_CST_TICKETS .".invoice_profm_id   = '". $invoice_profm_id ."', "
					. TABLE_CST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
					. TABLE_CST_TICKETS .".template_id        = '". $data['template_id'] ."', "
					. TABLE_CST_TICKETS .".template_file_1    = '". $data['template_file_1'] ."', "
                    . TABLE_CST_TICKETS .".template_file_2    = '". $data['template_file_2'] ."', "
                    . TABLE_CST_TICKETS .".template_file_3    = '". $data['template_file_3'] ."', "
					. TABLE_CST_TICKETS .".ss_id      	     = '". $data['ss_id'] ."', "
					. TABLE_CST_TICKETS .".ss_title   		 = '". $data['ss_title'] ."', "
					. TABLE_CST_TICKETS .".ss_file_1   		 = '". $data['ss_file_1'] ."', "
                    . TABLE_CST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
                    . TABLE_CST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
                    . TABLE_CST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
                    . TABLE_CST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
					. TABLE_CST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
                    . TABLE_CST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
					. TABLE_CST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
					. TABLE_CST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
					. TABLE_CST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
					. TABLE_CST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
					. TABLE_CST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
					. TABLE_CST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
					. TABLE_CST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
					. TABLE_CST_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
					//. TABLE_CST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
					//. TABLE_CST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
					//. TABLE_CST_TICKETS .".ticket_status     = '".$data['ticket_status']."', "
					. TABLE_CST_TICKETS .".ticket_status     = '".$ticket_status."', "
					. TABLE_CST_TICKETS .".ticket_child      = '".$ticket_id."', "
					. TABLE_CST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
					. TABLE_CST_TICKETS .".hrs1 = '". $hrs1 ."', "
					. TABLE_CST_TICKETS .".min1 = '". $min1 ."', "
					. TABLE_CST_TICKETS .".hrs = '". $data['hrs'] ."', "
					. TABLE_CST_TICKETS .".min = '". $data['min']."', "
					. TABLE_CST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
					. TABLE_CST_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
					. TABLE_CST_TICKETS .".ticket_replied    = '".$ticket_replied."', "
					. TABLE_CST_TICKETS .".from_admin_panel  = '".CeoTicket::ADMIN_PANEL."', "
					. TABLE_CST_TICKETS .".mail_client    = '".$mail_client."', "
					. TABLE_CST_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
					. TABLE_CST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
					. TABLE_CST_TICKETS .".domain_name       = '". $data['domain_name'] ."', "
					. TABLE_CST_TICKETS .".display_name           = '". $data['display_name'] ."', "
					. TABLE_CST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
					. TABLE_CST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
					. TABLE_CST_TICKETS .".is_private        = '". $data['is_private'] ."', "
					. TABLE_CST_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
					. TABLE_CST_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
					. TABLE_CST_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "
					. TABLE_CST_TICKETS .".to_email           = '". $data['to'] ."', "
					. TABLE_CST_TICKETS .".cc_email           = '". $data['cc'] ."', "
					. TABLE_CST_TICKETS .".bcc_email          = '". $data['bcc'] ."', "
					. TABLE_CST_TICKETS .".to_email_1         = '". $data['to_email_1'] ."', "
					. TABLE_CST_TICKETS .".cc_email_1         = '". $data['cc_email_1'] ."', "
					. TABLE_CST_TICKETS .".bcc_email_1        = '". $data['bcc_email_1'] ."', "
					. TABLE_CST_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
				
				//unset($GLOBALS["flag"]);
				//unset($GLOBALS["submit"]);
                            
                if ($db->query($query) && $db->affected_rows() > 0) {
                    
                    $variables['hid'] = $ticket_child  = $db->last_inserted_id() ;
                    $sql = '';
                    //echo $status;
                    $sqlstatus ='';
                    if($status == CeoTicket::DEACTIVE){
                        $sqlstatus = ",".TABLE_CST_TICKETS.".status='".CeoTicket::ACTIVE."'";   
                    }                    
                    //Mark pending towards client bof
                    $query_update = "UPDATE ". TABLE_CST_TICKETS 
							." SET ". TABLE_CST_TICKETS .".ticket_status = '".$ticket_status."'"                            .",".TABLE_CST_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
							.",".TABLE_CST_TICKETS.".last_comment_from = '".CeoTicket::ADMIN_COMMENT."'"
							.",".TABLE_CST_TICKETS.".last_comment = '".$data['ticket_text']."'"
							.",".TABLE_CST_TICKETS.".last_comment_by = '".$my['uid'] ."'"
							.",".TABLE_CST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
							.$sqlstatus                                                                     
							." WHERE ". TABLE_CST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				    $db->query($query_update) ;
                    //Mark pending towards client eof
                    
                    /**************************************************************************
                    * Send the notification to the ticket owner and the Administrators
                    * of the new Ticket being created
                    **************************************************************************/
                    
                    $data['ticket_no']    =   $ticket_no ;
                    $data['mticket_no']   =   $data['mticket_no'] ;
                    /*foreach( $status as $key=>$val){                           
                        if ($data['ticket_status'] == $val['status_id'])
                        {
                            $data['status']=$val['status_name'];
                        }
                    }*/
                    
                    $data['replied_by']   =   $my['f_name']." ". $my['l_name'] ;
                    $data['subject']    =     $_ALL_POST['ticket_subject'] ;
					$data['text']    =   $_ALL_POST['ticket_text'] ;
                    $data['attachment']   =   $attachfilename ;
                    $data['link']   = DIR_WS_MP .'/ceo-ticket.php?perform=view&ticket_id='. $ticket_id;
                    $file_name = '';
                    
					if(!empty($attachfilename)){
						$file_name[] = DIR_FS_CST_FILES ."/". $attachfilename;
					}
					if(!empty($data['template_file_1'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
					}
					if(!empty($data['template_file_2'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
					}
					if(!empty($data['template_file_3'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
					}
					if(!empty($data['ss_file_1'])){
						$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_1'];
					}
					if(!empty($data['ss_file_2'])){
						$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_2'];
					}
					if(!empty($data['ss_file_3'])){
						$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_3'];
					}
					if(!empty($data['sent_inv_id'])){	
						foreach($data['sent_inv_id'] as $key1=>$val1){
							$file_name[]  = DIR_FS_INV_PDF_FILES ."/".$val1 .".pdf";
						}				
					}
					if(!empty($data['sent_pinv_id'])){	
						foreach($data['sent_pinv_id'] as $key2=>$val2){
							$file_name[] = DIR_FS_INVPROFM_PDF_FILES ."/".$val2 .".pdf";
						}				
					}
					
                    //for sms
                    $sms_to_number = $sms_to =array();
                    $mobile1 = $mobile2='';
                    $counter=0;
                    $smsLength = strlen($data['text']); 
                    // Send Email to the ticket owner BOF                         
                    Clients::getList($db, $userDetails, 'number, f_name, l_name, email,check_email,email_1,email_2,email_3,email_4,
                        additional_email,title as ctitle,billing_name,mobile1,mobile2', " WHERE user_id = '".$data['ticket_owner_uid']."'");
                    if(!empty($userDetails)){
						$userDetails=$userDetails['0'];
						$data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
						$data['ctitle']    =   $userDetails['ctitle'];
						$data['mobile1']   =   $userDetails['mobile1'];
						$data['mobile2']   =   $userDetails['mobile2'];
						$data['check_email']   =   $userDetails['check_email'];
						$main_client_name  =   $userDetails['f_name']." ".$userDetails['l_name'];
						$billing_name     =   $userDetails['billing_name'];                      
						$email = $cc_to_sender = NULL;
						 
						if(!empty($data['cc'])){
							$cc_email = explode(",",$data['cc']);
							foreach ($cc_email  as $key=>$value){
								$cc[]   = array('name' => $value, 'email' => $value);                                
							}
						}else{
							$cc = Null;
						}
						
						if(!empty($data['bcc'])){
							$bcc_email = explode(",",$data['bcc']);
							foreach ($bcc_email  as $key=>$value){
								$bcc[]   = array('name' => $value, 'email' => $value);
							}
						}else{
							$bcc = Null;
						}
						if(!empty($data['to'])){
							$to_email = explode(",",$data['to']);
							foreach ($to_email  as $key=>$value){
								$to[]   = array('name' =>  $value, 'email' => $value);
							}  
						}else{
							$to = '';
						} 
                             
                        if ($data['is_private'] == 1 ){
							if ( getParsedEmail($db, $s, 'EMAIL_CST_CONFIDENTIAL', $data, $email) ) {
								//$to = '';
								$cc_to_sender=$cc=$bcc=''; // as confidential
								if(!empty($userDetails['email'])){
									$to[]   = array('name' => $userDetails['email'], 
									'email' => $userDetails['email']);     
									$mail_send_to_su .=",".$userDetails['email'];                                
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									/* 
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"],
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
								}
								//Send mail on alternate emails ids bof
								if(!empty($userDetails['email_1'])){                                       
									//$to = '';
									$cc_to_sender=$cc=$bcc='';
									$to[]   = array('name' => $userDetails['email_1'], 
									'email' =>	$userDetails['email_1']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$userDetails['email_1'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
								}
								if(!empty($userDetails['email_2'])){                                       
									//$to = '';
									$cc_to_sender=$cc=$bcc='';
									$to[]   = array('name' => $userDetails['email_2'], 'email' =>
									$userDetails['email_2']);                                   
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									
									$mail_send_to_su  .= ",".$userDetails['email_2'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
								}
								if(!empty($userDetails['email_3'])){                                       
									//$to = '';
									$cc_to_sender=$cc=$bcc='';
									$to[]   = array('name' => $userDetails['email_3'], 'email' =>
									$userDetails['email_3']);                                   
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$userDetails['email_3'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
								}
								if(!empty($userDetails['email_4'])){                                       
									//$to = '';
									$cc_to_sender=$cc=$bcc='';
									$to[]   = array('name' => $userDetails['email_4'], 'email' =>
									$userDetails['email_4']);                                   
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$userDetails['email_4'];
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
								}
								//Send mail on alternate emails ids eof
							}                       
                        }else{
							if ($mail_client == 1 ){  
								if ( getParsedEmail($db, $s, 'EMAIL_CST_MEMBER', $data, $email) ) {
									if(!empty($userDetails['email'])){
										$to[]   = array('name' =>  $userDetails['email'], 
										'email' => $userDetails['email']);
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);										 
										$mail_send_to_su  .= ",".$userDetails['email'];
									  
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],
										$cc_to_sender,$cc,$bcc,$file_name); */
									}
									//Send mail on alternate emails ids bof
									if(!empty($userDetails['email_1'])){                                       
										//$to = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['email_1'], 'email' =>
										$userDetails['email_1']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_1'];
										/* SendMail($to, $from, $reply_to, $email["subject"], 
										$email["body"],$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                        
									}
									if(!empty($userDetails['email_2'])){                                       
										//$to = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['email_2'], 'email' =>
										$userDetails['email_2']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_2'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
									}
									if(!empty($userDetails['email_3'])){                                       
										//$to = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['email_3'], 'email' =>
										$userDetails['email_3']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_3'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                      
									}
									if(!empty($userDetails['email_4'])){                                       
										//$to = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['email_4'], 'email' =>
										$userDetails['email_4']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_4'];
										/* SendMail($to, $from, $reply_to, $email["subject"], 
										$email["body"], $email["isHTML"], 
										$cc_to_sender,$cc,$bcc,$file_name);   
										*/
									}
								}                      
							}
							//Send mail on alternate emails ids bof
								if($mail_to_additional_email==1){                        
									getParsedEmail($db, $s, 'EMAIL_CST_MEMBER', $data, $email) ;
									if(!empty($userDetails['additional_email'])){
										$cc_to_sender=$cc=$bcc='';
										$arrAddEmail = explode(",",$userDetails['additional_email']); 
										foreach($arrAddEmail as $key=>$val ){
											if(!empty($val)){
												$mail_send_to_su  .= ",".$val;
												//$to = '';
												$to[]   = array('name' =>$val, 'email' => $val);
												/* SendMail($to, $from, $reply_to, $email["subject"], 
												$email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name); */
											}
										}
									}									
								}else{
									if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
										$arrAddEmail = $data['mail_additional_email'];
										if ( getParsedEmail($db, $s, 'EMAIL_CST_MEMBER', $data, $email) ) {
											foreach($arrAddEmail as $key=>$val ){
												if(!empty($val)){
													$mail_send_to_su  .= ",".$val;
													//$to = '';
													$to[]   = array('name' =>$val, 'email' => $val);  
													/* SendMail($to, $from, $reply_to, $email["subject"], 
													$email["body"], $email["isHTML"],
													$cc_to_sender,$cc,$bcc,$file_name); */
												}
											}
										}
									}
								}
							   //Send mail on alternate emails ids eof 
                        }						
						//Send SMS
						if(isset($data['mobile1_client']) && $smsLength<=130){
							//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
							$sms_to_number[] =$userDetails['mobile1'];
							$mobile1 = "91".$userDetails['mobile1'];
							$client_mobile .= ','.$mobile1;
							ProjectTask::sendSms($data['text'],$mobile1);
							$counter = $counter+1;								
						}
						if(isset($data['mobile2_client']) && $smsLength<=130){
							//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
							$sms_to_number[] =$userDetails['mobile2'] ;
							$mobile2 = "91".$userDetails['mobile2'];
							$client_mobile .= ','.$mobile2;
							ProjectTask::sendSms($data['text'],$mobile2);
							$counter = $counter+1;                                        
						}
						if(!empty($mobile1)|| !empty($mobile2)){
							 $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
						}
						//Send a copy to check the mail for clients
						/*if(!empty($smeerp_client_email)){
							getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
							$to     = '';
							$cc_to_sender=$cc=$bcc='';
							$to[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
							'email' => $smeerp_client_email);
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,
							$cc,$bcc,$file_name);
						}	*/					
                    }
                    // Send Email to the ticket owner EOF
                   
                    $subsmsUserDetails=array();
                    // Send Email to all the subuser BOF
                    if ($data['is_private'] != 1){
                    
                        if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
                            $followupSql= "";
							if(!empty($invoice_id)|| !empty($invoice_profm_id) || !empty($flw_ord_id)){
								$followupSql = " AND authorization_id  LIKE '%,".Clients::ACCOUNTS.",%'" ;
							}
                            if(isset($_POST['mail_to_all_su'])){
                                Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND service_id LIKE '%,".Clients::ST.",%' 
                                AND status='".Clients::ACTIVE."' AND send_mail='".Clients::SENDMAILYES."' ".$followupSql." ORDER BY f_name");
                            }elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
                               $mail_to_su= implode("','",$_POST['mail_to_su']);
                                Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND user_id IN('".$mail_to_su."')
                                AND status='".Clients::ACTIVE."' ORDER BY f_name");
                            }
							
                            if(!empty($subUserDetails)){
                                //$subUserDetails=$subUserDetails['0'];                               
                                foreach ($subUserDetails  as $key=>$value){
                                    $data['client_name']=$subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                    $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                    $email = NULL;
                                    $cc_to_sender= $cc = $bcc = Null;
                                    if ( getParsedEmail($db, $s, 'EMAIL_CST_MEMBER', $data, $email) ) {
                                        //$to = '';
                                        $to[]   = array('name' =>  $subUserDetails[$key]['email'], 
										'email' => $subUserDetails[$key]['email']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                       /*  if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
											$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                        } */
                                        if(!empty($subUserDetails[$key]['email_1'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_1'],
											'email' => $subUserDetails[$key]['email_1']);
                                            $from   = $reply_to = array('name' => $email["from_name"],
											'email' => $email['from_email']);
											 $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                            /* 
											if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                            } */
                                        } 
                                        if(!empty($subUserDetails[$key]['email_2'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_2'],
											'email' => $subUserDetails[$key]['email_2']);
                                            $from   = $reply_to = array('name' => $email["from_name"],
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                            /* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                            } */
                                        }
                                        if(!empty($subUserDetails[$key]['email_3'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_3'], 
											'email' => $subUserDetails[$key]['email_3']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                            /* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                            } */
                                        }
                                        if(!empty($subUserDetails[$key]['email_4'])){                                       
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email_4'], 
											'email' => $subUserDetails[$key]['email_4']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                            /* 
											if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                            } */
                                        }
                                        
                                    }                      
                                }
                            }
                        }else{
                           /*
						    if($data['ticket_owner_uid'] != $data['ticket_creator_uid']){
                                Clients::getList($db, $clientDetails, 'number, f_name, l_name, title as ctitle, email,email_1,email_2,email_3,email_4,
                                parent_id,mobile1,mobile2', " WHERE user_id = '".$data['ticket_creator_uid'] ."' AND send_mail='".Clients::SENDMAILYES."'");
                                if(!empty($clientDetails)){
                                    $clientDetails=$clientDetails['0'];
                                   
                                    if(!empty($clientDetails['parent_id'])){
                                        $data['client_name']    =   $clientDetails['f_name']." ".$clientDetails['l_name'];
                                        $data['ctitle']    =   $clientDetails['ctitle'];
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                                            $to = '';
                                            $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'],
                                            'email' => $clientDetails['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                            $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                $mail_send_to_su .= ",".$clientDetails['email'];
                                            }
                                            //Send mail on alternate emails ids bof
                                            if(!empty($clientDetails['email_1'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_1']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_1'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_2'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_2']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_2'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_3'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $clientDetails['email_3']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_3'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                            if(!empty($clientDetails['email_4'])){                                       
                                                $to = '';
                                                $cc_to_sender=$cc=$bcc='';
                                                $to[]   = array('name' => $clientDetails['f_name'] .' '. $clientDetails['l_name'], 'email' =>
                                                $userDetails['email_4']);                                   
                                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                                $mail_send_to_su  .= ",".$clientDetails['email_4'];
                                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                      
                                            }
                                        } 
                                    }
                                }
                            }*/
                        }
                        
                        if(isset($_POST['mobile1_to_su']) ){                                
                           $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
                            Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " 
                            WHERE parent_id = '".$data['ticket_owner_uid']."' AND user_id IN('".$mobile1_to_su."')
                            AND status='".Clients::ACTIVE."' ");
                            if(!empty($subsmsUserDetails)){                                                                   
                                foreach ($subsmsUserDetails  as $key=>$value){
                                    if(isset($value['mobile1']) && $smsLength<=130){
                                        $sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
                                        $sms_to_number[] =$value['mobile1'];
                                        $mobile1 = "91".$value['mobile1'];
                                        $client_mobile .= ','.$mobile1;
                                        ProjectTask::sendSms($data['text'],$mobile1);
                                        $counter = $counter+1;
                                            
                                    }
                                }
                            }
                        }
                        $subsmsUserDetails=array();
                        if(isset($_POST['mobile2_to_su']) ){                                
                           $mobile2_to_su = implode("','",$_POST['mobile2_to_su']);                                  
                            Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile2', " 
                            WHERE parent_id = '".$data['ticket_owner_uid']."' AND user_id IN('".$mobile2_to_su."')
                            AND status='".Clients::ACTIVE."' ");
                            if(!empty($subsmsUserDetails)){                                                                   
                                foreach ($subsmsUserDetails  as $key=>$value){
                                    if(isset($value['mobile2']) && $smsLength<=130){
                                        $sms_to[]  = $value['f_name'] .' '. $value['l_name'];
                                        $sms_to_number[] =$value['mobile2'];
                                        $mobile2 = "91".$value['mobile2'];
                                        $client_mobile .= ','.$mobile2;
                                        ProjectTask::sendSms($data['text'],$mobile2);
                                        $counter = $counter+1;
                                            
                                    }
                                }
                            }
                        }
                    }                    
                    // Send Email to all the subuser EOF                    
                    //Set the list subuser's email-id to whom the mails are sent bof                    
                    $query_update = "UPDATE ". TABLE_CST_TICKETS 
                        ." SET ". TABLE_CST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."',
                        sms_to_number='".implode(",",array_unique($sms_to_number))."', 
						sms_to ='".implode(",", array_unique($sms_to))."' "                         
						." WHERE ". TABLE_CST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
                    $db->query($query_update) ;
                   
                    //Set the list subuser's email-id to whom the mails are sent bof                       
                        
                    //update sms counter
                    if($counter >0){
                        $sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
                        $db->query($sql); 
                    }
                    //Set the list subuser's email-id to whom the mails are sent eof
					//Send one copy on clients BOF
					if(!empty($to)){
						getParsedEmail($db, $s, 'EMAIL_CST_MEMBER', $data, $email);
						//$to     = '';
						$cc_to_sender=$cc=$bcc='';
						$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
						'email' => $smeerp_client_email);
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
					}    
					//Send one copy on clients EOF					
                    // Send Email to the admin BOF
                    $email = NULL;
                    $data['link']   = DIR_WS_NC .'/ceo-ticket.php?perform=view&ticket_id='. $ticket_id;
                    $data['quicklink']   = DIR_WS_SHORTCUT .'/ceo-ticket.php?perform=view&ticket_id='. $ticket_id;      
                    $data['main_client_name']   = $main_client_name;
                    $data['billing_name']   = $billing_name;
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'EMAIL_CST_ADMIN', $data, $email) ) {
                        $to = '';
                        $to[]   = array('name' => $support_name , 'email' => $support_email);			 
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,
                        $cc,$bcc,$file_name);
                    }                        
                    // Send Email to the admin EOF                    
                    //$messages->setOkMessage("A comment has been added to the support ticket. And email Notification has been sent.");
                    $messages->setOkMessage("Ticket has been created successfully. And email Notification has been sent.");
                }
                //to flush the data.
				
                $_ALL_POST	= NULL;
                $data		= NULL;
               
				/* 
				if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
					header("Location:".DIR_WS_NC."/ceo-ticket.php?perform=invflw_view&ticket_id=".$ticket_id."&invoice_profm_id=".$invoice_profm_id."&invoice_id=".$invoice_id."&flw_tds=".$flw_tds."&flw_ord_id=".$flw_ord_id."&client_inv=".$client_inv);
				}else{
					 header("Location:".DIR_WS_NC."/ceo-ticket.php?perform=view&added=1&ticket_id=".$ticket_id);
				} */
            }
        }
        
        if( isset($_POST["btnUpdate"]) && $_POST["btnUpdate"] == "Close Ticket"){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            if(empty($data['ticket_rating'])){
                $messages->setErrorMessage("Please rate the service.");
            }else{
                $query = "UPDATE ". TABLE_CST_TICKETS 
                                ." SET "//. TABLE_CST_TICKETS .".ticket_priority = '". $data["ticket_priority"] ."', "
                                        . TABLE_CST_TICKETS .".ticket_status = '". CeoTicket::CLOSED ."', "
                                        . TABLE_CST_TICKETS .".ticket_rating = '". $data["ticket_rating"] ."', "
                                        . TABLE_CST_TICKETS .".ticket_replied = '0' "
                                ." WHERE ". TABLE_CST_TICKETS .".ticket_id = '". $ticket_id ."' " 
                                ." AND ". TABLE_CST_TICKETS .".ticket_owner_uid = '". $data["ticket_owner_uid"] ."' ";
                if($db->query($query)){
                    $messages->setOkMessage("Ticket Status Updated Successfully.");
                }
            }
		}
        
        if( isset($_POST["btnActive"])){
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $query = "UPDATE ". TABLE_CST_TICKETS 
                            ." SET ". TABLE_CST_TICKETS .".status = '". CeoTicket::ACTIVE ."'"
                            ." WHERE ". TABLE_CST_TICKETS .".ticket_id = '". $data['thread_ticket_id'] ."' " ;
            if($db->query($query)){
                $messages->setOkMessage("Ticket activated Updated Successfully.");
            }          
		}
        
        if( isset($_POST["btnDeActive"])){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $query = "UPDATE ". TABLE_CST_TICKETS 
                            ." SET ". TABLE_CST_TICKETS .".status = '". CeoTicket::DEACTIVE ."'"
                            ." WHERE ". TABLE_CST_TICKETS .".ticket_id = '". $data['thread_ticket_id'] ."' " ;
            if($db->query($query)){
                $messages->setOkMessage("Ticket de-activated Updated Successfully.");
            }            
		}
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        /* 
		if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
			
			if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
				header("Location:".DIR_WS_NC."/ceo-ticket.php?perform=invflw_view&ticket_id=".$ticket_id."&invoice_profm_id=".$invoice_profm_id."&invoice_id=".$invoice_id."&flw_tds=".$flw_tds."&flw_ord_id=".$flw_ord_id);
			
			}else{
				include ( DIR_FS_NC .'/support-ticket-list.php');
			} 
			
        }else{ */
		
            $condition_query= " WHERE ticket_id = '". $ticket_id ."' ";
            $_ALL_POST      = NULL;            
            if ( CeoTicket::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST[0];
                $mticket_no = $_ALL_POST['ticket_no'];
                $_ALL_POST['mail_send_to_su'] = chunk_split($_ALL_POST['mail_send_to_su']);
                $_ALL_POST['sms_to'] = chunk_split($_ALL_POST['sms_to']);
                $_ALL_POST['sms_to_number'] = chunk_split($_ALL_POST['sms_to_number']);
                $_ALL_POST['attachmentArr']='';
                if(!empty($_ALL_POST['attachment_imap'])){
                    $_ALL_POST['attachment_imap'] = trim($_ALL_POST['attachment_imap'],",") ;
                    $_ALL_POST['attachmentArr'] = explode(",",$_ALL_POST['attachment_imap']);
                } 
				
				$_ALL_POST['sent_inv_noArr']='';
                if(!empty($_ALL_POST['sent_inv_no'])){
                    $_ALL_POST['sent_inv_no'] = trim($_ALL_POST['sent_inv_no'],",") ;
                    $_ALL_POST['sent_inv_noArr'] = explode(",",$_ALL_POST['sent_inv_no']);
                } 
				$_ALL_POST['sent_inv_idArr']='';
                if(!empty($_ALL_POST['sent_inv_id'])){
                    $_ALL_POST['sent_inv_id'] = trim($_ALL_POST['sent_inv_id'],",") ;
                    $_ALL_POST['sent_inv_idArr'] = explode(",",$_ALL_POST['sent_inv_id']);
                } 
				$_ALL_POST['sent_pinv_noArr']='';
                if(!empty($_ALL_POST['sent_pinv_no'])){
                    $_ALL_POST['sent_pinv_no'] = trim($_ALL_POST['sent_pinv_no'],",") ;
                    $_ALL_POST['sent_pinv_noArr'] = explode(",",$_ALL_POST['sent_pinv_no']);
                } 
				$_ALL_POST['sent_pinv_idArr']='';
                if(!empty($_ALL_POST['sent_pinv_id'])){
                    $_ALL_POST['sent_pinv_id'] = trim($_ALL_POST['sent_pinv_id'],",") ;
                    $_ALL_POST['sent_pinv_idArr'] = explode(",",$_ALL_POST['sent_pinv_id']);
                } 
                $_ALL_POST['path_attachment_imap'] = DIR_WS_CST_FILES ;
            }
			
            $ticketStatusArr = CeoTicket::getTicketStatus();
            $ticketStatusArr = array_flip($ticketStatusArr);
            $_ALL_POST['ticket_status_name'] = $ticketStatusArr[$_ALL_POST['ticket_status']];   
            /*
			$table = TABLE_SETTINGS_DEPARTMENT;
            $condition2 = " WHERE ".TABLE_SETTINGS_DEPARTMENT .".id= '".$_ALL_POST['ticket_department'] ."' " ;
            $fields1 =  TABLE_SETTINGS_DEPARTMENT .'.department_name';
            $ticket_department= getRecord($table,$fields1,$condition2);            
            $_ALL_POST['department_name']    = $ticket_department['department_name'] ;     
			*/   
            
            $condition_query= " WHERE user_id = '". $_ALL_POST['ticket_owner_uid'] ."' ";
            $member = $additionalEmailArr =  NULL;            
            if ( Clients::getList($db, $member, 'f_name,l_name,billing_name,payment_remarks,
			additional_email,mobile1,mobile2,status,send_mail,email,check_email,email_1,email_2,
			email_3,email_4,authorization_id', $condition_query) > 0 ) {
                $_ALL_POST['client'] = $member[0];
                $_ALL_POST['client']['name'] = $main_client_name = $member[0]['f_name']." ".$member[0]['l_name'];      
                $_ALL_POST['mobile1'] =$member[0]['mobile1'];
                $_ALL_POST['mobile2'] =$member[0]['mobile2'];
                $billing_name = $member[0]['billing_name'];
                $_ALL_POST['email']=$member['0']['email'];
                $_ALL_POST['check_email']=$member['0']['check_email'];
                $_ALL_POST['payment_remarks']=$member['0']['payment_remarks'];
                $_ALL_POST['email_1'] = $member['0']['email_1'];
                $_ALL_POST['email_2'] = $member['0']['email_2'];
                $_ALL_POST['email_3'] = $member['0']['email_3'];
                $_ALL_POST['email_4'] = $member['0']['email_4'];
                $_ALL_POST['additional_email'] = $member['0']['additional_email'];
				$all_additional_email = trim($_ALL_POST['additional_email'],",");
				$additionalEmailArr = explode(",",$all_additional_email);
                $_ALL_POST['authorization_id'] = $member['0']['authorization_id'];
				$_ALL_POST['accountValid'] = 0;
				if(!empty($invoice_profm_id) || !empty($invoice_id)|| !empty($flw_ord_id)){
					$authorization_id = trim($_ALL_POST['authorization_id'],",");
					$authorizationArr = explode(",",$authorization_id);
					$isValid = in_array(Clients::ACCOUNTS,$authorizationArr);
					$_ALL_POST['accountValid'] = $isValid ;
				}
            }
			if(!isset($posted_data['ticket_text'])){
				$posted_data['ticket_text'] ='Dear....';
			}
            $_ALL_POST['mail_client']=0;            
            //$_ALL_POST['mail_to_all_su']=1;            
            $_ALL_POST['mail_to_additional_email']=1;            
            $subUserDetails=null;
            /*
            Clients::getList($db, $subUserDetails, 'number,user_id, f_name, l_name,title as ctitle,status, email,email_1,email_2, email_3,email_4', " 
                        WHERE parent_id = '".$_ALL_POST['ticket_owner_uid']."' AND service_id LIKE '%,".Clients::ST.",%' 
                        AND status='".Clients::ACTIVE."' ");
                        */
			$followupSql= "";
			if(!empty($invoice_profm_id) || !empty($invoice_id) || !empty($flw_ord_id)){
				$followupSql = " AND authorization_id  LIKE '%,".Clients::ACCOUNTS.",%'" ;
			}
			$suCondition =" WHERE parent_id = '".$_ALL_POST['ticket_owner_uid']."' 
            AND status='".Clients::ACTIVE."' AND service_id LIKE '%,".Clients::ST.",%' ".$followupSql." 
			ORDER BY f_name ";
            Clients::getList($db, $subUserDetails, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail, 
			email,check_email,email_1,email_2, email_3,email_4,mobile1,mobile2', $suCondition );
	   
	   
			if(!empty($invoice_profm_id) || !empty($invoice_id) || !empty($flw_ord_id)){
				$ticketSql = " WHERE  ". TABLE_CST_TICKETS .".ticket_owner_uid = '". $_ALL_POST['ticket_owner_uid'] ."' AND ( ".TABLE_CST_TICKETS.".flw_ord_id!=0 OR ".TABLE_CST_TICKETS.".invoice_id!=0 OR ".TABLE_CST_TICKETS.".invoice_profm_id!=0 OR ". TABLE_CST_TICKETS .".ticket_child = '". $ticket_id ."'  ) ";  
				 
			}else{
				/* $ticketSql = " WHERE ". TABLE_CST_TICKETS .".ticket_owner_uid 
				= '". $_ALL_POST['ticket_owner_uid'] ."' " ." AND (". TABLE_CST_TICKETS .".ticket_child = '". $ticket_id ."' OR ".TABLE_CST_TICKETS.".ticket_id=".$ticket_id." )"  ; */
				$ticketSql = " WHERE " ." (". TABLE_CST_TICKETS .".ticket_child = '". $ticket_id ."')"  ;
			}
			
			$query = "SELECT COUNT(DISTINCT(". TABLE_CST_TICKETS .".ticket_id)) as count FROM ".TABLE_CST_TICKETS.$ticketSql ;
			$db->query($query);        
            $db->next_record();
			$total = $db->f("count"); 
			
            $query = "SELECT DISTINCT("	. TABLE_CST_TICKETS .".ticket_id), "
								. TABLE_CST_TICKETS .".ticket_no, "
								. TABLE_CST_TICKETS .".to_email_1, "
								. TABLE_CST_TICKETS .".cc_email_1, "
								. TABLE_CST_TICKETS .".bcc_email_1, "
								. TABLE_CST_TICKETS .".to_email, "
								. TABLE_CST_TICKETS .".cc_email, "
								. TABLE_CST_TICKETS .".bcc_email, "
								. TABLE_CST_TICKETS .".on_behalf, "
								. TABLE_CST_TICKETS .".from_email, "
								. TABLE_CST_TICKETS .".mail_send_to_su, "
								. TABLE_CST_TICKETS .".ticket_subject, "
								. TABLE_CST_TICKETS .".ticket_text, "
								. TABLE_CST_TICKETS .".ticket_creator, "
								. TABLE_CST_TICKETS .".ticket_creator_uid, "
								. TABLE_CST_TICKETS .".ticket_attachment, "
								. TABLE_CST_TICKETS .".ticket_attachment_1, "
								. TABLE_CST_TICKETS .".ticket_attachment_2, "
								. TABLE_CST_TICKETS .".template_id, "
								. TABLE_CST_TICKETS .".template_file_1, "
								. TABLE_CST_TICKETS .".template_file_2, "
								. TABLE_CST_TICKETS .".template_file_3, "
								. TABLE_CST_TICKETS .".ss_id, "
								. TABLE_CST_TICKETS .".ss_file_1, "
								. TABLE_CST_TICKETS .".ss_file_2, "
								. TABLE_CST_TICKETS .".ss_file_3, "
								. TABLE_CST_TICKETS .".sent_inv_id, "
								. TABLE_CST_TICKETS .".sent_inv_no, "
								. TABLE_CST_TICKETS .".sent_pinv_id, "
								. TABLE_CST_TICKETS .".sent_pinv_no, "
								. TABLE_CST_TICKETS .".followup_pinv_str, "
								. TABLE_CST_TICKETS .".followup_inv_str, "
								. TABLE_CST_TICKETS .".ticket_attachment_path, "
								. TABLE_CST_TICKETS .".ticket_response_time, "
								. TABLE_CST_TICKETS .".attachment_imap, "
								. TABLE_CST_TICKETS .".ticket_date, "
								. TABLE_CST_TICKETS .".status, "
								. TABLE_CST_TICKETS .".hrs1, "
								. TABLE_CST_TICKETS .".min1, "
								. TABLE_CST_TICKETS .".hrs, "
								. TABLE_CST_TICKETS .".min, "
								. TABLE_CST_TICKETS .".ip, "
								. TABLE_CST_TICKETS .".headers_imap, "
								. TABLE_CST_TICKETS .".sms_to, "
								. TABLE_CST_TICKETS .".display_name, "
								. TABLE_CST_TICKETS .".sms_to_number "
                                //. TABLE_CLIENTS .".mobile1, "
                                //. TABLE_CLIENTS .".mobile2 "
								." FROM ". TABLE_CST_TICKETS   
								//." FROM ". TABLE_CST_TICKETS ." LEFT JOIN ".TABLE_CLIENTS." 
                                //ON ".TABLE_CLIENTS.".user_id = ".TABLE_CST_TICKETS.".ticket_creator_uid "
                                . $ticketSql
							    ." ORDER BY ". TABLE_CST_TICKETS .".ticket_date DESC LIMIT ". $next_record.", ".$rpp;
			
			 
			/* if(!empty($invoice_id) || !empty($flw_ord_id)){
					$condition_url = DIR_WS_NC."/ceo-ticket.php?perform=invflw_view".$condition_url ;
			}else{
				$condition_url = DIR_WS_NC."/ceo-ticket.php?perform=view".$condition_url ;
			} */
			$condition_url .="&perform=".$perform."&invoice_profm_id=".$invoice_profm_id."&ticket_id=".$ticket_id."&flw_tds=".$flw_tds."&invoice_id=".$invoice_id."&flw_ord_id=".$flw_ord_id."&client_inv=".$client_inv;        
			
			$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
			
			$db->query($query) ;
			$ticketThreads = array() ;
			while($db->next_record()){
				$response["h"] = round($db->f("ticket_response_time") / 3600) ;
				$response["m"] = round(($db->f("ticket_response_time") % 3600) / 60 ) ;
				$response["s"] = round(($db->f("ticket_response_time") % 3600) % 60 ) ;
                
				$db_new = new db_local();
               /*  $sql_phone = "SELECT ". TABLE_PHONE .".ac, "
					. TABLE_PHONE .".cc, "
					. TABLE_PHONE .".p_number "
					." FROM ". TABLE_PHONE.
					" WHERE ". TABLE_PHONE .".phone_of = '".$db->f("ticket_creator_uid")."' " 
					." AND ". TABLE_PHONE .".p_is_preferred = '1' " 
					." AND ". TABLE_PHONE .".table_name='".TABLE_CLIENTS."'"; */
							
				$sql1 = "SELECT ".TABLE_CLIENTS.".mobile1,".TABLE_CLIENTS.".mobile2 
				FROM ".TABLE_CLIENTS." WHERE ".TABLE_CLIENTS.".user_id='".$db->f("ticket_creator_uid")."'" ;
                $db_new->query($sql1);        
                $db_new->next_record();
				$mobile1=$mobile2='';
				while($db_new->next_record()){
					$mobile1 = $db->f("mobile1");
					$mobile2 = $db->f("mobile2");
				}
				$db_new->close();
				
                $attachmentArr='';
                $attachment_imap = $db->f("attachment_imap") ; 
                if(!empty($attachment_imap)){
                    $attachment_imap = trim($attachment_imap,",");
                    $attachmentArr = explode(",",$attachment_imap);
                }
				
				$sent_inv_noArr='';
                $sent_inv_no = $db->f("sent_inv_no") ; 
                if(!empty($sent_inv_no)){
                    $sent_inv_no = trim($sent_inv_no,",");
                    $sent_inv_noArr = explode(",",$sent_inv_no);
                }
				$sent_inv_idArr='';
                $sent_inv_id = $db->f("sent_inv_id") ; 
                if(!empty($sent_inv_id)){
                    $sent_inv_id = trim($sent_inv_id,",");
                    $sent_inv_idArr = explode(",",$sent_inv_id);
                }
				
				$sent_pinv_noArr='';
                $sent_pinv_no = $db->f("sent_pinv_no") ; 
                if(!empty($sent_pinv_no)){
                    $sent_pinv_no = trim($sent_pinv_no,",");
                    $sent_pinv_noArr = explode(",",$sent_pinv_no);
                }
                $sent_pinv_idArr='';
                $sent_pinv_id = $db->f("sent_pinv_id") ; 
                if(!empty($sent_pinv_id)){
                    $sent_pinv_id = trim($sent_pinv_id,",");
                    $sent_pinv_idArr = explode(",",$sent_pinv_id);
                } 
 
                $path_attachment_imap = DIR_WS_CST_FILES ;
				$ticketThreads[] = array(	"ticket_no" 			=> $db->f("ticket_no"), 
											"ticket_id" 		=> $db->f("ticket_id"), 
											"ticket_subject" 		=> $db->f("ticket_subject"), 
											"display_name" 		=> $db->f("display_name"), 
                                            "on_behalf" 		=> $db->f("on_behalf"), 
											"mail_send_to_su" 		=> chunk_split($db->f("mail_send_to_su")), 
											"ticket_text" 			=> $db->f("ticket_text"), 
											"ticket_creator" 		=> $db->f("ticket_creator"), 
											"ticket_attachment" 	=> $db->f("ticket_attachment"), 
											"ticket_attachment_1" 	=> $db->f("ticket_attachment_1"), 
											"ticket_attachment_2" 	=> $db->f("ticket_attachment_2"), 
											"template_id" 	=> $db->f("template_id"), 
											"template_file_1" 	=> $db->f("template_file_1"), 
											"template_file_2" 	=> $db->f("template_file_2"), 
											"template_file_3" 	=> $db->f("template_file_3"), 
											"ss_id" 	=> $db->f("ss_id"), 
											"ss_file_1" 	=> $db->f("ss_file_1"), 
											"ss_file_2" 	=> $db->f("ss_file_2"), 
											"ss_file_3" 	=> $db->f("ss_file_3"), 
											"followup_pinv_str" 	=> $db->f("followup_pinv_str"), 
											"followup_inv_str" 	=> $db->f("followup_inv_str"), 
											"ticket_attachment_path"=> $db->f("ticket_attachment_path"),
											"ticket_response_time" 	=> $response["h"] ." hr(s) : "
																		. $response["m"] ." min(s) : " 
																		. $response["s"]." sec(s) ",
											"ticket_date" 	=> date("d M Y H:i:s",$db->f("ticket_date")),
											"mobile1" 			=> $mobile1,
											"mobile2" 			=> $mobile2,
                                            "sms_to" 			=> chunk_split($db->f("sms_to")),
											"sms_to_number" 	=>chunk_split( $db->f("sms_to_number")),
											"cc" 			=> $db_new->f("cc"),
											"ac" 			=> $db_new->f("ac"),
											"p_number" 		=> $db_new->f("p_number"),
											"to_email_1" 	=> $db->f("to_email_1"),
											"cc_email_1" 	=> $db->f("cc_email_1"),
											"bcc_email_1" 	=> $db->f("bcc_email_1"),
											"to_email" 	=> $db->f("to_email"),
											"cc_email" 	=> $db->f("cc_email"),
											"bcc_email" 	=> $db->f("bcc_email"),
											"from_email" 	=> $db->f("from_email"),
											"status" 	=> $db->f("status"),
											"ip" 	=> $db->f("ip"),
											"hrs" 	=> $db->f("hrs"),
											"min" 	=> $db->f("min"),
											"hrs1" 	=> $db->f("hrs1"),
											"min1" 	=> $db->f("min1"),
											"ip" 	=> $db->f("ip"),
											"headers_imap" 	=> $db->f("headers_imap"),
											"path_attachment_imap" 	=> $path_attachment_imap,
											"attachmentArr" 	=> $attachmentArr,
											"sent_inv_idArr" 	=> $sent_inv_idArr,
											"sent_inv_noArr" 	=> $sent_inv_noArr,
											"sent_pinv_idArr" 	=> $sent_pinv_idArr,
											"sent_pinv_noArr" 	=> $sent_pinv_noArr
										);
			}
           // print_R($ticketThreads);
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
			if(!isset($posted_data['executive_id'])){
				/*  $randomUser = getRandomAssociate();
					$posted_data['executive_details']= $randomUser['name']." ( ".$randomUser['number']." )";
					$posted_data['executive_id'] = $randomUser['user_id'] ; 
				*/
				$posted_data['executive_details']= CEO_USER_NAME;
				$posted_data['executive_id'] = CEO_USER_ID;
			}
			$variables['client_sendmail'] = Clients::SENDMAILNO;
            $variables['ticket_closed'] = CeoTicket::CLOSED;
			$variables['type_cst'] = SupportTicketTemplate::CST;
            $variables['can_add_cmt_after_closed'] = false;
            $variables['can_update_status'] = false;
            $variables['can_view_client_details']  = false;
            $variables['can_view_headers']  = false;
            $variables['can_view_reply_email']  = false;             
            if ( $perm->has('nc_st_add_cmt_after_closed') ) {
                $variables['can_add_cmt_after_closed'] = true;
            }            
            if ( $perm->has('nc_st_update_status') ) {
                $variables['can_update_status'] = true;
            }
            if ( $perm->has('nc_st_headers') ) {
                $variables['can_view_headers'] = true;
            }
            if ( $perm->has('nc_uc_contact_details') ) {           
                $variables['can_view_client_details'] = true;
            }
            if ( $perm->has('nc_st_reply_email') ) {
                $variables['can_view_reply_email'] = true;
            }           
            //$hidden[] = array('name'=> 'perform','value' => 'view');
            $hidden[] = array('name'=> 'perform','value' => $perform);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'ticket_id', 'value' => $ticket_id);
            $hidden[] = array('name'=> 'invoice_id', 'value' => $invoice_id);
            $hidden[] = array('name'=> 'invoice_profm_id', 'value' => $invoice_profm_id);
            $hidden[] = array('name'=> 'flw_ord_id', 'value' => $flw_ord_id);
            $hidden[] = array('name'=> 'mticket_no', 'value' => $mticket_no);            
            $hidden[] = array('name'=> 'client_inv', 'value' => $client_inv);            
            $hidden[] = array('name'=> 'x', 'value' => $x);    
			$hidden[] = array('name'=> 'ajx','value' => $ajx);			
            $hidden[] = array('name'=> 'main_client_name', 'value' => $main_client_name);            
            $hidden[] = array('name'=> 'billing_name', 'value' => $billing_name);                   
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
			$page["var"][] = array('variable' => 'flw_tds', 'value' => 'flw_tds');
			$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'ticket_id', 'value' => 'ticket_id');
			$page["var"][] = array('variable' => 'invoice_id', 'value' => 'invoice_id');
			$page["var"][] = array('variable' => 'invoice_profm_id', 'value' => 'invoice_profm_id');
			$page["var"][] = array('variable' => 'flw_ord_id', 'value' => 'flw_ord_id');
			$page["var"][] = array('variable' => 'orderArr', 'value' => 'orderArr');
			$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
			$page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
            $page["var"][] = array('variable' => 'pendingInvlist', 'value' => 'fpendingInvlist');
            $page["var"][] = array('variable' => 'fpendingPInvlist', 'value' => 'fpendingPInvlist');
            $page["var"][] = array('variable' => 'totalBalanceAmount', 'value' => 'totalBalanceAmount');
            $page["var"][] = array('variable' => 'totalBalanceAmountP', 'value' => 'totalBalanceAmountP');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'posted_data', 'value' => 'posted_data');
            $page["var"][] = array('variable' => 'additionalEmailArr', 'value' => 'additionalEmailArr');
            $page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
            $page["var"][] = array('variable' => 'ticketThreads', 'value' => 'ticketThreads');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'ceo-ticket-view.html');
        //}
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>