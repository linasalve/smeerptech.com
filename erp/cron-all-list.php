<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    //error_reporting(E_ALL);
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    include_once ( DIR_FS_NC ."/header.php");

    if ( $perm->has('nc_cron_list') ){ ///nc_cron_list
        
		$action='All CRON - List ';
		$list = array(
			'IMAP CRONS'=>array(
				'1'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-imap-support-ticket.php',
						'file_title'=>'Create Support Ticket from Mail',
						'file_details'=>'support@smeerptech.com - To create support ticket from email'
					),
				'2'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-imap-bank-ticket.php',
						'file_title'=>'Create Accounts - Ticket from Mail',
						'file_details'=>'accounts@smeerptech.com - To create accounts ticket from email'
					),
				'3'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-imap-prospects-ticket.php',
						'file_title'=>'Create Sales - Ticket from Mail',
						'file_details'=>'sales@smeerptech.com - To create sales ticket from email'
					),
				'4'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-imap-transactions.php',
						'file_title'=>'Create Accounts - Ticket from Mail',
						'file_details'=>'autotran@smeerptech.com - To create transaction from email'
					),
				'5'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-imap-task-reminder.php',
						'file_title'=>'Create Task from Mail',
						'file_details'=>'autotask@smeerptech.com - Send mail on autotask@smeerptech.com along with the member\'s email-id'
					),
				'6'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-imap-bills.php',
						'file_title'=>'Create Purchase Entry',
						'file_details'=>'autobill@smeerptech.com - Send mail on autobill@smeerptech.com'
					),
				'6'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-imap-ceo-task.php',
						'file_title'=>'Create CEO task',
						'file_details'=>'autoceotask@smeerptech.com. Not working due to email id. To do it working change the Email-id & password'
					)					
				),
			'DAY MIS REPORTS'=>array(
				'7'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-day-task-schedular-comment.php',
						'file_title'=>'View Today\'s All Comments of Tasks',
						'file_details'=>''
					),
				'8'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-day-ceo-prospect-communication.php',
						'file_title'=>'View Today\'s All Sales Communication',
						'file_details'=>''						
					),
				'9'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-day-client-communication.php',
						'file_title'=>'View Today\'s Client Communication List',
						'file_details'=>'' 
					),	
				'10'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-visitor-book-day.php',
						'file_title'=>'View Today\'s All Visitors List',
						'file_details'=>''
					),
				'11'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-calls-in-day.php',
						'file_title'=>'View Today\'s Calls IN',
						'file_details'=>'View Today\'s Calls IN List'
					),
				'12'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-calls-out-day.php',
						'file_title'=>'View Today\'s Calls Out List',
						'file_details'=>''
						
					),
				'13'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-inward-day.php',
						'file_title'=>'View Today\'s In-Ward List',
						'file_details'=>''
						
					),
				'14'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-outward-day.php',
						'file_title'=>'View Today\'s Outward List',
						'file_details'=>''
					),	
				'15'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-payment-in-day.php',
						'file_title'=>'View Today\'s IN Transaction ',
						'file_details'=>''
					),
				'16'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-payment-out-day.php',
						'file_title'=>'View Today\'s OUT Transaction ',
						'file_details'=>'' 
					),
				'17'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-payment-internal-day.php',
						'file_title'=>'View Today\'s INTERNAL Transaction',
						'file_details'=>'' 
					),
				'18'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-orders-team-timesheet-day.php',
						'file_title'=>'Today\'s Total working HRS of team',
						'file_details'=>''
					),
				'19'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-day-workupdate.php',
						'file_title'=>'View Today\'s Order Work-update',
						'file_details'=>''
						
					),
				'20'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-orders-team-workreport-day.php',
						'file_title'=>'View Today\'s Team Reporting on each Project',
						'file_details'=>''
						
					),
				'21'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-day-invoice-followup.php',
						'file_title'=>'View Today\'s All Invoice Followups',
						'file_details'=>'' 
					), 
				'22'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-receipt-day.php',
						'file_title'=>'View Today\'s All Created Receipt',
						'file_details'=>'' 
					),
				'23'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-invoice-day.php',
						'file_title'=>'View Today\'s All Created Invoice',
						'file_details'=>''
					),
				'24'=>array(						
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-scores-day.php',
						'file_title'=>'View Today\'s All Scores',
						'file_details'=>''
					),
				'25'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-attendance-day.php',
						'file_title'=>'View Today\'s All Attendance',
						'file_details'=>''
					),
				'26'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-order-day.php',
						'file_title'=>'View Today\'s All Created Orders',
						'file_details'=>''
					),
				'27'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-all-tasks-added-today.php',
						'file_title'=>'View All Task Added Today',
						'file_details'=>''
					)
				),
		'PENDING MIS REPORTS'=>array(
				'28'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-pending-bills.php',
						'file_title'=>'List - Pending Purchase Bills',
						'file_details'=>''
					), 
				'29'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-pending-invoice-from-proforma.php',
						'file_title'=>'List - Pending Invoices to be Created from Proforma',
						'file_details'=>''
					),	 
				'30'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-pending-receipt-from-proforma.php',
						'file_title'=>'List - Proforma whose Receipts Pending to Create ',
						'file_details'=>''
					),
				'31'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-payment-in-pending.php',
						'file_title'=>'List of Pending - IN - Transactions',
						'file_details'=>''
					), 
				'32'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-payment-out-pending.php',
						'file_title'=>'List of Pending - OUT Transactions',
						'file_details'=>'' 
					), 
				'33'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-payment-internal-pending.php',
						'file_title'=>'List of Pending - INTERNAL Transactions',
						'file_details'=>'' 
					),
				'34'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-pending-transactions.php',
						'file_title'=>'List - Pending Transactions',
						'file_details'=>''						
					), 
				'35'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-pending-linking-transactions.php',
						'file_title'=>'List Transaction - Pending For Linking',
						'file_details'=>''
					), 
				'36'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-invoice-expiry-auto-order-create.php',
						'file_title'=>'Create Auto Order of Expiring Orders',
						'file_details'=>''
					), 				
				'37'=>array(						
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-tasks-ceo-pending.php',
						'file_title'=>'CEO Task List - Pending',
						'file_details'=>''
					), 
				'38'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-tasks-imp-pending.php',
						'file_title'=>'View All CRITICAL Task - List',
						'file_details'=>''
					),
				
				'39'=>array(
						'cron_file'=>THIS_DOMAIN.'/lib/cron/cron-nc-invoice-expiry-reminder.php',
						'file_title'=>'Send the Invoice Expiry Reminder to Client',
						'file_details'=>''
					)
			    )
			);
         
        $page["var"][] = array('variable' => 'action', 'value' => 'action');
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        // PAGE = CONTENT_MAIN
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'cron-all-list.html');
		$page["section"][] = array('container'=>'CONTENT', 'page' => 'cron-all-list.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
	
	$s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>