<?php
if ( $perm->has('nc_p_pm_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        		        
		// Include the hardware book class.
		include_once (DIR_FS_INCLUDES .'/payment-mode.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Paymentmode::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_PAYMENT_MODE
                            ." SET ". TABLE_PAYMENT_MODE .".payment_mode = '". 	$data['payment_mode'] ."'"                                                              
                                	.",". TABLE_PAYMENT_MODE .".status = '". 	$data['status'] ."'"                                
                                	.",". TABLE_PAYMENT_MODE .".date = '". 		date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New payment mode entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate'])  && $messages->getErrorMessageCount() <= 0){
             header("Location:".DIR_WS_NC."/payment-manage-payment-mode.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-manage-payment-mode.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            header("Location:".DIR_WS_NC."/payment-manage-payment-mode.php?added=1");
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-manage-payment-mode-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-add-permission.html');
        
    }
?>
