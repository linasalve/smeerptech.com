<?php
    if ( $perm->has('nc_bill_ord_tax_csv') ) { 
        $_ALL_POST  = NULL;
        $data       = NULL;
        $messages_ask 	= new Messager;	// create the object for the Message class.
		
		
        $CSV = BillOrderTax::getRestrictions();
        
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);          
			$files       = processUserData($_FILES);
    		$ask_added      = 0;
			$ask_duplicate  = 0;
			$ask_empty      = 0;
			
            $extra = array( 'file'      => &$files,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );
			 
			 
            if ( BillOrderTax::validateUpload($data, $extra) ) {
				if(!empty($files['file_csv']["name"])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_csv']["name"]);
					$ext = $filedata["extension"] ; 
					//$attachfilename = $username."-".mktime().".".$ext ;
					$tax_file_name = mktime()."_".$files['file_csv']["name"] ;
					$data['file_csv'] = $tax_file_name;
					
					$ticket_attachment_path = DIR_WS_ST_FILES;
					
					if(copy($files['file_csv']['tmp_name'], 
						DIR_FS_TAX_FILES."/".$tax_file_name)){
						@chmod(DIR_FS_TAX_FILES."/".$tax_file_name, 0777);
					}
				} 
			
			
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $invalid  = 0;
                $empty      = 0;
                $index      = array();
                $handle     = fopen($_FILES['file_csv']['tmp_name'],"rb");
                $message = '';
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
					
					
                    if ( $row == 1 ) {
                        $num = count($data);
                        for ($i=0; $i < $num; $i++) {
                            $case = trim($data[$i]);
                            switch ($case) {
                                case ('RcptNo'): {
                                    $index['RcptNo'] = $i;
                                    break;
                                }
								case ('rcpt_amount'): {
                                    $index['rcpt_amount'] = $i;
                                    break;
                                }
                                case ('tax1_id'): {
                                    $index['tax1id'] = $i;
                                    break;
                                }
                               
                                case ('tax1_name'): {
                                    $index['tax1_name'] = $i;
                                    break;
                                }
                                case ('tax1_value'): {
                                    $index['tax1_value'] = $i;
                                    break;
                                }
                                case ('tax1_percent_value'): {
                                    $index['tax1_percent_value'] = $i;
                                    break;
                                }
                                case ('tax1_amount'): {
                                    $index['tax1_amount'] = $i;
                                    break;
                                }
                                case ('tax1_sub1_id'): {
                                    $index['tax1_sub1_id'] = $i;
                                    break;
                                }
                                case ('tax1_sub1_name'): {
                                    $index['tax1_sub1_name'] = $i;
                                    break;
                                }
                                case ('tax1_sub1_value'): {
                                    $index['tax1_sub1_value'] = $i;
                                    break;
                                } 
								case ('tax1_sub1_percent_value'): {
                                    $index['tax1_sub1_percent_value'] = $i;
                                    break;
                                }
                                case ('tax1_sub1_amount'): {
                                    $index['tax1_sub1_amount'] = $i;
                                    break;
                                }
								case ('tax1_sub2_id'): {
                                    $index['tax1_sub2_id'] = $i;
                                    break;
                                }
                                case ('tax1_sub2_name'): {
                                    $index['tax1_sub2_name'] = $i;
                                    break;
                                }
                                case ('tax1_sub2_value'): {
                                    $index['tax1_sub2_value'] = $i;
                                    break;
                                } 
								case ('tax1_sub2_percent_value'): {
                                    $index['tax1_sub2_percent_value'] = $i;
                                    break;
                                }
                                case ('tax1_sub2_amount'): {
                                    $index['tax1_sub2_amount'] = $i;
                                    break;
                                }                          
								case ('tax_paid_dt'): {
                                    $index['tax_paid_dt'] = $i;
                                    break;
                                }									
								case ('remarks'): {
                                    $index['remarks'] = $i;
                                    break;
                                }	
								
                            }
                        }
                    }
                    else {                 
                          $index= array (
							"RcptNo"=>0,
							"rcpt_amount"=>1,
							"tax1_id"=>2,
							"tax1_name"=>3,
							"tax1_value"=>4,
							"tax1_percent_value"=>5,
							"tax1_amount"=>6,
							"tax1_sub1_id"=>7,
							"tax1_sub1_name"=>8,
							"tax1_sub1_value"=>9,
							"tax1_sub1_percent_value"=>10,
							"tax1_sub1_amount"=>11,
							"tax1_sub2_id"=>12,
							"tax1_sub2_name"=>13,
							"tax1_sub2_value"=>14,
							"tax1_sub2_percent_value"=>15,
							"tax1_sub2_amount"=>16,
							"tax_paid_dt"=>17,
							"remarks"=>18
                           );
						   
                        if ( isset($index['RcptNo']) && $data[$index['RcptNo']] != '' ) {
                             $table = TABLE_BILL_ORD_TAX;
							 $cond =" WHERE ".TABLE_BILL_ORD_TAX.".rcpt_no ='".$data[$index['RcptNo']]."'";
                            if ( !BillOrderTax::duplicateFieldValue($db, $table, 'id', $cond) ) {
						
								//Get ord no and proforma no
								$dataArr=array();
								$table = TABLE_BILL_RCPT;
								$fields1 = TABLE_BILL_ORDERS.".id,".
								TABLE_BILL_RCPT .".id as rcpt_id,".
								TABLE_BILL_RCPT .".number as rcpt_no,".
								TABLE_BILL_RCPT .".inv_profm_id,".
								TABLE_BILL_RCPT.".inv_profm_no, ".TABLE_BILL_RCPT.".ord_no " ;
								$condition1 = " LEFT JOIN ".TABLE_BILL_ORDERS." ON 
								".TABLE_BILL_RCPT.".ord_no= ".TABLE_BILL_ORDERS.".number
								WHERE ".TABLE_BILL_RCPT .".number = '".$data[$index['RcptNo']]."' LIMIT 0,1" ;
								$dataArr = getRecord($table,$fields1,$condition1);
								if(!empty($dataArr)){
									$val=array();
									$val['or_id'] = $dataArr['id'];
									$val['ord_no'] = $dataArr['ord_no'];
									$val['inv_profm_id'] = $dataArr['inv_profm_id'];
									$val['inv_profm_no'] = $dataArr['inv_profm_no'];
									$val['rcpt_id'] = $dataArr['rcpt_id'];
									$val['rcpt_no'] = $dataArr['rcpt_no'];
						$total_tax_paid_amount=0;
					$total_tax_paid_amount=$data[$index['tax1_amount']] +  $data[$index['tax1_sub1_amount']] + $data[$index['tax1_sub2_amount']];
						
						$dt=$data[$index['tax_paid_dt']];
						$dtArr = explode('/', $dt);
						$tax_paid_dt = $dtArr[2]."-".$dtArr[1]."-".$dtArr[0]." 00:00:00";
						
					echo $query	= " INSERT INTO ".TABLE_BILL_ORD_TAX
					." SET ".TABLE_BILL_ORD_TAX .".or_no = '".$val['ord_no']."'" 
					.",". TABLE_BILL_ORD_TAX .".or_id = '".$val['or_id']."'"  
					.",". TABLE_BILL_ORD_TAX .".inv_profm_no = '".$val['inv_profm_no']."'"  
					.",". TABLE_BILL_ORD_TAX .".inv_profm_id= '".$val['inv_profm_id']."'"   
					.",". TABLE_BILL_ORD_TAX .".rcpt_no= '".$val['rcpt_no']."'"   
					.",". TABLE_BILL_ORD_TAX .".rcpt_id= '".$val['rcpt_id']."'"   
					.",". TABLE_BILL_ORD_TAX .".rcpt_amount= '".processUserData($data[$index['rcpt_amount']])."'"   
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_id= '".processUserData($data[$index['tax1_id']])."'"   
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_name 
						= '".processUserData($data[$index['tax1_name']])."'"   
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_value
						= '".processUserData($data[$index['tax1_value']])."'"   
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_pvalue
						= '".processUserData($data[$index['tax1_percent_value']])."'"   
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_total_amount
						= '".processUserData($data[$index['tax1_amount']])."'" 
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub1_id
						= '".processUserData($data[$index['tax1_sub1_id']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub1_name
						= '".processUserData($data[$index['tax1_sub1_name']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub1_value
						= '".processUserData($data[$index['tax1_sub1_value']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub1_pvalue
						= '".processUserData($data[$index['tax1_sub1_percent_value']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub1_total_amount
						= '".processUserData($data[$index['tax1_sub1_amount']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub2_id 
						= '".processUserData($data[$index['tax1_sub2_id']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub2_name
						= '".processUserData($data[$index['tax1_sub2_name']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub2_value
						= '".processUserData($data[$index['tax1_sub2_value']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub2_pvalue
						= '".processUserData($data[$index['tax1_sub2_percent_value']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".rcpt_tax1_sub2_total_amount 
						= '".processUserData($data[$index['tax1_sub2_amount']])."'"   		
					.",". TABLE_BILL_ORD_TAX .".total_tax_paid_amount 
						= '".$total_tax_paid_amount."'"   		
					.",". TABLE_BILL_ORD_TAX .".tax_paid_dt= '".$tax_paid_dt."'"				     
					.",". TABLE_BILL_ORD_TAX .".tax_file_name= '".$tax_file_name."'"				     
					.",". TABLE_BILL_ORD_TAX .".paid_remarks='".processUserData($data[$index['remarks']])."'" 
					.",". TABLE_BILL_ORD_TAX .".paid_status= '1'"   		
					.",". TABLE_BILL_ORD_TAX .".added_by= '". $my['user_id']."'"   		
					.",". TABLE_BILL_ORD_TAX .".added_by_name= '". $my['f_name']." ".$my['l_name']."'"   		
					.",". TABLE_BILL_ORD_TAX .".do_e  = '". 	date('Y-m-d H:i:s')."'";
							  
					$db->query($query);					
					 
			$sqlu = " UPDATE ".TABLE_BILL_ORDERS 
			." SET total_tax_balance_amount = (total_tax_balance_amount - '".$total_tax_paid_amount ."'),"
			." total_tax_paid_amount =( total_tax_paid_amount + '".$total_tax_paid_amount ."'),"            
			." tax_file_name = CONCAT( tax_file_name, ',".$tax_file_name."' )"   
			//." proforma_invoice_no = CONCAT(proforma_invoice_no,',".$proforma_invoice['number']."')"
			." WHERE id = ".$val['or_id'];				
									$db->query($sqlu);
									$added += 1;
									$messages->setOkMessage('Added Receipt No. -'.$data[$index['RcptNo']]);
									
									//Update same data in receipt table bof
									$sqlu1 =" UPDATE ".TABLE_BILL_RCPT 
	." SET ". TABLE_BILL_RCPT .".rcpt_tax1_id= '".processUserData($data[$index['tax1_id']])."'"   
	.",". TABLE_BILL_RCPT .".rcpt_tax1_name = '".processUserData($data[$index['tax1_name']])."'"   
	.",". TABLE_BILL_RCPT .".rcpt_tax1_value = '".processUserData($data[$index['tax1_value']])."'"   
	.",". TABLE_BILL_RCPT .".rcpt_tax1_pvalue= '".processUserData($data[$index['tax1_percent_value']])."'"   
	.",". TABLE_BILL_RCPT .".rcpt_tax1_total_amount = '".processUserData($data[$index['tax1_amount']])."'" 
	.",". TABLE_BILL_RCPT .".rcpt_tax1_sub1_id	= '".processUserData($data[$index['tax1_sub1_id']])."'"   		
	.",". TABLE_BILL_RCPT .".rcpt_tax1_sub1_name = '".processUserData($data[$index['tax1_sub1_name']])."'"   	
	.",". TABLE_BILL_RCPT .".rcpt_tax1_sub1_value='".processUserData($data[$index['tax1_sub1_value']])."'"   	
	.",".TABLE_BILL_RCPT.".rcpt_tax1_sub1_pvalue='".processUserData($data[$index['tax1_sub1_percent_value']])."'"   
	.",". TABLE_BILL_RCPT.".rcpt_tax1_sub1_total_amount= '".processUserData($data[$index['tax1_sub1_amount']])."'"    .",". TABLE_BILL_RCPT .".rcpt_tax1_sub2_id = '".processUserData($data[$index['tax1_sub2_id']])."'"   		
	.",".TABLE_BILL_RCPT.".rcpt_tax1_sub2_name = '".processUserData($data[$index['tax1_sub2_name']])."'"   		
	.",". TABLE_BILL_RCPT .".rcpt_tax1_sub2_value='".processUserData($data[$index['tax1_sub2_value']])."'"   	
	.",".TABLE_BILL_RCPT.".rcpt_tax1_sub2_pvalue='".processUserData($data[$index['tax1_sub2_percent_value']])."'"   
	.",".TABLE_BILL_RCPT.".rcpt_tax1_sub2_total_amount='".processUserData($data[$index['tax1_sub2_amount']])."'"   	  
	.",". TABLE_BILL_RCPT .".total_tax_paid_amount = '".$total_tax_paid_amount."'"   		
	.",". TABLE_BILL_RCPT .".tax_paid_dt= '".$tax_paid_dt."'"				     
	.",". TABLE_BILL_RCPT .".tax_file_name= '".$tax_file_name."'"				     
	.",". TABLE_BILL_RCPT .".paid_remarks='".processUserData($data[$index['remarks']])."'"   		
	.",". TABLE_BILL_RCPT .".tax_doe= '".date('Y-m-d H:i:s')."'"   		
	." WHERE ".TABLE_BILL_RCPT.".number = '".$val['rcpt_no']."'";	
									
									$db->query($sqlu1);
									//Update same data in receipt table eof	
									
									
									
									
									
								}else{
									$invalid += 1;
									$messages->setErrorMessage('Not Get All details of Receipt No. 
									-'.$data[$index['RcptNo']]);
								}	
									
							}else{
								$duplicate += 1;
								$messages->setErrorMessage('Check Receipt No. -'.$data[$index['RcptNo']]);
							}
							 
                        }else{
                            $empty += 1;
                        } 
                        // The Row is not the First row.
                        
                    }                    
                }
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been added.');
                if ($duplicate)
                    $messages->setOkMessage($duplicate .' out of '. $row .' records were duplicate and neglected.');
                if ($invalid)
                    $messages->setOkMessage($invalid .' out of '. $row .' email invalid records found whose other data not found. <br/>'.$message);
				if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
        }
        
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$condition ='';
            include ( DIR_FS_NC .'/bill-order-tax-list.php');
        }else{
			
			 // Set the Permissions.
			$variables['can_add']     = false;
			$variables['can_view_list']     = false;
			if ( $perm->has('nc_bill_ord_tax_list') ) {
				$variables['can_view_list']     = true;
			}
			if ( $perm->has('nc_bill_ord_tax_csv') ) {
				$variables['can_add']     = true;
			}
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'csv_upload');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-tax-csv.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>