<?php
    // if ( $perm->has('nc_ss_list') ) {        
        include (DIR_FS_NC .'/score-sheet-board.php');

		if ( $perm->has('nc_ss_all_list') ) {
		 
		}elseif ( $perm->has('nc_ss_mylist') ) {
		 
			if(empty($condition_query)){
			
				$condition_query .= " WHERE ( ".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$my['user_id'].",%' 
					OR ".TABLE_SCORE_SHEET.".created_by ='".$my['user_id']."')";
			}else{
			 
				$condition_query .= " AND ( ".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$my['user_id'].",%' 
					OR ".TABLE_SCORE_SHEET.".created_by ='".$my['user_id']."')";
			}
		 
		}
		
		
		 $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
            $_SEARCH['search_by']=1;
        }
        $_SEARCH['searched'] = 1;
		
        // To count total records.
        $list	= 	NULL;
        if( isset($_SEARCH['search_by']) && $_SEARCH['search_by'] ==1){        
            $condition_query = " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_SCORE_SHEET .".created_by ".$condition_query ;
                        
        }elseif(isset($_SEARCH['search_by']) && $_SEARCH['search_by'] ==2){        
            $condition_query = " LEFT JOIN ". TABLE_USER
                        ." ON ". TABLE_USER .".user_id = ". TABLE_SCORE_SHEET .".comment_to ".$condition_query ;                        
        }
       
		
        $total	=	Scoresheet::getDetails( $db, $list, '', $condition_query);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
        $list	= NULL;
        $fields = TABLE_SCORE_SHEET .'.*';       
        Scoresheet::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        $reasonArr = array();
		$reasonArr = Scoresheet::getReason();
       
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               $executive=array();
               $string = str_replace(",","','", $val['comment_to']);
               $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
               User::getList($db,$executive,$fields1,$condition1);
               $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['comment_to'] = $executivename ;
               
               $exeCreted=$exeCreatedname='';
               $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
               User::getList($db,$exeCreted,$fields1,$condition2);
              
               foreach($exeCreted as $key2=>$val2){
                    $exeCreatedname .= $val2['f_name']." ".$val2['l_name']."<br/>" ;
               } 
               $val['created_by'] = $exeCreatedname;             
               
               if(!empty($val['reason_id'])){
                   $val['reason_title']= $reasonArr[$val['reason_id']];
               }
               //HTML CODE BOF
			    
			   
               //HTML CODE EOF
               $fList[$key]=$val;
            }
        }
      
        
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_add_followup']  = false;
       	$variables['can_view_hname']  	= false;
       	$variables['can_view_hcomment'] = false;
       	$variables['can_print'] = false;
       
        if ( $perm->has('nc_ss_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_ss_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_ss_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_ss_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_ss_details') ) {
            $variables['can_view_details'] = false;
        }
        if ( $perm->has('nc_ss_add_comment') ) {
            $variables['can_add_followup'] = true;
        }
        if ( $perm->has('nc_ss_hname') ) {
            $variables['can_view_hname'] = true;
        }
        if ( $perm->has('nc_ss_hcomment') ) {
            $variables['can_view_hcomment'] = true;
        }
        if ( $perm->has('nc_ss_print') ) {
            $variables['can_print'] = true;
        }
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN

        
   /*  }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    } */
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
	<td align="center" style="padding-top:1px;padding-bottom:5px">
  		<!-- table at the bottom in the body  -->
		<table cellpadding="1" cellspacing="0" border="0" width="100%" id="search">
		<tr>
            <td>
                <table cellpadding="1" cellspacing="0" border="0" width="100%">
                <tr>
                    <td style="padding: 2px 2px 0px 2px;text-align:left;" width="300px"><span class="pre_color txt12 bold">Learning @SMEERP</span></td>
                    <td style="padding: 2px 2px 0px 2px;text-align:right;">
						<table cellpadding="0" cellspacing="0" border="0">
						<tr>
						<?php 
						if( $variables['can_add'] == '1'){ 
						?>
							<td><a href="<?php echo $variables['nc']."/learning.php?perform=add"?>" title="Add New"	class="link_button addnew">Add New</a> </td>
						<?php 
						} 
						if( $variables['can_view_list'] == '1'){ 
						?> 
							<td><a href="<?php echo $variables['nc']."/learning.php?perform=list";?>" title="List"  class="link_button addnew">Show All</a> </td>
						<?php 
						} 
						if( $variables['can_view_list'] == '1'){ 
						?>
							<td><a href="#" onclick="javascript: toggleVisibilityOfElement( document.getElementById('tableSearch') );" title="Search" class="link_button addnew">Filter</a></td>
						<?php 
						}
						?>
						</tr>
						</table> 
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td  width="100%"  align="left">
				<form name="frmSearch" action="learning.php" method="post">
                <!-- BOF :: Table search -->
				<table border="0" align="left" cellpadding="2" cellspacing="2" id="tableSearch"  width="100%"
                        class="<?php if ($_SEARCH['searched']){ echo 'visible'; }else{ echo 'hidden'; } ?>">
				<tr>
                    <td class="table_text" valign="top">
                        Search
                        <input type="text" name="sString" value="<?php echo $_SEARCH['sString'] ; ?>" class="inputbox" size="25"/>

                        &nbsp;&nbsp;in
                        <select name="sType" class="inputbox">
						<?php 
						foreach($sTypeArray as $key=>$sTypeSub){
							foreach($sTypeSub as $key1=>$sType){ 
								if($_SEARCH['sType']==$sType){
									$selected =  "selected=\"selected\"" ;
								}
								echo "<option value='".$sType."' ".$selected.">".$key1."</option>";
							   
							}
						}
						?>
						</select>

                        &nbsp;&nbsp;Sort using
						
                        <select name="sOrderBy" class="inputbox">
                        <?php
						foreach($sOrderByArray as $key=>$sOrderBySub){
							foreach($sOrderBySub as $key1=>$sOrderBy){
								if($_SEARCH['sOrderBy']==$sOrderBy){
									$selected =  "selected=\"selected\"" ;
								}
								echo "<option value='".$sOrderBy."'".$selected.">".$key1."</option>";
                            }
                        }
						?>
                        </select>

                        &nbsp;&nbsp;Mode
                        <select name="sOrder" class="inputbox">
						<?php
							if($_SEARCH['sOrder']=='ASC'){
								$selected_asc =  "selected=\"selected\"" ;
							}
							if($_SEARCH['sOrder']=='DESC'){
								$selected_desc =  "selected=\"selected\"" ;
							}
                            echo "<option value='ASC'".$selected_asc.">ASC</option>
                            <option value='DESC'".$selected_desc.">DESC</option>";
						?>
                        </select>
                    </td>
				 </tr>
				 <!--
				 <tr>
					<td>
					   Score To &nbsp;<input  autocomplete="off"  type="text" id="off_exp_issue_to_name" name="off_exp_issue_to_name" style="width:250px;" class="inputbox" value="<?php // echo $_SEARCH['off_exp_issue_to_name'];?>"  size="60"/>
						<input type="text" id="executive_to" name='executive_to' value="<?php //echo $_SEARCH['executive_to']; ?>" readonly="1" 
						style="font-size: 10px; width: 20px;"/>
						 <a href="javascript:void(0);" name="clear12" onclick="javascript:clearAutoFields(document.frmSearch.off_exp_issue_to_name,document.frmSearch.executive_to)">
							clear</a>
						<div id="das_off_exp_issue_to_name" style="position: relative;left:0px; top: 2px; width: 200px;">
						</div>                 
						<script type="text/javascript">
						var options1 = {
							script:"<?php //echo $variables['nc']."/get-users.php?json=true&";?>",
							varname:"off_exp_issue_to_name",                        
							json:true,
							callback: function (obj) {document.getElementById('executive_to').value = obj.id; 
							}
						};
						var as_json = new AutoSuggest('off_exp_issue_to_name', options1);   
						</script> 
					</td>				 
				 </tr>-->
				 <tr>
					<td>
					   Member By &nbsp;<input  autocomplete="off"  type="text" id="off_exp_issue_by_name" name="off_exp_issue_by_name" style="width:250px;" class="inputbox" value="<?php echo $_SEARCH['off_exp_issue_by_name'] ;?>"  size="60"/>
						<input type="text" id="executive_by" name='executive_by' value="<?php echo $_SEARCH['executive_by'];?>" readonly="1" style="font-size: 10px; width: 20px;"/>
						 <a href="javascript:void(0);" name="clear12" onclick="javascript:clearAutoFields(document.frmSearch.off_exp_issue_by_name,document.frmSearch.executive_by)">
							clear</a>
						<div id="das_off_exp_issue_by_name" style="position: relative;left:0px; top: 2px; width: 200px;">
						</div>      
						<script type="text/javascript">
						var options1 = {
							script:"<?php echo $variables['nc']."/get-users.php?json=true&";?>",
							varname:"off_exp_issue_by_name",                        
							json:true,
							callback: function (obj) {document.getElementById('executive_by').value = obj.id; 
							}
						};
						var as_json = new AutoSuggest('off_exp_issue_by_name', options1);   
						</script>
						 
					 
					</td>				 
				 </tr>
                 <!--
				 <tr>
                    <td>
                       <div style="float:left;">
							<input type="checkbox" name="search_by" value="1"{if $_SEARCH.search_by == '1'} checked="checked"{/if}
									onclick="javascript: return selectOnlyOne(this);"/> Search in Comment by
							<input type="checkbox" name="search_by" value="2"{if $_SEARCH.search_by == '2'} checked="checked"{/if}
									onclick="javascript: return selectOnlyOne(this);"/>Search in Comment to
							
							</div>
							
                        </div>
                    </td>
                </tr>
				
                <tr>
                    <td>
                        <input type="checkbox" name="chk_reason" value="AND"{if $_SEARCH.chk_reason == 'AND'} checked="checked"{/if}
                                onclick="javascript: return selectOnlyOne(this);"/>and 
                        &nbsp;Reason
                        </div>
                        
                        <select name="rReason[]" class="inputbox" multiple="multiple">
                            {foreach from=$variables.reason item=reason key=rkey}
                            {strip}<option value="{$rkey}" 
                                {foreach from=$_SEARCH.rReason item=rReason key=key}
                                {if ($rkey == $rReason) } selected="selected"{/if}
                                {/foreach}
                            >{$reason}</option>{/strip}
                            {/foreach}
                        </select>
                    </td>
                </tr>-->
                <tr>
                    <td class="table_text">
                         
                        <input type="checkbox" name="chk_date_from" value="AND" 
						<?php if($_SEARCH['chk_date_from'] == 'AND') echo "checked=\"checked\" " ;?>
                        onclick="javascript: return selectOnlyOne(this);"/>and&nbsp;
                        &nbsp; 
						<script>
							$(function() {		
								$( "#date_from" ).datepicker({
									changeMonth: true,
									changeYear: true,
									dateFormat: 'dd/mm/yy'
								});
								$( "#date_to" ).datepicker({
									changeMonth: true,
									changeYear: true,
									dateFormat: 'dd/mm/yy'
								});
							});
						</script>        
                        end after 
                        <input type="text" name="date_from" id="date_from" value="<?php echo $_SEARCH['date_from'];?>" class="inputbox" size="12" readonly="readonly" /> 
                        <!--(dd/mm/yyyy)--> 
                        &nbsp;&nbsp;
                        <input type="checkbox" name="chk_date_to" value="AND" <?php if($_SEARCH['chk_date_to'] == 'AND') "checked=\"checked\""; ?> onclick="javascript: return selectOnlyOne(this);"/>and&nbsp;&nbsp; 
                        end date before 
                        <input type="text" name="date_to" id="date_to" value="<?php echo $_SEARCH['date_to'];?>" class="inputbox" size="12" readonly="readonly" /> 
                        <!--(dd/mm/yyyy)-->
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="rpp" value="<?php echo $variables['rpp'] ; ?>" class="inputbox" 
                                size="5" style="text-align:center;" />&nbsp;records per page
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="hidden" name="perform" value="search"/>
                        <input type="submit" name="submit_search" value="Apply" class="header" />
                    </td>
                </tr> 
				</table>
				<!-- EOF :: End of Table Search -->	
                </form>
			</td>
		  </tr>
		</table>
		<!-- table at the bottom in the body  -->
  	</td>
</tr>
<tr>
    <td align="center"><?php include(DIR_FS_NC."/messages.php"); ?></td>
</tr>
<tr>
	<td align="center"> 
	<table cellpadding="1" cellspacing="1" border="0" align="left" width="100%"> 
		<tr>
			<td valign="middle" style="padding: 0px 0px 0px 0px;">
				<table cellpadding="1" cellspacing="1" border="0" align="center" class="list" width="100%">
					<tr class="header">
						<td class="header" width="10">#</td>
						<td class="header" width="100" title="Comment by"> Comment by</td>
						<!--<td class="header" width="100" title="Comment to">Comment to</td>   
						<td class="header" width="100" title="Reason">Reason</td> -->              
						<td class="header" width="200" title="Particulars">Particulars</td>
						<!--<td class="header" width="50" title="Score">Score</td>-->
						<td class="header" width="150" title="Last Comment">Last Comment</td>
						<td class="header" width="150" title="Last Comment By">Last Comment By</td>
						<td class="header" width="50" title="Date">Date</td>
						<td class="header" width="50" title="Action">Action</td>
					</tr>
				 
			<?php 
			if(!empty($fList)){
			 
				foreach($fList as $key=>$detail){
				?> 
				<tr id="row<?php echo $detail['id'];?>" class="<?php if ($variables['hid'] == $detail['id']){ echo "highlight "; }else{
						if(($key%2)==0){ echo "odd "; }else{ echo "even ";} echo "normaltext "; } ?>" onMouseOver="this.className='on_over normaltext';" 
						onMouseOut="this.className='<?php if($variables['hid'] == $detail['id']){ echo "highlight ";}else{ 
							if(($key%2)==0){ echo "odd "; }else{ echo "even "; } echo "normaltext" ; } ?>'">

					<td>
						<?php echo $sr=$key+1+($variables['x']-1)*$variables['rpp'] ;?>
					</td>
					<td>
						<?php
						if($detail['hide_my_name']=='1'){
							if($variables['can_view_hname'] == '1'){
								echo $detail['created_by']." <b>(Hidden)</b>";
							}else{
								echo "Hidden";
							}
						}else{
							echo $detail['created_by'];
						}
						?>
					</td>
					<?php /*
					<td>echo $detail['comment_to']</td>; 
					<td> echo $detail['reason_title'];?></td>*/
					?>
					<td>
						<?php 
						 	
							echo nl2br($detail['particulars']);
						 
						?>
					</td>
					<?php //<td echo $detail['score'];</td>?>
					<td><?php echo $detail['last_comment'];?></td>
					<td><?php echo $detail['last_comment_by_name'];?></td>
					<td>
						<?php
						if ($detail['date'] !='0000-00-00'){
							echo date('d M Y',strtotime($detail['date']));
						}?>
					</td>
				    <td align="center">
					<?php 
					if($variables['can_edit'] == '1'){
					?>
						<!--
						<div class="coloumn w25" style="height:20px;">
						<a href="<?php //echo $variables['nc']."/learning.php?perform=edit&id=".$detail['id'].$extra_url ;?>">
						<img src="<?php //echo $variables['nc_images']."/edit-on.gif";?>" border="0" title="Edit" name="Edit" alt="Edit"/></a>  
						</div> -->
					<?php 
					} 					 
					if($variables['can_view_details'] == '1'){
					?>
						<!-- BO: View Link -->
						<div class="coloumn w25" style="height:20px;">   
						<a href="javascript:void(0);" 
								onclick="window.open('<?php echo $variables['nc']."/learning.php?perform=view&id=".$detail['id'];?>','mywin','left=20,top=20,width=600,height=400,toolbar=0,resizable=1,scrollbars=1');"
								class="action" title="">
							<img src="<?php echo $variables['nc_images'].'/view-on.gif';?>" border="0" 
									title="View" name="View" alt="View" /></a>  
						</div>
						<!-- EO: View Link -->
					<?php } 
					if($variables['can_delete'] == '1'){
					?> 
						<!--
						<div class="coloumn w25" style="height:20px;">
						<a href="<?php //echo $variables['nc'].'/learning.php?perform=delete&id='.$detail['id'].$extra_url ;?>"
								onclick="javascript: return reallyDel('Are you sure you want to delete this record');" 
								class="action" title="Delete">
						<img src="<?php //echo $variables['nc_images'].'/delete-on.gif';?>" border="0" 
								title="Delete" name="Delete" alt="Delete" /></a>
						</div>
						-->
						<!-- EO: Delete Link -->
					<?php 
					}
					if($variables['can_add_followup'] == '1'){
					?> 
						<!-- BO: Add Followup Link -->
						<div class="coloumn w25" style="height:20px;">   
						<a href="javascript:void(0);" 
						onclick="window.open('<?php echo $variables['nc'].'/learning.php?perform=acomm&id='.$detail['id'];?>','mywin','left=20,top=20,width=600,height=400,toolbar=0,resizable=1,scrollbars=1');"
								class="action" title="">
							<img src="<?php echo $variables['nc_images']."/comment-on.gif";?>" border="0"  
									title="Add comment" name="Add comment" alt="Add comment" /></a>  
						</div>
					<?php 
					}else{
					?>
						<div class="coloumn w25" style="height:20px;">
							<img src="<?php echo $variables['nc_images'].'/comment-off.gif';?>" border="0" 
								title="Add comment" name="Add comment" alt="Add comment"/></a>
						</div>  
					<!-- EO: Add Followup Link -->  
					<?php 
					}
					if($variables['can_print'] == '1'){
					
					?>	
						<!--
						<a href="javascript:void(0);" 
							onclick="openSmallWindow('<?php //echo $variables['nc'].'/learning.php?perform=print&id='.$detail['id'];?>',700,600);"
							class="action" title="">
							<img src="<?php //echo $variables['nc_images'].'/print-on.gif'; ?>" border="0" 
							title="Print" name="Print" alt="Print"/>
						</a>  -->
					<?php 
					} 
					?>	
					</td>
				</tr>
				<?php
				}
			}else{
				?>
			
				<tr class="{cycle values='odd,even'}" >
					<td colspan="8" class="normaltext" align="center">There are no Records to display</td>
				</tr>
			<?php
			}
			?>
			   
				</table>
				 
			</td>
		</tr>
		<?php
		if(!empty($pagination)){
		?>
		<tr>
			<td id="pagination" align="center"><?php echo $pagination;?></td>
		</tr>
		<?php } ?>
	</table>	
	</td>
</tr>
</table>