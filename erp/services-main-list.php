<?php

    if ( $perm->has('nc_sr_list') ) {
    
    
    
        $sOrderBy =clearSearchString($sOrderBy,"-");
         if(empty($condition_query )){
            $condition_query =" AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'";
        }
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched'] = 1;
        // To count total records.
        $list	= 	NULL;
        $db1 		= new db_local;
        // To count total records.
        
        $condition_query1 = " LEFT JOIN ".TABLE_SETTINGS_SERVICES_DIVISION."  ON 
                            ".TABLE_SETTINGS_SERVICES.".ss_division = ".TABLE_SETTINGS_SERVICES_DIVISION.".id " ;
        $condition_query1 .= " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id= '0' ". $condition_query ;
        $fields = TABLE_SETTINGS_SERVICES.".ss_id, ".TABLE_SETTINGS_SERVICES.".ss_title,
        ".TABLE_SETTINGS_SERVICES.".ss_status,".TABLE_SETTINGS_SERVICES.".tax1_name,
        ".TABLE_SETTINGS_SERVICES.".tax1_value,".TABLE_SETTINGS_SERVICES.".ss_punch_line,
		".TABLE_SETTINGS_SERVICES.".is_renewable, 
		".TABLE_SETTINGS_SERVICES.".sequence_services, 
        ".TABLE_SETTINGS_SERVICES_DIVISION.".title as division";
        //$total	=	Services::getList( $db, $list, $fields, $condition_query1);    
        
        $query = "SELECT ".$fields." FROM ". TABLE_SETTINGS_SERVICES.$condition_query1;
        $db->query($query);   
        if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
                $ss_title=$db->f('ss_title');
                $sequence_services=$db->f('sequence_services');
                $ss_id=$db->f('ss_id');
                $division=$db->f('division');                    
                $tax1_name=$db->f('tax1_name');                    
                $tax1_value=$db->f('tax1_value');                    
                $ss_punch_line=$db->f('ss_punch_line');   
                $ss_status=$db->f('ss_status');   
                $is_renewable=$db->f('is_renewable');   
               
                $inUse=0;
                //get subservices count 
                $sql1= "SELECT id FROM ".TABLE_BILL_ORD_P." WHERE "
                  . TABLE_BILL_ORD_P .".s_id = '". $ss_id ."' LIMIT 0,1" ;
                
                $db1->query($sql1);    
                while ( $db1->next_record() ) { 
                    $inUse = $db1->f('id');   
                }                
                
                //get services count
                /*
                  $sql1= "SELECT count(*) as totalSer FROM ".TABLE_BILL_ORD_P." WHERE "
                  . TABLE_BILL_ORD_P .".s_id = '". $ss_id ."' " ;
                */
                /*
                echo   $sql1= "SELECT SUM(".TABLE_BILL_ORDERS.".amount) as tot_amount FROM ".TABLE_BILL_ORD_P." 
                    INNER JOIN ". TABLE_BILL_ORDERS ." ON  ". TABLE_BILL_ORDERS.".number = ". TABLE_BILL_ORD_P.".ord_no WHERE "
                  . TABLE_BILL_ORD_P .".s_id = '". $ss_parent_id ."' AND ".TABLE_BILL_ORDERS.".is_invoice_create = '1'
                  GROUP BY ".TABLE_BILL_ORD_P.".s_id " ;
                echo "<br/>";  
                  
                echo "<br/>";
                $tot_amount='';
                $db1->query($sql1);    
                while ( $db1->next_record() ) { 
                   echo $tot_amount=$db1->f('tot_amount');   
                }                 
                */
                 
                 
                $list[]= array(
                    "ss_id"=>$ss_id,
                    "ss_title"=>$ss_title,
					"sequence_services"=>$sequence_services,
                    "tax1_name"=>$tax1_name,
                    "tax1_value"=>$tax1_value,
                    "ss_punch_line"=>$ss_punch_line,
                    "division"=>$division,
                    "inUse"=>$inUse,
                   "is_renewable"=>$is_renewable,
                    "ss_status"=>$ss_status
                );
            }
        }
        
        
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .= "&rpp=".$rpp."&perform=".$perform;
        /*
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        $list	= NULL;
        $condition_query1 = " LEFT JOIN ".TABLE_SETTINGS_SERVICES_DIVISION."  ON 
                            ".TABLE_SETTINGS_SERVICES.".ss_division = ".TABLE_SETTINGS_SERVICES_DIVISION.".id " ;
        $condition_query1 .= " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id= '0' ". $condition_query ;
        $fields = TABLE_SETTINGS_SERVICES.'.*,'.TABLE_SETTINGS_SERVICES_DIVISION.".title as division";
        Services::getList( $db, $list,$fields, $condition_query1, $next_record, $rpp);
        */
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_sr_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_sr_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_sr_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_sr_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_sr_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_sr_status') ) {
            $variables['can_change_status']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        //$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'services-main-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>