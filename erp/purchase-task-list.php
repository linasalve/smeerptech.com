<?php
if ( $perm->has('nc_pr_ts_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    
   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    
    // To count total records.
     $condition_query = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".project_id ='".$or_id."' AND ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '0' ".$condition_query;
    $list	= 	NULL;
    $total	=	PurchaseTask::getList( $db, $list, '', $condition_query);

    //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
   
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $condition_url .="&perform=".$perform."&or_id=".$or_id."&actual_time=".$actual_time;
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
   /*  $fields = TABLE_PURCHASE_TASK_DETAILS.".id,".TABLE_PURCHASE_TASK_DETAILS.".title as task_title, ".		
	TABLE_PURCHASE_TASK_DETAILS.".details,".TABLE_PURCHASE_TASK_DETAILS.".last_updated, "
	.TABLE_PURCHASE_TASK_MODULE.".title as module_title,".TABLE_PROJECT_TASK_STATUS.".title as status_title," ;
    */ 
	$fields = TABLE_PURCHASE_TASK_DETAILS.".id,".TABLE_PURCHASE_TASK_DETAILS.".title as task_title, ".		
	TABLE_PURCHASE_TASK_DETAILS.".details,".TABLE_PURCHASE_TASK_DETAILS.".last_updated, "	 ;
   
	$fields .=TABLE_PURCHASE_TASK_DETAILS.".resolved_date,".TABLE_PURCHASE_TASK_DETAILS.".is_client,";
    $fields .=TABLE_PURCHASE_TASK_DETAILS.".hrs,".TABLE_PURCHASE_TASK_DETAILS.".min,";
    $fields .=TABLE_PURCHASE_TASK_DETAILS.".hrs1,".TABLE_PURCHASE_TASK_DETAILS.".min1,";
    $fields .=TABLE_PURCHASE_TASK_DETAILS.".added_by," ;
    $fields .=TABLE_PURCHASE_TASK_DETAILS.".added_by_name," ;
    $fields .=TABLE_PURCHASE_TASK_DETAILS.".last_comment," ;
    $fields .=TABLE_PURCHASE_TASK_DETAILS.".last_comment_by_name " ;
	
/*     $fields .= TABLE_PROJECT_TASK_PRIORITY.".title as priority_title, ".TABLE_PROJECT_TASK_TYPE.".title as type_title,".TABLE_PROJECT_TASK_RESOLUTION.".title as rs_title" ; */
    PurchaseTask::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    $flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
          if($val['is_client']=='1'){
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                $val['is_client'] = '1';
            }else{
                $table = TABLE_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                 $val['is_client']='0';
            }
            
            if($actual_time){
                $val['tothrs'] = strlen($val['hrs']) ==1 ? '0'.$val['hrs'] : $val['hrs'] ;
                $val['totmin'] = strlen($val['min']) ==1 ? '0'.$val['min'] : $val['min'] ;
            }else{
                  // show atual total time (timesheet * 3 ) total cost
                $val['tothrs'] = $val['hrs1'] ;
                $val['totmin'] = $val['min1'] ;
                $extraHr = (int) ($val['min1'] / 60 ) ;
                $min1 =  ($val['min1'] % 60 ) ;
                $val['tothrs'] =  $val['tothrs'] + $extraHr ;
                $val['totmin'] = $min1 ;
                
                $val['tothrs'] = strlen($val['tothrs']) ==1 ? '0'.$val['tothrs'] : $val['tothrs'] ;
                $val['totmin'] = strlen($val['totmin']) ==1 ? '0'.$val['totmin'] : $val['totmin'] ;
            }
            //Calculate total bugs
            $condition_query_bug = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PURCHASE_TASK_DETAILS.".task_id = '".$val['id']."'";
            $listbug	= 	NULL;
            $totalbug	=	PurchaseTask::getList( $db, $listbug, TABLE_PURCHASE_TASK_DETAILS.'.id', $condition_query_bug);
            $val['totalbugs']= $totalbug;
            //Calculate total pending bugs
            $condition_query_bugp ="
                    WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PURCHASE_TASK_DETAILS.".task_id = '".$val['id']."'
                    AND ".TABLE_PROJECT_TASK_RESOLUTION.".title !='".PurchaseTask::CLOSE."'";
            $listpbug	= 	NULL;
            $totalpendingbugs	=	PurchaseTask::getList( $db, $listpbug, TABLE_PURCHASE_TASK_DETAILS.'.id', $condition_query_bugp);
            $val['totalpendingbugs']= $totalpendingbugs;
             //Calculate taskwise hrs spent
            if($actual_time){          
                    $fields_tws = ' ( SUM('.TABLE_PURCHASE_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min) % 60 ) as totMin, 
				( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost ' ;
            }else{
                // show atual total time (timesheet * 3 ) total cost
                  $fields_tws = ' ( SUM('.TABLE_PURCHASE_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min1) DIV 60 ))  as totHr ' .', ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min1) % 60 ) as totMin,
				  ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost' ;
            
            }
            $condition_query_twh = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '0' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_client ='0'";
            $list_tws	= 	NULL;
          	PurchaseTask::getList( $db, $list_tws, $fields_tws, $condition_query_twh);           
         
            $val['tot_bhrs'] = '00';
            $val['tot_bmin'] = '00' ;
            $val['worked_task_cost'] = '0' ;
            
            if(!empty($list_tws[0]['totHr']) || !empty($list_tws[0]['totMin'])){
                $list_tws =$list_tws[0];
                $val['tot_thrs'] = strlen($list_tws['totHr']) ==1 ? '0'.$list_tws['totHr'] : $list_tws['totHr'] ; 
                $val['tot_tmin'] = strlen($list_tws['totMin']) ==1 ? '0'.$list_tws['totMin'] : $list_tws['totMin'] ; 
				$val['tot_tcost'] = $list_tws['tot_tcost'] ;
            }else{
				$val['tot_thrs'] = '00';
                $val['tot_tmin'] = '00' ;
				$val['tot_tcost'] = 0 ;
            }
            
            //Calculate bugwise hrs spent
            if($actual_time){
                $fields_bws = ' ( SUM('.TABLE_PURCHASE_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min) % 60 ) as totMin , 
				( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_bcost ' ;
            }else{
                // show atual total time (timesheet * 3 ) total cost
                $fields_bws = ' ( SUM('.TABLE_PURCHASE_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min1) % 60 ) as totMin,
				( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_bcost ' ;
            
            }
             $condition_query_bwh = " WHERE (".TABLE_PURCHASE_TASK_DETAILS.".task_id ='".$val['id']."' AND 
                                    ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '1' 
                                    AND ".TABLE_PURCHASE_TASK_DETAILS.".is_client ='0' ) OR 
                                    ".TABLE_PURCHASE_TASK_DETAILS.".parent_id IN (
                                            SELECT ".TABLE_PURCHASE_TASK_DETAILS.".id FROM ".TABLE_PURCHASE_TASK_DETAILS." WHERE 
                                            ".TABLE_PURCHASE_TASK_DETAILS.".task_id ='".$val['id']."' AND 
                                            ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '1' 
                                            AND ".TABLE_PURCHASE_TASK_DETAILS.".is_client ='0' 
                                      )";
            $list_bws	= 	NULL;
          
          	PurchaseTask::getList( $db, $list_bws, $fields_bws, $condition_query_bwh); 
          
            if(!empty($list_bws[0]['totHr']) || !empty($list_bws[0]['totMin'])){
                $list_bws =$list_bws[0];
                $val['tot_bhrs'] = strlen($list_bws['totHr']) ==1 ? '0'.$list_bws['totHr'] : $list_bws['totHr'] ; 
                $val['tot_bmin'] = strlen($list_bws['totMin']) ==1 ? '0'.$list_bws['totMin'] : $list_bws['totMin'] ; 
                $val['tot_bcost']= $list_bws['tot_bcost'];
            }else{
                $val['tot_bhrs'] = '00';
                $val['tot_bmin'] = '00' ;
				$val['tot_bcost']=0;
            }
			$val['worked_task_cost']= $val['tot_tcost'] + $val['tot_bcost'] ;
			$val['worked_task_cost']= ceil($val['worked_task_cost']);
            //Get Last comment bof
            //$val['last_comment']='';
            //$condition_query_tlc = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '0' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_client ='0' ORDER BY ".TABLE_PURCHASE_TASK_DETAILS.".id DESC Limit 0,1";
            $condition_query_tlc = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '0' ORDER BY ".TABLE_PURCHASE_TASK_DETAILS.".id DESC Limit 0,1";
            $list_tlc	= 	NULL;
            $fields_tlc = TABLE_PURCHASE_TASK_DETAILS.".details";
          	PurchaseTask::getList( $db, $list_tlc, $fields_tlc, $condition_query_tlc);           
            if(!empty($list_tlc)){
                $list_tlc=$list_tlc[0];
                $val['last_comment'] =  $list_tlc['details'] ;
            }
            
            //Get Last comment eof
           $flist[$key]=$val;
        }
    }
	$variables['can_view_cost'] = false;
    if ( $perm->has('nc_p_ts_cost') ) {
        $variables['can_view_cost'] = true;
    }
    // Set the Permissions.    
    $page["var"][] = array('variable' => 'list', 'value' => 'flist');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-task-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
