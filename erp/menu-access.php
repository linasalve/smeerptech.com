<?php

$variables['address_book']=false;
$variables['address_book_list']=false;
$variables['address_book_add']=false;

$variables['billing']='0';
$variables['invoice']=false;
$variables['invoice_list']=false;

$variables['receipt']=false;
$variables['receipt_list']=false;

$variables['currency']=false;
$variables['currency_list']=false;
$variables['currency_add']=false;

$variables['service_division']=false;
$variables['service_division_list']=false;
$variables['service_division_add']=false;

$variables['services']=false;
$variables['services_list']=false;
$variables['services_add']=false;

$variables['tax_name']=false;
$variables['tax_name_list']=false;
$variables['tax_name_add']=false;

$variables['tax_price']=false;
$variables['tax_price_list']=false;
$variables['tax_price_add']=false;

$variables['manage_vendor']=false;
$variables['manage_vendor_list']=false;
$variables['manage_vendor_add']=false;

$variables['calls_entry']=false;
$variables['calls_entry_list']=false;
$variables['calls_entry_add']=false;

$variables['announcement']=false;
$variables['announcement_list']=false;
$variables['announcement_add']=false;

$variables['clients']=false;
$variables['clients_list']=false;
$variables['clients_add']=false;

$variables['complaint_box']=false;
$variables['complaint_box_add']=false;
$variables['complaint_box_list']=false;

$variables['console']=false;
$variables['console_edit']=false;

$variables['enquiry']=false;
$variables['enquiry_list']=false;

$variables['executives']=false;
$variables['executives_list']=false;
$variables['executives_add']=false;

$variables['fire_query_menu']='0';
$variables['fq']=false;
$variables['fq_list']=false;
$variables['fq_add']=false;

$variables['fq_priority']=false;
$variables['fq_priority_list']=false;
$variables['fq_priority_add']=false;

$variables['fq_status']=false;
$variables['fq_status_list']=false;
$variables['fq_status_add']=false;

$variables['fq_dept']=false;
$variables['fq_dept_list']=false;
$variables['fq_dept_add']=false;

$variables['fq_at_client']=false;
$variables['fq_at_client_list']=false;
$variables['fq_at_client_add']=false;

$variables['general']='0';
$variables['company']=false;
$variables['company_list']=false;
$variables['company_add']=false;

$variables['hr_settings_calendar']=false;
$variables['hr_settings_calendar_list']=false;
$variables['hr_settings_calendar_add']=false;

//$variables['settings']='0';
$variables['department']=false;
$variables['department_list']=false;
$variables['department_add']=false;

$variables['country']=false;
$variables['country_list']=false;
$variables['country_add']=false;

$variables['state']=false;
$variables['state_list']=false;
$variables['state_add']=false;

$variables['city']=false;
$variables['city_list']=false;
$variables['city_add']=false;

$variables['access_level']=false;
$variables['access_level_list']=false;
$variables['access_level_add']=false;

$variables['user_rights']=false;
$variables['user_rights_list']=false;
$variables['user_rights_add']=false;

$variables['user_roles']=false;
$variables['user_roles_list']=false;
$variables['user_roles_add']=false;

$variables['email_templates']=false;
$variables['email_templates_list']=false;
$variables['email_templates_add']=false;

$variables['terms_registration']=false;
$variables['terms_registration_list']=false;
$variables['terms_registration_add']=false;

$variables['terms_condition']=false;
$variables['terms_condition_list']=false;
$variables['terms_condition_add']=false;

$variables['global_settings']=false;
$variables['global_settings_list']=false;
$variables['global_settings_add']=false;

$variables['skills']=false;
$variables['skills_list']=false;
$variables['skills_add']=false;

$variables['mark_financial']=false;
$variables['mark_financial_list']=false;
$variables['mark_financial_add']=false;

$variables['hardware_entry']=false;
$variables['hardware_entry_list']=false;
$variables['hardware_entry_add']=false;

$variables['hr_module_att']=false;
$variables['hr_module_att_list']=false;
$variables['hr_module_att_add']=false;

$variables['hr_salary']=false;
$variables['hr_salary_list']=false;
$variables['hr_salary_add']=false;

$variables['ideas']=false;
$variables['ideas_list']=false;
$variables['ideas_add']=false;

$variables['inward']=false;
$variables['inward_list']=false;
$variables['inward_add']=false;

$variables['knowledge_share']=false;
$variables['knowledge_share_list']=false;
$variables['knowledge_share_add']=false;

$variables['payment_transaction']='0';
$variables['payment_in_out']=false;
$variables['payment_in_out_list']=false;
$variables['payment_in_out_add']=false;

$variables['manage_websites']=false;
$variables['manage_websites_page_list']=false;
$variables['manage_websites_add_page']=false;
$variables['manage_websites_menu_list']=false;
$variables['manage_websites_add_menu']=false;
$variables['manage_websites_ticker_list']=false;
$variables['manage_websites_add_ticker']=false;
$variables['manage_websites_css']=false;
$variables['manage_websites_news_events_list']=false;
$variables['manage_websites_add_news_events']=false;

$variables['marketing']='0';
$variables['quick_entry']=false;
$variables['quick_entry_list']=false;
$variables['quick_entry_add']=false;

$variables['lead_assign_to_tm']=false;
$variables['lead_assign_to_tm_list']=false;

$variables['telemarketing_leads']=false;
$variables['telemarketing_leads_list']=false;

$variables['lead_assign_to_mr']=false;
$variables['lead_assign_to_mr_list']=false;

$variables['marketing_leads']=false;
$variables['marketing_leads_list']=false;
$variables['marketing_leads_add']=false;

$variables['source']=false;
$variables['source_list']=false;
$variables['source_add']=false;

$variables['campaign']=false;
$variables['campaign_list']=false;
$variables['campaign_add']=false;

$variables['industry']=false;
$variables['industry_list']=false;
$variables['industry_add']=false;

$variables['rivals']=false;
$variables['rivals_list']=false;
$variables['rivals_add']=false;

$variables['followup_title']=false;
$variables['followup_title_list']=false;
$variables['followup_title_add']=false;

$variables['my_profile']=false;
$variables['my_profile_edit']=false;
$variables['my_profile_change_pswd']=false;

$variables['newsletter_menu']='0';
$variables['newsletter']=false;
$variables['newsletter_list']=false;
$variables['newsletter_add']=false;
$variables['newsletter_qlist']=false;

$variables['newsletter_smtp']=false;
$variables['newsletter_smtp_list']=false;
$variables['newsletter_smtp_add']=false;

$variables['score_sheet']=false;
$variables['score_sheet_list']=false;
$variables['score_sheet_add']=false;

$variables['order']='0';
$variables['orders']=false;
$variables['orders_list']=false;
$variables['orders_add']=false;

$variables['bill_pre_order']=false;
$variables['bill_pre_order_list']=false;
$variables['bill_pre_order_add']=false;

$variables['payment_account_head']=false;
$variables['payment_account_head_list']=false;
$variables['payment_account_head_add']=false;

$variables['payment_bank']=false;
$variables['payment_bank_list']=false;
$variables['payment_bank_add']=false;

$variables['payment_party']=false;
$variables['payment_party_list']=false;
$variables['payment_party_add']=false;

$variables['payment_mode']=false;
$variables['payment_mode_list']=false;
$variables['payment_mode_add']=false;

$variables['payment_party_bills']=false;
$variables['payment_party_bills_list']=false;
$variables['payment_party_bills_add']=false;

$variables['project_task']='0';
$variables['project_updates']=false;
$variables['project_updates_list']=false;

$variables['project_status']=false;
$variables['project_status_list']=false;
$variables['project_status_add']=false;

$variables['project_type']=false;
$variables['project_type_list']=false;
$variables['project_type_add']=false;

$variables['project_priority']=false;
$variables['project_priority_list']=false;
$variables['project_priority_add']=false;

$variables['project_resolution']=false;
$variables['project_resolution_list']=false;
$variables['project_resolution_add']=false;

$variables['project_task_template']=false;
$variables['project_task_template_list']=false;
$variables['project_task_template_add']=false;

$variables['quotation_menu']='0';
$variables['quotation']=false;
$variables['quotation_list']=false;
$variables['quotation_ac_approval']=false;
$variables['quotation_ac_approval_list']=false;

$variables['quotation_subject']=false;
$variables['quotation_subject_list']=false;
$variables['quotation_subject_add']=false;

$variables['reports']=false;
$variables['project_wise_reports']=false;
$variables['executive_wise_reports']=false;
$variables['day_wise_reports']=false;
$variables['task_reminder_reports']=false;
$variables['executive_score_reports']=false;
$variables['monthly_attendance']=false;
$variables['executive_attendance']=false;

$variables['service_book']=false;
$variables['service_book_list']=false;
$variables['service_book_add']=false;

$variables['sms']='0';
$variables['sms_api'] = false;
$variables['sms_api_list'] = false;
$variables['sms_api_add'] = false;
$variables['sms_gateway'] = false;
$variables['sms_gateway_list'] = false;
$variables['sms_gateway_add'] = false;
$variables['sms_purchase'] = false;
$variables['sms_purchase_list'] = false;
$variables['sms_purchase_add'] = false;
$variables['sms_sale_client'] = false;
$variables['sms_sale_client_list']=false;
$variables['sms_sale_client_add']=false;
$variables['send_sms_details']=false;
$variables['send_sms_details_list']=false;
$variables['send_sms_details_add']=false;
$variables['scheduled_sms_details']=false;
$variables['scheduled_sms_details_list']=false;
$variables['scheduled_sms_details_add']=false;
$variables['sms_client_account']=false;
$variables['sms_client_account_add']=false;
$variables['sms_client_account_list']=false;


$variables['support_ticket_menu']='0';
$variables['support_ticket']=false;
$variables['support_ticket_list']=false;
$variables['support_ticket_add']=false;

$variables['support_ticket_clients']=false;

$variables['support_ticket_assign']=false;
$variables['support_ticket_assign_list']=false;

$variables['ticket_priority']=false;
$variables['ticket_priority_list']=false;
$variables['ticket_priority_add']=false;

$variables['ticket_status']=false;
$variables['ticket_status_list']=false;
$variables['ticket_status_add']=false;

$variables['support_ticket_template']=false;
$variables['support_ticket_template_add']=false;
$variables['support_ticket_template_list']=false;

$variables['support_ticket_vendors_template']=false;
$variables['support_ticket_vendors_template_add']=false;
$variables['support_ticket_vendors_template_list']=false;

$variables['task_reminder']=false;
$variables['task_reminder_add']=false;
$variables['task_reminder_list']=false;
$variables['task_reminder_view_calendar']=false;

$variables['activity']=false;
$variables['activity_list']=false;
$variables['activity_add']=false;

$variables['uploads']=false;
$variables['uploads_list']=false;
$variables['uploads_add']=false;

$variables['vendors']=false;
$variables['vendors_list']=false;
$variables['vendors_add']=false;

$variables['vendors_support_ticket']=false;
$variables['vendors_support_ticket_add']=false;
$variables['vendors_support_ticket_list']=false;

$variables['visitor_book']=false;
$variables['visitor_book_list']=false;
$variables['visitor_book_add']=false;

$variables['website_access']=false;
$variables['website_access_list']=false;
$variables['website_access_add']=false;
$variables['website_access_mark']=false;
$variables['website_access_group_list']=false;

$variables['login_stats']=false;
$variables['login_stats_list']=false;

/*

$variables['billing']=false;
$variables['orders']=false;
$variables['invoices']=false;
$variables['new_receipt']=false;
$variables['receipt']=false;
$variables['work_status']=false;
$variables['work_stages']=false;
$variables['new_work_stage']=false;

$variables['clients']=false;
$variables['clients_list']=false;
$variables['clients_add']=false;
$variables['executives']=false;
$variables['executives_list']=false;
$variables['executives_add']=false;

$variables['my_profile']=false;
$variables['my_profile_edit']=false;
$variables['my_profile_change_pswd']=false;

$variables['add_timesheet']=false;
$variables['list_timesheet']=false;

$variables['website_access']=false;
$variables['website_access_list']=false;
$variables['website_access_add']=false;
$variables['website_access_group_list']=false;
$variables['website_access_group_add']=false;

$variables['reports']=false;
$variables['project_wise_reports']=false;
$variables['executive_wise_reports']=false;
$variables['day_wise_reports']=false;
$variables['task_reminder_reports']=false;
$variables['executive_score_reports']=false;

$variables['task_reminder']=false;
$variables['task_reminder_add']=false;
$variables['task_reminder_list']=false;

$variables['score_sheet']=false;
$variables['score_sheet_list']=false;
$variables['score_sheet_add']=false;

$variables['complaint_box']=false;
$variables['complaint_box_add']=false;
$variables['complaint_box_list']=false;

$variables['address_book']=false;
$variables['address_book_list']=false;
$variables['address_book_add']=false;

$variables['visitor_book']=false;
$variables['visitor_book_list']=false;
$variables['visitor_book_add']=false;

$variables['hardware_entry']=false;
$variables['hardware_entry_list']=false;
$variables['hardware_entry_add']=false;

$variables['inward']=false;
$variables['inward_list']=false;
$variables['inward_add']=false;

$variables['outward']=false;
$variables['outward_list']=false;
$variables['outward_add']=false;

$variables['calls_entry']=false;
$variables['calls_entry_list']=false;
$variables['calls_entry_add']=false;

$variables['login_stats']=false;
$variables['login_stats_list']=false;

$variables['payment_in_out']=false;
$variables['payment_in_out_list']=false;
$variables['payment_in_out_add']=false;

$variables['marketing']=false;
$variables['quick_entry']=false;
$variables['quick_entry_list']=false;
$variables['quick_entry_add']=false;
$variables['lead_assign_to_tm']=false;
$variables['lead_assign_to_tm_list']=false;
$variables['telemarketing_leads']=false;
$variables['telemarketing_leads_list']=false;
$variables['lead_assign_to_mr']=false;
$variables['lead_assign_to_mr_list']=false;
$variables['marketing_leads']=false;
$variables['marketing_leads_list']=false;
$variables['marketing_leads_add']=false;

$variables['quotation']=false;
$variables['quotation_list']=false;
$variables['quotation_ac_approval']=false;
$variables['quotation_ac_approval_list']=false;

$variables['settings']=false;
$variables['department']=false;
$variables['department_list']=false;
$variables['department_add']=false;

$variables['currency']=false;
$variables['currency_list']=false;
$variables['currency_add']=false;

$variables['accounts']=false;
$variables['accounts_list']=false;
$variables['accounts_add']=false;

$variables['source']=false;
$variables['source_list']=false;
$variables['source_add']=false;

$variables['campaign']=false;
$variables['campaign_list']=false;
$variables['campaign_add']=false;
$variables['industry']=false;
$variables['industry_list']=false;
$variables['industry_add']=false;
$variables['rivals']=false;
$variables['rivals_list']=false;
$variables['rivals_add']=false;
$variables['followup_title']=false;
$variables['followup_title_list']=false;
$variables['followup_title_add']=false;
$variables['payment_account_head']=false;
$variables['payment_account_head_list']=false;
$variables['payment_account_head_add']=false;
$variables['payment_bank']=false;
$variables['payment_bank_list']=false;
$variables['payment_bank_add']=false;
$variables['payment_party']=false;
$variables['payment_party_list']=false;
$variables['payment_party_add']=false;
$variables['payment_mode']=false;
$variables['payment_mode_list']=false;
$variables['payment_mode_add']=false;
$variables['terms_condition']=false;
$variables['terms_condition_list']=false;
$variables['terms_condition_add']=false;
$variables['category']=false;
$variables['category_list']=false;
$variables['category_add']=false;
$variables['csv']=false;
$variables['csv_upload']=false;
$variables['access_level']=false;
$variables['access_level_list']=false;
$variables['access_level_add']=false;
$variables['user_rights']=false;
$variables['user_rights_list']=false;
$variables['user_rights_add']=false;
$variables['user_roles']=false;
$variables['user_roles_list']=false;
$variables['user_roles_add']=false;
$variables['services']=false;
$variables['services_list']=false;
$variables['services_add']=false;
$variables['email_templates']=false;
$variables['email_templates_list']=false;
$variables['email_templates_add']=false;
$variables['manage_websites']=false;
$variables['manage_websites_page_list']=false;
$variables['manage_websites_add_page']=false;
$variables['manage_websites_menu_list']=false;
$variables['manage_websites_add_menu']=false;
$variables['manage_websites_ticker_list']=false;
$variables['manage_websites_add_ticker']=false;
$variables['manage_websites_css']=false;
$variables['rapid_sms']=false;
$variables['master_account']=false;
$variables['master_account_list']=false;
$variables['master_account_add']=false;
$variables['client_account']=false;
$variables['client_account_list']=false;
$variables['client_account_add']=false;
$variables['send_sms_details']=false;
$variables['send_sms_details_list']=false;
$variables['scheduled_sms_details']=false;
$variables['scheduled_sms_details_list']=false;
$variables['sms_credit_balance']=false;
$variables['sms_credit_balance_view']=false;
$variables['etasks']=false;
$variables['etasks_users']=false;
$variables['etasks_users_list']=false;
$variables['etasks_users_add']=false;
*/

if ( $perm->has('nc_ab') ) { 
    $variables['address_book']=true;
}
if ( $perm->has('nc_ab_list') ) { 
    $variables['address_book_list']=true;
}
if ( $perm->has('nc_ab_add') ) { 
    $variables['address_book_add']=true;
}



if ( $perm->has('nc_bl_inv') ) { 
    $variables['invoice']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_bl_inv_list') ) { 
    $variables['invoice_list']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_bl_rcpt') ) { 
    $variables['receipt']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_bl_rcpt_list') ) { 
    $variables['receipt_list']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_cur') ) { 
    $variables['currency']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_cur_list') ) { 
    $variables['currency_list']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_cur_add') ) { 
    $variables['currency_add']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_sr_div') ) { 
    $variables['service_division']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_sr_div_list') ) { 
    $variables['service_division_list']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_sr_div_add') ) { 
    $variables['service_division_add']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_sr') ) { 
    $variables['services']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_sr_list') ) { 
    $variables['services_list']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_sr_add') ) { 
    $variables['services_add']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_mt') ) { 
    $variables['tax_name']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_mt_list') ) { 
    $variables['tax_name_list']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_mt_add') ) { 
    $variables['tax_name_add']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_mtp') ) { 
    $variables['tax_price']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_mtp_list') ) { 
    $variables['tax_price_list']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_mtp_add') ) { 
    $variables['tax_price_add']=true;
    $variables['billing']='1';
}
if ( $perm->has('nc_vendor') ) { 
    $variables['manage_vendor']=true;
     $variables['billing']='1';
}

if ( $perm->has('nc_vendor_list') ) { 
    $variables['manage_vendor_list']=true;
     $variables['billing']='1';
}

if ( $perm->has('nc_vendor_add') ) { 
    $variables['manage_vendor_add']=true;
     $variables['billing']='1';
}



if ( $perm->has('nc_cl') ) { 
    $variables['calls_entry']=true;
}
if ( $perm->has('nc_cl_list') ) { 
    $variables['calls_entry_list']=true;
}
if ( $perm->has('nc_cl_add') ) { 
    $variables['calls_entry_add']=true;
}


if ( $perm->has('nc_anc') ) { 
    $variables['announcement']=true;
}
if ( $perm->has('nc_anc_list') ) { 
    $variables['announcement_list']=true;
}
if ( $perm->has('nc_anc_add') ) { 
    $variables['announcement_add']=true;
}


if ( $perm->has('nc_uc') ) { 
    $variables['clients']=true;
}
if ( $perm->has('nc_uc_add') ) { 
    $variables['clients_add']=true;    
}
if ( $perm->has('nc_uc_list') ) { 
    $variables['clients_list']=true;
}


if ( $perm->has('nc_cb') ) { 
    $variables['complaint_box']=true;
}
if ( $perm->has('nc_cb_add') ) { 
    $variables['complaint_box_add']=true;
}
if ( $perm->has('nc_cb_list') ) { 
    $variables['complaint_box_list']=true;
}


if ( $perm->has('nc_site_s') ) { 
    $variables['console']=true;
}
if ( $perm->has('nc_site_s_edit') ) { 
    $variables['console_edit']=true;
}


if ( $perm->has('nc_equ') ) { 
    $variables['enquiry']=true;
}
if ( $perm->has('nc_equ_list') ) { 
    $variables['enquiry_list']=true;
}


if ( $perm->has('nc_ue') ) { 
    $variables['executives']=true;
}
if ( $perm->has('nc_ue_add') ) { 
    $variables['executives_add']=true;    
}
if ( $perm->has('nc_ue_list') ) { 
    $variables['executives_list']=true;
}


if ( $perm->has('nc_fq') ) { 
    $variables['fq']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_list') ) { 
    $variables['fq_list']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_add') ) { 
    $variables['fq_add']=true;
    $variables['fire_query_menu']='1';
}

if ( $perm->has('nc_fq_pr') ) { 
    $variables['fq_priority']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_pr_list') ) { 
    $variables['fq_priority_list']=true;
    $variables['fire_query_menu']='1';
}

if ( $perm->has('nc_fq_pr_add') ) { 
    $variables['fq_priority_add']=true;
    $variables['fire_query_menu']='1';
}

if ( $perm->has('nc_fq_s') ) { 
    $variables['fq_status']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_s_list') ) { 
    $variables['fq_status_list']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_s_add') ) { 
    $variables['fq_status_add']=true;
    $variables['fire_query_menu']='1';
}

if ( $perm->has('nc_fq_d') ) { 
    $variables['fq_dept']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_d_list') ) { 
    $variables['fq_dept_list']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_d_add') ) { 
    $variables['fq_dept_add']=true;
    $variables['fire_query_menu']='1';
}


if ( $perm->has('nc_fq_at_c') ) { 
    $variables['fq_at_client']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_at_c_list') ) { 
    $variables['fq_at_client_list']=true;
    $variables['fire_query_menu']='1';
}
if ( $perm->has('nc_fq_at_c_add') ) { 
    $variables['fq_at_client_add']=true;
    $variables['fire_query_menu']='1';
}

if ( $perm->has('nc_cmp') ) { 
    $variables['company']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_cmp_list') ) { 
    $variables['company_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_cmp_add') ) { 
    $variables['company_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_cal') ) { 
    $variables['hr_settings_calendar']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_cal_list') ) { 
    $variables['hr_settings_calendar_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_cal_add') ) { 
    $variables['hr_settings_calendar_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_dp') ) { 
    $variables['department']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_dp_list') ) { 
    $variables['department_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_dp_add') ) { 
    $variables['department_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_ctry') ) { 
    $variables['country']=true;    
    $variables['general']='1';
}
if ( $perm->has('nc_ctry_add') ) { 
    $variables['country_add']=true;    
    $variables['general']='1';
}
if ( $perm->has('nc_ctry_list') ) { 
    $variables['country_list']=true;  
    $variables['general']='1';    
}
if ( $perm->has('nc_state') ) { 
    $variables['state']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_state_list') ) { 
    $variables['state_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_state_add') ) { 
    $variables['state_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_city') ) { 
    $variables['city']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_city_list') ) { 
    $variables['city_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_city_add') ) { 
    $variables['city_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_al') ) { 
    $variables['access_level']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_al_list') ) { 
    $variables['access_level_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_al_add') ) { 
    $variables['access_level_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_urht') ) { 
    $variables['user_rights']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_urht_list') ) { 
    $variables['user_rights_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_urht_add') ) { 
    $variables['user_rights_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_urol') ) { 
    $variables['user_roles']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_urol_list') ) { 
    $variables['user_roles_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_urol_add') ) { 
    $variables['user_roles_add']=true;
    $variables['general']='1';
}

if ( $perm->has('nc_emtpl') ) { 
    $variables['email_templates']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_emtpl_list') ) { 
    $variables['email_templates_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_emtpl_add') ) { 
    $variables['email_templates_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_tcr') ) { 
    $variables['terms_registration']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_tcr_list') ) { 
    $variables['terms_registration_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_tcr_add') ) { 
    $variables['terms_registration_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_tc') ) { 
    $variables['terms_condition']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_tc_list') ) { 
    $variables['terms_condition_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_tc_add') ) { 
    $variables['terms_condition_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_glb') ) { 
    $variables['global_settings']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_glb_list') ) { 
    $variables['global_settings_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_glb_add') ) { 
    $variables['global_settings_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_usk') ) { 
    $variables['skills']=true;
     $variables['general']='1';
}
if ( $perm->has('nc_usk_list') ) { 
    $variables['skills_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_usk_add') ) { 
    $variables['skills_add']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_fy') ) { 
    $variables['mark_financial']=true;
     $variables['general']='1';
}
if ( $perm->has('nc_fy_list') ) { 
    $variables['mark_financial_list']=true;
    $variables['general']='1';
}
if ( $perm->has('nc_fy_add') ) { 
    $variables['mark_financial_add']=true;
    $variables['general']='1';
}

if ( $perm->has('nc_hd') ) { 
    $variables['hardware_entry']=true;
}
if ( $perm->has('nc_hd_list') ) { 
    $variables['hardware_entry_list']=true;
}
if ( $perm->has('nc_hd_add') ) { 
    $variables['hardware_entry_add']=true;
}


if ( $perm->has('nc_hr_att') ) { 
    $variables['hr_module_att']=true;
}
if ( $perm->has('nc_hr_att_list') ) { 
    $variables['hr_module_att_list']=true;
}
if ( $perm->has('nc_hr_att_add') ) { 
    $variables['hr_module_att_add']=true;
}


if ( $perm->has('nc_hr_sal') ) { 
    $variables['hr_salary']=true;
}
if ( $perm->has('nc_hr_sal_list') ) { 
    $variables['hr_salary_list']=true;
}
if ( $perm->has('nc_hr_sal_add') ) { 
    $variables['hr_salary_add']=true;
}


if ( $perm->has('nc_ide') ) { 
    $variables['ideas']=true;
}
if ( $perm->has('nc_ide_list') ) { 
    $variables['ideas_list']=true;
}
if ( $perm->has('nc_ide_add') ) { 
    $variables['ideas_add']=true;
}


if ( $perm->has('nc_inward') ) { 
    $variables['inward']=true;
}
if ( $perm->has('nc_inward_list') ) { 
    $variables['inward_list']=true;
}
if ( $perm->has('nc_inward_add') ) { 
    $variables['inward_add']=true;
}


if ( $perm->has('nc_ksh') ) { 
    $variables['knowledge_share']=true;
}
if ( $perm->has('nc_ksh_list') ) { 
    $variables['knowledge_share_list']=true;
}
if ( $perm->has('nc_ksh_add') ) { 
    $variables['knowledge_share_add']=true;
}





if ( $perm->has('nc_site') ) { 
    $variables['manage_websites']=true;
}
if ( $perm->has('nc_site_sp_list') ) { 
    $variables['manage_websites_page_list']=true;
}
if ( $perm->has('nc_site_sp_add') ) { 
    $variables['manage_websites_add_page']=true;
}
if ( $perm->has('nc_site_menu_list') ) { 
    $variables['manage_websites_menu_list']=true;
}
if ( $perm->has('nc_site_menu_add') ) { 
    $variables['manage_websites_add_menu']=true;
}
if ( $perm->has('nc_site_tkr_list') ) { 
    $variables['manage_websites_ticker_list']=true;
}
if ( $perm->has('nc_site_tkr_add') ) { 
    $variables['manage_websites_add_ticker']=true;
}
if ( $perm->has('nc_site_css_edit') ) { 
    $variables['manage_websites_css']=true;
}
if ( $perm->has('nc_ne_list') ) { 
    $variables['manage_websites_news_events_list']=true;
}
if ( $perm->has('nc_ne_add') ) { 
    $variables['manage_websites_add_news_events']=true;
}


if ( $perm->has('nc_sl_ld_qe') ) { 
    $variables['quick_entry']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld_qe_list') ) { 
    $variables['quick_entry_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld_qe_add') ) { 
    $variables['quick_entry_add']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld_asig') ) { 
    $variables['lead_assign_to_tm']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld_asig_list') ) { 
    $variables['lead_assign_to_tm_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld') ) { 
    $variables['telemarketing_leads']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld_list') ) { 
    $variables['telemarketing_leads_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld_asig_mr') ) { 
    $variables['lead_assign_to_mr']=true;
    $variables['marketing']='1';
}

if ( $perm->has('nc_sl_ld_asig_mr_list') ) { 
    $variables['lead_assign_to_mr_list']=true;
    $variables['marketing']='1';
}



if ( $perm->has('nc_sl_ld') ) { 
    $variables['marketing_leads']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld_mr_add') ) { 
    $variables['marketing_leads_add']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ld_mr_list') ) { 
    $variables['marketing_leads_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_sr') ) { 
    $variables['source']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_sr_list') ) { 
    $variables['source_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_sr_add') ) { 
    $variables['source_add']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_camp') ) { 
    $variables['campaign']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_camp_list') ) { 
    $variables['campaign_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_camp_add') ) { 
    $variables['campaign_add']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ind') ) { 
    $variables['industry']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ind_list') ) { 
    $variables['industry_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_ind_add') ) { 
    $variables['industry_add']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_riv') ) { 
    $variables['rivals']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_riv_list') ) { 
    $variables['rivals_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_riv_add') ) { 
    $variables['rivals_add']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_flw_tl') ) { 
    $variables['followup_title']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_flw_tl_list') ) { 
    $variables['followup_title_list']=true;
    $variables['marketing']='1';
}
if ( $perm->has('nc_sl_flw_tl_add') ) { 
    $variables['followup_title_add']=true;
    $variables['marketing']='1';
}



if ( $perm->has('nc_myprofile') ) { 
    $variables['my_profile']=true;
}
if ( $perm->has('nc_myprofile_edit') ) { 
    $variables['my_profile_edit']=true;
}
if ( $perm->has('nc_myprofile_password') ) { 
    $variables['my_profile_change_pswd']=true;
}


if ( $perm->has('nc_nwl') ) { 
    $variables['newsletter']=true;
    $variables['newsletter_menu']='1';
}
if ( $perm->has('nc_nwl_list') ) { 
    $variables['newsletter_list']=true;
    $variables['newsletter_menu']='1';
}
if ( $perm->has('nc_nwl_add') ) { 
    $variables['newsletter_add']=true;
    $variables['newsletter_menu']='1';
}
if ( $perm->has('nc_nwl_qlist') ) { 
    $variables['newsletter_qlist']=true;
    $variables['newsletter_menu']='1';
}
if ( $perm->has('nc_nwl_smtp') ) { 
    $variables['newsletter_smtp']=true;
    $variables['newsletter_menu']='1';
}
if ( $perm->has('nc_nwl_smtp_list') ) { 
    $variables['newsletter_smtp_list']=true;
    $variables['newsletter_menu']='1';
}
if ( $perm->has('nc_nwl_smtp_add') ) { 
    $variables['newsletter_smtp_add']=true;
    $variables['newsletter_menu']='1';
}

if ( $perm->has('nc_ss') ) { 
    $variables['score_sheet']=true;
}
if ( $perm->has('nc_ss_list') ) { 
    $variables['score_sheet_list']=true;
}
if ( $perm->has('nc_ss_add') ) { 
    $variables['score_sheet_add']=true;
}


if ( $perm->has('nc_bl_or') ) { 
    $variables['orders']=true;
    $variables['order']='1';
}
if ( $perm->has('nc_bl_or_list') ) { 
    $variables['orders_list']=true;
    $variables['order']='1';
}
if ( $perm->has('nc_bl_or_add') ) { 
    $variables['orders_add']=true;
    $variables['order']='1';
}
if ( $perm->has('nc_bl_po') ) { 
    $variables['bill_pre_order']=true;
    $variables['order']='1';
}
if ( $perm->has('nc_bl_po_list') ) { 
    $variables['bill_pre_order_list']=true;
    $variables['order']='1';
}
if ( $perm->has('nc_bl_po_add') ) { 
    $variables['bill_pre_order_add']=true;
    $variables['order']='1';
}

if ( $perm->has('nc_p_pt') ) { 
    $variables['payment_in_out']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_pt_list') ) { 
    $variables['payment_in_out_list']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_pt_add') ) { 
    $variables['payment_in_out_add']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_ah') ) { 
    $variables['payment_account_head']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_ah_list') ) { 
    $variables['payment_account_head_list']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_ah_add') ) { 
    $variables['payment_account_head_add']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_mb') ) { 
    $variables['payment_bank']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_mb_list') ) { 
    $variables['payment_bank_list']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_mb_add') ) { 
    $variables['payment_bank_add']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_mp') ) { 
    $variables['payment_party']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_mp_list') ) { 
    $variables['payment_party_list']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_mp_add') ) { 
    $variables['payment_party_add']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_pm') ) { 
    $variables['payment_mode']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_pm_list') ) { 
    $variables['payment_mode_list']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_pm_add') ) { 
    $variables['payment_mode_add']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_pb') ) { 
    $variables['payment_party_bills']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_pb_list') ) { 
    $variables['payment_party_bills_list']=true;
    $variables['payment_transaction']='1';
}
if ( $perm->has('nc_p_pb_add') ) { 
    $variables['payment_party_bills_add']=true;
    $variables['payment_transaction']='1';
}




if ( $perm->has('nc_pu') ) { 
    $variables['project_updates']=true;
    $variables['project_task']='1'; 
}

if ( $perm->has('nc_pu_list') ) { 
    $variables['project_updates_list']=true;
    $variables['project_task']='1'; 
}

if ( $perm->has('nc_pts_sta') ) { 
    $variables['project_status']=true;
    $variables['project_task']='1'; 
}
if ( $perm->has('nc_pts_sta_list') ) { 
    $variables['project_status_list']=true;
    $variables['project_task']='1'; 
}
if ( $perm->has('nc_pts_sta_add') ) { 
    $variables['project_status_add']=true;
    $variables['project_task']='1'; 
}

if ( $perm->has('nc_pts_type') ) { 
    $variables['project_type']=true;
    $variables['project_task']='1';   
}
if ( $perm->has('nc_pts_type_add') ) { 
    $variables['project_type_list']=true;
    $variables['project_task']='1';   
}
if ( $perm->has('nc_pts_type_add') ) { 
    $variables['project_type_add']=true;
    $variables['project_task']='1';   
}

if ( $perm->has('nc_pts_pr') ) { 
    $variables['project_priority']=true;
    $variables['project_task']='1'; 
}
if ( $perm->has('nc_pts_pr_list') ) { 
    $variables['project_priority_list']=true;
    $variables['project_task']='1'; 
}
if ( $perm->has('nc_pts_pr_add') ) { 
    $variables['project_priority_add']=true;
    $variables['project_task']='1'; 
}

if ( $perm->has('nc_pts_res') ) { 
    $variables['project_resolution']=true;
    $variables['project_task']='1'; 
}
if ( $perm->has('nc_pts_res_list') ) { 
    $variables['project_resolution_list']=true;
    $variables['project_task']='1'; 
}
if ( $perm->has('nc_pts_res_add') ) { 
    $variables['project_resolution_add']=true;
    $variables['project_task']='1'; 
}

if ( $perm->has('nc_pt_t') ) { 
    $variables['project_task_template']=true;
    $variables['project_task']='1'; 
}
if ( $perm->has('nc_pt_t_list') ) { 
    $variables['project_task_template_list']=true;
    $variables['project_task']='1'; 
}
if ( $perm->has('nc_pt_t_add') ) { 
    $variables['project_task_template_add']=true;
    $variables['project_task']='1'; 
}


if ( $perm->has('nc_sl_ld_q') ) { 
    $variables['quotation']=true;
    $variables['quotation_menu']='1';
}
if ( $perm->has('nc_sl_ld_q_list') ) { 
    $variables['quotation_list']=true;
    $variables['quotation_menu']='1';
}
if ( $perm->has('nc_sl_ld_qac') ) { 
    $variables['quotation_ac_approval']=true;
    $variables['quotation_menu']='1';
}
if ( $perm->has('nc_sl_ld_qac_list') ) { 
    $variables['quotation_ac_approval_list']=true;
    $variables['quotation_menu']='1';
}

if ( $perm->has('nc_quot_sub') ) { 
    $variables['quotation_subject']=true;
    $variables['quotation_menu']='1';
}
if ( $perm->has('nc_quot_sub_list') ) { 
    $variables['quotation_subject_list']=true;
    $variables['quotation_menu']='1';
}
if ( $perm->has('nc_quot_sub_add') ) { 
    $variables['quotation_subject_add']=true;
    $variables['quotation_menu']='1';
}


if ( $perm->has('nc_rp') ) { 
    $variables['reports']=true;
}
if ( $perm->has('nc_rp_pr') ) { 
    $variables['project_wise_reports']=true;
}
if ( $perm->has('nc_rp_ex') ) { 
    $variables['executive_wise_reports']=true;
}
if ( $perm->has('nc_rp_dy') ) { 
    $variables['day_wise_reports']=true;
}
if ( $perm->has('nc_rp_tr') ) { 
    $variables['task_reminder_reports']=true;
}
if ( $perm->has('nc_rp_ss') ) { 
    $variables['executive_score_reports']=true;
}
if ( $perm->has('nc_rp_m_att') ) { 
    $variables['monthly_attendance']=true;
}
if ( $perm->has('nc_rp_ex_m_att') ) { 
    $variables['executive_attendance']=true;
}


if ( $perm->has('nc_sb') ) { 
    $variables['service_book']=true;
}
if ( $perm->has('nc_sb_list') ) { 
    $variables['service_book_list']=true;
}
if ( $perm->has('nc_sb_add') ) { 
    $variables['service_book_add']=true;
}


if ( $perm->has('nc_sms_api') ) { 
    $variables['sms_api'] = true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_api_list') ) { 
    $variables['sms_api_list'] = true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_api_add') ) { 
    $variables['sms_api_add'] = true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_gtw') ) { 
    $variables['sms_gateway'] = true;
   $variables['sms']='1';
}

if ( $perm->has('nc_sms_gtw_list') ) { 
    $variables['sms_gateway_list'] = true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_gtw_add') ) { 
    $variables['sms_gateway_add'] = true;
   $variables['sms']='1';
}

if ( $perm->has('nc_sms_pr') ) { 
    $variables['sms_purchase']=true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_pr_list') ) { 
    $variables['sms_purchase_list']=true;
   $variables['sms']='1';
}

if ( $perm->has('nc_sms_pr_add') ) { 
    $variables['sms_purchase_add']=true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_sl') ) { 
    $variables['sms_sale_client']=true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_sl_list') ) { 
    $variables['sms_sale_client_list']=true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_pr_add') ) { 
    $variables['sms_sale_client_add']=true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_lst') ) { 
    $variables['send_sms_details']=true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_lst_list') ) { 
    $variables['send_sms_details_list']=true;
    $variables['sms']='1';
}

if ( $perm->has('nc_sms_ca') ) { 
    $variables['sms_client_account']=true;
    $variables['sms']='1';
}
if ( $perm->has('nc_sms_ca_add') ) { 
    $variables['sms_client_account_add']=true;
    $variables['sms']='1';
}
if ( $perm->has('nc_sms_ca_list') ) { 
    $variables['sms_client_account_list']=true;
    $variables['sms']='1';
}



if ( $perm->has('nc_st') ) { 
    $variables['support_ticket']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_list') ) { 
    $variables['support_ticket_list']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_add') ) { 
    $variables['support_ticket_add']=true;
      $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_as') ) { 
    $variables['support_ticket_assign'] = true;
}

if ( $perm->has('nc_st_as_list') ) { 
    $variables['support_ticket_assign_list'] = true;
}

if ( $perm->has('nc_st_add_all') ) { 
    $variables['support_ticket_clients']=true;
      $variables['support_ticket_menu']='1';
}

/*$variables['settings']=true;*/
if ( $perm->has('nc_ac') ) { 
    $variables['accounts']=true;
    $variables['settings']='1';
}

if ( $perm->has('nc_ac_list') ) { 
    $variables['accounts_list']=true;
    $variables['settings']='1';
}

if ( $perm->has('nc_ac_add') ) { 
    $variables['accounts_add']=true;
    $variables['settings']='1';
}

if ( $perm->has('nc_st_pr') ) { 
    $variables['ticket_priority']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_pr_list') ) { 
    $variables['ticket_priority_list']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_pr_add') ) { 
    $variables['ticket_priority_add']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_sta') ) { 
    $variables['ticket_status']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_sta_list') ) { 
    $variables['ticket_status_list']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_sta_add') ) { 
    $variables['ticket_status_add']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_t') ) { 
    $variables['support_ticket_template']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_t_list') ) { 
    $variables['support_ticket_template_list']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_t_add') ) { 
    $variables['support_ticket_template_add']=true;
    $variables['support_ticket_menu']='1';
}

if ( $perm->has('nc_st_t') ) { 
    $variables['support_ticket_vendors_template']=true;    
}

if ( $perm->has('nc_st_t_list') ) { 
    $variables['support_ticket_vendors_template_list']=true;    
}

if ( $perm->has('nc_st_t_add') ) { 
    $variables['support_ticket_vendors_template_add']=true;    
}



if ( $perm->has('nc_ts') ) { 
    $variables['task_reminder']=true;
}
if ( $perm->has('nc_ts_add') ) { 
    $variables['task_reminder_add']=true;
}
if ( $perm->has('nc_ts_list') ) { 
    $variables['task_reminder_list']=true;
}
if ( $perm->has('nc_ts_list') ) { 
    $variables['task_reminder_list']=true;
}
if ( $perm->has('nc_ts_list') ) { 
    $variables['task_reminder_view_calendar']=true;
}

if ( $perm->has('nc_jb') ) { 
    $variables['activity']=true;
}
if ( $perm->has('nc_jb_add') ) { 
    $variables['activity_add']=true;
}
if ( $perm->has('nc_jb_list') ) { 
    $variables['activity_list']=true;
}

if ( $perm->has('nc_bl_or_up') ) { 
    $variables['uploads']=true;
}
if ( $perm->has('nc_bl_or_up_list') ) { 
    $variables['uploads_list']=true;
}
if ( $perm->has('nc_bl_or_up_add') ) { 
    $variables['uploads_add']=true;
}


if ( $perm->has('nc_vb') ) { 
    $variables['visitor_book']=true;
}
if ( $perm->has('nc_vb_list') ) { 
    $variables['visitor_book_list']=true;
}
if ( $perm->has('nc_vb_add') ) { 
    $variables['visitor_book_add']=true;
}

if ( $perm->has('nc_uc') ) { 
    $variables['vendors']=true;
}
if ( $perm->has('nc_uc_add') ) { 
    $variables['vendors_add']=true;    
}
if ( $perm->has('nc_uc_list') ) { 
    $variables['vendors_list']=true;
}


if ( $perm->has('nc_st') ) { 
    $variables['vendors_support_ticket']=true;    
}

if ( $perm->has('nc_st_list') ) { 
    $variables['vendors_support_ticket_list']=true;    
}

if ( $perm->has('nc_st_add') ) { 
    $variables['vendors_support_ticket_add']=true;    
}


if ( $perm->has('nc_wb_ac') ) { 
    $variables['website_access']=true;
}
if ( $perm->has('nc_wb_ac_ls') ) { 
 	$variables['website_access_list']=true;
}
if ( $perm->has('nc_wb_ac_add') ) { 
    $variables['website_access_add']=true;
}
if ( $perm->has('nc_wb_ac_mark') ) { 
    $variables['website_access_mark']=true;
}
if ( $perm->has('nc_wb_ac_gr_ls') ) { 
    $variables['website_access_group_list']=true;
}

if ( $perm->has('nc_lg_sts') ) { 
    $variables['login_stats']=true;
}

if ( $perm->has('nc_lg_sts') ) { 
    $variables['login_stats_list']=true;
}




/*
if ( $perm->has('nc_sms_schd_list') ) { 
    $variables['send_sms_details_list']=true;
    $variables['rapid_sms']='1';
}

if ( $perm->has('nc_sms_crbal') ) { 
    $variables['send_sms_details_add']=true;
    $variables['rapid_sms']='1';
}
*/
/*if ( $perm->has('nc_sl_sr_add') ) { 
    $variables['sms_credit_balance_view']=true;
}*/

/*if ( $perm->has('nc_sl_sr_add') ) { 
    $variables['etasks']=true;
}*/

/*if ( $perm->has('nc_etask_usr') ) { 
    $variables['etasks_users']=true;
    $variables['etasks']='1';
}

if ( $perm->has('nc_etask_usr_list') ) { 
    $variables['etasks_users_list']=true;
    $variables['etasks']='1';
}

if ( $perm->has('nc_etask_usr_add') ) { 
    $variables['etasks_users_add']=true;
    $variables['etasks']='1';
}*/

/*if ( $perm->has('nc_sms_gtw') ) { 
    $variables['sms_gateway']=true;
    $variables['sms']='1';    
}
if ( $perm->has('nc_sms_gtw_add') ) { 
    $variables['sms_gateway_add']=true;
    $variables['sms']='1';    
}
if ( $perm->has('nc_sms_gtw_list') ) { 
    $variables['sms_gateway_list']=true;
    $variables['sms']='1';    
}
if ( $perm->has('nc_sms_api') ) { 
    $variables['sms_api']=true;
    $variables['sms']='1';    
}
if ( $perm->has('nc_sms_api_add') ) { 
    $variables['sms_api_add']=true;
    $variables['sms']='1';    
}
if ( $perm->has('nc_sms_api_list') ) { 
    $variables['sms_api_list']=true;
    $variables['sms']='1';    
}*/
?>






