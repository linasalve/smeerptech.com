<?php

    if ( $perm->has('nc_fq_list') ) {
        
        //include_once ( DIR_FS_CLASS .'/Phone.class.php');
        
        $condition_query = " LEFT JOIN ".TABLE_FQ_STATUS." ON ".TABLE_FQ_STATUS.".status_id = ".TABLE_FQ_TICKETS.".ticket_status 
                             LEFT JOIN ".TABLE_FQ_DEPARTMENT." ON ".TABLE_FQ_DEPARTMENT.".id = ".TABLE_FQ_TICKETS.".ticket_department 
                             LEFT JOIN ".TABLE_FQ_PRIORITY." ON ".TABLE_FQ_PRIORITY.".priority_id = ".TABLE_FQ_TICKETS.".ticket_priority 
                             LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id = ".TABLE_FQ_TICKETS.".ticket_owner_uid 
                             WHERE ".TABLE_FQ_TICKETS.".ticket_child='0' ".$condition_query    ;
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
          //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        $total	=	FireQuery::getList( $db, $list, '', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $list	= NULL;
        $fields = TABLE_FQ_TICKETS.".*,".TABLE_FQ_STATUS.".status_name,";
        $fields .= TABLE_FQ_DEPARTMENT.".department_name,";
        $fields .= TABLE_FQ_PRIORITY.".priority_name,";
        $fields .= TABLE_CLIENTS.".f_name,".TABLE_CLIENTS.".l_name ";
        FireQuery::getList( $db, $list,$fields, $condition_query, $next_record, $rpp);
        
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                $val['ticket_department']    = $val['department_name'] ;     
                $val['ticket_status']    = $val['status_name'] ;     
                $val['ticket_priority']    = $val['priority_name'] ;   
                 
               $executive=array();
               $string = str_replace(",","','", $val['assign_members']);
               $fields4 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               $condition4 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
               User::getList($db,$executive,$fields4,$condition4);
               $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['assign_members'] = $executivename ; 
                 
                $db2 = new db_local ;
                
                $query = "SELECT COUNT(*) AS replies FROM ". TABLE_FQ_TICKETS
                                    ." WHERE ". TABLE_FQ_TICKETS .".ticket_child ='". $val['ticket_id'] ."' " ; 
                $db2->query($query) ;
                $db2->next_record() ;
                $ticket_replies = $db2->f("replies") ;
                
                $val['ticket_replied']    = $ticket_replies ;
                
               $fList[$key]=$val;
            }
        }
        
            // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_view_details']  = false;
        
        if ( $perm->has('nc_fq_add') ) {
            $variables['can_add'] = true;
        }
        
        if ( $perm->has('nc_fq_details') ) {
            $variables['can_view_details'] = true;
        }
        
        if ( $perm->has('nc_fq_list') ) {
            $variables['can_view_list'] = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'fire-query-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>