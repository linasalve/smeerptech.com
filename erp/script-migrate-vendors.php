<?php
if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
				   
  
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/vendors.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/vendors-bank.inc.php' );
	
	//Push in accounts
	$accounts=array(
		'c94e5f24e27a945d245e58214e3142d3',
		'd001ddbba936e4462355865ade7c8cd3',
		'ea3ba5d5f688afe3c39f8239237c6a11',
		'346efe2888bc37e28ab1778b19eb2f49',
		'fff3c15c949a1f562bd48c945ccee481',
		'3240f1beb7cbe6cdffc285e70070d98b',
		'48f212c6fc80c5846a995d502f341f5e',
		'994c87afc1f55f2fd1a3a187d07d40fb',
		'9aea84c011e7d7c3e21dcc01d86c12e6',
		'7a5f365e7d327e6d54e6f16f7adc1d47',
		'8eb22a0011657ff9505a2bd88209b16f',
		'37606da32de55fbe3d1a89a3b8f3502c',
		'7071c761698dc6e7e423736a39179304',
		'2987ee95f5674512e50b2c1aa900c68e',
		'efa4ea675db7222c4c7137b387c44eb4',
		'2390df7286f95e48adf1eac64d0c8c46',
		'fcaadb121392dea8f168be84245aa9ef',
		'09d0b03df94af45ad0de8679959b4aa6',
		'4fc0693ea89860e61d7437c1498e204f'	
	);
	
	//Vendor and Clients so donot push in clients
	$red = array(
		'32045d1c14f892494441b36fa6ed393e',
		'25a8c2d73f69f5164db7878e4b56f8b3',
		'201a2fa8f664df0d1ac4f4af77811862',
		'9fe473db1fb627fe795ad5b9647cdf94',
		'71df28ff650dd3676b2332d9195e43b4',
		'e9b2473ee6bc58b6aee07280b6b2152f',
		'ee7fcc52b6c467dab56eb328375efbe4',
		'166dbea06cb9b33c9af712d57b63062b',
		'589cc5eb1e6e453b2031978e5510426c',
		'49fe8d9a5a4fc223effe038ccd201e8a'	
		);
	$red_str1 = implode(",",$red) ;
	$red_str = str_replace(",","','", $red_str1);
	
	//dont add coz to create only one accounts for some vendors
	$dont_add =array(
		'f710ff2c5f4f2bdff16892805e2efb23', //replace by convence 4323e1e29743c19cc240f81e37021158
		'dd664a563efe0cdefa2bc52c16f34746', //replace by convence 4323e1e29743c19cc240f81e37021158
		'ba3a0ef9308aaac37b3d514c1a3803a2', //replace by convence 4323e1e29743c19cc240f81e37021158
		'a5b5bbe4685d0222843cf2a9dbac48fe', //replace by convence 4323e1e29743c19cc240f81e37021158
		'e620736db09f4038cc98289a3d82e0b9', //replace by convence 4323e1e29743c19cc240f81e37021158 
		'c01107139541ebf54a76794f418f1adc', //sunil savji replace by Food and Refres 005ed61922ee569730c39f4b9d4e0494
		'f90ebff8621994a3bc298a4c7f11ad30', //Food co replace  by Food and Refresh. 005ed61922ee569730c39f4b9d4e0494
		'c830df034bef730d06e3110881b327a0', // wrong computer - entry not req.
		'61b65dbb3a838357180ca51aa11c72ae', //team smeerp 
		'b3e3a2ba621b908b72b001c39150163a', //inserted in clients
		'd04cf3aca1d98f4090c983aa0486bbdd' //inserted in clients
	);
	
	$dont_add_str1 = implode(",",$dont_add) ;
	$dont_add_str = str_replace(",","','", $dont_add_str1);
	
	
	$db1 		= new db_local; // database handle
	$db2		= new db_local; // database handle
echo $query = "SELECT * FROM ".TABLE_VENDORS." WHERE user_id NOT IN ('".$dont_add_str."','".$red_str."' ) AND parent_id='' ";	
	$i=0;
	/*
	if ( $db->query($query) ) {
		if ( $db->nf() > 0 ) {
			while ($db->next_record()) {
				 echo $i++;
				 echo "<br/>";
                $user_id =trim($db->f('user_id'));				
				
                $parent_id =$db->f('parent_id');
				
				
                $service_id =$db->f('service_id');
                $authorization_id =$db->f('authorization_id');
                $manager =processUserData($db->f('manager'));
                $username =processUserData($db->f('username'));
                $password =$db->f('password');
                $access_level =$db->f('access_level');
                $created_by =$db->f('created_by');
                $team =processUserData($db->f('team'));
                $roles =processUserData($db->f('roles'));
                $status =$db->f('status');
                $credit_limit =processUserData($db->f('credit_limit'));
                $grade =processUserData($db->f('grade'));
                $email =processUserData($db->f('email'));
                $email_1 =processUserData($db->f('email_1'));
                $email_2 =processUserData($db->f('email_2'));
                $email_3 = processUserData($db->f('email_3'));
                $email_4 = processUserData($db->f('email_4'));
                $additional_email = processUserData($db->f('additional_email'));
                $title = processUserData($db->f('title'));
                $f_name = processUserData($db->f('f_name'));
                $m_name = processUserData($db->f('m_name'));
                $l_name = processUserData($db->f('l_name'));
                $p_name = processUserData($db->f('p_name'));
                $bank_account_no = processUserData($db->f('bank_account_no'));
                $cst_no =processUserData($db->f('cst_no'));
                $vat_no =processUserData($db->f('vat_no'));
                $pan_no =processUserData($db->f('pan_no'));
                $service_tax_no =processUserData($db->f('service_tax_no'));
                $desig = processUserData($db->f('desig'));
                $org = processUserData($db->f('org'));
                $domain = processUserData($db->f('domain'));
                $gender = processUserData($db->f('gender'));
                $do_birth = processUserData($db->f('do_birth'));
                $do_aniv = processUserData($db->f('do_aniv'));
                $do_add = processUserData($db->f('do_add'));
                $do_reg = processUserData($db->f('do_reg'));
                $policy_check =processUserData($db->f('policy_check'));
                $ip_reg =$db->f('ip_reg');
                $do_login =$db->f('do_login');
                $allow_ip =processUserData($db->f('allow_ip'));
                $valid_ip =processUserData($db->f('valid_ip'));
                $remarks =processUserData($db->f('remarks'));
                $lead_id =$db->f('lead_id');
                $industry =$db->f('industry');
                $wt_you_do =processUserData($db->f('wt_you_do'));
                $mobile1 =processUserData($db->f('mobile1'));
                $mobile2 =processUserData($db->f('mobile2'));
                $spouse_dob =processUserData($db->f('spouse_dob'));
                $alloted_clients = processUserData($db->f('alloted_clients'));
                $billing_name = processUserData($db->f('billing_name'));
                $is_soft_copy = $db->f('is_soft_copy');
                $send_mail =$db->f('send_mail');
				
				//add main users
				if(in_array($user_id, $accounts) ) {
					$number = VendorsBank::getNewAccNumber($db1);
					echo $query1	= " INSERT INTO ". TABLE_VENDORS_BANK
							." SET ". TABLE_VENDORS_BANK .".user_id     	= '". $user_id ."'"
                            .",". TABLE_VENDORS_BANK .".service_id   = '". $service_id ."'"
                            .",". TABLE_VENDORS_BANK .".authorization_id   = '". $authorization_id ."'"
                            .",". TABLE_VENDORS_BANK .".number      	= '". $number ."'"
							.",". TABLE_VENDORS_BANK .".manager 	   	= '". $manager ."'"
							.",". TABLE_VENDORS_BANK .".team			= '". $team."'"
                            .",". TABLE_VENDORS_BANK .".username    	= '". $username ."'"
                            .",". TABLE_VENDORS_BANK .".password    	= '". $password ."'"
                            .",". TABLE_VENDORS_BANK .".access_level	= '". $access_level ."'"
                            .",". TABLE_VENDORS_BANK .".created_by		= '". $created_by ."'"
                            .",". TABLE_VENDORS_BANK .".roles       	= '". $roles ."'"
                            .",". TABLE_VENDORS_BANK .".email       	= '". $email ."'"
                            .",". TABLE_VENDORS_BANK .".check_email 	= '0'"
                            .",". TABLE_VENDORS_BANK .".email_1     	= '". $email_1 ."'"						    
                            .",". TABLE_VENDORS_BANK .".email_2     	= '". $email_2 ."'"						
                            .",". TABLE_VENDORS_BANK .".email_3     	= '". $email_3 ."'"							
                            .",". TABLE_VENDORS_BANK .".email_4     	= '". $email_4 ."'"						
                            .",". TABLE_VENDORS_BANK .".additional_email = '". $additional_email ."'"
                            .",". TABLE_VENDORS_BANK .".title       	= '". $title ."'"
                            .",". TABLE_VENDORS_BANK .".f_name      	= '". $f_name ."'"
                            .",". TABLE_VENDORS_BANK .".m_name      	= '". $m_name ."'"
                            .",". TABLE_VENDORS_BANK .".l_name      	= '". $l_name ."'"
                            .",". TABLE_VENDORS_BANK .".p_name      	= '". $p_name."'"
                            .",". TABLE_VENDORS_BANK .".grade      	= '". $grade ."'"
                            .",". TABLE_VENDORS_BANK .".credit_limit = '". $credit_limit ."'"
                            .",". TABLE_VENDORS_BANK .".desig       	= '". $desig."'"
                            .",". TABLE_VENDORS_BANK .".cst_no       = '". $cst_no ."'"
                            .",". TABLE_VENDORS_BANK .".vat_no       = '". $vat_no ."'"
                            .",". TABLE_VENDORS_BANK .".service_tax_no  = '". $service_tax_no ."'"
                            .",". TABLE_VENDORS_BANK .".pan_no  		= '". $pan_no ."'"							 
                            .",". TABLE_VENDORS_BANK .".org         	= '". $org ."'"
                            .",". TABLE_VENDORS_BANK .".domain      	= '". $domain ."'"
                            .",". TABLE_VENDORS_BANK .".gender      	= '". $gender ."'"
                            .",". TABLE_VENDORS_BANK .".do_birth    	= '". $do_birth ."'"
                            .",". TABLE_VENDORS_BANK .".do_aniv     	= '". $do_aniv ."'"
                            .",". TABLE_VENDORS_BANK .".remarks     	= '". $remarks ."'"
                            .",". TABLE_VENDORS_BANK .".industry     = '". $industry."'"
                            .",". TABLE_VENDORS_BANK .".wt_you_do    = '". $wt_you_do ."'"
                            .",". TABLE_VENDORS_BANK .".mobile1      = '". $mobile1 ."'"
                            .",". TABLE_VENDORS_BANK .".mobile2      = '". $mobile2 ."'"
                            .",". TABLE_VENDORS_BANK .".alloted_clients   = '". $alloted_clients ."'"
                            .",". TABLE_VENDORS_BANK .".billing_name   = '". $billing_name."'"
                            .",". TABLE_VENDORS_BANK .".spouse_dob   = '". $spouse_dob ."'"
                            .",". TABLE_VENDORS_BANK .".do_add      	= '". $do_add."'"
                            .",". TABLE_VENDORS_BANK .".status      	= '". $status."'" ;
					echo "<br/>";
					$db2->query($query1);
					
				}else{
					//push in clients
					$number = Clients::getNewAccNumber($db1);
					//add main users
					echo $query1 = " INSERT INTO ". TABLE_CLIENTS
							." SET ". TABLE_CLIENTS .".user_id     	= '". $user_id ."'"
                            .",". TABLE_CLIENTS .".service_id   = '". $service_id ."'"
                            .",". TABLE_CLIENTS .".authorization_id   = '". $authorization_id ."'"
                            .",". TABLE_CLIENTS .".number      	= '". $number ."'"
							.",". TABLE_CLIENTS .".manager 	   	= '". $manager ."'"
							.",". TABLE_CLIENTS .".team			= '". $team."'"
                            .",". TABLE_CLIENTS .".username    	= '". $username ."'"
                            .",". TABLE_CLIENTS .".password    	= '". $password ."'"
                            .",". TABLE_CLIENTS .".access_level	= '". $access_level ."'"
                            .",". TABLE_CLIENTS .".created_by	= '". $created_by ."'"
                            .",". TABLE_CLIENTS .".roles       	= '". $roles ."'"
                            .",". TABLE_CLIENTS .".email       = '". $email ."'"
                            .",". TABLE_CLIENTS .".check_email = '0'"
                            .",". TABLE_CLIENTS .".email_1     = '". $email_1 ."'"						    
                            .",". TABLE_CLIENTS .".email_2     = '". $email_2 ."'"						
                            .",". TABLE_CLIENTS .".email_3     	= '". $email_3 ."'"							
                            .",". TABLE_CLIENTS .".email_4     	= '". $email_4 ."'"						
                            .",". TABLE_CLIENTS .".additional_email = '". $additional_email ."'"
                            .",". TABLE_CLIENTS .".title       	= '". $title ."'"
                            .",". TABLE_CLIENTS .".f_name      	= '". $f_name ."'"
                            .",". TABLE_CLIENTS .".m_name      	= '". $m_name ."'"
                            .",". TABLE_CLIENTS .".l_name      	= '". $l_name ."'"
                            .",". TABLE_CLIENTS .".p_name      	= '". $p_name."'"
                            .",". TABLE_CLIENTS .".grade      	= '". $grade ."'"
                            .",". TABLE_CLIENTS .".credit_limit = '". $credit_limit ."'"
                            .",". TABLE_CLIENTS .".desig       	= '". $desig."'"
                            .",". TABLE_CLIENTS .".cst_no       = '". $cst_no ."'"
                            .",". TABLE_CLIENTS .".vat_no       = '". $vat_no ."'"
                            .",". TABLE_CLIENTS .".service_tax_no  = '". $service_tax_no ."'"
                            .",". TABLE_CLIENTS .".pan_no  		= '". $pan_no ."'"							 
                            .",". TABLE_CLIENTS .".org         	= '". $org ."'"
                            .",". TABLE_CLIENTS .".domain      	= '". $domain ."'"
                            .",". TABLE_CLIENTS .".gender      	= '". $gender ."'"
                            .",". TABLE_CLIENTS .".do_birth    	= '". $do_birth ."'"
                            .",". TABLE_CLIENTS .".do_aniv     	= '". $do_aniv ."'"
                            .",". TABLE_CLIENTS .".remarks     	= '". $remarks ."'"
                            .",". TABLE_CLIENTS .".industry     = '". $industry."'"
                            .",". TABLE_CLIENTS .".wt_you_do    = '". $wt_you_do ."'"
                            .",". TABLE_CLIENTS .".mobile1      = '". $mobile1 ."'"
                            .",". TABLE_CLIENTS .".mobile2      = '". $mobile2 ."'"
                            .",". TABLE_CLIENTS .".alloted_clients   = '". $alloted_clients ."'"
                            .",". TABLE_CLIENTS .".billing_name   = '". $billing_name."'"
                            .",". TABLE_CLIENTS .".spouse_dob   = '". $spouse_dob ."'"
                            .",". TABLE_CLIENTS .".do_add      	= '". $do_add."'"
                            .",". TABLE_CLIENTS .".status      	= '". $status."'" ;
					
					echo "<br/>";
					
					$db2->query($query1);
				}
				
				
				
				
			}
		}
	}
	*/
?>