<?php

 
		// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/currency.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/followup.inc.php');

 $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x_csv 			= isset($_GET["x_csv"])         ? $_GET["x_csv"]        : ( isset($_POST["x_csv"])          ? $_POST["x_csv"]       : '');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');
    $deleted 	= isset($_GET["deleted"])   ? $_GET["deleted"]  : ( isset($_POST["deleted"])    ? $_POST["deleted"] : '0');
    $inv_no 	= isset($_GET["inv_no"])    ? $_GET["inv_no"]   : ( isset($_POST["inv_no"])     ? $_POST["inv_no"]  : '');

    $hid 		= isset($_GET["hid"])         ? $_GET["hid"]        : ( isset($_POST["hid"])          ? $_POST["hid"]       :'');
	$action='Proforma Invoice';
	
	$sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_BILL_INV_PROFORMA  =>  array('ID'            => 'id',
                                                            'Proforma Invoice No.'   => 'number',
															'Amount'     => 'amount',
                                                            'Order No.'     => 'or_no',
                                                            'Remarks'       => 'remarks'
                                                        ),
								TABLE_BILL_ORDERS =>  array('Order Title'   => 'order_title-'.TABLE_BILL_ORDERS,
								'Financial Year'   => 'financial_yr-'.TABLE_BILL_ORDERS 
								),
                                TABLE_BILL_ORD_P => array('Order Particulars' => 'particulars-'.TABLE_BILL_ORD_P ),                         
                                // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                TABLE_USER      =>  array(  'Creator Number'    => 'number-'.TABLE_USER,
                                                            'Creator Username'  => 'username-'.TABLE_USER,
                                                            'Creator Email'     => 'email-'.TABLE_USER,
                                                            'Creator First Name'=> 'f_name-'.TABLE_USER,
                                                            'Creator Last Name' => 'l_name-'.TABLE_USER,
                                                ),
								TABLE_CLIENTS 	=>  array(  'Billing Name'          => 'billing_name-'.TABLE_CLIENTS,
                                                            'Client Number'       => 'number-'.TABLE_CLIENTS,
                                                            'Client Username'     => 'username-'.TABLE_CLIENTS,
                                                            'Client Email'        => 'email-'.TABLE_CLIENTS,
                                                            'Client Company Name' => 'org-'.TABLE_CLIENTS,
                                                            'Client First Name'   => 'f_name-'.TABLE_CLIENTS,
                                                            'Client Last Name'    => 'l_name-'.TABLE_CLIENTS,
                                                )
                               
                            );

        
        $sOrderByArray  = array(
                                TABLE_BILL_INV_PROFORMA => array('ID'                => 'id',
                                                        'Proforma Invoice No.'       => 'number',
                                                        'Date of Expiry'    => 'do_e',
                                                        'Date of Proforma Invoice'   => 'do_i',
                                                        'Date of Creation'  => 'do_c',
                                                        'Status'            => 'status'
                                                    ),
                            );
	
	
    if ( empty($x_csv) ) {
        $x_csv              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x_csv-1) * $rpp;
    }
    $variables["x_csv"]     = $x_csv;
    $variables["rpp"]   = $rpp;
	
	 if(!empty($hid)){
		$_SEARCH['sOrderBy']= $sOrderBy = 'id';
		$variables['hid']=$hid;
	}else{
		$_SEARCH['sOrderBy']= $sOrderBy = 'do_i';
	}
	$_SEARCH['sOrder']  = $sOrder   = 'DESC';
	$order_by_table     = TABLE_BILL_INV_PROFORMA;

	
	
	include (DIR_FS_NC.'/bill-invoice-profm-csv-search.php'); 
	 
if($_POST['submit_search']=='Get CSV'){

		if ( !isset($condition_query) || $condition_query == '' ) {
            $_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"] = array(InvoiceProforma::COMPLETED,InvoiceProforma::PENDING,InvoiceProforma::ACTIVE);            
            $_SEARCH["chk_paid"]  = 'AND';
            $_SEARCH["paidStatus"] = InvoiceProforma::UN_PAID;           
            $condition_query = " WHERE (". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::COMPLETED ."' OR ". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::PENDING."' OR ". TABLE_BILL_INV_PROFORMA .".status = '". InvoiceProforma::ACTIVE."') AND ".TABLE_BILL_INV_PROFORMA.".paid_status='".InvoiceProforma::UN_PAID."'"   ;
        }
        
        // If the InvoiceProforma is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_inv_list_al') ) {
            $access_level += 1;
        }
       
        
        if ( $perm->has('nc_bl_invprfm_list_all') ) {
        
             $condition_query .='';
             
        }else{
                $condition_query .= " AND ( ";
            
                // If my has created this InvoiceProforma.
                $condition_query .= " ( ". TABLE_BILL_INV_PROFORMA .".created_by = '". $my['user_id'] ."' "
                                        ." AND ". TABLE_BILL_INV_PROFORMA .".access_level < $access_level ) ";
                
                // If my is the Client Manager
                $condition_query .= " OR ( ". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                                    ." AND ". TABLE_BILL_INV_PROFORMA .".access_level < $access_level ) ";
                
                // Check if the User has the Right to view InvoiceProformas created by other Users.
                if ( $perm->has('nc_bl_inv_list_ot') ) {
                    $access_level_o   = $my['access_level'];
                    if ( $perm->has('nc_bl_inv_list_ot_al') ) {
                        $access_level_o += 1;
                    }
                    $condition_query .= " OR ( ". TABLE_BILL_INV_PROFORMA. ".created_by != '". $my['user_id'] ."' "
                                        ." AND ". TABLE_BILL_INV_PROFORMA .".access_level < $access_level_o ) ";
                }
           $condition_query .= " ) ";
        }
     
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
        
            $extra_url  = $condition_url;
            
        }
        $extra_url  .= "&x_csv=$x_csv&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }        
        $condition_url .="&perform=".$perform;
        
        //Code to calculate total of Amount BOF
		 
		$totalAmt =array();
		$totalAmount=$totalAmountInr='';
		//Get total Amount BOF
		/*
		$sql = " SELECT SUM( c.amount_inr ) AS totalAmountInr,
				SUM( c.balance_inr ) AS totalBalanceInr,
				SUM( c.amount ) AS totalAmount,
				SUM( c.balance ) AS totalBalance
			FROM ( 
				SELECT DISTINCT(".TABLE_BILL_INV_PROFORMA.".id), ".TABLE_BILL_INV_PROFORMA.".amount_inr,
				".TABLE_BILL_INV_PROFORMA.".balance_inr,
				".TABLE_BILL_INV_PROFORMA.".amount,
				".TABLE_BILL_INV_PROFORMA.".balance 
			FROM ". TABLE_BILL_INV_PROFORMA."
			LEFT JOIN ". TABLE_BILL_ORDERS." ON ". TABLE_BILL_ORDERS .".number = ". TABLE_BILL_INV_PROFORMA .".or_no 
			LEFT JOIN ". TABLE_BILL_ORD_P ." ON ". TABLE_BILL_ORD_P .".ord_no = ". TABLE_BILL_INV_PROFORMA .".or_no 
			LEFT JOIN ". TABLE_BILL_INV." ON ( ". TABLE_BILL_INV .".inv_profm_no = ". TABLE_BILL_INV_PROFORMA .".number AND
				".TABLE_BILL_INV.".status != '".Invoice::DELETED."' ) 
			LEFT JOIN ". TABLE_USER." ON ". TABLE_USER .".user_id = ". TABLE_BILL_INV_PROFORMA .".created_by 
			LEFT JOIN ". TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_INV_PROFORMA .".client ";			 
		$sql .= " ". $condition_query." ) as c "; 
		if ( $db->query($sql) ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$totalAmountInr = $db->f("totalAmountInr") ;
					$totalAmount = $db->f("totalAmount") ;
					$totalBalanceInr = $db->f("totalBalanceInr") ;
					$totalBalance = $db->f("totalBalance") ;
				}
			}
		}*/
        //Code to calculate total of Amount EOF
        
		//CSV Variables BOF
		$csv_terminated = "\n";
		$csv_separator = ",";
		$csv_enclosed = '"';
		$csv_escaped = "\\";
		$schema_insert = '';
		$schema_insert_nt = '';
		$schema_insert1 = '';
		$schema_insert2 = '';
		$grant_total1 = 0; 
		//CSV Variables EOF
		$out_1=$out_2='';
		
        // To count total records.
        $list	= 	NULL;
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
		$searchStr=1;
        if($searchStr==1){
           $fields = 	" DISTINCT(".TABLE_BILL_INV_PROFORMA.".id)"
                        .','. TABLE_BILL_INV_PROFORMA .'.number'
                        .','. TABLE_BILL_INV_PROFORMA .'.or_no'
                        .','. TABLE_BILL_INV_PROFORMA .'.or_id'
                        .','. TABLE_BILL_INV_PROFORMA .'.access_level'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_fe'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_e'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_c'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_d'
                        .','. TABLE_BILL_INV_PROFORMA .'.do_i'
						.','. TABLE_BILL_INV_PROFORMA .'.paid_status'
                        .','. TABLE_BILL_INV_PROFORMA .'.currency_abbr'
						.','. TABLE_BILL_INV_PROFORMA .'.tax1_name'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_value'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_total_amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub1_name'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub1_value'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub1_total_amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub2_name'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub2_value'
                        .','. TABLE_BILL_INV_PROFORMA .'.tax1_sub2_total_amount'
						.','. TABLE_BILL_INV_PROFORMA .'.sub_total_amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.round_off_op'
						.','. TABLE_BILL_INV_PROFORMA .'.round_off'  
                        .','. TABLE_BILL_INV_PROFORMA .'.amount'
                        .','. TABLE_BILL_INV_PROFORMA .'.amount_inr'
                        .','. TABLE_BILL_INV_PROFORMA .'.balance as prfm_balance'
                        .','. TABLE_BILL_INV_PROFORMA .'.balance_inr as prfm_balance_inr'
						.','. TABLE_BILL_INV .'.number as inv_no'
						.','. TABLE_BILL_INV .'.do_i as inv_dt'
						.','. TABLE_BILL_INV .'.balance'
                        .','. TABLE_BILL_INV .'.balance_inr'
                        .','. TABLE_BILL_INV_PROFORMA .'.status'
                        .','. TABLE_BILL_INV_PROFORMA .'.is_old'
                        .','. TABLE_BILL_INV_PROFORMA .'.old_updated'
                        .','. TABLE_BILL_INV_PROFORMA .'.reason_of_delete'
                        .','. TABLE_BILL_ORDERS .'.order_title'
                        .','. TABLE_BILL_ORDERS .'.followup_status'
						.','. TABLE_BILL_ORDERS .'.tds_status'
						.','. TABLE_BILL_ORDERS .'.id as order_id'
						.','. TABLE_BILL_ORDERS .'.do_o'
						.','. TABLE_BILL_ORDERS .'.is_hardware_ord'
						.','. TABLE_BILL_ORDERS .'.special'
						.','. TABLE_BILL_ORDERS .'.discount_explanation'
						.','. TABLE_BILL_ORDERS .'.npa_status'
                        .','. TABLE_BILL_ORDERS .'.total_tax_paid_amount'
						.','. TABLE_BILL_ORDERS .'.total_tax_balance_amount'
						.','. TABLE_BILL_ORDERS .'.financial_yr'						
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.org'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.mobile1'
                        .','. TABLE_CLIENTS .'.mobile2'
                        .','. TABLE_CLIENTS .'.billing_name AS billing_name'
                        .','. TABLE_CLIENTS .'.status AS c_status';
			//echo $condition_query;
            $total	= InvoiceProforma::getDetails( $db, $list, '', $condition_query);        
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x_csv, $rpp, $condition_url, 'changePageWithExtra');            
            $list	= NULL;
            InvoiceProforma::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
            //InvoiceProforma::getDetails( $db, $list, $fields, $condition_query);
        }
        $key2=0;
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){  
				$key2++;
               $val['is_rcpt_created'] = false;
               $val['is_paid_fully'] = false;
               
				//check invoice id followup in ST BOF
				$val['ticket_id']='';
				$sql2 = "SELECT ticket_id FROM ".TABLE_ST_TICKETS." WHERE 
				(".TABLE_ST_TICKETS.".flw_ord_id = '".$val['order_id']."') AND 
				".TABLE_ST_TICKETS.".ticket_child =0  LIMIT 0,1";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                       while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                       }
                    }                   
                }
				//check invoice id followup in ST EOF
				$is_invoice_created=0;
				$sql= " SELECT COUNT(*) as count FROM ".TABLE_BILL_INV." WHERE ".TABLE_BILL_INV.".or_no
				='".$val['or_no']."' AND 
				".TABLE_BILL_INV.".status !='".Invoice::DELETED."' LIMIT 0,1";
                $db->query($sql);               
                if ( $db->nf()>0 ) {
                    while ( $db->next_record() ) {
                        $count = $db->f('count') ;
                        if( $count > 0){
                            $is_invoice_created = 1;
                        }
                    }
                }
				$val['is_invoice_created'] = $is_invoice_created ;
				$val['tax_amount'] = $val['tax1_total_amount'] + $val['tax1_sub1_total_amount'] + $val['tax1_sub2_total_amount'] ;
				$val['tax_amount'] = number_format($val['tax_amount'],2);
                $val['sub_total_amount']=number_format($val['sub_total_amount'],2);
				$val['amount_numeric'] = $val['amount'];
                $val['amount']=number_format($val['amount'],2);
                $val['amount_inr']=number_format($val['amount_inr'],2);
				
				/* if(!empty($val['inv_no'])){
					$val['balance']=number_format($val['balance'],2);
					$val['balance_inr']=number_format($val['balance_inr'],2);
				}else{					   
					$val['balance']=number_format($val['prfm_balance'],2);
					$val['balance_inr']=number_format($val['prfm_balance_inr'],2);
				} */
                $val['balance']=number_format($val['prfm_balance'],2);
				$val['balance_inr']=number_format($val['prfm_balance_inr'],2);
				$val['rcpt_amount'] = $val['amount_numeric'] - $val['prfm_balance'] ; 
				
				$val['tax1_value1'] = trim($val['tax1_value']);
				$val['tax1_value1'] = strstr($val['tax1_value1'], '%', true);
				
				$val['tax1_sub1_value1'] = trim($val['tax1_sub1_value']);
				 $val['tax1_sub1_value1'] = strstr($val['tax1_sub1_value1'], '%', true);
				
				$val['tax1_sub2_value1'] = trim($val['tax1_sub2_value']);
				$val['tax1_sub2_value1'] = strstr($val['tax1_sub2_value1'], '%', true);
				$sub1 = (float) ($val['tax1_sub1_value1']/$val['tax1_value1']);
				$sub2 = (float) ($val['tax1_sub2_value1']/$val['tax1_value1']);
				$dFac=100+$val['tax1_value1'] + $sub1 + $sub2 ;
				$mFact = $val['tax1_value1'];
				$m1Fact = $val['tax1_sub1_value1']/$val['tax1_value1'] ;
				$m2Fact = $val['tax1_sub2_value1']/$val['tax1_value1'] ;
				$tax= ($val['rcpt_amount']/$dFac)*$mFact ;
				$subtax1= ($val['rcpt_amount']/$dFac)*$m1Fact ;
				$subtax2= ($val['rcpt_amount']/$dFac)*$m2Fact ;
				$val['rcpt_amount_payable_tax'] = ($tax + $subtax1 + $subtax2);
				$val['rcpt_amount_payable_tax'] = round($val['rcpt_amount_payable_tax'],2);
                $sql= " SELECT id as rcpt_id FROM ".TABLE_BILL_RCPT." WHERE ".TABLE_BILL_RCPT.".inv_profm_id
				='".$val['id']."' AND ".TABLE_BILL_RCPT.".status ='".Receipt::ACTIVE."' LIMIT 0,1";
                $db->query($sql);   
				$is_rcpt_created = 0;				
                if ( $db->nf()>0 ) {
                    while ( $db->next_record() ) {
                        $rcpt_id = $db->f('rcpt_id') ;
                        if( $rcpt_id > 0){
                            $is_rcpt_created = 1;
                        }
                    }
                }
			    $val['is_rcpt_created'] = $is_rcpt_created;
                $flist[$key]=$val; 
				 
				$number =$val['number'] ;
				$inv_no =$val['inv_no'] ;
				$financial_yr =$val['financial_yr'] ;
				$or_no =$val['or_no'] ;
				$order_title =$val['order_title'] ;
				 
		 
				$do_o = date('d M Y',strtotime($val['do_o'])) ;
				$do_c =  date('d M Y',strtotime($val['do_c'])) ;
				if($val['do_fe']=='0000-00-00 00:00:00'){
					$do_fe =	'' ;
				}else{
					$do_fe =  date('d M Y',strtotime($val['do_fe'])) ;
				} 
				if($val['do_e']=='0000-00-00 00:00:00'){
					$do_e =	'' ;
				}else{
					$do_e =  date('d M Y',strtotime($val['do_e'])) ;
				}
				 
				$do_i = date('d M Y',strtotime($val['do_i'])) ;
				$billing_name =$val['billing_name'] ;
				$sub_total_amount =$val['sub_total_amount'] ;
				$tax1_name =$val['tax1_name'] ;
				$tax_amount =$val['tax_amount'] ;
				$round_off_op =$val['round_off_op']." ".$val['round_off'];
				$amount =$val['amount'] ;
				$amount_inr =$val['amount_inr'] ;
				$prfm_balance =$val['prfm_balance'] ;
				$prfm_balance_inr =$val['prfm_balance_inr'] ;
				$rcpt_amount =$val['rcpt_amount'] ;
				$rcpt_amount_payable_tax =$val['rcpt_amount_payable_tax'] ;
				$total_tax_paid_amount =$val['total_tax_paid_amount'] ;
				$total_tax_balance_amount =$val['total_tax_balance_amount'] ;
				
				$myTData=array(
					0=>'key2',
					1=>'number',
					2=>'inv_no',
					3=>'financial_yr',
					4=>'or_no',
					5=>'order_title',
					6=>'do_o',
					7=>'do_c',
					8=>'do_fe',
					9=>'do_e',
					10=>'do_i',
					11=>'billing_name',
					12=>'sub_total_amount',
					13=>'tax1_name',
					14=>'tax_amount',
					15=>'round_off_op',
                    16=>'amount',
					17=>'amount_inr',
					18=>'prfm_balance',
					19=>'prfm_balance_inr',
					20=>'rcpt_amount',
					21=>'rcpt_amount_payable_tax',
					22=>'total_tax_paid_amount',
					23=>'total_tax_balance_amount'
				);
				
				$schema_insert11='';
				foreach($myTData as $key_1 =>$val3){
					$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes($$val3)) . $csv_enclosed;
					$schema_insert11 .= $l.$csv_separator;
				}
				$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
				$schema_insert1 .= $csv_terminated;
				
            }
        }

	
	 
	$heading1=array('Sr No','Proforma Invoice Number','Invoice Number','Financial Yr','Order number',
	'Order Title','Order Date','Date of create','Service Frm Dt','Expiry Date','Proforma Invoice Date','Billing Name',
	'Sub Tot.','TaxName','Tax Amt','Round off','Amount','Amount INR','Balance','Balance INR','Rcpt Amt','Rcpt Payable Tax. Amt','Paid Tax.Amt','Tax Bal Amt');
	
	/*number, inv_no,financial_yr,or_no,order_title,do_o,do_c,do_fe,do_e,do_i,billing_name,sub_total_amount,tax1_name,tax_amount,round_off_op - round_off, amount,amount_inr,{if $invoice.prfm_balance > 0}  prfm_balance {/if}, prfm_balance_inr,rcpt_amount,rcpt_amount_payable_tax,
	total_tax_paid_amount,total_tax_balance_amount
	*/
	
	
	$fields_cnt = count($heading1);
	/* $mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results for Purchae Bills - '.$company_name.' - Period  - '.date('d M Y',strtotime($dfa)) .' - '.date('d M Y',strtotime($dta)).' '.$taxname_str.' ')) . $csv_enclosed;
	 */
	$mHead1 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes('Following Results found - ')) . $csv_enclosed;
			
	$schema_insert .= $mHead1;
	$schema_insert .= $csv_terminated;
	$schema_insert .= $csv_separator;
	$schema_insert .= $csv_terminated;
	$mHead2 = '';
	/* $mHead2 = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes( $taxname.' Particulars of the Bills')). $csv_enclosed; */
	$schema_insert .= $mHead2;
	$schema_insert .= $csv_terminated;
	$schema_insert .= $csv_separator;
	$schema_insert .= $csv_terminated;
	for ($i = 0; $i < $fields_cnt; $i++){
		$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($heading1[$i])) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
	}// end for
 
	$out1 = trim(substr($schema_insert, 0, -1));
	$out1 .= $csv_terminated;
	$out1 .= $schema_insert1 ;
	$filename = date('dmY').".csv";
	//$out = $out_1.$csv_terminated.$out_2;
	/* 
	$myTData=array(0=>'key',
			  1=>'billing_name',
			  2=>'inv_no',
			  3=>'do_i',
			  4=>'pinv_no',
			  5=>'do_pi',
			  6=>'pbalance',
			  7=>'pbalance_inr',
			  8=>'amount',
			  9=>'rcpt_no',
			  10=>'do_r',
			  11=>'do_transaction',
			  12=>'do_voucher',
			  13=>'rcpt_amount',
			  14=>'rcpt_amount_payable_tax',
			  15=>'total_tax_paid_amount',
			  16=>'total_tax_balance_amount',
			  17=>'tds_amount',
			  18=>'tax1_name',
			  19=>'tax1_value',
			  20=>'tax1_amount',
			  21=>'tax1_sub1_name',
			  22=>'tax1_sub1_value',
			  23=>'tax1_sub1_amount',
			  24=>'tax1_sub2_name',
			  25=>'tax1_sub2_value',
			  26=>'tax1_sub2_amount'
						  
	);
	$schema_insert11='';
	foreach($myTData as $key_1 =>$val){
		$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,stripslashes($$val)) . $csv_enclosed;
		//$schema_insert1 .= $l;
		$schema_insert11 .= $l.$csv_separator;
	}
	
	
	
	$schema_insert1 .= trim(substr($schema_insert11, 0, -1));
	$schema_insert1 .= $csv_terminated;*/
	//$out1 = trim(substr($schema_insert1, 0, -1));
	//$out1 .= $csv_terminated;
	
	
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Length: " . strlen($out1));
	// Output to browser with appropriate mime type, you choose ;)
	header("Content-type: text/x-csv");
	//header("Content-type: text/csv");
	//header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=$filename");
	echo $out1;
	//Write CSV EOF
		exit;
	//EOF DOWNLOAD CSV


}
	
	
?>