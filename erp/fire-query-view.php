<?php

    if ( $perm->has('nc_fq_details') ) {
        
        $ticket_id	= isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
	    //$user_id = $my['user_id'];
        
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        //BOF read the available departments
        //FireQuery::getDepartments($db,$department);
        //EOF read the available departments
        
        $variables["MAX_FILE_SIZE"] = MAX_FILE_SIZE ;
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
            
            if ( FireQuery::validateAdd($data, $extra) ) {
                
                $ticket_no  =  "PT". date("ymdhi-s");
                
                if(!empty($data['ticket_attachment'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['ticket_attachment']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    $attachfilename = $ticket_no.".".$ext ;
                    $data['ticket_attachment'] = $attachfilename;
                    
                    $ticket_attachment_path = DIR_WS_FQ_FILES;
                    
                    if (move_uploaded_file ($files['ticket_attachment']['tmp_name'], DIR_FS_FQ_FILES."/".$attachfilename)){
                        
                    }
                }
                
                $query = "SELECT "	. TABLE_FQ_TICKETS .".ticket_date "
								." FROM ". TABLE_FQ_TICKETS 
								." WHERE ". TABLE_FQ_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
								." AND ("
											. TABLE_FQ_TICKETS .".ticket_child = '". $ticket_id ."' " 
											." OR "
											. TABLE_FQ_TICKETS .".ticket_id = '". $ticket_id ."' " 
										.") "
								." AND ". TABLE_FQ_TICKETS .".ticket_owner_uid = '". $data["ticket_owner_uid"] ."' "
								." ORDER BY ". TABLE_FQ_TICKETS .".ticket_date DESC ";				
				
				$db->query($query) ;
				if($db->nf() > 0){
					$db->next_record() ;
					$last_time = $db->f("ticket_date") ;
					$ticket_response_time = time() - $last_time ;
				}
				else{
					$ticket_response_time = 0 ;
				}
				$ticket_date 		= time() ;
				$ticket_replied = '0' ;
                
				$query = "INSERT INTO "	. TABLE_FQ_TICKETS ." SET "
										. TABLE_FQ_TICKETS .".ticket_no         = '". $ticket_no ."', "
										. TABLE_FQ_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
										. TABLE_FQ_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
										. TABLE_FQ_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
										. TABLE_FQ_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
										. TABLE_FQ_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
										. TABLE_FQ_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
										. TABLE_FQ_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
										. TABLE_FQ_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
										. TABLE_FQ_TICKETS .".ticket_status     = '". $data['ticket_status'] ."', "
										. TABLE_FQ_TICKETS .".ticket_child      = '".$ticket_id."', "
										. TABLE_FQ_TICKETS .".ticket_attachment = '". $attachfilename ."', "
										. TABLE_FQ_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
										. TABLE_FQ_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
										. TABLE_FQ_TICKETS .".ticket_replied    = '".$ticket_replied."', "
                                        //. TABLE_FQ_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
                                        . TABLE_FQ_TICKETS .".is_private           = '". $data['is_private'] ."', "
                                        . TABLE_FQ_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
                                        . TABLE_FQ_TICKETS .".do_e    = '".date('Y-m-d H:i:s', time())."', "
										. TABLE_FQ_TICKETS .".ticket_date       = '". $ticket_date ."' " ;
				
				//unset($GLOBALS["flag"]);
				//unset($GLOBALS["submit"]);
                            
                if ($db->query($query) && $db->affected_rows() > 0) {
                    
                    $variables['hid'] = $db->last_inserted_id() ;
                    
                    $query = "UPDATE ". TABLE_FQ_TICKETS 
								." SET ". TABLE_FQ_TICKETS .".ticket_replied = '0' "
								." WHERE ". TABLE_FQ_TICKETS .".ticket_id = '". $ticket_child ."' " ;
				    $db->query($query) ;
                    /**************************************************************************
                    * Send the notification to the ticket owner and the Administrators
                    * of the new Ticket being created
                    **************************************************************************/
                    
                    $data['ticket_no']    =   $ticket_no ;
                    foreach( $status as $key=>$val){                           
                        if ($data['ticket_status'] == $val['status_id'])
                        {
                            $data['status']=$val['status_name'];
                        }
                    }
                    
                    $data['replied_by']   =   $data['ticket_owner'] ;
                    $data['subject']    =   $data['ticket_subject'] ;
                    $data['text']    =   $data['ticket_text'] ;
                    $data['attachment']   =   $attachfilename ;
                    $data['link']   = DIR_WS_MP .'/fire-query.php?perform=view&ticket_id='. $variables['hid'];
                    
                    // Send Email to the ticket owner BOF  
                    Clients::getList($db, $userDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$data['ticket_owner_uid']."'");
                    if(!empty($userDetails)){
                        $userDetails=$userDetails['0'];
                        $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                        $email = NULL;
                        
                        if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' => $userDetails['email']);
                           
                            
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }                        
                    }
                    // Send Email to the ticket owner EOF
                    
                    
                    // Send Email to the admin BOF
                    $email = NULL;
                    $data['link']   = DIR_WS_NC .'/fire-query.php?perform=view&ticket_id='. $variables['hid'];
                   
                    if ( getParsedEmail($db, $s, 'EMAIL_ST_ADMIN', $data, $email) ) {
                        $to = '';
                        $to[]   = array('name' => $support_name , 'email' => $support_email);
                       
                        
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }                        
                    
                    // Send Email to the admin EOF
                    
                    $messages->setOkMessage("A comment has been added to the support ticket. And email Notification has been sent.");
                    
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        if( isset($_POST["btnUpdate"]) && $_POST["btnUpdate"] == "Update"){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
			$query = "UPDATE ". TABLE_FQ_TICKETS 
							." SET ". TABLE_FQ_TICKETS .".ticket_priority = '". $data["ticket_priority"] ."', "
									. TABLE_FQ_TICKETS .".ticket_status = '". $data["ticket_status"] ."', "
									. TABLE_FQ_TICKETS .".ticket_replied = '0' "
							." WHERE ". TABLE_FQ_TICKETS .".ticket_id = '". $ticket_id ."' " 
							." AND ". TABLE_FQ_TICKETS .".ticket_owner_uid = '". $data["ticket_owner_uid"] ."' ";
			if($db->query($query)){
				$messages->setOkMessage("Status and Priority Updated Successfully.");
			}
			
			$query = "UPDATE ". TABLE_FQ_TICKETS 
							." SET ". TABLE_FQ_TICKETS .".ticket_priority = '". $data["ticket_priority"] ."', "
									. TABLE_FQ_TICKETS .".ticket_status = '". $data["ticket_status"] ."', "
									. TABLE_FQ_TICKETS .".ticket_replied = '0' "
							." WHERE ". TABLE_FQ_TICKETS .".ticket_child = '". $ticket_id ."' " 
							." AND ". TABLE_FQ_TICKETS .".ticket_owner_uid = '". $data["ticket_owner_uid"] ."' ";
			$db->query($query) ;
			
		}
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/fire-query-list.php');
        }
        else {
            $condition_query= " WHERE ticket_id = '". $ticket_id ."' ";
            $_ALL_POST      = NULL;
            
            if ( FireQuery::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST[0];
            }
                    
            $table = TABLE_FQ_DEPARTMENT;
            $condition2 = " WHERE ".TABLE_FQ_DEPARTMENT .".id= '".$_ALL_POST['ticket_department'] ."' " ;
            $fields1 =  TABLE_FQ_DEPARTMENT .'.department_name';
            $ticket_department= getRecord($table,$fields1,$condition2);
            
            $_ALL_POST['department_name']    = $ticket_department['department_name'] ;     
            
            $condition_query= " WHERE user_id = '". $_ALL_POST['ticket_owner_uid'] ."' ";
            $member      = NULL;
            
            if ( Clients::getList($db, $member, 'email', $condition_query) > 0 ) {
                $_ALL_POST['member'] = $member[0];
            }
            
            $query = "SELECT "	. TABLE_FQ_TICKETS .".ticket_no, "
								. TABLE_FQ_TICKETS .".ticket_subject, "
								. TABLE_FQ_TICKETS .".ticket_text, "
								. TABLE_FQ_TICKETS .".ticket_creator, "
								. TABLE_FQ_TICKETS .".ticket_attachment, "
								. TABLE_FQ_TICKETS .".ticket_attachment_path, "
								. TABLE_FQ_TICKETS .".ticket_response_time, "
								. TABLE_FQ_TICKETS .".ticket_date "
							." FROM ". TABLE_FQ_TICKETS 
							." WHERE ". TABLE_FQ_TICKETS .".ticket_owner_uid = '". $_ALL_POST['ticket_owner_uid'] ."' " 
							." AND ". TABLE_FQ_TICKETS .".ticket_child = '". $ticket_id ."' " 
							." ORDER BY ". TABLE_FQ_TICKETS .".ticket_date ASC";
			
			$db->query($query) ;
			$ticketThreads = array() ;
			while($db->next_record()){
				$response["h"] = round($db->f("ticket_response_time") / 3600) ;
				$response["m"] = round(($db->f("ticket_response_time") % 3600) / 60 ) ;
				$response["s"] = round(($db->f("ticket_response_time") % 3600) % 60 ) ;
				
				$ticketThreads[] = array(	"ticket_no" 			=> $db->f("ticket_no"), 
											"ticket_subject" 		=> $db->f("ticket_subject"), 
											"ticket_text" 			=> preg_replace("(\r\n|\r|\n)", "<br/>", $db->f("ticket_text")), 
											"ticket_creator" 		=> $db->f("ticket_creator"), 
											"ticket_attachment" 	=> $db->f("ticket_attachment"), 
											"ticket_attachment_path"=> $db->f("ticket_attachment_path"),
											"ticket_response_time" 	=> $response["h"] ." hr(s) : "
																		. $response["m"] ." min(s) :" 
																		. $response["s"]." sec(s) ",
											"ticket_date" 			=> $db->f("ticket_date")
										);
			}		
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'view');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'ticket_id', 'value' => $ticket_id);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'ticketThreads', 'value' => 'ticketThreads');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'fire-query-view.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>