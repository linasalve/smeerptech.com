<?php
    if ( $perm->has('nc_site_tkr_status') ) {
        $id = isset($_GET["id"])? $_GET["id"] : ( isset($_POST["id"])? $_POST["id"]: '' );
		$status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
        $extra = array( 'db' 		    => &$db,
                        'messages' 	    => $messages
                    );
        
        Ticker::updateStatus($id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/ticker-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Change Status.");
    }
?>