<?php

    if ( $perm->has('nc_vbk_details') ) {
        $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
       

        if ( VendorsBank::getList($db, $list, '*', " WHERE user_id = '$user_id'") > 0 ) {
            $_ALL_POST = $list['0'];
       
            
			include_once ( DIR_FS_CLASS .'/RegionVendorBank.class.php');
			include_once ( DIR_FS_CLASS .'/PhoneVendorBank.class.php');
			include_once ( DIR_FS_CLASS .'/ReminderVendorBank.class.php');
			

			$region         = new RegionVendorBank();
			$phone          = new PhoneVendorBank(TABLE_VENDORS_BANK);
			$reminder          = new ReminderVendorBank(TABLE_VENDORS_BANK);
			

			//$_ALL_POST['manager'] = VendorsBank::getManager($db, '', $_ALL_POST['manager']);
			$_ALL_POST['industry']    = explode(",", $_ALL_POST['industry']);
			//BOF read the available industries
			$industry_list = NULL;
			$required_fields ='*';
			$condition_query="WHERE status='".Industry::ACTIVE."'";
			Industry::getList($db,$industry_list,$required_fields,$condition_query);
			//EOF read the available industries
			
		   
			$_ALL_POST['team_list'] = NULL;
			$required_fields = "f_name,l_name,number";
			$_ALL_POST['team']= str_replace(',',"','",$_ALL_POST['team']);
			$condition_query= " WHERE user_id IN('".$_ALL_POST['team']."') ";
			User::getList($db,$_ALL_POST['team_list'],$required_fields,$condition_query);               
			// Read the available Access Level and User Roles.
			
			$_ALL_POST['access_level']  = VendorsBank::getAccessLevel($db, $access_level, $_ALL_POST['access_level']);
			$_ALL_POST['roles']         = VendorsBank::getRoles($db, $access_level, $_ALL_POST['roles']);
			//print_r($role_list);
			// Read the Contact Numbers.
			$phone->setPhoneOf(TABLE_VENDORS_BANK, $user_id);
			$_ALL_POST['phone'] = $phone->get($db);
			
			// Read the Addresses.
			$region->setAddressOf(TABLE_VENDORS_BANK, $user_id);
			$_ALL_POST['address_list'] = $region->get();
			
			// Read the Reminders.
			$reminder->setReminderOf(TABLE_VENDORS_BANK, $user_id);
			$_ALL_POST['reminder_list'] = $reminder->get($db);
			
			$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';                
            $condition2 = " WHERE ".TABLE_USER .".user_id IN('".$_ALL_POST['created_by']."') " ;
			$table =TABLE_USER ;              
            $createdArr = getRecord($table,$fields1,$condition2);
            if(!empty($createdArr)){
                $createdBy = $createdArr['f_name']." ".$createdArr['l_name'] ;   
            }
            $_ALL_POST['added_by'] = $createdBy ;
			
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-bank-view.html');
            
        }
        else {
            $messages->setErrorMessage("The Vendors details were not found. ");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>