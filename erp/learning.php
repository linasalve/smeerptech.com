<?php
	
	echo "START1=>".$time1 = microtime(true); 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header-ws.php" );
    include_once ( DIR_FS_INCLUDES .'/score-sheet.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/common-functions.inc.php' );
	
  
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]   : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
    $condition_url = $condition_query = '';
    
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
    $condition_query ='';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
   
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    
    if($added){
         $messages->setOkMessage("Record has been added successfully.");
    }
    if ( $perm->has('nc_ss') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_SCORE_SHEET   =>  array( 'Comment To'           => 'comment_to_name',
                                                               'Comment By'           => 'created_by_name',
                                                               'Particulars'          => 'particulars'
                                                          )
                              
                                                        
                                );
        
        $sOrderByArray  = array(
                                TABLE_SCORE_SHEET => array( 
                                                            'Date'  => 'date'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'date';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SCORE_SHEET;
        }
        
       // Read the available Status 
        $variables['score'] = Scoresheet::getScore();
        $variables['reason'] = Scoresheet::getReason();
       
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {
				ob_start();
				$main_file_name='learning-add.php';
			    include (DIR_FS_NC .'/index-ws.php');
				ob_flush();
                /*  
				include (DIR_FS_NC.'/score-sheet-add.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'score-sheet.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html'); */
                break;
            }
            case ('lblprint'): {
                include (DIR_FS_NC .'/score-sheet-pdf-print.php'); 
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('edit'): {
                include (DIR_FS_NC .'/score-sheet-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'score-sheet.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('acomm'): {
				 
				if ( $perm->has('nc_ss_add_comment') ) { 
					$main_file_name='learning-add-comment.php';
					include (DIR_FS_NC .'/blank.php'); 
					
				}else{
					$messages->setErrorMessage("You donot have the Right to Access this Module.");
					$error_messages = $messages->getErrorMessages();
					$success_messages = $messages->getOkMessages();
					$main_file_name='messages.php';
					include (DIR_FS_NC .'/blank.php'); 
				}
                //include (DIR_FS_NC .'/score-sheet-add-comment.php');
               // $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
            case ('view'): {
                include (DIR_FS_NC .'/score-sheet-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('print'): {
                include (DIR_FS_NC .'/score-sheet-print.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			            
            case ('search'): {
                include(DIR_FS_NC."/learning-search.php");
                $main_file_name='learning-list.php';
			    include (DIR_FS_NC .'/index-ws.php'); 
                //$page["section"][] = array('container'=>'CONTENT', 'page' => 'score-sheet.html');
                // $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('delete'): {
                $perform='list';
                include ( DIR_FS_NC .'/score-sheet-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'score-sheet.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }          
            case ('list'):
            default: {
				//ob_start();
				if ( $perm->has('nc_ss_list') ) { 
					$main_file_name='learning-list.php';
					include (DIR_FS_NC .'/index-ws.php'); 
					
				}else{
					$messages->setErrorMessage("You donot have the Right to Access this Module.");
					$error_messages = $messages->getErrorMessages();
					$success_messages = $messages->getOkMessages();
					$main_file_name='messages.php';
					include (DIR_FS_NC .'/index-ws.php');
				}
				//ob_flush();
                // CONTENT = CONTENT
                /* 
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'score-sheet.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html'); */
                break;
            }
        }
        /* $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray'); */
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
       /*  $page["section"][] = array('container'=>'CONTENT', 'page' => 'score-sheet.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html'); */
    }
	/*  */

    
	include_once( DIR_FS_NC ."/flush.php");
	
	echo "START2=>".$time2 = microtime(true);
	$time3=$time2-$time1;
	echo "<br/>TOTAL".$time3;
?>