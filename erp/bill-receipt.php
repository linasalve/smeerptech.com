<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
    include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
    
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $deleted 		= isset($_GET["deleted"])         ? $_GET["deleted"]        : ( isset($_POST["deleted"])          ? $_POST["deleted"]       :'0');
    $rcp_no 		= isset($_GET["rcp_no"])         ? $_GET["rcp_no"]        : ( isset($_POST["rcp_no"])          ? $_POST["rcp_no"]       :'');
    $hid 		= isset($_GET["hid"])         ? $_GET["hid"]        : ( isset($_POST["hid"])          ? $_POST["hid"]       :'');
    $condition_url =$condition_query = '';
    
    
    if($added){    
        $messages->setOkMessage("New Receipt $rcp_no has been created.");
    }
    if($deleted){    
        $messages->setOkMessage("Receipt $rcp_no has been deleted.");
    }
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    //By default search in On
    $_SEARCH["searched"]    = true ;
    if ( $perm->has('nc_bl_rcpt') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_BILL_RCPT  =>  array( 
                                                            'Receipt No.'   => 'number',
                                                            'Invoice No.'	=> 'inv_no',
															'Voucher No.'	=> 'voucher_no',
															'Proforma Invoice No.'	=> 'inv_profm_no',
                                                            'Remarks'       => 'remarks'
                                                        ),
								TABLE_BILL_ORDERS =>  array( 'Order No'   => 'number-'.TABLE_BILL_ORDERS,
															'Financial Year'   => 'financial_yr-'.TABLE_BILL_ORDERS,
                                                            'Order Title'   => 'order_title-'.TABLE_BILL_ORDERS
                                                        ),
                                                          
                                // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                TABLE_USER      =>  array(  'Creator Number'    => 'number-user',
                                                            'Creator Username'  => 'username-user',
                                                            'Creator Email'     => 'email-user',
                                                            'Creator First Name'=> 'f_name-user',
                                                            'Creator Last Name' => 'l_name-user',
                                                ),
								TABLE_CLIENTS 	=>  array(  'Client Number'    => 'number-clients',
                                                            'Client Username'  => 'username-clients',
                                                            'Client Email'     => 'email-clients',
                                                            'Client First Name'=> 'f_name-clients',
                                                            'Client Last Name' => 'l_name-clients',
                                                            'Billing Name' => 'billing_name-clients',
													),
                            );
        
        $sOrderByArray  = array(
                                TABLE_BILL_RCPT => array('ID'               => 'id',
                                                        'Receipt No.'       => 'number',
                                                        'Date of Receipt'   => 'do_r',
                                                        'Date of Creation'  => 'do_c',
                                                        'Status'            => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            if(!empty($hid)){
                $_SEARCH['sOrderBy']= $sOrderBy = 'id';
                $variables['hid']=$hid;
            }else{
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_r';
            }
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_BILL_RCPT;
        }

        // Read the available Status for the Pre Order.
        $variables['status']        = Receipt::getStatus();
        $variables['status_title']  = Receipt::getStatusTitle();
        
        //use switch case here to perform action. 
        switch ($perform) {
            case ('add_receipt'): 
            case ('add'): {
                //include (DIR_FS_NC.'/bill-receipt-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('rcpt_pdf'): {
			
				include (DIR_FS_NC.'/bill-receipt-pdf-create.php');
			    $page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
				 
				break;
			}
            case ('rcpt_prfm_add'): {
                include (DIR_FS_NC.'/bill-receipt-profm-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('add_o'): {
                include (DIR_FS_NC.'/bill-receipt-add-o.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_receipt'): 
            case ('edit'): {
                include (DIR_FS_NC .'/bill-receipt-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_receipt'): 
            case ('view'): {
                include (DIR_FS_NC .'/bill-receipt-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('view_file_receipt'): 
            case ('view_file'): {
                include (DIR_FS_NC .'/bill-receipt-view-file.php');
                break;
            }
            
            case ('send'): {    
                include (DIR_FS_NC .'/bill-receipt-send.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
            case ('change_status_receipt'): 
            case ('change_status'): {
                $searchStr=1;
                include ( DIR_FS_NC .'/bill-receipt-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search_receipt'): 
            case ('search'): {
                include(DIR_FS_NC."/bill-receipt-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('delete_receipt'):
            case ('delete'): { 
                include ( DIR_FS_NC .'/bill-receipt-delete.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('profm_delete'): {
                include ( DIR_FS_NC .'/bill-receipt-profm-delete.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			
			
            /*BOF added on 30-april-2009*/
            case ('followup'): {
                include (DIR_FS_NC .'/bill-receipt-followup.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*EOF added on 30-april-2009*/
            case ('list_receipt'):
            case ('list'):
            default: {
                $searchStr = 1;
                include (DIR_FS_NC .'/bill-receipt-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-receipt.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	
	include_once( DIR_FS_NC ."/flush.php");
?>