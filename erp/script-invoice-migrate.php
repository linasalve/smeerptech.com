<?php

// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/old-nc-db.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/common-functions.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
    ini_set("max_execution_time",0);// TO EXECUTE A SCRIPT WITH UN LIMITED TIME
    
    // define tables of old nc BOF
    define("TABLE_USER_OLD","auth_user");
    define("TABLE_MEMBER_DETAILS_OLD","member_details");
	define("TABLE_BILL_INVOICE_OLD","bills_invoice");
	define("TABLE_BILL_ORDERS_OLD","bills_orders");
	//define("TABLE_BILL_MISC","bills_misc");
	define("TABLE_BILL_USER","bills_member_account");
	define("TABLE_BILL_RECEIPT_OLD","bills_receipt");
	define("TABLE_MIGRATING_ORDER","migrating_order_info");
	define("TABLE_MIGRATING_INVOICE","migrating_invoice_info");
    // define tables of old nc EOF
    
   


    $db1 		= new db_local; // database handle
    // connect with old nc d/b
    $con = DB::makeConnection();
    $checkdate = '2007-03-31 23:59:59' ;
    
    $timedate= mktime("23","59","59", "03", "31", "2007") ;
     $timedate= mktime("23","59","59", 03, 31, 2007) ; // 31=> 32 as per given in old nc billOrders.php
    $con1 = DB::makeConnection();
    
    //$query = "SELECT ".TABLE_BILL_INVOICE_OLD.".*  FROM ".TABLE_BILL_INVOICE_OLD." WHERE invoice_created_date <= ".$timedate." ORDER BY  invoice_id " ;
    $query = "SELECT ".TABLE_BILL_INVOICE_OLD.".*  FROM ".TABLE_BILL_INVOICE_OLD." WHERE invoice_date <= ".$timedate." ORDER BY  invoice_id " ;
    
    //$query = "SELECT ".TABLE_BILL_INVOICE_OLD.".*  FROM ".TABLE_BILL_INVOICE_OLD." ORDER BY  invoice_id  " ;
    /*
    $query = "SELECT   ". TABLE_BILL_INVOICE_OLD .".invoice_id "
						.", ". TABLE_BILL_INVOICE_OLD .".uid " 
						.", ". TABLE_BILL_INVOICE_OLD .".b_name"
						.", ". TABLE_BILL_INVOICE_OLD .".b_add1"
						.", ". TABLE_BILL_INVOICE_OLD .".b_add2"
						.", ". TABLE_BILL_INVOICE_OLD .".b_city"
						.", ". TABLE_BILL_INVOICE_OLD .".b_state"
						.", ". TABLE_BILL_INVOICE_OLD .".b_country"
						.", ". TABLE_BILL_INVOICE_OLD .".b_zip"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_no"
						.", ". TABLE_BILL_INVOICE_OLD .".order_no"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_opening_balance"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_particulars"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_note"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_amount"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_amount_words "
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_balance"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_date"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_created_date"
						.", ". TABLE_BILL_INVOICE_OLD .".invoice_exp_date"
						.", ". TABLE_MEMBER_DETAILS_OLD .".title"
						.", ". TABLE_USER_OLD .".username" 
						.", ". TABLE_USER_OLD .".company" 
						.", ". TABLE_USER_OLD .".currency" 
						.", ". TABLE_MEMBER_DETAILS_OLD .".fname"
						.", ". TABLE_MEMBER_DETAILS_OLD .".lname"
						.", ". TABLE_MEMBER_DETAILS_OLD .".add1_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".add2_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".city_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".state_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".country_h"			
						.", ". TABLE_MEMBER_DETAILS_OLD .".zip_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".add1_o"				
						.", ". TABLE_MEMBER_DETAILS_OLD .".add2_o"
						.", ". TABLE_MEMBER_DETAILS_OLD .".city_o"				
						.", ". TABLE_MEMBER_DETAILS_OLD .".state_o"
						.", ". TABLE_MEMBER_DETAILS_OLD .".country_o"			
						.", ". TABLE_MEMBER_DETAILS_OLD .".zip_o"
						.", ". TABLE_MEMBER_DETAILS_OLD .".primary_address"
						.", ". TABLE_BILL_USER .".b_user_status "
				." FROM ". TABLE_BILL_INVOICE_OLD 
				." INNER JOIN ". TABLE_MEMBER_DETAILS_OLD ." ON  ". TABLE_BILL_INVOICE_OLD .".uid=". TABLE_MEMBER_DETAILS_OLD .".uid"
				." INNER JOIN ". TABLE_USER_OLD ." ON  ". TABLE_BILL_INVOICE_OLD .".uid=". TABLE_USER_OLD .".uid "
                ." INNER JOIN ". TABLE_BILL_USER ." ON  ". TABLE_BILL_INVOICE_OLD .".uid=". TABLE_BILL_USER .".uid"
                ." ORDER BY ".TABLE_BILL_INVOICE_OLD.".invoice_id ";
				
            
	$result = mysql_query($query,$con);
    $listInvDone =$listInvid= $orderNotFound= $invoiceExist=array();
    $orderNotExistContent  =$invoiceExistContent=$billaddNotFoundContent=$invoiceEnterdContent='';
	if( mysql_num_rows($result) > 0){
        while ($row = mysql_fetch_array($result)){
            $error=array();
            
            $oldinvoice_id = $row['invoice_id'];
            $uid = $row['uid'];
            //default curency
            $currency ='INR';
            $u_name = $company= '';
            //Default currency details bof
            $currency_id = 1 ;
            $currency_abbr = 'INR';
            $currency_name = 'Rupees';
            $currency_symbol = 'Rs';
            $currency_country = 'India';
            //Default currency details eof
            
            //select currency from user table
           
            $currency_old =$row['currency'];
            $currency = substr($row['currency'], 0, 3); 
            $currency = trim($currency);
            if($currency=='INR'){
                $currency_id = 1 ;
                $currency_abbr = 'INR';
                $currency_name = 'Rupees';
                $currency_symbol = 'Rs';
                $currency_country = 'India';
            }elseif($currency=='USD'){
                $currency_id = 2 ;
                $currency_abbr = 'USD';
                $currency_name = 'USD';
                $currency_symbol = '$';
                $currency_country = 'United States of America';
            }
            
            
            $pre = $row['primary_address'] ; 
            
            $u_name = $row['title']." ".$row['fname']." ".$row['lname'] ;
            $company = $row['company'] ;
            
            $b_add1= stripslashes(($row['b_add1']=='')? $row["add1$pre"] : $row['b_add1']);
            $b_add2= stripslashes(($row['b_add2']=='')? $row["add2$pre"] : $row['b_add2']);
            $city= stripslashes(($row["b_city"]=='')? $row["city$pre"] : $row['b_city']);
            $state= stripslashes(($row['b_state']=='')? $row["state$pre"] : $row['b_state']);
            $country= stripslashes(($row['b_country']=='')? $row["country$pre"] : $row['b_country']);
            $zip= stripslashes(($row['b_zip']=='')? $row["zip$pre"] : $row['b_zip']);
            $b_name = $row['b_name'];            
            if(empty($b_name)){                
                $b_name = (!empty($company)) ? $company : $u_name;       
            }        
            // Code for old billing address BOF            
            $old_billing_address= '' ;
            echo "<br/> Address" ;
            echo "b_add1 <br/>".$b_add1  ;
            echo "b_add2 <br/>".$b_add2 ;
            echo "b_city <br/>".$city ;
            echo "b_state <br/>".$state  ;
            echo "country <br/>".$country  ;
            // Code for old billing address BOF
            $b_addr = $b_add1."<br>".$b_add2 ;
            
            if(!empty($b_add1)){
                 $old_billing_address.=$b_add1."<br>";
            }
            if(!empty($b_add2)){
               $old_billing_address .=$b_add2."<br>";            
            }
            if(!empty($city)){
               $old_billing_address .= $city." ".$zip."<br>";            
            }
            if(!empty($state)){
               $old_billing_address .=$state."<br>";            
            }
            if(!empty($country)){
               $old_billing_address .=$country."<br>";            
            }            
            $data['old_billing_address'] = $old_billing_address;
            // Code for old billing address EOF             
            
            $invoice_no = $row['invoice_no'];
            $order_no = $row['order_no'];      
            //check order for created invoice was exist or not 
            $sql ='SELECT * FROM '.TABLE_BILL_ORDERS." WHERE ".TABLE_BILL_ORDERS .".number = '".$order_no."'";
            $db->query($sql);
            if($db->nf() == 0 ){
            
                  $orderNotExistContent .= "<br/>  '$order_no' Order no of current invoice '$invoice_no' is not exist in new database. "."\n";
                  $orderNotFound[] = $order_no ;
                  continue;
            }
            
            
            $invoice_opening_balance = $row['invoice_opening_balance'];
            $invoice_particulars = $row['invoice_particulars'];
            $invoice_amount = $row['invoice_amount'];
            $invoice_amount_words = $row['invoice_amount_words'];
            $amount_domain = '';
            $amount_hosting_rs ='';
            $amount_hosting_sb ='';
            $amount_development='';
            $amount_products = '';
            $amount_designing ='';
            $amount_consult = '';
            $amount_advsmt = '';
            $amount_seo = '';
            $amount_maintain =''; 
            $amount_network = '';
            $amount_hardware = '';
            $amount_bpo = '';
            $amount_research =''; 
            
            
            $invoice_note = $row['invoice_note'];
            $invoice_balance = $row['invoice_balance'];
            $invoice_exp_date = $row['invoice_exp_date'];
            $invoice_date = $row['invoice_date'];
            //$notify = $row['notify'];
            $invoice_created_date = $row['invoice_created_date'];
            $invoice_exp_date = $row['invoice_exp_date'];
            
           
           
            $invoice_date  = date("Y-m-d",$invoice_date );
            $invoice_created_date =  date("Y-m-d",$invoice_created_date );
            if($invoice_exp_date > 1){
                $invoice_exp_date =  date("Y-m-d",$invoice_exp_date );
            }else{
                $invoice_exp_date='';
            }
            //calculate invoice due date by adding 7 days in invoice date
            //list($y,$m,$d) = split("-",$invoice_date);
            //$invoice_due_date = date("Y-m-d", mktime(0, 0, 0, $m  , $d +7, $y));
            $invoice_due_date ='';
            
            
            // default access level 1 for top
            $access_level = "65535";            
            //webmaster
            $created_by = "e68adc58f2062f58802e4cdcfec0af2d";           
            //anjani.singh
            $manager="4c87b6500c1bca5328824568aaa26174";
            //anjani.singh
            $teamStr ="e68adc58f2062f58802e4cdcfec0af2d";
            // by default  service_id is 10 for 'Custom Development'
            $service_id = "10";
            $status = "1";
            
            
            $inv_list  = NULL;          
           
            $query = "SELECT id FROM ". TABLE_BILL_INV
                        ." WHERE number = '". $invoice_no."'";

            if ( $db->query($query) && $db->nf() > 0 ) {
               //$error[] = 'Already exist';                 
               $invoiceExistContent .= "<br/> '$invoice_no' Invoice Already exist in database. "."\n";
               $invoiceExist[] = $invoice_no;
               continue;
            }
            
            //check order for created invoice was exist or not 
           
            
            $discount=$octroi=$service_tax=$vat=0;
           
            
            $data['number'] =$invoice_no;
            $data['or_no']=$order_no;
            $data['service_id'][] = $service_id ;
            $data['access_level']=$access_level;
            $data['created_by']  = $created_by;
            $data['client']['user_id'] = $uid;
            $data['currency']=$currency;
            $data['discount']=$discount;
            $data['octroi'] =$octroi;
            $data['service_tax']=$service_tax;
            $data['vat'] = $vat;
            $data['amount']= $invoice_amount;
            $data['amount_inr'] = $invoice_amount;
            $data['amount_words'] = $invoice_amount_words;
            $data['do_c'] = $invoice_created_date;
            $data['do_i'] = $invoice_date;
            //$data['do_d'] = date('2009-02-31');
            $data['do_d'] = $invoice_due_date ;
            $data['do_e'] = $invoice_exp_date;
            $data['remarks'] = $invoice_note;
            $data['balance'] = $invoice_balance;
            $data['billing_name'] = $b_name;
            
   
            //get calculate $b_add1 id from address BOF
                $b_add1sql = "SELECT id FROM ".TABLE_ADDRESS." WHERE address_of ='".$uid."' AND table_name='clients' ";
                $db1->query($b_add1sql) ;
                if ( $db1->nf() > 0 ) {
                    while ($db1->next_record()) {
                        $billing_address = $db1->f('id');
                    }
                }
            //get calculate $b_add1 id from address EOF 
            $data['billing_address'] = $billing_address;
            //get address from Address table BOF
            include_once ( DIR_FS_CLASS .'/Region.class.php');
            $region         = new Region();
            $region->setAddressOf(TABLE_CLIENTS, $uid);
            if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                  //$messages->setErrorMessage("The selected billing address was not found.");
                  //$error[] = "The selected billing address was not found ";                     
                  $billaddNotFoundContent .= "<br/> Billing address was not found for invoice. Invoice no is '$invoice_no' "."\n";
                  $billaddNotFoundInvoice[] = $invoice_no;
                  continue;
            }
            //get address from Address table EOF            
            
            
            $data['status'] = $status ;                          
            $data['number'] = $invoice_no ;
            $data['domain'] = $amount_domain ;
            $data['server1_rs'] = $amount_hosting_rs ;
            $data['server2_sb'] =  $amount_hosting_sb ;
            $data['develop'] = $amount_development ;
            $data['product']= $amount_products ;
            $data['design']= $amount_designing ;
            $data['bpo'] = $amount_bpo ;
            $data['seo'] =   $amount_seo ;
            $data['consult']=   $amount_consult ;
            $data['research']=  $amount_research ;
            $data['advert'] =  $amount_advsmt ;
            $data['webmaint']=  $amount_maintain ;
            $data['network'] = $amount_network ;
            $data['hardware'] =  $amount_hardware;             
            $data['particulars'][0] =  $invoice_particulars;             
            $data['p_amount'][0] =  $invoice_amount;             
             
            if ( empty($error) ){      
                  
                 $inv_pid =0;
                
                 
                 if($invoice_balance >0){
                    $status =Invoice::PENDING;
                 }else{
                    $status =Invoice::COMPLETED;                 
                 }
                
                 
                 echo  $query	= " INSERT INTO ".TABLE_BILL_INV
                                ." SET ". TABLE_BILL_INV .".number = '".    processUserData($invoice_no )."'"
                                .",". TABLE_BILL_INV .".or_no = '".         processUserData($order_no) ."'"
                                .",". TABLE_BILL_INV .".service_id = '".    processUserData($service_id )."'"
                                .",". TABLE_BILL_INV .".company_id = '1'"
                                .",". TABLE_BILL_INV .".access_level = '".  $access_level ."'"
                                .",". TABLE_BILL_INV .".created_by = '".    $created_by ."'"
                                .",". TABLE_BILL_INV .".client = '".        processUserData($uid)."'"
                                .",". TABLE_BILL_INV .".currency_old = '".  processUserData($currency_old) ."'"
                                 .",". TABLE_BILL_INV .".currency_abbr = '".processUserData($currency_abbr) ."'"
                                .",". TABLE_BILL_INV .".currency_id = '".   processUserData($currency_id) ."'"
                                .",". TABLE_BILL_INV .".currency_name = '". processUserData($currency_name) ."'"
                                .",". TABLE_BILL_INV .".currency_symbol = '".processUserData($currency_symbol) ."'"
                                .",". TABLE_BILL_INV .".currency_country = '".processUserData($currency_country) ."'"
                                .",". TABLE_BILL_INV .".discount = '".      processUserData($discount) ."'"
                                .",". TABLE_BILL_INV .".octroi = '".        processUserData($octroi) ."'"
                                .",". TABLE_BILL_INV .".service_tax = '".   processUserData($service_tax) ."'"
                                .",". TABLE_BILL_INV .".vat = '".           processUserData($vat )."'"
                                .",". TABLE_BILL_INV .".amount = '".        processUserData($invoice_amount)."'"
                                .",". TABLE_BILL_INV .".amount_inr = '".    processUserData($invoice_amount) ."'"
                                .",". TABLE_BILL_INV .".amount_words = '".  processUserData($invoice_amount_words) ."'"
                                .",". TABLE_BILL_INV .".do_c = '".          $invoice_created_date ."'"
                                .",". TABLE_BILL_INV .".do_i = '".          $invoice_date ."'"
                                .",". TABLE_BILL_INV .".do_d = '".          $invoice_due_date ."'"
                                .",". TABLE_BILL_INV .".do_e = '".          $invoice_exp_date."'"
                                .",". TABLE_BILL_INV .".remarks = '".       processUserData($invoice_note) ."'"
                                .",". TABLE_BILL_INV .".balance = '".       processUserData($invoice_balance )."'"
                                .",". TABLE_BILL_INV .".balance_inr = '".   processUserData($invoice_balance )."'"
                                .",". TABLE_BILL_INV .".billing_name = '".  processUserData($b_name) ."'"
                                .",". TABLE_BILL_INV .".billing_address='". processUserData($billing_address) ."'"
                                .",". TABLE_BILL_INV .".old_billing_address='". processUserData($old_billing_address) ."'"
                                .",". TABLE_BILL_INV .".b_addr_company_name = '". processUserData($b_name) ."'"
                                .",". TABLE_BILL_INV .".b_addr              = '".processUserData($b_addr) ."'"
                                .",". TABLE_BILL_INV .".b_addr_city         = '".processUserData($city) ."'"
                                .",". TABLE_BILL_INV .".b_addr_state        = '".processUserData($state) ."'"
                                .",". TABLE_BILL_INV .".b_addr_country      = '".processUserData($country) ."'"
                                .",". TABLE_BILL_INV .".b_addr_zip          = '".processUserData($zip) ."'"                                
                                .",". TABLE_BILL_INV .".is_old      ='1'"
                                .",". TABLE_BILL_INV .".status = '".$status ."'" ;
                $db->query($query)   ;
                $invid = $db->last_inserted_id();
                echo "<br/>";
                // Insert the Bifurcations.
                $inv_bid =0;
                 
                echo "<br/>"; 
                //get $or_id of corresponding invoice
                echo $sqlget= "SELECT new_oid FROM .".TABLE_MIGRATING_ORDER." WHERE o_no = '".$order_no."'";                
                if ( $db1->query($sqlget) ) {
                    if ( $db1->nf() > 0 ) {
                        while ($db1->next_record()) {
                            $or_id = $db1->f('new_oid');
                        }
                    }
                }
                //get clients details from client table EOF 
                echo $query2 = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $uid ."'";
                $db1->query($query2);
                $db1->next_record();
                $data['client'] = processSQLData($db1->result());
                //get clients details from client table BOF 
                echo "<br/>"; 
                
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS, $uid);
                $data['b_address']  = $region->get($data['billing_address']);
                //creater details BOF
                echo $query3 = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $created_by ."'"
                            ." OR username = '". $created_by ."'"
                            ." OR number = '". $created_by ."'" ;
                $db1->query($query3);
                $db1->next_record();
                $data['creator'] = processSQLData($db1->result());
                
                //creater details EOF
                echo "<br/>"; 
                
                // Update the Order.
                /*
                if ( !Invoice::setOrderStatus($db, $or_id, Invoice::COMPLETED) ) {
                    $messages->setErrorMessage("The Order was not Marked as Completed.");
                } */
               
               
                $queOrdStatus = "UPDATE ". TABLE_BILL_ORDERS
                        ." SET status = '".Invoice::COMPLETED."'"
                        ." WHERE (  number = '".$order_no."' )";
                $db1->query($queOrdStatus) ;
               
                 // Create the Invoice PDF, HTML in file.
                
                
                //migration info               
                echo $sql2 = "INSERT INTO ".TABLE_MIGRATING_INVOICE
                       ." SET ". TABLE_MIGRATING_INVOICE .".old_inv_id  = '". $oldinvoice_id ."'"
                       .",". TABLE_MIGRATING_INVOICE .".new_inv_id      = '". $invid ."'"
                       .",". TABLE_MIGRATING_INVOICE .".new_inv_b_id    = '". $inv_bid ."'" 
                       .",". TABLE_MIGRATING_INVOICE .".new_inv_p_id    = '". $inv_pid  ."'" 
                       .",". TABLE_MIGRATING_INVOICE .".order_no        = '". $order_no  ."'" ;
                echo "<br/>";
                $db->query($sql2); 
                $listInvDone[]= $invoice_no  ;
                $invoiceEnterdContent.=" Invoice enterd now ( Current script )"." Invoice no ".$invoice_no."\n";
                
             
            }else{
                $listInvid[]= $invoice_no  ;                            
            }
            
        }
    }
    DB::closeConnection($con);
    // already entered invoice list 
    if(!empty( $listInvid)){ 
        echo " Following invoices are completed previously";
        print_r($listInvid);
        echo "<br>";
    }
    if($listInvDone){
        echo " Following invoices are created Now";
        echo "<br>";
        print_r($listInvDone);
    }
    //write the errors in file BOF 
    $file='migrate-invoice.txt';
    $fp = fopen($file, 'a+');
    $content = " ----------- Script date ".date('Y-m-d')." ----------- \n" ;
	$content .= "------------ Order not exist Content ------------"."\n" ;
	$content .= $orderNotExistContent ;
	$content .= "------------ invoice Exist in New Database ------------"."\n" ;
    $content .= $invoiceExistContent ;
    $content .= "------------ Bill address not found for invoice  ------------"."\n" ;
    $content .= $billaddNotFoundContent ;
    $content .= "------------ Script entered invoice (Currently entered invoice) ------------"."\n" ;
    echo $content .= $invoiceEnterdContent;
    
    if (fwrite($fp, $content) === FALSE) {
        echo "Cannot write to file ($file)";
        exit;
    }
     fclose($fp);*/
    //write the errors in file EOF 
?>