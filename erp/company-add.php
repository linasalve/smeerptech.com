<?php
  if ( $perm->has('nc_cmp_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the payment party class
		include_once (DIR_FS_INCLUDES .'/company.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
            if ( Company::validateAdd($data, $extra) ) {
                            
                    /*$query = "SELECT * FROM ".TABLE_SETTINGS_COMPANY." WHERE prefix='". $data['prefix'] ."'";
                    if ( $db->query($query) && $db->affected_rows() > 0 ) { 
                        $messages->setErrorMessage("This prefix is already exist.Please specify another.");
                    }*/
                    $do_implement ='';
					if($data['do_implement']){                    
						$do_implement  = date('Y-m-d H:i:s', $data['do_implement']) ;
						 
					}
					$do_implement_st ='';
					if($data['do_implement_st']){                    
						$do_implement_st  = date('Y-m-d H:i:s', $data['do_implement_st']) ; 
					}
					$do_iso ='';
					if($data['do_iso']){                    
						$do_iso  = date('Y-m-d', $data['do_iso']) ; 
					}
                    $query	= " INSERT INTO ".TABLE_SETTINGS_COMPANY
						." SET ".TABLE_SETTINGS_COMPANY .".name = '". 				$data['name'] ."'"  
                            .",". TABLE_SETTINGS_COMPANY .".prefix = '". 			$data['prefix'] ."'"                            
							.",". TABLE_SETTINGS_COMPANY .".quot_prefix = '". 		$data['quot_prefix'] ."'"   
                            .",". TABLE_SETTINGS_COMPANY .".status = '". 			$data['status'] ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".tin_no = '". 			$data['tin_no'] ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".service_tax_regn = '". 	$data['service_tax_regn'] ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".pan_no = '". 			$data['pan_no'] ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".cst_no = '". 			$data['cst_no'] ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".vat_no = '". 			$data['vat_no'] ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".do_implement = '". 		$do_implement ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".do_implement_st = '". 	$do_implement_st ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".do_iso 				= '".$do_iso ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".created_by = '". 		$my['uid'] ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".ip = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                            .",". TABLE_SETTINGS_COMPANY .".date = '". 		date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New company entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/company.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/company.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/company.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'company-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
