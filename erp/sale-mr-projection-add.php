<?php
    if ( $perm->has('nc_sl_mr_pr_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        $yearStart =date('Y');
        $yearEnd  =  $yearStart + 1 ;
        for($j=$yearStart; $j<=$yearEnd  ;$j++){
            $lst_year[$j] = $j;
        }  
		// Include the hardware sale-mr-projection class.
		include_once (DIR_FS_INCLUDES .'/sale-mr-projection.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( SaleMrProjection::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_SALE_MR_PROJECTION
                            ." SET ". TABLE_SALE_MR_PROJECTION .".month         = '".  $data['month'] ."'"
                                .",". TABLE_SALE_MR_PROJECTION .".year          = '".  $data['year'] ."'"
                                .",". TABLE_SALE_MR_PROJECTION .".user_id       = '".  $my['user_id'] ."'"
                                .",". TABLE_SALE_MR_PROJECTION .".amount        = '".  $data['amount'] ."'"
                                .",". TABLE_SALE_MR_PROJECTION .".access_level  = '".  $my['access_level'] ."'"
                                .",". TABLE_SALE_MR_PROJECTION .".created_by    = '".  $my['user_id'] ."'"
                                .",". TABLE_SALE_MR_PROJECTION .".ip            = '".  $_SERVER['REMOTE_ADDR'] ."'"                                
                                .",". TABLE_SALE_MR_PROJECTION .".do_e          = '".  date('Y-m-d H:i:s')."'";
                              
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Sale Marketing Projections entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sale-mr-projection.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sale-mr-projection.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sale-mr-projection.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');     
            $page["var"][] = array('variable' => 'lst_year', 'value' => 'lst_year');    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-mr-projection-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
