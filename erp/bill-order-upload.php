<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/bill-order-upload.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
    $condition_url = $condition_query = '';
    
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
    $condition_query ='';
    $condition_url ='';
   
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    $variables['type'] = BillOrderUpload::getType();
    $variables['status'] = BillOrderUpload::getStatus();
    
    if($added){
         $messages->setOkMessage("Path Upload entry has been done.");
    }
    
    if ( $perm->has('nc_e') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
                                TABLE_BILL_ORD_UPLOAD   =>  array(  'Path'               => 'upload_path',
                                                         	  'details'       => 'details'
                                                        ),
                                TABLE_BILL_ORDERS   =>  array(  'Order Title'  => 'order_title-'.TABLE_BILL_ORDERS
                                                        )
                                );
        
        $sOrderByArray  = array(
                                TABLE_BILL_ORD_UPLOAD => array(     'Date'              => 'do_e',
                                                              'Path'               => 'upload_path',
                                                         	  'details'       => 'details'
                                                         ),
                                TABLE_BILL_ORDERS   =>  array(  'Order Title'               => 'order_title'
                                                        )
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_BILL_ORD_UPLOAD;
        }
        $where_added =false ;
        
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {
            
                include (DIR_FS_NC.'/bill-order-upload-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-upload.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('edit'): {
                include (DIR_FS_NC .'/bill-order-upload-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-upload.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('acomm'): {
                include (DIR_FS_NC .'/bill-order-upload-add-comment.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
            case ('search'): {
                include(DIR_FS_NC."/bill-order-upload-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-upload.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('delete'): {
                $perform='list';
                include ( DIR_FS_NC .'/bill-order-upload-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-upload.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
             case ('mark_complete'):{
                $perform='list';
            	include ( DIR_FS_NC .'/bill-order-upload-mark-complete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-upload.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
			}
            /*case ('deactive'): {
                include ( DIR_FS_NC .'/bill-order-upload-deactive.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-upload.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            
            case ('list'):
            default: {
                include (DIR_FS_NC .'/bill-order-upload-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-upload.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-order-upload.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
