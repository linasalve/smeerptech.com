<?php

	$condition_query= '';
    $condition_url  = '';
    
    if ( $sString != "" ) {
	
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
		if ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
			$condition_url .= "&sString=". $sString;
			$condition_url .= "&sType=". $sType;
			$where_added = true;
		}
		elseif ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
			foreach( $sTypeArray as $table => $field_arr ) {
				if ( $table != "Any" ) {
					foreach( $field_arr as $key => $field ) {
                        $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
						$where_added = true;
					}
				}
			}
			$condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
			$condition_query .= ") ";
		}
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}
    
   // BO: Status data.
    $sStatusStr='';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( $chk_status == 'AND' || $chk_status == 'OR') {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             
        }else{
            $sStatusStr = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_SALE_LEADS .".lead_status IN ('". $sStatusStr ."') ";
        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;        
    }
    // EO: Status data.
    
	//echo $condition_query;
	$condition_url = "&rpp=$rpp"
                        ."&sString=$sString"
                        ."&sType=$sType"
                        ."&sOrderBy=$sOrderBy"
                        ."&sOrder=$sOrder"
                        ."&action=search";
			
	$_SEARCH["sString"] 	= $sString ;
	$_SEARCH["sType"] 		= $sType ;
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/sale-lead-assign-mr-list.php');
?>