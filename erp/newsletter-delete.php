<?php
    if ( $perm->has('nc_nwl_delete') ) {
	    $n_id = isset($_GET["n_id"]) ? $_GET["n_id"] : ( isset($_POST["n_id"]) ? $_POST["n_id"] : '' );
        
        
                
        $extra = array( 'db'           => &$db,
                        'messages'     => &$messages
                        );
        Newsletter::delete($n_id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/newsletter-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this module.");
         //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
