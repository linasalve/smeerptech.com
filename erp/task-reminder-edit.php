<?php
    if ( $perm->has('nc_ts_edit')  || $perm->has('nc_ts_edit_all') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $curz     = isset($_GET["curz"]) ? $_GET["curz"] : ( isset($_POST["curz"]) ? $_POST["curz"] : '' );
        $curno     = isset($_GET["curno"]) ? $_GET["curno"] : ( isset($_POST["curno"]) ? $_POST["curno"] : '' );
        $curdate    = isset($_GET["curdate"]) ? $_GET["curdate"] : ( isset($_POST["curdate"]) ? $_POST["curdate"] : '' );
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_ts_edit_al') ) {
            $access_level += 1;
        }
		$groups_list  = User::getGroups();
        // BO: Read the Team members list ie member list in dropdown box BOF
       /*  $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields); */
        // BO:  Read the Team members list ie member list in dropdown box  EOF 
        
        // BO: Read the available job list BOF
	    $lst_job = NULL;
        $required_fields ='*';
        $condition_query = "WHERE status='".Jobs::PENDING."'";
		Jobs::getList($db,$lst_job,$required_fields,$condition_query);
	    // EO: Read the job list  list in dropdown box EOF
        
        $lst_type=array_flip(Taskreminder::getTypes());
        
	    $allotted_to_str1=$allotted_to_client_str1=$allotted_to_lead_str1=$allotted_to_addr_str1='';
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
           
            
            /*
            if(!empty($data['allotted_to'])) {
                $allotted_to_str1 = implode(",",$data['allotted_to']);
                //$allotted_to_str1.=",".$data['created_by'];
                $allotted_to_str = ",".$allotted_to_str1.",";
            }else{
                //$allotted_to_str1 = $data['created_by'];
                //$allotted_to_str =",".$data['created_by'].",";
            }
            if(!empty($data['allotted_to_client'])) {
                $allotted_to_client_str1=implode(",",$data['allotted_to_client']);
                $allotted_to_client_str=",".$allotted_to_client_str1.",";
            }
            if(!empty($data['allotted_to_lead'])) {
                $allotted_to_lead_str1=implode(",",$data['allotted_to_lead']);  
                $allotted_to_lead_str=",".$allotted_to_lead_str1.",";                
            }
            if(!empty($data['allotted_to_addr'])){
                $allotted_to_addr_str1=implode(",",$data['allotted_to_addr']);      
                $allotted_to_addr_str=",".$allotted_to_addr_str1.",";                
            }
            */
            if ( !empty($data['job_id']) ) {
                Jobs::getList($db, $_ALL_POST['job_details'], 'title,description', "WHERE id ='". $data['job_id'] ."'");   
                //$_ALL_POST['job_details'] = $_ALL_POST['job_details']['0'];
                $data['job_title'] = $_ALL_POST['job_details']['0']['title'];
            }
            
            if ( Taskreminder::validateUpdate($data, $extra) ) {
             
                if(!empty($data['allotted_to'])) {
                    $allotted_to_str1 = implode(",",$data['allotted_to']);
                    //$allotted_to_str1.=",".$data['created_by'];
                    $allotted_to_str = ",".$allotted_to_str1.",";   
                    
                    $sql= " SELECT f_name,l_name FROM ".TABLE_USER." WHERE user_id IN('".str_replace(",","','",$allotted_to_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_name1 = trim($allotted_to_name,",") ;
                        $allotted_to_name = ",".$allotted_to_name1."," ;
                    }
                }else{
                    //$allotted_to_str1 = $data['created_by'];
                    //$allotted_to_str =",".$data['created_by'].",";
					$group_like_q = '';
					if(!empty($data['groups'])){
					
						$groupsStr='';
						$groups1=implode(",",$data['groups']);
						$groupsStr = ",".$groups1.",";
										
						
						foreach($data['groups'] as $key=>$val){
						
							$group_like_q .= TABLE_USER.".groups LIKE '%,".trim($val).",%' OR " ;
						}
						
						$group_like_q = substr( $group_like_q, 0, strlen( $group_like_q ) - 3 );
                        
						$sql= " SELECT f_name,l_name,user_id FROM ".TABLE_USER." WHERE ".$group_like_q." ";
						$db->query($sql);
						if ( $db->nf()>0 ) {
							while ( $db->next_record() ) { 
								$allotted_to_name .= $db->f('f_name')." ".$db->f('l_name').",";
								$allotted_to_str1 .= $db->f('user_id')."," ;
								
								$userDetails[] = array(
									
								
								);
							}
							$allotted_to_name1 = trim($allotted_to_name,",") ;
							$allotted_to_name = ",".$allotted_to_name1."," ;
							$allotted_to_str1 = trim($allotted_to_str1,",") ;
							$allotted_to_str = ",".$allotted_to_str1.",";
						}
					}
                }
                if(!empty($data['allotted_to_client'])) {
                    $allotted_to_client_str1=implode(",",$data['allotted_to_client']);
                    $allotted_to_client_str=",".$allotted_to_client_str1.",";
                    $sql= " SELECT f_name,l_name FROM ".TABLE_CLIENTS." WHERE user_id IN('".str_replace(",","','",$allotted_to_client_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_client_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_client_name1 = trim($allotted_to_client_name,",") ;
                        $allotted_to_client_name = ",".$allotted_to_client_name1."," ;
                    }
                    
                }
                if(!empty($data['allotted_to_vendor'])) {
                    $allotted_to_vendor_str1=implode(",",$data['allotted_to_vendor']);
                    $allotted_to_vendor_str=",".$allotted_to_vendor_str1.",";
                    $sql= " SELECT f_name,l_name FROM ".TABLE_VENDORS." WHERE user_id IN('".str_replace(",","','",$allotted_to_vendor_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_vendor_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_vendor_name1 = trim($allotted_to_vendor_name,",") ;
                         $allotted_to_vendor_name = ",".$allotted_to_vendor_name1."," ;
                    }
                    
                }
                if(!empty($data['allotted_to_lead'])) {
                    $allotted_to_lead_str1=implode(",",$data['allotted_to_lead']);  
                    $allotted_to_lead_str=",".$allotted_to_lead_str1.",";   
                    $sql= " SELECT f_name,l_name FROM ".TABLE_SALE_LEADS." WHERE lead_id IN('".str_replace(",","','",$allotted_to_lead_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_lead_name .= $db->f('f_name')." ".$db->f('l_name').",";
                        }
                        $allotted_to_lead_name1 = trim($allotted_to_lead_name,",") ;
                        $allotted_to_lead_name = ",".$allotted_to_lead_name1."," ;
                    }
                }
                
                if(!empty($data['allotted_to_addr'])){
                    $allotted_to_addr_str1=implode(",",$data['allotted_to_addr']);      
                    $allotted_to_addr_str=",".$allotted_to_addr_str1.",";     
                    $sql= " SELECT name FROM ".TABLE_ADDRESS_BOOK." WHERE lead_id IN('".str_replace(",","','",$allotted_to_addr_str1)."')";
                    $db->query($sql);
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) { 
                            $allotted_to_addr_name .= $db->f('name').",";
                        }
                        $allotted_to_addr_name1 = trim($allotted_to_addr_name,",") ;
                        $allotted_to_addr_name = ",".$allotted_to_addr_name1."," ;
                    }
    
                }            
            
                // BO: Read the Team members list ie member list in dropdown box BOF
                $reminder = Null;
                $condition_query=" WHERE id= '".$id."'";
                Taskreminder::getList($db,$reminder,'*',$condition_query);
                $reminder=processUserData($reminder[0]);
                
                // BO:  Read the Team members list ie member list in dropdown box  EOF 
                $sqlCompleted ='';
                if($data['status']==Taskreminder::COMPLETED){
                    $sqlCompleted =  ",". TABLE_TASK_REMINDER .".do_completion = '".date('Y-m-d',time())."', ".TABLE_TASK_REMINDER.".completed_by='".$my['user_id']."',
                    ".TABLE_TASK_REMINDER.".completed_by_name='".$my['f_name']." ".$my['l_name']."'" ;
                }
                $is_imp=0;
                if(isset($data['is_imp'])){
					$is_imp=1;
				}  
                $query  = " UPDATE ". TABLE_TASK_REMINDER
                         ." SET ". TABLE_TASK_REMINDER .".task = '".                 $data['task'] ."'"
                                .",". TABLE_TASK_REMINDER .".comment = '".              $data['comment'] ."'"
								.",". TABLE_TASK_REMINDER .".is_imp = '".               $is_imp ."'"   
                                .",". TABLE_TASK_REMINDER .".do_r = '".                 $data['do_r'] ."'"
                                .",". TABLE_TASK_REMINDER .".do_deadline = '".          $data['do_deadline'] ."'"
                                .",". TABLE_TASK_REMINDER .".type = '".               $data['type'] ."'"
                                .",". TABLE_TASK_REMINDER .".status = '".               $data['status'] ."'"
                                .$sqlCompleted
                                .",". TABLE_TASK_REMINDER .".priority = '".             $data['priority'] ."'"
								.",". TABLE_TASK_REMINDER .".send_sms = '".             $data['send_sms'] ."'"
							    .",". TABLE_TASK_REMINDER .".groups = '".          $groupsStr ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to = '".          $allotted_to_str ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_name = '".          $allotted_to_name ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_client = '".   $allotted_to_client_str ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_client_name = '".   $allotted_to_client_name ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_lead = '".     $allotted_to_lead_str ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_lead_name = '".     $allotted_to_lead_name ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_addr = '".     $allotted_to_addr_str ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_addr_name = '".     $allotted_to_addr_name ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_vendor = '".     $allotted_to_vendor_str ."'"
                                .",". TABLE_TASK_REMINDER .".allotted_to_vendor_name = '".     $allotted_to_vendor_name ."'"
                                .",". TABLE_TASK_REMINDER .".on_behalf_id = '".     $data['on_behalf_id'] ."'"
                                .",". TABLE_TASK_REMINDER .".on_behalf_id_name = '".     $data['on_behalf_details'] ."'"
                                .",". TABLE_TASK_REMINDER .".order_id = '".             $data['order_id'] ."'"
                                .",". TABLE_TASK_REMINDER .".job_id = '".               $data['job_id'] ."'"
                                .",". TABLE_TASK_REMINDER .".hrs = '".                  $data['hrs'] ."'"
                                .",". TABLE_TASK_REMINDER .".min = '".                  $data['min'] ."'"
                                .",". TABLE_TASK_REMINDER .".time_type = '".            $data['time_type'] ."'"
                            ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Task entry has been updated successfully.");
                    $variables['hid'] = $id;
                    
                    $history_query	= " INSERT INTO ".TABLE_TASK_REMINDER_HISTORY
                            ." SET ". TABLE_TASK_REMINDER_HISTORY .".task_id = '".              $reminder['id'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".task_no = '".              $reminder['task_no'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".task = '".                 $reminder['task'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".comment = '".              $reminder['comment'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".access_level = '".         $reminder['access_level'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".created_by = '".           $reminder['created_by'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".created_by_name = '".      $reminder['created_by_name'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".do_r = '".                 $reminder['do_r'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".do_deadline = '".          $reminder['do_deadline'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".do_e = '".                 $reminder['do_e'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".type = '".                 $reminder['type'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".status = '".               $reminder['status'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".priority = '".             $reminder['priority'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".groups = '".          		$reminder['groups'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".send_sms = '".          	$reminder['send_sms'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".sms_sent_mobile = '".      $reminder['sms_sent_mobile'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to = '".          $reminder['allotted_to'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_name = '".     $reminder['allotted_to_name'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_client = '".   $reminder['allotted_to_client'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_client_name='".$reminder['allotted_to_client_name'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_vendor = '".   $reminder['allotted_to_vendor'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_vendor_name = '".$reminder['allotted_to_vendor_name'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_lead = '".     $reminder['allotted_to_lead'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_lead_name = '".$reminder['allotted_to_lead_name'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_addr = '".     $reminder['allotted_to_addr'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".allotted_to_addr_name = '".$reminder['allotted_to_addr_name'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".on_behalf_id = '".         $reminder['on_behalf_id'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".on_behalf_id_name = '".    $reminder['on_behalf_id_name'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".mail_send_to = '".         $reminder['mail_send_to'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".mail_send_to_client = '".  $reminder['mail_send_to_client'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".mail_send_to_vendor = '".  $reminder['mail_send_to_vendor'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".order_id = '".             $reminder['order_id'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".job_id = '".               $reminder['job_id'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".hrs = '".                  $reminder['hrs'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".min = '".                  $reminder['min'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".time_type = '".            $reminder['time_type'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".do_u = '".                 date('Y-m-d') ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".updated_by = '".           $my['user_id'] ."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".updated_by_name = '".      $my['f_name']." ".$my['l_name']."'"
				.",". TABLE_TASK_REMINDER_HISTORY .".ip = '".                   $_SERVER['REMOTE_ADDR'] ."'";
                                
                    $db_new=new db_local();
                    $db_new->query($history_query);
                    
                    $statusArr = Taskreminder::getStatus();
                    $statusArr = array_flip($statusArr);
                    $priorityArr = Taskreminder::getPriority();
                    $priorityArr = array_flip($priorityArr);
                    $data['task_title']       =   $data['task'];
                    $data['task']       =   $_ALL_POST['comment'];
                    $data['task_no']       =   $reminder['task_no'];
                    //$data['comment']    =   $data['comment'] ;
                    $data['do_r']       =   $data['do_r'];
                    //$data['do_deadline']=   $data['do_deadline'];
                    $data['af_name']    =   $my['f_name'] ;
                    $data['al_name']    =   $my['l_name'] ;
                    $data['status']     =   $data['status'] ;
                    $data['priority']   =   $data['priority'] ;
                    $data['status']     =   $statusArr[$data['status']] ;
                    $data['priority']     =   $priorityArr[$data['priority']] ;                    
                    // Send Email to the users involved BOF  
                    
                    $string='';
                    if(!empty( $allotted_to_str1)){                          
                        $allotted_to_str1 = trim($allotted_to_str1,",");
                        $string = str_replace(",","','", $allotted_to_str1);
                    }
                    $data['team_members'] = '';
                    User::getList($db, $userDetails, 'number, f_name, l_name, email,mobile1,mobile2', " WHERE user_id IN( '".$string."' )");
					$data['mail_exec']=0;
                    if(!empty($userDetails) && isset($data['mail_exec']) && $data['mail_exec']==1){
                        foreach ( $userDetails as $user ) {
                            $data['uf_name']    =   $user['f_name'];
                            $data['ul_name']    =   $user['l_name'] ;
                            $email = NULL;
                            $team_members .=$user['f_name']." ".$user['l_name']."<br/>" ;
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_UPDATE', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                                
                                $send_to .=$user['email'].",";
                             /*    print_r($to);
                                print_r($from);
                                echo $email["body"];
                                echo $email["subject"]; 
                                 */
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                        $send_to = trim($send_to,",");
                        
                    }else{
                        foreach ( $userDetails as $user ) {
                             $team_members .=$user['f_name']." ".$user['l_name']."<br/>" ;
                        } 
						
						if(in_array($data['allotted_to'],'e68adc58f2062f58802e4cdcfec0af2d')){
							//Aryan Sir alloted in task then send mail
							$data['uf_name']    =   'Aryan';
                            $data['ul_name']    =  'Salve' ;
							$user['email'] 		= 'aryan@smeerptech.com';
                            $email = NULL;                            
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_ADD', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $user['uf_name'] .' '. $user['ul_name'], 'email' => $user['email']);                          	 
                                $send_to .=$user['email'].",";
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
								
							}
						}
						
                    }
					$data['sms_sent_mobile']='';
					$counter=0;
					if($data['send_sms']==1){
						foreach ( $userDetails as $user ) {
							if(!empty($user['mobile1'])){
								
								$data['sms_sent_mobile'] .= $user['mobile1'].",";
								$data['text'] = substr($data['task'], 0, 130);
								
								$mobile1 = "91".$user['mobile1'] ;
								ProjectTask::sendSms($data['text'],$mobile1);
								$counter = $counter+1;
							}
							if(!empty($user['mobile2'])){
								$mobile2 = "91".$user['mobile2'];
								$data['text'] = substr($data['task'], 0, 130);
								
								ProjectTask::sendSms($data['text'],$mobile2);
								$counter = $counter+1;
								$data['sms_sent_mobile'] .= $user['mobile2'].",";
							}
                            if(!empty($data['sms_sent_mobile'])){
								
								$query_update = "UPDATE ". TABLE_TASK_REMINDER 
                                ." SET ". TABLE_TASK_REMINDER .".sms_sent_mobile 
									= '".trim($data['sms_sent_mobile'],",").  "' "
                                    ." WHERE ". TABLE_TASK_REMINDER .".id = '". $variables['hid'] ."' " ;
								$db->query($query_update) ;
							
							}
							//update sms counter
							if($counter >0){
								$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter = sms_counter +".$counter ;
								$db->query($sql); 
							}
							 
                        }						
						 
					}
                    $data['team_members'] = $team_members ;
                    // Send Email to the users involved EOF
                    
                    // Send Email to the clients involved BOF                                   
                    if(!empty($allotted_to_client_str1) && isset($data['mail_client'])){
                        //include ( DIR_FS_INCLUDES .'/clients.inc.php');
                        $allotted_to_str1 = trim($allotted_to_client_str1,",");
                        $string = str_replace(",","','", $allotted_to_str1);
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id IN( '".$string."' )");
                        foreach ( $clientDetails as $client ) { 
                            $data['uf_name']    =   $client['f_name'];
                            $data['ul_name']    =   $client['l_name'] ;
                            $email = NULL;
                            
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_UPDATE', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $client['f_name'] .' '. $client['l_name'], 'email' => $client['email']);
                                //echo $email["body"];
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                $send_to_client .=$client['email'].",";
                                /*print_r($to);
                                print_r($from);
                                echo $email["body"];
                                echo $email["subject"];*/
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                        $send_to_client = trim($send_to_client,",");
                    }else{
                        $data['mail_client']=0;
                    }
                    // Send Email to the clients involved EOF
                    // Send Email to the vendors involved BOF                                   
                    if(!empty($allotted_to_vendor_str1) && isset($data['mail_vendor'])){
                        
                        //include ( DIR_FS_INCLUDES .'/clients.inc.php');                         
                        $allotted_to_str1 = trim($allotted_to_vendor_str1,",");
                        $string = str_replace(",","','", $allotted_to_str1);
                        Vendors::getList($db, $vendorDetails, 'number, f_name, l_name, email', " WHERE user_id IN( '".$string."' )");
                      
                        foreach ( $vendorDetails as $vendor ) { 
                            $data['uf_name']    =   $vendor['f_name'];
                            $data['ul_name']    =   $vendor['l_name'] ;
                            $email = NULL;
                            
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_UPDATE', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $vendor['f_name'] .' '. $vendor['l_name'], 'email' => $vendor['email']);
                                $send_to_vendor .=$vendor['email'].",";
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                /* print_r($to);
                                print_r($from);
                                echo $email["body"];
                                echo $email["subject"]; */
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                        $send_to_vendor = trim($send_to_vendor,",");
                    }else{
                        $data['mail_vendor']=0;
                    }                    
                    // Send Email to the vendors involved EOF
                    
                    // Send Email to the leads involved BOF  
                    if(!empty($allotted_to_lead_str1) && isset($data['mail_lead'])){
                       // include ( DIR_FS_INCLUDES .'/sale-lead.inc.php');                        
                        $allotted_to_str1 = trim($allotted_to_lead_str1,",");
                        $string = str_replace(",","','", $allotted_to_str1);
                        
                        Leads::getList($db, $leadDetails, 'lead_id, f_name, l_name, email', " WHERE lead_id IN( '".$string."' )");
                        foreach ( $leadDetails as $lead ) {
                            $data['uf_name']    =   $lead['f_name'];
                            $data['ul_name']    =   $lead['l_name'] ;
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_UPDATE', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $lead['f_name'] .' '. $lead['l_name'], 'email' => $lead['email']);
                                
                                //echo $email["body"];
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                /*print_r($to);
                                print_r($from);
                                echo $email["body"];
                                echo $email["subject"];*/
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }else{
                        $data['mail_lead']=0;
                    }
                    // Send Email to the leads involved EOF
                    
                    // Send Email to the external users involved BOF  
                    if(!empty($allotted_to_addr_str1) && isset($data['mail_external'])){
                        //include ( DIR_FS_INCLUDES .'/address-book.inc.php');
                        $allotted_to_str1 = trim($allotted_to_addr_str1,",");
                        $string = str_replace(",","','", $allotted_to_str1);
                        
                        Addressbook::getList($db, $addrDetails, 'name, email', " WHERE id IN( '".$string."' )");
                        foreach ( $addrDetails as $addr ) {
                            $data['uf_name']    =   $addr['name'];
                            $data['ul_name']    =   '' ;
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_UPDATE', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $addr['name'] , 'email' => $addr['email']);                                
                                //echo $email["body"];
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                /*print_r($to);
                                print_r($from);
                                echo $email["body"];
                                echo $email["subject"];*/
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }else{
                        $data['mail_external']=0;
                    }
                    // Send Email to the external users involved EOF
                    
                    // Send Email to the On Behalf users involved BOF  
                    if(!empty($data['on_behalf_id']) && isset($data['mail_on_behalf'])){
                        //include ( DIR_FS_INCLUDES .'/address-book.inc.php');
                        User::getList($db, $userOnBehalfDetails, 'user_id,f_name,l_name,number email', " WHERE user_id ='".$data['on_behalf_id']."'");
                        $userOnBehalfDetails = $userOnBehalfDetails['0'];
                        
                        $data['uf_name']    =   $userOnBehalfDetails['f_name'];
                        $data['ul_name']    =   $userOnBehalfDetails['l_name'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'TASK_REMINDER_OTHER_UPDATE', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $userOnBehalfDetails['f_name'].' '.$userOnBehalfDetails['l_name'] , 'email' => $userOnBehalfDetails['email']);
                            $send_to .=",".$userOnBehalfDetails['email'].",";
                            /*echo $email["body"];
                            echo $email["subject"];
                            print_r($to);
                            print_r($from);*/
                            
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }else{
                        $data['mail_on_behalf']=0;
                    }
                    // Send Email to the On Behalf users involved EOF
                    
                     //Set the list email-id to whom the mails are sent bof
                    $query_update = "UPDATE ". TABLE_TASK_REMINDER 
                                ." SET ". TABLE_TASK_REMINDER .".mail_send_to = '".trim($send_to,",")."', mail_send_to_client='".trim($send_to_client,",")."',
                                    mail_send_to_vendor ='".trim($send_to_vendor,",")."' "                                    
                                ." WHERE ". TABLE_TASK_REMINDER .".id = '". $id  ."' " ;
                    $db->query($query_update) ;
                    //Set the list subuser's email-id to whom the mails are sent eof 
                  
                    // Send Email to the admin BOF  
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'TASK_REMINDER_UPDATE_ADMIN', $data, $email) ) {
                        $to = '';
                        $to[]   = array('name' => $taskadmin_name , 'email' => $taskadmin_email);                           
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        /*print_r($to);
                        print_r($from);
                        echo $email["body"];
                        echo $email["subject"];
                        exit;*/
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                        $cc_to_sender,$cc,$bcc,$file_name);
                    }
                    // Send Email to the admin EOF  
                }
                
                $data       = NULL;
            }
           
            
        }
        else {
        
            // Read the record which is to be Updated.
            $fields = TABLE_TASK_REMINDER .'.*'  ;            
            $condition_query = " WHERE (". TABLE_TASK_REMINDER .".id = '". $id ."' )";
            
            /*
            $condition_query .= " AND ( ";
            // If my has created this Order.
            $condition_query .= " (". TABLE_TASK_REMINDER .".created_by = '". $my['user_id'] ."' "
                                    ." OR ". TABLE_TASK_REMINDER .".access_level < $access_level ) ";              
            // Check if the User has the Right to Edit Orders created by other Users.
           
            if ( $perm->has('nc_ts_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_ts_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_TASK_REMINDER. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_TASK_REMINDER .".access_level < $access_level_o ) ";
            }
            $condition_query .= " )";
            */
            if ( Taskreminder::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
    
                //if ( $_ALL_POST['access_level'] < $access_level ) {
                
                    $id         = $_ALL_POST['id'];
                    
                    // BO: Read the Team Members Information.
					//if ( !empty($_ALL_POST['allotted_to']) && !is_array($_ALL_POST['allotted_to']) ) {
					if ( !empty($_ALL_POST['allotted_to']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						$_ALL_POST['allotted_to']          = explode(',', $_ALL_POST['allotted_to']);
						$temp                       = "'". implode("','", $_ALL_POST['allotted_to']) ."'";
						$_ALL_POST['allotted_to_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['allotted_to_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['allotted_to'] = array();
						foreach ( $_ALL_POST['allotted_to_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['allotted_to'][] = $members['user_id'];
							$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}
					if(!empty($_ALL_POST['groups'])){
						$_ALL_POST['groups'] = explode(',', $_ALL_POST['groups']);
						$_ALL_POST['allotted_to']=array();
						$_ALL_POST['team_details']=array();
					}
					// EO: Read the Team Members Information.
                    
                    // BO: Read the Client Information.
					if ( !empty($_ALL_POST['allotted_to_client']) && !is_array($_ALL_POST['allotted_to_client']) ) {
						//include ( DIR_FS_INCLUDES .'/clients.inc.php');
						$_ALL_POST['allotted_to_client']          = explode(',', $_ALL_POST['allotted_to_client']);
						$temp                       = "'". implode("','", $_ALL_POST['allotted_to_client']) ."'";
						$_ALL_POST['allotted_to_client_members']  = '';
						$_ALL_POST['client_details']  = array();
						Clients::getList($db, $_ALL_POST['allotted_to_client_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['allotted_to_client'] = array();
						foreach ( $_ALL_POST['allotted_to_client_members'] as $key=>$members) {
						  //for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['allotted_to_client'][] = $members['user_id'];
							$_ALL_POST['client_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}
					// EO: Read the Client Information.
                    
                    // BO: Read the Vendor Information.
					if ( !empty($_ALL_POST['allotted_to_vendor']) && !is_array($_ALL_POST['allotted_to_vendor']) ) {
						//include ( DIR_FS_INCLUDES .'/clients.inc.php');
						$_ALL_POST['allotted_to_vendor']          = explode(',', $_ALL_POST['allotted_to_vendor']);
						$temp                       = "'". implode("','", $_ALL_POST['allotted_to_vendor']) ."'";
						$_ALL_POST['allotted_to_vendor_members']  = '';
						$_ALL_POST['vendor_details']  = array();
						Vendors::getList($db, $_ALL_POST['allotted_to_vendor_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['allotted_to_vendor'] = array();
						foreach ( $_ALL_POST['allotted_to_vendor_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['allotted_to_vendor'][] = $members['user_id'];
							$_ALL_POST['vendor_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}
					// EO: Read the Vendor Information.
                    
                    
                    // BO: Read the Lead Information.
					if ( !empty($_ALL_POST['allotted_to_lead']) && !is_array($_ALL_POST['allotted_to_lead']) ) {
						//include ( DIR_FS_INCLUDES .'/sale-lead.inc.php');
						$_ALL_POST['allotted_to_lead']          = explode(',', $_ALL_POST['allotted_to_lead']);
						$temp                       = "'". implode("','", $_ALL_POST['allotted_to_lead']) ."'";
						$_ALL_POST['allotted_to_lead_members']  = '';
						$_ALL_POST['lead_details']  = array();
						Leads::getList($db, $_ALL_POST['allotted_to_lead_members'], 'lead_id,f_name,l_name,email', "WHERE lead_id IN (". $temp .")");
						$_ALL_POST['allotted_to_lead'] = array();
						foreach ( $_ALL_POST['allotted_to_lead_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['allotted_to_lead'][] = $members['lead_id'];
							$_ALL_POST['lead_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['email'] .')';
						}
					}
					// EO: Read the Lead Information.
                    
                    // BO: Read the External Users Information.
					if ( !empty($_ALL_POST['allotted_to_addr']) && !is_array($_ALL_POST['allotted_to_addr']) ) {
						//include ( DIR_FS_INCLUDES .'/address-book.inc.php');
						$_ALL_POST['allotted_to_addr']          = explode(',', $_ALL_POST['allotted_to_addr']);
						$temp                       = "'". implode("','", $_ALL_POST['allotted_to_addr']) ."'";
						$_ALL_POST['allotted_to_addr_members']  = '';
						$_ALL_POST['addr_details']  = array();
						Addressbook::getList($db, $_ALL_POST['allotted_to_addr_members'], 'id,name,company,email', "WHERE id IN (". $temp .")");
						$_ALL_POST['allotted_to_addr'] = array();
						foreach ( $_ALL_POST['allotted_to_addr_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['allotted_to_addr'][] = $members['id'];
							$_ALL_POST['addr_details'][] = $members['name'] .' ('. $members['company'] .')';
						}
					}
					// EO: Read the External Users Information.
                    
                    // BO: Read the On Behalf Users Information.
					if ( !empty($_ALL_POST['on_behalf_id']) && !is_array($_ALL_POST['on_behalf_id']) ) {
						//include ( DIR_FS_INCLUDES .'/address-book.inc.php');
						$_ALL_POST['allotted_to_addr']          = explode(',', $_ALL_POST['allotted_to_addr']);
						$temp   = "'". implode("','", $_ALL_POST['allotted_to_addr']) ."'";
						$_ALL_POST['on_behalf_members']  = '';
						$_ALL_POST['on_behalf_details']  = array();
						User::getList($db, $_ALL_POST['on_behalf_members'], 'user_id,f_name,l_name,number', "WHERE user_id ='". $_ALL_POST['on_behalf_id']."'");
                        $_ALL_POST['on_behalf_members'] = $_ALL_POST['on_behalf_members']['0'];
						$_ALL_POST['on_behalf_id'] = array();
                        $_ALL_POST['on_behalf_id'] = $_ALL_POST['on_behalf_members']['user_id'];
                        $_ALL_POST['on_behalf_details'] = $_ALL_POST['on_behalf_members']['f_name'] .' '.$_ALL_POST['on_behalf_members']['l_name'].' ('. $_ALL_POST['on_behalf_members']['number'] .')';
					}
					// EO: Read the On Behalf Users Information.
                    
                    
                    // BO: Read the Job Information.
                    if ( !empty($_ALL_POST['job_id']) ) {
						Jobs::getList($db, $_ALL_POST['job_details'], 'description', "WHERE id ='". $_ALL_POST['job_id'] ."'");   
					}
                    // EO: Read the Job Information.
                    
                    // BO: Read the Order Information.
                    if ( !empty($_ALL_POST['order_id']) && !is_array($_ALL_POST['order_id']) ) {
                        $_ALL_POST['order_id']  = $_ALL_POST['order_id'];
                        Order::getList($db, $_ALL_POST['order_details'], 'order_title', "WHERE id='".$_ALL_POST['order_id']."'");
                        $_ALL_POST['order_details'] = $_ALL_POST['order_details']['0']['order_title'];
                    }
                    // EO: Read the Order Information.
                    
                    /*$_ALL_POST['allotted_to'] = explode(',',$_ALL_POST['allotted_to']);
                    $_ALL_POST['allotted_to_client'] = explode(',',$_ALL_POST['allotted_to_client']);
                    $_ALL_POST['allotted_to_lead'] = explode(',',$_ALL_POST['allotted_to_lead']);
                    $_ALL_POST['allotted_to_addr'] = explode(',',$_ALL_POST['allotted_to_addr']);*/
                     // Setup the date of delivery.
                    $_ALL_POST['do_r']  = explode(' ', $_ALL_POST['do_r']);
                    $temp               = explode('-', $_ALL_POST['do_r'][0]);
                    $_ALL_POST['do_r']  = NULL;
                    $_ALL_POST['do_r']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    
                    if($_ALL_POST['do_deadline'] !='0000-00-00 00:00:00'){
                        $_ALL_POST['do_deadline']  = explode(' ', $_ALL_POST['do_deadline']);
                        $temp               = explode('-', $_ALL_POST['do_deadline'][0]);
                        $_ALL_POST['do_deadline']  = NULL;
                        $_ALL_POST['do_deadline']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }else{
                        $_ALL_POST['do_deadline']='';
                    }
                }
                else {
                    $messages->setErrorMessage("You do not have the Permission to view the Record with the current Access Level.");
                }
            /* 
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }*/
        }
        // get list of all comments of change log BOF
        $condition_query1 = $condition_url1 = '';
        $listLog	= 	NULL;
        $fields = TABLE_TASK_REMINDER_LOG.".id " ;
        $condition_query1 = " WHERE ".TABLE_TASK_REMINDER_LOG.".task_id = '".$id."' " ;
        $condition_query1 .= " ORDER BY ".TABLE_TASK_REMINDER_LOG.".do_a DESC";
        $total	=	Taskreminder::getDetailsCommLog( $db, $listLog, $fields , $condition_query1);
        //print_r($listLog);
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
        //$extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $listLog	= NULL;
        $fields = TABLE_TASK_REMINDER_LOG .'.*, '.TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';   
        Taskreminder::getDetailsCommLog( $db, $listLog, $fields, $condition_query1, $next_record, $rpp);
        $fList=array();
        if(!empty($listLog)){
            foreach( $listLog as $key=>$val){  
            
               $executive=array();
               $string = str_replace(",","','", $val['to_id']);
               $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
               User::getList($db,$executive,$fields1,$condition1);
               $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['to_id'] = $executivename ;
               $fList[$key]=$val;
            }
        }
        
        // get list of all comments of change log EOF
        //After edit from caledar Refresh the parent window and close the exist window
        if (  isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ) {
                if(!empty($curdate)){
                
                    if( $curz =='x' ){
                        $url = $variables['nc']."/task-reminder.php?perform=view_calendar&nextdate=".$curdate."&x=".$curno ;
                    }elseif( $curz =='y'){
                        $url = $variables['nc']."/task-reminder.php?perform=view_calendar&previousdate=".$curdate."&y=".$curno ;
                    
                    }                        
                }else{
                
                      $url = $variables['nc']."/task-reminder.php?perform=view_calendar";
                
                }
                ?>
                        <script language="javascript" type="text/javascript">        
                         window.close();
                         window.opener.location.href="<?php echo $url ;    ?>"
                      </script>   
                
                <?php
                
            }
        
        
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            $condition_query='';
            include ( DIR_FS_NC .'/task-reminder-list.php');
        }
        else {
       
            //if( $messages->getErrorMessageCount() <= 0){
           
                // These parameters will be used when returning the control back to the List page.
                foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                    $hidden[]   = array('name'=> $key, 'value' => $value);
                }
                
                 // Set the Permissions.
                $variables['can_view_ulist']     = false;
                $variables['can_view_clist']     = false;
                $variables['can_view_vlist']     = false;
                $variables['can_view_sllist']    = false;
                $variables['can_view_exlist']    = false;
                $variables['can_add_on_behalf']  = false;
                $variables['can_update_order']    = false;
                
                
                if ( $perm->has('nc_ue_list') ) {
                    $variables['can_view_ulist'] = true;
                }
                //For Clients
                if ( $perm->has('nc_uc') && $perm->has('nc_uc_list')) {
                    $variables['can_view_clist']     = true;
                }
                //For Vendor
                if ( $perm->has('nc_uv') && $perm->has('nc_uv_list') ) {
                   $variables['can_view_vlist'] = true;
                }
                //For Lead
                if ( $perm->has('nc_sl_ld') && $perm->has('nc_sl_ld_list')) {
                    $variables['can_view_sllist']     = true;
                }                
                //For External Users
                if ( $perm->has('nc_ab') && $perm->has('nc_ab_list')) {
                    $variables['can_view_exlist']     = true;
                }
                //To add on behalf of
                if ( $perm->has('nc_ts_behalf')) {
                    $variables['can_add_on_behalf']     = true;
                }
                
                if ( $perm->has('nc_ts_up_or')) {
                    $variables['can_update_order']     = true;
                }
        
                $hidden[] = array('name'=> 'id', 'value' => $id);
                $hidden[] = array('name'=> 'curz', 'value' => $curz);
                $hidden[] = array('name'=> 'curdate', 'value' => $curdate);
                $hidden[] = array('name'=> 'curno', 'value' => $curno);
                $hidden[] = array('name'=> 'perform', 'value' => 'edit');
                $hidden[] = array('name'=> 'act', 'value' => 'save');
                $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');    
				$page["var"][] = array('variable' => 'groups_list', 'value' => 'groups_list');    
                $page["var"][] = array('variable' => 'lst_job', 'value' => 'lst_job');                
                $page["var"][] = array('variable' => 'lst_type', 'value' => 'lst_type');  
                $page["var"][] = array('variable' => 'listLog', 'value' => 'fList');
                $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'task-reminder-edit.html');
            //}
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>