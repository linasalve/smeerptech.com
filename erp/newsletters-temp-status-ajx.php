<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    
	   
	
   
    
	
	$type = isset($_GET["type"])   ? $_GET["type"]  : ( isset($_POST["type"])    ? $_POST["type"] : '' );
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new 		= new db_local; // database handle
	
    if (isset($type) && $type!=''){
	
        if( $type=='bounce'){
		
			$sql= " UPDATE ".TABLE_NEWSLETTERS_MEMBERS." SET 
			 ".TABLE_NEWSLETTERS_MEMBERS.".email_bounce_status='1', 
			 ".TABLE_NEWSLETTERS_MEMBERS.".email_bounce_temp_status='0'			 
			 WHERE ".TABLE_NEWSLETTERS_MEMBERS.".email_bounce_temp_status ='1'";
			
			$execute = $db_new->query($sql);
			if($execute){
				$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				<tr>
					<td class=\"message_header_ok\">
						<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
						Success
					</td>
				</tr>
				<tr>
					<td class=\"message_ok\" align=\"left\" valign=\"top\">
						<ul class=\"message_ok_2\">
							<li class=\"message_ok\">Temp Bounce Status marked successfully </li>
						</ul>
					</td>
				</tr>
				</table>";
			}
        }elseif($type=='unsubscribe'){
		
			$sql= " UPDATE ".TABLE_NEWSLETTERS_MEMBERS." SET 
			 ".TABLE_NEWSLETTERS_MEMBERS.".unsubscribed_status='1', 
			 ".TABLE_NEWSLETTERS_MEMBERS.".unsubscribed_temp_status='0'			 
			 WHERE ".TABLE_NEWSLETTERS_MEMBERS.".unsubscribed_temp_status ='1'";
			
			$execute = $db_new->query($sql);
			if($execute){
				$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				<tr>
					<td class=\"message_header_ok\">
						<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
						Success
					</td>
				</tr>
				<tr>
					<td class=\"message_ok\" align=\"left\" valign=\"top\">
						<ul class=\"message_ok_2\">
							<li class=\"message_ok\">Temp Unsubscribed Status marked successfully </li>
						</ul>
					</td>
				</tr>
				</table>";
			}
		
		} 	   
	}	

echo $message;
exit;

?>
