<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");

    include_once ( DIR_FS_INCLUDES .'/sms-api.inc.php' );
	
    $api_id = isset($_GET['api_id']) ? $_GET['api_id'] : '';
    
   	$apiDetails = '' ;
    if (isset($api_id) && !empty($api_id))
    {
		//$subcategory_list = array();
			
		$query	="SELECT is_scrubbed FROM ".TABLE_SMS_API." WHERE ".TABLE_SMS_API.".status ='1' AND ".TABLE_SMS_API.".id= '".$api_id."'";
        $query	.= " ORDER BY ".TABLE_SMS_API.".id ASC";
                
		$db->query( $query );
		$total	= $db->nf();	
		if( $db->nf() > 0 )
		{
			while($db->next_record())
			{
                $is_scrubbed = $db->f("is_scrubbed");
                if($is_scrubbed==SmsApi::SCRUBBED){
                    $apiDetails = 'SCRUBBED';
                }else{
                    $apiDetails = 'NON-SCRUBBED';
                }
				
			}
			echo $apiDetails ;
		}else{		
			echo "";
		}
   	   
	}	
	include_once( DIR_FS_NC ."/flush.php");

?>
