<?php
    if ( $perm->has('nc_site_sp_status') ) {
        $page_id= isset($_GET["page_id"])? $_GET["page_id"] : ( isset($_POST["page_id"])? $_POST["page_id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        Staticpages::updateStatus($page_id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/staticpage-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Change Status.");
    }
?>