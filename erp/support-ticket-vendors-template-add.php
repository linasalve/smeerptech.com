<?php
if ( $perm->has('nc_st_t_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( SupportTicketVendorsTemplate::validateAdd($data, $extra) ) { 
                            $query	= " INSERT INTO ".TABLE_VENDORS_TICKET_TEMPLATES
                                    ." SET ".TABLE_VENDORS_TICKET_TEMPLATES .".title = '". 		$data['title'] ."'" 
                                    .",". TABLE_VENDORS_TICKET_TEMPLATES .".details = '". 		$data['details'] ."'"
                              		.",". TABLE_VENDORS_TICKET_TEMPLATES .".status = '". 		$data['status'] ."'"                                
                              		.",". TABLE_VENDORS_TICKET_TEMPLATES .".access_level = '".   $my['access_level'] ."'"
                                    .",". TABLE_VENDORS_TICKET_TEMPLATES .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                                    .",". TABLE_VENDORS_TICKET_TEMPLATES .".created_by = '".     $my['user_id'] ."'"
                                	.",". TABLE_VENDORS_TICKET_TEMPLATES .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/support-ticket-vendors-template.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/support-ticket-vendors-template.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/support-ticket-vendors-template.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'support-ticket-vendors-template-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
