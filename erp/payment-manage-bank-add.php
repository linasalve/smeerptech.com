<?php
  if ( $perm->has('nc_p_mb_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the payment bank class
		include_once (DIR_FS_INCLUDES .'/payment-bank.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Paymentbank::validateAdd($data, $extra) ) {
				$data['company_name'] = '';
                $data['company_prefix'] ='';
                if(!empty($data['company_id'])){
                    $company1 =null;
                    $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                    $fields_c1 = " prefix,name ";
                    Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
                    if(!empty($company1)){                  
                       $data['company_name'] = $company1[0]['name'];
                       $data['company_prefix'] = $company1[0]['prefix'];
                    }
                }	

			
                $query	= " INSERT INTO ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".bank_name = '". 			$data['bank_name'] ."'"                            
							.",". TABLE_PAYMENT_BANK .".bank_address = '". 		$data['bank_address'] ."'"							  
							.",". TABLE_PAYMENT_BANK .".company_id = '". $data['company_id'] ."'"  
							.",". TABLE_PAYMENT_BANK .".company_name = '". $data['company_name'] ."'"  
							.",". TABLE_PAYMENT_BANK .".company_prefix = '". $data['company_prefix'] ."'" 
                            .",". TABLE_PAYMENT_BANK .".status = '". 			$data['status'] ."'"  
                            .",". TABLE_PAYMENT_BANK .".type = '". 			$data['type'] ."'"  
                            .",". TABLE_PAYMENT_BANK .".opening_balance = '". 	$data['opening_balance'] ."'"                            
							.",". TABLE_PAYMENT_BANK .".balance_amt = '". 	$data['opening_balance'] ."'"  
							.",". TABLE_PAYMENT_BANK .".date = '". 				date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                	
                    $messages->setOkMessage("New bank entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                    /*
                    $dbnew=new db_local();
                    
                    $bank_query	= " INSERT INTO ".TABLE_PAYMENT_USER_BANK
                            ." SET ". TABLE_PAYMENT_USER_BANK .".user_id = '". 					$my['uid'] ."'"                                                              
                                	.",". TABLE_PAYMENT_USER_BANK .".bank_id = '". 				$variables['hid'] ."'"                                         
                                 	.",". TABLE_PAYMENT_USER_BANK .".opening_balance_amt = '". 	$data['opening_balance'] ."'";
					$dbnew->query($bank_query);        
                    */
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
         // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0){
             header("Location:".DIR_WS_NC."/payment-manage-bank.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-manage-bank.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
              header("Location:".DIR_WS_NC."/payment-manage-bank.php?added=1");
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-manage-bank-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-add-permission.html');
        
    }
?>
