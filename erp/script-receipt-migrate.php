<?php

// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/old-nc-db.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/common-functions.inc.php' );
    ini_set("max_execution_time",0);// TO EXECUTE A SCRIPT WITH UN LIMITED TIME
    
    // define tables of old nc BOF
    define("TABLE_USER_OLD","auth_user");
    define("TABLE_MEMBER_DETAILS_OLD","member_details");
	define("TABLE_BILL_INVOICE_OLD","bills_invoice");
	define("TABLE_BILL_ORDERS_OLD","bills_orders");
    //define("TABLE_BILL_MISC","bills_misc");
	define("TABLE_BILL_USER","bills_member_account");
	define("TABLE_BILL_RECEIPT_OLD","bills_receipt");
	define("TABLE_MIGRATING_RECEIPT","migrating_receipt_info");
    // define tables of old nc EOF


/*
    $db1 		= new db_local; // database handle
    // connect with old nc d/b
    $con = DB::makeConnection();
    
    $con1 = DB::makeConnection();
    $checkdate = '2007-03-31 23:59:59' ;
    
    $timedate= mktime("23","59","59", "03", "31", "2007") ;
    $timedate= mktime("23","59","59", 03, 31, 2007) ; // 31=> 32 as per given in old nc billOrders.php
    //$query = "SELECT ".TABLE_BILL_RECEIPT_OLD.".*  FROM ".TABLE_BILL_RECEIPT_OLD." WHERE receipt_created_date <= ".$timedate." ORDER BY  receipt_id " ;
    //$query = "SELECT ".TABLE_BILL_RECEIPT_OLD.".*  FROM ".TABLE_BILL_RECEIPT_OLD." WHERE receipt_date <= ".$timedate." ORDER BY  receipt_id " ;
    //$query = "SELECT ".TABLE_BILL_RECEIPT_OLD.".*  FROM ".TABLE_BILL_RECEIPT_OLD." ORDER BY  receipt_id " ;
            
    $query = "SELECT   ". TABLE_BILL_RECEIPT_OLD .".receipt_id"
						.", ". TABLE_BILL_RECEIPT_OLD .".uid" 
						.", ". TABLE_BILL_RECEIPT_OLD .".b_name"
						.", ". TABLE_BILL_RECEIPT_OLD .".b_add1"
						.", ". TABLE_BILL_RECEIPT_OLD .".b_add2"
						.", ". TABLE_BILL_RECEIPT_OLD .".b_city"
						.", ". TABLE_BILL_RECEIPT_OLD .".b_state"
						.", ". TABLE_BILL_RECEIPT_OLD .".b_country"
						.", ". TABLE_BILL_RECEIPT_OLD .".b_zip"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_no"
						.", ". TABLE_BILL_RECEIPT_OLD .".invoice_no"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_opening_balance"
						.", ". TABLE_BILL_RECEIPT_OLD .".invoice_opening_balance"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_particulars"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_amount"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_amount_words "
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_payment_mode"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_mode_text"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_note"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_date"
						.", ". TABLE_BILL_RECEIPT_OLD .".receipt_created_date"
						.", ". TABLE_MEMBER_DETAILS_OLD .".title"
						.", ". TABLE_USER_OLD .".username" 
						.", ". TABLE_USER_OLD .".company" 
						.", ". TABLE_USER_OLD .".currency" 
						.", ". TABLE_MEMBER_DETAILS_OLD .".fname"
						.", ". TABLE_MEMBER_DETAILS_OLD .".lname"
						.", ". TABLE_MEMBER_DETAILS_OLD .".add1_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".add2_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".city_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".state_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".country_h"			
						.", ". TABLE_MEMBER_DETAILS_OLD .".zip_h"
						.", ". TABLE_MEMBER_DETAILS_OLD .".add1_o"				
						.", ". TABLE_MEMBER_DETAILS_OLD .".add2_o"
						.", ". TABLE_MEMBER_DETAILS_OLD .".city_o"				
						.", ". TABLE_MEMBER_DETAILS_OLD .".state_o"
						.", ". TABLE_MEMBER_DETAILS_OLD .".country_o"			
						.", ". TABLE_MEMBER_DETAILS_OLD .".zip_o"
						.", ". TABLE_MEMBER_DETAILS_OLD .".primary_address"
						.", ". TABLE_BILL_USER .".b_user_status "
				." FROM ". TABLE_BILL_RECEIPT_OLD 
				." INNER JOIN ". TABLE_MEMBER_DETAILS_OLD ." ON  ". TABLE_BILL_RECEIPT_OLD .".uid=". TABLE_MEMBER_DETAILS_OLD .".uid"
				." INNER JOIN ". TABLE_USER_OLD ." ON  ". TABLE_BILL_RECEIPT_OLD .".uid=". TABLE_USER_OLD .".uid"
				." INNER JOIN ". TABLE_BILL_USER ." ON  ". TABLE_BILL_RECEIPT_OLD .".uid=". TABLE_BILL_USER .".uid" ;
    
	$result = mysql_query($query,$con);
    
    $listInvDone =$listInvid= $invoiceNotFound=$billaddNotFoundReceipt=array();
    $invoiceNotExistContent=$receiptExistContent=$billaddNotFoundContent=$receiptEnterdContent='';
	if( mysql_num_rows($result) > 0){
        while ($row = mysql_fetch_array($result)){
            $error = array();
            $oldreceipt_id = $row['receipt_id'];
            $uid = $row['uid'];
            $receipt_no = $row['receipt_no'];
            
            //default curency
            $currency ='INR';
            $u_name = $company = '';
            //Default currency details bof
            $currency_id = 1 ;
            $currency_abbr = 'INR';
            $currency_name = 'Rupees';
            $currency_symbol = 'Rs';
            $currency_country = 'India';
            //Default currency details eof
            
            //select currency from user table
           
            $currency_old =$row['currency'];
            $currency = substr($row['currency'], 0, 3); 
            $currency = trim($currency);
            if($currency=='INR'){
                $currency_id = 1 ;
                $currency_abbr = 'INR';
                $currency_name = 'Rupees';
                $currency_symbol = 'Rs';
                $currency_country = 'India';
            }elseif($currency=='USD'){
                $currency_id = 2 ;
                $currency_abbr = 'USD';
                $currency_name = 'USD';
                $currency_symbol = '$';
                $currency_country = 'United States of America';
            }
            
            
            $pre = $row['primary_address'] ; 
            
            $u_name = $row['title']." ".$row['fname']." ".$row['lname'] ;
            $company = $row['company'] ;
            
            $b_add1= stripslashes(($row['b_add1']=='')? $row["add1$pre"] : $row['b_add1']);
            $b_add2= stripslashes(($row['b_add2']=='')? $row["add2$pre"] : $row['b_add2']);
            $city= stripslashes(($row['b_city']=='')? $row["city$pre"] : $row['b_city']);
            $state= stripslashes(($row['b_state']=='')? $row["state$pre"] : $row['b_state']);
            $country= stripslashes(($row['b_country']=='')? $row["country$pre"] : $row['b_country']);
            $zip= stripslashes(($row['b_zip']=='')? $row["zip$pre"] : $row['b_zip']);
            $b_name = $row['b_name'];            
            if(empty($b_name)){                
                $b_name = (!empty($company)) ? $company : $u_name;       
            }        
            // Code for old billing address BOF            
            $old_billing_address= '' ;
            echo "<br/> Address" ;
            echo "b_add1 <br/>".$b_add1  ;
            echo "b_add2 <br/>".$b_add2 ;
            echo "b_city <br/>".$city ;
            echo "b_state <br/>".$state  ;
            echo "country <br/>".$country  ;
            // Code for old billing address BOF
            $b_addr = $b_add1."<br>".$b_add2 ;
            
            if(!empty($b_add1)){
                 $old_billing_address.=$b_add1."<br>";
            }
            if(!empty($b_add2)){
               $old_billing_address .=$b_add2."<br>";            
            }
            if(!empty($city)){
               $old_billing_address .= $city." ".$zip."<br>";            
            }
            if(!empty($state)){
               $old_billing_address .=$state."<br>";            
            }
            if(!empty($country)){
               $old_billing_address .=$country."<br>";            
            }            
            $data['old_billing_address'] = $old_billing_address;
            // Code for old billing address EOF            
                       
            $invoice_no = $row['invoice_no'];  
            //Check This invoice exist in database or not BOF
            $sql ='SELECT * FROM '.TABLE_BILL_INV." WHERE ".TABLE_BILL_INV .".number = '".$invoice_no."'";
            $db->query($sql);
            if($db->nf() == 0 ){            
                  $invoiceNotExistContent .= " Invoice no of current receipt is not exist in new database. Invoice no is '$invoice_no' "."\n";
                  $invoiceNotFound[] = $invoice_no  ;
                  continue;
            }
            //Check This invoice exist in database or not EOF


          
           
            
            
            $receipt_opening_balance = $row['receipt_opening_balance'];            
            $invoice_opening_balance = $row['invoice_opening_balance'];
            $invoice_closing_balance =( ( floatval($row["invoice_opening_balance"]) ) - ( floatval($row["receipt_amount"]))) ;            
            $receipt_particulars = $row['receipt_particulars'];
            $receipt_amount = $row['receipt_amount'];
            $receipt_amount_words = $row['receipt_amount_words'];
            $amount_domain = '';
            $amount_hosting_rs ='';
            $amount_hosting_sb ='';
            $amount_development='';
            $amount_products = '';
            $amount_designing ='';
            $amount_consult = '';
            $amount_advsmt = '';
            $amount_seo = '';
            $amount_maintain =''; 
            $amount_network = '';
            $amount_hardware = '';
            $amount_bpo = '';
            $amount_research =''; 
            
            
            $receipt_note = $row['receipt_note'];
            $receipt_payment_mode = $row['receipt_payment_mode'];
            $receipt_mode_text = $row['receipt_mode_text'];
            $receipt_date = $row['receipt_date'];
            $receipt_created_date = $row['receipt_created_date'];
            
           
            // Code for old billing address EOF
             
            $receipt_date  = date("Y-m-d",$receipt_date );
            $receipt_created_date =  date("Y-m-d",$receipt_created_date );            
            
            // default access level 1 for top
            $access_level = "65535";            
            //username aryan
            $created_by = "e68adc58f2062f58802e4cdcfec0af2d";           
            //anjani.singh
            $manager="4c87b6500c1bca5328824568aaa26174";
            //username aryan
            $teamStr ="e68adc58f2062f58802e4cdcfec0af2d";
            // by default  service_id is 10 for 'Custom Development'
            $service_id = "10";
            
            $inv_list  = NULL;          
                        
            echo $query = "SELECT id FROM ". TABLE_BILL_RCPT
                        ." WHERE number = '". $receipt_no."'";
                        
            if ( $db->query($query) && $db->nf() > 0 ) {
               //$messages->setErrorMessage("Already exist receipt no.");                
               //$error[] = 'Already exist';
               $receiptExistContent .= " Receipt Already exist in database. receipt no is '$receipt_no' "."\n";
               $receiptExist[] = $receipt_no ;
               continue;
                
                
            }
            //get calculate $b_add1 id from address BOF
            $b_add1sql = "SELECT id FROM ".TABLE_ADDRESS." WHERE address_of ='".$uid."' AND table_name='clients' ";
            $db1->query($b_add1sql) ;
            if ( $db1->nf() > 0 ) {
                while ($db1->next_record()) {
                        $billing_address = $db1->f('id');
                }
            }
            //get calculate $b_add1 id from address EOF 
            $data['billing_address'] = $billing_address;
            //get address from Address table BOF
            include_once ( DIR_FS_CLASS .'/Region.class.php');
            $region         = new Region();
            $region->setAddressOf(TABLE_CLIENTS, $uid);
            if ( !($data['b_address']  = $region->get($data['billing_address'])) ) {
                  //$error[] = " The selected billing address was not found ";  
                  
                  $billaddNotFoundContent .= " Billing address was not found for receipt. receipt no is '$receipt_no' "."\n";
                  $billaddNotFoundReceipt[] = $receipt_no;
                  continue;
            }
            //get address from Address table EOF
           
            $discount=$octroi=$service_tax=$vat=0;
            $status = "1"   ;
            $data['number'] = $receipt_no;
            $data['inv_no']  = $invoice_no;       
            $data['access_level']  =$access_level;
            $data['created_by']     =$created_by;
            $data['client']['user_id']  =$uid;
            $data['currency']    =    $currency;
            $data['discount']    =    $discount;
            $data['octroi']      =  $octroi;
            $data['service_tax'] = $service_tax;
            $data['vat']        = $vat;
            $data['amount']     =  $receipt_amount;
            $data['amount_inr']  =  $receipt_amount;
            $data['amount_words']  =    $receipt_amount_words ;
            $data['p_mode']     =   $receipt_payment_mode;
            $data['p_details']  = $receipt_mode_text;
            $data['do_c']       =  $receipt_created_date;
            $data['do_r']       =     $receipt_date;
            $data['remarks']    =   $receipt_note;
            $data['balance']    =   $receipt_opening_balance;
            //inv balance
            $data['invbalance']    =   $invoice_closing_balance;
            
            $data['billing_name']  =    $b_name ;
            //$data['billing_address']  = $b_add1 ;
            $data['status']  = $status;
            $data['number']  =$receipt_no;
            $data['domain']  =$amount_domain ;
            $data['server1_rs'] =$amount_hosting_rs ;
            $data['server2_sb']  =$amount_hosting_sb ;
            $data['develop']  =$amount_development ;
            $data['product']  =  $amount_products ;
            $data['design']  =$amount_designing ;
            
            $data['bpo']  =$amount_bpo ;
            $data['seo']  = $amount_seo ;
            $data['consult']  = $amount_consult ;
            $data['research']  = $amount_research ;
            $data['advert']  =$amount_advsmt ;
            $data['webmaint']  =$amount_maintain  ;
            $data['network']  = $amount_network   ;
            $data['hardware']  = $amount_hardware  ;
            $data['particulars'][0] =  $receipt_particulars;             
            $data['p_amount'][0] =  $receipt_amount;  
           
            if ( empty($error) ){
                $rcp_pid = '';
                 
                 echo "<br/>";
                 
                echo  $query	= " INSERT INTO ".TABLE_BILL_RCPT
                                ." SET ". TABLE_BILL_RCPT .".number = '".    processUserData($receipt_no )."'"
                                .",". TABLE_BILL_RCPT .".inv_no = '".        processUserData($invoice_no) ."'"
                                .",". TABLE_BILL_RCPT .".company_id = '1'"
                                .",". TABLE_BILL_RCPT .".access_level = '".  processUserData($access_level) ."'"
                                .",". TABLE_BILL_RCPT .".created_by = '".    processUserData($created_by) ."'"
                                .",". TABLE_BILL_RCPT .".client = '".        processUserData($uid)."'"
                                .",". TABLE_BILL_RCPT .".currency_old = '".  processUserData($currency_old) ."'"
                                .",". TABLE_BILL_RCPT .".currency_abbr  = '".processUserData($currency_abbr) ."'"
                                .",". TABLE_BILL_RCPT .".currency_id = '".   processUserData($currency_id) ."'"
                                .",". TABLE_BILL_RCPT .".currency_name = '". processUserData($currency_name) ."'"
                                .",". TABLE_BILL_RCPT .".currency_symbol = '".processUserData($currency_symbol) ."'"
                                .",". TABLE_BILL_RCPT .".currency_country= '".processUserData($currency_country) ."'"
                                .",". TABLE_BILL_RCPT .".discount = '".      processUserData($discount )."'"
                                .",". TABLE_BILL_RCPT .".octroi = '".        processUserData($octroi) ."'"
                                .",". TABLE_BILL_RCPT .".service_tax = '".   processUserData($service_tax) ."'"
                                .",". TABLE_BILL_RCPT .".vat = '".           processUserData($vat) ."'"
                                .",". TABLE_BILL_RCPT .".amount = '".        processUserData($receipt_amount)."'"
                                .",". TABLE_BILL_RCPT .".amount_inr = '".    processUserData($receipt_amount) ."'"
                                .",". TABLE_BILL_RCPT .".amount_words = '".  processUserData($receipt_amount_words) ."'"
                                .",". TABLE_BILL_RCPT .".do_c = '".          $receipt_created_date ."'"
                                .",". TABLE_BILL_RCPT .".do_r = '".          $receipt_date ."'"
                                .",". TABLE_BILL_RCPT .".remarks = '".       processUserData($receipt_note) ."'"
                                .",". TABLE_BILL_RCPT .".p_mode = '".        processUserData($receipt_payment_mode) ."'"
                                .",". TABLE_BILL_RCPT .".payment_mode_name = '".processUserData($receipt_payment_mode) ."'"
                                .",". TABLE_BILL_RCPT .".p_details = '".     processUserData($receipt_mode_text) ."'"
                                .",". TABLE_BILL_RCPT .".balance = '".       processUserData($receipt_opening_balance) ."'" //invoice closing balance
                                .",". TABLE_BILL_RCPT .".invoice_closing_balance = '".processUserData($invoice_closing_balance) ."'" //invoice closing balance
                                .",". TABLE_BILL_RCPT .".invoice_opening_balance = '".processUserData($invoice_opening_balance) ."'"
                                .",". TABLE_BILL_RCPT .".billing_name = '".  processUserData($b_name )."'"
                                .",". TABLE_BILL_RCPT .".billing_address='". processUserData($billing_address) ."'"
                                .",". TABLE_BILL_RCPT .".old_billing_address = '". processUserData($old_billing_address) ."'"
                                 .",". TABLE_BILL_RCPT .".b_addr_company_name = '". processUserData($b_name) ."'"
                                .",". TABLE_BILL_RCPT .".b_addr              = '".processUserData($b_addr) ."'"
                                .",". TABLE_BILL_RCPT .".b_addr_city         = '".processUserData($city) ."'"
                                .",". TABLE_BILL_RCPT .".b_addr_state        = '".processUserData($state) ."'"
                                .",". TABLE_BILL_RCPT .".b_addr_country      = '".processUserData($country) ."'"
                                .",". TABLE_BILL_RCPT .".b_addr_zip          = '".processUserData($zip) ."'"         
                                .",". TABLE_BILL_RCPT .".is_old              ='1'"
                                .",". TABLE_BILL_RCPT .".status              = '".        $status ."'" ;
                                
                $db->query($query)   ;
                $rcpid = $db->last_inserted_id();
                echo "<br/>";
                // Insert the Bifurcations.
                $rcp_bid=0;
                
                 //get clients details from client table EOF 
                echo $query2 = "SELECT user_id, number, manager, f_name, l_name, email, status FROM ". TABLE_CLIENTS
                            ." WHERE user_id = '". $uid ."'";
                $db1->query($query2);
                $db1->next_record();
                $dataClient = processSQLData($db1->result());
                $data['client'] =  $dataClient;
               
                //get clients details from client table BOF 
                echo "<br/>"; 
                
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS, $uid);
                $data['b_address']  = $region->get($data['billing_address']);
                //creater details BOF
                echo $query3 = "SELECT user_id, number, f_name, l_name, email, status FROM ". TABLE_USER
                            ." WHERE user_id = '". $created_by ."'"
                            ." OR username = '". $created_by ."'"
                            ." OR number = '". $created_by ."'" ;
                $db1->query($query3);
                $db1->next_record();
                $data['creator'] = processSQLData($db1->result());
                //creater details EOF
                echo "<br/>"; 
               
                
                // Create the Invoice PDF, HTML in file.
               
                //migration info               
                echo $sql2 = " INSERT INTO ".TABLE_MIGRATING_RECEIPT
                       ." SET ". TABLE_MIGRATING_RECEIPT .".old_rcp_id  = '". $oldreceipt_id ."'"
                       .",". TABLE_MIGRATING_RECEIPT .".new_rcp_id      = '". $rcpid ."'"
                       .",". TABLE_MIGRATING_RECEIPT .".new_rcp_b_id    = '". $rcp_bid ."'" 
                       .",". TABLE_MIGRATING_RECEIPT .".new_rcp_p_id    = '". $rcp_pid  ."'" 
                       .",". TABLE_MIGRATING_RECEIPT .".receipt_no      = '". $receipt_no  ."'" ;
                 echo "<br/>";
                $db->query($sql2);
                
                $receiptEnterdContent.=" Receipt enterd now ( Current script )"." Receipt no ".$receipt_no."\n";                
                $listInvDone[] = $receipt_no  ;
                
            }else{
                $listInvid[]= $receipt_no  ;                            
            }
            
        }
    }
    DB::closeConnection($con);
    // already entered invoice list 
    if(!empty( $listInvid)){ 
        echo " Following receipt are completed previously";
        print_r($listInvid);
        echo "<br>";
    }
    if($listInvDone){
        echo "Following receipt are created Now";
        echo "<br>";
        print_r($listInvDone);
    }
    if( !empty($invoiceNotFound)){
        echo "Following invoice are not found";
        echo "<br>";
        print_r($invoiceNotFound);
    }

    //write the errors in file BOF 
    $file='migrate-receipt.txt';
    $fp = fopen($file, 'a+');
    $content = " ----------- Script date ".date('Y-m-d')." ----------- \n " ;
	$content .= "------------ Invoice not exist Content ------------"."\n" ;
	$content .= $invoiceNotExistContent ;
	$content .= "------------ receipt Exist in New Database ------------"."\n" ;
    $content .= $receiptExistContent ;
    $content .= "------------ Bill address not found for receipt  ------------"."\n" ;
    $content .= $billaddNotFoundContent ;
    $content .= "------------ Script entered receipt (Currently entered receipt) ------------"."\n" ;
    echo $content .= $receiptEnterdContent;
    
    if (fwrite($fp, $content) === FALSE) {
        echo "Cannot write to file ($file)";
        exit;
    }
     fclose($fp);
	*/
    //write the errors in file EOF 
?>