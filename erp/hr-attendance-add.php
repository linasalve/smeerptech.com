<?php
  if ( $perm->has('nc_hr_att_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        
		
        
		$lst_hr = HrAttendance::getHrs();
		$lst_min = HrAttendance::getMin();
		$lst_sec = HrAttendance::getSec();
        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                         	'messages'          => &$messages
                        );
                
               
            if ( HrAttendance::validateAdd($data, $extra) ) { 
                
                if($data['pstatus']==HrAttendance::PRESENT || $data['pstatus']==HrAttendance::HALFDAY){
                
                    $worked_hrs ='';
                    //Calculate Worked hrs between intime and outtime bof
                    $sql= " SELECT TIMEDIFF('".$data['todate']."','".$data['fromdate']."') as work_hrs";
                    $db->query($sql);	
                    while ( $db->next_record() ) {
                        $worked_hrs = $db->f('work_hrs');
                    }
                    //Calculate Worked hrs between intime and outtime eof
                   
                    $query	= " INSERT INTO ".TABLE_HR_ATTENDANCE
                            ." SET ".TABLE_HR_ATTENDANCE .".att_time_in = '".$data['fromdate']."'"  
                            .",". TABLE_HR_ATTENDANCE .".att_time_out = '". $data['todate']."'"
                            .",". TABLE_HR_ATTENDANCE .".att_place = '".OFFICE_PLACE ."'"
                            .",". TABLE_HR_ATTENDANCE .".pstatus = '". $data['pstatus'] ."'"
                            .",". TABLE_HR_ATTENDANCE .".att_reason = '". $data['att_reason'] ."'"    
                            .",". TABLE_HR_ATTENDANCE .".worked_hrs = '". $worked_hrs ."'"    
                            .",". TABLE_HR_ATTENDANCE .".date = '". date('Y-m-d H:i:s', $data['date']) ."'"    
                            .",". TABLE_HR_ATTENDANCE .".uid = '".$data['executive'] ."'"    
                            .",". TABLE_HR_ATTENDANCE .".created_by = '".$my['uid'] ."'"    
                            .",". TABLE_HR_ATTENDANCE .".ip       = '". $_SERVER['REMOTE_ADDR'] ."'"                                      
                            .",". TABLE_HR_ATTENDANCE .".do_e = '". date('Y-m-d H:i:s')."'";
           
                    
                }else{
                
                    $query	= " INSERT INTO ".TABLE_HR_ATTENDANCE
                                    ." SET ". TABLE_HR_ATTENDANCE .".att_place = '".OFFICE_PLACE ."'"
									.",". TABLE_HR_ATTENDANCE .".pstatus = '". $data['pstatus'] ."'"
                                    .",". TABLE_HR_ATTENDANCE .".att_reason = '". $data['att_reason'] ."'"    
                                    .",". TABLE_HR_ATTENDANCE .".date = '". date('Y-m-d H:i:s', $data['date']) ."'"    
                                    .",". TABLE_HR_ATTENDANCE .".uid = '".$data['executive'] ."'"    
                                    .",". TABLE_HR_ATTENDANCE .".created_by = '".$my['uid'] ."'"    
                                    .",". TABLE_HR_ATTENDANCE .".ip       = '". $_SERVER['REMOTE_ADDR'] ."'"                                      
                                	.",". TABLE_HR_ATTENDANCE .".do_e = '". date('Y-m-d H:i:s')."'";
                
                
                }
              
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                   
                    $variables['hid'] = $db->last_inserted_id();              
                }
             
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
       
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/hr-attendance.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/hr-attendance.php?perform=list");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/hr-attendance.php?added=1&perform=list");   
        }
        else {
	        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'lst_hr', 'value' => 'lst_hr');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'lst_sec', 'value' => 'lst_sec');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');  
			
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-attendance-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
