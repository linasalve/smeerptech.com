<?php

    if ( $perm->has('nc_bl_invprfm_details') ) {
        
		include_once (DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php') ;
		
		$profm_id = isset($_GET['profm_id'])?$_GET['profm_id']:( isset($_POST['profm_id'] )?$_POST['profm_id'] :'');
        $type   = isset ($_GET['type']) ? $_GET['type'] : ( isset($_POST['type'] ) ? $_POST['type'] :'');
        $condition_query= " WHERE ". TABLE_BILL_INV_PROFORMA .".id = '". $profm_id ."'" ;
        
        
        $list	= NULL;
        if ( InvoiceProforma::getDetails( $db, $list, TABLE_BILL_INV_PROFORMA .'.id, '. TABLE_BILL_INV_PROFORMA .'.dm_number', $condition_query) ) {
          
            if ( $type == 'pdf' ) {
                $file_path = DIR_FS_ORDDM_PDF_FILES ."/";
                $file_name = $list[0]["dm_number"] .".pdf";
                $content_type = 'application/pdf';
            } else {
                echo('<script language="javascript" type="text/javascript">alert("Invalid File specified");</script>');
            }           
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-type: $content_type");
            header("Content-Disposition: attachment; filename=". $file_name );
            header("Content-Length: ".filesize($file_path.$file_name));
            readfile($file_path.$file_name);
        }
        else {
            echo('<script language="javascript" type="text/javascript">alert("The File is not found or you do not have the permission to view the file.");</script>');
        }
    }
    else {
        echo('<script language="javascript" type="text/javascript">alert("You do not have the permission to view the list.");</script>');
    }
    exit;
?>