<?php
    if ( $perm->has('nc_bill_ord_tax_csv') ) { ////$perm->has('nc_ord_uf_yr_csv')
        $_ALL_POST  = NULL;
        $data       = NULL;
        $messages_ask 	= new Messager;	// create the object for the Message class.
		
		
        $CSV = Order::getRestrictionsFiles();
        
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);          
			$files       = processUserData($_FILES);
    		$ask_added      = 0;
			$ask_duplicate  = 0;
			$ask_empty      = 0;
			
            $extra = array( 'file'      => &$files,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        ); 
            if ( Order::validateUpload($data, $extra) ) {
				/* if(!empty($files['file_csv']["name"])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_csv']["name"]);
					$ext = $filedata["extension"] ; 
					//$attachfilename = $username."-".mktime().".".$ext ;
					$tax_file_name = mktime()."_".$files['file_csv']["name"] ;
					$data['file_csv'] = $tax_file_name;
					
					$ticket_attachment_path = DIR_WS_ST_FILES;
					
					if(copy($files['file_csv']['tmp_name'], 
						DIR_FS_TAX_FILES."/".$tax_file_name)){
						@chmod(DIR_FS_TAX_FILES."/".$tax_file_name, 0777);
					}
				}  */
			
			
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $invalid  = 0;
                $empty      = 0;
                $index      = array();
                $handle     = fopen($_FILES['file_csv']['tmp_name'],"rb");
                $message = '';
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
					
					
                    if ( $row == 1 ) {
                        $num = count($data);
                        for ($i=0; $i < $num; $i++) {
                            $case = trim($data[$i]);
                            switch ($case) {
                                case ('ProfmNo'): {
                                    $index['ProfmNo'] = $i;
                                    break;
                                }
								case ('financial_yr'): {
                                    $index['financial_yr'] = $i;
                                    break;
                                }								
                            }
                        }
                    }else{                 
                          $index= array (
							"ProfmNo"=>0,
							"financial_yr"=>1 
                           );
						   $or_no='';
                        if ( isset($index['ProfmNo']) && $data[$index['financial_yr']] != '' ) {
							//Get Ord No from ProfmNo BOF
							$sql1 = "SELECT or_no FROM ".TABLE_BILL_INV_PROFORMA." WHERE number='".$data[$index['ProfmNo']]."'";
							if ( $db->query($sql1) ) {
								if ( $db->nf() > 0 ) {
									while ($db->next_record()) {
										$or_no = $db->f("or_no") ;
									}
								} 
							}
							//Get Ord No from ProfmNo EOF
							
                            if(!empty($or_no)){
								$sqlu=" UPDATE ".TABLE_BILL_ORDERS 
								." SET financial_yr = '".trim($data[$index['financial_yr']])."'"
								." WHERE number = '".trim($or_no)."'";				
								$db->query($sqlu) ; 
								
								$added += 1;
								$messages->setOkMessage('Updated Order No. - '.$or_no.'; Proforma Invoice No - '.$data[$index['ProfmNo']]);  
								
							}else{
								$invalid += 1;
								$messages->setErrorMessage('Not updated. Check Proforma No. - '.$data[$index['ProfmNo']]); 
							}
							
                        }else{
                            $empty += 1;
                        } 
                        // The Row is not the First row. 
                    }                    
                }
				$row = $row-1;
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been undated.');
                
                if ($invalid)
                    $messages->setOkMessage($invalid .' out of '. $row .' not found in database');
				if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
        }
        
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel']) || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			 
			 header("Location:".DIR_WS_NC."/ord-financial-yr.php?perform=list");
        }else{
			
			 // Set the Permissions.
			$variables['can_add']     = false;
			$variables['can_view_list']     = false;
			if ( $perm->has('nc_bill_ord_tax_list') ) {
				$variables['can_view_list']     = true;
			}
			if ( $perm->has('nc_bill_ord_tax_csv') ) {
				$variables['can_add']     = true;
			}
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'csv_upload');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'ord-financial-yr-csv.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>