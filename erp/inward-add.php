<?php
    if ( $perm->has('nc_inward_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }*/
		$_ALL_POST['date']= date('d/m/Y');
        
		// Include the hardware book class.
		include_once (DIR_FS_INCLUDES .'/inward.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Inward::validateAdd($data, $extra) ) {
                
                $sql_u = '';                
                if($data['tpf_status'] == Inward::COMPLETED){
                    $sql_u = ",". TABLE_IN_WARD .".do_u = '".date("Y-m-d H:i:s")."'" ;
                }
                
                $query	= " INSERT INTO ".TABLE_IN_WARD
                            ." SET ".TABLE_IN_WARD .".type = '".                $data['type'] ."'"
                                .",". TABLE_IN_WARD .".account_no = '".         $data['account_no'] ."'"
                                .",". TABLE_IN_WARD .".is_client = '".         $data['is_client'] ."'"
                                .",". TABLE_IN_WARD .".from = '".               $data['from'] ."'"
                                .",". TABLE_IN_WARD .".particulars = '".        $data['particulars'] ."'"
                                .",". TABLE_IN_WARD .".access_level = '".       $my['access_level'] ."'"
                                .",". TABLE_IN_WARD .".created_by = '".         $data['created_by'] ."'"
                                .",". TABLE_IN_WARD .".content = '".            $data['content'] ."'"
                                .",". TABLE_IN_WARD .".content_type = '".       $data['content_type'] ."'"
                                .",". TABLE_IN_WARD .".mode = '".               $data['mode'] ."'"
                                .",". TABLE_IN_WARD .".for = '".                $data['for'] ."'"
                                .",". TABLE_IN_WARD .".tpf = '".                $data['tpf'] ."'"
                                .",". TABLE_IN_WARD .".tpf_status = '".         $data['tpf_status'] ."'"
                                .$sql_u
                                .",". TABLE_IN_WARD .".ip = '".        $_SERVER['REMOTE_ADDR'] ."'"
                                .",". TABLE_IN_WARD .".date = '".  $data['date']."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Inward entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
         // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/inward.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/inward.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/inward.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'inward-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }        
?>
