<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
	   

    
    $credit_pay_bank_id = isset($_GET['credit_pay_bank_id']) ? $_GET['credit_pay_bank_id'] : '';
    $debit_pay_bank_id = isset($_GET['debit_pay_bank_id']) ? $_GET['debit_pay_bank_id'] : '';
    $transaction_type = isset($_GET['transaction_type']) ? $_GET['transaction_type'] : '';
    
	  
    $optionLink =$stringOpt= '' ;
    if(!empty($client_details)){
        $sString = $client_details;
    }
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if( (!empty($debit_pay_bank_id) || !empty($credit_pay_bank_id) ) && $transaction_type!=''){
	
        header("Content-Type: application/json");

		
	    $query =" SELECT DISTINCT(".TABLE_BANK_CHEQUE.".id),".TABLE_BANK_CHEQUE.".cheque_no FROM 
		".TABLE_BANK_CHEQUE." WHERE ".TABLE_BANK_CHEQUE.".status_u='0' AND 
		".TABLE_BANK_CHEQUE.".status='1'";
		
		if($transaction_type==Paymenttransaction::PAYMENTIN  ){
			$query .= " AND ".TABLE_BANK_CHEQUE.".bank_id='".$credit_pay_bank_id."'" ;
		}elseif($transaction_type==Paymenttransaction::PAYMENTOUT ){
			$query .= " AND ".TABLE_BANK_CHEQUE.".bank_id='".$debit_pay_bank_id."'" ;
		}elseif($transaction_type==Paymenttransaction::INTERNAL ){
			$query .= " AND ".TABLE_BANK_CHEQUE.".bank_id='".$debit_pay_bank_id."'" ;
		}
		$query .= " LIMIT 0,".CHEQUE_LIMIT;
        $db->query($query);
        //$total	= $db->nf();	
		if( $db->nf() > 0 )
		{
			while($db->next_record())
			{
				$list .=  $db->f("cheque_no") ."|". $db->f("id") .",";
			}
			echo rtrim($list,',');
		}else{		
			echo "No data|0";
		}	   
	}
 
include_once( DIR_FS_NC ."/flush.php");

?>
