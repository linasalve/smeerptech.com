<?php
if ( $perm->has('nc_rps_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        $lst_type = null;
        $lst_type = Repository::getType();
        
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_REPOSITORY_FILE_SIZE     ;                
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                            );
            
            if ( Repository::validateUpdate($data, $extra) ) {
					$sql_file1=$sql_file2=$sql_file3=$sql_file4=$sql_file5='';
					$sql_print_file1=$sql_print_file2=$sql_print_file3=$sql_print_file4=$sql_print_file5='';
					if(!empty($data['file_1'])){
						if(!empty($data['old_file_1'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_1']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_1']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-1".".".$ext ;
						$data['file_1'] = $attachfilename;						
						if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   
						  @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						  $sql_file1 = ",". TABLE_REPOSITORY .".file_1 = '".    $data['file_1'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_1']) && $data['delete_file_1']==1){
							$data['file_1']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_1']);
							$sql_file1 = ",". TABLE_REPOSITORY .".file_1 = '".    $data['file_1'] ."'" ;
						}
					}
					if(!empty($data['file_2'])){
						if(!empty($data['old_file_2'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_2']);
						}						
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_2']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-2".".".$ext ;
						$data['file_2'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_2']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						   $sql_file2 = ",". TABLE_REPOSITORY .".file_2 = '".    $data['file_2'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_2']) && $data['delete_file_2']==1){
							$data['file_2']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_2']);
							$sql_file2 = ",". TABLE_REPOSITORY .".file_2 = '".    $data['file_2'] ."'" ;
						}
					}
					if(!empty($data['file_3'])){
						if(!empty($data['old_file_3'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_3']);
						}						
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_3']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-3".".".$ext ;
						$data['file_3'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_3']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						   $sql_file3 = ",". TABLE_REPOSITORY .".file_3 = '". $data['file_3'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_3']) && $data['delete_file_3']==1){
							$data['file_3']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_3']);
							$sql_file3 = ",". TABLE_REPOSITORY .".file_3 = '". $data['file_3'] ."'" ;
						}
					
					}				
					if(!empty($data['file_4'])){
						if(!empty($data['old_file_4'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_4']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_4']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-4".".".$ext ;
						$data['file_4'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_4']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						   $sql_file4 = ",". TABLE_REPOSITORY .".file_4 = '". $data['file_4'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_4']) && $data['delete_file_4']==1){
							$data['file_4']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_4']);
							$sql_file4 = ",". TABLE_REPOSITORY .".file_4 = '". $data['file_4'] ."'" ;
						}
					}
					if(!empty($data['file_5'])){
						if(!empty($data['old_file_5'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_5']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_5']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-5".".".$ext ;
						$data['file_5'] = $attachfilename;                    
						if( move_uploaded_file ($files['file_5']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						   $sql_file5 = ",". TABLE_REPOSITORY .".file_5 = '". $data['file_5'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_5']) && $data['delete_file_5']==1){
							$data['file_5']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_file_5']);
							$sql_file5 = ",". TABLE_REPOSITORY .".file_5 = '". $data['file_5'] ."'" ;
						}					
					}
					
					if(!empty($data['print_file_1'])){
						if(!empty($data['old_print_file_1'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_1']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_1']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-1".".".$ext ;
						$data['print_file_1'] = $attachfilename;						
						if (move_uploaded_file ($files['print_file_1']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   
						  @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						  $sql_print_file1 = ",". TABLE_REPOSITORY .".print_file_1 = '".    $data['print_file_1'] ."'" ;
						}
					}else{
						if(isset($data['delete_print_file_1']) && $data['delete_print_file_1']==1){
							$data['print_file_1']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_1']);
							$sql_print_file1 = ",". TABLE_REPOSITORY .".print_file_1 = '".    $data['print_file_1'] ."'" ;
						}
					}
					if(!empty($data['print_file_2'])){
						if(!empty($data['old_print_file_2'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_2']);
						}						
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_2']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-2".".".$ext ;
						$data['print_file_2'] = $attachfilename;                    
						if (move_uploaded_file ($files['print_file_2']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						   $sql_print_file2 = ",". TABLE_REPOSITORY .".print_file_2 = '".    $data['print_file_2'] ."'" ;
						}
					}else{
						if(isset($data['delete_print_file_2']) && $data['delete_print_file_2']==1){
							$data['print_file_2']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_2']);
							$sql_print_file2 = ",". TABLE_REPOSITORY .".print_file_2 = '".    $data['print_file_2'] ."'" ;
						}
					}
					if(!empty($data['print_file_3'])){
						if(!empty($data['old_print_file_3'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_3']);
						}						
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_3']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-3".".".$ext ;
						$data['print_file_3'] = $attachfilename;                    
						if (move_uploaded_file ($files['print_file_3']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						   $sql_print_file3 = ",". TABLE_REPOSITORY .".print_file_3 = '". $data['print_file_3'] ."'" ;
						}
					}else{
						if(isset($data['delete_print_file_3']) && $data['delete_print_file_3']==1){
							$data['print_file_3']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_3']);
							$sql_print_file3 = ",". TABLE_REPOSITORY .".print_file_3 = '". $data['print_file_3'] ."'" ;
						}
					
					}				
					if(!empty($data['print_file_4'])){
						if(!empty($data['old_print_file_4'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_4']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_4']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-4".".".$ext ;
						$data['file_4'] = $attachfilename;                    
						if (move_uploaded_file ($files['print_file_4']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						   $sql_print_file4 = ",". TABLE_REPOSITORY .".print_file_4 = '". $data['print_file_4'] ."'" ;
						}
					}else{
						if(isset($data['delete_print_file_4']) && $data['delete_print_file_4']==1){
							$data['print_file_4']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_4']);
							$sql_print_file4 = ",". TABLE_REPOSITORY .".print_file_4 = '". $data['print_file_4'] ."'" ;
						}
					}
					if(!empty($data['print_file_5'])){
						if(!empty($data['old_print_file_5'])){
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_5']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['print_file_5']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-print-5".".".$ext ;
						$data['print_file_5'] = $attachfilename;                    
						if( move_uploaded_file ($files['print_file_5']['tmp_name'], DIR_FS_REPOSITORY_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_REPOSITORY_FILES."/".$attachfilename,0777);
						   $sql_print_file5 = ",". TABLE_REPOSITORY .".print_file_5 = '". $data['print_file_5'] ."'" ;
						}
					}else{
						if(isset($data['delete_print_file_5']) && $data['delete_print_file_5']==1){
							$data['print_file_5']='';
							@unlink(DIR_FS_REPOSITORY_FILES."/".$data['old_print_file_5']);
							$sql_print_file5 = ",". TABLE_REPOSITORY .".print_file_5 = '". $data['print_file_5'] ."'" ;
						}					
					}
					
					
					
                    $query  = " UPDATE ".TABLE_REPOSITORY
							." SET ".TABLE_REPOSITORY .".filename = '". $data['filename'] ."'"        
							.",". TABLE_REPOSITORY .".type 	= '". $data['type'] ."'"
							.",". TABLE_REPOSITORY .".doc_of 	= '". $data['doc_of'] ."'"
							.",". TABLE_REPOSITORY .".subject 	= '". $data['subject'] ."'"
                            .",". TABLE_REPOSITORY .".text 		  = '". $data['text'] ."'"        
                            .",". TABLE_REPOSITORY .".description = '". $data['description'] ."'"
                            .$sql_file1
                            .$sql_file2
                            .$sql_file3
                            .$sql_file4
                            .$sql_file5
							.$sql_print_file1
                            .$sql_print_file2
                            .$sql_print_file3
                            .$sql_print_file4
                            .$sql_print_file5
                            .",". TABLE_REPOSITORY .".updated_by = 	  '".$my['user_id']."'"
                            .",". TABLE_REPOSITORY .".updated_by_name = '".$my['f_name']." ".$my['l_name'] ."'"
                            .",". TABLE_REPOSITORY .".do_e = '".date('Y-m-d H:i:s') ."'"                       
                        ." WHERE id = '". $id ."'";
                   
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL; 
				$fields = TABLE_REPOSITORY .'.*'  ;            
				$condition_query = " WHERE (". TABLE_REPOSITORY .".id = '". $id ."' )";            
				if ( Repository::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
					$_ALL_POST = $_ALL_POST['0'];  
				} 
            }
        }
        else {
            // Read the record which is to be Updated.
           
            $fields = TABLE_REPOSITORY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_REPOSITORY .".id = '". $id ."' )";            
            if ( Repository::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];  
				$_ALL_POST['old_file_1']=$_ALL_POST['file_1'];
                $_ALL_POST['old_file_2']=$_ALL_POST['file_2'];
                $_ALL_POST['old_file_3']=$_ALL_POST['file_3'];
                $_ALL_POST['old_file_4']=$_ALL_POST['file_4'];
                $_ALL_POST['old_file_5']=$_ALL_POST['file_5'];
            }else{ 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/repository-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
      
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');    
			$page["var"][] = array('variable' => 'lst_docof', 'value' => 'lst_docof');     
            $page["var"][] = array('variable' => 'lst_type', 'value' => 'lst_type'); 
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'repository-edit.html');
        }
    }
    else {
        
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
