<?php

    if ( $perm->has('nc_site_menu_list') ) {
	    
        $site_id	= isset($_GET["site_id"]) 	? $_GET["site_id"]	: ( isset($_POST["site_id"]) 	? $_POST["site_id"] : SITE_ID );
		$status		= isset($_GET["status"]) 	? $_GET["status"]	: ( isset($_POST["status"]) 	? $_POST["status"] 	: '' );
	
		//read Websites
		$list_sites		= NULL;
		getAllSites( $db, $list_sites);
		
		$menu_status['key'] 	= MenuSetting::getStatus();
		$menu_status['value'] 	= MenuSetting::getDisplayStatus();
		
        if ( !isset($condition_query) || $condition_query == '' ) {
			//$condition_query = " WHERE " . TABLE_MENU . ".site_id = '$site_id' ";

			//$condition_query =" ORDER BY  " . TABLE_MENU . ".placement ASC, group_id ASC, menu_order ASC, title ASC ";
			 $condition_query = " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
		}
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched'] = 1;
        // To count total records.
        $list	= 	NULL;
        $total	=	MenuSetting::getDisplayList( $db, $list, '', $condition_query);
    
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
       
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields ='*';
        MenuSetting::getDisplayList( $db, $list, $fields , $condition_query, $next_record, $rpp);
        
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_menu_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_menu_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_menu_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_menu_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_menu_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_menu_status') ) {
            $variables['can_change_status']     = true;
        }
		$page["var"][] = array('variable' => 'list', 'value' => 'list');
		$page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
		//$page["var"][] = array('variable' => 'extra', 'value' => 'extra');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
		$page["var"][] = array('variable' => 'list_sites', 'value' => 'list_sites');
		$page["var"][] = array('variable' => 'menu_status', 'value' => 'menu_status');
		$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'menu-settings-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>