<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_ue_edit') ) {
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
    
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
    
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new Region();
        $phone          = new Phone(TABLE_USER);
        $reminder       = new UserReminder(TABLE_USER);
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_ue_edit_al') ) {
            $access_level += 1;
        }
        $al_list    = getAccessLevel($db, $access_level);
        $role_list  = User::getRoles($db, $access_level);
		$groups_list  = User::getGroups();
        //echo $_SERVER['REMOTE_ADDR'];
        
        //include_once ( DIR_FS_INCLUDES .'/account.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/department.inc.php' );
        /*
		$account_list    = NULL;
        $condition_queryacount= " WHERE status = '". ACTIVE ."' ORDER BY name ASC ";
        Account::getList($db, $account_list, 'id,name', $condition_queryacount);
		*/
        
        $company_list    = NULL;
        $condition_querycomp= "WHERE status = '". ACTIVE ."' ORDER BY name ASC";
        Company::getList($db, $company_list, 'id,name', $condition_querycomp);
        
        $department_list    = NULL;
        $condition_querydept= "WHERE status = '". ACTIVE ."' ORDER BY department_name ASC";
        Department::getList($db, $department_list, 'id,department_name', $condition_querydept);
        $groups_list  = User::getGroups();
        //BOF read the available industries
		User::getCountryCode($db,$country_code);
        //EOF read the available industries

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);
			$files       = processUserData($_FILES);
			
            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder,
							'files'     => &$files
                        );
            $data['files'] = $files ;
            $allowed_photo_types=array( "image/jpeg",												
									  "image/pjpeg");
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['allowed_photo_types'] = $allowed_photo_types ;
            $data['max_file_size'] = 1024; // 1024 *1024 ; 1MB
            $data['max_photo_size'] = 50; // 1024 *4 ; 50kb
            
			
            if( User::validateUpdate($data, $extra) ) {
            
                 $reporting_members_str = $reporting_members_str1 = $report_to_str = $report_to_str1 = $jobp_str =$jobp_str1 ='';
            
                if(!empty($data['report_to'])) {
                    $report_to_str1=implode(",",$data['report_to']);
                    $report_to_str=",".$report_to_str1.",";
                }
				if(!empty($data['reporting_members'])){
                    $reporting_members_str1=implode(",",$data['reporting_members']);
                    $reporting_members_str=",".$reporting_members_str1.",";
                }
				if(!empty($data['task_members'])){
                    $task_members_str1=implode(",",$data['task_members']);
                    $task_members_str=",".$task_members_str1.",";
                }
				if(!empty($data['jobp'])) {
                    $jobp_str1=implode(",",$data['jobp']);
                    $jobp_str=",".$jobp_str1.",";
                }
				$groupsStr='';
				if(!empty($data['groups'])) {
                    $groups1=implode(",",$data['groups']);
                    $groupsStr = ",".$groups1.",";
                }
				
				
                $resume_attachment = '';
				$resume_file_sql = '';
				if(!empty($files['resume_attachment']["name"])){	
					
					$filedata["extension"] = '';
					$filedata = pathinfo($files['resume_attachment']["name"]);
					$ext = $filedata["extension"] ;  
					//$attachfilenamechq = $username."-".mktime().".".$ext ;                       
					$resume_attachment = $data['number'].".".$ext ;
					$data['resume_attachment'] = $resume_attachment;                        
					$attachment_path = DIR_WS_TEAMRESUME_FILES;
					if(file_exists(DIR_FS_TEAMRESUME_FILES."/".$data['old_resume_attachment']))
					{
						@unlink( DIR_FS_TEAMRESUME_FILES."/".$data['old_resume_attachment']);
					}
					
					if(move_uploaded_file($files['resume_attachment']['tmp_name'], 							DIR_FS_TEAMRESUME_FILES."/".$resume_attachment)){
						@chmod(DIR_FS_TEAMRESUME_FILES."/".$resume_attachment, 0777);
						 $resume_file_sql = ",". TABLE_USER .".resume_attachment = 	'". $resume_attachment ."'";	
					}else{
						$resume_file_sql = ",". TABLE_USER .".resume_attachment = ''";
					}
				}else{
					$data['resume_attachment']=$data['old_resume_attachment'];
					if(isset($data['delete_resume_attachment'])){
						if(file_exists(DIR_FS_TEAMRESUME_FILES."/".$data['old_resume_attachment'])){
							@unlink( DIR_FS_TEAMRESUME_FILES."/".$data['old_resume_attachment']);
						}
						$resume_file_sql = ",". TABLE_USER .".resume_attachment = ''";
						$data['resume_attachment']='';
					}
				}
				
				$team_photo = '';
				$photo_sql = '';
				if(!empty($files['team_photo']["name"])){	
					
					$filedata["extension"] = '';
					$filedata = pathinfo($files['team_photo']["name"]);
					$ext = $filedata["extension"] ;  
					//$attachfilenamechq = $username."-".mktime().".".$ext ;                       
					$team_photo = $data['number'].".".$ext ;
					                  
					$attachment_path = DIR_WS_TEAMPHOTO_FILES;
					if(file_exists(DIR_FS_TEAMPHOTO_FILES."/".$data['old_team_photo']))
					{
						@unlink( DIR_FS_TEAMPHOTO_FILES."/".$data['old_team_photo']);
					}
					
					if(move_uploaded_file($files['team_photo']['tmp_name'], 
					DIR_FS_TEAMPHOTO_FILES."/".$team_photo)){
						@chmod(DIR_FS_TEAMPHOTO_FILES."/".$team_photo, 0777);
						 $photo_sql = ",". TABLE_USER .".team_photo = '". $team_photo ."'";	
					}else{
						$photo_sql = ",". TABLE_USER .".team_photo = ''";
					}
				}else{
					
					if(isset($data['delete_team_photo'])){
						if(file_exists(DIR_FS_TEAMPHOTO_FILES."/".$data['old_team_photo'])){
							@unlink( DIR_FS_TEAMPHOTO_FILES."/".$data['old_team_photo']);
						}
						$photo_sql = ",". TABLE_USER .".team_photo = ''";
						
					}
				}

                $query  = " UPDATE ". TABLE_USER
                        ." SET ". TABLE_USER .".user_id     = '". $data['user_id'] ."'"
                            .",". TABLE_USER .".number      = '". $data['number'] ."'"
                            .",". TABLE_USER .".username    = '". $data['username'] ."'"
                            . $data['password']
							.$resume_file_sql
							.$photo_sql
                            .",". TABLE_USER .".access_level= '". $data['access_level'] ."'"
                            .",". TABLE_USER .".allow_ip       = '". $data['allow_ip'] ."'"
                            .",". TABLE_USER .".valid_ip       = '". $data['valid_ip'] ."'"
                            .",". TABLE_USER .".roles       = '". $data['roles'] ."'"
                            .",". TABLE_USER .".do_join     = '". $data['do_join'] ."'"
                            .",". TABLE_USER .".do_resign   = '". $data['do_resign'] ."'"
							.",". TABLE_USER .".current_salary  = '". $data['current_salary'] ."'"
                            .",". TABLE_USER .".email       = '". $data['email'] ."'"
							.",". TABLE_USER .".email_password  = '". $data['email_password'] ."'"
                            .",". TABLE_USER .".email_1     = '". $data['email_1'] ."'"
                            .",". TABLE_USER .".email_2     = '". $data['email_2'] ."'"
                            .",". TABLE_USER .".title       = '". $data['title'] ."'"
                            .",". TABLE_USER .".f_name      = '". $data['f_name'] ."'"
                            .",". TABLE_USER .".m_name      = '". $data['m_name'] ."'"
                            .",". TABLE_USER .".l_name      = '". $data['l_name'] ."'"
                            .",". TABLE_USER .".p_name      = '". $data['p_name'] ."'"
							.",". TABLE_USER .".mobile1     = '". $data['mobile1'] ."'"
                            .",". TABLE_USER .".mobile2     = '". $data['mobile2'] ."'"
							.",". TABLE_USER .".marketing_contact = '". $data['marketing_contact'] ."'"
							.",". TABLE_USER .".mkt_person_cron = '". $data['mkt_person_cron'] ."'"
                            //.",". TABLE_USER .".account_id         = '". $data['account_id'] ."'"
                            .",". TABLE_USER .".company_id  		 = '". $data['company_id'] ."'"
                            .",". TABLE_USER .".report_to   		 = '". $report_to_str ."'"
                            .",". TABLE_USER .".my_reporting_members = '". $reporting_members_str ."'"
                            .",". TABLE_USER .".task_members = '". $task_members_str ."'"
                            .",". TABLE_USER .".jobp   				 = '". $jobp_str ."'"
							 .",". TABLE_USER .".groups  	= '". $groupsStr ."'"
                            .",". TABLE_USER .".department  = '". $data['department'] ."'"
							.",". TABLE_USER .".qualification  = '". $data['qualification'] ."'"
                            .",". TABLE_USER .".desig       = '". $data['desig'] ."'"
                            .",". TABLE_USER .".org         = '". $data['org'] ."'"
                            .",". TABLE_USER .".domain      = '". $data['domain'] ."'"
                            .",". TABLE_USER .".gender      = '". $data['gender'] ."'"
                            .",". TABLE_USER .".do_birth    = '". $data['do_birth'] ."'"
                            .",". TABLE_USER .".do_aniv     = '". $data['do_aniv'] ."'"
                            .",". TABLE_USER .".remarks     = '". $data['remarks'] ."'"
                            .",". TABLE_USER .".status      = '". $data['status'] ."'" 
                            .",". TABLE_USER .".current_staff  = '". $data['current_staff'] ."'" 
                        ." WHERE ". TABLE_USER .".user_id   = '". $data['user_id'] ."'";
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['user_id'];

                    // Update the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        if ( isset($data['a_remove'][$i]) && $data['a_remove'][$i] == '1') {
                            if ( ! $region->delete($db, $data['address_id'][$i], $variables['hid'], TABLE_USER) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setOkMessage($errors['description']);
                                }
                            }
                        }
                        else {
                             $address_arr = array(   'id'            => isset($data['address_id'][$i])? $data['address_id'][$i] : '0',
                                                    'address_type'  => $data['address_type'][$i],
                                                    'company_name'  => $data['company_name'][$i],
                                                    'address'       => $data['address'][$i],
                                                    'city'          => $data['city'][$i],
                                                    'state'         => $data['state'][$i],
                                                    'country'       => $data['country'][$i],
                                                    'zipcode'       => $data['zipcode'][$i],
                                                    'is_preferred'  => isset($data['is_preferred'][$i])? $data['is_preferred'][$i] : '0',
                                                    'is_verified'   => isset($data['is_verified'][$i])? $data['is_verified'][$i] : '0'
                                                );
                            
                            if ( !$region->update($variables['hid'], TABLE_USER, $address_arr) ) {
                                foreach ( $region->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_USER) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $phone_arr = array( 'id' => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
													'p_type'        => $data['p_type'][$i],
													'cc'            => $data['cc'][$i],
													'ac'            => $data['ac'][$i],
													'p_number'      => $data['p_number'][$i],
													'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
													'p_is_verified' => isset($data['p_is_verified'][$i]) ? $data['p_is_verified'][$i] : '0'
                                                );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_USER, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Update the Reminders.
                    for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['remind_date_'.$i]) ) {
                            if ( isset($data['r_remove_'.$i]) && $data['r_remove_'.$i] == '1') {
                                if ( ! $reminder->delete($db, $data['remind_id_'.$i], $variables['hid'], TABLE_USER) ) {
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $reminder_arr = array(  'id'            => (isset($data['remind_id_'.$i])? $data['remind_id_'.$i] : ''),
                                                        'do_reminder'   => $data['remind_date_'.$i],
                                                        'description'   => $data['remind_text_'.$i]
                                                    );
                                if ( ! $reminder->update($db, $variables['hid'], TABLE_USER, $reminder_arr) ) {
                                    $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Send The Email to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_ue_add_notif', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_ue_add_notif_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link']       = DIR_WS_NC .'/user.php?perform=view_details&user_id='. $data['user_id'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'EXECUTIVE_INFO_UPDATE_MANAGERS', $data, $email) ) {
                            $to = '';
                            $to[] = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        
                    }
                    
                    // Send Email to the newly added Executive.
                    $data['link']       = DIR_WS_NC;
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'EXECUTIVE_INFO_UPDATE', $data, $email) ) {
                        $to = '';
                        $to[] = array('name' => $data['f_name'] .' '. $data['l_name'], 'email' => $data['email']);
                        $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    $messages->setOkMessage("Executives Profile has been updated.");
                    
					//get the data to display bof
					$condition_query= " WHERE user_id = '". $user_id ."' ";
					$_ALL_POST      = NULL;
					if ( User::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
						$_ALL_POST = $_ALL_POST[0];
						
						// BO: Read the Report To Members Information.
						if ( !empty($_ALL_POST['report_to']) && !is_array($_ALL_POST['report_to']) ) {
							//include ( DIR_FS_INCLUDES .'/user.inc.php');
							$_ALL_POST['report_to']          = explode(',', $_ALL_POST['report_to']);
							$temp                       = "'". implode("','", $_ALL_POST['report_to']) ."'";
							$_ALL_POST['report_to_members']  = '';
							$_ALL_POST['team_details']  = array();
							User::getList($db, $_ALL_POST['report_to_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
							$_ALL_POST['report_to'] = array();
							foreach ( $_ALL_POST['report_to_members'] as $key=>$members) {
							//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
								$_ALL_POST['report_to'][] = $members['user_id'];
								$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
							}
						}
						// EO: Read the Report To Members Information.
						 
						 
						// BO: Read the Report To Members Information.
						if ( !empty($_ALL_POST['my_reporting_members']) && !is_array($_ALL_POST['my_reporting_members']) ) {
							//include ( DIR_FS_INCLUDES .'/user.inc.php');
							$_ALL_POST['reporting_members']  = explode(',', $_ALL_POST['my_reporting_members']);
							$temp                       = "'". implode("','", $_ALL_POST['reporting_members']) ."'";
							$_ALL_POST['reporting_members1']  = '';
							$_ALL_POST['reporting_members_details']  = array();
							User::getList($db, $_ALL_POST['reporting_members1'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
							$_ALL_POST['reporting_members'] = array();
							foreach ( $_ALL_POST['reporting_members1'] as $key=>$members){
							//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
								$_ALL_POST['reporting_members'][] = $members['user_id'];
								$_ALL_POST['reporting_members_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
							}
						}
						// BO: Read the Task Members .
						if ( !empty($_ALL_POST['task_members']) && !is_array($_ALL_POST['task_members']) ) {
							//include ( DIR_FS_INCLUDES .'/user.inc.php');
							$_ALL_POST['task_members_a']  = explode(',', $_ALL_POST['task_members']);
							$temp                       = "'". implode("','", $_ALL_POST['task_members_a']) ."'";
							$_ALL_POST['task_members1']  = '';
							$_ALL_POST['task_members_details']  = array();
							User::getList($db, $_ALL_POST['task_members1'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
							$_ALL_POST['task_members'] = array();
							foreach ( $_ALL_POST['task_members1'] as $key=>$members){
							//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
								$_ALL_POST['task_members'][] = $members['user_id'];
								$_ALL_POST['task_members_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
							}
						}
						
						
						
						// EO: Read the Report To Members Information.
						if ( !empty($_ALL_POST['jobp']) && !is_array($_ALL_POST['jobp']) ) {
							//include ( DIR_FS_INCLUDES .'/user.inc.php');
							$_ALL_POST['jobp']          = explode(',', $_ALL_POST['jobp']);
							$temp                       = "'". implode("','", $_ALL_POST['jobp']) ."'";
							$_ALL_POST['jobp_details1']  = '';
							$_ALL_POST['jobp_details']  = array();
							
							$condition_query2= " LEFT JOIN ".TABLE_TJ_FUNCTIONS." ON 
							".TABLE_TJ_FUNCTIONS_P.".tjf_id =".TABLE_TJ_FUNCTIONS.".id 
							WHERE ".TABLE_TJ_FUNCTIONS_P.".id IN(".$temp.")";
							$jfields = TABLE_TJ_FUNCTIONS.".title,".TABLE_TJ_FUNCTIONS_P.".id," 
							.TABLE_TJ_FUNCTIONS_P.".jtitle as ptitle ";
							 
							TeamJobFunctions::getParticulars($db, $_ALL_POST['jobp_details1'], 
							$jfields, $condition_query2 );
							foreach ( $_ALL_POST['jobp_details1'] as $key=>$jobp){							 
								$_ALL_POST['jobp'][] = $jobp['id'];
								$_ALL_POST['jobp_details'][] = $jobp['ptitle'] .' ('. $jobp['title'] .')';
							}
						}
						$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
						$_ALL_POST['groups'] = explode(',', $_ALL_POST['groups']);
						
						// Check the Access Level.
						if ( $_ALL_POST['access_level'] < $access_level ) {
							// Read the Contact Numbers.
							$phone->setPhoneOf(TABLE_USER, $user_id);
							$_ALL_POST['phone'] = $phone->get($db);
							
							// Read the Addresses.
							$region->setAddressOf(TABLE_USER, $user_id);
							$_ALL_POST['address_list'] = $region->get();
							
							// Read the Reminders.
							$reminder->setReminderOf(TABLE_USER, $user_id);
							$_ALL_POST['reminder_list'] = $reminder->get($db);
						}
					}
					//get the data to display eof					
                    //to flush the data.
                    //$data       = NULL;
                    
                }
                else {
                    $messages->setErrorMessage('Executives Profile was not updated.');
                }
            }
            $_ALL_POST  = $data;
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/user-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  
						'id'  => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
						'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
						'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
						'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
						'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
						'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
						'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                        );
//                    }
                }
                
                // Preserve the Addresses.
                $count = count($_ALL_POST['city']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['address_list'][] = array(   'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'company_name'   => isset($_ALL_POST['company_name'][$i])? $_ALL_POST['company_name'][$i]:'',
                                                                'is_verified'   => isset($_ALL_POST['is_verified'][$i])? $_ALL_POST['is_verified'][$i]:'',
                                                                'is_preferred'  => isset($_ALL_POST['is_preferred'][$i])? $_ALL_POST['is_preferred'][$i]:'',
                                                                'address_type'  => isset($_ALL_POST['address_type'][$i])? $_ALL_POST['address_type'][$i]:'',
                                                                'address'       => isset($_ALL_POST['address'][$i])? $_ALL_POST['address'][$i]:'',
                                                                'city'          => isset($_ALL_POST['city'][$i])? $_ALL_POST['city'][$i]:'',
                                                                'state'         => isset($_ALL_POST['state'][$i])? $_ALL_POST['state'][$i]:'',
                                                                'zipcode'       => isset($_ALL_POST['zipcode'][$i])? $_ALL_POST['zipcode'][$i]:'',
                                                                'country'       => isset($_ALL_POST['country'][$i])? $_ALL_POST['country'][$i]:'',
                                                     );
//                    }
                }

                
                // Preserve the Reminders.
                $count = $_ALL_POST['reminder_count'];
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['reminder_list'][] = array(  'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'do_reminder'   => isset($_ALL_POST['remind_date_'.$i])? $_ALL_POST['remind_date_'.$i]:'',
                                                                'description'   => isset($_ALL_POST['remind_text_'.$i])? $_ALL_POST['remind_text_'.$i]:'',
                                                     );
//                    }
                }
                
                // To Suppress the undefined index warning.
                if ( !isset($_ALL_POST['reminder_list']) ) {
                    $_ALL_POST['reminder_list'] = '';
                }
            }
            else {
                // No error was generated, read the Executive data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( User::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                    
                    // BO: Read the Report To Members Information.
					if ( !empty($_ALL_POST['report_to']) && !is_array($_ALL_POST['report_to']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						$_ALL_POST['report_to']          = explode(',', $_ALL_POST['report_to']);
						$temp                       = "'". implode("','", $_ALL_POST['report_to']) ."'";
						$_ALL_POST['report_to_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['report_to_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['report_to'] = array();
						foreach ( $_ALL_POST['report_to_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['report_to'][] = $members['user_id'];
							$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}
					// EO: Read the Report To Members Information.
					// BO: Read the Report To Members Information.
					if ( !empty($_ALL_POST['my_reporting_members']) && !is_array($_ALL_POST['my_reporting_members']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						$_ALL_POST['reporting_members']  = explode(',', $_ALL_POST['my_reporting_members']);
						$temp                       = "'". implode("','", $_ALL_POST['reporting_members']) ."'";
						$_ALL_POST['reporting_members1']  = '';
						$_ALL_POST['reporting_members_details']  = array();
						User::getList($db, $_ALL_POST['reporting_members1'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['reporting_members'] = array();
						foreach ( $_ALL_POST['reporting_members1'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['reporting_members'][] = $members['user_id'];
							$_ALL_POST['reporting_members_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}
					// EO: Read the Report To Members Information.
					if ( !empty($_ALL_POST['task_members']) && !is_array($_ALL_POST['task_members']) ) {
							//include ( DIR_FS_INCLUDES .'/user.inc.php');
							$_ALL_POST['task_members_a']  = explode(',', $_ALL_POST['task_members']);
							$temp                       = "'". implode("','", $_ALL_POST['task_members_a']) ."'";
							$_ALL_POST['task_members1']  = '';
							$_ALL_POST['task_members_details']  = array();
							User::getList($db, $_ALL_POST['task_members1'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
							$_ALL_POST['task_members'] = array();
							foreach ( $_ALL_POST['task_members1'] as $key=>$members){
							//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
								$_ALL_POST['task_members'][] = $members['user_id'];
								$_ALL_POST['task_members_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
							}
						}
                    if ( !empty($_ALL_POST['jobp']) && !is_array($_ALL_POST['jobp']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						$_ALL_POST['jobp']          = explode(',', $_ALL_POST['jobp']);
						$temp                       = "'". implode("','", $_ALL_POST['jobp']) ."'";
						$_ALL_POST['jobp_details1']  = '';
						$_ALL_POST['jobp_details']  = array();
						
						$condition_query2= " LEFT JOIN ".TABLE_TJ_FUNCTIONS." ON 
						".TABLE_TJ_FUNCTIONS_P.".tjf_id =".TABLE_TJ_FUNCTIONS.".id 
						WHERE ".TABLE_TJ_FUNCTIONS_P.".id IN(".$temp.")";
						$jfields = TABLE_TJ_FUNCTIONS.".title,".TABLE_TJ_FUNCTIONS_P.".id," 
						.TABLE_TJ_FUNCTIONS_P.".jtitle as ptitle ";
						 
						TeamJobFunctions::getParticulars($db, $_ALL_POST['jobp_details1'], 
						$jfields, $condition_query2 );
						
						foreach ( $_ALL_POST['jobp_details1'] as $key=>$jobp) {
						 
							$_ALL_POST['jobp'][] = $jobp['id'];
							$_ALL_POST['jobp_details'][] = $jobp['ptitle'] .' ('. $jobp['title'] .' )';
						}
					}
                    $_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    $_ALL_POST['groups'] = explode(',', $_ALL_POST['groups']);
					
                    // Check the Access Level.
                    if ( $_ALL_POST['access_level'] < $access_level ) {
                        // Read the Contact Numbers.
                        $phone->setPhoneOf(TABLE_USER, $user_id);
                        $_ALL_POST['phone'] = $phone->get($db);
                        
                        // Read the Addresses.
                        $region->setAddressOf(TABLE_USER, $user_id);
                        $_ALL_POST['address_list'] = $region->get();
                        
                        // Read the Reminders.
                        $reminder->setReminderOf(TABLE_USER, $user_id);
                        $_ALL_POST['reminder_list'] = $reminder->get($db);
                    }
                }
            }

            if ( !empty($_ALL_POST['user_id']) ) {

                // Check the Access Level.
                if ( $_ALL_POST['access_level'] < $access_level ) {
                    
                    $phone_types    = $phone->getTypeList();
                    $address_types  = $region->getTypeList();
                    $lst_country    = $region->getCountryList();
                    $lst_state      = $region->getStateList();
                    $lst_city       = $region->getCityList();
					
                    $region  =null;
					$phone  =null; 
					$reminder=null;
					 
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    $variables['team_photo_url'] = DIR_WS_TEAMPHOTO_FILES;
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    $hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    $page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    $page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
					$page["var"][] = array('variable' => 'groups_list', 'value' => 'groups_list');
                    $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
                    $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
                    $page["var"][] = array('variable' => 'allowipArr', 'value' => 'allowipArr');
                    //$page["var"][] = array('variable' => 'account_list', 'value' => 'account_list');
                    $page["var"][] = array('variable' => 'company_list', 'value' => 'company_list');
                    $page["var"][] = array('variable' => 'department_list', 'value' => 'department_list');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-edit.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Executives with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected Executive was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>