<?php
    if ( $perm->has('nc_hd_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_hd_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/hardware.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Hardware::validateUpdate($data, $extra) ) {
                $query  = " UPDATE ". TABLE_HARDWARE_ENTRY
                         ." SET ". TABLE_HARDWARE_ENTRY .".customer_name = '".       $data['customer_name'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".material = '".            $data['material'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".fault = '".               $data['fault'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".bill_no = '".             $data['bill_no'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".charge = '".              $data['charge'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".payment_remark = '".      $data['payment_remark'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".out_date = '".            $data['out_date'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".payment = '".             $data['payment'] ."'"
                            ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Hardware entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the visitor which is to be Updated.
            $fields = TABLE_HARDWARE_ENTRY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_HARDWARE_ENTRY .".id = '". $id ."' )";
            /*$condition_query .= " AND ( ";
            // If my has created this Order.
            $condition_query .= " (". TABLE_HARDWARE_ENTRY .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_HARDWARE_ENTRY .".access_level < $access_level ) ";              
            // Check if the User has the Right to Edit Orders created by other Users.
            /*
            if ( $perm->has('nc_bl_or_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_bl_or_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_VISITOR_BOOK. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_VISITOR_BOOK .".access_level < $access_level_o ) ";
            }*/
           // $condition_query .= " )";
          
            if ( Hardware::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                //if ( $_ALL_POST['access_level'] < $access_level ) {
                
                    $id         = $_ALL_POST['id'];
                     // Setup the date of delivery.
                    $_ALL_POST['out_date']  = explode(' ', $_ALL_POST['out_date']);
                    $temp               = explode('-', $_ALL_POST['out_date'][0]);
                    $_ALL_POST['out_date']  = NULL;
                    $_ALL_POST['out_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                /*}
                else {
                    $messages->setErrorMessage("You do not have the Permission to view the Record with the current Access Level.");
                }*/
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/hardware-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hardware-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
