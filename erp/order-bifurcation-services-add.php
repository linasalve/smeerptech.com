<?php
  if ( $perm->has('nc_or_bs_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		// Include the payment party class
		//include_once (DIR_FS_INCLUDES .'/quotation-subject.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( OrderBifurcationServices::validateAdd($data, $extra) ) { 
                            $query	= " INSERT INTO ".TABLE_ORD_BIFURCATION_SERVICES
                                    ." SET ".TABLE_ORD_BIFURCATION_SERVICES .".title     = '". $data['title'] ."'"                                
                                	.",". TABLE_ORD_BIFURCATION_SERVICES .".status      = '". $data['status'] ."'"
                                    .",". TABLE_ORD_BIFURCATION_SERVICES .".created_by      = '". $my['user_id'] ."'"
                                    .",". TABLE_ORD_BIFURCATION_SERVICES .".do_e      = '". date('Y-m-d') ."'"
                                    .",". TABLE_ORD_BIFURCATION_SERVICES .".ip      = '". $_SERVER['REMOTE_ADDR'] ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New ticket status entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/order-bifurcation-services.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/order-bifurcation-services.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
           
            //include ( DIR_FS_NC .'/payment-transaction-list.php');
            header("Location:".DIR_WS_NC."/order-bifurcation-services.php?added=1");
            
        }else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'order-bifurcation-services-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
