<?php

    if ( $perm->has('nc_sms_ca_list') ) {
        @$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
		$required_fields = "ca_id,ca_userid,ca_displayname,(SELECT SUM(cah_noofsms) FROM ".TABLE_SMS_CLIENT_ACC_HISTORY." WHERE cah_caid=ca_id) as ca_allotedsms,ca_consumedsms,ca_balance,ca_status,ca_posturl";
        $total	=	ClientAccount::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        ClientAccount::getList( $db, $list, $required_fields, $condition_query, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_sms_ca_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_sms_ca_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_sms_ca_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_sms_ca_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_sms_ca_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_sms_ca_status') ) {
            $variables['can_change_status']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'client-account-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>