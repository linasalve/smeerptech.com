<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_myprofile_password') ) {
        $user_id = $my['user_id'];

        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        $access_level   += 1;
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages
                        );

            if ( Myprofile::validateProfile($data, $extra) ) {
                $query  = " UPDATE ". TABLE_USER
                        ." SET ". $data['newpassword']
                        ." WHERE ". TABLE_USER .".user_id   = '". $data['user_id'] ."'";
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['user_id'];

                    $messages->setOkMessage("Your password has been changed.");
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Cannot change password.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/user-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
            }
            else {
                // No error was generated, read the Executive data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( User::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                }
            }

            if ( !empty($_ALL_POST['user_id']) ) {

                // Check the Access Level.
                if ( $_ALL_POST['access_level'] < $access_level ) {

                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'change_password');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'change-password.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Change the Password with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected User was not found to Change the Password.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You do not have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>