<?php
if ( $perm->has('nc_sl_e_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
       	include_once (DIR_FS_INCLUDES .'/sale-lead-enquiry.inc.php');	
     
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
                            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                            );
            
              
            
            if ( SaleLeadEnquiry::validateUpdate($data, $extra) ) {
                
                $query  = " UPDATE ".TABLE_SALE_LEADS_ENQUIRY
						 ." SET ".TABLE_SALE_LEADS_ENQUIRY .".lead_id = '". $data['lead_id'] ."'"                                     
                            .",". TABLE_SALE_LEADS_ENQUIRY .".lead_details = '". $data['lead_details'] ."'"
                            .",". TABLE_SALE_LEADS_ENQUIRY .".title = '". $data['title'] ."'"
                            .",". TABLE_SALE_LEADS_ENQUIRY .".description = '". $data['description'] ."'"			                          
						    .",". TABLE_SALE_LEADS_ENQUIRY .".updated_by = '".     $my['user_id'] ."'"
                            .",". TABLE_SALE_LEADS_ENQUIRY .".updated_by_name = '".$my['f_name']." ".$my['l_name'] ."'"
                            .",". TABLE_SALE_LEADS_ENQUIRY  .".status = '".$data['status'] ."'"                             
                        ." WHERE id = '". $id ."'";
                    
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
           
            $fields = TABLE_SALE_LEADS_ENQUIRY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SALE_LEADS_ENQUIRY .".id = '". $id ."' )";
            
            if ( SaleLeadEnquiry::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST['0'];                                 
                   
                        
            }else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
            
               
            
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/sale-lead-enquiry-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
      
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');    
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-enquiry-edit.html');
        }
    }
    else {
        
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
