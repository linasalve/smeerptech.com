<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN')){
		require("../lib/config.php");
	}    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));    
    include_once(DIR_FS_NC ."/header.php");
    include_once(DIR_FS_INCLUDES .'/project-task.inc.php');
    include_once(DIR_FS_INCLUDES .'/bill-order.inc.php');
	//include_once(DIR_FS_INCLUDES .'/project-status.inc.php');
	include_once(DIR_FS_INCLUDES .'/clients.inc.php'); 
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0    
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$or_id 		= isset($_GET["or_id"])     ? $_GET["or_id"]        : ( isset($_POST["or_id"])          ? $_POST["or_id"]       :'');
	$client_id 	= isset($_GET["client_id"]) ? $_GET["client_id"]        : ( isset($_POST["client_id"])          ? $_POST["client_id"]       :'');
	$id 		 = isset($_GET["id"])       ? $_GET["id"]        : ( isset($_POST["id"])          ? $_POST["id"]       :'');
    $added 			= isset($_GET["added"]) ? $_GET["added"]  : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $flw_added 			= isset($_GET["flw_added"]) ? $_GET["flw_added"]  : ( isset($_POST["flw_added"])          ? $_POST["flw_added"]       :'0');
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    //$xd 			= isset($_GET["xd"])    ? $_GET["xd"]        : ( isset($_POST["xd"])          ? $_POST["xd"]       :'');
	$rpp = isset($_GET["rpp"]) ? $_GET["rpp"]:( isset($_POST["rpp"]) ? $_POST["rpp"]:( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    // ie by default every one can show applied ( * by factor) value timesteet values   
	if ( $perm->has('nc_p_ts_default_mult') ) { //show default multiplied hrs		 
		$actual_time 	= isset($_GET["actual_time"])       ? $_GET["actual_time"]      : ( isset($_POST["actual_time"])        ? $_POST["actual_time"]     : 0);
	}else{	 
		$actual_time 	= isset($_GET["actual_time"])       ? $_GET["actual_time"]      : ( isset($_POST["actual_time"])        ? $_POST["actual_time"]     : 1);
	}
	// ie by default every one can show Applied ( * by factor) value timesteet values
    //If order id is not set the redirect on home page bof
	
    if(empty($or_id) && empty($client_id )){
       header("Location: ".DIR_WS_NC."/index.php");  
    }
     
    $condition_url='';
    $team_members='';
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
       
    }
    $condition_query ='';
   
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    } else {
        $next_record    = ($x-1) * $rpp;
    }
    
    $variables["x"]     = $x;
    //$variables["xd"]     = $xd;
    $variables["rpp"]   = $rpp; 
    if($added){    
        $messages->setOkMessage("New Project Task entry has been done.");
    }
    if($flw_added){    
        $messages->setOkMessage("Followup sent successfully.");
    }
    $lst_submodule=null;
    //get order details BOF
    $order = NULL;
	
    if(!empty($or_id) ){
        $fields_or= TABLE_BILL_ORDERS.".team, ".TABLE_BILL_ORDERS.".order_title,".TABLE_BILL_ORDERS.".id as order_id,
        ".TABLE_BILL_ORDERS.".clients_su,
        ".TABLE_BILL_ORDERS.".number,
		".TABLE_BILL_ORDERS.".wp_support_team,
		".TABLE_BILL_ORDERS.".order_title_mail,
        ".TABLE_BILL_ORDERS.".st_date,
        ".TABLE_BILL_ORDERS.".es_ed_date,
        ".TABLE_BILL_ORDERS.".ac_ed_date, 
        ".TABLE_BILL_ORDERS.".do_o,
        ".TABLE_BILL_ORDERS.".client, 
		".TABLE_BILL_ORDERS.".status, 
        ".TABLE_BILL_ORDERS.".es_hrs, 
        ".TABLE_BILL_ORDERS.".daily_update_internal, 
        ".TABLE_BILL_ORDERS.".daily_update_dt, 
        ".TABLE_BILL_ORDERS.".last_est_completion_dt, 
        ".TABLE_BILL_ORDERS.".last_current_status, 
        ".TABLE_BILL_ORDERS.".last_pending_frm_client,  
		".TABLE_BILL_ORDERS.".incentives, 
		".TABLE_CLIENTS.".f_name,".TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name,
		".TABLE_CLIENTS.".send_mail, ".TABLE_CLIENTS.".additional_email ";
        $condition_or = " LEFT JOIN ".TABLE_CLIENTS." 
         ON ".TABLE_CLIENTS.".user_id = ".TABLE_BILL_ORDERS.".client WHERE ".TABLE_BILL_ORDERS.".id = '".$or_id."'" ;           
        if ( Order::getList($db, $order, $fields_or ,$condition_or ) > 0 ) {	
            include_once ( DIR_FS_INCLUDES.'/user.inc.php');       
            $order = $order[0];            
			$order['wp_support_team'] =	$order['wp_support_team'];	
			$order['incentives'] =	$order['incentives'];
			$order['clients_su'] = "'".implode("','", (explode(',', $order['clients_su']))) ."'";
            $order['team'] = "'".implode("','", (explode(',', $order['team']))) ."'";
            $order['team_members']= '';
            $order['dt_width']= '0';
            $order['dt_color']= '#cccccc';
			$order['dt_text'] = '';
			$order['dt_width_50per'] = 0 ; 
			$order['dt_width'] = 0 ;
			$order['dt_smiley'] = 'run-smiley.gif';
            $order['hr_width']= '0';
            $order['hr_color']= '#cccccc';
			$order['hr_text'] = '';
			$order['hr_width_50per'] = 0 ; 
			$order['hr_width'] = 0 ;
			$order['hr_smiley'] = 'run-smiley.gif';			
			//$order['es_hrs'] = '2011-05-11 00:00:00';
			//$order['es_ed_date'] = '2011-05-11 00:00:00';
			//$order['ac_ed_date'] = '2011-05-13 00:00:00';
			if($actual_time){    
				$order['es_hrs'] =	$order['es_hrs'] ;
			}else{
				$order['es_hrs'] =	$order['es_hrs'] * HRS_FACTOR;
			}
			$order['tot_est_min'] = ( $order['es_hrs'] * 60 ) ;
			
			
			//Date based Calculation BOF
			if($order['es_ed_date']!='0000-00-00 00:00:00'){
			
				$es_ed_dateArr  = explode(' ', $order['es_ed_date']);
                $temp               = explode('-', $es_ed_dateArr[0]); 
				$es_ed_date = mktime(0, 0, 0, $temp[1], $temp[2],$temp[0]); //m,dy
			}
			if($order['st_date']!='0000-00-00 00:00:00'){
			
				$st_dateArr  = explode(' ', $order['st_date']);
                $temp               = explode('-', $st_dateArr[0]);                
				$st_date = mktime(0, 0, 0, $temp[1], $temp[2],$temp[0]);
			}
			$ac_ed_dateFinal = 0;
			if($order['ac_ed_date']!='0000-00-00 00:00:00'){
				$ac_ed_dateArr  = explode(' ', $order['ac_ed_date']);
                $temp       = $ac_ed_date1   = explode('-', $ac_ed_dateArr[0]);                
				$ac_ed_date = $ac_ed_dateFinal = mktime(0, 0, 0, $temp[1], $temp[2],$temp[0]);
			}else{			
				$ac_ed_date =  mktime(0, 0, 0, date("m"), date("d"),date("y"));
			}
				
			$diff_seconds = $es_ed_date - $st_date ;
			$tot_days     = floor($diff_seconds/86400) + 1; //z 60*60*24
			$tot_wdays = 0;
			$cal_ratio = $tot_days/100 ;
			if($ac_ed_date > $st_date){
				$diff_wseconds = $ac_ed_date - $st_date ;
				$tot_wdays     = floor($diff_wseconds/86400) + 1;
			}
			$order['dt_width_50per'] = $tot_days * 0.5 ; 
			$order['dt_width'] = floor($tot_wdays *100)/$tot_days ;
			$delay_days =0;
			$delayStatement ='';
			$order['delayStatement']='';
			if($tot_wdays > $tot_days){
				$delay_days = $tot_wdays - $tot_days;
				$order['delayStatement'] = " Work delayed by ".$delay_days." days";
			}			
			if($order['ac_ed_date']!='0000-00-00 00:00:00'){
				if($order['dt_width'] > $order['dt_width_50per']){
					$order['dt_color']='#cb0e0e';	//danger 	
					$order['dt_text'] = 'Deadline is near';				
				}
				if($order['dt_width']>100){
					$order['dt_width'] = 100;
					$order['dt_text'] = 'Deadline is crossed';		
				}				
				if($ac_ed_dateFinal >$es_ed_date ){
					$order['dt_smiley'] = 'redflag.gif';
					 $order['dt_color'] = 'cb0e0e';
					$order['dt_text'] = 'Late Completed';
				}else{
					$order['dt_smiley'] = 'greenflag.gif';
					 $order['dt_color'] = '13cb0e';
					$order['dt_text'] = 'Successfully Completed ';
				}
			}else{
				if($order['dt_width'] > $order['dt_width_50per']){
					$order['dt_color']='#cb0e0e';	//danger 	
					$order['dt_text'] = 'Deadline is near';				
				}
				if($order['dt_width']>100){
					$order['dt_width'] = 100;
					$order['dt_color']='#cb0e0e';	//danger 	
					$order['dt_text'] = 'Deadline is crossed';	
				}
			}
			//Date based Calculation EOF
			//Hrs based Calculation BOf			 
            if($actual_time){          
                    $fields_tws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin, 
				( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost ' ;
            }else{
                // show atual total time (timesheet * 5 ) total cost
                  $fields_tws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) DIV 60 ))  as totHr ' .', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) % 60 ) as totMin,
				  ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost' ; 
            }
			
            $condition_query_twh = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id !=0 AND 
			".TABLE_PROJECT_TASK_DETAILS.".project_id = '".$order['order_id']."' AND 
			".TABLE_PROJECT_TASK_DETAILS.".is_client ='0'";
            $list_tws	= 	NULL;
          	ProjectTask::getList( $db, $list_tws, $fields_tws, $condition_query_twh);   
            $order['worked_task_cost'] = '0' ;
            
            if(!empty($list_tws[0]['totHr']) || !empty($list_tws[0]['totMin'])){
              $list_tws =$list_tws[0];
              $order['tot_thrs'] = strlen($list_tws['totHr']) == 1 ? '0'.$list_tws['totHr'] : $list_tws['totHr'] ; 
              $order['tot_tmin'] = strlen($list_tws['totMin']) == 1 ? '0'.$list_tws['totMin'] : $list_tws['totMin'] ; 
				$order['worked_task_cost'] = ceil($list_tws['tot_tcost']) ;
				$order['tot_worked_hrs'] = $order['tot_thrs'].":".$order['tot_tmin'] ;
				$order['tot_worked_min'] =$order['tot_thrs'] * 60 + $order['tot_tmin'] ;
            }else{
				$order['tot_thrs'] = '00';
                $order['tot_tmin'] = '00' ;
				$order['worked_task_cost'] = 0 ;
				$order['tot_worked_hrs'] = $order['tot_thrs'].":".$order['tot_tmin'] ;
				$order['tot_worked_min'] = $order['tot_thrs'] * 60 + $order['tot_tmin'] ;
            }
			$order['hr_width_50per'] = $order['tot_est_min'] * 0.5 ; 
			$order['hr_width'] = floor($order['tot_worked_min'] *100) / $order['tot_est_min'] ;
			$order['gain_loss_hrs'] ='';
			if($order['ac_ed_date']!='0000-00-00 00:00:00'){
				if($order['hr_width'] > $order['hr_width_50per']){
					$order['hr_color']='#cb0e0e';	//danger 	
					$order['hr_text'] = 'Estimated Hrs is near';				
				}
				if($order['hr_width']>100){
					$order['hr_width'] = 100;
					$order['hr_text'] = 'Estimated Hrs is crossed';		
				}
				if($order['tot_worked_min'] >$order['tot_est_min'] ){
					$order['hr_smiley'] = 'redflag.gif';
					$order['hr_color'] = 'cb0e0e';
					$order['hr_text'] = 'Late Completed';
					$order['loss_tot_min'] = $order['tot_worked_min'] - $order['tot_est_min'];
					$order['loss_hrs'] = (int) ($order['loss_tot_min']/60);
					$order['loss_min'] = $order['loss_tot_min']%60;
					$order['loss_hrs'] = ceil($order['loss_hrs']) ;
					$order['loss_min'] = ceil($order['loss_min']) ;
					$order['loss_min'] = strlen($order['loss_min']) ==1 ? '0'.$order['loss_min'] : $order['loss_min'] ;
					$order['gain_loss_hrs'] = "Loss ".$order['loss_hrs'].":".$order['loss_min'] ;
				}else{
					$order['hr_smiley'] = 'greenflag.gif';
					$order['hr_color'] = '13cb0e';
					$order['hr_text'] = 'Successfully Completed ';
					$order['gain_tot_min'] = $order['tot_est_min'] - $order['tot_worked_min'] ;
					$order['gain_hrs'] = (int) ($order['loss_tot_min']/60);
					$order['gain_min'] = $order['loss_tot_min']%60;
					$order['gain_hrs'] = ceil($order['gain_hrs']) ;
					$order['gain_min'] = ceil($order['gain_min']) ;	
					$order['gain_min'] = strlen($order['gain_min']) ==1 ? '0'.$order['gain_min'] : $order['gain_min'] ;
					$order['gain_loss_hrs'] = "Gain ".$order['gain_hrs'].":".$order['gain_min'] ;
				}		    
				
			}else{
			
				if($order['hr_width'] > $order['hr_width_50per']){
					$order['hr_color']='#cb0e0e';	//danger 	
					$order['hr_text'] = 'Estimated Hrs is near';				
				}
				if($order['hr_width']>100){
					$order['hr_width'] = 100;
					$order['hr_color']='#cb0e0e';	//danger 	
					$order['hr_text'] = 'Estimated Hrs is crossed';	
				}
				if($order['tot_worked_min'] >$order['tot_est_min'] ){
					
					$order['loss_tot_min'] = $order['tot_worked_min'] - $order['tot_est_min'];
					$order['loss_hrs'] = (int) ($order['loss_tot_min']/60);
					$order['loss_min'] = $order['loss_tot_min']%60 ;
					$order['loss_hrs'] = ceil($order['loss_hrs']) ;
					$order['loss_min'] = ceil($order['loss_min']) ;
				    $order['loss_min'] = strlen($order['loss_min']) ==1?'0'.$order['loss_min'] : $order['loss_min'] ;
					$order['gain_loss_hrs'] = "Overage of ".$order['loss_hrs'].":".$order['loss_min']." Hrs" ;
				}else{
					
					$order['gain_tot_min'] = $order['tot_est_min'] - $order['tot_worked_min'] ;
					$order['gain_hrs'] = (int)( $order['gain_tot_min']/60);
					$order['gain_min'] = $order['gain_tot_min']%60;					
					$order['gain_hrs'] = ceil($order['gain_hrs']) ;
					$order['gain_min'] = ceil($order['gain_min']) ;					
					$order['gain_min'] = strlen($order['gain_min']) ==1 ? '0'.$order['gain_min']:$order['gain_min'] ;
					$order['gain_loss_hrs'] = $order['gain_hrs'].":".$order['gain_min']." Hrs Remaining" ;
				}	
			} 
			//cb0e0e red
			//13cb0e green			
            if ( User::getList($db, $team_members, 'user_id, number, f_name, l_name, email', " WHERE user_id IN (". $order['team'] .")") > 0 ) {
               
            } 
			include_once ( DIR_FS_CLASS .'/Phone.class.php');
			$phone  = new Phone(TABLE_CLIENTS);
			if ( Clients::getList($db, $clients_su_members1, 'user_id, number, f_name, l_name, email, mobile1,mobile2', " WHERE user_id IN (". $order['clients_su'] .")") > 0 ) {
                if(!empty($clients_su_members1)){
					foreach($clients_su_members1 as $key1=>$val1){
						$phone->setPhoneOf(TABLE_CLIENTS, $val1['user_id']);
						$val1['phone'] = $phone->get($db);
						$clients_su_members[$key1]	=	$val1;
					}
			    }
            } 
        }
    }	
    // Retrieve the Members of the Order using the Order ID.
    $statusArr	= NULL;
   /*  $condition_querys = " WHERE status ='".ProjectTask::ACTIVE."'";
    ProjectStatus::getList( $db, $statusArr, 'id, title', $condition_querys); */
    $statusArr	= ProjectTask::getStatus();
	
	
    $variables['can_view_cost'] = false;
	$variables['can_send_comment'] = false;
	
	if ( $perm->has('nc_p_ts_sendmail') ) {
        $variables['can_send_comment'] = true;
    }
	
    if ( $perm->has('nc_p_ts_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_p_ts_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_p_ts_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_p_ts_delete') ) {
        $variables['can_delete'] = true;
    }    
    if ( $perm->has('nc_p_ts_details') ) {
        $variables['can_view_details'] = true;
    }    
    if ( $perm->has('nc_p_ts_cost') ) {
        $variables['can_view_cost'] = true;
    }
    if ( $perm->has('nc_p_tsb_add') ) {
        $variables['can_add_task_bug'] = true;
    }
    if ( $perm->has('nc_p_tsb_list') ) {
        $variables['can_view_list_bug'] = true;
    }
    //$variables['can_add_task_bug'] = true;
   
   
    //To show actual total timesheet ie ( timesheet * 3)
    if ( $perm->has('nc_p_ts_ac_tot') ) {
        $variables['can_view_actual_tot_time'] = true;
    }
    
   if ( $perm->has('nc_pts_mod_list') ) {
        $variables['can_view_module']     = true;
    }
    if ( $perm->has('nc_pts_mod_add') ) {
        $variables['can_add_module']     = true;
    }
     
    if ( $perm->has('nc_st') && $perm->has('nc_st_flw_inv') ) {
            $variables['can_add_followup'] = true;
    }    
    if ( $perm->has('nc_p_ts')) {
//    if ( $perm->has('nc_p_ts') && $order['status']==Order::ACTIVE ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
                                    TABLE_PROJECT_TASK_DETAILS   =>  array(
                                                                'Title'           => 'title',
																'Task id'         => 'id',
                                                                'Details'         => 'details',         
                                                                'Added By'        => 'added_by_name',         
                                                                'Task comment By' => 'last_comment_by_name'         
                                                            ),
                                    TABLE_PROJECT_TASK_MODULE   =>  array(
                                                                'Module Name'    => 'title-project_task_module'
                                                            ),
                                );
        
        $sOrderByArray  = array(
                                TABLE_PROJECT_TASK_DETAILS => array(                                 
                                                                'Title'    => 'title',
                                                                'Details'  => 'details'                                               
                                                            ),
                                );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PROJECT_TASK_DETAILS;
        }
        $where_added =false ;
      
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {
            
                include (DIR_FS_NC.'/project-task-add.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            
            case ('quick_add'): {
            
                include (DIR_FS_NC.'/project-task-quick-add.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            
            case ('details'): {
                include (DIR_FS_NC .'/project-task-details.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
           
            /*case ('view'): {
                include (DIR_FS_NC .'/address-book-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }*/
            
            case ('search'): {
            	
                include(DIR_FS_NC."/project-task-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }           
            case ('delete'): {
                include ( DIR_FS_NC .'/project-task-delete.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            } 
            case ('srs'):{
                include (DIR_FS_NC .'/work-srs-edit.php'); 
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            } 
            case ('followup'): { 
                include (DIR_FS_NC .'/project-task-followup.php'); 
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }  
            case ('list'):
            default: {            	
                include (DIR_FS_NC .'/project-task-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        //$messages->setErrorMessage("You donot have the Right to Access this Module.");        
    
		if($order['status']!=Order::ACTIVE){
			$messages->setErrorMessage("You donot view the tasks as this Order is not ACTIVE.<br/>
			Please contact to your Project Manager");
		}else{
			$messages->setErrorMessage("You donot have the Right to Access this Module.");
		}
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task.html');
        if($ajx==0){
			$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
		} 
    } 
    $project_id = $or_id;
    // always assign
    $s->assign("variables", $variables);
    $s->assign("actual_time", $actual_time);
    $s->assign("or_id", $or_id);
    $s->assign("project_id", $project_id);
    $s->assign("order", $order);
    $s->assign("statusArr", $statusArr);
    $s->assign("team_members", $team_members);
    $s->assign("clients_su_members", $clients_su_members);

    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
       
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>