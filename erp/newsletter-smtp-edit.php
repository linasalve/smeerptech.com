<?php
    if ( $perm->has('nc_nwl_smtp_edit') ) {
    
        $smtp_id     = isset($_GET["smtp_id"]) ? $_GET["smtp_id"] : ( isset($_POST["smtp_id"]) ? $_POST["smtp_id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
       
     
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            $extra = array( 'db'                => &$db,
                             'messages'          => &$messages
                        );
          
            if ( NewsletterSmtp::validateUpdate($data, $extra) ) {
                $query  = " UPDATE ". TABLE_NEWSLETTER_SMTP
                            ." SET ". TABLE_NEWSLETTER_SMTP .".sendmethod    	= '". $data['sendmethod'] ."', "
							. TABLE_NEWSLETTER_SMTP .".sockethost    			= '". $data['sockethost'] ."', "
							. TABLE_NEWSLETTER_SMTP .".smtpauth      			= '". $data['smtpauth'] ."', "
							. TABLE_NEWSLETTER_SMTP .".smtpauthuser  			= '". $data['smtpauthuser'] ."', "
							. TABLE_NEWSLETTER_SMTP .".smtpauthpass  			= '". $data['smtpauthpass'] ."', "
							. TABLE_NEWSLETTER_SMTP .".smtpport       			= '". $data['smtpport'] ."', "
							. TABLE_NEWSLETTER_SMTP .".from_name       			= '". $data['from_name'] ."', "
							. TABLE_NEWSLETTER_SMTP .".from_email       		= '". $data['from_email'] ."', "
							. TABLE_NEWSLETTER_SMTP .".replyto_name    			= '". $data['replyto_name'] ."', "
							. TABLE_NEWSLETTER_SMTP .".replyto_email    		= '". $data['replyto_email'] ."', "
							. TABLE_NEWSLETTER_SMTP .".client    				= '". $data['client'] ."', "
							. TABLE_NEWSLETTER_SMTP .".domain_name    			= '". $data['domain_name'] ."', "
							. TABLE_NEWSLETTER_SMTP .".domain_mailgun_key 	    = '". $data['domain_mailgun_key'] ."', "
                            . TABLE_NEWSLETTER_SMTP .".status 					= '". $data['status'] ."'"
                            ." WHERE smtp_id = '". $smtp_id ."'";
                
                if ( $db->query($query) ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    
                    //$variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_NEWSLETTER_SMTP .'.*'  ;            
            $condition_query = " WHERE (". TABLE_NEWSLETTER_SMTP .".smtp_id = '". $smtp_id ."' )";
           
            if ( NewsletterSmtp::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];                
                // Setup the date of delivery.
               
                $smtp_id         = $_ALL_POST['smtp_id'];
				if(!empty($_ALL_POST['client'])){
					include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
					Clients::getList($db, $_ALL_POST['client_data'], 'user_id,number,f_name,l_name,billing_name', "
					WHERE user_id IN ('". $_ALL_POST['client'] ."')");
					if(!empty($_ALL_POST['client_data'])){
						$_ALL_POST['client_data1'] = $_ALL_POST['client_data'][0];
						$_ALL_POST['client_details'] = 	$_ALL_POST['client_data1']['f_name']." ".$_ALL_POST['client_data1']['l_name']."(".$_ALL_POST['client_data1']['billing_name'].")";
					
					}
					
					
				}else{
				
				}
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            //$variables['hid'] = $id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/newsletter-smtp-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'smtp_id', 'value' => $smtp_id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletter-smtp-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>