<?php
    if ( $perm->has('nc_site_menu_delete') ) {
		$m_id	= isset($_GET["m_id"]) 	? $_GET["m_id"] 	: ( isset($_POST["m_id"]) 	? $_POST["m_id"] : '' );
		
		$extra = array( 'db' 		=> &$db,
						'messages' 	=> $messages
					);
		MenuSetting::delete($m_id, $extra);
		
		// Display the Webpage list.
		include ( DIR_FS_NC .'/menu-settings-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Permission to delete.");
    }
?>