<?php 
	
	
	
	$condition_query .='';
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
     
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
	
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');    
    $list	= NULL;
    $fields = TABLE_PREMIUM_EMAIL_SERVICE.'.*,'.TABLE_CLIENTS.'.billing_name,'.TABLE_CLIENT_DOMAINS.'.domain' ;
	
    PremiumEmailService::getList($db, $list, $fields, $condition_query, $next_record, $rpp);
    
	// Set the Permissions.
    $variables['can_list'] = true;
    $variables['can_add'] = true;
    $variables['can_edit'] = true;
	
    if ( $perm->has('nc_cdom_list') ) {
        $variables['can_list'] = true;
    }
    if ( $perm->has('nc_cdom_add') ){
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_cdom_edit') ) {
        $variables['can_edit'] = true;
    } 
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'premium-email-service-list.html');
?>