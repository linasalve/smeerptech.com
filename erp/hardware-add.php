<?php
    if ( $perm->has('nc_hd_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*
        if ( $perm->has('nc_hd_add_al') ) {
            $access_level += 1;
        }
		*/
        
		// Include the hardware book class.
		include_once (DIR_FS_INCLUDES .'/hardware.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Hardware::validateAdd($data, $extra) ) {
                $query	= " INSERT INTO ".TABLE_HARDWARE_ENTRY
                            ." SET ". TABLE_HARDWARE_ENTRY .".customer_name = '".       $data['customer_name'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".material = '".            $data['material'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".access_level = '".        $my['access_level'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".created_by = '".          $data['created_by'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".fault = '".               $data['fault'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".bill_no = '".             $data['bill_no'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".charge = '".              $data['charge'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".payment_remark = '".      $data['payment_remark'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".out_date = '".            $data['out_date'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".payment = '".             $data['payment'] ."'"
                                .",". TABLE_HARDWARE_ENTRY .".date = '".                date('Y-m-d') ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Hardware entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/hardware.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/hardware.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/hardware.php?added=1");   
        }
        else {
        
             $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hardware-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
                        
?>
