<?php
if ( $perm->has('nc_nav_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        include_once (DIR_FS_INCLUDES .'/navigation.inc.php');
        $menu=array(); 
        $menu = Navigation::getComboMenus();
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST  = $_POST;
		$data       = processUserData($_ALL_POST);
                        
		$extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
		if ( Navigation::validateUpdate($data, $extra) ) {
			$query  = " UPDATE ". TABLE_NAVIGATION
                        ." SET ". TABLE_NAVIGATION .".title     		  	= '". $data['title'] ."'"
                             .",". TABLE_NAVIGATION .".page        		= '". $data['page1'] ."'"
							 .",". TABLE_NAVIGATION .".parent_id        		= '". $data['parent_id'] ."'"
                            .",". TABLE_NAVIGATION .".image        		= '". $data['image'] ."'"							
					        .",". TABLE_NAVIGATION .".level    		  	= '0'"
					        .",". TABLE_NAVIGATION .".selected_page    	= '". $data['selected_page'] ."'"
                            .",". TABLE_NAVIGATION .".sequence    		= '". $data['sequence'] ."'"
							.",". TABLE_NAVIGATION .".perm_name    		= '". $data['perm_name'] ."'"
                            ." WHERE id = '". $id ."'";
                
			if ( $db->query($query) && $db->affected_rows() > 0 ) {
				$messages->setOkMessage("Record has been updated successfully.");
				$variables['hid'] = $id;
			}
			$data       = NULL;
		}
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_NAVIGATION .'.*'  ;            
            $condition_query = " WHERE (". TABLE_NAVIGATION .".id = '". $id ."' )";
            
			if ( Navigation::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
					$_ALL_POST = $_ALL_POST['0'];
					$_ALL_POST['page1']=$_ALL_POST['page'];
					// Setup the date of delivery.
						
					$id         = $_ALL_POST['id'];
					
			}else { 
				$messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
			}
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/navigation-main-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
      
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'menu', 'value' => 'menu');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'navigation-main-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
