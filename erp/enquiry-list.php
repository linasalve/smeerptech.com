<?php
if ( $perm->has('nc_equ_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    //Get count of enquiries bof
    $sql="SELECT total_enquiries_posted,total_enquiries_posted_ask,total_enquiries_posted_task,total_enquiries_posted_buzz
          FROM ".TABLE_SITE_SETTINGS." LIMIT 0,1";
    $db->query($sql);  
    if ( $db->nf()>0 ) {
        while ( $db->next_record() ) { 
            $total_enquiries_posted      = $db->f('total_enquiries_posted');
            $total_enquiries_posted_ask  =$db->f('total_enquiries_posted_ask');
            $total_enquiries_posted_task =$db->f('total_enquiries_posted_task');
            $total_enquiries_posted_buzz =$db->f('total_enquiries_posted_buzz');
        }
    }
    //Get count of enquiries eof
    
    
   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	Enquiry::getDetails( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_CONTACT_INFORMATION.'.*';
    Enquiry::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    $sitesArr = $variables['sites'];
    $sitesArr = array_flip($sitesArr);
    $fList = array();
    $countryArr = allCountryList();
    if(!empty($list)){
        foreach( $list as $key=>$val){
            $val['country_name']='';
            if(!empty($val['country'])){
                $val['country_name']=$countryArr[$val['country']];  
            }
            $val['site_name'] =$sitesArr[$val['site_id']];            
            $fList[$key]=$val;
        }
    }
    
    // Set the Permissions.
   
    if ( $perm->has('nc_equ_list') ) {
        $variables['can_view_list'] = true;
    }
    /*if ( $perm->has('nc_equ_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_equ_edit') ) {
        $variables['can_edit'] = true;
    }*/
    if ( $perm->has('nc_equ_delete') ) {
        $variables['can_delete'] = true;
    }
    if ( $perm->has('nc_equ_details') ) {
        $variables['can_view_details'] = true;
    }
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
    
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    $page["var"][] = array('variable' => 'total_enquiries_posted', 'value' => 'total_enquiries_posted');
    $page["var"][] = array('variable' => 'total_enquiries_posted_ask', 'value' => 'total_enquiries_posted_ask');
    $page["var"][] = array('variable' => 'total_enquiries_posted_task', 'value' => 'total_enquiries_posted_task');
    $page["var"][] = array('variable' => 'total_enquiries_posted_buzz', 'value' => 'total_enquiries_posted_buzz');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'enquiry-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
