<?php
	if ( $perm->has('nc_sms_api_edit') ) {
        $id	= isset($_GET["id"]) ? $_GET["id"] 	: ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
 
		 
        $extra = array( 'db'           		=> &$db,
                        'my' 				=> $my, 
                        'messages'     		=> &$messages
                    );
        SmsTemplate::updateStatus($id, $status, $extra);
        
         
        include ( DIR_FS_NC .'/sms-template-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>