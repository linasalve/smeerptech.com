<?php
    if ( $perm->has('nc_etask_usr_status') ) {
        $su_id= isset($_GET["su_id"])? $_GET["su_id"] : ( isset($_POST["su_id"])? $_POST["su_id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $extra = array( 'db' 		    => &$db,
                        'messages' 	    => $messages
                    );
        
        EtaskUser::updateStatus($su_id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/etask-users-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Change Status.");
    }
?>