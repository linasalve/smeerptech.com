<?php
      if ( $perm->has('nc_pba_delete') ) {
		$id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
		$access_level = $my['access_level'];       
        
		$extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
		PaymentBillsAgainst::delete($id, $extra);
        
		// Display the list.
		include ( DIR_FS_NC .'/payment-bills-against-list.php');
	}
	else {
		$messages->setErrorMessage("You donot have the Right to Delete the entry.");
		// $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
	}
?>
