<?php
    if ( $perm->has('nc_bl_or_delete') ) {
        
	    $or_id = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        
        $access_level = $my['access_level'];
        if ( $perm->has('nc_bl_or_delete_al') ) {
            $access_level += 1;
        }
    
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        
        /*        
        Order::delete($or_id, $extra);    
       */
        if ( (Order::getList($db, $order, TABLE_BILL_ORDERS.".id,".TABLE_BILL_ORDERS.".number,".TABLE_BILL_ORDERS.".ord_discount_against_no,".TABLE_BILL_ORDERS.".ord_discount_against_id,".TABLE_BILL_ORDERS.".ord_referred_used_against_no,".TABLE_BILL_ORDERS.".access_level, ".TABLE_BILL_ORDERS.".status", " WHERE ".TABLE_BILL_ORDERS.".id = '$or_id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != Order::COMPLETED ) {
                    if ( $order['status'] == Order::BLOCKED ) {
						if( empty($order['ord_referred_used_against_no'])){
                          if ( $order['access_level'] < $access_level ) {
							   if((Invoice::getList($db, $invoice, TABLE_BILL_INV.'.id', " WHERE ".TABLE_BILL_INV.".or_no = '".$order['number']."' AND ".TABLE_BILL_INV.".status !='".Invoice::DELETED."'") ) > 0){
									$messages->setErrorMessage("Please delete Invoice of order ".$order['number']."  ");
								}else{    
									/*
									$query = "UPDATE ". TABLE_BILL_ORDERS
												." SET status ='".Order::DELETED."' WHERE id = '$or_id'";
												
									if ( $db->query($query) && $db->affected_rows()>0 ) {                                
										
										//Delete worktimeline for selected order eof
										$messages->setOkMessage("The Order has been deleted.");
									}
									else {
										$messages->setErrorMessage("The Order was not deleted.");
									}
									*/                                
								}                            
							 }
							 else {
								$messages->setErrorMessage("Cannot Delete Order with the current Access Level.");
							 }
						}else {
							$messages->setErrorMessage("Cannot Delete Order, as we had gave the discount to the Referral in Order.".$order['ord_referred_used_against_no']);
						}
                    }else{
                        $messages->setErrorMessage("To Delete Order it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Order cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Order was not found.");
            }
        
         
        if($messages->getErrorMessageCount() > 0){
             $perform='list';
            include ( DIR_FS_NC .'/bill-order-list.php');
        
        }else{
        
             if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
                 $data		= processUserData($_POST);
                if(empty($data['reason_of_delete'])){
                    $messages->setErrorMessage("Please Specify Reason To Delete The Order ".$data['number']);
                }
                if($messages->getErrorMessageCount() <= 0){                    
                    //Sql to delete order
                    $query = "UPDATE ". TABLE_BILL_ORDERS
                            ." SET status ='".Order::DELETED."', reason_of_delete ='".$data['reason_of_delete']."',
                            delete_by ='".$my['uid']."',
                            delete_ip ='".$_SERVER['REMOTE_ADDR']."',
                            do_delete='".date("Y-m-d")."'
                            WHERE id = '$or_id'";                                            
                    if ( $db->query($query) && $db->affected_rows()>0 ) {                    
                        //Add Followup of order delete BOF
						//ord_discount_against_no
						if(!empty($order['ord_discount_against_no'])){
							$sql2 = " UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".ord_referred_status='".Order::RACTIVE."',  
								".TABLE_BILL_ORDERS.".ord_referred_used_against_no='',
								".TABLE_BILL_ORDERS.".ord_referred_used_against_id='' WHERE 
								".TABLE_BILL_ORDERS.".id='".$order['ord_discount_against_id']."'";
							$db->query($sql2) ;
							
							$sql3 = "UPDATE ". TABLE_BILL_ORDERS
								." SET  ord_discount_against_no ='',
									ord_discount_against_id='', 
									ord_discount_amt='', 
									ord_discount_to_name ='',
									ord_discount_to =''
							WHERE id = '$or_id'";          
							$db->query($sql3) ;
						}

					   /* 
						$query = " SELECT followup_no,lead_id,client FROM ".TABLE_SALE_FOLLOWUP." WHERE 
                                ".TABLE_SALE_FOLLOWUP.".followup_of = '".$or_id."' AND ".TABLE_SALE_FOLLOWUP .".table_name='".TABLE_BILL_ORDERS."'" ;
                        $db->query($query);
                        if($db->nf()>0){
                            $db->next_record();
                            $flwnumber=$db->f('followup_no');
                            $lead_id=$db->f('lead_id');
                            $client=$db->f('client');                            
                            $remarks = " Order has been deleted \n "." Reason To Delete Order : ".$data['reason_of_delete'] ;
                        }                    
                        $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                            ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"                        
                            .",". TABLE_SALE_FOLLOWUP .".remarks 		    = '".$remarks."'"
                            .",". TABLE_SALE_FOLLOWUP .".ip         	    = '". $_SERVER['REMOTE_ADDR'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".lead_id    	    = '". $lead_id ."'"
                            .",". TABLE_SALE_FOLLOWUP .".client    	        = '". $client ."'"
                            .",". TABLE_SALE_FOLLOWUP .".followup_of        = '". $or_id ."'"
                            .",". TABLE_SALE_FOLLOWUP .".table_name    	    = '".TABLE_BILL_ORDERS."'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by 	    = '". $my['uid'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".access_level 	    = '". $my['access_level'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by_dept    = '". $my['department'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".flollowup_type     = '". FOLLOWUP::ORDER ."'"
                            .",". TABLE_SALE_FOLLOWUP .".do_e 			    = '". date('Y-m-d h:i:s') ."'";                        
                        $db->query($query);  */
                        header("Location:".DIR_WS_NC."/bill-order.php?perform=list&deleted=1&or_no=".$data['number']);
                        //Add Followup of order delete EOF
                        $messages->setOkMessage("The Order has been deleted.");                         
                    }
                    else {
                        $messages->setErrorMessage("The Order was not deleted.");
                    }        
                }                
            }
            $hidden[] = array('name'=> 'or_id' ,'value' => $or_id);
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'number' ,'value' => $order['number']);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-delete.html');      
        }        
          
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the Order.");
    }
?>