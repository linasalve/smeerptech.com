<?php
    if ( $perm->has('nc_site_menu_view') ) {
		$site_id	= isset($_GET["site_id"]) 	? $_GET["site_id"]	: ( isset($_POST["site_id"]) 	? $_POST["site_id"] : SITE_ID );
		$m_id		= isset($_GET["m_id"]) 		? $_GET["m_id"] 	: ( isset($_POST["m_id"]) 		? $_POST["m_id"] 	: '' );
	
		$_ALL_POST	=	NULL;	
		if ( isset($_GET) && !empty($_GET) ) {
			$_ALL_POST = $_GET;
		}
		
		$al_list    = getAccessLevel($db, $my['access_level']);
		
		if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
			$_ALL_POST		= $_POST;
			$menu_detail	= processUserData($_ALL_POST);
			
			//read Websites
			$list_sites		= NULL;
			getAllSites( $db, $list_sites);
			
			//read static pages
			$list_static_pages		= NULL;
			getStaticPages( $db, $list_static_pages, $site_id);
			$condition = NULL;
		
			// Fetch all the parent menu
			$menu_placement 		= MenuSetting::getPlacements('title');
			$menu_type 				= MenuSetting::getType();
			$menu_status['key'] 	= MenuSetting::getStatus();
			$menu_status['value'] 	= MenuSetting::getDisplayStatus();
			$menu_existing			= NULL;
			$menu_array				= NULL;
		
			//MenuSetting::getAllMenus($db, $menu_existing, $site_id); 
			MenuSetting::getMenus($db, $menu_existing, $site_id);
			
			if ( is_array($menu_existing) && count($menu_existing)>0 ) {
				foreach ( $menu_placement as $key=>$placement ) {
					if ( array_key_exists($key, $menu_existing) ) {
						array_traverse($menu_array[$placement], $menu_existing[$key]);
					}
					else {
						$menu_array[$placement] = NULL;
					}
				}
			}
			
			
			$extra = array( 'db' 				=> &$db,
							'menu_existing' 	=> $menu_existing,
							'list_sites' 		=> $list_sites,
							'list_static_pages' => $list_static_pages,
							'menu_placement'	=> $menu_placement,
							'menu_status'		=> $menu_status,
						);
	
	
			if( isset($menu_detail["group_id"]) && $menu_detail["group_id"] != "0" ){
				// If the Parent Menu is selected, then retrieve the Placement 
				// of the Parent Menu to be applied to the Current Menu.
				$condition = " WHERE id = '". $menu_detail["group_id"] ."'";
								//." AND id != '". $m_id ."'";
				$parent = NULL;
				if ( MenuSetting::getList( $db, $parent, 'placement', $condition) ) {
					$menu_detail["placement"] = $parent[0]['placement'];
				}
				else {
					// Parent is not valid.
					$messages->setErrorMessage('The selected Parent Menu is invalid.');
				}
			}
	
			if (MenuSetting::validateUpdate($menu_detail, $messages, $extra)) {
				
				if( $menu_detail["type"] == "S" ){
					$menu_detail["page"] = $menu_detail["page_s"];
				}
				elseif( $menu_detail["type"] == "D" ){
					$menu_detail["page"] = $menu_detail["page_d"];
				}
				elseif( $menu_detail["type"] == "E" ){
					$menu_detail["page"] = $menu_detail["page_e"];
				}
				
				$query	= " UPDATE ".TABLE_MENU
							." SET " 		.TABLE_MENU .".group_id		= '". $menu_detail["group_id"] ."'"
										.",". TABLE_MENU .".site_id		= '". $menu_detail["site_id"] ."'"
										.",". TABLE_MENU .".title		= '". $menu_detail["title"] ."'" 
										.",". TABLE_MENU .".placement	= '". $menu_detail["placement"] ."'"
										.",". TABLE_MENU .".type		= '". $menu_detail["type"] ."'"
										.",". TABLE_MENU .".page		= '". $menu_detail["page"] ."'"
										.",". TABLE_MENU .".menu_order	= '". $menu_detail["order"] ."'"
										.",". TABLE_MENU .".status		= '". $menu_detail["status"] ."'"
										.",". TABLE_MENU .".access_level= '". $menu_detail["access_level"] ."'"
										.",". TABLE_MENU .".date		= '". date("Y-m-d H:i:s", time())."'"
										." WHERE ". TABLE_MENU .".id = '". $m_id ."'";
												
				if ($db->query($query) && $db->affected_rows() > 0) {
					$variables['hid'] = $m_id;
					$messages->setOkMessage('The Menu has been Updated.');
	
					// Change the Position of all the SubMenu.
					$mid_value = MenuSetting::getChildId($m_id);
					
					if (isset($mid_value) && $mid_value!='') {
						foreach( $mid_value as $value ){
								$change_submenu	= "UPDATE ". TABLE_MENU
													." SET ". TABLE_MENU.".placement = '". $menu_detail["placement"] ."'"
													." WHERE ". TABLE_MENU .".id = '". $value ."'";
								$db->query( $change_submenu );
						
						}
					}
	
					$_ALL_POST		= NULL;
					$menu_data		= NULL;
				}
				else {
					$messages->setErrorMessage('The Menu was not Updated, please try again.');
				}
			}
		}
		
	
		// Check if the Form to add is to be displayed or the control is to be sent to the List page.
		if ( isset($_POST['btnCancel'])
			|| (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$_GET = $_POST = '';
			include ( DIR_FS_NC .'/menu-settings-list.php');
		}
		else {
			// Read the Details of the Menu.
			$condition = ' WHERE id = \''. $m_id .'\'';
			$_ALL_POST = NULL;
			MenuSetting::getList( $db, $_ALL_POST, '*', $condition );
			
			if ( count($_ALL_POST) > 0 ) {
				$_ALL_POST = $_ALL_POST[0];
	
				if ( $_ALL_POST["type"] == "S" ){
					$_ALL_POST["page_s"] = $_ALL_POST["page"];
				}
				elseif ( $_ALL_POST["type"] == "D" ){
					$_ALL_POST["page_d"] = $_ALL_POST["page"];
				}
				elseif ( $_ALL_POST["type"] == "E" ){
					$_ALL_POST["page_e"] = $_ALL_POST["page"];
				}
				$_ALL_POST["order"] = $_ALL_POST["menu_order"];
	
				//read Websites
				$list_sites		= NULL;
				getAllSites( $db, $list_sites);
				
				//read static pages
				$list_static_pages		= NULL;
				getStaticPages( $db, $list_static_pages, $site_id);
				$condition = NULL;
			
				// Fetch all the parent menu
				$menu_placement 		= MenuSetting::getPlacements('title');
				$menu_type 				= MenuSetting::getType();
				$menu_status['key'] 	= MenuSetting::getStatus();
				$menu_status['value'] 	= MenuSetting::getDisplayStatus();
				$menu_existing			= NULL;
				$menu_array				= NULL;
			
				//MenuSetting::getAllMenus($db, $menu_existing, $site_id);
				MenuSetting::getMenus($db, $menu_existing, $site_id);
				if ( is_array($menu_existing) && count($menu_existing)>0 ) {
					foreach ( $menu_placement as $key=>$placement ) {
						if ( array_key_exists($key, $menu_existing) ) {
							array_traverse($menu_array[$placement], $menu_existing[$key]);
						}
						else {
							$menu_array[$placement] = NULL;
						}
					}
				}
				
				
			}
			else {
				$messages->setErrorMessage('The Menu was not found.');
			}
			
			$hidden[] = array('name'=> 'perform' , 'value' => 'edit');
			$hidden[] = array('name'=> 'act' , 'value' => 'save');
			$hidden[] = array('name'=> 'm_id' , 'value' => $m_id);
			$hidden[] = array('name'=> 'x' , 'value' => $x);
			$hidden[] = array('name'=> 'rpp' , 'value' => $rpp);
			
		
			$page["var"][] = array('variable' => 'site_id', 'value' => 'site_id');
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
			$page["var"][] = array('variable' => 'list_sites', 'value' => 'list_sites');
			$page["var"][] = array('variable' => 'list_static_pages', 'value' => 'list_static_pages');
			$page["var"][] = array('variable' => 'menu_array', 'value' => 'menu_array');
			$page["var"][] = array('variable' => 'menu_status', 'value' => 'menu_status');
			$page["var"][] = array('variable' => 'menu_type', 'value' => 'menu_type');
			$page["var"][] = array('variable' => 'menu_placement', 'value' => 'menu_placement');
			$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'menu-settings-view.html');
		}
    }
    else {
        $messages->setErrorMessage("You do not have the Permission to edit.");
    }	
?>