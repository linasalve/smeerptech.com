<?php
// include all files required for menu.
if ( !defined('THIS_DOMAIN') ) {
	require("../lib/config.php");
}

page_open(array("sess" => "NC_Session",
				"auth" => "NC_Auth",
				"perm" => "NC_Perm"
			   ));
			   
include_once ( DIR_FS_NC ."/header.php");

$date1=$_GET['date'];
$userid=$_GET['userid'];




if(!empty($date1)){
	 
		
	$arr=explode('-',$date1) ;
	$mm  =  $arr[1] ;
	$dd =$arr[2] ;
	$yy = $arr[0] ;
	$i=1;
	$date= date('Y-m-d',mktime(0, 0, 0, $mm, $dd-$i, $yy) ) ;
	$msg_txt=''; 
	$msg_txt.= "<div class=\"stdate\">
					<span class=\"timeline_square color1\"></span> 
					<span class=\"\"> ".date('d M Y',strtotime($date))." </span>
				</div>"; 
	$msg_txt.= "<div class=\"sttitle\">
					<span class=\"timeline_square color3\"></span> 
					<span class=\"\">Scores on date ".date('d M Y',strtotime($date))." </span>
				</div>";
				
			$query = "SELECT * FROM ".TABLE_SCORE_SHEET." WHERE (".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$userid.",%') AND 
			date_format(".TABLE_SCORE_SHEET.".date, '%Y-%m-%d') ='".$date."'";
			$db->query($query);  
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ){ 
					$particulars = $db->f('particulars'); 
					$id = $db->f('id'); 
					$created_by_name = $db->f('created_by_name'); 
					/*echo '<div class="postitem" id="'.$date.'">Post no '.$id.': '.$particulars.'</div>'; */
					
					$msg_txt.= "<div class=\"stbody\">
							<span class=\"timeline_square color2\"></span>
							<div class=\"stimg\"><img src=''></div>
							<div class=\"sttext\"><span title=\"Delete\" class=\"stdelete\"></span><b>".$created_by_name."</b><br>".nl2br($particulars)."
								<div class=\"sttime\"></div>
								<div class=\"stexpand\"></div>
							</div>
						  </div>";
					
					
					
				}
			}
			include_once ( DIR_FS_INCLUDES .'/task-reminder.inc.php');
			
			$msg_txt.= "<div class=\"sttitle\">
					<span class=\"timeline_square color2\"></span> 
					<span class=\"\">Tasks posted on date ".date('d M Y',strtotime($date))." </span>
				  </div>";
			 $sqlTask1 = " SELECT DISTINCT(task_no), task,created_by_name FROM ".TABLE_TASK_REMINDER ."   
                         WHERE   ( "
                                . TABLE_TASK_REMINDER .".allotted_to LIKE '%,". $user_id .",%'  " 
                            . " ) AND ".TABLE_TASK_REMINDER.".status IN('".Taskreminder::PENDING."') AND
						date_format(".TABLE_TASK_REMINDER.".do_e, '%Y-%m-%d') ='".$date."'";
							
			$db->query($sqlTask1);  
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ){ 
					$task = $db->f('task'); 
					$task_no = $db->f('task_no'); 
					$created_by_name = $db->f('created_by_name'); 
					
					$msg_txt.= "<div class=\"stbody\">
							<span class=\"timeline_square color2\"></span>
							<div class=\"stimg\"><img src=''></div>
							<div class=\"sttext\"><span title=\"Delete\" class=\"stdelete\"></span><b>".$created_by_name."</b><br> ".nl2br($task)."
								<div class=\"sttime\">task no. ".$task_no."</div>
								<div class=\"stexpand\"></div>
							</div>
						</div>";
				}
			}
			
			
			$msg_txt.= "<div class=\"sttitle\">
					<span class=\"timeline_square color2\"></span> 
					<span class=\"\">Tasks deadline on date ".date('d M Y',strtotime($date))." </span>
				  </div>";			
			$sqlTask1 = " SELECT DISTINCT(task_no), task,created_by_name FROM ".TABLE_TASK_REMINDER ."   
                         WHERE   ( "
                               . TABLE_TASK_REMINDER .".allotted_to LIKE '%,". $user_id .",%'  " 
                            . " ) AND ".TABLE_TASK_REMINDER.".status IN('".Taskreminder::PENDING."') AND
						date_format(".TABLE_TASK_REMINDER.".do_r, '%Y-%m-%d') ='".$date."'";
							
			$db->query($sqlTask1);  
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ){ 
					$task = $db->f('task'); 
					$task_no = $db->f('task_no'); 
					$created_by_name = $db->f('created_by_name'); 
					
					$msg_txt.= "<div class=\"stbody\">
							<span class=\"timeline_square color2\"></span>
							<div class=\"stimg\"><img src=''></div>
							<div class=\"sttext\"><span title=\"Delete\" class=\"stdelete\"></span><b>".$created_by_name."</b><br> ".nl2br($task)."
								<div class=\"sttime\">task no. ".$task_no."</div>
								<div class=\"stexpand\"></div>
							</div>
						</div>";
				}
			}
		echo $msg_txt."~".$date ;	
}
?>
