<?php

    if ( $perm->has('nc_ue_add') ) {
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
    	include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
		
        $_ALL_POST	    = NULL;
        $data           = NULL;
        $region         = new Region('91');
        $phone          = new Phone(TABLE_USER);
        $reminder       = new UserReminder(TABLE_USER);
		//echo '<br/> 1: ' . number_format(memory_get_usage(), 0, '.', ',') . " bytes\n";
		$access_level   = $my['access_level'];
        if ( $perm->has('nc_ue_add_al') ) {
            $access_level += 1;
        }
        $al_list    = getAccessLevel($db, $access_level);
		$role_list  = User::getRoles($db, $access_level);
		//echo '<br/> 2: ' . number_format(memory_get_usage(), 0, '.', ',') . " bytes\n";
        $groups_list  = User::getGroups();
        
        //include_once ( DIR_FS_INCLUDES .'/account.inc.php' );
        include_once(DIR_FS_INCLUDES.'/company.inc.php');
        include_once(DIR_FS_INCLUDES.'/department.inc.php');
        /*
		$account_list    = NULL;
        $condition_queryacount= "WHERE status = '". ACTIVE ."' ORDER BY name ASC";
        Account::getList($db, $account_list, 'id,name', $condition_queryacount);
		*/
        
        $company_list    = NULL;
        $condition_querycomp= "WHERE status = '". ACTIVE ."' ORDER BY name ASC";
        Company::getList($db, $company_list, 'id,name', $condition_querycomp);
        
        $department_list    = NULL;
        $condition_querydept= "WHERE status = '". ACTIVE ."' ORDER BY department_name ASC";
        Department::getList($db, $department_list, 'id,department_name', $condition_querydept);
        //BOF read the available industries
		User::getCountryCode($db,$country_code);
        //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 			= $_POST;
            $data				= processUserData($_ALL_POST);
			$files       = processUserData($_FILES);
			// BO: For generating default password.
			//$data['password']	= getRandom(6, '', array_merge($letters_b, $letters_s, $digits));
			//$data['re_password']= $data['password'];
			// EO: For generating default password.
			 
			$allowed_file_types=array( "image/jpeg",												
										"image/pjpeg",
										"application/pdf",
										"application/msword",
										"application/zip",
										"application/vnd.openxmlformats-officedocument.wordprocessingml.document"
									);			
           $allowed_photo_types=array( "image/jpeg",												
									  "image/pjpeg");
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['allowed_photo_types'] = $allowed_photo_types ;
            $data['max_file_size'] = 1024; // 1024 *1024 ; 1MB
            $data['max_photo_size'] = 40; // 1024 *4 ; 4kb
            $data['files'] = $files ;     
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            'reminder'  => $reminder,
							'files'          => &$files
                        );
           
            if ( User::validateAdd($data, $extra) ) {
                $reporting_members_str = $reporting_members_str1 = $report_to_str = $report_to_str1 = $jobp_str =$jobp_str1 ='';
            
                if(!empty($data['report_to'])) {
                    $report_to_str1=implode(",",$data['report_to']);
                    $report_to_str=",".$report_to_str1.",";
                }
				if(!empty($data['reporting_members'])){
                    $reporting_members_str1=implode(",",$data['reporting_members']);
                    $reporting_members_str=",".$reporting_members_str1.",";
                }
				if(!empty($data['task_members'])){
                    $task_members_str1=implode(",",$data['task_members']);
                    $task_members_str=",".$task_members_str1.",";
                }
				if(!empty($data['jobp'])) {
                    $jobp_str1=implode(",",$data['jobp']);
                    $jobp_str=",".$jobp_str1.",";
                }
				$groupsStr='';
				if(!empty($data['groups'])) {
                    $groups1=implode(",",$data['groups']);
                    $groupsStr = ",".$groups1.",";
                }
				
				
				$resume_attachment='';
				if(!empty($files['resume_attachment']["name"])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['resume_attachment']["name"]);
				   	$ext = $filedata["extension"] ; 
					$resume_attachment = $data['number'].".".$ext ;
					$data['resume_attachment'] = $resume_attachment;					
					$attachment_path = DIR_WS_TEAMRESUME_FILES;
					if(move_uploaded_file($files['resume_attachment']['tmp_name'], 
						DIR_FS_TEAMRESUME_FILES."/".$resume_attachment)){
						@chmod(DIR_FS_TEAMRESUME_FILES."/".$resume_attachment, 0777);
					}
				}
				$team_photo='';
				if(!empty($files['team_photo']["name"])){
					$photodata["extension"]='';
					$photodata = pathinfo($files['team_photo']["name"]);
				   	$ext = $photodata["extension"] ; 
					$team_photo = $data['number'].".".$ext ;
					$data['team_photo'] = $team_photo;					
					$attachment_path = DIR_WS_TEAMPHOTO_FILES;
					if(move_uploaded_file($files['team_photo']['tmp_name'], 
						DIR_FS_TEAMPHOTO_FILES."/".$team_photo)){
						@chmod(DIR_FS_TEAMPHOTO_FILES."/".$team_photo, 0777);
					}
				}
						
                
                $query	= " INSERT INTO ". TABLE_USER
						." SET ". TABLE_USER .".user_id     	= '". $data['user_id'] ."'"
                            .",". TABLE_USER .".number      	= '". $data['number'] 	."'"
                            .",". TABLE_USER .".username    	= '". $data['username'] ."'"
                            .",". TABLE_USER .".password    	= '". encParam($data['password']) ."'"
                            .",". TABLE_USER .".access_level	= '". $data['access_level'] ."'"
                            .",". TABLE_USER .".allow_ip    	= '". $data['allow_ip'] ."'"
                            .",". TABLE_USER .".valid_ip    	= '". $data['valid_ip'] ."'"
                            .",". TABLE_USER .".roles       	= '". $data['roles'] ."'"
                            .",". TABLE_USER .".do_join     	= '". $data['do_join'] ."'"
                            .",". TABLE_USER .".do_resign   	= '". $data['do_resign'] ."'"
                            .",". TABLE_USER .".email       	= '". $data['email'] ."'"
                            .",". TABLE_USER .".email_password  = '". $data['email_password'] ."'"
                            .",". TABLE_USER .".email_1     	= '". $data['email_1'] ."'"
                            .",". TABLE_USER .".email_2     	= '". $data['email_2'] ."'"
                            .",". TABLE_USER .".title       	= '". $data['title'] ."'"
                            .",". TABLE_USER .".f_name      	= '". $data['f_name'] ."'"
                            .",". TABLE_USER .".m_name      	= '". $data['m_name'] ."'"
                            .",". TABLE_USER .".l_name      	= '". $data['l_name'] ."'"
                            .",". TABLE_USER .".p_name      	= '". $data['p_name'] ."'"
							.",". TABLE_USER .".mobile1     	= '". $data['mobile1'] ."'"
                            .",". TABLE_USER .".mobile2     	= '". $data['mobile2'] ."'"
                            .",". TABLE_USER .".marketing_contact = '". $data['marketing_contact'] ."'"
                            .",". TABLE_USER .".mkt_person_cron = '". $data['mkt_person_cron'] ."'"
                            .",". TABLE_USER .".current_salary  = '". $data['current_salary'] ."'"
                            //.",". TABLE_USER .".account_id  = '". $data['account_id'] ."'"
                            .",". TABLE_USER .".company_id  = '". $data['company_id'] ."'"
                            .",". TABLE_USER .".report_to   = '". $report_to_str ."'"
                            .",". TABLE_USER .".my_reporting_members   = '". $reporting_members_str ."'"
                            .",". TABLE_USER .".task_members   = '". $task_members_str ."'"
                            .",". TABLE_USER .".groups  	= '". $groupsStr ."'"
                            .",". TABLE_USER .".jobp  	= '". $jobp_str ."'"
                            .",". TABLE_USER .".resume_attachment  = '". $resume_attachment ."'"
                            .",". TABLE_USER .".team_photo  = '". $team_photo ."'"
                            .",". TABLE_USER .".department  = '". $data['department'] ."'"
                            .",". TABLE_USER .".qualification  = '". $data['qualification'] ."'"
                            .",". TABLE_USER .".desig       = '". $data['desig'] ."'"
                            .",". TABLE_USER .".org         = '". $data['org'] ."'"
                            .",". TABLE_USER .".domain      = '". $data['domain'] ."'"
                            .",". TABLE_USER .".gender      = '". $data['gender'] ."'"
                            .",". TABLE_USER .".do_birth    = '". $data['do_birth'] ."'"
                            .",". TABLE_USER .".do_aniv     = '". $data['do_aniv'] ."'"
                            .",". TABLE_USER .".remarks     = '". $data['remarks'] ."'"
                            .",". TABLE_USER .".do_add      = '". date('Y-m-d H:i:s', time()) ."'"
							.",". TABLE_USER .".current_staff  = '". $data['current_staff'] ."'" 
                            .",". TABLE_USER .".status      = '". $data['status'] ."'" ;
                
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $variables['hid'] = $data['user_id'];

                    // Insert the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        $address_arr = array('address_type' => $data['address_type'][$i],
                                            'company_name'  => $data['company_name'][$i],
                                            'address'       => $data['address'][$i],
                                            'city'          => $data['city'][$i],
                                            'state'         => $data['state'][$i],
                                            'country'       => $data['country'][$i],
                                            'zipcode'       => $data['zipcode'][$i],
                                            'is_preferred'  => $data['is_preferred'][$i],
                                            'is_verified'   => $data['is_verified'][$i]
                                            );
                        
                        if ( !$region->update($variables['hid'], TABLE_USER, $address_arr) ) {
                            foreach ( $region->getError() as $errors) {
                                $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                            }
                        }
                    }
                    
                    // Insert the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {
                            $phone_arr = array( 'p_type'        => $data['p_type'][$i],
                                                'cc'            => $data['cc'][$i],
                                                'ac'            => $data['ac'][$i],
                                                'p_number'      => $data['p_number'][$i],
                                                'p_is_preferred'=> $data['p_is_preferred'][$i],
                                                'p_is_verified' => $data['p_is_verified'][$i]
                                                );
                            if ( ! $phone->update($db, $variables['hid'], TABLE_USER, $phone_arr) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Insert the Reminders.
                    for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['remind_date_'.$i]) ) {
                            $reminder_arr = array(  'id'            => (isset($data['remind_id_'.$i])? $data['remind_id_'.$i] : ''),
                                                    'do_reminder'   => $data['remind_date_'.$i],
                                                    'description'   => $data['remind_text_'.$i]
                                                );
                            if ( ! $reminder->update($db, $variables['hid'], TABLE_USER, $reminder_arr) ) {
                                $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                foreach ( $reminder->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Send The Email to the concerned persons.
                    //include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_ue_add_notif', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_ue_add_notif_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link']       = DIR_WS_NC .'/user.php?perform=view_details&user_id='. $data['user_id'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'NEW_EXECUTIVE_REGISTER_MANAGERS', $data, $email) ) {
                            $to = '';
                            $to[] = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        
                    }
                    
                    // Send Email to the newly added Executive.
                    $data['link']       = DIR_WS_NC;
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'NEW_EXECUTIVE_REGISTER', $data, $email) ) {
                        $to = '';
                        $to[] = array('name' => $data['f_name'] .' '. $data['l_name'], 'email' => $data['email']);
                        $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    $messages->setOkMessage("New Executive has been added.");
                    
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/user-list.php');
        }
        else {

            $_ALL_POST['number'] = User::getNewAccNumber($db);
            if ( empty($data['country']) ) {
                $data['country'] = '91';
            }
            $lst_country    = $region->getCountryList($data['country']);
            $lst_state      = $region->getStateList($data['state']);
            $lst_city       = $region->getCityList($data['city']);
			
			$region  =null;
			$phone  =null; 
			$reminder=null;
			 
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'reminder_count', 'value' => '6');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
            $page["var"][] = array('variable' => 'groups_list', 'value' => 'groups_list');
            $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
            $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
            $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
            $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            $page["var"][] = array('variable' => 'allowipArr', 'value' => 'allowipArr');
            //$page["var"][] = array('variable' => 'account_list', 'value' => 'account_list');
            $page["var"][] = array('variable' => 'company_list', 'value' => 'company_list');
            $page["var"][] = array('variable' => 'department_list', 'value' => 'department_list');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-add.html');
        }
		
    }else{
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>