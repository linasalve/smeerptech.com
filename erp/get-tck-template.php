<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
  
	include_once( DIR_FS_INCLUDES.'/support-ticket-template.inc.php');   

    $sString = isset($_GET['tpl_title']) ? $_GET['tpl_title'] : '';
	
    $optionLink = $stringOpt = '' ;
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if (isset($sString) && !empty($sString)){
        header("Content-Type: application/json");      
	    
		
		$condition_query = " WHERE ".TABLE_ST_TEMPLATE.".title LIKE '%".$sString."%'";
		 
        $fields = TABLE_ST_TEMPLATE.".subject,".TABLE_ST_TEMPLATE.".id,"
			.TABLE_ST_TEMPLATE.".file_1,".TABLE_ST_TEMPLATE.".file_2,".TABLE_ST_TEMPLATE.".file_3,
			".TABLE_ST_TEMPLATE.".title,".TABLE_ST_TEMPLATE.".details " ;
        
         $query = "SELECT ".$fields." FROM ". TABLE_ST_TEMPLATE.$condition_query;
        $db->query($query); 		
		
        echo "{\"results\": [";
        if ( $db->nf()>0 ) {
          
            $arr = array();
            while ($db->next_record()){			
                 $id=$db->f('id');   
                 $title=$db->f('title');   
                 $subject=$db->f('subject');   
                 $details=$db->f('details');   
                 $ss_title=$db->f('subject');   
                 
                 $file_1=$db->f('file_1');   
                 $file_2=$db->f('file_2');   
                 $file_3=$db->f('file_3');   
                 
				if(empty($file_1)){
					$file_1='';
				}else{ 
				}
				if(empty($file_2)){
					$file_2='';
				}else{ 
				}
                if(empty($file_3)){
					$file_3='';
				}else{ 
				
				} 
                 
                $arr[] = "{\"id\": \"".$id."\",\"data1\": \"".$file_1."\",\"data2\": \"".$file_2."\",
				 \"data3\": \"".$file_3."\",\"data4\": \"".processSQLData($subject)."\",
				 \"data5\": \"".processSQLData($details)."\",\"data6\": \"".processSQLData($title)."\",
				 \"value\": \"".processSQLData($db->f('title'))."\", \"info\": \"\"}";
            }
            echo implode(", ", $arr);
           
        }
        echo "]}";	   
	}
	exit;
include_once( DIR_FS_NC ."/flush.php");


?>
