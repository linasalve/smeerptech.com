<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   )); 
    include_once ( DIR_FS_NC ."/header.php");  
	include_once (DIR_FS_INCLUDES .'/prospects-order.inc.php');
	include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
	$id =  isset($_GET["id"])   ? $_GET["id"]  : ( isset($_POST["id"])    ? $_POST["id"] : '' );
 	$status = isset($_GET["status"])   ? $_GET["status"]  : ( isset($_POST["status"])    ? $_POST["status"] : '' ); 
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new = new db_local; // database handle
	
    if(isset($status) && $status!=''){   
		//BOF
		$flw_ord_id = $id;
		$fields = TABLE_PROSPECTS_ORDERS.".id,".TABLE_PROSPECTS_ORDERS.".client as cclient,".TABLE_PROSPECTS_ORDERS.".number,"
				.TABLE_PROSPECTS_ORDERS.".access_level,".TABLE_PROSPECTS_ORDERS.".lead_status,"
				.TABLE_PROSPECTS_ORDERS.".do_o,".TABLE_PROSPECTS_ORDERS.".order_title,".TABLE_PROSPECTS.".f_name,"
				.TABLE_PROSPECTS.".l_name,".TABLE_PROSPECTS.".billing_name" ;
		ProspectsOrder::getDetails($db, $orderinfo1, $fields, " WHERE ".TABLE_PROSPECTS_ORDERS.".id = '$id'");
		if(!empty($orderinfo1)){
			$orderinfo=$orderinfo1[0];
			$old_lstatus_id = $orderinfo['lead_status'];
			$lstatusArr = ProspectsOrder::getLStatus();
			$lstatusArr = array_flip($lstatusArr);
			$old_lstatus = $lstatusArr[$old_lstatus_id];
		}
		
		$sql = " UPDATE ".TABLE_PROSPECTS_ORDERS." SET 
			".TABLE_PROSPECTS_ORDERS.".lead_status='".$status."',
			".TABLE_PROSPECTS_ORDERS.".lead_status_dt='".date('Y-m-d h:i:s')."',
			".TABLE_PROSPECTS_ORDERS.".lead_status_by='".$my['user_id']."',
			".TABLE_PROSPECTS_ORDERS.".lead_status_by_name='".$my['f_name']." ".$my['l_name']."'
			WHERE ".TABLE_PROSPECTS_ORDERS.".id ='".$id."'"; 
		$execute = $db_new->query($sql); 
		
		if($status==ProspectsOrder::HOTLSTATUS){
			$status_name = 'HOT';
			$msg = 'You are Successfully Marked this Lead Order as HOT';
			$update_text = "HOT\n<a href=\"#\" title=\"Mark WARM\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WARMLSTATUS.")\">			WARM</a>\n<a href=\"#\" title=\"Mark COLD\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::COLDLSTATUS.")\">			COLD</a>\n<a href=\"#\" title=\"Mark NEGATIVE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::NEGATIVELSTATUS.")\">			NEGATIVE</a>\n<a href=\"#\" title=\"Mark WIN\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WINLSTATUS.")\">WIN</a>\n<a href=\"#\" title=\"Mark LOOSE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::LOOSELSTATUS.")\">LOOSE</a>";
		}elseif($status==ProspectsOrder::WARMLSTATUS){
			$status_name = 'WARM';
			$msg = 'You are Successfully Marked this Lead Order as WARM';
			$update_text = "WARM\n<a href=\"#\" title=\"Mark HOT\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::HOTLSTATUS.")\">			HOT</a>\n<a href=\"#\" title=\"Mark COLD\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::COLDLSTATUS.")\">			COLD</a>\n<a href=\"#\" title=\"Mark NEGATIVE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::NEGATIVELSTATUS.")\">NEGATIVE</a>\n<a href=\"#\" title=\"Mark WIN\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WINLSTATUS.")\">WIN</a>\n<a href=\"#\" title=\"Mark LOOSE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::LOOSELSTATUS.")\">LOOSE</a>";
		}elseif($status==ProspectsOrder::COLDLSTATUS){
			$status_name = 'COLD';
			$msg = 'You are Successfully Marked this Lead Order as COLD';
			$update_text = "COLD\n<a href=\"#\" title=\"Mark WARM\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WARMLSTATUS.")\">			WARM</a>\n<a href=\"#\" title=\"Mark HOT\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::HOTLSTATUS.")\">			HOT</a>\n<a href=\"#\" title=\"Mark NEGATIVE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::NEGATIVELSTATUS.")\">NEGATIVE</a>\n<a href=\"#\" title=\"Mark WIN\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WINLSTATUS.")\">WIN</a>\n<a href=\"#\" title=\"Mark LOOSE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::LOOSELSTATUS.")\">LOOSE</a>";
		}elseif($status==ProspectsOrder::NEGATIVELSTATUS){
			$status_name = 'NEGATIVE';
			$msg = 'You are Successfully Marked this Lead Order as NEGATIVE';
			$update_text = "NEGATIVE\n<a href=\"#\" title=\"Mark WARM\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WARMLSTATUS.")\">		WARM</a>\n<a href=\"#\" title=\"Mark HOT\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::HOTLSTATUS.")\">HOT</a>\n <a href=\"#\" title=\"Mark WIN\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WINLSTATUS.")\">WIN</a>\n<a href=\"#\" title=\"Mark LOOSE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::LOOSELSTATUS.")\">LOOSE</a>";
		}elseif($status==ProspectsOrder::WINLSTATUS){
			$status_name = 'WIN';
			$msg = 'You are Successfully Marked this Lead Order as WIN';
			/* 
			$update_text = "<a href=\"#\" title=\"Mark WARM\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WARMLSTATUS.")\">			WARM</a>\n<a href=\"#\" title=\"Mark HOT\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::HOTLSTATUS.")\">			HOT</a>\n<a href=\"#\" title=\"Mark NEGATIVE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::NEGATIVELSTATUS.")\">NEGATIVE</a>"; */
			$update_text = "WIN";
		}elseif($status==ProspectsOrder::LOOSELSTATUS){
			$status_name = 'LOOSE';
			$msg = 'You are Successfully Marked this Lead Order as LOOSE';
			/* $update_text = "<a href=\"#\" title=\"Mark WARM\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::WARMLSTATUS.")\">			WARM</a>\n<a href=\"#\" title=\"Mark HOT\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::HOTLSTATUS.")\">			HOT</a>\n<a href=\"#\" title=\"Mark NEGATIVE\" onClick=\"updatePLeadStatus(".$id.",".ProspectsOrder::NEGATIVELSTATUS.")\">NEGATIVE</a>"; */
			$update_text = "LOOSE";
		} 
		
		$sql2 = " SELECT ticket_id FROM ".TABLE_PROSPECTS_TICKETS." WHERE ".TABLE_PROSPECTS_TICKETS.".flw_ord_id = '".$id."' LIMIT 0,1";
		
		$db->query($sql2) ;
		if ( $db->nf() > 0 ) {
			while ($db->next_record()) {
			   $ticket_id= $db->f('ticket_id');
			}
		}
		$ticket_no  =  ProspectsTicket::getNewNumber($db);
		if ( $ticket_id>0 ) {	
			//Add Comment BOF	
			$data['ticket_owner_uid'] = $orderinfo['cclient'];
			$data['name']= $orderinfo['f_name']." ".$orderinfo['l_name'] ;
			$data['ticket_owner'] = $orderinfo['billing_name']." ";
			if(!empty($data['name'])){
				$data['ticket_owner'] = $orderinfo['billing_name']." ".$data['name'];
			}
			
			$data['ticket_text'] = " LEAD NO - ".$orderinfo['number']."<br/>
			OLD STATUS - ".$old_lstatus."<br/>
			NEW STATUS - ".$status_name." <br/>
			UPDATED BY - ".$my['f_name']." ". $my['l_name']." <br/>
			ON DATE - ".date('d M Y') ;
				
			$data['hrs']='';
			$data['min']=5;
			$data['posted_by'] = AUTO_USER_ID  ; 
			$data['posted_by_name'] = AUTO_USER_NAME ; 
			 								 
			$data['ticket_subject'] = 'Prospect Followup';
			
			$hrs1 = (int) ($data['hrs'] * 6);
			$min1 = (int) ($data['min'] * 6);   
			 $followup_query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
			. TABLE_PROSPECTS_TICKETS .".ticket_no  = '". $ticket_no ."', "
			. TABLE_PROSPECTS_TICKETS .".flw_ord_id = '". $flw_ord_id ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_owner = '". $data['ticket_owner'] ."', "	 
			. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $data['posted_by']."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $data['posted_by_name']."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_status     = '".ProspectsTicket::PENDINGWITHCLIENTS ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_child      = '".$ticket_id."',"
			. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
			. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
			. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
			. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
			. TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
			. TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
			. TABLE_PROSPECTS_TICKETS .".display_name           = '". $my['f_name']." ". $my['l_name'] ."', "
			. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $my['uid'] ."', "
			. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $my['desig'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
			. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
			. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
			. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
	
			$db->query($followup_query) ;
			//Add Comment EOF
			
		}else{
			
			
			
			//Add Comment BOF
			
			$data['ticket_owner_uid'] = $orderinfo['cclient'];
			$data['name']= $orderinfo['f_name']." ".$orderinfo['l_name'] ;
			$data['ticket_owner'] = $orderinfo['billing_name']." ";
			if(!empty($data['name'])){
				$data['ticket_owner'] = $orderinfo['billing_name']." ".$data['name'];
			}
			
			$data['ticket_text'] = " LEAD NO - ".$orderinfo['number']."<br/>
			OLD STATUS - ".$old_lstatus."<br/>
			NEW STATUS - ".$status_name." <br/>
			UPDATED BY - ".$my['f_name']." ". $my['l_name']." <br/>
			ON DATE - ".date('d M Y') ;
				
			$data['hrs']='';
			$data['min']=5;
		
			$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
			$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
			$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;							 
			$hrs1 = (int) ($data['hrs'] * 6);
			$min1 = (int) ($data['min'] * 6);  	
			$data['posted_by'] = AUTO_USER_ID  ; 
			$data['posted_by_name'] = AUTO_USER_NAME ; 

			
				
			 $followup_query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
			. TABLE_PROSPECTS_TICKETS .".ticket_no   = '". $ticket_no ."', "
			. TABLE_PROSPECTS_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $data['posted_by'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $data['posted_by_name'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_status     = '".ProspectsTicket::PENDINGWITHCLIENTS ."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_child      = '0',"
			. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
			. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
			. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
			. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
			. TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
			. TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
			. TABLE_PROSPECTS_TICKETS .".display_name           = '". $my['f_name']." ". $my['l_name'] ."', "
			. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $my['uid'] ."', "
			. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $my['desig'] ."', "
			. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
			. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
			. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
			. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
			. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
			
			$db->query($followup_query) ;
			//Add Comment EOF
		}
		//EOF 
		if($execute){
			$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
			<tr>
				<td class=\"message_header_ok\">
					<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
					Success
				</td>
			</tr>
			<tr>
				<td class=\"message_ok\" align=\"left\" valign=\"top\">
					<ul class=\"message_ok_2\">
						<li class=\"message_ok\"> ".$msg."  </li>
					</ul>
				</td>
			</tr>
			</table>"; 
			$message .= "|".$update_text; 
		} 
	}	

echo $message."|".$id ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
