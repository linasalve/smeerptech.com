<?php
    if ( $perm->has('nc_urht_add') ) {
        $_ALL_POST	= NULL;
        $data       = NULL;

		//get all the parent rights
		$rights = NULL;
		UserRights::getParent($db,$rights);

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 				=> &$db,
                            'data' 				=> $data,
                            'messages'          => $messages
                        );
            
            if (UserRights::validateAdd($data, $messages, $extra)) {
                $query	= " INSERT INTO ".TABLE_USER_RIGHTS
                                ." SET ". TABLE_USER_RIGHTS .".title 				='".$data['title']."'"
											.",".TABLE_USER_RIGHTS .".parent_id		='".$data['rights']."'"
                                            .",".TABLE_USER_RIGHTS .".value			='".$data['value']."'"
                                            .",".TABLE_USER_RIGHTS .".description	='".$data['description']."'"
                                            .",".TABLE_USER_RIGHTS .".status		='".$data['status']."'"	;
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $messages->setOkMessage("User Right added successfully");
                    $variables['hid'] = $db->last_inserted_id();
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        //To avoid duplicate entry
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/user-rights.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/user-rights.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/user-rights.php?added=1");   
        }
        else {
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
    
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
			$page["var"][] = array('variable' => 'rights', 'value' => 'rights');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-rights-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>