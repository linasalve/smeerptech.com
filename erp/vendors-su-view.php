<?php

    if ( $perm->has('nc_v_su_details') ) {
        $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_v_su_details_al') ) {
            $access_level += 1;
        }

        if ( Vendors::getList($db, $list, '*', " WHERE user_id = '$user_id'") > 0 ) {
            $_ALL_POST = $list['0'];
       
            if ( $_ALL_POST['access_level'] < $access_level ) {
                //include_once ( DIR_FS_CLASS .'/Region.class.php');
                include_once ( DIR_FS_CLASS .'/PhoneVendor.class.php');
                //include_once ( DIR_FS_CLASS .'/UserReminder.class.php');

                //$region         = new Region();
                $phone          = new PhoneVendor(TABLE_VENDORS);
                //$reminder       = new UserReminder(TABLE_VENDORS);

				
                $_ALL_POST['access_level']  = Vendors::getAccessLevel($db, $access_level, $_ALL_POST['access_level']);
                //$_ALL_POST['roles']         = Clients::getRoles($db, $access_level, $_ALL_POST['roles']);
                //print_r($role_list);
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_VENDORS, $user_id);
                $_ALL_POST['phone'] = $phone->get($db);
                
                // Read the Addresses.
                /*$region->setAddressOf(TABLE_VENDORS, $user_id);
                $_ALL_POST['address_list'] = $region->get();
                
                // Read the Reminders.
                $reminder->setReminderOf(TABLE_VENDORS, $user_id);
                $_ALL_POST['reminder_list'] = $reminder->get($db);*/
                
                //print_r($_ALL_POST);
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                //$page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
    
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-su-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Vendors with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("The Vendors details were not found. ");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>