<?php

    if ( $perm->has('nc_sl_ld_asig_list') ) {
	
		
		include_once ( DIR_FS_INCLUDES .'/leads-ticket.inc.php');
        
      
        
        if(isset($_POST['assign']) && $_POST['assign']=='Go')
		{
       
			if (!empty($_POST["check_lead"])) 
			{
				foreach ($_POST["check_lead"] as $key => $value) 
				{
					
                        if(!empty($_POST['lead_assign_to'][$value])){
                        
                            $usrDetails = array();                          
                            $table = TABLE_SALE_LEADS;
                            $condition2 = " WHERE ".TABLE_SALE_LEADS .".lead_id= '".$value ."' " ;
                            $fields1 =  TABLE_SALE_LEADS .'.f_name,'.TABLE_SALE_LEADS .'.l_name,
							'.TABLE_SALE_LEADS.'.billing_name';
                            $usrDetails= getRecord($table,$fields1,$condition2);                            
                            
							$ticket_owner_uid = $value;
							$ticket_owner =  $usrDetails['billing_name']." - ".$usrDetails['f_name']." ".$usrDetails['l_name'] ;
						    $query = "UPDATE  ".TABLE_SALE_LEADS
									." SET "
									. TABLE_SALE_LEADS .".lead_assign_to ='".$_POST['lead_assign_to'][$value]."',"
									. TABLE_SALE_LEADS .".lead_status = '".Leads::ASSIGNED."'"
									." WHERE ". TABLE_SALE_LEADS .".lead_id = '".$value."' "; 
                            $db->query($query);
                             
                            $assign_query	= " INSERT INTO ".TABLE_SALE_ASSIGN_HISTORY
										." SET ".TABLE_SALE_ASSIGN_HISTORY .".lead_id = '". $value ."'"  
										.",". TABLE_SALE_ASSIGN_HISTORY .".assign_to  = '".	$_POST['lead_assign_to'][$value] ."'"
										.",". TABLE_SALE_ASSIGN_HISTORY .".created_by = '".	$my['uid'] ."'"
										.",". TABLE_SALE_ASSIGN_HISTORY .".do_assign  = '".	date('Y-m-d')."'";
						    $db->query($assign_query);
                            //insert folloup
                            /*  
							$followup_query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                                    ." SET ".TABLE_SALE_FOLLOWUP .".followup_no 	= '". $followup_no ."'"  
                                    .",". TABLE_SALE_FOLLOWUP .".assign_to  		= '".	$_POST['lead_assign_to'][$value]  ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".remarks 			= 'Lead Assigned To Telemarketing Team' "
                                    .",". TABLE_SALE_FOLLOWUP .".ip         	    = '". $_SERVER['REMOTE_ADDR'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".lead_id    	    = '". $value ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".followup_of        = '". $value ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".table_name    	    = 'sale_leads'"
                                    .",". TABLE_SALE_FOLLOWUP .".created_by 		= '". $my['uid'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".access_level 		= '". $my['access_level'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".created_by_dept 	= '". $my['department'] ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".flollowup_type     = '". FOLLOWUP::LEADTM ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".status             = '". LEADS::ASSIGNED ."'"
                                    .",". TABLE_SALE_FOLLOWUP .".do_e  				= '". date('Y-m-d h:i:s')."'";
                            $db_followup = new db_local();
                            $db_followup->query($followup_query); 
							*/
							$teamMemDetails = array();
							$table = TABLE_USER;
                            $condition2 = " WHERE ".TABLE_USER .".user_id = '".$_POST['lead_assign_to'][$value]."' " ;
                            $fields1 =  TABLE_USER .'.f_name,'.TABLE_USER .'.l_name,
							'.TABLE_USER.'.number' ;
                            $teamMemDetails= getRecord($table,$fields1,$condition2);							 
							//Check ticket for followup is exist or not bof
							$data['ticket_owner_uid'] = $ticket_owner_uid;
							$data['ticket_owner'] = $ticket_owner;	
							$ticket_id=0;
							$sql2 = " SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
							".TABLE_LD_TICKETS.".leads_followup = '1' AND 
							".TABLE_LD_TICKETS.".ticket_owner_uid='".$ticket_owner_uid."' AND 
							".TABLE_LD_TICKETS.".ticket_child ='0' LIMIT 0,1";							
							if ( $db->query($sql2) ) {
								$ticket_id=0;
								if ( $db->nf() > 0 ) {
									while ($db->next_record()) {
									   $ticket_id = $db->f('ticket_id');
									}
								}
							}	
							$ticket_no  =  LeadsTicket::getNewNumber($db);
							
							//disply random name BOF 				 
							if($my['department'] == ID_MARKETING){
								//This is the marketing person identity
								$data['tck_owner_member_id'] = $my['user_id'];
								$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
								$data['tck_owner_member_email'] = $my['email'];
							}else{ 
								$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
								$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
								$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;
							}
							 
							$data['hrs'] =0;
							$data['min'] =10;
							$hrs1 = (int) ($data['hrs'] * 6);
							$min1 = (int) ($data['min'] * 6);  
					 
							$data['ticket_subject'] = 'Prospect Followup';
							$data['ticket_text'] = " Prospect  - 
							".$usrDetails['billing_name']." ".$usrDetails['f_name']." ".$usrDetails['f_name']." Assigned To 
							CRM Team Member ". $teamMemDetails['f_name']." ".$teamMemDetails['l_name']." 
							( ".$teamMemDetails['number'].") 
							<br/> Assigned By ".$my['f_name']." ".$my['l_name']." ( ".$my['number']." ) ";
							
							//disply random name EOF				
							if($ticket_id>0){
							
								$query= "SELECT "	. TABLE_LD_TICKETS .".ticket_date, ".TABLE_LD_TICKETS.".status"
										." FROM ". TABLE_LD_TICKETS 
										." WHERE ". TABLE_LD_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
										." AND  (". TABLE_LD_TICKETS .".ticket_child = '". $ticket_id ."' " 
													." OR "
													. TABLE_LD_TICKETS .".ticket_id = '". $ticket_id ."' " 
												.") "
										." ORDER BY ". TABLE_LD_TICKETS .".ticket_date DESC LIMIT 0,1";				
									
								$db->query($query) ;
								if($db->nf() > 0){
									$db->next_record() ;
									$last_time = $db->f("ticket_date") ;
									$ticket_response_time = time() - $last_time ;
									$status = $db->f("status") ;
								}else{
									$ticket_response_time = 0 ;
								}
								$ticket_date 	= time() ;
								$ticket_replied = '0' ;
								
								$query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
								. TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "								 
								. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
								. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
								. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
								. TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
								. TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
								. TABLE_LD_TICKETS .".ticket_creator_uid = '".$my['uid'] ."', "
								. TABLE_LD_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
								. TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
								. TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
								. TABLE_LD_TICKETS .".ticket_status     = '".LeadsTicket::PENDINGWITHCLIENTS."', "
								. TABLE_LD_TICKETS .".ticket_child      = '".$ticket_id."', "
								. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
								. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
								. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
								. TABLE_LD_TICKETS .".min = '". $data['min']."', "
								. TABLE_LD_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
								. TABLE_LD_TICKETS .".ticket_replied    = '".$ticket_replied."', "
								. TABLE_LD_TICKETS .".from_admin_panel  = '".LeadsTicket::ADMIN_PANEL."', "
								//. TABLE_LD_TICKETS .".mail_client    = '".$mail_client."', "
								//. TABLE_LD_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
								//. TABLE_LD_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
								//. TABLE_LD_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', "
								. TABLE_LD_TICKETS .".display_name           = '". $my['f_name']." ". $my['l_name'] ."', "
								. TABLE_LD_TICKETS .".display_user_id        = '". $my['uid']  ."', "
								. TABLE_LD_TICKETS .".display_designation    = '". $my['desig']  ."', "
								. TABLE_LD_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
								. TABLE_LD_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
								. TABLE_LD_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "
								. TABLE_LD_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
								
								$db->query($query) ;
								$variables['hid'] =  $db->last_inserted_id() ;

								$query_update = "UPDATE ". TABLE_LD_TICKETS 
										." SET ". TABLE_LD_TICKETS .".ticket_status = '".LeadsTicket::PENDINGWITHCLIENTS."'"  
										.",".TABLE_LD_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
										.",".TABLE_LD_TICKETS.".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."'"
										.",".TABLE_LD_TICKETS.".last_comment = '".$data['ticket_text']."'"
										.",".TABLE_LD_TICKETS.".last_comment_by = '".$my['uid'] ."'"
										.",".TABLE_LD_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
										.",".TABLE_LD_TICKETS.".status='".LeadsTicket::ACTIVE."'"
										." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $ticket_id ."' " ;
								$db->query($query_update) ;
							
							}else{
								//Create New Ticket 
								$query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
									. TABLE_LD_TICKETS .".leads_followup    = '1', "								
									. TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "								
									. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
									. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
									. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
									. TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
									. TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
									. TABLE_LD_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
									. TABLE_LD_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
									. TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
									. TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
									//. TABLE_LD_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
									//. TABLE_LD_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
									//. TABLE_LD_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', "
									. TABLE_LD_TICKETS .".ticket_status     = '". LeadsTicket::PENDINGWITHCLIENTS ."', "
									. TABLE_LD_TICKETS.".status='".LeadsTicket::ACTIVE."',"
									. TABLE_LD_TICKETS .".ticket_child   = '0', "
									. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
									. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
									. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
									. TABLE_LD_TICKETS .".min = '". $data['min']."', "                        
									. TABLE_LD_TICKETS .".ticket_response_time = '0', "
									. TABLE_LD_TICKETS .".ticket_replied        = '0', "
									. TABLE_LD_TICKETS .".from_admin_panel        = '".LeadsTicket::ADMIN_PANEL."', "
									. TABLE_LD_TICKETS .".display_name           = '". $my['f_name']." ". $my['l_name'] ."', "
									. TABLE_LD_TICKETS .".display_user_id        = '". $my['uid']  ."', "
									. TABLE_LD_TICKETS .".display_designation    = '". $my['desig']  ."', "
									. TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
									. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
									. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
									. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
									. TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
								$db->query($query) ;
								$variables['hid'] =  $db->last_inserted_id() ;
							}
							//POST DATA in LEADS PROPOSAL FOLLOWUP EOF
							
							 
                             //Check ticket for followup is exist or not eof
							 
							 
							 
							 
							 
							 
                            $messages->setOkMessage("Lead/Leads assigned successfully. ");  
                        }
				}
			}
			else
			{
				$messages->setErrorMessage("Please select the Lead to assign. ");
			}		
		}	
        // Set the Permissions.
        
		if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
		
        if(empty($condition_query)){
			
			$_SEARCH["chk_status"]  = 'AND';
			$_SEARCH["sStatus"]     = array(Leads::NEWONE); 
            $condition_query .= " WHERE lead_status= '".Leads::NEWONE."' AND lead_assign_to_mr='' ";
        } 
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        
        
        // To count total records.
        $list	= 	NULL;
        $total	=	Leads::getList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields ='';
        $fields = TABLE_SALE_LEADS.".lead_id, ".TABLE_SALE_LEADS.".company_name,".TABLE_SALE_LEADS.".f_name,".TABLE_SALE_LEADS.".m_name,".TABLE_SALE_LEADS.".l_name," ;
        $fields .= TABLE_SALE_LEADS.".email, ". TABLE_SALE_LEADS.".lead_status, ".TABLE_SALE_LEADS.".created_by, " ;
        $fields .= TABLE_SALE_LEADS.".lead_assign_to, ".TABLE_SALE_LEADS.".status " ;
        Leads::getList( $db, $list,$fields , $condition_query, $next_record, $rpp);
        
    	/*
		$phone          = new Phone(TABLE_SALE_LEADS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_SALE_LEADS, $list[$i]['lead_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}
        */
	
	
		
    $leadStatusArr =Leads::getLeadStatus();
    $leadStatusArr  = array_flip($leadStatusArr );
  
    $fList=array();
    if(!empty($list)){
        foreach( $list as $key=>$val){      
            
            $exeCreted=$exeCreatedname='';
            $table = TABLE_AUTH_USER;
            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by'] ."' " ;
            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_AUTH_USER .'.l_name';
            $exeCreted= getRecord($table,$fields1,$condition2);
        
            if(!empty($exeCreted)){
               
               $exeCreatedname = $exeCreted['f_name']." ".$exeCreted['l_name']."<br/>" ;
            }
            $val['created_by_name']    = $exeCreatedname ;     
            $leadstatus = $val['lead_status'];
            $val['lead_status']    = $leadStatusArr[$leadstatus] ;     
           
           $fList[$key]=$val;
        }
    }

        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
		$variables['can_view_tck']		= false;
		
        if ( $perm->has('nc_sl_ld_asig_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_sl_ld_asig_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_sl_ld_asig_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_sl_ld_asig_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_sl_ld_asig_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_sl_ld_asig_status') ) {
            $variables['can_change_status']     = true;
        }
        
		if ( $perm->has('nc_ldt') && $perm->has('nc_ldt_details') ) {
			$variables['can_view_tck'] = true;
		}
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-assign-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>