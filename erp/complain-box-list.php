<?php

    if ( $perm->has('nc_cb_list') ) {
		
	
        // If the entry is created by the my.
        $access_level   = $my['access_level'];
        
        // this permission is not added in d/b
        /*if ( $perm->has('nc_cb_list_al') ) {
            $access_level += 1;
        }
        
        if ( $where_added ) {
            $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= "  ( ";

        // If my has created this entry.
        $condition_query .= " ( ". TABLE_COMPLAIN_BOX .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_COMPLAIN_BOX .".access_level < $access_level ) ";*/       
        
         // Check if the User has the Right to view Orders created by other Users.
        /*
        if ( $perm->has('nc_cb_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_cb_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_COMPLAIN_BOX. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_COMPLAIN_BOX .".access_level < $access_level_o ) ";
        }*/
        //$condition_query .= ")";
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched'] = 1;
        
        
        // To count total records.
        $list	= 	NULL;
        $total	=	Complainbox::getDetails( $db, $list, '', $condition_query);
            
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    	$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
        $list	= NULL;
        $fields = TABLE_COMPLAIN_BOX .".*, CONCAT(".TABLE_USER.".f_name ,' ', ".TABLE_USER.".l_name ) as complain_of " ;
        Complainbox::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
          
         
               $statusArr = Complainbox::getStatus();
               $statusArr = array_flip($statusArr);
               $val['status'] = $statusArr[$val['status']];
               $priorityArr = Complainbox::getPriority();
               $priorityArr = array_flip($priorityArr);
               $val['priority'] = $priorityArr[$val['priority']];
               $exeCreted=$exeCreatedname='';
               $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               User::getList($db,$exeCreted,$fields1,$condition2);
               
               foreach($exeCreted as $key2=>$val2){
                    $exeCreatedname = $val2['f_name']." ".$val2['l_name']."<br/>" ;
               } 
               $val['created_by'] = $exeCreatedname;             
              
               $fList[$key]=$val;
            }
        }
        
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
       
        if ( $perm->has('nc_cb_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_cb_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_cb_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_cb_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_cb_details') ) {
            $variables['can_view_details'] = false;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'complain-box-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>
