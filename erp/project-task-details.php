<?php
//if ( $perm->has('nc_p_ts_details') ) {
if( $perm->has('nc_p_ts_list') ) {
		include_once ( DIR_FS_INCLUDES .'/st-template-category.inc.php');
		//include_once (DIR_FS_INCLUDES .'/project-priority.inc.php');
		//include_once (DIR_FS_INCLUDES .'/project-status.inc.php');
		//include_once (DIR_FS_INCLUDES .'/project-resolution.inc.php');
		include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
        include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
		include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
		
		$template_category = isset ($_GET['template_category']) ? $_GET['template_category'] : 
		( isset($_POST['template_category'] ) ? $_POST['template_category'] :'');
        $hideform = isset ($_GET['hideform']) ? $_GET['hideform'] : ( isset($_POST['hideform'] ) ? $_POST['hideform'] :0);
		include_once (DIR_FS_INCLUDES .'/project-module.inc.php');
		$moduleArr	= NULL;
		$condition_querym = " WHERE parent_id='0' AND ".TABLE_PROJECT_TASK_MODULE.".id > 871 AND ".TABLE_PROJECT_TASK_MODULE.".status = '".ProjectTask::ACTIVE."' ";
		ProjectModule::getList( $db, $moduleArr, 'id, title', $condition_querym);
		
		
		/* 
		If you want to use category un-comment me BOF
		$category_list	= 	NULL;
		$condition_query1 = " WHERE ".TABLE_ST_TEMPLATE_CATEGORY.".status = '".StTemplateCategory::ACTIVE."' 
		ORDER BY ".TABLE_ST_TEMPLATE_CATEGORY.".category ASC" ;
		StTemplateCategory::getList( $db, $category_list, TABLE_ST_TEMPLATE_CATEGORY.".category,".TABLE_ST_TEMPLATE_CATEGORY.".id", $condition_query1); 
		$stTemplates = null;
		if(!empty($template_category)){
			$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
			".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::WP.",%' AND 
			".TABLE_ST_TEMPLATE.".template_category = '".$template_category."'	ORDER BY title";
			SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'
			.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
		}
		If you want to use category un-comment me EOF
		*/
		
		$stTemplates = null;
		$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
		".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::WP.",%' ORDER BY title";
		SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'
		.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
		
		$posted_data = array();
    if((!empty($order['es_hrs']) && $order['st_date']!='0000-00-00 00:00:00' && 
		$order['es_ed_date']!='0000-00-00 00:00:00') || $perm->has('nc_p_ts_list_wohd')	){
		$lst_msg = ProjectTask::getStdMsg();
        $lst_filetype = ProjectTask::getFileType();
		//Hrs, Min array
        $lst_hrs = $lst_min = $lst_typ = NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
     // hrs, min. 
    $extra_url  =  $pagination='';

	if(array_key_exists('btnUpdate',$_POST)){
        $_ALL_POST1 	= $_POST;
        $data		= processUserData($_ALL_POST1);
        $temp = explode('/', $data['resolved_date']);                               
        $time =  mktime(0, 0, 0, $temp[1], $temp[0], $temp[2]);        
        $resolved_date = $datetime->unixTimeToDate($time);  
		
		$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
        $min1 = (int) ($data['min'] * HRS_FACTOR);
		
		
        $query = "UPDATE ".TABLE_PROJECT_TASK_DETAILS
                ." SET "
                ." title     	= '".$data['title']."',"	
                ." details     	= '".$data['details']."',"	
                ." internal_comment     	= '".$data['internal_comment']."',"	
                ." files_link     	= '".$data['files_link']."',"	
                ." files_link2     	= '".$data['files_link2']."',"	
                ." hrs     		= '".$data['hrs']."',"	
                ." min     		= '".$data['min']."',"	
				." hrs1  		= '".$hrs1."',"
				." min1  		= '".$min1."',"
                ." status_id    = '".$data['status_id']."',"	
                ." priority_id  = '".$data['priority_id']."',"
                ." resolution_id= '".$data['resolution_id']."',"
                ." resolved_date= '".$resolved_date."' WHERE id = ".$data['id'] ;
       
        if ( $db->query($query) && $db->affected_rows() > 0 ) {
            $messages->setOkMessage("Project Task has been updated Successfully.");
        }
    }
	
	$condition_queryd = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".id=".$id." AND ".TABLE_PROJECT_TASK_DETAILS.".project_id=".$or_id ;
    $details	= NULL;
    $fields = TABLE_PROJECT_TASK_DETAILS.".*, ".TABLE_PROJECT_TASK_MODULE.".title as module_title,";
	//.TABLE_PROJECT_TASK_STATUS.".title as status_title,".TABLE_PROJECT_TASK_PRIORITY.".title as priority_title, ".TABLE_PROJECT_TASK_RESOLUTION.".title as rs_title,"
    $fields .= TABLE_PROJECT_TASK_TYPE.".title as type_title," ;
    $fields .= TABLE_PROJECT_TASK_DETAILS.'.is_client' ; 
    ProjectTask::getList( $db, $details, $fields, $condition_queryd);
    if(!empty($details) ){      
        $details = $details[0];
		if($details['is_flw']==1){ //This is followup task
			$flw_task = 1;
		}
	}
	 
    if ( isset($_POST['btnCreate'])  && $_POST['act'] == 'save') {
          
            $posted_data = processUserData($_POST);
            $files		= processUserData($_FILES);
             
            $extra = array( 'db' 				=> &$db,
                         	'files'          => &$files,
                         	'messages'          => &$messages
                        );
            $posted_data['allowed_file_types'] = $allowed_file_types ;
          //$posted_data['max_file_size']= MAX_MULTIPLE_FILE_SIZE ;
            $posted_data['max_file_size']= MAX_FILE_SIZE ; 
              
            //if ( ProjectTask::validateCommentAdd($posted_data, $extra) ) { 
            if ( ProjectTask::validateCommentDailyAdd($posted_data, $extra) ) { 
                /*  
			    if(empty($posted_data['details'])){
                    $posted_data['details'] = $lst_msg[$posted_data['defaultMsg']];
                } */
               //$lst_filetype                
                $i=1; 
				$attachfilename='';
				if(!empty($files["attached_file"]["name"])){
					//$val["name"] = str_replace(' ','-',$val["name"]);
					$filedata["extension"]='';
					$filedata = pathinfo($files["attached_file"]["name"]); 
					$ext = $filedata["extension"] ; 
					//$attachfilename = $username."-".mktime().".".$ext ;
					$attachfilename = mktime()."-".$i.".".$ext ;
				   if(!empty($files["attached_file"]["name"])){                 
						if (move_uploaded_file($files["attached_file"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename, 0777);
							$attached_type = $files["attached_file"]["type"] ;
						}
					}
					$i++;
				}
				if(!empty($files["attached_file1"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file1"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename1 = mktime()."-file1".".".$ext ;
					if(!empty($files["attached_file1"]["name"])){                 
						if (move_uploaded_file($files["attached_file1"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename1)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename1, 0777); 
						}
					} 
				}
				if(!empty($files["attached_file2"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file2"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename2 = mktime()."-file2".".".$ext ;
					if(!empty($files["attached_file2"]["name"])){                 
						if (move_uploaded_file($files["attached_file2"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename2)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename2, 0777); 
						}
					} 
				}
				if(!empty($files["attached_file3"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file3"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename3 = mktime()."-file3".".".$ext ;
					if(!empty($files["attached_file3"]["name"])){                 
						if (move_uploaded_file($files["attached_file3"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename3)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename3, 0777); 
						}
					} 
				}
				if(!empty($files["attached_file4"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file4"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename4 = mktime()."-file4".".".$ext ;
					if(!empty($files["attached_file4"]["name"])){                 
						if (move_uploaded_file($files["attached_file4"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename4)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename4, 0777); 
						}
					} 
				}
				if(!empty($files["attached_file5"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file5"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename5 = mktime()."-file5".".".$ext ;
					if(!empty($files["attached_file5"]["name"])){                 
						if (move_uploaded_file($files["attached_file5"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename5)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename5, 0777); 
						}
					} 
				}
				if(!empty($files["attached_file6"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file6"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename6 = mktime()."-file6".".".$ext ;
					if(!empty($files["attached_file6"]["name"])){                 
						if (move_uploaded_file($files["attached_file6"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename6)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename6, 0777); 
						}
					} 
				}
				if(!empty($files["attached_file7"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file7"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename7 = mktime()."-file7".".".$ext ;
					if(!empty($files["attached_file7"]["name"])){                 
						if (move_uploaded_file($files["attached_file7"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename7)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename7, 0777); 
						}
					} 
				}
				if(!empty($files["attached_file8"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file8"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename8 = mktime()."-file8".".".$ext ;
					if(!empty($files["attached_file8"]["name"])){                 
						if (move_uploaded_file($files["attached_file8"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename8)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename8, 0777); 
						}
					} 
				}
				if(!empty($files["attached_file9"]["name"])){
					$filedata["extension"] = '';
					$filedata = pathinfo($files["attached_file9"]["name"]); 
					$ext = $filedata["extension"] ; 
					$attachfilename9 = mktime()."-file9".".".$ext ;
					if(!empty($files["attached_file9"]["name"])){                 
						if (move_uploaded_file($files["attached_file9"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename9)){
							@chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename9, 0777); 
						}
					} 
				}
				
				if($posted_data['dear']==1){
					$posted_data['details']="<b>Dear Sir</b>, <br/><br/>".$posted_data['details'] ;  
				}elseif($posted_data['dear']==2){
					$posted_data['details']="<b>Dear Madam</b>, <br/><br/>".$posted_data['details'] ; 
				}elseif($posted_data['dear']==3){
					$posted_data['details']="<b>Dear Sir / Madam</b>, <br/><br/>".$posted_data['details'] ;
				}elseif($posted_data['dear']==4){
					$posted_data['details']= "<b>".$posted_data['dear_text']."</b><br/><br/>".$posted_data['details'] ;
				}
				
                $hrs1 = (int) ($posted_data['hrs'] * HRS_FACTOR);
                $min1 = (int) ($posted_data['min'] * HRS_FACTOR);   
                //Mark send notify BOF   
						
                $mail_client=0;
				$is_visibleto_client = 0;     
                if(isset($_POST['mail_client']) ){
                    $mail_client=1;
					$is_visibleto_client = 1;
                }
                $mail_staff=0;
                if(isset($_POST['mail_staff']) ){
                    $mail_staff=1;
                }
                $mark_imp=0;
                if(isset($_POST['mark_imp']) ){
                    $mark_imp=1;
                }
				$mail_ceo=0;
				if(isset($_POST['mail_ceo'])){
                    $mail_ceo = 1;
                }
				$posted_data['design_counter']='';
                $posted_data['file_counter']=0;
                //Get counter of filetypes BOF
                /* if(!empty($attachfilename) ){
                    $sql= " SELECT MAX(file_counter) as count FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE 
                        project_id ='".$posted_data['or_id']."' AND is_bug='0' AND file_type='".$posted_data['file_type']."'";
                    if ( $db->query($sql) ) {
                        if ( $db->nf() > 0 ) {
                            while ($db->next_record()) {
                                $posted_data['file_counter'] = $db->f('count') + 1;
                                
                            }
                        }
                    }
                    if($data['file_type']!=ProjectTask::OTHER){
                        $filetype= $posted_data['file_type'];
                        //$posted_data['design_counter'] = $lst_filetype[$filetype]." ".$posted_data['file_counter'];
                    }
                }    */
                //Get counter of filetypes EOF 
                //display random name BOF 
				
				$data['display_name']=$data['display_user_id']=$data['display_designation']='';
				//if($mail_to_all_su==1 || $mail_to_additional_email==1 || $mail_client==1){
					$support_team = '';
					if(!empty($order['wp_support_team'])){
					 	$wp_support_team = trim($order['wp_support_team'],",") ;
						$wp_support_team_arr = explode(',', $wp_support_team) ;
						$no = array_rand($wp_support_team_arr, 1);
						$support_team = $wp_support_team_arr[$no];
					}
					$randomUser = getRandomAssociate($support_team);
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
				//}
				//disply random name EOF	
				$bworked_min=0;
				if(!empty($posted_data['behalf_executive'])){
					$table = TABLE_USER;
					$condition2 = " WHERE ".TABLE_USER .".user_id = '".$posted_data['behalf_executive'] ."' LIMIT 0,1 " ;
					$fields1 =  TABLE_USER .'.current_salary';
					$exeCreted = getRecord($table,$fields1,$condition2);                
					$behalfCurrentSalary = $exeCreted['current_salary'] ;					 
					$per_hour_salary = $behalfCurrentSalary / STD_WORKING_HRS;
					$per_hour_salary =  number_format($per_hour_salary,2,'.', '');
					$worked_min = $bworked_min = ( $posted_data['hrs']*60 ) + $posted_data['min'] ;
					$worked_hour = ($worked_min/60) ;
					$worked_hour =  number_format($worked_hour,2,'.', '');
					$worked_hour_salary = ( $per_hour_salary * $worked_hour);		 
					$query = "INSERT INTO ".TABLE_PROJECT_TASK_DETAILS
							  ." SET "
							  ." details      	= '".$posted_data['details']."',"	 
							  ." hrs             = '".$posted_data['hrs']."',"
							  ." min            = '".$posted_data['min']."',"
							  ." hrs1            = '".$hrs1."',"
							  ." min1           = '".$min1."',"
							  ." current_salary  = '".$behalfCurrentSalary."',"
							  ." per_hour_salary  = '".$per_hour_salary."',"
							  ." worked_hour_salary  = '".$worked_hour_salary."',"
							  ." last_updated 		= '".$last_updated."',"
							  ." added_by      		= '".$posted_data['behalf_executive']."',"
							  ." added_by_name     	= '".$exeCreted['f_name']." ".$exeCreted['l_name']."',"
							  ." behalf_added_by      	= '".$my['user_id']."',"
							  ." behalf_added_by_name   = '".$my['f_name']." ".$my['l_name']."',"
							  ." parent_id      = '".$posted_data['id']."',"
							  ." project_id      = '".$posted_data['or_id']."',"
							  ." is_bug       	= '0',"
							  ." file_type      = '".$posted_data['file_type']."',"
							  ." file_counter   = '".$posted_data['file_counter']."',"
							  //." attached_file	= '".$attachfilename."',"			
							  ." attached_file	= '".$attachfilename."',"			
							  ." attached_file1	= '".$attachfilename1."',"			
							  ." attached_file2	= '".$attachfilename2."',"			
							  ." attached_file3	= '".$attachfilename3."',"			
							  ." attached_file4	= '".$attachfilename4."',"			
							  ." attached_file5	= '".$attachfilename5."',"			
							  ." attached_file6	= '".$attachfilename6."',"			
							  ." attached_file7	= '".$attachfilename7."',"			
							  ." attached_file8	= '".$attachfilename8."',"			
							  ." attached_file9	= '".$attachfilename9."',"			
							  ." attached_type	= '".$attached_type."',"                              
							  ." attached_desc  = '".$data['attached_desc']."',"  
							  ." display_name  = '".$data['display_name']."',"
                              ." display_user_id  = '".$data['display_user_id']."',"
                              ." display_designation  = '".$data['display_designation']."',"
                              ." file_upload_path  = '".$posted_data['file_upload_path']."', "
							  ." is_visibleto_client = '".$is_visibleto_client."', "
                              ." mail_client = '".$mail_client."',"
							  ." mail_staff = '".$mail_staff."',"
							  ." mark_imp = '".$mark_imp."',"
							  ." mail_ceo = '".$mail_ceo."',"
							  ." ip  	        = '".$_SERVER['REMOTE_ADDR']."', "
							  ." do_e 	        = '".date('Y-m-d H:i:s')."'" ;
					$db->query($query);
					//As entry was onbehalf added by my BOF so by default one entry of 10min by my
					$posted_data['hrs'] = 0;
					$posted_data['min'] = 10;
					$hrs1 = (int) ($posted_data['hrs'] * HRS_FACTOR);
					$min1 = (int) ($posted_data['min'] * HRS_FACTOR);
					//As entry was onbehalf added by my EOF
				} 
				$current_salary = $my['current_salary'];
				$per_hour_salary = $my['current_salary'] / STD_WORKING_HRS;
				$per_hour_salary =  number_format($per_hour_salary,2,'.', '');
				$worked_min = ( $posted_data['hrs']*60 ) + $posted_data['min'] ;
				$worked_hour = ($worked_min/60) ;
				$worked_hour =  number_format($worked_hour,2,'.', '');
				$worked_hour_salary = ( $per_hour_salary * $worked_hour);			
				$query = "INSERT INTO ".TABLE_PROJECT_TASK_DETAILS
						  ." SET "
						  ." details      	= '".$posted_data['details']."',"	
						  ." internal_comment   = '".$posted_data['internal_comment']."',"	
						  ." current_status   	= '".$posted_data['current_status']."',"	
						  ." pending_frm_client = '".$posted_data['pending_frm_client']."',"	
						  ." est_completion_dt  = '".$posted_data['est_completion_dt']."',"	
						  ." hrs             = '".$posted_data['hrs']."',"
						  ." min            = '".$posted_data['min']."',"
						  ." hrs1            = '".$hrs1."',"
						  ." min1           = '".$min1."',"
						  ." current_salary  = '".$current_salary."',"
						  ." per_hour_salary  = '".$per_hour_salary."',"
						  ." worked_hour_salary  = '".$worked_hour_salary."',"
						  ." last_updated 	= '".$last_updated."',"
						  ." added_by      	= '".$my['user_id']."',"							 
						  ." added_by_name  = '".$my['f_name']." ".$my['l_name']."',"								 
						  ." parent_id      = '".$posted_data['id']."',"
						  ." project_id      = '".$posted_data['or_id']."',"
						  ." is_bug       	= '0',"
						  ." file_type      = '".$posted_data['file_type']."',"
						  ." file_counter   = '".$posted_data['file_counter']."',"
						//." attached_file	= '".$attachfilename."',"	
						  ." attached_file	= '".$attachfilename."',"			
						  ." attached_file1	= '".$attachfilename1."',"			
						  ." attached_file2	= '".$attachfilename2."',"			
						  ." attached_file3	= '".$attachfilename3."',"			
						  ." attached_file4	= '".$attachfilename4."',"			
						  ." attached_file5	= '".$attachfilename5."',"			
						  ." attached_file6	= '".$attachfilename6."',"			
						  ." attached_file7	= '".$attachfilename7."',"			
						  ." attached_file8	= '".$attachfilename8."',"			
						  ." attached_file9	= '".$attachfilename9."',"		 
						  ." attached_type	= '".$attached_type."',"                              
						  ." attached_desc  = '".$posted_data['attached_desc']."',"
						  ." template_id  = '".$posted_data['template_id']."',"
						  ." template_file_1  = '".$posted_data['template_file_1']."',"
						  ." template_file_2  = '".$posted_data['template_file_2']."',"
						  ." template_file_3  = '".$posted_data['template_file_3']."',"
						  ." display_name  = '".$data['display_name']."',"
						  ." display_user_id  = '".$data['display_user_id']."',"
						  ." display_designation  = '".$data['display_designation']."',"
						  ." file_upload_path  = '".$posted_data['file_upload_path']."', "
						  ." is_visibleto_client = '".$is_visibleto_client."', "
						  ." mail_client = '".$mail_client."',"
						  ." mail_staff = '".$mail_staff."',"
						  ." mark_imp = '".$mark_imp."',"
						  ." mail_ceo = '".$mail_ceo."',"
						  ." ip  	        = '".$_SERVER['REMOTE_ADDR']."', "
						  ." do_e 	        = '".date('Y-m-d H:i:s')."'" ;
				 $db->query($query);				 
                 $variables['hid'] = $db->last_inserted_id();
                //Update Main task to get last task comment and other details bof
                 $sql = " UPDATE ".TABLE_PROJECT_TASK_DETAILS." SET last_comment = '".$posted_data['details']."',
                        last_comment_by  	= '".$my['user_id']."', 
                        last_comment_by_name  	= '".$my['f_name']." ".$my['l_name']."', 
                        last_comment_dt  	= '".date('Y-m-d H:i:s')."' 
                        WHERE id ='".$posted_data['id']."'"  ;
                 $db->query($sql);
                //Update Main task to get last task comment and other details eof
                $messages->setOkMessage("Comment posted successfully.");
				
                $totHrs = $order['tot_thrs'] * 60  ;
				$totMin = $order['tot_tmin'] + $bworked_min + $worked_min ;
				$totalMin = $totHrs + $totMin  ;
                $totalEstMin = $order['tot_est_min'] ;
				if($totalMin > $totalEstMin){
					//SEND MAIL TO ADMIN BOF EXCEED 
					$email_data['order_title'] = $order['order_title'] ;
					$email_data['client_name'] = $order['f_name']." ".$order['l_name']." ".$order['billing_name'] ;
					$team_mem_str ='';
					if(!empty($team_members )){
						foreach($team_members  as $keyt=>$valt){
							$team_mem_str.= $valt['f_name']." ".$valt['l_name']."<br/> ";
						}
					}
					$team_mem_str = trim($team_mem_str,"<br/>");
					$email_data['team_members'] = $team_mem_str ;
					$email_data['est_hrs'] =  $order['es_hrs'] ;
					$thrs = (int) ($totalMin/60);
					$tmin = (int)($totalMin%60);
					$email_data['total_worked_hrs'] = $thrs.":".$tmin ;
					$exceed_min = $totalMin - $totalEstMin ;
					$ehrs = (int)($exceed_min/60);
					$emin = (int)($exceed_min%60);
					$email_data['exceed_hrs'] =  $ehrs.":".$emin ; 	
					/* if ( getParsedEmail($db, $s, 'TASK_NOTE_HRS_EXCEED', $email_data, $email) ) {
						$to1     = '';
						$to1[]   = array('name' => $admin_name, 'email' => $admin_email); 
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						SendMailSupport($to1, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
						
					} */
					//SEND MAIL TO ADMIN EOF
				}
                //Send notification BOF    
                    $data1=null;
                    $condition_query = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".id='".$posted_data['id']."'" ;
                    $fields = TABLE_PROJECT_TASK_DETAILS.".id, ".TABLE_PROJECT_TASK_DETAILS.".project_id, 
					".TABLE_PROJECT_TASK_DETAILS.".added_by,
					".TABLE_PROJECT_TASK_DETAILS.".sub_module_id,
                    ".TABLE_PROJECT_TASK_DETAILS.".is_client, ".TABLE_PROJECT_TASK_DETAILS.".title, 
                    ".TABLE_PROJECT_TASK_DETAILS.".details,".TABLE_PROJECT_TASK_DETAILS.".last_updated,"
                    .TABLE_PROJECT_TASK_MODULE.".title as module" ;
                    /* $fields .= TABLE_PROJECT_TASK_STATUS.".title as status,".TABLE_PROJECT_TASK_PRIORITY.".title as priority, ".TABLE_PROJECT_TASK_TYPE.".title as type,".TABLE_PROJECT_TASK_RESOLUTION.".title as resolution " ; */
                    ProjectTask::getList( $db, $data1, $fields, $condition_query); 
                    if(!empty($data1) ){      
                        $data = $data1[0];
						$data['title'] =$data['module'] ;
						if(!empty($data['sub_module_id'])){
							$table = TABLE_PROJECT_TASK_MODULE;
                            $condition2 = " WHERE ".TABLE_PROJECT_TASK_MODULE .".id= '".$data['sub_module_id'] ."' " ;
                            $fields1 =  TABLE_PROJECT_TASK_MODULE .'.title as submodule' ;
                            $subModArr= getRecord($table,$fields1,$condition2);
							if(!empty($subModArr)){
								$data['title'].=" - ".$subModArr['submodule'];
							}
						}	
                        if($data['is_client']=='1'){
                            $table = TABLE_CLIENTS;
                            $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$data['added_by'] ."' " ;
                            $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                            $exeCreted= getRecord($table,$fields1,$condition2);
                            $data['af_name'] =$exeCreted['f_name'];
                            $data['al_name'] = $exeCreted['l_name'];
                        }else{
                            $table = TABLE_USER;
                            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['added_by'] ."' " ;
                            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                            $exeCreted= getRecord($table,$fields1,$condition2);
                            $data['af_name'] =$exeCreted['f_name'];
                            $data['al_name'] = $exeCreted['l_name'] ;
                        }
                        
                        $data['cm_doe'] = date('Y-m-d H:i:s');
                        $data['cdetails'] = $posted_data['details'];
                    }
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
                    $data['project_title'] =$order['order_title'];
                    $data['images_nc']=DIR_WS_IMAGES_NC;
                    $data['design_counter']= $posted_data['design_counter']; 
                    $data['task_title']= $details['title']; 
					
                    $cc_to_sender= $cc = $bcc = Null;
					if(!empty($order['wp_support_team'])){ 
						include_once (DIR_FS_INCLUDES .'/user.inc.php');
						$production_team='';
						$wp_support_team_str = str_replace(",","','",$order['wp_support_team']) ; 
						$sql= " SELECT f_name,l_name,desig FROM ".TABLE_USER." WHERE user_id IN('".$wp_support_team_str."') AND ".TABLE_USER.".status='".User::ACTIVE."'";
						$db->query($sql);
						if ( $db->nf()>0 ) {
							while ( $db->next_record() ) { 
								$production_team .= "<div>".$db->f('f_name')." ".$db->f('l_name')." <span style=\"font-family: arial,sans-serif; font-size: 10px; color: rgb(102, 102, 102);\">(".$db->f('desig').")</span></div>";
							} 
						}
						$data['production_team']=$production_team ;				
					}
					$daily_update_team = DAILY_WORK_UPDATE_TELEPHON_TEAM;
					if(!empty($daily_update_team)){ 
						$telephonic_team='';
						$wp_support_team_str = str_replace(",","','",$daily_update_team) ; 
						$sql= " SELECT f_name,l_name,desig FROM ".TABLE_USER." WHERE user_id 
							IN('".$wp_support_team_str."') AND ".TABLE_USER.".status='".User::ACTIVE."'";
						$db->query($sql);
						if ( $db->nf()>0 ) {
							while ( $db->next_record() ) { 
								$telephonic_team .= "<div>".$db->f('f_name')." ".$db->f('l_name')." <span style=\"font-family: arial,sans-serif; font-size: 11px;color: rgb(102, 102, 102);\"\">&nbsp;&nbsp;(".$db->f('desig').")</span></div>";  
								//$telephonic_team .= "<div>".$db->f('f_name')." ".$db->f('l_name')." (".$db->f('desig').")</div>";
							} 
						}
						$data['telephonic_team']=$telephonic_team ;				
					} 
					$data['support_contact']= "<span style=\"font-family: arial,sans-serif; font-size: 11px; line-height: 14px; padding-top: 15px; color: rgb(102, 102, 102);\">ASIA &amp; OTHERS:&nbsp;&nbsp; +91- 9823066661, 9823066662, 9823066663, 9823066664<br/>USA & EUROPE:&nbsp;&nbsp;&nbsp; 1-837-621-1000 <br/>EMAIL:&nbsp;&nbsp;&nbsp; Support@SMEERPTech.in</span>"; 	
					
					$data['internal_comment'] = $posted_data['internal_comment'];
					$data['current_status'] = $posted_data['current_status'];
					$data['pending_frm_client'] =$posted_data['pending_frm_client'];
					$data['est_completion_dt'] =$posted_data['est_completion_dt'];
					//get the subclients allotted to the order bof
					$ord_details=null;
					$fields = TABLE_BILL_ORDERS.'.id, '.TABLE_BILL_ORDERS.'.clients_su, '.TABLE_BILL_ORDERS.'.number' ;
					$condition_query_ord = " WHERE ".TABLE_BILL_ORDERS.".id = ".$posted_data['or_id'] ;
					$total	=	Order::getDetails( $db, $ord_details, $fields, $condition_query_ord);
					$order_details =  $ord_details[0];
					$clients_su = trim($order_details['clients_su'],",") ;
					$clients_su_str='';
					if(!empty($clients_su)){
						$clients_su_str =  str_replace(",","','",$clients_su);
					}
					$data['order_no']= $order_details['number']; 
					//get the subclients allotted to the order eof
					/* if(!empty($posted_data['template_file_1'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_1'];
					}
					if(!empty($posted_data['template_file_2'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_2'];
					}
					if(!empty($posted_data['template_file_3'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_3'];
					} */
					if(!empty($attachfilename)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename;
					}
					if(!empty($attachfilename1)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename1;
					}
					if(!empty($attachfilename2)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename2;
					}
					if(!empty($attachfilename3)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename3;
					}
					if(!empty($attachfilename4)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename4;
					}
					if(!empty($attachfilename5)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename5;
					}
					if(!empty($attachfilename6)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename6;
					}
					if(!empty($attachfilename7)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename7;
					}
					if(!empty($attachfilename8)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename8;
					}
					if(!empty($attachfilename9)){
						$file_name[] = DIR_FS_ATTACHMENTS_FILES ."/". $attachfilename9;
					}
					if(!empty($posted_data['template_file_1'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_1'];
					}
					if(!empty($posted_data['template_file_2'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_2'];
					}
					if(!empty($posted_data['template_file_3'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $posted_data['template_file_3'];
					} 
					$mail_send_to ='';
					if($mail_ceo==1){
						$to[]   = array('name' =>CEO_USER_EMAIL,'email' => CEO_USER_EMAIL);
						$mail_send_to .=",".CEO_USER_EMAIL ;
					}
                    // Send Email to the Client BOF  
					$data['project_title'] = !empty($order['order_title_mail']) ? $order['order_title_mail'] : $order['order_title'];
   					
                    if(isset($posted_data['mail_client']) ){
						
						 $data['link']   = DIR_WS_MP .'/project-task.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'];
                         $subUserDetails=null;
						 if(!empty($clients_su_str)){
							//Clients::getList($db, $subUserDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE parent_id = '".$order['client']."' AND status='".Clients::ACTIVE."' AND user_id IN('".$clients_su_str."') ");                   
							Clients::getList($db, $subUserDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE status='".Clients::ACTIVE."' AND user_id IN('".$clients_su_str."') ");                   
						 }
						 Clients::getList($db, $clientDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE user_id = '".$order['client']."'");
                         if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['c_title'] =$client['title'];
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;                        
                            $data['cm_by_f_name'] =$my['f_name'];
                            $data['cm_by_l_name'] = $my['l_name'] ;
                            $email = NULL;
   
                            if(isset($posted_data['mark_imp']) ){  
                                /*
								if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_CLIENT_IMP', $data, $email) ) { 
    								$mail_send_to .=",".$client['email']; 
									$to     = '';
                                    $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"] ,$cc_to_sender,$cc,$bcc,$file_name);
                                    if(!empty($client['email_1'])){
                                        $mail_send_to .=",".$client['email_1']; 
										$to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_1']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                    if(!empty($client['email_2'])){
										$mail_send_to .=",".$client['email_2']; 
                                        $to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_2']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    } 
                                }
								*/ 
                                
                                // send mail to sub users bof
                                /* if(!empty($subUserDetails)){                                        
                                    foreach ($subUserDetails  as $key=>$value){
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        $data['c_title'] = $subUserDetails[$key]['title'];                            
                                        $data['cf_name'] = $subUserDetails[$key]['f_name'];
                                        $data['cl_name'] = $subUserDetails[$key]['l_name'] ;
                                        if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_CLIENT_IMP', $data, $email) ) {
                                            $mail_send_to .=",".$subUserDetails[$key]['email'];
											//$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email'], 
											'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                            
											if(!empty($subUserDetails[$key]['email_1'])){
												//$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['email_1'], 
												'email' => $subUserDetails[$key]['email_1']); 
												$mail_send_to .=",".$subUserDetails[$key]['email_1'];                                
												 $from   = $reply_to = array('name' => $email["from_name"], 
												 'email' => $email['from_email']);
												 
											}
											if(!empty($subUserDetails[$key]['email_2'])){
												//$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['email_2'], 
												'email' => $subUserDetails[$key]['email_2']);
                                            	$mail_send_to .=",".$subUserDetails[$key]['email_2'];                                
												$from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
											 
											}
										
										}
                                    }
                                } */
                                // send mail to sub users bof
								//Send a copy to check the mail for clients
								/* 
								if(!empty($smeerp_client_email)){
									//$to     = '';
									$bcc[]   = array('name' => $client['f_name'].' '. $client['l_name'], 
									'email' => $smeerp_client_email);
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
									 SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);  
								}  */
                                
                            }else{
								 
                                // send mail to sub users bof
                                if(!empty($subUserDetails)){                                        
                                    foreach ($subUserDetails  as $key=>$value){                                        
										if(!empty($subUserDetails[$key]['email'])){
											$to[]   = array('name' => $subUserDetails[$key]['email'], 
											'email' => $subUserDetails[$key]['email']);
											$mail_send_to .=",".$subUserDetails[$key]['email'];   
										}
										if(!empty($subUserDetails[$key]['email_1'])){
											$to[]   = array('name' => $subUserDetails[$key]['email_1'], 
											'email' => $subUserDetails[$key]['email_1']);
											$mail_send_to .=",".$subUserDetails[$key]['email_1'];   
										}
										if(!empty($subUserDetails[$key]['email_2'])){
											$to[]   = array('name' => $subUserDetails[$key]['email_2'], 
											'email' => $subUserDetails[$key]['email_2']);
											$mail_send_to .=",".$subUserDetails[$key]['email_2'];   
										}
										
										/* $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        $data['c_title'] =$subUserDetails[$key]['title'];                            
                                        $data['cf_name'] =$subUserDetails[$key]['f_name'];
                                        $data['cl_name'] = $subUserDetails[$key]['l_name'] ;
                                        if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_CLIENT', $data, $email) ) {
                                            $mail_send_to .=",".$subUserDetails[$key]['email'];
										    //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email'], 
											'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                            //echo $email["body"]; 
											if(!empty($subUserDetails[$key]['email_1'])){
												//$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['email_1'], 
												'email' => $subUserDetails[$key]['email_1']);
                                            	$mail_send_to  .=",".$subUserDetails[$key]['email_1'];                                
											    $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												 
											}
											if(!empty($subUserDetails[$key]['email_2'])){
												//$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['email_2'],
												'email' => $subUserDetails[$key]['email_2']);
                                            	$mail_send_to .=",".$subUserDetails[$key]['email_2'];                                
												$from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												 
											}
										} */
                                    }
									
                                }
                                // send mail to sub users bof
                            }
                        }
                    } 
					//Send a copy to check the mail for clients
					/* 
					if(!empty($to)){
						getParsedEmail($db, $s, 'TASK_NOTE_FOR_CLIENT', $data, $email) ;
						//$to     = '';
						$bcc[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $smeerp_client_email);
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); 
					}
					*/ 

					if(!empty($to) && !empty($posted_data['details'])){
						
						getParsedEmail($db, $s, 'PROJECT_DAILYWORK_UPDATE', $data, $email) ;
						$bcc[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $smeerp_client_email);
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						/* 	echo  $email["subject"];
						echo "<br/>";
						echo  $email["body"]; */
						SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); 
						 
						/*
						$project_title $cdetails $display_name $display_designation
						$current_status $pending_frm_client $telephonic_team {$production_team
						$support_contact
						 $est_completion_dt
						*/ 
						$messages->setOkMessage("Mail Sent Successfully on Order - ".$data['project_title']." - Client - ".$data['client_name']);   
					}
					$sql = " UPDATE ".TABLE_PROJECT_TASK_DETAILS." SET 
						last_comment = '".$posted_data['details']."',
						last_internal_comment = '".$posted_data['internal_comment']."',						
                        last_comment_by  	= '".$my['user_id']."', 
                        last_comment_by_name  	= '".$my['f_name']." ".$my['l_name']."', 
                        last_comment_dt  	= '".date('Y-m-d H:i:s')."' 
                        WHERE id ='".$posted_data['id']."'"  ;
					$db->query($sql); 
                    // Send Email to the Client EOF
                    //Set the list subuser's email-id to whom the mails are sent bof   
					if(!empty($mail_send_to)){ 
						$query_update = " UPDATE ". TABLE_PROJECT_TASK_DETAILS." 
										SET ". TABLE_PROJECT_TASK_DETAILS .".mail_send_to_su  = '".trim($mail_send_to ,",")."' 
										WHERE ". TABLE_PROJECT_TASK_DETAILS .".id = ". $variables['hid'] ." " ;  
						$db->query($query_update) ;
						
						$query_update = "UPDATE ". TABLE_BILL_ORDERS 
						." SET ".TABLE_BILL_ORDERS .".daily_update  = '".$posted_data['details']."',
						". TABLE_BILL_ORDERS .".daily_update_dt  = '".date('Y-m-d h:i:s')."',
						". TABLE_BILL_ORDERS .".daily_update_by  = '".$my['user_id']."',
						". TABLE_BILL_ORDERS .".daily_update_by_name  = '".$my['f_name']." ".$my['l_name']."'						
						 WHERE ". TABLE_BILL_ORDERS .".id = ". $posted_data['or_id'] ." " ;	
						$db->query($query_update) ; 
					}
					
					$sql_sub0=$sql_sub1=$sql_sub2=$sql_sub3='';
					if(!empty($posted_data['internal_comment'])){
						$sql_sub0 = " daily_update_internal = '".$posted_data['internal_comment']."',";
					}
					if(!empty($posted_data['est_completion_dt'])){
						$sql_sub1= " last_est_completion_dt  = '".$posted_data['est_completion_dt']."',";
					}
					if(!empty($posted_data['current_status'])){
						$sql_sub2= " last_current_status  = '".$posted_data['current_status']."',";
					}
					if(!empty($posted_data['pending_frm_client'])){
						$sql_sub3= " last_pending_frm_client  = '".$posted_data['pending_frm_client']."',";
					} 
					if(!empty($posted_data['internal_comment']) || !empty($posted_data['est_completion_dt']) || !empty($data['current_status']) 
						|| !empty($posted_data['pending_frm_client']) ){
						$sql_sub = $sql_sub0.$sql_sub1.$sql_sub2.$sql_sub3 ;
						$sql_sub = trim($sql_sub,",");
						
						$query_update = "UPDATE ". TABLE_BILL_ORDERS 
						." SET ".$sql_sub."	WHERE ". TABLE_BILL_ORDERS .".id = ". $posted_data['or_id'] ." " ;
						$db->query($query_update) ; 
					}					
                    // Send Email to the Staff BOF                    
                    if(isset($posted_data['mail_staff']) ){
                        $data['link']   = DIR_WS_NC .'/project-task.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'];
                        //include ( DIR_FS_INCLUDES .'/client.inc.php');
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                        if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;   
                        }
                        $data['cm_by_f_name'] =$my['f_name'];
                        $data['cm_by_l_name'] = $my['l_name'];    
                        if(!empty($team_members)){
                            foreach ( $team_members as $user ) {
                                $data['uf_name']    = $user['f_name'];
                                $data['ul_name']    = $user['l_name'];
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_STAFF', $data, $email) ) {
                                    $to     = '';
									$bcc ='';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                }
                            }                                 
                        }
                    }                 
                    // Send Email to the senior AS IMP BOF  
					/*
                    if(isset($posted_data['mark_imp']) ){                    
						$data['link']   = DIR_WS_NC .'/project-task.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'];
						//include ( DIR_FS_INCLUDES .'/client.inc.php');
						Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
						if(!empty($clientDetails)){
							$client = $clientDetails['0'];                       
							$data['cf_name'] =$client['f_name'];
							$data['cl_name'] = $client['l_name'] ;    
						}
						$data['cm_by_f_name'] = $my['f_name'];
						$data['cm_by_l_name'] = $my['l_name'] ;      
						if( getParsedEmail($db, $s, 'TASK_NOTE_FOR_STAFF_IMP', $data, $email) ) {
							$to     = '';
							$to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);                                    
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
						} 
                    }else{ 
                        $data['link'] = DIR_WS_NC .'/project-task.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'];
                        //include ( DIR_FS_INCLUDES .'/client.inc.php');
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                        if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] = $client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;       
                        }
                        $data['cm_by_f_name'] = $my['f_name'];
                        $data['cm_by_l_name'] = $my['l_name'] ;
                        // if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_STAFF', $data, $email) ) {
                        //  $to     = '';
                        //    $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);   
                        //    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']); 
                        //    SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                        //}  
                    }*/
					 
					$posted_data1['internal_comment'] =	$posted_data['internal_comment'] ;
					$posted_data1['current_status'] = $posted_data['current_status'] ;
					$posted_data1['pending_frm_client'] = $posted_data['pending_frm_client'] ;
					$posted_data1['est_completion_dt'] = $posted_data['est_completion_dt'] ;
					 
					$posted_data=array();
					$posted_data = $posted_data1;
                    // Send Email to the Staff BOF  
                    /* header("Location:".DIR_WS_NC."/project-task.php?perform=details&added=1&id=".$posted_data['id']."&or_id=".$posted_data['or_id']."&actual_time=".$posted_data['actual_time']); */ 
            }
    }else{
		$posted_data['internal_comment'] = $order['daily_update_internal'];
		$posted_data['current_status'] = $order['last_current_status'];
		$posted_data['pending_frm_client'] = $order['last_pending_frm_client'];
		$posted_data['est_completion_dt'] = $order['last_est_completion_dt']; 
	}
    
    //Get list of sub threads BOF
    $list	= 	NULL;
	$flw_sql='';
	if($details['is_flw']==1){
		$flw_sql = " OR ".TABLE_PROJECT_TASK_DETAILS.".mail_client = '1'" ;
	}
    $condition_query_th = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id='".$id."' ".$flw_sql." ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".do_e";
    $total	=	ProjectTask::getList($db, $list, TABLE_PROJECT_TASK_DETAILS.'.id', $condition_query_th);  
    $condition_url = "&perform=details&id=".$id."&or_id=".$or_id ;
    // $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    $extra_url  = '';
    
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
   // $extra_url  .= "xd=$xd&rpp=$rpp";
    $extra_url  = '&start=url'.$extra_url.'&end=url';
   
    //$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    //$list_th	= 	NULL;
    if($actual_time){
        $condition_query_th = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id='".$id."' ".$flw_sql." AND ".TABLE_PROJECT_TASK_DETAILS.".project_id='".$or_id."' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug='0' ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".do_e DESC";
        $fields_th =  TABLE_PROJECT_TASK_DETAILS.'.id,'.TABLE_PROJECT_TASK_DETAILS.'.details,'.TABLE_PROJECT_TASK_DETAILS.'.added_by,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.hrs,'.TABLE_PROJECT_TASK_DETAILS.'.min, '.TABLE_PROJECT_TASK_DETAILS.'.is_client,' ;
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file,'.TABLE_PROJECT_TASK_DETAILS.'.attached_desc,'.TABLE_PROJECT_TASK_DETAILS.'.is_visibleto_client,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.mail_send_to_su,';
		$fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.mail_ceo,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.file_type,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.file_counter,';
		$fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file1,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file2,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file3,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file4,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file5,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file6,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file7,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file8,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file9,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.do_e,';
		$fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.internal_comment,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.current_status,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.pending_frm_client,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.est_completion_dt,';
		$fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.template_id,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.template_file_1,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.template_file_2,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.template_file_3,';
		$fields_th .=  '('.TABLE_PROJECT_TASK_DETAILS.'.worked_hour_salary * '.SALARY_FACTOR.') as worked_hour_salary';
    }else{ 
         // show atual total time (timesheet * 5 ) total cost
         $condition_query_th = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id='".$id."' ".$flw_sql." AND ".TABLE_PROJECT_TASK_DETAILS.".project_id='".$or_id."'  AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug='0' ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".do_e DESC";
		 
        $fields_th =  TABLE_PROJECT_TASK_DETAILS.'.id,'.TABLE_PROJECT_TASK_DETAILS.'.details,'.TABLE_PROJECT_TASK_DETAILS.'.added_by,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.hrs1,'.TABLE_PROJECT_TASK_DETAILS.'.min1, '.TABLE_PROJECT_TASK_DETAILS.'.is_client,' ;
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file,'.TABLE_PROJECT_TASK_DETAILS.'.attached_desc,'.TABLE_PROJECT_TASK_DETAILS.'.is_visibleto_client,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.mail_send_to_su,';
		$fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.mail_ceo,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.file_type,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.file_counter,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file1,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file2,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file3,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file4,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file5,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file6,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file7,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file8,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.attached_file9,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.do_e,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.internal_comment,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.current_status,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.pending_frm_client,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.est_completion_dt,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.template_id,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.template_file_1,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.template_file_2,';
        $fields_th .=  TABLE_PROJECT_TASK_DETAILS.'.template_file_3,';
		$fields_th .=  '('.TABLE_PROJECT_TASK_DETAILS.'.worked_hour_salary * '.SALARY_FACTOR.' ) as  worked_hour_salary';
    }
	 
  	//ProjectTask::getList($db, $list_th,$fields_th, $condition_query_th,$next_recordd, $rpp);
  	ProjectTask::getList($db, $list_th,$fields_th, $condition_query_th);
    $flist_th = array();
    if(!empty($list_th)){
        foreach($list_th as $key=>$val){
            
            if($val['is_client']=='1'){
            
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                $val['is_client']='1';
                
            }else{
            
                $table = TABLE_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                 $val['is_client']='0';
            }
            $val['mail_send_to_su'] = chunk_split($val['mail_send_to_su']);
           
            if($actual_time){
                $val['hrs'] = strlen($val['hrs']) ==1 ? '0'.$val['hrs'] : $val['hrs'] ;
                $val['min'] = strlen($val['min']) ==1 ? '0'.$val['min'] : $val['min'] ;
				$val['cost'] = ceil($val['worked_hour_salary']) ;
            }else{
                 // show atual total time (timesheet * 3 ) total cost
                $val['tothrs'] = $val['hrs1'] ;
                $val['totmin'] = $val['min1'] ;
                $extraHr = (int) ($val['min1'] / 60 ) ;
                $min1 =  ($val['min1'] % 60 ) ;
                $val['tothrs'] =  $val['tothrs'] + $extraHr ;
                $val['totmin'] = $min1 ;
                
                $val['hrs'] = strlen($val['tothrs']) ==1 ? '0'.$val['tothrs'] : $val['tothrs'] ;
                $val['min'] = strlen($val['totmin']) ==1 ? '0'.$val['totmin'] : $val['totmin'] ;
				$val['cost'] = ceil($val['worked_hour_salary']) ;
            }
			if(!empty($val['file_type'])){
				$val['file_type_name'] = $lst_filetype[$val['file_type']];
             }
            $flist_th[$key]=$val;
        }
    }  
    //Get list of sub threads EOF 
	
    if(!empty($details) ){       
		
        if($details['is_client']=='1'){
            $table = TABLE_CLIENTS;           
            $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$details['added_by']."' " ;
            $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS .'.number,'.TABLE_CLIENTS .'.user_id';
            $exeCreted = getRecord($table,$fields1,$condition2);        
            $details['added_by_name'] = $exeCreted['f_name']." ".$exeCreted['l_name']."" ;            
        }else{
            $table = TABLE_USER;
            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$details['added_by']."' " ;
            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER .'.number,'.TABLE_USER .'.user_id';
            $exeCreted = getRecord($table,$fields1,$condition2);        
            $details['added_by_name'] = $exeCreted['f_name']." ".$exeCreted['l_name']."" ;
        }
        
        if(empty($details['resolved_date']) || $details['resolved_date']=='0000-00-00 00:00:00'){
            $details['resolved_date']='';          
            $details['resolved_date']  = date('d') .'/'. date('m').'/'.date('Y');
        }else{
            $details['resolved_date']  = explode(' ', $details['resolved_date']);
            $temp               = explode('-', $details['resolved_date'][0]);
            $details['resolved_date']  = NULL;
            $details['resolved_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];        
        }
        //Get submodule bof
        $details['sub_module_title'] ='';
        $table = TABLE_PROJECT_TASK_MODULE;
        $conditionm = " WHERE ".TABLE_PROJECT_TASK_MODULE .".id= '".$details['sub_module_id']."' " ;
        $fieldsm =  TABLE_PROJECT_TASK_MODULE .'.title';
        $moduleDetails = getRecord($table,$fieldsm,$conditionm);    
        if(!empty( $moduleDetails)){            
            $details['sub_module_title'] =$moduleDetails['title'];
        }               
        //Get submodule eof
        //Show estimated time         
        if($actual_time){
        
            $hrs=$min='0';        
            if(!empty($details['hrs'])){           
                $hrs = strlen($details['hrs']) ==1 ? '0'.$details['hrs'] : $details['hrs'] ;
              
            }
            if(!empty($details['min'])){
                $min = strlen($details['min']) ==1 ? '0'.$details['min'] : $details['min'] ;
               
            }
            $details['est_hrs']  = $hrs.":".$min ;
        }else{
            // show atual total time (timesheet * 3 ) total cost
                $hrs=$min='0';        
                if(!empty($details['hrs1'])){           
                    $hrs = strlen($details['hrs1']) ==1 ? '0'.$details['hrs1'] : $details['hrs1'] ;                   
                }
                if(!empty($details['min1'])){
                    $min = strlen($details['min1']) ==1 ? '0'.$details['min1'] : $details['min1'] ;  
                    $extraHr = (int) ($min / 60 ) ;   
                    $min =  ($min % 60 ) ;   
                    $hrs = $hrs +$extraHr;
                }
                
               
                
                $hrs = strlen($hrs) ==1 ? '0'.$hrs : $hrs ;
                $min = strlen($min) ==1 ? '0'.$min : $min ;
                $details['est_hrs']  = $hrs.":".$min ;
        }
        
            
        //Calculate total bugs
            $condition_query_bug = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PROJECT_TASK_DETAILS.".task_id = '".$details['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".project_id=".$or_id ;
            $listbug	= 	NULL;
            $totalbug	=	ProjectTask::getList( $db, $listbug, TABLE_PROJECT_TASK_DETAILS.'.id', $condition_query_bug);
            $details['totalbugs']= $totalbug;
            //Calculate total pending bugs
            $condition_query_bugp =" WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PROJECT_TASK_DETAILS.".project_id=".$or_id."
			AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PROJECT_TASK_DETAILS.".task_id = '".$details['id']."' ";
			// AND ".TABLE_PROJECT_TASK_RESOLUTION.".title !='".ProjectTask::CLOSE."'
			
            $listpbug	= 	NULL;
            $totalpendingbugs	=	ProjectTask::getList( $db, $listpbug, TABLE_PROJECT_TASK_DETAILS.'.id', $condition_query_bugp);
            $details['totalpendingbugs']= $totalpendingbugs;
            
            //Calculate taskwise hrs spent
            if($actual_time){
                $fields_tws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin, 
				( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary)  ) as tot_tcost' ;
           
            }else{
                // show atual total time (timesheet * 3 ) total cost   
                 $fields_tws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) % 60 ) as totMin,
				  ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost' ;
           
            }
            $condition_query_twh = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='".$details['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".project_id=".$or_id." AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0'";
            $list_tws	= 	NULL;
          	ProjectTask::getList( $db, $list_tws, $fields_tws, $condition_query_twh);           
         
            $details['tot_bhrs'] = '00';
            $details['tot_bmin'] = '00' ;
            $details['worked_task_cost'] = '0' ;
            if(!empty($list_tws[0]['totHr']) || !empty($list_tws[0]['totMin'])){
                $list_tws =$list_tws[0];
                $details['tot_thrs'] = strlen($list_tws['totHr']) ==1 ? '0'.$list_tws['totHr'] : $list_tws['totHr'] ; 
                $details['tot_tmin'] = strlen($list_tws['totMin']) ==1 ? '0'.$list_tws['totMin'] : $list_tws['totMin'] ; 
				$details['tot_tcost'] = $list_tws['tot_tcost'] ;
            }else{
                $details['tot_thrs'] = '00';
                $details['tot_tmin'] = '00' ;
				$details['tot_tcost'] = 0 ;
            }
            
            //Calculate bugwise hrs spent           
            if($actual_time){
                    $fields_bws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin, 
				( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_bcost' ;
            }else{
                // show atual total time (timesheet * 3 ) total cost  
                 $fields_bws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) % 60 ) as totMin,
				( SUM('. TABLE_PROJECT_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_bcost' ;
            
            }
             $condition_query_bwh = " WHERE (".TABLE_PROJECT_TASK_DETAILS.".task_id ='".$details['id']."' AND 
			 ".TABLE_PROJECT_TASK_DETAILS.".project_id=".$or_id." AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' 
                                    AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0' ) OR 
                                    ".TABLE_PROJECT_TASK_DETAILS.".parent_id IN (
                                            SELECT ".TABLE_PROJECT_TASK_DETAILS.".id FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE 
                                            ".TABLE_PROJECT_TASK_DETAILS.".task_id ='".$details['id']."' AND 
                                            ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' 
                                            AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0' 
                                        )";
            $list_bws	= 	NULL;          
          	ProjectTask::getList( $db, $list_bws, $fields_bws, $condition_query_bwh);
			if(!empty($list_bws[0]['totHr']) || !empty($list_bws[0]['totMin'])){
                $list_bws =$list_bws[0];
                $details['tot_bhrs'] = strlen($list_bws['totHr']) ==1 ? '0'.$list_bws['totHr'] : $list_bws['totHr'] ; 
                $details['tot_bmin'] = strlen($list_bws['totMin']) ==1 ? '0'.$list_bws['totMin'] : $list_bws['totMin'] ; 
                $details['tot_bcost'] = $list_bws['tot_bcost'];
            }else{
                $details['tot_bhrs'] = '00';
                $details['tot_bmin'] = '00' ;
                $details['tot_bcost'] = 0 ;
            }
			$details['worked_task_cost']= $details['tot_tcost'] + $details['tot_bcost'] ;
			$_ALL_POST =$details;
    }
    
    $variables['can_edit'] =false;
    $variables['can_view_list'] =false;
    $variables['can_add'] =false;
    $variables['can_view_client_details'] =false;
	$variables['can_view_cost'] = false;
	$variables['can_add_onbehalf'] = false;
	
    if ( $perm->has('nc_p_ts_cost') ) {
        $variables['can_view_cost'] = true;
    }
    if ( $perm->has('nc_p_ts_edit') ) {
        $variables['can_edit'] = true;
    }
   
    if ( $perm->has('nc_p_ts_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_p_ts_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_uc_contact_details') ) {           
        $variables['can_view_client_details'] = true;
    }
  	if ( $perm->has('nc_p_ts_add_onbhf') ) {
        $variables['can_add_onbehalf'] = true;
    }
   /*
    if ( $perm->has('nc_p_ts_details') ) {
        $variables['can_view_details'] = true;
    }*/
    $variables['can_view_details'] = true;
    $variables['hideform'] = $hideform;
    // Set the Permissions.
    $hidden[] = array('name'=> 'perform' ,'value' => 'details');
    $hidden[] = array('name'=> 'act' ,'value' => 'save');
    $hidden[] = array('name'=> 'or_id' , 'value' => $or_id);     
    $hidden[] = array('name'=> 'id' , 'value' => $id);     
    $hidden[] = array('name'=> 'ajx' , 'value' => $ajx);     
    $hidden[] = array('name'=> 'actual_time' , 'value' => $actual_time);     
    if(!empty($data['module_id'])){        
		$lst_submodule = null;
		$fields_a = TABLE_PROJECT_TASK_MODULE .'.id'
				   .','. TABLE_PROJECT_TASK_MODULE .'.title';
		$condition_query_a= "WHERE status='".ProjectModule::ACTIVE."' AND parent_id='".$data['module_id']."' ORDER BY ".TABLE_PROJECT_TASK_MODULE.".id ASC";
		ProjectModule::getList($db,$lst_submodule,$fields_a,$condition_query_a);        
	}
	
	
	
    $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
    $page["var"][] = array('variable' => 'lst_msg', 'value' => 'lst_msg');
	$page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
	$page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
    $page["var"][] = array('variable' => 'lst_filetype', 'value' => 'lst_filetype');
    $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
    $page["var"][] = array('variable' => 'posted_data', 'value' => 'posted_data');
    $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
    $page["var"][] = array('variable' => 'priorityArr', 'value' => 'priorityArr');
    $page["var"][] = array('variable' => 'statusArr', 'value' => 'statusArr');
    $page["var"][] = array('variable' => 'resolutionArr', 'value' => 'resolutionArr');
    $page["var"][] = array('variable' => 'moduleArr', 'value' => 'moduleArr');
	$page["var"][] = array('variable' => 'lst_submodule', 'value' => 'lst_submodule');
    $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => 'flist_th', 'value' => 'flist_th');
   
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task-details.html');
  }else{ 	
  
	$messages->setErrorMessage("Please fill Start Date, Est. End Date, Est. Hours. Contact to your Project Manager");
	$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
    
  }
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
	$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
}
?>
