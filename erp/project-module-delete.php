<?php
	if ( $perm->has('nc_pts_mod_delete') ) {
		$id	= isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'access_level' 	=> $my['access_level'],
						'messages' 		=> $messages
					);
		ProjectModule::delete($id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/project-module-list.php');
	}
	else {
		$messages->setErrorMessage("You donot have the Right to Access this module.");
	}
?>