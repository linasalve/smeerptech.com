<?php
    if ( $perm->has('nc_mt_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
		
		$parentTax= ServiceTax::getParentServiceTax($db);
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( ServiceTax::validateUpdate($data, $extra) ) {
				$data['parent_name']='';
				if(!empty($data['parent_id'])){
					$condition1=" WHERE id= ".$data['parent_id']." LIMIT 0,1" ;
					$required_fields='tax_name';
					ServiceTax::getDetails( $db, $parentTaxDetails, $required_fields, $condition1);
					$arr = $parentTaxDetails[0];
					$data['parent_name'] = $arr['tax_name'];
					
				}
			
                $query  = " UPDATE ". TABLE_SERVICE_TAX
                            ." SET ". TABLE_SERVICE_TAX .".tax_name = '".                 $data['tax_name'] ."'"
								.",". TABLE_SERVICE_TAX .".parent_id = '". $data['parent_id'] ."'"
								.",". TABLE_SERVICE_TAX .".parent_name = '". $data['parent_name'] ."'"
                                .",". TABLE_SERVICE_TAX .".tax_number = '". $data['tax_number'] ."'"
								.",". TABLE_SERVICE_TAX .".declaration = '". $data['declaration'] ."'"
                                .",". TABLE_SERVICE_TAX .".status = '".              $data['status'] ."'"
                            ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SERVICE_TAX .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SERVICE_TAX .".id = '". $id ."' )";
            /*
            $condition_query .= " AND ( ";
            // If my has created this record.
            $condition_query .= " (". TABLE_SERVICE_TAX .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_SERVICE_TAX .".access_level < $access_level ) ";      
                                           
            // Check if the User has the Right to Edit Orders created by other Users.
                       
            //if ( $perm->has('nc_mt_edit_ot') ) {
             //   $access_level_o   = $my['access_level'];
             //   if ( $perm->has('nc_mt_edit_ot_al') ) {
             //       $access_level_o += 1;
             //   }
             //   $condition_query .= " OR ( ". TABLE_SERVICE_TAX. ".created_by != '". $my['user_id'] ."' "
              //                      ." AND ". TABLE_SERVICE_TAX .".access_level < $access_level_o ) ";
            //}
            $condition_query .= " )";
            */
            
            if ( ServiceTax::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                
                    // Setup the date of delivery.
                    
                    //$_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/manage-tax-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'parentTax', 'value' => 'parentTax'); 
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'manage-tax-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
