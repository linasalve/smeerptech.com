<?php

    if ( $perm->has('nc_pr_lblprn') ) {
		
		include_once ( DIR_FS_INCLUDES .'/prospects-quotation.inc.php');
		
		$client_id = isset($_GET['client_id']) ? $_GET['client_id'] : ( isset($_POST['client_id'] ) ? $_POST['client_id'] :'');
         
        $inv_id = isset($_GET['inv_id']) ? $_GET['inv_id'] : ( isset($_POST['inv_id'] ) ? $_POST['inv_id'] :'');
        $add_id = isset($_GET['add_id']) ? $_GET['add_id'] : ( isset($_POST['add_id'] ) ? $_POST['add_id'] :'');
        $billing_name = isset($_GET['billing_name']) ? $_GET['billing_name'] : ( isset($_POST['billing_name'] ) ? $_POST['billing_name'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
		 
		if(!empty($inv_id)){
			$fields =TABLE_PROSPECTS_QUOTATION.'.billing_address,'.TABLE_PROSPECTS.".title,"
			.TABLE_PROSPECTS.".mobile1,".TABLE_PROSPECTS.".mobile2,
			".TABLE_PROSPECTS.".f_name,
			".TABLE_PROSPECTS.".l_name, ".TABLE_PROSPECTS.".billing_name,".TABLE_PROSPECTS.".user_id " ;
			$condition_query = " WHERE (". TABLE_PROSPECTS_QUOTATION .".id = '". $inv_id ."') " ;
			ProspectsQuotation::getDetails($db, $_ALL_POST, $fields, $condition_query);
			//".TABLE_PROSPECTS.".map,
		}
		
		if(!empty($add_id)){
			$_ALL_POST['0'] = array(
				'user_id'=>$client_id,
				'billing_address'=>$add_id,
				'billing_name'=>$billing_name
			);
		}
		 
		
       if ( !empty($_ALL_POST) ) {
		
				$data = $_ALL_POST['0'];
            	include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
				include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
				$region     = new RegionProspects('91','21','231');
				$phone      = new PhoneProspects(TABLE_PROSPECTS);
                $region->setAddressOf(TABLE_PROSPECTS,  $data['user_id']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $address_list  =  $address_list;
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
        
				$phone->setPhoneOf(TABLE_PROSPECTS, $data['user_id']);
                $data['phone'] = $phone->get($db);
                //TABLE_PROSPECTS
               /*  $data['mobile'] = '';
                if(!empty($data['mobile1']) && !empty($data['mobile2'])){
					$data['mobile'] = $data['mobile1'].", ".$data['mobile2'] ;
				}elseif(!empty($data['mobile1']) && empty($data['mobile2'])){
					$data['mobile'] = $data['mobile1'];
				}elseif(empty($data['mobile1']) && !empty($data['mobile2'])){
					$data['mobile'] = $data['mobile2'];
				} */
				 
                Prospects::clientPAddressPdf($data);				
				$file_path = DIR_FS_GN_FILES ."/";
                $file_name = "prospect-address.pdf";
                $content_type = 'application/pdf';
				header("Pragma: public"); // required
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false); // required for certain browsers 
				header("Content-type: $content_type");
				header("Content-Disposition: attachment; filename=". $file_name );
				header("Content-Length: ".filesize($file_path.$file_name));
				readfile($file_path.$file_name);
                exit;
                $page["var"][] = array('variable' => 'data', 'value' => 'data');
    
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-label-print.html');
             
        }
        else {
            $messages->setErrorMessage("The Executives details were not found. ");
        }  
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>