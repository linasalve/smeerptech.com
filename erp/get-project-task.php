<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");

    $order_id = isset($_GET['order_id']) ? $_GET['order_id'] : '';
    
   	$task_list = '' ;
    if (isset($order_id) && !empty($order_id))
    {
        
		$query	="SELECT id, title FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE ".TABLE_PROJECT_TASK_DETAILS.".project_id ='".$order_id."' AND parent_id='0' AND is_bug='0'";
        $query	.= " ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".do_e DESC";
        
		$db->query( $query );
		$total	= $db->nf();	
		if( $db->nf() > 0 )
		{
			while($db->next_record())
			{
				$task_list .=  trim($db->f("title"))."|".$db->f("id").",";
			}
			echo rtrim($task_list,',');
		}else{		
			echo "|";
		}
	}	
	include_once( DIR_FS_NC ."/flush.php");
?>
