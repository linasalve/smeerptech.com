<?php
if ( $perm->has('nc_p_iah_list') ) {
       
        /*
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_ab_list_al') ) {
            $access_level += 1;
        }
        if ( $where_added ) {
            $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= "  ( ";

        // If my has created this entry.
        $condition_query .= " ( ". TABLE_PAYMENT_ACCOUNT_HEAD .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_PAYMENT_ACCOUNT_HEAD .".access_level < $access_level ) ";
       
        
         // Check if the User has the Right to view Orders created by other Users.
        if ( $perm->has('nc_ab_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_ab_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_PAYMENT_ACCOUNT_HEAD. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_PAYMENT_ACCOUNT_HEAD .".access_level < $access_level_o ) ";
        }
        $condition_query .= ")";
        */
        
        
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $_SEARCH['searched'] = 1;
        
        //echo $condition_query;
        // To count total records.
        $list	= 	NULL;
        $total	=	PaymentIAccountHead::getDetails( $db, $list, '', $condition_query);
    
       
       
          
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $list	= NULL;
        $fields = TABLE_PAYMENT_IACCOUNT_HEAD.'.*' ;
        PaymentIAccountHead::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        // Set the Permissions.
        /*
        $variables['can_view_list']     = true;
        $variables['can_add']           = true;
        $variables['can_edit']          = true;
        $variables['can_delete']        = true;
        $variables['can_view_details']  = true;
        */
        if ( $perm->has('nc_p_iah_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_p_iah_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_p_iah_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_p_iah_delete') ) {
            $variables['can_delete'] = true;
        }
       
     
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-manage-iaccount-head-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
