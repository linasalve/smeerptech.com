<?php
	if ( $perm->has('nc_emtpl_status') ) {
        $tpl_id	= isset($_GET["tpl_id"]) ? $_GET["tpl_id"] 	: ( isset($_POST["tpl_id"]) ? $_POST["tpl_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"]  : ( isset($_POST["status"]) ? $_POST["status"] : '' );
        
        $extra = array( 'db' 		=> &$db,
                        'messages'  => $messages
                    );
        
        EmailTemplates::updateStatus($tpl_id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/settings-templates-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>