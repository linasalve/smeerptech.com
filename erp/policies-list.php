<?php

   
       
	$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
	
	if ( !isset($_SEARCH) ) {
		$_SEARCH = '';
	}
	 //By default search in On
	$_SEARCH["searched"]    = true ;
	// To count total records.
	$list	= 	NULL;
	$pagination   = '';
	$extra_url  = '';
	
	if($searchStr==1){
		$total	=	Policies::getList( $db, $list, '', $condition_query);
		 
		if ( isset($condition_url) && !empty($condition_url) ) {
			$extra_url  = $condition_url;
		}
		$condition_url .="&perform=".$perform;            
					
		$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','policies.php','frmSearch');
		$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  = '&start=url'. $extra_url .'&end=url';
	
		$list	= NULL;
		Policies::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
	}
	 
 
	$fList=array();
	if(!empty($list)){
		foreach( $list as $key=>$val){ 
		    $plist=array();
			$condition_query1=" WHERE ".TABLE_POLICIES_P.".tjf_id ='".$val['id']."'";
			Policies::getParticulars($db, $plist, '*',$condition_query1);
			$val['pdetails']=$plist ;
			$fList[$key]=$val;
		}
	}
	
	
	// Set the Permissions.
	
	$variables['deactive'] = Policies::DEACTIVE;
	$variables['active']   = Policies::ACTIVE;
	
	$variables['can_add']           = false;
	$variables['can_edit']          = false;
	$variables['can_delete']        = false;
	$variables['can_list']          = false;
	$variables['can_change_status'] = false;
  
	if ( $perm->has('nc_pl_add') ) {
		$variables['can_add'] = true;
	}
	if ( $perm->has('nc_pl_edit') ){
		$variables['can_edit'] = true;
	}      
	if ( $perm->has('nc_pl_list') ){
		$variables['can_list'] = true;
	}
	
	if ( $perm->has('nc_pl_delete') ) {
		$variables['can_delete'] = true;
	}
	
	if ( $perm->has('nc_pl_status') ) {
		$variables['can_change_status'] = true;
	}
	
   
	$variables['searchLink']=$searchLink ;
	$page["var"][] = array('variable' => 'list', 'value' => 'fList');
	$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
	$page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
	$page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
	// PAGE = CONTENT_MAIN
	$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'policies-list.html');
    
?>