<?php
    if ( $perm->has('nc_al_add') ) {
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            if (AccessLevel::validateAdd($data, $extra)) {
                $query	= " INSERT INTO ".TABLE_SETTINGS_ACC_LVL
                            ." SET ". TABLE_SETTINGS_ACC_LVL .".title = '". $data['title'] ."'"
                                .",". TABLE_SETTINGS_ACC_LVL .".access_level = '". $data['access_level'] ."'"
                                .",". TABLE_SETTINGS_ACC_LVL .".description = '". $data['description'] ."'"
                                .",". TABLE_SETTINGS_ACC_LVL .".status = '". $data['status'] ."'";
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $messages->setOkMessage("Access Level added successfully.");
                    $variables['hid'] = $db->last_inserted_id();
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
            }
        }
         if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/settings-access-level.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/settings-access-level.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/settings-access-level.php?added=1");   
        }
        else {
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
    
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-access-level-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>