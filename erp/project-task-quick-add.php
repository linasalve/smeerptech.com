<?php
    if ( $perm->has('nc_p_ts_details') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        $lst_order = NULL;
        $required_fields ='id, order_title';
        $condition =" where status='".Order::ACTIVE."'
                      AND ". TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",'  ORDER BY order_title";
                                        
		Order::getList($db,$lst_order,$required_fields,$condition);
        
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
            
            $lst_task =null;
            if(!empty($data['or_id'])){
                $required_fields = TABLE_PROJECT_TASK_DETAILS.'.id,'.TABLE_PROJECT_TASK_DETAILS.'.title';
                $condition =" WHERE ".TABLE_PROJECT_TASK_DETAILS.".project_id='".$data['or_id']."' AND ".TABLE_PROJECT_TASK_DETAILS .".parent_id='0' AND is_bug='0'";                
                ProjectTask::getList( $db, $lst_task, $required_fields, $condition);
            }
                
            if ( ProjectTask::validateCommentAddQuick($data, $extra) ) { 
                
                $hrs1 = (int) ($data['hrs'] * 3);
                $min1 = (int) ($data['min'] * 3);   
               
                $query = "INSERT INTO ".TABLE_PROJECT_TASK_DETAILS
							  ." SET "
							  ." details      	= '".$data['details']."',"	
							  ." hrs             = '".$data['hrs']."',"
							  ." min            = '".$data['min']."',"
                              ." hrs1            = '".$hrs1."',"
							  ." min1           = '".$min1."',"
							  ." last_updated 	= '".$last_updated."',"
							  ." added_by      	= '".$my['user_id']."',"
							  ." parent_id      = '".$data['task_id']."',"
							  ." project_id      = '".$data['or_id']."',"
							  ." is_bug       	= '0',"
							  //." attached_file	= '".$attachfilename."',"
                              //." attached_desc  = '".$_ALL_POST['attached_desc']."',"
                              //." file_upload_path  = '".$_ALL_POST['file_upload_path']."', "
							  ." is_visibleto_client = '0', "
                              //." mail_client = '".$mail_client."',"
							  //." mail_staff = '".$mail_staff."',"
							  //." mail_senior = '".$mail_senior."',"
							  ." ip  	        = '".$_SERVER['REMOTE_ADDR']."', "
							  ." do_e 	        = '".date('Y-m-d H:i:s')."'" ;
                
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Comment added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/project-task.php?perform=quick_add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/project-task.php?perform=quick_add");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/project-task.php?perform=quick_add&added=1");   
        }
        else {
            $hidden[] = array('name'=> 'perform' ,'value' => 'quick_add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'lst_order', 'value' => 'lst_order');     
            $page["var"][] = array('variable' => 'lst_task', 'value' => 'lst_task');     
            $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task-quick-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>