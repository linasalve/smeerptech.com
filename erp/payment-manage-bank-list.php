<?php
if ( $perm->has('nc_p_mb_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    

    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	Paymentbank::getDetails( $db, $list, '', $condition_query);


    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_PAYMENT_BANK.'.*' ;
    Paymentbank::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    $flist_r = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
            $type = $val['type'];
            $is_transaction_done=0; 
            if($type == '0'){
           
                $val['type_name'] = 'Global' ;
                
            }else{
           
                $val['type_name'] = 'Inhouse' ;
                $sql = "SELECT id FROM ".TABLE_PAYMENT_TRANSACTION." WHERE credit_pay_bank_id =".$val['id']." OR debit_pay_bank_id=".$val['id'] ;
                $db->query($sql);
                if($db->nf()>0 )
                {
                    while ($db->next_record()) {
                         $is_transaction_done = 1; 
                    }
                }                
            } 
            $val['is_transaction_done']=$is_transaction_done ;
            $flist_r[$key]=$val;
        }
    }
    // Set the Permissions.
    
    if ( $perm->has('nc_p_mb_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_p_mb_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_p_mb_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_p_mb_delete') ) {
        $variables['can_delete'] = true;
    }
    if ( $perm->has('nc_p_mb_details') ) {
        $variables['can_view_details'] = true;
    }
    
    $page["var"][] = array('variable' => 'list', 'value' => 'flist_r');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-manage-bank-list.html');

}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>


