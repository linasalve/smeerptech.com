<?php

   if ( $perm->has('nc_bl_invprfm') ) {//nc_bl_invprfm_details
        $inv_id = isset ($_GET['inv_id']) ? $_GET['inv_id'] : ( isset($_POST['inv_id'] ) ? $_POST['inv_id'] :'');
        $type   = isset ($_GET['type']) ? $_GET['type'] : ( isset($_POST['type'] ) ? $_POST['type'] :'');
        
        $condition_query= " WHERE ". TABLE_BILL_INV_PROFORMA .".id = '". $inv_id ."'"
							." OR ". TABLE_BILL_INV_PROFORMA .".number = '". $inv_id ."'";
        
        $access_level   = $my['access_level'];
        /*
		if ( $perm->has('nc_bl_invprfm_list_al') ) {
            $access_level += 1;
        }        
		$condition_query .= " AND ( ";
        // If my has created this InvoiceProforma.
        $condition_query .= " (". TABLE_BILL_INV_PROFORMA .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_INV_PROFORMA .".access_level < $access_level ) ";
        
        // If my is the Client Manager
        $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                            ." AND ". TABLE_BILL_INV_PROFORMA .".access_level < $access_level ) ";
        $condition_query .= " )"; */

        $list	= NULL;
        if ( InvoiceProforma::getDetails( $db, $list, TABLE_BILL_INV_PROFORMA .'.id, '. TABLE_BILL_INV_PROFORMA .'.number', $condition_query) ) {
          
            if ( $type == 'html' ) {
                $file_path = DIR_FS_INVPROFM_HTML_FILES ."/";
                $file_name = $list[0]["number"] .".html";
                $content_type = 'text/html';
            }elseif ( $type == 'htmlprint' ) {
                $file_path = DIR_FS_INVPROFM_HTMLPRINT_FILES ."/";
                $file_name = $list[0]["number"] .".html";
                $content_type = 'text/html';
            }
            elseif ( $type == 'pdf' ) {
                $file_path = DIR_FS_INVPROFM_PDF_FILES ."/";
                $file_name = $list[0]["number"] .".pdf";
                $content_type = 'application/pdf';
            }
            else {
                echo('<script language="javascript" type="text/javascript">alert("Invalid File specified");</script>');
            }
            
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-type: $content_type");
            header("Content-Disposition: attachment; filename=". $file_name );
            header("Content-Length: ".filesize($file_path.$file_name));
            readfile($file_path.$file_name);
        }
        else {
            echo('<script language="javascript" type="text/javascript">alert("The File is not found or you do not have the permission to view the file.");</script>');
        }
    }
    else {
        echo('<script language="javascript" type="text/javascript">alert("You do not have the permission to view the list.");</script>');
    } 
    exit;
?>