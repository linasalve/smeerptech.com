<?php
	if ( $perm->has('nc_bl_po_status') ) {
        $po_id	= isset($_GET["po_id"])  ? $_GET["po_id"] 	: ( isset($_POST["po_id"]) 	? $_POST["po_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        /*if ( $perm->has('nc_bl_po_status_al') ) {
            $access_level += 1;
        }*/
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                    );
        
        PreOrder::updateStatus($po_id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/bill-pre-order-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>
