<?php
    if ( $perm->has('nc_p_pb_tax_csv') ) { //
        $_ALL_POST  = NULL;
        $data       = NULL;
        $messages_ask 	= new Messager;	// create the object for the Message class.
		
		
        $CSV = PaymentPartyBills::getRestrictions();
        
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);          
			$files       = processUserData($_FILES);
    		$ask_added      = 0;
			$ask_duplicate  = 0;
			$ask_empty      = 0;
			print_r($files);
            $extra = array( 'file'      => &$files,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );
			 
			 
            if ( PaymentPartyBills::validateUpload($data, $extra) ) {
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $invalid  = 0;
                $empty      = 0;
                $index      = array();
                $handle     = fopen($_FILES['file_csv']['tmp_name'],"rb");
                $message = '';
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
					
					
                    if ( $row == 1 ) {
                        $num = count($data);
                        for ($i=0; $i < $num; $i++) {
                            $case = trim($data[$i]);
                            switch ($case) {
                                case ('ncNo'): {
                                    $index['ncNo'] = $i;
                                    break;
                                }                        
								case ('tax_paid_dt'): {
                                    $index['tax_paid_dt'] = $i;
                                    break;
                                }									
								case ('remarks'): {
                                    $index['remarks'] = $i;
                                    break;
                                }	
								
                            }
                        }
                    }
                    else {                 
                          $index= array (
							"ncNo"=>0,
							"tax_paid_dt"=>1,
							"remarks"=>2
                           );
						   
						    $dt=$data[$index['tax_paid_dt']];
						    $dtArr = explode('/', $dt);
						    $tax_paid_dt = $dtArr[2]."-".$dtArr[1]."-".$dtArr[0]." 00:00:00";
							//$tax_paid_dt = mktime(0, 0, 0, $dtArr[1], $dtArr[0], $dtArr[2]);
							//$tax_paid_dt = date('Y-m-d H:i:s', $tax_paid_dt);
							
							
							if ( isset($index['ncNo']) && $data[$index['ncNo']] != '' ) {
						
								$sqlu=" UPDATE ".TABLE_PAYMENT_PARTY_BILLS 
								." SET used_for_tax = '1',"
								." tax_paid_dt = '".$tax_paid_dt."'," 
								." tax_remarks = '". $data[$index['remarks']]."'," 
								." tax_used_by = '". $my['user_id']."'," 
								." tax_used_by_name = '". $my['f_name']." ".$my['l_name']."' " 
								." WHERE number = ".$data[$index['ncNo']];		
 							
								$db->query($sqlu);
								$added += 1;
								$messages->setOkMessage('Added Nc No. -'.$data[$index['ncNo']]);
							}else{
								$empty += 1;
								$messages->setErrorMessage('NC No. -'.$data[$index['ncNo']]." not found.");
							}							
							 
                      
                        
                        // The Row is not the First row.
                        
                    }                    
                }
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been added.');
                if ($notfound)
                    $messages->setOkMessage($notfound .' out of '. $row .' records were not found and neglected.');
                
				if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
        }
        
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$condition ='';
            include ( DIR_FS_NC .'/payment-party-bills-list.php');
        }else {
			
			 
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'csv_upload');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-party-bills-tax-csv.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>