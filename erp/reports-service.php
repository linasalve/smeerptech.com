<?php
if ( $perm->has('nc_rp_serv') ) {
	include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
	include_once (DIR_FS_INCLUDES .'/services.inc.php');
	include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
	
	$data	= isset($_POST)? $_POST	: ( isset($_GET)? $_GET	:'');
	$x	= isset($_GET["x"])? $_GET["x"]	: ( isset($_POST["x"])? $_POST["x"]	:'');
	$rpp = isset($_GET["rpp"]) ? $_GET["rpp"]:(isset($_POST["rpp"])? $_POST["rpp"]:(defined('RESULTS_PER_PAGE') 
	? RESULTS_PER_PAGE : 10)); // Result per page 
	 
	$sString = isset($_GET["sString"]) ? $_GET["sString"]: ( isset($_POST["sString"]) ? $_POST["sString"] : '' );
	$sType = isset($_GET["sType"]) ? $_GET["sType"] : ( isset($_POST["sType"]) ? $_POST["sType"]   : '' );
	$sOrder = isset($_GET["sOrder"]) ? $_GET["sOrder"] : ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy = isset($_GET["sOrderBy"])? $_GET["sOrderBy"]	:( isset($_POST["sOrderBy"])?$_POST["sOrderBy"]: '' );
	$perform = isset($_GET["perform"])? $_GET["perform"]:( isset($_POST["perform"])? $_POST["perform"] : '' );
	$s_id= isset($_GET["s_id"])? $_GET["s_id"]:( isset($_POST["s_id"])? $_POST["s_id"] : '' );
    $date_from =isset($_GET["date_from"])? $_GET["date_from"]:(isset($_POST["date_from"])? $_POST["date_from"]:'' );
	$date_to= isset($_GET["date_to"]) ? $_GET["date_to"]:( isset($_POST["date_to"])? $_POST["date_to"] : '' );

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : 
	(isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
	$condition_url  = '';
	$condition_query1='';
	$extra='';
	
	if ( empty($x) ) {
		$x            = 1;
		$next_record	= 0 ;
	}else {
		$next_record	= ($x-1) * $rpp;
	}
	$variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
	
	if(array_key_exists('submit_search',$data)){
		$show=1;
		if(empty($date_from) || empty($date_to)){        
		  $messages->setErrorMessage('Please select From Date and To Date.');
		} 
		if(empty($s_id)){
			$messages->setErrorMessage('Please select Sub Service.');
		}
	}    
		
	if ( !isset($_SEARCH) ) {
		$_SEARCH = '';
	}
		
	$lst_service    = NULL;
	$condition_querys= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC"; 
	Services::getList($db, $lst_service, 'ss_id,ss_title', $condition_querys);

    //As validate both dates
    if($messages->getErrorMessageCount() <= 0 && $show==1 ){
    
        $list	= $dateArr=	NULL; 
		$condition_query2 = '';
        $where_added=true;
		$condition_url = "&perform=".$perform."&submit_search=1" ;
		$dt_field = isset($_POST["dt_field"]) ? $_POST["dt_field"]:(isset($_GET["dt_field"]) 
		? $_GET["dt_field"]      :'');
		
        // BO: From Date
        if( !empty($date_from)){
            if ( $where_added ){
                $condition_query2 .= " AND " ;
            }else{
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            $dfa = explode('/', $date_from);
            $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';            
            $condition_query2 .= " ". TABLE_BILL_INV_PROFORMA.".".$dt_field." >= '". $dfa ."'";
            $condition_url .= "&date_from=$date_from";
            $_SEARCH["date_from"] = $date_from;
			$_SEARCH["dt_field"]       = $dt_field;
        }
        // EO: From Date
        
         
        if ( !empty($date_to)){
            if ( $where_added ){
                $condition_query2 .= " AND " ;
            }else{
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            $dta = explode('/', $date_to);
            $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
            $condition_query2 .= " ". TABLE_BILL_INV_PROFORMA .".".$dt_field." <= '". $dta ."'";
            $condition_url          .= "&date_to=$date_to";
            $_SEARCH["date_to"]     = $date_to ;
			$_SEARCH["dt_field"]       = $dt_field;
        }
		//BO : Company
		$sCompIdStr='';
		$chk_cmp = isset($_POST["chk_cmp"])   ? $_POST["chk_cmp"]  : (isset($_GET["chk_cmp"])   ? $_GET["chk_cmp"]   :'');
		$sCompId    = isset($_POST["sCompId"])      ? $_POST["sCompId"]     : (isset($_GET["sCompId"])      ?$_GET["sCompId"]     :'');
		if ( ($chk_cmp == 'AND' || $chk_cmp == 'OR' ) && !empty($sCompId)) {
			$searchStr = 1;
			if ( $where_added ) {
			   $condition_query2 .= " ". $chk_cmp;
			}
			else {
				$condition_query2.= ' WHERE ';
				$where_added    = true;
			}
				  
			if(is_array($sCompId)){
				 $sCompIdStr = implode("','", $sCompId) ;
				 $sCompIdStr1 = implode(",", $sCompId) ;
				 
			}else{
				$sCompIdStr = str_replace(',',"','",$sCompId);
				$sCompIdStr1 = $sCompId ;
				$sCompId = explode(",",$sCompId) ;
				
			}
		   
		   $condition_query2 .= " ". TABLE_BILL_INV_PROFORMA .".company_id  IN ('". $sCompIdStr ."') ";
		   $condition_url          .= "&chk_cmp=$chk_cmp&sCompId=$sCompIdStr1";   
		   $_SEARCH["chk_cmp"]  = $chk_cmp;
		   $_SEARCH["sCompId"]     = $sCompId;
		   
		} 
		//EO : Company
        if ( !empty($s_id)){
            if ( $where_added ){
                $condition_query2 .= " AND " ;
            }else{
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            
            $condition_query2 .= " ". TABLE_BILL_ORD_P .".s_id = '". $s_id ."'";
            $condition_url          .= "&s_id=$s_id";
            $_SEARCH["s_id"]     = $s_id ;
        }
        // EO: Upto Date
        
        
	    
		$fields = TABLE_BILL_INV_PROFORMA .'.id'
		.','. TABLE_BILL_INV_PROFORMA .'.number'
		.','. TABLE_BILL_INV_PROFORMA .'.or_no'
		.','. TABLE_BILL_INV_PROFORMA .'.or_id'
		.','. TABLE_BILL_INV_PROFORMA .'.do_fe'
		.','. TABLE_BILL_INV_PROFORMA .'.do_e'
		.','. TABLE_BILL_INV_PROFORMA .'.do_c'
		.','. TABLE_BILL_INV_PROFORMA .'.do_d'
		.','. TABLE_BILL_INV_PROFORMA .'.do_i'
		.','. TABLE_BILL_INV_PROFORMA .'.paid_status'
		.','. TABLE_BILL_INV_PROFORMA .'.currency_abbr'		
		.','. TABLE_BILL_INV_PROFORMA .'.round_off_op'
		.','. TABLE_BILL_INV_PROFORMA .'.round_off'   
		.','. TABLE_BILL_INV_PROFORMA .'.amount'
		//.','. TABLE_BILL_INV_PROFORMA .'.amount_inr'
		//.','. TABLE_BILL_INV_PROFORMA .'.balance as prfm_balance'
		//.','. TABLE_BILL_INV_PROFORMA .'.balance_inr as prfm_balance_inr'
		.','. TABLE_BILL_ORDERS .'.order_title'
		.','. TABLE_BILL_ORDERS .'.id as order_id'
		.','. TABLE_BILL_ORDERS .'.number as ord_no'
		.','. TABLE_BILL_ORDERS .'.do_o' 	
		.','. TABLE_BILL_ORD_P .'.particulars'
		.','. TABLE_BILL_ORD_P .'.s_type'
		.','. TABLE_BILL_ORD_P .'.s_quantity'
		.','. TABLE_BILL_ORD_P .'.s_amount'
		.','. TABLE_BILL_ORD_P .'.ss_title'
		.','. TABLE_BILL_ORD_P .'.ss_punch_line'
		.','. TABLE_BILL_ORD_P .'.p_amount' 
		.','. TABLE_BILL_ORD_P .'.tax1_name'
		.','. TABLE_BILL_ORD_P .'.tax1_value'
		.','. TABLE_BILL_ORD_P .'.tax1_amount'
		.','. TABLE_BILL_ORD_P .'.tax1_sub1_name'
		.','. TABLE_BILL_ORD_P .'.tax1_sub1_value'
		.','. TABLE_BILL_ORD_P .'.tax1_sub1_amount'
		.','. TABLE_BILL_ORD_P .'.tax1_sub2_name'
		.','. TABLE_BILL_ORD_P .'.tax1_sub2_value'
		.','. TABLE_BILL_ORD_P .'.tax1_sub2_amount'
		.','. TABLE_BILL_ORD_P .'.stot_amount'
		.','. TABLE_BILL_ORD_P .'.d_amount'
		.','. TABLE_BILL_ORD_P .'.tot_amount'
		.','. TABLE_CLIENTS .'.user_id AS c_user_id'
		.','. TABLE_CLIENTS .'.number AS c_number'
	 	.','. TABLE_CLIENTS .'.f_name AS c_f_name'
		.','. TABLE_CLIENTS .'.l_name AS c_l_name'
		.','. TABLE_CLIENTS .'.email AS c_email'
		.','. TABLE_CLIENTS .'.mobile1'
		.','. TABLE_CLIENTS .'.mobile2'
		.','. TABLE_CLIENTS .'.billing_name AS billing_name'
		.','. TABLE_CLIENTS .'.status AS c_status';
		
		
		$query = " SELECT SUM(".TABLE_BILL_ORD_P.".tot_amount) as total_amount FROM ".TABLE_BILL_INV_PROFORMA." " ;
		$query .= " LEFT JOIN ".TABLE_BILL_ORDERS." ON ".TABLE_BILL_ORDERS.".id = ".TABLE_BILL_INV_PROFORMA.".or_id ";
		$query .= " LEFT JOIN ".TABLE_BILL_ORD_P." ON ".TABLE_BILL_ORD_P.".ord_no = ".TABLE_BILL_ORDERS.".number ";
		$query .= " LEFT JOIN ".TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client " ;
		$query .= " WHERE ". TABLE_BILL_INV_PROFORMA .".status IN ('".InvoiceProforma::ACTIVE ."',
				  '".InvoiceProforma::PENDING ."','".InvoiceProforma::COMPLETED."')".$condition_query2 ; 
	    $order_by = " ORDER BY ". TABLE_BILL_INV_PROFORMA .".do_i DESC ";
		$query  .=  $order_by ;
		$db->query($query);  
		if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
                $total_amount =$db->f('total_amount');
			}
		}
		
		
		
		$query = " SELECT COUNT(*) as total FROM ".TABLE_BILL_INV_PROFORMA." " ;
		$query .= " LEFT JOIN ".TABLE_BILL_ORDERS." ON ".TABLE_BILL_ORDERS.".id = ".TABLE_BILL_INV_PROFORMA.".or_id ";
		$query .= " LEFT JOIN ".TABLE_BILL_ORD_P." ON ".TABLE_BILL_ORD_P.".ord_no = ".TABLE_BILL_ORDERS.".number ";
		$query .= " LEFT JOIN ".TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client " ;
		$query .= " WHERE ". TABLE_BILL_INV_PROFORMA .".status IN ('".InvoiceProforma::ACTIVE ."',
				  '".InvoiceProforma::PENDING ."','".InvoiceProforma::COMPLETED."')".$condition_query2 ; 
	    $order_by = " ORDER BY ". TABLE_BILL_INV_PROFORMA .".do_i DESC ";
		echo $query  .=  $order_by ;
		$db->query($query);  
		if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
                $total =$db->f('total');
			}
		}
		


		$query = " SELECT ".$fields." FROM ".TABLE_BILL_INV_PROFORMA." " ;
		$query .= " LEFT JOIN ".TABLE_BILL_ORDERS." ON ".TABLE_BILL_ORDERS.".id = ".TABLE_BILL_INV_PROFORMA.".or_id ";
		$query .= " LEFT JOIN ".TABLE_BILL_ORD_P." ON ".TABLE_BILL_ORD_P.".ord_no = ".TABLE_BILL_ORDERS.".number ";
		$query .= " LEFT JOIN ".TABLE_CLIENTS." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_ORDERS .".client " ;
		$query .= " WHERE ". TABLE_BILL_INV_PROFORMA .".status IN ('".InvoiceProforma::ACTIVE ."',
		'".InvoiceProforma::PENDING ."','".InvoiceProforma::COMPLETED."') ".$condition_query2;
		$query  .=  $order_by ;
		//$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','report-service.php','frmSearch');
		
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');        
		$query .= " LIMIT ". $next_record .", ". $rpp;
		$db->query($query);
		 
		if ( $db->nf()>0 ) {
            while ( $db->next_record() ){ 
               
                $list[] = array(
					'ord_no'=>$db->f('ord_no'),
					'inv_no'=>$db->f('number'),
					'do_o'=>$db->f('do_o'),
					'do_fe'=>$db->f('do_fe'),
					'do_e'=>$db->f('do_e'),
					'do_i'=>$db->f('do_i'),
					'amount'=>$db->f('amount'),
					'order_title'=>$db->f('order_title'),
					'particulars'=>$db->f('particulars'),
					's_type'=>$db->f('s_type'),
					's_quantity'=>$db->f('s_quantity'),
					's_amount'=>$db->f('s_amount'),
					'ss_title'=>$db->f('ss_title'),
					'ss_punch_line'=>$db->f('ss_punch_line'),
					'p_amount'=>$db->f('p_amount'),
					'tax1_name'=>$db->f('tax1_name'),
					'tax1_value'=>$db->f('tax1_value'),
					'tax1_amount'=>$db->f('tax1_amount'),
					'tax1_sub1_name'=>$db->f('tax1_sub1_name'),
					'tax1_sub1_value'=>$db->f('tax1_sub1_value'),
					'tax1_sub1_amount'=>$db->f('tax1_sub1_amount'),
					'tax1_sub2_name'=>$db->f('tax1_sub2_name'),
					'tax1_sub2_value'=>$db->f('tax1_sub2_value'),
					'tax1_sub2_amount'=>$db->f('tax1_sub2_amount'),
					'stot_amount'=>$db->f('stot_amount'),
					'd_amount'=>$db->f('d_amount'),
					'tot_amount'=>$db->f('tot_amount'),
					'c_number'=>$db->f('c_number'),
					'c_f_name'=>$db->f('c_f_name'),
					'c_l_name'=>$db->f('c_l_name'),
					'c_email'=>$db->f('c_email'),
					'mobile1'=>$db->f('mobile1'),
					'mobile2'=>$db->f('mobile2'),
					'billing_name'=>$db->f('billing_name')				
				);
			}
		}
    } 
	
	InvoiceProforma::getCompany($db,$company);
	$page["var"][] = array('variable' => 'company', 'value' => 'company');
    $page["var"][] = array('variable' => 'list', 'value' => 'list');      
	$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
	$page["var"][] = array('variable' => 'total', 'value' => 'total');
	$page["var"][] = array('variable' => 'total_amount', 'value' => 'total_amount');
    $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
 
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-service.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>