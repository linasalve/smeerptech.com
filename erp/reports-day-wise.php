<?php


if ( $perm->has('nc_rp_m_att') ) {    
    
    $date	= isset($_GET["date"]) 	? $_GET["date"]	: ( isset($_POST["date"]) 	? $_POST["date"] : '' );
    $_SEARCH["date"] 	= $date;
    $allData = $_GET 	? $_GET	: ($_POST 	? $_POST : '' );
    $extra_url=$pagination ='';
    $reportList=array();
    $db1 		= new db_local;
    //to view report date should be compulsary
    if(array_key_exists('submit_search',$allData)){
    
        if(!empty($date)){
            $dateArr= explode("/",$date);
            $checkDate = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            

            $query = "SELECT COUNT(*) as count FROM ". TABLE_PROJECT_TASK_DETAILS.",".TABLE_USER
					." WHERE  ". TABLE_PROJECT_TASK_DETAILS .".added_by = ". TABLE_USER .".user_id AND  (  (parent_id !=0 ) OR  (task_id != 0 AND parent_id=0) ) AND DATE_FORMAT(".TABLE_PROJECT_TASK_DETAILS .".do_e,'%Y-%m-%d') = '".$checkDate."'" ;
            $query .= " GROUP BY ".TABLE_PROJECT_TASK_DETAILS.".added_by, ".TABLE_PROJECT_TASK_DETAILS.".project_id ";
            $query .= " ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".added_by "; 
            
			$db->query($query) ;
            if($db->nf() > 0){
                $total = $db->f('count');
            }else{
                $total = 0 ;
            }
			
            $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
			$query = "SELECT ". TABLE_PROJECT_TASK_DETAILS .".added_by "
							.", ". TABLE_USER .".f_name"
							.", ". TABLE_USER .".l_name "
							//.", ". TABLE_PROJECT_TASK_DETAILS .".details "
							.", ( SUM(".TABLE_PROJECT_TASK_DETAILS .".hrs) + ( SUM(". TABLE_PROJECT_TASK_DETAILS .".min) DIV 60 ))  as totHr "
                            .", ( SUM(". TABLE_PROJECT_TASK_DETAILS .".min) % 60 ) as totMin"
							.", ". TABLE_PROJECT_TASK_DETAILS .".project_id"
                	." FROM ". TABLE_PROJECT_TASK_DETAILS.",".TABLE_USER
					." WHERE  ". TABLE_PROJECT_TASK_DETAILS .".added_by = ". TABLE_USER .".user_id AND  (  (parent_id !=0 ) OR  (task_id != 0 AND parent_id=0) ) AND DATE_FORMAT(".TABLE_PROJECT_TASK_DETAILS .".do_e,'%Y-%m-%d') = '".$checkDate."'" ;
            $query .= " GROUP BY ".TABLE_PROJECT_TASK_DETAILS.".added_by, ".TABLE_PROJECT_TASK_DETAILS.".project_id ";
            $query .= " ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".added_by "; 
            $query .= " LIMIT ". $next_record .", ". $rpp ;
		    $db->query($query) ;
            while($db->next_record()){
                $round1 =0;
			    //$list[]=processSqlData($db->result());
                $name = $db->f('f_name')." ".$db->f('l_name');
                $user_id = $db->f('added_by');
                $project_id = $db->f('project_id');
                
                $subQuery = "SELECT order_title FROM ".TABLE_BILL_ORDERS." WHERE ".TABLE_BILL_ORDERS.".id = ".$project_id;
                $db1->query($subQuery) ;
                while($db1->next_record())  {
                    $order_title = $db1->f('order_title');
                }
                //$order_title = $db->f('order_title');
                $totHr = $db->f('totHr');
                $totMin = $db->f('totMin');
                $totalHrs = $totHr.":".$totMin;
                
                // To Calculate total hrs and min of executive
                $grandTothr[$user_id][] = $totHr;
                $grandTotmin[$user_id][] = $totMin;
                
                $grandTotHr = array_sum($grandTothr[$user_id]);
                $grandTotMin = array_sum($grandTotmin[$user_id]);
                
                if($grandTotMin >= 60){
                    $round1 =  (int) ($grandTotMin/ 60);
                    $grandTotMin = $grandTotMin % 60 ;                     
                }
                $grandTotHr = $grandTotHr + $round1;
                
                $comment = '';
                // To find out the last comment
                $subQuery1 =" SELECT details FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE project_id = '".$project_id ."' AND ".TABLE_PROJECT_TASK_DETAILS.".added_by='".$user_id."'
                     AND  (  (parent_id !=0 ) OR  (task_id != 0 AND parent_id=0) ) AND DATE_FORMAT(".TABLE_PROJECT_TASK_DETAILS .".do_e,'%Y-%m-%d') = '".$checkDate."'";
                $db1->query($subQuery1);
                if ( $db1->nf() > 0 ) {
                    while ($db1->next_record()) {
                        $comment[] =$db1->f('details');
                    }
                }
                            
                $reportList[$user_id][$name][$project_id] = array('order'=>$order_title,
                                                                'hrs'=> $totalHrs,
                                                                //'comment'=> $comment,
                                                                ); 
                
                $grandtotalHrs = $grandTotHr.":".$grandTotMin ;
                //$reportList[$user_id]['comment'] = $comment;
                $reportList[$user_id]['TOTAL'][1111] = array('order'=>$grandtotalHrs);
                
                
            }
            $extra_url  = '';
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $extra_url  .= "&x=$x&rpp=$rpp&date=$date&submit_search=1";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            
        }else{
            $messages->setErrorMessage('Please select Date.');
        }
    }   
            
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    
    $page["var"][] = array('variable' => 'list', 'value' => 'reportList');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'totalHrsArr', 'value' => 'totalHrsArr');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-day-wise.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>