<?php
	if ( $perm->has('nc_nav_delete') ) {
		$id	= isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'access_level' 	=> $my['access_level'],
						'messages' 		=> $messages
					);
		Navigation::delete($id, $extra);
		$variables['hid'] = $id;
		// Display the list.
		include ( DIR_FS_NC .'/navigation-main-list.php');
	}
	else {
		$messages->setErrorMessage("You donot have the Right to Access this module.");
	}
?>
