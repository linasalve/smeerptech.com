<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php');
	   
	
   
    
	$ord_no =  isset($_GET["ord_no"])   ? $_GET["ord_no"]  : ( isset($_POST["ord_no"]) ? $_POST["ord_no"] : '' );
	$id =  isset($_GET["id"])   ? $_GET["id"]  : ( isset($_POST["id"]) ? $_POST["id"] : '' );
    $lead_closing_dt = isset($_GET["lead_closing_dt"])? $_GET["lead_closing_dt"]:( isset($_POST["lead_closing_dt"])?$_POST["lead_closing_dt"] : '' );
    
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new 		= new db_local; // database handle
	if(!empty($lead_closing_dt)){
		$arr = explode('/',$lead_closing_dt); 
		$lead_closing_dt1 = $arr['2']."-".$arr['1']."-".$arr['0']." 00:00:00" ;
		$lead_closing_dt2 = mktime(0, 0, 0, $arr['1'], $arr['0'], $arr['2']);
		$lead_closing_dt3 = mktime(0, 0, 0, date('m'),  date('d'),  date('Y'));
		$lead_closing_dt_txt = date('d M Y',strtotime($lead_closing_dt1 ));
		
		if($lead_closing_dt2 <= $lead_closing_dt3){
			$messageText = 'Please select upcoming date for closing lead';
			$error_message = "<table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_error_header\">
							<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\" border=\"0\" />&nbsp;
							Not Success
						</td>
					</tr>
					<tr>
						<td class=\"message_error\" align=\"left\" valign=\"top\">
							<ul class=\"message_error\">
								<li class=\"message_error\">".$messageText." </li>
							</ul>
						</td>
					</tr>
					</table>";
			echo $error_message."|".$id."|";
			exit;
		}
	}
	
    if (isset($id) && $id!=''){  
		$sql = " UPDATE ".TABLE_PROSPECTS_ORDERS." SET ".TABLE_PROSPECTS_ORDERS.".lead_closing_dt = '".$lead_closing_dt1."', 
				".TABLE_PROSPECTS_ORDERS.".lead_closing_dt_by = '".$my['uid']."', 
				".TABLE_PROSPECTS_ORDERS.".lead_closing_dt_by_name 	 = '".$my['f_name']." ".$my['l_name']."', 
				".TABLE_PROSPECTS_ORDERS.".lead_closing_dt_added 	 = '".date('Y-m-d')."', 
				".TABLE_PROSPECTS_ORDERS.".status = '".ProspectsOrder::ACTIVE."' 
				WHERE ".TABLE_PROSPECTS_ORDERS.".id ='".$id."'";  
		$execute = $db_new->query($sql);
		
		$query	= " INSERT INTO ".TABLE_PROSPECTS_ORD_CLOSE_DT." SET ".TABLE_PROSPECTS_ORD_CLOSE_DT .".ord_no = '". $ord_no ."'"  
			.",". TABLE_PROSPECTS_ORD_CLOSE_DT.".ord_id = '". $id ."'"
			.",". TABLE_PROSPECTS_ORD_CLOSE_DT.".lead_closing_dt = '".$lead_closing_dt1."'"
			.",". TABLE_PROSPECTS_ORD_CLOSE_DT.".added_by = '".$my['uid'] ."'"                                
			.",". TABLE_PROSPECTS_ORD_CLOSE_DT.".added_by_name = '".$my['f_name']." ".$my['l_name']."'"                                
			.",". TABLE_PROSPECTS_ORD_CLOSE_DT.".ip = '".$_SERVER['REMOTE_ADDR'] ."'"                                
			.",". TABLE_PROSPECTS_ORD_CLOSE_DT.".do_added = '". date('Y-m-d')."'"; 
		$execute = $db_new->query($query);
		
		if($execute){
			$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
			<tr>
				<td class=\"message_header_ok\">
					<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
					Success
				</td>
			</tr>
			<tr>
				<td class=\"message_ok\" align=\"left\" valign=\"top\">
					<ul class=\"message_ok_2\">
						<li class=\"message_ok\">Record updated successfully </li>
					</ul>
				</td>
			</tr>
			</table>"; 				
		} 
	}	

echo $message."|".$id."|".$lead_closing_dt_txt  ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
