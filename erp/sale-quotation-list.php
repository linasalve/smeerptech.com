<?php

    if ( $perm->has('nc_sl_ld_q_list') ) {
	
		//include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
        $and_added = false ;
        if ( !isset($condition_query) || $condition_query == '' ) {
           $condition_query = ' WHERE (';
           $where_added = true;
        }
        else {
            $condition_query .= ' AND (';
            $and_added = true;
        }
        
        $access_level   = $my['access_level'];       
        /*        
        // If the User has the Right to View Leads of the same Access Level.
        if ( $perm->has('nc_sl_ld_qe_list_al') ) {
            $access_level += 1;
        }
        // If my is the Manager of this Client.
        //$condition_query .= " ("." AND ". TABLE_CLIENTS .".access_level < $access_level ) ";
        */
        
        // Check if the User has the Right to view Leads of other Executive.
        if ( $perm->has('nc_sl_ld_q_list_ot') ) {            
            $access_level_o   = $my['access_level'];
            $access_level_o += 1;
            /*
            if ( $perm->has('nc_sl_ld_qe_list_ot_al') ) {
                $access_level_o += 1;
            }
            */
           
            $condition_query .= " ( ". TABLE_SALE_QUOTATION .".access_level < $access_level_o ) ";
        }
        
        
        
        if ( $perm->has('nc_sl_ld_q_list_my') ) {
              if ( $perm->has('nc_sl_ld_q_list_ot') ) { 
                 $condition_query .= " OR ";
              }

              $condition_query .= "  (". TABLE_SALE_QUOTATION .".created_by = '". $my['user_id'] ."' "." ) ";
        }
        $condition_query .= ')';
                
        
        if ( $perm->has('nc_sl_ld_q_list_my') ||  $perm->has('nc_sl_ld_q_list_ot')) {
               $condition_query .= " AND ";
        }
        
        $condition_query .= ' '.TABLE_SALE_QUOTATION.".parent_q_id ='0' ";
        
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $fields = TABLE_SALE_QUOTATION.'.id ' ;
        $total	=	Quotation::getDetails( $db, $list, '', $condition_query);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
       
       
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        // Set the Permissions here to pass the value of can_view_file & and can_send_file in quotation.inc.php
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_view_file'] = false;
        $variables['can_send_file'] = false;
        //$variables['can_create_invoice']= false;
        
        
        if ( $perm->has('nc_sl_ld_q_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_sl_ld_q_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_sl_ld_q_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_sl_ld_q_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_sl_ld_q_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_sl_ld_q_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_sl_ld_q_fview') ) {
            $variables['can_view_file'] = true;
        }
        if ( $perm->has('nc_sl_ld_q_fsend') ) {
            $variables['can_send_file'] = true;
        }
        
        $list	= NULL;
        $fields = TABLE_SALE_QUOTATION .'.id'
                    .','. TABLE_SALE_QUOTATION .'.number'
					//.','. TABLE_SALE_QUOTATION .'.subject'
                    .','. TABLE_SALE_QUOTATION .'.access_level'
                    .','. TABLE_SALE_QUOTATION .'.do_e'
                    .','. TABLE_SALE_QUOTATION .'.status'
                    .','. TABLE_SETTINGS_QUOT_SUBJECT .'.title as subject'
                    .','. TABLE_SALE_LEADS .'.lead_number' 
                    .','. TABLE_SALE_LEADS .'.f_name' 
                    .','. TABLE_SALE_LEADS .'.m_name' 
                    .','. TABLE_SALE_LEADS .'.l_name' 
                    .','. TABLE_SALE_LEADS .'.org' ;
        Quotation::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
       $db1 		= new db_local;
        $flist=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){               
               $catg_list=null;
                $val['createcopy'] =    false;   
              
                $sideMenu  ='';
                $categoryid =0;
                $parentArr = null;
                $uptolevel ='';
                /*
                //Check whether child of quote id created or not. If child created  'createcopy'=false 
                //also if status of quote id is active, pending 'createcopy'=false else ie status rejected 'createcopy'=true
                //that means  If child !created  && status rejected   'createcopy'=true
                */
                $group_id =$val['id'];
                $level =1;
                Quotation::getLevelQuote( $db1, $catg_list, $group_id, $level);
                Quotation::createSublevel($catg_list,$sideMenu,$categoryid,$parentArr,$uptolevel,$variables);
                //$val['submenu']=$catg_list;
                if(!empty($sideMenu)){
                     $table = "<table cellpadding='0' cellspacing='0' border='0' width='100%' width='100%' >".$sideMenu."</table>";
                     $val['createcopy'] =    false;   
                }else{
                
                    $table ='';
                    if( $val['status']==Quotation::REJECTED ){
                      $val['createcopy'] =    true;   
                    }
                }
               
                $val['sideMenu']=$table;
                
                $val['fullname']=$val['f_name']." ".$val['m_name']." ".$val['l_name'] ; 
                
               $flist[$key] =    $val;
            }
        }
		

        
        /*if ( $perm->has('nc_sl_pq_add') ) {
            $variables['can_create_invoice'] = true;
        }*/
        //print_r($flist);
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
		    
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-quotation-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>