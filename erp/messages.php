<?php

if($success_messages[0] != ""){
?>
    <table id="table_message_ok" border="0" cellspacing="0" cellpadding="1" align="center">
    <tr>
        <td class="message_header_ok">
        	<img src="<?php echo $variables['nc_images'].'/success.png';?>" border="0">&nbsp;
        	Success
        </td>
    </tr>
    <tr>
        <td class="message_ok" align="left" valign="top">
			<ul class="message_ok_2">
			<?php
			foreach($success_messages as $key=>$success){
				echo "<li class=\"message_ok\">".$success."</li>";
			}
			?>
			</ul>
        </td>
    </tr>
    </table>
<?php
}
if($error_messages[0] != ""){
?>
    <table id="table_message_error" border="0" cellspacing="0" cellpadding="2" align="center">
    <tr>
        <td class="message_error_header" height="10px">
			<img src="<?php echo $variables['nc_images'].'/exclamation.png';?>" border="0">&nbsp;
			Not Success
        </td>
    </tr>
    <tr>
		<td class="message_error" align="left" valign="top">
        	<ul class="message_error">
			<?php
			foreach($error_messages as $key=>$error){
				echo "<li class=\"error\">".$error."</li>";
			}
			?> 
       		</ul>
        </td>
    </tr>
    </table>
<?php
}
?>