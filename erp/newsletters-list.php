<?php

    if ( $perm->has('nc_nwl_list') ) {
        
     
                            
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
          //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        
        $total	=	Newsletters::getList( $db, $list, '', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
       
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';        
        $list	= NULL;         
        $fields = TABLE_NEWSLETTERS.".*,".TABLE_CLIENTS.".f_name,".TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name,".TABLE_NEWSLETTERS_MEMBERS_CATEGORY.".category";
        Newsletters::getList( $db, $list,$fields, $condition_query, $next_record, $rpp);
        
		
		
		$variables['can_add'] = false;
		$variables['can_edit'] = false;
		$variables['can_view_list'] = false;
		$variables['can_copy'] = false;
		$variables['draft'] = Newsletters::DRAFT;
		
        if ( $perm->has('nc_nwl_add') ) {
            $variables['can_add'] = true;
        }
		 if ( $perm->has('nc_nwl_copy') ) {
            $variables['can_copy'] = true;
        }
        if ( $perm->has('nc_nwl_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_nwl_list') ) {
            $variables['can_view_list'] = true;
        }
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletters-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>