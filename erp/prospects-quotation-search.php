<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
  
    $searchStr = 0;
    if ( $sString != "" ) 
    {
        $searchStr = 1;
        
         // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
       
        //$where_added = true;
        $sub_query='';        
        if ( $search_table == "Any" ) 
        {
            
            if ( $where_added ) 
            {
                $condition_query .= "OR ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ) 
            {
              
                if ( $table != "Any" ){ 
                    
                    if ($table == TABLE_PROSPECTS_QUOTATION){
                        $sub_query .= "( " ;
                        foreach( $field_arr as $key => $field ){
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            /*
                            if($field !='number' && $field !='or_no' && $field !='id' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                            
                    }elseif($table == TABLE_PROSPECTS_ORD_P){
                        $sub_query .= "( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            //if($field !='number' && $field !='or_no' && $field !='id' ){
                              //  $sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                            //}else{
                              //    $sub_query .= $table .".". clearSearchString($field) ." = '".trim($sString)."' OR " ;
                            //}
                            //$sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    }elseif($table == TABLE_USER){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if($field !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_PROSPECTS_ORDERS){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if($field !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_PROSPECTS){
                    
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                             $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if($field !='number'){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }
                            */
                           
                            //$sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }
                    
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= " AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                /*
                if($sType !='number' && $sType !='or_no' && $sType !='id' ){
                    $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                }else{
                     $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ."='".trim($sString) ."' )" ;
                }*/
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
    //code for search EOF 2009-03-mar-21 
    // BO: Status data.
    $sStatusStr = '';
    $sStatusStrPass = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR') && isset($sStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }        
        /*
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStrPass = implode(",", $sStatus) ;
        }else{
            $sStatusStrPass =$sStatus;
            $sStatusStr = str_replace(",","','",$sStatus) ;
            $sStatus = explode(",", $sStatus) ;
        }*/
        
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ; 
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        } 
        $condition_query .= " ". TABLE_PROSPECTS_QUOTATION .".status IN ('". $sStatusStr ."') "; 
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
    } 
    // EO: Status data.
	
	// BO: Status data.
    $aStatusStr = '';
    $aStatusStrPass = '';
    $chk_astatus = isset($_POST["chk_astatus"])   ? $_POST["chk_astatus"]  : (isset($_GET["chk_astatus"])   ? $_GET["chk_astatus"]   :'');
    $aStatus    = isset($_POST["aStatus"])      ? $_POST["aStatus"]     : (isset($_GET["aStatus"])      ? $_GET["aStatus"]      :'');
    if ( ($chk_astatus == 'AND' || $chk_astatus == 'OR') && isset($aStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_astatus;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }        
        
        
        if(is_array($aStatus)){
             $aStatusStr = implode("','", $aStatus) ;
             $aStatusStr1 = implode(",", $aStatus) ; 
        }else{
            $aStatusStr = str_replace(',',"','",$aStatus);
            $aStatusStr1 = $sStatus ;
            $aStatus = explode(",",$aStatus) ;
            
        } 
        $condition_query .= " ". TABLE_PROSPECTS_QUOTATION .".quotation_status IN ('". $aStatusStr ."') "; 
        $condition_url          .= "&chk_astatus=$chk_astatus&aStatus=$aStatusStr1";
        $_SEARCH["chk_astatus"]  = $chk_astatus;
        $_SEARCH["aStatus"]     = $aStatus;
    } 
    // EO: Status data.
    //User Search BOF
	$user_id = isset($_POST["user_id"])   ? $_POST["user_id"]  : (isset($_GET["user_id"])   ? $_GET["user_id"]   :'');
	if(!empty($user_id)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		
		$condition_query .= " (
		(". TABLE_PROSPECTS_ORDERS .".created_by = '". $user_id ."' "."  ) ";
		// If my is present in the Team.
		$condition_query .= " OR ( ". TABLE_PROSPECTS_ORDERS .".team LIKE '%,". $user_id .",%' ". "  "."  ) )";
							
		$condition_url  .= "&user_id=$user_id";
        $_SEARCH["user_id"]  = $user_id;
	}	
	//User Search EOF
	
	//Executive Search BOF
	$search_executive_id = isset($_POST["search_executive_id"]) ? $_POST["search_executive_id"]  : (isset($_GET["search_executive_id"]) ? $_GET["search_executive_id"]   :'');
	$search_executive_details = isset($_POST["search_executive_details"])   ? $_POST["search_executive_details"]  : (isset($_GET["search_executive_details"])   ? $_GET["search_executive_details"]   :'');
	if(!empty($search_executive_id)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		
		$condition_query .= " ( (". TABLE_PROSPECTS_ORDERS .".order_closed_by = '". $search_executive_id ."' "."  ) ";
		// If my is present in the Team.
		$condition_query .= " OR ( "
								. TABLE_PROSPECTS_ORDERS .".team LIKE '%,". $search_executive_id .",%' "
							. "  "
							."  ) ) ";
							
		$condition_url  .= "&search_executive_details=$search_executive_details&search_executive_id=$search_executive_id";
        $_SEARCH["search_executive_id"]  = $search_executive_id;
        $_SEARCH["search_executive_details"] = $search_executive_details;     							
	}
	//Executive  serach eof
	
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
 
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from) ){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_PROSPECTS_QUOTATION .".".$dt_field." >= '". $dfa ."'";        
        $condition_url  .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;
    }
    
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_PROSPECTS_QUOTATION .".".$dt_field." <= '". $dta ."'";
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }    
    // EO: Upto Date
    

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/prospects-quotation-list.php');
?>