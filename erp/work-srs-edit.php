<?php
    if ( $perm->has('nc_wk_srs') ) {
       
         $or_id 		= isset($_GET["or_id"]) ? $_GET["or_id"]        : ( isset($_POST["or_id"])          ? $_POST["or_id"]       :'');
        $srs_id = isset($_GET['srs_id']) ? $_GET['srs_id'] : ( isset($_POST['srs_id'] ) ? $_POST['srs_id'] :'');

        $list           = NULL;
        $_ALL_POST      = NULL;
		$hidden      	= NULL;
		$order      	= NULL;
        $access_level   = $my['access_level'];

		include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
		include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
        
		//Read the available srs type
        $srsType=ProjectTask::getSrsType();
        
		if ( Order::getList($db, $order, 'id', " WHERE id = '$or_id'" ) > 0 ) {
			$order = $order[0];
			
			$extra 			= array('db'        => &$db,
									'my'		=> $my,
									'messages'  => &$messages
								);
       
			if ( isset($_POST['btnSubmit']) && $_POST['act'] == 'save' ) {
				$data 		= processUserData($_POST);
				$_ALL_POST 	= $_POST;
				if(empty($srs_id) ){
                    if ( ProjectTask::validateSRS($data, $extra) ) {
                         $query = "INSERT INTO ". TABLE_WORK_SRS
                            ." SET "
                                ." order_id = '". $data['or_id'] ."'"
                                .",added_by = '". $my['user_id'] ."'"
                                .",added_by_name = '". $my['f_name']." ".$my['l_name']."'"                               
                                .",srs_type = '". $data['srs_type'] ."'"
                                .",srs_text = '". $data['srs_text'] ."'"
                                .",ip       = '". $_SERVER['REMOTE_ADDR'] ."'"
                                .",do_a 	= '". date('Y-m-d H:i:s', time()) ."'" ;
                        if ( $db->query($query) && ($db->affected_rows() > 0) ) {
                            $messages->setOkMessage("SRS was saved.");
                        }else {
                            $messages->setErrorMessage("SRS was not saved, please try again later.");
                        }
                            
                        $_ALL_POST 	= '';
                    }
                }else{
                    
                    if ( ProjectTask::validateSRS($data, $extra) ) {
                        $query = "UPDATE ". TABLE_WORK_SRS
                            ." SET "
                                ."updated_by = '". $my['user_id'] ."'"
                                .",updated_by_name = '". $my['f_name']." ".$my['l_name']."'"
                                .",srs_type = '". $data['srs_type'] ."'"
                                .",srs_text = '". $data['srs_text'] ."'"
                                .",do_update 	= '". date('Y-m-d H:i:s', time()) ."' 
                                WHERE id ='".$srs_id."'" ;
                        if ( $db->query($query) && ($db->affected_rows() > 0) ) {
                            $messages->setOkMessage("SRS was updated.");
                        }else {
                            $messages->setErrorMessage("SRS was not updated, please try again later.");
                        }
                         $_ALL_POST 	= '';
                         $srs_id='';
                    }
                }
                
			}else{
                if(isset($srs_id) && !empty($srs_id)){
                   $condition_query1= "WHERE order_id = '".$or_id."' AND id='".$srs_id."'";	
                   $fields='*';
                   ProjectTask::getSRS($db,$dataDetails,$fields, $condition_query1);
                   $_ALL_POST 	= $dataDetails[0];
                  
                }
                
            }
            $condition_query="";
            if ( isset($_POST['btnSearch']) && $_POST['search'] == 'go' ) {
				
                $data 		= processUserData($_POST);
				$_ALL_POST 	= $_POST;
			    
                $condition_query="AND srs_type='".$_ALL_POST['srs_type']."'";	
			}
			$fields='*';
            $condition_query= " WHERE order_id = '".$or_id."' ".$condition_query;
            ProjectTask::getSRS( $db,$list,$fields, $condition_query);
            
			
            $page["var"][] = array('variable' => 'srsType', 'value' => 'srsType');
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'list', 'value' => 'list');
            //$page["var"][] = array('variable' => 'order_id', 'value' => $order_id);
		}
        else {
            $messages->setErrorMessage("The Order was not found.");
		}
		
		$hidden[] = array('name'=> 'perform','value' => 'edit');
		$hidden[] = array('name'=> 'act', 'value' => 'save');
        $hidden[] = array('name'=> 'search', 'value' => 'go');
        $hidden[] = array('name'=> 'or_id', 'value' => $or_id);
        $hidden[] = array('name'=> 'srs_id', 'value' => $srs_id);
		
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => 'or_id', 'value' => 'or_id');
		$page["var"][] = array('variable' => 'srs_id', 'value' => 'srs_id');
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'work-srs-edit.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>