<?php
if ( $perm->has('nc_letr') ) {
    
        $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');
        $file   = isset ($_GET['file']) ? $_GET['file'] : ( isset($_POST['file'] ) ? $_POST['file'] :'');
        
        $condition_query= " WHERE ". TABLE_LETTERS .".id = '". $id ."'";
        
        $list	= NULL;
        if ( Letters::getDetails( $db, $list, TABLE_LETTERS .'.id, '. TABLE_LETTERS .'.'.$file, $condition_query) ) {
          
            if ( $id>0 ) {
                $file_path = DIR_FS_LETTERS_FILES ."/";
                $file_name = $list[0][$file]  ;
               // $content_type = mime_content_type(DIR_FS_LETTERS_FILES ."/".$file_name);
				                 
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				$content_type = finfo_file($finfo, DIR_FS_LETTERS_FILES ."/".$file_name);
				finfo_close($finfo);
				
            } 
            else {
                echo('<script language="javascript" type="text/javascript">alert("Invalid File specified");</script>');
            }
            
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-type: $content_type");
            header("Content-Disposition: attachment; filename=". $file_name );
            header("Content-Length: ".filesize($file_path.$file_name));
            readfile($file_path.$file_name);
        }else{
            echo('<script language="javascript" type="text/javascript">alert("The File is not found or you do not have the permission to view the file.");</script>');
        }
    }
    else {
        echo('<script language="javascript" type="text/javascript">alert("You do not have the permission to view the list.");</script>');
    }
    exit;
?>