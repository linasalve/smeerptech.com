<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/prospects.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include ( DIR_FS_INCLUDES .'/sale-industry.inc.php');
	
	
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
    $added 		= isset($_GET["added"]) ? $_GET["added"]: ( isset($_POST["added"]) ? $_POST["added"]       :'0');
    $sendinv    = isset($_GET["sendinv"]) ? $_GET["sendinv"]        : ( isset($_POST["sendinv"])          ? $_POST["sendinv"]       :'0');
    $x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $condition_url='';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if($added){
         $messages->setOkMessage("Prospect entry has been done.");
    }
    if($sendinv){
         $messages->setOkMessage("Invitation to prospect has been sent successfully.");
    }
    
    $searchLink=0;
    if ( $perm->has('nc_pr') ) {

        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_PROSPECTS   =>  array( 'Relationship Number'        => 'number',
                                                            'Billing Name'  => 'billing_name',
                                                            'User Name'     => 'username',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Middle Name'   => 'm_name',
                                                            'Last Name'     => 'l_name',
                                                            'Pet Name'      => 'p_name',
                                                            'Designation'   => 'desig',
                                                            'Organization'  => 'org',
															'Site'  		=> 'site_url',
                                                            'Domain'        => 'domain'
                                                        )
                            );
        
        $sOrderByArray  = array(
                                TABLE_PROSPECTS => array('Relationship Number'   => 'number',
                                                    'User Name'     => 'username',
                                                    'E-mail'        => 'email',
                                                    'First Name'    => 'f_name',
                                                    'Last Name'     => 'l_name',
                                                    'Date of Birth' => 'do_birth',
                                                    'Date of Regis.'=> 'do_reg',
                                                    'Date of Add.'=> 'do_add',
                                                    'Date of Login' => 'do_login',
                                                    'Status'        => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_add';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PROSPECTS;
        }
    
        $variables['title_type'] = Prospects::getTitleType();
        $variables['status'] = Prospects::getStatus();
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add_prospects'):
            case ('add'): {
            
                include (DIR_FS_NC.'/prospects-add.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('qadd'): {
            
                include (DIR_FS_NC.'/prospects-qadd.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
			case ('lblprint'): {
                include (DIR_FS_NC .'/prospects-label-print.php'); 
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('copy'): {
                include (DIR_FS_NC.'/prospects-to-clients.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('invite'): {
            
                include (DIR_FS_NC.'/prospects-invite.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
                if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            }
            case ('edit_prospects'):
            case ('edit'): {
                include (DIR_FS_NC .'/prospects-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
				if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            }
/*          case ('editprofile'): {
                include (DIR_FS_NC .'/myedit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        */
            case ('view_prospects'):
            case ('view'): {
                include (DIR_FS_NC .'/prospects-view.php');
               
				$page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
				
                break;
            }
            case ('send_pwd'):
            {
                include (DIR_FS_NC .'/prospects-send-pwd.php');
                
                if($ajx==0){
					$page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
				}
                break;
            }
            case ('change_status'): {
                $searchStr=1;
                include ( DIR_FS_NC .'/prospects-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
				
				if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            }
            case ('change_mail_status'): {
                
                include ( DIR_FS_NC .'/prospects-mail-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
				if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('search'): {
                $searchLink=1;
                include(DIR_FS_NC."/prospects-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('delete_prospects'):
            case ('delete'): {
                include ( DIR_FS_NC .'/prospects-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
				if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('list_prospects'):{
                 $searchStr = 0;
                 //include (DIR_FS_NC .'/prospects-list.php');
                 include (DIR_FS_NC .'/prospects-select-search.php');
                // CONTENT = CONTENT
                /*
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-select.html');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-search.html');
                */
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-select.html');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-select-search.html');
                
                $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
                break;
            }
            case ('list_specific_prospects'):{
                include (DIR_FS_NC .'/prospects-specific-list.php');
                include (DIR_FS_NC .'/prospects-specific-select-search.php');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-specific-select.html');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-select-search.html');
                
                $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
                break;
            }
            
            
            case ('edit_sub_user'): {
                include (DIR_FS_NC .'/prospects-sub-user-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
				if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('list_sub_user'): {
                include (DIR_FS_NC .'/prospects-sub-user-list.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
				if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('delete_sub_user'): {
                include (DIR_FS_NC .'/prospects-sub-user-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
                if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            }
            case ('change_status_sub_user'): {
                include ( DIR_FS_NC .'/prospects-sub-user-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
                if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            }
            case ('view_sub_user'): {
                include (DIR_FS_NC .'/prospects-sub-user-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('list'):
            default: {
                $searchStr = 1;
                include (DIR_FS_NC .'/prospects-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
                if($ajx==0){
				
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>