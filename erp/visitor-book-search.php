<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    
                    $table = TABLE_VISITOR_BOOK ;
                    foreach( $field_arr as $key => $field ) {
                            $condition_query .= " ". $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                        }
                    $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
                }
            }
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
    /*
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( $chk_status == 'AND' || $chk_status == 'OR') {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".status IN ('". implode("','", $sStatus) ."') ";
    }
    $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatus";
    $_SEARCH["chk_status"]  = $chk_status;
    $_SEARCH["sStatus"]     = $sStatus;
    // EO: Status data.
    */
	
	// EO: Status
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ( $chk_status == 'AND' ) &&  isset($sStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		//Chk Proforma
		$condition_query .= " ". TABLE_VISITOR_BOOK .".status ='".$sStatus."' ";        
		
       
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatus";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;        
    }    
    // EO: Status
	// EO: IMP Status
    $chk_imp = isset($_POST["chk_imp"])   ? $_POST["chk_imp"]  : (isset($_GET["chk_imp"])   ? $_GET["chk_imp"]   :'');
    $imp    = isset($_POST["imp"])      ? $_POST["imp"]     : (isset($_GET["imp"])      ? $_GET["imp"]      :'');
    if ( ( $chk_imp == 'AND' ) &&  isset($imp)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_imp;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		//Chk Proforma
		$condition_query .= " ". TABLE_VISITOR_BOOK .".important ='".$imp."' ";        
		
       
        $condition_url          .= "&chk_imp=$chk_imp&imp=$imp";
        $_SEARCH["chk_imp"]  = $chk_imp;
        $_SEARCH["imp"]     = $imp;        
    }    
    // EO: IMP Status
	
	
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    
    // BO: From Date
    if ( $chk_date_from == 'AND' || $chk_date_from == 'OR' && !empty($date_from)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_VISITOR_BOOK .".date >= '". $dfa ."'";
    
    $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
    $_SEARCH["chk_date_from"]   = $chk_date_from;
    $_SEARCH["date_from"]       = $date_from;
    
    }
    // EO: From Date
    
    // BO: Upto Date
    if ( $chk_date_to == 'AND' || $chk_date_to == 'OR' && !empty($date_to)) {
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_VISITOR_BOOK .".date <= '". $dta ."'";    
    	$condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
    	$_SEARCH["chk_date_to"] = $chk_date_to ;
    	$_SEARCH["date_to"]     = $date_to ;
    }
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/visitor-book-list.php');
?>
