<?php

    if ( $perm->has('nc_site_sp_add') ) {
    
        $page_id = isset($_GET["page_id"]) ? $_GET["page_id"] : ( isset($_POST["page_id"]) ? $_POST["page_id"] : '' );
        $act	= isset($_GET["act"]) ? $_GET["act"] : (isset($_POST["act"]) ? $_POST["act"] : '');
        
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        $al_list    = getAccessLevel($db, $my['access_level']);
        
        //code for remove banner BOF
        if(isset($act) && $act =='rmvbanner' && !empty($page_id)){
       
            $getData=array();
            $table = TABLE_SETTINGS_STATICPAGES ;
            $condition2 = " WHERE ".TABLE_SETTINGS_STATICPAGES.".page_id= '".$page_id ."' " ;
            $fields1 =  TABLE_SETTINGS_STATICPAGES.'.banner,'.TABLE_SETTINGS_STATICPAGES.'.is_default_banner' ;
            $getData= getRecord($table,$fields1,$condition2);
            if(!empty($getData['banner'])){
               $banner_name= $getData['banner'] ;
               @unlink(DIR_FS_BANNER ."/". $banner_name);            
            }
            if($getData['is_default_banner']=='0'){
                $query_update = "UPDATE ".TABLE_SETTINGS_STATICPAGES
                                        ." SET ".TABLE_SETTINGS_STATICPAGES.".banner	= '' "
                                        .",".TABLE_SETTINGS_STATICPAGES.".is_default_banner	= '0'
                                         WHERE ".TABLE_SETTINGS_STATICPAGES .".page_id		= '". $page_id ."'";
                $db->query($query_update);
                $messages->setOkMessage("Banner deleted successfully.");
            }else{
                $messages->setErrorMessage("Sorry! unable to delete the banner as it is marked as default banner.");
            }
            
        }
        //code for remove banner EOF
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list
                        );

            if ( Staticpages::validateAdd($data, $extra) ) {
            
                // Code for banner BOF
                $data["banner"] = $banner_sub=$banner_name ='' ;
                if ( isset($_FILES) && @$_FILES['banner']['error'] == 0 && $_FILES['banner']['name'] != '' ) {
                    $srcSize 		= getimagesize($_FILES['banner']['tmp_name']);
                    $banner_name 	= eregi_replace(" ", "_", $_FILES['banner']['name']);
                    
                    if ( !in_array($srcSize["mime"], $BANNER['MIME']) ) {
                        $messages->setErrorMessage("The image type '". $srcSize["mime"] ."' is not supported.");
                    }
                    if ( $_FILES["banner"]["size"] > $BANNER['MAX_SIZE']['bytes'] ) {
                        $messages->setErrorMessage("The image size is more than supported of ". $BANNER['MAX_SIZE']['display'] ." .");
                    }
                    
                    if ( $srcSize['0'] > $BANNER['MAX_DIM']['width'] || $srcSize['1'] > $BANNER['MAX_DIM']['height'] ) {
                        $messages->setErrorMessage("The image dimension is larger than supported.");
                    }
                    if ( file_exists(DIR_FS_BANNER .'/'. $banner_name) ) {
                        $messages->setErrorMessage("A file with the same name already exists. Please rename and then upload.");
                    }
                }
                if( empty($_FILES['banner']['name'])  && $data["is_default_banner"]==1){
                    $messages->setErrorMessage("Please upload the default banner.");
                }
                // Code for banner EOF
                
                if ( $messages->getErrorMessageCount() <= 0 ) {
     
                    // There are no errors, copy the banner file.
                    // Make the Directory writable.
                    if(!empty($banner_name)){                    
                        // Make the Directory writable.
                        @chmod(DIR_FS_BANNER, 0777) ;
                        if ( @copy($_FILES['banner']['tmp_name'], DIR_FS_BANNER ."/". $banner_name) ) {
                            $data["banner"] = $banner_name;
                            $banner_sub = ",".TABLE_SETTINGS_STATICPAGES.".banner = '". $data["banner"] ."'"	;
                        }else{
                        $messages->setErrorMessage("The file was not uploaded, try again.");
                        }
                    }
                    $query = "INSERT INTO ". TABLE_SETTINGS_STATICPAGES
                        ." SET "
                            . TABLE_SETTINGS_STATICPAGES .".page_name			= '". $data["page_name"] ."'"
                        .",". TABLE_SETTINGS_STATICPAGES .".page_intro			= '". $data["page_intro"] ."'"
                        .",". TABLE_SETTINGS_STATICPAGES .".metakeyword			= '". $data["meta_keyword"] ."'"
                        .",". TABLE_SETTINGS_STATICPAGES .".metadesc			= '". $data["meta_desc"] ."'"
                        .",". TABLE_SETTINGS_STATICPAGES .".heading				= '". $data["page_heading"] ."'"
                        .$banner_sub
                        //.",". TABLE_SETTINGS_STATICPAGES .".banner				= '". $data["banner"] ."'"	
                        .",". TABLE_SETTINGS_STATICPAGES .".is_default_banner	= '". $data["is_default_banner"] ."'"	
                        .",". TABLE_SETTINGS_STATICPAGES .".content				= '". $data["content"] ."'"
                        .",". TABLE_SETTINGS_STATICPAGES .".show_heading		= '". $data["page_heading"] ."'"
                        .",". TABLE_SETTINGS_STATICPAGES .".page_status			= '". $data["status"] ."'"
                        .",". TABLE_SETTINGS_STATICPAGES .".last_edited_by		= '". $my['user_id'] ."'"
                        .",". TABLE_SETTINGS_STATICPAGES .".access_level		= '". $data['access_level'] ."'";
    
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        $variables['hid'] = $db->last_inserted_id();      
                        if($data["is_default_banner"] == 1){
                            $query1= "UPDATE ".TABLE_SETTINGS_STATICPAGES
                            ." SET ".TABLE_SETTINGS_STATICPAGES .".is_default_banner	= '0' "	
                                    ." WHERE ".TABLE_SETTINGS_STATICPAGES .".page_id != '". $variables['hid']."'";
                             $db->query($query1)   ;
                        }
                        $messages->setOkMessage("Page created successfully.");
                    }else{
                        $messages->setErrorMessage("Cannot create page. Try again.");
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
                }
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/staticpage-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'reminder_count', 'value' => '6');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'staticpage-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>