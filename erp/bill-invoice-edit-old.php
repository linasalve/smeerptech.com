<?php
    if ( $perm->has('nc_bl_inv_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        
        $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $or_no          = isset($_GET["or_no"]) ? $_GET["or_no"] : ( isset($_POST["or_no"]) ? $_POST["or_no"] : '' );

        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_inv_edit_al') ) {
            $access_level += 1;
        }
        
        $currency = NULL;
        $required_fields ='*';
        Currency::getList($db,$currency,$required_fields);
        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);

            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                           // 'lst_service'       => $lst_service,
                            'messages'          => &$messages
                        );

            if ( Invoice::validateUpdate($data, $extra) ) {
            
                if(!empty($data['do_e'])){
                    $data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                }
                if(!empty($data['do_fe'])){
                    $data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }                
                
                $query  = " UPDATE ".TABLE_BILL_INV
                            ." SET "
                                ."". TABLE_BILL_INV .".remarks = '".$data['remarks'] ."'"                              
                                .",". TABLE_BILL_INV .".exchange_rate = '". $data['exchange_rate'] ."'"
                                .",". TABLE_BILL_INV .".amount = '".        $data['amount'] ."'"
                                .",". TABLE_BILL_INV .".amount_inr = '".    $data['amount_inr'] ."'"
                                .",". TABLE_BILL_INV .".amount_words = '".  $data['amount_words'] ."'"
                                .",". TABLE_BILL_INV .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
                                .",". TABLE_BILL_INV .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
                                .",". TABLE_BILL_INV .".do_e = '".          $data['do_e'] ."'" //date of expiry
                                .",". TABLE_BILL_INV .".do_fe = '".         $data['do_fe']."'" //date of expiry from
                                .",". TABLE_BILL_INV .".access_level = '".  $data['access_level'] ."'"
                                .",". TABLE_BILL_INV .".remarks = '".       $data['remarks'] ."'"
                                .",". TABLE_BILL_INV .".balance = '".       $data['balance'] ."'"
                                .",". TABLE_BILL_INV .".balance_inr = '".   $data['amount_inr'] ."'"
                                .",". TABLE_BILL_INV .".amount_words = '".   $data['amount_words'] ."'"
                                .",". TABLE_BILL_INV .".billing_address ='". $data['billing_address'] ."'"
                                .",". TABLE_BILL_INV .".status ='". $data['status'] ."'"
                                ." WHERE ".TABLE_BILL_INV.".number ='".$data['number']."'";
                
               if ( $db->query($query) ) {
                    $messages->setOkMessage('The Invoice "'. $data['number'] .'" has been Updated.');
                    
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
                    Order::getParticulars($db, $temp, '*', $condition_query);
                    if(!empty($temp)){
                        foreach ( $temp as $pKey => $parti ) {
                            /*
                           if($temp[$pKey]['s_type']=='1'){
                                $temp[$pKey]['s_type']='Years';
                           }elseif($temp[$pKey]['s_type']=='2'){
                                $temp[$pKey]['s_type']='Quantity';
                           }*/
                            $temp_p[]=array(
                                            'p_id'=>$temp[$pKey]['id'],
                                            'particulars'=>$temp[$pKey]['particulars'],
                                            'p_amount' =>$temp[$pKey]['p_amount'] ,                                   
                                            's_type' =>$temp[$pKey]['s_type'] ,                                   
                                            's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                            's_amount' =>$temp[$pKey]['s_amount'] ,                                   
                                            's_id' =>$temp[$pKey]['s_id'] ,                                   
                                            'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                            'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                            'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                            'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                            'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                            'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                            'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                            'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                            'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                            'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                            );
                        }
                    }
                    
                    $data['particulars']=$temp_p ;
                    include_once ( DIR_FS_CLASS .'/Region.class.php');
                    $region         = new Region();
                    $region->setAddressOf(TABLE_CLIENTS, $data['client']['user_id']);
                    $addId = $data['billing_address'] ;
                    $address_list  = $region->get($addId);
                    $data['b_address'] = $address_list ;
                    
                    //get invoice details bof
                    
                    
                    // Create the Invoice PDF, HTML in file.
                    $extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';
                    
                    if ( !($attch = Invoice::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Invoice file was not created.");
                    }
                    
                }
                
                  
                /*
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage('The Invoice "'. $data['number'] .'" has been Updated.');
                  
                    
                    // Insert the Particulars.
                    
                    if ( !$db->query($data['query_p']) ) {
                        $messages->setErrorMessage("The Invoice Particulars were not Updated.");
                    }
                    
                    // Insert the Bifurcations.
                    
                    $query = "INSERT INTO ". TABLE_BILL_INV_B
                                ." SET inv_no = '".     $data['number'] ."'"
                                    .", domain = '".    $data['domain'] ."'"
                                    .", server1_rs = '".$data['server1_rs'] ."'"
                                    .", server2_sb = '".$data['server2_sb'] ."'"
                                    .", develop = '".   $data['develop'] ."'"
                                    .", product = '".   $data['product'] ."'"
                                    .", design = '".    $data['design'] ."'"
                                    .", bpo = '".       $data['bpo'] ."'"
                                    .", seo = '".       $data['seo'] ."'"
                                    .", consult = '".   $data['consult'] ."'"
                                    .", research = '".  $data['research'] ."'"
                                    .", advert = '".    $data['advert'] ."'"
                                    .", webmaint = '".  $data['webmaint'] ."'"
                                    .", network = '".   $data['network'] ."'"
                                    .", hardware = '".  $data['hardware'] ."'";
                  
                      if ( !$db->query($query) ) {
                            $messages->setErrorMessage("The Invoice Bifurcation were not Saved.");
                      }
                    
                    // Update the Order.
                   
                    if ( !Invoice::setOrderStatus($db, $or_id, Invoice::COMPLETED) ) {
                        $messages->setErrorMessage("The Order was not Marked as Completed.");
                    }
                    
                    $or_id = NULL;
                    
                    // Update the Client Services.
                    /*
                    if ( !Invoice::UpdateClientService($db, $data['client']['user_id'], $data['service_id']) ) {
                        $messages->setErrorMessage("The Clients Services were not updated.");
                    }
                    // Create the Invoice PDF, HTML in file.
                    $extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';
                    if ( !($attch = Invoice::createFile($data, 'HTML', $extra)) ) {
                        $messages->setErrorMessage("The Invoice file was not created.");
                    }
                    
                    // Send the notification mails to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_inv_add', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_inv_add_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );

                    // Organize the data to be sent in email.
                    $data['link']   = DIR_WS_MP .'/bill-invoice.php?perform=view&inv_id='. $variables['hid'];

                    // Read the Client Manager information.
                    include ( DIR_FS_INCLUDES .'/clients.inc.php');
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');

                    // Send Email to the Client.
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    
                    $data['link']   = DIR_WS_NC .'/bill-invoice.php?perform=view&inv_id='. $variables['hid'];
                    // Send mail to the Concerned Executives.
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_INV_NEW_MANAGERS', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                        }
                    }
                    
                    // Send Email to the Client Manager.
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT_MANAGER', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['manager']['f_name'] .' '. $data['manager']['l_name'], 'email' => $data['manager']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                    }
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }*/
            }
        }
        else {
      
           
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $inv_id;
            include ( DIR_FS_NC .'/bill-invoice-list.php');
        }
        else {
           
             // Read the Invoice which is to be Updated.
            $fields = TABLE_BILL_INV .'.*'
                        .','. TABLE_BILL_ORDERS .'.old_particulars'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.status AS c_status' ;
            
            $condition_query = " WHERE (". TABLE_BILL_INV .".id = '". $inv_id ."' "
                                ." OR ". TABLE_BILL_INV .".number = '". $inv_id ."')";

            $condition_query .= " AND ( ";
    
            // If my has created this Invoice.
            $condition_query .= " (". TABLE_BILL_INV .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_INV .".access_level < $access_level ) ";
            
            // If my is the Client Manager (Remove if Client manager cannot edit the Order)
            $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                                ." AND ". TABLE_BILL_INV .".access_level < $access_level ) ";
            
            // Check if the User has the Right to Edit Invoices created by other Users.
            if ( $perm->has('nc_bl_inv_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_bl_inv_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_BILL_INV. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_INV .".access_level < $access_level_o ) ";
            }
            $condition_query .= " )";
            
            if ( Invoice::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
               
                
                if ( $_ALL_POST['access_level'] < $access_level ) {
               
                    // Set up the Client Details field.
                    $_ALL_POST['client_details']= $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
                                                    .' ('. $_ALL_POST['c_number'] .')'
                                                    .' ('. $_ALL_POST['c_email'] .')';
                    
                    // Set up the dates.
                    $_ALL_POST['do_i']  = explode(' ', $_ALL_POST['do_i']);
                    $temp               = explode('-', $_ALL_POST['do_i'][0]);
                    $_ALL_POST['do_i']  = NULL;
                    $_ALL_POST['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    
                    $_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
                    $temp               = explode('-', $_ALL_POST['do_d'][0]);
                    $_ALL_POST['do_d']  = NULL;
                    $_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    
                    if($_ALL_POST['do_e'] !='0000-00-00 00:00:00'){
                        $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                        $temp               = explode('-', $_ALL_POST['do_e'][0]);
                        $_ALL_POST['do_e']  = NULL;
                        $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }else{
                        $_ALL_POST['do_e']  ='';
                    }
                    
                    if($_ALL_POST['do_fe'] !='0000-00-00 00:00:00'){
                        $_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
                        $temp               = explode('-', $_ALL_POST['do_fe'][0]);
                        $_ALL_POST['do_fe']  = NULL;
                        $_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }else{
                    
                        $_ALL_POST['do_fe']  ='';
                    }
                    // Set up the Services.
                    //$_ALL_POST['service_id'] = explode(',', $_ALL_POST['service_id']);                  
                    $condition_query = '';
                   
                    
                   
                    $or_no              = $_ALL_POST['or_no'];
                }
                else {
              
                    $messages->setErrorMessage("You do not have the Permission to edit the Invoice with the current Access Level.");
                }
            }
            else {
               
                $messages->setErrorMessage("The Invoice was not found or you do not have the Permission to access this Invoice.");
            }
        
        
        
        
        
            if ( !empty($or_no) ) {
         
                // Read the Order details.
                include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
                $order  = NULL;
                /*
                $fields = 'id,'. TABLE_BILL_ORDERS .'.number AS or_no,'. TABLE_BILL_ORDERS .'.access_level, '. TABLE_BILL_ORDERS .'.status, particulars, details';
                $fields .= ', client, user_id, '. TABLE_CLIENTS .'.number, f_name, l_name';
                */
                 
                $fields = TABLE_BILL_ORDERS.'.id,'.TABLE_BILL_ORDERS .'.do_o AS do_o,'.TABLE_BILL_ORDERS .'.number AS or_no,'. TABLE_BILL_ORDERS .'.access_level, '. TABLE_BILL_ORDERS .'.status, '.TABLE_BILL_ORDERS.'.details';
                $fields .= ', '.TABLE_BILL_ORDERS.'.order_title, '. TABLE_BILL_ORDERS .'.order_closed_by';
                $fields .= ', '.TABLE_BILL_ORDERS.'.is_renewable ';
                $fields .= ', '.TABLE_BILL_ORDERS.'.currency_id, '. TABLE_BILL_ORDERS .'.company_id  ,'.TABLE_BILL_ORDERS .'.amount';
                $fields .= ', '.TABLE_CLIENTS.'.user_id, '. TABLE_CLIENTS .'.number, '.TABLE_CLIENTS.'.f_name, '.TABLE_CLIENTS.'.l_name';
                $fields .= ', '.TABLE_BILL_ORDERS.'.client, '. TABLE_USER .'.number AS u_number, '.TABLE_USER.'.f_name AS u_f_name, '.TABLE_USER.'.l_name AS u_l_name ';
                
   
               
                if ( Order::getDetails($db, $order, $fields, "WHERE ". TABLE_BILL_ORDERS .".number = '". $or_no ."' ") > 0 ) {
              
                    $order = $order[0];
                  
                        if ( $access_level > $order['access_level'] ) {
                            /*$_ALL_POST['or_no']             = $order['or_no'];                            
                            $_ALL_POST['or_details']        = $order['details'];
                            $_ALL_POST['or_user_id']        = $order['user_id'];
                            $_ALL_POST['or_number']         = $order['number'];
                            $_ALL_POST['or_f_name']         = $order['f_name'];
                            $_ALL_POST['or_l_name']         = $order['l_name'];
                            $_ALL_POST['or_access_level']   = $order['access_level'];*/
                            
                             $_ALL_POST['or_no']             = $order['or_no'];
                           // $_ALL_POST['or_particulars']    = $order['particulars'];
                            $_ALL_POST['or_details']        = $order['details'];
                            $_ALL_POST['or_user_id']        = $order['user_id'];
                            $_ALL_POST['or_number']         = $order['number'];
                            $_ALL_POST['or_f_name']         = $order['f_name'];
                            $_ALL_POST['or_l_name']         = $order['l_name'];
                            $_ALL_POST['or_access_level']   = $order['access_level'];
                         
                            $_ALL_POST['do_o']              = $order['do_o'];
                            $_ALL_POST['u_f_name']          = $order['u_f_name'];
                            $_ALL_POST['u_l_name']          = $order['u_l_name'];
                            $_ALL_POST['u_number']          = $order['u_number'];
                            $_ALL_POST['order_title']       = $order['order_title'];
                            $_ALL_POST['order_closed_by']   = $order['order_closed_by'];
                            $_ALL_POST['is_renewable']   = $order['is_renewable'];
                            
                            if(!empty($order['order_closed_by'])){
                                
                                $condition = TABLE_USER." WHERE  user_id='".$order['order_closed_by']."'";
                                $clientfields = ' '.TABLE_USER.'.user_id, '. TABLE_USER .'.number, '.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
                                User::getList($db, $closed_by, $clientfields, $condition) ;
                                $_ALL_POST['order_closed_by_name']   = $closed_by['0'];
                            }
                                                    
                            
                            $_ALL_POST['amount']= $order['amount'];
                            $_ALL_POST['amount_inr']= $order['amount'] * $_ALL_POST['exchange_rate'];
                            $_ALL_POST['currency_id']= $order['currency_id'];
                            $_ALL_POST['company_id']= $order['company_id'];
                            
                       
                            
                           
                           /*get particulars details bof*/
                            $temp = NULL;
                            $temp_p = NULL;
                            $condition_query = "WHERE ord_no = '". $order['or_no'] ."'";
                            Order::getParticulars($db, $temp, '*', $condition_query);
                            if(!empty($temp)){
                                foreach ( $temp as $pKey => $parti ) {
                                  if($temp[$pKey]['s_type']=='1'){
                                        $temp[$pKey]['s_type']='Years';
                                   }elseif($temp[$pKey]['s_type']=='2'){
                                        $temp[$pKey]['s_type']='Quantity';
                                   }
                                    $temp_p[]=array(
                                                    'p_id'=>$temp[$pKey]['id'],
                                                    'particulars'=>$temp[$pKey]['particulars'],
                                                    'p_amount' =>$temp[$pKey]['p_amount'] ,                                   
                                                    's_id' =>$temp[$pKey]['s_id'] ,                                   
                                                    'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                                    'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                                    'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                                    'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                                    'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                                    'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                                    'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                                    'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                                    'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                                    'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                                    );
                                }
                            }
                            $_ALL_POST['particulars']=$temp_p ;
                           
                            $order = NULL;
                            // Read the Client Addresses.
                            include_once ( DIR_FS_CLASS .'/Region.class.php');
                            $region         = new Region();
                            $region->setAddressOf(TABLE_CLIENTS, $_ALL_POST['or_user_id']);
                            $_ALL_POST['address_list']  = $region->get();
                            
                           
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to edit invoice for this Order.");
                            $_ALL_POST = '';
                        }
                        
                 
                }
                else {
                    $messages->setErrorMessage("The Selected Order was not found.");
                }
            }

            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['or_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['or_access_level'], $al_list, $index);
                $_ALL_POST['or_access_level'] = $al_list[$index[0]]['title'];
            }
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'inv_id', 'value' => $inv_id);
            $hidden[] = array('name'=> 'or_no', 'value' => $or_no);
            $hidden[] = array('name'=> 'is_renewable', 'value' => $_ALL_POST['is_renewable']);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit_o');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
            
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            //$page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            //$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-edit-old.html');
        }
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>