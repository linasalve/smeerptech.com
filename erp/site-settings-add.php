<?php
  if ( $perm->has('nc_st_settings') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
	    $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST); 
                 
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
            );
			if ( SiteSettings::validateAdd($data, $extra) ) {
                   
                    $temp = "'".implode("','", $data['team'])."'";
                  
				    $_ALL_POST['team_members']  = '';
                    $_ALL_POST['team_details']  = array();
                    User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
                    $_ALL_POST['team'] = array();
					
					$data['transaction_task_associates']=$data['transaction_task_associates_name']='';
                    foreach ( $_ALL_POST['team_members'] as $key=>$members) {                  
                        $data['transaction_task_associates'] .= $members['user_id'].",";
                        $data['transaction_task_associates_name'] .= $members['f_name'] .' '. $members['l_name'].",";
                    }
					if(!empty($data['transaction_task_associates'])){
						$data['transaction_task_associates']=trim($data['transaction_task_associates'],",");
						$data['transaction_task_associates']=",".$data['transaction_task_associates'].",";
					}
					if(!empty($data['transaction_task_associates_name'])){
						$data['transaction_task_associates_name']=trim($data['transaction_task_associates_name'],",");
						$data['transaction_task_associates_name']=",".$data['transaction_task_associates_name'].",";
					}
					 
				    $query  = " UPDATE ".TABLE_SITE_SETTINGS
						." SET ".TABLE_SITE_SETTINGS .".transaction_task_associates = '". $data['transaction_task_associates'] ."'"                                     
						.",". TABLE_SITE_SETTINGS .".transaction_task_associates_name = '". $data['transaction_task_associates_name'] ."'"
						.",". TABLE_SITE_SETTINGS .".administrative_task_associates = '". $data['administrative_task_associates'] ."'"
						.",". TABLE_SITE_SETTINGS .".administrative_task_associates_name = '". $data['administrative_task_associates_name'] ."'"
						." WHERE id = '". $id ."'";
				if ( $db->query($query) && $db->affected_rows() > 0 ) {
					$messages->setOkMessage("Record has been added successfully.");
					            
				}
				//to flush the data.
				$_ALL_POST	= NULL;
				//$data		= NULL;
			}
        }
       
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/site-settings.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/site-settings.php");
        }
 
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/site-settings.php?added=1");   
        }
        else {
			//get details bof
			$fields = TABLE_SITE_SETTINGS .'.transaction_task_associates,'.TABLE_SITE_SETTINGS.".id"  ;            
            $condition_query = " LIMIT 0,1 ";            
            if ( SiteSettings::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];  
				if(!empty($_ALL_POST['transaction_task_associates'])){					
					$_ALL_POST['team'] = trim($_ALL_POST['transaction_task_associates'],",") ;
					$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
					$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
					$_ALL_POST['team_members']  = '';
					$_ALL_POST['team_details']  = array();
					User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
					$_ALL_POST['team']=$_ALL_POST['team_details'] = array();
					foreach ( $_ALL_POST['team_members'] as $key=>$members) {
				  
						$_ALL_POST['team'][] = $members['user_id'];
						$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
					}
				}
            }else{ 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
			 
			//get details eof
			
			$hidden[] = array('name'=> 'id', 'value' => $_ALL_POST['id']);
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'site-settings-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
