<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    include_once ( DIR_FS_INCLUDES .'/newsletters.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');    
    $added_all 	= isset($_GET["added_all"]) ? $_GET["added_all"]: ( isset($_POST["added_all"])  ? $_POST["added_all"]:'0');    
    $i 	        = isset($_GET["i"])         ? $_GET["i"]        : ( isset($_POST["i"])          ? $_POST["i"]   :     '0');    
    $j 	        = isset($_GET["j"])         ? $_GET["j"]        : ( isset($_POST["j"])          ? $_POST["j"]   :     '0');    
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $condition_query = '';
    $condition_url = '';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    //echo "added=>".$added;
    
    if($added){
		$message='';
		if($added==2){
			$message=' and added in a queue.';
		}elseif($added==3){
			$message=' and Save as a Draft <br/> as No members Found in NDB';
		}elseif($added==4){
			$message=' and Save as a Draft.';
		}
        $messages->setOkMessage("Newsletter has been created.".$message);
    }
    
    if($added_all){
        $messages->setOkMessage("Record has been created successfully for ".$i ."/". $j .".");
    }
    
    if ( $perm->has('nc_nwl') ) {
    
        
		$sTypeArray     = array('Any'        =>  array(  'Any of following' => '-1'),
								TABLE_NEWSLETTERS   =>  array(   
															'Subject'       => 'subject', 
															'Text'       => 'text', 
															'Creator' => 'created_by_name'
														)
							);
		
		$sOrderByArray  = array(
								TABLE_NEWSLETTERS   => array( 
													'Subject'               => 'subject',
													'Create Date'    => 'do_e'                                                  
													)
							);
	
		// Set the sorting order of the user list.
		if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
			$_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
			$_SEARCH['sOrder']  = $sOrder   = 'DESC';
			$order_by_table     = TABLE_NEWSLETTERS;
		}

		$variables['completed']  = Newsletters::COMPLETED;
		$variables['pending']  = Newsletters::PENDING;
		$variables['draft']  = Newsletters::DRAFT;
		$variables['status']= Newsletters::getStatus();
         
        //use switch case here to perform action. 
        switch ($perform) {
        
            case ('add'): {
            
                include (DIR_FS_NC.'/newsletters-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletters.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('copy'): {
            
                include (DIR_FS_NC.'/newsletters-copy.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletters.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('edit'): {
            
                include (DIR_FS_NC.'/newsletters-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletters.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('download_file'): {
				include (DIR_FS_NC .'/newsletters-download.php');
				break;
			}
            case ('sent_mails'): {
                include (DIR_FS_NC .'/newsletters-sent-emails.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
           
            case ('search'): {
                include(DIR_FS_NC."/newsletters-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletters.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
                                   
            case ('list'):
            default: {
                include (DIR_FS_NC .'/newsletters-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletters.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'newsletters.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
           $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>