<?php

    if ( $perm->has('nc_wk_tl_status') ) {
        $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
		$or_id	= isset($_GET["or_id"]) ? $_GET["or_id"] 	: ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
    
        $_ALL_POST      = NULL;
        $data           = NULL;
		$work_details	= NULL;
        $access_level   = $my['access_level'];
        
		$extra = array( 'db'        => &$db,
						'messages'  => $messages
					);
					
		// Check if the Timeline is present or not.	
		/*if ( WorkTimeline::getWorkDetails($id, $work_details, $extra) > 0 ) {*/
		if ( WorkTimeline::getOrderDetails($or_id, $work_details, $extra) > 0 ) {
			
            
			// Include the Order Status class.
			include_once (DIR_FS_INCLUDES .'/work-stage.inc.php');
			$lst_work_stage = NULL;
			WorkStage::getWorkStages($db, $lst_work_stage);
            
			if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
				$_ALL_POST  = $_POST;
				$data       = processUserData($_POST);
				$files       = processUserData($_FILES);
               
                $data['files'] = $files;
                $data['allowed_file_types'] = $allowed_file_types;
                $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ;
              
				$extra = array( 'db'        	=> &$db,
								'work_details'  => $work_details,
								'lst_work_stage'=> $lst_work_stage,
								'messages'  	=> $messages
							  );
				
				if ( WorkTimeline::validateForwardWork($data, $extra) ) {
                    //Code for upload files BOF
                    $data['files_list'] = '';
                    $i=1;
                    foreach($data['files'] as $key => $val){
                        
                        //$val["name"] = str_replace(' ','-',$val["name"]);
                        $filedata["extension"]='';
                        $filedata = pathinfo($val["name"]);
                       
                        $ext = $filedata["extension"] ; 
                        //$attachfilename = $username."-".mktime().".".$ext ;
                       
                        $attachfilename = mktime()."-".$i.".".$ext ;
                      
                       if(!empty($val["name"])){
                     
                            if (copy($val['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename)){
                                $data['files_list'] .= $attachfilename."," ;
                            }
                        }
                        $i++;
                    }
                    
                    //Code for upload files EOF
                     $query  = " INSERT INTO ". TABLE_WORK_TL
								." SET ". TABLE_WORK_TL .".order_id		= '". $work_details['order_id'] ."'"
									.",". TABLE_WORK_TL .".order_no		= '". $work_details['order_no'] ."'"
									.",". TABLE_WORK_TL .".to_id		= '". $data['to_id'] ."'"
									.",". TABLE_WORK_TL .".by_id		= '". $my['user_id'] ."'"
									.",". TABLE_WORK_TL .".attach_files		= '". $data['files_list'] ."'"
									.",". TABLE_WORK_TL .".comments		= '". $data['comments'] ."'"
									.",". TABLE_WORK_TL .".do_assign	= '". date('Y-m-d H:i:s', time()) ."'"
									.",". TABLE_WORK_TL .".status   	= '". $data['status'] ."'"
								;
                  
					if ( $db->query($query) ) {
						$id = $db->last_inserted_id();
						$messages->setOkMessage("The Comment has been added.");
						
						// Check if the stage is the last stage (with order = 999) or not?
						$temp = NULL;
						if ( WorkStage::getList($db, $temp, 'id', " WHERE id = '". $data['status'] ."' AND status_order = '999'" ) > 0 ) {
							$temp = NULL;
							// The Stage is the last stage, update the status of the Order.
							include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');

							$extra = array( 'db'           => &$db,
											'access_level' => '16777215',
											'messages'     => &$messages
										);
							Order::updateStatus($work_details['order_id'], Order::PROCESSED, $extra);
							
						}
						
                        if(!empty($data['id'])){
                            $query  = " UPDATE ". TABLE_WORK_TL
                                            ." SET forward_to = '". $id ."'"
                                        ." WHERE id = '". $data['id'] ."'";
                            if ( !$db->query($query) || $db->affected_rows() == 0 ) {
                                $messages->setErrorMessage("The previous Work was not closed.");
                            }
                        }
						
						// Send the Mail to the concerned persons.
						include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
						
						// Prepare the Mail data.
						$data['order_id'] 	= $work_details['order_id'];
						$data['order_no'] 	= $work_details['order_no'];
						$data['order_title']= $work_details['order_title'];
						$data['stage'] 		= WorkStage::getWorkStageTitle($db,$data['status']);
						$data['do_assign'] 	= time();
						$data['comments'] 	= $data['comments'];
						$data['by'] 		= $my;
						$data['assign_to'] 	= $my;
						$data['link']   	= DIR_WS_NC .'/work-timeline.php?perform=details&or_id='. $work_details['order_id'];
						
						// Send mail to the Team Members.
						foreach ( $work_details['team'] as $member ) {
							$data['member']	= $member;
							$email 			= NULL;
							if ( getParsedEmail($db, $s, 'WORK_COMMENT_TEAM', $data, $email) ) {
								$to     = '';
								$to[]   = array('name' => $member['f_name'] .' '. $member['l_name'], 'email' => $member['email']);
								$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
							}
						}
						
					}
					else {
						$messages->setErrorMessage('The Comment was not added.');
					}
				}
			}
            $or_id = $work_details['order_id']; 
			// Check if the Form to add is to be displayed or the control is to be sent to the List page.
			if ( isset($_POST['btnCancel'])
					|| (isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 )) {
				
				$variables['hid'] = $id;
				include_once ( DIR_FS_NC .'/work-timeline-details.php');
			}
			else {
				
				// These parameters will be used when returning the control back to the List page.
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}
				$hidden[] = array('name'=> 'perform','value' => 'change_stage');
				$hidden[] = array('name'=> 'act', 'value' => 'save');
				$hidden[] = array('name'=> 'id', 'value' => $id);
				$hidden[] = array('name'=> 'or_id', 'value' => $or_id);
				
				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
				$page["var"][] = array('variable' => 'lst_work_stage', 'value' => 'lst_work_stage');
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["var"][] = array('variable' => 'work_details', 'value' => 'work_details');
	
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'work-timeline-stage.html');
			}
			
			
		}
		else {
			$messages->setErrorMessage("The Selected Order Status was not found.");
		}
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>