<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
  
	   

    $lead_details = isset($_GET['lead_details']) ? $_GET['lead_details'] : '';
     
    $optionLink =$stringOpt= '' ;
    if(!empty($lead_details)){
        $sString = $lead_details;
    }
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if (isset($sString) && !empty($sString)){
        header("Content-Type: application/json");      
	    $query =" SELECT DISTINCT(".TABLE_SALE_LEADS.".lead_id),".TABLE_SALE_LEADS.".f_name,".TABLE_SALE_LEADS.".l_name,".TABLE_SALE_LEADS.".company_name FROM ".TABLE_SALE_LEADS." WHERE ( ".TABLE_SALE_LEADS.".f_name LIKE '%".$sString."%' OR ".TABLE_SALE_LEADS.".l_name LIKE '%".$sString."%' OR ".TABLE_SALE_LEADS.".company_name LIKE '%".$sString."%' ) AND  ".TABLE_SALE_LEADS.".status='1'";
        $db->query($query);
        echo "{\"results\": [";
        if ( $db->nf()>0 ) {
          
            $arr = array();
            while ($db->next_record()){                
                $arr[] = "{\"id\": \"".$db->f('lead_id')."\", \"value\": \"".processSQLData($db->f('f_name'))." ".processSQLData($db->f('l_name'))." (".processSQLData($db->f('company_name')).")".""."\", \"info\": \"\"}";
            }
            echo implode(", ", $arr);
           
        }
         echo "]}";	   
	}

include_once( DIR_FS_NC ."/flush.php");

?>
