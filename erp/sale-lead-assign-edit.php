<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_sl_ld_asig_edit') ) {
        $lead_id = isset($_GET["lead_id"]) ? $_GET["lead_id"] : ( isset($_POST["lead_id"]) ? $_POST["lead_id"] : '' );
    
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
    
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new Region();
        $phone          = new Phone(TABLE_LEADS);
        //$reminder       = new UserReminder(TABLE_LEADS);
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_sl_ld_asig_edit_al') ) {
            $access_level += 1;
        }
        $al_list    = getAccessLevel($db, $access_level);
        //$role_list  = Lead::getRoles($db, $access_level);


        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
			//As invoice-number formate changed on the basis of ( fanancial yr + sr no ) 
            //to generate 
  	        $_ALL_POST['number'] = getCounterNumber($db,'FLW'); 
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            //'reminder'  => $reminder
                        );
            
            if ( Leads::validateUpdate($data, $extra) ) {
			
				$query  = " UPDATE ". TABLE_SALE_LEADS
                        ." SET ". TABLE_SALE_LEADS .".access_level			= '". $my['access_level'] ."'"
                            .",". TABLE_SALE_LEADS .".created_by			= '". $my['uid'] ."'"                           
							.",". TABLE_SALE_LEADS .".lead_owner   			= '". $my['f_name'] ."'"
							.",". TABLE_SALE_LEADS .".company_name   		= '". $data['company_name'] ."'"
							.",". TABLE_SALE_LEADS .".billing_name   		= '". $data['company_name'] ."'"
							.",". TABLE_SALE_LEADS .".lead_assign_to   		= '". $data['lead_assign_to'] ."'"
							.",". TABLE_SALE_LEADS .".lead_source_from		= '". $data['lead_source_from'] ."'"
							.",". TABLE_SALE_LEADS .".lead_source_to		= '". $data['lead_source_to'] ."'"
							.",". TABLE_SALE_LEADS .".lead_campaign_id   	= '". $data['lead_campaign_id'] ."'"
							.",". TABLE_SALE_LEADS .".lead_industry_id   	= '". $data['lead_industry_id'] ."'"
							.",". TABLE_SALE_LEADS .".no_emp			   	= '". $data['no_emp'] ."'"
							.",". TABLE_SALE_LEADS .".rating			   	= '". $data['rating'] ."'"
							.",". TABLE_SALE_LEADS .".annual_revenue   		= '". $data['annual_revenue'] ."'"
							.",". TABLE_SALE_LEADS .".rivals_id		   		= '". $data['rivals_id'] ."'"
                            .",". TABLE_SALE_LEADS .".email       			= '". $data['email'] ."'"
                            .",". TABLE_SALE_LEADS .".email_1     			= '". $data['email_1'] ."'"
                            .",". TABLE_SALE_LEADS .".email_2     			= '". $data['email_2'] ."'"
                            .",". TABLE_SALE_LEADS .".title       			= '". $data['title'] ."'"
                            .",". TABLE_SALE_LEADS .".f_name      			= '". $data['f_name'] ."'"
                            .",". TABLE_SALE_LEADS .".m_name      			= '". $data['m_name'] ."'"
                            .",". TABLE_SALE_LEADS .".l_name      			= '". $data['l_name'] ."'"
                            .",". TABLE_SALE_LEADS .".p_name      			= '". $data['p_name'] ."'"
                            .",". TABLE_SALE_LEADS .".desig       			= '". $data['desig'] ."'"
                            .",". TABLE_SALE_LEADS .".org         			= '". $data['org'] ."'"
                            .",". TABLE_SALE_LEADS .".domain      			= '". $data['domain'] ."'"
                            .",". TABLE_SALE_LEADS .".gender      			= '". $data['gender'] ."'"
                            .",". TABLE_SALE_LEADS .".do_birth    			= '". $data['do_birth'] ."'"
                            .",". TABLE_SALE_LEADS .".do_aniv     			= '". $data['do_aniv'] ."'"
                            .",". TABLE_SALE_LEADS .".remarks     			= '". $data['remarks'] ."'" 
							.",". TABLE_SALE_LEADS .".followup_no     		= '". $data['number'] ."'"
							.", ". TABLE_SALE_LEADS .".status      			= '". $data['status'] ."'"                            .		                            " WHERE ". TABLE_SALE_LEADS .".lead_id   		= '". $data['lead_id'] ."'";
                
				if ( $db->query($query) ) {
				
                    $variables['hid'] = $data['lead_id'];
					
					//assign history
					$assign_query	= " INSERT INTO ".TABLE_SALE_ASSIGN_HISTORY
                            			." SET ".TABLE_SALE_ASSIGN_HISTORY .".lead_id = '". $variables['hid'] ."'"  
                                		.",". TABLE_SALE_ASSIGN_HISTORY .".assign_to  = '".	$data['lead_assign_to'] ."'"
										.",". TABLE_SALE_ASSIGN_HISTORY .".created_by = '".	$my['uid'] ."'"
										.",". TABLE_SALE_ASSIGN_HISTORY .".do_assign  = '".	date('Y-m-d')."'";
                	$db_assign=new db_local();
					$db_assign->query($assign_query);
					
					//insert folloup
					$followup_query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                            			." SET ".TABLE_SALE_FOLLOWUP .".followup_no 	= '". $data['number'] ."'"  
                                		//.",". TABLE_SALE_FOLLOWUP .".title_id  			= '". $data['lead_assign_to'] ."'"
										.",". TABLE_SALE_FOLLOWUP .".remarks 			= 'lead alloted'"
										.",". TABLE_SALE_FOLLOWUP .".assign_to 			= '". $data['lead_assign_to'] ."'"
										.",". TABLE_SALE_FOLLOWUP .".created_by 		= '". $my['uid'] ."'"
										.",". TABLE_SALE_FOLLOWUP .".access_level 		= '". $my['access_level'] ."'"
										.",". TABLE_SALE_FOLLOWUP .".created_by_dept 	= '". $my['department'] ."'"
										.",". TABLE_SALE_FOLLOWUP .".do_e  				= '". date('Y-m-d')."'";
                	$db_followup=new db_local();
					$db_followup->query($followup_query);
					
					///////update counter 
			        //After insert, update followup counter in 
                    updateCounterOf($db,'FLW');
				
                    // Update the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        if ( isset($data['a_remove'][$i]) && $data['a_remove'][$i] == '1') {
                            if ( ! $region->delete($db, $data['address_id'][$i], $variables['hid'], TABLE_SALE_LEADS) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setOkMessage($errors['description']);
                                }
                            }
                        }
                        else {
                            $address_arr = array(   'id'            => isset($data['address_id'][$i])? $data['address_id'][$i] : '0',
                                                    'address_type'  => $data['address_type'][$i],
                                                    'company_name'  => $data['company_name'][$i],
                                                    'address'       => $data['address'][$i],
                                                    'city'          => $data['city'][$i],
                                                    'state'         => $data['state'][$i],
                                                    'country'       => $data['country'][$i],
                                                    'zipcode'       => $data['zipcode'][$i],
                                                    'is_preferred'  => isset($data['is_preferred'][$i])? $data['is_preferred'][$i] : '0',
                                                    'is_verified'   => isset($data['is_verified'][$i])? $data['is_verified'][$i] : '0'
                                                );
                            
                            if ( !$region->update($variables['hid'], TABLE_SALE_LEADS, $address_arr) ) {
                                foreach ( $region->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                }
                            }
                        }
                    }
                    
                    
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_SALE_LEADS) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $phone_arr = array( 'id'            => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
                                                    'p_type'        => $data['p_type'][$i],
                                                    'cc'            => $data['cc'][$i],
                                                    'ac'            => $data['ac'][$i],
                                                    'p_number'      => $data['p_number'][$i],
                                                    'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
                                                    'p_is_verified' => isset($data['p_is_verified'][$i]) ? $data['p_is_verified'][$i] : '0'
                                                );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_SALE_LEADS, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Update the Reminders.
                    /*for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['remind_date_'.$i]) ) {
                            if ( isset($data['r_remove_'.$i]) && $data['r_remove_'.$i] == '1') {
                                if ( ! $reminder->delete($db, $data['remind_id_'.$i], $variables['hid'], TABLE_SALE_LEADS) ) {
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            }
                            else {
                                $reminder_arr = array(  'id'            => (isset($data['remind_id_'.$i])? $data['remind_id_'.$i] : ''),
                                                        'do_reminder'   => $data['remind_date_'.$i],
                                                        'description'   => $data['remind_text_'.$i]
                                                    );
                                if ( ! $reminder->update($db, $variables['hid'], TABLE_LEADS, $reminder_arr) ) {
                                    $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                    foreach ( $reminder->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }*/
                    
                    // Send The Email to the concerned persons.
                    /*include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_sl_ld_add_notif', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_sl_ld_add_notif_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    //$manager = Lead::getManager($db, '', $data['manager'], 'f_name,l_name,email');
                    //$data['manager_name']   = $manager['f_name'] .' '. $manager['l_name'];
                    //$data['manager_email']  = $manager['email'];
                    //$data['manager_phone']  = '';
                    
                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $data['link']       = DIR_WS_NC .'/sale-lead.php?perform=view_details&lead_id='. $data['lead_id'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'CLIENT_INFO_UPDATE_MANAGERS', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        
                    }
                    
                    // Send Email to the Client whose infromation was updated.
                    $data['link']       = DIR_WS_NC;
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'CLIENT_INFO_UPDATE', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['f_name'] .' '. $data['l_name'], 'email' => $data['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }*/
                    $messages->setOkMessage("Lead Details has been updated.");
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Lead was not updated.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $lead_id;
            include ( DIR_FS_NC .'/sale-lead-assign-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'            => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
                
                // Preserve the Addresses.
                $count = count($_ALL_POST['city']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['address_list'][] = array(   'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'is_verified'   => isset($_ALL_POST['is_verified'][$i])? $_ALL_POST['is_verified'][$i]:'',
                                                                'is_preferred'  => isset($_ALL_POST['is_preferred'][$i])? $_ALL_POST['is_preferred'][$i]:'',
                                                                'address_type'  => isset($_ALL_POST['address_type'][$i])? $_ALL_POST['address_type'][$i]:'',
                                                                'address'       => isset($_ALL_POST['address'][$i])? $_ALL_POST['address'][$i]:'',
                                                                'city'          => isset($_ALL_POST['city'][$i])? $_ALL_POST['city'][$i]:'',
                                                                'state'         => isset($_ALL_POST['state'][$i])? $_ALL_POST['state'][$i]:'',
                                                                'zipcode'       => isset($_ALL_POST['zipcode'][$i])? $_ALL_POST['zipcode'][$i]:'',
                                                                'country'       => isset($_ALL_POST['country'][$i])? $_ALL_POST['country'][$i]:'',
                                                     );
//                    }
                }

                
                // Preserve the Reminders.
                //$count = $_ALL_POST['reminder_count'];
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['remind_date_'.$i) && !empty($_ALL_POST['remind_date_'.$i) ) {
                        $_ALL_POST['reminder_list'][] = array(  'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
                                                                'do_reminder'   => isset($_ALL_POST['remind_date_'.$i])? $_ALL_POST['remind_date_'.$i]:'',
                                                                'description'   => isset($_ALL_POST['remind_text_'.$i])? $_ALL_POST['remind_text_'.$i]:'',
                                                     );
//                    }
                }
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE lead_id = '". $lead_id ."' ";
                $_ALL_POST      = NULL;
                if ( Leads::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                  
                    //$_ALL_POST['manager'] 	= Lead::getManager($db, '', $_ALL_POST['manager']);
					//$_ALL_POST['roles'] 	= explode(',', $_ALL_POST['roles']);
					
					// BO: Read the Team Members Information.
					/*if ( !empty($_ALL_POST['team']) && !is_array($_ALL_POST['team']) ) {
						//include ( DIR_FS_INCLUDES .'/user.inc.php');
						
						$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
						$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
						$_ALL_POST['team_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
						$_ALL_POST['team'] = array();
						foreach ( $_ALL_POST['team_members'] as $key=>$members) {
						//for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
							$_ALL_POST['team'][] = $members['user_id'];
							$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
						}
					}*/
					// EO: Read the Team Members Information.
					
                    // Check the Access Level.
                    if ( $my['access_level'] < $access_level ) {
                        // Read the Contact Numbers.
                        $phone->setPhoneOf(TABLE_SALE_LEADS, $lead_id);
                        $_ALL_POST['phone'] = $phone->get($db);
                        
                        // Read the Addresses.
                        $region->setAddressOf(TABLE_SALE_LEADS, $lead_id);
                        $_ALL_POST['address_list'] = $region->get();
                        
                        // Read the Reminders.
                        //$reminder->setReminderOf(TABLE_LEADS, $user_id);
                        //$_ALL_POST['reminder_list'] = $reminder->get($db);
                    }
                   
                }
            }

            if ( !empty($_ALL_POST['lead_id']) ) {

                // Check the Access Level.
                if ( $my['access_level'] < $access_level ) {

                    $phone_types    = $phone->getTypeList();
                    $address_types  = $region->getTypeList();
                    $lst_country    = $region->getCountryList();
                    
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'lead_id', 'value' => $lead_id);
                    //$hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    $page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
                    $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
        
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-assign-edit.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Quick Entry with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected Quick Entry was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>