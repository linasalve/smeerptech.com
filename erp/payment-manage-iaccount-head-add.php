<?php
   if ( $perm->has('nc_p_iah_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
      	
        
		// Include the account head class.
		include_once (DIR_FS_INCLUDES .'/payment-iaccount-head.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( PaymentIAccountHead::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_PAYMENT_IACCOUNT_HEAD
                            ." SET ". TABLE_PAYMENT_IACCOUNT_HEAD .".account_head_name = '". $data['account_head_name'] ."'"                                                              
                                //.",". TABLE_PAYMENT_IACCOUNT_HEAD .".transaction_type = '". 	$data['transaction_type'] ."'"     
                                .",". TABLE_PAYMENT_IACCOUNT_HEAD .".account_head_code = '".$data['account_head_code'] ."'"    
                                .",". TABLE_PAYMENT_IACCOUNT_HEAD .".status = '". 			$data['status'] ."'"                                                           
                                .",". TABLE_PAYMENT_IACCOUNT_HEAD .".date = '". 				date('Y-m-d')."'";
               
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New account head entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
       // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/payment-manage-iaccount-head.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-manage-iaccount-head.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/payment-manage-iaccount-head.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-manage-iaccount-head-add.html');
        }
    }     
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
