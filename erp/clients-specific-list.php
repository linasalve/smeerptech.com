<?php

    if ( $perm->has('nc_uc_list') ) {
        
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        $div    = isset($_GET["div"])   ? $_GET["div"]  : ( isset($_POST["div"])    ? $_POST["div"] : 'add_clients_su_info' );
		
		
		$access_level   = $my['access_level'];
        
        if ( !isset($condition_query) || $condition_query == '' ) {
           // $condition_query = ' WHERE (';
		   
        }else {
        	$condition_query .= " AND ";        
		}
		$condition_query .= "  parent_id = ''";
		 
		$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
		$list	= 	NULL;
        if($searchStr==1){  
        // To count total records.
			
			$total	=	Clients::getList( $db, $list, '', $condition_query);
		
			$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
			$extra_url  = '';
			if ( isset($condition_url) && !empty($condition_url) ) {
				$extra_url  = $condition_url;
			}
			$extra_url  .= "&x=$x&rpp=$rpp";
			$extra_url  = '&start=url'. $extra_url .'&end=url';
		
			$list	= NULL;
			Clients::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
        }
        /*$phone  = new Phone(TABLE_CLIENTS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_CLIENTS, $list[$i]['user_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}*/
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_view_sub_user'] = false;

        if ( $perm->has('nc_uc_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_uc_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_uc_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_uc_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_uc_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_uc_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_uc_su_list') ) {
            $variables['can_view_sub_user'] = true;
        }
		
        $hidden[] = array('name'=> 'div' , 'value' => $div);      
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
		$page["var"][] = array('variable' => 'div', 'value' => 'div');
        // PAGE = CONTENT_MAIN
       // $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>