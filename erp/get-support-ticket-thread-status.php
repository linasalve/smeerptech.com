<?php
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    //error_reporting(E_ALL);
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   )); 
    include_once ( DIR_FS_NC ."/header.php"); 
    include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	
	
    if ( $perm->has('nc_st_update_status') ){ //
	 
		$ticket_id =isset($_GET["ticket_id"])? $_GET["ticket_id"] : (isset($_POST["ticket_id"])?$_POST["ticket_id"] : '' );
		$ticket_status = isset($_GET["status"]) ? $_GET["status"] : (isset($_POST["status"]) ? $_POST["status"] : '' );
		$message='';
        
        if( $ticket_status=='1'){
			$sql= " UPDATE ".TABLE_ST_TICKETS." SET ".TABLE_ST_TICKETS.".status='".$ticket_status."' WHERE 
			".TABLE_ST_TICKETS.".ticket_id ='".$ticket_id."'";
			$execute= $db->query($sql);
			if($execute){
				$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
						<tr>
							<td class=\"message_header_ok\">
								<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
								Success
							</td>
						</tr>
						<tr>
							<td class=\"message_ok\" align=\"left\" valign=\"top\">
								<ul class=\"message_ok_2\">
									<li class=\"message_ok\"> Ticket status is updated Successfully </li>
								</ul>
							</td>
						</tr>
					</table>"; 
					
					$message.= "| <input type=\"button\" name=\"btnDeActive\" 
					onClick=\"updateThreadStatus(".$ticket_id.",0)\"
					class=\"deactive-buttton\"/> ";
			}
        }elseif($ticket_status=='0'){
		
			$sql = " UPDATE ".TABLE_ST_TICKETS." SET ".TABLE_ST_TICKETS.".status='".$ticket_status."' WHERE 
			".TABLE_ST_TICKETS.".ticket_id ='".$ticket_id."'";
			$execute= $db->query($sql);
			if($execute){
				$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				<tr>
					<td class=\"message_header_ok\">
						<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
						Success
					</td>
				</tr>
				<tr>
					<td class=\"message_ok\" align=\"left\" valign=\"top\">
						<ul class=\"message_ok_2\">
							<li class=\"message_ok\">Ticket status is updated Successfully </li>
						</ul>
					</td>
				</tr>
				</table>";
				$message.= "| <input type=\"button\" name=\"btnActive\" 																onClick=\"updateThreadStatus(".$ticket_id.",1)\" class=\"active-buttton\"/> "; 
			}
		 
		}  
		echo $message ;
		
	}else{
	
		   $message = "<table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				<tr>
					<td class=\"message_error_header\">
						<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\" border=\"0\" />&nbsp;
						Success
					</td>
				</tr>
				<tr>
					<td class=\"message_error\" align=\"left\" valign=\"top\">
						<ul class=\"message_error\">
							<li class=\"message_error\">You do not have the permission to access this module. </li>
						</ul>
					</td>
				</tr>
				</table>";
		echo $message .= "| ";
	
	}
	//include_once( DIR_FS_NC ."/flush.php");
	 
?>
