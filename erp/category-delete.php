<?php

	$cat_id	= isset($_GET["cat_id"]) ? $_GET["cat_id"] : ( isset($_POST["cat_id"]) ? $_POST["cat_id"] : '' );
	
	$extra = array( 'db' 		=> &$db,
					'messages'  => &$messages
				);
   	Categories::delete($cat_id, $extra);
	
	// Display the list.
    include ( DIR_FS_NC .'/category-list.php');
?>
