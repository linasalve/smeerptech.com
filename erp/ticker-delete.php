<?php
	if ( $perm->has('nc_site_tkr_delete') ) {
		$id	= isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'messages' 		=> $messages
					);
		Ticker::delete($id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/ticker-list.php');
	}
	else {
		$messages->setErrorMessage("You do not have the Right to Access this module.");
	}
?>