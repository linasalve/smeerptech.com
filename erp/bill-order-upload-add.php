<?php
    if ( $perm->has('nc_bl_or_up_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        				
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( BillOrderUpload::validateAdd($data, $extra) ) { 
                $data['upload_path'] = addslashes($data['upload_path']);
                $query	= " INSERT INTO ".TABLE_BILL_ORD_UPLOAD
                            ." SET ". TABLE_BILL_ORD_UPLOAD .".order_id = '".                 $data['order_id'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".upload_path = '".         $data['upload_path'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".details = '".         $data['details'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".type = '".             	$data['type'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".status        = '". 		BillOrderUpload::PENDING ."'" 
                                .",". TABLE_BILL_ORD_UPLOAD .".access_level = '".         $my['access_level'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".created_by = '".           $my['user_id'] ."'"
                                .",". TABLE_BILL_ORD_UPLOAD .".ip            = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                                .",". TABLE_BILL_ORD_UPLOAD .".do_e          = '". 		date('Y-m-d H:i:s')."'";
                              
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Path upload entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
         // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/bill-order-upload.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/bill-order-upload.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/bill-order-upload.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-upload-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
