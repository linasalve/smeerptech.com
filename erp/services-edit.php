<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_sr_edit') ) {
        $ss_id = isset($_GET["ss_id"]) ? $_GET["ss_id"] : ( isset($_POST["ss_id"]) ? $_POST["ss_id"] : '' );
    
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
/*        if ( $perm->has('nc_uc_edit_al') ) {
            $access_level += 1;
        }/
        $al_list    = getAccessLevel($db, $access_level);
        $role_list  = Clients::getRoles($db, $access_level);
        */

        $services = NULL;
		Services::getParent($db,$services);
        
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);
        
        $divisions = NULL;
        $required_fields ='*';
        $condition_query = "WHERE status='".ServiceDivision::ACTIVE."'";
		ServiceDivision::getList($db,$divisions,$required_fields,$condition_query);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);
			$files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
             
            $extra = array( 'db'        => &$db,
                            'messages'  => $messages
                        );
            
            $tax_arr = split('#',$data['tax1_name']);    
            $data['tax1_name'] = $tax_arr['0'];
            $data['tax1_id'] = $tax_arr['1'];
            
            if ( Services::validateSubUpdate($data, $extra) ) {
                $sql_file1=$sql_file2=$sql_file3='';
				if(!empty($data['file_1'])){
					if(!empty($data['old_file_1'])){
						//@unlink(DIR_FS_SVC_FILES."/".$data['old_file_1']);
					}
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_1']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-1".".".$ext ;
					$data['old_file_1']=$data['file_1'] = $attachfilename;						
					if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_SVC_FILES."/".$attachfilename)){					    @chmod(DIR_FS_SVC_FILES."/".$attachfilename,0777);
					  $sql_file1 = ",". TABLE_SETTINGS_SERVICES .".file_1 = '". $data['file_1'] ."'" ;
					}
				}else{
					if(isset($data['delete_file_1']) && $data['delete_file_1']==1){
						$data['file_1']='';
						//@unlink(DIR_FS_SVC_FILES."/".$data['old_file_1']);
						$sql_file1 = ",". TABLE_SETTINGS_SERVICES .".file_1 = '".$data['file_1'] ."'" ;
					}
				}
				if(!empty($data['file_2'])){
					if(!empty($data['old_file_2'])){
						//@unlink(DIR_FS_SVC_FILES."/".$data['old_file_2']);
					}						
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_2']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-2".".".$ext ;
					$data['old_file_2']=$data['file_2'] = $attachfilename;                    
					if (move_uploaded_file ($files['file_2']['tmp_name'], DIR_FS_SVC_FILES."/".$attachfilename)){
					   @chmod(DIR_FS_SVC_FILES."/".$attachfilename,0777);
					   $sql_file2 = ",". TABLE_SETTINGS_SERVICES .".file_2 = '".    $data['file_2'] ."'" ;
					}
				}else{
					if(isset($data['delete_file_2']) && $data['delete_file_2']==1){
						$data['file_2']='';
						//@unlink(DIR_FS_SVC_FILES."/".$data['old_file_2']);
						$sql_file2 = ",". TABLE_SETTINGS_SERVICES .".file_2 = '". $data['file_2'] ."'" ;
					}
				}
				if(!empty($data['file_3'])){
					if(!empty($data['old_file_3'])){
						//@unlink(DIR_FS_SVC_FILES."/".$data['old_file_3']);
					}						
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_3']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-3".".".$ext ;
					$data['old_file_3']=$data['file_3'] = $attachfilename;                    
					if (move_uploaded_file ($files['file_3']['tmp_name'], DIR_FS_SVC_FILES."/".$attachfilename)){
					   @chmod(DIR_FS_SVC_FILES."/".$attachfilename,0777);
					   $sql_file3 = ",". TABLE_SETTINGS_SERVICES .".file_3 = '". $data['file_3'] ."'" ;
					}
				}else{
					if(isset($data['delete_file_3']) && $data['delete_file_3']==1){
						$data['file_3']='';
						//@unlink(DIR_FS_SVC_FILES."/".$data['old_file_3']);
						$sql_file3 = ",". TABLE_SETTINGS_SERVICES .".file_3 = '". $data['file_3'] ."'" ;
					}
				}
                $query  = " UPDATE ". TABLE_SETTINGS_SERVICES
                        ." SET ". TABLE_SETTINGS_SERVICES .".ss_title     	  = '". $data['ss_title'] ."'"
							.",". TABLE_SETTINGS_SERVICES .".ss_particulars   = '". $data['ss_particulars'] ."'"
							.",". TABLE_SETTINGS_SERVICES .".subject = '".    $data['subject'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".details = '".    $data['details'] ."'"
                            //.",". TABLE_SETTINGS_SERVICES .".ss_order_tpl       = '". $data['ss_order_tpl'] ."'"
                            .$sql_file1
							.$sql_file2
							.$sql_file3
							.",". TABLE_SETTINGS_SERVICES .".ss_division        = '". $data['ss_division'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".ss_punch_line        = '". $data['ss_punch_line'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".ss_parent_id      	  = '". $data['ss_parent_id'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".is_renewable        = '". $data['is_renewable'] ."'"
                            //.",". TABLE_SETTINGS_SERVICES .".ss_price      	  = '". $data['ss_price'] ."'"
							//.",". TABLE_SETTINGS_SERVICES .".ss_client_detail   = '". $data['ss_client_detail'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".ss_status    		  = '". $data['ss_status'] ."'"
                            //.",". TABLE_SETTINGS_SERVICES .".tax1_name    		  = '". $data['tax1_name'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".tax1_name    		  = '". $data['tax1_name'] ."'"
					        .",". TABLE_SETTINGS_SERVICES .".tax1_id    		  = '". $data['tax1_id'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".tax1_value    		  = '". $data['tax1_value'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".sequence_subservices    		= '". $data['sequence_subservices'] ."'"							
							." WHERE ". TABLE_SETTINGS_SERVICES .".ss_id   = '".  $data['ss_id'] ."'";
                            
                
                if ( $db->query($query) ) {
					$messages->setOkMessage("The Service has been updated.");
                    if(isset($data['service_price']) && !empty($data['service_price'])){
                        for ( $i=0; $i < count($data['service_price']); $i++ ) {
                            if(!empty($data['sp_id'][$i])){
                              $sql	= " UPDATE ". TABLE_SETTINGS_SERVICES_PRICE
                                ." SET ". TABLE_SETTINGS_SERVICES_PRICE .".service_price = '". $data['service_price'][$i] ."'"
                                .",". TABLE_SETTINGS_SERVICES_PRICE .".currency_id      	  = '". $data['currency_id'][$i] ."'"
                                ." WHERE ". TABLE_SETTINGS_SERVICES_PRICE .".id   		 	  = '". $data['sp_id'][$i] ."'";
                                $db->query($sql);
                                
                            }else{
                                 $sql	= " INSERT INTO ". TABLE_SETTINGS_SERVICES_PRICE
                                ." SET ". TABLE_SETTINGS_SERVICES_PRICE .".service_price  = '". $data['service_price'][$i] ."'"
                                .",". TABLE_SETTINGS_SERVICES_PRICE .".currency_id   	  = '". $data['currency_id'][$i] ."'"
                                .",". TABLE_SETTINGS_SERVICES_PRICE .".service_id   	  = '". $data['ss_id'] ."'";
                                $db->query($sql);
                            }
                        }
                    }
                    
                }
                else {
                    $messages->setErrorMessage('Service was not updated.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $ss_id;
            $condition_query ='';
            $perform ='list';
            include ( DIR_FS_NC .'/services-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
				$_ALL_POST = $_POST;
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE ss_id = '". $ss_id ."' ";
                $_ALL_POST      = NULL;
                if ( Services::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) 
                    $_ALL_POST = $_ALL_POST[0];
                    $number=$_ALL_POST['number'];
					$_ALL_POST['old_file_1']=$_ALL_POST['file_1'];
					$_ALL_POST['old_file_2']=$_ALL_POST['file_2'];
					$_ALL_POST['old_file_3']=$_ALL_POST['file_3'];
                if( !empty($_ALL_POST['ss_id']) ){
                
                     $condition_query = " WHERE  service_id = '". $ss_id ."' ";
                     $servicePriceList     = NULL;
                     
                    if ( Services::getPriceList($db, $servicePriceList, '*', $condition_query) > 0 ) {                       
                        
                        if(!empty($servicePriceList) ){
                            foreach($servicePriceList as $key => $value){
                                $sp_id =$value['id'];
                                $currId =$value['currency_id'];
                                $service_price =$value['service_price'];
                                
                                
                                $_ALL_POST['service_price'][$key]       = $service_price;
                                $_ALL_POST['currency_id'][$key]       = $currId;
                                $_ALL_POST['sp_id'][$key]       = $sp_id;
                           }
                          
                        }
                    }
                    
                }
                
                
            }

            if ( !empty($_ALL_POST['ss_id']) ) {
                    //print_r($_ALL_POST);
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'ss_id', 'value' => $ss_id);
                    $hidden[] = array('name'=> 'number', 'value' => $number);
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'services', 'value' => 'services');
                    $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
                    $page["var"][] = array('variable' => 'divisions', 'value' => 'divisions');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'services-edit.html');
            }
            else {
                $messages->setErrorMessage("The Selected Service was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>