<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

if ( $perm->has('nc_myprofile_view') ) {
	$user_id = $my['user_id'];

	include_once ( DIR_FS_CLASS .'/Region.class.php');
	include_once ( DIR_FS_CLASS .'/Phone.class.php');
	include_once ( DIR_FS_CLASS .'/UserReminder.class.php');

	$_ALL_POST      = NULL;
	$data           = NULL;
	$region         = new Region();
	$phone          = new Phone(TABLE_USER);
	$reminder       = new UserReminder(TABLE_USER);
	$access_level   = $my['access_level']+1;
   
         
		// Check if the Edit form had been submitted and if error is generated.
		if( !empty( $user_id) ) { 
		
			// No error was generated, read the Executive data from the Database.
			$condition_query= " WHERE user_id = '". $user_id ."' ";
			$_ALL_POST      = NULL;
			if ( User::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
				$_ALL_POST = $_ALL_POST[0];
				
				$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
				
				// Check the Access Level.
			   /*  
			   if ( $_ALL_POST['access_level'] < $access_level ) {
					// Read the Contact Numbers.
					$phone->setPhoneOf(TABLE_USER, $user_id);
					$_ALL_POST['phone'] = $phone->get($db);
					
					// Read the Addresses.
					$region->setAddressOf(TABLE_USER, $user_id);
					$_ALL_POST['address_list'] = $region->get();
					
					// Read the Reminders.
					$reminder->setReminderOf(TABLE_USER, $user_id);
					$_ALL_POST['reminder_list'] = $reminder->get($db);
				}
				*/
				if ( !empty($_ALL_POST['jobp']) && !is_array($_ALL_POST['jobp']) ) {
					include_once ( DIR_FS_INCLUDES .'/team-job-functions.inc.php' );
					$_ALL_POST['jobp']          = explode(',', trim($_ALL_POST['jobp'],","));
					$temp                       = "'". implode("','", $_ALL_POST['jobp']) ."'";
					
					$_ALL_POST['jobp_details']  = array();
					
					$condition_query2= " LEFT JOIN ".TABLE_TJ_FUNCTIONS." ON 
					".TABLE_TJ_FUNCTIONS_P.".tjf_id =".TABLE_TJ_FUNCTIONS.".id 
					WHERE ".TABLE_TJ_FUNCTIONS_P.".id IN(".$temp.")";
					$jfields = TABLE_TJ_FUNCTIONS.".title,".TABLE_TJ_FUNCTIONS_P.".id," 
					.TABLE_TJ_FUNCTIONS_P.".jdescription as pdescription ," 
					.TABLE_TJ_FUNCTIONS_P.".jtitle as ptitle " ;
					 
					TeamJobFunctions::getParticulars($db, $_ALL_POST['jobp_details'], 
					$jfields, $condition_query2 );
					
					/* foreach ( $_ALL_POST['jobp_details1'] as $key=>$jobp) {
					 
						$_ALL_POST['jobp'][] = $jobp['id'];
						$_ALL_POST['jobp_details'][] = $jobp['ptitle'] .' - '
						.nl2br($jobp['pdescription']).' ('. $jobp['title'] .' )';
					} */
				}
			}
		}

		if ( !empty($_ALL_POST['user_id']) ) {

			// Check the Access Level.
		   
				
				/* 
				$phone_types    = $phone->getTypeList();
				$address_types  = $region->getTypeList();
				$lst_country    = $region->getCountryList();
				$lst_state      = $region->getStateList($data['state']);
				$lst_city       = $region->getCityList($data['city']); 
				*/


				// These parameters will be used when returning the control back to the List page.
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}
				$hidden[] = array('name'=> 'perform','value' => 'edit');
				$hidden[] = array('name'=> 'act', 'value' => 'save');
				$hidden[] = array('name'=> 'user_id', 'value' => $user_id);
				$hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
				
				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
				$page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
				$page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
				$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
				$page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
				$page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
				$page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
				$page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'my-profile-view.html');
			 
		}
		else {
			$messages->setErrorMessage("The Selected Executive was not found.");
		}
        
}
else {
	$messages->setErrorMessage("You do not have the Permission to Access this module.");
}
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>