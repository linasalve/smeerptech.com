<?php
    if ( $perm->has('nc_inward_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/inward.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Inward::validateUpdate($data, $extra) ) {
                $sql_u = '';                
                if($data['tpf_status'] == Inward::COMPLETED){
                    $sql_u = ",". TABLE_IN_WARD .".do_u = '".date("Y-m-d H:i:s")."'" ;
                }
                $query  = " UPDATE ". TABLE_IN_WARD
                           ." SET ".TABLE_IN_WARD .".type = '".                 $data['type'] ."'"
                                .",". TABLE_IN_WARD .".account_no = '".         $data['account_no'] ."'"
                                .",". TABLE_IN_WARD .".is_client = '".         $data['is_client'] ."'"
                                .",". TABLE_IN_WARD .".from = '".               $data['from'] ."'"
                                .",". TABLE_IN_WARD .".particulars = '".        $data['particulars'] ."'"
                                .",". TABLE_IN_WARD .".access_level = '".       $my['access_level'] ."'"
                                .",". TABLE_IN_WARD .".created_by = '".         $my['uid'] ."'"
                                .",". TABLE_IN_WARD .".content = '".            $data['content'] ."'"
                                .",". TABLE_IN_WARD .".content_type = '".       $data['content_type'] ."'"
                                .",". TABLE_IN_WARD .".mode = '".               $data['mode'] ."'"
                                .",". TABLE_IN_WARD .".for = '".                $data['for'] ."'"
                                .",". TABLE_IN_WARD .".tpf = '".                $data['tpf'] ."'"
                                . $sql_u 
                                .",". TABLE_IN_WARD .".tpf_status = '".         $data['tpf_status'] ."'"
                                .",". TABLE_IN_WARD .".ip = '".        $_SERVER['REMOTE_ADDR'] ."'"
                                ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Inward entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_IN_WARD .'.*'  ;            
            $condition_query = " WHERE (". TABLE_IN_WARD .".id = '". $id ."' )";
           /* $condition_query .= " AND ( ";
            // If my has created this Order.
            $condition_query .= " (". TABLE_IN_WARD .".created_by = '". $my['user_id'] ."' ) "; 
                                    
            /*$condition_query .= " (". TABLE_IN_WARD .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_IN_WARD .".access_level < $access_level ) ";    */          
            // Check if the User has the Right to Edit Orders created by other Users.
            /*
            if ( $perm->has('nc_bl_or_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_bl_or_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_VISITOR_BOOK. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_VISITOR_BOOK .".access_level < $access_level_o ) ";
            }*/
            //$condition_query .= " )";
          
            if ( Inward::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                //if ( $_ALL_POST['access_level'] < $access_level ) {
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                //}
                //else {
                    //$messages->setErrorMessage("You do not have the Permission to view the Record with the current Access Level.");
                //}
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/inward-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'inward-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
