<?php

    if ( $perm->has('nc_etask_usr_add') ) {

        $_ALL_POST	= NULL;
        $data       = NULL;

		//get client list for the dropdown
		EtaskUser::showUsers($clientlist);
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

			$sql = "select * FROM ".TABLE_USER ." WHERE user_id='".$_POST['id']."'";
			$db->query($sql);

			if($db->nf() > 0)
			{
				$db->next_record();
				$query = "INSERT INTO ".TABLE_ETASK_USER." (su_id,username,password,fname,lname,primary_email,";
				$query .= "parent_id,status,company_name) VALUES ('".$db->f('user_id')."','".$db->f('username')."',";
				$query .= "'".$db->f('password')."','".$db->f('f_name')."','".$db->f('l_name')."','".$db->f('email')."',";
				$query .= "'0','".$db->f('status')."','".$db->f('org')."')";
				if($db->query($query))
					$messages->setOkMessage("User added successfully.");
				else
					$messages->setErrorMessage("Cannot add user.");
			}
			else
				$messages->setErrorMessage("Cannot find user to add.");


				//to flush the data.
				$_ALL_POST	= NULL;
				$data		= NULL;
			}
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel']) || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/etask-users-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
  			$page["var"][] = array('variable' => 'clientlist', 'value' => 'clientlist');

            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'etask-users-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>