<?php
if ( $perm->has('nc_p_tsb_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    
   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $condition_query = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PROJECT_TASK_DETAILS.".task_id = '".$task_id."'".$condition_query;
    $list	= 	NULL;
    $total	=	ProjectTask::getList( $db, $list, '', $condition_query);

    //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
    // $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'.$extra_url.'&end=url';
    $condition_url .="&perform=".$perform."&task_id=".$task_id."&or_id=".$or_id."&actual_time=".$actual_time;;
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
   /*  $fields = TABLE_PROJECT_TASK_DETAILS.".id,".TABLE_PROJECT_TASK_DETAILS.".title as task_title, ".TABLE_PROJECT_TASK_DETAILS.".details,".TABLE_PROJECT_TASK_DETAILS.".last_updated, ".TABLE_PROJECT_TASK_MODULE.".title as module_title,".TABLE_PROJECT_TASK_STATUS.".title as status_title," ;
    $fields .= TABLE_PROJECT_TASK_PRIORITY.".title as priority_title, ".TABLE_PROJECT_TASK_TYPE.".title as type_title,".TABLE_PROJECT_TASK_RESOLUTION.".title as rs_title," ; */
	 $fields = TABLE_PROJECT_TASK_DETAILS.".id,".TABLE_PROJECT_TASK_DETAILS.".title as task_title, ".TABLE_PROJECT_TASK_DETAILS.".details,".TABLE_PROJECT_TASK_DETAILS.".last_updated, ".TABLE_PROJECT_TASK_MODULE.".title as module_title," ;
     
    $fields .= TABLE_PROJECT_TASK_DETAILS.".is_client,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".hrs,".TABLE_PROJECT_TASK_DETAILS.".min,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".hrs1,".TABLE_PROJECT_TASK_DETAILS.".min1,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".added_by";
    ProjectTask::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    $flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
        
          if($val['is_client']=='1'){
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                $val['is_client'] = '1';
            }else{
                $table = TABLE_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                 $val['is_client']='0';
            }
            
             //Calculate bugwise hrs spent
            if($actual_time){
                $fields_bws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr '
                    .', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin' ;
            }else{
                // show atual total time (timesheet * 3 ) total cost
                $fields_bws = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '
                    .', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min1) % 60 ) as totMin' ;
            
            }
             $condition_query_bwh = " WHERE (".TABLE_PROJECT_TASK_DETAILS.".id ='".$val['id']."' AND 
                                    ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '1' 
                                    AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0' ) OR 
                                    ".TABLE_PROJECT_TASK_DETAILS.".parent_id IN ('".$val['id']."')";
            $list_bws	= 	NULL;
          
          	ProjectTask::getList( $db, $list_bws, $fields_bws, $condition_query_bwh); 
          
            if(!empty($list_bws[0]['totHr']) || !empty($list_bws[0]['totMin'])){
                $list_bws =$list_bws[0];
                $val['tot_bhrs'] = strlen($list_bws['totHr']) ==1 ? '0'.$list_bws['totHr'] : $list_bws['totHr'] ; 
                $val['tot_bmin'] = strlen($list_bws['totMin']) ==1 ? '0'.$list_bws['totMin'] : $list_bws['totMin'] ; 
                
            }else{
                $val['tot_bhrs'] = '00';
                $val['tot_bmin'] = '00' ;
            }
            $val['last_comment']='';
             //Get Last comment bof
            //$condition_query_blc = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_client ='0' ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".id DESC Limit 0,1";
            $condition_query_blc = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".id DESC Limit 0,1";
            $list_blc	= 	NULL;
            $fields_blc = TABLE_PROJECT_TASK_DETAILS.".details";
          	ProjectTask::getList( $db, $list_blc, $fields_blc, $condition_query_blc);           
            if(!empty($list_blc)){
                $list_blc=$list_blc[0];
                $val['last_comment'] =  $list_blc['details'] ;
            }
         
            //Get Last comment eof
            
            
            
            $flist[$key]=$val;
        }
    }
    
    
    $page["var"][] = array('variable' => 'list', 'value' => 'flist');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task-bug-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
