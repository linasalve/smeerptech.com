<?php

    if ( $perm->has('nc_api_dmn_list') ) { //nc_rclub_dmnlist  
		$sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
		$sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
		$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
		$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
		$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
		$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
		$added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');    
		$added_all 	= isset($_GET["added_all"]) ? $_GET["added_all"]: ( isset($_POST["added_all"])  ? $_POST["added_all"]:'0');    
		$i 	        = isset($_GET["i"])         ? $_GET["i"]        : ( isset($_POST["i"])          ? $_POST["i"]   :     '0');    
		$j 	        = isset($_GET["j"])         ? $_GET["j"]        : ( isset($_POST["j"])          ? $_POST["j"]   :     '0');    
		/* $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20)); // Result per page	 */
		$rpp = isset($_GET["rpp"]) ? $_GET["rpp"] : ( isset($_POST["rpp"]) ? $_POST["rpp"] : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20)); 
		$condition_query = '';
		$condition_url = '';
		if (empty($x)){
			$x              = 1;
			$next_record    = 0 ;
		} else {
			$next_record    = ($x-1) * $rpp;
		}
		$variables["x"]     = $x;
		$variables["rpp"]   = $rpp;
		
		 
		$sTypeArray     = array('Any'  =>  array(  'Any of following' => '-1'),
                                    TABLE_ST_TICKETS  => array( 'Domain name'        => 'domain-name' 
                                                            ) 
                                );
            
        $sOrderByArray  = array(
                                    TABLE_ST_TICKETS  => array('Expiry Date'    => 'endtime',
														'Order Id'  => 'orderid',
                                                        'Order Creation' => 'creationtime'
													)
                                );
		
		if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ){
            $_SEARCH['sOrderBy']= $sOrderBy = 'endtime';
            $_SEARCH['sOrder']  = $sOrder   = 'asc';
            $order_by_table     = TABLE_ST_TICKETS;
        }
		/* $variables['status'] = array(
		'InActive'=>'InActive',
		'Active'=>'Active',
		'Suspended'=>'Suspended',
		'Pending Delete Restorable'=>'Pending Delete Restorable',
		'Deleted'=>'Deleted'); */
		$list=$condition_query=$query=$key=$val=$ticket_replies=$executive=$ticketStatusArr=$ticketRatingArr=$fields=null;
		
	if ( $sString != "" ){
		 // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray))){
			$sType = "";
			$search_table = NULL ;
		}
        //$where_added = true;
        $sub_url='';        
        if ( $search_table == "Any" ){
            foreach( $sTypeArray as $table => $field_arr ){	 
				if ( $table != "Any" ){  
					foreach( $field_arr as $key => $field ){
						$sub_url .= "&".$field ."=".trim($sString)." " ; 
					} 
                }
            }
            $sub_url = substr( $sub_url, 0, strlen( $sub_url ) - 1 );  
        }else{    
			$sub_url .= "&".$sType ."=".trim($sString) ." " ; 
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
	$dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
    
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR' ) && !empty($date_from)) { 
        
        $dfa = explode('/', $date_from);       
        $mktimefrm = mktime(0,0,0,$dfa[1],$dfa[0],$dfa[2]);
        
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
         
        $sub_url .= "&".$dt_field."-start" ."=".strtotime($dfa) ."" ; 
         
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
		$_SEARCH["dt_field"]       = $dt_field;
    }  
    // EO: From Date 
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
         
        $dta = explode('/', $date_to);
        $mktimeto = mktime(60,0,0,$dta[1],$dta[0],$dta[2]);
        
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $sub_url .= "&".$dt_field."-end" ."=".strtotime($dta) ."" ; 
        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
		$_SEARCH["dt_field"]       = $dt_field;
    } 
    	 
		echo $url="https://httpapi.com/api/domains/search.json?auth-userid=35247&api-key=qE1QP2Kd9xEbx6U0xICEi29nPvSbQ8tW&no-of-records=".$rpp."&page-no=".$x.$sub_url."&order-by=".$sOrderBy."+".$sOrder;
		$_SEARCH["searched"]    = true ;
		$curl_session = curl_init();
		curl_setopt($curl_session, CURLOPT_URL, $url); 
		curl_setopt($curl_session, CURLOPT_HEADER, false);
        curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl_session);
        curl_close($curl_session); 
		$dataArr=json_decode($response, true);		 
		$total = $dataArr['recsindb'];
		
		unset($dataArr['recsindb']);
		unset($dataArr['recsonpage']);
		
		//print_R($dataArr);
		if(!empty($dataArr)){
			foreach($dataArr as $key => $value)	{
				$dataArr1=array();
				foreach($value as $key1 => $value1)	{ 
					$key2 = str_replace('.','_',$key1);
					$dataArr1[$key2] = $value1; 
				}
				$dataArr2[]=$dataArr1; 
			}
		}
		 
		$condition_url.="&dt_field=".$dt_field."&perform=".$perform;     
		//$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','support-ticket.php','frmSearch');
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');        
        
		
		// Set the Permissions.
       
	    $_SEARCH["sOrderBy"]	= $sOrderBy ;
		$_SEARCH["sOrder"] 		= $sOrder ;
 
        $page["var"][] = array('variable' => 'can_view_classified', 'value' => 'can_view_classified');
        
       
        $page["var"][] = array('variable' => 'dataArr', 'value' => 'dataArr2');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'api-domain-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>