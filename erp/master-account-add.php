<?php

    if ( $perm->has('nc_sms_ma_add') ) {

        $_ALL_POST	= NULL;
        $data       = NULL;

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            if ( MasterAccount::validateAdd($data, $extra) ) {
				$sql = "SELECT ma_id,ma_totalsms,ma_allotedsms FROM ".TABLE_SMS_MASTER_ACC;
				$db->query($sql);
				if($db->nf() > 0)
				{
					$db->next_record();
					$numsms = $db->f('ma_totalsms');
					$id = $db->f('ma_id');
				}
				else
				{
					@$firstrecdflg = 1;
					$numsms = 0;
				}
	
				if(@$firstrecdflg == 1)
				{
					$sql = "INSERT INTO ".TABLE_SMS_MASTER_ACC." SET 
							ma_userid = '".$data['userid']."',
							ma_username = '".$data['username']."',
							ma_password = '".$data['pass']."',
							ma_totalsms = '".$data['totalsms']."',
							ma_posturl = '".$data['posturl']."',
							ma_status = '".$data['ma_status']."'";
					if($db->query($sql))
					{
						$id = mysql_insert_id();
						$sql = "INSERT INTO ".TABLE_SMS_MASTER_ACC_HISTORY." SET 
							mah_maid = '".$id."',
							mah_noofsms = '".$data['totalsms']."',
							mah_amount = '".$data['amount']."',
							mah_opening = '0',
							mah_balance = '".$data['totalsms']."',
							mah_persmsrate = '".$data['persmsrate']."',
							mah_date = '".date("Y-m-d")."'";
						$db->query($sql);
						$flg = 1;
					}
					else
						$flg = 0;
				}
				else
				{
					$sql = "UPDATE ".TABLE_SMS_MASTER_ACC." SET 
							ma_userid = '".$data['userid']."',
							ma_username = '".$data['username']."',
							ma_password = '".$data['pass']."',
							ma_totalsms = '".($numsms + $data['totalsms'])."',
							ma_posturl = '".$data['posturl']."',
							ma_status = '".$data['ma_status']."'";
					if($db->query($sql))
					{
						$sql = "INSERT INTO ".TABLE_SMS_MASTER_ACC_HISTORY." SET 
							mah_maid = '".$id."',
							mah_noofsms = '".$data['totalsms']."',
							mah_amount = '".$data['amount']."',
							mah_opening = '".$numsms."',
							mah_balance = '".($numsms + $data['totalsms'])."',
							mah_persmsrate = '".$data['persmsrate']."',
							mah_date = '".date("Y-m-d")."'";
						$db->query($sql);
						$flg = 1;
					}
					else
						$flg = 0;
				}

                if ($flg == 1) 
					$messages->setOkMessage("The Package has been added.");
				else
					$messages->setErrorMessage('Problem adding Package. Try later');
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/master-account-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'master-account-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>