<?php
  if ( $perm->has('nc_sl_riv_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the payment party class
		include_once (DIR_FS_INCLUDES .'/sale-rivals.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( Rivals::validateAdd($data, $extra) ) { 
                            $query	= " INSERT INTO ".TABLE_SALE_RIVALS
                            ." SET ".TABLE_SALE_RIVALS .".f_name 				= '". $data['f_name'] ."'"  
                                	.",". TABLE_SALE_RIVALS .".l_name 			= '". $data['l_name'] ."'"                                    .",". TABLE_SALE_RIVALS .".company_name 	= '". $data['company_name'] ."'"                                    .",". TABLE_SALE_RIVALS .".address 			= '". $data['address'] ."'"                                    .",". TABLE_SALE_RIVALS .".phone 			= '". $data['phone'] ."'"                                    .",". TABLE_SALE_RIVALS .".mobile 			= '". $data['mobile'] ."'"                                	.",". TABLE_SALE_RIVALS .".website_1		= '". $data['website_1'] ."'"                                	.",". TABLE_SALE_RIVALS .".website_2 		= '". $data['website_2'] ."'"                                	.",". TABLE_SALE_RIVALS .".website_3 		= '". $data['website_3'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".name_server_1 	= '". $data['name_server_1'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_1 			= '". $data['ip_1'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".name_server_2 	= '". $data['name_server_2'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_2 			= '". $data['ip_2'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".name_server_3	= '". $data['name_server_3'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_3				= '". $data['ip_3'] ."'"                    

                                	.",". TABLE_SALE_RIVALS .".name_server_4 	= '". $data['name_server_4'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_4				= '". $data['ip_4'] ."'"                    

                                	.",". TABLE_SALE_RIVALS .".name_server_5 	= '". $data['name_server_5'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_5 			= '". $data['ip_5'] ."'"                    
									.",". TABLE_SALE_RIVALS .".remarks 			= '". $data['remarks'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".date = '". 		date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
       if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sale-rivals.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sale-rivals.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sale-rivals.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-rivals-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
