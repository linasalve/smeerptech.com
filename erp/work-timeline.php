<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/work-timeline.inc.php' );
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page

    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
 
    
    if ( $perm->has('nc_wk_tl') ) {

        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_WORK_TL   =>  array( 	'Order Number'	=> 	'order_no',
															'Comments'		=> 'comments')
                            );
        
        $sOrderByArray  = array(
                                TABLE_WORK_TL => array(	  'ID'			=> 	'id',
														  'Order Number'=>	'order_no',
														  'Date'		=>	'do_assign')
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $sOrderBy 		= 'do_assign';
            $sOrder   		= 'DESC';
            $order_by_table	= TABLE_WORK_TL;
        }
    	$_SEARCH['sOrderBy']= $sOrderBy;
		$_SEARCH['sOrder']  = $sOrder;
        
        //use switch case here to perform action. 
        switch ($perform) {
            case ('srs'):{
                include (DIR_FS_NC .'/work-srs-edit.php');
                
				$page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
//           case ('add_timeline'):
//            case ('add'): {
//            
//                include (DIR_FS_NC.'/work-timeline-add.php');
//                
//                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
//                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
//                break;
//            }
//            case ('edit_timeline'):
//            case ('edit'): {
//                include (DIR_FS_NC .'/work-timeline-edit.php');
//                
//                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
//                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
//                break;
//            }
//            case ('view_timeline'):
//            case ('view'): {
//                include (DIR_FS_NC .'/work-timeline-view.php');
//                
//                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
//                break;
//            }
            case ('change_stage'): {
                include ( DIR_FS_NC .'/work-timeline-stage.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_attachment'): {
             
                include (DIR_FS_NC .'/work-timeline-view-attachment.php');
                break;
            }            
            case ('search'): {
                include(DIR_FS_NC."/work-timeline-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
//            case ('delete_timeline'):
//            case ('delete'): {
//                include ( DIR_FS_NC .'/work-timeline-delete.php');
//                
//                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
//                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
//                break;
//            }
			case ('details_timeline'):
            case ('details'): {
                include ( DIR_FS_NC .'/work-timeline-details.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list'):
            default: {
                include (DIR_FS_NC .'/work-timeline-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-timeline.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>