<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/vendors.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    //include ( DIR_FS_INCLUDES .'/sale-industry.inc.php');
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
    $added 		= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $sendinv    = isset($_GET["sendinv"])         ? $_GET["sendinv"]        : ( isset($_POST["sendinv"])          ? $_POST["sendinv"]       :'0');
    $x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $client_id	= isset($_GET["client_id"]) 	? $_GET["client_id"]	: ( isset($_POST["client_id"]) 	? $_POST["client_id"] : '' );

    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if($added){
         $messages->setOkMessage("Vendor Sub User entry has been done.");
    }
    
    if ( $perm->has('nc_v_su') ) {

        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_VENDORS   =>  array(  'Relationship Number'        => 'number',
                                                            //'Billing Name'     => 'billing_name',
                                                            'User Name'     => 'username',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Middle Name'   => 'm_name',
                                                            'Last Name'     => 'l_name',
                                                            'Pet Name'      => 'p_name',
                                                            'Designation'   => 'desig',
                                                            'Organization'  => 'org',
                                                            'Domain'        => 'domain'
                                                        )
                            );
        
        $sOrderByArray  = array(
                                TABLE_VENDORS => array('Relationship Number'   => 'number',
                                                    'User Name'     => 'username',
                                                    'E-mail'        => 'email',
                                                    'First Name'    => 'f_name',
                                                    'Last Name'     => 'l_name',
                                                    'Date of Birth' => 'do_birth',
                                                    'Date of Entry'=> 'do_add',
                                                    'Date of Login' => 'do_login',
                                                    //'Status'        => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_add';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_VENDORS;
        }
    
        $variables['title_type'] = Vendors::getTitleType();
        $variables['status'] = Vendors::getStatus();
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {
            
                include (DIR_FS_NC.'/vendors-su-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-su.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('edit'): {
                include (DIR_FS_NC .'/vendors-su-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-su.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('view'): {
                include (DIR_FS_NC .'/vendors-su-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
            /*case ('send_pwd'):
            {
                include (DIR_FS_NC .'/clients-send-pwd.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }*/
            
            case ('change_status'): {
                include ( DIR_FS_NC .'/vendors-su-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-su.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('change_mail_status'): {
                include ( DIR_FS_NC .'/vendors-su-mail-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-su.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('search'): {
                include(DIR_FS_NC."/vendors-su-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-su.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('delete'): {
                include ( DIR_FS_NC .'/vendors-su-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-su.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('list'):
            default: {
                include (DIR_FS_NC .'/vendors-su-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-su.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-su.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("client_id", $client_id);
    
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>