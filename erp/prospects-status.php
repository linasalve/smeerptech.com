<?php
    if ( $perm->has('nc_pr_status') ) {
        $user_id= isset($_GET["user_id"])? $_GET["user_id"] : ( isset($_POST["user_id"])? $_POST["user_id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        $access_level += 1;
        
        
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        Prospects::updateStatus($user_id, $status, $extra);
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-status.html');
        // Display the  list.
        //include ( DIR_FS_NC .'/prospects-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>