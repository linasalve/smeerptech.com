<?php

	/* $condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:''); */
    $condition_url  = '';
    $where_added    = true;
   
    // BO: Status data.
    $sStatusStr='';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' ) && isset($sStatus)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
           
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_TASK_REMINDER .".status IN ('". $sStatusStr ."') ";
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
   
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
    }
   // BO: Status data.
    $tTypeStr='';
    $chk_ttype = isset($_POST["chk_ttype"])   ? $_POST["chk_ttype"]  : (isset($_GET["chk_ttype"])   ? $_GET["chk_ttype"]   :'');
    $tType    = isset($_POST["tType"])      ? $_POST["tType"]     : (isset($_GET["tType"])      ? $_GET["tType"]      :'');
    if ( ($chk_ttype == 'AND' || $chk_ttype == 'OR' ) && isset($tType)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_ttype;
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        } 
        if(is_array($tType)){
             $tTypeStr = implode("','", $tType) ;
             $tTypeStr1 = implode(",", $tType) ; 
        }else{
            $tTypeStr = str_replace(',',"','",$tType);
            $tTypeStr1 = $tType ;
            $tType = explode(",",$tType) ; 
        } 
        $condition_query .= " ". TABLE_TASK_REMINDER .".task_type IN ('". $tTypeStr ."') ";
        $condition_url          .= "&chk_ttype=$chk_ttype&tType=$tTypeStr1"; 
        $_SEARCH["chk_ttype"]  = $chk_ttype;
        $_SEARCH["tType"]     = $tType;
    }
	
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
          
            if ( $where_added ) {
                $condition_query .= " AND (";
            }
            else {
                $condition_query.= ' WHERE (';
                $where_added    = true;
            }
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    
                    $table = TABLE_TASK_REMINDER ;
                    foreach( $field_arr as $key => $field ) {
                            $condition_query .= " ". $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                        }
                    $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
                }
            }
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            if ( $where_added ) {
                $condition_query .= " AND ";
            }
            else {
                $condition_query.= " WHERE ";
                $where_added    = true;
            }
       
            $condition_query .= " (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
        
	} 
    
    // EO: Status data.
	$chk_is_imp  = isset($_POST["chk_is_imp"])? $_POST["chk_is_imp"]   : (isset($_GET["chk_is_imp"])? $_GET["chk_is_imp"]:'');
	if ( isset($chk_is_imp) && $chk_is_imp==1 ){
		if ( $where_added ) {
            $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
	    $condition_query .= " ". TABLE_TASK_REMINDER .".is_imp = '". $chk_is_imp ."'"; 
        
        $condition_url              .= "&chk_is_imp=$chk_is_imp";
        $_SEARCH["chk_is_imp"]   = $chk_is_imp;
	  
	}
	
    $chk_is_sjm  = isset($_POST["chk_is_sjm"])? $_POST["chk_is_sjm"]   : (isset($_GET["chk_is_sjm"])? $_GET["chk_is_sjm"]:'');
	if ( isset($chk_is_sjm) && $chk_is_sjm==1 ){
		if ( $where_added ) {
            $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
	    //$condition_query .= " ". TABLE_TASK_REMINDER .".type = '". Taskreminder::CEOTASK ."'"; 
	    $condition_query .= " ". TABLE_TASK_REMINDER .".is_ceo = '1'"; 
        
        $condition_url              .= "&chk_is_sjm=$chk_is_sjm";
        $_SEARCH["chk_is_sjm"]   = $chk_is_sjm;
	  
	}
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
 
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from) ){
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_TASK_REMINDER .".".$dt_field." >= '". $dfa ."'"; 
        
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;
    }
 
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR')  && !empty($date_to)){
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        
        $condition_query .= " ". TABLE_TASK_REMINDER .".".$dt_field." <= '". $dta ."'";
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }
  
    // EO: Upto Date
   
    //Task reminder with selected executives BOF
     $executive  = isset($_POST["executive"])? $_POST["executive"]   : (isset($_GET["executive"])? $_GET["executive"]:'');
     $alloted_by  = isset($_POST["alloted_by"])? $_POST["alloted_by"]   : (isset($_GET["alloted_by"])? $_GET["alloted_by"]:'');
   
    if(!empty($executive) ){ 
        $_SEARCH['executive']=$executive ;
        $edetails	= 	NULL;
        $condition_querye=" WHERE "." user_id='".$_SEARCH['executive']."'";
        User::getList( $db, $edetails, 'f_name,l_name,number,email', $condition_querye);
        $edetails = $edetails[0];
        
        $_SEARCH['executive_details'] = $edetails['f_name']." ".$edetails['l_name']." (".$edetails['number'].") (".$edetails['email'].")" ;
        if(isset($alloted_by)){
            $_SEARCH['alloted_by']= $alloted_by;
            
            $search_other=1;
            if( $_SEARCH['alloted_by'] == Taskreminder::ALLOTED_BY_ME){
                if ( $where_added ) {
                    $condition_query .= " AND ";
                }
                else {
                    $condition_query.= ' WHERE ';
                    $where_added    = true;
                }
               
                $condition_query .= "  ( "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $_SEARCH['executive'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $_SEARCH['executive'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',".$_SEARCH['executive'].",' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^".$_SEARCH['executive'].",' "
                            . " ) AND ". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."'" ;
                            
                $condition_url .= "&executive=".$_SEARCH['executive']."&alloted_by=".$_SEARCH['alloted_by'];
               
               
                
            }elseif( $_SEARCH['alloted_by'] == Taskreminder::SHOW_ALL ){
                //$subcondition = " AND ". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."'" ;
                if ( $where_added ) {
                    $condition_query .= " AND ";
                }
                else {
                    $condition_query.= ' WHERE ';
                    $where_added    = true;
                }
                $condition_query .= " ( "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $_SEARCH['executive'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',".$_SEARCH['executive'] ."$' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP ',".$_SEARCH['executive'].",' OR "
                                . TABLE_TASK_REMINDER .".allotted_to REGEXP '^".$_SEARCH['executive'].",' "
                            . "  OR ".TABLE_TASK_REMINDER. ".created_by = '". $_SEARCH['executive']."' )"  ;
               
                $condition_url .= "&executive=".$_SEARCH['executive']."&alloted_by=".$_SEARCH['alloted_by'];
                
               
            }
        }
        
    }else{
        if(isset($alloted_by)){
            $_SEARCH['alloted_by']= $alloted_by;
            
            $search_other=1;
            if( $_SEARCH['alloted_by'] == Taskreminder::ALLOTED_BY_ME){
                if ( $where_added ) {
                    $condition_query .= " AND ";
                }
                else {
                    $condition_query.= ' WHERE ';
                    $where_added    = true;
                }
               
                $condition_query .=  TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."'" ;
                            
                $condition_url .= "&alloted_by=".$_SEARCH['alloted_by'];
               
               
                
            }elseif( $_SEARCH['alloted_by'] == Taskreminder::SHOW_ALL ){
                //$subcondition = " AND ". TABLE_TASK_REMINDER. ".created_by = '". $my['user_id']."'" ;
               if ( $where_added ) {
                    $condition_query .= " AND ";
                }
                else {
                    $condition_query.= ' WHERE ';
                    $where_added    = true;
                }
                $condition_query .= " ( 1 ) "      ;
               
                $condition_url .= "&alloted_by=".$_SEARCH['alloted_by'];
                
               
            }
        }
    
    }
    //Task reminder with selected executives EOF
   
    
    
    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/task-reminder-list.php');
?>