<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : 
	(isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    $searchStr = 0;
	if ( $sString != "" ){
		$searchStr = 1;
		// Set the field on which to make the search.
		if( !($search_table = findIndex($sType, $sTypeArray )) ){
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ){
                if ( $table != "Any" ) {
                    
                    //$table = TABLE_PAYMENT_TRANSACTION ;
                    foreach( $field_arr as $key => $field ) {                      	   
						$condition_query .= " ". $table .".". clearSearchString($field,'-') ." LIKE 
						'%". $sString ."%' OR " ;
                        $where_added = true;
                    }                 
                }
               
            }
            $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType,'-') ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
  
    $sStatusStr='';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' ) && isset($sStatus)) {
		
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
        }
        
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".status IN ('". $sStatusStr ."') ";
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";   
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
    }
    // EO: Status data.
	
	// BO:fStatus data    $fStatusStr='';
    $chk_fstatus = isset($_POST["chk_fstatus"])   ? $_POST["chk_fstatus"]  : (isset($_GET["chk_fstatus"])   ? $_GET["chk_fstatus"]   :'');
    $fStatus    = isset($_POST["fStatus"])      ? $_POST["fStatus"]     : (isset($_GET["fStatus"])      ? $_GET["fStatus"]      :'');
    if ( ($chk_fstatus == 'AND' || $chk_fstatus == 'OR' ) && isset($fStatus)) {
		
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_fstatus;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($fStatus)){
             $fStatusStr = implode("','", $fStatus) ;
             $fStatusStr1 = implode(",", $fStatus) ;
             
        }else{
            $fStatusStr = str_replace(',',"','",$fStatus);
            $fStatusStr1 = $fStatus ;
            $fStatus = explode(",",$fStatus) ;
        }
        
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".f_status IN ('". $fStatusStr ."') ";
        $condition_url          .= "&chk_fstatus=$chk_fstatus&fStatus=$fStatusStr1";   
        $_SEARCH["chk_fstatus"]  = $chk_fstatus;
        $_SEARCH["fStatus"]     = $fStatus;
    }
    // EO: fStatus data.
	// BO: LinkingStatus data.
    $lStatusStr='';
    $chk_lstatus = isset($_POST["chk_lstatus"])   ? $_POST["chk_lstatus"]  : (isset($_GET["chk_lstatus"])   ? $_GET["chk_lstatus"]   :'');
    $lStatus    = isset($_POST["lStatus"])      ? $_POST["lStatus"]     : (isset($_GET["lStatus"])      ? $_GET["lStatus"]      :'');
    if ( ($chk_lstatus == 'AND' || $chk_lstatus == 'OR' ) && isset($lStatus)) {
		
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_lstatus;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($lStatus)){
             $lStatusStr = implode("','", $lStatus) ;
             $lStatusStr1 = implode(",", $lStatus) ;
             
        }else{
            $lStatusStr = str_replace(',',"','",$lStatus);
            $lStatusStr1 = $lStatus ;
            $lStatus = explode(",",$lStatus) ;
            
        }
        
       $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".linking_status IN ('". $lStatusStr ."') ";
        $condition_url          .= "&chk_lstatus=$chk_lstatus&lStatus=$lStatusStr1";   
        $_SEARCH["chk_lstatus"]  = $chk_lstatus;
        $_SEARCH["lStatus"]     = $lStatus;
    }
    // EO: LinkingStatus data.
	// BO: LinkingStatus data.
    $bStatusStr='';
    $chk_bstatus = isset($_POST["chk_bstatus"])   ? $_POST["chk_bstatus"]  : (isset($_GET["chk_bstatus"])   ? $_GET["chk_bstatus"]   :'');
    $bStatus    = isset($_POST["bStatus"])      ? $_POST["bStatus"]     : (isset($_GET["bStatus"])      ? $_GET["bStatus"]      :'');
    if ( ($chk_bstatus == 'AND' || $chk_bstatus == 'OR' ) && isset($bStatus)) {
		
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_bstatus;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($bStatus)){
             $bStatusStr = implode("','", $bStatus) ;
             $bStatusStr1 = implode(",", $bStatus) ;
             
        }else{
            $bStatusStr = str_replace(',',"','",$bStatus);
            $bStatusStr1 = $bStatus ;
            $bStatus = explode(",",$bStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".bank_stmt_status IN ('". $bStatusStr ."') ";
        $condition_url          .= "&chk_lstatus=$chk_bstatus&bStatus=$bStatusStr1";   
        $_SEARCH["chk_bstatus"]  = $chk_bstatus;
        $_SEARCH["bStatus"]     = $bStatus;
    }
    // EO: LinkingStatus data.
    // BO: Type data.
    $sTrTypeStr='';
    $chk_type = isset($_POST["chk_type"])   ? $_POST["chk_type"]  : (isset($_GET["chk_type"])   ? $_GET["chk_type"]   :'');
    $sTrType = isset($_POST["sTrType"]) ? $_POST["sTrType"] : (isset($_GET["sTrType"]) ?$_GET["sTrType"]     :'');
    if ( ($chk_type == 'AND' || $chk_type == 'OR' ) && isset($sTrType)) {
		$searchStr = 1;
        if ( $where_added ) {
           $condition_query .= " ". $chk_type;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($sTrType)){
             $sTrTypeStr = implode("','", $sTrType) ;
             $sTrTypeStr1 = implode(",", $sTrType) ;
             
        }else{
            $sTrTypeStr = str_replace(',',"','",$sTrType);
            $sTrTypeStr1 = $sTrType ;
            $sTrType = explode(",",$sTrType) ;
            
        }
       
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".transaction_type IN ('". $sTrTypeStr ."') ";
        $condition_url   .= "&chk_type=$chk_type&sTrType=$sTrTypeStr1";   
        $_SEARCH["chk_type"]  = $chk_type;
        $_SEARCH["sTrType"]     = $sTrType;
    } 
    // EO: Type data.
	// BO: Type data.
    $chk_advance = isset($_POST["chk_advance"])   ? $_POST["chk_advance"]  : (isset($_GET["chk_advance"])   ? $_GET["chk_advance"]   :'');  
    if ( ($chk_advance == 'AND' || $chk_advance == 'OR' ) ) {
		$searchStr = 1;
        if ( $where_added ) {
           $condition_query .= " ". $chk_advance;
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".is_salary_advance = '1' ";
        $condition_url   .= "&chk_advance=$chk_advance";   
        $_SEARCH["chk_advance"]  = $chk_advance;
    } 
    // EO: Type data.
    //BO : Internal Account Head
    $iAccHdStr='';
    $chk_iacc= isset($_POST["chk_iacc"])   ? $_POST["chk_iacc"]  : (isset($_GET["chk_iacc"])   ? $_GET["chk_iacc"]   :'');
    $iAccHd    = isset($_POST["iAccHd"])      ? $_POST["iAccHd"]     : (isset($_GET["iAccHd"])      ?$_GET["iAccHd"]     :'');
    if ( ($chk_iacc == 'AND' || $chk_iacc == 'OR' ) && isset($iAccHd)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_iacc;
        } else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }  
        if(is_array($iAccHd)){
             $iAccHdStr = implode("','", $iAccHd) ;
             $iAccHdStr1 = implode(",", $iAccHd) ;
             
        }else{
            $iAccHdStr = str_replace(',',"','",$iAccHd);
            $iAccHdStr1 = $iAccHd ;
            $iAccHd = explode(",",$iAccHd) ;
            
        } 
       $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".iaccount_head_id IN ('". $iAccHdStr ."') ";
       $condition_url          .= "&chk_iacc=$chk_iacc&iAccHd=$iAccHdStr1";   
       $_SEARCH["chk_iacc"]  = $chk_iacc;
       $_SEARCH["iAccHd"]     = $iAccHd;
    } 
    //EO : Internal Account Head
	
	
	
	
	
	
    //BO : Account Head
    $sAccHdStr='';
    $chk_acc = isset($_POST["chk_acc"])   ? $_POST["chk_acc"]  : (isset($_GET["chk_acc"])   ? $_GET["chk_acc"]   :'');
    $sAccHd    = isset($_POST["sAccHd"]) ? $_POST["sAccHd"]: (isset($_GET["sAccHd"])      ?$_GET["sAccHd"]     :'');
    if ( ($chk_acc == 'AND' || $chk_acc == 'OR' ) && isset($sAccHd)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_acc;
        }else{
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if(is_array($sAccHd)){
             $sAccHdStr = implode("','", $sAccHd) ;
             $sAccHdStr1 = implode(",", $sAccHd) ;
             
        }else{
            $sAccHdStr = str_replace(',',"','",$sAccHd);
            $sAccHdStr1 = $sAccHd ;
            $sAccHd = explode(",",$sAccHd) ;
            
        }
       
       $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".account_head_id IN ('". $sAccHdStr ."') ";
       $condition_url          .= "&chk_acc=$chk_acc&sAccHd=$sAccHdStr1";   
       $_SEARCH["chk_acc"]  = $chk_acc;
       $_SEARCH["sAccHd"]     = $sAccHd;
    } 
    //EO : Account Head
	//BO : Client's Bank
    $cBankStr='';
    $chk_clbank = isset($_POST["chk_clbank"])   ? $_POST["chk_clbank"]  : (isset($_GET["chk_clbank"])   ? $_GET["chk_clbank"]   :'');
    $cBank    = isset($_POST["cBank"])      ? $_POST["cBank"]     : (isset($_GET["cBank"]) ?$_GET["cBank"]     :'');
    if ( ($chk_clbank == 'AND' || $chk_clbank == 'OR' ) && isset($cBank)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_clbank;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
       
        
        if(is_array($cBank)){
             $cBankStr = implode("','", $cBank) ;
             $cBankStr1 = implode(",", $cBank) ;
             
        }else{
            $cBankStr = str_replace(',',"','",$cBank);
            $cBankStr1 = $cBank ;
            $cBank = explode(",",$cBank) ;
            
        }
       
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".pay_bank_company IN('". $cBankStr ."')  ";
	   
       $condition_url          .= "&chk_clbank=$chk_clbank&cBank=$cBankStr1";   
       $_SEARCH["chk_clbank"]  = $chk_clbank;
       $_SEARCH["cBank"]     = $cBank;
    } 
    //EO : Client's Bank
	
	//BO : Credit Bank
    $scBankStr='';
    $chk_cbank = isset($_POST["chk_cbank"])   ? $_POST["chk_cbank"]  : (isset($_GET["chk_cbank"])   ? $_GET["chk_cbank"]   :'');
    $scBank    = isset($_POST["scBank"])      ? $_POST["scBank"]     : (isset($_GET["scBank"]) ?$_GET["scBank"]     :'');
    if ( ($chk_cbank == 'AND' || $chk_cbank == 'OR' ) && isset($scBank)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_cbank;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
       
        
        if(is_array($scBank)){
             $scBankStr = implode("','", $scBank) ;
             $scBankStr1 = implode(",", $scBank) ;
             
        }else{
            $scBankStr = str_replace(',',"','",$scBank);
            $scBankStr1 = $scBank ;
            $scBank = explode(",",$scBank) ;
            
        }
       
        $condition_query .= " ( ". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id IN('". $scBankStr ."') 
	    OR ". TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id IN('". $scBankStr ."') ) ";
	   
       $condition_url          .= "&chk_cbank=$chk_cbank&scBank=$scBankStr1";   
       $_SEARCH["chk_cbank"]  = $chk_cbank;
       $_SEARCH["scBank"]     = $scBank;
    } 
    //EO : Credit Bank
	
	//BO : Debit Bank
    $sdBankStr='';
    $chk_dbank = isset($_POST["chk_dbank"])   ? $_POST["chk_dbank"]  : (isset($_GET["chk_dbank"])   ? $_GET["chk_dbank"]   :'');
    $sdBank = isset($_POST["sdBank"]) ? $_POST["sdBank"] : (isset($_GET["sdBank"]) ?$_GET["sdBank"]:'');
    if ( ($chk_dbank == 'AND' || $chk_dbank == 'OR' ) && isset($sdBank)) {
        $searchStr = 1;
		
		if ( $where_added ) {
           $condition_query .= " ". $chk_dbank;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
       
        
        if(is_array($sdBank)){
             $sdBankStr = implode("','", $sdBank) ;
             $sdBankStr1 = implode(",", $sdBank) ;
             
        }else{
            $sdBankStr = str_replace(',',"','",$sdBank);
            $sdBankStr1 = $sdBank ;
            $sdBank = explode(",",$sdBank) ;
            
        }
       
       $condition_query .= " (". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id IN('". $sdBankStr ."') 
		OR ". TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id IN('". $sdBankStr ."'))   ";
		
       $condition_url          .= "&chk_dbank=$chk_dbank&sdBank=$sdBankStr1";   
       $_SEARCH["chk_dbank"]  = $chk_dbank;
       $_SEARCH["sdBank"]     = $sdBank;
    } 
    //EO : Debit Bank
	
    //BO : Mode
    $sModeIdStr='';
    $chk_mode = isset($_POST["chk_mode"])   ? $_POST["chk_mode"]  : (isset($_GET["chk_mode"])   ? $_GET["chk_mode"]   :'');
    $sModeId    = isset($_POST["sModeId"])      ? $_POST["sModeId"]     : (isset($_GET["sModeId"])      ?$_GET["sModeId"]     :'');
    if ( ($chk_mode == 'AND' || $chk_mode == 'OR' ) && isset($sModeId)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_mode;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
              
        if(is_array($sModeId)){
             $sModeIdStr = implode("','", $sModeId) ;
             $sModeIdStr1 = implode(",", $sModeId) ;
             
        }else{
            $sModeIdStr = str_replace(',',"','",$sModeId);
            $sModeIdStr1 = $sModeId ;
            $sModeId = explode(",",$sModeId) ;
            
        }
       
       $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".mode_id  IN ('". $sModeIdStr ."') ";
       $condition_url          .= "&chk_mode=$chk_mode&sModeId=$sModeIdStr1";   
       $_SEARCH["chk_mode"]  = $chk_mode;
       $_SEARCH["sModeId"]     = $sModeId;
    } 
    //EO : Mode
	
	 //BO : Mode
    $sGlIdStr='';
    $chk_gl = isset($_POST["chk_gl"])   ? $_POST["chk_gl"]  : (isset($_GET["chk_gl"])   ? $_GET["chk_gl"]   :'');
    $sGlId    = isset($_POST["sGlId"])      ? $_POST["sGlId"]     : (isset($_GET["sGlId"])      ?$_GET["sGlId"]     :'');
    if ( ($chk_gl == 'AND' || $chk_gl == 'OR' ) && isset($sGlId)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_gl;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
              
        if(is_array($sGlId)){
             $sGlIdStr = implode("','", $sGlId) ;
             $sGlIdStr1 = implode(",", $sGlId) ;
             
        }else{
            $sGlIdStr = str_replace(',',"','",$sGlId);
            $sGlIdStr1 = $sModeId ;
            $sGlId = explode(",",$sGlId) ;
            
        }
       
       $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".gl_type  IN ('". $sGlIdStr ."') ";
       $condition_url          .= "&chk_gl=$chk_gl&sGlId=$sGlIdStr1";    
       $_SEARCH["chk_gl"]  = $chk_gl;
       $_SEARCH["sGlId"]     = $sGlId;
    } 
    //EO : Mode
    //BO : Company
    $sCompIdStr='';
    $chk_cmp = isset($_POST["chk_cmp"])   ? $_POST["chk_cmp"]  : (isset($_GET["chk_cmp"])   ? $_GET["chk_cmp"]   :'');
    $sCompId    = isset($_POST["sCompId"])      ? $_POST["sCompId"]     : (isset($_GET["sCompId"])      ?$_GET["sCompId"]     :'');
    
    if ( ($chk_cmp == 'AND' || $chk_cmp == 'OR' ) && !empty($sCompId)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_cmp;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
              
        if(is_array($sCompId)){
             $sCompIdStr = implode("','", $sCompId) ;
             $sCompIdStr1 = implode(",", $sCompId) ;
             
        }else{
            $sCompIdStr = str_replace(',',"','",$sCompId);
            $sCompIdStr1 = $sCompId ;
            $sCompId = explode(",",$sCompId) ;
            
        }
       
       $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".company_id  IN ('". $sCompIdStr ."') ";
       $condition_url          .= "&chk_cmp=$chk_cmp&sCompId=$sCompIdStr1";   
       $_SEARCH["chk_cmp"]  = $chk_cmp;
       $_SEARCH["sCompId"]     = $sCompId;
    } 
    //EO : Mode
    
    
    //BO Financial Yr 
    $fYrStr='';
    $chk_fy = isset($_POST["chk_fy"])   ? $_POST["chk_fy"]  : (isset($_GET["chk_fy"])   ? $_GET["chk_fy"]   :'');
    $fYr    = isset($_POST["fYr"])      ? $_POST["fYr"]     : (isset($_GET["fYr"])      ?$_GET["fYr"]     :'');
    if ( ($chk_fy == 'AND' || $chk_fy == 'OR' ) && isset($fYr) && !empty($fYr)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_fy;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
              
        if(is_array($fYr)){
             $fYrStr = implode("','", $fYr) ;
             $fYrStr1 = implode(",", $fYr) ;
             
        }else{
            $fYrStr = str_replace(',',"','",$fYr);
            $fYrStr1 = $fYr ;
            $fYr = explode(",",$fYr) ;
            
        }
        $sqlYr='';
        if(!empty($fYr)){
            foreach($fYr as $key=>$val){
                // $sqlYr.= TABLE_PAYMENT_TRANSACTION .".transaction_id LIKE '%". $val ."%' OR " ;
                 $sqlYr.= TABLE_PAYMENT_TRANSACTION .".financial_yr  LIKE '%". $val ."%' OR " ;
            }
            $sqlYr = substr( $sqlYr, 0, strlen( $sqlYr ) - 3 );
            $sqlYr = " ( ".$sqlYr." ) ";
        }
       
       $condition_query .= $sqlYr ;
       $condition_url          .= "&chk_fy=$chk_fy&fYr=$fYrStr1";   
       $_SEARCH["chk_fy"]  = $chk_fy;
       $_SEARCH["fYr"]     = $fYr;
    } 
    
   
    //EO : Financial Yr
    
    // BO: is_pdc data.
    $is_pdcStr='';
    $chk_is_pdc = isset($_POST["chk_is_pdc"])   ? $_POST["chk_is_pdc"]  : (isset($_GET["chk_is_pdc"])   ? 
	$_GET["chk_is_pdc"]   :'');
    $is_pdc    = isset($_POST["is_pdc"])      ? $_POST["is_pdc"]     : (isset($_GET["is_pdc"])      ? $_GET["is_pdc"]      :'');
    if ( ($chk_is_pdc == 'AND' || $chk_is_pdc == 'OR' ) && $is_pdc!='') {    	
        $searchStr = 1;
		if ( $where_added ) {
            $condition_query .= " ". $chk_is_pdc;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($is_pdc)){
             $is_pdcStr = implode(",", $is_pdc) ;
             
        }else{
            $is_pdcStr = $is_pdc ;
            
            
        }        
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".is_pdc IN ('". $is_pdcStr ."') ";
        $condition_url          .= "&chk_is_pdc=$chk_is_pdc&is_pdc=$is_pdcStr";
   
        $_SEARCH["chk_is_pdc"]  = $chk_is_pdc;
        $_SEARCH["is_pdc"]     = $is_pdc;
    }  
    // EO: is_pdc data.
	
	$is_cronStr='';
    $chk_cron = isset($_POST["chk_cron"])   ? $_POST["chk_cron"]  : (isset($_GET["chk_cron"])   ? 
	$_GET["chk_cron"]   :'');
    $created_by_cron  = isset($_POST["created_by_cron"])      ? $_POST["created_by_cron"]     
	: (isset($_GET["created_by_cron"])      ? $_GET["created_by_cron"]      :'');
    if ( ($chk_cron == 'AND' || $chk_cron == 'OR' ) && $created_by_cron!='') {    	
        $searchStr = 1;
		if ( $where_added ) {
            $condition_query .= " ". $chk_cron;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($created_by_cron)){
             $is_cronStr = implode(",", $created_by_cron) ;
             
        }else{
            $is_cronStr = $created_by_cron ;
            
            
        }        
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".created_by_cron IN ('". $is_cronStr ."') ";
        $condition_url          .= "&chk_cron=$chk_cron&created_by_cron=$is_cronStr";
   
        $_SEARCH["chk_cron"]  = $chk_cron;
        $_SEARCH["created_by_cron"]     = $created_by_cron;
    }
	
	 // BO: is_double data.
    $is_doubleStr='';
    $chk_is_double = isset($_POST["chk_is_double"])   ? $_POST["chk_is_double"]  : (isset($_GET["chk_is_double"])   ? 
	$_GET["chk_is_double"]   :'');
    $is_double    = isset($_POST["is_double"])  ? $_POST["is_double"]  : (isset($_GET["is_double"]) 
	? $_GET["is_double"]      :'');
    if ( ($chk_is_double == 'AND' || $chk_is_double == 'OR' ) && $is_double!='') {    	
        $searchStr = 1;
		if ( $where_added ) {
            $condition_query .= " ". $chk_is_double;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($is_double)){
             $is_doubleStr = implode(",", $is_double) ;
             
        }else{
            $is_doubleStr = $is_double ;
        }
        
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".is_double IN ('". $is_doubleStr ."') ";
        $condition_url          .= "&chk_is_double=$chk_is_double&is_double=$is_doubleStr";
   
        $_SEARCH["chk_is_double"]  = $chk_is_double;
        $_SEARCH["is_double"]     = $is_double;
    }  
    // EO: is_double data.
	
	
	
    //Bank Id BOF
    $bank_id    = isset($_POST["bank_id"])      ? $_POST["bank_id"]     : (isset($_GET["bank_id"])      ? $_GET["bank_id"]      :'');
    $bank_id_type   = isset($_POST["bank_id_type"])      ? $_POST["bank_id_type"]     : (isset($_GET["bank_id_type"])      ? $_GET["bank_id_type"]      :'');

    if (  isset($bank_id) && !empty($bank_id) && !empty($bank_id_type)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if($bank_id_type=='in'){
            
          $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id ='". $bank_id ."' ";
        }elseif($bank_id_type=='out'){
        
         $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id ='". $bank_id ."' ";
        }
        
      
        $condition_url          .= "&bank_id=$bank_id&bank_id_type=$bank_id_type";
   
      
    }
    //Bank Id EOF
    
    
    //Client serach bof
	$search_client = isset($_POST["search_client"])   ? $_POST["search_client"]  : (isset($_GET["search_client"])   ? $_GET["search_client"]   :'');
	$search_client_details = isset($_POST["search_client_details"])   ? $_POST["search_client_details"]  : (isset($_GET["search_client_details"])   ? $_GET["search_client_details"]   :'');
	
	if(!empty($search_client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".client_id ='". $search_client ."' ";        
        $condition_url          .= "&search_client_details=$search_client_details&search_client=$search_client";
        $_SEARCH["search_client_details"]  = $search_client_details;
        $_SEARCH["search_client"]     = $search_client;       
	
	}
	//Client serach eof
    
    //Bank/CAs serach bof
	$search_vendor_bank_id = isset($_POST["search_vendor_bank_id"])   ? $_POST["search_vendor_bank_id"]  : (isset($_GET["search_vendor_bank_id"])   ? $_GET["search_vendor_bank_id"]   :'');
	$search_vendor_bank_details = isset($_POST["search_vendor_bank_details"])   ? $_POST["search_vendor_bank_details"]  : (isset($_GET["search_vendor_bank_details"])   ? $_GET["search_vendor_bank_details"]   :'');
	
	if(!empty($search_vendor_bank_id)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id ='". $search_vendor_bank_id ."' ";        
        $condition_url  .= "&search_vendor_bank_details=$search_vendor_bank_details&search_vendor_bank_id=$search_vendor_bank_id";
        $_SEARCH["search_vendor_bank_id"]  = $search_vendor_bank_id;
        $_SEARCH["search_vendor_bank_details"]     = $search_vendor_bank_details;       
	
	}
	//Bank/CAs  serach eof
    //Executive serach bof
	$search_executive_id = isset($_POST["search_executive_id"])   ? $_POST["search_executive_id"]  : (isset($_GET["search_executive_id"])   ? $_GET["search_executive_id"]   :'');
	$search_executive_details = isset($_POST["search_executive_details"])   ? $_POST["search_executive_details"]  : (isset($_GET["search_executive_details"])   ? $_GET["search_executive_details"]   :'');
	
	if(!empty($search_executive_id)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".executive_id ='". $search_executive_id ."' ";        
        $condition_url  .= "&search_executive_details=$search_executive_details&search_executive_id=$search_executive_id";
        $_SEARCH["search_executive_id"]  = $search_executive_id;
        $_SEARCH["search_executive_details"] = $search_executive_details;       
	
	}
	//Executive  serach eof
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
    // BO: From Date
    if ( ( $chk_date_from == 'AND' || $chk_date_from == 'OR' ) && !empty($date_from)) {
        $searchStr = 1;
		if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".". $dt_field." >= '". $dfa ."'";
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]  = $date_from;
		
    }   
    // EO: From Date
    if(!empty($dt_field)){
		$condition_url  .= "&dt_field=$dt_field";
		$_SEARCH["dt_field"]   = $dt_field;
	}
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        $searchStr = 1;
		if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_PAYMENT_TRANSACTION .".".$dt_field." <= '". $dta ."'";
         $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]   = $date_to ;
    }
   
    // EO: Upto Date
  
    
    
    
    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/payment-transaction-list.php');
?>
