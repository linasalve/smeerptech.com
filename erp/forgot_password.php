<?php

	if(!@constant("THIS_DOMAIN")){
		require_once("../lib/config.php"); 
	} 

	include(NERVE_CENTER_FS ."/header.php");
	
	if(isset($_POST["submit"]) && $_POST["submit"] == "Send Password"){
		if($_POST["fname"] == ""){
			$error["empty_user"] = 1 ;
		}
		if($_POST["email"] == ""){
			$error["email_empty"] = 1 ;
		}
		else{
			if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", $_POST["email"])) {
				$error["email_invalid"] = 1 ;
			}
		}

		if(empty($error)){
			
			/*	To check if the user is registered or not  */
			$query = "SELECT "	. TABLE_USER .".uid, "
								. TABLE_USER .".fname, "
								. TABLE_USER .".lname, "
								. TABLE_USER .".email "
						." FROM ". TABLE_USER 
						." WHERE ". TABLE_USER .".fname = '". $_POST["fname"] ."'"
						." AND ". TABLE_USER .".email = '". $_POST["email"] ."'" ;
						
			$db->query($query);
			if($db->nf() > 0 ){
				$db->next_record() ;
				$user = array(	"uid" 		=> $db->f("uid"),
								"fname" 	=> $db->f("fname"),
								"lname" 	=> $db->f("lname"),
								"email" 	=> $db->f("email")
							) ;
				$new_pass =  $user["username"] . rand(5, 125) ;
				$query = "UPDATE ". TABLE_USER 	." SET password = '". md5($new_pass) ."' "
												." WHERE uid = '". $user["uid"] ."'" ; 
				if(!$db->query($query)){
					$error["password_not_set"] = 1 ;
					$variables["show"] = "form" ;
				}
				else{
					
					$to[] = array(	"email" => $user["email"],
									"name" => $user["fname"] ." ". $user["lname"]
								);
					
					$from["name"] 	= $donotreplyname ;
					$from["email"] 	= $donotreply ;
													
					$subject 	= translate_email("new_password_sub") ; 
					$greet 		= translate_email("greet", $user["fname"] ." ". $user["lname"]) ; 
					$link 		= NERVE_CENTER ."/" ;
					$message 	= translate_email("new_password", $new_pass, $link, "admin@pretechno.com") ; 
					$footer 	= translate_email("general_footer", "Support Department") ; 
				
					$c_message = $greet . $message . $footer ;
				
					if(SendMail($to, $from, $from, $subject, $c_message, $sendAs, false, $cc, $bcc)){
						$variables["show"] = "success" ;
					}
					else{
						$error["password_not_set"] = 1 ;
						$variables["show"] = "form" ;					
					}							
				}
			}		
			else{
				$error["not_found"] = 1 ;
				$variables["show"] = "form" ;
			}
		}
		else{
			$variables["show"] = "form" ;
		}
	}
	
	if($variables["show"] <> "form" && $variables["show"] <> "success"){
		$variables["show"] = "form" ;
	}
		
		
	$s->assign("variables",$variables);
	$s->assign("error",$error);
	
/*	$s->assign('CONTENT',$s->fetch("forgot_password.html"));
	$s->display('index.html') ;
*/	
	$s->display('forgot_password.html') ;
	
?>