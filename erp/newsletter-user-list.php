<?php

   // if ( $perm->has('nc_ue_list') ) {
        
        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_USER   =>  array(   
                                                            'Number'        => 'number',
                                                            'User Name'     => 'username',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Middle Name'   => 'm_name',
                                                            'Last Name'     => 'l_name',
                                                            'Pet Name'      => 'p_name',
                                                            'Designation'   => 'desig',
                                                            'Organization'  => 'org',
                                                            'Domain'        => 'domain'
                                                        )
                            );
        
        $sOrderByArray  = array(
                                TABLE_USER => array('Number'        => 'number',
                                                    'User Name'     => 'username',
                                                    'E-mail'        => 'email',
                                                    'First Name'    => 'f_name',
                                                    'Last Name'     => 'l_name',
                                                    'Date of Birth' => 'do_birth',
                                                    'Date of Regis.'=> 'do_reg',
                                                    'Date of Login' => 'do_login',
                                                    'Status'        => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_reg';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_USER;
        }
   
        /*
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
        }
        else {
            $condition_query .= ' AND (';
        }
        $access_level   = $my['access_level'];
        
        // If the User has the Right to View Clients of the same Access Level.
        if ( $perm->has('nc_ue_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " (". TABLE_USER .".access_level < $access_level ) ";
        $condition_query .= ')';
        */
		if(!empty($order_by_table))
        	$condition_query1 .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	User::getList( $db, $list, '', $condition_query1);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        User::getList( $db, $list, '*', $condition_query1, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_u_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_u_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_u_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_u_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_u_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_u_status') ) {
            $variables['can_change_status']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletter-user-list.html');
    /*}
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }*/
?>