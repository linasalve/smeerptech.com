<?php
    if ( $perm->has('nc_itm_p_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		include_once (DIR_FS_INCLUDES .'/items-type.inc.php');	
		include_once (DIR_FS_INCLUDES .'/items-purchase.inc.php');	
		
		$condition_query = "WHERE ".TABLE_ITEMS_TYPE.".status='".ItemsType::ACTIVE."'";
		$fields="*";
        ItemsType::getDetails( $db, $item_type_list, $fields, $condition_query);
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
            
            if ( ItemsPurchase::validateAdd($data, $extra) ) { 
		 
				if(!empty($data['warranty_dt'])){
                    $data['warranty_dt'] = explode('/', $data['warranty_dt']);
                    $data['warranty_dt'] = mktime(0, 0, 0, $data['warranty_dt'][1], $data['warranty_dt'][0], $data['warranty_dt'][2]);
                    $data['warranty_dt'] = date('Y-m-d H:i:s', $data['warranty_dt']);
                }
				if(isset($data['in_used'])){
					$in_used =1 ;
				}else{
					$in_used =0 ;
				}
				if(isset($data['is_sold'])){
					$is_sold =1 ;
				}else{
					$is_sold =0 ;
				}
				$family_linking='';
				
				if(!empty($data['family_linking'])){
					$family_linking = implode(",",$data['family_linking']);
					$family_linking = ",".$family_linking.",";
				}
				
					  $query	= " INSERT INTO ".TABLE_ITEMS_PURCHASE
							." SET ".TABLE_ITEMS_PURCHASE .".item_name = '". 		$data['item_name'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".item_type_id = '".$data['item_type_id'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".item_description = '".$data['item_description'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".in_used = '".$in_used ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".is_sold = '".$is_sold ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".sold_to_name = '".$data['sold_to_name'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".code = '".$data['code'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".model_no = '".$data['model_no'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".serial_no = '".$data['serial_no'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".batch_no = '".$data['batch_no'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".brand = '".$data['brand'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".warranty_dt = '".$data['warranty_dt'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".vendor_id = '".$data['vendor_id'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".pp_bill_id = '".$data['pp_bill_id'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".pp_bill_description = '".$data['pp_bill_description'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".price = '".$data['price'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".location = '".$data['location'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".family_linking = '".$family_linking ."'" 							
							//.",". TABLE_ITEMS_PURCHASE .".status = '". 		$data['status'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".created_by = '".     $my['user_id'] ."'"
							.",". TABLE_ITEMS_PURCHASE .".created_by_name = '".$my['f_name']." ".$my['l_name']."'"
							.",". TABLE_ITEMS_PURCHASE .".do_e   = '". 		date('Y-m-d H:i:s')."'";
              
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
          
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/items-purchase.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/items-purchase.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/items-purchase.php?added=1");   
        }
        else {
        
			$variables['INUSED'] = ItemsPurchase::INUSED ;
			$variables['DEAD'] = ItemsPurchase::DEAD ;
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["var"][] = array('variable' => 'item_type_list', 'value' => 'item_type_list');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'items-purchase-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
