<?php
 if ( $perm->has('nc_or_bd_add') ||  $perm->has('nc_or_bd_orlist')) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
		
        $order_id = isset($_GET["order_id"])  ? $_GET["order_id"]  : ( isset($_POST["order_id"])  ? $_POST["order_id"] :'');
		
		//Get Order Details BOF
		$table = TABLE_BILL_ORDERS;
		$condition2 = " WHERE ".TABLE_BILL_ORDERS.".id = '".$order_id ."' LIMIT 0,1 " ;
		$fields1 =  TABLE_BILL_ORDERS.'.order_title';
		$orderDetails = getRecord($table,$fields1,$condition2);    
		//Get Order Details EOF
		$total_amt = 0;
		$variables['showbidform']=1;
		$_ALL_POST_DETAILS=$list= array();
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        $condition1 = " WHERE ".TABLE_USER.".status='".User::ACTIVE."' 
		ORDER BY ".TABLE_USER.".f_name";    //AND ".TABLE_USER.".current_staff='1'     
        User::getList($db,$lst_executive,$fields,$condition1);
		//$_ALL_POST['transaction_type'] = Paymenttransaction::PAYMENTIN;
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
				$_ALL_POST 	= $_POST;
				$data		= processUserData($_ALL_POST);
           
				$extra = array( 'db' 				=> &$db,
                           	'messages'          => &$messages 
                        );
            
   
				if ( OrderBid::validateAdd($data,$extra) ) {
			
					$query	= " INSERT INTO ".TABLE_ORDER_BID
							." SET ".TABLE_ORDER_BID .".order_id = '". $data['order_id'] ."'"  
							.",". TABLE_ORDER_BID .".created_by = '". $my['user_id'] ."'"
							.",". TABLE_ORDER_BID .".created_by_name = '". $my['f_name']." ".$my['l_name'] ."'"
							.",". TABLE_ORDER_BID .".status = '".OrderBid::PENDING ."'"
							.",". TABLE_ORDER_BID .".access_level = '". $my['access_level'] ."'"
							.",". TABLE_ORDER_BID .".do_e = '".date('Y-m-d h:i:s')."'" ;
                
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {	
						
						$variables['hid'] =$data['ord_bid_id']= $db->last_inserted_id(); 
                        $messages->setOkMessage("Record has been added successfully.");
                       
                        if(isset($data['team_member'])){
							  $data['query_p'] = 'INSERT INTO '. TABLE_ORDER_BID_DETAILS .' (ord_bid_id, order_id, 
							  team_member, team_member_name,remarks,hrs, hrs1, current_salary, per_hour_salary, 
							  projected_hour_salary, do_e) VALUES ';
							
							foreach ( $data['team_member'] as $key=>$particular ) {								
								if ( !empty($data['team_member'][$key]) || !empty($data['hrs'][$key]) ) {
									
									$table = TABLE_USER;
									$condition2 = " WHERE ".TABLE_USER .".user_id = '".$data['team_member'][$key] ."' LIMIT 0,1 " ;
									$fields1 =  TABLE_USER.'.current_salary,'. TABLE_USER.'.f_name,'. TABLE_USER .'.l_name';
									$exeCreted = getRecord($table,$fields1,$condition2);                
									$currentSalary = $data['current_salary'][$key] = $exeCreted['current_salary'] ;				
									$data['name'][$key] = $exeCreted['f_name']." ".$exeCreted['l_name'] ;
									$data['per_hour_salary'][$key] = round($currentSalary / STD_WORKING_HRS);
									//$data['per_hour_salary'][$key] =  number_format($data['per_hour_salary'][$key],2);
								 
								 
									$data['projected_hour_salary'][$key] = ( $data['per_hour_salary'][$key] * $data['hrs'][$key]);									  $data['hrs1'][$key]  = ( 3 * $data['hrs'][$key] );
									 
									$is_part = true;  
									$data['query_p'] .= "('". $data['ord_bid_id'] ."', '". $data['order_id'] ."', 
										'". $data['team_member'][$key] ."', 
										'". processUserData(trim($data['name'][$key])) ."', 
										'". processUserData(trim($data['remarks'][$key])) ."', 
										'". $data['hrs'][$key] ."','". $data['hrs1'][$key] ."',
										'". $data['current_salary'][$key] ."','".$data['per_hour_salary'][$key]."',
										'". processUserData($data['projected_hour_salary'][$key]) ."','". date('Y-m-d h:i:s')."' 
										)," ;
								}
							}             
							if(!empty($data['query_p'])){
								$data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
							
								if ( !$db->query($data['query_p']) ) {
									$messages->setErrorMessage("The Particulars were not Saved.");
								}
							}
						}		
                    }
					
					$db1 		= new db_local; // database handle
					if(!empty($order_id)){
						 
						$variables['showbidform']=0; 
						if($perm->has('nc_or_bd_orlist')){
							$sql = " SELECT ".TABLE_ORDER_BID.".* FROM ".TABLE_ORDER_BID." WHERE 
							".TABLE_ORDER_BID.".order_id=".$order_id;
						}else{
							$sql = " SELECT ".TABLE_ORDER_BID.".* FROM ".TABLE_ORDER_BID." WHERE 
							".TABLE_ORDER_BID.".order_id=".$order_id." AND 
							".TABLE_ORDER_BID.".created_by='".$my['user_id']."'";
						}
						$db->query($sql);				
						if ( $db->nf()>0 ){
							while ( $db->next_record()){ 
								$created_by =$db->f('created_by'); 
								$created_by_name =$db->f('created_by_name'); 
								$do_e =$db->f('do_e'); 
								$ord_bid_id =$db->f('id'); 
								$tot_projected_hour_salary =$tot_hrs=0;
								$temp_p = $bidDetails = NULL;			 
								$condition_query_p = " WHERE ".TABLE_ORDER_BID_DETAILS.".ord_bid_id = '".$ord_bid_id ."'";
								OrderBid::getParticulars($db1, $temp_p, '*', $condition_query_p);
								if(!empty($temp_p)){					 
								   foreach ( $temp_p as $pKey => $parti ) { 
									$bidDetails[$pKey]['projected_hour_salary']	= $temp_p[$pKey]['projected_hour_salary']  * SALARY_FACTOR;
									$tot_projected_hour_salary =  $tot_projected_hour_salary + $bidDetails[$pKey]['projected_hour_salary'];
									$tot_hrs = $tot_hrs + $temp_p[$pKey]['hrs']	; 
									$bidDetails[$pKey]['hrs']  = $temp_p[$pKey]['hrs'];
									$bidDetails[$pKey]['team_member_name']  = $temp_p[$pKey]['team_member_name'];
									$bidDetails[$pKey]['remarks'] = $temp_p[$pKey]['remarks'] ;
									
								   }
								}
								$list[$created_by][] =  array("bid_details" =>$bidDetails,
																"tot_projected_hour_salary" =>$tot_projected_hour_salary,
																"tot_hrs" =>$tot_hrs,
																"created_by_name"=>$created_by_name,
																"do_e"=>$do_e
																		
															);
							   
							}
						}
					 
					 }
                   $_ALL_POST=null;
            }
                
        }else{
			
			$_ALL_POST['team_member']=array('0'=>'');
			$fields = TABLE_ORDER_BID .'.*'  ;            
            $condition_query = " WHERE (". TABLE_ORDER_BID .".order_id = '". $order_id ."' AND  
				".TABLE_ORDER_BID.".created_by='".$my['user_id']."')";
            
            if ( OrderBid::getDetails($db, $_ALL_POST_DETAILS, $fields, $condition_query) > 0 ) {
                $_ALL_POST_DETAILS = $_ALL_POST_DETAILS['0'];	
				$variables['showbidform']=0; 
			}
			$db1 		= new db_local; // database handle
			if(!empty($order_id)){
				if($perm->has('nc_or_bd_orlist')){
					$sql = " SELECT ".TABLE_ORDER_BID.".* FROM ".TABLE_ORDER_BID." WHERE ".TABLE_ORDER_BID.".order_id=".$order_id;
				}else{
					$sql = " SELECT ".TABLE_ORDER_BID.".* FROM ".TABLE_ORDER_BID." WHERE 
					".TABLE_ORDER_BID.".order_id=".$order_id." AND ".TABLE_ORDER_BID.".created_by='".$my['user_id']."'";
				
				}
				$db->query($sql);				
				if ( $db->nf()>0 ){
					while ( $db->next_record()){ 
					    $created_by =$db->f('created_by'); 
					    $created_by_name =$db->f('created_by_name'); 
					    $do_e =$db->f('do_e'); 
					    $ord_bid_id =$db->f('id'); 
					    $tot_projected_hour_salary =$tot_hrs=0;
					    $temp_p = $bidDetails = NULL;			 
						$condition_query_p = " WHERE ".TABLE_ORDER_BID_DETAILS.".ord_bid_id = '".$ord_bid_id ."'";
						OrderBid::getParticulars($db1, $temp_p, '*', $condition_query_p);
					    if(!empty($temp_p)){					 
						   foreach ( $temp_p as $pKey => $parti ) { 
							$bidDetails[$pKey]['projected_hour_salary']	= $temp_p[$pKey]['projected_hour_salary']  * SALARY_FACTOR;
							$tot_projected_hour_salary =  $tot_projected_hour_salary + $bidDetails[$pKey]['projected_hour_salary'];
							$tot_hrs = $tot_hrs + $temp_p[$pKey]['hrs']	; 
							$bidDetails[$pKey]['hrs']  = $temp_p[$pKey]['hrs'];
							$bidDetails[$pKey]['team_member_name']  = $temp_p[$pKey]['team_member_name'];
							$bidDetails[$pKey]['remarks'] = $temp_p[$pKey]['remarks'] ;
							
						   }
						}
						$list[$created_by][] =  array("bid_details" =>$bidDetails,
														"tot_projected_hour_salary" =>$tot_projected_hour_salary,
														"tot_hrs" =>$tot_hrs,
														"created_by_name"=>$created_by_name,
														"do_e"=>$do_e
													);
					   
					}
				}
			 
			 }			
		}
        
        
		$variables['can_view_amt']=false;
		$variables['can_list_orbid']=true;
		$variables['can_add_bid']=false;
		
		
		if($perm->has('nc_or_bd_view_amt')){
			$variables['can_view_amt']=true;
		}
		if($perm->has('nc_or_bd_add')){
			$variables['can_add_bid']=true;
		}
       /*  if($perm->has('nc_or_bd_orlist')){
			$variables['can_list_orbid']=true;
		} */
		
        /*  
		if(isset($_POST['btnCreate'])  && $messages->getErrorMessageCount() <= 0){
             header("Location:".DIR_WS_NC."/payment-transaction.php?perform=add&added=1&number=".$data['number']);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-transaction.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {           
            //include ( DIR_FS_NC .'/payment-transaction-list.php');
            header("Location:".DIR_WS_NC."/payment-transaction.php?added=1&number=".$data['number']);            
        }
        else { */    
		
		
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'ajx' ,'value' => $ajx);
            $hidden[] = array('name'=> 'order_id' ,'value' => $order_id);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
  
            $page["var"][] = array('variable' => '_ALL_POST_DETAILS', 'value' => '_ALL_POST_DETAILS');      
            $page["var"][] = array('variable' => 'list', 'value' => 'list');      
            $page["var"][] = array('variable' => 'total_amt', 'value' => 'total_amt');      
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');      
            $page["var"][] = array('variable' => 'orderDetails', 'value' => 'orderDetails');      
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'order-bid-add.html');
       // }
    }
    else {
         $messages->setErrorMessage("You donot have the Permisson to Access this module.");
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
    }
?>
