<?php

		$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
		
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
         
        }
		$_SEARCH['searched']=1;
		
        // To count total records.
        $list	= 	NULL;
       
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  .= $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $list	= NULL;
        $query = "SELECT COUNT(*) as count FROM ". TABLE_LOGIN_STATS .$condition_query;							
		$db->query($query);
		while ( $db->next_record() ) {
			$total = $db->f("count");
		}
		$query = "SELECT * FROM ". TABLE_LOGIN_STATS .$condition_query;		
		$extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        //$pagination = showPagination($total, $x, $rpp, "x", $extra_url);
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        $query .= " LIMIT $next_record, $rpp";
        $db->query( $query );
		while ( $db->next_record() ) {
			
			$log_stat;
			
			$login_status = $db->f("login_status");
			$terms = $db->f("terms");
			
			if($login_status==0){
				$log_stat="Failure";
			}elseif($login_status==1){
				$log_stat="Success";
			}elseif($login_status==2){
                $log_stat="Invalid";
            }
			if($terms==1){
				$terms="Agree";
			}elseif($terms==0){
				$terms="Disagree";
			}
			$list[] = array("id"=> $db->f("id"),
							"username"=> $db->f("username"),
							"password"=> $db->f("password"),
							"date"	=> $db->f("date"),
							"terms"	=> $terms,
							"ip"	=> $db->f("ip"),
							"login_status"	=> $log_stat);
		}	
   
        // Set the Permissions.
       $variables['can_view_list']     = true;
       $variables['can_add']           = true;
       $variables['can_edit']          = true;
       $variables['can_delete']        = true;
       $variables['can_view_details']  = true;
         
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'total', 'value' => 'total');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'login-stats-list.html');
    
?>
