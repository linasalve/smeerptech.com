<?php
    if ( $perm->has('nc_al_delete') ) {
        $al_id	= isset($_GET["al_id"]) ? $_GET["al_id"] : ( isset($_POST["al_id"]) ? $_POST["al_id"] : '' );
        
        $extra = array( 'db' 		=> &$db,
                        'messages'  => &$messages
                    );
        AccessLevel::delete($al_id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/settings-access-level-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>