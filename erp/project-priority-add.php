<?php
  if ( $perm->has('nc_pts_pr_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( ProjectPriority::validateAdd($data, $extra) ) { 
                            $query	= " INSERT INTO ".TABLE_PROJECT_TASK_PRIORITY
                                    ." SET ".TABLE_PROJECT_TASK_PRIORITY .".title = '". $data['title'] ."'"  
                                    .",". TABLE_PROJECT_TASK_PRIORITY .".order = '". 		$data['order'] ."'"                                
                                    .",". TABLE_PROJECT_TASK_PRIORITY .".details = '". 		$data['details'] ."'"                                
                                	.",". TABLE_PROJECT_TASK_PRIORITY .".status = '". 		$data['status'] ."'"                                
                                    .",". TABLE_PROJECT_TASK_PRIORITY .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
                                    .",". TABLE_PROJECT_TASK_PRIORITY .".created_by     = '". 		$my['user_id'] ."'"                                
                                	.",". TABLE_PROJECT_TASK_PRIORITY .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New project priority entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
       if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/project-priority.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/project-priority.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/project-priority.php?added=1");
        }else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-priority-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
