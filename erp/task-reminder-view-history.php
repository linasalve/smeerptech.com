<?php

    if ( $perm->has('nc_ts_view_h') ) {
		
	    $id = isset($_POST["id"])   ? $_POST["id"]  : (isset($_GET["id"])   ? $_GET["id"]   :'');
        
		$variables["completed"]         = Taskreminder::COMPLETED;
		$variables["pending"]           = Taskreminder::PENDING;
	
        // If the task is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_ts_view_h_al') ) {
            $access_level += 1;
        }
       
        $condition_query .= "   LEFT JOIN ".TABLE_JOBS." ON ".TABLE_JOBS.".id=".TABLE_TASK_REMINDER_HISTORY.".job_id
                                LEFT JOIN ".TABLE_BILL_ORDERS." ON ".TABLE_BILL_ORDERS.".id=".TABLE_TASK_REMINDER_HISTORY.".order_id
                                WHERE ".TABLE_TASK_REMINDER_HISTORY.".task_id='".$id."' ORDER BY ".TABLE_TASK_REMINDER_HISTORY.".do_u";
    
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        $extra_url  = '';    
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }        
        $condition_url .="&perform=".$perform;
        //echo $condition_query;
        // To count total records.
        $list	= 	NULL;
        $total	=	Taskreminder::getHistoryList( $db, $list, TABLE_TASK_REMINDER_HISTORY.'.id', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        //$extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        //echo $condition_query;
        $lst_type=array_flip(Taskreminder::getTypes());
        $list	= NULL;
        $fields = TABLE_TASK_REMINDER_HISTORY .'.*'.','.TABLE_JOBS.'.title as job_title'.','.TABLE_BILL_ORDERS.'.order_title';
        Taskreminder::getHistoryList( $db, $list, $fields, $condition_query, $next_record, $rpp);
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
               /*
               $executive=array();
               $string = str_replace(",","','", $val['allotted_to']);
               $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
               User::getList($db,$executive,$fields1,$condition1);
               $executivename='';
              
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['allotted_to'] = $executivename ;
               */
               $statusArr = Taskreminder::getStatus();
               $statusArr = array_flip($statusArr);
               $val['status'] = $statusArr[$val['status']];
               $priorityArr = Taskreminder::getPriority();
               $priorityArr = array_flip($priorityArr);
               $val['priority'] = $priorityArr[$val['priority']];
               $exeCreted=$exeCreatedname='';
               $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
               $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
               User::getList($db,$exeCreted,$fields1,$condition2);
              
               foreach($exeCreted as $key2=>$val2){
                    $exeCreatedname .= $val2['f_name']." ".$val2['l_name']."<br/>" ;
               } 
               $val['created_by_name'] = $exeCreatedname;             
               if($val['created_by']==$my['user_id']){
                    $val['can_editable'] = true;
                    $val['can_deletable'] = true;
                }
                $val['typename'] ='';
               if($val['type']!=0){               
                $val['typename'] =$lst_type[$val['type']];
               }
               $val['allotted_to_name']=trim($val['allotted_to_name'],',');
               $val['allotted_to_client_name']=trim($val['allotted_to_client_name'],',');
               $val['allotted_to_vendor_name']=trim($val['allotted_to_vendor_name'],',');
               $val['allotted_to_lead_name']=trim($val['allotted_to_lead_name'],',');
               $val['allotted_to_addr_name']=trim($val['allotted_to_addr_name'],',');
                
               /*
               $client=array();
               $string = str_replace(",","','", $val['allotted_to_client']);
               $fields3 =  TABLE_CLIENTS .'.f_name'.','.TABLE_CLIENTS .'.l_name';
               $condition3 = " WHERE ".TABLE_CLIENTS .".user_id IN('".$string."') " ;
               Clients::getList($db,$client,$fields3,$condition3);
               $clientname='';
              
               foreach($client as $key1=>$val1){
                    $clientname .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['allotted_to_client'] = $clientname ;
            
               $lead=array();
               $string = str_replace(",","','", $val['allotted_to_lead']);
               $fields4 =  TABLE_SALE_LEADS .'.f_name'.','.TABLE_SALE_LEADS .'.l_name';
               $condition4 = " WHERE ".TABLE_SALE_LEADS .".lead_id IN('".$string."') " ;
               Leads::getList($db,$lead,$fields4,$condition4);
               $leadname='';
              
               foreach($lead as $key1=>$val1){
                    $leadname .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
               } 
               $val['allotted_to_lead'] = $leadname ;
               
               $addr=array();
               $string = str_replace(",","','", $val['allotted_to_addr']);
               $fields5 =  TABLE_ADDRESS_BOOK .'.name';
               $condition5 = " WHERE ".TABLE_ADDRESS_BOOK .".id IN('".$string."') " ;
               Addressbook::getList($db,$addr,$fields5,$condition5);
               $addrname='';
              
               foreach($addr as $key1=>$val1){
                    $addrname .= $val1['name']."<br/>" ;
                } 
                $val['allotted_to_addr'] = $addrname ;
               
                $exeBehalf=array();
                $exeBehalfname='';
                $condition6 = " WHERE ".TABLE_USER .".user_id= '".$val['on_behalf_id']."' " ;
                User::getList($db,$exeBehalf,$fields1,$condition6);
              
                foreach($exeBehalf as $key6=>$val6){
                    $exeBehalfname .= $val6['f_name']." ".$val6['l_name']."<br/>" ;
                } 
                $val['on_behalf_of_name'] = $exeBehalfname; 
               */
               $fList[$key]=$val;
            }
        }
      
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
       
        if ( $perm->has('nc_ts_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_ts_add') ) {
            $variables['can_add'] = true;
        }
        
        /*
        if ( $perm->has('nc_ts_edit') ) {            
            $variables['can_edit'] = true;
        }
        */
        /*if ( $perm->has('nc_ts_delete') ) {
            $variables['can_delete'] = true;
        }*/
        if ( $perm->has('nc_ts_details') ) {
            $variables['can_view_details'] = true;
        }
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'task-reminder-view-history.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>