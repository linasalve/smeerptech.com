<?php
 
	include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	
	
	$_ALL_POST	= NULL;
	$data       = NULL;
	
  
	
	 	
	if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST 	= $_POST;
		$data		= processUserData($_ALL_POST);
		 
		$extra = array( 'db' 		=> &$db,
						'messages'  => $messages 
					);
	  
		if ( TeamJobFunctions::validateAdd($data, $extra) ) {
			 
			
			$query	= " INSERT INTO ". TABLE_TJ_FUNCTIONS
					." SET ". TABLE_TJ_FUNCTIONS .".title     	= '". $data['title'] ."'"
						.",". TABLE_TJ_FUNCTIONS .".description   = '". $data['description']."'"
						.",". TABLE_TJ_FUNCTIONS .".ip	= '".$_SERVER['REMOTE_ADDR'] ."'"
						.",". TABLE_TJ_FUNCTIONS .".added_by	= '". $my['uid'] ."'"
						.",". TABLE_TJ_FUNCTIONS .".added_by_name	= '". $my['f_name']." ".$my['l_name']."'"
						.",". TABLE_TJ_FUNCTIONS .".do_e      	= '". date('Y-m-d H:i:s', time()) ."'"
						.",". TABLE_TJ_FUNCTIONS .".status      	= '". $data['status'] ."'" ;
						
			if ($db->query($query) && $db->affected_rows() > 0) {
				 $variables['hid'] = $db->last_inserted_id();   
				 
				$data['query_p'] = 'INSERT INTO '. TABLE_TJ_FUNCTIONS_P .' (tjf_id,jtitle, jdescription) VALUES ';
						
				foreach ( $data['jtitle'] as $key=>$particular ) {
					
					$data['query_p'] .= "('". $variables['hid'] ."', 
					 '". processUserData(trim($data['jtitle'][$key],',')) ."', 
					 '". processUserData($data['jdescription'][$key]) ."' 
					)," ;
				 
				}
				
				if(!empty($data['query_p'])){
					$data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
				
					if ( !$db->query($data['query_p']) ) {
						$messages->setErrorMessage("The Particulars were not Saved.");
					}
				}
					$messages->setOkMessage("The New Job Function has been added.");
				
			}
			//to flush the data.
			$_ALL_POST	= NULL;
			$data		= NULL;
			$_ALL_POST['jtitle']=array('0'=>'');
		} 
	}else{
		$_ALL_POST['jtitle']=array('0'=>'');
	}
	
	
	// Check if the Form to add is to be displayed or the control is to be sent to the List page.
	/*
	if ( isset($_POST['btnCancel'])
		|| (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
		include ( DIR_FS_NC .'/clients-list.php');
	}*/
	if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
		header("Location:".DIR_WS_NC."/team-job-functions.php?perform=list&added=1");
		
	}
	else {		 
	   
		$hidden[] = array('name'=> 'perform','value' => 'add');
		$hidden[] = array('name'=> 'act', 'value' => 'save');
		
		$hidden[] = array('name'=> 'ajx','value' => $ajx);
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
		$page["var"][] = array('variable' => 'data', 'value' => 'data');
	
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'team-job-functions-add.html');
	}
  
?>