<?php
    if ( $perm->has('nc_p_pr_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		// Include the PLP Period class.
		include_once (DIR_FS_INCLUDES .'/plp-period.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
           
            $extra = array( 'db' 				=> &$db,
							'messages'          => &$messages
                        );
                
            if ( PlpPeriod::validateAdd($data, $extra) ) { 
               $query	= " INSERT INTO ".TABLE_PLP_PERIOD
                            ." SET ". TABLE_PLP_PERIOD .".plp_period_start = '".     $data['plp_period_start'] ."'"
                                .",". TABLE_PLP_PERIOD .".plp_period_end = '".       $data['plp_period_end'] ."'"
                                .",". TABLE_PLP_PERIOD .".plp_period_title = '".     $data['plp_period_title'] ."'"
                                .",". TABLE_PLP_PERIOD .".plp_period_status = '".    $data['plp_period_status'] ."'"
                              	.",". TABLE_PLP_PERIOD .".ip            = '". 		 $_SERVER['REMOTE_ADDR'] ."'"  
                              	.",". TABLE_PLP_PERIOD .".created_by = '".           $data['created_by'] ."'"                              
                                .",". TABLE_PLP_PERIOD .".do_e = '".                 date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/plp-period.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/plp-period.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/plp-period.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'plp-period-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
