<?php
    if ( $perm->has('nc_sl_pq_add') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        
        $pq_id          = isset($_GET["pq_id"]) ? $_GET["pq_id"] : ( isset($_POST["pq_id"]) ? $_POST["pq_id"] : '' );
        $lead_id        = isset($_GET["lead_id"]) ? $_GET["lead_id"] : ( isset($_POST["lead_id"]) ? $_POST["lead_id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_sl_pq_add_al') ) {
            $access_level += 1;
        }
		
		// Include the Work Stage class and Work Timeline class.
		/*include_once (DIR_FS_INCLUDES .'/work-stage.inc.php');
		include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');*/
		include_once (DIR_FS_INCLUDES .'/user.inc.php');
		include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
		/*$lst_work_stage = NULL;
		WorkStage::getWorkStages($db, $lst_work_stage);
        // list of executives to assign project manager
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields);*/
        
       
        
        //code to generate $vendorOptionList BOF
        /*$vendorSql ="SELECT * FROM ".TABLE_VENDOR ;
        $db->query( $vendorSql );
        if( $db->nf() > 0 )
		{
            $vendorList['0'] = "" ;
			while($db->next_record())
			{
                $vendorList[$db->f('id')] = $db->f('f_name')." ".$db->f('l_name') ;
            }
            
        }*/
        //code to generate $vendorOptionList EOF
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);
        
        // code for particulars bof 
        // Read the Services.
        include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
        include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
        
        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC";
        Services::getList($db, $lst_service, 'ss_id,ss_title,ss_punch_line, tax1_name, tax1_value ', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
                    $ss_id= $key['ss_id'];
                    $ss_title= $key['ss_title'];
                    $ss_punch_line= $key['ss_punch_line'];
                    $tax1_name = $key['tax1_name'];
                    $tax1_value = $key['tax1_value'];
                    $tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
                    
                    $keyNew['ss_id'] = $ss_id ; 
                    $keyNew['ss_title'] = $ss_title ; 
                    $keyNew['ss_punch_line'] = $ss_punch_line ; 
                    $keyNew['tax1_name'] = $tax1_name ; 
                    $keyNew['tax1_value'] = $tax1_value ; 
                    $keyNew['tax1_pvalue'] = $tax1_pvalue ; 
                    $lst_service[$val] = $keyNew;
            }
        }
        // code for particulars eof
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'lst_service'       => $lst_service,
							//'lst_work_stage'    => $lst_work_stage,
                            'messages'          => &$messages
                        );
            $data['do_e'] = time();
  
          
            if ( PreQuote::validateAdd($data, $extra) ) {
			
                 $query	= " INSERT INTO ".TABLE_SALE_PRE_QUOTE
                            ." SET ". TABLE_SALE_PRE_QUOTE .".number = '".         	$data['number'] ."'"
                                .",". TABLE_SALE_PRE_QUOTE .".lead_id = '".     	$data['lead_id'] ."'"
							    .",". TABLE_SALE_PRE_QUOTE .".created_by = '".     	$my['uid'] ."'"
                                .",". TABLE_SALE_PRE_QUOTE .".subject = '".     	$data['subject'] ."'"
                                //.",". TABLE_SALE_PRE_QUOTE .".filetext_1 = '".     	$data['filetext_1'] ."'"
                                //.",". TABLE_SALE_PRE_QUOTE .".filename_1 = '".     	$data['filename_1'] ."'"
								//.",". TABLE_SALE_PRE_QUOTE .".filetext_2 = '".     	$data['filetext_2'] ."'"
                                //.",". TABLE_SALE_PRE_QUOTE .".filename_2 = '".     	$data['filename_2'] ."'"
								//.",". TABLE_SALE_PRE_QUOTE .".filetext_3 = '".     	$data['filetext_3'] ."'"
                                //.",". TABLE_SALE_PRE_QUOTE .".filename_3 = '".     	$data['filename_3'] ."'"
								//.",". TABLE_SALE_PRE_QUOTE .".filetext_4 = '".     	$data['filetext_4'] ."'"
                                //.",". TABLE_SALE_PRE_QUOTE .".filename_4 = '".     	$data['filename_4'] ."'"
								//.",". TABLE_SALE_PRE_QUOTE .".filetext_5 = '".     	$data['filetext_5'] ."'"
                                //.",". TABLE_SALE_PRE_QUOTE .".filename_5 = '".     	$data['filename_5'] ."'"
                                .",". TABLE_SALE_PRE_QUOTE .".access_level = '".    $my['access_level'] ."'"
                                .",". TABLE_SALE_PRE_QUOTE .".status = '1' "
	                            .",". TABLE_SALE_PRE_QUOTE .".do_e = '".         	date('Y-m-d H:i:s') ."'" ;
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New Pre Quote has been created.");
                    //After insert, update order counter in 
                    updateCounterOf($db,'PQT');
                    
                    $variables['hid'] = $db->last_inserted_id();
                    
                    // Insert the Particulars.
                    if ( !$db->query($data['query_p']) ) {
                        $messages->setErrorMessage("The Particulars were not Saved.");
                    }
                    
                    // Mark the Pre Order as PROCESSED.
                    /*
                    if ( !(Order::setPreOrderStatus($db, $data['po_id'], Order::COMPLETED)) ) {
                        $messages->setErrorMessage("Pre Order was not marked as Processed.");
                    }
                    $po_id = NULL;
                    */
                    
					// Add the Record to the Work Timeline.
					/*$query  = " INSERT INTO ". TABLE_WORK_TL
								." SET ". TABLE_WORK_TL .".order_id		= '". $variables['hid'] ."'"
									.",". TABLE_WORK_TL .".order_no		= '". $data['number'] ."'"
									.",". TABLE_WORK_TL .".to_id		= '". $data['team'][0] ."'"
									.",". TABLE_WORK_TL .".by_id		= '". $my['user_id'] ."'"
									.",". TABLE_WORK_TL .".comments		= '". $data['work_comments'] ."'"
									.",". TABLE_WORK_TL .".do_assign	= '". date('Y-m-d H:i:s', time()) ."'"
									.",". TABLE_WORK_TL .".status   	= '". $data['work_stage'] ."'"
								;
					if ( !$db->query($query) ) {
						$messages->setErrorMessage("The comment was not entered into Work Timeline.");
					}*/
					
                    // Send the notification mails to the concerned persons.
                    /*include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_or_add', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_or_add_al', 'access_level'=>($data['access_level']-1) ),
                                                            // array('right'=>'nc_bl_in_add', 'access_level'=>$data['access_level']),
                                                            // array('right'=>'nc_bl_in_add_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );
                    
                    // Organize the data to be sent in email.
                    //$data['do_d']   = date('d M, Y', $data['do_d']);
                    //$data['do_c']   = date('d M, Y', $data['do_c']);
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];

                    // Read the Client Manager information.
                    include ( DIR_FS_INCLUDES .'/clients.inc.php');
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');
                    
                    // Send mail to the Concerned Executives with same access level and higher access levels
                    if(isset($data['mail_exec_ac'])){ 
                       
                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_OR_NEW_MANAGERS', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }
                    
                    // Send Email to the Client.
                    if(isset($data['mail_client'])){ 
                     
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_CLIENT', $data, $email) ) {
                       
                            $to     = '';
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }    
                    // Send Email to the Client Manager / creator email .
                    if(isset($data['mail_client_mgr'])){ 
                     
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_CLIENT_MANAGER', $data, $email) ) {
                            
                            $to     = '';
                            $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                    // Send Email to Team Members.
                    if(isset($data['mail_executive'])){    
                         
                        // For Speed notification is sent in one email to all the team members.
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_NEW_TEAM', $data, $email) ) {
                          
                            $to     = '';
                            foreach ( $data['team_members'] as $key=>$executive) {
                                $to[]   = array('name' => $executive['f_name'] .' '. $executive['l_name'], 'email' => $executive['email']);
                            }
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }*/
                    
                    // Send Email to the Creator of the Order.
                    /*
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'NOT REQUIRED, SO NOT DEFINED', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    */
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $_ALL_POST['number'] = getCounterNumber($db,'PQT'); 
                $_ALL_POST['exchange_rate'] = 1; 
                $data		= NULL;
            }
        }
        else {
            // Set up the default values to be displayed.
            //$_ALL_POST['number'] = "PT". date("Y") ."-OR-". date("mdhi-s") ;
            //As order-number formate changed on the basis of ( fanancial yr + sr no ) 
            $_ALL_POST['number'] = getCounterNumber($db,'PQT'); 
             $_ALL_POST['exchange_rate'] = 1; 
            
        }
 
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            include ( DIR_FS_NC .'/sale-pre-quote-list.php');
        }
        else {
           /*
            if ( !empty($po_id) ) {
                // Read the Pre Order details.
                include_once ( DIR_FS_INCLUDES .'/bill-pre-order.inc.php');
                $pre_order  = NULL;
                $fields     = 'id, '. TABLE_BILL_PO .'.access_level, '.TABLE_BILL_PO .'.attach_files, '. TABLE_BILL_PO .'.status, order_details, client_details';
                $fields     .= ', user_id, number, f_name, l_name';
                if ( PreOrder::getDetails($db, $pre_order, $fields, "WHERE id = '". $po_id ."' ") > 0 ) {
                    $pre_order = $pre_order[0];
                    
                    if ( $pre_order['status'] == ACTIVE ) {
                        if ( $access_level > $pre_order['access_level'] ) {
                            $_ALL_POST['po_id']             = $pre_order['id'];
                            $_ALL_POST['po_order_details']  = $pre_order['order_details'];
                            $_ALL_POST['po_client_details'] = $pre_order['client_details'];
                            $attach_files                   = $pre_order['attach_files'];
                            $files=array();
                            $filenames='';
                            if(!empty($attach_files)){
                                $files = explode(",",$attach_files);
                                $i=1;                           
                                foreach($files as $key1=>$val1){
                                    if(!empty($val1)){
                                                                               
                                        $filenames .= "<a href=javascript:void(0); onclick=javascript:showFileAttachment('$val1') title='View file' class='file'>file ".$i."</a><br/>" ;
                                        $i++;
                                    }
                                } 
                            }
                            $_ALL_POST['files'] = $filenames ;
                            $_ALL_POST['po_user_id']        = $pre_order['user_id'];
                            $_ALL_POST['po_number']         = $pre_order['number'];
                            $_ALL_POST['po_f_name']         = $pre_order['f_name'];
                            $_ALL_POST['po_l_name']         = $pre_order['l_name'];
                            $_ALL_POST['po_access_level']   = $pre_order['access_level'];
                            
                            $pre_order = NULL;
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                            $_ALL_POST = '';
                        }
                    }
                    else {
                        $messages->setErrorMessage("The Pre Order is not yet approved.");
                        $_ALL_POST = '';
                    }
                }
                else {
                    $messages->setErrorMessage("The Selected Pre Order was not found.");
                }
            }
            */
            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['pq_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['pq_access_level'], $al_list, $index);
                $_ALL_POST['pq_access_level'] = $al_list[$index[0]]['title'];
            }
            
            $hidden[] = array('name'=> 'pq_id' ,'value' => $pq_id);
            $hidden[] = array('name'=> 'lead_id' ,'value' => $lead_id);
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			//$page["var"][] = array('variable' => 'lst_work_stage', 'value' => 'lst_work_stage');
			//$page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            
            
            //$page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
			$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            //$page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-pre-quote-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>