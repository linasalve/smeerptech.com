<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include ( DIR_FS_INCLUDES .'/sale-industry.inc.php');
	include_once ( DIR_FS_CLASS .'/Region.class.php');
    include_once ( DIR_FS_CLASS .'/Phone.class.php');
    
	$sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
    $added 		= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $sendinv    = isset($_GET["sendinv"])         ? $_GET["sendinv"]        : ( isset($_POST["sendinv"])          ? $_POST["sendinv"]       :'0');
    $x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $condition_url='';
	$rpp 		=100;
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if($added){
         $messages->setOkMessage("Client entry has been done.");
    }
    if($sendinv){
         $messages->setOkMessage("Invitation to client has been sent successfully.");
    }
    
    $searchLink=0;
    if ( $perm->has('nc_c_all') ) { // 

        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_CLIENTS   =>  array(  'Relationship Number'        => 'number',
                                                            'Billing Name'  => 'billing_name',
                                                            'User Name'     => 'username',
                                                            'E-mail'        => 'email',
                                                            'E-mail 1'      => 'email_1',
                                                            'E-mail 2'      => 'email_2',
                                                            'First Name'    => 'f_name',
                                                            'Middle Name'   => 'm_name',
                                                            'Last Name'     => 'l_name',
                                                            'Pet Name'      => 'p_name',
                                                            'Designation'   => 'desig',
															'Mobile 1'      => 'mobile1',
                                                            'Mobile 2'      => 'mobile2',
                                                           // 'Organization'  => 'org',
                                                            'cst_no'  => 'cst_no',
                                                            'cst_no_details'  => 'cst_no_details',
                                                            'vat_no'  => 'vat_no',
                                                            'vat_no_details'  => 'vat_no_details',
                                                            'pan_no'  => 'pan_no',
                                                            'pan_no_details'  => 'pan_no_details',
                                                            'service_tax_no'  => 'service_tax_no',
                                                            'st_no_details'  => 'st_no_details',
                                                            'Bank1 Details'  => 'bank1_details',
                                                            'Bank2 Details'  => 'bank2_details',
                                                            'Search keywords'  => 'search_keywords',
                                                            'Company details'  => 'company_details',
                                                            'Connected via'  => 'connected_via',
                                                            'Remarks'  => 'remarks',
                                                            'Special Remarks'  => 'special_remarks',
                                                            'Payment Remarks'  => 'payment_remarks',
                                                            'Domain'        => 'domain'
                                                        ),
								TABLE_PHONE  =>  array(  'Phone Number'    => 'p_number' 
													),
								TABLE_ADDRESS  =>  array(  'Address'    => 'address'													 
													)
                            );							
        
        $sOrderByArray  = array(
                                TABLE_CLIENTS => array(
									'Relationship Number' => 'number',
									'User Name'     => 'username',
									'E-mail'        => 'email',
									'First Name'    => 'f_name',
									'Last Name'     => 'l_name',
									'Date of Birth' => 'do_birth',
									'Date of Regis.'=> 'do_reg',
									'Date of Login' => 'do_login',
									'Status'        => 'status'
								),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'number';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_CLIENTS;
        }
    
        $variables['title_type'] = Clients::getTitleType();
        $variables['status'] = Clients::getStatus();
        $variables['grades'] = Clients::getGrades();
        //use switch case here to perform action. 
        switch ($perform) {           
            case ('search'): {
                $searchLink=1;
                include(DIR_FS_NC."/clients-all-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'clients-all.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list'):
            default: {
                $searchStr = 1;
                include (DIR_FS_NC .'/clients-all-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'clients-all.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'clients-all.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>