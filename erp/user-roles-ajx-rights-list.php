<?php
	 
	if(!defined( "THIS_DOMAIN")) {
		require("../lib/config.php");
	}
	$ajx 		= isset($_GET["ajx"])       ? $_GET["ajx"]      : ( isset($_POST["ajx"])        ? $_POST["ajx"]     : 0  );// Result per page
    $rpp1        = isset($_GET["rpp1"]) ? $_GET["rpp1"] : ( isset($_POST["rpp1"]) ? $_POST["rpp1"] : 5);// Result per page
	$x          = isset($_GET["x"]) ? $_GET["x"] : (isset($_POST["x"]) ? $_POST["x"]:'');
	$role_id          = isset($_GET["role_id"]) ? $_GET["role_id"] : (isset($_POST["role_id"]) ? $_POST["role_id"]:'');
	
	if (empty($x) ) {
        $x              = 1;
        $next_record1    = 0 ;
    }
    else {
        $next_record1    = ($x-1) * $rpp1;
    }

    $variables["x"]     = $x;
    $variables["rpp1"]   = $rpp1;
	
	if($ajx==1){
		page_open(array("sess" => "NC_Session",
						"auth" => "NC_Auth",
						"perm" => "NC_Perm"
					   ));
		
		include ( DIR_FS_NC ."/header.php");
		include ( DIR_FS_INCLUDES .'/user-roles.inc.php');
	}
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	
    if ( $perm->has('nc_urol_add') ) { 
        /*  
		$data		= NULL;
        $extra      = NULL;
        $_ALL_POST	= NULL;
        $chk_output	= NULL;
        $chk_rights	= NULL;
        $rights_array = NULL; */
        $al_list    = getAccessLevel($db, $my['access_level']); 
		/* if(!empty($slct_rts)){
			$data['chk_rights']	= explode($slct_rts,",");
		} */
		//read access leveles
        //$userrights_list		= NULL;
        if(!empty($role_id)){
			$condition_query    = " WHERE id = '". $role_id ."' ";
			$data               = NULL;
			if( UserRoles::getList($db, $data, '*', $condition_query) > 0){
				$data                  =   $data['0']; 
				$data['chk_rights']    = explode(",", $data['rights']); 
			} 
		}
		$list	= 	NULL;
		$condition = " WHERE parent_id = 0 ";
        $total	= UserRoles::getUserRightsList( $db, $list, '', $condition);
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        //$extra_url  .= "&x1=$x1&rpp1=$rpp1";
        //$extra_url  = '&start=url'. $extra_url .'&end=url';
        $condition_url .="&rpp1=".$rpp1."&perform=".$perform;    
		$extra = "'role_id=".$role_id."'" ;
        //$pagination = showPaginationAjaxWithExtra($total, $x, $rpp1, $condition_url,'changePageWithExtra');
		$pagination = ajaxPagination2($total, $x, $rpp1, 'rightsRecordList','user-roles-ajx-rights-list.php','frmUserRoleAdd',$extra);
        //$list	= NULL;
        //UserRights::getList( $db, $list, 'id, parent_id, title', $condition_query, $next_record, $rpp);
		$userrightsParent_list	= NULL;
        UserRoles::getUserRightsList( $db, $userrightsParent_list, 'id, parent_id, title', $condition,$next_record1, $rpp1);
        $rightList=array();
        if(!empty($userrightsParent_list)){
            foreach( $userrightsParent_list as $key=>$val){
                $userRtlist=null;
                $id = $val['id'];
                $parent_id = $val['parent_id'];
                $title= $val['title'];
                $condition = " WHERE parent_id = '".$id."'";
                UserRoles::getUserRights( $db, $userRtlist, 'id, parent_id, title', $condition);
               
                $rightList[]= array('id'=>$id,
                                    'parent_id'=>$parent_id,
                                    'title'=>$title,
                                    'child'=>$userRtlist
                                    ); 
			}
        }
		 
		$hidden2[] = array('name'=> 'x' , 'value' => $x);
		$hidden2[] = array('name'=> 'rpp1' , 'value' => $rpp1);


		$page["var"][] = array('variable' => 'hidden2', 'value' => 'hidden2');
		$page["var"][] = array('variable' => 'data', 'value' => 'data');
		$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
		$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
		$page["var"][] = array('variable' => 'rightList', 'value' => 'rightList');
		$page["var"][] = array('variable' => 'extra', 'value' => 'extra');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');

		$page["section"][] = array('container'=>'RIGHTS_LIST', 'page' => 'user-roles-ajx-rights-list.html');
		
		if ( isset($page["var"]) && is_array($page["var"]) ) {
			foreach ( $page["var"] as $key=>$fetch ) {
				$s->assign( $fetch["variable"], ${$fetch["value"]});
			}
		}
		if($ajx==1 ){		
			if(is_array($page["section"])){
				$sections = count($page["section"]);
				for ( $i=0; $i<($sections-1); $i++){ 
					$s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
				}
			}
			$s->display($page["section"][$sections-1]["page"]);
		}	
		
		
		
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>