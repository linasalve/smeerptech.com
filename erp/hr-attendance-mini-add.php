<?php
  if ( $perm->has('nc_hr_attm_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        
		$step1 = isset ($_GET['step1']) ? $_GET['step1'] : ( isset($_POST['step1'] ) ? $_POST['step1'] : '1');
        $step2 = isset ($_GET['step2']) ? $_GET['step2'] : ( isset($_POST['step2'] ) ? $_POST['step2'] : '0');
		
        
		$dateList=array();
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
			 
            $data		= processUserData($_ALL_POST);
                      
            $extra = array( 'db' 				=> &$db,
                         	'messages'          => &$messages
                        );
			
            if($data['step1']=='1'){    
				if ( HrAttendanceMini::validateMemberSelect($data, $extra) ) {
                    $step2 = 1;
                    $step1 = 0;
					$frDt  = $data['from_date1'];
					$toDt  = $data['to_date1'];
					$dateList = getDiffDates($frDt,$toDt);
					$_ALL_POST['dateList']=$dateList ;
					$dateCount = count($dateList);
					for($i=0;$i<$dateCount;$i++){
						$_ALL_POST['executive_details1'][] =$data['executive_details'];
						$_ALL_POST['executive_id1'][] =$data['executive_id'];
						
					}
				}
			}else{
					
				//$dateList = $data['dateList'];
				//$_ALL_POST['executive_details1'] =
				if ( HrAttendanceMini::validateAdd($data, $extra) ) { 
					
					if(!empty($data['dateList'])){
						foreach($data['dateList'] as $key=>$value){
							if(!empty($data['executive_id1'][$key]) && !empty($data['dateList'][$key]) 
							&& !empty($data['present_score'][$key])){

							$temp2 = explode('/', $data['dateList'][$key]);
							$data["cmbMonth"]	= $temp2[1];
							$data["cmbDay"]		= $temp2[0];
							$data["cmbYear"]  	= $temp2[2];
							$attendance_dt = $data["cmbYear"]."-".$data["cmbMonth"]."-".$data["cmbDay"]." 00:00:00";
								 
							$query = "SELECT  id FROM ". TABLE_HR_ATTENDANCE_MINI 
						   ." WHERE ".TABLE_HR_ATTENDANCE_MINI.".user_id = '".$data['executive_id1'][$key]."' 
						   AND 
						   date_format(".TABLE_HR_ATTENDANCE_MINI.".attendance_dt, '%Y-%m-%d') = '".$dt."'";
							$db->query($query);
							if ( $db->query($query) ){
								if ( $db->nf() > 0 ){
									$messages->setErrorMessage('Attendance Already marked for Selected Associate on Selected date. Check date '.$data['dateList'][$key]);
								}else{
								
									$query	= " INSERT INTO ".TABLE_HR_ATTENDANCE_MINI
								." SET ".TABLE_HR_ATTENDANCE_MINI .".user_id = '".$data['executive_id1'][$key]."'"  
								.",". TABLE_HR_ATTENDANCE_MINI.".user_name ='". $data['executive_details1'][$key]."'"
								.",". TABLE_HR_ATTENDANCE_MINI .".attendance_dt ='". $attendance_dt ."'"
								.",". TABLE_HR_ATTENDANCE_MINI .".present_score ='".$data['present_score'][$key]."'"    						 	.",". TABLE_HR_ATTENDANCE_MINI .".created_by ='".$my['uid'] ."'"  
							    .",".TABLE_HR_ATTENDANCE_MINI.".created_by_name = '".$my['f_name']." ".$my['l_name']."'"    						    .",". TABLE_HR_ATTENDANCE_MINI .".ip  ='". $_SERVER['REMOTE_ADDR'] ."'"                            	   .",". TABLE_HR_ATTENDANCE_MINI .".do_e = '". date('Y-m-d H:i:s')."'";
									$db->query($query)  ;
									
									$messages->setOkMessage('Row at positon '.$key1.' inserted successefully.');
								}
							}

							
							
						 }
					
					
						}
					}
				  
					 
				 
					//to flush the data.
					$_ALL_POST	= NULL;
					$data		= NULL;
				}
			}
        }               
		
		if(isset($_POST['btnCreate']) && $data['step1']!='1' && $messages->getErrorMessageCount() <= 0 ){
			 header("Location:".DIR_WS_NC."/hr-attendance-mini.php?perform=add&added=1");
        } 
        
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/hr-attendance-mini.php?perform=list");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        /* if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/hr-attendance-mini.php?added=1&perform=list");   
        }
        else { */
	        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $hidden[] = array('name'=> 'step1' , 'value' => $step1);           
            $hidden[] = array('name'=> 'step2' , 'value' => $step2);           
               
            
             
             
            $page["var"][] = array('variable' => 'step1', 'value' => 'step1');
            $page["var"][] = array('variable' => 'step2', 'value' => 'step2');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');  
			
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-attendance-mini-add.html');
        //}
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
