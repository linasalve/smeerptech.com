<?php
if ( $perm->has('nc_plp_ap_list') ) {

    $id    = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
    
    include_once ( DIR_FS_INCLUDES .'/calendar.inc.php' );
    //echo $condition_query;
    // To count total records.
    
    if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
        header("Location:".DIR_WS_NC."/plp-performance-appraisal.php?perform=edit2&id=".$id);
    }
    
    $list	= NULL;
    $fields = TABLE_PLP_USER_INFO.'.*'.','.TABLE_SETTINGS_CALENDAR.'.title'.','.TABLE_SETTINGS_CALENDAR.'.start_date'.','.
              TABLE_SETTINGS_CALENDAR.'.end_date';
    $condition_query = " LEFT JOIN  ".TABLE_SETTINGS_CALENDAR." ON ".TABLE_SETTINGS_CALENDAR.".id=".TABLE_PLP_USER_INFO.".calendar_id
                        WHERE ".TABLE_PLP_USER_INFO.".id ='".$id."'";
    PlpPerformanceAppraisal::getList( $db, $list, $fields, $condition_query);
    $list = $list['0'];
    
    $list['fromLastMonth']  = strtotime($list['start_date']);											  
	$list['toCurrentMonth'] = strtotime($list['end_date']);									
	$list['todayMonth']     = date("F",$list['toCurrentMonth']);
    

    $hidden[] = array('name'=> 'perform' ,'value' => 'edit');
    $hidden[] = array('name'=> 'act' , 'value' => 'save');           
    $hidden[] = array('name'=> 'id' , 'value' => $id);           
    
    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'plp-performance-appraisal-edit.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
