<?php
  if ( $perm->has('nc_sms_api_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
       
         
		// Include the payment party class
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( SmsApi::validateAdd($data, $extra) ) { 
                        $query	= " INSERT INTO ".TABLE_SMS_API
                            ." SET ".TABLE_SMS_API .".gateway_id 		= '". $data['client'] ."'"  
                                	.",". TABLE_SMS_API .".gateway_name	= '". $data['client_details'] ."'"                                
                                	.",". TABLE_SMS_API .".name 		= '". $data['name'] ."'"                                
                                	.",". TABLE_SMS_API .".is_scrubbed 	= '". $data['is_scrubbed'] ."'"                    
                                	.",". TABLE_SMS_API .".username 	= '". $data['username'] ."'"                    
                                	.",". TABLE_SMS_API .".password 	= '". $data['password'] ."'"                    
                                	.",". TABLE_SMS_API .".post_url 	= '". $data['post_url'] ."'"                    
                                    .",". TABLE_SMS_API .".host  	    = '". $data['host'] ."'"                    
                                	.",". TABLE_SMS_API .".details 	    = '". $data['details'] ."'"                    
                                    .",". TABLE_SMS_API .".created_by 	= '". $my['user_id'] ."'"                    
									.",". TABLE_SMS_API .".status 		= '". $data['status'] ."'"                    
                                    .",". TABLE_SMS_API .".ip 			= '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_SMS_API .".do_e = '". 	date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sms-api.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sms-api.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sms-api.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-api-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
