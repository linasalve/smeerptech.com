<?php
	if ( $perm->has('nc_site_sp_delete') ) {
		$page_id	= isset($_GET["page_id"]) ? $_GET["page_id"] : ( isset($_POST["page_id"]) ? $_POST["page_id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'access_level' 	=> $my['access_level'],
						'messages' 		=> $messages
					);
		Staticpages::delete($page_id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/staticpage-list.php');
	}
	else {
		$messages->setErrorMessage("You do not have the Right to Access this module.");
	}
?>