<?php
    if ( $perm->has('nc_hr_attm_edit') ) {
    
        $id    = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,                          
                            'messages'          => &$messages
                        );
          
            if ( HrAttendanceMini::validateUpdate($data, $extra) ) {
			
				$query	= " UPDATE  ".TABLE_HR_ATTENDANCE_MINI
				." SET ".TABLE_HR_ATTENDANCE_MINI .".present_score = '".$data['present_score'] ."'"  
				.",". TABLE_HR_ATTENDANCE_MINI .".user_id = '". $data['executive_id'] ."'"
			 	.",". TABLE_HR_ATTENDANCE_MINI .".user_name = '". $data['executive_details'] ."'" 
				.",". TABLE_HR_ATTENDANCE_MINI .".attendance_dt='".$data['attendance_dt'] ."'  
     				WHERE ".TABLE_HR_ATTENDANCE_MINI.".id ='".$id."'";
           
                    
                 
                            
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_HR_ATTENDANCE_MINI .'.*'  ;            
            $condition_query = " WHERE (". TABLE_HR_ATTENDANCE_MINI .".id = '". $id ."' )";
           
            if ( HrAttendanceMini::getList($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                 
                
                
                    // Setup the date of delivery.
                    $_ALL_POST['attendance_dt']  = explode(' ', $_ALL_POST['attendance_dt']);
                    $temp               = explode('-', $_ALL_POST['attendance_dt'][0]);
                    $_ALL_POST['attendance_dt']  = NULL;
                    $_ALL_POST['dateList']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    $id         = $_ALL_POST['id'];
                    
                    $condition_querye=" WHERE "." user_id='".$_ALL_POST['user_id']."'";
                    User::getList( $db, $edetails, 'f_name,l_name,number,email', $condition_querye);
                    $edetails = $edetails[0];
                    $_ALL_POST['executive_details'] = $edetails['f_name']." ".$edetails['l_name']." (".$edetails['number'].") " ;
                    $_ALL_POST['executive_id'] = $_ALL_POST['user_id'];
                    
                   
                  
                   
                   
                                                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $att_id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/hr-attendance-mini-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save'); 
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-attendance-mini-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>