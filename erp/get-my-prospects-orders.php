<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php'); 
    $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : '' ; 
    $optionLink =$stringOpt= '' ; 
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if (isset($client_id) && !empty($client_id)){
        header("Content-Type: application/json");      
	    $query =" SELECT DISTINCT(".TABLE_PROSPECTS_ORDERS.".id),".TABLE_PROSPECTS_ORDERS.".order_title,".TABLE_PROSPECTS_ORDERS.".number, ".TABLE_PROSPECTS_ORDERS.".lead_closing_dt, ".TABLE_PROSPECTS_ORDERS.".status
		FROM ".TABLE_PROSPECTS_ORDERS." WHERE (".TABLE_PROSPECTS_ORDERS.".client = '".$client_id."' OR ".TABLE_PROSPECTS_ORDERS.".clients_su LIKE '%,".$client_id.",%' ) AND ( ".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$my['user_id'].",%' )  ";		
		$db->query($query);
		if ( $db->nf()>0 ) { 
            while ($db->next_record()){ 
                /* $arr[] = "{\"id\": \"".$db->f('id')."\", \"value\": \"".processSQLData($db->f('order_title'))." (".processSQLData($db->f('number')).")".""."\", \"info\": \"\"}"; */
				$status=processSQLData($db->f('status'));
				$lead_closing_dt	=	processSQLData($db->f('lead_closing_dt'));
				if($status == ProspectsOrder::SUSPENDED || $lead_closing_dt=='0000-00-00 00:00:00'){
					$text.= "Suspended OR No Close Dt <input type=\"radio\" disabled name=\"flw_ord_id\" value=\"".$db->f('id')."\"> ".processSQLData($db->f('order_title'))." - ".processSQLData($db->f('number'));
				}else{
					$text.= "<input type=\"radio\" name=\"flw_ord_id\" value=\"".$db->f('id')."\"> ".processSQLData($db->f('order_title'))." - ".processSQLData($db->f('number'));
				}
            } 
        }else{
			$text="No Leads Found";
		}
		echo $text;
       /*  echo "{\"results\": [";
        if ( $db->nf()>0 ) {
          
            $arr = array();
            while ($db->next_record()){ 
                $arr[] = "{\"id\": \"".$db->f('id')."\", \"value\": \"".processSQLData($db->f('order_title'))." (".processSQLData($db->f('number')).")".""."\", \"info\": \"\"}";
            }
            echo implode(", ", $arr);
           
        }
         echo "]}";	  */  
	}
include_once( DIR_FS_NC ."/flush.php");


?>
