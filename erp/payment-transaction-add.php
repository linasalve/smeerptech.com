<?php
 if ( $perm->has('nc_p_pt_add') ) {
        $transaction_id = isset($_GET["transaction_id"]) ? $_GET["transaction_id"] : 
		( isset($_POST["transaction_id"]) ? $_POST["transaction_id"] : '' );
		
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
        include_once ( DIR_FS_INCLUDES .'/company.inc.php');        
        include_once ( DIR_FS_INCLUDES .'/services.inc.php');
		include_once ( DIR_FS_INCLUDES .'/currency.inc.php');    
		
		$region         = new Region('91');
		$lst_city       = $region->getCityList();
        /*
        $clientlist	= NULL;
        $condition_queryc = " WHERE parent_id = '' AND status ='".Clients::ACTIVE."' ORDER BY f_name,l_name";
        Clients::getList( $db, $clientlist, 'f_name,l_name,user_id,billing_name,number', $condition_queryc);
        include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
        $executivelist	= NULL;
        $condition_queryc = " WHERE status ='".User::ACTIVE."' ORDER BY f_name,l_name";
        User::getList( $db, $executivelist, 'f_name,l_name,user_id', $condition_queryc);
        */
		
		$currency = NULL ;
        $required_fields = '*' ;
		Currency::getList($db,$currency,$required_fields);     
		
        $services = NULL;
		Services::getParent($db,$services);
         
        
        $variables['status'] = Paymenttransaction::getStatus();
		$variables['gltype'] = Paymenttransaction::getGLType();
        /*
        if ( $perm->has('nc_ts_add_al') ) {
            $access_level += 1;
        }
        */
        $variables['can_add_rstr'] = false;
        if( $perm->has('nc_p_pt_add_restricted') ) {
            $variables['can_add_rstr'] = true;
            $variables['can_add_rstr'] = false;
        }
		//COPY TRANSACTION BOF
		if(!empty($transaction_id)){
			$fields = TABLE_PAYMENT_TRANSACTION .'.*'  ;            
			$condition_query = " WHERE (". TABLE_PAYMENT_TRANSACTION .".id = '". $transaction_id ."' )";
			if ( Paymenttransaction::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
			   $_ALL_POST = $_ALL_POST['0'];   
				
				if(isset($_ALL_POST['is_pdc']) && $_ALL_POST['is_pdc']==1){
					$temp  =null;
					if(isset($_ALL_POST['do_pdc_chq']) && $_ALL_POST['do_pdc_chq']!='0000-00-00 00:00:00'){
						$_ALL_POST['do_pdc_chq']  = explode(' ', $_ALL_POST['do_pdc_chq']);
							$temp               = explode('-', $_ALL_POST['do_pdc_chq'][0]);
						 $_ALL_POST['do_pdc_chq']  = NULL;
						 $_ALL_POST['do_pdc_chq']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
					}else{
						$_ALL_POST['do_pdc_chq']  ='';  
					}					
				}else{
					 $_ALL_POST['do_pdc_chq']  ='';  
				}
				$_ALL_POST['attachment_imap_arr'] = array();
				if(!empty($_ALL_POST['attachment_imap'])){
					$_ALL_POST['attachment_imap_arr']=explode(",",$_ALL_POST['attachment_imap']);
				}
				//Vendor Bank details bof
				$_ALL_POST['vendor_bank_details'] ='';
				if(!empty($_ALL_POST['vendor_bank_id'])){
					$table = TABLE_VENDORS_BANK;
					$condition2 = " WHERE ".TABLE_VENDORS_BANK .".user_id= '".$_ALL_POST['vendor_bank_id'] ."' " ;
					$fields1 =  TABLE_VENDORS_BANK .'.f_name,'.TABLE_VENDORS_BANK.".l_name,
					".TABLE_VENDORS_BANK.".billing_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['vendor_bank_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." 
						(".$detailsArr['billing_name'].")" ;   
					}
				 
				}
				//vendor details bof
				/* 
				$_ALL_POST['vendor_details'] ='';
				if(!empty($_ALL_POST['party_id'])){
					$table = TABLE_VENDORS;
					$condition2 = " WHERE ".TABLE_VENDORS .".user_id= '".$_ALL_POST['party_id'] ."' " ;
					$fields1 =  TABLE_VENDORS .'.f_name,'.TABLE_VENDORS.".l_name,".TABLE_VENDORS.".billing_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['vendor_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." (".$detailsArr['billing_name'].")" ;   
					}
				 
				} 
				*/
				$_ALL_POST['client_details'] ='';
				//clients details bof
				if(!empty($_ALL_POST['client_id'])){
					$table = TABLE_CLIENTS;
					$condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$_ALL_POST['client_id'] ."' " ;
					$fields1 =  TABLE_CLIENTS .'.f_name,'.TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['client_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." (".$detailsArr['billing_name'].")" ;   
					}				 
				}
				//assosiate details bof
				$_ALL_POST['executive_details'] ='';
				if(!empty($_ALL_POST['executive_id'])){
					$table = TABLE_USER;
					$condition2 = " WHERE ".TABLE_USER .".user_id= '".$_ALL_POST['executive_id'] ."' " ;
					$fields1 =  TABLE_USER .'.f_name,'.TABLE_USER.".l_name" ;
					$detailsArr = getRecord($table,$fields1,$condition2);
					if(!empty($detailsArr)){
						$_ALL_POST['executive_details'] = $detailsArr['f_name']." ".$detailsArr['l_name']." " ;   
					}				 
				}

				if(isset($_ALL_POST['do_transaction']) && $_ALL_POST['do_transaction']!='0000-00-00 00:00:00'){
					$temp  =null;
					$_ALL_POST['do_transaction']  = explode(' ', $_ALL_POST['do_transaction']);
					$temp               = explode('-', $_ALL_POST['do_transaction'][0]);
					$_ALL_POST['do_transaction']  = NULL;
					$_ALL_POST['do_transaction']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['do_transaction']  = '';                            
				}
				if(isset($_ALL_POST['do_voucher']) && $_ALL_POST['do_voucher']!='0000-00-00 00:00:00'){
                    $temp  =null;
                    $_ALL_POST['do_voucher']  = explode(' ', $_ALL_POST['do_voucher']);
                    $temp               = explode('-', $_ALL_POST['do_voucher'][0]);
                    $_ALL_POST['do_voucher']  = NULL;
                    $_ALL_POST['do_voucher']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                }else{
                    $_ALL_POST['do_voucher']  = '';                            
                }
				if(isset($_ALL_POST['period_from']) && $_ALL_POST['period_from']!='0000-00-00 00:00:00'){
					$temp  =null;
					$_ALL_POST['period_from']  = explode(' ', $_ALL_POST['period_from']);
					$temp               = explode('-', $_ALL_POST['period_from'][0]);
					$_ALL_POST['period_from']  = NULL;
					$_ALL_POST['period_from']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['period_from']  = '';                            
				}
				if(isset($_ALL_POST['period_to']) && $_ALL_POST['period_to']!='0000-00-00 00:00:00'){
					$temp  =null;
					$_ALL_POST['period_to']  = explode(' ', $_ALL_POST['period_to']);
					$temp               = explode('-', $_ALL_POST['period_to'][0]);
					$_ALL_POST['period_to']  = NULL;
					$_ALL_POST['period_to']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['period_to']  = '';                            
				}
				if(isset($_ALL_POST['do_pay_received']) && $_ALL_POST['do_pay_received']!='0000-00-00'){
					$temp  =null;
					$_ALL_POST['do_pay_received']  = explode(' ', $_ALL_POST['do_pay_received']);
					$temp               = explode('-', $_ALL_POST['do_pay_received'][0]);
					$_ALL_POST['do_pay_received']  = NULL;
					$_ALL_POST['do_pay_received']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
				
					 $_ALL_POST['do_pay_received']  = '';
				}
				if(isset($_ALL_POST['do_credit_trans']) && $_ALL_POST['do_credit_trans']!='0000-00-00'){
					$temp  =null;
					$_ALL_POST['do_credit_trans']  = explode(' ', $_ALL_POST['do_credit_trans']);
					$temp               = explode('-', $_ALL_POST['do_credit_trans'][0]);
					$_ALL_POST['do_credit_trans']  = NULL;
					$_ALL_POST['do_credit_trans']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					$_ALL_POST['do_credit_trans']  = '';
					
				}
				if(isset($_ALL_POST['do_debit_trans']) && $_ALL_POST['do_debit_trans']!='0000-00-00'){
					$temp  =null;
					$_ALL_POST['do_debit_trans']  = explode(' ', $_ALL_POST['do_debit_trans']);
					$temp               = explode('-', $_ALL_POST['do_debit_trans'][0]);
					$_ALL_POST['do_debit_trans']  = NULL;
					$_ALL_POST['do_debit_trans']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}else{
					 $_ALL_POST['do_debit_trans']  = '';
				}
				
			}
		}
		//COPY TRANSACTION EOF
	    
		//$_ALL_POST['transaction_type'] = Paymenttransaction::PAYMENTIN;
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
           
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages,
							'_ALL_POST'          => &$_ALL_POST
                        );
            
            $dbnew=new db_local();    
            $account_head=null;
            
            if ( Paymenttransaction::validateAdd($data,$account_head, $extra) ) {
               
			    $currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                $fields1 = "*";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);              
                if(!empty($currency1)){
                   $data['currency_abbr'] = $currency1[0]['abbr'];
                   $data['currency_name'] = $currency1[0]['currency_name'];
                   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
                   $data['currency_country'] = $currency1[0]['country_name'];
                }
			   
                //Code To transfer payment details in user account BOF
                 $data['company_name'] = '';
                 $data['company_prefix'] ='';
                if(!empty($data['company_id'])){
                    $company1 =null;
                    $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                    $fields_c1 = " prefix,name ";
                    Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
                    if(!empty($company1)){                  
                       $data['company_name'] = $company1[0]['name'];
                       $data['company_prefix'] = $company1[0]['prefix'];
                    }
                }				
				$data['behalf_company_name'] ='';
				if(!empty($data['behalf_company_id'])){
                    $company1 =null;
                    $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['behalf_company_id'];
                    $fields_c1 = "name ";
                    Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
                    if(!empty($company1)){                  
                       $data['behalf_company_name'] = $company1[0]['name'];
                    }
                }
                if($data['mode_id']==2){
                    $data['pay_cheque_no'] ='';
                    $data['do_pay_received'] ='';
                    $data['pay_bank_company'] ='';
                    $data['pay_branch'] ='';
                }
                $tr_cr_bank_amt =$tr_db_bank_amt ='';
            
                    if($data['status']==Paymenttransaction::COMPLETED && 
						$data['transaction_type']==Paymenttransaction::PAYMENTIN){  						 
						 
                        $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$data['credit_pay_amt']." 
							WHERE id='".$data['credit_pay_bank_id']."'";
                        $executed = $db->query($update_query);
                    
                        //get bank opening balance 
                        $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['credit_pay_bank_id'];
                        $db->query($sql);
                        if($db->nf()>0 ){
                            while ($db->next_record()) {
                                $tr_cr_bank_amt = $db->f('balance_amt');
                            }
                        } 
						
                        if($executed){
                            $messages->setOkMessage("Your Amount Details has been updated.");
                        }else{
                            $messages->setErrorMessage("Sorry! Amount Details can not be updated.");
                        }
					
					}elseif( $data['status']==Paymenttransaction::COMPLETED &&
						$data['transaction_type']== Paymenttransaction::PAYMENTOUT ){
					  
						                    	
						/* -ve allowed bof
						$retrieve_query= "SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE 
						id='".$data['debit_pay_bank_id']."'";
						$dbnew->query($retrieve_query);
                        $executed = 0;
						while ($dbnew->next_record()) {
                        	 $balance=$dbnew->f('balance_amt');
                    	} 
						
						
                    	if($balance < $data['debit_pay_amt']){
                    		$messages->setErrorMessage("Your balance amount is less than the amount you want 
							to debit.");
						}
						else{
                    	    $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$data['debit_pay_amt']." 
							WHERE id='".$data['debit_pay_bank_id']."' ";
                            $executed = $db->query($update_query);
                            
                            $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE 
							id = ".$data['debit_pay_bank_id'];
                            $db->query($sql);
                            if($db->nf()>0 )
                            {
                                while ($db->next_record()) {
                                    $tr_db_bank_amt = $db->f('balance_amt');
                                }
                            }
                            
						}-ve allowed eof
						*/
						
							$update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$data['debit_pay_amt']."
							WHERE id='".$data['debit_pay_bank_id']."' ";
                            $executed = $db->query($update_query);
                            
                            $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['debit_pay_bank_id'];
                            $db->query($sql);
                            if($db->nf()>0 )
                            {
                                while ($db->next_record()) {
                                    $tr_db_bank_amt = $db->f('balance_amt');
                                }
                            }
							
                        if($executed){
                            $messages->setOkMessage("Your Amount Details has been updated.");
                        }else{
                            $messages->setErrorMessage("Sorry! Amount Details can not be updated.");
                        }
					}elseif($data['status']==Paymenttransaction::COMPLETED && $data['transaction_type']==Paymenttransaction::INTERNAL){
                        
                        $executed = 0 ;
                        /*  
						-ve allowed bof 
					    $retrieve_query= "SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." 
						WHERE id='".$data['debit_pay_bank_id']."'";
						$dbnew->query($retrieve_query);
                        
						while ($dbnew->next_record()) {
                        	$balance=$dbnew->f('balance_amt');
                    	}
                    	if($balance < $data['debit_pay_amt']){
                    		$messages->setErrorMessage("Your balance amount is less than the amount you want to debit.");
						}
						else{
                    	    $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$data['debit_pay_amt']." WHERE id='".$data['debit_pay_bank_id']."'";
                            $executed = $db->query($update_query);
                            if($executed){                                            	
                                $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                                    ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$data['credit_pay_amt']." 
									WHERE id='".$data['credit_pay_bank_id']."'";
                                $executed = $db->query($update_query);     
                                //Get balance amount after transaction BOF
                                $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['credit_pay_bank_id'];
                                $db->query($sql);
                                if($db->nf()>0 )
                                {
                                    while ($db->next_record()) {
                                        $tr_cr_bank_amt = $db->f('balance_amt');
                                    }
                                }
                                
                                $sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['debit_pay_bank_id'];
                                $db->query($sql);
                                if($db->nf()>0 )
                                {
                                    while ($db->next_record()) {
                                        $tr_db_bank_amt = $db->f('balance_amt');
                                    }
                                }
                                //Get balance amount after transaction EOF
                            }                 
						} 
						-ve allowed eof
						*/
						
						
						$update_query	= " UPDATE ".TABLE_PAYMENT_BANK
                            ." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$data['debit_pay_amt']."
							WHERE id='".$data['debit_pay_bank_id']."'";
						$executed = $db->query($update_query);
						if($executed){ 
							$update_query = " UPDATE ".TABLE_PAYMENT_BANK
								." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$data['credit_pay_amt']."
								WHERE id='".$data['credit_pay_bank_id']."'";
							$executed = $db->query($update_query);     
							//Get balance amount after transaction BOF
							$sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['credit_pay_bank_id'];
							$db->query($sql);
							if($db->nf()>0 ){
								while ($db->next_record()) {
									$tr_cr_bank_amt = $db->f('balance_amt');
								}
							}
							
							$sql="SELECT balance_amt FROM ".TABLE_PAYMENT_BANK." WHERE id = ".$data['debit_pay_bank_id'];
							$db->query($sql);
							if($db->nf()>0 )
							{
								while ($db->next_record()) {
									$tr_db_bank_amt = $db->f('balance_amt');
								}
							}
							//Get balance amount after transaction EOF
						}          
                        if($executed){
                            $messages->setOkMessage("Your Amount Details has been updated.");
                        }else{
                            $messages->setErrorMessage("Sorry! Amount Details can not be updated.");
                        } 
					}                    
                //Code To transfer payment details in account EOF
				$ref_bank_id = $data['ref_bank_id'];				
                if($data['transaction_type']==Paymenttransaction::PAYMENTIN){
                        $data['debit_pay_bank_id'] ='';
                        $data['debit_pay_amt'] ='';
                        $tr_db_bank_amt ='';
                        $data['do_debit_trans']='';
                        $data['debit_pay_trans_no']='';   
						$data['ref_credit_pay_bank_id'] =$ref_bank_id ;
                }elseif($data['transaction_type']==Paymenttransaction::PAYMENTOUT){
                     $data['credit_pay_bank_id'] = '';
                     $data['credit_pay_amt'] = '';
                     $tr_cr_bank_amt = '';
                     $data['do_credit_trans'] = '';
                     $data['credit_pay_trans_no'] = '';
					 $data['ref_debit_pay_bank_id'] =$ref_bank_id ;
                }            
                if(!empty($data['do_transaction'])){
                    $data['do_transaction'] = explode('/', $data['do_transaction']);
                    $data['do_transaction'] = mktime(0, 0, 0, $data['do_transaction'][1], $data['do_transaction'][0], $data['do_transaction'][2]);
                    $data['do_transaction'] = date('Y-m-d H:i:s', $data['do_transaction']);
                }
                if(!empty($data['do_voucher'])){
                    $data['do_voucher'] = explode('/', $data['do_voucher']);
                    $data['do_voucher'] = mktime(0, 0, 0, $data['do_voucher'][1], $data['do_voucher'][0], $data['do_voucher'][2]);
                    $data['do_voucher'] = date('Y-m-d H:i:s', $data['do_voucher']);
                }
                
                if(!empty($data['do_pay_received'])){
                    $data['do_pay_received'] = explode('/', $data['do_pay_received']);
                    $data['do_pay_received'] = mktime(0, 0, 0, $data['do_pay_received'][1], $data['do_pay_received'][0], 
					$data['do_pay_received'][2]);
                    $data['do_pay_received'] = date('Y-m-d H:i:s', $data['do_pay_received']);
                }
                
                if(!empty($data['do_credit_trans'])){
                    $data['do_credit_trans'] = explode('/', $data['do_credit_trans']);
                    $data['do_credit_trans'] = mktime(0, 0, 0, $data['do_credit_trans'][1], $data['do_credit_trans'][0],
					$data['do_credit_trans'][2]);
                    $data['do_credit_trans'] = date('Y-m-d H:i:s', $data['do_credit_trans']);
                }
                
                if(!empty($data['do_debit_trans'])){
                    $data['do_debit_trans'] = explode('/', $data['do_debit_trans']);
                    $data['do_debit_trans'] = mktime(0, 0, 0, $data['do_debit_trans'][1], $data['do_debit_trans'][0], 
					$data['do_debit_trans'][2]);
                    $data['do_debit_trans'] = date('Y-m-d H:i:s', $data['do_debit_trans']);
                }
                if(!empty($data['do_pdc_chq'])){
                    $data['do_pdc_chq'] = explode('/', $data['do_pdc_chq']);
                    $data['do_pdc_chq'] = mktime(0, 0, 0, $data['do_pdc_chq'][1], $data['do_pdc_chq'][0], $data['do_pdc_chq'][2]);
                    $data['do_pdc_chq'] = date('Y-m-d H:i:s', $data['do_pdc_chq']);
                }
                if(!empty($data['period_from'])){
                    $data['period_from'] = explode('/', $data['period_from']);
                    $data['period_from'] = mktime(0, 0, 0, $data['period_from'][1], $data['period_from'][0], $data['period_from'][2]);
                    $data['period_from'] = date('Y-m-d H:i:s', $data['period_from']);
                }
                if(!empty($data['period_to'])){
                    $data['period_to'] = explode('/', $data['period_to']);
                    $data['period_to'] = mktime(0, 0, 0, $data['period_to'][1], $data['period_to'][0], $data['period_to'][2]);
                    $data['period_to'] = date('Y-m-d H:i:s', $data['period_to']);
                }
                if( $messages->getErrorMessageCount() <= 0){
                
                    $data['off_exp_issue_by_name_str']=$data['off_exp_issue_to_name_str']='';
					if(!empty($data['off_exp_issue_by'])){
						$table = TABLE_USER;
						$condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['off_exp_issue_by'] ."' " ;
						$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
						$exeCreted= getRecord($table,$fields1,$condition2);                
						$data['off_exp_issue_by_name_str'] = $exeCreted['f_name']." ".$exeCreted['l_name']." " ;
					}
					if(!empty($data['off_exp_issue_to'])){
						$table = TABLE_USER;
						$condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['off_exp_issue_to'] ."' " ;
						$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
						$exeCreted= getRecord($table,$fields1,$condition2);                
						$data['off_exp_issue_to_name_str'] = $exeCreted['f_name']." ".$exeCreted['l_name']." " ;
					}
					$data['pay_city_name'] ='';
					if(!empty($data['pay_city_id'])){
						$table = TABLE_LIST_CITY;
						$condition2 = " WHERE ".TABLE_LIST_CITY .".id= '".$data['pay_city_id'] ."' " ;
						$fields1 =  TABLE_LIST_CITY .'.name';
						$cityArr= getRecord($table,$fields1,$condition2);                
						$data['pay_city_name'] = $cityArr['name'] ;
					}					 
					$special = 0 ;
					if(isset($data['special'])){
						$special = 1;					
					}
					$query	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION
                    ." SET ". TABLE_PAYMENT_TRANSACTION .".transaction_id = '".    $data['transaction_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".number = '".$data['number'] ."'"      
					.",". TABLE_PAYMENT_TRANSACTION .".special = '".$special ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".company_id = '".$data['company_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".company_name = '".$data['company_name'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".company_prefix = '".$data['company_prefix'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".currency_abbr ='".processUserData($data['currency_abbr'])."'"
					.",". TABLE_PAYMENT_TRANSACTION .".currency_id = '".$data['currency_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION.".currency_name = '".processUserData($data['currency_name']) ."'"
					.",". TABLE_PAYMENT_TRANSACTION.".currency_symbol='".processUserData($data['currency_symbol'])."'"
			.",". TABLE_PAYMENT_TRANSACTION .".currency_country ='".processUserData($data['currency_country']) ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".date = '".$variables['date'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".user_id = '".$my['uid'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".service_id = '". $data['service_id'] ."'"
				    .",". TABLE_PAYMENT_TRANSACTION .".transaction_type = '".$data['transaction_type'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".do_transaction = '".$data['do_transaction'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".do_voucher = '".$data['do_voucher'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".account_head_id = '". $data['account_head_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".iaccount_head_id = '". $data['iaccount_head_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id = '".  $data['vendor_bank_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".party_id = '".$data['party_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".executive_id = '".$data['executive_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".client_id = '".$data['client_id'] ."'"                    
					.",". TABLE_PAYMENT_TRANSACTION .".mode_id = '".       $data['mode_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".ref_debit_pay_bank_id = '".$data['ref_debit_pay_bank_id']."'"
					.",". TABLE_PAYMENT_TRANSACTION .".ref_credit_pay_bank_id= '".$data['ref_credit_pay_bank_id']."'"
					.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt = '". $data['pay_received_amt'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt  = '". $data['pay_received_amt'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".exchange_rate = '". $data['exchange_rate'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_inr  = '". $data['pay_received_amt_inr'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt_inr = '". $data['pay_received_amt_inr'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_words = '".   $data['pay_received_amt_words'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".pay_cheque_no = '".             $data['pay_cheque_no'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".do_pay_received = '".          $data['do_pay_received'] ."'"                        .",". TABLE_PAYMENT_TRANSACTION .".pay_bank_company= '".               $data['pay_bank_company'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".pay_branch = '".             $data['pay_branch'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".pay_city_id = '".             $data['pay_city_id'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".pay_city_name = '".           $data['pay_city_name'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id = '".          $data['credit_pay_bank_id'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_amt = '".               $data['credit_pay_amt'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".tr_cr_bank_amt = '".               $tr_cr_bank_amt ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".do_credit_trans = '".             $data['do_credit_trans'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".credit_pay_trans_no = '".          $data['credit_pay_trans_no'] ."'"
						.",". TABLE_PAYMENT_TRANSACTION .".will_receipt_generate = '".$data['will_receipt_generate']."'"
						//.",". TABLE_PAYMENT_TRANSACTION .".is_receipt = '".$data['is_receipt']."'"
						//.",". TABLE_PAYMENT_TRANSACTION .".receipt_no = '".$data['receipt_no']."'"
						.",". TABLE_PAYMENT_TRANSACTION .".gl_type = '".    $data['gl_type'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = '".    $data['debit_pay_bank_id'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_amt = '".        $data['debit_pay_amt'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".tr_db_bank_amt = '".       $tr_db_bank_amt ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".do_debit_trans = '".       $data['do_debit_trans'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".debit_pay_trans_no = '".   $data['debit_pay_trans_no'] ."'"                    .",". TABLE_PAYMENT_TRANSACTION .".is_bill_receive = '".      $data['is_bill_receive'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".bill_no = '".              $data['bill_no'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".voucher_no = '".           $data['voucher_no'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".particulars = '".          $data['particulars'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".remarks = '".              $data['remarks'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".is_pdc = '".  			  $data['is_pdc'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".off_exp_issue_by 	 = '".$data['off_exp_issue_by']."'"
					.",". TABLE_PAYMENT_TRANSACTION.".off_exp_issue_by_name='".$data['off_exp_issue_by_name_str'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION.".off_exp_issue_to= '".$data['off_exp_issue_to'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION.".off_exp_issue_to_name='".$data['off_exp_issue_to_name_str'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".behalf_vendor_bank 	 = '".$data['behalf_vendor_bank'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".behalf_vendor_bank_name='".$data['behalf_vendor_bank_name']."'"
					.",". TABLE_PAYMENT_TRANSACTION .".behalf_client 	 = '".$data['behalf_client'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".behalf_client_name = '".$data['behalf_client_name'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".behalf_executive = '".$data['behalf_executive'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION.".behalf_executive_name ='".$data['behalf_executive_name']."'"
					.",". TABLE_PAYMENT_TRANSACTION.".behalf_company_id = '".$data['behalf_company_id'] ."'"    
					.",". TABLE_PAYMENT_TRANSACTION.".behalf_company_name='".$data['behalf_company_name'] ."'"    
					.",". TABLE_PAYMENT_TRANSACTION .".do_pdc_chq = '". $data['do_pdc_chq'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".period_from = '". $data['period_from'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".period_to = '". $data['period_to'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".pdc_chq_details = '".$data['pdc_chq_details'] ."'"
					.",". TABLE_PAYMENT_TRANSACTION .".ip = '".$_SERVER['REMOTE_ADDR']."'"
					.",". TABLE_PAYMENT_TRANSACTION .".status = '".$data['status'] ."'";
                
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                         $variables['hid'] = $db->last_inserted_id(); 
                        //After insert, update transaction counter in 
                        //updateCounterOf($db,CONTR_VCH,$data['company_id']);
                        // updateOldCounterOf($db,CONTR_VCH, $data['company_id'],$data['financialYr']);                                       
                        $messages->setOkMessage("Payment transaction ".$data['number']." entry has been done.");
                       
                        $dataDetails=array();
                        //Send mail to admin after adding record bof
						/*   
						$fields = TABLE_PAYMENT_TRANSACTION .'.* , '.TABLE_USER .'.f_name as efname, '.TABLE_USER.'.l_name as elname, '.TABLE_PAYMENT_ACCOUNT_HEAD .'.account_head_name, ' . TABLE_PAYMENT_MODE .'.payment_mode ,'. TABLE_VENDORS .'.f_name as fname, ' . TABLE_VENDORS .'.l_name as lname, '. TABLE_VENDORS .'.billing_name as ppcompany_name, '.TABLE_SETTINGS_COMPANY.'.name AS company_name, '. TABLE_VENDORS_BANK .'.f_name as vfname, 
						'. TABLE_VENDORS_BANK .'.l_name as vlname,
						'. TABLE_VENDORS_BANK .'.billing_name as vbilling_name'; 
						*/
						
						
						$fields = TABLE_PAYMENT_TRANSACTION .'.* , '.TABLE_USER .'.f_name as efname, '.TABLE_USER.'.l_name as elname, '.TABLE_PAYMENT_ACCOUNT_HEAD .'.account_head_name, ' . TABLE_PAYMENT_MODE .'.payment_mode ,'.TABLE_SETTINGS_COMPANY.'.name AS company_name, '. TABLE_VENDORS_BANK .'.f_name as vfname, '. TABLE_VENDORS_BANK .'.l_name as vlname,
						'. TABLE_VENDORS_BANK .'.billing_name as vbilling_name';
                        $condition_query=" WHERE ".TABLE_PAYMENT_TRANSACTION.".id= ".$variables['hid'] ;
                        Paymenttransaction::getDetails($db, $dataDetails, $fields, $condition_query) ;
                       
                        $dataDetails = $dataDetails[0] ;
                        $email = NULL;    
                        $statusArr = Paymenttransaction::getStatusAll();
                        $statusArr = array_flip($statusArr);
                        if(!empty($dataDetails)){
                               $dataDetails['will_receipt_generate_name']='No';
                               $dataDetails['is_pdc_name']='No';
                               $dataDetails['is_bill_receive_name'] = 'No';
                               $dataDetails['creator']= $my['f_name']." ".$my['l_name'] ;
                               $dataDetails['status_name'] = $statusArr[$dataDetails['status']];
                               //get bank name on the basis of type IN/OUT
                                if( $dataDetails['transaction_type'] ==Paymenttransaction::PAYMENTIN ){
                               
                                    $table = TABLE_PAYMENT_BANK;
                                    $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$dataDetails['credit_pay_bank_id'] ."' " ;
                                    $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                                    $banknameArr = getRecord($table,$fields1,$condition2);
                                    if(!empty($banknameArr)){
                                        $bankname = $banknameArr['bank_name'] ;                    
                                    }
                                    $dataDetails['transaction_typename']='Payment In';
                                    
                                    if($dataDetails['will_receipt_generate']){
                                        $dataDetails['will_receipt_generate_name']='Yes';
                                    }
                                    if($dataDetails['is_pdc']){
                                        $dataDetails['is_pdc_name']='Yes';
                                    }
                                    $dataDetails['creditbankname'] = $bankname ; 
                                }elseif($dataDetails['transaction_type'] ==Paymenttransaction::PAYMENTOUT){
                               
                                    $table = TABLE_PAYMENT_BANK;
                                    $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$dataDetails['debit_pay_bank_id'] ."' " ;
                                    $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                                    $banknameArr = getRecord($table,$fields1,$condition2);
                                    if(!empty($banknameArr)){
                                        $bankname = $banknameArr['bank_name'] ;                                           
                                    }
                                    $dataDetails['transaction_typename']='Payment Out';
                                    if($dataDetails['is_bill_receive'] ){           
                                        $dataDetails['is_bill_receive_name'] = "Yes";
                                    }
                                     $dataDetails['debitbankname'] = $bankname ;   
                                }elseif($dataDetails['transaction_type'] ==Paymenttransaction::INTERNAL){
                               
                                    $table = TABLE_PAYMENT_BANK;
                                    $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$dataDetails['debit_pay_bank_id'] ."' " ;
                                    $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                                    $banknameArr = getRecord($table,$fields1,$condition2);
                                    if(!empty($banknameArr)){
                                        $debitbankname = $banknameArr['bank_name'] ;
                                    }                
                                    $table = TABLE_PAYMENT_BANK;
                                    $condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id= '".$dataDetails['credit_pay_bank_id'] ."' " ;
                                    $fields1 =  TABLE_PAYMENT_BANK .'.bank_name' ;
                                    $banknameArr = getRecord($table,$fields1,$condition2);
                                    if(!empty($banknameArr)){
                                        $creditbankname = $banknameArr['bank_name'] ;   
                                    }
                                    $dataDetails['transaction_typename']='Internal';
                                    //$val['rowspan']=3;
                                    if($dataDetails['will_receipt_generate']){
                                        $dataDetails['will_receipt_generate_name']='Yes';
                                    }
                                    if($dataDetails['is_pdc']){
                                        $dataDetails['is_pdc_name']='Yes';
                                    }
                                    if($dataDetails['is_bill_receive'] ){           
                                        $dataDetails['is_bill_receive_name'] = "Yes";
                                    }
                                }
                                if($dataDetails['transaction_type'] ==Paymenttransaction::INTERNAL){
                                    $dataDetails['debitbankname'] = $debitbankname ;           
                                    $dataDetails['creditbankname'] = $creditbankname ;  
                                }
                                else{
                                    $dataDetails['bankname'] = $bankname ;           
                                }
                            $dataDetails['link']= DIR_WS_NC .'/payment-transaction.php?perform=view&id='.$variables['hid'] ;
                        }
                        
                   
                        if ( getParsedEmail($db, $s, 'PAYMENT_TRANSACTION_ADD', $dataDetails, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => 'Admin', 'email' => $email_transaction_updates);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
						
						 
						
                        //Send mail to admin after adding record eof
                                    
                    }else{
                       
                        if($data['status']==Paymenttransaction::COMPLETED && $data['transaction_type']==Paymenttransaction::PAYMENTIN){                
                            $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
								." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$data['credit_pay_amt']." WHERE id='".$data['credit_pay_bank_id']."'";
							$executed = $db->query($update_query);
                            
                        }elseif( $data['status']==Paymenttransaction::COMPLETED && $data['transaction_type']== Paymenttransaction::PAYMENTOUT ){
                            $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
								." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$data['debit_pay_amt']." WHERE id='".$data['debit_pay_bank_id']."'";
							$executed = $db->query($update_query);
                        
                        }elseif( $data['status']==Paymenttransaction::COMPLETED && $data['transaction_type']== Paymenttransaction::INTERNAL ){
                        
                            $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
								." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt - ".$data['credit_pay_amt']." WHERE id='".$data['credit_pay_bank_id']."'";
							$executed = $db->query($update_query);
                            
                            $update_query	= " UPDATE ".TABLE_PAYMENT_BANK
								." SET ". TABLE_PAYMENT_BANK .".balance_amt = balance_amt + ".$data['debit_pay_amt']." WHERE id='".$data['debit_pay_bank_id']."'";
							$executed = $db->query($update_query);
                        
                        }                    
                       
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                }
                
                
                    //$_ALL_POST['transaction_id'] = getCounterNumber($db,CONTR_VCH,$data['company_id']);  
                    //$data		= NULL;
                
                
                
            }
            
        }
		 
        /*
        if(!isset($_ALL_POST['number'])){
            $number = Paymenttransaction::getNewNumber($db);
            $_ALL_POST['number']=$number;
        }*/
		
       /*  if(isset($_POST['btnCreate'])  && $messages->getErrorMessageCount() <= 0){
             header("Location:".DIR_WS_NC."/payment-transaction.php?perform=add&added=1&number=".$data['number']);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-transaction.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {           
            //include ( DIR_FS_NC .'/payment-transaction-list.php');
            header("Location:".DIR_WS_NC."/payment-transaction.php?added=1&number=".$data['number']);            
        }
        else { */    
		
			if(empty($_ALL_POST['exchange_rate'])){
				$_ALL_POST['exchange_rate']=1;
			}
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'ajx' ,'value' => $ajx);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'account_head', 'value' => 'account_head');
            //$page["var"][] = array('variable' => 'clientlist', 'value' => 'clientlist');
            //$page["var"][] = array('variable' => 'executivelist', 'value' => 'executivelist');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');	
            $page["var"][] = array('variable' => 'services', 'value' => 'services');      
            $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');      
           // $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');      
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-add.html');
       // }
    }
    else {
         $messages->setErrorMessage("You donot have the Permisson to Access this module.");
      //  $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-add-permission.html');
    }
?>
