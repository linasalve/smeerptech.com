<?php
if ( $perm->has('nc_st_tc_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
                     
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                            );
            
            if ( StTemplateCategory::validateUpdate($data, $extra) ) {
			
					 
                    $query  = " UPDATE ".TABLE_ST_TEMPLATE_CATEGORY
							." SET ".TABLE_ST_TEMPLATE_CATEGORY .".category = '". $data['category'] ."'"  
							.",". TABLE_ST_TEMPLATE_CATEGORY .".details 	= '". $data['details'] ."'"                     	   .",". TABLE_ST_TEMPLATE_CATEGORY .".status 	= '". $data['status'] ."'"
							." WHERE id = '". $id ."'";
                   
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL; 
				$fields = TABLE_ST_TEMPLATE_CATEGORY .'.*'  ;            
				$condition_query = " WHERE (". TABLE_ST_TEMPLATE_CATEGORY .".id = '". $id ."' )";            
				if ( StTemplateCategory::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
					$_ALL_POST = $_ALL_POST['0'];  
					 
				} 
            }
        } else {
            // Read the record which is to be Updated.
           
            $fields = TABLE_ST_TEMPLATE_CATEGORY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_ST_TEMPLATE_CATEGORY .".id = '". $id ."' )";            
            if ( StTemplateCategory::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0']; 
            }else{ 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to 
				access this Module.");
            }
        }

        
		// These parameters will be used when returning the control back to the List page.
		foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
			$hidden[]   = array('name'=> $key, 'value' => $value);
		}
		
  
		$hidden[] = array('name'=> 'id', 'value' => $id);
		$hidden[] = array('name'=> 'ajx','value' => $ajx);
		$hidden[] = array('name'=> 'perform', 'value' => 'edit');
		$hidden[] = array('name'=> 'act', 'value' => 'save');
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');    
		
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'st-template-category-edit.html');
        
    }
    else {
        
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
