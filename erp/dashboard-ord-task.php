<?php
	if ( !defined('THIS_DOMAIN') ) {
		require_once("../lib/config.php");
	}
	$ajx	= isset($_GET["ajx"])? $_GET["ajx"]	: ( isset($_POST["ajx"])? $_POST["ajx"]	:'');
    if($ajx==1){
		page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
		include_once ( DIR_FS_NC ."/header.php");
	}
	
if ( $perm->has('nc_dashboard_orders') ) {
    
		include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
		include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
		
		 
		// For lead list
		$xt	= isset($_GET["xt"])? $_GET["xt"]	: ( isset($_POST["xt"])? $_POST["xt"]	:'');
		//$rpp= isset($_GET["rpp"])	? $_GET["rpp"]	: ( isset($_POST["rpp"])? $_POST["rpp"]	: 
		//(defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
		
        $rpp = 10;
		
        /* 
		$sString = isset($_GET["sString"]) ? $_GET["sString"]: ( isset($_POST["sString"]) ? $_POST["sString"] : '' );
        $sType= isset($_GET["sType"])? $_GET["sType"]    : ( isset($_POST["sType"]) ? $_POST["sType"]   : '' );
	    $sOrder = isset($_GET["sOrder"])? $_GET["sOrder"]: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	    $sOrderBy = isset($_GET["sOrderBy"])? $_GET["sOrderBy"]	:( isset($_POST["sOrderBy"])?$_POST["sOrderBy"]: '' );
		*/	    
		$or_id = isset($_GET["or_id"])? $_GET["or_id"]:( isset($_POST["or_id"])? $_POST["or_id"] : '' );
		$perform = isset($_GET["perform"])? $_GET["perform"]:( isset($_POST["perform"])? $_POST["perform"] : '' );
	
        $condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : 
		(isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
        $condition_url  = '';
        $condition_query1='';
        $extra='';
		
		if ( empty($xt) ) {
			$xt            = 1;
			$next_record_xt	= 0 ;
		}
		else {
			$next_record_xt	= ($xt-1) * $rpp;
		}
        
   
    // EO: Upto Date        
   
    $condition_query = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".project_id ='".$or_id."' AND 
	".TABLE_PROJECT_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' ".$condition_query;
    $list	= 	NULL;
   
    
   
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $condition_url .="&perform=".$perform."&or_id=".$or_id;
    
    //$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','dashboard-ord-task.php','frmSearch');
    
	$db	= new db_local;
	$list	= NULL;
    $fields = TABLE_PROJECT_TASK_DETAILS.".id,".TABLE_PROJECT_TASK_DETAILS.".title as task_title, ".TABLE_PROJECT_TASK_DETAILS.".details,".TABLE_PROJECT_TASK_DETAILS.".last_updated, ".TABLE_PROJECT_TASK_MODULE.".title as module_title,".TABLE_PROJECT_TASK_STATUS.".title as status_title," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".resolved_date,".TABLE_PROJECT_TASK_DETAILS.".is_client,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".added_by,";
    $fields .=TABLE_PROJECT_TASK_DETAILS.".added_by_name," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".last_comment_dt," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".last_comment," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".last_comment_by_name," ;
    $fields .=TABLE_PROJECT_TASK_DETAILS.".project_id," ;
    $fields .= TABLE_PROJECT_TASK_PRIORITY.".title as priority_title, ".TABLE_PROJECT_TASK_TYPE.".title as type_title,".TABLE_PROJECT_TASK_RESOLUTION.".title as rs_title" ;
   // ProjectTask::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    $total	=	ProjectTask::getList( $db, $list, $fields , $condition_query);
	$db->close();
	$db = new db_local;
    $flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){  
			 
            if(!empty($val['is_client']) && $val['is_client']=='1'){
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                $val['is_client'] = '1';
            }else{
                $table = TABLE_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number, '.TABLE_USER.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                 $val['is_client']='0';
            }
            
             
          
            //Get Last comment bof
            $val['last_comment']='';            
            $condition_query_tlc = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".parent_id ='".$val['id']."' AND ".TABLE_PROJECT_TASK_DETAILS.".is_bug = '0' ORDER BY ".TABLE_PROJECT_TASK_DETAILS.".id DESC Limit 0,1";
            $list_tlc	= 	NULL;
            $fields_tlc = TABLE_PROJECT_TASK_DETAILS.".details";
          	ProjectTask::getList( $db, $list_tlc, $fields_tlc, $condition_query_tlc);           
            if(!empty($list_tlc)){
                $list_tlc=$list_tlc[0];
                $val['last_comment'] =  $list_tlc['details'] ;
            }
         
            //Get Last comment eof
           $flist[$key]=$val;
        }
    }
	$db->close();
    $s->assign("flist", $flist); 
	$flist=$list=null;
	//$page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
    //$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    //$page["var"][] = array('variable' => 'flist', 'value' => 'flist');
	$page["section"][] = array('container'=>'TEAM_ORDERS_TASKS', 'page' => 'dashboard-ord-task.html');
	if($ajx==1){
		
		$s->assign("variables", $variables);
		$s->assign("error_messages", $messages->getErrorMessages());
		$s->assign("success_messages", $messages->getOkMessages());
	  
		if ( isset($page["var"]) && is_array($page["var"]) ) {
			foreach ( $page["var"] as $key=>$fetch ) {
				$s->assign( $fetch["variable"], ${$fetch["value"]});
			}
		}
		
		if ( is_array($page["section"]) ) {
			$sections = count($page["section"]);
			for ( $i=0; $i<($sections-1); $i++) {
				
				$s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
			}
		}
		
		$s->display($page["section"][$sections-1]["page"]);
	}

	
}		
?>