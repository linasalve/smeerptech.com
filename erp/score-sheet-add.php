<?php
    if ( $perm->has('nc_ss_add') ) {
		include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }*/
		 // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
  /*       $condition1 = " WHERE status='".User::ACTIVE."' AND user_id NOT IN('50a9c7dbf0fa09e8969978317dca12e8',
        'e68adc58f2062f58802e4cdcfec0af2d','febc8f8ac083f5fc27e032c81e7b536a') ORDER BY f_name";    */    

		$condition1 = " WHERE status='".User::ACTIVE."' ORDER BY f_name";   		
        User::getList($db,$lst_executive,$fields,$condition1);
        // BO:  Read the Team members list ie member list in dropdown box  EOF 
		 
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
            
            if(empty($data['hide_my_name'])){
            	$data['hide_my_name']=0;
            }	            
            if(empty($data['hide_my_comment'])){
            	$data['hide_my_comment']=0;
            }
            
            
            
            if ( Scoresheet::validateAdd($data, $extra) ) { 
                
                $comment_to = implode(",",$data['comment_to']);
				$comment_to_user = str_replace(",","','",$comment_to);
                $condition_ct = " status='".User::ACTIVE."' AND user_id IN('".$comment_to_user."') ORDER BY f_name";
                $ct_field =TABLE_USER.".user_id, CONCAT_WS(' ', ".TABLE_USER.".f_name,
					".TABLE_USER.".l_name) AS name " ;
                $sql= " SELECT ".$ct_field." FROM ".TABLE_USER." WHERE ".$condition_ct;
				$db1 = new db_local; // database handle
                if ( $db->query($sql) ) {
					if ( $db->nf() > 0 ) {
						while($db->next_record()){
							
							$comment_to = $db->f('user_id') ;
							$comment_to_name = $db->f('name');
							
							
							$query	= " INSERT INTO ".TABLE_SCORE_SHEET
                            ." SET ". TABLE_SCORE_SHEET .".comment_to =  ',".$comment_to.",'"
							.",". TABLE_SCORE_SHEET .".comment_to_name = ',".$comment_to_name .",'"
							.",". TABLE_SCORE_SHEET .".particulars = '".  $data['particulars'] ."'"
							.",". TABLE_SCORE_SHEET .".score       = '".  $data['score'] ."'"
							.",". TABLE_SCORE_SHEET .".reason_id = '". $data['reason_id'] ."'"
							.",". TABLE_SCORE_SHEET .".hide_my_name = '". $data['hide_my_name'] ."'"
							.",". TABLE_SCORE_SHEET .".hide_my_comment = '". $data['hide_my_comment'] ."'"
							.",". TABLE_SCORE_SHEET .".access_level = '".$my['access_level'] ."'"
							.",". TABLE_SCORE_SHEET .".created_by = '".$data['created_by'] ."'"
							.",". TABLE_SCORE_SHEET .".created_by_name = '".$my['f_name']." ".$my['l_name']."'"
							.",". TABLE_SCORE_SHEET .".date = '". date('Y-m-d') ."'";
							
							//echo "<br/>";
							$db1->query($query);	
							$dataDetails['date'] =  date('d M Y h:i:s') ;
							$dataDetails['score'] = $data['score'] ;
							$dataDetails['particulars'] = $data['particulars'] ;
							$dataDetails['score_to'] = $comment_to_name ;
							$dataDetails['score_by'] = $my['f_name']." ".$my['l_name']  ;
							
							if ( getParsedEmail($db1, $s, 'SCORE_ADD', $dataDetails, $email) ) {
								$to     = $bcc = '';
								$to[]   = array('name' => 'Admin', 'email' => $admin_email);
								//$bcc[]   = array('name' => 'Admin', 'email' => '3groupweb@gmail.com');
								$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
								//echo $email["body"];
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
								
							 
							}

							
						}
					
						$messages->setOkMessage("Score entry has been done.");
					}
				}
				
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
       /* if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/score-sheet.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/score-sheet.php");
        }*/
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/score-sheet.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
			$hidden[] = array('name'=> 'ajx' ,'value' => $ajx);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');      
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'score-sheet-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>