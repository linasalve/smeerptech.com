<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
    include_once ( DIR_FS_INCLUDES .'/ticket-reminder.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	
	
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');    
    $added_all 	= isset($_GET["added_all"]) ? $_GET["added_all"]: ( isset($_POST["added_all"])  ? $_POST["added_all"]:'0');    
    $i 	        = isset($_GET["i"])         ? $_GET["i"]        : ( isset($_POST["i"])          ? $_POST["i"]   :     '0');    
    $j 	        = isset($_GET["j"])         ? $_GET["j"]        : ( isset($_POST["j"])          ? $_POST["j"]   :     '0');    
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20)); // Result per page	
	// $rpp =3;
    $condition_query = '';
    $condition_url = '';
    if (empty($x)){
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    $action = 'Scheduler Tickets'; 
    
    if($added){
        $messages->setOkMessage("Ticket has been created successfully. And email Notification has been sent.");
    }
    
    if($added_all){
        $messages->setOkMessage("Ticket has been created successfully for ".$i ."/". $j .". And email Notification has been sent.");
    }
    
    if ( 1) {// $perm->has('nc_tck_r')
    
        if($perform !='list_st_clients'){
            $sTypeArray     = array('Any'  =>  array(  'Any of following' => '-1'),
                                    TABLE_TICKET_REMINDER  => array( 'Ticket no.'        => 'ticket_no',
                                                                'Subject'       => 'ticket_subject',
                                                                'Ticket Text'       => 'ticket_text' 
                                                            ),
                                     TABLE_CLIENTS    =>  array(  'Client First Name'        => 'f_name',
                                                                  'Client Last Name'       => 'l_name',
                                                                  'Client Account No.'       => 'number',
                                                                  'Client Billing Name'       => 'billing_name',
                                                                  'Client Email'       => 'email',
                                                                  'Client Username.'       => 'username'
                                                                )
                                );
            
            $sOrderByArray  = array(
                                    TABLE_TICKET_REMINDER    => array('Reminder Date'  => 'ces_reminder_dt',
                                                        'Ticket no'               => 'ticket_no',
                                                        'Subject'               => 'ticket_subject',
                                                        'Ticket Create Date'    => 'ticket_date')
                                );
        
            // Set the sorting order of the user list.
            if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ){
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_comment';
                $_SEARCH['sOrder']  = $sOrder   = 'DESC';
                $order_by_table     = TABLE_TICKET_REMINDER;
            } 
            $variables['active']  = TicketReminder::ACTIVE;
            $variables['deactive']  = TicketReminder::DEACTIVE;
            $variables['closed']  = TicketReminder::CLOSED; 
           //BOF read the available status
            $variables['status'] = TicketReminder::getStatus();
            $variables['types'] = TicketReminder::getType();
            $variables['ticket_status'] = TicketReminder::getTicketStatus();
            $variables['PENDINGWITHSMEERP'] = TicketReminder::PENDINGWITHSMEERP;
            $variables['PENDINGWITHSMEERPCOZCLIENT'] = TicketReminder::PENDINGWITHSMEERPCOZCLIENT;
			$variables['ticket_close']=TicketReminder::CLOSED;
			
			
            //EOF read the available status
       }else{
                $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                        TABLE_CLIENTS   =>  array(  'Relationship Number'        => 'number',
                                                                    'Billing Name'     => 'billing_name',
                                                                    'User Name'     => 'username',
                                                                    'E-mail'        => 'email',
                                                                    'First Name'    => 'f_name',
                                                                    'Middle Name'   => 'm_name',
                                                                    'Last Name'     => 'l_name',
                                                                    'Pet Name'      => 'p_name',
                                                                    'Designation'   => 'desig',
                                                                    'Organization'  => 'org',
                                                                    'Domain'        => 'domain'
                                                                )
                                    );
                
                $sOrderByArray  = array(
                                        TABLE_CLIENTS => array('Relationship Number'   => 'number',
                                                            'User Name'     => 'username',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Last Name'     => 'l_name',
                                                            'Date of Birth' => 'do_birth',
                                                            'Date of Regis.'=> 'do_reg',
                                                            'Date of Login' => 'do_login',
                                                            'Status'        => 'status'
                                                            ),
                                    );
            
                // Set the sorting order of the user list.
                if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
                    $_SEARCH['sOrderBy']= $sOrderBy = 'do_reg';
                    $_SEARCH['sOrder']  = $sOrder   = 'DESC';
                    $order_by_table     = TABLE_CLIENTS;
                }
            
                $variables['status'] = Clients::getStatus();
        
       }
        //BOF read the available priority
        //TicketReminder::getPriority($db,$priority);
        //EOF read the available priority
        
        //use switch case here to perform action. 
        switch ($perform) {
        
            case ('add'): { 
                include (DIR_FS_NC.'/ticket-reminder-add.php'); 
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'ticket-reminder.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}else{
					$page["section"][] = array('container'=>'INDEX', 'page' => 'popup.html');
				}
                break;
            }
            case ('invflw_add'): { 
                include (DIR_FS_NC .'/ticket-reminder-add.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('invflw_view'): {			
                include (DIR_FS_NC .'/ticket-reminder-view.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
		    case ('download_file'): {
				include (DIR_FS_NC .'/ticket-reminder-template-download.php');
				break;
			}
            case ('add_all'): { 
                include (DIR_FS_NC.'/ticket-reminder-add-all.php'); 
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'ticket-reminder.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            
            /*case ('edit_user'):
            case ('edit'): {
                include (DIR_FS_MP .'/user-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'user.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            /*
            case ('select_member'): {
                include (DIR_FS_NC .'/ticket-reminder-select-member.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'ticket-reminder.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/

            case ('view'): {
                include (DIR_FS_NC.'/ticket-reminder-view.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'ticket-reminder.html');
				$page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                /* if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				} */
                break;
            }
            case ('view_header'): {
                include (DIR_FS_NC.'/ticket-reminder-header.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
            case ('search'): {
                include(DIR_FS_NC."/ticket-reminder-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'ticket-reminder.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            } 
            
			 
            case ('list'):
            default: {
                include (DIR_FS_NC .'/ticket-reminder-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'ticket-reminder.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    } else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");    
		$page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'ticket-reminder.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("action", $action);    
    $s->assign("variables", $variables);    
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
           $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	
	include_once( DIR_FS_NC ."/flush.php");
?>