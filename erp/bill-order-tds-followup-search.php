<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : 
	(isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
    $searchStr = 0;
    if ( $sString != "" ) 
    {
        $searchStr = 1;
        // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
        //$where_added = true;
        $sub_query='';        
        if ( $search_table == "Any" ) 
        {
            
            if ( $where_added ) 
            {
                $condition_query .= "OR ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ) 
            {
              
                if ( $table != "Any" ) 
                { 
                    
                    if ($table == TABLE_BILL_ORDERS) 
                    {
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                           
                          
                        }
                        
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                            
                    }elseif($table == TABLE_USER){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                         
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_CLIENTS){
                    
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                             $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;

                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }
                    
                }else{
                
                
                
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= "AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." 
				LIKE '%".trim($sString) ."%' )" ;
               
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
    
    
    // BO: Status data.
    $sStatusStr = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ( $chk_status == 'AND' || $chk_status == 'OR') &&  isset($sStatus)){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".status IN ('". $sStatusStr ."') ";        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
        
        
    }
    
    // EO: Status data.
	
	// BO: Followup Status data.
    $fStatusStr = '';
    $chk_fstatus = isset($_POST["chk_fstatus"])   ? $_POST["chk_fstatus"]  : (isset($_GET["chk_fstatus"])   ? $_GET["chk_fstatus"]   :'');
    $fStatus    = isset($_POST["fStatus"])      ? $_POST["fStatus"]     : (isset($_GET["fStatus"])      ? $_GET["fStatus"]      :'');
    if ( ( $chk_fstatus == 'AND' || $chk_fstatus == 'OR') &&  isset($fStatus)){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($fStatus)){
             $fStatusStr = implode("','", $fStatus) ;
             $fStatusStr1 = implode(",", $fStatus) ;
             
        }else{
            $fStatusStr = str_replace(',',"','",$fStatus);
            $fStatusStr1 = $fStatus ;
            $fStatus = explode(",",$fStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".followup_status IN ('". $fStatusStr ."') ";        
        $condition_url          .= "&chk_fstatus=$chk_fstatus&fStatus=$fStatusStr1";
        $_SEARCH["chk_fstatus"]  = $chk_fstatus;
        $_SEARCH["fStatus"]     = $fStatus;
        
        
    }
    
    // EO: Followup Status data.
    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
 
    // BO: From Date
    if ( $chk_date_from == 'AND' || $chk_date_from == 'OR') {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_BILL_ORDERS .".".$dt_field." >= '". $dfa ."'";
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
         $_SEARCH["dt_field"]       = $dt_field;
    }  
    // EO: From Date
    
    // BO: Upto Date
    if ( $chk_date_to == 'AND' || $chk_date_to == 'OR') {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_BILL_ORDERS .".".$dt_field." <= '". $dta ."'";
        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }
    
    
     
    
    $chk_old_update        = isset($_POST["chk_old_update"])      ? $_POST["chk_old_update"]         : (isset($_GET["chk_old_update"])      ? $_GET["chk_old_update"]      :'');
    if(isset($chk_old_update ) && $chk_old_update!=''){
		 $searchStr = 1;
         if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= " ". TABLE_BILL_ORDERS .".old_updated = '". $chk_old_update ."' AND ".TABLE_BILL_ORDERS.".is_old='1'";
         
        $condition_url          .= "&chk_old_update=$chk_old_update";
        $_SEARCH["chk_old_update"] = $chk_old_update ;
     
    }
	
	
	 $chk_cron_created        = isset($_POST["chk_cron_created"])      ? $_POST["chk_cron_created"]         : 
	 (isset($_GET["chk_cron_created"])      ? $_GET["chk_cron_created"]      :'');
    if(isset($chk_cron_created ) && $chk_cron_created!=''){
	 $searchStr = 1;
         if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= " ". TABLE_BILL_ORDERS .".cron_created = '". $chk_cron_created ."' ";
         
        $condition_url          .= "&chk_cron_created=$chk_cron_created";
        $_SEARCH["chk_cron_created"] = $chk_cron_created ;
     
    }
    
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/bill-order-tds-followup-list.php');
?>