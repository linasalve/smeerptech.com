<?php
if ( $perm->has('nc_letr_list') ) {

 
        
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    
	
    // To count total records.
    $list	= 	NULL;
    $total	=	Letters::getList( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','letters.php','frmSearch');
    
    $list	= NULL;
    $fields = TABLE_LETTERS.'.*' ;
    Letters::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
	 
    
    
    $variables['can_view_list'] = false;
    $variables['can_add'] = false;
	$variables['can_edit'] = false;
	$variables['can_delete'] = false;
	$variables['can_sendmail'] = false;
	 
    // Set the Permissions.   
    if ( $perm->has('nc_letr_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_letr_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_letr_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_letr_delete') ) {
        $variables['can_delete'] = true;
    }
	 

    //repository_path mime_content_type();
	
    $variables['path'] = DIR_WS_LETTERS_FILES;
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
	
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'letters-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
