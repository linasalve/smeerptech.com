<?php

    if ( $perm->has('nc_st_all_add') ) {
        
		$ticket_id     = isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) 
		? $_POST["ticket_id"] : '' );		
        $subUserDetails=null;
       
        
        $_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates=null;
        $condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."'";
        SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'
			.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
    
        $domains = array();
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["status"] = SupportTicket::getTicketStatusList();
		
        
        if( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
          
                if ( SupportTicket::validateStAllAdd($data, $extra) ) {
                    
                   
                    $ticket_no  =  SupportTicket::getStAllNewNumber($db);
                    $attachfilename=$ticket_attachment_path='';
                    if(!empty($data['ticket_attachment'])){
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['ticket_attachment']["name"]); 
                        $ext = $filedata["extension"] ;  
                        $attachfilename = 'stall_'.$ticket_no.".".$ext ;
                        $data['ticket_attachment'] = $attachfilename;                        
                        $ticket_attachment_path = DIR_WS_ST_FILES;                        
                        if (move_uploaded_file($files['ticket_attachment']['tmp_name'], 
							DIR_FS_ST_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_ST_FILES."/".$attachfilename, 0777);
                        }
                    }
                    
                    $mail_to_all_su=0;
                    if(isset($data['mail_to_all_su']) ){
                        $mail_to_all_su=1;
                    }                   
                    $mail_client=0;
                    if(isset($data['mail_client']) ){
                        $mail_client=1;
                    }
					$mail_vendor=0;
                    if(isset($data['mail_vendor'])){
                        $mail_vendor = 1;
                    }
					$email = $data['email'] ;
					$condition ='';
					if($mail_to_all_su ==1 && $mail_client ==1 && $mail_vendor ==1){
						
					}elseif($mail_to_all_su==1 && $mail_client==1 && $mail_vendor ==0){					
						$condition = " AND ".TABLE_CLIENTS.".member_type LIKE '%".Clients::MEMBER_CLIENTS."%' ";
					}elseif($mail_to_all_su==1 && $mail_client==0 && $mail_vendor ==1){
						$condition = " AND ".TABLE_CLIENTS.".member_type LIKE '%".Clients::MEMBER_VENDORS."%' ";
					}elseif($mail_to_all_su ==0 && $mail_client ==1 && $mail_vendor ==1){					 
						$condition = " AND ".TABLE_CLIENTS.".parent_id ='' ";					 
					}elseif($mail_to_all_su ==0 && $mail_client ==1 && $mail_vendor ==0){
						$condition = " AND ".TABLE_CLIENTS.".parent_id ='' AND 
						".TABLE_CLIENTS.".member_type LIKE '%".Clients::MEMBER_CLIENTS."%' " ;
					}elseif($mail_to_all_su ==0 && $mail_client ==0 && $mail_vendor ==1){
						$condition = " AND ".TABLE_CLIENTS.".parent_id ='' AND 
						".TABLE_CLIENTS.".member_type LIKE '%".Clients::MEMBER_VENDORS."%' " ;
					}elseif($mail_to_all_su ==0 && $mail_client ==1 && $mail_vendor ==1){
						$condition = " AND ".TABLE_CLIENTS.".parent_id =''" ;
					}    
					
					$total_members = 0;
                    $per_batch = PER_BATCH;
					$total_batches=0;
					if(empty($email)){
						$sqlCount = "SELECT COUNT(user_id) as total FROM ".TABLE_CLIENTS." WHERE 
						".TABLE_CLIENTS.".status='".Clients::ACTIVE."' AND ".TABLE_CLIENTS.".check_email='0' 
						".$condition;
						$db->query($sqlCount);
						if( $db->nf() > 0 ){
							while ($db->next_record()) {
								$total_members	= $db->f('total');
							}
						}
						if($total_members > 0){
							$total_batches =  ceil($total_members/$per_batch)  ;	
						}
					}else{
						$total_batches=1;
					}
										
                    $query = "INSERT INTO "	. TABLE_TICKET_ALL ." SET "
						. TABLE_TICKET_ALL .".ticket_no         = '". $ticket_no ."', "
						. TABLE_TICKET_ALL .".ticket_subject    = '". $data['ticket_subject'] ."', "
						. TABLE_TICKET_ALL .".ticket_text       = '". $data['ticket_text'] ."', "
						. TABLE_TICKET_ALL .".test_email       = '". $data['test_email'] ."', "
						. TABLE_TICKET_ALL .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_TICKET_ALL .".template_id       = '". $data['template_id'] ."', "
						. TABLE_TICKET_ALL .".template_file_1   = '". $data['template_file_1'] ."', "
                        . TABLE_TICKET_ALL .".template_file_2   = '". $data['template_file_2'] ."', "
                        . TABLE_TICKET_ALL .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_TICKET_ALL .".mail_vendor       = '".$mail_vendor."', "
						. TABLE_TICKET_ALL .".mail_client       = '".$mail_client."', "
						. TABLE_TICKET_ALL .".mail_to_all_su    = '".$mail_to_all_su."', "
						. TABLE_TICKET_ALL .".total_members     = '".$total_members."', "
						. TABLE_TICKET_ALL .".per_batch         = '".$per_batch."', "
						. TABLE_TICKET_ALL .".total_batches     = '".$total_batches."', "
						. TABLE_TICKET_ALL .".email       		= '".$email."', "
						. TABLE_TICKET_ALL .".status        	= '0', "
						. TABLE_TICKET_ALL .".ticket_date       = '". time() ."' ,"
						. TABLE_TICKET_ALL .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_TICKET_ALL .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_TICKET_ALL .".created_by= '". $my['uid'] ."', "
						. TABLE_TICKET_ALL .".created_by_name    = '". $my['f_name']." ". $my['l_name'] ."'" ;

                 
                                
                    if($db->query($query) && $db->affected_rows() > 0){
                        
                        $variables['hid'] = $db->last_inserted_id() ;						
					  	
						$query1 = "INSERT INTO ". TABLE_TICKET_ALL_BATCHES ." SET "
						. TABLE_TICKET_ALL_BATCHES .".ticket_no         = '". $ticket_no ."', "
						. TABLE_TICKET_ALL_BATCHES .".ticket_id         = '". $variables['hid']."', "
						. TABLE_TICKET_ALL_BATCHES .".ticket_subject    = '". $data['ticket_subject'] ."', "
						. TABLE_TICKET_ALL_BATCHES .".ticket_text       = '". $data['ticket_text'] ."', "
						. TABLE_TICKET_ALL_BATCHES .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_TICKET_ALL_BATCHES .".template_id       = '". $data['template_id'] ."', "
						. TABLE_TICKET_ALL_BATCHES .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_TICKET_ALL_BATCHES .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_TICKET_ALL_BATCHES .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_TICKET_ALL_BATCHES .".mail_vendor       = '".$mail_vendor."', "
						. TABLE_TICKET_ALL_BATCHES .".mail_client       = '".$mail_client."', "
						. TABLE_TICKET_ALL_BATCHES .".mail_to_all_su    = '".$mail_to_all_su."', "
						. TABLE_TICKET_ALL_BATCHES .".ticket_date       = '". time() ."' ,"
						. TABLE_TICKET_ALL_BATCHES .".total_batches     = '".$total_batches."', "
						. TABLE_TICKET_ALL_BATCHES .".pending_batches   = '".$total_batches."', "
						. TABLE_TICKET_ALL_BATCHES .".total_members     = '".$total_members."', "
						. TABLE_TICKET_ALL_BATCHES .".per_batch         = '".$per_batch."', "
						. TABLE_TICKET_ALL_BATCHES .".email        		= '".$email."', "
						. TABLE_TICKET_ALL_BATCHES .".do_e    			= '".date('Y-m-d H:i:s')."' ";                
                 
						$db->query($query1);
						
						
                        /**************************************************************************
                        * Send the notification to the ticket owner and the Administrators
                        * of the new Ticket being created
                        **************************************************************************/
                        $data['ticket_no']  =   $ticket_no ;                       
                        $data['mticket_no'] =   $ticket_no ;
                        $data['replied_by'] =   $my['f_name']." ".$my['l_name'] ;
                        $data['subject']    =   $_ALL_POST['ticket_subject'] ;
                        $data['text']    	=   $_ALL_POST['ticket_text'] ;
                        $data['attachment'] =   $attachfilename ;
						$file_name = '';
						if(!empty($attachfilename)){
							$file_name[] = DIR_FS_ST_FILES ."/". $attachfilename;
						}
						
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
						
						if(!empty($data['ss_file_1'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_1'];
						}
						if(!empty($data['ss_file_2'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_2'];
						}
						if(!empty($data['ss_file_3'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_3'];
						}
                        //Send a copy to check the mail for clients
						 
                         
						if ( getParsedEmail($db, $s, 'EMAIL_STALL_MEMBER', $data, $email) ) { 
							if(!empty($data['test_email'])){
								$smeerp_support_email=$data['test_email'];
							}
							if(!empty($smeerp_support_email)){
                                $to     = '';
                                $cc_to_sender=$cc=$bcc='';
                                $to[]   = array('name' => $smeerp_support_name, 
                                'email' => $smeerp_support_email);
								$from   = $reply_to = array('name' => $email["from_name"], 'email' => 
								$email['from_email']);
                                //echo $email["body"] ;
								 
								SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 	
								$email["isHTML"],
                                $cc_to_sender,$cc,$bcc,$file_name); 
								
                            }                                
                        }   
						 
                        // Send Email to the admin BOF
                        $email = NULL;
                       
                        //$data['main_client_name']   = $main_client_name;
                        //$data['billing_name']   = $billing_name;
                        $cc_to_sender= $cc = $bcc = Null ;
                        if ( getParsedEmail($db, $s, 'EMAIL_STALL_ADMIN', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $admin_name , 'email' => $admin_email);                           
                            $from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);
                            //print_r($to);
                            //print_r($from);
                            //echo $email["body"];
                            //echo $email["subject"];
							
                           /*  SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                            $cc_to_sender,$cc,$bcc,$file_name); */
                        }                        
                        // Send Email to the admin EOF
                        $messages->setOkMessage("New Support Ticket has been created.And email Notification has been sent.");
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
                }
        }else{
		
			$fields = TABLE_TICKET_ALL .'.*'  ;            
            $condition_query = " WHERE (". TABLE_TICKET_ALL .".ticket_id = '". $ticket_id ."' )";
           
            if (SupportTicket::getStAllList( $db,$_ALL_POST,$fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
				 
            }  
		}
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
            header("Location:".DIR_WS_NC."/support-ticket-all.php?perform=add&added=1");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/support-ticket-all-list.php');
        }
        else {
        
		
			
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
			
			$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
			$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'support-ticket-all-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
        
        
    }
?>