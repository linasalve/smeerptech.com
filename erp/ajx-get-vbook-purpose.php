<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
 
	include_once ( DIR_FS_INCLUDES .'/visitor-book.inc.php');
	include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
	
	
	
    $pid = isset($_GET['pid']) ? $_GET['pid'] : '';
	 
	$pList = Visitor::getPurposeList();
	
    if(!empty($pid) ){
		echo $pList[$pid];
	}	


?>
