<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
    
if ( $perm->has('nc_ld_or_flw') ) {  
	include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
	
	$company_ip = isset ($_GET['company_ip']) ? $_GET['company_ip'] : ( isset($_POST['company_ip'] ) ? $_POST['company_ip'] :'');
	$followup_id = isset ($_GET['followup_id']) ? $_GET['followup_id'] : ( isset($_POST['followup_id'] ) ? $_POST['followup_id'] :'');
	$lead_id = isset ($_GET['lead_id']) ? $_GET['lead_id'] : ( isset($_POST['lead_id'] ) ? $_POST['lead_id'] :'');
	
	$added_by = isset ($_GET['added_by']) ? $_GET['added_by'] : ( isset($_POST['added_by'] ) ? $_POST['added_by'] :'');
	$_ALL_POST =null;
    
	if(array_key_exists('btnGo',$_GET) && !empty($company_ip)){
	
        $data       = processUserData($_GET);
         
         if( $messages->getErrorMessageCount() <= 0){
               
                $query  = " UPDATE ". TABLE_LEADS_ORD_FLW
                ." SET ". TABLE_LEADS_ORD_FLW .".company_inputs = '".$data['company_ip']."'"   
                ." WHERE id = '". $followup_id ."'";                
				$db->query($query);
				
				//Get details of $followup_id who send the query to send the company i/ps on his mail id bof
				$sql= "SELECT ".TABLE_LEADS_ORD_FLW.".your_queries, 
				".TABLE_LEADS_ORD_FLW.".company_inputs,
				".TABLE_LEADS_ORD_FLW.".proposal_name,
				".TABLE_LEADS_ORD_FLW.".added_by_name,
				".TABLE_LEADS_ORD_FLW.".prospect_id, ".TABLE_USER.".email, ".TABLE_USER.".email_1, ".TABLE_USER.".email_2 
				FROM ".TABLE_LEADS_ORD_FLW." LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id = ".TABLE_LEADS_ORD_FLW.".added_by
				WHERE ".TABLE_LEADS_ORD_FLW.".id ='".$followup_id."'" ;
				$db->query($sql);
				if ( $db->nf()>0 ){
					while ( $db->next_record() ) { 
						$data['your_queries'] = $db->f('your_queries'); 
						$data['prospect_id'] = $db->f('prospect_id'); 
						$data['company_inputs'] = $db->f('company_inputs'); 
						$data['lead_details'] = $db->f('proposal_name'); 
						$data['query_by_name'] = $db->f('added_by_name'); 
						$data['email1'] = $db->f('email'); 
						$data['email_11'] = $db->f('email_1'); 
						$data['email_22'] = $db->f('email_2'); 
					}
				}
				
				$sql = " SELECT ". TABLE_SALE_LEADS .'.lead_id '.','. TABLE_SALE_LEADS.'.number AS c_number'
						.','. TABLE_SALE_LEADS .'.f_name AS c_f_name'
						.','. TABLE_SALE_LEADS .'.l_name AS c_l_name'
						.','. TABLE_SALE_LEADS .'.billing_name AS billing_name'
						.','. TABLE_SALE_LEADS .'.company_name AS c_company_name'
						.','. TABLE_SALE_LEADS .'.email '
						.','. TABLE_SALE_LEADS .'.email_1 '
						.','. TABLE_SALE_LEADS .'.email_2 '
						.','. TABLE_SALE_LEADS .'.email_3 '
						.','. TABLE_SALE_LEADS .'.email_4 '
						.','. TABLE_SALE_LEADS .'.mobile1 '
						.','. TABLE_SALE_LEADS .'.mobile2 '
						.','. TABLE_SALE_LEADS .'.mobile3 FROM '.TABLE_SALE_LEADS." WHERE lead_id = ".$data['prospect_id'] ;
				$db->query($sql);
				if ( $db->nf()>0 ){
					while ( $db->next_record() ) {
						 $data['email']   = $db->f('email')  ;
						$data['email_1'] = $db->f('email_1') ;
						$data['email_2'] = $db->f('email_2') ;
						$data['email_3'] = $db->f('email_3') ;
						$data['email_4'] = $db->f('email_4') ;
						$data['mobile1'] = $db->f('mobile1') ;
						$data['mobile2'] = $db->f('mobile2') ;
						$data['mobile3'] = $db->f('mobile3') ;
						$data['name'] 	 = $db->f('c_f_name')." ".$db->f('c_l_name') ;
						 $data['company_name'] 	= $db->f('c_company_name') ;
					}
				}
				 
				
				//Email						 
				if(!empty($data['company_ip'])){
					//Send on mail bof 
					$data['link'] = DIR_WS_NC."/"."leads-order.php?perform=followup&id=".$data['lead_id'];
					if( getParsedEmail($db, $s, 'EMAIL_LDF_QRY_RPL', $data, $email) ){
					
						$to[]   = array('name' => $data['email1'], 'email' => $data['email1']);                                   
						$to[]   = array('name' => $data['email_11'], 'email' => $data['email_11']);                                   
						$to[]   = array('name' => $data['email_22'], 'email' => $data['email_22']); 
						$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 'email' => $smeerp_client_email);						
						$from   = $reply_to = array('name' => $my["email"],'email' => $my['email']);
						 
						
						SendMail($to,$from,$reply_to,$email["subject"],$email["body"],$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);  
					}
					//Send on mail eof
				}
				//Get details of $followup_id who send the query to send the company i/ps on his mail id eof
				
                $message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Company Inputs updated successfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					$message .= "|".$followup_id;
					
					
					echo $message;
         }        
    }  
   
 
}else {
    //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
	
}
  
?>