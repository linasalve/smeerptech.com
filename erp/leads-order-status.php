<?php
	if ( $perm->has('nc_ld_or_status') ) {
	
		include_once ( DIR_FS_INCLUDES .'/leads-ticket.inc.php');
		include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
		include_once (DIR_FS_INCLUDES .'/user.inc.php');
        $or_id	= isset($_GET["or_id"])  ? $_GET["or_id"] 	: ( isset($_POST["or_id"]) 	? $_POST["or_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        if ( $perm->has('nc_bl_or_status_al') ) {
            $access_level += 1;
        }
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages,
						'my'=>&$my,
						's'=>&$s
                    );
        LeadsOrder::updateStatus($or_id, $status, $extra);
        
        
        
        $perform = 'search';
        // Display the  list.
        include ( DIR_FS_NC .'/leads-order-search.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>