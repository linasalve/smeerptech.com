<?php
    if ( $perm->has('nc_cat_edit') ) {	
		$cat_id	= isset($_GET["cat_id"]) ? $_GET["cat_id"] 	: ( isset($_POST["cat_id"]) ? $_POST["cat_id"] : '' );
	
		$data		= "";
		$_ALL_POST	= "";
		if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
			$_ALL_POST 	= $_POST;
			$data		= processUserData($_ALL_POST);
	
			$extra = array( 'db' 				=> &$db,
							'messages' 			=> $messages
						);
			
			if (Categories::validateUpdate($data,$extra)) {
					
				$query	= " UPDATE ".TABLE_SETTINGS_CATEGORY
							." SET ". TABLE_SETTINGS_CATEGORY .".sc_parent_id	= '". $data["category"] ."'"
								.",". TABLE_SETTINGS_CATEGORY .".sc_title 		= '". $data['sc_title'] ."'"
								.",". TABLE_SETTINGS_CATEGORY .".sc_description	= '". $data["sc_description"] ."'"
							." WHERE sc_cat_id = '". $cat_id ."' ";
	
				if ( $db->query($query) ) {
					if ( $db->affected_rows() > 0 ) {
						$messages->setOkMessage("Category has been updated successfully.");
					}
					else {
						$messages->setOkMessage("Problem updating category. Try later.");
					}
				}
				// Clear all the data.
				$_ALL_POST	= "";
				$data		= "";
			}
		}
		
		// Get list of category and subcategory.
		Categories::getComboMenus();
		$option   = null;
		// fetch parent id from list.
		$query ="SELECT sc_parent_id FROM ".TABLE_SETTINGS_CATEGORY. " WHERE sc_cat_id='".$cat_id."'";
		$db->query($query);
		if ($db->nf() > 0){
			$db->next_record();
			$parent_id = $db->f('sc_parent_id');
		}
		
		foreach ($output as $key => $val) {
	
				if ($parent_id==$key) { 
					$select = "selected";
				}
				else {
					$select ='';
				}
			$option.="<option value='$key' $select >$val</option>";
		}
		
		
		// Check if the Form to add is to be displayed or the control is to be sent to the List page.
		if ( isset($_POST['btnCancel'])
				|| (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$variables['hid'] = $cat_id;
	
			if ( isset($_POST['action']) && $_POST['action'] == 'search' ) {
				include ( DIR_FS_NC .'/category-search.php');
			}
			else {
				include ( DIR_FS_NC .'/category-list.php');
			}
		}
		else {
				
			$condition_query    = " WHERE sc_cat_id = '". $cat_id ."' ";
			$data               = NULL;
			if ( Categories::getList($db, $data, '*', $condition_query) > 0 ) {
				$data       =   $data['0'];
				
				// These parameters will be used when returning the control back to the List page.
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}
				$hidden[]   = array('name'=> 'perform' , 'value' => 'edit');
				$hidden[]   = array('name'=> 'act' , 'value' => 'save');
				$hidden[]   = array('name'=> 'cat_id' , 'value' => $cat_id);
			}
	
			$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'data', 'value' => 'data');
			$page["var"][] = array('variable' => 'option', 'value' => 'option');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'category-edit.html');
		}
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
