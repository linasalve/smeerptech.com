<?php

    if ( $perm->has('nc_cl_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_cl_edit_al') ) { 
        
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/incoming-calls.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Incomingcalls::validateUpdate($data, $extra) ) {
                $query  = " UPDATE ". TABLE_INCOMING_CALLS
                            ." SET ". TABLE_INCOMING_CALLS .".caller_name = '".       $data['caller_name'] ."'"
                                .",". TABLE_INCOMING_CALLS .".company_name = '".      $data['company_name'] ."'"
                                .",". TABLE_INCOMING_CALLS .".account_no = '".         $data['account_no'] ."'"
                                .",". TABLE_INCOMING_CALLS .".is_client = '".         $data['is_client'] ."'"
                                .",". TABLE_INCOMING_CALLS .".attend_by = '".         $data['attend_by'] ."'"
                                .",". TABLE_INCOMING_CALLS .".contact_no = '".        $data['contact_no'] ."'"
                                .",". TABLE_INCOMING_CALLS .".purpose = '".           $data['purpose'] ."'"
                                .",". TABLE_INCOMING_CALLS .".remark = '".            $data['remark'] ."'"
                                 .",". TABLE_INCOMING_CALLS .".type = '".             $data['type'] ."'"
                                .",". TABLE_INCOMING_CALLS .".ps = '".                $data['ps'] ."'"
                                .",". TABLE_INCOMING_CALLS .".date = '".              $data['date'] ."'"
                            ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Calls entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_INCOMING_CALLS .'.*'  ;            
            $condition_query = " WHERE (". TABLE_INCOMING_CALLS .".id = '". $id ."' )";
            /*$condition_query .= " AND ( ";
            // If my has created this Order.
            $condition_query .= " (". TABLE_INCOMING_CALLS .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_INCOMING_CALLS .".access_level < $access_level ) ";              
            // Check if the User has the Right to Edit Orders created by other Users.
            
            if ( $perm->has('nc_cl_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_cl_edit_ot_al') ) { 
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_INCOMING_CALLS. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_INCOMING_CALLS .".access_level < $access_level_o ) ";
            }
            $condition_query .= " )";*/
          
            if ( Incomingcalls::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                //if ( $_ALL_POST['access_level'] < $access_level ) {
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                /*}
                else {
                    $messages->setErrorMessage("You do not have the Permission to view the Record with the current Access Level.");
                }*/
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/incoming-calls-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'ps_arr', 'value' => 'ps_arr');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'incoming-calls-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
