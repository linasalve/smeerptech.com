<?php
if ( $perm->has('nc_sms_pr_list') ) {

    // If the entry is created by the my.
    /*$access_level   = $my['access_level'];
    if ( $perm->has('nc_ab_list_al') ) {
        $access_level += 1;
    }*/
    
   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    $lst_api=null;
    $condition_query = " LEFT JOIN ".TABLE_SMS_GATEWAY." ON ".TABLE_SMS_GATEWAY.".id=".TABLE_SMS_PURCHASE.".gateway_id 
                         LEFT JOIN ".TABLE_SMS_API." ON ".TABLE_SMS_API.".id=".TABLE_SMS_PURCHASE.".api_id ".$condition_query;
                         //LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id=".TABLE_SMS_API.".created_by ".$condition_query;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $fields = TABLE_SMS_PURCHASE.'.id ' ;
    $total	=	SmsPurchase::getDetails( $db, $list,'', $condition_query);

    $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
    // $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';

    $list	= NULL;
    //$fields = TABLE_SMS_PURCHASE.'.*' ;
    $fields = TABLE_SMS_PURCHASE .'.*'
                    .','. TABLE_SMS_GATEWAY.'.company as gateway_name, '.TABLE_SMS_API.'.name as api_name';
    SmsPurchase::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    // Set the Permissions.
   
    $variables['can_view_list'] = false;
    $variables['can_add'] = false;
    //$variables['can_edit'] = false;
    $variables['can_renew'] = false;
    $variables['can_delete'] = false;
    $variables['can_view_h'] = false;
    
    if ( $perm->has('nc_sms_pr_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_sms_pr_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_sms_pr_renew') ) {
        $variables['can_renew'] = true;
    }
    if ( $perm->has('nc_sms_pr_delete') ) {
        $variables['can_delete'] = true;
    }
    if ( $perm->has('nc_sms_pr_view_h') ) {
        $variables['can_view_h'] = true;
    }
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-purchase-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
