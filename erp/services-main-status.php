<?php
    if ( $perm->has('nc_sr_status') ) {
        $ss_id= isset($_GET["ss_id"])? $_GET["ss_id"] : ( isset($_POST["ss_id"])? $_POST["ss_id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
/*        if ( $perm->has('nc_uc_status_al') ) {
            $access_level += 1;
        }*/
        
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        Services::updateStatus($ss_id, $status, $extra);
         $variables['hid'] = $ss_id;
        // Display the  list.
        include ( DIR_FS_NC .'/services-main-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>