<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
				   
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/suggestions.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
 
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    
	include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$do_r 			= isset($_GET["do_r"])         ? $_GET["do_r"]        : ( isset($_POST["do_r"])          ? $_POST["do_r"]       :       date('d/m/Y'));
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $added 			= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined(        'RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
     $where_added    = false;
	 
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
    $condition_query ='';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
   
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    $condition_url='';
    
    if($added){
         $messages->setOkMessage("Task entry has been done.");
    }
    
  
    
    if ( $perm->has('nc_sugg') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_SUGGESTIONS   =>  array( 
															'No' => 'task_no',                                                            'Suggestion' => 'comment',
                                                            //'Status'        => 'status'
                                                        ),
                                );
        
        $sOrderByArray  = array(
                                TABLE_SUGGESTIONS => array(
                                                              'Date of Entry'  => 'do_e'
                                                            
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SUGGESTIONS;
        }
        
         // Read the available Status 
        $variables['status'] = Suggestions::getStatus();
        /* $variables['priority'] = Suggestions::getPriority();
        $variables['time_type'] = Suggestions::getTimeType(); */
       
       //hrs, min array
      $lst_hrs = $lst_min = NULL;
         /*  $lst_hrs[0] = 'Hr';
        for($i=1; $i<=12;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
         */
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {
                include (DIR_FS_NC.'/suggestions-add.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'suggestions.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}else{
					$page["section"][] = array('container'=>'INDEX', 'page' => 'popup.html');
				}
                break;
            }
			 
             
            
           case ('acomm'): {
                include (DIR_FS_NC .'/suggestions-add-comment.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
             
            
            
			 
			
            case ('search'): {
                include(DIR_FS_NC."/suggestions-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'suggestions.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            
            case ('list'):
			default:
            {
                include (DIR_FS_NC .'/suggestions-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'suggestions.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
             
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'suggestions.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("perform", $perform);
    $s->assign("variables", $variables);
    $s->assign("lst_hrs", $lst_hrs);
    $s->assign("lst_min", $lst_min);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>