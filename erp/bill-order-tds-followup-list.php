<?php

    if ( $perm->has('nc_st') && $perm->has('nc_st_flwtds_inv') ) {
	
		include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
		include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
		
		// Status
		$variables["blocked"]           = Order::BLOCKED;
		$variables["active"]            = Order::ACTIVE;
		$variables["pending"]           = Order::PENDING;
		$variables["deleted"]           = Order::DELETED;
		$variables["completed"]         = Order::COMPLETED;
		$variables["processed"]         = Order::PROCESSED;
   
        
	   
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = '';			 
            $condition_query = " WHERE ("
                                     ." ". TABLE_BILL_ORDERS .".followup_status = '". Order::FOLLOWUPTDS ."'"                                .")";
        }else{
			$condition_query .= " AND ( "
                              ." ". TABLE_BILL_ORDERS .".followup_status = '". Order::FOLLOWUPTDS ."'"
                            .")";
		}
     
        

		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
        //By default search in On
        $_SEARCH["searched"]    = true ;
        
        if($searchStr==1){
          
            $fields = '';
            $total	=	Order::getDetails( $db, $list, $fields, $condition_query);
            $extra_url  = '';
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            $list	= NULL;
            $fields = TABLE_BILL_ORDERS .'.id'
                    .','. TABLE_BILL_ORDERS .'.number'
                    .','. TABLE_BILL_ORDERS .'.order_title'
                    .','. TABLE_BILL_ORDERS .'.access_level'
                    .','. TABLE_BILL_ORDERS .'.team'
                    .','. TABLE_BILL_ORDERS .'.details'
                    .','. TABLE_BILL_ORDERS .'.do_d'
                    .','. TABLE_BILL_ORDERS .'.do_c'
                    .','. TABLE_BILL_ORDERS .'.do_o'
                    .','. TABLE_BILL_ORDERS .'.status'
                    .','. TABLE_BILL_ORDERS .'.followup_status'
                    //.','. TABLE_BILL_INV .'.number as inv_no'
                    .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                    .','. TABLE_CLIENTS .'.number AS c_number'
                    .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                    .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                    .','. TABLE_CLIENTS .'.billing_name'
                    .','. TABLE_CLIENTS .'.email AS c_email'
                    .','. TABLE_CLIENTS .'.status AS c_status';
             Order::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        
      
        $flist = array();
        if(!empty($list)){
            foreach( $list as $key=>$val){   
			   
     		   $val['invoice_pf_id']= '';
			   $val['invoice_pf_no']='';
			   
               $val['invoice_id']= '';
			   $val['invoice_number']='';
			   
               $is_invoice_created = 0;
			   $sql= " SELECT id, number, do_i as inv_date  FROM ".TABLE_BILL_INV." WHERE 
				".TABLE_BILL_INV.".or_no='".$val['number']."' AND 
				".TABLE_BILL_INV.".status !='".Invoice::DELETED."' LIMIT 0,1";
			   $db->query($sql);               
			   if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						  $val['invoice_id']= $db->f('id');
						  $val['invoice_no']= $db->f('number');
						   $val['inv_date']= $db->f('inv_date');
					}
				}
				//
				$sql= " SELECT id, number,do_i as pinv_date  FROM ".TABLE_BILL_INV_PROFORMA." WHERE 
				".TABLE_BILL_INV_PROFORMA.".or_no='".$val['number']."' AND 
				".TABLE_BILL_INV_PROFORMA.".status !='".InvoiceProforma::DELETED."' LIMIT 0,1";
			    $db->query($sql);               
			    if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						  $val['invoice_pf_id']= $db->f('id');
						  $val['invoice_pf_no']= $db->f('number');
						   $val['pinv_date']= $db->f('pinv_date');
					}
				}
				
				//check order id followup in ST BOF
				$val['ticket_id']=0;
				$sql2 = "SELECT ticket_id,last_comment,ticket_text FROM ".TABLE_ST_TICKETS." WHERE 
				(".TABLE_ST_TICKETS.".flw_ord_id = '".$val['id']."') AND ".TABLE_ST_TICKETS.".ticket_child = 0 LIMIT 0,1 ";
                if ( $db->query($sql2) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $val['ticket_id']= $db->f('ticket_id');
                           $val['last_comment']= $db->f('last_comment');
						   if(empty($val['last_comment'])){
								$val['last_comment'] = $db->f('ticket_text');
						   }
                        }
                    }                   
                }
				//check order id followup in ST EOF 
                $flist[$key]=$val;
            }
        }
       /*
	   
	   
	   */
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_create_invoice']= false;
        $variables['can_followup'] = false;
        $variables['can_bifurcate'] = false;
		$variables['can_view_inv'] = false;
		$variables['can_view_client_details'] = false;
        
        if ( $perm->has('nc_bl_or_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_bl_or_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_bl_or_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_bl_or_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_bl_or_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_bl_or_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_bl_inv_add') ) {
            $variables['can_create_invoice'] = true;
        }
        
		if (  $perm->has('nc_uc') && $perm->has('nc_uc_details') ) {
            $variables['can_view_client_details'] = true;
        }
		
        if ( $perm->has('nc_bl_or_bifur') ) {
            $variables['can_bifurcate'] = true;
        }
		if ( $perm->has('nc_st') && $perm->has('nc_st_flwtds_inv') ) {
            $variables['can_followup'] = true;
        }
		if( $perm->has('nc_bl_inv') && $perm->has('nc_bl_inv_details') ) {
            $variables['can_view_inv']     = true;
        }
		$variables['tdsflw'] = Order::FOLLOWUPTDS;
		$variables['ceoflw'] = Order::FOLLOWUPCEO;
		$variables['noneflw'] = Order::FOLLOWUPPENDING;
		$variables['activeflw'] = Order::FOLLOWUPACTIVE;
		
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-tds-followup-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>