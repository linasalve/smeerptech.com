<?php
	if ( $perm->has('nc_urht_edit') ) {
        $right_id	= isset($_GET["right_id"]) ? $_GET["right_id"] 	: ( isset($_POST["right_id"]) ? $_POST["right_id"] : '' );

		//get all the parent rights
		$rights = NULL;
		UserRights::getParent($db,$rights);
    
        $data		= "";
        $_ALL_POST	= "";
        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 				=> &$db,
                            'data' 				=> $data,
                            'messages' 			=> $messages,
                        );
            
            if (UserRights::validateUpdate($data, $messages, $extra, $right_id)) {
                
                $query	= " UPDATE ".TABLE_USER_RIGHTS
                            ." SET ". TABLE_USER_RIGHTS .".title 			= '". $data['title'] ."'"
									.",".TABLE_USER_RIGHTS .".parent_id		= '". $data['rights']."'"
                                    .",". TABLE_USER_RIGHTS .".value		= '". $data['value'] ."'"
                                    .",". TABLE_USER_RIGHTS .".description	= '". $data['description'] ."'"
                            ." WHERE id = '". $right_id ."' ";
                if ( $db->query($query) ) {
                    if ( $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("User Right has been Updated Successfully.");
                    }
                    else {
                        $messages->setOkMessage("There were no changes in the User Right.");
                    }
                }
                // Clear all the data.
                $_ALL_POST	= "";
                $data		= "";
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
                || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $right_id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/user-rights-list.php');
            
            /*
            if ( isset($_POST['action']) && $_POST['action'] == 'search' ) {
                include ( DIR_FS_NC .'/user-rights-search.php');
            }
            else {
                include ( DIR_FS_NC .'/user-rights-list.php');
            }*/
        }
        else {
    
            $condition_query= " WHERE id = '". $right_id ."' ";
            $data           = NULL;
            if ( UserRights::getList($db, $data, '*', $condition_query) > 0 ) {
                $data       =   $data['0'];
                
                // These parameters will be used when returning the control back to the List page.
                foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                    $hidden[]   = array('name'=> $key, 'value' => $value);
                }

                $hidden[]   = array('name'=> 'perform' , 'value' => 'edit');
                $hidden[]   = array('name'=> 'act' , 'value' => 'save');
                $hidden[]   = array('name'=> 'right_id' , 'value' => $right_id);
            }
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
  			$page["var"][] = array('variable' => 'rights', 'value' => 'rights');

            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-rights-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>