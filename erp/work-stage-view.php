<?php
    if ( $perm->has('nc_wst_details') ) {
        $id = isset($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');

        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];

        if ( WorkStage::getList($db, $list, '*', " WHERE id = '$id'") > 0 ) 
            $_ALL_POST = $list['0'];
        else
            $messages->setErrorMessage("The Order Status details were not found. ");

		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'work-stage-view.html');

    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>