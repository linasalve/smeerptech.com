<?php
 
 if ( $perm->has('nc_vjb_add') ) {
		$id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
    
		
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $_POST;		
            $data		= processUserData($_ALL_POST);	
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );                
			if ( VendorsJobwork::validateAdd($data, $extra) ) {
								
				$query	= " INSERT INTO ". TABLE_VENDORS_JOBWORK
				." SET ".TABLE_VENDORS_JOBWORK .".access_level		= '". $my['access_level'] ."'"
				.", ". TABLE_VENDORS_JOBWORK .".added_by			= '". $my['uid'] ."'"			
				.", ". TABLE_VENDORS_JOBWORK .".added_by_name			= '". $my['f_name']." ".$my['l_name'] ."'"			
				.", ". TABLE_VENDORS_JOBWORK .".title   		= '". $data['title'] ."'"
				.", ". TABLE_VENDORS_JOBWORK .".vendors_id   		= '". $data['vendors_id'] ."'"
				.", ". TABLE_VENDORS_JOBWORK .".description     	= '". $data['description'] ."'"
				.", ". TABLE_VENDORS_JOBWORK .".ip     				= '". $_SERVER['REMOTE_ADDR'] ."'"
				.", ". TABLE_VENDORS_JOBWORK .".do_e      = '". date('Y-m-d H:i:s', time()) ."'"
				.", ". TABLE_VENDORS_JOBWORK .".status      = '". VendorsJobwork::PENDING ."'"  ;
				
				if ($db->query($query) && $db->affected_rows() > 0) {
					
					$variables['hid'] = $db->last_inserted_id();
				}
				$messages->setOkMessage("The New Record has been added."); 
				$_ALL_POST	= NULL;
				$data		= NULL;
			}
        }
		
		if(!empty($id)){
			$condition_query= " WHERE ".TABLE_VENDORS_JOBWORK.".id = '". $id ."' ";
			$_ALL_POST      = NULL;
			$fields = TABLE_VENDORS_JOBWORK.".*,".TABLE_VENDORS.".billing_name,
			".TABLE_VENDORS.".f_name,".TABLE_VENDORS.".l_name" ;      
			if ( VendorsJobwork::getList($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
				$_ALL_POST = $_ALL_POST[0];
				$_ALL_POST['vendors_details']=$_ALL_POST['billing_name']." ".$_ALL_POST['f_name']." ".$_ALL_POST['l_name'] ;
			}
		
		}
		
		// These parameters will be used when returning the control back to the List page.
		foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
			$hidden[]   = array('name'=> $key, 'value' => $value);
		}
		$hidden[] = array('name'=> 'perform','value' => 'add');
		$hidden[] = array('name'=> 'ajx','value' => $ajx);
		$hidden[] = array('name'=> 'act', 'value' => 'save');
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');           
		$page["var"][] = array('variable' => 'my', 'value' => 'my');
		$page["var"][] = array('variable' => 'data', 'value' => 'data');        
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-jobwork-add.html');
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
	//echo  $start['microtime'] = microtime();
?>