<?php
    if ( $perm->has('nc_vb_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }*/
		
        
		// Include the hardware book class.
		include_once (DIR_FS_INCLUDES .'/telemarketing.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Telemarketing::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_TELEMARKETING
                            ." SET ". TABLE_TELEMARKETING .".name = '".                 $data['name'] ."'"
                                .",". TABLE_TELEMARKETING .".company_address = '".      $data['company_address'] ."'"
                                .",". TABLE_TELEMARKETING .".access_level = '".         $my['access_level'] ."'"
                                .",". TABLE_TELEMARKETING .".created_by = '".           $data['created_by'] ."'"
                                .",". TABLE_TELEMARKETING .".contact_no = '".           $data['contact_no'] ."'"
                                .",". TABLE_TELEMARKETING .".remark = '".               $data['remark'] ."'"
                                .",". TABLE_TELEMARKETING .".call_date = '".            $data['call_date'] ."'"
                                .",". TABLE_TELEMARKETING .".date = '".                 date('Y-m-d') ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Telemarketing entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/telemarketing-list.php');
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'telemarketing-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>