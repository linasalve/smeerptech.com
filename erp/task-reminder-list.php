<?php

    if ( $perm->has('nc_ts_list') ) {
		
		$variables["completed"]         = Taskreminder::COMPLETED;
		$variables["pending"]           = Taskreminder::PENDING;
	
        // If the task is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_ts_list_al') ) {
            $access_level += 1;
        }
        //$condition_query .= " AND ( ";
         $chk_status_ma = isset($_POST["chk_status_ma"])   ? $_POST["chk_status_ma"]  : (isset($_GET["chk_status_ma"])   ? $_GET["chk_status_ma"]   :'');
         $chk_status_mr = isset($_POST["chk_status_mr"])   ? $_POST["chk_status_mr"]  : (isset($_GET["chk_status_mr"])   ? $_GET["chk_status_mr"]   :'');
      
       
        //***************EOF********** 
        //***************BOF**********
        
        // If my has created this task And Check for My reminders ie reminders for me
        /*
        $condition_query .= " (". TABLE_TASK_REMINDER .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_TASK_REMINDER .".access_level < $access_level ) ";
        */
        
         //By default search in On
        $_SEARCH["searched"]    = true ;
    
        if ( $perm->has('nc_ts_list_other') ) {

            if ( !isset($condition_query) || $condition_query == ''   ) {
                $condition_query = '';
                /*
                $_SEARCH['alloted_by'] == Taskreminder::ALLOTED_BY_ME;               
                $condition_query = " AND ";
                $condition_query .= " ". TABLE_TASK_REMINDER .".created_by = '". $my['user_id'] ."' ";
                $condition_url .= "&alloted_by=".$_SEARCH['alloted_by'];
                */
                $chk_status = "AND" ;
                $condition_query= " AND ".TABLE_TASK_REMINDER.".status='".Taskreminder::PENDING."'" ;
                $_SEARCH['sStatus'] = Taskreminder::PENDING;  
                $_SEARCH['chk_status'] = $chk_status;  
                $_SEARCH['alloted_by'] = Taskreminder::SHOW_ALL;   
                
                
                //$condition_query .= " AND ";
                //$condition_query .= " (1) ";
                $condition_url .= "&alloted_by=".$_SEARCH['alloted_by']."&sStatus=".$_SEARCH['sStatus']."&chk_status=".$chk_status ;
            }        

            $condition_query = " WHERE (". TABLE_TASK_REMINDER .".comment != '' ) ".$condition_query ;    
        }else{
        
            if ( !isset($condition_query) || $condition_query == ''   ) {
                $condition_query = '';
                $chk_status = "AND" ;
                
                $condition_query= " AND ".TABLE_TASK_REMINDER.".status='".Taskreminder::PENDING."'" ;
                
                 $_SEARCH['chk_status'] = $chk_status;  
                $_SEARCH['sStatus'] = Taskreminder::PENDING;  
                $condition_url .= "&sStatus=".$_SEARCH['sStatus']."&chk_status=".$chk_status ;
            }
            
            $condition_query = " WHERE ( (".TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
                                        . TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' 
                                        ) OR (". TABLE_TASK_REMINDER .".created_by REGEXP '". $my['user_id']."'
                                        OR ". TABLE_TASK_REMINDER .".on_behalf_id REGEXP '". $my['user_id']."') )".
										$condition_query ;
        }
        
      
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        $lst_type=array_flip(Taskreminder::getTypes());
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        $extra_url  = '';    
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }        
        $condition_url .="&perform=".$perform;
        //echo $condition_query;
        // To count total records.
        $list	= 	NULL;
        $total	=	Taskreminder::getDetails( $db, $list, '', $condition_query);
        
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        //$extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        //echo $condition_query;
    
        $list	= NULL;
        $fields = TABLE_TASK_REMINDER .'.*'    ;
        Taskreminder::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        $fList=array();
        $statusArr = Taskreminder::getStatus();
        $statusArr = array_flip($statusArr);
        $priorityArr = Taskreminder::getPriority();
        $priorityArr = array_flip($priorityArr);
		$groups_list  = User::getGroups();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
				$groupsName ='';
								
				if(!empty($val['groups'])){				
					$groups1 = explode(",",$val['groups']);
					
					foreach($groups1 as $key1=>$val1){
						$groupsName .=$groups_list[$val1].",";						 	
					}
					$groupsName = trim($groupsName,',');					
				}
				$val['groupsName'] = $groupsName ;
				
				if(empty($val['allotted_to_name'])){
				   $executive=array();
				   $string = str_replace(",","','", $val['allotted_to']);
				   $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
				   $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
				   User::getList($db,$executive,$fields1,$condition1);
				   $executivename='';              
				   foreach($executive as $key1=>$val1){
						$executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
				   } 
				   $val['allotted_to_name'] = $executivename ;  
				   
			    }else{
				   $val['allotted_to_name'] =  wordwrap(trim($val['allotted_to_name'],","),30);
			    } 
				//Check task & last_comment is acknowledged by you or not
				$val['acknowledge_task']=$val['acknowledge_task_cmnt'] = true;
				$val['bold_class']='';
				$tempA = $tempB = $tempC = array();
				$tempA = explode(',',trim($val['allotted_to'],','));
				if(!empty($val['acknowledge_by'])){
					$tempB = explode(',',trim($val['acknowledge_by'],','));
				}
				
				if(!empty($val['last_comment_acknowledge_by'])){
					$tempC = explode(',',trim($val['last_comment_acknowledge_by'],','));
				}
				//task alloted to me and I did not acknowledge it then show acknowledge-me link
				if( in_array($my['user_id'],$tempA,true) && !in_array($my['user_id'],$tempB,true) ){
					$val['acknowledge_task'] = false;
					$val['bold_class']=' b ';
				}
				if(!empty($val['last_comment_id'])){
					if( in_array($my['user_id'],$tempA,true) && !in_array($my['user_id'],$tempC,true) ){
						$val['acknowledge_task_cmnt'] = false;
						$val['bold_class']=' b ';
					}
				}
				$val['allotted_to_client_name']=  wordwrap(trim($val['allotted_to_client_name'],","),30);
				$val['allotted_to_lead_name']=  wordwrap(trim($val['allotted_to_lead_name'],","),30);
				$val['allotted_to_addr_name']=  wordwrap(trim($val['allotted_to_addr_name'],","),30);
				$val['allotted_to_vendor_name']=  wordwrap(trim($val['allotted_to_vendor_name'],","),30);
				$val['status_name'] = $statusArr[$val['status']];
				if(!empty($val['priority'])){
					$val['priority'] = $priorityArr[$val['priority']];
				}
				$val['typename'] ='';
				if($val['type']!=0){               
					$val['typename'] =$lst_type[$val['type']];
                }
               
                $exeCreted=array();
                $exeCreatedname='';
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                User::getList($db,$exeCreted,$fields1,$condition2);
              
                foreach($exeCreted as $key2=>$val2){
                    $exeCreatedname .= $val2['f_name']." ".$val2['l_name']."<br/>" ;
                }
               
                $val['created_by_name'] = $exeCreatedname;  
                if(!empty($val['on_behalf_id_name'])){
                    $exeBehalf=array();
                    $exeBehalfname='';
                    $condition3 = " WHERE ".TABLE_USER .".user_id= '".$val['on_behalf_id']."' " ;
                    User::getList($db,$exeBehalf,$fields1,$condition3);
                  
                    foreach($exeBehalf as $key3=>$val3){
                        $exeBehalfname .= $val3['f_name']." ".$val3['l_name']."<br/>" ;
                    } 
                    $val['on_behalf_of_name'] = $exeBehalfname; 
                    
                }else{
                    $val['on_behalf_of_name'] = $val['on_behalf_id_name']; 
                }
                //BOF to retrieve last two comments
                /*$lastComments = array();
                $last_comment_desc='';
                $fields2 =  TABLE_TASK_REMINDER_LOG .'.comment';
                $condition4 = " WHERE ".TABLE_TASK_REMINDER_LOG .".task_id= '".$val['id']."' ORDER BY do_a DESC LIMIT 0,2" ;
                Taskreminder::getDetailsCommLog($db,$lastComments,$fields2,$condition4);
              
                foreach($lastComments as $key4=>$val4){
                    $last_comment_desc .= $val4['comment']."<br/><br/>" ;
                }                 
                $val['last_comment_desc'] = $last_comment_desc; */
               //EOF to retrieve last two comments
              
               //BOF to calculate the delay
                /*
                $do_r_history = array();
                $first_do_deadline = '';
                $fields3 =  TABLE_TASK_REMINDER_HISTORY .'.do_r';
                $condition5 = " WHERE ".TABLE_TASK_REMINDER_HISTORY .".task_id= '".$val['id']."' ORDER BY do_r LIMIT 0,1" ;
                Taskreminder::getHistoryList($db,$do_r_history,$fields3,$condition5);
                
                if(empty($do_r_history)){
                    $first_do_deadline = $val['do_r'];
                }else{
                    $first_do_deadline = $do_r_history['0']['do_r'];
                }
                */
                
                $first_do_deadline = $val['do_r'];
                
                if( $val['do_completion'] == '0000-00-00 00:00:00'){
                    $do_compare = date('Y-m-d h:i:s');
                }else{
                    $do_compare = $val['do_completion'];
                }
                
                $sql= " SELECT DATEDIFF( DATE_FORMAT('".$do_compare."', '%Y-%m-%d'),DATE_FORMAT('".$first_do_deadline."', '%Y-%m-%d')) as delay" ;
                if ( $db->query($sql) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                            $delay = $db->f('delay');   
                        }
                    }                
                } 
                if($delay > 0){
                    $val['delay'] = $delay;
                }
                //EOF to calculate the delay
                $val['can_editable'] = false;
                $val['can_deletable'] = false;
                if($val['created_by']==$my['user_id']){                    
                    if ( $perm->has('nc_ts_delete') ) {
                        $variables['can_deletable'] = true;
                        $val['can_deletable'] = true;
                    }
                    if ( $perm->has('nc_ts_edit') ) {            
                        $variables['can_editable'] = true;                        
                        $val['can_editable'] = true;
                    }
                }
                $fList[$key]=$val;
				
            }
        }
     
    if(!isset($_SEARCH['alloted_by'])){
        $_SEARCH['alloted_by'] = Taskreminder::ALLOTED_BY_ME ; //By default Alloted by me
    }     
    
    $variables['alloted_by_me'] = Taskreminder::ALLOTED_BY_ME ;
    $variables['show_all'] = Taskreminder::SHOW_ALL ;
    // Set the Permissions.
    $variables['can_view_list']     = false;
    $variables['can_add']           = false;
    $variables['can_edit_all']          = false;
    $variables['can_delete']        = false;
    $variables['can_delete_all']        = false;
    $variables['can_view_details']  = false;
    $variables['can_search']  = false;
    $variables['can_view_list_other'] = false;
    $variables['can_add_impt'] = false;
	
    if ( $perm->has('nc_ts_list_other') ) {
        $variables['can_view_list_other'] = true;
    }
    if ( $perm->has('nc_ts_list') ) {
        $variables['can_view_list'] = true;
        $variables['can_search'] = true;
    }
    if ( $perm->has('nc_ts_add') ) {
        $variables['can_add'] = true;
    }    
    
    /*
	if ( $perm->has('nc_ts_edit') ) {            
        $variables['can_edit'] = true;
    }
	*/
    
    if ( $perm->has('nc_ts_edit_all') ) {            
        $variables['can_edit_all'] = true;
    }
    
    if ( $perm->has('nc_ts_delete_all') ) {
        $variables['can_delete_all'] = true;
    }
    
    if ( $perm->has('nc_ts_details') ) {
        $variables['can_view_details'] = true;
    }
	
	if ( $perm->has('nc_ts_add_imp') ) {
        $variables['can_add_impt'] = true;
    }
    if ( $perm->has('nc_ts_sjmadd') ) {
        $variables['can_add_sjmt'] = true;
    }
       
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'task-reminder-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>