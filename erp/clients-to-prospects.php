<?php
 if ( $perm->has('nc_pr_add') ) {
	
	$user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
    
	include_once ( DIR_FS_CLASS .'/Region.class.php');
	include_once ( DIR_FS_CLASS .'/Phone.class.php');
	
	include_once ( DIR_FS_INCLUDES .'/prospects.inc.php' );
	include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
    include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
	
	$region         = new Region();
    $phone          = new Phone(TABLE_CLIENTS);
	
	$pregion     = new RegionProspects('91','21','231');
    $pphone      = new PhoneProspects(TABLE_PROSPECTS);
			
	if(!empty($user_id)){
		$condition_query= " WHERE user_id = '". $user_id ."' ";
		$_ALL_POST      = NULL;
		
		if ( Clients::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
			$data = $_ALL_POST[0];
			$phone->setPhoneOf(TABLE_CLIENTS, $user_id);
			$data['phone'] = $phone->get($db);
			$data['phone_count']= count($data['phone']);
			// Read the Addresses.
			$region->setAddressOf(TABLE_CLIENTS, $user_id);
			$data['address_list'] = $region->get();
			$data['address_count']= count($data['address_list']);
			
			if ( !empty($data['email']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email'] ."' || email_1 = '". $data['email'] ."' || email_2 = '". $data['email'] ."'")>0 ) {
                    $messages->setErrorMessage("This email id is already exist.Please specify another - ".$data['email']);
                }
            }
			if ( !empty($data['email_2']) ) {
				if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_2'] ."' || 
				email_1 = '". $data['email_2'] ."' || email_2 = '". $data['email_2'] ."' || 
				email_3 = '". $data['email_2']."' || email_4='".$data['email_2']."'")>0 ){
						$messages->setErrorMessage("The second Other Email Address is already exist. 
						Please specify another - ".$data['email_2']); 
				}
            }
			if ( !empty($data['email_3']) ) {
                $list = NULL;
                if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $data['email_3'] ."' || 
				email_1 = '". $data['email_3'] ."' || email_2 = '". $data['email_3'] ."' || 
				email_3 = '". $data['email_3']."' || email_4='".$data['email_3']."'")>0 ) {
                    $messages->setErrorMessage("The Third Other Email Address is already exist.
					Please specify another - ".$data['email_3']); 
                }
            }
			$data['pnumber'] = Prospects::getNewAccNumber($db);
			$data['user_id'] = '';
			$data['user_id'] = encParam($data['username'].'?'.$data['pnumber']); 
			
			
			if ( $messages->getErrorMessageCount() <= 0 ) {
			
				$query	= " INSERT INTO ". TABLE_PROSPECTS
				." SET ". TABLE_PROSPECTS .".user_id     	= '". $data['user_id'] ."'"
					.",". TABLE_PROSPECTS .".number      	= '". $data['pnumber'] ."'"
					.",". TABLE_PROSPECTS .".manager 	   	= '". $data['manager'] ."'"
					.",". TABLE_PROSPECTS .".team			= '". $data['team'] ."'"
					.",". TABLE_PROSPECTS .".username    	= '". $data['username'] ."'"
					.",". TABLE_PROSPECTS .".password    	= '". encParam($data['password']) ."'"					 
					.",". TABLE_PROSPECTS .".access_level	= '". $my['access_level'] ."'"
					.",". TABLE_PROSPECTS .".created_by	= '". $my['uid'] ."'"
					.",". TABLE_PROSPECTS .".email       = '". $data['email'] ."'"
					.",". TABLE_PROSPECTS .".check_email = '". $data['check_email'] ."'"
					.",". TABLE_PROSPECTS .".email_1     = '". $data['email_1'] ."'"
					.",". TABLE_PROSPECTS .".email_2     = '". $data['email_2'] ."'"						
					.",". TABLE_PROSPECTS .".email_3     	= '". $data['email_3'] ."'"	
					.",". TABLE_PROSPECTS .".email_4     	= '". $data['email_4'] ."'"						
					.",". TABLE_PROSPECTS .".additional_email = '". $data['additional_email'] ."'"
					.",". TABLE_PROSPECTS .".title       	= '". $data['title'] ."'"
					.",". TABLE_PROSPECTS .".f_name      	= '". $data['f_name'] ."'"
					.",". TABLE_PROSPECTS .".m_name      	= '". $data['m_name'] ."'"
					.",". TABLE_PROSPECTS .".l_name      	= '". $data['l_name'] ."'"
					.",". TABLE_PROSPECTS .".p_name      	= '". $data['p_name'] ."'"
					.",". TABLE_PROSPECTS .".grade      	= '". $data['grade'] ."'"
					.",". TABLE_PROSPECTS .".credit_limit = '". $data['credit_limit'] ."'"
					.",". TABLE_PROSPECTS .".desig       	= '". $data['desig'] ."'"
					.",". TABLE_PROSPECTS .".org         	= '". $data['org'] ."'"
					.",". TABLE_PROSPECTS .".domain      	= '". $data['domain'] ."'"
					.",". TABLE_PROSPECTS .".gender      	= '". $data['gender'] ."'"
					.",". TABLE_PROSPECTS .".do_birth    	= '". $data['do_birth'] ."'"
					.",". TABLE_PROSPECTS .".do_aniv     	= '". $data['do_aniv'] ."'"
					.",". TABLE_PROSPECTS .".remarks     	= '". $data['remarks'] ."'"
					.",". TABLE_PROSPECTS .".industry     = '". $data['industry_id'] ."'"
					.",". TABLE_PROSPECTS .".wt_you_do    = '". $data['wt_you_do'] ."'"
					.",". TABLE_PROSPECTS .".mobile1      = '". $data['mobile1'] ."'"
					.",". TABLE_PROSPECTS .".mobile2      = '". $data['mobile2'] ."'"
					.",". TABLE_PROSPECTS .".alloted_clients   = '". $data['alloted_clients'] ."'"
					.",". TABLE_PROSPECTS .".billing_name   = '". $data['billing_name'] ."'"
					.",". TABLE_PROSPECTS .".spouse_dob   = '". $data['spouse_dob'] ."'"
					.",". TABLE_PROSPECTS .".do_add      	= '". date('Y-m-d H:i:s', time()) ."'"
					.",". TABLE_PROSPECTS .".status      	= '". Prospects::ACTIVE ."'" ;
				
				
				if ($db->query($query)){
					$variables['hid'] = $data['user_id'] ;
					for ( $i=0; $i<$data['address_count']; $i++ ) {
						
						$address_arr = array('address_type' => $data['address_list'][$i]['address_type'],
											'company_name'  => '',
											'address'       => $data['address_list'][$i]['address'],
											'city'          => $data['address_list'][$i]['city'],
											'state'         => $data['address_list'][$i]['state'],
											'country'       => $data['address_list'][$i]['country'],
											'zipcode'       => $data['address_list'][$i]['zipcode'],
											'is_preferred'  => $data['address_list'][$i]['is_preferred'],
											'is_verified'   => $data['address_list'][$i]['is_verified']
											);
						
						if ( !$pregion->update($variables['hid'], TABLE_PROSPECTS, $address_arr) ) {
							foreach ( $pregion->getError() as $errors) {
								$messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
							}
						}
					}
				
					// Insert the Contact Numbers.
					for ( $i=0; $i<$data['phone_count']; $i++ ) {
						if ( !empty($data['phone'][$i]['p_number']) ) {
							$phone_arr = array( 'p_type'        => $data['phone'][$i]['p_type'],
												'cc'            => $data['phone'][$i]['cc'],
												'ac'            => $data['phone'][$i]['ac'],
												'p_number'      => $data['phone'][$i]['p_number'],
												'p_is_preferred'=> $data['phone'][$i]['p_is_preferred'],
												'p_is_verified' => $data['phone'][$i]['p_is_verified']
												);
							if ( ! $pphone->update($db, $variables['hid'], TABLE_PROSPECTS, $phone_arr) ) {
								foreach ( $pphone->getError() as $errors) {
									$messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. 
									$errors['description']);
								}
							}
						}
					}
				} 
				$messages->setOkMessage("The Client ".$data['f_name']." ".$data['l_name']." - "
					.$data['billing_name']." copied to CEO-Prospects has been added.");
			
			
				//Add Sub users BOF
				$condition_query = " WHERE parent_id='".$user_id."'" ; 
                Clients::getList( $db, $sublist, '*', $condition_query);
                $data['sublist_list'] = $sublist;
				
				if(!empty($data['sublist_list'])){		
					$client_id = $data['user_id'] ;
					foreach($data['sublist_list'] as $key=>$val){
						$valid_str = mktime()."?".$key;
						$val['user_id'] = encParam($valid_str);
						$val['password'] = $val['username'] = 'smeerp';
						$phone->setPhoneOf(TABLE_CLIENTS, $val['user_id']);
						$val['phone'] = $phone->get($db);
						$val['phone_count'] = count($val['phone']);
						// Read the Addresses.
						$region->setAddressOf(TABLE_CLIENTS, $val['user_id']);
						$val['address_list'] = $region->get();
						$val['address_count'] = count($val['address_list']);
						if ( !empty($val['email']) ) {
							$list = NULL;
							if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $val['email'] ."' 
							   || email_1 = '". $val['email'] ."' || email_2 = '". $val['email'] ."'")>0 ) {
								$messages->setErrorMessage("This email id is already exist.Please specify another ".$val['email']);
							}
						}
						if(!empty($val['email_2'])){
							$list = NULL;
							if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $val['email_2'] ."'
								|| email_1 = '". $val['email_2'] ."' || email_2 = '". $val['email_2'] ."' || 
								email_3 = '". $val['email_2']."' || email_4='".$val['email_2']."'")>0 ) {
								$messages->setErrorMessage("The second Other Email Address is already exist.
								Please specify another - ".$val['email_2']);
							}
						}
						if ( !empty($val['email_3']) ) {
							$list = NULL;
							if ( Prospects::getList($db, $list, 'user_id', " WHERE email = '". $val['email_3'] ."'
							|| email_1 = '". $val['email_3'] ."' || email_2 = '". $val['email_3'] ."' || 
							email_3 = '". $val['email_3']."' || email_4='".$val['email_3']."'")>0 ) {
								$messages->setErrorMessage("The Third Other Email Address is already exist.
								Please specify another - ".$val['email_3']);
							}
						}
						if ( $messages->getErrorMessageCount() <= 0 ) {
						
							$query	= " INSERT INTO ". TABLE_PROSPECTS
							." SET ". TABLE_PROSPECTS .".user_id     	= '". $val['user_id'] ."'"
							.",". TABLE_PROSPECTS .".parent_id    		= '". $client_id ."'"
							.",". TABLE_PROSPECTS .".service_id   		= '". $val['service_id'] ."'"
							.",". TABLE_PROSPECTS .".authorization_id   = '". $val['authorization_id'] ."'"
							.",". TABLE_PROSPECTS .".number      	= '". $data['pnumber'] ."'"
							.",". TABLE_PROSPECTS .".username    	= '". $val['username'] ."'"
							.",". TABLE_PROSPECTS .".password    	= '". encParam($val['password']) ."'"
							.",". TABLE_PROSPECTS .".access_level	= '1'"
							.",". TABLE_PROSPECTS .".created_by	    = '". $my['uid'] ."'"
							.",". TABLE_PROSPECTS .".email       	= '". $val['email'] ."'"
							.",". TABLE_PROSPECTS .".check_email    = '". $val['check_email'] ."'"
							.",". TABLE_PROSPECTS .".email_1     	= '". $val['email_1'] ."'"
							.",". TABLE_PROSPECTS .".email_2     	= '". $val['email_2'] ."'"
							.",". TABLE_PROSPECTS .".title       	= '". $val['title'] ."'"
							.",". TABLE_PROSPECTS .".f_name      	= '". $val['f_name'] ."'"
							.",". TABLE_PROSPECTS .".m_name      	= '". $val['m_name'] ."'"
							.",". TABLE_PROSPECTS .".l_name      	= '". $val['l_name'] ."'"
							.",". TABLE_PROSPECTS .".p_name      	= '". $val['p_name'] ."'"
							.",". TABLE_PROSPECTS .".desig       	= '". $val['desig'] ."'"
							.",". TABLE_PROSPECTS .".org         	= '". $val['org'] ."'"
							.",". TABLE_PROSPECTS .".domain      	= '". $val['domain'] ."'"
							.",". TABLE_PROSPECTS .".gender      	= '". $val['gender'] ."'"
							.",". TABLE_PROSPECTS .".do_birth    	= '". $val['do_birth'] ."'"
							.",". TABLE_PROSPECTS .".do_aniv     	= '". $val['do_aniv'] ."'"
							.",". TABLE_PROSPECTS .".remarks     	= '". $val['remarks'] ."'"
							.",". TABLE_PROSPECTS .".mobile1        = '". $val['mobile1'] ."'"
							.",". TABLE_PROSPECTS .".mobile2        = '". $val['mobile2'] ."'"
							.",". TABLE_PROSPECTS .".allow_ip       = '". $val['allow_ip'] ."'"
							.",". TABLE_PROSPECTS .".valid_ip       = '". $val['valid_ip'] ."'"
							.",". TABLE_PROSPECTS .".do_add      	= '". date('Y-m-d H:i:s', time()) ."'"
							.",". TABLE_PROSPECTS .".status      	= '". $val['status'] ."'" ;
							
							if ($db->query($query)){
								$variables['shid']=$val['user_id'] ;
							
								for ( $i=0; $i<$val['address_count']; $i++ ) {					
									$address_arr1=array();
									$address_arr1 = array('address_type' => $val['address_list'][$i]['address_type'],
														'company_name'  => '',
														'address'       => $val['address_list'][$i]['address'],
														'city'          => $val['address_list'][$i]['city'],
														'state'         => $val['address_list'][$i]['state'],
														'country'       => $val['address_list'][$i]['country'],
														'zipcode'       => $val['address_list'][$i]['zipcode'],
														'is_preferred'  => $val['address_list'][$i]['is_preferred'],
														'is_verified'   => $val['address_list'][$i]['is_verified']
														);
									
									if ( !$pregion->update($variables['shid'], TABLE_PROSPECTS, $address_arr1) ) {
										foreach ( $pregion->getError() as $errors) {
											$messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. 
											$errors['description']);
										}
									}
								}
								
								// Insert the Contact Numbers.
								for ( $i=0; $i<$val['phone_count']; $i++ ) {
									if ( !empty($val['phone'][$i]['p_number']) ){
										$phone_arr1 = array();
										$phone_arr1 = array( 'p_type'        => $val['phone'][$i]['p_type'],
															'cc'            => $val['phone'][$i]['cc'],
															'ac'            => $val['phone'][$i]['ac'],
															'p_number'      => $val['phone'][$i]['p_number'],
															'p_is_preferred'=> $val['phone'][$i]['p_is_preferred'],
															'p_is_verified' => $val['phone'][$i]['p_is_verified']
															);
									    if(!$pphone->update($db, $variables['shid'],TABLE_PROSPECTS,$phone_arr1)){
											foreach ( $pphone->getError() as $errors){
												$messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.												$errors['description']);
											}
										}
									}
								}
								$messages->setOkMessage("The Sub Client ".$val['f_name']." ".$val['l_name']." - "
								.$val['billing_name']." copied to CEO-Prospects has been added.");
							}  
						
						}
						
					
					}
				}
				//Add Sub users EOF
			
			}
			
							
		}
		 
		 
		
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-to-prospects.html');
		
		
 
	}else{
	
		$messages->setErrorMessage("User Id is not found.");
	}
 
}else {
    $messages->setErrorMessage("You donot have the Permisson to Access this module.");
}