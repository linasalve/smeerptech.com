<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
				   
  
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/vendors-jobwork.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/vendors.inc.php' );
	
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	
	echo "i m here";
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	$pageno = isset($_GET["pageno"])  ? $_GET["pageno"]        : ( isset($_POST["pageno"])      ? $_POST["pageno"]       :'');
	
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
    $condition_url = $condition_query = '';
    
    if ( empty($x) ) {
        $x              = 1;
        $pageno              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["pageno"]     = $pageno;
    $variables["rpp"]   = $rpp;
    
    
    if($added){
         $messages->setOkMessage("Record has been added successfully.");
    }
    
    //By default search in On
    $_SEARCH["searched"]    = true ;
    if ( $perm->has('nc_vjb') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_VENDORS_JOBWORK   =>  array( 
                                                            'Job Title'  => 'title',
                                                            'Description'        => 'description',
                                                            'Added By'    => 'added_by_name'
                                                        ), 
								 TABLE_VENDORS   =>  array( 
                                                            'Vendor Billing Name'     => 'billing_name',
                                                            'Vendor First Name'    => 'f_name',
                                                            'Vendor Last Name'    => 'l_name',
                                                            'E-mail'        => 'email'
                                                        ) 
                                 
                            );
        
       $sOrderByArray  = array(
                                TABLE_VENDORS_JOBWORK => array(
                                                    'Job Title'  => 'title',
                                                    'Date of Job Assign'=> 'do_job',
                                                    'Date of Add'=> 'do_e'
                                                    )
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_VENDORS_JOBWORK;
        }

        // Read the available Status for the Pre Order.      
        $variables['status']   = VendorsJobwork::getStatus();
	
        //use switch case here to perform action. 
        switch ($perform) {
        
           
            case ('add'): {
				
                include (DIR_FS_NC.'/vendors-jobwork-add.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-jobwork.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}				
                break;
            }
            case ('edit'): {
                include (DIR_FS_NC .'/vendors-jobwork-edit.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-jobwork.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            } 
            case ('view'): {
                include (DIR_FS_NC .'/vendors-jobwork-view.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('change_status'): {
                include ( DIR_FS_NC .'/vendors-jobwork-status.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-jobwork.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }		 
            case ('search'): {
                include(DIR_FS_NC."/vendors-jobwork-search.php");
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-jobwork.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            }           
            case ('delete'): {
                include ( DIR_FS_NC .'/vendors-jobwork-delete.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-jobwork.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list'):
            default: {
			
                include (DIR_FS_NC .'/vendors-jobwork-list.php');
                // CONTENT = CONTENT
				if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-jobwork.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'vendors-jobwork.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
	$s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());


  
	if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");

?>