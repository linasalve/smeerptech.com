<?php
    if ( $perm->has('nc_emtpl_edit') ) {
        $tpl_id	= isset($_GET["tpl_id"]) ? $_GET["tpl_id"] 	: ( isset($_POST["tpl_id"]) ? $_POST["tpl_id"] : '' );
    
        $data		= "";
        $_ALL_POST	= "";
        
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 				=> &$db,
                            'messages' 			=> $messages,
                        );
            
            if (EmailTemplates::validateUpdate($tpl_id, $data, $extra)) {
                
                $query	= " UPDATE ".TABLE_SETTINGS_EMAIL_TPL
                            ." SET ". TABLE_SETTINGS_EMAIL_TPL .".name              = '".$data['name'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".description       = '".$data['description'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".from_name         = '".$data['from_name'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".from_email        = '".$data['from_email'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".subject           = '".$data['subject'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".text              = '".$data['text'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".allowed_constant  = '".$data['allowed_constant'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".is_html           = '".$data['is_html'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".status            = '".$data['status'] ."'"
                            ." WHERE id = '". $tpl_id ."' ";
                if ( $db->query($query) ) {
                    if ( $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Email Template has been Updated Successfully.");
                    }
                    else {
                        $messages->setOkMessage("There were no changes in the Email Template.");
                    }
                }
                // Clear all the data.
                $_ALL_POST	= "";
                $data		= "";
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
                || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $tpl_id;
           
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/settings-templates-list.php');
        }
        else {
            $condition_query    = " WHERE id = '". $tpl_id ."' ";
            $data               = NULL;
            if ( EmailTemplates::getList($db, $data, '*', $condition_query) > 0 ) {
                $data       =   $data['0'];
                
                // These parameters will be used when returning the control back to the List page.
                foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                    $hidden[]   = array('name'=> $key, 'value' => $value);
                }
                $hidden[]   = array('name'=> 'perform' , 'value' => 'edit');
                $hidden[]   = array('name'=> 'act' , 'value' => 'save');
                $hidden[]   = array('name'=> 'tpl_id' , 'value' => $tpl_id);
            }
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
    
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-templates-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>