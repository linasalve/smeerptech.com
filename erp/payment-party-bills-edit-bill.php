<?php
  if ( $perm->has('nc_p_pb_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
		include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
       
		include_once ( DIR_FS_INCLUDES .'/task-reminder.inc.php');        
		include_once ( DIR_FS_INCLUDES .'/site-settings.inc.php');        
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');    
       
         
        include_once ( DIR_FS_INCLUDES .'/user.inc.php');         
		
		
		$currency = NULL ;
        $required_fields = '*' ;
		 
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save' ) {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages,
							'files'          => &$files
                        );
            //$account_head=null;
			$data['files'] = $files ;
            $allowed_file_types=array( "image/jpeg",												
										"image/pjpeg"
									);			
           
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = 300; // 1024 *300 ; 300 KB
				
            if ( PaymentPartyBills::validateUpdateBill($data, $extra) ) {                
                      
                
				
				//Code To transfer payment details in user account EOF
				$attachfilename='';
				$bl_file_sql='';
				if(!empty($files['bill_attachment']["name"])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['bill_attachment']["name"]);
					$ext = $filedata["extension"] ;  
					//$attachfilename = $username."-".mktime().".".$ext ;                       
					$attachfilename = $data['number'].".".$ext ;
					$data['bill_attachment'] = $attachfilename;                        
					$attachment_path = DIR_WS_BILLS_FILES;
					if(file_exists(DIR_FS_BILLS_FILES."/".$data['old_bill_attachment']))
					{
						@unlink( DIR_FS_BILLS_FILES."/".$data['old_bill_attachment']);
					}
					
					if(move_uploaded_file($files['bill_attachment']['tmp_name'], 							
						DIR_FS_BILLS_FILES."/".$attachfilename)){
						@chmod(DIR_FS_BILLS_FILES."/".$attachfilename, 0777);
						 $bl_file_sql = " ". TABLE_PAYMENT_PARTY_BILLS .".bill_attachment = 
						'". $attachfilename ."'";	
					}else{
						$bl_file_sql = " ". TABLE_PAYMENT_PARTY_BILLS .".bill_attachment = ''";
					}
				}else{
					$data['bill_attachment']=$data['old_bill_attachment'];
					if(isset($data['delete_bill_attachment'])){
						if(file_exists(DIR_FS_BILLS_FILES."/".$data['old_bill_attachment'])){
							@unlink( DIR_FS_BILLS_FILES."/".$data['old_bill_attachment']);
						}
						$bl_file_sql = " ". TABLE_PAYMENT_PARTY_BILLS .".bill_attachment = ''";
						$data['bill_attachment']='';
					}
				}
				$query  = " UPDATE ".TABLE_PAYMENT_PARTY_BILLS
						  ." SET  ".$bl_file_sql." WHERE id = '". $id ."'";
				 
				
				if ( $db->query($query) ) { 
					$messages->setOkMessage("Purchase bill updated successfully.");   
					
					$fields = TABLE_PAYMENT_PARTY_BILLS .'.*'  ;            
					$condition_query = " WHERE (". TABLE_PAYMENT_PARTY_BILLS .".id = '". $id ."' )";
					
					if ( PaymentPartyBills::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
							$_ALL_POST = $_ALL_POST['0'];
							$number = $_ALL_POST ['number'];
					}
				}					  
							   
            }
        }
        else {
     
            
			$fields = TABLE_PAYMENT_PARTY_BILLS .'.*'  ;            
            $condition_query = " WHERE (". TABLE_PAYMENT_PARTY_BILLS .".id = '". $id ."' )";
            
            if ( PaymentPartyBills::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST['0'];
					$number = $_ALL_POST ['number'];
			}
            
        }
        // get list of all comments of change log BOF
        $condition_query1 = $condition_url1 = '';
       
			 

		// These parameters will be used when returning the control back to the List page.
		foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
			$hidden[]   = array('name'=> $key, 'value' => $value);
		}            
		$hidden[] = array('name'=> 'id', 'value' => $id);
		$hidden[] = array('name'=> 'ajx', 'value' => $ajx);
		$hidden[] = array('name'=> 'number', 'value' => $number);
		$hidden[] = array('name'=> 'perform', 'value' => 'edit_bill');
		$hidden[] = array('name'=> 'act', 'value' => 'save');
		$page["var"][] = array('variable' => 'id', 'value' => 'id');    
		$page["var"][] = array('variable' => 'services', 'value' => 'services');   
		$page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');      
		$page["var"][] = array('variable' => 'currency', 'value' => 'currency');						
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' =>'payment-party-bills-edit-bill.html');
	 
        
   }else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
   }
?>
