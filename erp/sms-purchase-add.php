<?php
  if ( $perm->has('nc_sms_pr_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        //Get gateway list 
        $lst_gateway=null;
        $fields = TABLE_SMS_GATEWAY .'.id'
                   .','. TABLE_SMS_GATEWAY .'.company';
        $condition_query= "WHERE status='".SmsGateway::ACTIVE."'";
        SmsGateway::getList($db,$lst_gateway,$fields,$condition_query);
        
        //Get api list 
        /*$lst_api=null;
        $fields = TABLE_SMS_API .'.id'
                   .','. TABLE_SMS_API .'.name';
        $condition_query= "WHERE status='".SmsPurchase::ACTIVE."'";
        SmsApi::getList($db,$lst_api,$fields,$condition_query);
		print_R($lst_api);*/
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
               
                if(!empty($data['gateway_id'])){        
                    $lst_api=null;
                    $fields = TABLE_SMS_API .'.id'
                               .','. TABLE_SMS_API .'.name';
                    $condition_query= "WHERE status='".SmsApi::ACTIVE."' AND gateway_id='".$data['gateway_id']."'";
                    SmsApi::getList($db,$lst_api,$fields,$condition_query);        
                }
                
                if ( SmsPurchase::validateAdd($data, $extra) ) { 
                    $query	= " INSERT INTO ".TABLE_SMS_PURCHASE
                            ." SET ".TABLE_SMS_PURCHASE .".gateway_id 			= '". $data['gateway_id'] ."'"  
                                	.",". TABLE_SMS_PURCHASE .".api_id 			= '". $data['api_id'] ."'"                                
                                	.",". TABLE_SMS_PURCHASE .".allotted_sms 	= '". $data['allotted_sms'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE .".balance_sms 	= '". $data['allotted_sms'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE .".rate 	        = '". $data['rate'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE .".amount 			= '". $data['amount'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE .".created_by 		= '". $my['user_id'] ."'"                    
									//.",". TABLE_SMS_PURCHASE .".status 			= '". $data['status'] ."'"                    
                                    .",". TABLE_SMS_PURCHASE .".ip 			    = '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE .".do_e            = '". date('Y-m-d H:i:s')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New sms purchase entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();   

                     $query	= " INSERT INTO ".TABLE_SMS_PURCHASE_HISTORY
                            ." SET ".TABLE_SMS_PURCHASE_HISTORY .".purchase_id 			= '". $variables['hid'] ."'"  
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".gateway_id 		= '". $data['gateway_id'] ."'"                                
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".api_id 			= '". $data['api_id'] ."'"                                
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".no_of_sms 	= '". $data['allotted_sms'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".balance_sms 	= '". $data['allotted_sms'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".rate 	        = '". $data['rate'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".amount 			= '". $data['amount'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".created_by 		= '". $my['user_id'] ."'"                    
									//.",". TABLE_SMS_PURCHASE_HISTORY .".status 			= '". $data['status'] ."'"                    
                                    .",". TABLE_SMS_PURCHASE_HISTORY .".ip 			    = '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".do_e            = '". date('Y-m-d H:i:s')."'"
                                    .",". TABLE_SMS_PURCHASE_HISTORY .".ip_h 			    = '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_SMS_PURCHASE_HISTORY .".do_he            = '". date('Y-m-d H:i:s')."'"
                                    .",". TABLE_SMS_PURCHASE_HISTORY .".history_created_by 		= '". $my['user_id'] ."'";   
                    $db->query($query);                 
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sms-purchase.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sms-purchase.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sms-purchase.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');    
            $page["var"][] = array('variable' => 'lst_gateway', 'value' => 'lst_gateway');                 
            $page["var"][] = array('variable' => 'lst_api', 'value' => 'lst_api');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-purchase-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
