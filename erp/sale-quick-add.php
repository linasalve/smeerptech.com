<?php
// $start['time'] = time();
//echo  $start['microtime'] = microtime();

    if ( $perm->has('nc_sl_ld_qe_add') ) {

        include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
    	include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	    include_once ( DIR_FS_INCLUDES .'/followup.inc.php');
	    include_once ( DIR_FS_INCLUDES .'/sale-segment.inc.php');
        include_once ( DIR_FS_INCLUDES .'/leads-ticket.inc.php');
		include_once ( DIR_FS_INCLUDES .'/leads-order.inc.php');
        $_ALL_POST	= NULL;
        $data       = NULL;
        $regionlead     = new RegionLead('91','21','231');
        $phonelead      = new PhoneLead(TABLE_SALE_LEADS);
        //$reminder   = new UserReminder(TABLE_LEADS);
        
        $al_list=null;
        //print_r($my);
        //BOF read the available country codes
		//Quick::getCountryCode($db,$country_code);
		$country_code=null;
        //EOF read the available country codes
        
        $variables['title_type'] = Quick::getTitleType();

        $segment_list = NULL;
        /* $required_fields1 = '*';
        $condition_query1 = "WHERE status='".Segment::ACTIVE."' ORDER BY title";
		Segment::getList($db,$segment_list,$required_fields1,$condition_query1); */
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $_POST;
			 
            $data		= processUserData($_ALL_POST);
			// BO: For generating default password.
			//$data['password']	= getRandom(6, '', array_merge($letters_b, $letters_s, $digits));
			//$data['re_password']= $data['password'];
			// EO: For generating default password.

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'regionlead'    => $regionlead,
                            'phonelead'     => $phonelead,
                            //'reminder'  => $reminder
                        );
              
                
			if ( Quick::validateAdd($data, $extra) ) {
				$already_exist_no = 0;
				for ( $i=0; $i<$data['phone_count']; $i++ ) {
					if ( !empty($data['p_number'][$i]) ) {
						$query="SELECT * FROM ". TABLE_LEAD_PHONE." WHERE cc='".$data['cc'][$i]."' AND 
						ac='".$data['ac'][$i]."' AND p_number='".$data['p_number'][$i]."' LIMIT 0,1 ";
							$db->query($query);
							if($db->nf()>0){
								$already_exist_no = 1 ;
							}
					}
				}
				
				//$leadnumber = getCounterNumber($db,'LEAD'); 
				//insert into  database
							$query	= " INSERT INTO ". TABLE_SALE_LEADS
							." SET ".TABLE_SALE_LEADS .".access_level		= '". $my['access_level'] ."'"
							.", ". TABLE_SALE_LEADS .".created_by				= '". $my['uid'] ."'"
							//.", ". TABLE_SALE_LEADS .".lead_number				= '". $leadnumber ."'"
							//.", ". TABLE_SALE_LEADS .".lead_owner   			= '". $my['f_name'] ."'"
							//.", ". TABLE_SALE_LEADS .".lead_assign_to   		= '". $data['lead_assign_to'] ."'"
							.", ". TABLE_SALE_LEADS .".company_name   		= '". $data['company_name'] ."'"
							.",". TABLE_SALE_LEADS .".billing_name			= '". $data['company_name'] ."'"
							.", ". TABLE_SALE_LEADS .".lead_source_from			= '". $data['lead_source_from'] ."'"
							.", ". TABLE_SALE_LEADS .".lead_source_to			= '". $data['lead_source_to'] ."'"
							.", ". TABLE_SALE_LEADS .".lead_segment_id		= '". $data['lead_segment_id'] ."'"
							//.", ". TABLE_SALE_LEADS .".lead_campaign_id   	= '". $data['lead_campaign_id'] ."'"
							//.", ". TABLE_SALE_LEADS .".lead_industry_id   	= '". $data['lead_industry_id'] ."'"
							//.", ". TABLE_SALE_LEADS .".no_emp			   		= '". $data['no_emp'] ."'"
							//.", ". TABLE_SALE_LEADS .".rating			   		= '". $data['rating'] ."'"
							//.", ". TABLE_SALE_LEADS .".annual_revenue   		= '". $data['annual_revenue'] ."'"
							//.", ". TABLE_SALE_LEADS .".rivals_id   			= '". $data['rivals_id'] ."'"
							.", ". TABLE_SALE_LEADS .".email       				= '". $data['email'] ."'"
							.", ". TABLE_SALE_LEADS .".email_1     				= '". $data['email_1'] ."'"
							.", ". TABLE_SALE_LEADS .".email_2     				= '". $data['email_2'] ."'"
							.",".  TABLE_SALE_LEADS .".mobile1     			   = '". $data['mobile1'] ."'"
                            .",".  TABLE_SALE_LEADS .".mobile2     			   = '". $data['mobile2'] ."'"
                            .",".  TABLE_SALE_LEADS .".mobile3     			   = '". $data['mobile3'] ."'"
							.", ". TABLE_SALE_LEADS .".title       				= '". $data['title'] ."'"
							.", ". TABLE_SALE_LEADS .".f_name      				= '". $data['f_name'] ."'"
							.", ". TABLE_SALE_LEADS .".m_name      				= '". $data['m_name'] ."'"
							.", ". TABLE_SALE_LEADS .".l_name      				= '". $data['l_name'] ."'"
							.", ". TABLE_SALE_LEADS .".p_name      				= '". $data['p_name'] ."'"
							.", ". TABLE_SALE_LEADS .".desig       				= '". $data['desig'] ."'"
							.", ". TABLE_SALE_LEADS .".org         				= '". $data['org'] ."'"
							.", ". TABLE_SALE_LEADS .".domain      				= '". $data['domain'] ."'"
							.", ". TABLE_SALE_LEADS .".gender      				= '". $data['gender'] ."'"
							.", ". TABLE_SALE_LEADS .".do_birth    				= '". $data['do_birth'] ."'"
							.", ". TABLE_SALE_LEADS .".do_aniv     				= '". $data['do_aniv'] ."'"
							.", ". TABLE_SALE_LEADS .".remarks     				= '". $data['remarks'] ."'"
							.", ". TABLE_SALE_LEADS .".ip     				    = '". $_SERVER['REMOTE_ADDR'] ."'"
							//.", ". TABLE_SALE_LEADS .".followup_no 				= '". $flwnumber ."'"
							.", ". TABLE_SALE_LEADS .".already_exist_no			= '". $already_exist_no ."'"
							.", ". TABLE_SALE_LEADS .".do_add      				= '". date('Y-m-d H:i:s', time()) ."'"
							.", ". TABLE_SALE_LEADS .".lead_status      		= '". Quick::NEWONE ."'"
							.", ". TABLE_SALE_LEADS .".status      				= '1'" ;
				
					if ($db->query($query) && $db->affected_rows() > 0) {
					
						$variables['hid'] = $db->last_inserted_id();
							//Default Company SMEERP E-TECHNOLOGIES PVT. LTD.
							$data['comp_id'] = 1;
							$data['comp_name'] = 'SMEERP E-Technologies Pvt. Ltd.';
							$data['comp_prefix'] = 'PET';
							$data['order_title'] = 'Website Development '.$data['domain'];
							$data['currency_id'] = '1';
							$data['currency_abbr'] = 'INR';
							$data['currency_name'] = 'Rupees';
							$data['currency_symbol'] = 'Rs.';
							$data['currency_country'] = 'India';
							$data['team'] = ',adedc2804591f418cb806e0f8a1c5822,'; 
							//$data['team'] = ','.$my['uid'].","; 
							$data['order_closed_by'] = 'dec32cb82fe9bd2ffbec266ab14fdac6'; 
							$data['order_closed_by_name'] = 'Auto System' ;
							$data['project_manager'] = 'e68adc58f2062f58802e4cdcfec0af2d';
							$data['number']=$data['ord_counter'] ='';
							$data['do_o'] = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
							if(!empty($data['do_o'])){				
								$detailOrdNo = getLeadNumber($data['do_o']); 				
								if(!empty($detailOrdNo)){
									$data['number'] = $detailOrdNo['number']; 
									$data['ord_counter'] = $detailOrdNo['ord_counter'];
								}
							}
							$data['crm_status'] = LeadsOrder::CRM_ASSIGN;
							$data['status'] = LeadsOrder::PENDING;
							$data['order_type'] = LeadsOrder::TARGETED;
							//Create default lead bof
							$query	= " INSERT INTO ".TABLE_LEADS_ORDERS
							." SET ". TABLE_LEADS_ORDERS .".number       = '". $data['number'] ."'"
							.",". TABLE_LEADS_ORDERS .".ord_counter      = '". $data['ord_counter'] ."'"
							.",". TABLE_LEADS_ORDERS .".access_level     = '". $my['access_level'] ."'"
							.",". TABLE_LEADS_ORDERS .".client           = '". $variables['hid'] ."'"
							.",". TABLE_LEADS_ORDERS .".created_by       = '". $my['uid'] ."'"
							.",". TABLE_LEADS_ORDERS .".company_id	     = '". $data['comp_id'] ."'"
							.",". TABLE_LEADS_ORDERS .".company_name	 = '". $data['comp_name'] ."'"
							.",". TABLE_LEADS_ORDERS .".company_prefix   = '". $data['comp_prefix'] ."'"
							.",". TABLE_LEADS_ORDERS .".order_title	     = '". $data['order_title'] ."'"
							.",". TABLE_LEADS_ORDERS .".currency_id      = '". $data['currency_id'] ."'"
							.",". TABLE_LEADS_ORDERS .".currency_abbr    = '". $data['currency_abbr'] ."'"
							.",".TABLE_LEADS_ORDERS .".currency_name     = '". $data['currency_name'] ."'"
							.",". TABLE_LEADS_ORDERS .".currency_symbol  = '". $data['currency_symbol'] ."'"
							.",". TABLE_LEADS_ORDERS .".currency_country = '". $data['currency_country'] ."'"
							.",". TABLE_LEADS_ORDERS .".order_type 		 = '". $data['order_type'] ."'"
							.",". TABLE_LEADS_ORDERS .".order_closed_by	 = '". $data['order_closed_by'] ."'"
							.",". TABLE_LEADS_ORDERS .".team             = '".$data['team'] ."'"
							.",". TABLE_LEADS_ORDERS .".do_o             = '". date('Y-m-d H:i:s', $data['do_o']) ."'"
							.",". TABLE_LEADS_ORDERS .".do_c             = '". date('Y-m-d H:i:s') ."'"
							.",". TABLE_LEADS_ORDERS .".project_manager  = '". $data['project_manager']."'"
							.",". TABLE_LEADS_ORDERS .".crm_status       = '". $data['crm_status'] ."'" 
							.",". TABLE_LEADS_ORDERS .".status           = '". $data['status'] ."'" 
							.",". TABLE_LEADS_ORDERS .".ip           = '".$_SERVER['REMOTE_ADDR']."'" ;
		  
							$db->query($query); 
							$flw_ord_id =  $db->last_inserted_id();
							//Create default lead eof
						 
						 
						 
							//Create Followup ticket bof
							$ticket_no  =  LeadsTicket::getNewNumber($db);							
							$data['ticket_owner_uid'] = $variables['hid'];
							$data['name']= $data['f_name']." ".$data['l_name'] ;
							$data['ticket_owner'] = $data['company_name']." ";
							if(!empty($data['name'])){
								$data['ticket_owner'] = $data['company_name']." ".$data['name'];
							}
							$data['ticket_subject'] = 'Prospect Followup';
							$data['ticket_text'] = " New Prospect - ".$data['company_name']." ".$data['name'].", 	
							added by ". $my['f_name']." ". $my['l_name'];
								
							$data['hrs']='';
						    $data['min']=5;
						
							$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
							$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
							$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;							 
							$hrs1 = (int) ($data['hrs'] * 6);
							$min1 = (int) ($data['min'] * 6);  
							$followup_query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
							. TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_LD_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
							//. TABLE_LD_TICKETS .".leads_followup    = '1', "
							. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
							/* . TABLE_LD_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_LD_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', " */
							. TABLE_LD_TICKETS .".ticket_creator_uid = '".$data['order_closed_by'] ."', "
							. TABLE_LD_TICKETS .".ticket_creator     = '".$data['order_closed_by_name']."', "
							. TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
							. TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
							. TABLE_LD_TICKETS .".ticket_status     = '".LeadsTicket::PENDINGWITHCLIENTS ."', "
							. TABLE_LD_TICKETS .".ticket_child      = '0', "
							. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_LD_TICKETS .".min = '". $data['min']."', "
							. TABLE_LD_TICKETS .".ticket_response_time = '0', "
							. TABLE_LD_TICKETS .".ticket_replied        = '0', "
							. TABLE_LD_TICKETS .".from_admin_panel        = '".LeadsTicket::ADMIN_PANEL."', "
							. TABLE_LD_TICKETS .".display_name           = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_LD_TICKETS .".display_user_id        = '". $my['uid'] ."', "
							. TABLE_LD_TICKETS .".display_designation    = '". $my['desig'] ."', "
							. TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
							. TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
							
							$db->query($followup_query) ;
							
						//Create Followup ticket eof			
						
						// Insert the address.
						for ( $i=0; $i<$data['address_count']; $i++ ) {
							$address_arr = array('address_type' => $data['address_type'][$i],
												'company_name'  => $data['company_name'][$i],
												'address'       => $data['address'][$i],
												'city'          => $data['city'][$i],
												'state'         => $data['state'][$i],
												'country'       => $data['country'][$i],
												'zipcode'       => $data['zipcode'][$i],
												'is_preferred'  => $data['is_preferred'][$i],
												'is_verified'   => $data['is_verified'][$i]
												);
							
							if ( !$regionlead->update($variables['hid'], TABLE_SALE_LEADS, $address_arr) ) {
								foreach ( $regionlead->getError() as $errors) {
									$messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
								}
							}
						}
					
					// Insert the Contact Numbers.
					for ( $i=0; $i<$data['phone_count']; $i++ ) {
						if ( !empty($data['p_number'][$i]) ) {
							$phone_arr = array( 'p_type'        => $data['p_type'][$i],
												'cc'            => $data['cc'][$i],
												'ac'            => $data['ac'][$i],
												'p_number'      => $data['p_number'][$i],
												'p_is_preferred'=> $data['p_is_preferred'][$i],
												'p_is_verified' => $data['p_is_verified'][$i]
												);
							 
							if ( ! $phonelead->update($db, $variables['hid'], TABLE_SALE_LEADS, $phone_arr) ) {
								foreach ( $phonelead->getError() as $errors) {
								 $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
								}
							}
						}
					}
					
					$phone_details ='';
					$phonelead->setPhoneOf(TABLE_SALE_LEADS, $variables['hid']);
					$data['phone'] = $phonelead->get($db);
					if(!empty($data['phone'])){
						foreach($data['phone'] as $key=>$val){
							$sr=$key+1 ;
				$phone_details .= "Sr.".$sr.". "."(".$val['title'].")".$val['cc']."-".$val['ac']."-".$val['p_number']."<br/>";
						
						}
					}
					
					
					// Read the Addresses.
					$regionlead->setAddressOf(TABLE_SALE_LEADS, $variables['hid']);
					$data['address_list'] = $regionlead->get();
					
					if(!empty($data['address_list'])){
						foreach($data['address_list'] as $key1=>$val1){
							$sr1 = $key1+1 ;
				$address_details .= "Sr.".$sr1.". "."(".$val1['title'].")".$val1['cc']."-".$val1['ac']."-".$val1['p_number']."<br/>";
						
						}
					}
					//SEND MAIL TO ADMIN BOF
					/* 
					$data['added_by']=$my['f_name']." ".$my['l_name'] ;
					$data['do_add']= date('d M Y');
					$data['id'] = $variables['hid'] ;
					$data['company_name'] = $data['company_name'] ;
					$data['name'] = $data['name'] ;
					$data['no_emp'] = $data['no_emp'] ;
					$data['gender_name'] = ($data['gender']=='m') ? 'Male' : 'Female' ;
					$data['email'] 	 = $data['email'] ;
					$data['email_1'] = $data['email_1'] ;
					$data['email_2'] = $data['email_2'] ;
					$data['mobile1'] = $data['mobile1'] ;
					$data['mobile2'] = $data['mobile2'] ;
					$data['mobile3'] = $data['mobile3'] ;
					$data['phone_details'] = $phone_details ;
					$data['address_details'] = $address_details ;
						
					if ( getParsedEmail($db, $s, 'PROSPECT_ADD', $data, $email) ) {
						$to     = '';
						$to[]   = array('name' => $sales_name, 'email' => $sales_email);
						$from   = $reply_to = array('name' => $sales_name, 'email' => $sales_email);
						//echo $email["body"];
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
					} */
					//SEND MAIL TO ADMIN EOF					
				}
				$messages->setOkMessage("The New Quick Entry has been added."); 
				$_ALL_POST	= NULL;
				$data		= NULL;
			}
           
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
		/*      
		if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/sale-quick.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/sale-quick.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/sale-quick.php?added=1");   
        } 
		*/
        

            //$_ALL_POST['number'] = LeadsRivals::getNewAccNumber($db);
            if ( empty($data['country']) ) {
                $data['country'] = '91';
            }
            
            if(empty($data['state'])){
                $data['state']='';
            }
            
            if(empty($data['city'])){
                $data['city']='';
            }
			if(empty($_ALL_POST['lead_source_to']) && empty($_ALL_POST['lead_source_from'])){
                $_ALL_POST['lead_source_to']= 3;
            }
            
            
			
            $lst_country = $lst_state = $lst_city = array();
            $lst_country    = $regionlead->getCountryList($data['country']);
            $lst_state      = $regionlead->getStateList($data['state']);
            $lst_city       = $regionlead->getCityList($data['city']);
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'ajx','value' => $ajx);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //$hidden[] = array('name'=> 'reminder_count', 'value' => '6');

            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
            $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
            $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
            $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
            $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
            $page["var"][] = array('variable' => 'segment_list', 'value' => 'segment_list');
			$page["var"][] = array('variable' => 'my', 'value' => 'my');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-quick-add.html');
            //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-quick-div.html');
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
	//echo  $start['microtime'] = microtime();
?>