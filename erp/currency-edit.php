<?php
if ( $perm->has('nc_cur_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/currency.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Currency::validateUpdate($data, $extra) ) {
                
                $sql= " SELECT title FROM ".TABLE_COUNTRIES." WHERE ".TABLE_COUNTRIES.".code='".$data['country_code']."'";
                $db->query($sql);
                $country_name ='';
                if ( $db->nf()>0 ) {
                    while ( $db->next_record() ) {
                        $country_name = $db->f('title') ;
                    }
                }
                
               $query  = " UPDATE ". TABLE_SETTINGS_CURRENCY
                            ." SET ". TABLE_SETTINGS_CURRENCY .".currency_name = '".		$data['currency_name'] ."'"
                                   	.",". TABLE_SETTINGS_CURRENCY .".currency_symbol = '". 		$data['currency_symbol'] ."'"
									.",". TABLE_SETTINGS_CURRENCY .".abbr = '". 		$data['abbr'] ."'"
									.",". TABLE_SETTINGS_CURRENCY .".country_code = '". 		$data['country_code'] ."'"
									.",". TABLE_SETTINGS_CURRENCY .".country_name = '". 		$country_name ."'"
									.",". TABLE_SETTINGS_CURRENCY .".status = '". 		$data['status'] ."'"                               
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Currency has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SETTINGS_CURRENCY .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SETTINGS_CURRENCY .".id = '". $id ."' )";
            
            if ( Currency::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/currency-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'currency-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
