<?php

    if ( $perm->has('nc_bl_or_up_list') ) {
		
	    $variables["pending"]   = BillOrderUpload::PENDING;
		$variables["completed"] = BillOrderUpload::COMPLETED;
		$variables["cancelled"] = BillOrderUpload::CANCELLED;
		
       
        $_SEARCH['searched'] = true;
       
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        $_SEARCH['searched'] = 1;
        
        $condition_query = " LEFT JOIN ".TABLE_BILL_ORDERS." ON ".TABLE_BILL_ORDERS.".id=".TABLE_BILL_ORD_UPLOAD.".order_id "
                            ." LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id=".TABLE_BILL_ORD_UPLOAD.".created_by ". 
                            $condition_query ;

        //echo $condition_query;
        // To count total records.
        $list	= 	NULL;
        $fields =   '';
        $total	=	BillOrderUpload::getDetails( $db, $list, $fields, $condition_query);
    
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
    
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
        $list	= NULL;
        $fields = TABLE_BILL_ORD_UPLOAD .'.*, '.TABLE_BILL_ORDERS.'.order_title,'.TABLE_USER.'.f_name as added_by_fname,'.TABLE_USER.'.l_name as added_by_lname';
        BillOrderUpload::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        $fList=array();
        $typeArr =BillOrderUpload::getType();
        $typeArr  = array_flip($typeArr);
		if(!empty($list)){
			foreach( $list as $key=>$val){      
				
				
				if(isset($val['type']) ){
                    $val['type'] = $typeArr[$val['type']];
                }
               	
				
				$uploaded_by_name='';
				$table = TABLE_USER;
				$condition1 = " WHERE ".TABLE_USER .".user_id= '".$val['uploaded_by'] ."' " ;
				$fields1 =  TABLE_USER .'.f_name'.','.TABLE_USER .'.l_name';
				$uploaded_by= getRecord($table,$fields1,$condition1);
			        
				if(!empty($uploaded_by)){
				   
				   $uploaded_by_name = $uploaded_by['f_name']." ".$uploaded_by['l_name']."<br/>" ;
				}
				$val['uploaded_by_name']    = $uploaded_by_name ;     
			      
			   $fList[$key]=$val;
			}
		}
        
        
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_upload']        = false;
     
       
        if ( $perm->has('nc_bl_or_up_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_bl_or_up_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_bl_or_up_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_bl_or_up_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_bl_or_up_mu') ) {
            $variables['can_upload'] = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-upload-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>
