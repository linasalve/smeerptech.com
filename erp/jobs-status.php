<?php
    if ( $perm->has('nc_jb_status') ) {
        $id= isset($_GET["id"])? $_GET["id"] : ( isset($_POST["id"])? $_POST["id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
        $access_level = $my['access_level'];
       
       /* if ( $perm->has('nc_ue_status_al') ) {
            $access_level += 1;
        }*/
        
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        Jobs::updateStatus($id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/jobs-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>
