<?php
    if ( $perm->has('nc_hd_delete') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
        if ( $perm->has('nc_hd_delete_al') ) {
            $access_level += 1;
        }
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        Hardware::delete($id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/hardware-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the hardware entry.");
    }
?>