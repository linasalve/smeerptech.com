<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/prospects-quotation.inc.php');
    include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php');
    //include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/currency.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/followup.inc.php');
    
    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');
    $deleted 	= isset($_GET["deleted"])   ? $_GET["deleted"]  : ( isset($_POST["deleted"])    ? $_POST["deleted"] : '0');
    $inv_no 	= isset($_GET["inv_no"])    ? $_GET["inv_no"]   : ( isset($_POST["inv_no"])     ? $_POST["inv_no"]  : '');

    $hid 		= isset($_GET["hid"])         ? $_GET["hid"]        : ( isset($_POST["hid"])          ? $_POST["hid"]       :'');
  
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    $condition_url =$condition_query ='';
    //By default search in On
    $_SEARCH["searched"]    = true ;
    if($deleted){    
        $messages->setOkMessage("Proposal $inv_no has been Cancelled.");
    }
    if($added){    
        $messages->setOkMessage("New Proposal $inv_no has been created.");
    }
   
    
    if ( $perm->has('nc_ps_qt') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_PROSPECTS_QUOTATION  =>  array('ID'            => 'id',
                                                            'Proposal No.'   => 'number',
                                                            'LeadOrder No.'     => 'or_no',
                                                            'Remarks'       => 'remarks'
                                                        ),
								TABLE_PROSPECTS_ORDERS =>  array('Order Title'   => 'order_title-'.TABLE_PROSPECTS_ORDERS),
                                                        
                                // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                TABLE_USER  	=>  array(  'Creator Number'    => 'number-'.TABLE_USER,
                                                            'Creator Username'  => 'username-'.TABLE_USER,
                                                            'Creator Email'     => 'email-'.TABLE_USER,
                                                            'Creator First Name'=> 'f_name-'.TABLE_USER,
                                                            'Creator Last Name' => 'l_name-'.TABLE_USER,
													),
								TABLE_PROSPECTS =>  array(  'Billing Name'          => 'billing_name-'.TABLE_PROSPECTS,
                                                            //'Customer Number'       => 'number-'.TABLE_PROSPECTS,
                                                            'Prospect Username'     => 'username-'.TABLE_PROSPECTS,
                                                            'Prospect Email'        => 'email-'.TABLE_PROSPECTS,
                                                            'Prospect Company Name' => 'org-'.TABLE_PROSPECTS,
                                                            'Prospect First Name'   => 'f_name-'.TABLE_PROSPECTS,
                                                            'Prospect Last Name'    => 'l_name-'.TABLE_PROSPECTS,
                                                )
                               
                            );
        
        $sOrderByArray  = array(
                                TABLE_PROSPECTS_QUOTATION => array('ID'                => 'id',
                                                        'Proposal No.'       => 'number',
                                                        'Date of Proposal'   => 'do_i',
                                                        'Date of Creation'  => 'do_c',
                                                        'Status'            => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
           
            if(!empty($hid)){
                $_SEARCH['sOrderBy']= $sOrderBy = 'id';
                $variables['hid']=$hid;
            }else{
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_i';
            }
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PROSPECTS_QUOTATION;
        }

        // Read the available Status for the Pre Order.
        $variables['status']        = ProspectsQuotation::getStatus();
        $variables['qt_status']        = ProspectsQuotation::getQuotStatus();
        $variables['status_title']  = ProspectsQuotation::getStatusTitle();
       
        //use switch case here to perform action. 
        switch ($perform) {
            case ('add-test'): {            
                include (DIR_FS_NC.'/prospects-quotation-add-test.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('add-intro'): {            
                include (DIR_FS_NC.'/prospects-quotation-add-intro.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('add_pre_order'):			
            case ('add'): { 
                include (DIR_FS_NC.'/prospects-quotation-add.php'); 
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            /*Not require 
            case ('add_o'): {
            
                include (DIR_FS_NC.'/prospects-quotation-add-o.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            
            case ('edit_pre_order'):
            case ('edit'): {
                include (DIR_FS_NC .'/prospects-quotation-edit.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('verify'): {
                include (DIR_FS_NC .'/prospects-quotation-verify.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('services_pdf'): {
                include (DIR_FS_NC .'/prospects-quotation-create-service-pdf.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('view_pre_order'):
            case ('view'): {
                include (DIR_FS_NC .'/prospects-quotation-view.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('view_file'): {
                include (DIR_FS_NC .'/prospects-quotation-view-file.php');
                break;
            }
            case ('change_status'): {
                $searchStr=1;
                include ( DIR_FS_NC .'/prospects-quotation-status.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/prospects-quotation-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('send'): {
                
                include (DIR_FS_NC .'/prospects-quotation-send.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('delete_pre_order'):
            case ('delete'): {
                
                include ( DIR_FS_NC .'/prospects-quotation-delete.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('list_pre_order'):
            case ('list'):
            default: {
                $searchStr = 1;
				/* if(){
				
				} */
                include (DIR_FS_NC .'/prospects-quotation-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-quotation.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>