<?php
	if ( $perm->has('nc_ld_qt_status') ) {
        $inv_id	= isset($_GET["inv_id"]) ? $_GET["inv_id"] 	: ( isset($_POST["inv_id"]) ? $_POST["inv_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        
        $extra = array( 'db'           		=> &$db,
                        'my' 				=> $my,
						'access_level' 		=> $access_level,
						'access_level_ot' 	=> $access_level_ot,
                        'messages'     		=> &$messages
                    );
        LeadsQuotation::updateStatus($inv_id, $status, $extra);
        
        $perform = 'search';
        // Display the  list.
        include ( DIR_FS_NC .'/leads-quotation-search.php');
        
        //include ( DIR_FS_NC .'/bill-invoice-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>