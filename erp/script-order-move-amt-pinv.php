<?php

// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
	 
	 
	echo $sql= "SELECT ".TABLE_BILL_INV_PROFORMA.".or_no, 
	".TABLE_BILL_INV_PROFORMA.".tax1_id,
	".TABLE_BILL_INV_PROFORMA.".tax1_number,
	".TABLE_BILL_INV_PROFORMA.".tax1_name,
	".TABLE_BILL_INV_PROFORMA.".tax1_value,
	".TABLE_BILL_INV_PROFORMA.".tax1_pvalue,
	".TABLE_BILL_INV_PROFORMA.".tax1_total_amount,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub1_id,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub1_number,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub1_name,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub1_value,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub1_pvalue,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub1_total_amount,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub2_id,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub2_number,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub2_name,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub2_value,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub2_pvalue,
	".TABLE_BILL_INV_PROFORMA.".tax1_sub2_total_amount
	FROM ".TABLE_BILL_INV_PROFORMA." WHERE ".TABLE_BILL_INV_PROFORMA.".status IN('4','1','2') 
	AND ".TABLE_BILL_INV_PROFORMA.".tax1_id !=0	"; 
	
	$db->query($sql);
     
    $db1 		= new db_local;
    if ( $db->nf()>0 ) {
        while ( $db->next_record() ){ 
			$tax1_id = 	$tax1_number =$tax1_name =$tax1_value =	$tax1_pvalue =$tax1_total_amount ='';
			$tax1_sub1_id =  $tax1_sub1_number =  $tax1_sub1_name =  $tax1_sub1_value = $tax1_sub1_pvalue ='';
		    $tax1_sub1_total_amount = $tax1_sub2_id = $tax1_sub2_number = $tax1_sub2_name ='';
		    $tax1_sub2_value = $tax1_sub2_pvalue = $tax1_sub2_total_amount =$total_tax_amount='';
               
			$or_no =$db->f('or_no');
			$tax1_id =$db->f('tax1_id');
			$tax1_number =$db->f('tax1_number');
			$tax1_name =$db->f('tax1_name');
			$tax1_value =$db->f('tax1_value');
			$tax1_pvalue =$db->f('tax1_pvalue');
			$tax1_total_amount = $db->f('tax1_total_amount');
			$tax1_sub1_id =$db->f('tax1_sub1_id');
			$tax1_sub1_number =$db->f('tax1_sub1_number');
			$tax1_sub1_name =$db->f('tax1_sub1_name');
			$tax1_sub1_value =$db->f('tax1_sub1_value');
			$tax1_sub1_pvalue =$db->f('tax1_sub1_pvalue');
			$tax1_sub1_total_amount =$db->f('tax1_sub1_total_amount');
			$tax1_sub2_id =$db->f('tax1_sub2_id');
			$tax1_sub2_number =$db->f('tax1_sub2_number');
			$tax1_sub2_name =$db->f('tax1_sub2_name');
			$tax1_sub2_value =$db->f('tax1_sub2_value');
			$tax1_sub2_pvalue =$db->f('tax1_sub2_pvalue');
			$tax1_sub2_total_amount =$db->f('tax1_sub2_total_amount');
			$total_tax_amount = $tax1_total_amount + $tax1_sub1_total_amount + $tax1_sub2_total_amount ;
			 
			echo $sql_u = " UPDATE ".TABLE_BILL_ORDERS." SET 
				tax1_id='".$tax1_id."', 
				tax1_number='".$tax1_number."', 
				tax1_name='".$tax1_name."',
				tax1_value='".$tax1_value."', 
				tax1_pvalue='".$tax1_pvalue."', 
				tax1_total_amount='".$tax1_total_amount."', 
				tax1_sub1_id='".$tax1_sub1_id."', 
				tax1_sub1_number='".$tax1_sub1_number."', 
				tax1_sub1_name='".$tax1_sub1_name."', 
				tax1_sub1_value='".$tax1_sub1_value."', 
				tax1_sub1_pvalue='".$tax1_sub1_pvalue."', 
				tax1_sub1_total_amount='".$tax1_sub1_total_amount."', 
				tax1_sub2_id='".$tax1_sub2_id."', 
				tax1_sub2_number='".$tax1_sub2_number."', 
				tax1_sub2_name='".$tax1_sub2_name."', 
				tax1_sub2_value='".$tax1_sub2_value."', 
				tax1_sub2_pvalue='".$tax1_sub2_pvalue."', 
				tax1_sub2_total_amount='".$tax1_sub2_total_amount."', 
				total_tax_amount='".$total_tax_amount."', 
				total_tax_balance_amount='".$total_tax_amount."'
				WHERE number='".$or_no."'" ;
			
			$db1->query($sql_u); 
			echo "<br/>";	 
		}
	}
	
	
	
?>  		 