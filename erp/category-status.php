<?php
    if ( $perm->has('nc_cat_status') ) {
        $sc_cat_id= isset($_GET["cat_id"])? $_GET["cat_id"] : ( isset($_POST["cat_id"])? $_POST["cat_id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $extra = array( 'db' 		    => &$db,
                        'messages' 	    => $messages
                    );
        
        Categories::updateStatus($sc_cat_id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/category-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Change Status.");
    }
?>