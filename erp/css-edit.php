<?php
	if(!defined( "THIS_DOMAIN"))
		{
			require("../lib/config.php");
		}	
		page_open(array(
						"sess"=>"NC_Session",
						"auth"=>"NC_Auth",
						"perm"=>"NC_Perm"
						));
	include(DIR_FS_NC."/header.php");
	//include( DIR_FS_CLASS ."Validation.class.php");

	if ( $perm->has('nc_site_css_edit') ) {
		//if (isset($_POST['b1']) && $_POST['b1'] == 'Submit') 
		if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && @$_POST['act'] == 'edit') 
		{
			$cssContent = isset($_GET["cssContent"]) 	? $_GET["cssContent"] 	: (isset($_POST["cssContent"])	? $_POST["cssContent"] : '');
			if(Validation::isEmpty($cssContent))	{
				$messages->setErrorMessage("content should be provided.");
			}
			if( $messages->getErrorMessageCount() <= 0 )
			{
				/******* BO :: Adding content to the css files ****************/
				$handle = fopen (DIR_FS_CSS."/en_IN_User_Style.css", "w+");
				if((int)$handle == 0)
				{
					$messages->setErrorMessage("Unable to open css file.");
				}
				else
				{
					fwrite($handle, $cssContent);
					fclose($handle);
					$messages->setOkMessage("CSS file updated successfully");
				}
				/******* EO :: Adding content to the css files ****************/
			}
		}
		
		/******* BO :: read content to the css files ****************/
		$handle = fopen (DIR_FS_CSS."/en_IN_User_Style.css", "a+");
		if((int)$handle == 0)
		{
			$messages->setErrorMessage("Unable to open css file.");
		}
		else
		{
			$cssContent = fread($handle, filesize(DIR_FS_CSS."/en_IN_User_Style.css"));
			fclose($handle);
		}
		/******* EO :: read content to the css files ****************/
    }
    else
        $messages->setErrorMessage("You do not have the Right to Access this Module.");

    $page["var"][] = array('variable' => 'cssContent', 'value' => 'cssContent');

	$page["section"][] = array('container'=>'CONTENT', 'page' => 'css-edit.html');
	$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');

    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>
