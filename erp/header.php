<?php
	//ob_start("ob_gzhandle");
	ob_start();
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Pragma: no-cache"); 
	
	if (!@constant("THIS_DOMAIN")){
		require_once ("../lib/config.php"); 
	} 
   	include_once(DIR_FS_CLASS."/class.datetime.php");
    include_once ( DIR_FS_CLASS .'/Validation.class.php');
    include_once ( DIR_FS_INCLUDES ."/navigation.inc.php");
    include_once ( DIR_FS_INCLUDES .'/announcement.inc.php');
    
    
	/*
	include_once ( DIR_FS_INCLUDES .'/encryption.inc.php');
	$obje = new Encryption();
	echo $data = $obje->encode('testing dd @');
	echo "<br/>";
	echo $data_org = $obje->decode( $data);
	*/
	 
	
   // echo 'Initial: ' . number_format(memory_get_usage(), 0, '.', ',') . " bytes\n";
	
	$db = new db_local; // database handle
	//$s = new smarty; 	//smarty template handle
	$s = new Smarty();
	$messages 	= new Messager;	// create the object for the Message class.
	$datetime 	= new UsrDateTime ;
	//putenv("TZ=GMT"); // done in config.php
	//echo 'open: ' . number_format(memory_get_usage(), 0, '.', ',') . " bytes\n";
	//print_R($GLOBALS['_PHPA']);
	//echo $date_exp_int 	= mktime(date("h"), date("i"), date("s"), 0,0, 0) ;
	// Initialize the configuratioin of the Template engine.
 
	$s->debugging = false;
	$s->caching = false;
	$s->cache_lifetime = 0;
	
	$s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	//$s->allow_php_templates= true;
	$s->force_compile = true;
	//$s->caching = true;
	//$s->cache_lifetime = 100 ;
	
	$s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	//$s->allow_php_templates= true;
	$s->force_compile = true;
	//$s->caching = true;
	//$s->cache_lifetime = 100; 
	/* 	
	$array1 =array(
		'id'=>1,
		'name'=>'leena',
		'm_name'=>'D',
		'l_name'=>'Salve'		
	);
	$array2 =array(
		'id'=>1,
		'name'=>'Leena',
		'm_name'=>'p',
		'l_name'=>'Salve' 
	);
	$result = array_diff($array1, $array2);
	print_R($result); 
	*/ 
	$ajx = isset($_GET["ajx"])         ? $_GET["ajx"]        : ( isset($_POST["ajx"])  ? $_POST["ajx"]       :'0');
	$curr_page = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'],"/")+1) ;
	$query_str = isset($_GET["perform"]) ? $_GET["perform"] : ( isset($_POST["perform"]) ? $_POST["perform"] : '' );
	$query_str = 'perform='. $query_str;	
	$variables['curr_page'] = $curr_page;
	$variables['query_str'] = $query_str;
    $variables['team_photo_url'] = DIR_WS_TEAMPHOTO_FILES;
	/*  
	$variables['notifications']="$.jGrowl(\"Sticky notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio notificatio otificatiootification with a header\", { header: 'A Header', sticky: true ,beforeClose: function() {
						return false;
					}});$.jGrowl(\"Sticky notification with a header2\", { header: 'A Header', sticky: true ,beforeClose: function() {
						return false;
					}});";  
	*/
    if ( is_object($auth) ) {
        if ( $auth->auth["logged_in"] ){
            $my = $auth->auth;
             //Used in Hr-attendance Module
            $my["timezone"]		= $local_setting["timezone"];
            $my["time_offset"]	= $local_setting["time_offset"];
            /****************************************************************
            // To check if the user has marked his attendence 
            // for today or not.
			att_status     Meaning
			0			User has not marked his attendence for today .
			1			User has marked his attendence for today .
			2			User has marked his attendence for today and 
							also entered leaving time i.e. not present 
							in Office.
            ****************************************************************/
            $query = "SELECT att_id, att_time_out FROM ". TABLE_HR_ATTENDANCE
                        ." WHERE uid = '". $my["uid"] ."' "
                        ." AND DATE_FORMAT(att_time_in,'%Y-%m-%d') ='".date("Y-m-d")."'"
                        ." ORDER BY att_time_in DESC" ;

            $db->query( $query );
            if ( $db->next_record() ) {
                // User has marked his attencence for today
                // Now to check if user has entered leaving time.
                $data = processSqlData( $db->Record );
                if ( $data["att_time_out"] != "0000-00-00 00:00:00") {
                    // User has enterd his/her leaving time.
                    $my["att_status"] = "2";
                }
                else {
                    $my["att_status"] = "1"; //Not marked Out time
                }
                $my["att_id"] = $data["att_id"];
            }
            else {
                // User has not marked his attencence
                $my["att_status"] = "0"; 
            }
            // Attendence check end
        } else {
            $sess->delete();
            $auth->logout();
            echo "<font color='red' size=3>Access Denied!</font>";
            exit;
        }
	} 
    $ip = $_SERVER['REMOTE_ADDR'] ;
   /* Code to check valid IP whether */
   
   if ( is_object($auth) ) {
        if($auth->auth["allow_ip"]== 1){ // if allow_ip is ANY            
            if ( isset($auth->auth["logged_in"]) && $auth->auth["logged_in"] ) {
                $my = $auth->auth;
                 //Used in Hr-attendance Module
                $my["timezone"]		= $local_setting["timezone"];
                $my["time_offset"]	= $local_setting["time_offset"];
                $logged_in = true;                
                /*  // Update the last login field.
                $query = "UPDATE ". TABLE_AUTH_USER
                ." SET do_login = '". (date("Y-m-d H:i:s", time())) ."' , login_ip='".$_SERVER['REMOTE_ADDR']."' "
                ." WHERE user_id = '".$my["uid"]."'";
				
                $db->query($query);
                $query = NULL;
                //insert into login stats
                $ip = $_SERVER['REMOTE_ADDR'] ;      
                $login_status=1;
                $date=date("Y-m-d H:i:s");
                $insert = " INSERT INTO ".TABLE_LOGIN_STATS
							. " SET "
							.TABLE_LOGIN_STATS.".number = '".$auth->auth['number']."'"
							.",".TABLE_LOGIN_STATS.".username 	= '".$auth->auth['uname']."'"
							.",".TABLE_LOGIN_STATS.".password 	= ''"
							.",".TABLE_LOGIN_STATS.".terms 	= '1'"
							.",".TABLE_LOGIN_STATS.".date 	= '". $date ."'"
							.",".TABLE_LOGIN_STATS.".ip 	= '". $ip ."'"
							.",".TABLE_LOGIN_STATS.".login_status 	= '". $login_status ."'" ;
                $db->query($insert); 
				*/
                 /****************************************************************
                // To check if the user has marked his attendence 
                // for today or not.
				att_status     Meaning
				0			User has not marked his attendence for today .
				1			User has marked his attendence for today .
				2			User has marked his attendence for today and 
								also entered leaving time i.e. not present 
								in Office.
                ****************************************************************/
                $query = "SELECT att_id, att_time_out FROM ". TABLE_HR_ATTENDANCE
                            ." WHERE uid = '". $my["uid"] ."' "
                            ." AND DATE_FORMAT(att_time_in,'%Y-%m-%d') ='".date("Y-m-d")."'"
                            ." ORDER BY att_time_in DESC" ;
    
                $db->query( $query );
                if ( $db->next_record() ) {
                    // User has marked his attencence for today
                    // Now to check if user has entered leaving time.
                    $data = processSqlData( $db->Record );
                    if ( $data["att_time_out"] != "0000-00-00 00:00:00") {
                        // User has enterd his/her leaving time.
                        $my["att_status"] = "2";
                    }
                    else {
                        $my["att_status"] = "1";
                    }
                    $my["att_id"] = $data["att_id"];
                }
                else {
                    // User has not marked his attencence
                    $my["att_status"] = "0";
                }                
                // Attendence check end
            }
            else {
                $logged_in = false;
            }
        }elseif($auth->auth["allow_ip"]== 2 ){
   
            $allowedIpArr = explode(",",$auth->auth["valid_ip"]);
			if(in_array($ip,$allowedIpArr )){
				if ( isset($auth->auth["logged_in"]) && $auth->auth["logged_in"] ) {
					$my = $auth->auth;
					//Used in Hr-attendance Module
					$my["timezone"]		= $local_setting["timezone"];
					$my["time_offset"]	= $local_setting["time_offset"];
					$logged_in = true;
					/*  // Update the last login field.
					$query = "UPDATE ". TABLE_AUTH_USER
							 ." SET do_login = '". (date("Y-m-d H:i:s", time())) ."', login_ip='".$_SERVER['REMOTE_ADDR']."' "
							 ." WHERE user_id = '".$my["uid"]."'";
					$db->query($query);                        
					$query = NULL;                        
					//insert into login stats
					$ip = $_SERVER['REMOTE_ADDR'] ;      
					$login_status=1;
					$date=date("Y-m-d H:i:s");
					$insert = " INSERT INTO ".TABLE_LOGIN_STATS
												. " SET "
												.TABLE_LOGIN_STATS.".number = '".$auth->auth['number']."'"
												.",".TABLE_LOGIN_STATS.".username 	= '".$auth->auth['uname']."'"
												.",".TABLE_LOGIN_STATS.".password 	= ''"
												.",".TABLE_LOGIN_STATS.".terms 	= '1'"
												.",".TABLE_LOGIN_STATS.".date 	= '". $date ."'"
												.",".TABLE_LOGIN_STATS.".ip 	= '". $ip ."'"
												.",".TABLE_LOGIN_STATS.".login_status 	= '". $login_status ."'" ;
				
					 
					$db->query($insert); */
					/****************************************************************
					// To check if the user has marked his attendence 
					// for today or not.
					
						att_status     Meaning
						0			User has not marked his attendence for today .
						1			User has marked his attendence for today .
						2			User has marked his attendence for today and 
										also entered leaving time i.e. not present 
										in Office.
					****************************************************************/
					$query = "SELECT att_id, att_time_out FROM ". TABLE_HR_ATTENDANCE
								." WHERE uid = '". $my["uid"] ."' "
								." AND DATE_FORMAT(att_time_in,'%Y-%m-%d') ='".date("Y-m-d")."'"
								." ORDER BY att_time_in DESC" ;
		
					$db->query( $query );
					if ( $db->next_record() ) {
						// User has marked his attencence for today
						// Now to check if user has entered leaving time.
						$data = processSqlData( $db->Record );
						if ( $data["att_time_out"] != "0000-00-00 00:00:00") {
							// User has enterd his/her leaving time.
							$my["att_status"] = "2";
						}
						else {
							$my["att_status"] = "1";
						}
						$my["att_id"] = $data["att_id"];
					}
					else {
						// User has not marked his attencence
						$my["att_status"] = "0";
					}
					
					// Attendence check end
					
					
				}
				else {
					$logged_in = false;
				}
			}else{                    
				
				 //insert into login stats
					$ip = $_SERVER['REMOTE_ADDR'] ;      
					$login_status=2;
					$date=date("Y-m-d H:i:s");
					$insert = " INSERT INTO ".TABLE_LOGIN_STATS
												. " SET "
												.TABLE_LOGIN_STATS.".number = '".$auth->auth['number']."'"
												.",".TABLE_LOGIN_STATS.".username 	= '".$auth->auth['uname']."'"
												.",".TABLE_LOGIN_STATS.".password 	= ''"
												.",".TABLE_LOGIN_STATS.".terms 	= '1'"
												.",".TABLE_LOGIN_STATS.".date 	= '". $date ."'"
												.",".TABLE_LOGIN_STATS.".ip 	= '". $ip ."'"
												.",".TABLE_LOGIN_STATS.".login_status 	= '". $login_status ."'" ;
				
					 
					$db->query($insert);                   
				$auth->logout();
				$auth = NULL;
				header("Location: ".DIR_WS_NC."/invalid-account.php");                   
				exit;
			}        
        }
   }
    if($ajx==0){
		//Total Pending Task BOF
		$taskSummary=array();
		include_once ( DIR_FS_INCLUDES .'/task-reminder.inc.php' );
		//TABLE_TASK_REMINDER .".do_r <= '". $rDate ."' " ." AND
		if ( isset($auth->auth["logged_in"]) && $auth->auth["logged_in"] ) {
		   $sqlTask2 = " SELECT COUNT(*) as pendingTasks FROM ".TABLE_TASK_REMINDER ."   
							 WHERE  ( "
									. TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] ."$' OR "
									. TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] ."$' OR "
									. TABLE_TASK_REMINDER .".allotted_to REGEXP ',". $my['user_id'] .",' OR "
									. TABLE_TASK_REMINDER .".allotted_to REGEXP '^". $my['user_id'] .",' "
								. " ) AND ".TABLE_TASK_REMINDER.".status IN( '".Taskreminder::PENDING."' ) ";
			 
			 $db->query($sqlTask2);  
			 $pendingTasks =0;
			 if ( $db->nf()>0 ) {
				while ( $db->next_record() ) { 
					$pendingTasks=$db->f('pendingTasks'); 
					$taskSummary['pendingTasks']=$pendingTasks;
				}
			}
		}
		$page["var"][] = array('variable' => 'pendingTasks', 'value' => 'pendingTasks');
	 
     
		$variables['can_mark_att'] = false;
		if ( $perm->has('nc_hr_att_mark') &&  $perm->has('nc_hr_att') ) {
			$variables['can_mark_att'] = true;
		}
		/* BOF Announcement   */
		$mtime          = mktime()-86400;
		$condition_query_announce='';
		$fields = TABLE_ANNOUNCEMENT.'.*';
/* 		$condition_query_announce      = " WHERE status = '".Announcement::ACTIVE."'"   							
								." AND do_e >= '". $mtime ."'"
								." ORDER BY do_e DESC LIMIT 0,4"; */
		$condition_query_announce  = " WHERE status = '".Announcement::ACTIVE."'" 
								." ORDER BY do_e ";
		Announcement::getDetails( $db, $list_announce , $fields, $condition_query_announce);
		/* EOF Announcement   */
		$page["var"][] = array('variable' => 'list_announce', 'value' => 'list_announce');
		
		//Birth day List BOF
		$sql = "SELECT f_name,l_name FROM ".TABLE_USER." WHERE date_format(".TABLE_USER.".do_birth,'%m-%d')
		='".date('m-d')."' AND status='1'" ;
		$db->query($sql);  			 
		if ( $db->nf()>0 ) {
			while ( $db->next_record() ) { 
				$bday_list[] = $db->f('f_name')." ".$db->f('l_name'); 
			}
		}
		$page["var"][] = array('variable' => 'bday_list', 'value' => 'bday_list');
		//Birth day List EOF
	}
	$variables["title"] 			= TITLE ;
	$variables["domain"] 			= THIS_DOMAIN;
    $variables["lib"]               = DIR_WS_LIB;
    $variables["nc"] 				= DIR_WS_NC;
    $variables["mp"] 				= DIR_WS_MP;
    $variables["nc_images"]		    = DIR_WS_IMAGES_NC;
    $variables["images"]            = DIR_WS_IMAGES;
    $variables["css"]               = DIR_WS_CSS;
    $variables["scripts"]           = DIR_WS_SCRIPTS;
    $variables["file_save_domain"] 	= DIR_WS_ST_FILES;
    $variables["year"]              = date('Y', time());
    
    // Status
    $variables["blocked"]           = BLOCK;
    $variables["active"]            = ACTIVE;
    $variables["pending"]           = PENDING;
    $variables["deleted"]           = DELETED;
    $variables["completed"]         = COMPLETED;
 
    //Comment on 11-02-10 
	//getMenuLinks( $db, "", $menus );
    //$page["section"][] = array('container'=>'MENU', 'page' => 'menu.html');
    //include munu access file
    //require_once ("menu-access.php"); 
    
     /* BOF top menu  */       
		$curr_page = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'],"/")+1) ;
		$query_str = isset($_GET["perform"]) ? $_GET["perform"] : ( isset($_POST["perform"]) ? $_POST["perform"] : '' );
		$query_str = 'perform='. $query_str;
		//$main_menu = getAdminMenu($curr_page, $query_str, $variables);
    /* EOF top menu*/
    if($ajx==0){
		/* if(isset($_SESSION['main_menu']) && !empty($_SESSION['main_menu']) ){       
			$main_menu =  $_SESSION['main_menu'] ;
		}else{               
			$_SESSION['main_menu'] =  $main_menu = Navigation::getMenu($curr_page, $query_str, $perm);    
		} */
		$main_menu = Navigation::getMenu($curr_page, $query_str, $perm);    
		$s->assign("main_menu", $main_menu);
		
	}
	//print_R( $main_menu);
   
    if(isset($_SESSION['toggelVal']) && !empty($_SESSION['toggelVal']) ){
        //echo $_SESSION['toggelVal'];        
        $toggelVal= $_SESSION['toggelVal'] ;
    }else{
         
         $toggelVal='left';
         $_SESSION['toggelVal'] = $toggelVal;
    
    }
    
    $displayLanding = 0;
    if($curr_page =='index.php'){
       $displayLanding=1 ;
    }
    $variables['add_transaction']=false;
 	$variables['add_task']=false;
	$variables['add_score']=false;
	$variables['add_supportticket']=false;
	$variables['add_ordm']=false;	
	$variables['list_ordm']=false;	
	$variables['add_qprospect']=false;	
	$variables['nc_ts_sjmadd']=false;	
	$variables['create_service_pdf']= false;	
	$variables['add_sales_reporting']= false;	
    //print_r($my);
	if ( $perm->has('nc_p_pt') && $perm->has('nc_p_pt_add')) {
		$variables['add_transaction']=true;	
	}
	if ( $perm->has('nc_ts') && $perm->has('nc_ts_add')) {
		$variables['add_task']=true;	
	}
	
	if ( $perm->has('nc_ss') && $perm->has('nc_ss_add')) {
		$variables['add_score']=true;	
	}
	if ( $perm->has('nc_st') && $perm->has('nc_st_add')) {
		$variables['add_supportticket']=true;	
	}
	
	if ( $perm->has('nc_ordm_t') && $perm->has('nc_ordm_t_add')) {
		$variables['add_ordm']=true;	
	}
	if ( $perm->has('nc_ordm_t') && $perm->has('nc_ordm_t_list')) {
		$variables['list_ordm']=true;	
	}
	if ( $perm->has('nc_pr') && $perm->has('nc_pr_qadd')) {
		$variables['add_qprospect']=true;	
	}
	if ( $perm->has('nc_ts') && $perm->has('nc_ts_sjmadd')) {
		$variables['add_sjmtask']=true;	
	}
	if ( $perm->has('nc_ps_qt') && $perm->has('nc_ps_qt_cr_srv_pdf')) {
		$variables['create_service_pdf']=true;	
	}
	if ( $perm->has('nc_pqt') && $perm->has('nc_pqt_add')) {
		$variables['add_quote']=true;	
	} 
	if ( $perm->has('nc_pst') && $perm->has('nc_pst_add')) {
		$variables['add_sales_reporting']=true;	
	}
	
	$s->assign("my", $my);
   
   
	//$s->assign("menus", $menus);
	//$s->assign("menu_count", count($menus));
	$s->assign("variables", $variables);
    //$s->assign("list_announce", $list_announce);
	$s->assign("displayLanding", $displayLanding);
	$s->assign("toggelVal", $toggelVal);
?>
