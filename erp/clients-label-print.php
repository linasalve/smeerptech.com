<?php

    if ( $perm->has('nc_uc_lblprn') ) {
        include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
        
        include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
		$client_id = isset($_GET['client_id']) ? $_GET['client_id'] : ( isset($_POST['client_id'] ) ? $_POST['client_id'] :'');
        $pinv_id = isset($_GET['pinv_id']) ? $_GET['pinv_id'] : ( isset($_POST['pinv_id'] ) ? $_POST['pinv_id'] :'');
        $inv_id = isset($_GET['inv_id']) ? $_GET['inv_id'] : ( isset($_POST['inv_id'] ) ? $_POST['inv_id'] :'');
        $add_id = isset($_GET['add_id']) ? $_GET['add_id'] : ( isset($_POST['add_id'] ) ? $_POST['add_id'] :'');
         
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
		if(!empty($pinv_id)){
			$fields =TABLE_BILL_INV_PROFORMA.'.billing_address,'.TABLE_CLIENTS.".title,"
			.TABLE_CLIENTS.".mobile1,".TABLE_CLIENTS.".mobile2,".TABLE_CLIENTS.".f_name,
			".TABLE_CLIENTS.".l_name, ".TABLE_CLIENTS.".billing_name,".TABLE_CLIENTS.".user_id " ;
			$condition_query = " WHERE (". TABLE_BILL_INV_PROFORMA .".id = '". $pinv_id ."') " ;
			InvoiceProforma::getDetails($db, $_ALL_POST, $fields, $condition_query);
		}
		if(!empty($inv_id)){
			$fields =TABLE_BILL_INV.'.billing_address,'.TABLE_CLIENTS.".title,"
			.TABLE_CLIENTS.".mobile1,".TABLE_CLIENTS.".mobile2,
			".TABLE_CLIENTS.".f_name,
			".TABLE_CLIENTS.".l_name, ".TABLE_CLIENTS.".billing_name,".TABLE_CLIENTS.".user_id " ;
			$condition_query = " WHERE (". TABLE_BILL_INV .".id = '". $inv_id ."') " ;
			Invoice::getDetails($db, $_ALL_POST, $fields, $condition_query);
			//".TABLE_CLIENTS.".map,
		}
		
		if(!empty($add_id)){
		
			$table = TABLE_CLIENTS;
			$fields1 =  TABLE_CLIENTS .'.billing_name' ;
			$condition1 = " WHERE ".TABLE_CLIENTS .".user_id ='".$client_id."' " ;
			$arr = getRecord($table,$fields1,$condition1);
			if(!empty($arr)){
				$billing_name = $arr['billing_name'];
			}
		
			$_ALL_POST['0'] = array(
				'user_id'=>$client_id,
				'billing_address'=>$add_id,
				'billing_name'=>$billing_name
			);
		}
		 
		
       if ( !empty($_ALL_POST) ) {
		
				$data = $_ALL_POST['0'];
            
            
                include_once ( DIR_FS_CLASS .'/Region.class.php');
                include_once ( DIR_FS_CLASS .'/Phone.class.php');
				$phone          = new Phone(TABLE_CLIENTS);
                $region         = new Region();
                $region->setAddressOf(TABLE_CLIENTS,  $data['user_id']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $address_list  =  $address_list;
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
        
				$phone->setPhoneOf(TABLE_CLIENTS, $data['user_id']);
                $data['phone'] = $phone->get($db);
                //TABLE_CLIENTS
               /*  $data['mobile'] = '';
                if(!empty($data['mobile1']) && !empty($data['mobile2'])){
					$data['mobile'] = $data['mobile1'].", ".$data['mobile2'] ;
				}elseif(!empty($data['mobile1']) && empty($data['mobile2'])){
					$data['mobile'] = $data['mobile1'];
				}elseif(empty($data['mobile1']) && !empty($data['mobile2'])){
					$data['mobile'] = $data['mobile2'];
				} */
				
                Clients::clientAddressPdf($data);				
				$file_path = DIR_FS_GN_FILES ."/";
                $file_name = "address.pdf";
                $content_type = 'application/pdf';
				header("Pragma: public"); // required
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false); // required for certain browsers 
				header("Content-type: $content_type");
				header("Content-Disposition: attachment; filename=". $file_name );
				header("Content-Length: ".filesize($file_path.$file_name));
				readfile($file_path.$file_name);
                exit;
				 
                $page["var"][] = array('variable' => 'data', 'value' => 'data');
    
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-label-print.html');
             
        }
        else {
            $messages->setErrorMessage("The Executives details were not found. ");
        }  
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>