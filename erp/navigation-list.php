<?php
if ( $perm->has('nc_nav_list') ) {
	   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $condition_query1 = " WHERE subTbl.parent_id  != 0 ".$condition_query;  
        
    
    $list	= 	NULL;
    $total	=	Navigation::getList( $db, $list, '', $condition_query1);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    //$fields = TABLE_NAVIGATION.'.*';
    $fields = 'subTbl.id,subTbl.parent_id,subTbl.title,subTbl.do_e ,subTbl.status,subTbl.ip,subTbl.access_level,subTbl.created_by,
	subTbl.page,subTbl.image,subTbl.level,subTbl.selected_page,subTbl.sequence,subTbl.perm_name,subTbl.main_perm_name,mTbl.title as main_menu_name';
	
	 
	
    Navigation::getList( $db, $fList, $fields, $condition_query1, $next_record, $rpp);
 
	
    /*$fList=array();
		if(!empty($list)){
			foreach( $list as $key=>$val){      
				
				/$main_menu=$main_menu_name='';
				$table = TABLE_NAVIGATION;
				$condition1 = " WHERE ".TABLE_NAVIGATION .".id= '".$val['parent_id'] ."' " ;
				$fields1 =  TABLE_NAVIGATION .'.title';
				$main_menu= getRecord($table,$fields1,$condition1);
			
				if(!empty($main_menu)){
				   
				   $main_menu_name = $main_menu['title']."<br/>" ;
				}
				$val['main_menu_name']    = $main_menu_name ;     
			     			 
			   $fList[$key]=$val;
			}
		}*/
    
    
    	// Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_change_status']  = false;
	   
		if ( $perm->has('nc_nav_list') ) {
			$variables['can_view_list'] = true;
		}
		if ( $perm->has('nc_nav_add') ) {
			$variables['can_add'] = true;
		}
		if ( $perm->has('nc_nav_edit') ) {
			$variables['can_edit'] = true;
		}
		if ( $perm->has('nc_nav_delete') ) {
			$variables['can_delete'] = true;
		}    
		if ( $perm->has('nc_nav_status') ) {
			$variables['can_change_status']     = true;
		 }
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'navigation-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
