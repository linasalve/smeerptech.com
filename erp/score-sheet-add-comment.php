<?php   
	if ( $perm->has('nc_ss_add_comment') ) {
    
    $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');
    $condition_url='';
    
    $_ALL_POSTDATA	   =$_ALL_POST = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
       
    // Read the record details
    $fields = TABLE_SCORE_SHEET .'.*'  ;            
    $condition_query = " WHERE (". TABLE_SCORE_SHEET .".id = '". $id ."' )";
           
    if ( Scoresheet::getDetails($db, $_ALL_POSTDATA, $fields, $condition_query) > 0 ){
        
        $_ALL_POSTDATA = $_ALL_POSTDATA['0'];
        $created_by = $_ALL_POSTDATA['created_by'];
        /*
        $allotted_to = $_ALL_POSTDATA['allotted_to'];
        $access_level = $_ALL_POSTDATA['access_level'];
        $allottedtoArr = explode(",", $allotted_to) ;
        */
        //array_push($allottedtoArr,$created_by) ;
        
        // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null;
       // $allotted_to_str= str_replace(',',"','",$allotted_to);
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
       
        $my_condition_query = " WHERE ".TABLE_USER .".status ='".User::ACTIVE."' ORDER BY ".TABLE_USER.".f_name" ;        
        User::getList($db,$lst_executive,$fields,$my_condition_query);
        // BO:  Read the Team members list ie member list in dropdown box  EOF

      
        
            if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
                  $_ALL_POST  = $_POST;
                  $data       = processUserData($_ALL_POST);
            
            
                    $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          			$db1 		= new db_local;
          			
                if ( Scoresheet::validateCommentAdd($data, $extra) ) {
                    // insert into log of task reminder
                    
         			 $query	= " INSERT INTO ".TABLE_SCORE_SHEET_LOG
								." SET ". TABLE_SCORE_SHEET_LOG .".task_id = '".              $id ."'"
                                .",". TABLE_SCORE_SHEET_LOG .".by_id = '".                $my['user_id'] ."'"
                                .",". TABLE_SCORE_SHEET_LOG .".to_id = '".                implode(',',$data['to_id']) ."'"
                                .",". TABLE_SCORE_SHEET_LOG .".comment = '".              $data['comment'] ."'"
                                .",". TABLE_SCORE_SHEET_LOG .".do_e = '".                 date('Y-m-d h:i:s') ."'"
                                .",". TABLE_SCORE_SHEET_LOG .".ip            = '". 		$_SERVER['REMOTE_ADDR'] ."'"      
                                .",". TABLE_SCORE_SHEET_LOG .".created_by = '".$my['user_id'] ."'";
                                $db->query($query);                                           
                     
                   
                   $to=implode(',',$data['to_id']);
               	   $string = str_replace(",","','", $to );
                   
                   $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                   $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                   User::getList($db,$executive,$fields1,$condition1);
                   $executivename='';
                  
                   foreach($executive as $key1=>$val1){
                        $executivename .= $val1['f_name']." ".$val1['l_name']."," ;
                   } 
                   
                                        
					$query_last_comment  = " UPDATE ". TABLE_SCORE_SHEET
                          	." SET ". TABLE_SCORE_SHEET .".last_comment = '".           $data['comment'] ."'"
                                .",". TABLE_SCORE_SHEET .".last_comment_by = '".      $my['user_id'] ."'"
                                .",". TABLE_SCORE_SHEET .".last_comment_by_name = '". $my['f_name']." ".$my['l_name'] ."'"
                                .",". TABLE_SCORE_SHEET .".last_comment_to = '".      implode(',',$data['to_id']) ."'"
                                .",". TABLE_SCORE_SHEET .".last_comment_to_name = '". $executivename ."'"
                                .",". TABLE_SCORE_SHEET .".last_comment_dt = '".      date('Y-m-d h:i:s') ."'"
                          ." WHERE id = '". $id ."'";
                	$db1->query($query_last_comment); 
                    
                    $messages->setOkMessage("Comment added successfully.");
                }		
                
            }
            /*
            if (  (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && !empty($frm) && $messages->getErrorMessageCount() <= 0 ) {
                
                      $url = $variables['nc']."/task-reminder.php?perform=list";
                
             
                ?>
                        <!--<script language="javascript" type="text/javascript">        
                         window.close();
                         window.opener.location.href="<?php echo $url ;    ?>"
                      </script>   -->
                
                <?php
                
            }*/
        
             // get list of all comments of change log BOF
            $condition_query1 = $condition_url1 =$extra_url= '';
            $condition_url1 .="&perform=".$perform."&id=".$id;
            $perform = 'acomm';
            $listLog	= 	NULL;
            $fields = TABLE_SCORE_SHEET_LOG.".id " ;
            $condition_query1 = " WHERE ".TABLE_SCORE_SHEET_LOG.".task_id = '".$id."' " ;
            $condition_query1 .= " ORDER BY ".TABLE_SCORE_SHEET_LOG.".do_e DESC";
            $total	=	Scoresheet::getDetailsCommLog( $db, $listLog, $fields , $condition_query1);
           
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
            //$extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  .= "&perform=".$perform;
            $extra_url  .= "&id=".$id ;
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
            $listLog	= NULL;
            $fields = TABLE_SCORE_SHEET_LOG .'.*, '.TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';   
            Scoresheet::getDetailsCommLog( $db, $listLog, $fields, $condition_query1, $next_record, $rpp);
            $condition_url .="&perform=".$perform;
            
            $fList=array();
            if(!empty($listLog)){
                foreach( $listLog as $key=>$val){  
                
                   $executive=array();
                   $string = str_replace(",","','", $val['to_id']);
                   $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                   $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                   User::getList($db,$executive,$fields1,$condition1);
                   $executivename='';
                  
                   foreach($executive as $key1=>$val1){
                        $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
                   } 
                   $val['to_id'] = $executivename ;
                   
                   
                   $fList[$key]=$val;
                }
            }
          
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'acomm');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'listLog', 'value' => 'fList');
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive'); 
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'score-sheet-add-comment.html');
    }else{
        $messages->setErrorMessage("Records not found.");
    } 
    
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }  
?>
