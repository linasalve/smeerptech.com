<?php

    if ( $perm->has('nc_tcm_details')   ) {
        
        $ticket_id	= isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
        $main_client_name	= isset($_GET["main_client_name"]) ? $_GET["main_client_name"] : 	( isset($_POST["main_client_name"]) ? 
		$_POST["main_client_name"] : '' );
        $billing_name	= isset($_GET["billing_name"]) ? $_GET["billing_name"] : ( isset($_POST["billing_name"]) ? $_POST["billing_name"] : '' );
		$client_inv = isset ($_GET['client_inv']) ? $_GET['client_inv'] : ( isset($_POST['client_inv'] ) ? $_POST['client_inv'] : '');
		$invoice_id = isset ($_GET['invoice_id']) ? $_GET['invoice_id'] : ( isset($_POST['invoice_id'] ) ?	$_POST['invoice_id'] : '');
        $flw_ord_id = isset ($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : ( isset($_POST['flw_ord_id'] ) ?	$_POST['flw_ord_id'] : '');
		
		$fpendingInvlist = array();
		$totalBalanceAmount = 0;
	    
		
	    //$user_id = $my['user_id'];
        $mail_send_to_su = "";
        $_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates=null;
       // $condition_query_st = " WHERE status='".SupportTicketTemplate::ACTIVE."'";
       // SupportTicketTemplate::getDetails( $db, $stTemplates, 'id,title', $condition_query_st);
      
        //BOF read the available departments
        //TeamCommunication::getDepartments($db,$department);
        //EOF read the available departments
        
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["rating"] = TeamCommunication::getRating();
        $variables["status"] = TeamCommunication::getTicketStatusList();        
        
		//hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=12; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
		// hrs, min.        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files      = processUserData($_FILES);
            print_r($files);
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE     ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
            
            if ( TeamCommunication::validateAddComment($data, $extra) ) {
			
    			$assign_members_old = trim($data['assign_members_old'],",");
				$assign_members_name_old = trim($data['assign_members_name_old'],",");
				if(isset($data['assign_members'])){
					$assign_members_str = implode(",",$data['assign_members']);
					$assign_members_str =   $assign_members_str.",".$assign_members_old;	
					$assign_members_arr = array_unique(explode(",",$assign_members_str)) ;
					$assign_members_str = implode(",",$assign_members_arr);	
					$assign_members_str =",".$assign_members_str."," ;
				}else{
					$assign_members_str =  ",".$assign_members_old.",";	
				}
				
				
				if(isset($data['assign_members_details'])){
					$assign_members_details_str = implode(",",$data['assign_members_details']);
					$assign_members_details_str = $assign_members_details_str.",".$assign_members_name_old;
					$assign_members_details_arr = array_unique(explode(",",$assign_members_details_str)) ;
				    $assign_members_details_str = implode(",",$assign_members_details_arr);	
					$assign_members_details_str =",".$assign_members_details_str."," ;					
				}else{
					$assign_members_details_str = ",".$assign_members_name_old."," ;	
				}
			
				
				
                $ticket_no  =  TeamCommunication::getNewNumber($db);
                $attachfilename=$ticket_attachment_path='';
                
                if(!empty($data['ticket_attachment'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['ticket_attachment']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    $attachfilename = $ticket_no.".".$ext ;
                    $data['ticket_attachment'] = $attachfilename;
                    
                    $ticket_attachment_path = DIR_WS_TM_FILES;
                    
                    if(move_uploaded_file ($files['ticket_attachment']['tmp_name'], DIR_FS_TM_FILES."/".$attachfilename)){
                        
                    }
                }
                
                $query = "SELECT "	. TABLE_TM_COMMUNICATIONS .".ticket_date, ".TABLE_TM_COMMUNICATIONS.".status"
								." FROM ". TABLE_TM_COMMUNICATIONS 
								." WHERE ". TABLE_TM_COMMUNICATIONS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
								." AND (". TABLE_TM_COMMUNICATIONS .".ticket_child = '". $ticket_id ."' " 
											." OR "
											. TABLE_TM_COMMUNICATIONS .".ticket_id = '". $ticket_id ."' " 
										.") "
								." ORDER BY ". TABLE_TM_COMMUNICATIONS .".ticket_date DESC LIMIT 0,1";				
						
				$db->query($query) ;
				if($db->nf() > 0){
					$db->next_record() ;
					$last_time = $db->f("ticket_date") ;
					$ticket_response_time = time() - $last_time ;
					$status = $db->f("status") ;
				}
				else{
					$ticket_response_time = 0 ;
				}
				$ticket_date 	= time() ;
				$ticket_replied = '0' ;

                $mail_to_all_su=0;
                if(isset($data['mail_to_all_su']) ){
                    $mail_to_all_su=1;
                }
                $mail_to_additional_email=0;
                if(isset($data['mail_to_additional_email']) ){
                    $mail_to_additional_email=1;
                }
                $mail_client=0;
                if(isset($data['mail_client']) ){
                    $mail_client=1;
                }
				 
                if(!isset($data['ticket_status']) ){                    
                    $ticket_status = TeamCommunication::OPEN;
                }else{
                    $ticket_status = $data['ticket_status'];
                }
				$hrs1 = (int) ($data['hrs'] * 6);
                $min1 = (int) ($data['min'] * 6);   		                
				  $query = "INSERT INTO "	. TABLE_TM_COMMUNICATIONS ." SET "
					. TABLE_TM_COMMUNICATIONS .".ticket_no         = '". $ticket_no ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_owner      = '". $data['ticket_owner'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_creator_uid= '". $my['uid'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_subject    = '". $data['ticket_subject'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_text       = '". $data['ticket_text'] ."', "					 
					. TABLE_TM_COMMUNICATIONS .".ticket_status     = '".$ticket_status."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_child      = '".$ticket_id."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_attachment = '". $attachfilename ."', "
					. TABLE_TM_COMMUNICATIONS .".hrs1 = '". $hrs1 ."', "
					. TABLE_TM_COMMUNICATIONS .".min1 = '". $min1 ."', "
					. TABLE_TM_COMMUNICATIONS .".hrs = '". $data['hrs'] ."', "
					. TABLE_TM_COMMUNICATIONS .".min = '". $data['min']."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_response_time = '".$ticket_response_time."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_replied    = '".$ticket_replied."', "
					. TABLE_TM_COMMUNICATIONS .".from_admin_panel  = '".TeamCommunication::ADMIN_PANEL."', "
					. TABLE_TM_COMMUNICATIONS .".mail_client    = '".$mail_client."', "
					. TABLE_TM_COMMUNICATIONS .".mail_to_all_su    = '".$mail_to_all_su."', "
					. TABLE_TM_COMMUNICATIONS .".mail_to_additional_email = '".$mail_to_additional_email."', "
					. TABLE_TM_COMMUNICATIONS .".is_private        = '". $data['is_private'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
					. TABLE_TM_COMMUNICATIONS .".do_e              = '".date('Y-m-d H:i:s')."', "
					. TABLE_TM_COMMUNICATIONS .".do_comment        = '".date('Y-m-d H:i:s')."', "
					. TABLE_TM_COMMUNICATIONS .".to_email           = '". $data['to'] ."', "
					. TABLE_TM_COMMUNICATIONS .".cc_email           = '". $data['cc'] ."', "
					. TABLE_TM_COMMUNICATIONS .".bcc_email          = '". $data['bcc'] ."', "
					. TABLE_TM_COMMUNICATIONS .".to_email_1         = '". $data['to_email_1'] ."', "
					. TABLE_TM_COMMUNICATIONS .".cc_email_1         = '". $data['cc_email_1'] ."', "
					. TABLE_TM_COMMUNICATIONS .".bcc_email_1        = '". $data['bcc_email_1'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_date        = '". $ticket_date ."' " ;
                            
                if ($db->query($query) && $db->affected_rows() > 0) {
                    
                    $variables['hid'] = $ticket_child  = $db->last_inserted_id() ;
                    $sql = '';
                    //echo $status;
                    $sqlstatus ='';
                    if($status == TeamCommunication::DEACTIVE){
                        $sqlstatus = ",".TABLE_TM_COMMUNICATIONS.".status='".TeamCommunication::ACTIVE."'";   
                    }                    
                    //Mark pending towards client bof
                     $query_update = "UPDATE ". TABLE_TM_COMMUNICATIONS 
								." SET ". TABLE_TM_COMMUNICATIONS .".ticket_status = '".$ticket_status."'"                               
                                    .",".TABLE_TM_COMMUNICATIONS.".do_comment = '".date('Y-m-d H:i:s')."'"
                                    .",".TABLE_TM_COMMUNICATIONS.".last_comment_from = '".TeamCommunication::ADMIN_COMMENT."'"
                                    .",".TABLE_TM_COMMUNICATIONS.".last_comment = '".$data['ticket_text']."'"
                                    .",". TABLE_TM_COMMUNICATIONS .".assign_members 	= '".$assign_members_str."' "
									.",".TABLE_TM_COMMUNICATIONS .".assign_members_name  = '".$assign_members_details_str."' "
                                    .",".TABLE_TM_COMMUNICATIONS.".last_comment_by = '".$my['uid'] ."'"
                                    .",".TABLE_TM_COMMUNICATIONS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
                                    .$sqlstatus                                                                     
                                    ." WHERE ". TABLE_TM_COMMUNICATIONS .".ticket_id = '". $ticket_id ."' " ;
				    $db->query($query_update) ;
                    //Mark pending towards client eof
                    
                    /**************************************************************************
                    * Send the notification to the ticket owner and the Administrators
                    * of the new Ticket being created
                    **************************************************************************/
                    if($ticket_status==TeamCommunication::OPEN){
						$ticket_status_name = 'OPEN';
					}else{
						$ticket_status_name = 'CLOSED';
					}
                    $data['ticket_no']    =   $ticket_no ;
                    $data['mticket_no']   =   $data['mticket_no'] ;
                    $data['replied_by']   =   $my['f_name']." ". $my['l_name'] ;
                    $data['subject']    =     $_ALL_POST['ticket_subject'] ;
					$data['text']    	=   $_ALL_POST['ticket_text'] ;
                    $data['attachment']   =   $attachfilename ;
					$data['status_name']    = $ticket_status_name;
					$data['assign_members_name']  = trim($assign_members_details_str,",");
					
                    $data['link']   = DIR_WS_MP .'/team-communication.php?perform=view&ticket_id='. $ticket_id;
                    $file_name = '';
                    if(!empty($attachfilename)){
                        $file_name = DIR_FS_TM_FILES ."/". $attachfilename;
                    }
					$assign_members_str = str_replace(",","','", trim($assign_members_str,","));
					User::getList($db, $userDetailsList, 'number, f_name, l_name, email,email_1,email_2,send_mail,
					title as ctitle,  mobile1, mobile2 ', " WHERE user_id IN ('".trim($assign_members_str,",")."')");
					if(!empty($userDetailsList)){
						 $sms_to_number = $sms_to =array();
						foreach ($userDetailsList  as $key=>$userDetails){
							
							$mobile1 = $mobile2='';
							$data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
							$data['ctitle']    =   $userDetails['ctitle'];
							$send_mail		= $userDetails['send_mail'];
							$email = NULL;	
							if($send_mail){
								if ( getParsedEmail($db, $s, 'EMAIL_TC_MEMBER', $data, $email) ) {                                    
									$to = '';
									$to[]   = array('name' => $userDetails['f_name'] .' '. 
									$userDetails['l_name'], 'email' => 
									$userDetails['email']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$userDetails['email'];
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,
									$cc,$bcc,$file_name);
								   
									/*
									if(!empty($userDetails['email_1'])){                                       
										$to = '';
										$to[]   = array('name' => $userDetails['f_name'] .' '.
										$userDetails['l_name'], 'email' =>
										$userDetails['email_1']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_1'];
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);  
									}
									if(!empty($userDetails['email_2'])){                                       
										$to = '';
										$to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
										$userDetails['email_2']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_2'];
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"],
										$email["isHTML"],    $cc_to_sender,$cc,$bcc,$file_name); 
																  
									} 
									*/
									
									if(!empty($smeerp_client_email)){
										$to     = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
										'email' => $smeerp_client_email);
										$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
										$cc_to_sender,$cc,$bcc,$file_name);
									}  
								}
							}
							if(isset($data['send_sms']) && $smsLength<=130 && !empty($userDetails['mobile1'])){
								//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
								$sms_to_number[] =$userDetails['mobile1'];
								$mobile1 = "91".$userDetails['mobile1'];
								$client_mobile .= ','.$mobile1;
								ProjectTask::sendSms($data['text'],$mobile1);
								$counter = $counter+1;
									
							}
							if(isset($data['send_sms']) && $smsLength<=130 && !empty($userDetails['mobile2'])){
								//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
								$sms_to_number[] =$userDetails['mobile2'] ;
								$mobile2 = "91".$userDetails['mobile2'];
								$client_mobile .= ','.$mobile2;
								ProjectTask::sendSms($data['text'],$mobile2);
								$counter = $counter+1;                                        
							}
							if(!empty($mobile1) || !empty($mobile2)){
								 $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
							}
						}
					}
				   //Set the list subuser's email-id to whom the mails are sent bof
					$query_update = "UPDATE ". TABLE_TM_COMMUNICATIONS 
								." SET ". TABLE_TM_COMMUNICATIONS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', sms_to_number='".implode(",",array_unique($sms_to_number))."',
									sms_to ='".implode(",",array_unique($sms_to))."' "                                    
								." WHERE ". TABLE_TM_COMMUNICATIONS .".ticket_id = '". $variables['hid'] ."' " ;
					$db->query($query_update) ;
					//Set the list subuser's email-id to whom the mails are sent eof                        
					//update sms counter
					if($counter >0){
						$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
						$db->query($sql); 
					}                    
                                  
                        
                   
                    
                    // Send Email to the admin BOF
                    $email = NULL;
                    $data['link']   = DIR_WS_NC .'/team-communication.php?perform=view&ticket_id='. $ticket_id;
                    $data['quicklink']   = DIR_WS_SHORTCUT .'/team-communication.php?perform=view&ticket_id='. $ticket_id;      
                    $data['main_client_name']   = $main_client_name;
                    $data['billing_name']   = $billing_name;
                    $cc_to_sender= $cc = $bcc = Null;
                    if ( getParsedEmail($db, $s, 'EMAIL_TC_ADMIN', $data, $email) ) {
                        $to = '';
					 
                        $to[]   = array('name' => $team_communication_name , 'email' => $team_communication_email);			 
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,
                        $cc,$bcc,$file_name);
                    }                  
                    // Send Email to the admin EOF                    
                    //$messages->setOkMessage("A comment has been added to the support ticket. And email Notification has been sent.");
                    $messages->setOkMessage("Ticket has been created successfully.");
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
               
				 
				header("Location:".DIR_WS_NC."/team-communication.php?perform=view&added=1&ticket_id=".$ticket_id);
				 
            }
        }
        
        if( isset($_POST["btnUpdate"]) && $_POST["btnUpdate"] == "Close Ticket"){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            if(empty($data['ticket_rating'])){
                $messages->setErrorMessage("Please rate the service.");
            }else{
                $query = "UPDATE ". TABLE_TM_COMMUNICATIONS 
                                ." SET "//. TABLE_TM_COMMUNICATIONS .".ticket_priority = '". $data["ticket_priority"] ."', "
                                        . TABLE_TM_COMMUNICATIONS .".ticket_status = '". TeamCommunication::CLOSED ."', "
                                        . TABLE_TM_COMMUNICATIONS .".ticket_rating = '". $data["ticket_rating"] ."', "
                                        . TABLE_TM_COMMUNICATIONS .".ticket_replied = '0' "
                                ." WHERE ". TABLE_TM_COMMUNICATIONS .".ticket_id = '". $ticket_id ."' " 
                                ." AND ". TABLE_TM_COMMUNICATIONS .".ticket_owner_uid = '". $data["ticket_owner_uid"] ."' ";
                if($db->query($query)){
                    $messages->setOkMessage("Ticket Status Updated Successfully.");
                }
            }
		}
        
        if( isset($_POST["btnActive"])){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
           
            $query = "UPDATE ". TABLE_TM_COMMUNICATIONS 
                            ." SET ". TABLE_TM_COMMUNICATIONS .".status = '". TeamCommunication::ACTIVE ."'"
                            ." WHERE ". TABLE_TM_COMMUNICATIONS .".ticket_id = '". $data['thread_ticket_id'] ."' " ;
            if($db->query($query)){
                $messages->setOkMessage("Ticket activated Updated Successfully.");
            }          
		}
        
        if( isset($_POST["btnDeActive"])){
        
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $query = "UPDATE ". TABLE_TM_COMMUNICATIONS 
                            ." SET ". TABLE_TM_COMMUNICATIONS .".status = '". TeamCommunication::DEACTIVE ."'"
                            ." WHERE ". TABLE_TM_COMMUNICATIONS .".ticket_id = '". $data['thread_ticket_id'] ."' " ;
            if($db->query($query)){
                $messages->setOkMessage("Ticket de-activated Updated Successfully.");
            }            
		}
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
			 
			include ( DIR_FS_NC .'/team-communication-list.php');
			 
			
        }
        else {
            $condition_query= " WHERE ticket_id = '". $ticket_id ."' ";
            $_ALL_POST      = NULL;            
            if ( TeamCommunication::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST[0];
                $mticket_no = $_ALL_POST['ticket_no'];
                $_ALL_POST['mail_send_to_su'] = chunk_split($_ALL_POST['mail_send_to_su']);
                $_ALL_POST['sms_to'] = chunk_split($_ALL_POST['sms_to']);
                $_ALL_POST['sms_to_number'] = chunk_split($_ALL_POST['sms_to_number']);
				$_ALL_POST['assign_members_name'] = trim($_ALL_POST['assign_members_name'] ,",");
                $assign_members_old = $_ALL_POST['assign_members'] ;
                $assign_members_name_old = $_ALL_POST['assign_members_name'] ;
                $_ALL_POST['attachmentArr']='';
                if(!empty($_ALL_POST['attachment_imap'])){
                    $_ALL_POST['attachment_imap'] = trim($_ALL_POST['attachment_imap'],",") ;
                    $_ALL_POST['attachmentArr'] = explode(",",$_ALL_POST['attachment_imap']);
                }                
                $_ALL_POST['path_attachment_imap'] = DIR_WS_ST_FILES ;
            }

            $ticketStatusArr = TeamCommunication::getTicketStatus();
            $ticketStatusArr = array_flip($ticketStatusArr);
            $_ALL_POST['ticket_status_name'] = $ticketStatusArr[$_ALL_POST['ticket_status']];   
            
	   
            $query = "SELECT "	. TABLE_TM_COMMUNICATIONS .".ticket_no, "
								. TABLE_TM_COMMUNICATIONS .".ticket_id, "
								. TABLE_TM_COMMUNICATIONS .".to_email_1, "
								. TABLE_TM_COMMUNICATIONS .".cc_email_1, "
								. TABLE_TM_COMMUNICATIONS .".bcc_email_1, "
								. TABLE_TM_COMMUNICATIONS .".on_behalf, "
								. TABLE_TM_COMMUNICATIONS .".from_email, "
								. TABLE_TM_COMMUNICATIONS .".mail_send_to_su, "
								. TABLE_TM_COMMUNICATIONS .".ticket_subject, "
								. TABLE_TM_COMMUNICATIONS .".ticket_text, "
								. TABLE_TM_COMMUNICATIONS .".ticket_creator, "
								. TABLE_TM_COMMUNICATIONS .".ticket_creator_uid, "
								. TABLE_TM_COMMUNICATIONS .".ticket_attachment, "
								. TABLE_TM_COMMUNICATIONS .".ticket_attachment_1, "
								. TABLE_TM_COMMUNICATIONS .".ticket_attachment_2, "
								. TABLE_TM_COMMUNICATIONS .".ticket_attachment_path, "
								. TABLE_TM_COMMUNICATIONS .".ticket_response_time, "
								. TABLE_TM_COMMUNICATIONS .".attachment_imap, "
								. TABLE_TM_COMMUNICATIONS .".ticket_date, "
								. TABLE_TM_COMMUNICATIONS .".status, "
								. TABLE_TM_COMMUNICATIONS .".hrs1, "
								. TABLE_TM_COMMUNICATIONS .".min1, "
								. TABLE_TM_COMMUNICATIONS .".hrs, "
								. TABLE_TM_COMMUNICATIONS .".min, "
								. TABLE_TM_COMMUNICATIONS .".ip, "
								. TABLE_TM_COMMUNICATIONS .".headers_imap, "
								. TABLE_TM_COMMUNICATIONS .".sms_to, "
								. TABLE_TM_COMMUNICATIONS .".sms_to_number, "
                                . TABLE_USER  .".mobile1, "
                                . TABLE_USER .".mobile2 "
							." FROM ". TABLE_TM_COMMUNICATIONS ." LEFT JOIN ".TABLE_USER." 
                                ON ".TABLE_USER.".user_id = ".TABLE_TM_COMMUNICATIONS.".ticket_creator_uid
                                WHERE ". TABLE_TM_COMMUNICATIONS .".ticket_owner_uid = '". $_ALL_POST['ticket_owner_uid'] ."' " 
							    ." AND ". TABLE_TM_COMMUNICATIONS .".ticket_child = '". $ticket_id ."' " 
							    ." ORDER BY ". TABLE_TM_COMMUNICATIONS .".ticket_date ASC";
			
			$db->query($query) ;
			$ticketThreads = array() ;
			while($db->next_record()){
				$response["h"] = round($db->f("ticket_response_time") / 3600) ;
				$response["m"] = round(($db->f("ticket_response_time") % 3600) / 60 ) ;
				$response["s"] = round(($db->f("ticket_response_time") % 3600) % 60 ) ;
                
				
                $attachmentArr='';
                $attachment_imap = $db->f("attachment_imap") ; 
                if(!empty($attachment_imap)){
                    $attachment_imap = trim($attachment_imap,",");
                    $attachmentArr = explode(",",$attachment_imap);
                }
                $path_attachment_imap = DIR_WS_TM_FILES ;
				$ticketThreads[] = array(	"ticket_no" 			=> $db->f("ticket_no"), 
											"ticket_id" 		=> $db->f("ticket_id"), 
											"ticket_subject" 		=> $db->f("ticket_subject"), 
                                            "on_behalf" 		=> $db->f("on_behalf"), 
											"mail_send_to_su" 		=> chunk_split($db->f("mail_send_to_su")), 
											"ticket_text" 			=> $db->f("ticket_text"), 
											"ticket_creator" 		=> $db->f("ticket_creator"), 
											"ticket_attachment" 	=> $db->f("ticket_attachment"), 
											"ticket_attachment_1" 	=> $db->f("ticket_attachment_1"), 
											"ticket_attachment_2" 	=> $db->f("ticket_attachment_2"), 
											"ticket_attachment_path"=> $db->f("ticket_attachment_path"),
											"ticket_response_time" 	=> $response["h"] ." hr(s) : "
																		. $response["m"] ." min(s) : " 
																		. $response["s"]." sec(s) ",
											"ticket_date" 			=> $db->f("ticket_date"),
											"mobile1" 			=> $db->f("mobile1"),
											"mobile2" 			=> $db->f("mobile2"),
                                            "sms_to" 			=> chunk_split($db->f("sms_to")),
											"sms_to_number" 	=>chunk_split( $db->f("sms_to_number")),
											"cc" 			=> '',
											"ac" 			=> '',
											"p_number" 		=> '',
											"to_email_1" 	=> $db->f("to_email_1"),
											"cc_email_1" 	=> $db->f("cc_email_1"),
											"bcc_email_1" 	=> $db->f("bcc_email_1"),
											"from_email" 	=> $db->f("from_email"),
											"status" 	=> $db->f("status"),
											"ip" 	=> $db->f("ip"),
											"hrs" 	=> $db->f("hrs"),
											"min" 	=> $db->f("min"),
											"hrs1" 	=> $db->f("hrs1"),
											"min1" 	=> $db->f("min1"),
											"ip" 	=> $db->f("ip"),
											"headers_imap" 	=> $db->f("headers_imap"),
											"path_attachment_imap" 	=> $path_attachment_imap,
											"attachmentArr" 	=> $attachmentArr
										);
			}
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
           
            $variables['ticket_closed'] = TeamCommunication::CLOSED;
            $variables['can_add_cmt_after_closed'] = false;
            $variables['can_update_status'] = false;
            $variables['can_view_client_details']  = false;
            $variables['can_view_headers']  = false;
            $variables['can_view_reply_email']  = false;   
			
            if ( $perm->has('nc_tcm_add_cmt_after_closed') ) {
                $variables['can_add_cmt_after_closed'] = true;
            }            
            if ( $perm->has('nc_tcm_update_status') ) {
                $variables['can_update_status'] = true;
            }
            if ( $perm->has('nc_tcm_headers') ) {
                $variables['can_view_headers'] = true;
            }
               
			
            
            $hidden[] = array('name'=> 'perform','value' => $perform);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'ticket_id', 'value' => $ticket_id); 
            $hidden[] = array('name'=> 'mticket_no', 'value' => $mticket_no);            
            $hidden[] = array('name'=> 'assign_members_old', 'value' => $assign_members_old);            
            $hidden[] = array('name'=> 'assign_members_name_old', 'value' => $assign_members_name_old);            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
			$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'ticket_id', 'value' => 'ticket_id'); 
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
             
            $page["var"][] = array('variable' => 'ticketThreads', 'value' => 'ticketThreads');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'team-communication-view.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>