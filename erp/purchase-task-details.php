<?php
//if ( $perm->has('nc_pr_ts_details') ) {
if ( $perm->has('nc_pr_ts_list') ) {
		include_once (DIR_FS_INCLUDES .'/project-priority.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-status.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-resolution.inc.php');
		include_once ( DIR_FS_INCLUDES .'/purchase-order.inc.php');
        include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');

        $posted_data = array();
     
    //Get all req array BOF           
        $statusArr	= NULL;
        $condition_querys = " WHERE status ='".PurchaseTask::ACTIVE."'";
        ProjectStatus::getList( $db, $statusArr, 'id, title', $condition_querys);
       
        $priorityArr	= NULL;
        $condition_queryp = " WHERE  status ='".PurchaseTask::ACTIVE."'";
        ProjectPriority::getList( $db, $priorityArr, 'id, title', $condition_queryp);
        
        $resolutionArr	= NULL;
        $condition_queryr = " WHERE status ='".PurchaseTask::ACTIVE."'";
        ProjectResolution::getList( $db, $resolutionArr, 'id, title', $condition_queryr);
    //Get all req array EOF
        $lst_msg=PurchaseTask::getStdMsg();
        $lst_filetype=PurchaseTask::getFileType();
    //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
     // hrs, min.

     
    $extra_url  =  $pagination='';

	if(array_key_exists('btnUpdate',$_POST)){
        $_ALL_POST 	= $_POST;
        $data		= processUserData($_ALL_POST);
        $temp = explode('/', $data['resolved_date']);                               
        $time =  mktime(0, 0, 0, $temp[1], $temp[0], $temp[2]);        
        $resolved_date = $datetime->unixTimeToDate($time); 
        
        $query = "UPDATE ".TABLE_PURCHASE_TASK_DETAILS
                  ." SET "
                  ." status_id     	= '".$data['status_id']."',"	
                  ." priority_id   	= '".$data['priority_id']."',"
                  ." resolution_id  = '".$data['resolution_id']."',"
                  ." resolved_date  = '".$resolved_date."' WHERE id = ".$data['id'] ;
       
        if ( $db->query($query) && $db->affected_rows() > 0 ) {
            $messages->setOkMessage("Project Task has been updated Successfully.");
        }
    }

    if ( isset($_POST['btnCreate'])  && $_POST['act'] == 'save') {
          
            $posted_data = processUserData($_POST);
            $files		= processUserData($_FILES);
            
            $extra = array( 'db' 				=> &$db,
                         	'files'          => &$files,
                         	'messages'          => &$messages
                        );
            $posted_data['allowed_file_types'] = $allowed_file_types ;
           // $posted_data['max_file_size']= MAX_MULTIPLE_FILE_SIZE ;
            $posted_data['max_file_size']= MAX_FILE_SIZE ; 
              
            if ( PurchaseTask::validateCommentAdd($posted_data, $extra) ) {
                
                $is_visibleto_client = 0;                
                if(isset($posted_data['is_visibleto_client'])){
                    $is_visibleto_client = 1;
                }
                if(empty($posted_data['details'])){
                    $posted_data['details'] = $lst_msg[$posted_data['defaultMsg']];
                }
               //$lst_filetype                
                $i=1;
                $attachfilename='';
                if(!empty($files["attached_file"]["name"])){
                  
                    //$val["name"] = str_replace(' ','-',$val["name"]);
                    $filedata["extension"]='';
                    $filedata = pathinfo($files["attached_file"]["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                    $attachfilename = mktime()."-".$i.".".$ext ;                  
                    if(!empty($files["attached_file"]["name"])){                 
                        if(move_uploaded_file($files["attached_file"]['tmp_name'], DIR_FS_PRATTACHMENTS_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_PRATTACHMENTS_FILES."/".$attachfilename, 0777);
                            $attached_type = $files["attached_file"]["type"] ;
                        }
                    }
					
                    $i++;
                }
                
                $hrs1 = (int) ($posted_data['hrs'] * 3);
                $min1 = (int) ($posted_data['min'] * 3);   
                
                //Mark send notify BOF   
						
                $mail_client=0;
                if(isset($_POST['mail_client']) ){
                    $mail_client=1;
                }
                $mail_staff=0;
                if(isset($_POST['mail_staff']) ){
                    $mail_staff=1;
                }
                $mark_imp=0;
                if(isset($_POST['mark_imp']) ){
                    $mark_imp=1;
                }
                $posted_data['design_counter']='';
                $posted_data['file_counter']=0;
                //Get counter of filetypes BOF
                if(!empty($attachfilename) ){
                    $sql= " SELECT count(*) as count FROM ".TABLE_PURCHASE_TASK_DETAILS." WHERE 
                        project_id ='".$posted_data['or_id']."' AND is_bug='0' AND file_type='".$posted_data['file_type']."'";
                    if ( $db->query($sql) ) {
                        if ( $db->nf() > 0 ) {
                            while ($db->next_record()) {
                                $posted_data['file_counter'] = $db->f('count') + 1;
                                
                            }
                        }
                    }
                    if($data['file_type']!=PurchaseTask::OTHER){
                        $filetype= $posted_data['file_type'];
                        $posted_data['design_counter'] = $lst_filetype[$filetype]." ".$posted_data['file_counter'];
                    }
                }
                //Get counter of filetypes EOF
                //disply random name BOF 
				$data['display_name']=$data['display_user_id']=$data['display_designation']='';
				//if($mail_to_all_su==1 || $mail_to_additional_email==1 || $mail_client==1){ 
					$support_team='';
					if(!empty($order['wp_support_team'])){
					 	$wp_support_team = trim($order['wp_support_team'],",") ;
						$wp_support_team_arr = explode(',', $wp_support_team) ;
						$no = array_rand($wp_support_team_arr, 1);
						$support_team = $wp_support_team_arr[$no];
					}
					$randomUser = getRandomAssociate($support_team);
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
				//}
				//disply random name EOF	
				$bworked_min=0;
				if(!empty($posted_data['behalf_executive'])){
					$table = TABLE_USER;
					$condition2 = " WHERE ".TABLE_USER .".user_id= '".$posted_data['behalf_executive'] ."' 
					LIMIT 0,1 " ;
					$fields1 =  TABLE_USER .'.current_salary';
					$exeCreted= getRecord($table,$fields1,$condition2);                
					$behalfCurrentSalary = $exeCreted['current_salary'] ;				
					 
					$per_hour_salary = $behalfCurrentSalary / STD_WORKING_HRS;
					$per_hour_salary =  number_format($per_hour_salary,2);
					$worked_min = $bworked_min = ( $posted_data['hrs']*60 ) + $posted_data['min'] ;
					$worked_hour = ($worked_min/60) ;
					$worked_hour =  number_format($worked_hour,2);
					$worked_hour_salary = ( $per_hour_salary * $worked_hour);				
                 				
					 $query = "INSERT INTO ".TABLE_PURCHASE_TASK_DETAILS
							  ." SET "
							  ." details      	= '".$posted_data['details']."',"	
							  ." hrs             = '".$posted_data['hrs']."',"
							  ." min            = '".$posted_data['min']."',"
							  ." hrs1            = '".$hrs1."',"
							  ." min1           = '".$min1."',"
							  ." current_salary  = '".$behalfCurrentSalary."',"
							  ." per_hour_salary  = '".$per_hour_salary."',"
							  ." worked_hour_salary  = '".$worked_hour_salary."',"
							  ." last_updated 		= '".$last_updated."',"
							  ." added_by      		= '".$posted_data['behalf_executive']."',"
							  ." added_by_name     	= '".$exeCreted['f_name']." ".$exeCreted['l_name']."',"
							  ." behalf_added_by      	= '".$my['user_id']."',"
							  ." behalf_added_by_name   = '".$my['f_name']." ".$my['l_name']."',"
							  ." parent_id      = '".$posted_data['id']."',"
							  ." project_id      = '".$posted_data['or_id']."',"
							  ." is_bug       	= '0',"
							  ." file_type      = '".$posted_data['file_type']."',"
							  ." file_counter   = '".$posted_data['file_counter']."',"
							  ." attached_file	= '".$attachfilename."',"			
                              ." attached_type	= '".$attached_type."',"                              
                              ." attached_desc  = '".$posted_data['attached_desc']."',"
							  ." display_name  = '".$data['display_name']."',"
                              ." display_user_id  = '".$data['display_user_id']."',"
                              ." display_designation  = '".$data['display_designation']."',"
                              ." file_upload_path  = '".$posted_data['file_upload_path']."', "
							  ." is_visibleto_client = '".$is_visibleto_client."', "
                              ." mail_client = '".$mail_client."',"
							  ." mail_staff = '".$mail_staff."',"
							  ." mark_imp = '".$mark_imp."',"
							  ." ip  	  = '".$_SERVER['REMOTE_ADDR']."', "
							  ." do_e 	  = '".date('Y-m-d H:i:s')."'" ;
					 
					$db->query($query);
				
					//As entry was onbehalf added by my BOF so by default one entry of 10min by my
					$posted_data['hrs'] = 0;
					$posted_data['min'] = 10;
					$hrs1 = (int) ($posted_data['hrs'] * 3);
					$min1 = (int) ($posted_data['min'] * 3);
					//As entry was onbehalf added by my EOF
					
				}
				
				$current_salary = $my['current_salary'];
				$per_hour_salary = $my['current_salary'] / STD_WORKING_HRS;
				$per_hour_salary =  number_format($per_hour_salary,2);
				$worked_min = ( $posted_data['hrs']*60 ) + $posted_data['min'] ;
				$worked_hour = ($worked_min/60) ;
				$worked_hour =  number_format($worked_hour,2);
				$worked_hour_salary = ( $per_hour_salary * $worked_hour);			
				$query = "INSERT INTO ".TABLE_PURCHASE_TASK_DETAILS
						  ." SET "
						  ." details      	 = '".$posted_data['details']."',"	
						  ." hrs             = '".$posted_data['hrs']."',"
						  ." min             = '".$posted_data['min']."',"
						  ." hrs1            = '".$hrs1."',"
						  ." min1            = '".$min1."',"
						  ." current_salary  = '".$current_salary."',"
						  ." per_hour_salary = '".$per_hour_salary."',"
						  ." worked_hour_salary  = '".$worked_hour_salary."',"
						  ." last_updated 	= '".$last_updated."',"
						  ." added_by      	= '".$my['user_id']."',"							 
						  ." added_by_name  = '".$my['f_name']." ".$my['l_name']."',"							 
						  ." parent_id      = '".$posted_data['id']."',"
						  ." project_id     = '".$posted_data['or_id']."',"
						  ." is_bug       	= '0',"
						  ." file_type      = '".$posted_data['file_type']."',"
						  ." file_counter   = '".$posted_data['file_counter']."',"
						  ." attached_file	= '".$attachfilename."',"			
						  ." attached_type	= '".$attached_type."',"                              
						  ." attached_desc  = '".$posted_data['attached_desc']."',"
						  ." display_name   = '".$data['display_name']."',"
						  ." display_user_id  = '".$data['display_user_id']."',"
						  ." display_designation  = '".$data['display_designation']."',"
						  ." file_upload_path  = '".$posted_data['file_upload_path']."', "
						  ." is_visibleto_client = '".$is_visibleto_client."', "
						  ." mail_client = '".$mail_client."',"
						  ." mail_staff = '".$mail_staff."',"
						  ." mark_imp = '".$mark_imp."',"
						  ." ip  	        = '".$_SERVER['REMOTE_ADDR']."', "
						  ." do_e 	        = '".date('Y-m-d H:i:s')."'" ;				
                 $db->query($query);				
				 $variables['hid'] = $db->last_inserted_id();
                //Update Main task to get last task comment and other details bof
                 $sql = " UPDATE ".TABLE_PURCHASE_TASK_DETAILS." SET last_comment = '".$posted_data['details']."',
                        last_comment_by  	= '".$my['user_id']."', 
                        last_comment_by_name  	= '".$my['f_name']." ".$my['l_name']."', 
                        last_comment_dt  	= '".date('Y-m-d H:i:s')."' 
                        WHERE id ='".$posted_data['id']."'"  ;
                 $db->query($sql);
                //Update Main task to get last task comment and other details eof
				
				$totHrs = $order['tot_thrs'] * 60  ;
				$totMin = $order['tot_tmin'] + $bworked_min + $worked_min ;
				$totalMin = $totHrs + $totMin  ;
                $totalEstMin = $order['tot_est_min'] ;
				if($totalMin > $totalEstMin){
					//SEND MAIL TO ADMIN BOF EXCEED 
					$email_data['order_title'] = $order['order_title'] ;
					$email_data['client_name'] = $order['f_name']." ".$order['l_name']." ".$order['billing_name'] ;
					$team_mem_str ='';
					if(!empty($team_members )){
						foreach($team_members  as $keyt=>$valt){
							$team_mem_str.= $valt['f_name']." ".$valt['l_name']."<br/> ";
						}
					}
					$team_mem_str = trim($team_mem_str,"<br/>");
					$email_data['team_members'] = $team_mem_str ;
					$email_data['est_hrs'] =  $order['es_hrs'] ;
					$thrs = (int) ($totalMin/60);
					$tmin = (int)($totalMin%60);
					$email_data['total_worked_hrs'] = $thrs.":".$tmin ;
					$exceed_min = $totalMin - $totalEstMin ;
					$ehrs = (int)($exceed_min/60);
					$emin = (int)($exceed_min%60);
					$email_data['exceed_hrs'] =  $ehrs.":".$emin ; 	
					/* 
					if ( getParsedEmail($db, $s, 'TASK_NOTE_HRS_EXCEED', $email_data, $email) ) {
						$to     = '';
						$to[]   = array('name' => $admin_name, 'email' => $admin_email);
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
						
					} */
					//SEND MAIL TO ADMIN EOF
				}
                //Send notification BOF    
                    $data1=null;
                    $condition_query = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".id='".$posted_data['id']."'" ;
                    $fields = TABLE_PURCHASE_TASK_DETAILS.".id, ".TABLE_PURCHASE_TASK_DETAILS.".project_id, 
					".TABLE_PURCHASE_TASK_DETAILS.".added_by,
					".TABLE_PURCHASE_TASK_DETAILS.".added_by_name,
                    ".TABLE_PURCHASE_TASK_DETAILS.".is_client, ".TABLE_PURCHASE_TASK_DETAILS.".title, 
                    ".TABLE_PURCHASE_TASK_DETAILS.".details,".TABLE_PURCHASE_TASK_DETAILS.".last_updated,
                    ".TABLE_PROJECT_TASK_MODULE.".title as module,".TABLE_PROJECT_TASK_STATUS.".title as status," ;
                    $fields .= TABLE_PROJECT_TASK_PRIORITY.".title as priority, ".TABLE_PROJECT_TASK_TYPE.".title 
					as type,".TABLE_PROJECT_TASK_RESOLUTION.".title as resolution" ;
                    PurchaseTask::getList( $db, $data1, $fields, $condition_query);
                    
                    if(!empty($data1) ){      
                        $data = $data1[0] ;
                        if($data['is_client']=='1'){
                            $table = TABLE_CLIENTS;
                            $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$data['added_by'] ."' " ;
                            $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.
							TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                            $exeCreted= getRecord($table,$fields1,$condition2);
                            $data['af_name'] =$exeCreted['f_name'];
                            $data['al_name'] = $exeCreted['l_name'] ;
                        }else{
                            $table = TABLE_USER;
                            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$data['added_by'] ."' " ;
                            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                            $exeCreted= getRecord($table,$fields1,$condition2);
                            $data['af_name'] =$exeCreted['f_name'];
                            $data['al_name'] = $exeCreted['l_name'] ;
                        }
                        
                        $data['cm_doe'] = date('Y-m-d H:i:s');
                        $data['cdetails'] = $posted_data['details'];
                    }
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
                    $data['project_title'] =$order['order_title'];
                    $data['images_nc']=DIR_WS_IMAGES_NC;
                    $data['design_counter']= $posted_data['design_counter'];
                    $file_name = DIR_FS_PRATTACHMENTS_FILES ."/". $attachfilename;
                    $cc_to_sender= $cc = $bcc = Null;
					
					//get the subclients allotted to the order bof
					$ord_details=null;
					$fields = TABLE_PURCHASE_ORDER.'.id, '.TABLE_PURCHASE_ORDER.'.clients_su ' ;
					$condition_query_ord = " WHERE ".TABLE_PURCHASE_ORDER.".id = ".$posted_data['or_id'] ;
					$total	=	PurchaseOrder::getDetails( $db, $ord_details, $fields, $condition_query_ord);
					$order_details =  $ord_details[0];
					$clients_su = $order_details['clients_su'] ;
					$clients_su_str='';
					if(!empty($clients_su)){
						$clients_su_str =  str_replace(",","','",$clients_su);
					}
					//get the subclients allotted to the order eof
					
					
					
					
                    // Send Email to the Client BOF                    
                    if(isset($posted_data['mail_client']) ){
						 $mail_send_to ='';
						 $data['link']   = DIR_WS_MP .'/purchase-task.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'];
                         $subUserDetails=null;
						 if(!empty($clients_su_str)){
							//Clients::getList($db, $subUserDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE parent_id = '".$order['client']."' AND status='".Clients::ACTIVE."' AND user_id IN('".$clients_su_str."') ");                   
							Clients::getList($db, $subUserDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE status='".Clients::ACTIVE."' AND user_id IN('".$clients_su_str."') ");                   
						 }
						 Clients::getList($db, $clientDetails, 'number, f_name, l_name, email,email_1,email_2,title', " WHERE user_id = '".$order['client']."'");
                         if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['c_title'] =$client['title'];
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;                        
                            $data['cm_by_f_name'] =$my['f_name'];
                            $data['cm_by_l_name'] = $my['l_name'] ;
                            $email = NULL;
							$data['project_title'] = !empty($order['order_title_mail']) ? $order['order_title_mail'] : $order['order_title'];
                       

							if(isset($posted_data['mark_imp']) ){
						
                                /*if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_CLIENT_IMP', $data, $email) ) {
                                   
    								$mail_send_to .=",".$client['email']; 
									$to     = '';
                                    $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 
									'email' => $client['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"] ,$cc_to_sender,$cc,$bcc,$file_name);
                                    if(!empty($client['email_1'])){
                                        $mail_send_to .=",".$client['email_1']; 
										$to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_1']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                    if(!empty($client['email_2'])){
										$mail_send_to .=",".$client['email_2']; 
                                        $to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_2']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
									 
                                }*/ 
                                
                                // send mail to sub users bof
                                if(!empty($subUserDetails)){                                        
                                    foreach ($subUserDetails  as $key=>$value){
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        $data['c_title'] =$subUserDetails[$key]['title'];                            
                                        $data['cf_name'] =$subUserDetails[$key]['f_name'];
                                        $data['cl_name'] = $subUserDetails[$key]['l_name'] ;
                                        if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_CLIENT_IMP', $data, $email) ) {
                                            $mail_send_to .=",".$subUserDetails[$key]['email'];
											$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
											
											if(!empty($subUserDetails[$key]['email_1'])){
												$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_1']);
                                            
												$mail_send_to .=",".$subUserDetails[$key]['email_1'];                                
												 $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
												SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name);
											}
											if(!empty($subUserDetails[$key]['email_2'])){
												$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_2']);
                                            	$mail_send_to .=",".$subUserDetails[$key]['email_2'];                                
												 $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
												SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name);
											}
										
										}
                                    }
                                }
                                // send mail to sub users bof
								//Send a copy to check the mail for clients
								if(!empty($smeerp_client_email)){
									$to     = '';
									$to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $smeerp_client_email);
									$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
								} 
                                
                            }else{
								/*
                                if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_CLIENT', $data, $email) ) {
                                    $mail_send_to .=",".$client['email']; 
									$to     = '';
                                    $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                  
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"] ,$cc_to_sender,$cc,$bcc,$file_name);
                                    if(!empty($client['email_1'])){
                                        $mail_send_to .=",".$client['email_1'];
										$to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_1']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                         SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                    if(!empty($client['email_2'])){
										$mail_send_to .=",".$client['email_2'];
                                        $to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email_2']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }
                                    
                                }*/
                                // send mail to sub users bof
                                if(!empty($subUserDetails)){                                        
                                    foreach ($subUserDetails  as $key=>$value){
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        $data['c_title'] =$subUserDetails[$key]['title'];                            
                                        $data['cf_name'] =$subUserDetails[$key]['f_name'];
                                        $data['cl_name'] = $subUserDetails[$key]['l_name'] ;
                                        if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_CLIENT', $data, $email) ) {
                                            $mail_send_to .=",".$subUserDetails[$key]['email'];
										    $to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                        
											if(!empty($subUserDetails[$key]['email_1'])){
												$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_1']);
                                            	$mail_send_to  .=",".$subUserDetails[$key]['email_1'];                                
											    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
												SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name);
											}
											if(!empty($subUserDetails[$key]['email_2'])){
												$to = '';
												$to[]   = array('name' => $subUserDetails[$key]['f_name'] .' '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_2']);
                                            	$mail_send_to .=",".$subUserDetails[$key]['email_2'];                                
												$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
												SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name);
											}
										}
                                    }
									
									//Send a copy to check the mail for clients
                                    if(!empty($smeerp_client_email)){
                                        $to     = '';
                                        $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 
										'email' => $smeerp_client_email);
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                    }   
                                }
                                // send mail to sub users bof
                            }
                         }
                       
                    }
                    // Send Email to the Client EOF
                    //Set the list subuser's email-id to whom the mails are sent bof   
					if(!empty($mail_send_to)){
						$query_update = "UPDATE ". TABLE_PURCHASE_TASK_DETAILS 
                                ." SET ". TABLE_PURCHASE_TASK_DETAILS .".mail_send_to_su  = '".trim($mail_send_to ,",")."' 
                                WHERE ". TABLE_PURCHASE_TASK_DETAILS .".id = ". $variables['hid'] ." " ;
						$db->query($query_update) ;
					}
					
                    // Send Email to the Staff BOF                    
                    if(isset($posted_data['mail_staff']) ){
                        $data['link']   = DIR_WS_NC .'/project-task.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'];
                        //include ( DIR_FS_INCLUDES .'/client.inc.php');
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                        if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;   
                        }
                        $data['cm_by_f_name'] =$my['f_name'];
                        $data['cm_by_l_name'] = $my['l_name'];    
                        if(!empty($team_members)){
                            foreach ( $team_members as $user ) {
                                $data['uf_name']    = $user['f_name'];
                                $data['ul_name']    = $user['l_name'];
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_STAFF', $data, $email) ) {
                                    $to     = '';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                                }
                            }                                 
                        }
                    }                 
                    // Send Email to the senior AS IMP BOF 
                  
                    //$users = getExecutivesWith($db, array(  array('right'=>'nc_p_ts_notify', 'access_level'=>'')  ) );   
                    if(isset($posted_data['mark_imp']) ){                    
                            $data['link']   = DIR_WS_NC .'/project-task.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'];
                            //include ( DIR_FS_INCLUDES .'/client.inc.php');
                            Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                            if(!empty($clientDetails)){
                                $client = $clientDetails['0'];                       
                                $data['cf_name'] =$client['f_name'];
                                $data['cl_name'] = $client['l_name'] ;    
                            }
                            $data['cm_by_f_name'] =$my['f_name'];
                            $data['cm_by_l_name'] = $my['l_name'] ;      
                            if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_STAFF_IMP', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);                                    
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                            }
                            /*
                            if(!empty($users)){
                                    foreach ( $users as $user ) {
                                        $data['uf_name']    = $user['f_name'];
                                        $data['ul_name']    = $user['l_name'];
                                        $email = NULL;
                                        if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_STAFF_IMP', $data, $email) ) {
                                            $to     = '';
                                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                            
                                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                        }
                                    }
                             
                            }*/
                    }else{
                    
                        $data['link']   = DIR_WS_NC .'/project-task.php?perform=details&id='.$posted_data['id'].'&or_id='.$posted_data['or_id'];
                        //include ( DIR_FS_INCLUDES .'/client.inc.php');
                        Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".
						$order['client']."'");
                        if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;       
                        }
                        $data['cm_by_f_name'] =$my['f_name'];
                        $data['cm_by_l_name'] = $my['l_name'] ;
                        if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_STAFF', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);   
                            $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                             
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,
							$cc,$bcc,$file_name);
                        }
                        /*
                        if(!empty($users)){
                            foreach ( $users as $user ) {
                                $data['uf_name']    = $user['f_name'];
                                $data['ul_name']    = $user['l_name'];
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'TASK_NOTE_FOR_STAFF', $data, $email) ) {
                                    $to     = '';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                }
                            }                         
                        }
                        */                        
                    }
					
                    // Send Email to the Staff BOF  
                   
            }
    }
    
    //Get list of sub threads BOF
    $list	= 	NULL;
    $condition_query_th = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id='".$id."' ORDER BY ".TABLE_PURCHASE_TASK_DETAILS.".do_e";
    $total	=	PurchaseTask::getList($db, $list, TABLE_PURCHASE_TASK_DETAILS.'.id', $condition_query_th);
   
    $condition_url = "&perform=details&id=".$id."&or_id=".$or_id ;
    // $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    $extra_url  = '';
    
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
   // $extra_url  .= "xd=$xd&rpp=$rpp";
    $extra_url  = '&start=url'.$extra_url.'&end=url';
   
    //$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    //$list_th	= 	NULL;
    if($actual_time){
        $condition_query_th = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id='".$id."' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug='0' ORDER BY ".TABLE_PURCHASE_TASK_DETAILS.".do_e DESC";
        $fields_th =  TABLE_PURCHASE_TASK_DETAILS.'.id,'.TABLE_PURCHASE_TASK_DETAILS.'.details,'.TABLE_PURCHASE_TASK_DETAILS.'.added_by,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.hrs,'.TABLE_PURCHASE_TASK_DETAILS.'.min, '.TABLE_PURCHASE_TASK_DETAILS.'.is_client,' ;
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.attached_file,'.TABLE_PURCHASE_TASK_DETAILS.'.attached_desc,'.TABLE_PURCHASE_TASK_DETAILS.'.is_visibleto_client,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.mail_send_to_su,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.file_type,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.file_counter,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.do_e,';
        $fields_th .=  '('.TABLE_PURCHASE_TASK_DETAILS.'.worked_hour_salary * '.SALARY_FACTOR.' ) as  worked_hour_salary';
        
    }else{
         // show atual total time (timesheet * 3 ) total cost
         $condition_query_th = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id='".$id."' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug='0' ORDER BY ".TABLE_PURCHASE_TASK_DETAILS.".do_e DESC";
        $fields_th =  TABLE_PURCHASE_TASK_DETAILS.'.id,'.TABLE_PURCHASE_TASK_DETAILS.'.details,
		'.TABLE_PURCHASE_TASK_DETAILS.'.added_by,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.hrs1,'.TABLE_PURCHASE_TASK_DETAILS.'.min1,'.TABLE_PURCHASE_TASK_DETAILS.'.is_client,' ;
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.attached_file,'.TABLE_PURCHASE_TASK_DETAILS.'.attached_desc,'.TABLE_PURCHASE_TASK_DETAILS.'.is_visibleto_client,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.mail_send_to_su,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.file_type,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.file_counter,';
        $fields_th .=  TABLE_PURCHASE_TASK_DETAILS.'.do_e,';
		$fields_th .=  '('.TABLE_PURCHASE_TASK_DETAILS.'.worked_hour_salary * '.SALARY_FACTOR.' ) as  worked_hour_salary';
    }
  	//PurchaseTask::getList($db, $list_th,$fields_th, $condition_query_th,$next_recordd, $rpp);
  	PurchaseTask::getList($db, $list_th,$fields_th, $condition_query_th);
    $flist_th = array();
    if(!empty($list_th)){
        foreach($list_th as $key=>$val){
            
            if($val['is_client']=='1'){
            
                $table = TABLE_CLIENTS;
                $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS.'.number,'.TABLE_CLIENTS.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                $val['is_client']='1';
                
            }else{
            
                $table = TABLE_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['added_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER.'.number,'.TABLE_USER.'.user_id';
                $exeCreted= getRecord($table,$fields1,$condition2);
                $exeCreted['f_name']." ".$exeCreted['l_name'];
                $val['creator'] = array(    'f_name'=>$exeCreted['f_name'],
                                            'l_name'=>$exeCreted['l_name'],
                                            'number'=>$exeCreted['number'],
                                            'user_id'=>$exeCreted['user_id']
                                        ) ;
                 $val['is_client']='0';
            }
            $val['mail_send_to_su'] = chunk_split($val['mail_send_to_su']);
           
            if($actual_time){
                $val['hrs'] = strlen($val['hrs']) ==1 ? '0'.$val['hrs'] : $val['hrs'] ;
                $val['min'] = strlen($val['min']) ==1 ? '0'.$val['min'] : $val['min'] ;
				$val['cost'] = ceil($val['worked_hour_salary']) ;
            }else{
                 // show atual total time (timesheet * 3 ) total cost
                $val['tothrs'] = $val['hrs1'] ;
                $val['totmin'] = $val['min1'] ;
                $extraHr = (int) ($val['min1'] / 60 ) ;
                $min1 =  ($val['min1'] % 60 ) ;
                $val['tothrs'] =  $val['tothrs'] + $extraHr ;
                $val['totmin'] = $min1 ;
                
                $val['hrs'] = strlen($val['tothrs']) ==1 ? '0'.$val['tothrs'] : $val['tothrs'] ;
                $val['min'] = strlen($val['totmin']) ==1 ? '0'.$val['totmin'] : $val['totmin'] ;
				$val['cost'] = ceil($val['worked_hour_salary']) ;
            }
			if(!empty($val['file_type'])){
				$val['file_type_name'] = $lst_filetype[$val['file_type']];
            }
            $flist_th[$key]=$val;
        }
    } 
   
    //Get list of sub threads EOF


    $condition_query = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".id=".$id;
    $details	= NULL;
    $fields = TABLE_PURCHASE_TASK_DETAILS.".*, ".TABLE_PROJECT_TASK_MODULE.".title as module_title,".TABLE_PROJECT_TASK_STATUS.".title as status_title," ;
    $fields .= TABLE_PROJECT_TASK_PRIORITY.".title as priority_title, ".TABLE_PROJECT_TASK_TYPE.".title as type_title,".TABLE_PROJECT_TASK_RESOLUTION.".title as rs_title," ;
    $fields .= TABLE_PURCHASE_TASK_DETAILS.'.is_client' ;
    
    PurchaseTask::getList( $db, $details, $fields, $condition_query);
    if(!empty($details) ){      
        $details = $details[0];
        if($details['is_client']=='1'){
        
            $table = TABLE_CLIENTS;
           
            $condition2 = " WHERE ".TABLE_CLIENTS .".user_id= '".$details['added_by']."' " ;
            $fields1 =  TABLE_CLIENTS .'.f_name'.','. TABLE_CLIENTS .'.l_name,'.TABLE_CLIENTS .'.number,
			'.TABLE_CLIENTS .'.user_id';
            $exeCreted = getRecord($table,$fields1,$condition2);        
            $details['added_by_name'] = $exeCreted['f_name']." ".$exeCreted['l_name']."" ;
            
        }else{
        
            $table = TABLE_USER;
            $condition2 = " WHERE ".TABLE_USER .".user_id= '".$details['added_by']."' " ;
            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name,'.TABLE_USER .'.number,'.TABLE_USER .'.user_id';
            $exeCreted = getRecord($table,$fields1,$condition2);        
            $details['added_by_name'] = $exeCreted['f_name']." ".$exeCreted['l_name']."" ;
        }
        
        if(empty($details['resolved_date']) || $details['resolved_date']=='0000-00-00 00:00:00'){
            $details['resolved_date']='';
          
            $details['resolved_date']  = date('d') .'/'. date('m').'/'.date('Y');
        }else{
            $details['resolved_date']  = explode(' ', $details['resolved_date']);
            $temp               = explode('-', $details['resolved_date'][0]);
            $details['resolved_date']  = NULL;
            $details['resolved_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
        
        }
        //Get submodule bof
        $details['sub_module_title'] ='';
        $table = TABLE_PROJECT_TASK_MODULE;
        $conditionm = " WHERE ".TABLE_PROJECT_TASK_MODULE .".id= '".$details['sub_module_id']."' " ;
        $fieldsm =  TABLE_PROJECT_TASK_MODULE .'.title';
        $moduleDetails = getRecord($table,$fieldsm,$conditionm);    
        if(!empty( $moduleDetails)){
            
            $details['sub_module_title'] =$moduleDetails['title'];
        }        
     
       
        //Get submodule eof
        //Show estimated time         
        if($actual_time){
        
            $hrs=$min='0';        
            if(!empty($details['hrs'])){           
                $hrs = strlen($details['hrs']) ==1 ? '0'.$details['hrs'] : $details['hrs'] ;
              
            }
            if(!empty($details['min'])){
                $min = strlen($details['min']) ==1 ? '0'.$details['min'] : $details['min'] ;
               
            }
            $details['est_hrs']  = $hrs.":".$min ;
        }else{
            // show atual total time (timesheet * 3 ) total cost
                $hrs=$min='0';        
                if(!empty($details['hrs1'])){           
                    $hrs = strlen($details['hrs1']) ==1 ? '0'.$details['hrs1'] : $details['hrs1'] ;                   
                }
                if(!empty($details['min1'])){
                    $min = strlen($details['min1']) ==1 ? '0'.$details['min1'] : $details['min1'] ;  
                    $extraHr = (int) ($min / 60 ) ;   
                    $min =  ($min % 60 ) ;   
                    $hrs = $hrs +$extraHr;
                }
                
               
                
                $hrs = strlen($hrs) ==1 ? '0'.$hrs : $hrs ;
                $min = strlen($min) ==1 ? '0'.$min : $min ;
                $details['est_hrs']  = $hrs.":".$min ;
        }
        
            
        //Calculate total bugs
            $condition_query_bug = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='0' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PURCHASE_TASK_DETAILS.".task_id = '".$details['id']."'";
            $listbug	= 	NULL;
            $totalbug	=	PurchaseTask::getList( $db, $listbug, TABLE_PURCHASE_TASK_DETAILS.'.id', $condition_query_bug);
            $details['totalbugs']= $totalbug;
            //Calculate total pending bugs
            $condition_query_bugp =" WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='0' AND 
				".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PURCHASE_TASK_DETAILS.".task_id = '".$details['id']."'
                AND ".TABLE_PROJECT_TASK_RESOLUTION.".title !='".PurchaseTask::CLOSE."'";
            $listpbug	= 	NULL;
            $totalpendingbugs	=	PurchaseTask::getList( $db, $listpbug, TABLE_PURCHASE_TASK_DETAILS.'.id', $condition_query_bugp);
            $details['totalpendingbugs']= $totalpendingbugs;
            
            //Calculate taskwise hrs spent
            if($actual_time){
                $fields_tws = ' ( SUM('.TABLE_PURCHASE_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min) % 60 ) as totMin, 
				( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost' ;
           
            }else{
                // show atual total time (timesheet * 3 ) total cost   
                 $fields_tws = ' ( SUM('.TABLE_PURCHASE_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min1) % 60 ) as totMin,
				  ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_tcost' ;
           
            }
            $condition_query_twh = " WHERE ".TABLE_PURCHASE_TASK_DETAILS.".parent_id ='".$details['id']."' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '0' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_client ='0'";
            $list_tws	= 	NULL;
          	PurchaseTask::getList( $db, $list_tws, $fields_tws, $condition_query_twh);           
         
            $details['tot_bhrs'] = '00';
            $details['tot_bmin'] = '00' ;
            $details['worked_task_cost'] = '0' ;
            if(!empty($list_tws[0]['totHr']) || !empty($list_tws[0]['totMin'])){
                $list_tws =$list_tws[0];
                $details['tot_thrs'] = strlen($list_tws['totHr']) ==1 ? '0'.$list_tws['totHr'] : $list_tws['totHr'] ; 
                $details['tot_tmin'] = strlen($list_tws['totMin']) ==1 ? '0'.$list_tws['totMin'] : $list_tws['totMin'] ; 
				$details['tot_tcost'] = ceil($list_tws['tot_tcost']) ;
                
            }else{
                $details['tot_thrs'] = '00';
                $details['tot_tmin'] = '00' ;
				$details['tot_tcost'] = 0 ;
            }
            
            //Calculate bugwise hrs spent           
            if($actual_time){
                    $fields_bws = ' ( SUM('.TABLE_PURCHASE_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min) % 60 ) as totMin, ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_bcost' ;
            }else{
                // show atual total time (timesheet * 3 ) total cost  
                 $fields_bws = ' ( SUM('.TABLE_PURCHASE_TASK_DETAILS .'.hrs1) + ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min1) DIV 60 ))  as totHr '.', ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.min1) % 60 ) as totMin, ( SUM('. TABLE_PURCHASE_TASK_DETAILS .'.worked_hour_salary) * '.SALARY_FACTOR.' ) as tot_bcost' ;
            
            }
             $condition_query_bwh = " WHERE (".TABLE_PURCHASE_TASK_DETAILS.".task_id ='".$details['id']."' AND 
			".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '1' AND ".TABLE_PURCHASE_TASK_DETAILS.".is_client ='0' ) 
			OR ".TABLE_PURCHASE_TASK_DETAILS.".parent_id IN (
				SELECT ".TABLE_PURCHASE_TASK_DETAILS.".id FROM ".TABLE_PURCHASE_TASK_DETAILS." WHERE 
				".TABLE_PURCHASE_TASK_DETAILS.".task_id ='".$details['id']."' AND 
				".TABLE_PURCHASE_TASK_DETAILS.".is_bug = '1' AND 
				".TABLE_PURCHASE_TASK_DETAILS.".is_client ='0' 
            )";
            $list_bws	= 	NULL;
          	PurchaseTask::getList( $db, $list_bws, $fields_bws, $condition_query_bwh); 
            if(!empty($list_bws[0]['totHr']) || !empty($list_bws[0]['totMin'])){
                $list_bws =$list_bws[0];
                $details['tot_bhrs'] = strlen($list_bws['totHr']) ==1 ? '0'.$list_bws['totHr'] :$list_bws['totHr'] ; 
             $details['tot_bmin'] = strlen($list_bws['totMin']) ==1 ? '0'.$list_bws['totMin'] : $list_bws['totMin']; 
                $details['tot_bcost'] = ceil($list_bws['tot_bcost']);
            }else{
                $details['tot_bhrs'] = '00';
                $details['tot_bmin'] = '00' ;
                $details['tot_bcost'] = 0 ;
            }
			$details['worked_task_cost']= $details['tot_tcost'] + $details['tot_bcost'] ;
			$details['worked_task_cost'] = ceil($details['worked_task_cost']);
			$_ALL_POST = $details;
    }
    
    $variables['can_edit'] =false;
    $variables['can_view_list'] =false;
    $variables['can_add'] =false;
    $variables['can_view_client_details'] =false;
	$variables['can_view_cost'] = false;
	$variables['can_add_onbehalf'] = false;
	
    if ( $perm->has('nc_pr_ts_cost') ) {
        $variables['can_view_cost'] = true;
    }
    if ( $perm->has('nc_pr_ts_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_pr_ts_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_pr_ts_add') ) {
        $variables['can_add'] = true;
    }
	if ( $perm->has('nc_p_ts_add_onbhf') ) {
        $variables['can_add_onbehalf'] = true;
    }
	
    if ( $perm->has('nc_uc_contact_details') ) {           
        $variables['can_view_client_details'] = true;
    }
  
   /*
    if ( $perm->has('nc_p_ts_details') ) {
        $variables['can_view_details'] = true;
    }*/
    $variables['can_view_details'] = true;
    // Set the Permissions.
    $hidden[] = array('name'=> 'perform' ,'value' => 'details');
    $hidden[] = array('name'=> 'act' ,'value' => 'save');
    $hidden[] = array('name'=> 'or_id' , 'value' => $or_id);     
    $hidden[] = array('name'=> 'id' , 'value' => $id);     
    $hidden[] = array('name'=> 'actual_time' , 'value' => $actual_time);     
	$hidden[] = array('name'=> 'ajx' , 'value' => $ajx);     
    
    $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
    $page["var"][] = array('variable' => 'lst_msg', 'value' => 'lst_msg');
    $page["var"][] = array('variable' => 'lst_filetype', 'value' => 'lst_filetype');
    $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
    $page["var"][] = array('variable' => 'posted_data', 'value' => 'posted_data');
    $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
    $page["var"][] = array('variable' => 'priorityArr', 'value' => 'priorityArr');
    $page["var"][] = array('variable' => 'statusArr', 'value' => 'statusArr');
    $page["var"][] = array('variable' => 'resolutionArr', 'value' => 'resolutionArr');
    $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => 'flist_th', 'value' => 'flist_th');
   
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-task-details.html');
	
  
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
