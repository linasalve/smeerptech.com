<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
	
	
	
	echo phpinfo();
	
	exit;
    include_once ( DIR_FS_INCLUDES .'/ajax.inc.php' );
	
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$response 	= '';

    if ( $perm->has('nc_ajax') ) {

        //use switch case here to perform action. 
        switch ($perform) {
            case ('getSitePages'): {
            	$site_id	= isset($_GET["site_id"]) 	? $_GET["site_id"]	: ( isset($_POST["site_id"]) 	? $_POST["site_id"] : '' );
               
				$page_list = array();
				if ( isset($site_id) && $site_id != '' ) {
					getSitePages($db, $page_list, $site_id);
				}
				$page_list = array_merge(array(array('page_id' => 0, 'page_name' => 'All pages' )), $page_list);

				$response = ajaxResponse($page_list);
                break;
            }
            case ('getLinkPageList'): {
                $site_id	= isset($_GET["site_id"]) 	? $_GET["site_id"]	: ( isset($_POST["site_id"]) 	? $_POST["site_id"] : '' );
               
				$page_list = array();
				if ( isset($site_id) && $site_id != '' ) {
					getSitePages($db, $page_list, $site_id);
				}
				$response = ajaxResponse($page_list);
                break;
            }
            
            default: {
                $response = 'Invalid action!';
            }
        }
    }
    else {
        $response = 'You do not have the Right to Access this Module.';
    }
    
    echo($response);
?>