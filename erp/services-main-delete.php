<?php
	if ( $perm->has('nc_sr_delete') ) {
		$ss_id	= isset($_GET["ss_id"]) ? $_GET["ss_id"] : ( isset($_POST["ss_id"]) ? $_POST["ss_id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'access_level' 	=> $my['access_level'],
						'messages' 		=> $messages
					);
		Services::delete($ss_id, $extra);
		$variables['hid'] = $ss_id;
		// Display the list.
		include ( DIR_FS_NC .'/services-main-list.php');
	}
	else {
		$messages->setErrorMessage("You donot have the Right to Access this module.");
	}
?>