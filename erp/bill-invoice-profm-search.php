<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
  
    $searchStr = 0;
    if ( $sString != "" ) 
    {
        $searchStr = 1;
        
         // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
       
        //$where_added = true;
        $sub_query='';        
        if ( $search_table == "Any" ) 
        {
            
            if ( $where_added ) 
            {
                $condition_query .= "OR ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ) 
            {
              
                if ( $table != "Any" ) 
                { 
                    
                    if ($table == TABLE_BILL_INV_PROFORMA) 
                    {
                        $sub_query .= "( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            /*
                            if($field !='number' && $field !='or_no' && $field !='id' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                            
                    }elseif($table == TABLE_BILL_ORD_P){
                        $sub_query .= "( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            //if($field !='number' && $field !='or_no' && $field !='id' ){
                              //  $sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                            //}else{
                              //    $sub_query .= $table .".". clearSearchString($field) ." = '".trim($sString)."' OR " ;
                            //}
                            //$sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    }elseif($table == TABLE_USER){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if($field !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_BILL_ORDERS){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if($field !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_CLIENTS){
                    
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                             $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if($field !='number'){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }
                            */
                           
                            //$sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }
                    
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= " AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                /*
                if($sType !='number' && $sType !='or_no' && $sType !='id' ){
                    $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                }else{
                     $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ."='".trim($sString) ."' )" ;
                }*/
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
    //code for search EOF 2009-03-mar-21 
    
    //Client serach bof
	$search_client = isset($_POST["search_client"])   ? $_POST["search_client"]  : (isset($_GET["search_client"])   ? $_GET["search_client"]   :'');
	$search_client_details = isset($_POST["search_client_details"])   ? $_POST["search_client_details"]  : (isset($_GET["search_client_details"])   ? $_GET["search_client_details"]   :'');
	
	if(!empty($search_client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_BILL_INV_PROFORMA .".client ='". $search_client ."' ";        
        $condition_url          .= "&search_client_details=$search_client_details&search_client=$search_client";
        $_SEARCH["search_client_details"]  = $search_client_details;
        $_SEARCH["search_client"] = $search_client;       
	}
	//Client serach eof
    $tax_amt = isset($_POST["tax_amt"])   ? $_POST["tax_amt"]  : (isset($_GET["tax_amt"])   ? $_GET["tax_amt"]   :'');
    if(!empty($tax_amt)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
					 
		if($tax_amt==1){ //Tax Paid
			$condition_query .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount=0";
		}elseif($tax_amt==2){ //Tax Extra Paid
			$condition_query .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount<0";
		}elseif($tax_amt==3){ //Tax Not Paid
			$condition_query .= " ".TABLE_BILL_ORDERS.".total_tax_balance_amount>0";
		}
		 $condition_url          .= "&tax_amt=$tax_amt";
        $_SEARCH["tax_amt"]  = $tax_amt;			 
	}
    // BO: Status data.
    $sStatusStr = '';
    $sStatusStrPass = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR') && isset($sStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }        
       /*
       if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStrPass = implode(",", $sStatus) ;
        }else{
            $sStatusStrPass =$sStatus;
            $sStatusStr = str_replace(",","','",$sStatus) ;
            $sStatus = explode(",", $sStatus) ;
           
        }*/
        
        if(is_array($sStatus)){
            $sStatusStr = implode("','", $sStatus) ;
            $sStatusStr1 = implode(",", $sStatus) ;             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;            
        }
        
        $condition_query .= " ". TABLE_BILL_INV_PROFORMA .".status IN ('". $sStatusStr ."') ";
        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;
    }
  
    // EO: Status data.
	// EO: Proforma created or not bof  
    $chk_paid = isset($_POST["chk_paid"]) ? $_POST["chk_paid"]  : (isset($_GET["chk_paid"])? $_GET["chk_paid"]   :'');
    $paidStatus = isset($_POST["paidStatus"]) ? $_POST["paidStatus"] : (isset($_GET["paidStatus"])
	? $_GET["paidStatus"]      :'');
    if ( ( $chk_paid == 'AND' || $chk_paid == 'OR') &&  isset($paidStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_paid;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_INV_PROFORMA .".paid_status ='".$paidStatus."' "; 
        $condition_url          .= "&chk_paid=$chk_paid&paidStatus=$paidStatus";
        $_SEARCH["chk_paid"]  = $chk_paid;
        $_SEARCH["paidStatus"]     = $paidStatus;        
    }    
    // EO: Proforma created or not eof.
	
     // EO: Proforma created or not bof  
    $chk_pinv = isset($_POST["chk_pinv"])   ? $_POST["chk_pinv"]  : (isset($_GET["chk_pinv"])   ? $_GET["chk_pinv"]   :'');
    $pInv    = isset($_POST["pInv"])      ? $_POST["pInv"]     : (isset($_GET["pInv"])      ? $_GET["pInv"]      :'');
    if ( ( $chk_pinv == 'AND' || $chk_pinv == 'OR') &&  isset($pInv)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_pinv;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".is_invoiceprofm_create ='".$pInv."' ";        
		
       
        $condition_url          .= "&chk_pinv=$chk_pinv&pInv=$pInv";
        $_SEARCH["chk_pinv"]  = $chk_pinv;
        $_SEARCH["pInv"]     = $pInv;        
    }    
    // EO: Proforma created or not eof.
	// EO: Invoice created or not bof  
    $chk_inv = isset($_POST["chk_inv"])   ? $_POST["chk_inv"]  : (isset($_GET["chk_inv"])   ? $_GET["chk_inv"]   :'');
    $sInv    = isset($_POST["sInv"])      ? $_POST["sInv"]     : (isset($_GET["sInv"])      ? $_GET["sInv"]      :'');
    if ( ( $chk_inv == 'AND' || $chk_inv == 'OR') &&  isset($sInv)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_inv;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".is_invoice_create ='".$sInv."' ";        
		
       
        $condition_url          .= "&chk_inv=$chk_inv&sInv=$sInv";
        $_SEARCH["chk_inv"]  = $chk_inv;
        $_SEARCH["sInv"]     = $sInv;        
    }    
    // EO: Invoice created or not eof.
	
	 //BO: Hardware Ord or not bof  
	$chk_hd_ord = isset($_POST["chk_hd_ord"]) ? $_POST["chk_hd_ord"]  : (isset($_GET["chk_hd_ord"])   ? $_GET["chk_hd_ord"]   :'');
	$is_hardware_ord = isset($_POST["is_hardware_ord"]) ? $_POST["is_hardware_ord"]:(isset($_GET["is_hardware_ord"]) ? 
	$_GET["is_hardware_ord"]      :'');
	if ( ( $chk_hd_ord == 'AND' || $chk_hd_ord == 'OR') &&  isset($is_hardware_ord)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_hd_ord;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".is_hardware_ord ='".$is_hardware_ord."' ";  
	   
		$condition_url          .= "&chk_hd_ord=$chk_hd_ord&is_hardware_ord=$is_hardware_ord";
		$_SEARCH["chk_hd_ord"]  = $chk_hd_ord;
		$_SEARCH["is_hardware_ord"]     = $is_hardware_ord;        
	}
	//BO: Special Ord or not bof  
	//BO : Company
    $sCompIdStr='';
    $chk_cmp = isset($_POST["chk_cmp"])   ? $_POST["chk_cmp"]  : (isset($_GET["chk_cmp"])   ? $_GET["chk_cmp"]   :'');
    $sCompId    = isset($_POST["sCompId"])      ? $_POST["sCompId"]     : (isset($_GET["sCompId"])      ?$_GET["sCompId"]     :'');
    if ( ($chk_cmp == 'AND' || $chk_cmp == 'OR' ) && !empty($sCompId)) {
        $searchStr = 1;
		if ( $where_added ) {
           $condition_query .= " ". $chk_cmp;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
              
        if(is_array($sCompId)){
             $sCompIdStr = implode("','", $sCompId) ;
             $sCompIdStr1 = implode(",", $sCompId) ;
             
        }else{
            $sCompIdStr = str_replace(',',"','",$sCompId);
            $sCompIdStr1 = $sCompId ;
            $sCompId = explode(",",$sCompId) ;
            
        }
       
       $condition_query .= " ". TABLE_BILL_INV_PROFORMA .".company_id  IN ('". $sCompIdStr ."') ";
       $condition_url          .= "&chk_cmp=$chk_cmp&sCompId=$sCompIdStr1";   
       $_SEARCH["chk_cmp"]  = $chk_cmp;
       $_SEARCH["sCompId"]     = $sCompId;
    } 
    //EO : Company
	$chk_special = isset($_POST["chk_special"]) ? $_POST["chk_special"]  : (isset($_GET["chk_special"])   ? $_GET["chk_special"]   :'');
	$is_special = isset($_POST["is_special"]) ? $_POST["is_special"]:(isset($_GET["is_special"]) ? 
	$_GET["is_special"]      :'');
	if ( ( $chk_special == 'AND' || $chk_special == 'OR') &&  isset($is_special)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_special;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".special ='".$is_special."' ";  
	   
		$condition_url          .= "&chk_special=$chk_special&is_special=$is_special";
		$_SEARCH["is_special"]  = $is_special;
		$_SEARCH["chk_special"]     = $chk_special;        
	}
	//BO: Special Ord or not bof  
	//BO: Invoice Delete or not bof  
	$chk_inv_delete = isset($_POST["chk_inv_delete"]) ? $_POST["chk_inv_delete"]  : (isset($_GET["chk_inv_delete"]) ? 
	$_GET["chk_inv_delete"]   :'');
	$is_inv_delete = isset($_POST["is_inv_delete"]) ? $_POST["is_inv_delete"]:(isset($_GET["is_inv_delete"]) ? 
	$_GET["is_inv_delete"]      :'');
	if ( ( $chk_inv_delete == 'AND' || $chk_inv_delete == 'OR') &&  isset($is_inv_delete)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_inv_delete;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".is_inv_delete ='".$is_inv_delete."' ";  
	   
		$condition_url          .= "&chk_inv_delete=$chk_inv_delete&is_inv_delete=$is_inv_delete";
		$_SEARCH["chk_inv_delete"]  = $chk_inv_delete;
		$_SEARCH["is_inv_delete"]     = $is_inv_delete;        
	} 	
	//BO: Tax Search
	$chk_tax = isset($_POST["chk_tax"]) ? $_POST["chk_tax"]  : (isset($_GET["chk_tax"])   ? $_GET["chk_tax"]   :'');
	$tax_ids = isset($_POST["tax_ids"]) ? $_POST["tax_ids"]:(isset($_GET["tax_ids"]) ? 
	$_GET["tax_ids"]      :'');
	if ( ( $chk_tax == 'AND' || $chk_tax == 'OR') &&  isset($tax_ids)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_tax;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		if($tax_ids=='-1'){
			//$condition_query .= " ( ". TABLE_BILL_ORDERS .".tax_ids = '%,0,%' OR  ".TABLE_BILL_ORDERS .".tax_ids='' )" ;  
			$condition_query .= " ( ". TABLE_BILL_INV_PROFORMA .".tax_ids = '%,0,%' OR  ".TABLE_BILL_INV_PROFORMA .".tax_ids='' )" ;  
		}else{
			//$condition_query .= " ". TABLE_BILL_ORDERS .".tax_ids LIKE '%,".$tax_ids.",%' ";  
			$condition_query .= " ". TABLE_BILL_INV_PROFORMA .".tax_ids LIKE '%,".$tax_ids.",%' ";  
		}
		$condition_url          .= "&chk_tax=$chk_tax&tax_ids=$tax_ids";
		$_SEARCH["chk_tax"]  = $chk_tax;
		$_SEARCH["tax_ids"]     = $tax_ids;        
	} 
	
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
 
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from) ){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_BILL_INV_PROFORMA .".".$dt_field." >= '". $dfa ."'";        
        $condition_url  .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
        $_SEARCH["dt_field"]       = $dt_field;
    }
    
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_BILL_INV_PROFORMA .".".$dt_field." <= '". $dta ."'";
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }    
    // EO: Upto Date
    $chk_old_update        = isset($_POST["chk_old_update"])      ? $_POST["chk_old_update"]         : (isset($_GET["chk_old_update"])      ? $_GET["chk_old_update"]      :'');
   if(isset($chk_old_update ) && $chk_old_update!=''){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= " ". TABLE_BILL_INV_PROFORMA .".old_updated = '". $chk_old_update ."' AND 
		".TABLE_BILL_INV_PROFORMA.".is_old='1'";
         
        $condition_url          .= "&chk_old_update=$chk_old_update";
        $_SEARCH["chk_old_update"] = $chk_old_update ;
     
    }
	
	$chk_cron_created = isset($_POST["chk_cron_created"]) ? $_POST["chk_cron_created"]: (isset($_GET["chk_cron_created"])      ? $_GET["chk_cron_created"]      :'');	 
	$is_cron = isset($_POST["is_cron"]) ? $_POST["is_cron"]:(isset($_GET["is_cron"]) ? 
	$_GET["is_cron"]      :'');
    if(isset($chk_cron_created ) && $chk_cron_created!='' && $is_cron!=''){
	 $searchStr = 1;
         if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= " ". TABLE_BILL_ORDERS .".cron_created = '". $is_cron ."' ";
         
        $condition_url .= "&chk_cron_created=$chk_cron_created&is_cron=".$is_cron;
        $_SEARCH["chk_cron_created"] = $chk_cron_created ;
        $_SEARCH["is_cron"] = $is_cron ;
     
    }
    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/bill-invoice-profm-list.php');
?>