<?php
    if ( $perm->has('nc_bk_stmt_delete') ) {  
        if(array_key_exists('submit_delete',$_POST)){
			if(!empty($_POST['del_stmt'])){ 
				$delStr =  implode($_POST['del_stmt'],",");
				$sql = "SELECT id FROM ".TABLE_BANK_STATEMENT." WHERE 
					".TABLE_BANK_STATEMENT.".id IN(".$delStr.") AND ".TABLE_BANK_STATEMENT.".status_u='".BankStatement::LINKED."'";
				$db->query($sql);
				if ( $db->nf() > 0 ) {
					$messages->setErrorMessage("Please Check the Selected Records, as there are Some Records which are LINKED with Transactions.");
				}else{				
					$sql = "DELETE FROM ".TABLE_BANK_STATEMENT." WHERE ".TABLE_BANK_STATEMENT.".id IN(".$delStr.") ";
					$db->query($sql);				
					$messages->setOkMessage("Selected Records Deleted Successfully.");
				}
			}else{ 
				$messages->setErrorMessage("Please Select the Records to Delete.");
			}
		} 
    } else {
        $messages->setErrorMessage("You do not have the Right to Access this module.");
    }
	// Display the list.
	$searchStr=1; 
	include ( DIR_FS_NC .'/bank-statement-list.php');
?>
