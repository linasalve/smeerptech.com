<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
	
    $client = isset($_GET['client']) ? $_GET['client'] : '';
    
   	$smtp_list = '' ;
    if (isset($client) && !empty($client)){
		
		$query = "SELECT ".TABLE_NEWSLETTER_SMTP.".* FROM ".TABLE_NEWSLETTER_SMTP." WHERE 
				".TABLE_NEWSLETTER_SMTP.".status ='1' AND ".TABLE_NEWSLETTER_SMTP.".client= '".$client."'";
        $query	.= " ORDER BY ".TABLE_NEWSLETTER_SMTP.".smtp_id ASC";
                
		$db->query( $query );
		$total	= $db->nf();	
		if( $db->nf() > 0 ){
			while($db->next_record()){
				$details = trim($db->f("domain_name")). " ".trim($db->f("from_email")) ;
				$smtp_list .= $details."|".$db->f("smtp_id").",";
			}
			echo rtrim($smtp_list,',');
		}else{		
			echo "|";
		}
   	   
	}	

    include_once( DIR_FS_NC ."/flush.php");
?>
