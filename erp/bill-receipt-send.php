<?php
 if ( $perm->has('nc_bl_rcpt_send') ) {
    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
    $rcpt_id  = isset($_GET["rcpt_id"])? $_GET["rcpt_id"]: ( isset($_POST["rcpt_id"])? $_POST["rcpt_id"]: '' );

    $_ALL_POST	    = NULL;
    $_ALL_Data	    = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
   
    /*$condition_query = " WHERE (". TABLE_BILL_INV .".id = '". $inv_id ."' "
                            ." OR ". TABLE_BILL_INV .".number = '". $inv_id ."')";*/
    $condition_query = " WHERE (". TABLE_BILL_RCPT .".id = '". $rcpt_id ."')";

    $fields = TABLE_BILL_RCPT .'.number'
                .','. TABLE_BILL_RCPT .'.inv_no'
                .','. TABLE_BILL_RCPT .'.amount'
                .','. TABLE_BILL_RCPT .'.currency_name'
                .','. TABLE_BILL_RCPT .'.do_r'
                .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                .','. TABLE_CLIENTS .'.number AS c_number'
                .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                .','. TABLE_CLIENTS .'.email AS c_email';
                
    if ( Receipt::getDetails($db, $_ALL_Data, $fields, $condition_query) > 0 ) {
        $_ALL_Data = $_ALL_Data['0'];
      
        //$_ALL_POST['to']= $_ALL_Data['c_email'];
        $rcpt_no =  $_ALL_Data['number'];
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            //print_r($_ALL_POST);    
            $data		= processUserData($_ALL_POST);   
            $extra = array( 'db' 		=> &$db,
                            'messages'          => &$messages
                          );
                          
            if(Receipt::validateSendReceipt($data, $extra)){
            
                $email = NULL;
                $cc_to_sender= $cc = $bcc = Null;
                
                $data['client'] = array(
                                    "f_name" => $_ALL_Data['c_f_name'],
                                    "l_name" => $_ALL_Data['c_l_name']
                                    );
                $data['number'] = $_ALL_Data['number'];
                $data['inv_no'] = $_ALL_Data['inv_no'];
                $data['amount'] = $_ALL_Data['amount'];
                $data['currency'] = $_ALL_Data['currency_name'];
                $data['do_r'] = $_ALL_Data['do_r'];
                
                 $file_name = DIR_FS_RPT_PDF_FILES ."/". $data["number"] .".pdf";
                
                if ( getParsedEmail($db, $s, 'RECEIPT_CREATE_TO_CLIENT', $data, $email) ) {        
                    $to = '';
                    $to[]   = array('name' => $_ALL_Data['c_f_name'] .' '. $_ALL_Data['c_f_name'], 'email' => $_ALL_POST['to']);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                    /*echo $email["body"];
                    echo $email["subject"];
                    print_r($to);
                    print_r($from);*/
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                }
				if(!empty($smeerp_client_email)){
					$to     = '';
					$cc_to_sender=$cc=$bcc='';
					$to[]   = array('name' => $_ALL_Data['c_f_name'] .' '. $_ALL_Data['c_f_name'], 
					'email' => $smeerp_client_email);
					$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
					SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
					$cc_to_sender,$cc,$bcc,$file_name);
				}   
                $messages->setOkMessage("Receipt has been sent successfully on given email id.");  
                /*
                    $_ALL_POST['email']= $_ALL_POST['to'];
                 
                    $file_name = DIR_FS_INV_FILES ."/". $data["inv_no"] .".html";
                    $handle = fopen($file_name, "r");
                    $contents = fread($handle, filesize($file_name));
                    //To check 
                    //$contents = htmlspecialchars($contents) ;
                    fclose($handle);
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT', $data, $email) ) {
                      
                        $cc_to_sender= $cc = $bcc = Null;
                        $to     = '';
                        $email["isHTML"] = true;
                        $to[]   = array('name' => $_ALL_Data['c_f_name'] .' '. $_ALL_Data['c_f_name'], 'email' => $_ALL_POST['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        
                        //SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                        SendMail($to, $from, $reply_to, $email["subject"], $contents, $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                        $messages->setOkMessage("Invoice has been sent successfully on given email id.");  
                    }
                
               */
            }
                
        }
        
        $hidden[] = array('name'=> 'rcpt_id' ,'value' => $rcpt_id);
        $hidden[] = array('name'=> 'rcpt_no' ,'value' => $rcpt_no);
        $hidden[] = array('name'=> 'perform' ,'value' => 'send');
        $hidden[] = array('name'=> 'act' , 'value' => 'save');

        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        $page["var"][] = array('variable' => 'rcpt_no', 'value' => 'rcpt_no');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-receipt-send.html');
        
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
}else{
 
    $messages->setErrorMessage("You donot have the Right to Send Invoice.");
}
?>