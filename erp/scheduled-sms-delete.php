<?php
	if ( $perm->has('nc_sms_schd_delete') ) {
		$s_id	= isset($_GET["s_id"]) ? $_GET["s_id"] : ( isset($_POST["s_id"]) ? $_POST["s_id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'messages' 		=> $messages
					);
		ScheduledSmsList::delete($s_id, $extra);
		
		// Display the list.
		include ( DIR_FS_NC .'/scheduled-sms-list.php');
	}
	else {
		$messages->setErrorMessage("You do not have the Right to Access this module.");
	}
?>