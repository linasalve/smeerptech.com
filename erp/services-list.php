<?php

    if ( $perm->has('nc_sr_list') ) {
      
   
        $sOrderBy =clearSearchString($sOrderBy,"-");
        if(empty($condition_query )){
            $condition_query =" AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'";
        }
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        } 
        $_SEARCH['searched'] = 1;
        $db1 		= new db_local;
        // To count total records.
        $list	= 	NULL;
        $condition_query1 = " LEFT JOIN ".TABLE_SETTINGS_SERVICES." as ss2 ON 
          ".TABLE_SETTINGS_SERVICES.".ss_parent_id = ss2.ss_id  
          WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id != '0' AND ss2.ss_status='1' ". $condition_query ;  
        
		/* $condition_query1 = " LEFT JOIN ".TABLE_SETTINGS_SERVICES." as ss2 ON 
			".TABLE_SETTINGS_SERVICES.".ss_parent_id = ss2.ss_id  
			". $condition_query ; */
		$fields = TABLE_SETTINGS_SERVICES.".ss_title,".TABLE_SETTINGS_SERVICES.".ss_id,
			".TABLE_SETTINGS_SERVICES.".ss_status,ss2.ss_title as ss_parent_title,".TABLE_SETTINGS_SERVICES.".file_1,
			".TABLE_SETTINGS_SERVICES.".file_2,".TABLE_SETTINGS_SERVICES.".file_3, 
			".TABLE_SETTINGS_SERVICES.".sequence_subservices,
			".TABLE_SETTINGS_SERVICES.".ss_parent_id ";

        
        $query = "SELECT ".$fields." FROM ". TABLE_SETTINGS_SERVICES.$condition_query1;
        $db->query($query);   
        if ( $db->nf()>0 ) {
            while ( $db->next_record() ) { 
                 $ss_title=$db->f('ss_title');   
                 $sequence_subservices=$db->f('sequence_subservices');				 
                 $ss_id=$db->f('ss_id');   
                 $ss_parent_id=$db->f('ss_parent_id');   
                 
                 $file_1=$db->f('file_1');   
                 $file_2=$db->f('file_2');   
                 $file_3=$db->f('file_3');   
				 
                 $ss_status=$db->f('ss_status');   
                 $ss_parent_title=$db->f('ss_parent_title');   
                 $inUse=0;
                //get subservices count 
                $sql1= "SELECT id FROM ".TABLE_BILL_ORD_P." WHERE "
                  . TABLE_BILL_ORD_P .".sub_s_id REGEXP ',". $ss_id .",' LIMIT 0,1" ;
                
                $db1->query($sql1);    
                while ( $db1->next_record() ) { 
                    $inUse = $db1->f('id');   
                }                
                 //get subservices price 
                $sql2=  "SELECT ".TABLE_SETTINGS_SERVICES_PRICE.".service_price,
				".TABLE_SETTINGS_CURRENCY.".currency_name
				FROM ".TABLE_SETTINGS_SERVICES_PRICE." LEFT JOIN ".TABLE_SETTINGS_CURRENCY." 
				ON ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = ".TABLE_SETTINGS_CURRENCY.".id 
				WHERE ".TABLE_SETTINGS_SERVICES_PRICE.".service_id=".$ss_id ;
			 
                $db1->query($sql2);  
				$priceList = array();
                while ( $db1->next_record() ) { 
                    $service_price=$db1->f('service_price'); 
                    $currency_name=$db1->f('currency_name'); 
					$priceList[] = array('currency_name'=>$currency_name,
										'service_price'=>$service_price
										);
                } 
                //get services count
                /*
                  $sql1= "SELECT count(*) as totalSer FROM ".TABLE_BILL_ORD_P." WHERE "
                  . TABLE_BILL_ORD_P .".s_id = '". $ss_id ."' " ;
                */
                /*
                echo   $sql1= "SELECT SUM(".TABLE_BILL_ORDERS.".amount) as tot_amount FROM ".TABLE_BILL_ORD_P." 
                    INNER JOIN ". TABLE_BILL_ORDERS ." ON  ". TABLE_BILL_ORDERS.".number = ". TABLE_BILL_ORD_P.".ord_no WHERE "
                  . TABLE_BILL_ORD_P .".s_id = '". $ss_parent_id ."' AND ".TABLE_BILL_ORDERS.".is_invoice_create = '1'
                  GROUP BY ".TABLE_BILL_ORD_P.".s_id " ;
                echo "<br/>";  
                  
                echo "<br/>";
                $tot_amount='';
                $db1->query($sql1);    
                while ( $db1->next_record() ) { 
                   echo $tot_amount=$db1->f('tot_amount');   
                }                 
                */
                 
                 
                $list[]= array(
                    "ss_id"=>$ss_id,
                    "ss_title"=>$ss_title,
					"sequence_subservices"=>$sequence_subservices,					
                    "file_1"=>$file_1,
                    "file_2"=>$file_2,
                    "file_3"=>$file_3,
                    "inUse"=>$inUse,
                    "ss_status"=>$ss_status,
                    "priceList"=>$priceList,
                    "ss_parent_title"=>$ss_parent_title
                );
            }
        }
        
      
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $condition_url .= "&rpp=".$rpp."&perform=".$perform; 
       
       
       
       
       
       
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;

        if ( $perm->has('nc_sr_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_sr_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_sr_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_sr_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_sr_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_sr_status') ) {
            $variables['can_change_status']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        //$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'services-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>