<?php

    if ( $perm->has('nc_pst_details') ) {
        include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php');
        $ticket_id	= isset($_GET["ticket_id"]) ? $_GET["ticket_id"] : ( isset($_POST["ticket_id"]) ? $_POST["ticket_id"] : '' );
        $client	= isset($_GET["client"]) ? $_GET["client"] : ( isset($_POST["client"]) ? $_POST["client"] : '' );
	    
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        //BOF read the available departments
        //ProspectsTicket::getDepartments($db,$department);
        //EOF read the available departments
         
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
            
            if ( ProspectsTicket::validateAssign($data, $extra) ) {
            
                /*
				$assign_members_str=implode(",",$data['assign_members']);
                $assign_members_str=",".$assign_members_str.",";
            
                $query = "UPDATE ". TABLE_PROSPECTS_TICKETS 
                    ." SET ". TABLE_PROSPECTS_TICKETS .".assign_members = '".$assign_members_str."' "
                    ." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				*/
				$or_detail = $data['number']."<br/>".$data['order_details'] ;
				$query = "UPDATE ". TABLE_PROSPECTS_TICKETS 
                    ." SET ". TABLE_PROSPECTS_TICKETS .".order_id = '".$data['order_id']."', "
					. TABLE_PROSPECTS_TICKETS .".flw_ord_id = '".$data['order_id']."', "
					. TABLE_PROSPECTS_TICKETS .".order_details = '".$or_detail."' "
                    ." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_id = '". $ticket_id ."' " ; 
                            
                if ($db->query($query) && $db->affected_rows() > 0){
                    $variables['hid'] = $db->last_inserted_id() ; 
                    $messages->setOkMessage("Order assigned successfully to Main ticket and corresponding threads of this ticket.");
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                //$data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCreate'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            //include ( DIR_FS_NC .'/prospects-ticket-list.php');
			
			header("Location:".DIR_WS_NC."/prospects-ticket.php?perform=ordflw_view&flw_ord_id=".$data['order_id']."&client_inv=".$client."&ticket_id=".$ticket_id);
        }
        else { 
            $condition_query= " WHERE ticket_id = '". $ticket_id ."' ";
            $_ALL_POST      = NULL;
            
            if ( ProspectsTicket::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST[0];
                $ticketStatusArr = ProspectsTicket::getTicketStatus();
                $ticketStatusArr = array_flip($ticketStatusArr);
                $_ALL_POST['ticket_status_name'] = $ticketStatusArr[$_ALL_POST['ticket_status']];   
            }
            /*        
            $table = TABLE_SETTINGS_DEPARTMENT;
            $condition2 = " WHERE ".TABLE_SETTINGS_DEPARTMENT .".id= '".$_ALL_POST['ticket_department'] ."' " ;
            $fields1 =  TABLE_SETTINGS_DEPARTMENT .'.department_name';
            $ticket_department= getRecord($table,$fields1,$condition2);
            
            $_ALL_POST['department_name']    = $ticket_department['department_name'] ;     
            */
            $condition_query= " WHERE user_id = '". $_ALL_POST['ticket_owner_uid'] ."' ";
            $member      = NULL;
            if ( Prospects::getList($db, $member, 'f_name,l_name,org,mobile1,mobile2', $condition_query) > 0 ) {
                $_ALL_POST['client'] = $member[0];
                $_ALL_POST['client']['name'] = $member[0]['f_name']." ".$member[0]['l_name'];
            }
            
            
            // BO: Read the Team Members Information.
            if ( !empty($_ALL_POST['assign_members']) && !is_array($_ALL_POST['assign_members']) ) {
                //include ( DIR_FS_INCLUDES .'/user.inc.php');
                
                $_ALL_POST['assign_members']          = explode(',', $_ALL_POST['assign_members']);
                $temp                       = "'". implode("','", $_ALL_POST['assign_members']) ."'";
                $_ALL_POST['assign_members_st']  = '';
                $_ALL_POST['assign_members_details']  = array();
                User::getList($db, $_ALL_POST['assign_members_st'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
                $_ALL_POST['assign_members'] = array();
                foreach ( $_ALL_POST['assign_members_st'] as $key=>$members) {
                //for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
                    $_ALL_POST['assign_members'][] = $members['user_id'];
                    $_ALL_POST['assign_members_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
                }
            }
            // EO: Read the Team Members Information.
            //Get the list of current orders of this prospect bof
			if(!empty($_ALL_POST['ticket_owner_uid'])){
				$sql="SELECT ". TABLE_PROSPECTS_ORDERS .".number"
                    .",". TABLE_PROSPECTS_ORDERS .".order_title"
					.",". TABLE_PROSPECTS_ORDERS .".details FROM "
					.TABLE_PROSPECTS_ORDERS." WHERE 
					".TABLE_PROSPECTS_ORDERS.".lead_status NOT IN('".ProspectsOrder::LOOSELSTATUS."','".ProspectsOrder::NEGATIVELSTATUS."') AND "
					.TABLE_PROSPECTS_ORDERS .".client ='".$_ALL_POST['ticket_owner_uid']."' ORDER BY ".TABLE_PROSPECTS_ORDERS.".do_o" ;
					
				$db->query($sql);               
			   if ( $db->nf()>0 ) {
					while ( $db->next_record() ){
						$number = $db->f('number') ;
						$order_title = $db->f('order_title') ;
						$details = $db->f('details') ;
						$ord_details[] = array(
							'number'=>$number,
							'order_title'=>$order_title,
							'details'=>$details 
						);
					}
				}
			}
            //Get the list of current orders of this prospect eof
			
			
            // These parameters will be used when returning the control back to the List page.
            foreach( parseURL($_SERVER['QUERY_STRING']) as $key=>$value){
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'assign_st');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'ticket_id', 'value' => $ticket_id);
            $hidden[] = array('name'=> 'number', 'value' => $number);
            $hidden[] = array('name'=> 'client', 'value' => $_ALL_POST['ticket_owner_uid']);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'ord_details', 'value' => 'ord_details');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            //$page["var"][] = array('variable' => 'status', 'value' => 'status');
            //$page["var"][] = array('variable' => 'priority', 'value' => 'priority');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            //print_r($_ALL_POST);
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-ticket-assign-st.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>