<?php

    if ( $perm->has('nc_bkt_as_list') ) {
        
       
        if(empty($condition_query)){
            $condition_query = " AND ticket_status IN('".BankTicket::PENDINGWITHSMEERP."','".BankTicket::PENDINGWITHSMEERPCOZCLIENT."') ";
        }
        
        $condition_query = " LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id = ".TABLE_VENDORS_BANK_TICKETS.".ticket_owner_uid 
                             WHERE ".TABLE_VENDORS_BANK_TICKETS.".ticket_child='0' AND ".TABLE_VENDORS_BANK_TICKETS.".assign_members LIKE '%,". $my['user_id'] .",%' AND ".TABLE_VENDORS_BANK_TICKETS.".status='".BankTicket::ACTIVE."'".$condition_query    ;
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        
        $total	=	BankTicket::getList( $db, $list, '', $condition_query);
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
    
        $list	= NULL;
        $fields = TABLE_VENDORS_BANK_TICKETS.".*,";
        $fields .= TABLE_CLIENTS.".f_name,".TABLE_CLIENTS.".l_name,".TABLE_CLIENTS.".billing_name,".TABLE_CLIENTS.".org,".TABLE_CLIENTS.".email,".TABLE_CLIENTS.".mobile1,".TABLE_CLIENTS.".mobile2";
        BankTicket::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        $ticketStatusArr = BankTicket::getTicketStatus();
        $ticketStatusArr = array_flip($ticketStatusArr);
        $ticketRatingArr = BankTicket::getRating();
        $ticketRatingArr = array_flip($ticketRatingArr);
        
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                $val['ticket_status_name'] = $ticketStatusArr[$val['ticket_status']];  
               
                if(!empty($val['ticket_rating'])){
                    $val['ticket_rating_name'] = $ticketRatingArr[$val['ticket_rating']];  
                }
                
                $executive=array();
                $string = str_replace(",","','", $val['assign_members']);
                $fields4 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
                $condition4 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
                User::getList($db,$executive,$fields4,$condition4);
                $executivename='';
              
                foreach($executive as $key1=>$val1){
                    $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
                } 
                $val['assign_members'] = $executivename ; 
                 
                $db2 = new db_local ;
                
                $query = "SELECT COUNT(*) AS replies FROM ". TABLE_VENDORS_BANK_TICKETS
                ." WHERE ". TABLE_VENDORS_BANK_TICKETS .".ticket_child = '". $val['ticket_id'] ."' " ; 
                $db2->query($query) ;
                $db2->next_record() ;
                $ticket_replies = $db2->f("replies") ;
                
                $val['ticket_replied']    = $ticket_replies ;                
                $fList[$key]=$val;
            }
        }
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_view_details']  = false;
        $variables['can_view_client_details']  = false;
       
        //nc_st_client_details
        if ( $perm->has('nc_bkt_contact_details') ) {
            $variables['can_view_client_details'] = true;
        }
        if ( $perm->has('nc_bkt_as_details') ) {
            $variables['can_view_details'] = true;
        }
        
        if ( $perm->has('nc_bkt_as_list') ) {
            $variables['can_view_list'] = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bank-ticket-assign-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>