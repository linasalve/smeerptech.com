<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
    include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	
	
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
   	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
   
    
    if($added){
        $messages->setOkMessage("Ticket has been created successfully. And email Notification has been sent.");
    }
    
    if($added_all){
        $messages->setOkMessage("Ticket has been created successfully for ".$i ."/". $j .". And email Notification has been sent.");
    }
    
    if ( $perm->has('nc_api') ) { //nc_api  
	
		 
        //BOF read the available priority
        //SupportTicket::getPriority($db,$priority);
        //EOF read the available priority
        
        //use switch case here to perform action. 
        switch ($perform) {
        
             
            case ('change-pwd'): {
			    $action = 'Email Change Password';
                $perform = 'change-pwd';
                include ( DIR_FS_NC .'/api-email-change-pwd.php');  
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'api.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            } 
			case ('domain-dt-list'): {
			    $perform = 'list';
				$action = 'Domain List';
                include ( DIR_FS_NC .'/api-domain-dt-list.php');  
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'api.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            } 
            case ('domain-list'):
            default: {
                $perform = 'list';
				$action = 'Domain List';
                include ( DIR_FS_NC .'/api-domain-list.php');  
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'api.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    } else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");    
		$page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'api.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("action", $action);    
    $s->assign("variables", $variables);    
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
           $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	
	include_once( DIR_FS_NC ."/flush.php");
?>