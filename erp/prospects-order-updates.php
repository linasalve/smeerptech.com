<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   )); 
    include_once ( DIR_FS_NC ."/header.php"); 
    include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php');
	include_once (DIR_FS_INCLUDES .'/prospects.inc.php'); 
    
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$or_id 		= isset($_GET["or_id"]) ? $_GET["or_id"]        : ( isset($_POST["or_id"])          ? $_POST["or_id"]       :'');
	$id 		= isset($_GET["id"])       ? $_GET["id"]        : ( isset($_POST["id"])          ? $_POST["id"]       :'');
    $added 		= isset($_GET["added"]) ? $_GET["added"]  : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	//$xd 		= isset($_GET["xd"])    ? $_GET["xd"]        : ( isset($_POST["xd"])          ? $_POST["xd"]       :'');
    $rpp = isset($_GET["rpp"]) ? $_GET["rpp"] : ( isset($_POST["rpp"]) ? $_POST["rpp"] : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));	// Result per page 
 
    $condition_url='';
    
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE; 
    }
    $condition_query ='';
   
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    /*
    if ( empty($xd) ) {
        $xd              = 1;
        $next_recordd    = 0 ;
    }
    else {
        $next_recordd    = ($xd-1) * $rpp;
    }*/
    
    $variables["x"]     = $x;
    //$variables["xd"]     = $xd;
    $variables["rpp"]   = $rpp;
    
    
    if($added){    
        $messages->setOkMessage("New Project Task entry has been done.");
    }
        
    if ( $perm->has('nc_ps_ou') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_PROSPECTS_ORDERS   =>  array(  'Order No.'     => 'number',
                                                                'Order Title'   => 'order_title',
																'Site'   		=> 'site_url',
                                                              //'Particulars'   => 'particulars',
                                                                'Details'       => 'details'
                                                        ),
                                 // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                 TABLE_USER      =>  array(  'Creator Number'    => 'number-user',
                                                            'Creator Username'  => 'username-user',
                                                            'Creator Email'     => 'email-user',
                                                            'Creator First Name'=> 'f_name-user',
                                                            'Creator Last Name' => 'l_name-user',
                                                ),  
								TABLE_PROSPECTS 	=>  array( //'Prospect Number'    => 'number-clients',
                                                            'Prospect Company'  => 'org-'.TABLE_PROSPECTS,
                                                           // 'Prospect Username'  => 'username-'.TABLE_PROSPECTS,
                                                            'Prospect Email'     => 'email-'.TABLE_PROSPECTS,
                                                            'Prospect First Name'=> 'f_name-'.TABLE_PROSPECTS,
                                                            'Prospect Last Name' => 'l_name-'.TABLE_PROSPECTS,
                                                ),
								
                            );
       $sOrderByArray  = array(
                                TABLE_PROSPECTS_ORDERS => array( 'Date of Order'     => 'do_o',
                                                            'Date of Delivery'  => 'do_d',
                                                            'Date of Creation'  => 'do_c',
                                                            'Status'            => 'status',
                                                            'Delay'            => 'delay'
														),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
           if(!empty($hid)){
                $_SEARCH['sOrderBy']= $sOrderBy = 'id';
                $variables['hid']=$hid;
            }else{
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_o';
            }
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PROSPECTS_ORDERS;
        }
		$variables['status'] = ProspectsOrder::getStatus();
        $variables['flwstatus'] = ProspectsOrder::getFollowupStatus();
        $variables['types'] = ProspectsOrder::getTypes();
		$variables['lstatus'] = ProspectsOrder::getLStatus(); 
        $where_added =false ;
       
        //use switch case here to perform action. prospects-order-updates.php
        switch ($perform) {           
            case ('list'): {            
                include (DIR_FS_NC.'/prospects-order-updates-list.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-order-updates.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }            
            case ('search'): {            	
                include(DIR_FS_NC."/prospects-order-updates-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-order-updates.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }            
            case ('list'):
            default: { 
                include (DIR_FS_NC .'/prospects-order-updates-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-order-updates.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        //$messages->setErrorMessage("You donot have the Right to Access this Module.");
        
         $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-updates.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
       
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
