<?php

    if ( $perm->has('nc_sms_ca_add') ) {

        $_ALL_POST	= NULL;
        $data       = NULL;

		//get client list for the dropdown
		ClientAccount::showUsers($clientlist);
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            if ( ClientAccount::validateAdd($data, $extra) ) {
				$sql = "SELECT ca_id,ca_balance,ca_allotedsms FROM ".TABLE_SMS_CLIENT_ACC." WHERE ca_username='".$data['username']."' AND ca_password='".md5($data['pass'])."'";
				$db->query($sql);
				if($db->nf() > 0)
				{
					$db->next_record();
					$numsms = $db->f('ca_balance');
					$id = $db->f('ca_id');
				}
				else
				{
					$firstrecdflg = 1;    
					$numsms = 0;
				}
	
				$exp_date = explode("/",$data['expire_date']);
				$expdate = $exp_date['2']."-".$exp_date['1']."-".$exp_date['0'];
				if($firstrecdflg == 1)
				{
	
					$sql = "INSERT INTO ".TABLE_SMS_CLIENT_ACC." SET 
							ca_id 		= '".$data['id']."',
							ca_userid 		= '".$data['userid']."',
							ca_username 	= '".$data['username']."',
							ca_password 	= '".md5($data['pass'])."',
							ca_allotedsms 	= '".$data['allotedsms']."',
							ca_balance 		= '".$data['allotedsms']."',
							ca_displayname 	= '".$data['displayname']."',
							ca_mobileno 	= '".$data['mobileno']."',
							ca_expirydate 	= '".$expdate."',
							ca_posturl 		= '".$data['posturl']."',
							ca_status 		= '".$data['ca_status']."'";
					if($db->query($sql))
					{
						$sql = "INSERT INTO ".TABLE_SMS_CLIENT_ACC_HISTORY." SET 
							cah_caid 		= '".$data['id']."',
							cah_noofsms 	= '".$data['allotedsms']."',
							cah_amount 		= '".$data['amount']."',
							cah_opening 	= '0',
							cah_balance 	= '".$data['allotedsms']."',
							cah_persmsrate 	= '".$data['persmsrate']."',
							cah_date 		= '".date("Y-m-d")."'";
						$db->query($sql);
						$sql = "UPDATE ".TABLE_SMS_MASTER_ACC." SET ma_allotedsms=(ma_allotedsms + ".$data['allotedsms']."), 
								ma_totalsms = (ma_totalsms - ".$data['allotedsms'].")";
						$db->query($sql);
						$flg = 1;
					}
					else
						$flg = 0;
				}
				else
				{
					$sql = "UPDATE ".TABLE_SMS_CLIENT_ACC." SET 
							ca_id 		= '".$data['id']."',
							ca_userid 		= '".$data['userid']."',
							ca_username 	= '".$data['username']."',
							ca_password 	= '".md5($data['pass'])."',
							ca_displayname 	= '".$data['displayname']."',
							ca_mobileno 	= '".$data['mobileno']."',
							ca_allotedsms 	= '".$data['allotedsms']."',
							ca_balance 		= '".($numsms + $data['allotedsms'])."',
							ca_expirydate 	= '".$expdate."',
							ca_posturl 		= '".$data['posturl']."',
							ca_status 		= '".$data['ca_status']."' WHERE ca_username='".$data['username']."' AND ca_password='".md5($data['pass'])."'";
					if($db->query($sql))
					{
						$sql = "INSERT INTO ".TABLE_SMS_CLIENT_ACC_HISTORY." SET 
							cah_caid 		= '".$data['id']."',
							cah_noofsms 	= '".$data['allotedsms']."',
							cah_amount 		= '".$data['amount']."',
							cah_opening 	= '".$numsms."',
							cah_balance 	= '".($numsms + $data['allotedsms'])."',
							cah_persmsrate 	= '".$data['persmsrate']."',
							cah_date 		= '".date("Y-m-d")."'";
						$db->query($sql);
						$sql = "UPDATE ".TABLE_SMS_MASTER_ACC." SET ma_allotedsms=(ma_allotedsms + ".$data['allotedsms']."), 
							ma_totalsms = (ma_totalsms - ".$data['allotedsms'].")";
						$db->query($sql);
						$flg = 1;
					}
					else
						$flg = 0;
				}
                if ($flg == 1) 
					$messages->setOkMessage("The Package has been added.");
				else
					$messages->setErrorMessage('Problem adding Package. Try later');
				//to flush the data.
				$_ALL_POST	= NULL;
				$data		= NULL;
			}
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/client-account-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
  			$page["var"][] = array('variable' => 'clientlist', 'value' => 'clientlist');

            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'client-account-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>