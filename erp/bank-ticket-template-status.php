<?php
    if ( $perm->has('nc_bkt_t_status') ) {
        $id= isset($_GET["id"])? $_GET["id"] : ( isset($_POST["id"])? $_POST["id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
        $access_level = $my['access_level'];
       
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        BankTicketTemplate::updateStatus($id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/bank-ticket-template-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Change Status.");
    }
?> 
