<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   )); 
				   
    include_once ( DIR_FS_NC ."/header.php"); 
    
	$id =  isset($_GET["id"])   ? $_GET["id"]  : ( isset($_POST["id"])    ? $_POST["id"] : '' );
	$status = isset($_GET["status"])   ? $_GET["status"]  : ( isset($_POST["status"])    ? $_POST["status"] : '' );
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new 		= new db_local; // database handle
	
    if (isset($status) && $status!=''){      
        if( $status=='1'){
			$sql= " UPDATE ".TABLE_PAYMENT_TRANSACTION." SET ".TABLE_PAYMENT_TRANSACTION.".linking_status='".$status."' WHERE ".TABLE_PAYMENT_TRANSACTION.".id ='".$id."'";
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>	
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Transaction link status is updated Successfully as NOLINK  </li>
							</ul>
						</td>
					</tr>
					</table>";
					$message .= "| NO LINKED BILL/RCPT ";
					/*$message .= "| <a href=\"#\" onClick=\"updateLinkStatus(".$id.",0)\">
                         <img src=\"".addslashes($variables['nc_images'])."/update-on.gif\" border=\"0\" 
                                title=\"Active\" name=\"Active\" alt=\"Active\"/></a> ";*/
								
			}
        }elseif($status=='0'){
		
			$sql= " UPDATE ".TABLE_PAYMENT_TRANSACTION." SET ".TABLE_PAYMENT_TRANSACTION.".linking_status='".$status."' WHERE ".TABLE_PAYMENT_TRANSACTION.".id ='".$id."'";
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Transaction link status is updated Successfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					$message .= "| <a href=\"#\" onClick=\"updateLinkStatus(".$id.",1)\">
                        Mark NO LINKED BILL/RCPT </a> ";
							
			}
		
		} 	   
	}	

echo $message."|".$id ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
