<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    
	   
	
   
    
	$id =  isset($_GET["id"])   ? $_GET["id"]  : ( isset($_POST["id"])    ? $_POST["id"] : '' );
	$tdsstatus = isset($_GET["tdsstatus"])   ? $_GET["tdsstatus"]  : ( isset($_POST["tdsstatus"])    ? $_POST["tdsstatus"] : '' );
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new 		= new db_local; // database handle
	
    if (isset($tdsstatus) && $tdsstatus!=''){      
        if( $tdsstatus=='1'){
			$sql= " UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".tds_status='".$tdsstatus."' WHERE ".TABLE_BILL_ORDERS.".id ='".$id."'";
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Order Assigned for TDS followup is Activated Successfully  </li>
							</ul>
						</td>
					</tr>
					</table>";
					$message .= "| <a href=\"#\" onClick=\"updateTdsStatus(".$id.",0)\">
                         <img src=\"".addslashes($variables['nc_images'])."/update-on.gif\" border=\"0\" 
                                title=\"Active\" name=\"Active\" alt=\"Active\"/></a> ";
								
			}
        }elseif($tdsstatus=='0'){
		
			$sql= " UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".tds_status='".$tdsstatus."' WHERE ".TABLE_BILL_ORDERS.".id ='".$id."'";
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Order Assigned for TDS followup is Deactivated Successfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					
					$message .= "| <a href=\"#\" onClick=\"updateTdsStatus(".$id.",1)\">
                         <img src=\"".addslashes($variables['nc_images'])."/update-off.gif\" border=\"0\" 
                                title=\"De-active\" name=\"De-active\" alt=\"De-active\"/></a> ";
								
			}
		
		} 	   
	}	

echo $message."|".$id ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
