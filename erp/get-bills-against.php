<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");

    $vendor_id = isset($_GET['vendor_id']) ? $_GET['vendor_id'] : '';
    $vendor_bank_id = isset($_GET['vendor_bank_id']) ? $_GET['vendor_bank_id'] : '';
    
   	$billsAgainstList = '' ;
    if (isset($vendor_id) && !empty($vendor_id))
    {
        
		$query	="SELECT id, bill_against FROM ".TABLE_PAYMENT_BILLS_AGAINST." WHERE 
		".TABLE_PAYMENT_BILLS_AGAINST.".vendor_id ='".$vendor_id."' AND 
		".TABLE_PAYMENT_BILLS_AGAINST.".vendor_bank_id ='' AND
		".TABLE_PAYMENT_BILLS_AGAINST.".status='1'";
        $query	.= " ORDER BY ".TABLE_PAYMENT_BILLS_AGAINST.".bill_against ASC";
        
		$db->query( $query );
		$total	= $db->nf();	
		if( $db->nf() > 0 )
		{
			while($db->next_record())
			{
				$billsAgainstList .=  trim($db->f("bill_against"))."|".$db->f("id").",";
			}
			echo rtrim($billsAgainstList,',');
		}else{		
			echo "|";
		}
	}

	if (isset($vendor_bank_id) && !empty($vendor_bank_id)){
        
		$query	="SELECT id, bill_against FROM ".TABLE_PAYMENT_BILLS_AGAINST." WHERE 
		".TABLE_PAYMENT_BILLS_AGAINST.".vendor_bank_id ='".$vendor_bank_id."' AND 
		".TABLE_PAYMENT_BILLS_AGAINST.".vendor_id ='' AND ".TABLE_PAYMENT_BILLS_AGAINST.".status='1'";
        $query	.= " ORDER BY ".TABLE_PAYMENT_BILLS_AGAINST.".bill_against ASC";
        
		$db->query( $query );
		$total	= $db->nf();	
		if( $db->nf() > 0 )
		{
			while($db->next_record())
			{
				$billsAgainstList .=  trim($db->f("bill_against"))."|".$db->f("id").",";
			}
			echo rtrim($billsAgainstList,',');
		}else{		
			echo "|";
		}
	}		
	include_once( DIR_FS_NC ."/flush.php");
?>
