<?php
if ( $perm->has('nc_fq_at_c_renew') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        $lst_client=null;
        $fields = TABLE_CLIENTS .'.user_id'
                   .','. TABLE_CLIENTS .'.f_name'.','.TABLE_CLIENTS .'.l_name';
        $condition_query= "WHERE service_id LIKE '%,". Clients::FIREQUERY .",%'";
        Clients::getList($db,$lst_client,$fields,$condition_query);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( FqAllotedClient::validateUpdate($data, $extra) ) {
            
               $query = "SELECT * FROM ".TABLE_FQ_ALLOTED_CLIENT." WHERE id='".$id."'";
               if ( $db->query($query) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                            $history[] = processSqlData($db->result());
                        }
                    }
                }
                $history = $history['0'];
                
               $query  = " UPDATE ". TABLE_FQ_ALLOTED_CLIENT
                            ." SET ". TABLE_FQ_ALLOTED_CLIENT .".balance_fq 		= balance_fq+'".$data['allotted_fq'] ."'"
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".allotted_fq 	= allotted_fq+'". $data['allotted_fq'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".rate 	        = '". $data['rate'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT .".amount 			= '". $data['amount'] ."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT .".do_expiry 		= '". $data['do_expiry']."'"                                    
                                    .",". TABLE_FQ_ALLOTED_CLIENT .".status 		= '". $data['status']."'"                                    
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Firequery entry has been renewed successfully.");
                    $variables['hid'] = $id;
                    
               /*     $query	= " SELECT api_id, gateway_id  FROM ".TABLE_SMS_CLIENT_ACCOUNT." WHERE id='".$data['client_acc_id']."'";   
                    
                    if ( $db->query($query) ) {
                        if ( $db->nf() > 0 ) {
                            while ($db->next_record()) {
                                $api_id = $db->f('api_id');
                                $gateway_id = $db->f('gateway_id');
                            }
                        }
                    }
                    
                    $query  = " UPDATE ". TABLE_SMS_PURCHASE
                            ." SET ". TABLE_SMS_PURCHASE .".consumed_fq= consumed_fq+'".$data['allotted_fq'] ."'"
                                	.",". TABLE_SMS_PURCHASE .".balance_fq = balance_fq-'". $data['allotted_fq'] ."'"                    
                            		." WHERE api_id='".$api_id."' AND gateway_id='".$gateway_id."'";
                    
                    $db->query($query) ;
                    */                    
                    $balance_fq = $history['balance_fq'] + $data['allotted_fq'];
                    $query	= " INSERT INTO ".TABLE_FQ_ALLOTED_CLIENT_HISTORY
                            ." SET ".TABLE_FQ_ALLOTED_CLIENT_HISTORY .".alloted_client_id 		= '". $history['id'] ."'"  
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".client_id 		= '". $history['client_id'] ."'"                                
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".no_of_fq	    = '". $data['allotted_fq'] ."'"                    
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".consumed_fq	= '". $history['consumed_fq'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".opening_fq	= '". $history['balance_fq'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".balance_fq	= '".$balance_fq ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".rate 	        = '". $history['rate'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".amount 			= '". $history['amount'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".created_by 		= '". $history['created_by'] ."'"                    
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".ip 			    = '". $history['ip'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".do_e             = '". $history['do_e']."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".do_expiry 		= '". $history['do_expiry']."'"                                    
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".status 		    = '". $history['status']."'"                                    
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".ip_h 			= '". $_SERVER['REMOTE_ADDR'] ."'"                    
                                	.",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".do_he            = '". date('Y-m-d H:i:s')."'"
                                    .",". TABLE_FQ_ALLOTED_CLIENT_HISTORY .".history_created_by = '". $my['user_id'] ."'";   
                    $db->query($query);                 
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_FQ_ALLOTED_CLIENT .'.id,'.TABLE_FQ_ALLOTED_CLIENT .'.client_id';            
            $condition_query = " WHERE (". TABLE_FQ_ALLOTED_CLIENT .".id = '". $id ."' )";
            
            if ( FqAllotedClient::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                $id         = $_ALL_POST['id'];
                /*$client_acc_id = $_ALL_POST['client_acc_id'];
                
                $query	= " SELECT ".TABLE_SMS_PURCHASE.".balance_fq FROM ".TABLE_SMS_CLIENT_ACCOUNT."
                        LEFT JOIN ". TABLE_SMS_PURCHASE." ON ( ".TABLE_SMS_PURCHASE.".api_id = ".TABLE_SMS_CLIENT_ACCOUNT.".api_id 
                        AND ".TABLE_SMS_PURCHASE.".gateway_id = ".TABLE_SMS_CLIENT_ACCOUNT.".gateway_id )
                    WHERE ".TABLE_SMS_CLIENT_ACCOUNT.".id='".$client_acc_id."'";   
                 */   
                if ( $db->query($query) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                             $_ALL_POST['balance_fq'] = $db->f("balance_fq");
                        }
                    }
                }
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        /*if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            include ( DIR_FS_NC .'/sms-purchase-list.php');
        }*/
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/fq-alloted-client.php?perform=renew&added=1&id=".$id);
             $variables['hid'] = $id;
            $condition_query='';
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/fq-alloted-client.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/fq-alloted-client.php?added=1");   
            $variables['hid'] = $id;
            $condition_query='';
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'renew');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'lst_client', 'value' => 'lst_client');                 
            //$page["var"][] = array('variable' => 'lst_account', 'value' => 'lst_account');    
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'fq-alloted-client-renew.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
