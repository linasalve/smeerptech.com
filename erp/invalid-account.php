<?php

	if (!defined("THIS_DOMAIN")){
		require_once("../lib/config.php"); 
	} 
    
    /*page_open(array("sess" => "NC_Session",
                "auth" => "NC_Default_Auth"
            ));
*/
    //include( THIS_PATH ."/header.php" );

    $s 			= new smarty; 	//smarty template handle
	// Initialize the configuratioin of the Template engine.
    $s->template_dir	= DIR_FS_NC .'/templates/';
    $s->compile_dir 	= DIR_FS_NC .'/templates_c/';
    $s->config_dir 		= DIR_FS_SMARTY .'/configs/';
    $s->cache_dir 		= DIR_FS_SMARTY .'/cache/';
	$s->use_sub_dirs 	= false;
	$s->force_compile 	= true;
    $variables["title"] 			= TITLE ;
	$variables["domain"] 			= THIS_DOMAIN;
    $variables["lib"]               = DIR_WS_LIB;
    $variables["nc"] 				= DIR_WS_NC;
    $variables["mp"] 				= DIR_WS_MP;
    $variables["nc_images"]		    = DIR_WS_IMAGES_NC;
    $variables["images"]            = DIR_WS_IMAGES;
    $variables["css"]               = DIR_WS_CSS;
    $variables["scripts"]           = DIR_WS_SCRIPTS;
    $page["section"][] = array('container'=>'CONTENT', 'page' => 'content.html');
    $page["section"][] = array('container'=>'INDEX', 'page' => 'invalid-account.html');
    
  //echo$_SERVER['REMOTE_ADDR'];
    
    // always assign
    $s->assign("variables", $variables);
   

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>