<?php

    if ( $perm->has('nc_pr_list') || $perm->has('nc_pr_select_list') || $perm->has('nc_pr_search') ) {
        
        include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
        include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
        
        $region         = new RegionProspects();
        $phone          = new PhoneProspects(TABLE_PROSPECTS);
        
        $variables['send_mail_yes'] = Prospects::SENDMAILYES;
        $variables['send_mail_no'] = Prospects::SENDMAILNO;
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE (';
        }
        else {
            $condition_query .= ' AND (';
        }
        
        $access_level   = $my['access_level'];        
       
         //show the list of clients of same access levels
        if ( $perm->has('nc_pr_list_all') ) { 
            $condition_query .= " ".TABLE_PROSPECTS.".parent_id = '' )"; 
        }else{      
            $condition_query .= " (".TABLE_PROSPECTS .".created_by ='".$my['user_id']."' OR ".TABLE_PROSPECTS .".shared_member LIKE ',%".$my['user_id']."%,')";    
            $condition_query .= ')';
            $condition_query .= " AND parent_id = '' ";            
        }
        
       
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
         //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
	
        if($searchStr==1){
            $total	=	Prospects::getList( $db, $list, '', $condition_query);
            // $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            //$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');            
			$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','prospects.php','frmSearch');
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';
        
            $list	= NULL;
            Prospects::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
        }
        /*$phone  = new Phone(TABLE_PROSPECTS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_PROSPECTS, $list[$i]['user_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}
		*/
     
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                $client=array();
                $string = str_replace(",","','", $val['alloted_clients']);
                $clientname='';
                if(!empty($string)){
                   $fields1 =  TABLE_PROSPECTS .'.f_name'.','. TABLE_PROSPECTS .'.l_name';
                   $condition1 = " WHERE ".TABLE_PROSPECTS .".user_id IN('".$string."') " ;
                   Prospects::getList($db,$client,$fields1,$condition1);                   
                   if(!empty($client)){
                        foreach($client as $key1=>$val1){
                            $clientname .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
                        } 
                   }
                }
                $val['alloted_clients'] = $clientname ;               
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_PROSPECTS, $val['user_id']);
                $val['phone'] = $phone->get($db);
                
                // Read the Addresses.
                $region->setAddressOf(TABLE_PROSPECTS, $val['user_id']);
                $val['address_list'] = $region->get();
                $val['sublist_list']=array();
                if($searchLink && $perm->has('nc_uc_su_list')){
                    $parent_id = $val['user_id'];
                    $sublist	= 	NULL;
                    $condition_query=" WHERE parent_id='".$parent_id."'" ; 
                    Prospects::getList( $db, $sublist,
					'user_id,status,f_name,l_name,mobile1,mobile2,email,email_1,email_2', $condition_query);
                     $val['sublist_list']=$sublist;
                
                }
				$val['lead_source_to_name']= '';
				if(!empty($val['lead_source_to'])){				
					$table = TABLE_SALE_SOURCE;
					$fields1 =  TABLE_SALE_SOURCE .'.title' ;
					$condition1 = " WHERE ".TABLE_SALE_SOURCE .".id =".$val['lead_source_to']." " ;
					$iaccArr = getRecord($table,$fields1,$condition1);
					if(!empty($iaccArr)){
						$val['lead_source_to_name'] = $iaccArr['title'];
					}					
				}
				$val['lead_source_from_name']= '';
				if(!empty($val['lead_source_from'])){				
					$table = TABLE_SALE_SOURCE;
					$fields1 =  TABLE_SALE_SOURCE .'.title' ;
					$condition1 = " WHERE ".TABLE_SALE_SOURCE .".id =".$val['lead_source_from']." " ;
					$iaccArr = getRecord($table,$fields1,$condition1);
					if(!empty($iaccArr)){
						$val['lead_source_from_name'] = $iaccArr['title'];
					}					
				} 
                $fList[$key]=$val;
            }
        }
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_invite']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_view_sub_user'] = false;
        $variables['can_send_password'] = false;
        $variables['can_up_mail_status'] = false;
        $variables['can_view_client_details'] = false;
        $variables['can_add_followup'] = false;
        $variables['can_edit_su'] = false;
        $variables['can_copy_to_client'] = false;
        
		//nc_pr_list_all
		if ( $perm->has('nc_uc_add') ) {
            $variables['can_copy_to_client'] = true;
        }
		
        if ( $perm->has('nc_pr_su_edit') ) {
            $variables['can_edit_su'] = true;
        }
       if ( $perm->has('nc_pr_contact_details') ) {           
            $variables['can_view_client_details'] = true;
        }
        if ( $perm->has('nc_pr_list') ) {
            $variables['can_view_list'] = true;
        }
		 if ( $perm->has('nc_pr_list_all') ) {
            $variables['can_view_list_all'] = true;
        }
		
        if ( $perm->has('nc_pr_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_pr_invite') ) {
            $variables['can_invite'] = true;
        }
        if ( $perm->has('nc_pr_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_pr_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_pr_details') ) {
            $variables['can_view_details'] = true;
        }
        if ( $perm->has('nc_pr_status') ) {
            $variables['can_change_status'] = true;
        }
        if ( $perm->has('nc_pr_su_list') ) {
            $variables['can_view_sub_user'] = true;
        }
        if ( $perm->has('nc_pr_send_pwd') ) {
            $variables['can_send_password'] = true;
        }
        if ( $perm->has('nc_pr_up_mst') ) {
            $variables['can_up_mail_status'] = true;
        }
		
		//can_view_list_all
       // Read the available source
	    include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
       	Leads::getSource($db,$source);
        $variables['searchLink']=$searchLink ;
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'source', 'value' => 'source');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>