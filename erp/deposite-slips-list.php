<?php
if ( $perm->has('nc_ds_list') ) {    
       
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	DepositeSlips::getList( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_DEPOSITE_SLIPS.'.* '  ;
    DepositeSlips::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    /*$flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
            $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';                
            $condition2 = " WHERE ".TABLE_USER .".user_id IN('".$val['created_by']."') " ;
           
            $table =TABLE_USER ;              
            $createdArr = getRecord($table,$fields1,$condition2);
            if(!empty($createdArr)){
                $createdBy = $createdArr['f_name']." ".$createdArr['l_name'] ;   
            }
            $val['added_by']=$createdBy ;
            $flist[$key]=$val;
            
        }
    }*/
    
    
    
    // Set the Permissions.
   
    if ( $perm->has('nc_ds_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_ds_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_ds_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_ds_delete') ) {
        $variables['can_delete'] = true;
    }
    
    if ( $perm->has('nc_ds_status') ) {
        $variables['can_change_status']     = true;
    }
	
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'deposite-slips-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
