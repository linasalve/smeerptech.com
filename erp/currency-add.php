<?php
  if ( $perm->has('nc_cur_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the payment party class
		include_once (DIR_FS_INCLUDES .'/currency.inc.php');
		
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( Currency::validateAdd($data, $extra) ) { 
                    
                    $sql= " SELECT title FROM ".TABLE_COUNTRIES." WHERE ".TABLE_COUNTRIES.".code='".$data['country_code']."'";
                    $db->query($sql);
                    $country_name ='';
                    if ( $db->nf()>0 ) {
                        while ( $db->next_record() ) {
                            $country_name = $db->f('title') ;
                        }
                    }
                            $query	= " INSERT INTO ".TABLE_SETTINGS_CURRENCY
                            ." SET ".TABLE_SETTINGS_CURRENCY .".currency_name = '". $data['currency_name'] ."'"  
									.",". TABLE_SETTINGS_CURRENCY .".currency_symbol = '". 		$data['currency_symbol'] ."'"
									.",". TABLE_SETTINGS_CURRENCY .".abbr = '". 		$data['abbr'] ."'"
									.",". TABLE_SETTINGS_CURRENCY .".country_code = '". 		$data['country_code'] ."'"
                                    .",". TABLE_SETTINGS_CURRENCY .".country_name = '". 		$country_name ."'"
                                	.",". TABLE_SETTINGS_CURRENCY .".status = '". 		$data['status'] ."'"    
                                    .",". TABLE_SETTINGS_CURRENCY .".ip            = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                      
                                	.",". TABLE_SETTINGS_CURRENCY .".date = '". 		date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New currency entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/currency.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/currency.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/currency.php?added=1");   
        }
        else {
	        //$lst_country    = $region->getCountryList($data['country']);
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');  
			//$page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');   
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'currency-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
