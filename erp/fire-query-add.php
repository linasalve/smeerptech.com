<?php

    if ( $perm->has('nc_fq_add') ) {
        
        //$user_id = isset ($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
	    $step1=1;
        $step2=0;
        //$user_id = $my['user_id'];
        //$parent_id = $my['parent_id'];
        
        //$ticket_owner_uid=$user_id;
        
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        //BOF read the available departments
        FireQuery::getDepartments($db,$department);
        //EOF read the available departments
        
        $variables["MAX_FILE_SIZE"] = MAX_FILE_SIZE ;
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
            //print_r($data);
            if($data['step1']=='1'){
                if ( FireQuery::validateMemberSelect($data, $extra) ) {
                    $step2=1;
                    $step1=0;
                    $condition_query= " WHERE user_id = '". $data['ticket_owner_uid'] ."' ";
                    $clients      = NULL;
                    if ( Clients::getList($db, $clients, '*', $condition_query) > 0 ) {
                        //$_ALL_POST = $_ALL_POST[0];
                        $_ALL_POST['name']=$clients[0]['f_name']." ".$clients[0]['l_name'];
                        $_ALL_POST['email']=$clients[0]['email'];
                    }
                    
                    //BOF read the available domains
                    //FireQuery::getDomains($db,$domains,$data['client']);
                    //EOF read the available domains
                   
                }
            }else{
                if ( FireQuery::validateAdd($data, $extra) ) {
                    
                    //BOF read the available status
                    //SupportTicket::getStatus($db,$status);
                    
                    //EOF read the available status
                    
                    //BOF read the available priority
                    //SupportTicket::getPriority($db,$priority);
                    //EOF read the available priority
                    
                    $ticket_no  =  "PT". date("ymdhi-s");
                    
                    if(!empty($data['ticket_attachment'])){
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['ticket_attachment']["name"]);
                       
                        $ext = $filedata["extension"] ; 
                        //$attachfilename = $username."-".mktime().".".$ext ;
                       
                        $attachfilename = $ticket_no.".".$ext ;
                        $data['ticket_attachment'] = $attachfilename;
                        
                        $ticket_attachment_path = DIR_WS_FQ_FILES;
                        
                        if (move_uploaded_file ($files['ticket_attachment']['tmp_name'], DIR_FS_FQ_FILES."/".$attachfilename)){
                        
                        }
                    }
                    
                    $query = "INSERT INTO "	. TABLE_FQ_TICKETS ." SET "
                                            . TABLE_FQ_TICKETS .".ticket_no         = '". $ticket_no ."', "
                                            . TABLE_FQ_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_status     = '". $data['ticket_status'] ."', "
                                            . TABLE_FQ_TICKETS .".ticket_child      = '0', "
                                            . TABLE_FQ_TICKETS .".ticket_attachment = '". $attachfilename ."', "
                                            . TABLE_FQ_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
                                            . TABLE_FQ_TICKETS .".ticket_response_time = '0', "
                                            . TABLE_FQ_TICKETS .".ticket_replied        = '0', "
                                            //. TABLE_FQ_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
                                            . TABLE_FQ_TICKETS .".is_private           = '". $data['is_private'] ."', "
                                            . TABLE_FQ_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
                                            . TABLE_FQ_TICKETS .".do_e    = '".date('Y-m-d H:i:s', time())."', "
                                            . TABLE_FQ_TICKETS .".ticket_date       = '". time() ."' " ;
                    
                    //unset($GLOBALS["flag"]);
                    //unset($GLOBALS["submit"]);
                                
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        
                        $variables['hid'] = $db->last_inserted_id() ;
                        
                        $query_update_fqs  = " UPDATE ". TABLE_FQ_ALLOTED_CLIENT
                                            ." SET ". TABLE_FQ_ALLOTED_CLIENT .".consumed_fq = consumed_fq + 1"
                                	            .",". TABLE_FQ_ALLOTED_CLIENT .".balance_fq = balance_fq - 1"                    
                            		            ." WHERE client_id='".$data['ticket_owner_uid']."'";
                        $db->query($query_update_fqs) ;
                        
                        /**************************************************************************
                        * Send the notification to the ticket owner and the Administrators
                        * of the new Ticket being created
                        **************************************************************************/
                        $data['ticket_no']    =   $ticket_no ;
                        foreach( $status as $key=>$val){                           
                            if ($data['ticket_status'] == $val['status_id'])
                            {
                                $data['status']=$val['status_name'];
                            }
                        }
                        $data['domain']   = THIS_DOMAIN;
                        //$data['replied_by']   =   $data['ticket_owner'] ;
                        $data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
                        $data['subject']    =   $data['ticket_subject'] ;
                        $data['text']    =   $data['ticket_text'] ;
                        $data['attachment']   =   $attachfilename ;
                        $data['link']   = DIR_WS_MP .'/fire-query.php?perform=view&ticket_id='. $variables['hid'];
                        
                        // Send Email to the ticket owner BOF  
                        Clients::getList($db, $userDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$data['ticket_owner_uid'] ."'");
                        if(!empty($userDetails)){
                            $userDetails=$userDetails['0'];
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $email = NULL;
                            
                            if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
                                $to = '';
                                $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' => $userDetails['email']);
                               
                              
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }                        
                        }
                        // Send Email to the ticket owner EOF
                        
                        
                        // Send Email to the admin BOF
                        $email = NULL;
                         $data['link']   = DIR_WS_NC .'/fire-query.php?perform=view&ticket_id='. $variables['hid'];
                       
                        if ( getParsedEmail($db, $s, 'EMAIL_ST_ADMIN', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $admin_name , 'email' => $admin_email);                           
                           
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }                        
                        // Send Email to the admin EOF
                        
                        $messages->setOkMessage("New Fire Query Ticket has been created.And email Notification has been sent.");
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
                }else{
                    $step2=1;
                    $step1=0;
                    $_ALL_POST = $data;
                }
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/fire-query-list.php');
        }
        else {
            /*$condition_query= " WHERE user_id = '". $user_id ."' ";
            $clients      = NULL;
            if ( Clients::getList($db, $clients, '*', $condition_query) > 0 ) {
                //$_ALL_POST = $_ALL_POST[0];
                $_ALL_POST['name']=$clients[0]['f_name']." ".$clients[0]['l_name'];
                $_ALL_POST['email']=$clients[0]['email'];
            }*/
            
            $condition_query= " WHERE service_id LIKE '%,". Clients::FIREQUERY .",%'";
            
            $clients      = NULL;
            Clients::getList($db, $clients, '*', $condition_query);
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //$hidden[] = array('name'=> 'user_id', 'value' => $user_id);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'department', 'value' => 'department');
            //$page["var"][] = array('variable' => 'status', 'value' => 'status');
            //$page["var"][] = array('variable' => 'priority', 'value' => 'priority');
            //$page["var"][] = array('variable' => 'domains', 'value' => 'domains');
            $page["var"][] = array('variable' => 'clients', 'value' => 'clients');
            $page["var"][] = array('variable' => 'step1', 'value' => 'step1');
            $page["var"][] = array('variable' => 'step2', 'value' => 'step2');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'fire-query-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>