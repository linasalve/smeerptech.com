<?php
if ( $perm->has('nc_hr_attm_list') ) {

    
   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $condition_query1 = " LEFT JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id = 
	".TABLE_HR_ATTENDANCE_MINI.".user_id ".$condition_query ; 
    $total	=	HrAttendanceMini::getList( $db, $list, '', $condition_query1);
    
    
 
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    $list	= NULL;
    $fields = TABLE_HR_ATTENDANCE_MINI.'.* ,'.TABLE_USER.".f_name,".TABLE_USER.".l_name";
    HrAttendanceMini::getList( $db, $list, $fields, $condition_query1, $next_record, $rpp);

   /*  $pstatusArr = array_flip($variables['pstatus']);
    $flist= array();
    if(!empty($list)){
        foreach( $list as $key=>$val){      
           
           
          $val['pstatus'] = $pstatusArr[$val['pstatus']];
          $flist[$key]=$val;
        }
    } */
    // Set the Permissions.
    $variables['can_view_list'] = false;
    $variables['can_add'] = false;
    $variables['can_edit'] = false;
     

    if ( $perm->has('nc_hr_attm_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_hr_attm_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_hr_attm_edit') ) {
        $variables['can_edit'] = true;
    }
	
    
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hr-attendance-mini-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
