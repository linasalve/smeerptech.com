<?php
    // include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/bill-receipt.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/currency.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
    
    //get invoice details of order whose data has been updated by checking old_update =1 in orders
    $fields = TABLE_BILL_INV.'.id,'.TABLE_BILL_INV.'.number,'.TABLE_BILL_INV.'.or_no,'.TABLE_BILL_INV.'.client,';
    $fields .=TABLE_BILL_INV.'.currency_id,'.TABLE_BILL_INV.'.currency_abbr,'.TABLE_BILL_INV.'.currency_name,';
    $fields .=TABLE_BILL_INV.'.currency_symbol,'.TABLE_BILL_INV.'.discount,';
    $fields .=TABLE_BILL_INV.'.octroi,'.TABLE_BILL_INV.'.service_tax,'.TABLE_BILL_INV.'.vat,';
    $fields .=TABLE_BILL_INV.'.amount,'.TABLE_BILL_INV.'.amount_inr,'.TABLE_BILL_INV.'.amount_words,';
    $fields .=TABLE_BILL_INV.'.do_c,'.TABLE_BILL_INV.'.do_i,'.TABLE_BILL_INV.'.do_d,';
    $fields .=TABLE_BILL_INV.'.do_e,'.TABLE_BILL_INV.'.do_fe,'.TABLE_BILL_INV.'.balance,';
    $fields .=TABLE_BILL_INV.'.balance_inr,'.TABLE_BILL_INV.'.billing_address,';
    $fields .=TABLE_BILL_INV.'.old_billing_address,'.TABLE_BILL_INV.'.billing_name,';
    $fields .=TABLE_CLIENTS.'.number as client_number,' ;
    $fields .=TABLE_BILL_ORDERS.'.old_updated' ;
    
    
   /*  $query = " SELECT ".$fields." FROM ".TABLE_BILL_INV." LEFT JOIN 
                ".TABLE_BILL_ORDERS." ON ".TABLE_BILL_INV.".or_no = ".TABLE_BILL_ORDERS.".number 
                LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_BILL_INV.".client = ".TABLE_CLIENTS.".user_id 
                WHERE ".TABLE_BILL_ORDERS.".old_updated='1' AND ".TABLE_BILL_INV.".old_updated='0'";
  */  
    if ( $db->query($query) ) {
        if ( $db->nf() > 0 ) {
            while ($db->next_record()) {
               
                echo $data['id'] = $db->f('id');
                echo "<br/>";
                $data['number'] = $db->f('number');
                $data['or_no'] = $db->f('or_no');
               
                $data['currency_abbr'] = $db->f('currency_abbr');
                $data['currency_symbol'] = $db->f('currency_symbol');
                $data['currency_id'] = $db->f('currency_id');
                $data['discount'] = $db->f('discount');
                $data['octroi'] = $db->f('octroi');
                $data['service_tax'] = $db->f('service_tax');
                $data['vat'] = $db->f('vat');
                $data['amount'] = $db->f('amount');
                $data['amount_inr'] = $db->f('amount_inr');
                $data['amount_words'] = $db->f('amount_words');
                $data['do_c'] = $db->f('do_c');
                $data['do_i'] = $db->f('do_i');
                $data['do_d'] = $db->f('do_d');
                $data['do_e'] = $db->f('do_e');
                $data['do_fe'] = $db->f('do_fe');
                $data['balance'] = $db->f('balance');
                $data['balance_inr'] = $db->f('balance_inr');
                $data['billing_name'] = $db->f('billing_name');
                $data['billing_address'] = $db->f('billing_address');
                $data['old_billing_address'] = $db->f('old_billing_address');
                $data['client']['number'] = $db->f('client_number');
                
                if(!empty($data['do_d']) && ($data['do_d'] !='0000-00-00 00:00:00')){
                   // $data['do_d'] = date('Y-m-d H:i:s', $data['do_d']) ;
                }else{
                    $data['do_d'] ='';
                }
                if(!empty($data['do_e']) && ($data['do_e'] !='0000-00-00 00:00:00')){
                    //$data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                }else{
                    $data['do_e'] ='';
                }
                if(!empty($data['do_fe']) && ($data['do_fe'] !='0000-00-00 00:00:00')){
                    //$data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }else{
                    $data['do_fe'] ='';
                }
                    $temp = NULL;
                    $temp_p = NULL;
                    $db1 		= new db_local; // database handle
                    $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
                    Order::getParticulars($db1, $temp, '*', $condition_query);
                    if(!empty($temp)){
                        foreach ( $temp as $pKey => $parti ) {
                           /*if($temp[$pKey]['s_type']=='1'){
                                $temp[$pKey]['s_type']='Years';
                           }elseif($temp[$pKey]['s_type']=='2'){
                                $temp[$pKey]['s_type']='Quantity';
                           }*/
                            $temp_p[]=array(
                                            'p_id'=>$temp[$pKey]['id'],
                                            'particulars'=>$temp[$pKey]['particulars'],
                                            'p_amount' =>$temp[$pKey]['p_amount'] ,                                   
                                            's_type' =>$temp[$pKey]['s_type'] ,                                   
                                            's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                            's_amount' =>$temp[$pKey]['s_amount'] ,                                   
                                            's_id' =>$temp[$pKey]['s_id'] ,                                   
                                            'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                            'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                            'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                            'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                            'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                            'd_amount' =>$temp[$pKey]['d_amount'] ,                                   
                                            'stot_amount' =>$temp[$pKey]['stot_amount'] ,                                   
                                            'tot_amount' =>$temp[$pKey]['tot_amount'] ,                                   
                                            'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                            'pp_amount' =>$temp[$pKey]['pp_amount']                                    
                                            );
                        }
                    }
                    $data['particulars'] = $temp_p ;
                    
                    /*
                    include_once ( DIR_FS_CLASS .'/Region.class.php');
                    $region         = new Region();
                    $region->setAddressOf(TABLE_CLIENTS,  $data['client']['user_id']);
                    $addId = $data['billing_address'] ;
                    $address_list  = $region->get($addId);
                    $data['b_address'] = $address_list ;
                    */
                    
                     // Create the Invoice PDF, HTML in file.
                     
                    $extra = array( 's'         => &$s,
                                    'messages'  => &$messages
                                  );
                    $attch = '';
                   
                    if ( !($attch = Invoice::createMigrateInvoice($data, 'HTML', $extra)) ) {
                       
                        $messages->setErrorMessage("The Invoice file was not created.");
                        
                    }else{
                        $messages->setOkMessage("The Invoice file was created for ".$data['number']);
                        echo $sql=" UPDATE ".TABLE_BILL_INV." SET old_updated ='1' WHERE number='".$data['number']."'" ;
                        echo "<br/>";
                        $db1->query($sql) ;
                        
                    }
               
            }
        }
    }
    print_r($messages);
    
    // 

                   
    //    

?>
