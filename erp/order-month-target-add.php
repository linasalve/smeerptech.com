<?php
// $start['time'] = time();
//echo  $start['microtime'] = microtime();

    if ( $perm->has('nc_ordm_t_add') ) {

        
		
        $_ALL_POST	= NULL;
        $data       = NULL;
       
        
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $_ALL_POST 	= $_POST;
			 
            $data		= processUserData($_ALL_POST);
		 
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                            
                        );
              
                
			if ( OrdMonthTarget::validateAdd($data, $extra) ) {
				
					$temp =null;
					if ( !empty($data['frm_dt']) ) {
						$temp = explode('/', $data['frm_dt']);
						$data['frm_dt'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
					}
					$temp =null;
					if ( !empty($data['to_dt']) ) {
						$temp = explode('/', $data['to_dt']);
						$data['to_dt'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
					}
				  
					$query	= " INSERT INTO ". TABLE_ORDM_TARGET
					." SET ".TABLE_ORDM_TARGET .".target_amount		= '". $data['target_amount'] ."'"
					.", ". TABLE_ORDM_TARGET .".frm_dt			= '". $data['frm_dt'] ."'"
					.", ". TABLE_ORDM_TARGET .".to_dt			= '". $data['to_dt'] ."'"
					.", ". TABLE_ORDM_TARGET .".executive_id			= '". $data['executive_id'] ."'"
					.", ". TABLE_ORDM_TARGET .".executive_details			= '". $data['executive_details'] ."'"
					.", ". TABLE_ORDM_TARGET .".created_by				= '". $my['uid'] ."'"
					.", ". TABLE_ORDM_TARGET .".created_by_name   		= '". $my['f_name']." ".$my['l_name'] ."'"
					.", ". TABLE_ORDM_TARGET .".do_e      				= '". date('Y-m-d H:i:s') ."'"			 ;
		
					if ($db->query($query) && $db->affected_rows() > 0) {
					
						$variables['hid'] = $db->last_inserted_id();					
 					
					 	
					}
				$messages->setOkMessage("The New Record has been added."); 
				$_ALL_POST	= NULL;
				$data		= NULL;
			}
           
        }           
		
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'ajx','value' => $ajx);
            $hidden[] = array('name'=> 'act', 'value' => 'save');


            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'order-month-target-add.html');
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
	//echo  $start['microtime'] = microtime();
?>