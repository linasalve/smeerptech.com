<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
	   

    $lead_details = isset($_GET['lead_details']) ? $_GET['lead_details'] : '';
    if(empty($lead_details)){
		$lead_details = isset($_GET['search_lead_details']) ? $_GET['search_lead_details'] : '';
    }
	
    $optionLink =$stringOpt= '' ;
    if(!empty($lead_details)){
        $sString = $lead_details;
    }
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if (isset($sString) && !empty($sString)){
        header("Content-Type: application/json");      
	    
		
		if ( $perm->has('nc_sl_ld') && $perm->has('nc_sl_ld_list_all') ) {
			/* $query =" SELECT DISTINCT(".TABLE_SALE_LEADS.".lead_id),".TABLE_SALE_LEADS.".f_name,
			".TABLE_SALE_LEADS.".l_name,".TABLE_SALE_LEADS.".company_name FROM ".TABLE_SALE_LEADS." 
			WHERE ( ".TABLE_SALE_LEADS.".f_name LIKE '%".$sString."%' OR ".TABLE_SALE_LEADS.".l_name LIKE 
			'%".$sString."%' OR ".TABLE_SALE_LEADS.".company_name LIKE '%".$sString."%' ) "; */
			
			
			    $query =" SELECT DISTINCT(".TABLE_SALE_LEADS.".lead_id),".TABLE_SALE_LEADS.".f_name,".TABLE_SALE_LEADS.".l_name,".TABLE_SALE_LEADS.".company_name FROM ".TABLE_SALE_LEADS." WHERE ( ".TABLE_SALE_LEADS.".f_name LIKE '%".$sString."%' OR ".TABLE_SALE_LEADS.".l_name LIKE '%".$sString."%' OR ".TABLE_SALE_LEADS.".company_name LIKE '%".$sString."%' ) AND ".TABLE_SALE_LEADS.".status='1'";
			$db->query($query);
			echo "{\"results\": [";
			if ( $db->nf()>0 ) {
			  
				$arr = array();
				while ($db->next_record()){                
					$arr[] = "{\"id\": \"".$db->f('lead_id')."\", \"value\": \"".processSQLData($db->f('f_name'))." ".processSQLData($db->f('l_name'))." (".processSQLData($db->f('company_name')).")".""."\", \"info\": \"\"}";
				}
				echo implode(", ", $arr);
			   
			}
			echo "]}";	 
			 
		}elseif($perm->has('nc_sl_ld') && $perm->has('nc_sl_ld_list')){
			
			$sql_sub='(';
			$my_team_list=array();		 
			$sql_user = "SELECT DISTINCT(user_id) FROM ".TABLE_USER." WHERE ".TABLE_USER.".report_to LIKE '%".trim($my['user_id'])."%' 	AND ".TABLE_USER.".status='".User::ACTIVE."'";
			$db->query($sql_user);
			
			if ( $db->nf()>0 ) {
				$sql_sub .=" " ;
				while ($db->next_record()){
					$user_id = processSQLData($db->f('user_id'));
					$sql_sub .= TABLE_SALE_LEADS.".lead_assign_to LIKE '%".$user_id."%' OR  ".TABLE_SALE_LEADS.".lead_assign_to_mr 
					LIKE '%".$user_id."%' OR ".TABLE_SALE_LEADS.".created_by LIKE '%".$user_id."%' OR " ;
				}
				
			}
			$sql_sub .= TABLE_SALE_LEADS.".lead_assign_to LIKE '%".$my['user_id']."%' OR  
			".TABLE_SALE_LEADS.".lead_assign_to_mr LIKE '%".$my['user_id']."%' OR 
			".TABLE_SALE_LEADS.".created_by LIKE '%".$my['user_id']."%'" ;
			
			$sql_sub .= ")";
			
			$query =" SELECT DISTINCT(".TABLE_SALE_LEADS.".lead_id),".TABLE_SALE_LEADS.".f_name,".TABLE_SALE_LEADS.".l_name,
			 ".TABLE_SALE_LEADS.".company_name FROM ".TABLE_SALE_LEADS." WHERE 
				( ".TABLE_SALE_LEADS.".f_name LIKE '%".$sString."%' OR ".TABLE_SALE_LEADS.".l_name LIKE '%".$sString."%' 
				OR ".TABLE_SALE_LEADS.".company_name LIKE '%".$sString."%' ) AND ".TABLE_SALE_LEADS.".status='1' AND ".$sql_sub;
		
			$db->query($query);
			echo "{\"results\": [";
			if ( $db->nf()>0 ) {
			  
				$arr = array();
				while ($db->next_record()){                
					$arr[] = "{\"id\": \"".$db->f('lead_id')."\", \"value\": \"".processSQLData($db->f('f_name'))."	".processSQLData($db->f('l_name'))." (".processSQLData($db->f('company_name')).")".""."\", \"info\": \"\"}";
				}
				echo implode(", ", $arr);
				
			   
			}
			echo "]}"; 
		
		
		}
		
		
	}
include_once( DIR_FS_NC ."/flush.php");


?>