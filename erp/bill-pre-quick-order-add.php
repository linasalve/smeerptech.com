<?php
   // if ( $perm->has('nc_bl_po_add') ) {
        $_ALL_POST      = NULL;
        $data           = NULL;
         
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'data' 				=> $data,
                            'messages'          => $messages
                        );
            $data['date'] = time();
            
            if ( PreQuickOrder::validateAdd($data, $messages, $extra) ) {
            
                
                 $query	= " INSERT INTO ".TABLE_BILL_PQO
							." SET " 
							."". TABLE_BILL_PQO .".access_level     = '". $data['access_level'] ."'"
							.",". TABLE_BILL_PQO .".created_by       = '". $data['created_by'] ."'"
							.",". TABLE_BILL_PQO .".order_details    = '". $data['order_details'] ."'"
							.",". TABLE_BILL_PQO .".date            = '". date('Y-m-d H:i:s',$data['date']) ."'"
							.",". TABLE_BILL_PQO .".status		    = '". $data['status'] ."'" ;
                
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $messages->setOkMessage("New Pre-Quick Order has been created");
                    $variables['hid'] = $db->last_inserted_id();
                    
                    // Send the notification mails to the concerned persons.
                    /*
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_po_app', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_po_app_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );

                    include ( DIR_FS_INCLUDES .'/user.inc.php');
                    $data['link'] = DIR_WS_NC .'/bill-pre-order.php?perform=view&po_id='. $variables['hid'];
                    $data['creator']= '';
                    $data['date']   = date('d M, Y', $data['do_po']);
                    User::getList($db, $data['creator'], 'username,number,email,f_name,l_name', "WHERE user_id = '". $data['created_by'] ."'");
                    $data['creator'] = $data['creator'][0];

                    foreach ( $users as $user ) {
                        $data['myFname']    = $user['f_name'];
                        $data['myLname']    = $user['l_name'];
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_PO_NEW_MANAGERS', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    } 
                    // Send Email to the Creator of the Pre-Order.
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_PO_NEW', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }*/
                    
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/bill-pre-quick-order-list.php');
        }
        else {
            
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            //$page["var"][] = array('variable' => 'services', 'value' => 'services');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            //$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-pre-quick-order-add.html');
        }
        
    /*}
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }*/
?>