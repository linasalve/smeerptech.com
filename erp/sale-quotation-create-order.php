<?php
if ( $perm->has('nc_sl_ld_order_add') ) {
        
        $q_id        = isset($_GET["q_id"]) ? $_GET["q_id"] : ( isset($_POST["q_id"]) ? $_POST["q_id"] : '' );
        $lead_id        = isset($_GET["lead_id"]) ? $_GET["lead_id"] : ( isset($_POST["lead_id"]) ? $_POST["lead_id"] : '' );
        
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
        include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
        //include ( DIR_FS_INCLUDES .'/followup.inc.php');
        
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        //Check whether client of lead-id exist or not BOF 
         $condition_order = " WHERE ".TABLE_CLIENTS.".lead_id = '".$lead_id."'" ;       
         $clientDetails   = NULL;
         Clients::getList($db, $clientDetails, '*', $condition_order) ;
      
        if(empty($clientDetails)){
        
            //Get details of lead id and push it in the form
           
            $condition_lead = " WHERE ".TABLE_SALE_LEADS.".lead_id = '".$lead_id."'" ;       
            $leadDetails      = NULL;
            Leads::getList($db, $leadDetails, '*', $condition_lead) ;
            $leadDetails = $leadDetails[0];
            $_ALL_POST['title']  = $leadDetails['title'] ;
            $_ALL_POST['f_name'] = $leadDetails['f_name'] ;
            $_ALL_POST['m_name'] = $leadDetails['m_name'] ;
            $_ALL_POST['l_name'] = $leadDetails['l_name'] ;
            $_ALL_POST['email']  = $leadDetails['email'] ;
            $_ALL_POST['username']  = strtolower($leadDetails['f_name']).".".strtolower($leadDetails['l_name']);
           
            $_ALL_POST['lead_id']  = $lead_id ;

        }else{
            $redirectLocation = DIR_WS_NC."/bill-order.php?perform=add&lead_id=".$lead_id."&q_id=".$q_id ;
            header('Location:'. $redirectLocation);
            
        }
        //Check whether client of lead-id exist or not EOF 
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                            
                        );

            if ( Clients::validateInviteClient($data, $extra) ) {
                $data['status'] = Clients::PENDING;
                
                $register_link   = DIR_WS_MP .'/register.php?perform=register&u_id='.$data['user_id'];
                
                // $data['unregister_link']   = DIR_WS_MP .'/register.php?perform=unregister&u_id='. $data['user_id'];
               

                //Generate Followup &lead no bof
                $query	= " INSERT INTO ". TABLE_CLIENTS
                        ." SET ". TABLE_CLIENTS .".user_id     	= '". $data['user_id'] ."'"
                            .",". TABLE_CLIENTS .".number      	= '". $data['number'] ."'"
							.",". TABLE_CLIENTS .".username    	= '". $data['username'] ."'"
                            .",". TABLE_CLIENTS .".password    	= '". encParam($data['password']) ."'"
                            .",". TABLE_CLIENTS .".access_level	= '". $my['access_level'] ."'"
                            .",". TABLE_CLIENTS .".created_by	= '". $my['uid'] ."'"
                            .",". TABLE_CLIENTS .".email       	= '". $data['email'] ."'"
                            .",". TABLE_CLIENTS .".title       	= '". $data['title'] ."'"
                            .",". TABLE_CLIENTS .".f_name      	= '". $data['f_name'] ."'"
                            .",". TABLE_CLIENTS .".m_name      	= '". $data['m_name'] ."'"
                            .",". TABLE_CLIENTS .".l_name      	= '". $data['l_name'] ."'"
                            .",". TABLE_CLIENTS .".lead_id      	= '". $data['lead_id'] ."'"
                            .",". TABLE_CLIENTS .".do_add      	= '". date('Y-m-d H:i:s', time()) ."'"
                            .",". TABLE_CLIENTS .".status      	= '". $data['status'] ."'" ;
                
                $db->query($query);
                //insert into  database 
                
                $query=$query  = " UPDATE ". TABLE_SALE_LEADS
                                    ." SET ". TABLE_SALE_LEADS .".lead_status    	= '". Leads::CONVERTED ."'"
                                    ." WHERE ". TABLE_SALE_LEADS .".lead_id   		= '". $lead_id ."'";
                $db->query($query);
                
                $query="SELECT followup_no FROM ".TABLE_SALE_LEADS." WHERE lead_id= '".$lead_id."'";
                $db->query($query);
                if($db->nf()>0){
                    $db->next_record();
                    $flwnumber=$db->f('followup_no');
                }
                
                $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                        ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"
                            //.",". TABLE_SALE_FOLLOWUP .".title_id 		= '". $data ['title_id'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".remarks 		= ' Invitation has been send to the client '"
                            //.",". TABLE_SALE_FOLLOWUP .".date    		= '". $data['date'] ."'"
                            //.",". TABLE_SALE_FOLLOWUP .".time    		= '". $data ['hrs'].':'.$data ['min'] ."'"
                            //.",". TABLE_SALE_FOLLOWUP .".time_type 		= '". $data ['time_type'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".lead_id    	= '". $lead_id ."'"
                            .",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data['user_id'] ."'"
                            //.",". TABLE_SALE_FOLLOWUP .".assign_to 		= '". $data ['assign_to'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $data['user_id'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".table_name    	= 'clients'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". FOLLOWUP::CLIENT ."'"
                            .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
                
                $db->query($query);
               
                $data["register_link"] 	 = $register_link;
                $name = $data['f_name'] ." ".$data['l_name'] ;
                
                //$data["unregister_link"] = $unregister_link;
                
                // Organize the data to be sent in email.
             

                // Read the Client Manager information.
                $email = NULL;
                if ( getParsedEmail($db, $s, 'EMAIL_MEMBER_INV_ADMIN', $data, $email) ) {
              
                    $to     = '';
                  
                    $to[]   = array('name' => $name , 'email' => $data['email']);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                    
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    $messages->setOkMessage("The Invitation has been sent successfully.");
                    
                }
           
                $_ALL_POST = null;
             
            }      
        }
        else{            
            $_ALL_POST['number'] = Clients::getNewAccNumber($db);            
        }
      
        // Check if the Form to add is to be displayed or the control is to be sent to the List page. 
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/sale-quotation-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'perform','value' => 'create_order');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'lead_id', 'value' => $lead_id);
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'my', 'value' => 'my');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-quotation-create-order.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
     
    
    
    
    
?>