<?php
    if ( $perm->has('nc_ts_delete') || $perm->has('nc_ts_delete_all')) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
        if ( $perm->has('nc_ts_delete_al') ) {
            $access_level += 1;
        }
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        Taskreminder::delete($id, $extra);
        
        // Display the list.
        //include ( DIR_FS_NC .'/task-reminder-list.php');
        include ( DIR_FS_NC .'/task-reminder-search.php');
       
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the task reminder entry.");
    }
?>