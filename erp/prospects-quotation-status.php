<?php
	if ( $perm->has('nc_ps_qt_status') ) {
        $inv_id	= isset($_GET["inv_id"]) ? $_GET["inv_id"] 	: ( isset($_POST["inv_id"]) ? $_POST["inv_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        
        $extra = array( 'db'           		=> &$db,
                        'my' 				=> $my,
						'access_level' 		=> $access_level,
						'access_level_ot' 	=> $access_level_ot,
                        'messages'     		=> &$messages
                    );
        ProspectsQuotation::updateStatus($inv_id, $status, $extra);
        
        $perform = 'search';
        // Display the Default Order
		$searchStr=1; 
		$_SEARCH["chk_status"]  = 'AND';
		$_SEARCH["sStatus"]     = array(ProspectsQuotation::BLOCKED,ProspectsQuotation::PENDING);            
            
        $condition_query = " WHERE (". TABLE_PROSPECTS_QUOTATION .".status = '". ProspectsQuotation::BLOCKED ."' OR ". TABLE_PROSPECTS_QUOTATION .".status = '". ProspectsQuotation::PENDING."') AND ".TABLE_PROSPECTS_QUOTATION.".id = ".$inv_id   ;
        
        include ( DIR_FS_NC .'/prospects-quotation-list.php');        
        
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>