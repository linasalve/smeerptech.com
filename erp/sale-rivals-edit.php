<?php
if ( $perm->has('nc_sl_riv_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/sale-rivals.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Rivals::validateUpdate($data, $extra) ) {
               $query  = " UPDATE ". TABLE_SALE_RIVALS
                            ." SET ". TABLE_SALE_RIVALS .".f_name 				= '".		$data['f_name'] ."'"
                                   	.",". TABLE_SALE_RIVALS .".l_name 			= '". $data['l_name'] ."'"                                    .",". TABLE_SALE_RIVALS .".company_name 	= '". $data['company_name'] ."'"                                    .",". TABLE_SALE_RIVALS .".address 			= '". $data['address'] ."'"                                    .",". TABLE_SALE_RIVALS .".phone 			= '". $data['phone'] ."'"                                    .",". TABLE_SALE_RIVALS .".mobile 			= '". $data['mobile'] ."'"                                	.",". TABLE_SALE_RIVALS .".website_1		= '". $data['website_1'] ."'"                                	.",". TABLE_SALE_RIVALS .".website_2 		= '". $data['website_2'] ."'"                                	.",". TABLE_SALE_RIVALS .".website_3 		= '". $data['website_3'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".name_server_1 	= '". $data['name_server_1'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_1 			= '". $data['ip_1'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".name_server_2 	= '". $data['name_server_2'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_2 			= '". $data['ip_2'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".name_server_3	= '". $data['name_server_3'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_3				= '". $data['ip_3'] ."'"                    

                                	.",". TABLE_SALE_RIVALS .".name_server_4 	= '". $data['name_server_4'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_4				= '". $data['ip_4'] ."'"                    

                                	.",". TABLE_SALE_RIVALS .".name_server_5 	= '". $data['name_server_5'] ."'"                    
                                	.",". TABLE_SALE_RIVALS .".ip_5 			= '". $data['ip_5'] ."'"                    
									.",". TABLE_SALE_RIVALS .".remarks 			= '". $data['remarks'] ."'"                                                   
                            		." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Rivals has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SALE_RIVALS .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SALE_RIVALS .".id = '". $id ."' )";
            
            if ( Rivals::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                
                    // Setup the date of delivery.
                    $_ALL_POST['date']  = explode(' ', $_ALL_POST['date']);
                    $temp               = explode('-', $_ALL_POST['date'][0]);
                    $_ALL_POST['date']  = NULL;
                    $_ALL_POST['date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform = 'list';
            $condition_query='';
            include ( DIR_FS_NC .'/sale-rivals-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-rivals-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
