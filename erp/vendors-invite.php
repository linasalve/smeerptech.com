<?php    
if ( $perm->has('nc_uv_invite') ) {  

    include_once ( DIR_FS_INCLUDES .'/vendors.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
     include ( DIR_FS_INCLUDES .'/followup.inc.php');
     
    if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,                            
                        );

            if ( Vendors::validateInviteClient($data, $extra) ) {
                $data['status'] = Vendors::PENDING;
                
                $register_link   = DIR_WS_MP .'/register.php?perform=register&user_id='.$data['user_id'];
                // $data['unregister_link']   = DIR_WS_MP .'/register.php?perform=unregister&u_id='. $data['user_id'];
                $service_id = ",".Vendors::BILLING.",".Vendors::ST.",";

                //Generate Followup &lead no bof
                $query	= " INSERT INTO ". TABLE_VENDORS
                        ." SET ". TABLE_VENDORS .".user_id     	= '". $data['user_id'] ."'"
                            .",". TABLE_VENDORS .".number      	= '". $data['number'] ."'"
							.",". TABLE_VENDORS .".username    	= '". $data['username'] ."'"
                            .",". TABLE_VENDORS .".password    	= '". encParam($data['password']) ."'"
                            .",". TABLE_VENDORS .".access_level	= '". $my['access_level'] ."'"
                            .",". TABLE_VENDORS .".created_by	= '". $my['uid'] ."'"
                            .",". TABLE_VENDORS .".email       	= '". $data['email'] ."'"
                            .",". TABLE_VENDORS .".title       	= '". $data['title'] ."'"
                            .",". TABLE_VENDORS .".f_name      	= '". $data['f_name'] ."'"
                            .",". TABLE_VENDORS .".m_name      	= '". $data['m_name'] ."'"
                            .",". TABLE_VENDORS .".l_name      	= '". $data['l_name'] ."'"
                            .",". TABLE_VENDORS .".service_id   = '". $service_id ."'"
                            .",". TABLE_VENDORS .".do_add      	= '". date('Y-m-d H:i:s', time()) ."'"
                            .",". TABLE_VENDORS .".status      	= '". $data['status'] ."'" ;
                
                $db->query($query);
                //insert into  database 
                /*
                $query="SELECT followup_no FROM ".TABLE_SALE_FOLLOWUP." WHERE client= '".$data['user_id']."'";
                $db->query($query);
                if($db->nf() > 0){
                    $db->next_record();
                    $flwnumber=$db->f('followup_no');
                }else{
                    $flwnoquery="SELECT MAX(followup_no) as followup_no FROM ".TABLE_SALE_FOLLOWUP;
                    $dbnew=new db_local();
                    $dbnew->query($flwnoquery);
                        
                    if($dbnew ->nf() > 0){
                        $dbnew->next_record();
                        $max_flwnumber=$dbnew->f('followup_no');
                        if(!isset($max_flwnumber)){
                            $flwnumber = FOLLOWUP;
                        }else{
                            $flwnumber = $max_flwnumber + 1;
                        }
                    }
                } 
                $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                        ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"
                            .",". TABLE_SALE_FOLLOWUP .".remarks 		= ' Invitation has been send to the client '"
                            .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data['user_id'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $data['user_id'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".table_name    	= '".TABLE_VENDORS."'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                            .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". FOLLOWUP::CLIENT ."'"
                            .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
                
                $db->query($query);
               */
                $data["register_link"] 	 = $register_link;
                $name = $data['f_name'] ." ".$data['l_name'] ;
                
                //$data["unregister_link"] = $unregister_link;
                
                // Organize the data to be sent in email.
             

                // Read the Client Manager information.
                $email = NULL;
                if ( getParsedEmail($db, $s, 'VENDOR_INVITE', $data, $email) ) {
              
                    $to     = '';                   
                    $to[]   = array('name' => $name, 'email' => $data['email']);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    $messages->setOkMessage("The Invitation has been sent successfully.");
                   
                }
                $_ALL_POST = null;             
            }      
        }
        else{            
            $_ALL_POST['number'] = Vendors::getNewAccNumber($db);            
        }
      
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/vendors.php?perform=invite&sendinv=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/vendors.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/vendors.php?sendinv=1");   
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'perform','value' => 'invite');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'my', 'value' => 'my');
            //$page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-invite.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>