<?php

    if ( $perm->has('nc_myprofile_password') ) {
        
        include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
        $user_id = $my['user_id'];
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        $access_level   += 1;
        
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=12;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        }        
       $_ALL_POST['date'] = date('d/m/Y');
        
        $lst_typ =array('1'=>'AM','2'=>'PM');
        
        
        if ( (isset($_POST['btnCreate']) ) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages
                        );
            
            if ( Myprofile::validateTimesheet($data, $extra) ) {
                
                /*
                $st_time=$data['st_hr'].":".$data['st_min'] ;
                $ed_time=$data['ed_hr'].":".$data['ed_min'] ;
                
                if($data['st_type']==2){
                    $st_hr = $data['st_hr'] + 12;
                }else{
                    $st_hr=$data['st_hr'];
                }
                if($data['ed_type']==2) {
                    $ed_hr = $data['ed_hr'] + 12;
                }else{
                    $ed_hr = $data['ed_hr'];
                }
                
                //calculate total hrs
                $total_hrs = '';
                $hrs =( mktime($ed_hr,$data['ed_min'],0,date('d'),date('m'),date('Y')) - mktime($st_hr,$data['st_min'],0,date('d'),date('m'),date('Y')) )  ;
                $total_hrs = $hrs / (60*60);
                $date =date('Y-m-d');
                
                $query  = " INSERT INTO ". TABLE_USER_TIMESHEET
                            ." SET ". TABLE_USER_TIMESHEET .".order_id = '".     $data['order_id'] ."'"
                            .",". TABLE_USER_TIMESHEET .".user_id = '".          $my['user_id'] ."'"
                            .",". TABLE_USER_TIMESHEET .".st_time = '".          $st_time ."'"
                            .",". TABLE_USER_TIMESHEET .".st_type = '".          $data['st_type'] ."'"
                            .",". TABLE_USER_TIMESHEET .".ed_time = '".          $ed_time ."'" 
                            .",". TABLE_USER_TIMESHEET .".ed_type = '".          $data['ed_type'] ."'" 
                            .",". TABLE_USER_TIMESHEET .".date = '".             $date."'" 
                            .",". TABLE_USER_TIMESHEET .".total_hrs = '".        $total_hrs ."'" 
                            .",". TABLE_USER_TIMESHEET .".note = '".             $data['note'] ."'" ;
                */
               
                //$date =date('Y-m-d');
                if ( isset($data['date']) && !empty($data['date']) ) {
                     $data['date'] = explode('/', $data['date']);
                     $data['date'] =$data['date'][2]."-".$data['date'][1]."-".$data['date'][0] ;
                }
                
                
                $query  = " INSERT INTO ". TABLE_USER_TIMESHEET
                            ." SET ". TABLE_USER_TIMESHEET .".order_id = '".     $data['order_id'] ."'"
                            .",". TABLE_USER_TIMESHEET .".user_id = '".          $my['user_id'] ."'"
                            .",". TABLE_USER_TIMESHEET .".hrs = '".              $data['hrs'] ."'"
                            .",". TABLE_USER_TIMESHEET .".min = '".              $data['min'] ."'" 
                            .",". TABLE_USER_TIMESHEET .".date = '".             $data['date']."'" 
                            //.",". TABLE_USER_TIMESHEET .".date = '".             $date."'" 
                            .",". TABLE_USER_TIMESHEET .".entry_date= '".        date('Y-m-d H:i:s',time()) ."'"
                            .",". TABLE_USER_TIMESHEET .".note = '".             $data['note'] ."'" ;
                if ( $db->query($query)) {                 

                    $messages->setOkMessage("Timesheet added successfully.");
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $_ALL_POST['date'] = date('d/m/Y');
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Unable to add record.');
                }
            }
        }

       
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $_POST=null;
            $date='';
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/my-timesheet.php');
        }
        else {
            // BO: Read the Team Orders list
            $condition_query = " WHERE ". TABLE_BILL_ORDERS .".status = '". Order::ACTIVE ."'";
            // If the Order is created by the my.
            $access_level   = $my['access_level'];
            if ( $perm->has('nc_bl_or_list_al') ) {
                $access_level += 1;
            }
            
            $condition_query .= " AND ( ";
            // If my has created this Order.
            $condition_query .= " (". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // If my is present in the Team.
            /*
            $condition_query .= " OR ( ( "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
                                        . " ) "
                                        ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            */
             $condition_query .= " OR ( ( "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
                                        . " )  ) ";
       
            // If my is the Client Manager
            $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // Check if the User has the Right to view Orders created by other Users.
            /*
            if ( $perm->has('nc_bl_or_list_ot') ) {
               $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_bl_or_list_ot_al') ) {
                   $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_BILL_ORDERS. ".created_by != '". $my['user_id'] ."' "
                                  ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level_o ) ";
            }*/
            $condition_query .= " )";
            
           
            // If you want to check my is present in team or not then remove comment but comment above code
            /*$condition_query .= " AND  ( "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
                                        . " ) OR ".TABLE_BILL_ORDERS .".access_level < $access_level " ;
            
            */
            
            $condition_query .= " ORDER BY ". TABLE_BILL_ORDERS .".do_d DESC ";
            $fields = TABLE_BILL_ORDERS.".id, ".TABLE_BILL_ORDERS.".order_title";
            // To count total projects records.
            $lst_project	= 	NULL;
            $total	=	Order::getOrderList( $db, $lst_project, $fields , $condition_query);
                
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'addtimesheet');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'lst_project', 'value' => 'lst_project');
            $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'lst_typ', 'value' => 'lst_typ');

            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'my-timesheet-add.html');
             
        }
    }
    else {
        $messages->setErrorMessage("You do not have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>