<?php
	if ( $perm->has('nc_ps_qt_cr_srv_pdf') ) {
		//include_once ( DIR_FS_INCLUDES .'/currency.inc.php');      
		include_once ( DIR_FS_INCLUDES .'/prospects.inc.php' );
		include_once ( DIR_FS_INCLUDES .'/financial-year.inc.php' );
		include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php' );
		include_once ( DIR_FS_INCLUDES .'/prospects-quotation.inc.php');
	   
		$data['filename'] = 'smeerp-services';
		$data['service-pdf']=1;
		$data['company_name'] = 'SMEERP E-Technologies Pvt. Ltd.';
		ProspectsQuotation::createPdfFile($data);
		$file_name =  $data["filename"] .".pdf";
		$file_path = DIR_FS_PST_QUOT_PDF."/" ;
		$content_type = 'application/pdf';
		
		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers 
		header("Content-type: $content_type");
		header("Content-Disposition: attachment; filename=". $file_name );
		header("Content-Length: ".filesize($file_path.$file_name));
		readfile($file_path.$file_name);
		exit;
	}   
?>