<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/reports.inc.php' );
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    $access_level   = $my['access_level'];
    $access_level = $access_level+1;
    $_ALL_POST=array();
    
    // check rt for view reports
   if ( $perm->has('nc_rp') ) { 
   
              
        // by default display search block        
        $_SEARCH["searched"]    = true ;
        //to view report project id should be compulsary
        $list =$teamList =array();
        $totalHrsArr='';
        
        //use switch case here to perform action. 
        switch ($perform) {
            case ('project-wise'): {
               
                include(DIR_FS_NC."/reports-project-wise.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('executive-wise'): {
               
                include(DIR_FS_NC."/reports-executive-wise.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('month-att'): {
               
                include(DIR_FS_NC."/reports-monthly-attendance.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('exec-month-att'): {
               
                include(DIR_FS_NC."/reports-executive-months-att.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('day-wise'): {
               
                include(DIR_FS_NC."/reports-day-wise.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('task-reminder'): {
               
                include(DIR_FS_NC."/reports-task-reminder.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('executive-score'): {
               
                include(DIR_FS_NC."/reports-executive-score.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('list'):
            default: {
                include (DIR_FS_NC .'/reports-project-wise.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module .");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'reports.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>