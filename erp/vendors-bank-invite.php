<?php    
if ( $perm->has('nc_vbk_invite') ) {  

    include_once ( DIR_FS_INCLUDES .'/vendors-bank.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
     include ( DIR_FS_INCLUDES .'/followup.inc.php');
     
    if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,                            
                        );

            if ( VendorsBank::validateInviteClient($data, $extra) ) {
                $data['status'] = VendorsBank::PENDING;
                
                $register_link   = DIR_WS_MP .'/register.php?perform=register&user_id='.$data['user_id'];
                // $data['unregister_link']   = DIR_WS_MP .'/register.php?perform=unregister&u_id='. $data['user_id'];
                $service_id = ",".VendorsBank::BILLING.",".VendorsBank::ST.",";

                //Generate Followup &lead no bof
                $query	= " INSERT INTO ". TABLE_VENDORS_BANK
                        ." SET ". TABLE_VENDORS_BANK .".user_id     	= '". $data['user_id'] ."'"
                            .",". TABLE_VENDORS_BANK .".number      	= '". $data['number'] ."'"
							.",". TABLE_VENDORS_BANK .".username    	= '". $data['username'] ."'"
                            .",". TABLE_VENDORS_BANK .".password    	= '". encParam($data['password']) ."'"
                            .",". TABLE_VENDORS_BANK .".access_level	= '". $my['access_level'] ."'"
                            .",". TABLE_VENDORS_BANK .".created_by	= '". $my['uid'] ."'"
                            .",". TABLE_VENDORS_BANK .".email       	= '". $data['email'] ."'"
                            .",". TABLE_VENDORS_BANK .".title       	= '". $data['title'] ."'"
                            .",". TABLE_VENDORS_BANK .".f_name      	= '". $data['f_name'] ."'"
                            .",". TABLE_VENDORS_BANK .".m_name      	= '". $data['m_name'] ."'"
                            .",". TABLE_VENDORS_BANK .".l_name      	= '". $data['l_name'] ."'"
                            .",". TABLE_VENDORS_BANK .".service_id   = '". $service_id ."'"
                            .",". TABLE_VENDORS_BANK .".do_add      	= '". date('Y-m-d H:i:s', time()) ."'"
                            .",". TABLE_VENDORS_BANK .".status      	= '". $data['status'] ."'" ;
                
                $db->query($query);
                //insert into  database 
               
                $data["register_link"] 	 = $register_link;
                $name = $data['f_name'] ." ".$data['l_name'] ;
                
                //$data["unregister_link"] = $unregister_link;
                
                // Organize the data to be sent in email.
             

                // Read the Client Manager information.
                $email = NULL;
                if ( getParsedEmail($db, $s, 'VENDOR_BANK_INVITE', $data, $email) ) {
              
                    $to     = '';                   
                    $to[]   = array('name' => $name, 'email' => $data['email']);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    $messages->setOkMessage("The Invitation has been sent successfully.");
                   
                }
                $_ALL_POST = null;             
            }      
        }
        else{            
            $_ALL_POST['number'] = VendorsBank::getNewAccNumber($db);            
        }
      
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/vendors-bank.php?perform=invite&sendinv=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/vendors-bank.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/vendors-bank.php?sendinv=1");   
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'perform','value' => 'invite');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'my', 'value' => 'my');
            //$page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-bank-invite.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>