<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN')){
		require("../lib/config.php");
	}    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));    
    include_once(DIR_FS_NC ."/header.php");
    include_once(DIR_FS_INCLUDES .'/project-task.inc.php');
    include_once(DIR_FS_INCLUDES .'/bill-order.inc.php');
	//include_once(DIR_FS_INCLUDES .'/project-status.inc.php');
	include_once(DIR_FS_INCLUDES .'/clients.inc.php'); 
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0    
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : 'list' );
	$or_id 		= isset($_GET["or_id"])     ? $_GET["or_id"]        : ( isset($_POST["or_id"])          ? $_POST["or_id"]       :'');
	$client_id 	= isset($_GET["client_id"]) ? $_GET["client_id"]        : ( isset($_POST["client_id"])          ? $_POST["client_id"]       :'');
	$id 		 = isset($_GET["id"])       ? $_GET["id"]        : ( isset($_POST["id"])          ? $_POST["id"]       :'');
    $added 			= isset($_GET["added"]) ? $_GET["added"]  : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $flw_added 			= isset($_GET["flw_added"]) ? $_GET["flw_added"]  : ( isset($_POST["flw_added"])          ? $_POST["flw_added"]       :'0');
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    //$xd 			= isset($_GET["xd"])    ? $_GET["xd"]        : ( isset($_POST["xd"])          ? $_POST["xd"]       :'');
	$rpp = isset($_GET["rpp"]) ? $_GET["rpp"]:( isset($_POST["rpp"]) ? $_POST["rpp"]:( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    // ie by default every one can show applied ( * by factor) value timesteet values   
	if ( $perm->has('nc_p_ts_default_mult') ) { //show default multiplied hrs		 
		$actual_time 	= isset($_GET["actual_time"])       ? $_GET["actual_time"]      : ( isset($_POST["actual_time"])        ? $_POST["actual_time"]     : 0);
	}else{	 
		$actual_time 	= isset($_GET["actual_time"])       ? $_GET["actual_time"]      : ( isset($_POST["actual_time"])        ? $_POST["actual_time"]     : 1);
	}
	// ie by default every one can show Applied ( * by factor) value timesteet values
    //If order id is not set the redirect on home page bof
	
     
     
    $condition_url='';
    $team_members='';
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
       
    }
    $condition_query ='';
   
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    } else {
        $next_record    = ($x-1) * $rpp;
    }
    
    $variables["x"]     = $x;
    //$variables["xd"]     = $xd;
    $variables["rpp"]   = $rpp; 
    
    $lst_submodule=null;
    //get order details BOF
    $order = NULL;
	
     	
    // Retrieve the Members of the Order using the Order ID.
    $statusArr	= NULL;
   
    $statusArr	= ProjectTask::getStatus();
	
	
    $variables['can_view_cost'] = false;
	$variables['can_send_comment'] = false;
	
	if ( $perm->has('nc_p_ts_sendmail') ) {
        $variables['can_send_comment'] = true;
    }
	
    if ( $perm->has('nc_p_ts_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_p_ts_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_p_ts_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_p_ts_delete') ) {
        $variables['can_delete'] = true;
    }    
    if ( $perm->has('nc_p_ts_details') ) {
        $variables['can_view_details'] = true;
    }    
    if ( $perm->has('nc_p_ts_cost') ) {
        $variables['can_view_cost'] = true;
    }
    if ( $perm->has('nc_p_tsb_add') ) {
        $variables['can_add_task_bug'] = true;
    }
    if ( $perm->has('nc_p_tsb_list') ) {
        $variables['can_view_list_bug'] = true;
    }
    //$variables['can_add_task_bug'] = true;      
    //To show actual total timesheet ie ( timesheet * 3)
    if ( $perm->has('nc_p_ts_ac_tot') ) {
        $variables['can_view_actual_tot_time'] = true;
    }
    
   if ( $perm->has('nc_pts_mod_list') ) {
        $variables['can_view_module']     = true;
    }
    if ( $perm->has('nc_pts_mod_add') ) {
        $variables['can_add_module']     = true;
    }
     
    if ( $perm->has('nc_st') && $perm->has('nc_st_flw_inv') ) {
            $variables['can_add_followup'] = true;
    }    
    if ( $perm->has('nc_p_ts')) {
//    if ( $perm->has('nc_p_ts') && $order['status']==Order::ACTIVE ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
									TABLE_BILL_ORDERS   =>  array(
                                                                'Order Title'           => 'order_title',
                                                                'Order no.'         => 'number'         
                                                            ),
                                    TABLE_PROJECT_TASK_DETAILS   =>  array(
                                                                'Title'           => 'title',
                                                                'Task id'           => 'id',
                                                                'Details'         => 'details',         
                                                                'Added By'         => 'added_by_name',  
                                                                'Task comment By' => 'last_comment_by_name'         
                                                            ),
                                    TABLE_PROJECT_TASK_MODULE   =>  array(
                                                                'Module Name'    => 'title-project_task_module'
                                                            ),
                                );
        
        $sOrderByArray  = array(
                                TABLE_PROJECT_TASK_DETAILS => array(                           
																'Date'         => 'do_e',    
                                                                'Title'    => 'title',
                                                                'Details'  => 'details'                                               
                                                            ),
                                );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_PROJECT_TASK_DETAILS;
        }
        $where_added = true ;
      
        //use switch case here to perform action. 
        switch ($perform) {            
            case ('search'): {            	
                include(DIR_FS_NC."/project-task-all-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task-all.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }        
            case ('list'):
            default: {            	
                include (DIR_FS_NC .'/project-task-all-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'project-task-all.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        //$messages->setErrorMessage("You donot have the Right to Access this Module.");        
    
		if($order['status']!=Order::ACTIVE){
			$messages->setErrorMessage("You donot view the tasks as this Order is not ACTIVE.<br/>
			Please contact to your Project Manager");
		}else{
			$messages->setErrorMessage("You donot have the Right to Access this Module.");
		}
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task-all.html');
        if($ajx==0){
			$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
		} 
    } 
    $project_id = $or_id;
    // always assign
    $s->assign("variables", $variables);
    $s->assign("actual_time", $actual_time);
  //  $s->assign("or_id", $or_id);
  //  $s->assign("project_id", $project_id);
   // $s->assign("order", $order);
    $s->assign("statusArr", $statusArr);
   // $s->assign("team_members", $team_members);
   // $s->assign("clients_su_members", $clients_su_members);

    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
       
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>