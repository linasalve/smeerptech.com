<?php
if ( $perm->has('nc_p_pb_list') ) {
	include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
	
	if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
	if(empty($condition_query)){
		$_SEARCH["chk_status"]  = 'AND';
		$_SEARCH["sStatus"]    = array(PaymentPartyBills::ACTIVE);
		$_SEARCH["chk_fstatus"]  = 'AND';
		$_SEARCH["fStatus"]    = array(PaymentPartyBills::FORCEDEACTIVE);
		$_SEARCH["chk_bstatus"]  = 'AND';
		$_SEARCH["bStatus"] = array(PaymentPartyBills::BILL_STATUS_UNPAID,PaymentPartyBills::BILL_STATUS_PARTIALPAID); 
		$where_added=true;
		$condition_query = " WHERE ".TABLE_PAYMENT_PARTY_BILLS .".f_status='". PaymentPartyBills::FORCEDEACTIVE ."'
		AND ".TABLE_PAYMENT_PARTY_BILLS .".bill_status IN('".PaymentPartyBills::BILL_STATUS_UNPAID ."',
		'".PaymentPartyBills::BILL_STATUS_PARTIALPAID ."') AND ".TABLE_PAYMENT_PARTY_BILLS .".status IN('".PaymentPartyBills::ACTIVE ."')";
		
		
		$perform = 'search';
        $condition_url   = "&chk_fstatus=".$_SEARCH["chk_fstatus"]."&fStatus=".PaymentPartyBills::FORCEDEACTIVE."&
		chk_bstatus=".$_SEARCH["chk_bstatus"]."&bStatus=".PaymentPartyBills::BILL_STATUS_UNPAID.","
		.PaymentPartyBills::BILL_STATUS_PARTIALPAID."&chk_status=".$_SEARCH["chk_status"]."&
		sStatus=".PaymentPartyBills::ACTIVE;   
	}
	
	if ($perm->has('nc_uc_tran_purchase_classified') ) {
			  
	}else{			 
		if( $where_added ) {
			$condition_query .= " AND ";
		}else{
			$condition_query.= ' WHERE ';
			$where_added    = true;
		} 
		$condition_query .= "  (".TABLE_CLIENTS .".is_tran_purchase_classified ='".Clients::CLASSIFIED_NO ."' )" ;     
	}
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
   
    //echo $condition_query;
	$totalAmt =array();
	$totalAmount=$totalAmountPerbill=$totalBalanceAmount='';
	$fields=" SUM(".TABLE_PAYMENT_PARTY_BILLS.".bill_amount) as totalAmount,
	SUM(".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_perbill) as totalAmountPerbill,
	SUM(".TABLE_PAYMENT_PARTY_BILLS.".bill_amount_adjs) as totalAmountAdjsbill,
	SUM(".TABLE_PAYMENT_PARTY_BILLS.".balance_inr) as totalBalanceAmount ";
	PaymentPartyBills::getList($db, $totalAmt, $fields, $condition_query);
	if(!empty($totalAmt)){
		$totalAmount = $totalAmt[0]['totalAmount'];
		$totalAmountPerbill = $totalAmt[0]['totalAmountPerbill'];
		$totalBalanceAmount = $totalAmt[0]['totalBalanceAmount'];
		$totalAmountAdjsbill = $totalAmt[0]['totalAmountAdjsbill']; 
	}
	
	
	
	
    // To count total records.
    $list	= 	NULL;
    $total	=	PaymentPartyBills::getList( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    //$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    $pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','payment-party-bills.php','frmSearch');
    $list	= NULL;
    $fields = TABLE_PAYMENT_PARTY_BILLS.'.*,'.TABLE_CLIENTS.'.f_name as mfname,'.TABLE_CLIENTS.'.l_name as mlname,
	'.TABLE_CLIENTS.'.billing_name as mbilling_name, ';
	$fields .= TABLE_VENDORS_BANK.".f_name as vfname,".TABLE_VENDORS_BANK.".l_name as vlname,
	".TABLE_VENDORS_BANK.'.billing_name as vbilling_name, ' ;
	$fields .= "team_table.f_name as tfname,team_table.l_name as tlname," ;
	$fields .= TABLE_USER.".f_name as ufname,".TABLE_USER.".l_name as ulname," ;
    $fields .= TABLE_PAYMENT_BILLS_AGAINST.".bill_against" ;
    PaymentPartyBills::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    $flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
            $fields1 =  TABLE_SETTINGS_COMPANY .'.name as company_name';                
            $condition2 = " WHERE ".TABLE_SETTINGS_COMPANY .".id=".$val['company_id']." " ;
            $company_name='';
            $table =TABLE_SETTINGS_COMPANY ;              
            $compArr = getRecord($table,$fields1,$condition2);
            if(!empty($compArr)){
                $company_name = $compArr['company_name']  ;   
            }
			$val['company_name']=$company_name ;
			
			$fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';                
            $condition2 = " WHERE ".TABLE_USER .".user_id IN('".$val['created_by']."') " ;
           
            $table =TABLE_USER ;              
            $createdArr = getRecord($table,$fields1,$condition2);
            if(!empty($createdArr)){
                $createdBy = $createdArr['f_name']." ".$createdArr['l_name'] ;   
            }
            $val['added_by']=$createdBy ;
			$val['attachment_imap_arr'] = array();
			if(!empty($val['attachment_imap'])){
				$val['attachment_imap_arr']=explode(",",$val['attachment_imap']);
			}
			$val['bill_status_name']='';
			$val['type_name']='';
			 
			if($val['bill_status']== PaymentPartyBills::BILL_STATUS_UNPAID){
				$val['bill_status_name']='Unpaid';
			}elseif($val['bill_status']== PaymentPartyBills::BILL_STATUS_PAID){
				$val['bill_status_name']='Paid';			
			}elseif($val['bill_status']== PaymentPartyBills::BILL_STATUS_PARTIALPAID){
				$val['bill_status_name']='Partly Paid';
			}
			if($val['type']== PaymentPartyBills::BILL){
				$val['type_name']='Bill';
			}elseif($val['type_name']== PaymentPartyBills::BILL_OTHER){
				$val['type_name']='No Bill';			
			}elseif($val['type_name']== PaymentPartyBills::BILL_SALARY){
				$val['type_name']= 'Salary';
			} 
            $flist[$key]=$val;
            
        }
    }
    
    // Set the Permissions.
   
    if ( $perm->has('nc_p_pb_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_p_pb_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_p_pb_edit') ) {
        $variables['can_edit'] = true;
    }
	if ( $perm->has('nc_p_pb_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_p_pb_view') ) {
        $variables['can_view'] = true;
    }
    
    if ( $perm->has('nc_p_pb_status') ) {
        $variables['can_change_status']     = true;
    }
	//can view list of payment party bills transactions
	$variables['can_view_pbt_list']=false;
	if ( $perm->has('nc_p_pbt_list') ) {
        $variables['can_view_pbt_list'] = true;
    }
	 
    $variables['bills_path'] = DIR_WS_BILLS_FILES;
	
	include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
	$tax_list    = NULL;
	$condition_queryst= " WHERE status = '".ACTIVE ."' AND parent_id=0 ORDER BY id ASC";
	ServiceTax::getList($db, $tax_list, 'id,tax_name', $condition_queryst);
	
    $page["var"][] = array('variable' => 'list', 'value' => 'flist');
	$page["var"][] = array('variable' => 'tax_list', 'value' => 'tax_list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');	
	$page["var"][] = array('variable' => 'total', 'value' => 'total');
	$page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount'); 
	$page["var"][] = array('variable' => 'totalAmountAdjsbill', 'value' => 'totalAmountAdjsbill');
	$page["var"][] = array('variable' => 'totalBalanceAmount', 'value' => 'totalBalanceAmount');
	$page["var"][] = array('variable' => 'totalAmountPerbill', 'value' => 'totalAmountPerbill');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-party-bills-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
