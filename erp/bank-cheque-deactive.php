<?php
    if ( $perm->has('nc_bk_chq_status') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
                
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        BankCheque::deactive($id, $extra);
        $searchStr=1;
        // Display the list.
        include ( DIR_FS_NC .'/bank-cheque-list.php');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Access this module.");
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
