<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");  
	$id =  isset($_GET["id"])   ? $_GET["id"]  : ( isset($_POST["id"])    ? $_POST["id"] : '' );
	$task_cid =  isset($_GET["task_cid"]) ? $_GET["task_cid"]  : ( isset($_POST["task_cid"]) ?  $_POST["task_cid"] : '' );
	$status = isset($_GET["status"])   ? $_GET["status"]  : ( isset($_POST["status"])    ? $_POST["status"] : '' );
	$action = isset($_GET["action"])   ? $_GET["action"]  : ( isset($_POST["action"])    ? $_POST["action"] : '' );
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new = new db_local; // database handle
	
	
	$fields = TABLE_TASK_REMINDER .'.*,'.TABLE_USER.'.f_name,'.TABLE_USER.'.l_name';            
    $condition_query = " LEFT JOIN ". TABLE_USER ." ON ".TABLE_USER.".user_id =".TABLE_TASK_REMINDER.".created_by                                WHERE (". TABLE_TASK_REMINDER .".id = '". $id ."' )";
	Taskreminder::getDetails($db, $_ALL_POSTDATA, $fields, $condition_query); 
    $_ALL_POSTDATA = $_ALL_POSTDATA['0'];
	
	if( $action=='acklge' && $id>0 ){
		
		$_ALL_POSTDATA['acknowledge_task']=$_ALL_POSTDATA['acknowledge_task_cmnt'] = true;
		$_ALL_POSTDATA['bold_class']='';
		$date = date('d M Y  h:i:s  a ');
		$tempA = $tempB = $tempC = array();
		$tempA = explode(',',trim($_ALL_POSTDATA['allotted_to'],','));
		if(!empty($_ALL_POSTDATA['acknowledge_by'])){
			$tempB = explode(',',trim($_ALL_POSTDATA['acknowledge_by'],','));
		}
		if(!empty($_ALL_POSTDATA['last_comment_acknowledge_by'])){
			$tempC = explode(',',trim($_ALL_POSTDATA['last_comment_acknowledge_by'],','));
		}
		//task alloted to me and I did not acknowledge it then show acknowledge-me link
		if( in_array($my['user_id'],$tempA,true) && !in_array($my['user_id'],$tempB,true) ){
			$_ALL_POSTDATA['acknowledge_task'] = false;
			$_ALL_POSTDATA['bold_class']=' b ';
		}
		if(!empty($_ALL_POSTDATA['last_comment_id'])){
			if( in_array($my['user_id'],$tempA,true) && !in_array($my['user_id'],$tempC,true) ){
				$_ALL_POSTDATA['acknowledge_task_cmnt'] = false;
				$_ALL_POSTDATA['bold_class']=' b ';
			}
		}
		
		if($id>0 && $task_cid==0){ 
			if($_ALL_POSTDATA['acknowledge_task']==false){
				$sql = " UPDATE ".TABLE_TASK_REMINDER." SET ".TABLE_TASK_REMINDER.".acknowledge_by = CONCAT(".TABLE_TASK_REMINDER.".acknowledge_by,',".$my['user_id']."'), ".TABLE_TASK_REMINDER.".acknowledge_by_name = CONCAT(".TABLE_TASK_REMINDER.".acknowledge_by_name,',".$my['f_name']." ".$my['l_name']."(".$my['number'].")'), ".TABLE_TASK_REMINDER.".acknowledge_details = CONCAT(".TABLE_TASK_REMINDER.".acknowledge_details,',".$my['f_name']." ".$my['l_name']." (".$my['number'].") ".$date." <br/>'),".TABLE_TASK_REMINDER.".last_comment_acknowledge_by = CONCAT(".TABLE_TASK_REMINDER.".last_comment_acknowledge_by,',".$my['user_id']."')
				WHERE ".TABLE_TASK_REMINDER.".id ='".$id."'"; 
				$execute = $db_new->query($sql);
			}
			$text = 'You acknowledge the task successfully';
		}  
		if($id>0 && $task_cid > 0){
			if($_ALL_POSTDATA['acknowledge_task_cmnt']==false){
				$sql = " UPDATE ".TABLE_TASK_REMINDER." SET ".TABLE_TASK_REMINDER.".last_comment_acknowledge_by = CONCAT(".TABLE_TASK_REMINDER.".last_comment_acknowledge_by,',".$my['user_id']."') WHERE ".TABLE_TASK_REMINDER.".id ='".$id."'";
				$execute = $db_new->query($sql); 
				
				$sql = " UPDATE ".TABLE_TASK_REMINDER_LOG." SET ".TABLE_TASK_REMINDER_LOG.".acknowledge_by = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_by,',".$my['user_id']."'), ".TABLE_TASK_REMINDER_LOG.".acknowledge_by_name = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_by_name,',".$my['f_name']." ".$my['l_name']."(".$my['number'].")'), ".TABLE_TASK_REMINDER_LOG.".acknowledge_details = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_details,',".$my['f_name']." ".$my['l_name']." (".$my['number'].") ".$date." <br/>') WHERE ".TABLE_TASK_REMINDER_LOG.".id ='".$task_cid."'"; 
				$execute = $db_new->query($sql); 
			}
			$text = 'You acknowledge the task comment successfully';
		}
		
		$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">".$text."</li>
							</ul>
						</td>
					</tr>
					</table>"; 
	}
	
	if( $action=='acklge_c' && $task_cid>0 ){
	
		$_ALL_POSTDATA['acknowledge_task_cmnt'] =$_ALL_POSTDATA['acknowledge_tasklast_cmnt']= true;
		$_ALL_POSTDATA['bold_class']='';
		$date = date('d M Y  h:i:s  a ');
		$tempA = $tempB = $tempC = array();
		$tempA = explode(',',trim($_ALL_POSTDATA['allotted_to'],','));
		if(!empty($_ALL_POSTDATA['acknowledge_by'])){
			$tempB = explode(',',trim($_ALL_POSTDATA['acknowledge_by'],','));
		}
		if(!empty($_ALL_POSTDATA['last_comment_acknowledge_by'])){
			$tempC = explode(',',trim($_ALL_POSTDATA['last_comment_acknowledge_by'],','));
		}
	
		if(!empty($_ALL_POSTDATA['last_comment_id']) && ($_ALL_POSTDATA['last_comment_id'] == $task_cid)){			
			if( in_array($my['user_id'],$tempA,true) && !in_array($my['user_id'],$tempC,true) ){
				$_ALL_POSTDATA['acknowledge_tasklast_cmnt'] = false;
				$_ALL_POSTDATA['bold_class']=' b ';
			}
		}
		
		if($id>0 && $task_cid > 0){
			if($_ALL_POSTDATA['acknowledge_tasklast_cmnt']==false){ 
				$sql = " UPDATE ".TABLE_TASK_REMINDER." SET ".TABLE_TASK_REMINDER.".last_comment_acknowledge_by = CONCAT(".TABLE_TASK_REMINDER.".last_comment_acknowledge_by,',".$my['user_id']."') WHERE ".TABLE_TASK_REMINDER.".id ='".$id."'";
				$execute = $db_new->query($sql); 
				
				$sql = " UPDATE ".TABLE_TASK_REMINDER_LOG." SET ".TABLE_TASK_REMINDER_LOG.".acknowledge_by = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_by,',".$my['user_id']."'), ".TABLE_TASK_REMINDER_LOG.".acknowledge_by_name = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_by_name,',".$my['f_name']." ".$my['l_name']."(".$my['number'].")'), ".TABLE_TASK_REMINDER_LOG.".acknowledge_details = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_details,',".$my['f_name']." ".$my['l_name']." (".$my['number'].") ".$date." <br/>') WHERE ".TABLE_TASK_REMINDER_LOG.".id ='".$task_cid."'"; 
				$execute = $db_new->query($sql); 
			}
			//Get details of comment 
			$condition_query2 = " WHERE ".TABLE_TASK_REMINDER_LOG.".id='".$task_cid."'";
			Taskreminder::getDetailsCommLog( $db_new, $cData, "*", $condition_query2);
			if(!empty($cData)){
				$cData = $cData[0]; 
				if(!empty($cData['acknowledge_by'])){
					$tempD = explode(',',trim($cData['acknowledge_by'],','));
				}
				 
				if(!empty($cData['last_comment_id']) && ($cData['last_comment_id'] == $task_cid)){ 
					
					if( in_array($my['user_id'],$tempA,true) && !in_array($my['user_id'],$tempD,true) ){
						$cData['acknowledge_task_cmnt'] = false;
						
					}
				}
				 
				 
				if($cData['acknowledge_task_cmnt']==false){   
					$sql = " UPDATE ".TABLE_TASK_REMINDER_LOG." SET ".TABLE_TASK_REMINDER_LOG.".acknowledge_by = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_by,',".$my['user_id']."'), ".TABLE_TASK_REMINDER_LOG.".acknowledge_by_name = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_by_name,',".$my['f_name']." ".$my['l_name']."(".$my['number'].")'), ".TABLE_TASK_REMINDER_LOG.".acknowledge_details = CONCAT(".TABLE_TASK_REMINDER_LOG.".acknowledge_details,',".$my['f_name']." ".$my['l_name']." (".$my['number'].") ".$date." <br/>') WHERE ".TABLE_TASK_REMINDER_LOG.".id ='".$task_cid."'"; 
					$execute = $db_new->query($sql); 
				}
				
				
			}
			
			//Get details of comment 
			$id = $task_cid;
			$text = 'You acknowledge the task comment successfully'; 
			$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
						<tr>
							<td class=\"message_header_ok\">
								<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
								Success
							</td>
						</tr>
						<tr>
							<td class=\"message_ok\" align=\"left\" valign=\"top\">
								<ul class=\"message_ok_2\">
									<li class=\"message_ok\">".$text."</li>
								</ul>
							</td>
						</tr>
					</table>"; 
		} 
	}
	
    if (isset($status) && $status!=''){
	
        if( $status=='1'){
			$sql= " UPDATE ".TABLE_TASK_REMINDER." SET 
			 ".TABLE_TASK_REMINDER.".status = '".$status."', 
			 ". TABLE_TASK_REMINDER .".do_completion = '".date('Y-m-d',time())."',
			 ".TABLE_TASK_REMINDER.".completed_by='".$my['user_id']."',
             ".TABLE_TASK_REMINDER.".completed_by_name='".$my['f_name']." ".$my['l_name']."' 
			 WHERE ".TABLE_TASK_REMINDER.".id ='".$id."'";
			
			$execute = $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Status marked to completed successfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					/* $message .= "| <img src=\"".addslashes($variables['nc_images'])."/completed-on.gif\" border=\"0\" 
                                title=\"Completed\" name=\"Completed\" alt=\"Completed\"/> ";
					$message .= "| ".date('j M Y')."<b> Completion Mark By </b>".$my['f_name']." ".$my['l_name'] ;
					$message .= "| <img src=\"".addslashes($variables['nc_images'])."/edit-off.gif\" border=\"0\" 
                                title=\"Edit\" name=\"Edit\" alt=\"Edit\"/> ";
					$message .= "| <img src=\"".addslashes($variables['nc_images'])."/comment-off.gif\" border=\"0\" 
                                title=\"Comment\" name=\"Comment\" alt=\"Comment\"/> ";
					$message .= "| <img src=\"".addslashes($variables['nc_images'])."/delete-off.gif\" border=\"0\" 
                                title=\"Delete\" name=\"Delete\" alt=\"Delete\"/> "; */
								
					$message .= "| completed ";
					$message .= "| ".date('j M Y')."<b> Completion Mark By </b>".$my['f_name']." ".$my['l_name'] ;
					$message .= "|  ";
					$message .= "| <a href=\"javascript:void(0);\" onclick=\"javascript: return loadAjxPage('task-div','taskIframeId','".$variables['nc'].'/task-reminder.php?perform=acomm&id='.$id."',750,600,'add comment')\" >comment & acknowledge </a>";
					$message .= "| delete";
			}
        } 	   
	}	

echo $message."|".$id ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
