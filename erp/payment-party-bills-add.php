<?php
  if ( $perm->has('nc_p_pb_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		$id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
		
	    include_once ( DIR_FS_INCLUDES .'/currency.inc.php');  
	    include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
		include_once ( DIR_FS_INCLUDES .'/service-tax-price.inc.php');
	    include_once (DIR_FS_INCLUDES .'/user.inc.php');
	    include_once (DIR_FS_INCLUDES .'/payment-transaction.inc.php');	
		include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
		include_once ( DIR_FS_INCLUDES .'/vendors-bank.inc.php');
		
		
		
		$number='';
		$stype_list = PaymentPartyBills::getSTypes();
		$no_list=PaymentPartyBills::getNos();
		$type_list = PaymentPartyBills::getType();
		
		$op_list=PaymentPartyBills::getRoundOffOp();
		$roundOff_list=PaymentPartyBills::getRoundOff();
		
		$currency = NULL ;
        $required_fields = '*' ;
		Currency::getList($db,$currency,$required_fields);  
		
        // Read the taxlist 
		$subtax_list    = NULL;		
		$tax_list    = NULL;
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=0 ORDER BY id ASC";
		ServiceTax::getList($db, $tax_list, 'id,tax_name', $condition_queryst);
		
		$tax_vlist    = NULL;
		$condition_queryvt= " WHERE status = '". ACTIVE ."' ORDER BY tax_price ASC";
		ServiceTaxPrice::getList($db, $tax_vlist, 'id,tax_price,tax_percent', $condition_queryvt);
		
		//
		include_once ( DIR_FS_INCLUDES .'/services.inc.php'); 
        $lst_service    = NULL;
        $condition_querys = " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC"; 
        Services::getList($db, $lst_service, 'ss_division,ss_id,ss_title,ss_punch_line,tax1_id, 
		tax1_name, tax1_value,is_renewable', $condition_querys);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
				$ss_id= $key['ss_id'];
				$ss_div_id= $key['ss_division'];
				$ss_title= $key['ss_title'];
				$ss_punch_line= $key['ss_punch_line'];
				$tax1_id = $key['tax1_id'];
				$tax1_name = $key['tax1_name'];
				$tax1_value = $key['tax1_value'];
				$is_renewable = $key['is_renewable'];
				$tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
				$keyNew['ss_div_id'] = $ss_div_id ; 
				$keyNew['ss_id'] = $ss_id ; 
				$keyNew['ss_title'] = $ss_title ; 
				$keyNew['ss_punch_line'] = $ss_punch_line ; 
				$keyNew['tax1_id'] = $tax1_id ; 
				$keyNew['tax1_name'] = $tax1_name ; 
				$keyNew['tax1_value'] = $tax1_value ; 
				$keyNew['tax1_pvalue'] = $tax1_pvalue ; 
				$keyNew['is_renewable'] = $is_renewable ; 
				$lst_service[$val] = $keyNew;
            }
        }
		//

	   /*
		$party =null;
        Paymenttransaction::getParty($db,$party);
        $executivelist	= NULL;
        $condition_queryc = " WHERE status ='".User::ACTIVE."' ORDER BY f_name,l_name";
        User::getList( $db, $executivelist, 'f_name,l_name,user_id', $condition_queryc);
       */ 
        $billagainstlist =array();
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files      = processUserData($_FILES);
			
           
			
            //only JPG
			$allowed_file_types=array( "image/jpeg",												
										"image/pjpeg",
										"application/pdf"
									);			
           
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = 1000; // 1024 *300 ; 600 KB
			
				if(!empty($data['party_id'])){
					$query	="SELECT id, bill_against FROM ".TABLE_PAYMENT_BILLS_AGAINST." WHERE 
					".TABLE_PAYMENT_BILLS_AGAINST.".vendor_id ='".$data['party_id']."' AND status='1'";
					$query	.= " ORDER BY ".TABLE_PAYMENT_BILLS_AGAINST.".bill_against ASC";
					
					$db->query( $query );
					
					if( $db->nf() > 0 ){
						while($db->next_record()){
							$id =$db->f("id");
							$bill_against = $db->f("bill_against") ;
							$billagainstlist[$id] =  $bill_against ;
						}
					}                 
				}    
                if(!empty($data['vendor_bank_id'])){
					$query	="SELECT id, bill_against FROM ".TABLE_PAYMENT_BILLS_AGAINST." WHERE 
					".TABLE_PAYMENT_BILLS_AGAINST.".vendor_bank_id ='".$data['vendor_bank_id']."' AND status='1'";
					$query	.= " ORDER BY ".TABLE_PAYMENT_BILLS_AGAINST.".bill_against ASC ";
					$db->query( $query );
					if( $db->nf() > 0 ){
						while($db->next_record()){
							$against_id =$db->f("id");
							$bill_against = $db->f("bill_against") ;
							$billagainstlist[$against_id] =  $bill_against ;                    
						}
					}                 
				}  
				$extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'lst_service'       => $lst_service,
                            'messages'          => &$messages,
                            'files'          => &$files,
							'data'          => &$data
				);
                if ( PaymentPartyBills::validateAdd($data, $extra) ) {
				
					//
					$currency1 =null;
					$condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
					$fields1 = "*";
					Currency::getList( $db, $currency1, $fields1, $condition_query1);              
					if(!empty($currency1)){
					   $data['currency_abbr'] = $currency1[0]['abbr'];
					   $data['currency_name'] = $currency1[0]['currency_name'];
					   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
					   $data['currency_country'] = $currency1[0]['country_name'];
					}
					//
				
					$number = $data['number'] = PaymentPartyBills::getNewNumber($db);
					$attachfilename='';
                    if(!empty($data['bill_attachment'])){
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['bill_attachment']["name"]);
                       
                        $ext = $filedata["extension"] ; 
                        //$attachfilename = $username."-".mktime().".".$ext ;
                       
                        $attachfilename = $data['number'].".".$ext ;
                        $data['bill_attachment'] = $attachfilename;
                        
                        $attachment_path = DIR_WS_BILLS_FILES;
                        if(move_uploaded_file($files['bill_attachment']['tmp_name'], 
							DIR_FS_BILLS_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_BILLS_FILES."/".$attachfilename, 0777);

                        }
                    }
					
                    if(!empty($data['bill_dt'])){
                        $data['bill_dt'] = explode('/', $data['bill_dt']);
                        $data['bill_dt'] = mktime(0, 0, 0, $data['bill_dt'][1], $data['bill_dt'][0], 
						$data['bill_dt'][2]);
                        $data['bill_dt'] = date('Y-m-d H:i:s', $data['bill_dt']);
                    }
					if(!empty($data['profm_bill_dt'])){
                        $data['profm_bill_dt'] = explode('/', $data['profm_bill_dt']);
                        $data['profm_bill_dt'] = mktime(0, 0, 0, $data['profm_bill_dt'][1], $data['profm_bill_dt'][0], $data['profm_bill_dt'][2]);
                        $data['profm_bill_dt'] = date('Y-m-d H:i:s', $data['profm_bill_dt']);
                    }
					
                    if(!empty($data['pay_due_dt'])){
                        $data['pay_due_dt'] = explode('/', $data['pay_due_dt']);
                        $data['pay_due_dt'] = mktime(0, 0, 0, $data['pay_due_dt'][1], $data['pay_due_dt'][0], $data['pay_due_dt'][2]);
                        $data['pay_due_dt'] = date('Y-m-d H:i:s', $data['pay_due_dt']);
                    }
                    if(!empty($data['late_pay_dt'])){
                        $data['late_pay_dt'] = explode('/', $data['late_pay_dt']);
                        $data['late_pay_dt'] = mktime(0, 0, 0, $data['late_pay_dt'][1], $data['late_pay_dt'][0], $data['late_pay_dt'][2]);
                        $data['late_pay_dt'] = date('Y-m-d H:i:s', $data['late_pay_dt']);
                    }
					if(!empty($data['after_pay_dt'])){
                        $data['after_pay_dt'] = explode('/', $data['after_pay_dt']);
                        $data['after_pay_dt'] = mktime(0, 0, 0, $data['after_pay_dt'][1], $data['after_pay_dt'][0], $data['after_pay_dt'][2]);
                        $data['after_pay_dt'] = date('Y-m-d H:i:s', $data['after_pay_dt']);
                    }
                    if(!empty($data['period_from'])){
                        $data['period_from'] = explode('/', $data['period_from']);
                        $data['period_from'] = mktime(0, 0, 0, $data['period_from'][1], $data['period_from'][0], 
						$data['period_from'][2]);
                        $data['period_from'] = date('Y-m-d H:i:s', $data['period_from']);
                    }
                    if(!empty($data['period_to'])){
                        $data['period_to'] = explode('/', $data['period_to']);
                        $data['period_to'] = mktime(0, 0, 0, $data['period_to'][1], $data['period_to'][0], 
						$data['period_to'][2]);
                        $data['period_to'] = date('Y-m-d H:i:s', $data['period_to']);
                    }
					 
                    $tax_ids_str='';				
					if(!empty($data['alltax_ids'])){						
						$tax_ids_str = implode(",", $data['alltax_ids']);
						$tax_ids_str = ",".$tax_ids_str."," ;
					}
					$s_id_str ='';
					if(!empty($data['s_id'])){
						$s_id_str = implode(",", $data['s_id']);
						$s_id_str = ",".$s_id_str."," ;
					}
					if(!empty($data['gl_type'])){
				
						$data['gl_type_name'] = 'FL';
					}
					$clients_su_str='';
					if(!empty($data['clients_su'])){
						$clients_su_str = implode(",", $data['clients_su']);
						$clients_su_str = ",".$clients_su_str."," ;
					}
					//Assign Support team bof
					$sql="SELECT DISTINCT(user_id), f_name,l_name FROM ".TABLE_USER." WHERE ".TABLE_USER.".groups 
						LIKE '%,". User::TP_OSTP.",%' ORDER BY RAND() LIMIT 0,3 " ;
					$db->query($sql);
					$wp_support_team = $wp_support_team_name = $wp_sql = '';
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ){ 
							$wp_support_team .= $db->f('user_id').",";
							$wp_support_team_name .= $db->f('f_name')." ".$db->f('l_name').",";
						}
						$wp_support_team = trim($wp_support_team,',');
						$wp_support_team = ",".$wp_support_team."," ;
						$wp_support_team_name = trim($wp_support_team_name,',');
						$wp_sql = ",". TABLE_PAYMENT_PARTY_BILLS .".wp_support_team  = '".$wp_support_team ."'
						,".TABLE_PAYMENT_PARTY_BILLS.".wp_support_team_name  = '".$wp_support_team_name ."'" ;
					}
					//Assign Support team eof
					
                    $query	= " INSERT INTO ".TABLE_PAYMENT_PARTY_BILLS
					." SET ".TABLE_PAYMENT_PARTY_BILLS .".bill_no = '". $data['bill_no'] ."'"  
					.",". TABLE_PAYMENT_PARTY_BILLS .".number = '". $data['number'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".profm_bill_no = '". $data['profm_bill_no'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".service_id       = '". $s_id_str ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".gl_type = '". $data['gl_type'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".gl_type_name = '". $data['gl_type_name'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".company_id = '". $data['company_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_id = '". $data['currency_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_abbr = '". $data['currency_abbr'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_name = '". $data['currency_name'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_symbol = '". $data['currency_symbol'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".currency_country = '". $data['currency_country'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".exchange_rate = '". $data['exchange_rate'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".tax_ids     = '". $tax_ids_str ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_attachment = '".$attachfilename ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".party_id = '". $data['party_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".vendor_bank_id = '". $data['vendor_bank_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".team_id = '". $data['team_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_against_id = '". $data['bill_against_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".executive_id = '". $data['executive_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_dt = '". $data['bill_dt'] ."'"                           
					.",". TABLE_PAYMENT_PARTY_BILLS .".profm_bill_dt = '". $data['profm_bill_dt'] ."'"					  .",". TABLE_PAYMENT_PARTY_BILLS .".pay_due_dt = '". $data['pay_due_dt'] ."'"                    
					.",". TABLE_PAYMENT_PARTY_BILLS .".type = '". $data['type'] ."'"
					//.",".TABLE_PAYMENT_PARTY_BILLS.".bill_type='".$data['bill_type']."'"                            
				    //.",".TABLE_PAYMENT_PARTY_BILLS .".amount = '". $data['amount'] ."'"                            
					//.",".TABLE_PAYMENT_PARTY_BILLS .".balance = '". $data['amount'] ."'"                            
					.",". TABLE_PAYMENT_PARTY_BILLS .".round_off_op = '". $data['round_off_op'] ."'"                    .",". TABLE_PAYMENT_PARTY_BILLS .".round_off = '". $data['round_off'] ."'"                		
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_amount = '". $data['bill_amount'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".stotal_amount = '". $data['stotal_amount'] ."'"
                    .",". TABLE_PAYMENT_PARTY_BILLS .".bill_amount_adjs = '". $data['bill_amount_adjs'] ."'"	                
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_amount_perbill = '". $data['bill_amount_perbill'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".late_amount = '". $data['late_amount'] ."'"                    
					.",". TABLE_PAYMENT_PARTY_BILLS .".late_pay_dt = '". $data['late_pay_dt'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".after_amount = '". $data['after_amount'] ."'"                    
					.",". TABLE_PAYMENT_PARTY_BILLS .".after_pay_dt = '". $data['after_pay_dt'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".period_from = '". $data['period_from'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".period_to = '". $data['period_to'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_details = '". $data['bill_details'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".bill_particulars = '". $data['bill_particulars'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".access_level = '".   $my['access_level'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".team             = ',". implode(",", $data['team']) .",'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".clients_su       = '".$clients_su_str ."'" 
					.",". TABLE_PAYMENT_PARTY_BILLS .".order_title_mail = '". $data['order_title_mail'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".order_title= '". $data['order_title_mail'] ."'"
					.$wp_sql
					.",". TABLE_PAYMENT_PARTY_BILLS .".status = '". $data['status'] ."'"                        
					.",". TABLE_PAYMENT_PARTY_BILLS .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"       
					.",". TABLE_PAYMENT_PARTY_BILLS .".created_by = '".     $my['user_id'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".created_by_name='". $my['f_name']." ".$my['l_name'] ."'"
					.",". TABLE_PAYMENT_PARTY_BILLS .".do_e   = '".date('Y-m-d H:i:s')."'";
                    
                    if ( $db->query($query) && $db->affected_rows() > 0 ) {
                        $messages->setOkMessage("Record has been added successfully.");
                        $variables['hid'] = $db->last_inserted_id();   
						
						
						$data['query_p']='';
						$data['query_p'] = 'INSERT INTO '. TABLE_PAYMENT_BILLS_P .' 
						(bill_id, particulars, p_amount, ss_div_id,s_id, sub_s_id, s_type, s_quantity, s_amount, 
ss_title, ss_punch_line,tax1_id,tax1_number, tax1_name,tax1_value, tax1_pvalue,tax1_amount,tax1_sub1_id,tax1_sub1_number, tax1_sub1_name,tax1_sub1_value, tax1_sub1_pvalue,tax1_sub1_amount,tax1_sub2_id,tax1_sub2_number, tax1_sub2_name,tax1_sub2_value, tax1_sub2_pvalue,tax1_sub2_amount,d_amount, stot_amount, tot_amount,is_renewable) VALUES ';						
						foreach ( $data['particulars'] as $key=>$particular ) {
							$data['tax1_number'][$key]='';
							$sub_s_id_str='';
							$sub_s_id_str =trim($data['subsidstr'][$key],",");
							if(!empty($sub_s_id_str)){
								$data['subsid'][$key] = explode(",",$sub_s_id_str);
								$sub_s_id_str=",".$sub_s_id_str.",";
							}
							if(!empty($data['tax1_id_check'][$key])){
								$sql1 ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." WHERE 
								id='".$data['tax1_id_check'][$key]."'" ;
								$db->query( $sql1 );
								if( $db->nf() > 0 ){
									while($db->next_record()){			
										$data['tax1_number'][$key] = $db->f('tax_number')  ;
									}
								}
							}
						    //Get tax number EOF
						    //Get subtax1 number 
							$data['tax1_sub1_number'][$key]='';
							if(!empty($data['tax1_sub1_id_check'][$key])){
								$sql1 ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." WHERE 
								id='".$data['tax1_sub1_id_check'][$key]."'" ;
								$db->query( $sql1 );
								if( $db->nf() > 0 ){
									while($db->next_record()){			
										$data['tax1_sub1_number'][$key] = $db->f('tax_number')  ;
									}
								}
							}
							//Get subtax2 number 
							$data['tax1_sub2_number'][$key]='';
							if(!empty($data['tax1_sub2_id_check'][$key])){
								$sql1 ="SELECT tax_number FROM ".TABLE_SERVICE_TAX." WHERE 
								id='".$data['tax1_sub2_id_check'][$key]."'" ;
								$db->query( $sql1 );
								if( $db->nf() > 0 ){
									while($db->next_record()){			
										$data['tax1_sub2_number'][$key] = $db->f('tax_number')  ;
									}
								}
							}
							 
							 
							$data['query_p'] .= "('". $variables['hid'] ."', 
							'". processUserData(trim($data['particulars'][$key],',')) ."', 
							'". $data['p_amount'][$key] ."',
							'". $data['ss_div_id'][$key] ."',
							'". $data['s_id'][$key] ."',
							'". $sub_s_id_str ."',
							'". processUserData($data['s_type'][$key]) ."', 
							'". processUserData($data['s_quantity'][$key]) ."',
							'". processUserData($data['s_amount'][$key]) ."',   
							'". processUserData($data['ss_title'][$key]) ."', 
							'". processUserData($data['ss_punch_line'][$key]) ."',							 
                            '". processUserData($data['tax1_id_check'][$key]) ."',
							'". processUserData($data['tax1_number'][$key]) ."',  
                            '". processUserData($data['tax1_name_str'][$key]) ."',
							'". processUserData($data['tax1_value'][$key]) ."',
							'". processUserData($data['tax1_pvalue'][$key]) ."',
							'". processUserData($data['tax1_amount'][$key])."',                                       
							'". processUserData($data['tax1_sub1_id_check'][$key]) ."',
							'". processUserData($data['tax1_sub1_number'][$key]) ."',  
                            '". processUserData($data['tax1_sub1_name_str'][$key]) ."', 
							'". processUserData($data['tax1_sub1_value'][$key]) ."',
							'". processUserData($data['tax1_sub1_pvalue'][$key]) ."',
							'". processUserData($data['tax1_sub1_amount'][$key]) ."',					
							'". processUserData($data['tax1_sub2_id_check'][$key]) ."',
							'". processUserData($data['tax1_sub2_number'][$key]) ."',  
                            '". processUserData($data['tax1_sub2_name_str'][$key]) ."', 
							'". processUserData($data['tax1_sub2_value'][$key]) ."',
							'". processUserData($data['tax1_sub2_pvalue'][$key]) ."',
							'". processUserData($data['tax1_sub2_amount'][$key]) ."',
							'". processUserData($data['d_amount'][$key]) ."', 
							'". processUserData($data['stot_amount'][$key]) ."',
                            '". processUserData($data['tot_amount'][$key]) ."' ,
                            '". processUserData($data['is_renewable'][$key]) ."' 
                            )," ;
						 
						}
						
						if(!empty($data['query_p'])){
							$data['query_p'] = substr($data['query_p'], 0, (strlen($data['query_p'])-1));
						
							if ( !$db->query($data['query_p']) ) {
								$messages->setErrorMessage("The Particulars were not Saved.");
							}
						}
						
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
					$data['particulars']=array('0'=>'');
		        }
				
				
        }else{
			$data['exchange_rate'] =1;
			$data['particulars']=array('0'=>'');
			
			//copy bof
			if(!empty($id)){
				$fields = TABLE_PAYMENT_PARTY_BILLS .'.*'  ;            
				$condition_query = " WHERE (". TABLE_PAYMENT_PARTY_BILLS .".id = '". $id ."' )";
				
				if ( PaymentPartyBills::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
						$_ALL_POST = $_ALL_POST['0'];
					   
						// Setup the date of delivery.
						if($_ALL_POST['bill_dt'] !='0000-00-00 00:00:00'){
							 $_ALL_POST['bill_dt']  = explode(' ', $_ALL_POST['bill_dt']);
							$temp               = explode('-', $_ALL_POST['bill_dt'][0]);
							$_ALL_POST['bill_dt']  = NULL;
							$_ALL_POST['bill_dt']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
							$_ALL_POST['bill_dt']='';
						}
					   if($_ALL_POST['pay_due_dt']!='0000-00-00 00:00:00'){
							$_ALL_POST['pay_due_dt']  = explode(' ', $_ALL_POST['pay_due_dt']);
							$temp               = explode('-', $_ALL_POST['pay_due_dt'][0]);
							$_ALL_POST['pay_due_dt']  = NULL;
							$_ALL_POST['pay_due_dt']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
						 $_ALL_POST['pay_due_dt']='';
						}
					   if($_ALL_POST['late_pay_dt']!='0000-00-00 00:00:00'){
							$_ALL_POST['late_pay_dt']  = explode(' ', $_ALL_POST['late_pay_dt']);
							$temp               = explode('-', $_ALL_POST['late_pay_dt'][0]);
							$_ALL_POST['late_pay_dt']  = NULL;
							$_ALL_POST['late_pay_dt']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
						 $_ALL_POST['late_pay_dt']='';
						}
						if($_ALL_POST['after_pay_dt']!='0000-00-00 00:00:00'){
							$_ALL_POST['after_pay_dt']  = explode(' ', $_ALL_POST['after_pay_dt']);
							$temp               = explode('-', $_ALL_POST['after_pay_dt'][0]);
							$_ALL_POST['after_pay_dt']  = NULL;
							$_ALL_POST['after_pay_dt']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
						 $_ALL_POST['after_pay_dt']='';
						}
						
						if($_ALL_POST['period_from']!='0000-00-00 00:00:00'){
							$_ALL_POST['period_from']  = explode(' ', $_ALL_POST['period_from']);
							$temp               = explode('-', $_ALL_POST['period_from'][0]);
							$_ALL_POST['period_from']  = NULL;
							$_ALL_POST['period_from']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
						 $_ALL_POST['period_from']='';
						}
						if($_ALL_POST['profm_bill_dt'] !='0000-00-00 00:00:00'){
							 $_ALL_POST['profm_bill_dt']  = explode(' ', $_ALL_POST['profm_bill_dt']);
							$temp               = explode('-', $_ALL_POST['profm_bill_dt'][0]);
							$_ALL_POST['profm_bill_dt']  = NULL;
							$_ALL_POST['profm_bill_dt']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
							$_ALL_POST['profm_bill_dt']='';
						}
						if($_ALL_POST['period_to']!='0000-00-00 00:00:00'){
							$_ALL_POST['period_to']  = explode(' ', $_ALL_POST['period_to']);
							$temp               = explode('-', $_ALL_POST['period_to'][0]);
							$_ALL_POST['period_to']  = NULL;
							$_ALL_POST['period_to']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
						}else{
						 $_ALL_POST['period_to']='';
						}
						$db1 		= new db_local; // database handle
						$_ALL_POST['executive_details']='';
						if(!empty($_ALL_POST['executive_id'])){
							User::getList($db1, $executive_details, 'user_id,number,f_name,l_name', " WHERE 
							user_id ='".$_ALL_POST['executive_id']."'");
							$executive_details1 = $executive_details[0];                       
							$_ALL_POST['executive_details'] = $executive_details1['f_name'].' 
							'.$executive_details1['l_name'].' 
							('. $executive_details1['number'] .')'; 
						}  
						if(!empty($_ALL_POST['team_id'])){
							User::getList($db1, $team_details, 'user_id,number,f_name,l_name', " WHERE 
							user_id ='".$_ALL_POST['team_id']."'");
							$team_details1 = $team_details[0];                       
							$_ALL_POST['team_details'] = $team_details1['f_name'].' '.$team_details1['l_name'].' 
							('. $team_details1['number'] .')'; 
						}  					
						if(!empty($_ALL_POST['party_id'])){
							$party_details=array();
							Clients::getList($db1, $party_details, 'user_id,number,f_name,l_name', "WHERE 
							 user_id IN ('".$_ALL_POST['party_id']."')");
							$party_details1 = $party_details[0];                        
							$_ALL_POST['party_details'] = $party_details1['f_name'].' '.$party_details1['l_name'].' 
							('. $party_details1['number'] .')'; 
						}
						if(!empty($_ALL_POST['vendor_bank_id'])){
							 VendorsBank::getList($db1, $party_details, 'user_id,number,f_name,l_name', "WHERE 
							 user_id IN ('".$_ALL_POST['vendor_bank_id']."')");
							$party_details1 = $party_details[0];                        
							$_ALL_POST['vendor_bank_details'] = $party_details1['f_name'].' 
							'.$party_details1['l_name'].' 
							('. $party_details1['number'] .')'; 
						}
						$_ALL_POST['team'] = trim($_ALL_POST['team'],",") ;
						$_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
						$temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
						$_ALL_POST['team_members']  = '';
						$_ALL_POST['team_details']  = array();
						User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE 
						user_id IN (". $temp .")");
						$_ALL_POST['team'] = array();
						foreach ( $_ALL_POST['team_members'] as $key=>$members) {
					  
							$_ALL_POST['team'][] = $members['user_id'];
							$_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' 
							('. $members['number'] .')';
						}
						//Read the Clients Sub Members Information.
						$_ALL_POST['clients_su']= trim($_ALL_POST['clients_su']);
						$_ALL_POST['clients_su']          = explode(',', $_ALL_POST['clients_su']);
						$temp_su                       = "'". implode("','", $_ALL_POST['clients_su']) ."'";
						$_ALL_POST['clients_su_members']  = '';
						$_ALL_POST['clients_su_details']  = array();
						Clients::getList($db, $_ALL_POST['clients_su_members'], 'user_id,number,f_name,l_name', "
						WHERE user_id IN (". $temp_su .")");
						$_ALL_POST['clients_su'] = array();
						if(!empty($_ALL_POST['clients_su_members'])){
							foreach ( $_ALL_POST['clients_su_members'] as $key=>$members) {
						  
								$_ALL_POST['clients_su'][] = $members['user_id'];
								$_ALL_POST['clients_su_details'][] = $members['f_name'] .' '. $members['l_name'] .'';
							}
						}
						//$id         = $_ALL_POST['id'];
					
				}
				
				if(!empty($_ALL_POST['party_id'])){
					$query	="SELECT id, bill_against FROM ".TABLE_PAYMENT_BILLS_AGAINST." WHERE 
					".TABLE_PAYMENT_BILLS_AGAINST.".vendor_id ='".$_ALL_POST['party_id']."' AND status='1'";
					$query	.= " ORDER BY ".TABLE_PAYMENT_BILLS_AGAINST.".bill_against ASC";
					$db->query( $query );                
					if( $db->nf() > 0 ){
						while($db->next_record()){
							$id1 =$db->f("id");
							$bill_against =$db->f("bill_against") ;
							$billagainstlist[$id1] =  $bill_against ;                    
						}
					}                 
				}
				// Read the Particulars.
				$temp_p = NULL;			 
				$condition_query_p = " WHERE ".TABLE_PAYMENT_BILLS_P.".bill_id = '". $id ."'";
				PaymentPartyBills::getParticulars($db, $temp_p, '*', $condition_query_p);
				if(!empty($temp_p)){
					foreach ( $temp_p as $pKey => $parti ) {                            
						$_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];	
						$_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
						$_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];							
						$_ALL_POST['ss_title'][$pKey]       = $temp_p[$pKey]['ss_title'];								 
						$_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];	
						$_ALL_POST['ss_div_id'][$pKey]   = $temp_p[$pKey]['ss_div_id'];						
						$_ALL_POST['s_type'][$pKey]       = $temp_p[$pKey]['s_type'];
						$_ALL_POST['s_quantity'][$pKey]       = $temp_p[$pKey]['s_quantity'];
						$_ALL_POST['s_amount'][$pKey]       = $temp_p[$pKey]['s_amount'];
						$_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
						$_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
						$_ALL_POST['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];					
						$_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
						$_ALL_POST['tax1_id_check'][$pKey]   = $temp_p[$pKey]['tax1_id'];
						$_ALL_POST['tax1_value'][$pKey]   = $temp_p[$pKey]['tax1_value'];
						$_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
						$_ALL_POST['tax1_number'][$pKey]   = $temp_p[$pKey]['tax1_number'];
						$_ALL_POST['tax1_amount'][$pKey]   = $temp_p[$pKey]['tax1_amount'];					
						$_ALL_POST['tax1_sub1_name'][$pKey]   = $temp_p[$pKey]['tax1_sub1_name'];
						$_ALL_POST['tax1_sub1_id_check'][$pKey]   = $temp_p[$pKey]['tax1_sub1_id'];
						$_ALL_POST['tax1_sub1_value'][$pKey]   = $temp_p[$pKey]['tax1_sub1_value'];
						$_ALL_POST['tax1_sub1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub1_pvalue'];
						$_ALL_POST['tax1_sub1_number'][$pKey]   = $temp_p[$pKey]['tax1_sub1_number'];
						$_ALL_POST['tax1_sub1_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub1_amount'];					
						$_ALL_POST['tax1_sub2_name'][$pKey]   = $temp_p[$pKey]['tax1_sub2_name'];
						$_ALL_POST['tax1_sub2_id_check'][$pKey]   = $temp_p[$pKey]['tax1_sub2_id'];
						$_ALL_POST['tax1_sub2_value'][$pKey]   = $temp_p[$pKey]['tax1_sub2_value'];
						$_ALL_POST['tax1_sub2_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub2_pvalue'];
						$_ALL_POST['tax1_sub2_number'][$pKey]   = $temp_p[$pKey]['tax1_sub2_number'];
						$_ALL_POST['tax1_sub2_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub2_amount'];					 
						$_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
						$_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
						$_ALL_POST['is_renewable'][$pKey]   = $temp_p[$pKey]['is_renewable'];
						
						$_ALL_POST['tax1_sid_opt'][$pKey]=array();
						$_ALL_POST['tax1_sid_opt_count'][$pKey]=0;
						if($_ALL_POST['tax1_id_check'][$pKey]>0){
							//tax1_sub_id Options of taxes 
							//$_ALL_POST['tax1_sid_opt'][$pKey] = array('0'=>2);
							$tax_opt = array();
							$tax_id = $_ALL_POST['tax1_id_check'][$pKey];
							$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
							ORDER BY tax_name ASC ";
							ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
							if(!empty($tax_opt)){
								$_ALL_POST['tax1_sid_opt'][$pKey] = $tax_opt ;  
								$_ALL_POST['tax1_sid_opt_count'][$pKey] = count($tax_opt) ;  
							}
						}
						$_ALL_POST['sub_sid_list'][$pKey]= $_ALL_POST['subsid'][$pKey]=$ss_sub_id_details=array();

						if(!empty($_ALL_POST['s_id'][$pKey])){
							$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = 
							".$_ALL_POST['s_id'][$pKey]." 
							AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
							$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id 
							= ".$_ALL_POST['currency_id']." 
							AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;
							
							$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
							TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
							$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".
							TABLE_SETTINGS_SERVICES_PRICE." ".$condition_query_sid ; 
							$query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

							if ( $db->query($query) ) {
								if ( $db->nf() > 0 ) {
									while ($db->next_record()) {
										$ss_subtitle =  trim($db->f("ss_title")).",";
										$ss_subprice =  $db->f("ss_price") ;
										$ss_subid =  $db->f("ss_id") ;
										$ss_sub_id_details[] = array(
											'ss_title'=>$ss_subtitle,
											'ss_price'=>$ss_subprice,
											'ss_id'=>$ss_subid	
										);
									}
								}
							} 
					   }
					   $_ALL_POST['subsidstr'][$pKey] =  $_ALL_POST['sub_s_id'][$pKey] ;
					   $_ALL_POST['sub_sid_list'][$pKey]=$ss_sub_id_details;
					   $sub_s_id_str='';
					   $sub_s_id_str =trim($_ALL_POST['subsidstr'][$pKey],",");
					   if(!empty($sub_s_id_str)){
							$_ALL_POST['subsid'][$pKey] = explode(",",$sub_s_id_str);
							$sub_s_id_str=",".$sub_s_id_str.",";
					   }
					   
					}
					$data=$_ALL_POST;
				} 
				 
			} 
			//copy eof
			
		}               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
       /*  if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/payment-party-bills.php?perform=add&added=1&number=".$number);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/payment-party-bills.php");
        } */
        /*
        if(!isset($_ALL_POST['number'])){
            $number = PaymentPartyBills::getNewNumber($db);
            $_ALL_POST['number']=$number;
        }
        */
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
         	
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $hidden[] = array('name'=> 'ajx','value' => $ajx);
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => 'data');     
            $page["var"][] = array('variable' => 'data', 'value' => 'data');     
            $page["var"][] = array('variable' => 'type_list', 'value' => 'type_list');     
            $page["var"][] = array('variable' => 'stype_list', 'value' => 'stype_list');     
            $page["var"][] = array('variable' => 'no_list', 'value' => 'no_list');  
            $page["var"][] = array('variable' => 'op_list', 'value' => 'op_list');     
			
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');    			
            $page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');    			
            $page["var"][] = array('variable' => 'roundOff_list', 'value' => 'roundOff_list');    			
            $page["var"][] = array('variable' => 'billagainstlist', 'value' => 'billagainstlist');     
            $page["var"][] = array('variable' => 'tax_list', 'value' => 'tax_list');    
			$page["var"][] = array('variable' => 'subtax_list', 'value' => 'subtax_list');			
            $page["var"][] = array('variable' => 'tax_vlist', 'value' => 'tax_vlist');     
            //$page["var"][] = array('variable' => 'party', 'value' => 'party');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-party-bills-add.html');
       
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
