<?php


if ( $perm->has('nc_rp_pr') ) {

        $project_id	= isset($_GET["project_id"]) ? $_GET["project_id"]	: ( isset($_POST["project_id"]) 	? $_POST["project_id"] : '' );
        $_SEARCH["project_id"] 	= $project_id;
        $_ALL_POST 	= $_POST;
        //to view report project id should be compulsary
        $teamArr=$teamList = $_ORDER_DATA = array();
        $teamStr=$condition_url ='';
        
         // BO: Read the Team Orders list ie project list in dropdown box BOF
        //$condition_query1 = " WHERE ". TABLE_BILL_ORDERS .".status = '". Order::ACTIVE ."' AND ".TABLE_BILL_ORDERS .".order_title!='' ";
        $condition_query1 = " WHERE ". TABLE_BILL_ORDERS .".order_title!='' ";
        // If the Order is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_or_list_al') ) {
            $access_level += 1;
        }
        $condition_query1 .= " AND ( ";
    
        // If my has created this Order.
        $condition_query1 .= " (". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
        
        // If my is present in the Team.
        $condition_query1 .= " OR ( ( "
                                        . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                        . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
                                    . " ) "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
        
        // If my is the Client Manager
        $condition_query1 .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                            ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
        
        // Check if the User has the Right to view Orders created by other Users.
        if ( $perm->has('nc_bl_or_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_bl_or_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query1 .= " OR ( ". TABLE_BILL_ORDERS. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level_o ) ";
        }
        $condition_query1 .= " ) ";
        //$condition_query1 .= " ORDER BY ". TABLE_BILL_ORDERS .".do_d DESC ";
        $condition_query1 .= " ORDER BY ". TABLE_BILL_ORDERS .".order_title ASC ";
        $fields1 = TABLE_BILL_ORDERS.".id, ".TABLE_BILL_ORDERS.".order_title";
        // To count total projects records.
        $lst_project	= 	NULL;
        $total1	=	Order::getOrderList( $db, $lst_project, $fields1, $condition_query1);
        // BO: Read the Team Orders list ie project list in dropdown box EOF
              
        if(array_key_exists('submit_search',$_POST)){
        
            if(!empty($project_id)){        
                /*$subquery = " SELECT ".TABLE_BILL_ORDERS.".team FROM ".TABLE_BILL_ORDERS." WHERE ".TABLE_BILL_ORDERS.".id=".$project_id ;
                if ( $db->query($subquery) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                            $teamArr = processSqlData($db->result());
                            $teamStr = str_replace(",","','",$teamArr['team']);
                        }
                    }
                }*/
                
                $subquery = " SELECT DISTINCT ".TABLE_PROJECT_TASK_DETAILS.".added_by FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE ".TABLE_PROJECT_TASK_DETAILS.".project_id=".$project_id ;
                if ( $db->query($subquery) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                            $teamArr[] = processSqlData($db->result());
                            //$teamStr = str_replace(",","','",$teamArr['added_by']);
                        }
                    }
                }
                
                
                
                if(!empty($teamArr)){
                    foreach( $teamArr as $key=>$val){ 
                        $query = "SELECT f_name, l_name, user_id FROM ".TABLE_USER." WHERE user_id ='".$val['added_by']."' ORDER BY user_id";
                        if ( $db->query($query) ) {
                            if ( $db->nf() > 0 ) {
                                while ($db->next_record()) {
                                    $teamList[$db->f('user_id')] = $db->f('f_name')." ".$db->f('l_name');                               
                                }
                            }
                        }         
                    }
                }
                
            }else{
                $messages->setErrorMessage('Please select Project.');
            }
        }   
            
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    
    //As validate project id 
    if($messages->getErrorMessageCount() <= 0 && !empty($teamArr)){
    
        $list	= $dateArr=	NULL;    
        // Read the Date data.
        $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
        $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
        $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
        $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
        $condition_query2 ='';
        $where_added =true;
        // BO: From Date
        if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR')  && !empty($date_from)){
            if ( $where_added ) {
                $condition_query2 .= " ". $chk_date_from;
            }
            else {
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            $dfa = explode('/', $date_from);
            $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
            //$dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0];
            $condition_query2 .= " ". TABLE_PROJECT_TASK_DETAILS .".do_e >= '". $dfa ."'";
            
            $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
            $_SEARCH["chk_date_from"]   = $chk_date_from;
            $_SEARCH["date_from"]       = $date_from;
        }
        // EO: From Date
        
        // BO: Upto Date
        if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
            if ( $where_added ) {
                $condition_query2 .= $chk_date_to;
            }
            else {
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            $dta = explode('/', $date_to);
            $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
            //$dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0];
            $condition_query2 .= " ". TABLE_PROJECT_TASK_DETAILS .".do_e <= '". $dta ."'";
            
            $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
            $_SEARCH["chk_date_to"] = $chk_date_to ;
            $_SEARCH["date_to"]     = $date_to ;
        }
        // EO: Upto Date
        
        //Step 1 : fetch distinct date from TABLE_USER_TIMESHEET;
        $sql="SELECT DISTINCT( DATE_FORMAT(".TABLE_PROJECT_TASK_DETAILS.".do_e,'%Y-%m-%d')) as do_e FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE ".TABLE_PROJECT_TASK_DETAILS.".project_id = ".$project_id." AND ( parent_id !=0 OR task_id !=0 )";
        $condition_query2.=" ORDER BY do_e ASC";
        $sql .= " ". $condition_query2;
        //echo $sql;
        if ( $db->query($sql) ) {
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $dateArr[] =  $db->f('do_e');
                }
            }
        }
        //print_r($dateArr);
        // fields req to in query
        $fields = ' ( SUM('.TABLE_PROJECT_TASK_DETAILS .'.hrs) + ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) DIV 60 ))  as totHr '
                    .', ( SUM('. TABLE_PROJECT_TASK_DETAILS .'.min) % 60 ) as totMin'
                    .', '. TABLE_PROJECT_TASK_DETAILS .'.do_e';
                    
        $condition_query = $endDate='';
        $grandTotalArr=array();
        if(!empty($dateArr)){
            foreach($dateArr as $dateStr => $date1){
                $condition_query =$query='';
                $detail = array();
                $dateTothr = 0;
                $dateTotmin = 0;
                
                foreach($teamList As $key => $val){
                    // cal. all hr per date and per user
                    $condition_query = " WHERE ". TABLE_PROJECT_TASK_DETAILS .".added_by = '". $key ."' AND ". TABLE_PROJECT_TASK_DETAILS .".project_id = ".$project_id." AND DATE_FORMAT(".TABLE_PROJECT_TASK_DETAILS.".do_e,'%Y-%m-%d') ='".$date1."'" ;
                    $condition_query .= " AND ( parent_id !=0 OR task_id !=0 ) GROUP BY ".TABLE_PROJECT_TASK_DETAILS.".added_by ORDER BY added_by ";
         
                    //Myprofile::getTimesheetDetails( $db, $list, $fields, $condition_query);
                    $query = "SELECT  ".$fields;            
                    $query .= " FROM ". TABLE_PROJECT_TASK_DETAILS;
                    $query .= " LEFT JOIN ". TABLE_BILL_ORDERS
                                ." ON ". TABLE_PROJECT_TASK_DETAILS .".project_id = ". TABLE_BILL_ORDERS .".id ";
                    $query .= " ". $condition_query;     
                    //echo $query;              
                    $totalHr=$hrs=$min='';
                    if ( $db->query($query) ) {
                        if ( $db->nf() > 0 ) {
                            while ($db->next_record()) {    
                                $hrs =  $db->f('totHr');                           
                                $min =  $db->f('totMin');                           
                                $totalHr = $db->f('totHr').":".$db->f('totMin') ;                               
                            }
                        }
                    }
                    
                    $detail[$key] = $totalHr;
                    $dateTothr = $dateTothr + $hrs ;
                    $dateTotmin = $dateTotmin + $min ;
                    $grandTotalArr[$key][] = array($hrs,$min);
                }
                $round1=0;
                if($dateTotmin >= 60){
                    $round1 =  (int) ($dateTotmin / 60);
                    $dateTotmin = $dateTotmin % 60 ;
                }
                $dateTothr = $dateTothr + $round1 ;      
                $detail['datetot']= $dateTothr.":".$dateTotmin;
                $list[][$date1]=$detail ;
                $endDate = $date1 ;
            }
           
        }
        //Calculate total of each users in hrs and min
        $totalHrsArr=array();
        $grandTotHr = $grandTotMin = $round2 =0;
       
        if(!empty($grandTotalArr)){
            foreach($grandTotalArr as $key=>$value){               
                $totalhr =  $totalmin ='00';
                $round = 0;                
                foreach($value as $key1=>$hrmin){
                    $totalhr =$totalhr + $hrmin[0];
                    $totalmin = $totalmin + $hrmin[1];
                    
                }
                if($totalmin >= 60){
                    $round =(int) ($totalmin/60) ;
                    $totalmin = $totalmin % 60 ;
                }
                $totalhr = $totalhr + $round  ;               
                $totalHrsArr[$key] = $totalhr.":".$totalmin ;
                $grandTotHr =  $grandTotHr + $totalhr;
                $grandTotMin = $grandTotMin + $totalmin;
            }
            if($grandTotMin >= 60){
                $round2=(int) ($grandTotMin/60) ;
                $grandTotMin = $grandTotMin % 60 ;
            }
            $grandTotHr = $grandTotHr + $round2 ;
            $totalHrsArr['grtotal'] = $grandTotHr.":".$grandTotMin   ;
        }
        /*
        Update table bill-order 
        1. check act. end date & act hrs from table bill-order
        2. If act. end date and act hrs are not match update act. end date and act. hrs
        */
        
        $condition_query3 = " WHERE (". TABLE_BILL_ORDERS .".id = '". $project_id ."' )";
        $fields = "DATE(".TABLE_BILL_ORDERS .".st_date) as st_date"
                  .",DATE(". TABLE_BILL_ORDERS .".ac_ed_date) as ac_ed_date"
                  .",DATE(". TABLE_BILL_ORDERS .".es_ed_date) as es_ed_date"
                  .",". TABLE_BILL_ORDERS .'.es_hrs' 
                  .",". TABLE_BILL_ORDERS .'.ac_hrs' 
                  .",". TABLE_BILL_ORDERS .'.ac_min' ;
        Order::getDetails($db, $_ALL_DATA, $fields, $condition_query3);
        $_ORDER_DATA = $_ALL_DATA[0];
        
        //As if report is not viewed formdate  To to date
        
        /*if(empty($condition_query2)){
           $updateDate = $updateHrs = $updateSql='';
           if(trim($_ORDER_DATA['ac_ed_date']) != trim($endDate) && !empty($endDate)){
                $updateDate = TABLE_BILL_ORDERS.".ac_ed_date = '".$endDate."'";
                $_ORDER_DATA['ac_ed_date'] = $endDate;            
           }       
           
           $checkAcHrs = (float)( $_ORDER_DATA['ac_hrs'].".".$_ORDER_DATA['ac_min'] );
           $calAcHrs = (float) ($grandTotHr.".".$grandTotMin );
           
           if($checkAcHrs != $calAcHrs ){
                if(!empty($updateDate)) $updateDate = $updateDate.", ";
                
                $updateHrs = TABLE_BILL_ORDERS.".ac_hrs = ".$grandTotHr.", ".TABLE_BILL_ORDERS.".ac_min = ".$grandTotMin;  
                $_ORDER_DATA['ac_hrs'] = $grandTotHr ;
                $_ORDER_DATA['ac_min'] = $grandTotMin ;            
           }
           $updateSql = $updateDate.$updateHrs ;
           if(!empty($updateSql)){
                $sql = " UPDATE ".TABLE_BILL_ORDERS." SET ".$updateSql." WHERE ".TABLE_BILL_ORDERS .".id = ". $project_id ;
                //$db->query($sql);
                
           }
        }*/
    }
    //print_r($list);
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'teamList', 'value' => 'teamList');
    $page["var"][] = array('variable' => 'lst_project', 'value' => 'lst_project');        
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'totalHrsArr', 'value' => 'totalHrsArr');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
    $page["var"][] = array('variable' => '_ORDER_DATA', 'value' => '_ORDER_DATA');
    //$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    //$page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-project-wise.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>