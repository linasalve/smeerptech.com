<?php
    if ( $perm->has('nc_pop_status') ) {
        $id= isset($_GET["id"])? $_GET["id"] : ( isset($_POST["id"])? $_POST["id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
        $access_level = $my['access_level'];
       
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        PurchaseOrderPdf::updateStatus($id, $status, $extra);
            
        // Display the  list.
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order-pdf-status.html');
    }
    else {
        $messages->setErrorMessage("You do not have the Right to Change Status.");
		 $page["section"][] = array('container'=>'CONTENT', 'page' => 'popup.html');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'purchase-order.html');
    }
?>
