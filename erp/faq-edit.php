<?php
if ( $perm->has('nc_faq_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        include_once (DIR_FS_INCLUDES .'/faq.inc.php');
        $displayList = Faq::getDisplayList();
	  
	   
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Faq::validateUpdate($data, $extra) ) {
			
				$sql_file1=$sql_file2=$sql_file3='';
				if(!empty($data['file_1'])){
					if(!empty($data['old_file_1'])){
						//@unlink(DIR_FS_FAQ_FILES."/".$data['old_file_1']);
					}
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_1']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-1".".".$ext ;
					$data['old_file_1']=$data['file_1'] = $attachfilename;						
					if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_FAQ_FILES."/".$attachfilename)){					    @chmod(DIR_FS_FAQ_FILES."/".$attachfilename,0777);
					  $sql_file1 = ",". TABLE_FAQ .".file_1 = '".    $data['file_1'] ."'" ;
					}
				}else{
					if(isset($data['delete_file_1']) && $data['delete_file_1']==1){
						$data['file_1']='';
						//@unlink(DIR_FS_FAQ_FILES."/".$data['old_file_1']);
						$sql_file1 = ",". TABLE_FAQ .".file_1 = '".    $data['file_1'] ."'" ;
					}
				}
				if(!empty($data['file_2'])){
					if(!empty($data['old_file_2'])){
						//@unlink(DIR_FS_FAQ_FILES."/".$data['old_file_2']);
					}						
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_2']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-2".".".$ext ;
					$data['old_file_2']=$data['file_2'] = $attachfilename;                    
					if (move_uploaded_file ($files['file_2']['tmp_name'], DIR_FS_FAQ_FILES."/".$attachfilename)){
					   @chmod(DIR_FS_FAQ_FILES."/".$attachfilename,0777);
					   $sql_file2 = ",". TABLE_FAQ .".file_2 = '".    $data['file_2'] ."'" ;
					}
				}else{
					if(isset($data['delete_file_2']) && $data['delete_file_2']==1){
						$data['file_2']='';
						//@unlink(DIR_FS_FAQ_FILES."/".$data['old_file_2']);
						$sql_file2 = ",". TABLE_FAQ .".file_2 = '". $data['file_2'] ."'" ;
					}
				}
				if(!empty($data['file_3'])){
					if(!empty($data['old_file_3'])){
						//@unlink(DIR_FS_FAQ_FILES."/".$data['old_file_3']);
					}						
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_3']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = $data['number']."-".mktime()."-3".".".$ext ;
					$data['old_file_3']=$data['file_3'] = $attachfilename;                    
					if (move_uploaded_file ($files['file_3']['tmp_name'], DIR_FS_FAQ_FILES."/".$attachfilename)){
					   @chmod(DIR_FS_FAQ_FILES."/".$attachfilename,0777);
					   $sql_file3 = ",". TABLE_FAQ .".file_3 = '". $data['file_3'] ."'" ;
					}
				}else{
					if(isset($data['delete_file_3']) && $data['delete_file_3']==1){
						$data['file_3']='';
						//@unlink(DIR_FS_FAQ_FILES."/".$data['old_file_3']);
						$sql_file3 = ",". TABLE_FAQ .".file_3 = '". $data['file_3'] ."'" ;
					}
				}
				$show_instr='';
                if(isset($_POST['show_in']) && !empty($_POST['show_in'])){
                    $show_in_ids=$_POST['show_in'];
                    $show_in_idstr=implode(",",$show_in_ids);
                    $show_instr = ",".$show_in_idstr.",";	
                } 
				$query  = " UPDATE ". TABLE_FAQ
                        ." SET ". TABLE_FAQ .".title = '".		$data['title'] ."'"
							.$sql_file1
							.$sql_file2
							.$sql_file3
							.",". TABLE_FAQ .".show_in = '".$show_instr."'"
							.",". TABLE_FAQ .".subject = '".$data['subject'] ."'"
							.",". TABLE_FAQ .".details = '".$data['details'] ."'"
							.",". TABLE_FAQ .".status = '".$data['status'] ."'"                               
							." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                //$data       = NULL;
				$fields = TABLE_FAQ .'.*'  ;            
				$condition_query = " WHERE (". TABLE_FAQ .".id = '". $id ."' )";
				if ( Faq::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
					$_ALL_POST = $_ALL_POST['0'];
					$_ALL_POST['old_file_1']=$_ALL_POST['file_1'];
					$_ALL_POST['old_file_2']=$_ALL_POST['file_2'];
					$_ALL_POST['old_file_3']=$_ALL_POST['file_3'];
                    // Setup the date of delivery.
                    $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                    $temp               = explode('-', $_ALL_POST['do_e'][0]);
                    $_ALL_POST['do_e']  = NULL;
                    $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
					$_ALL_POST['show_in']    = explode(",", $_ALL_POST['show_in']);
                   $id         = $_ALL_POST['id'];
                
				} 
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_FAQ .'.*'  ;            
            $condition_query = " WHERE (". TABLE_FAQ .".id = '". $id ."' )";
            
            if ( Faq::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                $number=$_ALL_POST['number'];
                $_ALL_POST['old_file_1']=$_ALL_POST['file_1'];
                $_ALL_POST['old_file_2']=$_ALL_POST['file_2'];
                $_ALL_POST['old_file_3']=$_ALL_POST['file_3'];
                    // Setup the date of delivery.
                    $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                    $temp               = explode('-', $_ALL_POST['do_e'][0]);
                    $_ALL_POST['do_e']  = NULL;
                    $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                   $id         = $_ALL_POST['id'];
                   $_ALL_POST['show_in']    = explode(",", $_ALL_POST['show_in']);
            } else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/faq-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
           
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'number', 'value' => $number);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'displayList', 'value' => 'displayList');   
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'faq-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
