<?php
	/**
	 * Short description for file
	 * 
	 *
	 * PHP versions All
	 *
	 * @category   Hotel MODULE
	 * @package    ehotel.com
	 * @author     
	 * @copyright  SMEERP Technologies
	 * @license    As described below
	 * @version    1.2.0
	 * @link       
	 * @see        -NA-
	 * @since      File available since Release 1.2.0 dt. tue, 13 Feb, 2007
	 * @deprecated -NA-
	 */
	
	/*********************************************************
	* Licence:
	* This file is sole property of the installer.
	* Any type of copy or reproduction without the consent
	* of owner is prohibited.
	* If in any case used leave this part intact without 
	* any modification.
	* All Rights Reserved
	* Copyright 2007 Owner
	*******************************************************/
	//$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
	//$pagination = isset($_POST["pagination"]) ? $_POST["pagination"] : (isset($_GET["pagination"]) ? $_GET["pagination"]:'');
	$condition_query= '';
    $condition_url  = '';
    if ( $sString != "" ) {
	
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
		if ( $search_table != "All" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
			$condition_url .= "&sString=". $sString;
			$condition_url .= "&sType=". $sType;
			$where_added = true;
		}
		elseif ( $search_table == "All" ) {
            $condition_query .= " WHERE (";
			foreach( $sTypeArray as $table => $field_arr ) {
				if ( $table != "All" ) {
					foreach( $field_arr as $key => $field ) {
                        $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
						$where_added = true;
					}
				}
			}
			$condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
			$condition_query .= ") ";
		}
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}
	//echo $condition_query;
	$condition_url = "&rpp=$rpp"
                        ."&sString=$sString"
                        ."&sType=$sType"
                        ."&sOrderBy=$sOrderBy"
                        ."&sOrder=$sOrder"
                        ."&action=search";
			
	$_SEARCH["sString"] 	= $sString ;
	$_SEARCH["sType"] 		= $sType ;
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
	include ( DIR_FS_NC .'/user-rights-list.php');
?>