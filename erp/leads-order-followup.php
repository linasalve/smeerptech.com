<?php   

    $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');
    $frm    = isset($_GET["frm"]) ? $_GET["frm"] : ( isset($_POST["frm"]) ? $_POST["frm"] : '' );
    
    $_ALL_POSTDATA	   =$_ALL_POST = NULL;
    $condition_query= NULL;

   include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
   include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
  include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
    // Read the record details
 if($perm->has('nc_ld_or_flw')){ //
                   
    $fields = TABLE_LEADS_ORDERS .'.*'
		.','. TABLE_SALE_LEADS .'.lead_id AS c_user_id'
		.','. TABLE_SALE_LEADS .'.number AS c_number'
		.','. TABLE_SALE_LEADS .'.f_name AS c_f_name'
		.','. TABLE_SALE_LEADS .'.l_name AS c_l_name'
		.','. TABLE_SALE_LEADS .'.billing_name AS billing_name'
		.','. TABLE_SALE_LEADS .'.company_name AS c_company_name'
		.','. TABLE_SALE_LEADS .'.email '
		.','. TABLE_SALE_LEADS .'.email_1 '
		.','. TABLE_SALE_LEADS .'.email_2 '
		.','. TABLE_SALE_LEADS .'.email_3 '
		.','. TABLE_SALE_LEADS .'.email_4 '
		.','. TABLE_SALE_LEADS .'.mobile1 '
		.','. TABLE_SALE_LEADS .'.mobile2 '
		.','. TABLE_SALE_LEADS .'.mobile3 '
		.','. TABLE_SALE_LEADS .'.status AS c_status';
		
    if ( LeadsOrder::getDetails($db, $_ALL_POSTDATA, $fields, " WHERE ".TABLE_LEADS_ORDERS.".id = ".$id ) > 0 ){
        
        $_ALL_POSTDATA = $_ALL_POSTDATA['0'];
        $_ALL_POSTDATA['client_details'] = $_ALL_POSTDATA['c_f_name'] .' '. $_ALL_POSTDATA['c_l_name'].' ('. $_ALL_POSTDATA['c_company_name'] .')';
										//.' ('. $_ALL_POSTDATA['c_email'] .')';                    
		// Read the Team Members Information. lead_details
		$_ALL_POSTDATA['lead'] = $_ALL_POSTDATA['c_user_id'] ;
		$_ALL_POSTDATA['lead_details'] = $_ALL_POSTDATA['client_details'] ;
		$_ALL_POSTDATA['team'] = trim($_ALL_POSTDATA['team'],",") ;
		$_ALL_POSTDATA['old_team'] 		= $_ALL_POSTDATA['team'] ;
		$_ALL_POSTDATA['team']          = explode(',', $_ALL_POSTDATA['team']);
		$temp                       = "'". implode("','", $_ALL_POSTDATA['team'])."'";
		$_ALL_POSTDATA['team_members']  = '';
		$_ALL_POSTDATA['team_details']  = array();
		User::getList($db, $_ALL_POSTDATA['team_members'], 'user_id,number,f_name,l_name', 
		" WHERE user_id IN (". $temp .")");
		$_ALL_POSTDATA['team'] = array();
		foreach ( $_ALL_POSTDATA['team_members'] as $key=>$members) {
			$_ALL_POSTDATA['team'][] = $members['user_id'];
			$_ALL_POSTDATA['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
		}
		 // Read the Clients Sub Members Information.
		$_ALL_POSTDATA['clients_su']= trim($_ALL_POSTDATA['clients_su']);
		$_ALL_POSTDATA['clients_su']          = explode(',', $_ALL_POSTDATA['clients_su']);
		$temp_su                       = "'". implode("','", $_ALL_POSTDATA['clients_su']) ."'";
		$_ALL_POSTDATA['clients_su_members']  = '';
		$_ALL_POSTDATA['clients_su_details']  = array();
		Leads::getList($db, $_ALL_POSTDATA['clients_su_members'], 'lead_id as user_id,number,f_name,l_name', "
		WHERE lead_id IN (". $temp_su .")");
		$_ALL_POSTDATA['clients_su'] = array();
		if(!empty($_ALL_POSTDATA['clients_su_members'])){
			foreach ( $_ALL_POSTDATA['clients_su_members'] as $key=>$members) {
		  
				$_ALL_POSTDATA['clients_su'][] = $members['user_id'];
				/*$_ALL_POSTDATA['clients_su_details'][] = $members['f_name'] .' '. $members['l_name'] 
				.' ('. $members['number'] .')'; */
				$_ALL_POSTDATA['clients_su_details'][] = $members['f_name'] .' '. $members['l_name'] .'';
			}
		}
		//date of order
		$_ALL_POSTDATA['do_o']  = explode(' ', $_ALL_POSTDATA['do_o']);
		$temp               = explode('-', $_ALL_POSTDATA['do_o'][0]);
		$_ALL_POSTDATA['do_o']  = NULL;
		$_ALL_POSTDATA['do_o']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
		// Setup the date of delivery.
		$_ALL_POSTDATA['do_d']  = explode(' ', $_ALL_POSTDATA['do_d']);
		$temp               = explode('-', $_ALL_POSTDATA['do_d'][0]);
		$_ALL_POSTDATA['do_d']  = NULL;
		$_ALL_POSTDATA['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
		$po_id              = $_ALL_POSTDATA['po_id'];
		
		// Setup the start date and est date
		if( $_ALL_POSTDATA['st_date'] =='0000-00-00 00:00:00'){
			$_ALL_POSTDATA['st_date']  ='';
		}else{
			$_ALL_POSTDATA['st_date']  = explode(' ', $_ALL_POSTDATA['st_date']);
			$temp               = explode('-', $_ALL_POSTDATA['st_date'][0]);
			$_ALL_POSTDATA['st_date']  = NULL;
			$_ALL_POSTDATA['st_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
		}
		// Setup the start date and est date
		
		if( $_ALL_POSTDATA['es_ed_date'] =='0000-00-00 00:00:00'){
			$_ALL_POSTDATA['es_ed_date']  ='';
		}else{
			$_ALL_POSTDATA['es_ed_date']  = explode(' ', $_ALL_POSTDATA['es_ed_date']);
			$temp               = explode('-', $_ALL_POSTDATA['es_ed_date'][0]);
			$_ALL_POSTDATA['es_ed_date']  = NULL;
			$_ALL_POSTDATA['es_ed_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
		}
		if( $_ALL_POSTDATA['do_e'] =='0000-00-00 00:00:00'){
			$_ALL_POSTDATA['do_e']  ='';
		}else{
			$_ALL_POSTDATA['do_e']  = explode(' ', $_ALL_POSTDATA['do_e']);
			$temp               = explode('-', $_ALL_POSTDATA['do_e'][0]);
			$_ALL_POSTDATA['do_e']  = NULL;
			$_ALL_POSTDATA['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
		}
		if( $_ALL_POSTDATA['do_fe'] =='0000-00-00 00:00:00'){
			$_ALL_POSTDATA['do_fe']  ='';
		}else{
			$_ALL_POSTDATA['do_fe']  = explode(' ', $_ALL_POSTDATA['do_fe']);
			$temp               = explode('-', $_ALL_POSTDATA['do_fe'][0]);
			$_ALL_POSTDATA['do_fe']  = NULL;
			$_ALL_POSTDATA['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
		}
		
		//print_R($_ALL_POSTDATA); 
        
		if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
			  $_ALL_POST  = $_POST;
			  $data       = processUserData($_ALL_POST);
			  $files  = processUserData($_FILES);
		 
				$extra = array( 'db'                => &$db,
						'messages'          => &$messages
					);
				$data['files'] = $files ;
				$data['allowed_file_types'] = $allowed_file_types   ;
				$data['max_file_size'] = MAX_FILE_SIZE ;
	  
			if ( LeadsOrder::validateFollowup($data, $extra) ) {
				
				/* 
				if(!empty($data['file_1'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['file_1']["name"]);                   
					$ext = $filedata["extension"] ; 
					$attachfilename = mktime()."-1".".".$ext ;
					$data['file_1'] = $attachfilename;
					if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_TASKSCHEDULER_FILES."/".$attachfilename)){
						@chmod(DIR_FS_TASKSCHEDULER_FILES."/".$attachfilename,0777);
					}
				}
				*/
			 
				$data['query_by_name'] = $my['f_name']." ".$my['l_name'] ;
				$data['lead_details']  = $_ALL_POSTDATA['lead_details'] ; 
				$data['email']   = $_ALL_POSTDATA['email'] ;
				$data['email_1'] = $_ALL_POSTDATA['email_1'] ;
				$data['email_2'] = $_ALL_POSTDATA['email_2'] ;
				$data['email_3'] = $_ALL_POSTDATA['email_3'] ;
				$data['email_4'] = $_ALL_POSTDATA['email_4'] ;
				$data['mobile1'] = $_ALL_POSTDATA['mobile1'] ;
				$data['mobile2'] = $_ALL_POSTDATA['mobile2'] ;
				$data['mobile3'] = $_ALL_POSTDATA['mobile3'] ;
				$data['name'] 	 = $_ALL_POSTDATA['c_f_name']." ".$_ALL_POSTDATA['c_l_name'] ;
				$data['company_name'] 	= $_ALL_POSTDATA['c_company_name'] ;
				if(empty($my['marketing_contact'] )){
					$my['marketing_contact']  = SALES_NOS ;
				}
				$data['marketing_contact'] 	= $my['marketing_contact'] ;
				
				//$communication_details 	$your_queries 	$company_inputs 	$next_strate
				if(!empty($data['your_queries'])){
					//Send on mail bof 
					$data['link'] = DIR_WS_NC."/"."leads-order.php?perform=followup&id=".$id;
					if ( getParsedEmail($db, $s, 'EMAIL_LDF_QRY', $data, $email) ) {
						$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
							'email' => $smeerp_client_email);
						$to[]   = array('name' => CEO_USER_EMAIL, 'email' => CEO_USER_EMAIL);                                   
						$from   = $reply_to = array('name' => $my["email"],'email' => $my['email']);
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);  
						 
					}
					//Send on mail eof
				}
				$data['link'] = DIR_WS_NC."/"."leads-order.php?perform=followup&id=".$id;
				if ( getParsedEmail($db, $s, 'EMAIL_LDF_FLW', $data, $email) ) {
					$to = '';
					$to[]   = array('name' => 'admin', 'email' => $sales_email);
					/* $to[]   = array('name' => CEO_USER_EMAIL, 'email' => CEO_USER_EMAIL);         */     
					//echo $email["body"];
					
					$from   = $reply_to = array('name' => $my["email"],'email' => $my['email']);
					SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);  
					 
				}
				
				$query	= " INSERT INTO ".TABLE_LEADS_ORD_FLW
						." SET ". TABLE_LEADS_ORD_FLW .".lead_id = '". $id ."'"
							.",". TABLE_LEADS_ORD_FLW .".prospect_id = '".$_ALL_POSTDATA['lead'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".proposal_name = '".$_ALL_POSTDATA['lead_details']."'"
							.",". TABLE_LEADS_ORD_FLW .".communication_details = '".$data['communication_details'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".your_queries = '".$data['your_queries'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".next_strategy = '".$data['next_strategy'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".expenses = '".$data['expenses'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".medium_comm = '".$data['medium_comm'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".distance_km = '".$data['distance_km'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".do_followup = '".$data['do_followup2'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".ip 		 = '".$_SERVER['REMOTE_ADDR']."'"
							.",". TABLE_LEADS_ORD_FLW .".added_by 	 = '". $my['user_id'] ."'"
							.",". TABLE_LEADS_ORD_FLW .".added_by_name = '". $my['f_name']." ".$my['l_name']."'"
							.",". TABLE_LEADS_ORD_FLW .".do_e = '". date('Y-m-d h:i:s') ."'";
				$db->query($query);    
				$variables['hid'] = $db->last_inserted_id();   
				$messages->setOkMessage("New Followup has been added."); 
				
				//update total cost, kms,visits,calls
				if($data['medium_comm']==1){
					$sql_visit = TABLE_LEADS_ORDERS.".total_visits = (".TABLE_LEADS_ORDERS.".total_visits + 1), " ;
				}
				if($data['medium_comm']==2){
					$sql_calls = TABLE_LEADS_ORDERS.".total_calls = (".TABLE_LEADS_ORDERS.".total_calls + 1), " ;
				}
				
				if(empty($data['distance_km'])){
					$data['distance_km'] = 0;
				}
				if(empty($data['expenses'])){
					$data['expenses'] = 0;
				}
				
				$sql = "UPDATE ".TABLE_LEADS_ORDERS." 
					SET ".TABLE_LEADS_ORDERS.".total_expenses = ".TABLE_LEADS_ORDERS.".total_expenses + ".$data['expenses'].",
					".TABLE_LEADS_ORDERS.".total_km = ".TABLE_LEADS_ORDERS.".total_km + ".$data['distance_km'].",
					".$sql_visit.$sql_calls." ".TABLE_LEADS_ORDERS.".do_followup = '".$data['do_followup2']."',
					".TABLE_LEADS_ORDERS.".followup_by = '".$my['user_id']."',
					".TABLE_LEADS_ORDERS.".followup_by_name = '".$my['f_name']." ".$my['l_name']."'
					WHERE ".TABLE_LEADS_ORDERS.".id = ".$id." ";
				$db->query($sql); 				 
				 
				$_ALL_POST  = null;
			}
			
		}
		
		 
	
		 // get list of all comments of change log BOF
		$condition_query1 = $condition_url1 =$extra_url= '';
		$condition_url1 .="&perform=".$perform."&id=".$id;
		$perform = 'acomm';
		$listLog	= 	NULL;
		$fields = TABLE_LEADS_ORD_FLW.".id " ;
		$condition_query1 = " WHERE ".TABLE_LEADS_ORD_FLW.".lead_id = '".$id."' " ;
		$condition_query1 .= " ORDER BY ".TABLE_LEADS_ORD_FLW.".id DESC";
		$total	=	LeadsOrder::getFollowupLog( $db, $listLog, '' , $condition_query1);
	   
		
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
		//$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  .= "&perform=".$perform;
		$extra_url  .= "&id=".$id ;
		$extra_url  .= "&x=$x&rpp=$rpp";
		$extra_url  = '&start=url'. $extra_url .'&end=url';
		$listLog	= NULL;
		$fields = TABLE_LEADS_ORD_FLW .'.* ';   
		LeadsOrder::getFollowupLog( $db, $listLog, $fields, $condition_query1, $next_record, $rpp);
		$condition_url .="&perform=".$perform;
		
		$variables['can_input'] = false;
		if($perm->has('nc_ld_or_flwip')){
			$variables['can_input'] = true;
		}
		       	  
	
		$hidden[] = array('name'=> 'id', 'value' => $id);
		$hidden[] = array('name'=> 'perform', 'value' => 'followup');
		$hidden[] = array('name'=> 'act', 'value' => 'save');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
		$page["var"][] = array('variable' => '_ALL_POSTDATA', 'value' => '_ALL_POSTDATA');
		$page["var"][] = array('variable' => 'listLog', 'value' => 'listLog');
		$page["var"][] = array('variable' => 'id', 'value' => 'id');
		
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-order-followup.html'); 
    
    }else{
        $messages->setErrorMessage("Records not found.");
    }
	
}else{
    $messages->setErrorMessage("You do not have this access"); 
}

?>