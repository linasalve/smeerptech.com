<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/sale-quick.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
	
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	$ajx 			= isset($_GET["ajx"])         ? $_GET["ajx"]        : ( isset($_POST["ajx"])  ? $_POST["ajx"]       :'0');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   :'0');
    $condition_url = $condition_query = '';
    
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if($added){
         $messages->setOkMessage("Record has been added successfully.");
    }
    
    //By default search in On
    $_SEARCH["searched"]    = true ;
    if ( $perm->has('nc_sl_ld_qe') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_SALE_LEADS   =>  array( 
                                                            'Company Name'  => 'company_name',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Middle Name'   => 'm_name',
                                                            'Last Name'     => 'l_name',
                                                            //'Pet Name'      => 'p_name',
                                                            'Designation'   => 'desig',
                                                            'Organization'  => 'org'
                                                        ),
                                TABLE_USER   =>  array( 
                                                            'Added By First Name'  => 'f_name-'.TABLE_USER,
                                                            'Added By Last Name'   => 'l_name-'.TABLE_USER
                                                        ),
                            );
        
       $sOrderByArray  = array(
                                TABLE_SALE_LEADS => array(
                                                    'E-mail'        => 'email',
                                                    'First Name'    => 'f_name',
                                                    'Last Name'     => 'l_name',
                                                    'Date of Birth' => 'do_birth',
                                                    'Date of Add'=> 'do_add',
                                                    'Status'        => 'status'
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_add';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SALE_LEADS;
        }

        // Read the available Status for the Pre Order.
       // $variables['status']        = Lead::getStatus();
        $variables['status']        = Quick::getLeadStatus();
		
         
		
		
		// Read the available users
       	//LeadsRivals::getUsers($db,$users);
		
		// Read the available source
       	Quick::getSource($db,$source);
		
		// Read the available industry
       	//LeadsRivals::getIndustry($db,$industry);
		
		// Read the available campaign
       	//LeadsRivals::getCampaign($db,$campaign);
		
		// Read the available rivals
       	//LeadsRivals::getRivals($db,$rivals);
		
        //use switch case here to perform action. 
        switch ($perform) {
        
            case ('add_quick'):
            case ('add'): {
				 //$start['time'] = time();
				 echo "1--->".$start['microtime'] = microtime();
                include (DIR_FS_NC.'/sale-quick-add.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quick.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
				 echo "1--->".$start['microtime'] = microtime();
                break;
            }
            case ('edit_quick'):
            case ('edit'): {
                include (DIR_FS_NC .'/sale-quick-edit.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quick.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            }
            /*
            case ('add_pre_order'):
            case ('add'): {
            
                include (DIR_FS_NC.'/bill-invoice-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('edit_pre_order'):
            case ('edit'): {
                include (DIR_FS_NC .'/bill-invoice-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'bill-invoice.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            case ('view_quick'):
            case ('view'): {
                include (DIR_FS_NC .'/sale-quick-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*case ('view_file'): {
                include (DIR_FS_NC .'/bill-invoice-view-file.php');
                break;
            }*/
			case ('change_status_quick'):
            case ('change_status'): {
                include ( DIR_FS_NC .'/sale-quick-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quick.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('search_quick'):
            case ('search'): {
                include(DIR_FS_NC."/sale-quick-search.php");
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quick.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                }
				break;
            }
            case ('delete_quick'):
            case ('delete'): {
                include ( DIR_FS_NC .'/sale-quick-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quick.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
			case ('list_quick'):
            case ('list'):
            default: {
                include (DIR_FS_NC .'/sale-quick-list.php');
                // CONTENT = CONTENT
				if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quick.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'sale-quick.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("variables", $variables);
	$s->assign("source", $source);
	//$s->assign("industry", $industry);
	//$s->assign("campaign", $campaign);
	//$s->assign("rivals", $rivals);
	//$s->assign("users", $users);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());


  
	if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");

?>