<?php
	
	if(!@constant("THIS_DOMAIN")){
		require_once("../lib/config.php"); 
	} 

	 page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));

	// include files
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/web-access.inc.php' );
   // include_once ( DIR_FS_CLASS .'/Validation.class.php' );
    
	//$page = 'web-access-groups.html';
	$msg = "";
	
	if($perm->has('nc_wb_ac_gr_ls'))
	{
		$catname 	= isset($_GET["catname"]) 	? $_GET["catname"] 	: (isset($_POST["catname"])	? $_POST["catname"] : '');
		$parentID 	= isset($_GET["parentID"]) 	? $_GET["parentID"] : (isset($_POST["parentID"])? $_POST["parentID"]: '');
		$cat_id 	= isset($_GET["cat_id"]) 	? $_GET["cat_id"] 	: (isset($_POST["cat_id"])	? $_POST["cat_id"] 	: '');
		$new_cat 	= isset($_GET["new_cat"]) 	? $_GET["new_cat"] 	: (isset($_POST["new_cat"])	? $_POST["new_cat"] : '');
		$move_id 	= isset($_GET["move_id"]) 	? $_GET["move_id"] 	: (isset($_POST["move_id"])	? $_POST["move_id"] : '');
		$related 	= isset($_GET["related"]) 	? $_GET["related"] 	: (isset($_POST["related"])	? $_POST["related"] : '');
	
		if(isset($_POST["submit"])) 
		{
            if($_POST["submit"]=="Create") {
                if($perm->has('nc_wb_ac_gr_add'))
                {
                    if( empty($catname) ){
                        $messages->setErrorMessage("category name cannot be empty.");
                    }
                    if ( $messages->getErrorMessageCount() <= 0 ) 
                    {
                        // Create a new book_cat
                        //Check whether that book_cat exists or not
                        $catname=trim($catname);
                    
                        //echo $query="SELECT * FROM ".TABLE_WEB_ACCESS_GROUPS." WHERE CatName='$catname' AND parentID='$parentID'";
                        //$db->query($query);
                    
                        if($db->affected_rows()<>0) 
                        {
                            $msg = "Category '$catname' already exists!";
                            $messages->setErrorMessage($msg);						
                        }
                        else
                        {
                            //Category name is unique, insert it.....
                            $query = "INSERT INTO ". TABLE_WEB_ACCESS_GROUPS
                                        ."(CatName,parentID) "
                                        ." VALUES ('". $catname ."','". $parentID ."')";
                            $db->query($query);
                            $msg = "Category '$catname' successfully created!";
                            $messages->setOkMessage($msg);
                        }
                        
                    }
                    //$page = 'web-access-groups.html';
                }
                else{
                    //$page = 'no-add-permission.html';
                    $messages->setErrorMessage("You donot have the Permisson to Access this module.");
                }
		    }else if($_POST["submit"]=="Delete") {
                if($perm->has('nc_wb_ac_gr_delete'))
                {
                    $query = " SELECT * FROM ".TABLE_WEB_ACCESS_LIST
                            ." WHERE group_id='". $cat_id ."'";
                    $db->query($query);
                    if($db->nf() > 0)
                    {
                        $msg = "Sorry !!, Selected Category contains IP Range. Please delete IP under this category to delete it.";
                        $messages->setErrorMessage($msg);
                    }
                    else
                    {
                        $query="DELETE FROM ".TABLE_WEB_ACCESS_GROUPS." WHERE catID='". $cat_id ."'";
                        $db->query($query);
                        $msg = "Category successfully deleted!";
                        $messages->setOkMessage($msg);
                    }
                   // $page = 'web-access-groups.html';
                }
                else{
                    //$page = 'no-delete-permission.html';
                    $messages->setErrorMessage("You donot have the Permisson to Access this module.");
                }
			
		    }
            else if($_POST["submit"]=="Edit") 
            {
                if($perm->has('nc_wb_ac_gr_edit'))
                {
                    $query = "SELECT * FROM ".TABLE_WEB_ACCESS_GROUPS." WHERE catID='$cat_id'";
                    $db->query($query);
                    $db->next_record();
                    $parent = $db->f("parentID");
                    if(is_array($related)) {
                        $r_cats = implode(",",$related);
                    } else {
                        $r_cats="";
                    }
                
                    if(empty($new_cat)) {
                        $query="UPDATE ".TABLE_WEB_ACCESS_GROUPS." SET related_cats='$r_cats' WHERE catID='$cat_id'";
                        $db->query($query);
                    } else {
                        $query = "SELECT * FROM ".TABLE_WEB_ACCESS_GROUPS." WHERE parentID='$parent' AND CatName='$new_cat'";
                        $db->query($query);
                        if($db->affected_rows()<>0) {
                            $msg = "Category with this name already exists!";
                            $messages->setOkMessage($msg);
                        } else {
                            $query="UPDATE ".TABLE_WEB_ACCESS_GROUPS." SET CatName='$new_cat',related_cats='$r_cats' WHERE catID='$cat_id'";
                            $db->query($query);
                        }
                    }
                }
                else{
                    //$page = 'no-edit-permission.html';
                    $messages->setErrorMessage("You donot have the Permisson to Access this module.");
                }
            }else if($_POST["submit"] == "Move") {
                if($perm->has('nc_wb_ac_gr_edit'))
                {
                    $extra = array( 'db'        => &$db,
                                    'messages'  => &$messages
                                  );
                    
                    if ( !isset($move_id) || $move_id == '' ) {
                        $messages->setErrorMessage('The Parent Id cannot be empty');
                    }
                    elseif ( $move_id == $cat_id ) {
                        $messages->setErrorMessage('A category cannot be moved under itself');
                    }
                    elseif ( Webaccess::isWebAccessGroupChild($move_id, $cat_id, $extra) ) {
                        $messages->setErrorMessage('A category cannot be moved under its sub-tree.');
                    }
                    
                    if ( $messages->getErrorMessageCount() <= 0 ) {
                        $query = "UPDATE ".TABLE_WEB_ACCESS_GROUPS." SET parentID='".$move_id."' WHERE catID='".$cat_id."'";
                        if ( $db->query($query) ) {
                            $messages->setOkMessage("Category has been moved.");
                        }
                        else {
                            $messages->setOkMessage("Category was not moved.");
                        }
                    }
                    //$page = 'web-access-groups.html';
                }
                else{
                    //$page= 'no-edit-permission.html';
                    $messages->setErrorMessage("You donot have the Permisson to Access this module.");
                }
            } 
            else {
                //What are you doing here?
                // This should never be reached!
                //exit();
                
               // $page = 'web-access-groups.html';
            }
		
		}
		else {
			//What are you doing here?
			// This should never be reached!
			//exit();
			
            //$page = 'web-access-groups.html';
		}
		
		
		$option="";
		
		// output is a global variable defined in the function below _getAllCategory()
		$output = "";
		Webaccess::getAllWebAccessGroups();
        if ( !empty($output) ) {
            foreach($output as $key => $val) {
                $option.="<option value='$key'>$val</option>";
            }
        }
        //$page["section"][] = array('container'=>'CONTENT', 'page' => 'web-access-groups.html');
        //$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
         $page["section"][] = array('container'=>'CONTENT', 'page' => 'web-access-groups.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
	}
	else
	{
        //$messages->setErrorMessage("You do not have the permission to view the list.");
		//$page = 'no-read-permission.html';
		//$display_page = 'index.html';
       
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'web-access-groups.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
       
	}
    
    $variables["option"] = $option;
    
	/*
	
	$variables["msg"] = $msg;
	
	// assign variables to smarty
	$s->assign("error_messages", $messages->getErrorMessages());
	$s->assign("success_messages", $messages->getOkMessages());
	
	$s->assign("variables",$variables);
	$s->assign("CONTENT",$s->fetch($page));
	$s->display("home.html");
    */
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>
