<?php
if ( $perm->has('nc_itm_p_edit') ) { 
		include_once (DIR_FS_INCLUDES .'/items-type.inc.php');	

        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
		$condition_query = "WHERE ".TABLE_ITEMS_TYPE.".status='".ItemsType::ACTIVE."'";
		$fields="*";
        ItemsType::getDetails( $db, $item_type_list, $fields, $condition_query);
		
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }
		*/
        include_once (DIR_FS_INCLUDES .'/items-purchase.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( ItemsPurchase::validateUpdate($data, $extra) ) {
              
                if(!empty($data['warranty_dt'])){
                    $data['warranty_dt'] = explode('/', $data['warranty_dt']);
                    $data['warranty_dt'] = mktime(0, 0, 0, $data['warranty_dt'][1], $data['warranty_dt'][0], $data['warranty_dt'][2]);
                    $data['warranty_dt'] = date('Y-m-d H:i:s', $data['warranty_dt']);
                }
					if(isset($data['in_used'])){
						$in_used =1 ;
					}else{
						$in_used =0 ;
					}
					if(isset($data['is_sold'])){
						$is_sold =1 ;
					}else{
						$is_sold =0 ;
					}
					$family_linking='';
				
					if(!empty($data['family_linking'])){
						$family_linking = explode(",",$data['family_linking']);
						$family_linking = ",".$family_linking.",";
					}
					
					$query	= " UPDATE  ".TABLE_ITEMS_PURCHASE
							." SET ".TABLE_ITEMS_PURCHASE .".item_name = '".$data['item_name'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".item_type_id = '".$data['item_type_id'] ."'"     
							.",". TABLE_ITEMS_PURCHASE .".item_description = '".$data['item_description'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".in_used = '".$in_used ."'"             
							.",". TABLE_ITEMS_PURCHASE .".is_sold = '".$is_sold ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".sold_to_name = '".$data['sold_to_name'] ."'"                                						
							.",". TABLE_ITEMS_PURCHASE .".model_no = '".$data['model_no'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".serial_no = '".$data['serial_no'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".batch_no = '".$data['batch_no'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".brand = '".$data['brand'] ."'"                                
							.",". TABLE_ITEMS_PURCHASE .".warranty_dt = '".$data['warranty_dt'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".vendor_id = '".$data['vendor_id'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".pp_bill_id = '".$data['pp_bill_id'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".pp_bill_description = '".$data['pp_bill_description'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".price = '".$data['price'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".location = '".$data['location'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".family_linking = '".$family_linking ."'" 												
							//.",". TABLE_ITEMS_PURCHASE .".status = '". 		$data['status'] ."'" 
							.",". TABLE_ITEMS_PURCHASE .".updated_by = '".     $my['user_id'] ."'"
							.",". TABLE_ITEMS_PURCHASE .".updated_by_name = '".$my['f_name']." ".$my['l_name']."'"
							.",". TABLE_ITEMS_PURCHASE .".do_u   = '". 		date('Y-m-d H:i:s')."'" 
							." WHERE ".TABLE_ITEMS_PURCHASE.".id = '". $id ."'";
							
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_ITEMS_PURCHASE .'.*'  ;            
            $condition_query = " WHERE (". TABLE_ITEMS_PURCHASE .".id = '". $id ."' )";
            
            if ( ItemsPurchase::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                 if(isset($_ALL_POST['warranty_dt']) && $_ALL_POST['warranty_dt']!='0000-00-00 00:00:00'){
                    $temp  =null;
                    $_ALL_POST['warranty_dt']  = explode(' ', $_ALL_POST['warranty_dt']);
                    $temp               = explode('-', $_ALL_POST['warranty_dt'][0]);
                    $_ALL_POST['warranty_dt']  = NULL;
                    $_ALL_POST['warranty_dt']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                }else{
                    $_ALL_POST['warranty_dt']  = '';                            
                }                    
				if(!empty($_ALL_POST['family_linking'])){
					
					$str_fl=trim(",",$_ALL_POST['family_linking']);
					$family_link_str = str_replace(",","','",$_ALL_POST['family_linking']);
					$fields = TABLE_ITEMS_PURCHASE.".item_name,".TABLE_ITEMS_PURCHASE.".code";
					$condition = " WHERE ".TABLE_ITEMS_PURCHASE.".id IN (".$family_link_str.")" ;
					ItemsPurchase::getDetails( $db, $family_link, $fields, $condition_query);
				}
					// Setup the date of delivery.
					/*
                    $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                    $temp               = explode('-', $_ALL_POST['do_e'][0]);
                    $_ALL_POST['do_e']  = NULL;
                    $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];*/
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/items-purchase-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $variables['INUSED'] = ItemsPurchase::INUSED ;
			$variables['DEAD'] = ItemsPurchase::DEAD ;
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'item_type_list', 'value' => 'item_type_list');   
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'items-purchase-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
