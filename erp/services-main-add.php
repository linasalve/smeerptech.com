<?php

    if ( $perm->has('nc_sr_add') ) {

        $_ALL_POST	= NULL;
        $data       = NULL;


        $services = NULL;
		Services::getParent($db,$services);
        
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);
        
        $divisions = NULL;
        $required_fields ='*';
        $condition_query = "WHERE status='".ServiceDivision::ACTIVE."'";
		ServiceDivision::getList($db,$divisions,$required_fields,$condition_query);
        
        $_ALL_POST['ss_parent_id']=0;
        $subtax_list    = NULL;
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
        
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            $tax_arr = split('#',$data['tax1_name']);    
            $data['tax1_name'] = $tax_arr['0'];
            $_ALL_POST['tax1_id'] =$data['tax1_id'] = $tax_arr['1'];
            
			if(isset($data['tax1_sub1_name'])){
				$tax_arrs1 = split('#',$data['tax1_sub1_name']);    
				$data['tax1_sub1_name'] = $tax_arrs1['0'];
				$_ALL_POST['tax1_sub1_id'] =$data['tax1_sub1_id'] = $tax_arrs1['1'];
			}
			if(isset($data['tax1_sub2_name'])){ 
				$tax_arrs2 = split('#',$data['tax1_sub2_name']);    
				$data['tax1_sub2_name'] = $tax_arrs2['0'];
				$_ALL_POST['tax1_sub2_id'] =$data['tax1_sub2_id'] = $tax_arrs2['1'];
			}
			    
			$subtax_list = array();
			if(!empty($data['tax1_id'])){
				$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$data['tax1_id']." ORDER BY tax_name ASC";
				ServiceTax::getList($db, $subtax_list, 'id,tax_name', $condition_queryst);
			}
			
			
            if ( Services::validateAdd($data, $extra) ) {
                
				$data['number'] = Services::getNewAccNumber($db);
			   $query	= " INSERT INTO ". TABLE_SETTINGS_SERVICES
                        ." SET ". TABLE_SETTINGS_SERVICES .".ss_title     		  = '". $data['ss_title'] ."'"
							.",". TABLE_SETTINGS_SERVICES .".number       = '". $data['number'] ."'"
                            //.",". TABLE_SETTINGS_SERVICES .".ss_order_tpl      	  = '". $data['ordertpl'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".ss_parent_id      	  = '". $data['ss_parent_id'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".ss_division      	  = '". $data['ss_division'] ."'"
                            //.",". TABLE_SETTINGS_SERVICES .".ss_price      	      = '". $data['ss_price'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".ss_punch_line        = '". $data['ss_punch_line'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".is_renewable        = '". $data['is_renewable'] ."'"
							//.",". TABLE_SETTINGS_SERVICES .".ss_client_detail 	  = '". $data['clientdetails'] ."'"
					        .",". TABLE_SETTINGS_SERVICES .".tax1_name    		  = '". $data['tax1_name'] ."'"
					        .",". TABLE_SETTINGS_SERVICES .".tax1_id    		  = '". $data['tax1_id'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".tax1_value    		  = '". $data['tax1_value'] ."'"
							.",". TABLE_SETTINGS_SERVICES .".tax1_sub1_name    	  = '". $data['tax1_sub1_name'] ."'"
					        .",". TABLE_SETTINGS_SERVICES .".tax1_sub1_id         = '". $data['tax1_sub1_id'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".tax1_sub1_value      = '". $data['tax1_sub1_value'] ."'"
							.",". TABLE_SETTINGS_SERVICES .".tax1_sub2_name    	  = '". $data['tax1_sub2_name'] ."'"
					        .",". TABLE_SETTINGS_SERVICES .".tax1_sub2_id         = '". $data['tax1_sub2_id'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".tax1_sub2_value      = '". $data['tax1_sub2_value'] ."'"
							.",". TABLE_SETTINGS_SERVICES .".sequence_services    	  = '". $data['sequence_services'] ."'"
							.",". TABLE_SETTINGS_SERVICES .".ss_status    		  = '". $data['ss_status'] ."'"
                            .",". TABLE_SETTINGS_SERVICES .".ss_date   		 	  = '". date('Y-m-d') ."'";
                            
                if ($db->query($query) && $db->affected_rows() > 0){
                
                    $variables['hid'] =$variables['service_id'] =  $db->last_inserted_id();
					$messages->setOkMessage("The New Service has been added.");
				    // Insert the address.
                    if(isset($data['service_price']) && !empty($data['service_price'])){
                        for ( $i=0; $i < count($data['service_price']); $i++ ) {
                         
                              $sql	= " INSERT INTO ". TABLE_SETTINGS_SERVICES_PRICE
                                ." SET ". TABLE_SETTINGS_SERVICES_PRICE .".service_price      = '". $data['service_price'][$i] ."'"
                                .",". TABLE_SETTINGS_SERVICES_PRICE .".currency_id      	     = '". $data['currency_id'][$i] ."'"
                                .",". TABLE_SETTINGS_SERVICES_PRICE .".service_id   		 	 = '". $variables['service_id'] ."'";
                                $db->query($sql);
                        }
                    }
                   
                    
                }else{
					$messages->setErrorMessage('Problem saving record. Try later');
                }
                
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
            $_ALL_POST	= $data;
        }
        
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/services-main.php?perform=add&added=1&hid=".$variables['hid']);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/services-main.php?perform=list");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/services-main.php?added=1&hid=".$variables['hid']);   
            
        }else{
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'subtax_list', 'value' => 'subtax_list');
            $page["var"][] = array('variable' => 'services', 'value' => 'services');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["var"][] = array('variable' => 'divisions', 'value' => 'divisions');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'services-main-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>