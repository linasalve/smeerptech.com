<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ){
				
                    foreach( $field_arr as $key => $field ) {
                            $condition_query .= " ". $table .".". clearSearchString($field,'-') ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                    }   
                }
            }
            $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType,'-') ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
    $sStatusStr='';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' ) && isset($sStatus)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
       $condition_query .= " ". TABLE_LETTERS .".status IN ('". $sStatusStr ."') ";
       $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";   
       $_SEARCH["chk_status"]  = $chk_status;
       $_SEARCH["sStatus"]     = $sStatus;
    }
    // EO: Status data.
    //Client serach bof
	$client = isset($_POST["client"])   ? $_POST["client"]  : (isset($_GET["client"])   ? $_GET["client"]   :'');
	
	if(!empty($client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_LETTERS .".client_id ='". $client ."' ";        
        $condition_url          .= "&client=$client";
		$table = TABLE_CLIENTS;
		$fields1 =  TABLE_CLIENTS .'.f_name,'.TABLE_CLIENTS.'.l_name,'.TABLE_CLIENTS.'.billing_name' ;
		$condition1 = " WHERE ".TABLE_CLIENTS .".user_id ='".$client."' " ;
		$arr = getRecord($table,$fields1,$condition1);
		if(!empty($arr)){
			$client_details = $arr['f_name']." ".$arr['l_name']." (".$arr['billing_name'].")";
		}
		$_SEARCH["client_details"]  = $client_details;
        $_SEARCH["client"]     = $client; 
	}
	//Client serach eof
    
    //Bank/CAs serach bof
	$vendor_bank_id = isset($_POST["vendor_bank_id"])   ? $_POST["vendor_bank_id"]  : (isset($_GET["vendor_bank_id"])   ? $_GET["vendor_bank_id"]   :'');
	 
	
	if(!empty($vendor_bank_id)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_LETTERS .".vendor_bank_id ='". $vendor_bank_id ."' ";        
        $condition_url  .= "&vendor_bank_id=$vendor_bank_id";
        $_SEARCH["vendor_bank_id"]  = $vendor_bank_id;
		
		$table = TABLE_VENDORS_BANK;
		$fields1 =  TABLE_VENDORS_BANK .'.f_name,'.TABLE_VENDORS_BANK.'.l_name,'.TABLE_VENDORS_BANK.'.billing_name' ;
		$condition1 = " WHERE ".TABLE_VENDORS_BANK .".user_id ='".$vendor_bank_id."' " ;
		$arr = getRecord($table,$fields1,$condition1);
		if(!empty($arr)){
			$vendor_bank_details = $arr['f_name']." ".$arr['l_name']." (".$arr['billing_name'].")";
		}		 
        $_SEARCH["vendor_bank_details"]     = $vendor_bank_details;       
	
	}
	//Bank/CAs  serach eof
    //Executive serach bof
	$executive_id = isset($_POST["executive_id"])   ? $_POST["executive_id"]  : (isset($_GET["executive_id"])   ? $_GET["executive_id"]   :'');
	 
	
	if(!empty($executive_id)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_LETTERS .".executive_id ='". $executive_id ."' ";        
        $condition_url  .= "&executive_id=$executive_id";
        $_SEARCH["executive_id"]  = $executive_id;
		
		$table = TABLE_USER;
		$fields1 =  TABLE_USER .'.f_name,'.TABLE_USER.'.l_name' ;
		$condition1 = " WHERE ".TABLE_USER .".user_id ='".$client."' " ;
		$arr = getRecord($table,$fields1,$condition1);
		if(!empty($arr)){
			$executive_details = $arr['f_name']." ".$arr['l_name']." ";
		}	
        $_SEARCH["executive_details"] = $executive_details;       
	
	}
	//Executive  serach eof
	
	
	
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
  
    // BO: From Date
    if (( $chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_LETTERS .".".$dt_field." >= '". $dfa ."'";
        
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
		 $_SEARCH["dt_field"]       = $dt_field;
    }
    // EO: From Date
    
    // BO: Upto Date
    if (( $chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_LETTERS .".".$dt_field." <= '". $dta ."'";        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
		 $_SEARCH["dt_field"]       = $dt_field;
    }
    
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
    include ( DIR_FS_NC .'/letters-list.php');
?>
