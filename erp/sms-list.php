<?php

    if ( $perm->has('nc_sms_lst_list') ) {
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $condition_query1 = "LEFT JOIN ". TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id= ".TABLE_SMS_HISTORY.".sh_userid ".$condition_query;
        // To count total count records.
        $list	= 	NULL;
        $total	=	smsList::getList( $db, $list, 'sh_id', $condition_query1);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields ='';
        $fields .= TABLE_SMS_HISTORY.".*".",".TABLE_CLIENTS.".f_name".",".TABLE_CLIENTS.".l_name";
        
        smsList::getList( $db, $list, $fields, $condition_query1, $next_record, $rpp);
    
        // Set the Permissions.
        $variables['can_view_list']     = false;

        if ( $perm->has('nc_sms_lst_list') ) {
            $variables['can_view_list']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>