<?php
if ( $perm->has('nc_sl_e_list') ) {

    include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
    include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
        
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
     
	
    // To count total records.
    $list	= 	NULL;
    $total	=	SaleLeadEnquiry::getList( $db, $list, '', $condition_query);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_SALE_LEADS_ENQUIRY.'.*,'.TABLE_SALE_LEADS.".f_name,".TABLE_SALE_LEADS.".l_name, ".TABLE_SALE_LEADS.".company_name ";
    SaleLeadEnquiry::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
   
    $flist = array();
    if(!empty($list)){
        foreach( $list as $key=>$val){
			$val['address_list'] = '';
			$val['phone'] = '';
            
			if(!empty($val['lead_id'])){
				$val['address_list'] = '';
				$val['phone'] = '';
				$phone   = new PhoneLead(TABLE_SALE_LEADS);
				$region  = new RegionLead(TABLE_SALE_LEADS);
				
				$phone->setPhoneOf(TABLE_SALE_LEADS, $val['lead_id']);
                $val['phone'] = $phone->get($db);
                
                // Read the Addresses.
                $region->setAddressOf(TABLE_SALE_LEADS, $val['lead_id']);
                $val['address_list'] = $region->get();
           
			}
			
			
             $flist[$key]=$val;
            
        }
    }
    
    
    
    // Set the Permissions.
   
    if ( $perm->has('nc_sl_e_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_sl_e_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_sl_e_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_sl_e_delete') ) {
        $variables['can_delete'] = true;
    }
    
    if ( $perm->has('nc_sl_e_status') ) {
        $variables['can_change_status']     = true;
    }
	
    
    $page["var"][] = array('variable' => 'list', 'value' => 'flist');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
	
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-enquiry-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
