<?php

    if ( $perm->has('nc_wk_tl_list') ) {
		
		$or_id	= isset($_GET["or_id"]) ? $_GET["or_id"] 	: ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
		
		if ( empty($condition_query) ) {
			$condition_query= " WHERE ".TABLE_WORK_TL.".order_id = '$or_id' ";
			$sOrderBy		= 'do_assign';
			$sOrder			= 'DESC';
		}
		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	WorkTimeline::getList( $db, $list, TABLE_WORK_TL.'.id', $condition_query);

		/*if ( $total ) {*/
			//$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $condition_url ="&perform=details&or_id=".$or_id	 ;
			$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
			$extra_url  = '';
			if ( isset($condition_url) && !empty($condition_url) ) {
				$extra_url  = $condition_url;
			}
            
			
			$extra_url  .= "&x=$x&rpp=$rpp";
			$extra_url  = '&start=url'. $extra_url .'&end=url';
    
			// Include the Order Status class.
			include_once (DIR_FS_INCLUDES .'/work-stage.inc.php');
			$lst_work_stage = NULL;
			WorkStage::getWorkStages($db, $lst_work_stage);

			$list	= NULL;
			WorkTimeline::getDisplayList( $db, $list, '*', $condition_query, $next_record, $rpp);
			//WorkTimeline::getDisplayList( $db, $list, '*', $condition_query );
            $fList=array();
            if(!empty($list)){
                foreach( $list as $key=>$val){  
                
                   $files=array();
                   $filenames='';
                   $files = explode(",",$val['attach_files']);
                   $i=1;                           
                   foreach($files as $key1=>$val1){
                        if(!empty($val1)){
                            
                            
                            $filenames .= "<a href=javascript:void(0); onclick=javascript:showFileAttachment('$val1') title='View file' class='file'>file ".$i."</a><br/>" ;
                            $i++;
                        }
                   } 
                   $val['files'] = $filenames ;
                   $fList[$key]=$val;
                }
            }
			// Read the last comment added.
			$last_comment = NULL;
			if ( WorkTimeline::getLastComment($db, $last_comment, $or_id) ) {
				
				// Read the Order Details.
				$order_details = NULL;
				$extra = array( 'db'        => &$db,
								'messages'  => $messages
							);
				WorkTimeline::getWorkDetails($last_comment[0]['id'], $order_details, $extra);
			}
            
			if(empty($last_comment)){
                $order_details = NULL;
                $extra = array( 'db'        => &$db,
								'messages'  => $messages
							);
                WorkTimeline::getOrderDetails($or_id, $order_details, $extra);   
            }
            
			// These parameters will be used when returning the control back to the List page.
			if ( isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) ) {
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}
			}
            
			$hidden[] = array('name'=> 'perform','value' => 'change_stage');
			$hidden[] = array('name'=> 'act', 'value' => 'save');
			$hidden[] = array('name'=> 'id', 'value' => $last_comment[0]['id']);
			$hidden[] = array('name'=> 'or_id','value' => $or_id);
			
			// Set the Permissions.
			$variables['can_view_list']     = false;
			$variables['can_add']           = false;
			$variables['can_edit']          = false;
			$variables['can_delete']        = false;
			$variables['can_view_details']  = false;
			$variables['can_change_status'] = false;
	
			if ( $perm->has('nc_wk_tl_list') ) {
				$variables['can_view_list'] = true;
			}
			if ( $perm->has('nc_wk_tl_add') ) {
				$variables['can_add'] = true;
			}
			if ( $perm->has('nc_wk_tl_edit') ) {
				$variables['can_edit'] = true;
			}
			if ( $perm->has('nc_wk_tl_delete') ) {
				$variables['can_delete'] = true;
			}
			if ( $perm->has('nc_wk_tl_details') ) {
				$variables['can_view_details'] = true;
			}
			if ( $perm->has('nc_wk_tl_status') ) {
				$variables['can_change_status'] = true;
			}
			
			$page["var"][] = array('variable' => 'list', 'value' => 'fList');
			$page["var"][] = array('variable' => 'order_details', 'value' => 'order_details');
			$page["var"][] = array('variable' => 'lst_work_stage', 'value' => 'lst_work_stage');
			$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
			$page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        	$page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
		/*}
		else {
			$messages->setErrorMessage("The Timeline comments were not found for this order.");
		}*/
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'work-timeline-details.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>