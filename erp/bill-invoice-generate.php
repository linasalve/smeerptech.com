<?php
    if ( $perm->has('nc_bl_inv_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        
        $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $or_no          = isset($_GET["or_no"]) ? $_GET["or_no"] : ( isset($_POST["or_no"]) ? $_POST["or_no"] : '' );

        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $lst_currency   = Currency::getCurrency();        
        $currency = NULL;
        $required_fields ='*';
        Currency::getList($db,$currency,$required_fields);
		
		$invDetails =null;
		$condition_query1 = " WHERE ".TABLE_BILL_INV.".id = '".$inv_id."'";
		$fields1 = TABLE_BILL_INV.".currency_abbr,".TABLE_BILL_INV.".currency_name,
		".TABLE_BILL_INV.".currency_symbol,".TABLE_BILL_INV.".currency_country,
		".TABLE_BILL_INV.".do_st,
		".TABLE_BILL_INV.".do_i,
		".TABLE_BILL_INV.".do_d,
		".TABLE_BILL_INV.".do_fe,
		".TABLE_BILL_INV.".do_e,
		".TABLE_BILL_INV.".billing_name,
		".TABLE_BILL_INV.".number,
		".TABLE_BILL_INV.".amount,
		".TABLE_BILL_INV.".amount_inr,
		".TABLE_BILL_INV.".amount_words,
		".TABLE_BILL_INV.".balance,
		".TABLE_BILL_INV.".or_no,
		".TABLE_BILL_INV.".client,
		".TABLE_BILL_INV.".round_off_op,
		".TABLE_BILL_INV.".round_off ,
		".TABLE_BILL_INV.".company_quot_prefix,				
		".TABLE_BILL_INV.".company_name,
		".TABLE_BILL_INV.".b_addr_company_name,".TABLE_BILL_INV.".b_addr,
		".TABLE_BILL_INV.".b_addr_city,".TABLE_BILL_INV.".b_addr_state,
		".TABLE_BILL_INV.".b_addr_country,".TABLE_BILL_INV.".b_addr_zip, 
		".TABLE_BILL_INV.".tin_no, 
		".TABLE_BILL_INV.".cst_no,".TABLE_BILL_INV.".vat_no,".TABLE_BILL_INV.".do_tax,
		".TABLE_BILL_INV.".service_tax_regn,".TABLE_BILL_INV.".pan_no,
		".TABLE_BILL_INV.".delivery_at,
		".TABLE_BILL_INV.".show_period,
		".TABLE_BILL_INV.".tax1_declaration,
		".TABLE_BILL_INV.".sub_total_amount,".TABLE_BILL_INV.".tax1_id,
		".TABLE_BILL_INV.".tax1_number,".TABLE_BILL_INV.".tax1_name,
		".TABLE_BILL_INV.".tax1_value,".TABLE_BILL_INV.".tax1_pvalue,
		".TABLE_BILL_INV.".tax1_total_amount,
		".TABLE_BILL_INV.".tax1_sub1_id,".TABLE_BILL_INV.".tax1_sub1_number,
		".TABLE_BILL_INV.".tax1_sub1_name,".TABLE_BILL_INV.".tax1_sub1_value,
		".TABLE_BILL_INV.".tax1_sub1_pvalue,".TABLE_BILL_INV.".tax1_sub1_total_amount,				
		".TABLE_BILL_INV.".tax1_sub2_id,".TABLE_BILL_INV.".tax1_sub2_number,
		".TABLE_BILL_INV.".tax1_sub2_name,".TABLE_BILL_INV.".tax1_sub2_value,
		".TABLE_BILL_INV.".tax1_sub2_pvalue,".TABLE_BILL_INV.".tax1_sub2_total_amount	";
		
		Invoice::getList( $db, $invDetails, $fields1, $condition_query1);
		$data['currency_abbr'] = '';
		$data['currency_name'] = '';
		$data['currency_symbol'] ='';
		$data['currency_country'] ='';
		$data['b_addr_company_name'] ='';
		$data['b_addr'] ='';
		$data['b_addr_city'] ='';
		$data['b_addr_state'] ='';
		$data['b_addr_country'] ='';
		$data['b_addr_zip'] ='';
		
		if(!empty($invDetails)){
		   $invDetails=$invDetails[0];
		   
		   $data['do_st'] = $invDetails['do_st'];
		   $data['do_i'] = $invDetails['do_i'];
		   $data['do_d'] = $invDetails['do_d'];
		   $data['do_fe'] = $invDetails['do_fe'];
		   $data['do_e'] = $invDetails['do_e'];
		   $data['show_period'] = $invDetails['show_period'];
		   $data['billing_name'] = $invDetails['billing_name'];
		   $data['number'] = $invDetails['number'];
		   $data['or_no'] = $invDetails['or_no'];
		   $data['amount'] = $invDetails['amount'];
		   $data['amount_inr'] = $invDetails['amount_inr'];
		   $data['amount_words'] = $invDetails['amount_words'];
		   $data['balance'] = $invDetails['balance'];
		   $data['client']['user_id'] = $invDetails['client'];
		   $data['currency_abbr'] = $invDetails['currency_abbr'];
		   $data['currency_name'] = $invDetails['currency_name'];
		   $data['currency_symbol'] = $invDetails['currency_symbol'];
		   $data['currency_country'] = $invDetails['currency_country'];
		   $data['round_off_op'] =  $invDetails['round_off_op'];
			$data['round_off'] =  $invDetails['round_off'];
		   $data['company_name'] =  $invDetails['company_name'];
		   $data['company_quot_prefix'] =  $invDetails['company_quot_prefix'];
		   $data['b_addr_company_name'] =  $invDetails['b_addr_company_name'];
		   $data['b_addr'] =  $invDetails['b_addr'];
		   $data['b_addr_city'] =  $invDetails['b_addr_city'];
		   $data['b_addr_state'] =  $invDetails['b_addr_state'];
		   $data['b_addr_country'] =  $invDetails['b_addr_country'];
		   $data['b_addr_zip'] =  $invDetails['b_addr_zip'];		   
		   $data['tax1_declaration'] =  $invDetails['tax1_declaration'];			
			$data['sub_total_amount'] = number_format($invDetails['sub_total_amount'],2);
			$data['tax1_id'] 	 =  $invDetails['tax1_id'];
			$data['tax1_number'] =  $invDetails['tax1_number'];
			$data['tax1_name']   =  $invDetails['tax1_name'];
			$data['tax1_name']   =  $invDetails['tax1_name'];
			$data['tax1_value']  =  $invDetails['tax1_value'];
			$data['tax1_pvalue'] =  $invDetails['tax1_pvalue'];
			$data['tax1_total_amount']  = number_format($invDetails['tax1_total_amount'],2);
			
			$data['tax1_sub1_id'] =  $invDetails['tax1_sub1_id'];
			$data['tax1_sub1_number'] =  $invDetails['tax1_sub1_number'];
			$data['tax1_sub1_name'] =  $invDetails['tax1_sub1_name'];
			$data['tax1_sub1_value'] =  $invDetails['tax1_sub1_value'];
			$data['tax1_sub1_pvalue'] =  $invDetails['tax1_sub1_pvalue'];
			$data['tax1_sub1_total_amount'] = number_format($invDetails['tax1_sub1_total_amount'],2);
			
			$data['tax1_sub2_id'] =  $invDetails['tax1_sub2_id'];
			$data['tax1_sub2_number'] =  $invDetails['tax1_sub2_number'];
			$data['tax1_sub2_name'] =  $invDetails['tax1_sub2_name'];
			$data['tax1_sub2_value'] =  $invDetails['tax1_sub2_value'];
			$data['tax1_sub2_pvalue'] =  $invDetails['tax1_sub2_pvalue'];
			$data['tax1_sub2_total_amount'] = number_format($invDetails['tax1_sub2_total_amount'],2);
			
			$data['delivery_at'] =  $invDetails['delivery_at'];
			$data['pan_no'] =  $invDetails['pan_no'];
			$data['do_tax'] =  $invDetails['do_tax'];	
		   
			if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
				$data['do_tax']= strtotime($data['do_tax']);				
			}
			if( $data['do_i']!=0 &&  $data['do_i']!='0000:00:00 00:00:00'){
				$data['do_i']= strtotime($data['do_i']);				
			}
			if( $data['do_st']!=0 &&  $data['do_st']!='0000:00:00 00:00:00'){
				$do_st = $data['do_st'];
				$data['do_st'] = strtotime($data['do_st']);				
			}
			if( $data['do_d']!=0 &&  $data['do_d']!='0000:00:00 00:00:00'){
				$data['do_d']= strtotime($data['do_d']);				
			}
			if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){
			   $data['tin_no'] =  $invDetails['tin_no'];
			   $data['service_tax_regn'] =  $invDetails['service_tax_regn'];
			  
			   $data['cst_no'] =  $invDetails['cst_no'];
			   $data['vat_no'] =  $invDetails['vat_no'];
			}
			 
		}
				
				
		//Get billing address BOF
		/*
		include_once ( DIR_FS_CLASS .'/Region.class.php');
		$region         = new Region();
		$region->setAddressOf(TABLE_CLIENTS,  $data['client']['user_id']);
		$addId = $data['billing_address'] ;
		$address_list  = $region->get($addId);
		$data['b_addr_company_name'] = $address_list['company_name'];
		$data['b_addr'] = $address_list['address'];
		$data['b_addr_city'] = $address_list['city_title'];
		$data['b_addr_state'] = $address_list['state_title'];
		$data['b_addr_country'] = $address_list['c_title'];
		$data['b_addr_zip'] = $address_list['zipcode'];
		//$data['b_address'] = $address_list ;
		//Get billing address EOF
		*/
		$show_period=$data['show_period_chk']=0;
		if(isset($data['show_period']) && $data['show_period'] ==1){
			$data['show_period_chk'] = 1;
		}
		//$data['show_period_chk'] = 1;
		
		$data['do_i_chk'] = $data['do_i'];
		$data['do_rs_symbol'] = strtotime('2010-07-20');
		$data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
		$data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
		if(!empty($data['do_e']) && $data['do_e']!='0000-00-00 00:00:00'){
			//$data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
		}else{
			$data['do_e'] = '';
		}
		if(!empty($data['do_fe']) && $data['do_fe']!='0000-00-00 00:00:00'){
			//$data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
		}else{
			$data['do_fe'] = '';
		}                
		$data['invoice_title'] = 'INVOICE';
		$data['profm']='';
		if(!empty($data['tax1_name'])){
			$data['invoice_title']='TAX INVOICE';
		}
		
		/* $query  = " UPDATE ".TABLE_BILL_INV	." SET ". TABLE_BILL_INV .".show_period ='1'"
		." WHERE ".TABLE_BILL_INV.".id ='".$inv_id."'"; */
		
	   if ( $db->query($query) ) {
			$messages->setOkMessage('The Invoice "'. $data['number'] .'" has been Updated.');
			
			$data['amount']= number_format($data['amount'],2);
			$data['amount_inr']= number_format($data['amount_inr'],2);
			$data['balance']= number_format($data['balance'],2);
			$data['amount_words']=processSqlData($data['amount_words']);
			
			$temp = NULL; 
			$temp_p = NULL;
			$condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
			Order::getParticulars($db, $temp, '*', $condition_query);
			$data['show_discount']=0;
			$data['show_nos']=0;
			//$data['colsSpanNo1']=8;
			//$data['colsSpanNo2']=9;
			$data['colsSpanNo1']=5;
			$data['colsSpanNo2']=6;
			if(!empty($temp)){
				 /*
				$data['wd1']=10;
				$data['wdinvp']=217;
				$data['wdpri']=70;
				$data['wdnos']=70;
				$data['wdamt']=70;
				$data['wddisc']=50;
				$data['wdst']=70;
				//$data['wdtx']=40; as three columns are removed so manage its width in other cols
			   // $data['wdtxp']=50;
				//$data['wdtotam']=75;
				$data['wd2']=9;*/
				
				$data['wd1']=10;
				$data['wdinvp']=265; //217+28=245+20
				$data['wdpri']=88;   //70+28=98-10
				$data['wdnos']=88;   //70+28=98-10
				$data['wdamt']=97;   //70+27=97
				$data['wddisc']=77;  //50+27
				$data['wdst']=97;    //70+27                     
				$data['wd2']=9;
			
				foreach ( $temp as $pKey => $parti ) {
					/*
					   if($temp[$pKey]['s_type']=='1'){
							$temp[$pKey]['s_type']='Years';
					   }elseif($temp[$pKey]['s_type']=='2'){
							$temp[$pKey]['s_type']='Quantity';
					   }
					*/
					if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
						$data['show_discount']=1;
						$show_discountNo=1;
					}
					if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
						$data['show_nos']=1;
						$show_nos=1;
					}
					$temp_p[]=array(
						'p_id'=>$temp[$pKey]['id'],
						'particulars'=>$temp[$pKey]['particulars'],
						'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
						's_type' =>$temp[$pKey]['s_type'] ,                                   
						's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
						's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
						's_id' =>$temp[$pKey]['s_id'] ,                                   
						'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
						'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
						'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
						'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
						'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
						'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
						'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,   
						'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,                                   
						'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                   
						'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
						'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,                                   
						'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,
						'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,                                   
						'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                   
						'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
						'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,                                   
						'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,                                
						'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
						'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
						'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
						'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
						'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
					);
				}
				if($data['show_discount'] ==0){ 
					// disc : 50 & subtot : 70
				   /* $data['wdinvp']= $data['wdinvp']+20 + 40;
					$data['wdpri'] = $data['wdpri'] + 5 + 6;
					$data['wdamt'] = $data['wdamt'] + 5+ 6;                            
					$data['wdtx']  = $data['wdtx'] + 5+ 6;
					$data['wdtxp'] = $data['wdtxp'] +10 + 6;
					$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
					*/
					 // disc : 77 & subtot : 97
					$data['wdinvp']= $data['wdinvp']+50 + 70;
					$data['wdpri'] = $data['wdpri'] +10 + 10;
					$data['wdamt'] = $data['wdamt'] + 17+ 17;                            
					//$data['wdtx']  = $data['wdtx'] + 5+ 6;
					//$data['wdtxp'] = $data['wdtxp'] +10 + 6;
					//$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
					
					$data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
					$data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
					
				}
				if($data['show_nos'] ==0){
					// disc : 70
				   /* $data['wdinvp']= $data['wdinvp']+30;
					$data['wdpri'] = $data['wdpri'] + 5;
					$data['wdamt'] = $data['wdamt'] + 5;
					$data['wdtx']  = $data['wdtx'] + 10;
					$data['wdtxp'] = $data['wdtxp'] + 10;
					$data['wdtotam'] =$data['wdtotam'] + 10;
					*/
					// disc :98
					$data['wdinvp']= $data['wdinvp']+80;
					$data['wdpri'] = $data['wdpri'] + 8;
					$data['wdamt'] = $data['wdamt'] + 10;
					/*
					$data['wdtx']  = $data['wdtx'] + 10;
					$data['wdtxp'] = $data['wdtxp'] + 10;
					$data['wdtotam'] =$data['wdtotam'] + 10;*/
					$data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
					$data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;                            
				}
			}
			$data['particulars']=$temp_p ;
			//get invoice details bof
			 //Get details of PAN,ST,VAT,CST BOF
			$table = TABLE_CLIENTS;
			$fields1 =  TABLE_CLIENTS .'.number , 
			'.TABLE_CLIENTS .'.cst_no, '.TABLE_CLIENTS .'.vat_no, 
			'.TABLE_CLIENTS .'. service_tax_no,'.TABLE_CLIENTS .'. pan_no' ;
			$condition1 = " WHERE ".TABLE_CLIENTS .".user_id ='".$data['client']['user_id']."' " ;
			$clientArr = getRecord($table,$fields1,$condition1);
			if(!empty($clientArr)){
				$data['client_cst_no'] = $clientArr['cst_no'];
				$data['client_vat_no'] = $clientArr['vat_no'];
				$data['client_st_no'] = $clientArr['service_tax_no'];
				$data['client_pan_no'] = $clientArr['pan_no'];
				$data['client']['number']=$clientArr['number'];
			}
		
			//Get details of PAN,ST,VAT,CST EOF
			// Create the Invoice PDF, HTML in file.
			$extra = array( 's'         => &$s,
							'messages'  => &$messages
						  );
			$attch =$attch1= '';
			 
			if ( !($attch1 = Invoice::createFile($data, 'HTMLPRINT', $extra)) ) {
				$messages->setErrorMessage("The Invoice print html  file was not created.");
			}
			if ( !($attch = Invoice::createFile($data, 'HTML', $extra)) ) {
				$messages->setErrorMessage("The Invoice file was not created.");
			}
			Invoice::createPdfFile($data); 
			//Invoice::createPdfFileSJM($data); 
			$file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";
			$messages->setOkMessage("The Invoice file was created.");
			
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-generate.html');
		}
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>