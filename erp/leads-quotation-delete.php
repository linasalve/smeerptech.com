<?php
if ( $perm->has('nc_ld_qt_delete') ) {
    $inv_id = isset($_GET["inv_id"]) ? $_GET["inv_id"] : ( isset($_POST["inv_id"]) ? $_POST["inv_id"] : '' );
        
       
    
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        
        
        
        if ( (LeadsQuotation::getList($db, $invoice, TABLE_LEADS_QUOTATION.'.id,'.TABLE_LEADS_QUOTATION.'.or_no, 
		'.	TABLE_LEADS_QUOTATION.'.number, '.TABLE_LEADS_QUOTATION.'.access_level, 
		'.TABLE_LEADS_QUOTATION.'.status ', " WHERE ".TABLE_LEADS_QUOTATION.".id = '$inv_id'")) > 0 ) {
                $invoice = $invoice[0];
                if ( $invoice['status'] != LeadsQuotation::ACTIVE ) {
                    if ( $invoice['status'] == LeadsQuotation::BLOCKED ) {
                         
                    }
                    else {
                        $messages->setErrorMessage("To Delete Quotation ".$invoice['number']." it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("To Delete Quotation ".$invoice['number']." it should be Blocked.");
                }
            }
            else {
                $messages->setErrorMessage("The Quotation ".$invoice['number']." was not found.");
            }
        
        if( isset($_POST['btnCancel'])){
            header("Location:".DIR_WS_NC."/leads-quotation.php?perform=list");
         
        }  
            
        if($messages->getErrorMessageCount() > 0){
             // Display the list.
            $perform = 'list';
            include ( DIR_FS_NC .'/leads-quotation-list.php');
        
        }else{
        
            if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
                $data		= processUserData($_POST);
                if(empty($data['reason_of_delete'])){
                    $messages->setErrorMessage("Please Specify Reason To Delete the Quotation ".$data['number']);
                }
                if($messages->getErrorMessageCount() <= 0){                    
                    //Sql to delete invoice 
                     $query = "UPDATE ". TABLE_LEADS_QUOTATION
                                    ." SET status ='".LeadsQuotation::DELETED."', 
									reason_of_delete ='".$data['reason_of_delete']."',
                                    delete_by ='".$my['uid']."',
                                    delete_ip ='".$_SERVER['REMOTE_ADDR']."',
                                    do_delete='".date("Y-m-d")."'
                                    WHERE id = '$inv_id'";
                                            
                    if ( $db->query($query) && $db->affected_rows()>0 ) {    
                        //Update in order, invoice_created =0                       
                        $query = "UPDATE ".TABLE_LEADS_ORDERS." SET ".TABLE_LEADS_ORDERS.".is_invoice_create='0' WHERE 
                        ".TABLE_LEADS_ORDERS.".number = '".$invoice['or_no']."'";
                        $db->query($query);
                        
                        //Add Followup of order delete EOF
                         header("Location:".DIR_WS_NC."/leads-quotation.php?perform=list&deleted=1&inv_no=".$data['number']);
                    }
                    else {
                        $messages->setErrorMessage("The Quotation was not deleted.");
                    }
                    
        
                }
             
            }
            
            $hidden[] = array('name'=> 'inv_id' ,'value' => $inv_id);
            $hidden[] = array('name'=> 'number' ,'value' => $invoice['number']);
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-quotation-delete.html');
                    
        }  
        
       
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the Quotation.");
    }
?>