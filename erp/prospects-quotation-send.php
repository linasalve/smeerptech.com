<?php
 if ( $perm->has('nc_ps_qt_send') ) {
    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
	include_once ( DIR_FS_INCLUDES .'/prospects.inc.php' );
	
    $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );
	$flw_ord_id = isset($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : (isset($_POST['flw_ord_id']) ?	$_POST['flw_ord_id'] : '');
    $_ALL_POST	    = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
   
    
    $condition_query = " WHERE (". TABLE_PROSPECTS_QUOTATION .".id = '". $inv_id ."')";

    $fields = 	  TABLE_PROSPECTS_QUOTATION .'.number'
			.','. TABLE_PROSPECTS_QUOTATION .'.or_no'
			.','. TABLE_PROSPECTS_QUOTATION .'.amount'
			.','. TABLE_PROSPECTS_QUOTATION .'.order_closed_by'
			.','. TABLE_PROSPECTS_QUOTATION .'.order_closed_by2'
			.','. TABLE_PROSPECTS_QUOTATION .'.order_closed_by2_name'
			.','. TABLE_PROSPECTS_QUOTATION .'.currency_abbr'
			.','. TABLE_PROSPECTS_QUOTATION .'.currency_name'
			.','. TABLE_PROSPECTS_QUOTATION .'.do_i'
			.','. TABLE_PROSPECTS_QUOTATION .'.do_d'
			.','. TABLE_PROSPECTS_QUOTATION .'.filename'
			.','. TABLE_PROSPECTS_QUOTATION .'.attach_file1'
			.','. TABLE_PROSPECTS_QUOTATION .'.attach_file2'
			.','. TABLE_PROSPECTS_QUOTATION .'.attach_file3'
			.','. TABLE_PROSPECTS_QUOTATION .'.attach_file4'
			.','. TABLE_PROSPECTS_QUOTATION .'.attach_file5'
			.','. TABLE_PROSPECTS .'.user_id AS c_user_id'
			.','. TABLE_PROSPECTS .'.number AS c_number'
			.','. TABLE_PROSPECTS .'.f_name AS c_f_name'
			.','. TABLE_PROSPECTS .'.l_name AS c_l_name'
			.','. TABLE_PROSPECTS .'.billing_name'
			.','. TABLE_PROSPECTS .'.email AS c_email'
			.','. TABLE_PROSPECTS .'.email_1 AS c_email_1'
			.','. TABLE_PROSPECTS .'.email_2 AS c_email_2'
			.','. TABLE_PROSPECTS .'.email_3 AS c_email_3';
			
if ( ProspectsQuotation::getDetails($db, $_ALL_Data, $fields, $condition_query) > 0 ) {
	$_ALL_Data =  $_ALL_Data['0'];
	$filename=$_ALL_Data['filename'];
	//$_ALL_POST['to']= $_ALL_Data['c_email'];
	$inv_no =  $_ALL_Data['number'];
	$_ALL_Data['amount'] =  number_format($_ALL_Data['amount'],2);
        
		
	if(!empty($_ALL_Data['order_closed_by'])){
			$table = TABLE_USER;
			 $condition2 = " WHERE ".TABLE_USER.".user_id = '".$_ALL_Data['order_closed_by']."'
			AND ".TABLE_USER.".user_id != '".SALES_MEMBER_USER_ID."' 
			AND ".TABLE_USER.".department ='".ID_MARKETING."'" ;
			$fields1 =  TABLE_USER .".f_name, ".TABLE_USER.".l_name,".TABLE_USER.".department,
			".TABLE_USER.".marketing_contact, 
			".TABLE_USER.".email, 
			".TABLE_USER.".user_id " ;
			$detailsArr = getRecord($table,$fields1,$condition2);
		if(!empty($detailsArr)){
		
				//disply random name BOF 				 
				/* $data_mkt['display_name'] = $my['f_name']." ".$my['l_name'];
				$data_mkt['display_user_id'] = $my['user_id'];
				$data_mkt['display_designation'] = $my['desig']; */
				$data_mkt['display_name'] = $detailsArr['f_name']." ".$detailsArr['l_name'];
				$data_mkt['display_user_id'] = $detailsArr['user_id'];
				$data_mkt['display_designation'] = $detailsArr['desig'];
				//This is the marketing person identity
				$data_mkt['tck_owner_member_id'] = $detailsArr['user_id'];
				$data_mkt['tck_owner_member_name']  = $detailsArr['f_name']." ".$detailsArr['l_name'];
				$data_mkt['tck_owner_member_email'] = $detailsArr['email'];
				$data_mkt['marketing_email'] = $detailsArr['email'];
				$data_mkt['marketing_contact'] = $detailsArr['marketing_contact'];
				$data_mkt['marketing'] = 1;
				
				if(!empty($_ALL_Data['order_closed_by2'])){
					$condition2 = TABLE_USER." WHERE  user_id='".$_ALL_Data['order_closed_by2']."'";
					$clientfields = ' '.TABLE_USER.'.marketing_contact, '. TABLE_USER .'.desig,
					'.TABLE_USER .'.email,'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name,
					'.TABLE_USER.'.department';
					User::getList($db, $closed_by2, $clientfields, $condition) ;
					$data['mkex_fname']   = $closed_by2['0']['f_name'];
					$data['mkex_lname']   = $closed_by2['0']['l_name'];
					$data['mkex_designation']   = $closed_by2['0']['desig'];
					$data['mkex_mobile1']   = $closed_by2['0']['marketing_contact'];
					$data['marketing_email'] =$data['mkex_email']   = $closed_by2['0']['email'];
					$data['display_name']  = $data['mkex_fname']." ".$data['mkex_lname'] ;
					$data['display_designation']  = $data['mkex_designation'] ; 
					if(empty($data['mkex_email'])){
						$data['marketing_email'] = $data['mkex_email']=SALES_MEMBER_USER_EMAIL;
					} 
				} 
				
				
				
				
			if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
				$_ALL_POST 	= $_POST;
				$_ALL_POST['email'] = $_ALL_Data['c_email'];
				$_ALL_POST['email_1']=$_ALL_Data['c_email_1'];
				$_ALL_POST['email_2']=$_ALL_Data['c_email_2'];
				$_ALL_POST['email_3']=$_ALL_Data['c_email_3'];
				//print_r($_ALL_POST);    
				$data		= processUserData($_ALL_POST);   
				$extra = array( 'db' 		=> &$db,
								'messages'          => &$messages
							  );
				


				  
				if(ProspectsQuotation::validateSendInvoice($data, $extra)){
				
					$email = NULL;
					$cc_to_sender = $cc = $bcc = Null;
					
					$data['client'] = array(
										"f_name" => $_ALL_Data['c_f_name'],
										"l_name" => $_ALL_Data['c_l_name']
										);
					$data['number'] = $_ALL_Data['number'];
					$data['amount'] = $_ALL_Data['amount'];
					$data['currency_name'] = $_ALL_Data['currency_name'];
					$data['do_i'] = date("d M Y",strtotime($_ALL_Data['do_i']));
					$data['do_d'] = date("d M Y",strtotime($_ALL_Data['do_d']));
					
					if(!empty($_ALL_Data['order_closed_by'])){
						$clientfields='';
						$condition = TABLE_USER." WHERE  user_id='".$_ALL_Data['order_closed_by']."'";
						$clientfields .= ' '.TABLE_USER.'.mobile1, '. TABLE_USER .'.desig, 
						'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
						User::getList($db, $closed_by, $clientfields, $condition) ;
						
						$data['mkex_fname']   	= $closed_by['0']['f_name'];
						$data['mkex_lname']   	= $closed_by['0']['l_name'];
						$data['display_designation']   = $closed_by['0']['desig'];
						$data['display_mobile1']   = $closed_by['0']['mobile1'];
						$data['display_name'] = $data['mkex_fname']." ".$data['mkex_lname'];
						
						if(!empty($_ALL_Data['order_closed_by2'])){
							$condition2 = TABLE_USER." WHERE  user_id='".$_ALL_Data['order_closed_by2']."'";
							$clientfields = ' '.TABLE_USER.'.marketing_contact, '. TABLE_USER .'.desig,
							'.TABLE_USER .'.email,'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name,
							'.TABLE_USER.'.department';
							User::getList($db, $closed_by2, $clientfields, $condition) ;
							$data['mkex_fname']   = $closed_by2['0']['f_name'];
							$data['mkex_lname']   = $closed_by2['0']['l_name'];
							$data['mkex_designation']   = $closed_by2['0']['desig'];
							$data['mkex_mobile1']   = $closed_by2['0']['marketing_contact'];
							$data['marketing_email'] =$data['mkex_email']   = $closed_by2['0']['email'];
							$data['display_name']  = $data['mkex_fname']." ".$data['mkex_lname'] ;
							$data['display_designation']  = $data['mkex_designation'] ; 
							if(empty($data['mkex_email'])){
								$data['marketing_email'] = $data['mkex_email'] = SALES_MEMBER_USER_EMAIL;
							} 
						}
						
					}
						
					$mail_client = $mail_to_all_su = 0;
					if(isset($data['mail_client'])){
						$mail_client = 1 ;
					}				
					if(isset($data['mail_to_all_su'])){
						$mail_to_all_su = 1 ;
					}	
					
					if(!empty($_ALL_Data['attach_file1'])){
						$file_name[] = DIR_FS_PST_QUOT ."/". $_ALL_Data['attach_file1'];
					}
					if(!empty($_ALL_Data['attach_file2'])){
						$file_name[] = DIR_FS_PST_QUOT ."/". $_ALL_Data['attach_file2'];
					}
					if(!empty($_ALL_Data['attach_file3'])){
						$file_name[] = DIR_FS_PST_QUOT ."/". $_ALL_Data['attach_file3'];
					}
					if(!empty($_ALL_Data['attach_file4'])){
						$file_name[] = DIR_FS_PST_QUOT ."/". $_ALL_Data['attach_file4'];
					}
					if(!empty($_ALL_Data['attach_file5'])){
						$file_name[] = DIR_FS_PST_QUOT ."/". $_ALL_Data['attach_file5'];
					}
					$temp = NULL;
					$temp_p = NULL;
					$condition_query = "WHERE ".TABLE_PROSPECTS_ORD_P.".ord_no = '". $_ALL_Data['or_no'] ."'";
					ProspectsOrder::getParticulars($db, $temp, 'particulars,ss_title,ss_punch_line,ord_no', $condition_query);
					if(!empty($temp)){
						foreach ( $temp as $pKey => $parti ) {
							$temp_p[]=array(
									'particulars'=>$temp[$pKey]['particulars'],
									'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
									'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] 
									);
						}
					}                    
					$data['particulars'] = $temp_p ;     
					$file_name[] = DIR_FS_PST_QUOT_PDF ."/". $filename .".pdf";
					$inv_no = $data["number"] ;
					$mail_send_to_su='';
					//Send Company Service PDF BOF
					include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
					$id = 39;
					$fields = TABLE_ST_TEMPLATE .'.*'  ;            
					$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
					SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1);
					if(!empty($details)){
						$details = $details[0];
						$data['template_id'] = $details['id'];
						$data['template_file_1'] = $details['file_1'];
						$data['template_file_2'] = $details['file_2'];
						$data['template_file_3'] = $details['file_3'];
						
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
					}
					
					if ( getParsedEmail($db, $s, 'QUOTATION_CREATE_TO_CLIENT', $data, $email) ) {        
						if(!empty($_ALL_POST['to'])){
							$to = '';
							$mail_send_to_su .= ",".$_ALL_POST['to'];
							$to[]   = array('name' => $_ALL_POST['to'], 'email' => $_ALL_POST['to']);
							$from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);	
							//echo $email["body"];
							/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
							$email["isHTML"],$cc_to_sender,
							$cc,$bcc,$file_name); */
						}
						
					}
					if($mail_client){
						
						if(!empty($_ALL_POST['email'])){						
							$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
							$mail_send_to_su .= ",".$_ALL_POST['email'];
							//$to = '';
							$to[]   = array('name' => $_ALL_POST['email'], 'email' => $_ALL_POST['email'] );
							$from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);						  /*                     	
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name);
							*/					
						}
						if(!empty($_ALL_POST['email_1'])){						
						
							$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
							$mail_send_to_su .= ",".$_ALL_POST['email_1'];
							//$to = '';
							$to[]   = array('name' => $_ALL_POST['email_1'], 'email' => $_ALL_POST['email_1'] );
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    	
							/*
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name);
							*/
						}						
						if(!empty($_ALL_POST['email_2'])){						
						
							$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
							$mail_send_to_su .= ",".$_ALL_POST['email_2'];
							//$to = '';
							$to[]   = array('name' => $_ALL_POST['email_2'], 'email' => $_ALL_POST['email_2'] );
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    	/*
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name);*/
						}
						if(!empty($_ALL_POST['email_3'])){
							$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
							$mail_send_to_su .= ",".$_ALL_POST['email_3'];
							//$to = '';
							$to[]   = array('name' => $_ALL_POST['email_3'], 'email' => $_ALL_POST['email_3'] );
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    	/*SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name);*/
						}
						
						
						if( $mail_to_all_su || !empty($data['mail_to_su']) ){
						
							if($mail_to_all_su){
								Prospects::getList($db, $subUDetails, ' user_id, f_name, l_name,
								title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', 
								" WHERE parent_id = '".$_ALL_Data['c_user_id']."' AND 
								status='".Prospects::ACTIVE."' ORDER BY f_name");
							
							}elseif(!empty($data['mail_to_su'])){
								$mail_to_su= implode("','",$data['mail_to_su']);							
								Prospects::getList($db, $subUDetails, 'user_id, f_name, l_name,
								title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', 
								" WHERE parent_id = '".$_ALL_Data['c_user_id']."' AND status='".Prospects::ACTIVE."' 
								AND user_id IN('".$mail_to_su."')
								ORDER BY f_name");
							}						
							if(!empty($subUDetails)){

								foreach ($subUDetails  as $key=>$value){
								
									if(!empty($subUDetails[$key]['email'])){
										$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
										$mail_send_to_su .= ",".$subUDetails[$key]['email'];
										
										//$to = '';
										$to[]   = array('name' => $subUDetails[$key]['email'], 
										'email' =>$subUDetails[$key]['email'] );
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);                    				
										/*
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
										*/
									}
									if(!empty($subUDetails[$key]['email_1'])){
										$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
										$mail_send_to_su .= ",".$subUDetails[$key]['email_1'];
										
										//$to = '';
										$to[]   = array('name' => $subUDetails[$key]['email_1'], 
										'email' =>$subUDetails[$key]['email_1'] );
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);                     
										/*
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
										*/
									}
									if(!empty($subUDetails[$key]['email_2'])){
										$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
										$mail_send_to_su .= ",".$subUDetails[$key]['email_2'];
										
										//$to = '';
										$to[]   = array('name' => $subUDetails[$key]['email_2'], 
										'email' =>$subUDetails[$key]['email_2'] );
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);                    				
										/*
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],
										$cc_to_sender,$cc,$bcc,$file_name);*/
									}
								}
							}
						}
					}
					if(!empty($to)){
						$cc_to_sender=$cc=$bcc='';
						$bcc[]   = array('name' => $smeerp_client_email, 'email' => $smeerp_client_email);  
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						 
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
					} 

					//POST DATA in LEADS PROPOSAL FOLLOWUP BOF 
					$data['ticket_owner_uid'] = $_ALL_Data['c_user_id'];
					$data['ticket_owner'] = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name'];
					if(!empty($flw_ord_id)){					
						$sql2 = " SELECT ticket_id FROM ".TABLE_PROSPECTS_TICKETS." WHERE 
						".TABLE_PROSPECTS_TICKETS.".flw_ord_id = '".$flw_ord_id."' AND ".TABLE_PROSPECTS_TICKETS.".ticket_child = 0  LIMIT 0,1";
						if ( $db->query($sql2) ) {
							$ticket_id=0;
							if ( $db->nf() > 0 ){
								while ($db->next_record()) {
								   $ticket_id = $db->f('ticket_id');
								}
							}
						}				 
					}
					$ticket_no  =  ProspectsTicket::getNewNumber($db); 
					//disply random name BOF 				 
					/* if($my['department'] == ID_MARKETING){
						//This is the marketing person identity
						$data['tck_owner_member_id'] = $my['user_id'];
						$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
						$data['tck_owner_member_email'] = $my['email'];
					}else{ 
						$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
						$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
						$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;
					} */
					
					$data['display_name']=$data['display_user_id']=$data['display_designation']='';
					$randomUser = getCeoDetails();
					/* $data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];			 */	
					$data['ticket_subject'] = $email["subject"];
					$data['ticket_text'] = $email["body"];
					$data['hrs'] =0;
					$data['min'] =10;
					$hrs1 = (int) ($data['hrs'] * 6);
					$min1 = (int) ($data['min'] * 6);  
					
					//disply random name EOF
					if($ticket_id>0){
					
						$query= "SELECT "	. TABLE_PROSPECTS_TICKETS .".ticket_date, ".TABLE_PROSPECTS_TICKETS.".status"
								." FROM ". TABLE_PROSPECTS_TICKETS 
								." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
								." AND  (". TABLE_PROSPECTS_TICKETS .".ticket_child = '". $ticket_id ."' " 
											." OR "
											. TABLE_PROSPECTS_TICKETS .".ticket_id = '". $ticket_id ."' " 
										.") "
								." ORDER BY ". TABLE_PROSPECTS_TICKETS .".ticket_date DESC LIMIT 0,1";				
							
						$db->query($query) ;
						if($db->nf() > 0){
							$db->next_record() ;
							$last_time = $db->f("ticket_date") ;
							$ticket_response_time = time() - $last_time ;
							$status = $db->f("status") ;
						}else{
							$ticket_response_time = 0 ;
						}
						$ticket_date 	= time() ;
						$ticket_replied = '0' ;
						
						$query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
						. TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
						. TABLE_PROSPECTS_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
						. TABLE_PROSPECTS_TICKETS .".sent_inv_id         = '". $inv_id ."', "
						. TABLE_PROSPECTS_TICKETS .".sent_inv_no         = '". $inv_no ."', "
						. TABLE_PROSPECTS_TICKETS .".template_id       = '". $data['template_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid = '". $my['uid'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_status     = '".ProspectsTicket::PENDINGWITHCLIENTS."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_child      = '".$ticket_id."', "
						. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_replied    = '".$ticket_replied."', "
						. TABLE_PROSPECTS_TICKETS .".from_admin_panel  = '".ProspectsTicket::ADMIN_PANEL."', "
						. TABLE_PROSPECTS_TICKETS .".mail_client    = '".$mail_client."', "
						. TABLE_PROSPECTS_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
						. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
						. TABLE_PROSPECTS_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', "
						. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
						. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_PROSPECTS_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
						
						$db->query($query) ;
						$variables['hid'] =  $db->last_inserted_id() ;

						$query_update = "UPDATE ". TABLE_PROSPECTS_TICKETS 
						." SET ". TABLE_PROSPECTS_TICKETS .".ticket_status = '".ProspectsTicket::PENDINGWITHCLIENTS."'"  
						.",".TABLE_PROSPECTS_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
						.",".TABLE_PROSPECTS_TICKETS.".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."'"
						.",".TABLE_PROSPECTS_TICKETS.".last_comment = '".$data['ticket_text']."'"
						.",".TABLE_PROSPECTS_TICKETS.".last_comment_by = '".$my['uid'] ."'"
						.",".TABLE_PROSPECTS_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
						.",".TABLE_PROSPECTS_TICKETS.".status='".ProspectsTicket::ACTIVE."'"
						." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_id = '". $ticket_id ."' " ;
						$db->query($query_update) ;
					
					}else{
						//Create New Ticket 
						$query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
							. TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_PROSPECTS_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
							. TABLE_PROSPECTS_TICKETS .".sent_inv_id         = '". $inv_id ."', "
							. TABLE_PROSPECTS_TICKETS .".sent_inv_no         = '". $inv_no ."', "
							. TABLE_PROSPECTS_TICKETS .".template_id       = '". $data['template_id'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
							. TABLE_PROSPECTS_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
							. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_PROSPECTS_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_status     = '". ProspectsTicket::PENDINGWITHCLIENTS ."', "
							. TABLE_PROSPECTS_TICKETS.".status='".ProspectsTicket::ACTIVE."',"
							. TABLE_PROSPECTS_TICKETS .".ticket_child      = '0', "
							. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "                        
							. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
							. TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
							. TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
							. TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
							. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
							. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
							. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
							. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
						$db->query($query) ;
						$variables['hid'] =  $db->last_inserted_id() ;
					}
					//POST DATA in LEADS PROPOSAL FOLLOWUP EOF 
					$messages->setOkMessage("Proposal has been sent successfully on ".trim($mail_send_to_su,",")." email id.");   
				} 
			}else{
				$_ALL_POST['email'] = $_ALL_Data['c_email'];
				$_ALL_POST['email_1']=$_ALL_Data['c_email_1'];
				$_ALL_POST['email_2']=$_ALL_Data['c_email_2'];
				$_ALL_POST['email_3']=$_ALL_Data['c_email_3'];
			}
				$variables['can_view_client_details']=1;
				Prospects::getList($db, $subUserDetails, 'user_id, f_name, l_name, title as ctitle, 	
				email, email_1, email_2, email_3, email_4, mobile1, mobile2', " WHERE parent_id = '".$_ALL_Data['c_user_id']."' AND status='".Prospects::ACTIVE."' ORDER BY f_name");
				
				
				$hidden[] = array('name'=> 'inv_id' ,'value' => $inv_id);
				$hidden[] = array('name'=> 'inv_no' ,'value' => $inv_no);
				$hidden[] = array('name'=> 'flw_ord_id' ,'value' => $flw_ord_id);
				$hidden[] = array('name'=> 'perform' ,'value' => 'send');
				$hidden[] = array('name'=> 'act' , 'value' => 'save');

				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
				$page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["var"][] = array('variable' => 'variables', 'value' => 'variables');
				$page["var"][] = array('variable' => '_ALL_Data', 'value' => '_ALL_Data');
				$page["var"][] = array('variable' => 'inv_no', 'value' => 'inv_no');
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-quotation-send.html');
		}else{
			$messages->setErrorMessage('Selected Lead is not closed by Marketing Person.');
		}  
	}else{
	  $messages->setErrorMessage('Selected Lead is not closed by anyone<br/> Please update it.'); 
	}  
 }else{
	$messages->setErrorMessage("You donot have the Right to Access this Module.");
 }
}else{ 
    $messages->setErrorMessage("You donot have the Right to Send Quotation.");
}
?>