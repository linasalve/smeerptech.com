<?php

    if ( $perm->has('nc_sms_lst_view') ) {
		$shid = isset($_GET['sh_id']) ? $_GET['sh_id'] : ( isset($_POST['sh_id'] ) ? $_POST['sh_id'] :'');

		if(empty($condition_query))
			$condition_query = " WHERE shp_shid='".$shid."'";
		else
			$condition_query = " AND shp_shid='".$shid."'";
        //$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total    this records.
        $list	= 	NULL;
        $total	=	smsList::viewList( $db, $list, '*', $condition_query);
    	//$variables['hid'] = $cah_caid;
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        smsList::viewList( $db, $list, '*', $condition_query, $next_record, $rpp);

        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sms-list-view.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>