<?php
    if ( $perm->has('nc_sl_ld_status') ) {
        $lead_id= isset($_GET["lead_id"])? $_GET["lead_id"] : ( isset($_POST["lead_id"])? $_POST["lead_id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        if ( $perm->has('nc_sl_ld_status_al') ) {
            $access_level += 1;
        }
        
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        Leads::updateLeadStatus($lead_id, $status, $extra);
        
        if($status=='2'){
            $status_sql=",". TABLE_SALE_FOLLOWUP .".remarks = 'Status of lead is updated as Positive' ";
        }elseif($status=='3'){
            $status_sql=",". TABLE_SALE_FOLLOWUP .".remarks = 'Status of lead is updated as Negative' ";
        }
        
        $query_flw="SELECT followup_no FROM ".TABLE_SALE_FOLLOWUP." WHERE lead_id ='".$lead_id."'";
        $db->query($query_flw);
        
        if($db->nf()>0){
            $db->next_record();
            $flwnumber=$db->f('followup_no');
        }
        
        $query	= " INSERT INTO ".TABLE_SALE_FOLLOWUP
                    ." SET ". TABLE_SALE_FOLLOWUP .".followup_no 	= '". $flwnumber ."'"
                        //.",". TABLE_SALE_FOLLOWUP .".title_id 		= '". $data ['title_id'] ."'"
                        .$status_sql
                        //.",". TABLE_SALE_FOLLOWUP .".date    		= '". $data['date'] ."'"
                        //.",". TABLE_SALE_FOLLOWUP .".time    		= '". $data ['hrs'].':'.$data ['min'] ."'"
                        //.",". TABLE_SALE_FOLLOWUP .".time_type 		= '". $data ['time_type'] ."'"
                        .",". TABLE_SALE_FOLLOWUP .".ip         	= '". $_SERVER['REMOTE_ADDR'] ."'"
                        .",". TABLE_SALE_FOLLOWUP .".lead_id    	= '". $lead_id ."'"
                        //.",". TABLE_SALE_FOLLOWUP .".client    	    = '". $data ['lead_id'] ."'"
                        //.",". TABLE_SALE_FOLLOWUP .".assign_to 		= '". $data ['assign_to'] ."'"
                        .",". TABLE_SALE_FOLLOWUP .".followup_of    = '". $lead_id ."'"
                        .",". TABLE_SALE_FOLLOWUP .".table_name    	= 'sale_leads'"
                        .",". TABLE_SALE_FOLLOWUP .".created_by 	= '". $my['uid'] ."'"
                        .",". TABLE_SALE_FOLLOWUP .".access_level 	= '". $my['access_level'] ."'"
                        .",". TABLE_SALE_FOLLOWUP .".created_by_dept= '". $my['department'] ."'"
                        .",". TABLE_SALE_FOLLOWUP .".flollowup_type = '". FOLLOWUP::LEADTM ."'"
                        .",". TABLE_SALE_FOLLOWUP .".status = '". $status ."'"
                        .",". TABLE_SALE_FOLLOWUP .".do_e 			= '". date('Y-m-d h:i:s') ."'";
        $db->query($query); 
                                         
        // Display the  list.
        include ( DIR_FS_NC .'/sale-lead-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>