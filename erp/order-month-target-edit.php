<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_ordm_t_edit') ) {
        $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
    
       
        $_ALL_POST      = NULL;
        $data           = NULL;
       
     

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages
                        );
            
            if ( OrdMonthTarget::validateUpdate($data, $extra) ) {
				
				$temp =null;
					if ( !empty($data['frm_dt']) ) {
						$temp = explode('/', $data['frm_dt']);
						$data['frm_dt'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
					}
					$temp =null;
					if ( !empty($data['to_dt']) ) {
						$temp = explode('/', $data['to_dt']);
						$data['to_dt'] = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
					}
				$query  = " UPDATE ". TABLE_ORDM_TARGET
                        ." SET ".TABLE_ORDM_TARGET .".target_amount		= '". $data['target_amount'] ."'"
						.", ". TABLE_ORDM_TARGET .".frm_dt			= '". $data['frm_dt'] ."'"
						.", ". TABLE_ORDM_TARGET .".to_dt			= '". $data['to_dt'] ."'"
						.", ". TABLE_ORDM_TARGET .".executive_id			= '". $data['executive_id'] ."'"
						.", ". TABLE_ORDM_TARGET .".executive_details			= '". $data['executive_details'] ."'"     
                        ." WHERE ". TABLE_ORDM_TARGET .".id   		= '". $data['id'] ."'";
                if ( $db->query($query) ) {
					
                    $messages->setOkMessage("Record has been updated.");
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Record was not updated.');
                }
            }
        }

        
        
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
                
                
                  
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE id = '". $id ."' ";
                $_ALL_POST      = NULL;
                if ( OrdMonthTarget::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
					if(isset($_ALL_POST['frm_dt']) && $_ALL_POST['frm_dt']!='0000-00-00 00:00:00'){
						$temp  =null;
						$_ALL_POST['frm_dt']  = explode(' ', $_ALL_POST['frm_dt']);
						$temp               = explode('-', $_ALL_POST['frm_dt'][0]);
						$_ALL_POST['frm_dt']  = NULL;
						$_ALL_POST['frm_dt']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
					}else{
						$_ALL_POST['frm_dt']  = '';
						
					}
					if(isset($_ALL_POST['to_dt']) && $_ALL_POST['to_dt']!='0000-00-00 00:00:00'){
						$temp  =null;
						$_ALL_POST['to_dt']  = explode(' ', $_ALL_POST['to_dt']);
						$temp               = explode('-', $_ALL_POST['to_dt'][0]);
						$_ALL_POST['to_dt']  = NULL;
						$_ALL_POST['to_dt']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
					}else{
						$_ALL_POST['to_dt']  = '';
						
					}
					 
                   
                }
            }

            if ( !empty($_ALL_POST['id']) ) {

                // Check the Access Level.
				// These parameters will be used when returning the control back to the List page.
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}
				$hidden[] = array('name'=> 'perform','value' => 'edit');
				$hidden[] = array('name'=> 'act', 'value' => 'save');
				$hidden[] = array('name'=> 'id', 'value' => $id);
				$hidden[] = array('name'=> 'ajx','value' => $ajx);
				 
				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		  
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST'); 
	
			   $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'order-month-target-edit.html');
				
                
            }
            else {
                $messages->setErrorMessage("The Selected Quick Entry was not found.");
            }
       
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>