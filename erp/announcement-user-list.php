<?php
if ( $perm->has('nc_anc_user_list') ) {

    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder ;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $condition_query1 = " WHERE status = '".Announcement::ACTIVE."'".$condition_query;
    $list	= 	NULL;
    $total	=	Announcement::getDetails( $db, $list, '', $condition_query1);
    
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';
    
    $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
    $list	= NULL;
    $fields = TABLE_ANNOUNCEMENT.'.*';
    Announcement::getDetails( $db, $list, $fields, $condition_query1, $next_record, $rpp);
    
    // Set the Permissions.
   
    if ( $perm->has('nc_anc_list') ) {
        $variables['can_view_list'] = true;
    }
    
    if ( $perm->has('nc_anc_user_list') ) {
        $variables['can_view_list_active_announcement'] = true;
    }
       
    if ( $perm->has('nc_anc_details') ) {
        $variables['can_view_details'] = true;
    }
    
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'announcement-user-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
