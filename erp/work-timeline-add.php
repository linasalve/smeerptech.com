<?php

    if ( $perm->has('nc_sr_add') ) {

        $_ALL_POST	= NULL;
        $data       = NULL;
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            if ( WorkTimeline::validateAdd($data, $extra) ) {
                $query	= " INSERT INTO ". TABLE_WORK_TL
							." SET ". TABLE_WORK_TL .".title     		= '". $data['title'] ."'"
								.",". TABLE_WORK_TL .".status_order   	= '". $data['status_order'] ."'"
								.",". TABLE_WORK_TL .".status    		= '". $data['status'] ."'";
                if ($db->query($query) && $db->affected_rows() > 0) {
					$messages->setOkMessage("The New Order Status has been added.");
					$hid = $db->last_inserted_id();
				}
				else {
					$messages->setErrorMessage('The new status could not be saved. Please try again later.');
				}
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/work-timeline-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'work-timeline-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>