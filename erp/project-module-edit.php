<?php

    if ( $perm->has('nc_pts_mod_edit') ) {
        $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
          
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        $modules = NULL;
		ProjectModule::getParent($db,$modules,$or_id);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages
                        );
            
            if ( ProjectModule::validateUpdate($data, $extra) ) {
            
                $query  = " UPDATE ". TABLE_PROJECT_TASK_MODULE
                        ." SET ". TABLE_PROJECT_TASK_MODULE .".title     		  = '". $data['title'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".parent_id      	  = '". $data['parent_id'] ."'"
                            //.",". TABLE_PROJECT_TASK_MODULE .".order        = '". $data['order'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".details        = '". $data['details'] ."'"
                            .",". TABLE_PROJECT_TASK_MODULE .".status    		  = '". $data['status'] ."'"
							." WHERE ". TABLE_PROJECT_TASK_MODULE .".id   = '".  $data['id'] ."'";
                
                if ( $db->query($query) ) {
					$messages->setOkMessage("The Module has been updated.");
                }
                else {
                    $messages->setErrorMessage('Module was not updated.');
                }
            }
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $id;
            include ( DIR_FS_NC .'/project-module-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the posted data and display it back on the screen.
				$_ALL_POST = $_POST;
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE id = '". $id ."' ";
                $_ALL_POST      = NULL;
                if ( ProjectModule::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) 
                    $_ALL_POST = $_ALL_POST[0];
            }

            if ( !empty($_ALL_POST['id']) ) {
                    
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'id', 'value' => $id);
                    $hidden[] = array('name'=> 'or_id', 'value' => $or_id);
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                    $page["var"][] = array('variable' => 'modules', 'value' => 'modules');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-module-edit.html');
            }
            else {
                $messages->setErrorMessage("The Selected Module was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }

?>