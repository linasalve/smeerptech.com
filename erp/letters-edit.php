<?php
if ( $perm->has('nc_letr_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
			.','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_REPOSITORY_FILE_SIZE ;                
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                            );
            
            if ( Letters::validateUpdate($data, $extra) ) {
			
					$sql_file1=$sql_file2=$sql_file3=$sql_file4=$sql_file5='';
					if(!empty($data['file_1'])){
						if(!empty($data['old_file_1'])){
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_1']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_1']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-1".".".$ext ;
						$data['file_1'] = $attachfilename;						
						if (move_uploaded_file ($files['file_1']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   
						  @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						  $sql_file1 = ",". TABLE_LETTERS .".file_1 = '".$data['file_1'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_1']) && $data['delete_file_1']==1){
							$data['file_1']='';
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_1']);
							$sql_file1 = ",". TABLE_LETTERS .".file_1 = '".$data['file_1'] ."'" ;
						}
					}
					if(!empty($data['file_2'])){
						if(!empty($data['old_file_2'])){
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_2']);
						}						
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_2']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-2".".".$ext ;
						$data['file_2'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_2']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						   $sql_file2 = ",". TABLE_LETTERS .".file_2 = '".    $data['file_2'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_2']) && $data['delete_file_2']==1){
							$data['file_2']='';
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_2']);
							$sql_file2 = ",". TABLE_LETTERS .".file_2 = '".    $data['file_2'] ."'" ;
						}
					}
					if(!empty($data['file_3'])){
						if(!empty($data['old_file_3'])){
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_3']);
						}						
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_3']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-3".".".$ext ;
						$data['file_3'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_3']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						   $sql_file3 = ",". TABLE_LETTERS .".file_3 = '". $data['file_3'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_3']) && $data['delete_file_3']==1){
							$data['file_3']='';
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_3']);
							$sql_file3 = ",". TABLE_LETTERS .".file_3 = '". $data['file_3'] ."'" ;
						}
					}				
					if(!empty($data['file_4'])){
						if(!empty($data['old_file_4'])){
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_4']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_4']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-4".".".$ext ;
						$data['file_4'] = $attachfilename;                    
						if (move_uploaded_file ($files['file_4']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						   $sql_file4 = ",". TABLE_LETTERS .".file_4 = '". $data['file_4'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_4']) && $data['delete_file_4']==1){
							$data['file_4']='';
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_4']);
							$sql_file4 = ",". TABLE_LETTERS .".file_4 = '". $data['file_4'] ."'" ;
						}
					}
					if(!empty($data['file_5'])){
						if(!empty($data['old_file_5'])){
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_5']);
						}
						$filedata["extension"]='';
						$filedata = pathinfo($files['file_5']["name"]);                   
						$ext = $filedata["extension"] ; 
						$attachfilename = mktime()."-5".".".$ext ;
						$data['file_5'] = $attachfilename;                    
						if( move_uploaded_file ($files['file_5']['tmp_name'], DIR_FS_LETTERS_FILES."/".$attachfilename)){
						   @chmod(DIR_FS_LETTERS_FILES."/".$attachfilename,0777);
						   $sql_file5 = ",". TABLE_LETTERS .".file_5 = '". $data['file_5'] ."'" ;
						}
					}else{
						if(isset($data['delete_file_5']) && $data['delete_file_5']==1){
							$data['file_5']='';
							@unlink(DIR_FS_LETTERS_FILES."/".$data['old_file_5']);
							$sql_file5 = ",". TABLE_LETTERS .".file_5 = '". $data['file_5'] ."'" ;
						}					
					}
                    $query  = " UPDATE ".TABLE_LETTERS
							." SET ".TABLE_LETTERS .".filename = '". $data['filename'] ."'"  
							.",". TABLE_LETTERS .".subject 	= '". $data['subject'] ."'"
                            .",". TABLE_LETTERS .".text 		  = '". $data['text'] ."'"        
                            .",". TABLE_LETTERS .".description = '". $data['description'] ."'"
							.",". TABLE_LETTERS .".client_id 	= '". $data['client_id'] ."'"
							.",". TABLE_LETTERS .".client_details 	= '". $data['client_details'] ."'"
							.",". TABLE_LETTERS .".executive_id 	= '". $data['executive_id'] ."'"
							.",". TABLE_LETTERS .".executive_details = '". $data['executive_details'] ."'"
                            .$sql_file1
                            .$sql_file2
                            .$sql_file3
                            .$sql_file4
                            .$sql_file5
                            .",". TABLE_LETTERS .".updated_by = 	  '".$my['user_id']."'"
                            .",". TABLE_LETTERS .".updated_by_name = '".$my['f_name']." ".$my['l_name'] ."'"
                            .",". TABLE_LETTERS .".do_e = '".date('Y-m-d H:i:s') ."'"                       
                        ." WHERE id = '". $id ."'";
                   
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL; 
				$fields = TABLE_LETTERS .'.*'  ;            
				$condition_query = " WHERE (". TABLE_LETTERS .".id = '". $id ."' )";            
				if ( Letters::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
					$_ALL_POST = $_ALL_POST['0'];  
					$_ALL_POST['old_file_1']=$_ALL_POST['file_1'];
					$_ALL_POST['old_file_2']=$_ALL_POST['file_2'];
					$_ALL_POST['old_file_3']=$_ALL_POST['file_3'];
					$_ALL_POST['old_file_4']=$_ALL_POST['file_4'];
					$_ALL_POST['old_file_5']=$_ALL_POST['file_5'];
				} 
            }
        }
        else {
            // Read the record which is to be Updated.
           
            $fields = TABLE_LETTERS .'.*'  ;            
            $condition_query = " WHERE (". TABLE_LETTERS .".id = '". $id ."' )";            
            if ( Letters::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];  
				$_ALL_POST['old_file_1']=$_ALL_POST['file_1'];
                $_ALL_POST['old_file_2']=$_ALL_POST['file_2'];
                $_ALL_POST['old_file_3']=$_ALL_POST['file_3'];
                $_ALL_POST['old_file_4']=$_ALL_POST['file_4'];
                $_ALL_POST['old_file_5']=$_ALL_POST['file_5'];
            }else{ 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        
		// These parameters will be used when returning the control back to the List page.
		foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
			$hidden[]   = array('name'=> $key, 'value' => $value);
		}
		
  
		$hidden[] = array('name'=> 'id', 'value' => $id);
		$hidden[] = array('name'=> 'ajx','value' => $ajx);
		$hidden[] = array('name'=> 'perform', 'value' => 'edit');
		$hidden[] = array('name'=> 'act', 'value' => 'save');
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');    
		$page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company'); 
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'letters-edit.html');
        
    }
    else {
        
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
