<?php

    if ( $perm->has('nc_pts_mod_list') ) {
        
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        //$condition_query=" WHERE order_id='".$or_id."'".$condition_query;
		
		//Till id 871 all project module is order based. Now We are creating Master module
        $condition_query=" WHERE ".TABLE_PROJECT_TASK_MODULE.".status='".ProjectModule::ACTIVE."' 
		AND ".TABLE_PROJECT_TASK_MODULE.".id > 871 ".$condition_query;
        $total	=	ProjectModule::getList( $db, $list, '', $condition_query);
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $list	= NULL;
        ProjectModule::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
    
    
        $fList=array();
        if(!empty($list)){ 
            foreach( $list as $key=>$val){      
                $parent=$parentname=$username='';
                $table = TABLE_PROJECT_TASK_MODULE;
                $condition1 = " WHERE ".TABLE_PROJECT_TASK_MODULE .".id= '".$val['parent_id'] ."' " ;
                $fields1 =  TABLE_PROJECT_TASK_MODULE .'.title';
                $parent= getRecord($table,$fields1,$condition1);
                if(!empty($parent)){
                   $parentname = $parent['title'];
                }
                $val['parent_title']    = $parentname ;   

				$table = TABLE_USER;
                $condition1 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name, '.TABLE_USER .'.l_name';
                $user= getRecord($table,$fields1,$condition1);
                if(!empty($user)){
                   $username = $user['f_name']." ".$user['l_name'];
                }	
				$val['added_by']    = $username ;   

               $fList[$key]=$val;
            }
        } 
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_view_task']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        
        if ( $perm->has('nc_pts_mod_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_pts_mod_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_pts_mod_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_pts_mod_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_p_ts_list') ) {
            $variables['can_view_task']     = true;
        }
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-module-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>