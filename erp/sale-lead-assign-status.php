<?php
    if ( $perm->has('nc_sl_ld_asig_status') ) {
        $lead_id= isset($_GET["lead_id"])? $_GET["lead_id"] : ( isset($_POST["lead_id"])? $_POST["lead_id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        if ( $perm->has('nc_sl_ld_asig_status_al') ) {
            $access_level += 1;
        }
        
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        Leads::updateStatus($lead_id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/sale-lead-assign-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>