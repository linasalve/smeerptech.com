<?php
    if ( $perm->has('nc_ps_qt_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/prospects-quotation.inc.php');
		include_once ( DIR_FS_INCLUDES .'/prospects.inc.php');
		include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php' );
		include_once ( DIR_FS_INCLUDES .'/prospects-quotation-template.inc.php');
        $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $or_no          = isset($_GET["or_no"]) ? $_GET["or_no"] : ( isset($_POST["or_no"]) ? $_POST["or_no"] : '' );
        $test_pdf          = isset($_GET["test_pdf"]) ? $_GET["test_pdf"] : ( isset($_POST["test_pdf"]) ? $_POST["test_pdf"] : '' );
        $inv_no          = isset($_GET["inv_no"]) ? $_GET["inv_no"] : ( isset($_POST["inv_no"]) ? $_POST["inv_no"] : '' );

        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level']+1; 
        $currency = NULL;
        $required_fields ='*';
        Currency::getList($db,$currency,$required_fields); 
		$lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                   .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
		
		$condition_query_pqt = " WHERE ".TABLE_PROSPECTS_QUOTATION_TEMPLATES.".status = '".ProspectsQuotationTemplate::ACTIVE."' 
			AND ".TABLE_PROSPECTS_QUOTATION_TEMPLATES.".type = '".ProspectsQuotationTemplate::PROPOSAL_TYP."'" ;
        ProspectsQuotationTemplate::getList( $db, $pqtlist, TABLE_PROSPECTS_QUOTATION_TEMPLATES.'.id,'.TABLE_PROSPECTS_QUOTATION_TEMPLATES.'.title', $condition_query_pqt);      
		
		$condition_query_oqt = " WHERE ".TABLE_PROSPECTS_QUOTATION_TEMPLATES.".status = '".ProspectsQuotationTemplate::ACTIVE."' 
			AND ".TABLE_PROSPECTS_QUOTATION_TEMPLATES.".type = '".ProspectsQuotationTemplate::OPTIONAL_TYP."'" ;
        ProspectsQuotationTemplate::getList( $db, $oqtlist, TABLE_PROSPECTS_QUOTATION_TEMPLATES.'.id,'.TABLE_PROSPECTS_QUOTATION_TEMPLATES.'.title', $condition_query_oqt);  
        //show invoice bof
         // Read the Invoice which is to be Updated.
        $fields = TABLE_PROSPECTS_QUOTATION .'.*' 
                    .','. TABLE_PROSPECTS .'.user_id as c_user_id'
                    .','. TABLE_PROSPECTS .'.number AS c_number'
                    .','. TABLE_PROSPECTS .'.f_name AS c_f_name'
                    .','. TABLE_PROSPECTS .'.l_name AS c_l_name'
                    .','. TABLE_PROSPECTS .'.email AS c_email'
					.','. TABLE_PROSPECTS .'.status AS c_status' 
					.','. TABLE_PROSPECTS_ORDERS .'.old_particulars'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by2'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by2_name'
                    .','. TABLE_PROSPECTS_ORDERS .'.order_closed_by'                   ; 
        $condition_query = " WHERE (". TABLE_PROSPECTS_QUOTATION .".id = '". $inv_id ."' "
                            ." OR ". TABLE_PROSPECTS_QUOTATION .".number = '". $inv_id ."')"; 
        if ( ProspectsQuotation::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0']; 
			 
			//Get the list of Main Prospect and their Sub-Users BOF
			$listUser=array();
			$sql= "SELECT ".TABLE_PROSPECTS .".f_name"
                    .",". TABLE_PROSPECTS .".l_name "
                    .",". TABLE_PROSPECTS .".title "
                    .",". TABLE_PROSPECTS .".user_id "
                    .",". TABLE_PROSPECTS .".email FROM ".TABLE_PROSPECTS." WHERE 
				(".TABLE_PROSPECTS.".user_id='".$_ALL_POST['c_user_id']."' OR ".TABLE_PROSPECTS.".parent_id ='".$_ALL_POST['c_user_id']."')
				AND ".TABLE_PROSPECTS.".status='".Prospects::ACTIVE."'";
			$db->query($sql);
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					$name= $db->f('f_name')." ".$db->f('l_name');
					$title= $db->f('title');
					$prospect_id= $db->f('user_id');
					$listUser[]= array(
						'prospect_id'=>$prospect_id,
						'title'=>$title,
						'name'=>$name
					);
				}
			}
			// Set up the Client Details field.
			$_ALL_POST['client_details']= $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
											.' ('. $_ALL_POST['c_number'] .')'
											.' ('. $_ALL_POST['c_email'] .')';
			
			// Set up the dates.
			$_ALL_POST['do_i']  = explode(' ', $_ALL_POST['do_i']);
			$temp               = explode('-', $_ALL_POST['do_i'][0]);
			$_ALL_POST['do_i']  = NULL;
			$_ALL_POST['do_i']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
			
			$_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
			$temp               = explode('-', $_ALL_POST['do_d'][0]);
			$_ALL_POST['do_d']  = NULL;
			$_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
			
			if($_ALL_POST['do_e'] !='0000-00-00 00:00:00'){
				$_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
				$temp               = explode('-', $_ALL_POST['do_e'][0]);
				$_ALL_POST['do_e']  = NULL;
				$_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
			}else{
				$_ALL_POST['do_e']  ='';
			}
			
			if($_ALL_POST['do_fe'] !='0000-00-00 00:00:00'){
				$_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
				$temp               = explode('-', $_ALL_POST['do_fe'][0]);
				$_ALL_POST['do_fe']  = NULL;
				$_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
			}else{ 
				$_ALL_POST['do_fe']  ='';
			}
			// Set up the Services.
			//$_ALL_POST['service_id'] = explode(',', $_ALL_POST['service_id']);                  
			$condition_query = '';
			$or_no = $_ALL_POST['or_no']; 
        } else { 
            $messages->setErrorMessage("The Quotation was not found or you do not have the Permission to access this Quotation.");
        } 
		
		if($test_pdf==1){
			$file_path_test_pdf =  DIR_WS_PST_QUOT_PDF."/".$inv_id."-test.pdf";
			$messages->setOkMessage("Proposal created for test. 
			<a href='".$file_path_test_pdf."' target='_blank'><b>Click here to view the File</b> </a>");
		} 
        
if(empty($_ALL_POST['number']) && $_ALL_POST['pdf_created']=='0' ){    
    if( ( ( $_ALL_POST['order_closed_by']==$my['user_id'] ) || $perm->has('nc_ps_qt_list_all')) && $perm->has('nc_ps_qt_edit')  ){
	   
         if ( (isset($_POST['btnSave']) || isset($_POST['btnCreateTestPdf']) || isset($_POST['btnCreatePdf']) ) && 
			$_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);

            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            if(!empty($data['attn_prospect'])){
				$attn_p=explode('#',$data['attn_prospect']);
				$data['attn_prospect_id'] =$attn_p[0];
				$data['attn_prospect_name'] =$attn_p[1];
				
				$_ALL_POST['attn_prospect_id'] =$attn_p[0];
				$_ALL_POST['attn_prospect_name'] =$attn_p[1];
			}
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                           // 'lst_service'       => $lst_service,
                            'messages'          => &$messages
                        );

            if ( ProspectsQuotation::validateUpdate($data, $extra) ) {
                
                $invDetails =null;
                $condition_query1 = " WHERE ".TABLE_PROSPECTS_QUOTATION.".id ='".$data['inv_id']."'";
                $fields1 = TABLE_PROSPECTS_QUOTATION.".currency_abbr,".TABLE_PROSPECTS_QUOTATION.".currency_name,
				".TABLE_PROSPECTS_QUOTATION.".currency_symbol,".TABLE_PROSPECTS_QUOTATION.".currency_country,
				".TABLE_PROSPECTS_QUOTATION.".round_off_op,".TABLE_PROSPECTS_QUOTATION.".round_off ,
				".TABLE_PROSPECTS_QUOTATION.".dont_show_total,
				".TABLE_PROSPECTS_QUOTATION.".company_name,".TABLE_PROSPECTS_QUOTATION.".tin_no, 
				".TABLE_PROSPECTS_QUOTATION.".cst_no,".TABLE_PROSPECTS_QUOTATION.".vat_no,".
				TABLE_PROSPECTS_QUOTATION.".do_tax,
				".TABLE_PROSPECTS_QUOTATION.".service_tax_regn,".TABLE_PROSPECTS_QUOTATION.".do_st,
				".TABLE_PROSPECTS_QUOTATION.".pan_no,".TABLE_PROSPECTS_QUOTATION.".delivery_at,
				".TABLE_PROSPECTS_QUOTATION.".order_closed_by,
				".TABLE_PROSPECTS_QUOTATION.".order_closed_by2,
				".TABLE_PROSPECTS_QUOTATION.".order_closed_by2_name,
				".TABLE_PROSPECTS_QUOTATION.".tax1_declaration,
				".TABLE_PROSPECTS_QUOTATION.".other_details,
				".TABLE_PROSPECTS_QUOTATION.".filename,
				".TABLE_PROSPECTS_QUOTATION.".sub_total_amount,".TABLE_PROSPECTS_QUOTATION.".tax1_id,
				".TABLE_PROSPECTS_QUOTATION.".tax1_number,".TABLE_PROSPECTS_QUOTATION.".tax1_name,
				".TABLE_PROSPECTS_QUOTATION.".tax1_value,".TABLE_PROSPECTS_QUOTATION.".tax1_pvalue,
				".TABLE_PROSPECTS_QUOTATION.".tax1_total_amount,
				".TABLE_PROSPECTS_QUOTATION.".tax1_sub1_id,".TABLE_PROSPECTS_QUOTATION.".tax1_sub1_number,
				".TABLE_PROSPECTS_QUOTATION.".tax1_sub1_name,".TABLE_PROSPECTS_QUOTATION.".tax1_sub1_value,
				".TABLE_PROSPECTS_QUOTATION.".tax1_sub1_pvalue,
				".TABLE_PROSPECTS_QUOTATION.".tax1_sub1_total_amount,				
				".TABLE_PROSPECTS_QUOTATION.".tax1_sub2_id,".TABLE_PROSPECTS_QUOTATION.".tax1_sub2_number,
				".TABLE_PROSPECTS_QUOTATION.".tax1_sub2_name,".TABLE_PROSPECTS_QUOTATION.".tax1_sub2_value,
				".TABLE_PROSPECTS_QUOTATION.".tax1_sub2_pvalue,".TABLE_PROSPECTS_QUOTATION.".tax1_sub2_total_amount	";
                
				ProspectsQuotation::getList( $db, $invDetails, $fields1, $condition_query1 );
                $data['currency_abbr'] = '';
                $data['currency_name'] = '';
                $data['currency_symbol'] = '';
                $data['currency_country'] = '';
                $data['do_tax']=0;
                if(!empty($invDetails)){
                    $invDetails = $invDetails[0] ;
					//$data['other_details'] = $invDetails['other_details'];
                    $data['order_closed_by'] = $invDetails['order_closed_by'];
                    $data['order_closed_by2'] = $invDetails['order_closed_by2'];
                    $data['order_closed_by2_name'] = $invDetails['order_closed_by2_name'];
                    $data['currency_abbr'] = $invDetails['currency_abbr'];
                    $data['currency_name'] = $invDetails['currency_name'];
                    $data['currency_symbol'] = $invDetails['currency_symbol'];
                    $data['currency_country'] = $invDetails['currency_country'];
					$data['round_off_op'] =  $invDetails['round_off_op'];
                    $data['round_off'] 	  =  $invDetails['round_off'];
                    $data['dont_show_total'] 	  =  $invDetails['dont_show_total'];
                    $data['company_name'] =  $invDetails['company_name'];
                    $data['tax1_declaration'] =  $invDetails['tax1_declaration'];
					
                    $data['sub_total_amount'] = number_format($invDetails['sub_total_amount'],2);
                    $data['tax1_id'] 	 =  $invDetails['tax1_id'];
                    $data['tax1_number'] =  $invDetails['tax1_number'];
                    $data['tax1_name']   =  $invDetails['tax1_name'];
                    $data['tax1_value']  =  $invDetails['tax1_value'];
                    $data['tax1_pvalue'] =  $invDetails['tax1_pvalue'];
                    $data['tax1_total_amount']  = number_format($invDetails['tax1_total_amount'],2);
					
					$data['tax1_sub1_id'] =  $invDetails['tax1_sub1_id'];
                    $data['tax1_sub1_number'] =  $invDetails['tax1_sub1_number'];
                    $data['tax1_sub1_name'] =  $invDetails['tax1_sub1_name'];
                    $data['tax1_sub1_value'] =  $invDetails['tax1_sub1_value'];
                    $data['tax1_sub1_pvalue'] =  $invDetails['tax1_sub1_pvalue'];
                    $data['tax1_sub1_total_amount'] = number_format($invDetails['tax1_sub1_total_amount'],2);
					
					$data['tax1_sub2_id'] =  $invDetails['tax1_sub2_id'];
                    $data['tax1_sub2_number'] =  $invDetails['tax1_sub2_number'];
                    $data['tax1_sub2_name'] =  $invDetails['tax1_sub2_name'];
                    $data['tax1_sub2_value'] =  $invDetails['tax1_sub2_value'];
                    $data['tax1_sub2_pvalue'] =  $invDetails['tax1_sub2_pvalue'];
                    $data['tax1_sub2_total_amount'] = number_format($invDetails['tax1_sub2_total_amount'],2);
					
					$data['delivery_at'] =  $invDetails['delivery_at'];
					$data['pan_no'] =  $invDetails['pan_no'];
					$data['filename'] =  $invDetails['filename'];
					
                  
                    $data['do_tax'] =  $invDetails['do_tax'];	
				    
					$data['do_st'] =  $invDetails['do_st'];
				    if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
						$data['do_tax']= strtotime($data['do_tax']);				
					}
					if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){					    
					   $data['tin_no'] =  $invDetails['tin_no'];
					   $data['cst_no'] =  $invDetails['cst_no'];
					   $data['vat_no'] =  $invDetails['vat_no'];
					}
					$data['service_tax_regn'] =  '';
					if( $data['do_st']!=0 &&  $data['do_st']!='0000:00:00 00:00:00'){
						$data['do_st']= strtotime($data['do_st']);				
					}
					if($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00'){					    
					   
					   $data['service_tax_regn'] =  $invDetails['service_tax_regn'];
					  
					}
                }
				$data['invoice_title']=' PROPOSAL ';
                //Get billing address BOF
                 include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
                $region         = new RegionProspects();
                $region->setAddressOf(TABLE_PROSPECTS, $data['client']['user_id']);
                $addId = $data['billing_address'] ;
                $address_list  = $region->get($addId);
                $data['b_addr_company_name'] = $address_list['company_name'];
                $data['b_addr'] = $address_list['address'];
                $data['b_addr_city'] = $address_list['city_title'];
                $data['b_addr_state'] = $address_list['state_title'];
                $data['b_addr_country'] = $address_list['c_title'];
                $data['b_addr_zip'] = $address_list['zipcode'];
                //$data['b_address'] = $address_list ;
                //Get billing address EOF
                
                $data['do_i_chk'] = $data['do_i'];
				$data['do_rs_symbol'] = strtotime('2010-07-20');
                $data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
                $data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
                if(!empty($data['do_e'])){
                    $data['do_e'] = date('Y-m-d H:i:s', $data['do_e']) ;
                }
                if(!empty($data['do_fe'])){
                    $data['do_fe'] = date('Y-m-d H:i:s', $data['do_fe']) ;
                }                       
                
                $query  = " UPDATE ".TABLE_PROSPECTS_QUOTATION
                    ." SET "
					."". TABLE_PROSPECTS_QUOTATION .".remarks = '".$data['remarks'] ."'"    
					.",". TABLE_PROSPECTS_QUOTATION .".quotation_subject = '".  $data['quotation_subject'] ."'"					   
					.",". TABLE_PROSPECTS_QUOTATION .".other_details = '".  $data['other_details'] ."'"					  
					.",". TABLE_PROSPECTS_QUOTATION .".exchange_rate = '". $data['exchange_rate'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".amount = '".        $data['amount'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".amount_inr = '".    $data['amount_inr'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".do_e = '".          $data['do_e'] ."'" //date of expiry
					.",". TABLE_PROSPECTS_QUOTATION .".do_fe = '".         $data['do_fe']."'" //date of expiry from
					//.",". TABLE_PROSPECTS_QUOTATION .".access_level = '".  $data['access_level'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".remarks = '".       $data['remarks'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".balance = '".       $data['balance'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".balance_inr = '".   $data['amount_inr'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".amount_words = '".   $data['amount_words'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".attn_prospect_id = '".   $data['attn_prospect_id'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".attn_prospect_name = '".   $data['attn_prospect_name'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".billing_name = '".processUserData($data['billing_name']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".billing_address='". processUserData($data['billing_address']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".b_addr_company_name 
					= '".processUserData($data['b_addr_company_name'])."'"
					.",". TABLE_PROSPECTS_QUOTATION .".b_addr          = '".processUserData($data['b_addr']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".b_addr_city     = '".processUserData($data['b_addr_city'] )."'"
					.",". TABLE_PROSPECTS_QUOTATION .".b_addr_state = '".processUserData( $data['b_addr_state']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".b_addr_country= '".processUserData($data['b_addr_country']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".b_addr_zip      = '". $data['b_addr_zip'] ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".quotation_template_id  = '".processUserData($data['quotation_template_id']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".page1  = '".processUserData($data['page1']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".page2  = '".processUserData($data['page2']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".page3  = '".processUserData($data['page3']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".optional_template1  = '".processUserData($data['optional_template1']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".optional_template2  = '".processUserData($data['optional_template2']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".optional_template3  = '".processUserData($data['optional_template3']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".optional_template4  = '".processUserData($data['optional_template4']) ."'"
					.",". TABLE_PROSPECTS_QUOTATION .".optional_template5  = '".processUserData($data['optional_template5']) ."'"	
					//.",". TABLE_PROSPECTS_QUOTATION .".status ='". $data['status'] ."'"
					." WHERE ".TABLE_PROSPECTS_QUOTATION.".id ='".$data['inv_id']."'";
               
               if ( $db->query($query) ) {
                    $messages->setOkMessage('The Proposal has been Updated.');
                    
                    $data['amount']= number_format($data['amount'],2);
                    $data['amount_inr']= number_format($data['amount_inr'],2);
                    $data['balance']= number_format($data['balance'],2);
                    $data['amount_words']=processSqlData($data['amount_words']);
                    
					$data['other_details'] = processSqlData($data['other_details']);
					
                    $temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_PROSPECTS_ORD_P.".ord_no = '". $data['or_no'] ."'";
                    ProspectsOrder::getParticulars($db, $temp, '*', $condition_query);
                    $data['show_discount']=0;
                    $data['show_nos']=0;
                    $data['colsSpanNo1']=5;
                    $data['colsSpanNo2']=6; 
                    if(!empty($temp)){
                        /*
						$data['wd1']=10;
                        $data['wdinvp']=217;
                        $data['wdpri']=70;
                        $data['wdnos']=70;
                        $data['wdamt']=70;
                        $data['wddisc']=50;
                        $data['wdst']=70;
                        //$data['wdtx']=40; as three columns are removed so manage its width in other cols
                       // $data['wdtxp']=50;
                        //$data['wdtotam']=75;
                        $data['wd2']=9;*/
						
						$data['wd1']=10;
                        $data['wdinvp']=245;//217+28
                        $data['wdpri']=98;//70+28
                        $data['wdnos']=98;//70+28
                        $data['wdamt']=97;//70+27
                        $data['wddisc']=77; //50+27
                        $data['wdst']=97;   //70+27                     
                        $data['wd2']=9;
                    
                        foreach ( $temp as $pKey => $parti ) {
                            /*
                               if($temp[$pKey]['s_type']=='1'){
                                    $temp[$pKey]['s_type']='Years';
                               }elseif($temp[$pKey]['s_type']=='2'){
                                    $temp[$pKey]['s_type']='Quantity';
                               }
                           */
                            if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
                                $data['show_discount']=1;
                                $show_discountNo=1;
                            }
                            if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
                                $data['show_nos']=1;
                                $show_nos=1;
                            }
                            $temp_p[]=array(
                                'p_id'=>$temp[$pKey]['id'],
                                'particulars'=>$temp[$pKey]['particulars'],
                                'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
                                's_type' =>$temp[$pKey]['s_type'] ,                                   
                                's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
                                's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
                                's_id' =>$temp[$pKey]['s_id'] ,                                   
                                'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
                                'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
                                'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
                                'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
                                'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
                                'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,   
								'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,                                   
                                'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                   
                                'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
                                'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,                                   
                                'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,
								'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,                                   
                                'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                   
                                'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
                                'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,                                   
                                'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,                               
								'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
                                'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
                                'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
                                'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
                                'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
                                );
								if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){
								
									$data['sub_total_amount'] = $data['sub_total_amount'] + $temp[$pKey]['stot_amount'] ; 
									
									$data['tax1_id'] =   $temp[$pKey]['tax1_id'] ; 
									$data['tax1_number'] =   $temp[$pKey]['tax1_number'] ; 
									$data['tax1_name'] =   $temp[$pKey]['tax1_name'] ; 
									$data['tax1_value'] =   $temp[$pKey]['tax1_value'] ; 
									$data['tax1_pvalue'] =   $temp[$pKey]['tax1_pvalue'] ; 
									$data['tax1_total_amount'] =  $data['tax1_total_amount'] + $temp[$pKey]['tax1_amount'] ; 
									
									
									$data['tax1_sub1_id'] =   $temp[$pKey]['tax1_sub1_id'] ; 
									$data['tax1_sub1_number'] =   $temp[$pKey]['tax1_sub1_number'] ; 
									$data['tax1_sub1_name'] =   $temp[$pKey]['tax1_sub1_name'] ; 
									$data['tax1_sub1_value'] =   $temp[$pKey]['tax1_sub1_value'] ; 
									$data['tax1_sub1_pvalue'] =   $temp[$pKey]['tax1_sub1_pvalue'] ; 
									$data['tax1_sub1_total_amount'] =  $data['tax1_sub1_total_amount'] + 
									$temp[$pKey]['tax1_sub1_amount'] ; 
									
									$data['tax1_sub2_id'] =   $temp[$pKey]['tax1_sub2_id'] ; 
									$data['tax1_sub2_number'] =   $temp[$pKey]['tax1_sub2_number'] ; 
									$data['tax1_sub2_name'] =   $temp[$pKey]['tax1_sub2_name'] ; 
									$data['tax1_sub2_value'] =   $temp[$pKey]['tax1_sub2_value'] ; 
									$data['tax1_sub2_pvalue'] =   $temp[$pKey]['tax1_sub2_pvalue'] ; 
									$data['tax1_sub2_total_amount'] =  $data['tax1_sub2_total_amount'] + 
									$temp[$pKey]['tax1_sub2_amount'] ; 
								}
                        }
                        if($data['show_discount'] ==0){ 
                            // disc : 50 & subtot : 70
                           /* $data['wdinvp']= $data['wdinvp']+20 + 40;
                            $data['wdpri'] = $data['wdpri'] + 5 + 6;
                            $data['wdamt'] = $data['wdamt'] + 5+ 6;                            
                            $data['wdtx']  = $data['wdtx'] + 5+ 6;
                            $data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            $data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							*/
							// disc : 77 & subtot : 97
							$data['wdinvp']= $data['wdinvp']+50 + 70;
                            $data['wdpri'] = $data['wdpri'] +10 + 10;
                            $data['wdamt'] = $data['wdamt'] + 17+ 17;                            
                            //$data['wdtx']  = $data['wdtx'] + 5+ 6;
                            //$data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            //$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
                            
                        }
                         if($data['show_nos'] ==0){
                            // disc : 70
                           /* $data['wdinvp']= $data['wdinvp']+30;
                            $data['wdpri'] = $data['wdpri'] + 5;
                            $data['wdamt'] = $data['wdamt'] + 5;
                            $data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;
							*/
							  // disc :98
							$data['wdinvp']= $data['wdinvp']+80;
                            $data['wdpri'] = $data['wdpri'] + 8;
                            $data['wdamt'] = $data['wdamt'] + 10;
                            /*
							$data['wdtx']  = $data['wdtx'] + 10;
                            $data['wdtxp'] = $data['wdtxp'] + 10;
                            $data['wdtotam'] =$data['wdtotam'] + 10;*/
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;
                            
                        }       
                    }
					
					$data['mkex_fname']   = '';
					$data['mkex_lname']   = '';
					$data['mkex_designation'] ='';
					$data['mkex_mobile1']   ='';					
					$data['mkex_email']   ='';					
                    if(!empty($data['order_closed_by'])){
						$clientfields='';
						$condition = TABLE_USER." WHERE  user_id='".$data['order_closed_by']."'";
						$clientfields .= ' '.TABLE_USER.'.marketing_contact, '. TABLE_USER .'.desig,  '. TABLE_USER .'.email,
						'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
						User::getList($db, $closed_by, $clientfields, $condition) ;
						$data['mkex_fname']   	= $closed_by['0']['f_name'];
						$data['mkex_lname']   	= $closed_by['0']['l_name'];
						$data['mkex_designation']   = $closed_by['0']['desig'];
						$data['mkex_mobile1']   = $closed_by['0']['marketing_contact'];
						$data['mkex_email']   = $closed_by['0']['email'];
						if(empty($data['mkex_email'])){
							$data['mkex_email']   = SALES_MEMBER_USER_EMAIL;
						}
						
						if(!empty($data['order_closed_by2'])){
							$condition2 = TABLE_USER." WHERE  user_id='".$data['order_closed_by2']."'";
							$clientfields = ' '.TABLE_USER.'.marketing_contact, '. TABLE_USER .'.desig,
							'.TABLE_USER .'.email,'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name,
							'.TABLE_USER.'.department';
							User::getList($db, $closed_by2, $clientfields, $condition) ;
							$data['mkex_fname']   = $closed_by2['0']['f_name'];
							$data['mkex_lname']   = $closed_by2['0']['l_name'];
							$data['mkex_designation']   = $closed_by2['0']['desig'];
							$data['mkex_mobile1']   = $closed_by2['0']['marketing_contact'];
							$data['marketing_email'] =$data['mkex_email']   = $closed_by2['0']['email'];
							$data['display_name']  = $data['mkex_fname']." ".$data['mkex_lname'] ;
							$data['display_designation']  = $data['mkex_designation'] ; 
							if(empty($data['mkex_email'])){
								$data['marketing_email'] = $data['mkex_email']=SALES_MEMBER_USER_EMAIL;
							} 
						} 
						
					}
					/* 
					$sub_tax_str='';
                    if(!empty($data['tax1_name'])){
					
						//get the tax's declaration bof
						$condition1=" WHERE id= ".$data['tax1_id']." LIMIT 0,1" ;
						$required_fields='declaration';
						$parentTaxDetails=array();
						$data['tax1_declaration']='';
						ServiceTax::getDetails( $db, $parentTaxDetails, $required_fields, $condition1);
						if(!empty($parentTaxDetails)){
							$arr = $parentTaxDetails[0];
							$data['tax1_declaration'] = $arr['declaration'];
						}
						
						//get the tax's declaration eof  
						$sqlu  = " UPDATE ".TABLE_PROSPECTS_QUOTATION
                            ." SET "
							."". TABLE_PROSPECTS_QUOTATION .".sub_total_amount = '".$data['sub_total_amount'] ."'"                            
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_id = '". $data['tax1_id'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_number = '". $data['tax1_number'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_name = '". $data['tax1_name'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_declaration = '". $data['tax1_declaration'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_value = '". $data['tax1_value'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_pvalue = '". $data['tax1_pvalue'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_total_amount = '". $data['tax1_total_amount'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub1_id = '". $data['tax1_sub1_id'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub1_number = '". $data['tax1_sub1_number'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub1_name = '". $data['tax1_sub1_name'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub1_value = '". $data['tax1_sub1_value'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub1_pvalue = '". $data['tax1_sub1_pvalue'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub1_total_amount = '". $data['tax1_sub1_total_amount'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub2_id = '". $data['tax1_sub2_id'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub2_number = '". $data['tax1_sub2_number'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub2_name = '". $data['tax1_sub2_name'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub2_value = '". $data['tax1_sub2_value'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub2_pvalue = '". $data['tax1_sub2_pvalue'] ."'"
							.",". TABLE_PROSPECTS_QUOTATION .".tax1_sub2_total_amount = '". $data['tax1_sub2_total_amount'] ."'"
							." WHERE ".TABLE_PROSPECTS_QUOTATION.".id ='".$variables['hid'] ."'";	
													
						$db->query($sqlu);
						
						$data['sub_total_amount']=number_format($data['sub_total_amount'],2);
						$data['tax1_total_amount']=number_format($data['tax1_total_amount'],2);
						$data['tax1_sub1_total_amount']=number_format($data['tax1_sub1_total_amount'],2);
						$data['tax1_sub2_total_amount']=number_format($data['tax1_sub2_total_amount'],2);
					} 
					*/
					 
                    $data['particulars']=$temp_p ;
                    //get Quotation details bof
                    //Optional templates Data BOF
					if(!empty($data['optional_template1']) || !empty($data['optional_template2']) || !empty($data['optional_template3']) || 
						!empty($data['optional_template4']) || !empty($data['optional_template5'])){
						$sql = "SELECT * FROM ".TABLE_PROSPECTS_QUOTATION_TEMPLATES." WHERE 
						".TABLE_PROSPECTS_QUOTATION_TEMPLATES.".id IN(".$data['optional_template1'].",".$data['optional_template2'].",".$data['optional_template3'].",".$data['optional_template4'].",".$data['optional_template5'].")"; 
						$db->query( $sql); 
						$data['optional'] = array();
						if( $db->nf() > 0 )	{
							while($db->next_record()){
								$op_intro_page1 = processSQLData($db->f("intro_page1"));
								$op_intro_page2 = processSQLData($db->f("intro_page2"));
								$op_intro_page3 = processSQLData($db->f("intro_page3"));
								$data['optional'][] = $op_intro_page1;
								$data['optional'][] = $op_intro_page2;
								$data['optional'][] = $op_intro_page3;
							}
						}
					}
					//Optional templates Data EOF
					
					//Get intro pages and terms-condition BOF
					$data['page1'] = $_ALL_POST['page1'];
					$data['page2'] = $_ALL_POST['page2'];
					$data['page3'] = $_ALL_POST['page3'];
					$table = TABLE_PROSPECTS_QUOT_INTRO;
					$condition2 = " WHERE ".TABLE_PROSPECTS_QUOT_INTRO .".id= '1' " ;
					$fields1 = "*" ;
					$introdata = getRecord($table,$fields1,$condition2);
					if(!empty($introdata)){
						$data['quotation_intro1'] = $introdata['intro_page1'];
						$data['quotation_intro2'] = $introdata['intro_page2'];
						$data['quotation_intro3'] = $introdata['intro_page3'];
						$data['quotation_intro4'] = $introdata['intro_page4'];
						$data['quotation_term1'] = $introdata['terms_page1'];
						$data['quotation_term2'] = $introdata['terms_page2'];
						$data['quotation_term3'] = $introdata['terms_page3'];
					}
					
					//Get terms and condition eof 
					$data['service_pdf']=0;
					if(isset($data['btnCreatePdf'])){
						$file_test = DIR_FS_PST_QUOT_PDF ."/".$data['inv_id']."-test".".pdf";
						if (file_exists($file_test)) {
							@unlink($file_test);
						}
						$data['filename'] = "SMEERP_".$data['number']."_".date('dMY',$data['do_i']);
						ProspectsQuotation::createPdf3File($data);
						//Update Status of Proposal As pdf_created = '1' 						
						$sql1 = " UPDATE ".TABLE_PROSPECTS_QUOTATION." SET ".TABLE_PROSPECTS_QUOTATION.".pdf_created ='1'"
								.",". TABLE_PROSPECTS_QUOTATION .".number = '".$data['number'] ."'"
								.",". TABLE_PROSPECTS_QUOTATION .".inv_counter = '". $data['inv_counter'] ."'" 
								.",". TABLE_PROSPECTS_QUOTATION .".filename = '".  $data['filename'] ."' 
							WHERE ".TABLE_PROSPECTS_QUOTATION.".id ='".$data['inv_id'] ."'";	
						$db->query($sql1);
						
					}
					if(isset($data['btnCreateTestPdf'])){
						$data["filename"] = $data['inv_id']."-test";
						$data["dummy"] = 1;
						ProspectsQuotation::createPdf3File($data);
					}
                    //ProspectsQuotation::createPdfFile($data); 
                    //$file_name = DIR_FS_PST_QUOT_PDF ."/". $data["filename"] .".pdf"; 
                }
            }
          }
        
        
			if( (isset($_POST['btnSave']) || isset($_POST['btnCreatePdf'])) && $messages->getErrorMessageCount() <= 0 ){ 
			   header("Location:".DIR_WS_NC."/prospects-quotation.php?added=1&hid=".$inv_id."&inv_no=".$data['number']);           
			} 
			if( isset($_POST['btnCreateTestPdf']) && $messages->getErrorMessageCount() <= 0 ){ 
			   header("Location:".DIR_WS_NC."/prospects-quotation.php?perform=edit&inv_id=".$inv_id."&inv_no=".$data['number']."&test_pdf=1");           
			}
			if(isset($_POST['btnCancel'])){
				 header("Location:".DIR_WS_NC."/prospects-quotation.php");
			} 
			// Check if the Form to add is to be displayed or the control is to be sent to the List page.
        
        
            if ( !empty($or_no) ) {
         
                // Read the Order details.
                include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php');
                $order  = NULL;
                $fields = TABLE_PROSPECTS_ORDERS.'.id,'.TABLE_PROSPECTS_ORDERS .'.do_o AS do_o,
				'.TABLE_PROSPECTS_ORDERS .'.number AS or_no,'.TABLE_PROSPECTS_ORDERS .'.access_level, 
				'. TABLE_PROSPECTS_ORDERS .'.status, '.TABLE_PROSPECTS_ORDERS.'.details';
                $fields .= ', '.TABLE_PROSPECTS_ORDERS.'.order_title,'. TABLE_PROSPECTS_ORDERS .'.order_closed_by2,'. TABLE_PROSPECTS_ORDERS .'.order_closed_by2_name,'. TABLE_PROSPECTS_ORDERS .'.order_closed_by';
                $fields .= ', '.TABLE_PROSPECTS_ORDERS.'.is_renewable ';
                $fields .= ', '.TABLE_PROSPECTS_ORDERS.'.currency_id, '. TABLE_PROSPECTS_ORDERS .'.company_id  
				,'.TABLE_PROSPECTS_ORDERS .'.amount' ;
                $fields .= ', '.TABLE_PROSPECTS.'.user_id as user_id, '. TABLE_PROSPECTS .'.number, 
				'.TABLE_PROSPECTS.'.f_name, '.TABLE_PROSPECTS.'.l_name';
                $fields .= ', '.TABLE_PROSPECTS_ORDERS.'.client, '. TABLE_USER .'.number AS u_number, 
				'.TABLE_USER.'.f_name AS u_f_name, '.TABLE_USER.'.l_name AS u_l_name ' ;
                
				if ( ProspectsOrder::getDetails($db, $order, $fields, 
					" WHERE ".TABLE_PROSPECTS_ORDERS .".number = '". $or_no ."' ") > 0 ) {
                    $order = $order[0]; 
					/*
					$_ALL_POST['or_no']             = $order['or_no'];                            
					$_ALL_POST['or_details']        = $order['details'];
					$_ALL_POST['or_user_id']        = $order['user_id'];
					$_ALL_POST['or_number']         = $order['number'];
					$_ALL_POST['or_f_name']         = $order['f_name'];
					$_ALL_POST['or_l_name']         = $order['l_name'];
					$_ALL_POST['or_access_level']   = $order['access_level'];
					*/                            
					$_ALL_POST['or_no']             = $order['or_no'];
				   // $_ALL_POST['or_particulars']    = $order['particulars'];
					$_ALL_POST['or_details']        = $order['details'];
					$_ALL_POST['or_user_id']        = $order['user_id'];
					$_ALL_POST['or_number']         = $order['number'];
					$_ALL_POST['or_f_name']         = $order['f_name'];
					$_ALL_POST['or_l_name']         = $order['l_name'];
					$_ALL_POST['or_access_level']   = $order['access_level'];
					$_ALL_POST['do_o']              = $order['do_o'];
					$_ALL_POST['u_f_name']          = $order['u_f_name'];
					$_ALL_POST['u_l_name']          = $order['u_l_name'];
					$_ALL_POST['u_number']          = $order['u_number'];
					$_ALL_POST['order_title']       = $order['order_title'];
					$_ALL_POST['order_closed_by']   = $order['order_closed_by'];
					$_ALL_POST['order_closed_by2']   = $order['order_closed_by2'];
					$_ALL_POST['order_closed_by2_name']   = $order['order_closed_by2_name'];
					$_ALL_POST['is_renewable']   = $order['is_renewable'];
					
					if(!empty($order['order_closed_by'])){ 
						$condition = TABLE_USER." WHERE  user_id='".$order['order_closed_by']."'";
						$clientfields = ' '.TABLE_USER.'.user_id, '. TABLE_USER .'.number, 
						'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
						User::getList($db, $closed_by, $clientfields, $condition) ;
						$_ALL_POST['order_closed_by_name']   = $closed_by['0'];
					}
					$_ALL_POST['amount']= $order['amount'];
					$_ALL_POST['gramount']= number_format($order['amount'],2);
					$_ALL_POST['amount_inr']= $order['amount'] * $_ALL_POST['exchange_rate'];
					$_ALL_POST['currency_id']= $order['currency_id'];
					$_ALL_POST['company_id']= $order['company_id'];
				   
				    /*get particulars details bof*/
					$temp = NULL;
					$temp_p = NULL;
					$condition_query = "WHERE ord_no = '". $order['or_no'] ."'";
					ProspectsOrder::getParticulars($db, $temp, '*', $condition_query);
					if(!empty($temp)){
						foreach ( $temp as $pKey => $parti ) {
							/*
							if($temp[$pKey]['s_type']=='1'){
								$temp[$pKey]['s_type']='Years';
							}elseif($temp[$pKey]['s_type']=='2'){
								$temp[$pKey]['s_type']='Quantity';
							}*/
							$temp_p[]=array(
							'p_id'=>$temp[$pKey]['id'],
							'particulars'=>$temp[$pKey]['particulars'],
							'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
							's_id' =>$temp[$pKey]['s_id'] ,                                   
							's_type' =>$temp[$pKey]['s_type'] ,                                   
							's_quantity' =>$temp[$pKey]['s_quantity'] , 
							's_amount' =>number_format($temp[$pKey]['s_amount'],2),
							'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
							'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
							'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
							'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
							'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
							'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,    
							'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,
							'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
							'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                    
							'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                    
							'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
							'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)
							);
							
						}
					}
					$_ALL_POST['particulars']=$temp_p ;
				    /* deleted_proposal list to copy quotation */
					$_ALL_POST['deleted_proposal']=array();
					$fields = TABLE_PROSPECTS_QUOTATION .'.id '
					.','. TABLE_PROSPECTS_QUOTATION .'.number as inv_no'
					.','. TABLE_PROSPECTS_QUOTATION .'.quotation_status'
					.','. TABLE_PROSPECTS_QUOTATION .'.do_i'
					.','. TABLE_PROSPECTS_QUOTATION .'.do_c'
					.','. TABLE_PROSPECTS_QUOTATION .'.page1'
					.','. TABLE_PROSPECTS_QUOTATION .'.page2'
					.','. TABLE_PROSPECTS_QUOTATION .'.page3'
					.','. TABLE_PROSPECTS_QUOTATION .'.incentive_remarks'
					.','. TABLE_PROSPECTS_ORDERS .'.id as or_id'
					.','. TABLE_PROSPECTS_ORDERS .'.number'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by2'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by2_name'
					.','. TABLE_PROSPECTS_ORDERS .'.order_closed_by' ;
					$condition_query_qt = " WHERE (". TABLE_PROSPECTS_QUOTATION .".or_id = '". $order['id'] ."' AND 
					".TABLE_PROSPECTS_QUOTATION.".status ='".ProspectsQuotation::DELETED."' )
					ORDER BY ".TABLE_PROSPECTS_QUOTATION.".id DESC"; 
					ProspectsQuotation::getDetails($db, $_ALL_POST['deleted_proposal'], $fields, $condition_query_qt);
					
					
					$order = NULL;
					// Read the Client Addresses.
					include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
					$region         = new RegionProspects();
					$region->setAddressOf(TABLE_PROSPECTS, $_ALL_POST['or_user_id']);
					$_ALL_POST['address_list']  = $region->get();   
					
					
					
					
					
                } else {
                    $messages->setErrorMessage("The Selected Order was not found.");
                }
            }

            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['or_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['or_access_level'], $al_list, $index);
                $_ALL_POST['or_access_level'] = $al_list[$index[0]]['title'];
            }
			 
            // These parameters will be used when returning the control back to the List page.
            foreach( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'inv_id', 'value' => $inv_id);
            $hidden[] = array('name'=> 'or_no', 'value' => $or_no);
            $hidden[] = array('name'=> 'is_renewable', 'value' => $_ALL_POST['is_renewable']);
            $hidden[] = array('name'=> 'perform', 'value' => $perform);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
            $page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
			$page["var"][] = array('variable' => 'pqtlist', 'value' => 'pqtlist');
			$page["var"][] = array('variable' => 'oqtlist', 'value' => 'oqtlist');
            $page["var"][] = array('variable' => 'listUser', 'value' => 'listUser');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            //$page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            //$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-quotation-edit.html');
		}else{
			$messages->setErrorMessage("The Proposal ".$_ALL_POST['number']." is created. You can not edit the proposal once you created PDF.");
		} 
	}else{
		$messages->setErrorMessage("The Proposal ".$_ALL_POST['number']." is created. You can not edit the proposal once you created PDF.");

	}
        
}
else {
	$messages->setErrorMessage("You donot have the Permisson to Access this module.");
}
?>