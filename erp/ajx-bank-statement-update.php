<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/visitor-book.inc.php');
	   
	
   
    
	$id =  isset($_GET["stmt_id"])   ? $_GET["stmt_id"]  : ( isset($_POST["stmt_id"])    ? $_POST["stmt_id"] : '' );
    $private_comment = isset($_GET["private_comment"])? $_GET["private_comment"]:( isset($_POST["private_comment"])?$_POST["private_comment"] : '' );
    $public_comment = isset($_GET["public_comment"]) ? $_GET["public_comment"]  : ( isset($_POST["public_comment"])?$_POST["public_comment"] : '' );
    
     
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new 		= new db_local; // database handle
	
    if (isset($id) && $id!=''){   
		$sql= " UPDATE ".TABLE_BANK_STATEMENT." SET ".TABLE_BANK_STATEMENT.".private_comment='".$private_comment."',
		".TABLE_BANK_STATEMENT.".public_comment='".$public_comment."' 
		WHERE ".TABLE_BANK_STATEMENT.".id ='".$id."'";
		
		$execute= $db_new->query($sql);
		if($execute){
			$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
			<tr>
				<td class=\"message_header_ok\">
					<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
					Success
				</td>
			</tr>
			<tr>
				<td class=\"message_ok\" align=\"left\" valign=\"top\">
					<ul class=\"message_ok_2\">
						<li class=\"message_ok\">Record updated successfully </li>
					</ul>
				</td>
			</tr>
			</table>"; 				
		} 
	}	

echo $message  ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
