<?php
    if ( $perm->has('nc_bl_invprfm_add') ) {
    //nc_bl_or_adddm
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
		include_once ( DIR_FS_INCLUDES .'/common-functions.inc.php');       
		include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
		include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php' );
		$inv_id = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"]) ? $_POST["inv_id"]: '' );
 
        //echo $tbl_name;
        $_ALL_POST      = NULL;
        $data           = NULL;
        
		 
		
      
		
		//COPY ORDER BOF
		if(!empty($inv_id)){
		
			//$condition_query = " WHERE (".TABLE_BILL_INV_PROFORMA.".id = '". $inv_id ."') ";
			
			$condition_query = " WHERE (".TABLE_BILL_INV_PROFORMA.".id = '". $inv_id ."') AND 
		    ".TABLE_BILL_INV_PROFORMA.".dm_number=''";  

			$fields = TABLE_BILL_INV_PROFORMA .'.*'
                .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                .','. TABLE_CLIENTS .'.number AS c_number'
                .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                .','. TABLE_CLIENTS .'.billing_name'
                .','. TABLE_CLIENTS .'.email AS c_email';
				
                
			if ( InvoiceProforma::getList($db, $invDetails, $fields, $condition_query) > 0 ) {
				
				$data = $invDetails[0];
				$data['currency_abbr'] = '';
				$data['currency_name'] = '';
				$data['currency_symbol'] = '';
				$data['currency_country'] = '';
				$data['old_billing_address'] = '';
				$data['do_tax'] = $data['do_st'] = 0;
				$data['client']= array('number'=>$data['c_number'],
								'f_name'=>$data['c_f_name'],
								'l_name'=>$data['c_l_name']
							);
				$data['dm_dt'] = $data['do_i'] = strtotime($data['do_i']) ;
				$data['do_d'] = strtotime($data['do_d']) ;
				$detailDmNo = getDmNumber($data['dm_dt'],$data['company_id']); 				
				if(!empty($detailDmNo)){
					$data['number'] = $detailDmNo['number'];
					$data['dm_counter'] = $detailDmNo['dm_counter'];
					
					$sql = " UPDATE ".TABLE_BILL_INV_PROFORMA." SET
						 ".TABLE_BILL_INV_PROFORMA.".dm_counter ='".$data['dm_counter']."',
						 ".TABLE_BILL_INV_PROFORMA.".dm_number ='".$data['number']."',
						 ".TABLE_BILL_INV_PROFORMA.".dm_dt ='".date('Y-m-d H:i:s', $data['dm_dt'])."',
						 ".TABLE_BILL_INV_PROFORMA.".dm_create_dt ='".date('Y-m-d H:i:s')."',
						 ".TABLE_BILL_INV_PROFORMA.".dm_created_by ='".$my['user_id']."',
						 ".TABLE_BILL_INV_PROFORMA.".dm_created_by_name ='".$my['f_name']." ".$my['l_name']."',
						 ".TABLE_BILL_INV_PROFORMA.".dm_ip   = '".$_SERVER['REMOTE_ADDR']."'
						 WHERE ".TABLE_BILL_INV_PROFORMA.".id='".$inv_id."'";
					
					$db->query($sql);    
					
				}
				
				 
				$currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id = ".$data['currency_id'];
                $fields1 = "*";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);              
                if(!empty($currency1)){
                   $data['currency_abbr'] = $currency1[0]['abbr'];
                   $data['currency_name'] = $currency1[0]['currency_name'];
                   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
                   $data['currency_country'] = $currency1[0]['country_name'];
                }
				
				
				
				
				//get currency abbr from d/b eof                      
                $company1 =null;
                $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                $fields_c1 = "name,prefix,quot_prefix, 
				tin_no,service_tax_regn,pan_no,cst_no,vat_no,do_implement,do_implement_st";
                Company::getList( $db, $company1, $fields_c1, $condition_query_c1); 
				$data['do_tax']=$data['do_st']=0;
				$do_tax=$do_st='';
                if(!empty($company1)){                  
                   $data['company_name'] = $company1[0]['name'];
                   $data['company_prefix'] = $company1[0]['prefix'];
                   $data['company_quot_prefix'] = $company1[0]['quot_prefix'];
				   $data['do_tax']     = $company1[0]['do_implement'];	
				   $data['do_st']     = $company1[0]['do_implement_st'];	
				   
				   $data['tin_no'] 			 = '';
				   $data['service_tax_regn'] 	 = '';
				   $data['pan_no'] 			 = $company1[0]['pan_no'];
				   $data['cst_no'] 			 = '';
				   $data['vat_no'] 			 = '';
				   
				    if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
						$do_tax=$data['do_tax'];
						$data['do_tax']= strtotime($data['do_tax']);				
					}					
					if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){
					   $data['tin_no'] 			 = $company1[0]['tin_no'];
					   $data['cst_no'] 			 = $company1[0]['cst_no'];
					   $data['vat_no'] 			 = $company1[0]['vat_no'];
					}
					if( $data['do_st']!=0 &&  $data['do_st']!='0000:00:00 00:00:00'){
						$do_st = $data['do_st'];
						$data['do_st'] = strtotime($data['do_st']);				
					}
					if($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00'){
					   $data['service_tax_regn'] = $company1[0]['service_tax_regn'];
					}
					
                }
                $data['invoice_title'] = 'DELIVERY NOTE (CHALLAN)';
                $data['profm']='';	
				$show_period=$data['show_period_chk']=0;
				  
				if(isset($data['show_period'])){
					$data['show_period_chk'] = $data['show_period'];
				} 
				
				if( $_ALL_POST['do_e'] =='0000-00-00 00:00:00'){
					$_ALL_POST['do_e']  ='';
				}else{
					$_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
					$temp               = explode('-', $_ALL_POST['do_e'][0]);
					$_ALL_POST['do_e']  = NULL;
					$_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}
				if( $_ALL_POST['do_fe'] =='0000-00-00 00:00:00'){
					$_ALL_POST['do_fe']  ='';
				}else{
					$_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
					$temp               = explode('-', $_ALL_POST['do_fe'][0]);
					$_ALL_POST['do_fe']  = NULL;
					$_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
				}
				 
				$data['amount']= number_format($data['amount'],2);
                $data['amount_inr']= number_format($data['amount_inr'],2);
                $data['balance']= number_format($data['balance'],2);
                $data['amount_words']=processSqlData($data['amount_words']);
                $data['do_i_chk'] = $data['do_i'];			 
			    $data['do_rs_symbol'] = strtotime('2010-07-20');
			    $data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
			    $data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
				 
				$data['sub_total_amount'] = 0;
				$data['tax1_total_amount']=$data['tax1_sub1_total_amount'] = $data['tax1_sub2_total_amount'] = 0;
				
			    $data['sub_total_amount'] = 0;
				$data['tax1_total_amount']=$data['tax1_sub1_total_amount'] = $data['tax1_sub2_total_amount'] = 0;
				$temp = NULL;
				$temp_p = NULL; 
				$condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
				$data['show_discount']=0;
				$data['show_nos']=0;
				$data['colsSpanNo1']=5;
				$data['colsSpanNo2']=6;
				Order::getParticulars($db, $temp, '*', $condition_query);
				if(!empty($temp)){
					$data['wd1']=10;
					$data['wdinvp']=245;//217+28
					$data['wdpri']=98;//70+28
					$data['wdnos']=98;//70+28
					$data['wdamt']=97;//70+27
					$data['wddisc']=77; //50+27
					$data['wdst']=97;   //70+27                     
					$data['wd2']=9;
					
					foreach ( $temp as $pKey => $parti ) {                      
						   
						if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
							$data['show_discount'] = 1; 
						}
						if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
							$data['show_nos']=1;     
						}                            
						$temp_p[]=array(
							'p_id'=>$temp[$pKey]['id'],
							'particulars'=>$temp[$pKey]['particulars'],
							'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                            	
							's_type' =>$temp[$pKey]['s_type'] ,                                   
							's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
							's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                            
							's_id' =>$temp[$pKey]['s_id'] ,                                   
							'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
							'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
							'tax1_name' =>$temp[$pKey]['tax1_name'] ,    
							'tax1_number' =>$temp[$pKey]['tax1_number'] ,                            
							'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
							'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,     
							'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2),							
							'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,    
							'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                            
							'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
							'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,     
							'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,							'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,    
							'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                             
							'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
							'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,     
							'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,							'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                            
							'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                            'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                            
							'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
							'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)
						);
							
						if(($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00')
							|| ($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00')
						){
							//if(!empty($temp[$pKey]['tax1_id'])){
							$data['sub_total_amount'] = $data['sub_total_amount'] + $temp[$pKey]['stot_amount'] ; 
							$data['tax1_id'] =   $temp[$pKey]['tax1_id'] ; 
							$data['tax1_number'] =   $temp[$pKey]['tax1_number'] ; 
							$data['tax1_name'] =   $temp[$pKey]['tax1_name'] ; 
							$data['tax1_value'] =   $temp[$pKey]['tax1_value'] ; 
							$data['tax1_pvalue'] =   $temp[$pKey]['tax1_pvalue'] ; 
							$data['tax1_total_amount']=$data['tax1_total_amount'] + $temp[$pKey]['tax1_amount'] ; 
							
							$data['tax1_sub1_id'] =   $temp[$pKey]['tax1_sub1_id'] ; 
							$data['tax1_sub1_number'] =   $temp[$pKey]['tax1_sub1_number'] ; 
							$data['tax1_sub1_name'] =   $temp[$pKey]['tax1_sub1_name'] ; 
							$data['tax1_sub1_value'] =   $temp[$pKey]['tax1_sub1_value'] ; 
							$data['tax1_sub1_pvalue'] =   $temp[$pKey]['tax1_sub1_pvalue'] ; 
							$data['tax1_sub1_total_amount'] =  $data['tax1_sub1_total_amount'] + 							$temp[$pKey]['tax1_sub1_amount'] ; 
							
							$data['tax1_sub2_id'] =   $temp[$pKey]['tax1_sub2_id'] ; 
							$data['tax1_sub2_number'] =   $temp[$pKey]['tax1_sub2_number'] ; 
							$data['tax1_sub2_name'] =   $temp[$pKey]['tax1_sub2_name'] ; 
							$data['tax1_sub2_value'] =   $temp[$pKey]['tax1_sub2_value'] ; 
							$data['tax1_sub2_pvalue'] =   $temp[$pKey]['tax1_sub2_pvalue'] ; 
							$data['tax1_sub2_total_amount'] =  $data['tax1_sub2_total_amount'] + 
							$temp[$pKey]['tax1_sub2_amount'] ; 
							//}
						}							
					}
					
					if($data['show_discount'] ==0){ 
						 
						$data['wdinvp']= $data['wdinvp']+50 + 70;
						$data['wdpri'] = $data['wdpri'] +10 + 10;
						$data['wdamt'] = $data['wdamt'] + 17+ 17;                            
						//$data['wdtx']  = $data['wdtx'] + 5+ 6;
						//$data['wdtxp'] = $data['wdtxp'] +10 + 6;
						//$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
						
						$data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
						$data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
						
					}
					if($data['show_nos'] ==0){
						 
						$data['wdinvp']= $data['wdinvp']+80;
						$data['wdpri'] = $data['wdpri'] + 8;
						$data['wdamt'] = $data['wdamt'] + 10;
						/*
						$data['wdtx']  = $data['wdtx'] + 10;
						$data['wdtxp'] = $data['wdtxp'] + 10;
						$data['wdtotam'] =$data['wdtotam'] + 10;*/
						$data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
						$data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;
					}                        
				}
				$sub_tax_str='';
                if(!empty($data['tax1_name'])){
					
					//get the tax's declaration bof
					$condition1 = " WHERE id = ".$data['tax1_id']." LIMIT 0,1" ;
					$required_fields='declaration';
					$parentTaxDetails=array();
					$data['tax1_declaration']='';
					ServiceTax::getDetails( $db, $parentTaxDetails, $required_fields, $condition1);
					if(!empty($parentTaxDetails)){
						$arr = $parentTaxDetails[0];
						$data['tax1_declaration'] = $arr['declaration'];
					}						
					//get the tax's declaration eof							 
					$data['sub_total_amount'] = number_format($data['sub_total_amount'],2);
					$data['tax1_total_amount'] = number_format($data['tax1_total_amount'],2);
					$data['tax1_sub1_total_amount'] = number_format($data['tax1_sub1_total_amount'],2);
					$data['tax1_sub2_total_amount'] = number_format($data['tax1_sub2_total_amount'],2);
				}
					
                $data['particulars'] = $temp_p ;                    
                // Update the Order. for time being   
				$data['do_i_chk'] = $data['do_i'] ;
				$data['do_rs_symbol'] = strtotime('2010-07-20'); //Rupee symbol published
				$data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
				$data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
				
				Order::createPdfDmFile($data);
				$data = $_ALL_POST=null;
				$messages->setOkMessage("The DM created successfully.");
			} else {
				$messages->setErrorMessage("The DM was already created, Please download the DM");
			}
		}
		
		
		$hidden[] = array('name'=> 'ajx' ,'value' => $ajx);
		$hidden[] = array('name'=> 'perform' ,'value' => $perform);
		$hidden[] = array('name'=> 'act' , 'value' => 'save');		
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["var"][] = array('variable' => 'data', 'value' => 'data'); 		
		
		//$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-add-dm.html');
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
     
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'popup.html');
    }
?>