<?php
    if ( $perm->has('nc_emtpl_add') ) {
        $_ALL_POST	= NULL;
        $data       = NULL;
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            $extra = array( 'db' 				=> &$db,
                            'messages'          => $messages
                        );
            
            if (EmailTemplates::validateAdd($data, $extra)) {
                $query	= " INSERT INTO ".TABLE_SETTINGS_EMAIL_TPL
                            ." SET ". TABLE_SETTINGS_EMAIL_TPL .".name 		        = '".$data['name'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".description	    = '".$data['description'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".from_name	        = '".$data['from_name'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".from_email        = '".$data['from_email'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".subject           = '".$data['subject'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".text              = '".$data['text'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".allowed_constant  = '".$data['allowed_constant'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".is_html           = '".$data['is_html'] ."'"
                                .",". TABLE_SETTINGS_EMAIL_TPL .".status            = '".$data['status'] ."'";
                if ($db->query($query) && $db->affected_rows() > 0) {
                    $messages->setOkMessage("Email Template added successfully.");	
                    $variables['hid'] = $db->last_inserted_id();
                    
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }
            }
        }
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/settings-templates.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/settings-templates.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
		header("Location:".DIR_WS_NC."/settings-templates.php?added=1");   

        }else {
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
    
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-templates-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>