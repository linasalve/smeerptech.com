<?php
if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
				   
  
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
	
	include_once ( DIR_FS_INCLUDES .'/vendors-bank.inc.php' );


	$query = "SELECT * FROM ".TABLE_VENDORS_BANK." WHERE status='1'  ";	
	$i=0;
	if ( $db->query($query) ) {
		if ( $db->nf() > 0 ) {
			while ($db->next_record()) {
				
				$user_id =trim($db->f('user_id'));				
				$number =trim($db->f('number'));
                $parent_id =$db->f('parent_id');				
                $service_id =$db->f('service_id');
                $authorization_id =$db->f('authorization_id');
                $manager =processUserData($db->f('manager'));
                $username =processUserData($db->f('username'));
                $password =$db->f('password');
                $access_level =$db->f('access_level');
                $created_by =$db->f('created_by');
                $team =processUserData($db->f('team'));
                $roles =processUserData($db->f('roles'));
                $status =$db->f('status');
                $credit_limit =processUserData($db->f('credit_limit'));
                $grade =processUserData($db->f('grade'));
                $email =processUserData($db->f('email'));
                $email_1 =processUserData($db->f('email_1'));
                $email_2 =processUserData($db->f('email_2'));
                $email_3 = processUserData($db->f('email_3'));
                $email_4 = processUserData($db->f('email_4'));
                $additional_email = processUserData($db->f('additional_email'));
                $title = processUserData($db->f('title'));
                $f_name = processUserData($db->f('f_name'));
                $m_name = processUserData($db->f('m_name'));
                $l_name = processUserData($db->f('l_name'));
                $p_name = processUserData($db->f('p_name'));
                $bank_account_no = processUserData($db->f('bank_account_no'));
                $cst_no =processUserData($db->f('cst_no'));
                $vat_no =processUserData($db->f('vat_no'));
                $pan_no =processUserData($db->f('pan_no'));
                $service_tax_no =processUserData($db->f('service_tax_no'));
                $desig = processUserData($db->f('desig'));
                $org = processUserData($db->f('org'));
                $domain = processUserData($db->f('domain'));
                $gender = processUserData($db->f('gender'));
                $do_birth = processUserData($db->f('do_birth'));
                $do_aniv = processUserData($db->f('do_aniv'));
                $do_add = processUserData($db->f('do_add'));
                $do_reg = processUserData($db->f('do_reg'));
                $policy_check =processUserData($db->f('policy_check'));
                $ip_reg =$db->f('ip_reg');
                $do_login =$db->f('do_login');
                $allow_ip =processUserData($db->f('allow_ip'));
                $valid_ip =processUserData($db->f('valid_ip'));
                $remarks =processUserData($db->f('remarks'));
                $lead_id =$db->f('lead_id');
                $industry =$db->f('industry');
                $wt_you_do =processUserData($db->f('wt_you_do'));
                $mobile1 =processUserData($db->f('mobile1'));
                $mobile2 =processUserData($db->f('mobile2'));
                $spouse_dob =processUserData($db->f('spouse_dob'));
                $alloted_clients = processUserData($db->f('alloted_clients'));
                $billing_name = processUserData($db->f('billing_name'));
                $is_soft_copy = $db->f('is_soft_copy');
                $send_mail =$db->f('send_mail');
				
				 
				echo $query1 = " INSERT INTO ". TABLE_CLIENTS
				." SET ". TABLE_CLIENTS .".user_id     	= '". $user_id ."'"
				.",". TABLE_CLIENTS .".parent_id   = '". $parent_id ."'"
				.",". TABLE_CLIENTS .".service_id   = '". $service_id ."'"
				.",". TABLE_CLIENTS .".member_type   = '6'"
				.",". TABLE_CLIENTS .".authorization_id = '". $authorization_id ."'"
				.",". TABLE_CLIENTS .".number      	= '". $number ."'"
				.",". TABLE_CLIENTS .".manager 	   	= '". $manager ."'"
				.",". TABLE_CLIENTS .".team			= '". $team."'"
				.",". TABLE_CLIENTS .".username    	= '". $username ."'"
				.",". TABLE_CLIENTS .".password    	= '". $password ."'"
				.",". TABLE_CLIENTS .".access_level	= '". $access_level ."'"
				.",". TABLE_CLIENTS .".created_by	= '". $created_by ."'"
				.",". TABLE_CLIENTS .".roles       	= '". $roles ."'"
				.",". TABLE_CLIENTS .".email       = '". $email ."'"
				.",". TABLE_CLIENTS .".check_email = '0'"
				.",". TABLE_CLIENTS .".email_1     = '". $email_1 ."'"						    
				.",". TABLE_CLIENTS .".email_2     = '". $email_2 ."'"						
				.",". TABLE_CLIENTS .".email_3     	= '". $email_3 ."'"							
				.",". TABLE_CLIENTS .".email_4     	= '". $email_4 ."'"						
				.",". TABLE_CLIENTS .".additional_email = '". $additional_email ."'"
				.",". TABLE_CLIENTS .".title       	= '". $title ."'"
				.",". TABLE_CLIENTS .".f_name      	= '". $f_name ."'"
				.",". TABLE_CLIENTS .".m_name      	= '". $m_name ."'"
				.",". TABLE_CLIENTS .".l_name      	= '". $l_name ."'"
				.",". TABLE_CLIENTS .".p_name      	= '". $p_name."'"
				.",". TABLE_CLIENTS .".grade      	= '". $grade ."'"
				.",". TABLE_CLIENTS .".credit_limit = '". $credit_limit ."'"
				.",". TABLE_CLIENTS .".desig       	= '". $desig."'"
				.",". TABLE_CLIENTS .".cst_no       = '". $cst_no ."'"
				.",". TABLE_CLIENTS .".vat_no       = '". $vat_no ."'"
				.",". TABLE_CLIENTS .".service_tax_no  = '". $service_tax_no ."'"
				.",". TABLE_CLIENTS .".pan_no  		= '". $pan_no ."'"							 
				.",". TABLE_CLIENTS .".org         	= '". $org ."'"
				.",". TABLE_CLIENTS .".domain      	= '". $domain ."'"
				.",". TABLE_CLIENTS .".gender      	= '". $gender ."'"
				.",". TABLE_CLIENTS .".do_birth    	= '". $do_birth ."'"
				.",". TABLE_CLIENTS .".do_aniv     	= '". $do_aniv ."'"
				.",". TABLE_CLIENTS .".remarks     	= '". $remarks ."'"
				.",". TABLE_CLIENTS .".industry     = '". $industry."'"
				.",". TABLE_CLIENTS .".wt_you_do    = '". $wt_you_do ."'"
				.",". TABLE_CLIENTS .".mobile1      = '". $mobile1 ."'"
				.",". TABLE_CLIENTS .".mobile2      = '". $mobile2 ."'"
				.",". TABLE_CLIENTS .".alloted_clients   = '". $alloted_clients ."'"
				.",". TABLE_CLIENTS .".billing_name   = '". $billing_name."'"
				.",". TABLE_CLIENTS .".spouse_dob   = '". $spouse_dob ."'"
				.",". TABLE_CLIENTS .".do_add      	= '". $do_add."'"
				.",". TABLE_CLIENTS .".status      	= '". $status."'" ;
				
				echo "<br/>";
				echo "<br/>";
			}
		}
	}



	
?>