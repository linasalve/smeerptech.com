<?php
// $start['time'] = time();
// $start['microtime'] = microtime();
    if ( $perm->has('nc_c_su_edit') ) {
        $user_id = isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
        $client_id	= isset($_GET["client_id"]) ? $_GET["client_id"] : ( isset($_POST["client_id"]) ? $_POST["client_id"] : '' );
        
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
		include_once ( DIR_FS_CLASS .'/DomainAccess.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
        include ( DIR_FS_INCLUDES .'/sms-client-account.inc.php');
        include_once ( DIR_FS_INCLUDES .'/client-domains.inc.php');
		
		$condition_query_dm = " WHERE ".TABLE_CLIENT_DOMAINS.".client='".$client_id."'";
		$fieldsdm = "*";
		ClientDomains::getDetails( $db, $domainlist1, $fieldsdm, $condition_query_dm);
		
		
        $_ALL_POST      = NULL;
        $data           = NULL;
        $region         = new Region();
        $phone          = new Phone(TABLE_CLIENTS);
        $reminder       = new UserReminder(TABLE_CLIENTS);
		$domain_acc 	= new DomainAccess(TABLE_CLIENTS);
        $access_level   = $my['access_level'];
        
		$lst_sms_acc=null;
        $fields = TABLE_SMS_CLIENT_ACCOUNT.'.name as account_name'
				.','.TABLE_SMS_CLIENT_ACCOUNT .'.id as account_id'
				.','.TABLE_CLIENTS .'.user_id'
                .','. TABLE_CLIENTS .'.f_name'.','.TABLE_CLIENTS .'.l_name';
        $condition_query1= "LEFT JOIN ".TABLE_CLIENTS." ON ".TABLE_CLIENTS.".user_id=".TABLE_SMS_CLIENT_ACCOUNT.".client_id 
						WHERE ".TABLE_SMS_CLIENT_ACCOUNT.".client_id = '".$client_id."'";
        SmsClientAccount::getList($db,$lst_sms_acc,$fields,$condition_query1);
		 
		
        if ( $perm->has('nc_c_su_edit_al') ) {
            $access_level += 1;
        }
        //For Clients list allotment
        /*if ( $perm->has('nc_uc') && $perm->has('nc_uc_list')) {
            $variables['can_view_clist']     = true;
        }*/
        
        $al_list    = getAccessLevel($db, $access_level);
        //$role_list  = Clients::getRoles($db, $access_level);

        //$grade_list  = Clients::getGrades();
        
        //BOF read the available services
		$authorization_list  = Clients::getAuthorization();
        $services_list  = Clients::getServices();
        //EOF read the available services
        $memberTypeList  = Clients::getMemberType();
        //BOF read the available industries
        /*$industry_list = NULL;
        $required_fields ='*';
        $condition_query="WHERE status='".Industry::ACTIVE."'";
		Industry::getList($db,$industry_list,$required_fields,$condition_query);*/
        //EOF read the available industries
        
        //BOF read the available industries
		Clients::getCountryCode($db,$country_code);
        //EOF read the available industries
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
							'domain_acc'     => $domain_acc,
                            'reminder'  => $reminder
                        );
            
            if ( Clients::validateSubUserUpdate($data, $extra) ) {
				$sms_accounts ='';
				if(!empty($data['sms_accounts'])){  
					$sms_accounts = implode(",",$data['sms_accounts']);
					$sms_accounts = ','.$sms_accounts.',';
				}
				
                $service_ids = $data['service_id'];
                $service_idstr = implode(",",$service_ids);
                
                if(!empty($service_idstr)){
                    $service_idstr=",".$service_idstr.",";	
                }
                $authorization_idstr='';
                if(isset($_POST['authorization_id']) && !empty($_POST['authorization_id'])){
                    $authorization_ids=$_POST['authorization_id'];
                    $authorization_idstr=implode(",",$authorization_ids);
                    $authorization_idstr = ",".$authorization_idstr.",";	
                }
				$membertype_ids = $_POST['member_type'];
                $membertypestr = implode(",",$membertype_ids);
                if(!empty($membertypestr)){
                    $membertypestr = ",".$membertypestr.",";
                }
				if( isset($data['check_email'])){
					$data['check_email']=1;
				}else{
					$data['check_email']=0;
				}
				
				
                $query  = " UPDATE ". TABLE_CLIENTS
                        ." SET "
                        ."". TABLE_CLIENTS .".service_id   = '". $service_idstr ."'"
						.",". TABLE_CLIENTS .".authorization_id   = '". $authorization_idstr ."'"
					    /* 
					    .",". TABLE_CLIENTS .".number      = '". $data['number'] ."'"
					    .",". TABLE_CLIENTS .".manager     = '". $data['manager'] ."'" */
						//.",". TABLE_CLIENTS .".team			= '". implode(',', $data['team']) ."'"
						. ",". TABLE_CLIENTS .".username    = '". $data['username'] ."'"
						. $data['password']
						//.",". TABLE_CLIENTS .".access_level= '". $data['access_level'] ."'"
						.",". TABLE_CLIENTS .".sms_accounts   = '". $sms_accounts ."'"
						.",". TABLE_CLIENTS .".member_type   = '". $membertypestr ."'"
						.",". TABLE_CLIENTS .".enable_login  = '". $data['enable_login'] ."'"
						.",". TABLE_CLIENTS .".email       	= '". $data['email'] ."'"
						.",". TABLE_CLIENTS .".check_email 	= '". $data['check_email'] ."'"
						.",". TABLE_CLIENTS .".email_1     	= '". $data['email_1'] ."'"
						.",". TABLE_CLIENTS .".email_2     = '". $data['email_2'] ."'"
						.",". TABLE_CLIENTS .".title       = '". $data['title'] ."'"
						.",". TABLE_CLIENTS .".f_name      = '". $data['f_name'] ."'"
						.",". TABLE_CLIENTS .".m_name      = '". $data['m_name'] ."'"
						.",". TABLE_CLIENTS .".l_name      = '". $data['l_name'] ."'"
						.",". TABLE_CLIENTS .".p_name      = '". $data['p_name'] ."'"                            
						.",". TABLE_CLIENTS .".do_update      = '". date('Y-m-d H:i:s') ."'"
						.",". TABLE_CLIENTS .".updated_by      = '". $my['uid']."'"  
						.",". TABLE_CLIENTS .".updated_by_name = '". $my['f_name']." ".$my['l_name']."'"
						//.",". TABLE_CLIENTS .".grade      	= '". $data['grade'] ."'"
						//.",". TABLE_CLIENTS .".credit_limit = '". $data['credit_limit'] ."'"
						.",". TABLE_CLIENTS .".desig       = '". $data['desig'] ."'"
						.",". TABLE_CLIENTS .".org         = '". $data['org'] ."'"
						.",". TABLE_CLIENTS .".domain      = '". $data['domain'] ."'"
						.",". TABLE_CLIENTS .".gender      = '". $data['gender'] ."'"
						.",". TABLE_CLIENTS .".do_birth    = '". $data['do_birth'] ."'"
						.",". TABLE_CLIENTS .".do_aniv     = '". $data['do_aniv'] ."'"
						.",". TABLE_CLIENTS .".remarks     = '". $data['remarks'] ."'"
						.",". TABLE_CLIENTS .".special_remarks 	= '". $data['special_remarks'] ."'"
						.",". TABLE_CLIENTS .".payment_remarks 	= '". $data['payment_remarks'] ."'"
						.",". TABLE_CLIENTS .".search_keywords 	 = '". $data['search_keywords'] ."'"
						.",". TABLE_CLIENTS .".company_details 	 = '". $data['company_details'] ."'"
						.",". TABLE_CLIENTS .".connected_via 	 = '". $data['connected_via'] ."'"
						.",". TABLE_CLIENTS .".mobile1      = '". $data['mobile1'] ."'"
						.",". TABLE_CLIENTS .".mobile2      = '". $data['mobile2'] ."'"
						.",". TABLE_CLIENTS .".allow_ip   = '". $data['allow_ip'] ."'"
						.",". TABLE_CLIENTS .".valid_ip    = '". $data['valid_ip'] ."'"
						.",". TABLE_CLIENTS .".status      = '". $data['status'] ."'" 
                        ." WHERE ". TABLE_CLIENTS .".user_id   = '". $data['user_id'] ."'";
                
                if ( $db->query($query) ) {
                    $variables['hid'] = $data['user_id'];
					//Push email id into Newsletters Table BOF
					$table = TABLE_NEWSLETTERS_MEMBERS;
					include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
					if(!empty($data['email'])){
						$domain1 = strtolower(strstr($data['email'], '@'));
						$condi1= " WHERE email='".strtolower(trim($data['email']))."'";
						if( $domain1!= 'smeerptech.com' && 
							!NewslettersMembers::duplicateFieldValue($db, $table, 'email',$condi1) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email']))."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_1'])){
						$domain2 = strtolower(strstr($data['email_1'], '@'));
						$condi2= " WHERE email='".strtolower(trim($data['email_1']))."'";
						if( $domain2!='smeerptech.com' && 
						  !NewslettersMembers::duplicateFieldValue($db, $table, 'email',$condi2) ) {
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_1']))."'" ;
						  $db->query($sql1);
						}
					}
					if(!empty($data['email_2'])){
						$domain3 = strtolower(strstr($data['email_2'], '@'));
						$condi3 = " WHERE email='".strtolower(trim($data['email_2']))."'";
						if($domain3 != 'smeerptech.com' && !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $condi3) ) {
						  
						  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
						  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".strtolower(trim($data['email_2']))."'" ;
						  $db->query($sql1);
						}
					}
					//Push email id into Newsletters Table EOF
					
                    // Update the Contact Numbers.
                    // Update the address.
                    for ( $i=0; $i<$data['address_count']; $i++ ) {
                        if ( isset($data['a_remove'][$i]) && $data['a_remove'][$i] == '1') {
                            if ( ! $region->delete($db, $data['address_id'][$i], $variables['hid'], TABLE_CLIENTS) ) {
                                foreach ( $phone->getError() as $errors) {
                                    $messages->setOkMessage($errors['description']);
                                }
                            }
                        }else{
                            $address_arr = array(   
								'id'   => isset($data['address_id'][$i])? $data['address_id'][$i] : '0',
								'address_type'  => $data['address_type'][$i],
								'company_name'  => '',
								'address'       => $data['address'][$i],
								'city'          => $data['city'][$i],
								'state'         => $data['state'][$i],
								'country'       => $data['country'][$i],
								'zipcode'       => $data['zipcode'][$i],
								'is_preferred'  => isset($data['is_preferred'][$i])? $data['is_preferred'][$i] : '0',
								'is_verified'   => isset($data['is_verified'][$i])? $data['is_verified'][$i] : '0'
                            );
                            
                            if ( !$region->update($variables['hid'], TABLE_CLIENTS, $address_arr) ) {
                                foreach ( $region->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. 
									$errors['description']);
                                }
                            }
                        }
                    }
                    
                    // Update the Contact Numbers.
                    for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {

                            if ( isset($data['p_remove'][$i]) && $data['p_remove'][$i] == '1') {
                                if ( ! $phone->delete($db, $data['phone_id'][$i], $variables['hid'], TABLE_CLIENTS) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            } else {
                               $phone_arr = array( 
									'id'            => isset($data['phone_id'][$i]) ? $data['phone_id'][$i] : '',
									'p_type'        => $data['p_type'][$i],
									'cc'            => $data['cc'][$i],
									'ac'            => $data['ac'][$i],
									'p_number'      => $data['p_number'][$i],
									'p_is_preferred'=> isset($data['p_is_preferred'][$i]) ? $data['p_is_preferred'][$i] : '0',
									'p_is_verified' => isset($data['p_is_verified'][$i])?$data['p_is_verified'][$i]:'0'
                                );
                                if ( ! $phone->update($db, $variables['hid'], TABLE_CLIENTS, $phone_arr) ) {
                                    foreach ( $phone->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
					// Update the Contact Numbers.
                    for ( $i=0; $i<$data['domain_count']; $i++ ) {
                        if ( !empty($data['domain_id'][$i]) ) {
                            if ( isset($data['d_remove'][$i])) {
                                if ( ! $domain_acc->delete($db, $data['d_remove'][$i]) ) {
                                    foreach ( $domain_acc->getError() as $errors) {
                                        $messages->setOkMessage($errors['description']);
                                    }
                                }
                            } else {
								$dmn_arr = array(   'domain_id'        	=> $data['domain_id'][$i],
													'excluded_emails'    => $data['excluded_emails'][$i],
													'cda_id'           	=> $data['cda_id'][$i] 
												);
                                if ( ! $domain_acc->update($db, $variables['hid'], $dmn_arr) ) {
                                    foreach ( $domain_acc->getError() as $errors) {
                                        $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
                                    }
                                }
                            }
                        }
                    }
					 
					
                    // Update the Reminders.
                    for ( $i=0; $i<$data['reminder_count']; $i++ ) {
                        if ( !empty($data['do_reminder'][$i]) && 
							!empty($data['subject'][$i]) && !empty($data['description'][$i]) ) {
                            
							$reminder_arr = array(
								'id' => (isset($data['remind_id'][$i])? $data['remind_id'][$i] : ''),
								'do_reminder'   => $data['do_reminder'][$i],
								'subject'   	=> $data['subject'][$i],
								'description'   => $data['description'][$i]
							); 
							
                            if(!$reminder->update($db, $variables['hid'], TABLE_CLIENTS, $reminder_arr)){
                                $messages->setErrorMessage('There is/are error(s) in Reminder '. ($i+1) .'.');
                                foreach ( $reminder->getError() as $errors) {
                                    $messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. 
									$errors['description']);
                                }
                            }
                        }
                    }
					
					if($_ALL_POST['send_service_mail']=='1'){  
						/*SEND SERVICE PDF BOF*/
						include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
						include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
						include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php'); 
						$id=39;					
						$fields = TABLE_ST_TEMPLATE .'.*'  ;            
						$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
						SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1);
						
						if(!empty($details)){
							$data['ticket_owner_uid']=$client_id;
							$table = TABLE_CLIENTS;
							$fields1 =  TABLE_CLIENTS .'.billing_name,'.TABLE_CLIENTS.'.f_name,
							'.TABLE_CLIENTS.'.l_name ';
							$condition1 = " WHERE ".TABLE_CLIENTS .".user_id = '".$client_id."' " ;
							$arr = getRecord($table,$fields1,$condition1);
							if(!empty($arr)){
								$data['ticket_owner'] = $arr['f_name']." ".$arr['l_name'];
							}						 
							$details = $details[0];
							$data['template_id'] = $details['id'];
							$data['template_file_1'] = $details['file_1'];
							$data['template_file_2'] = $details['file_2'];
							$data['template_file_3'] = $details['file_3'];
							$data['subject'] = processUserData($details['subject']);
							$data['text'] = processUserData($details['details']);
							
							$data['display_name']=$data['display_user_id']= $data['display_designation']='';				
							$randomUser = getCeoDetails();
							$data['display_name'] = $randomUser['name'];
							$data['display_user_id'] = $randomUser['user_id'];
							$data['display_designation'] = $randomUser['desig'];
							
							$ticket_no  =  SupportTicket::getNewNumber($db);
							$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
							
							$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
							. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_ST_TICKETS .".template_id       = '". $data['template_id'] ."', "
							. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_ST_TICKETS .".ticket_subject    = '". $data['subject'] ."', "
							. TABLE_ST_TICKETS .".ticket_text       = '". $data['text'] ."', "
							. TABLE_ST_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
							. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
							. TABLE_ST_TICKETS .".ticket_child      = '0', "
							. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_ST_TICKETS .".min = '". $data['min']."', "
							. TABLE_ST_TICKETS .".service_pdf_tkt = '1', "
							. TABLE_ST_TICKETS .".ticket_response_time = '0', "
							. TABLE_ST_TICKETS .".ticket_replied        = '0', "
							. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
							. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
							. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
							. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
							. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
							. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ;
							
							if(!empty($data['template_file_1'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
							}
							if(!empty($data['template_file_2'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
							}
							if(!empty($data['template_file_3'])){
								$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
							}
							if(!empty($data['email']) || !empty($data['email_1']) || !empty($data['email_2'])){
								$db->query($query) ;
								$ticket_id = $db->last_inserted_id() ;
								$mail_send_to_su='';
								$data['mticket_no']   =   $ticket_no ;
								if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ){
									$to = '';
									if(!empty($data['email'])){									
										$to[]   = array('name' => $data['email'], 'email' => 
										$data['email']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email'];
										/* SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
									if(!empty($data['email_1'])){
										//$to = '';
										$to[]   = array('name' => $data['email_1'], 'email' => 
										$data['email_1']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email_1'];
										/* SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
									if(!empty($data['email_2'])){
										//$to = '';
										$to[]   = array('name' => $data['email_2'], 'email' => 
										$data['email_2']);                                   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$data['email_2'];
										/* SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									}
								 
									if(!empty($to)){
										//$to = '';
										$bcc[]  = array('name' => $data['ticket_owner'] , 'email' => 
										$smeerp_client_email);        
										SendMailSupport($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name);
										//echo  $email["body"];
									}
								}
								
								if(!empty($mail_send_to_su)){
									$query_update = "UPDATE ". TABLE_ST_TICKETS 
										." SET ". TABLE_ST_TICKETS .".mail_send_to_su
										= '".trim($mail_send_to_su,",")."'"                                   
										." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
									$db->query($query_update) ;
								}
							}
						}
					}
					/*SEND SERVICE PDF EOF*/
					$messages->setOkMessage("Clients Profile has been updated.");
                    //to flush the data.
                    $_ALL_POST  = NULL;
                    $data       = NULL;
                }else{
                    $messages->setErrorMessage('Clients Profile was not updated.');
                }
            }
        }else{
			$_ALL_POST['send_service_mail']=1;
			$_ALL_POST['domain_a']   = array('0'=>'');
		}

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            //$client_id=$client_id
            $variables['hid'] = $user_id;
            include ( DIR_FS_NC .'/clients-su-list.php');
        }
        else {
            
            // Check if the Edit form had been submitted and if error is generated.
            if ( $messages->getErrorMessageCount() > 0 ) {
                // Preserve the Contact Numbers.
                $count = count($_ALL_POST['phone_id']);
                for ( $i=0; $i<$count; $i++ ) {
//                    if ( isset($_ALL_POST['p_number'][$i]) && !empty($_ALL_POST['p_number'][$i]) ) {
                        $_ALL_POST['phone'][] = array(  'id'            => isset($_ALL_POST['phone_id'][$i])? $_ALL_POST['phone_id'][$i]:'',
                                                        'p_is_verified' => isset($_ALL_POST['p_is_verified'][$i])? $_ALL_POST['p_is_verified'][$i]:'',
                                                        'p_type'        => isset($_ALL_POST['p_type'][$i])? $_ALL_POST['p_type'][$i]:'',
                                                        'cc'            => isset($_ALL_POST['cc'][$i])? $_ALL_POST['cc'][$i]:'',
                                                        'ac'            => isset($_ALL_POST['ac'][$i])? $_ALL_POST['ac'][$i]:'',
                                                        'p_number'      => isset($_ALL_POST['p_number'][$i])? $_ALL_POST['p_number'][$i]:'',
                                                        'p_is_preferred'=> isset($_ALL_POST['p_is_preferred'][$i])? $_ALL_POST['p_is_preferred'][$i]:'',
                                                    );
//                    }
                }
                
                // Preserve the Addresses.
                $count = count($_ALL_POST['city']);
                for ( $i=0; $i<$count; $i++ ) {
//                  if ( isset($_ALL_POST['city'][$i]) && !empty($_ALL_POST['city'][$i]) ) {
                        $_ALL_POST['address_list'][] = array(   
						'id'            => isset($_ALL_POST['address_id'][$i])? $_ALL_POST['address_id'][$i]:'',
						//'company_name'   => isset($_ALL_POST['company_name'][$i])? $_ALL_POST['company_name'][$i]:'',
						'is_verified'   => isset($_ALL_POST['is_verified'][$i])? $_ALL_POST['is_verified'][$i]:'',
						'is_preferred'  => isset($_ALL_POST['is_preferred'][$i])? $_ALL_POST['is_preferred'][$i]:'',
						'address_type'  => isset($_ALL_POST['address_type'][$i])? $_ALL_POST['address_type'][$i]:'',
						'address'       => isset($_ALL_POST['address'][$i])? $_ALL_POST['address'][$i]:'',
						'city'          => isset($_ALL_POST['city'][$i])? $_ALL_POST['city'][$i]:'',
						'state'         => isset($_ALL_POST['state'][$i])? $_ALL_POST['state'][$i]:'',
						'zipcode'       => isset($_ALL_POST['zipcode'][$i])? $_ALL_POST['zipcode'][$i]:'',
						'country'       => isset($_ALL_POST['country'][$i])? $_ALL_POST['country'][$i]:'',
                        );
//                  }
                }
				
				$count = count($_ALL_POST['domain_id']);
                for ( $i=0; $i<$count; $i++ ) {
					$_ALL_POST['domain_a'][] = array(
											'cda_id'     => isset($_ALL_POST['cda_id'][$i])? $_ALL_POST['cda_id'][$i]:'',
											'domain_id' => isset($_ALL_POST['domain_id'][$i])? $_ALL_POST['domain_id'][$i]:'',
											'clients_id' => isset($_ALL_POST['clients_id'][$i])? $_ALL_POST['clients_id'][$i]:'',
											'excluded_emails'     => isset($_ALL_POST['excluded_emails'][$i])? $_ALL_POST['excluded_emails'][$i]:''
										); 
                }
				
				
            }
            else {
                // No error was generated, read the Client data from the Database.
                $condition_query= " WHERE user_id = '". $user_id ."' ";
                $_ALL_POST      = NULL;
                if ( Clients::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                    if ( empty($_ALL_POST['country']) ) {
                            $_ALL_POST['country'] = '91';
                    }
                    $_ALL_POST['password']='';
                    
                    if(!empty($_ALL_POST['service_id'])){
                        $_ALL_POST['service_id'] = trim($_ALL_POST['service_id'],",");
                        $_ALL_POST['service_id']    = explode(",", $_ALL_POST['service_id']);
                    }
                    if(!empty($_ALL_POST['authorization_id'])){
                        $_ALL_POST['authorization_id'] = trim($_ALL_POST['authorization_id'],",");
                        $_ALL_POST['authorization_id'] = explode(",", $_ALL_POST['authorization_id']);
                    }
					if(!empty($_ALL_POST['member_type'])){
                        $_ALL_POST['member_type'] = trim($_ALL_POST['member_type'],",");
                        $_ALL_POST['member_type'] = explode(",", $_ALL_POST['member_type']);
                    }
					if(!empty($_ALL_POST['sms_accounts'])){
                        $_ALL_POST['sms_accounts'] = trim($_ALL_POST['sms_accounts'],",");
                        $_ALL_POST['sms_accounts'] = explode(",", $_ALL_POST['sms_accounts']);
                    }
                    // Check the Access Level.
                    if ( $_ALL_POST['access_level'] < $access_level ) {
                        // Read the Contact Numbers.
                        $phone->setPhoneOf(TABLE_CLIENTS, $user_id);
                        $_ALL_POST['phone'] = $phone->get($db);
						
                        $domain_acc->setDomainAccess($user_id);
                        $_ALL_POST['domain_a'] = $domain_acc->get($db);
						if(empty($_ALL_POST['domain_a'])){
						$_ALL_POST['domain_a'] = array(0=>'');
						}
                        // Read the Addresses.
                        $region->setAddressOf(TABLE_CLIENTS, $user_id);
                        $_ALL_POST['address_list'] = $region->get();
                        
                        // Read the Reminders.
                        $reminder->setReminderOf(TABLE_CLIENTS, $user_id);
                        $_ALL_POST['reminder_list1'] = $reminder->get($db);
						if(!empty($_ALL_POST['reminder_list1'])){
							foreach($_ALL_POST['reminder_list1'] as $key1=>$val1){
								$_ALL_POST['remind_id'][]= $val1['id'];
								$_ALL_POST['subject'][]= $val1['subject'];
								$_ALL_POST['description'][]= $val1['description'];
								$data['do_reminder'][]=$val1['do_reminder'];
								/* $temp1 = explode(" ",$val1['do_reminder']);
								$temp  = explode("-",$temp1[0]);
								$data['do_reminder'][]= $temp[2]."/".$temp[1]."/".$temp[0]; */
							}
						}
                    }
                    
                }
            }
 
            if ( !empty($_ALL_POST['user_id']) ) {

                // Check the Access Level.
                if ( $_ALL_POST['access_level'] < $access_level ) {

                    $phone_types    = $phone->getTypeList();
                    $address_types  = $region->getTypeList();
                    $lst_country    = $region->getCountryList();
                    $lst_state      = $region->getStateList();
                    $lst_city       = $region->getCityList();
                    $region  =	$phone =	$reminder =	null;
                    
                    $condition_query = " WHERE user_id = '". $client_id ."' ";
                    if ( Clients::getList($db, $_ALL_POST['main_client_details'], 'service_id', $condition_query) > 0 ) {
                        $_ALL_POST['main_client_details'] = $_ALL_POST['main_client_details'][0];
                        $_ALL_POST['main_client_details']['service_id'] = trim($_ALL_POST['main_client_details']['service_id'],",");
                        $my_services_list = explode(",",$_ALL_POST['main_client_details']['service_id']);
                    }
                    
                    
                    // Change the Roles from String to Array.
                    //$_ALL_POST['roles'] = explode(',', $_ALL_POST['roles']);
                    $_ALL_POST['reminder_list']=array(0=>'',1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'');
              
                    // These parameters will be used when returning the control back to the List page.
                    foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                        $hidden[]   = array('name'=> $key, 'value' => $value);
                    }
                    $hidden[] = array('name'=> 'perform','value' => 'edit');
                    $hidden[] = array('name'=> 'act', 'value' => 'save');
                    $hidden[] = array('name'=> 'user_id', 'value' => $user_id);
                    $hidden[] = array('name'=> 'client_id', 'value' => $client_id);
                    //$hidden[] = array('name'=> 'reminder_count', 'value' => count($_ALL_POST['reminder_list']));
                    
                    $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
                    $page["var"][] = array('variable' => 'phone_types', 'value' => 'phone_types');
                    $page["var"][] = array('variable' => 'address_types', 'value' => 'address_types');
                    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
					 $page["var"][] = array('variable' => 'data', 'value' => 'data');
                    $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                    //$page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
                    //$page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
                    $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
                    $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
                    $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
                    $page["var"][] = array('variable' => 'grade_list', 'value' => 'grade_list');
					
                    $page["var"][] = array('variable' => 'domainlist1', 'value' => 'domainlist1');
                    $page["var"][] = array('variable' => 'services_list', 'value' => 'services_list');
					$page["var"][] = array('variable' => 'memberTypeList', 'value' => 'memberTypeList');
					$page["var"][] = array('variable' => 'authorization_list', 'value' => 'authorization_list');
                    $page["var"][] = array('variable' => 'my_services_list', 'value' => 'my_services_list');
                    $page["var"][] = array('variable' => 'lst_sms_acc', 'value' => 'lst_sms_acc');
                    $page["var"][] = array('variable' => 'allowipArr', 'value' => 'allowipArr');
                    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'clients-su-edit.html');
                }
                else {
                    $messages->setErrorMessage("You do not have the Right to Edit the Client with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Selected Client was not found.");
            }
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>