<?php
    if ( $perm->has('nc_csv_upload_tran') ) {
        $_ALL_POST  = NULL;
        $data       = NULL;
       
        $CSV = CSV::getRestrictions();
        // The table in which the data will be imported.
        $table = 'temp_user';
        include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php' );
       
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            $extra = array( 'file'      => $_FILES,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );

            if ( CSV::validateUpload($data, $extra) ) {
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $empty      = 0;
                $index      = array();
                $handle     = fopen($_FILES['file_csv']['tmp_name'],"rb");
                
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
                    if ( $row == 1 ) {
                        $num = count($data);
                        for ($i=0; $i < $num; $i++) {
                            $case = trim($data[$i]);
                            switch ($case) {
                                case ('TRANDT'): {
                                    $index['TRANDT'] = $i;
                                    break;
                                }
                                case ('CHQNO'): {
                                    $index['CHQNO'] = $i;
                                    break;
                                }
                                case ('REMARK'): {
                                    $index['REMARK'] = $i;
                                    break;
                                }
                                case ('CRDR'): {
                                    $index['CRDR'] = $i;
                                    break;
                                }
                                case ('TRAMT'): {
                                    $index['TRAMT'] = $i;
                                    break;
                                }
                                case ('COMPANYID'): {
                                    $index['COMPANYID'] = $i;
                                    break;
                                }
                                case ('FINANCIALYR'): {
                                    $index['FINANCIALYR'] = $i;
                                    break;
                                }
                                case ('VENDORBANK'): {
                                    $index['VENDORBANK'] = $i;
                                    break;
                                }
								case ('VENDOR'): {
                                    $index['VENDOR'] = $i;
                                    break;
                                }
								case ('CLIENT'): {
                                    $index['CLIENT'] = $i;
                                    break;
                                }
								case ('TEAM'): {
                                    $index['TEAM'] = $i;
                                    break;
                                }
								case ('FBANKID'): {
                                    $index['FBANKID'] = $i;//from bank
                                    break;
                                }
								case ('TBANKID'): {
                                    $index['TBANKID'] = $i;//to bank
                                    break;
                                }
								case ('DOUBLE'): {
                                    $index['DOUBLE'] = $i;
                                    break;
                                }
                                case ('ACCHEAD'): {
                                    $index['ACCHEAD'] = $i;
                                    break;
                                }
								case ('MODE'): {
                                    $index['MODE'] = $i;
                                    break;
                                }
								case ('MODE'): {
                                    $index['MODE'] = $i;
                                    break;
                                }
								case ('MODE'): {
                                    $index['MODE'] = $i;
                                    break;
                                }
								case ('MODE'): {
                                    $index['MODE'] = $i;
                                    break;
                                }
								case ('OffExpIssueBy'): {
                                    $index['OffExpIssueBy'] = $i;
                                    break;
                                }
								case ('OffExpIssueById'): {
                                    $index['OffExpIssueById'] = $i;
                                    break;
                                }
								case ('OffExpIssueTo'): {
                                    $index['OffExpIssueTo'] = $i;
                                    break;
                                }
								case ('OffExpIssueToId'): {
                                    $index['OffExpIssueToId'] = $i;
                                    break;
                                }
								case ('IsBillRcv'): {
                                    $index['IsBillRcv'] = $i;
                                    break;
                                }
                            }
                        }
                    }
                    else {
                 
                          $index= array (
                            "TRANDT"=> "0",
                            "CHQNO"    =>"1",
                            "REMARK"    =>"2",
                            "CRDR" =>"3",
                            "TRAMT" =>"4",
                            "COMPANYID" =>"5",
                            "FINANCIALYR" =>"6",
                            "VENDORBANK" =>"7",
							"VENDOR" =>"8",
                            "CLIENT" =>"9",
                            "TEAM" =>"10",
                            "FBANKID" =>"11",
                            "TBANKID" =>"12",
                            "DOUBLE" =>"13",
                            "ACCHEAD" =>"14",
                            "MODE" =>"15",
                            "OffExpIssueBy" =>"16",
                            "OffExpIssueById" =>"17",
							"OffExpIssueTo" =>"18",
                            "OffExpIssueToId" =>"19",                           
                            "IsBillRcv" =>"20",
                           );
                        
                        $data['debit_pay_amt'] ='';
                        $data['debit_pay_bank_id'] = '' ;
                        $data['credit_pay_amt'] = '' ;
                        $data['credit_pay_bank_id'] = '' ;                        
                        $data['mode_id'] = 0;
                        $data['pay_received_amt'] ='';
						$is_bill_receive=0;
						$is_bill_receive_str = strtolower(trim($data[$index['IsBillRcv']]));
						if(!empty($is_bill_receive_str)){
							if($is_bill_receive_str=='yes'){
								$is_bill_receive=1;
							}
						}
						
                        if ( isset($index['REMARK']) && $data[$index['REMARK']] != '' ) {
                        
                            $data['number'] = Paymenttransaction::getNewNumber($db);
                            if(!empty($data[$index['CRDR']])){
                                if(trim($data[$index['CRDR']])=='CR'){
                                    $data['transaction_type'] = Paymenttransaction::PAYMENTIN ;
                                    $data['debit_pay_amt'] = '' ;
                                    $data['debit_pay_bank_id'] = '' ;
                                    $data['credit_pay_bank_id'] = trim($data[$index['TBANKID']]);
                                }elseif(trim($data[$index['CRDR']])=='DR'){
                                    $data['transaction_type'] = Paymenttransaction::PAYMENTOUT ;
                                    $data['credit_pay_amt'] = '' ;
                                    $data['credit_pay_bank_id'] = '' ; 
                                    $data['debit_pay_bank_id'] = trim($data[$index['FBANKID']]);                                          
                                }elseif(trim($data[$index['CRDR']])=='CRDR'){
									$data['transaction_type'] = Paymenttransaction::INTERNAL ;
									$data['credit_pay_bank_id'] = trim($data[$index['TBANKID']]);
                                    $data['debit_pay_bank_id'] = trim($data[$index['FBANKID']]);
								
								}
                            }
                            if(!empty($data[$index['TRAMT']])){
                                if($data['transaction_type'] == Paymenttransaction::PAYMENTIN){
                                    $data['credit_pay_amt'] = (double) $data[$index['TRAMT']] ;
                                    $data['pay_received_amt'] = $data['credit_pay_amt'];                                   
                                    
                                }elseif($data['transaction_type'] == Paymenttransaction::PAYMENTOUT){
                                    $data['debit_pay_amt'] = (double) $data[$index['TRAMT']] ;    
                                    $data['pay_received_amt'] = $data['debit_pay_amt'];                                                                  
                                }elseif($data['transaction_type'] == Paymenttransaction::INTERNAL){
                                    $data['debit_pay_amt'] = (double) $data[$index['TRAMT']] ;    
                                    $data['credit_pay_amt'] = $data['debit_pay_amt'];
                                    $data['pay_received_amt'] = $data['debit_pay_amt'];
                                }                              
                            }
							$data['exchange_rate']  =1;
							$data['pay_received_amt_inr']=  $data['pay_received_amt'] ;
                            if(!empty($data[$index['CHQNO']])){                                 
                                $data['pay_cheque_no'] = $data[$index['CHQNO']] ;
                            }
                            $data['mode_id'] = $data[$index['MODE']];
                            $data['account_head_id'] = $data[$index['ACCHEAD']];
                            $data['is_double'] = $data[$index['DOUBLE']];
							
                            $do_transaction='';
                            if(!empty($data[$index['TRANDT']])){   
                                $temp=array();
                                $pos1 = strpos($data[$index['TRANDT']], '/');
                                if($pos1 >0){
                                    $temp = explode('/', $data[$index['TRANDT']]);
                                }
                                $pos2 = strpos($data[$index['TRANDT']], '-');
                                if($pos2 >0){
                                    $temp = explode('-', $data[$index['TRANDT']]);
                                }
                                $do_transaction = $temp[2] .'-'. $temp[1] .'-'. $temp[0] .' 00:00:00';
                            }
							
						$query	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION
							." SET ".  TABLE_PAYMENT_TRANSACTION .".number = '".$data['number'] ."'"      
							.",". TABLE_PAYMENT_TRANSACTION .".date = '". date('Y-m-d h:i:s') ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".do_transaction = '".$do_transaction."'"
							.",". TABLE_PAYMENT_TRANSACTION .".user_id = '". $my['uid'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".transaction_type = '".$data['transaction_type'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".remarks = '". processUserData($data[$index['REMARK']] )."'"                            .",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt = '". $data['pay_received_amt']."'"                            .",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt  = '". $data['pay_received_amt'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".exchange_rate  = '". $data['exchange_rate'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_inr   = '". $data['pay_received_amt_inr'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".pay_received_amt_rcpt_inr='". $data['pay_received_amt_inr'] ."'"
							.",". TABLE_PAYMENT_TRANSACTION .".mode_id = '". $data['mode_id']."'"                                
							.",". TABLE_PAYMENT_TRANSACTION .".is_double = '". $data['is_double']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".account_head_id = '". $data['account_head_id']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".party_id = '". trim($data[$index['VENDOR']])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".vendor_bank_id = '". trim($data[$index['VENDORBANK']])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".client_id = '". trim($data[$index['CLIENT']])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".executive_id = '". trim($data[$index['TEAM']])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".financial_yr = '". trim($data[$index['FINANCIALYR']])."'"                            .",". TABLE_PAYMENT_TRANSACTION .".company_id = '". trim($data[$index['COMPANYID']])."'"                            .",". TABLE_PAYMENT_TRANSACTION .".pay_cheque_no = '". $data['pay_cheque_no']."'"                            .",". TABLE_PAYMENT_TRANSACTION .".debit_pay_amt = '". $data['debit_pay_amt']."'"                            .",". TABLE_PAYMENT_TRANSACTION .".debit_pay_bank_id = '". $data['debit_pay_bank_id']."'"                            .",". TABLE_PAYMENT_TRANSACTION .".credit_pay_amt = '". $data['credit_pay_amt']."'"                            .",". TABLE_PAYMENT_TRANSACTION .".credit_pay_bank_id = '". $data['credit_pay_bank_id']."'"                            .",". TABLE_PAYMENT_TRANSACTION .".by_csv = '1'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_abbr = 'INR'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_id = '1'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_name = 'Rupees'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_symbol = 'Rs.'"
							.",". TABLE_PAYMENT_TRANSACTION .".currency_country = 'India'"
							.",". TABLE_PAYMENT_TRANSACTION .".off_exp_issue_by_name='". trim($data[$index['OffExpIssueBy']])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".off_exp_issue_by = '". trim($data[$index['OffExpIssueById']])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".off_exp_issue_to_name = '". trim($data[$index['OffExpIssueTo']])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".off_exp_issue_to = '". trim($data[$index['OffExpIssueToId']])."'"
							.",". TABLE_PAYMENT_TRANSACTION .".is_bill_receive = '". $is_bill_receive."'"
							.",". TABLE_PAYMENT_TRANSACTION .".ip = '".$_SERVER['REMOTE_ADDR']."'"
							.",". TABLE_PAYMENT_TRANSACTION .".status = '".Paymenttransaction::PENDING ."'";
			
                            //Insert product EOF
                            $db->query($query);  
                            $added += 1;

                        }else {
                            $empty += 1;
                        }
                    }                    
                }
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been added.');
                if ($duplicate)
                    $messages->setOkMessage($duplicate .' out of '. $row .' records were duplicate and neglected.');
                if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
        }
        
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$condition ='';
            include ( DIR_FS_NC .'/settings-csv.php');
        }
        else {
            $table_list = NULL;
            CSV::getTables($db, $table_list);
            //print_r($table_list);
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'upload_tran');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            $page["var"][] = array('variable' => 'table_list', 'value' => 'table_list');
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'settings-csv-upload-tran.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>