<?php

	//$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
    //$searchStr = 0;
    if( $sString != "" ){
        $searchStr = 1;
        // Set the field on which to make the search.
		if( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
        //$where_added = true;
        $sub_query='';        
        if( $search_table == "Any" ){
            
            if ( $where_added ){
                $condition_query .= "OR ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ) 
            {
              
                if ( $table != "Any" ) 
                { 
                    
                    if ($table == TABLE_BILL_ORDERS) 
                    {
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ){
                            
                            $sub_query .= $table .".".clearSearchString($field,"-")." LIKE '%".trim($sString)."%' OR " ;
                           
                            /*
                            if($field !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                        }
                        
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                            
                    }elseif($table == TABLE_USER){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            /*
                            if($field !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                            
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_CLIENTS){
                    
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                             $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                             
                            /*
                            if($field !='number'){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                            //$sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_BILL_ORD_P){
                    
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                             $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                             
                            /*
                            if($field !='number'){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }*/
                            //$sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }
                    
                }else{
                
                
                
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= "AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                /*
                if($sType !='number' && $sType !='or_no' && $sType !='id' ){
                    $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                }else{
                     $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ."='".trim($sString) ."' )" ;
                }*/
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
    
    
    // BO: Status data.
    $sStatusStr = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ( $chk_status == 'AND' || $chk_status == 'OR') &&  isset($sStatus)){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ;
             
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".status IN ('". $sStatusStr ."') ";        
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;        
    }    
    // EO: Status data.
	// BO: Referral Status data BOF
    $rStatusStr = '';
    $chk_rstatus = isset($_POST["chk_rstatus"])   ? $_POST["chk_rstatus"]  : (isset($_GET["chk_rstatus"])   ? $_GET["chk_rstatus"]   :'');
    $rStatus    = isset($_POST["rStatus"])      ? $_POST["rStatus"]     : (isset($_GET["rStatus"])      ? $_GET["rStatus"]      :'');
    if ( ( $chk_rstatus == 'AND' || $chk_rstatus == 'OR') &&  isset($rStatus)){
         $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_rstatus;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($rStatus)){
             $rStatusStr = implode("','", $rStatus) ;
             $rStatusStr1 = implode(",", $rStatus) ;
             
        }else{
            $rStatusStr = str_replace(',',"','",$rStatus);
            $rStatusStr1 = $rStatus ;
            $rStatus = explode(",",$rStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".ord_referred_status IN ('". $rStatusStr ."') ";        
        $condition_url   .= "&chk_rstatus=$chk_rstatus&rStatus=$rStatusStr1";
        $_SEARCH["chk_rstatus"]  = $chk_rstatus;
        $_SEARCH["rStatus"]     = $rStatus;        
    }    
    //EO: Referral Status data EOF
	//Client serach bof
	$search_client = isset($_POST["search_client"])   ? $_POST["search_client"]  : (isset($_GET["search_client"])   ? $_GET["search_client"]   :'');
	$search_client_details = isset($_POST["search_client_details"]) ? $_POST["search_client_details"]  : (isset($_GET["search_client_details"])   ? $_GET["search_client_details"]   :'');
	
	if(!empty($search_client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_BILL_ORDERS .".client ='". $search_client ."' ";        
        $condition_url          .= "&search_client_details=$search_client_details&search_client=$search_client";
        $_SEARCH["search_client_details"]  = $search_client_details;
        $_SEARCH["search_client"]     = $search_client;  
	}
	//Client serach eof
	
	
	//Order Referred by bof
	$ord_referred_by = isset($_POST["ord_referred_by"])   ? $_POST["ord_referred_by"]  : (isset($_GET["ord_referred_by"])   ? $_GET["ord_referred_by"]   :'');
	$ord_referred_by_name = isset($_POST["ord_referred_by_name"]) ? $_POST["ord_referred_by_name"]  : (isset($_GET["ord_referred_by_name"])   ? $_GET["ord_referred_by_name"]   :'');
	
	if(!empty($ord_referred_by)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_BILL_ORDERS .".ord_referred_by ='". $ord_referred_by ."' ";        
        $condition_url          .= "&ord_referred_by_name=$ord_referred_by_name&ord_referred_by=$ord_referred_by";
        $_SEARCH["ord_referred_by_name"]  = $ord_referred_by_name;
        $_SEARCH["ord_referred_by"]     = $ord_referred_by;  
	}
	//Order Referred by eof
	
	// BO: Followup Status data.
    $fStatusStr = '';
    $chk_fstatus = isset($_POST["chk_fstatus"])   ? $_POST["chk_fstatus"]  : (isset($_GET["chk_fstatus"])   ? $_GET["chk_fstatus"]   :'');
    $fStatus    = isset($_POST["fStatus"])      ? $_POST["fStatus"]     : (isset($_GET["fStatus"])      ? $_GET["fStatus"]      :'');
    if ( ( $chk_fstatus == 'AND' || $chk_fstatus == 'OR') &&  isset($fStatus)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($fStatus)){
             $fStatusStr = implode("','", $fStatus) ;
             $fStatusStr1 = implode(",", $fStatus) ;
             
        }else{
            $fStatusStr = str_replace(',',"','",$fStatus);
            $fStatusStr1 = $fStatus ;
            $fStatus = explode(",",$fStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORDERS .".followup_status IN ('". $fStatusStr ."') ";        
        $condition_url          .= "&chk_fstatus=$chk_fstatus&fStatus=$fStatusStr1";
        $_SEARCH["chk_fstatus"]  = $chk_fstatus;
        $_SEARCH["fStatus"]     = $fStatus;
    }    
    // EO: Followup Status data.
	
	
	// EO: Proforma created or not bof  
    $chk_pinv = isset($_POST["chk_pinv"])   ? $_POST["chk_pinv"]  : (isset($_GET["chk_pinv"])   ? $_GET["chk_pinv"]   :'');
    $pInv    = isset($_POST["pInv"])      ? $_POST["pInv"]     : (isset($_GET["pInv"])      ? $_GET["pInv"]      :'');
    if ( ( $chk_pinv == 'AND' || $chk_pinv == 'OR') &&  isset($pInv)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_pinv;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".is_invoiceprofm_create ='".$pInv."' ";        
		
       
        $condition_url          .= "&chk_pinv=$chk_pinv&pInv=$pInv";
        $_SEARCH["chk_pinv"]  = $chk_pinv;
        $_SEARCH["pInv"]     = $pInv;        
    }    
    // EO: Proforma created or not eof.
	
	// EO: Invoice created or not bof  
    $chk_inv = isset($_POST["chk_inv"])   ? $_POST["chk_inv"]  : (isset($_GET["chk_inv"])   ? $_GET["chk_inv"]   :'');
    $sInv    = isset($_POST["sInv"])      ? $_POST["sInv"]     : (isset($_GET["sInv"])      ? $_GET["sInv"]      :'');
    if ( ( $chk_inv == 'AND' || $chk_inv == 'OR') &&  isset($sInv)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_inv;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".is_invoice_create ='".$sInv."' ";        
		
       
        $condition_url          .= "&chk_inv=$chk_inv&sInv=$sInv";
        $_SEARCH["chk_inv"]  = $chk_inv;
        $_SEARCH["sInv"]     = $sInv;        
    }    
    // EO: Invoice created or not eof.
	
	//BO: Special Ord or not bof  
	$chk_special = isset($_POST["chk_special"]) ? $_POST["chk_special"]  : (isset($_GET["chk_special"])   ? $_GET["chk_special"]   :'');
	$is_special = isset($_POST["is_special"]) ? $_POST["is_special"]:(isset($_GET["is_special"]) ? 
	$_GET["is_special"]      :'');
	if ( ( $chk_special == 'AND' || $chk_special == 'OR') &&  isset($is_special)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_special;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".special ='".$is_special."' ";  
	   
		$condition_url          .= "&chk_special=$chk_special&is_special=$is_special";
		$_SEARCH["is_special"]  = $is_special;
		$_SEARCH["chk_special"]     = $chk_special;        
	}
	//BO: Special Ord or not bof  
	
	$chk_hd_ord = isset($_POST["chk_hd_ord"]) ? $_POST["chk_hd_ord"]  : (isset($_GET["chk_hd_ord"])   ? $_GET["chk_hd_ord"]   :'');
	$is_hardware_ord = isset($_POST["is_hardware_ord"]) ? $_POST["is_hardware_ord"]:(isset($_GET["is_hardware_ord"]) ? 
	$_GET["is_hardware_ord"]      :'');
	if ( ( $chk_hd_ord == 'AND' || $chk_hd_ord == 'OR') &&  isset($is_hardware_ord)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_hd_ord;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		//Chk Proforma
		$condition_query .= " ". TABLE_BILL_ORDERS .".is_hardware_ord ='".$is_hardware_ord."' ";  
	   
		$condition_url          .= "&chk_hd_ord=$chk_hd_ord&is_hardware_ord=$is_hardware_ord";
		$_SEARCH["chk_hd_ord"]  = $chk_hd_ord;
		$_SEARCH["is_hardware_ord"]     = $is_hardware_ord;        
	} 
	
	// BO: services
    $s_idStr = '';
    $chk_s_id = isset($_POST["chk_s_id"])   ? $_POST["chk_s_id"]  : (isset($_GET["chk_s_id"])   ? $_GET["chk_s_id"]   :'');
    $s_id    = isset($_POST["s_id"])      ? $_POST["s_id"]     : (isset($_GET["s_id"])      ? $_GET["s_id"]      :'');
    if ( ( $chk_s_id == 'AND' || $chk_s_id == 'OR') &&  isset($s_id)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_s_id;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($s_id)){
             $s_idStr = implode("','", $s_id) ;
             $s_idStr1 = implode(",", $s_id) ;
             
        }else{
            $s_idStr = str_replace(',',"','",$s_id);
            $s_idStr1 = $s_id ;
            $s_id = explode(",",$s_id) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORD_P.".s_id IN ('". $s_idStr ."') ";        
        $condition_url          .= "&chk_s_id=$chk_s_id&s_id=$s_idStr1";
        $_SEARCH["chk_s_id"]  = $chk_s_id;
        $_SEARCH["s_id"]     = $s_id;
    }      
    // EO: services
	// BO: Internal Account head
    $i_accStr = '';
    $chk_iaccount_head_id = isset($_POST["chk_iaccount_head_id"])   ? $_POST["chk_iaccount_head_id"]  : 
	(isset($_GET["chk_iaccount_head_id"])   ? $_GET["chk_iaccount_head_id"]   :'');
    $iaccount_head_id = isset($_POST["iaccount_head_id"])      ? $_POST["iaccount_head_id"]     : (isset($_GET["iaccount_head_id"])      ? $_GET["iaccount_head_id"]      :'');
    if ( ( $chk_iaccount_head_id == 'AND' || $chk_iaccount_head_id == 'OR') &&  isset($iaccount_head_id)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_iaccount_head_id;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        if(is_array($iaccount_head_id)){
            // $i_accStr = implode("','", $iaccount_head_id) ;
             $i_accStr = implode(",", $iaccount_head_id) ;
             $i_accStr1 = implode(",", $iaccount_head_id) ;
             
        }else{
           // $i_accStr = str_replace(',',"','",$iaccount_head_id);
            $i_accStr = str_replace(',',",",$iaccount_head_id);
            $i_accStr1 = $iaccount_head_id ;
            $iaccount_head_id = explode(",",$iaccount_head_id) ;
            
        }
        
        $condition_query .= " ". TABLE_BILL_ORDERS.".iaccount_head_id IN (".$i_accStr.") ";        
        $condition_url          .= "&chk_iaccount_head_id=$chk_iaccount_head_id&iaccount_head_id=$i_accStr1";
        $_SEARCH["chk_iaccount_head_id"]  = $chk_iaccount_head_id;
        $_SEARCH["iaccount_head_id"]     = $iaccount_head_id;
    }      
    // EO: Internal Account head
	
	
	
	//BO: Tax Search
	$chk_tax = isset($_POST["chk_tax"]) ? $_POST["chk_tax"]  : (isset($_GET["chk_tax"])   ? $_GET["chk_tax"]   :'');
	$tax_ids = isset($_POST["tax_ids"]) ? $_POST["tax_ids"]:(isset($_GET["tax_ids"]) ? 
	$_GET["tax_ids"]      :'');
	if ( ( $chk_tax == 'AND' || $chk_tax == 'OR') &&  isset($tax_ids)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_tax;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		if($tax_ids=='-1'){
			$condition_query .= " ( ". TABLE_BILL_ORDERS .".tax_ids = '%,0,%' OR  ".TABLE_BILL_ORDERS .".tax_ids='' )" ;  
		}else{
			$condition_query .= " ". TABLE_BILL_ORDERS .".tax_ids LIKE '%,".$tax_ids.",%' ";  
		}
		$condition_url          .= "&chk_tax=$chk_tax&tax_ids=$tax_ids";
		$_SEARCH["chk_tax"]  = $chk_tax;
		$_SEARCH["tax_ids"]     = $tax_ids;        
	} 
	
	
	$chk_oof = isset($_POST["chk_oof"]) ? $_POST["chk_oof"]  : (isset($_GET["chk_oof"])   ? $_GET["chk_oof"]   :'');
	$order_of = isset($_POST["order_of"]) ? $_POST["order_of"]:(isset($_GET["order_of"]) ? $_GET["order_of"]   :'');
	if( ( $chk_oof == 'AND' || $chk_oof == 'OR') &&  isset($order_of)){
		$searchStr = 1;
		if ( $where_added ){
			$condition_query .= " ". $chk_oof;
		} else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		 
		$condition_query .= " ". TABLE_BILL_ORDERS .".order_of LIKE '%,".$order_of.",%' ";  
		 
		$condition_url    .= "&chk_oof=$chk_oof&order_of=$order_of";
		$_SEARCH["chk_oof"]  = $chk_oof;
		$_SEARCH["order_of"]     = $order_of;        
	} 
	
	//BO: Special Ord or not bof  
	$chk_inprod = isset($_POST["chk_inprod"]) ? $_POST["chk_inprod"]  : (isset($_GET["chk_inprod"])   ? $_GET["chk_inprod"]   :'');
	$inproduction_status = isset($_POST["inproduction_status"]) ? $_POST["inproduction_status"]:(isset($_GET["inproduction_status"]) ? 
	$_GET["inproduction_status"]      :'');
	if ( ( $chk_inprod == 'AND' || $chk_inprod == 'OR') &&  isset($inproduction_status)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_inprod;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		//Chk Proforma 
		$condition_query .= " ". TABLE_BILL_ORDERS .".inproduction_status ='".$inproduction_status."' ";  
	   
		$condition_url          .= "&chk_inprod=$chk_inprod&inproduction_status=$inproduction_status";
		$_SEARCH["inproduction_status"]  = $inproduction_status;
		$_SEARCH["chk_inprod"]     = $chk_inprod;        
	}
	//BO: Special Ord or not bof 
	$chk_sapproval = isset($_POST["chk_sapproval"]) ? $_POST["chk_sapproval"]  : (isset($_GET["chk_sapproval"])   ? $_GET["chk_sapproval"]   :'');
	$sent_approval = isset($_POST["sent_approval"]) ? $_POST["sent_approval"]:(isset($_GET["sent_approval"]) ? 
	$_GET["sent_approval"]      :'');
	if ( ( $chk_sapproval == 'AND' || $chk_sapproval == 'OR') &&  isset($sent_approval)){
		$searchStr = 1;
		if ( $where_added ) {
			$condition_query .= " ". $chk_sapproval;
		}
		else {
			$condition_query.= ' WHERE ';
			$where_added    = true;
		}
		//Chk Proforma 
		$condition_query .= " ". TABLE_BILL_ORDERS .".sent_for_approval ='".$sent_approval."' ";  
	   
		$condition_url          .= "&chk_sapproval=$chk_sapproval&sent_approval=$sent_approval";
		$_SEARCH["sent_approval"]  = $sent_approval;
		$_SEARCH["chk_sapproval"]     = $chk_sapproval;        
	}
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
 
    // BO: From Date                             
    if ( $chk_date_from == 'AND' || $chk_date_from == 'OR') {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa = explode('/', $date_from);
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        $condition_query .= " ". TABLE_BILL_ORDERS .".".$dt_field." >= '". $dfa ."'";
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
         $_SEARCH["dt_field"]       = $dt_field;
    }  
    // EO: From Date
    
    // BO: Upto Date
    if ( $chk_date_to == 'AND' || $chk_date_to == 'OR') {
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        $condition_query .= " ". TABLE_BILL_ORDERS .".".$dt_field." <= '". $dta ."'";
        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
        $_SEARCH["dt_field"]       = $dt_field;
    }
    
    
     
    
    $chk_old_update        = isset($_POST["chk_old_update"])      ? $_POST["chk_old_update"]         : (isset($_GET["chk_old_update"])      ? $_GET["chk_old_update"]      :'');
    if(isset($chk_old_update ) && $chk_old_update!=''){
		 $searchStr = 1;
         if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= " ". TABLE_BILL_ORDERS .".old_updated = '". $chk_old_update ."' AND ".TABLE_BILL_ORDERS.".is_old='1'";
         
        $condition_url          .= "&chk_old_update=$chk_old_update";
        $_SEARCH["chk_old_update"] = $chk_old_update ;
     
    }
	
	
$chk_cron_created = isset($_POST["chk_cron_created"]) ? $_POST["chk_cron_created"]: (isset($_GET["chk_cron_created"])      ? $_GET["chk_cron_created"]      :'');	 
	$is_cron = isset($_POST["is_cron"]) ? $_POST["is_cron"]:(isset($_GET["is_cron"]) ? 
	$_GET["is_cron"]      :'');
    if(isset($chk_cron_created ) && $chk_cron_created!='' && $is_cron!=''){
	 $searchStr = 1;
         if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= " ". TABLE_BILL_ORDERS .".cron_created = '". $is_cron ."' ";
         
        $condition_url .= "&chk_cron_created=$chk_cron_created&is_cron=".$is_cron;
        $_SEARCH["chk_cron_created"] = $chk_cron_created ;
        $_SEARCH["is_cron"] = $is_cron ;
     
    }
    
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
   // include ( DIR_FS_NC .'/bill-order-list.php');
?>