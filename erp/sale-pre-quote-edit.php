<?php
    if ( $perm->has('nc_sl_pq_edit') ) {
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        $pq_id          = isset($_GET["pq_id"]) ? $_GET["pq_id"] : ( isset($_POST["pq_id"]) ? $_POST["pq_id"] : '' );
        $po_id          = isset($_GET["po_id"]) ? $_GET["po_id"] : ( isset($_POST["po_id"]) ? $_POST["po_id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_sl_pq_edit_al') ) {
            $access_level += 1;
        }
        include_once (DIR_FS_INCLUDES .'/user.inc.php');
        // list of executives to assign project manager
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields);
        
        //code to generate $vendorOptionList BOF
        $vendorSql ="SELECT * FROM ".TABLE_VENDOR ;
        $db->query( $vendorSql );
        if( $db->nf() > 0 )
		{
            $vendorList['0'] = "" ;
			while($db->next_record())
			{
                $vendorList[$db->f('id')] = $db->f('f_name')." ".$db->f('l_name') ;
            }
            
        }
        //code to generate $vendorOptionList EOF
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);
        
         // code for particulars bof 
        // Read the Services.
        include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
        include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
        
        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC";
        Services::getList($db, $lst_service, 'ss_id,ss_title,ss_punch_line, tax1_name, tax1_value ', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
                    $ss_id= $key['ss_id'];
                    $ss_title= $key['ss_title'];
                    $ss_punch_line= $key['ss_punch_line'];
                    $tax1_name = $key['tax1_name'];
                    $tax1_value = $key['tax1_value'];
                    $tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
                    
                    $keyNew['ss_id'] = $ss_id ; 
                    $keyNew['ss_title'] = $ss_title ; 
                    $keyNew['ss_punch_line'] = $ss_punch_line ; 
                    $keyNew['tax1_name'] = $tax1_name ; 
                    $keyNew['tax1_value'] = $tax1_value ; 
                    $keyNew['tax1_pvalue'] = $tax1_pvalue ; 
                    $lst_service[$val] = $keyNew;
            }
        }
        // code for particulars eof
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'lst_service'       => $lst_service,
                            'messages'          => &$messages
                        );
            $data['do_c'] = time();
            if ( PreQuote::validateUpdate($data, $extra) ) {
                $query  = " UPDATE ". TABLE_SALE_PRE_QUOTE
                            ." SET ". TABLE_BILL_ORDERS .".number = '".         $data['number'] ."'"
                                .",". TABLE_BILL_ORDERS .".po_id = '".          $data['po_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".access_level = '".   $data['access_level'] ."'"
                                .",". TABLE_BILL_ORDERS .".client = '".         $data['client']['user_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".created_by = '".     $data['created_by'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_title	= '".   $data['order_title'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_domain	= '".   $data['order_domain'] ."'"
                                .",". TABLE_BILL_ORDERS .".currency_id = '".   $data['currency_id'] ."'"
                                .",". TABLE_BILL_ORDERS .".amount = '".        $data['amount'] ."'"
                                .",". TABLE_BILL_ORDERS .".existing_client	= '".   $data['existing_client'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_type	= '".   $data['order_type'] ."'"
                                .",". TABLE_BILL_ORDERS .".order_closed_by	= '".   $data['order_closed_by'] ."'"
                                .",". TABLE_BILL_ORDERS .".team = '".           implode(",", $data['team']) ."'"
                                .",". TABLE_BILL_ORDERS .".old_particulars = '".    $data['old_particulars'] ."'"
                                .",". TABLE_BILL_ORDERS .".details = '".        $data['details'] ."'"
                                .",". TABLE_BILL_ORDERS .".do_c = '".           date('Y-m-d H:i:s', $data['do_c']) ."'"
                                .",". TABLE_BILL_ORDERS .".do_d = '".           date('Y-m-d H:i:s', $data['do_d']) ."'"
                                .",". TABLE_BILL_ORDERS .".project_manager = '".$data['project_manager']."'"
                                .",". TABLE_BILL_ORDERS .".st_date = '".        $data['st_date']."'"
                                .",". TABLE_BILL_ORDERS .".es_ed_date = '".     $data['es_ed_date']."'"
                                .",". TABLE_BILL_ORDERS .".es_hrs = '".         $data['es_hrs']."'"
                                .",". TABLE_BILL_ORDERS .".status = '".         $data['status'] ."'" 
                            ." WHERE id = '". $or_id ."'";
                
                
                
                
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Pre Quote has been updated.");
                    $variables['hid'] = $pq_id;
                    //Particulars edit bof 
                    if(isset($data['p_id'])){
                        foreach ( $data['p_id'] as $key=>$pid ) {
                            
                            if(!empty($data['p_id'][$key])){
                                if(!isset($data['particulars'][$key]) && empty($data['particulars'][$key]) ){
                                   //delete pid
                                   //Get amount from TABLE_SALE_PRE_QUOTE_P and update in 
                                   
                                   
                                   
                                   $query1 = "DELETE FROM ". TABLE_SALE_PRE_QUOTE_P
                                                ." WHERE id = '".$data['p_id'][$key]."'";
                                        
                                    $db->query($query1);
                                
                                }else{
                                   
                                   
                                      $query2  = " UPDATE ". TABLE_SALE_PRE_QUOTE_P
                                    ." SET ". TABLE_SALE_PRE_QUOTE_P .".particulars = '".   processUserData($data['particulars'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".p_amount = '".        $data['p_amount'][$key]  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".ss_title = '".     processUserData($data['ss_title'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".ss_punch_line	= '".  processUserData($data['ss_punch_line'][$key])."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".tax1_name	= '".   processUserData($data['tax1_name'][$key])."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".tax1_value	= '".   processUserData($data['tax1_value'][$key]) ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".tax1_pvalue	= '". processUserData($data['tax1_pvalue'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".d_amount	= '".    processUserData($data['d_amount'][$key]) ."'"                                
                                    .",". TABLE_SALE_PRE_QUOTE_P .".stot_amount = '".   processUserData($data['tot_amount'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".discount_type = '". processUserData($data['discount_type'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".pp_amount = '". processUserData($data['pp_amount'][$key]) ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".vendor = '". processUserData($data['vendor'][$key]) ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".purchase_particulars = '".processUserData($data['purchase_particulars'][$key]) ."'"                            
                                    ." WHERE id = '". $data['p_id'][$key] ."'";
                                     $db->query($query2);
                                }
                              
                            }else{
                                //insert
                                 $query3  = " INSERT INTO ". TABLE_SALE_PRE_QUOTE_P
                                    ." SET ". TABLE_SALE_PRE_QUOTE_P .".particulars = '".   processUserData($data['particulars'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".pq_no = '".        $data['number']  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".p_amount = '".        $data['p_amount'][$key]  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".s_id = '".        $data['s_id'][$key]  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".sub_s_id = '".        $data['sub_s_id'][$key]  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".ss_title = '".     processUserData($data['ss_title'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".ss_punch_line	= '".  processUserData($data['ss_punch_line'][$key])."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".tax1_name	= '".   processUserData($data['tax1_name'][$key])."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".tax1_value	= '".   processUserData($data['tax1_value'][$key]) ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".tax1_pvalue	= '". processUserData($data['tax1_pvalue'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".d_amount	= '".    processUserData($data['d_amount'][$key]) ."'"                                
                                    .",". TABLE_SALE_PRE_QUOTE_P .".stot_amount	= '".    processUserData($data['stot_amount'][$key]) ."'"                                
                                    .",". TABLE_SALE_PRE_QUOTE_P .".tot_amount = '".   processUserData($data['tot_amount'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".discount_type = '". processUserData($data['discount_type'][$key])  ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".pp_amount = '". processUserData($data['pp_amount'][$key]) ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".vendor = '". processUserData($data['vendor'][$key]) ."'"
                                    .",". TABLE_SALE_PRE_QUOTE_P .".purchase_particulars = '".processUserData($data['purchase_particulars'][$key]) ."'" ;
                                 
                                   $db->query($query3);
                            
                            }
                            
                        }
                    }
                    
                    //Particulars edit eof 
                    
                    // Mark the Pre Order as PROCESSED.
                    /*
                    if ( !(Order::setPreOrderStatus($db, $data['po_id'], Order::COMPLETED)) ) {
                        $messages->setErrorMessage("Pre Order was not marked as Processed.");
                    }
                    $po_id = NULL;
                    */
              
                    // Send the notification mails to the concerned persons.
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_or_edit', 'access_level'=>$data['access_level']),
                                                            array('right'=>'nc_bl_or_edit_al', 'access_level'=>($data['access_level']-1) ),
                                                            //array('right'=>'nc_bl_in_add', 'access_level'=>$data['access_level']),
                                                            //array('right'=>'nc_bl_in_add_al', 'access_level'=>($data['access_level']-1) )
                                                        )
                                            );

                    // Organize the data to be sent in email.
                    $data['link']   = DIR_WS_NC .'/bill-order.php?perform=view&or_id='. $variables['hid'];
                    $data['updater']= array('f_name'=> $my['f_name'],
                                            'l_name'=> $my['l_name'],
                                            'number'=> $my['number']);
                                            
                    // Read the Client Manager information.
                    include ( DIR_FS_INCLUDES .'/clients.inc.php');
                    $data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');

                    
                    // Send mail to the Concerned Executives with same access level and higher access levels
                    if($data['mail_exec_ac']){ 
                        foreach ( $users as $user ) {
                            $data['myFname']    = $user['f_name'];
                            $data['myLname']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_MANAGERS', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }
                    // Send Email to the Client.
                      
                    if($data['mail_client']){
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_CLIENT', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                    
                    // Send Email to the Client Manager / creator email .
                    if($data['mail_client_mgr']){ 
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_CLIENT_MANAGER', $data, $email) ) {
                            $to     = '';
                            $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                    }
                                     
                    // Send Email to Team Members.
                    if($data['mail_executive']){    
                        // For Speed notification is sent in one email to all the team members.
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'BILL_OR_UPDATE_TEAM', $data, $email) ) {
                            $to     = '';
                            foreach ( $data['team_members'] as $key=>$executive) {
                                $to[]   = array('name' => $executive['f_name'] .' '. $executive['l_name'], 'email' => $executive['email']);
                            }
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                        }
                        // Send Email to the Creator of the Order.
                    }
                    /*
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'NOT REQUIRED, SO NOT DEFINED', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $data['creator']['f_name'] .' '. $data['creator']['l_name'], 'email' => $data['creator']['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    */
                }
                $data       = NULL;
            }
        }
        else {
            // Read the Order which is to be Updated.
            $fields = TABLE_BILL_ORDERS .'.*'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.status AS c_status';
            
            $condition_query = " WHERE (". TABLE_BILL_ORDERS .".id = '". $or_id ."' "
                                    ." OR ". TABLE_BILL_ORDERS .".number = '". $or_id ."')";

            $condition_query .= " AND ( ";
    
            // If my has created this Order.
            $condition_query .= " (". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // If my is present in the Team (Remove if Team Member cannot edit the Order).
            $condition_query .= " OR ( ( "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] ."$' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP ',". $my['user_id'] .",' OR "
                                            . TABLE_BILL_ORDERS .".team REGEXP '^". $my['user_id'] .",' "
                                        . " ) "
                                        ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            
            // If my is the Client Manager (Remove if Client manager cannot edit the Order)
            $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                                ." AND ". TABLE_SALE_PRE_QUOTE .".access_level < $access_level ) ";
            
            // Check if the User has the Right to Edit Orders created by other Users.
            if ( $perm->has('nc_sl_pq_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_sl_pq_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_SALE_PRE_QUOTE. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_SALE_PRE_QUOTE .".access_level < $access_level_o ) ";
            }
            $condition_query .= " )";
/*
            $condition_query .= " AND ( ". TABLE_BILL_ORDERS .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";

            // Check if the User has the Right to edit Orders created by other Users.
            if ( $perm->has('nc_bl_or_edit_ot') ) {
                $access_level   = $my['access_level'];
                if ( $perm->has('nc_bl_or_edit_ot_al') ) {
                    $access_level += 1;
                }
                $condition_query .= " OR ( ". TABLE_BILL_ORDERS. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_BILL_ORDERS .".access_level < $access_level ) ";
            }
            */
            
            if ( PreQuote::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];

                if ( $_ALL_POST['access_level'] < $access_level ) {
                    // Set up the Client Details field.
                    $_ALL_POST['client_details']= $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
                                                    .' ('. $_ALL_POST['c_number'] .')'
                                                    .' ('. $_ALL_POST['c_email'] .')';
                    
                    
                    //include ( DIR_FS_INCLUDES .'/user.inc.php');
                    // Read the Order Creator's Information. No need to show Creator on Edit Form.
                   //$_ALL_POST['creator']= '';
                   //User::getList($db, $_ALL_POST['creator'], 'number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                   //$_ALL_POST['creator'] = $_ALL_POST['creator'][0];
                
                    // Read the Team Members Information.
                    $_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
                    $temp                       = "'". implode("','", $_ALL_POST['team']) ."'";
                    $_ALL_POST['team_members']  = '';
                    $_ALL_POST['team_details']  = array();
                    User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN (". $temp .")");
                    $_ALL_POST['team'] = array();
                    foreach ( $_ALL_POST['team_members'] as $key=>$members) {
                    //for ( $i=0; $i<count($_ALL_POST['team_members']); $i++ ) {
                        $_ALL_POST['team'][] = $members['user_id'];
                        $_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
                    }
                    //date of order
                     $_ALL_POST['do_o']  = explode(' ', $_ALL_POST['do_o']);
                    $temp               = explode('-', $_ALL_POST['do_o'][0]);
                    $_ALL_POST['do_o']  = NULL;
                    $_ALL_POST['do_o']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    // Setup the date of delivery.
                    $_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
                    $temp               = explode('-', $_ALL_POST['do_d'][0]);
                    $_ALL_POST['do_d']  = NULL;
                    $_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    $po_id              = $_ALL_POST['po_id'];
                    
                    // Setup the start date and est date
                    if( $_ALL_POST['st_date'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['st_date']  ='';
                    }else{
                        $_ALL_POST['st_date']  = explode(' ', $_ALL_POST['st_date']);
                        $temp               = explode('-', $_ALL_POST['st_date'][0]);
                        $_ALL_POST['st_date']  = NULL;
                        $_ALL_POST['st_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    // Setup the start date and est date
                    
                    if( $_ALL_POST['es_ed_date'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['es_ed_date']  ='';
                    }else{
                        $_ALL_POST['es_ed_date']  = explode(' ', $_ALL_POST['es_ed_date']);
                        $temp               = explode('-', $_ALL_POST['es_ed_date'][0]);
                        $_ALL_POST['es_ed_date']  = NULL;
                        $_ALL_POST['es_ed_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    
                     // Set up the Services.
                    //$_ALL_POST['service_id'] = explode(',', $_ALL_POST['service_id']);
                   
                    // Read the Particulars.
                    $temp_p = NULL;
                    $condition_query_p ='';
                    $service_idArr= array();
                    $condition_query_p = "WHERE ".TABLE_SALE_PRE_QUOTE_P.".ord_no = '". $_ALL_POST['number'] ."'";
                    PreQuote::getParticulars($db, $temp_p, '*', $condition_query_p);
                    foreach ( $temp_p as $pKey => $parti ) {
                        
                        $_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];
                        $_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
                        $_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];
                        $_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
                        $_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
                        $_ALL_POST['ss_title'][$pKey]   = $temp_p[$pKey]['ss_title'];
                        $_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];
                        $_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
                        $_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
                        $_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
                        $_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
                        $_ALL_POST['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
                        $_ALL_POST['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
                        $_ALL_POST['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
                        $_ALL_POST['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];
                        $service_idArr[] = $temp_p[$pKey]['s_id'];
                    }
                   $_ALL_POST['service_id'] = $service_idArr;
                }
                else {
                    $messages->setErrorMessage("You do not have the Permission to view the Order with the current Access Level.");
                }
            }
            else {
                $messages->setErrorMessage("The Pre Quote was not found or you do not have the Permission to access this Order.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $or_id;
            
            include ( DIR_FS_NC .'/sale-pre-quote-list.php');
        }
        else {
        
            /*
            if ( !empty($po_id) ) {
                // Read the Pre Order details.
                include_once ( DIR_FS_INCLUDES .'/bill-pre-order.inc.php');
                $pre_order  = NULL;
                $fields     = 'id, '. TABLE_BILL_PO .'.access_level, '. TABLE_BILL_PO .'.status, order_details, client_details';
                $fields     .= ', user_id, number, f_name, l_name';
                if ( PreOrder::getDetails($db, $pre_order, $fields, "WHERE id = '". $po_id ."' ") > 0 ) {
                    $pre_order = $pre_order[0];
                    
                    if ( $pre_order['status'] == COMPLETED ) {
                        if ( $access_level > $pre_order['access_level'] ) {
                            $_ALL_POST['po_id']             = $pre_order['id'];
                            $_ALL_POST['po_order_details']  = $pre_order['order_details'];
                            $_ALL_POST['po_client_details'] = $pre_order['client_details'];
                            $_ALL_POST['po_user_id']        = $pre_order['user_id'];
                            $_ALL_POST['po_number']         = $pre_order['number'];
                            $_ALL_POST['po_f_name']         = $pre_order['f_name'];
                            $_ALL_POST['po_l_name']         = $pre_order['l_name'];
                            $_ALL_POST['po_access_level']   = $pre_order['access_level'];
                            $pre_order = NULL;
                        }
                        else {
                            $messages->setErrorMessage("You donot have the Right to create Order for this Pre Order.");
                            $_ALL_POST = '';
                        }
                    }
                    else {
                        $messages->setErrorMessage("There is anomaly in the Status of the Pre Order.");
                        $_ALL_POST = '';
                    }
                }
                else {
                    $messages->setErrorMessage("The Selected Pre Order was not found.");
                }
            }
            */

            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['po_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['po_access_level'], $al_list, $index);
                $_ALL_POST['po_access_level'] = $al_list[$index[0]]['title'];
            }
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'pq_id', 'value' => $pq_id);
            $hidden[] = array('name'=> 'po_id', 'value' => $po_id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            //$page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
            //$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
            $page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-order-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>