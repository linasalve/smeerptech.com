<?php

    if ( $perm->has('nc_site_tkr_add') ) {

        $_ALL_POST	= NULL;
        $data       = NULL;
		$placement 	= Ticker::getPlacement();
		$type 		= Ticker::getType();


		//get the list of site to show on the dropdown
		$sitelist = Ticker::getSites();

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            if ( Ticker::validateAdd($data, $extra) ) {
				$date_arr = explode("/", $data["activation_date"] ) ;
				//$start_date = mktime(0,0,0, $date_arr[1], $date_arr[0], $date_arr[2] ) ;
				$start_date = $date_arr[2] .'-'. $date_arr[1] .'-'. $date_arr[0] .' 00-00-00';
				
				if ( !empty($data["expiry_date"] ) ) {
					$date_arr = explode("/", $data["expiry_date"] ) ;
					//$end_date = mktime(0,0,0, $date_arr[1], $date_arr[0], $date_arr[2] ) ;
					$end_date = $date_arr[2] .'-'. $date_arr[1] .'-'. $date_arr[0] .' 23-59-59';
				}
				else {
					$end_date	= 0 ;
				}
				$site_id_str = implode(",", $data["site_id"]) ;
				$query = "INSERT INTO ". TABLE_TICKERS 
							." SET "
								." site_id 			= '" . $site_id_str ."', "
								." title 			= '" . $data["title"] ."', "
								." text 			= '" . $data["text"] ."', "
								." url 				= '" . $data["url"] ."', "
								." ticker_order 	= '" . $data["ticker_order"] ."', "
								." status 			= '" . $data["status"] ."', "
								." activation_date 	= '" . $start_date ."', "
								." expiry_date 		= '" . $end_date ."' " ;
                if ($db->query($query) && $db->affected_rows() > 0) 
					$messages->setOkMessage("The New Ticker has been added.");
				else
					$messages->setErrorMessage('Problem saving record. Try later');
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/ticker-list.php');
        }
        else {

$data["site_id"] = array('1');
$_ALL_POST["site_id"] = array('1');
		
			$page_list = array();
			if ( isset($data["site_id"]) && $data["site_id"] != '' ) {
				getSitePages($db, $page_list, $data["site_id"]);
			}
			$page_list = array_merge(array(array('page_id' => 0, 'page_name' => 'All pages' )), $page_list);
		
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');

            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
			$page["var"][] = array('variable' => 'placement', 'value' => 'placement');
			$page["var"][] = array('variable' => 'type', 'value' => 'type');
			$page["var"][] = array('variable' => 'page_list', 'value' => 'page_list');
  			$page["var"][] = array('variable' => 'all_sites', 'value' => 'sitelist');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'ticker-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You do not have the Permisson to Access this module.");
    }
?>