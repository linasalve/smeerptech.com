<?php

if ( $perm->has('nc_rp_mkt_ex') ) {

	    include_once ( DIR_FS_INCLUDES .'/prospects-order.inc.php');     
		include_once ( DIR_FS_INCLUDES .'/prospects-quotation.inc.php');	
		include_once ( DIR_FS_INCLUDES .'/prospects.inc.php');	
		include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
        //to view report member id should be compulsary
        $executive	= isset($_GET["executive"]) 	? $_GET["executive"]	: ( isset($_POST["executive"]) 	? $_POST["executive"] : '' );
        $executive_details	= isset($_GET["executive_details"]) 	? $_GET["executive_details"]	: ( isset($_POST["executive_details"]) 	? $_POST["executive_details"] : '' );
        $_SEARCH["executive"] 	= $executive;       
        $_SEARCH["executive_details"] 	= $executive_details;       
        $where_added    = true;
        $allData = $_GET 	? $_GET	: ($_POST 	? $_POST : '' );
        $extra_url=$condition_query=$condition_url=$pagination ='';
        
		// Read the Date data.
        $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
        $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
        $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
        $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
                
		include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
		$fieldsu=TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".number,".TABLE_USER.".user_id" ;
		$ustr= str_replace(",","','",trim($my['my_reporting_members'],","));
	    $condition_query_u  = " WHERE ".TABLE_USER.".status='".User::ACTIVE."' AND ".TABLE_USER.".user_id IN('".$ustr."','".$my['uid']."')" ;
		User::getList( $db, $ulist, $fieldsu, $condition_query_u);
		
            
        if(array_key_exists('submit_search',$allData)){
        
            if(!empty($executive) && !empty($chk_date_from) && !empty($date_from) 
				&& !empty($chk_date_to) && !empty($date_to) ){        
                
                // BO: From Date
                if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR') && !empty($date_from)) {
                    
                    $dfa = explode('/', $date_from);
                   // $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
                    $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] ;
                    
                    $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
                    $_SEARCH["chk_date_from"]   = $chk_date_from;
                    $_SEARCH["date_from"]       = $date_from;
                }
                
                // EO: From Date
                
                // BO: Upto Date
                if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)){ 
                    $dta = explode('/', $date_to);
                    //$dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';                   
                    $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0];    
                    $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
                    $_SEARCH["chk_date_to"] = $chk_date_to ;
                    $_SEARCH["date_to"]     = $date_to ;
                }                
                // EO: Upto Date    
 
 
				$condition_query = " WHERE ".TABLE_USER.".user_id IN ('".$executive."')";
				
			    $list	= 	NULL;
				$total	=	User::getList($db, $list, '', $condition_query);
			
				$extra_url  = '';
				if ( isset($condition_url) && !empty($condition_url) ) {
					$extra_url  = $condition_url;
				}
				$condition_url .="&perform=".$perform;            
				$extra_url  .= "&x=$x&rpp=$rpp";
				$extra_url  = '&start=url'. $extra_url .'&end=url';
				
				
				$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
			
				$list	= NULL;
				$fields=TABLE_USER.".f_name,".TABLE_USER.".l_name,".TABLE_USER.".number, ".TABLE_USER.".user_id " ; 
				User::getList( $db, $list, $fields, $condition_query, $next_record, $rpp);
                 
                
				foreach($list as $key=>$val){ 
					
					$name = $val['f_name']." ".$val['l_name']." - ( ".$val['number']." ) " ;
					$prospects = '';
					$leads = '';
					$negative_leads = $active_leads = $crm_negative_leads = $crm_positive_leads = '';
					$leads_members = $proposals = '';
					$orders_members = $followups = '';
					
					$sql = " SELECT COUNT(user_id) as  prospects FROM ".TABLE_PROSPECTS." 
						WHERE ".TABLE_PROSPECTS.".created_by='".$val['user_id']."' AND ".TABLE_PROSPECTS.".parent_id='' AND 
						date_format(".TABLE_PROSPECTS.".do_add,'%Y-%m-%d') >= '".$dfa."' AND 
						date_format(".TABLE_PROSPECTS.".do_add,'%Y-%m-%d') <= '".$dta."'"; 
						
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ){ 
							$prospects = $db->f('prospects');			
						}
					}
					 
					$sql = " SELECT COUNT(id) as self_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$val['user_id']."') AND
						date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record()) { 
							$self_leads =$db->f('self_leads');			
						}
					} 
					$sql = " SELECT COUNT(id) as shared_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$val['user_id']."' AND 
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%') AND
						date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record()) { 
							$shared_leads = $db->f('shared_leads');			
						}
					} 
					
					$sql = " SELECT COUNT(id) as self_hot_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$val['user_id']."')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::HOTLSTATUS."'"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$self_hot_leads = $db->f('self_hot_leads');
						}
					}	 
					
					$sql = " SELECT COUNT(id) as shared_hot_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$val['user_id']."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::HOTLSTATUS."'"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$shared_hot_leads = $db->f('shared_hot_leads');
						}
					} 	
					
					$sql = " SELECT COUNT(id) as self_warm_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$val['user_id']."')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::WARMLSTATUS."'";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ){ 
							$self_warm_leads = $db->f('self_warm_leads');
						}
					}	
					$sql = " SELECT COUNT(id) as shared_warm_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$val['user_id']."' AND 
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::WARMLSTATUS."'";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$shared_warm_leads =$db->f('shared_warm_leads');
						}
					}	
					
					$sql = " SELECT COUNT(id) as self_cold_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$val['user_id']."' )
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::COLDLSTATUS."'";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$self_cold_leads = $db->f('self_cold_leads');
						}
					}
					$sql = " SELECT COUNT(id) as shared_cold_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$val['user_id']."' OR 
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::COLDLSTATUS."'";
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ){ 
							$shared_cold_leads =$db->f('shared_cold_leads');
						}
					}	
					
					$sql = " SELECT COUNT(id) as self_negative_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$val['user_id']."') 
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::NEGATIVELSTATUS."'";
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$self_negative_leads =$db->f('self_negative_leads');
						}
					}
					
					
					$sql = " SELECT COUNT(id) as shared_negative_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$val['user_id']."' OR 
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%') 
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::NEGATIVELSTATUS."'";
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$shared_negative_leads =$db->f('shared_negative_leads');
						}
					}	
					
					
					$sql = " SELECT COUNT(id) as self_win_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by = '".$val['user_id']."' )
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::WINLSTATUS."'"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$self_win_leads =$db->f('self_win_leads');
						}
					}
					
					$sql = " SELECT COUNT(id) as shared_win_leads FROM ".TABLE_PROSPECTS_ORDERS." 
						WHERE (".TABLE_PROSPECTS_ORDERS.".order_closed_by != '".$val['user_id']."' AND 
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_ORDERS.".status='".ProspectsOrder::ACTIVE."'
						AND ".TABLE_PROSPECTS_ORDERS.".lead_status='".ProspectsOrder::WINLSTATUS."'"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$shared_win_leads =$db->f('shared_win_leads');
						}
					}	
					
					
					
					/* $sql = " SELECT COUNT(id) as leads_members FROM ".TABLE_LEADS_ORDERS." 
						WHERE ".TABLE_LEADS_ORDERS.".team LIKE '%,".$val['user_id'].",%' 
						AND date_format(".TABLE_LEADS_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_LEADS_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."' 
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$leads_members =$db->f('leads_members'); 
						}
					} team */ 
					
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as total_self_proposals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."')
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
						('".ProspectsQuotation::PENDING."','".ProspectsQuotation::BLOCKED."','".ProspectsQuotation::DELETED."')"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$total_self_proposals = $db->f('total_self_proposals'); 
						} 
					} 
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as total_shared_proposals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$val['user_id']."'  AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
						('".ProspectsQuotation::PENDING."','".ProspectsQuotation::BLOCKED."','".ProspectsQuotation::DELETED."')"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$total_shared_proposals =$db->f('total_shared_proposals'); 
						} 
					}
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as proposals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."' )
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::BLOCKED."')"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$total_self_block_proposals =$db->f('proposals'); 
						} 
					} 
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as proposals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$val['user_id']."' 
						AND	".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::BLOCKED."')"; 
					$db->query($sql); 
					if ( $db->nf()>0 ){
						while ( $db->next_record() ) { 
							$total_shared_block_proposals = $db->f('proposals'); 
						} 
					}  
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as proposals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."')
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
						('".ProspectsQuotation::DELETED."')"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$total_self_cancelled_proposals =$db->f('proposals'); 
						} 
					}
					 
					
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as proposals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$val['user_id']."' AND  
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::DELETED."')"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$total_shared_cancelled_proposals =$db->f('proposals'); 
						} 
					}
					
					
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as pending_proposals FROM ".TABLE_PROSPECTS_QUOTATION." 
						LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."')
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN 
						('".ProspectsQuotation::PENDING."')"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$pending_self_proposals = $db->f('pending_proposals'); 
						} 
					}
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as pending_proposals FROM ".TABLE_PROSPECTS_QUOTATION." 
						LEFT JOIN ".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."' 
						AND ".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%') AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."' AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$pending_shared_proposals = $db->f('pending_proposals'); 
						} 
					}  
					
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as approved_proposals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."')	
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')
						AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::APPROVED."')
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$approved_self_proposals =$db->f('approved_proposals'); 
						} 
					}
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as approved_proposals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."' AND
						".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')	
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')
						AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::APPROVED."')
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$approved_shared_proposals =$db->f('approved_proposals'); 
						} 
					}
				
					
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as pending_for_approvals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."')	 
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')
						AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::SEND_FOR_APPROVAL."')
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$self_pending_for_approvals =$db->f('pending_for_approvals'); 
						} 
					}
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as pending_for_approvals FROM ".TABLE_PROSPECTS_QUOTATION." LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."' 
						AND ".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')	 
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')
						AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::SEND_FOR_APPROVAL."')
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$shared_pending_for_approvals =$db->f('pending_for_approvals'); 
						} 
					}
					
					//NotSendForApproval
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as nsfa_proposals FROM ".TABLE_PROSPECTS_QUOTATION."  LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."' )	 
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')
						AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::NONEAPPROVAL."')
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$nsfa_self_proposals = $db->f('nsfa_proposals'); 
						} 
					}
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as nsfa_proposals FROM ".TABLE_PROSPECTS_QUOTATION."  LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$val['user_id']."' 
						AND ".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')	 
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')
						AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::NONEAPPROVAL."')
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$nsfa_shared_proposals = $db->f('nsfa_proposals'); 
						} 
					}
					
					
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as disapproved_proposals FROM ".TABLE_PROSPECTS_QUOTATION."  LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by = '".$val['user_id']."')	 
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')
						AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::DISAPPROVED."')
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$disapproved_self_proposals =$db->f('disapproved_proposals'); 
						} 
					}
					$sql = " SELECT COUNT(".TABLE_PROSPECTS_QUOTATION.".id) as disapproved_proposals FROM ".TABLE_PROSPECTS_QUOTATION."  LEFT JOIN 
						".TABLE_PROSPECTS_ORDERS." ON ".TABLE_PROSPECTS_QUOTATION.".or_id = ".TABLE_PROSPECTS_ORDERS.".id
						WHERE (".TABLE_PROSPECTS_QUOTATION.".order_closed_by != '".$val['user_id']."' 
						AND ".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$val['user_id'].",%')	 
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_QUOTATION.".do_c,'%Y-%m-%d') <= '".$dta."'
						AND ".TABLE_PROSPECTS_QUOTATION.".status IN ('".ProspectsQuotation::PENDING."')
						AND ".TABLE_PROSPECTS_QUOTATION.".quotation_status IN ('".ProspectsQuotation::DISAPPROVED."')
						"; 
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$disapproved_shared_proposals =$db->f('disapproved_proposals'); 
						} 
					}

					$sql = " SELECT COUNT(ticket_id) as total_activity FROM ".TABLE_PROSPECTS_TICKETS." 
						WHERE ".TABLE_PROSPECTS_TICKETS.".ticket_creator_uid = '".$val['user_id']."' 
						AND ".TABLE_PROSPECTS_TICKETS.".ticket_type ='".ProspectsTicket::TYP_REPORTING."'
						AND date_format(".TABLE_PROSPECTS_TICKETS.".do_e,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_PROSPECTS_TICKETS.".do_e,'%Y-%m-%d') <= '".$dta."'
						";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$total_activity =$db->f('total_activity'); 
						}
					}
					
					 
					/* $sql = " SELECT COUNT(id) as orders_members FROM ".TABLE_BILL_ORDERS." 
						WHERE ".TABLE_BILL_ORDERS.".team LIKE '%,".$val['user_id'].",%' 
						AND date_format(".TABLE_BILL_ORDERS.".do_c,'%Y-%m-%d') >= '".$dfa."'
						AND date_format(".TABLE_BILL_ORDERS.".do_c,'%Y-%m-%d') <= '".$dta."'
						";
					
					$db->query($sql); 
					if ( $db->nf()>0 ) {
						while ( $db->next_record() ) { 
							$orders_members =$db->f('orders_members');
					
						}
					} 
					 
					*/
					$name = $val['f_name']." ".$val['l_name']." - ( ".$val['number']." ) " ;  
					
					$fList[]= array(
						'name'=>$name,
						'prospects'=>$prospects,
						'self_leads'=>$self_leads,
						'shared_leads'=>$shared_leads,
						'self_hot_leads'=>$self_hot_leads,
						'shared_hot_leads'=>$shared_hot_leads,
						'self_warm_leads'=>$self_warm_leads,
						'shared_warm_leads'=>$shared_warm_leads,
						'self_cold_leads'=>$self_cold_leads,
						'shared_cold_leads'=>$shared_cold_leads,
						'self_negative_leads'=>$self_negative_leads,
						'shared_negative_leads'=>$shared_negative_leads,
						'self_win_leads'=>$self_win_leads, 
						'shared_win_leads'=>$shared_win_leads,						
						'total_self_proposals'=>$total_self_proposals,
						'total_shared_proposals'=>$total_shared_proposals,
						'total_shared_block_proposals'=>$total_shared_block_proposals,
						'total_self_block_proposals'=>$total_self_block_proposals,
						'total_self_cancelled_proposals'=>$total_self_cancelled_proposals, 
						'total_shared_cancelled_proposals'=>$total_shared_cancelled_proposals, 
						'pending_self_proposals'=>$pending_self_proposals,
						'pending_shared_proposals'=>$pending_shared_proposals,
						'approved_self_proposals'=>$approved_self_proposals,
						'approved_shared_proposals'=>$approved_shared_proposals,
						'self_pending_for_approvals'=>$self_pending_for_approvals,
						'shared_pending_for_approvals'=>$shared_pending_for_approvals,
						'nsfa_self_proposals'=>$nsfa_self_proposals,
						'nsfa_shared_proposals'=>$nsfa_shared_proposals,
						'disapproved_self_proposals'=>$disapproved_self_proposals,
						'disapproved_shared_proposals'=>$disapproved_shared_proposals,
						'total_activity'=>$total_activity 
					);
				
				}
               
              
                $extra_url  .= "&x=$x&rpp=$rpp&executive=$executive&executive_details=$executive_details&submit_search=1";
                $extra_url  = '&start=url'. $extra_url .'&end=url'; 
				
				//Datewise Leads Details Along With The Leads Reporting BOF 
			}else{
                $messages->setErrorMessage('Please select Executive and Date Duration, From date and To date.');
            } 
        }   
            
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    
    $variables['can_view_list_all'] = false;
    if ( $perm->has('nc_ld_or_list_all') ) {
		$variables['can_view_list_all'] = true;
	}
     
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
	$page["var"][] = array('variable' => 'ulist', 'value' => 'ulist');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    
    
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-marketing-executive.html');
}
else {
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>