<?php
	if ( $perm->has('nc_pr_delete') ) {
		$user_id	= isset($_GET["user_id"]) ? $_GET["user_id"] : ( isset($_POST["user_id"]) ? $_POST["user_id"] : '' );
		
		$extra = array( 'db' 			=> &$db,
						'access_level' 	=> $my['access_level'],
						'messages' 		=> $messages
					);
		Prospects::delete($user_id, $extra);
		
		// Display the list.
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-delete.html');
	}
	else {
		$messages->setErrorMessage("You donot have the Right to Access this module.");
	}
?>