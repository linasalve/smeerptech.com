<?php
// $start['time'] = time();
// $start['microtime'] = microtime();

    if ( $perm->has('nc_tcr_edit') ) {
        $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
    
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        $al_list    = getAccessLevel($db, $access_level);

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            //print_r($_POST);
            $data       = processUserData($_ALL_POST);

            $extra = array( 'db'        => &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list
                        );
            
            if ( TermsRegistration::validateUpdate($data, $extra) ) {
				
                $query  = " UPDATE ". TABLE_TERMS_CONDITION
                        ." SET "
							. TABLE_TERMS_CONDITION .".title			= '". $data["title"] ."'"
                            .",". TABLE_TERMS_CONDITION .".sequence			= '". $data["sequence"] ."'"
						    .",". TABLE_TERMS_CONDITION .".status			= '". $data["status"] ."'"
						    .",". TABLE_TERMS_CONDITION .".condition_content= '". $data["condition_content"] ."'"
						    ." WHERE ". TABLE_TERMS_CONDITION .".id   	= '". $data['id'] ."'";
                if ( $db->query($query) ) {
                    $messages->setOkMessage("Terms and Condition has been updated.");
                    
                    //to flush the data.
                    //$_ALL_POST  = NULL;
                    $data       = NULL;
                }
                else {
                    $messages->setErrorMessage('Terms and Condition was not updated.');
                }
            }
            
        }else{
        
                $condition_query= " WHERE id = '". $id ."' ";
                $_ALL_POST      = NULL;
                if ( TermsRegistration::getList($db, $_ALL_POST, '*', $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST[0];
                    
                }else { 
                    $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
                }
        
        }

        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            $variables['hid'] = $id;
            $condition_query='';
            $perform='list';
            include ( DIR_FS_NC .'/terms-registration-list.php');
        }
        else {
          
          
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                    $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'id', 'value' => $id);
            //print_r($_ALL_POST);
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'terms-registration-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
//echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
//echo "<br/>";
//echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));

?>