<?php
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    error_reporting(E_ALL);
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   )); 
    include_once ( DIR_FS_NC ."/header.php"); 
    include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php' );
	include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
    if ( $perm->has('nc_st_close_status') ){ //
	 
		$ticket_id =isset($_GET["id"])? $_GET["id"] : (isset($_POST["id"])?$_POST["id"] : '' );
		$ticket_status = isset($_GET["status"]) ? $_GET["status"] : (isset($_POST["status"]) ? $_POST["status"] : '' );

        
        $access_level = $my['access_level'];
                
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,                        
                        'messages'     => &$messages
                        );
        SupportTicket::tStatus($ticket_id, $ticket_status,$extra);
		
		$randomUser = getRandomAssociate();
		$data['display_name']= $randomUser['name']." ( ".$randomUser['number']." )";
		$data['display_user_id'] = $randomUser['user_id'] ;
		$data['display_designation'] = $randomUser['desig'];		 
				
		$ticket_no  =  SupportTicket::getNewNumber($db);
		
		$data['hrs']=0;
		$data['min']=2;
		$hrs1 = (int) ($data['hrs'] * 6);
        $min1 = (int) ($data['min'] * 6);  
		$sql1= "SELECT ". TABLE_ST_TICKETS .".invoice_profm_id, 
			".TABLE_ST_TICKETS.".invoice_id,"."
			".TABLE_ST_TICKETS.".flw_ord_id,"."
			".TABLE_ST_TICKETS.".ticket_owner_uid,"." 
			".TABLE_ST_TICKETS.".ticket_owner"."
			FROM ". TABLE_ST_TICKETS ." WHERE (". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' )" ;
		$db->query($sql1) ;
		if($db->nf() > 0){
			$db->next_record() ;
			$invoice_profm_id = $db->f("invoice_profm_id") ;
			$invoice_id = $db->f("invoice_id") ;
			$flw_ord_id = $db->f("flw_ord_id") ; 
			$data['ticket_owner_uid'] = $db->f("ticket_owner_uid") ; 
			$data['ticket_owner'] 	  = $db->f("ticket_owner") ; 
		}
		$data['ticket_subject']='';
		$data['ticket_text'] = 'Status marked as Close ';
		
		$query = "SELECT "	. TABLE_ST_TICKETS .".ticket_date, ".TABLE_ST_TICKETS.".status"
				." FROM ". TABLE_ST_TICKETS 
				." WHERE " 
				." (". TABLE_ST_TICKETS .".ticket_child = '". $ticket_id ."' " 
							." OR "
							. TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " 
						.") "
				." ORDER BY ". TABLE_ST_TICKETS .".ticket_date DESC LIMIT 0,1";				
				
		$db->query($query) ;
		if($db->nf() > 0){
			$db->next_record() ;
			$last_time = $db->f("ticket_date") ;
			$ticket_response_time = time() - $last_time ;
			 
		}
		else{
			$ticket_response_time = 0 ;
		}
		$ticket_date 	= time() ;
		$ticket_replied = '0' ;
		
		//Submit a thread to main ticket BOF
		 $query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
			. TABLE_ST_TICKETS .".ticket_no          = '". $ticket_no ."', "
			. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
			. TABLE_ST_TICKETS .".invoice_profm_id   = '". $invoice_profm_id ."', "
			. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
			/* 
			. TABLE_ST_TICKETS .".template_id        = '". $data['template_id'] ."', "
			. TABLE_ST_TICKETS .".template_file_1    = '". $data['template_file_1'] ."', "
			. TABLE_ST_TICKETS .".template_file_2    = '". $data['template_file_2'] ."', "
			. TABLE_ST_TICKETS .".template_file_3    = '". $data['template_file_3'] ."', "
			. TABLE_ST_TICKETS .".ss_id      	     = '". $data['ss_id'] ."', "
			. TABLE_ST_TICKETS .".ss_title   		 = '". $data['ss_title'] ."', "
			. TABLE_ST_TICKETS .".ss_file_1   		 = '". $data['ss_file_1'] ."', "
			. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
			. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', " 
			. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
			. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
			. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
			. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
			. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
			. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "*/
			. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
			. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', " 
			. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
			. TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
			. TABLE_ST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
			. TABLE_ST_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
			//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
			//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
			. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status."', "
			. TABLE_ST_TICKETS .".ticket_child      = '".$ticket_id."', "
			//. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
			. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
			. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
			. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
			. TABLE_ST_TICKETS .".min = '". $data['min']."', "
			//. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
			. TABLE_ST_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
			. TABLE_ST_TICKETS .".ticket_replied    = '".$ticket_replied."', "
			. TABLE_ST_TICKETS .".from_admin_panel  = '".SupportTicket::ADMIN_PANEL."', "
			/* . TABLE_ST_TICKETS .".mail_client    = '".$mail_client."', "
			. TABLE_ST_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
			. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
			. TABLE_ST_TICKETS .".domain_name       = '". $data['domain_name'] ."', " */
			. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
			. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
			. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
			/* . TABLE_ST_TICKETS .".is_private        = '". $data['is_private'] ."', " */
			. TABLE_ST_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
			. TABLE_ST_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
			. TABLE_ST_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "
		/*  . TABLE_ST_TICKETS .".to_email           = '". $data['to'] ."', "
			. TABLE_ST_TICKETS .".cc_email           = '". $data['cc'] ."', "
			. TABLE_ST_TICKETS .".bcc_email          = '". $data['bcc'] ."', "
			. TABLE_ST_TICKETS .".to_email_1         = '". $data['to_email_1'] ."', "
			. TABLE_ST_TICKETS .".cc_email_1         = '". $data['cc_email_1'] ."', "
			. TABLE_ST_TICKETS .".bcc_email_1        = '". $data['bcc_email_1'] ."', " */
			. TABLE_ST_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
		$execute = $db->query($query) ;
		//Submit a thread to main ticket EOF
        
		if($execute){
				$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				<tr>
					<td class=\"message_header_ok\">
						<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
						Success
					</td>
				</tr>
				<tr>
					<td class=\"message_ok\" align=\"left\" valign=\"top\">
						<ul class=\"message_ok_2\">
							<li class=\"message_ok\">Status marked to close successfully </li>
						</ul>
					</td>
				</tr>
				</table>";
			echo $message .= "|".$ticket_id;
				/*
				$message .= "| ".date('j M Y')."<b> Completion Mark By </b>".$my['f_name']." ".$my['l_name'] ;
				$message .= "| <img src=\"".addslashes($variables['nc_images'])."/edit-off.gif\" border=\"0\" 
							title=\"Edit\" name=\"Edit\" alt=\"Edit\"/> ";
				$message .= "| <img src=\"".addslashes($variables['nc_images'])."/comment-off.gif\" border=\"0\" 
							title=\"Comment\" name=\"Comment\" alt=\"Comment\"/> ";
				$message .= "| <img src=\"".addslashes($variables['nc_images'])."/delete-off.gif\" border=\"0\" 
							title=\"Delete\" name=\"Delete\" alt=\"Delete\"/> ";*/
				
				$query_update = "UPDATE ". TABLE_ST_TICKETS 
							." SET ".TABLE_ST_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
							.",".TABLE_ST_TICKETS.".last_comment_from = '".SupportTicket::ADMIN_COMMENT."'"
							.",".TABLE_ST_TICKETS.".last_comment = '".$data['ticket_text']."'"
							.",".TABLE_ST_TICKETS.".last_comment_by = '".$my['uid'] ."'"
							.",".TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
							." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $ticket_id ."' " ;
			    $db->query($query_update) ;				
			 
		}
		
	}else{
	
		   $message = "<table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				<tr>
					<td class=\"message_error_header\">
						<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\" border=\"0\" />&nbsp;
						Success
					</td>
				</tr>
				<tr>
					<td class=\"message_error\" align=\"left\" valign=\"top\">
						<ul class=\"message_error\">
							<li class=\"message_error\">You do not have the permission to access this module. </li>
						</ul>
					</td>
				</tr>
				</table>";
		echo $message .= "|".$ticket_id;
	
	}
	//include_once( DIR_FS_NC ."/flush.php");
	 
?>
