<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    
$user_id =  isset($_GET["user_id"])   ? $_GET["user_id"]  : ( isset($_POST["user_id"])    ? $_POST["user_id"] : '' );
$randomstatus = isset($_GET["randomstatus"])   ? $_GET["randomstatus"]  : ( isset($_POST["randomstatus"]) ? 
$_POST["randomstatus"] : '' );   

$sendmail_status = isset($_GET["sendmail_status"])   ? $_GET["sendmail_status"]  : ( isset($_POST["sendmail_status"]) ? 
$_POST["sendmail_status"] : '' );  
  
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    $db_new 		= new db_local; // database handle
	
    if (isset($randomstatus) && $randomstatus!=''){      
        if( $randomstatus=='1'){
			$sql= " UPDATE ".TABLE_USER." SET ".TABLE_USER.".random_select='".$randomstatus."' WHERE ".TABLE_USER.".user_id ='".$user_id."'";
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Random Selection Status changed Successesfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					
					$message .= "| <a href=\"#\" onClick=\"updateRandomStatus('".$user_id."',0)\">
                         <img src=\"".addslashes($variables['nc_images'])."/update-on.gif\" border=\"0\" 
                                title=\"Active\" name=\"Active\" alt=\"Active\"/></a> ";
								
			}
        }elseif($randomstatus=='0'){		
		 
			$sql= " UPDATE ".TABLE_USER." SET ".TABLE_USER.".random_select='".$randomstatus."' WHERE ".TABLE_USER.".user_id ='".$user_id."'";
			
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Random Selection Status changed Successesfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					
					$message .= "| <a href=\"#\" onClick=\"updateRandomStatus('".$user_id."',1)\">
                         <img src=\"".addslashes($variables['nc_images'])."/update-off.gif\" border=\"0\" 
                                title=\"De-active\" name=\"De-active\" alt=\"De-active\"/></a> ";
								
			}
		
		} 	   
	}	
	
	
	if (isset($sendmail_status) && $sendmail_status!=''){      
        if( $sendmail_status=='1'){
			$sql= " UPDATE ".TABLE_USER." SET ".TABLE_USER.".send_mail='".$sendmail_status."' WHERE ".TABLE_USER.".user_id ='".$user_id."'";
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Send Mail Status changed Successesfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					
					$message .= "| <a href=\"#\" onClick=\"updateSendMailStatus('".$user_id."',0)\">
                         <img src=\"".addslashes($variables['nc_images'])."/update-on.gif\" border=\"0\" 
                                title=\"Active\" name=\"Active\" alt=\"Active\"/></a> ";
								
			}
        }elseif($sendmail_status=='0'){		
		 
			$sql= " UPDATE ".TABLE_USER." SET ".TABLE_USER.".send_mail='".$sendmail_status."' WHERE ".TABLE_USER.".user_id ='".$user_id."'";
			
			$execute= $db_new->query($sql);
			if($execute){
					$message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">Send Mail Status changed Successesfully </li>
							</ul>
						</td>
					</tr>
					</table>";
					
					$message .= "| <a href=\"#\" onClick=\"updateSendMailStatus('".$user_id."',1)\">
                         <img src=\"".addslashes($variables['nc_images'])."/update-off.gif\" border=\"0\" 
                                title=\"De-active\" name=\"De-active\" alt=\"De-active\"/></a> ";
								
			}
		
		} 	   
	}
echo $message ;
include_once( DIR_FS_NC ."/flush.php");
exit;

?>
