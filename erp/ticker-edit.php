<?php
    if ( $perm->has('nc_site_tkr_edit') ) {	
		$id	= isset($_GET["id"]) ? $_GET["id"] 	: ( isset($_POST["id"]) ? $_POST["id"] : '' );
	
		$data		= "";
		$_ALL_POST	= "";
		//get the list of site to show on the dropdown
		$sitelist = Ticker::getSites();
		if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
			$_ALL_POST 	= $_POST;
			$data		= processUserData($_ALL_POST);
	
			$extra = array( 'db' 				=> &$db,
							'messages' 			=> $messages
						);
			if (Ticker::validateUpdate($data,$extra)) {
			$date_arr = explode("/", $data["activation_date"] ) ;
			//$start_date = mktime(0,0,0, $date_arr[1], $date_arr[0], $date_arr[2] ) ;
			$start_date = $date_arr[2] .'-'. $date_arr[1] .'-'. $date_arr[0] .' 00-00-00';
			
			if ( !empty($data["expiry_date"] ) ) {
				$date_arr = explode("/", $data["expiry_date"] ) ;
				//$end_date = mktime(0,0,0, $date_arr[1], $date_arr[0], $date_arr[2] ) ;
				$end_date = $date_arr[2] .'-'. $date_arr[1] .'-'. $date_arr[0] .' 23-59-59';
			}
			else {
				$end_date	= 0 ;
			}

			$condition = " WHERE id = '". $id ."' " ;
			$site_id_str = implode(",", $data["site_id"]) ;
			$query = "UPDATE ". TABLE_TICKERS 
							." SET "
								." site_id = '". 		$site_id_str ."', "
								." title = '". 			$data["title"] ."', "
								." text = '". 			$data["text"] ."', "
								." url = '". 			$data["url"] ."', "
								." ticker_order = '". 	$data["ticker_order"] ."', "
								." status = '". 		$data["status"] ."', "
								." activation_date = '".$start_date ."', "
								." expiry_date = '". 	$end_date ."' " 
							. $condition ;

				if ( $db->query($query) ) {
					if ( $db->affected_rows() > 0 ) {
						$messages->setOkMessage("Ticker has been updated successfully.");
					}
				}
				// Clear all the data.
				$_ALL_POST	= "";
				$data		= "";
			}
		}
		
		// Check if the Form to add is to be displayed or the control is to be sent to the List page.
		if ( isset($_POST['btnCancel'])
				|| (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$variables['hid'] = $id;
	
			if ( isset($_POST['action']) && $_POST['action'] == 'search' ) {
				include ( DIR_FS_NC .'/ticker-search.php');
			}
			else {
				include ( DIR_FS_NC .'/ticker-list.php');
			}
		}
		else {
				
			$condition_query    = " WHERE id = '". $id ."' ";
			$data               = NULL;
			if ( Ticker::gedata($db, $data, '*', $condition_query) > 0 ) {
				$data       =   $data['0'];
				$_ALL_POST = $data;	
				// These parameters will be used when returning the control back to the List page.
				foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
					$hidden[]   = array('name'=> $key, 'value' => $value);
				}
				$hidden[]   = array('name'=> 'perform' , 'value' => 'edit');
				$hidden[]   = array('name'=> 'act' , 'value' => 'save');
				$hidden[]   = array('name'=> 'id' , 'value' => $id);
			}
	
			$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'data', 'value' => 'data');
			$page["var"][] = array('variable' => 'option', 'value' => 'option');
			$page["var"][] = array('variable' => 'all_sites', 'value' => 'sitelist');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'ticker-edit.html');
		}
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
