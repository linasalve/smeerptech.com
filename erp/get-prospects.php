<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
  
	   

    $client_details = isset($_GET['client_details']) ? $_GET['client_details'] : '';
     
    $optionLink =$stringOpt= '' ;
    if(!empty($client_details)){
        $sString = $client_details;
    }
    
    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
	header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header ("Pragma: no-cache"); // HTTP/1.0
    
    if (isset($sString) && !empty($sString)){
        header("Content-Type: application/json");      
	    $query =" SELECT DISTINCT(".TABLE_PROSPECTS.".user_id),".TABLE_PROSPECTS.".f_name,".TABLE_PROSPECTS.".l_name,".TABLE_PROSPECTS.".billing_name FROM ".TABLE_PROSPECTS." WHERE 
        ( ".TABLE_PROSPECTS.".f_name LIKE '%".$sString."%' OR ".TABLE_PROSPECTS.".l_name LIKE '%".$sString."%' OR ".TABLE_PROSPECTS.".billing_name LIKE '%".$sString."%' ) AND ".TABLE_PROSPECTS.".parent_id='' ";
        $db->query($query);
        echo "{\"results\": [";
        if ( $db->nf()>0 ) {          
            $arr = array();
            while ($db->next_record()){
                $billing_name = processSQLData($db->f('billing_name'));
                $billing_name = str_replace("\r\n"," ",$billing_name);
                 //$arr[] = "{\"id\": \"".$db->f('product_id')."\", \"value\": \"".htmlspecialchars($db->f('title')).""."\", \"info\": \"\"}";
                 $arr[] = "{\"id\": \"".$db->f('user_id')."\", \"value\": \"".processSQLData($db->f('f_name'))." ".processSQLData($db->f('l_name'))." (".$billing_name.")".""."\", \"info\": \"\"}";
            }
            echo implode(", ", $arr); 
        }
        echo "]}";	   
	}

include_once( DIR_FS_NC ."/flush.php");

?>
