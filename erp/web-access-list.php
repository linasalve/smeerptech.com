<?php
if ( $perm->has('nc_wb_ac_ls') ) {   
   
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	Webaccess::getDetails( $db, $list, '', $condition_query);
    $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
    //$pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';

    $list	= $fList=NULL;
    $fields = TABLE_WEB_ACCESS_LIST .'.*, '.TABLE_WEB_ACCESS_GROUPS.'.CatName' ;
    Webaccess::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    if(!empty($list)){
        foreach( $list as $key=>$val){     
         $val['collected_ip']    = trim($val['collected_ip'],",");               
         $fList[$key]=$val;
        }
    }
    // Set the Permissions.
    $variables['can_view_list']     = true;
    $variables['can_add']           = true;
    $variables['can_edit']          = true;
    $variables['can_delete']        = true;
    $variables['can_view_details']  = true;
   
    
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'web-access-list.html');
}
else {
    $messages->setErrorMessage("You do not have the permission to view the list.");
}

?>