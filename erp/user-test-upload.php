<?php

    if ( $perm->has('nc_ue_add') ) {
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
    	include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
		
        $_ALL_POST	    = NULL;
        $data           = NULL;
        $region         = new Region('91');
        $phone          = new Phone(TABLE_USER);
        $reminder       = new UserReminder(TABLE_USER);
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_ue_add_al') ) {
            $access_level += 1;
        }
        
        $al_list    = getAccessLevel($db, $access_level);
        $role_list  = User::getRoles($db, $access_level);
        
        //include_once ( DIR_FS_INCLUDES .'/account.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/department.inc.php' );
        /*$account_list    = NULL;
        $condition_queryacount= "WHERE status = '". ACTIVE ."' ORDER BY name ASC";
        Account::getList($db, $account_list, 'id,name', $condition_queryacount);*/
        
        $company_list    = NULL;
        $condition_querycomp= "WHERE status = '". ACTIVE ."' ORDER BY name ASC";
        Company::getList($db, $company_list, 'id,name', $condition_querycomp);
        
        $department_list    = NULL;
        $condition_querydept= "WHERE status = '". ACTIVE ."' ORDER BY department_name ASC";
        Department::getList($db, $department_list, 'id,department_name', $condition_querydept);
        
        //BOF read the available industries
		User::getCountryCode($db,$country_code);
        //EOF read the available industries
        define("DIR_FS_BANNERIMG",  "/home/httpd/vhosts/askoptions.com/httpdocs/files/products/banner");
        //define("DIR_FS_BANNERIMG",  "/var/www/html/leena/askoptions.com/askoptions.com/files/products/banner");

        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 			= $_POST;
            $data				= processUserData($_ALL_POST);
			print_r($_FILES);
			// BO: For generating default password.
			//$data['password']	= getRandom(6, '', array_merge($letters_b, $letters_s, $digits));
			//$data['re_password']= $data['password'];
			// EO: For generating default password.
			
			//Upload Banner Image BOF 
				$banner_image ='';
				if(!empty($_FILES['banner_image']['name'])){
					$imgDetails = pathinfo($_FILES['banner_image']['name']);
					$imgtitle= getValidStr($data['title']);
					 $banner_image = $imgtitle."_test.".$imgDetails['extension'];  					
					$uploaddir = DIR_FS_BANNERIMG."/". $banner_image;
					if (move_uploaded_file($_FILES["banner_image"]["tmp_name"], $uploaddir)){
						@chmod($uploaddir,0777);    
						$messages->setOkMessage("upload has been done successfully.");						
					}
				}
			//Upload Banner Image EOF
          
            
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/user-list.php');
        }
        else {

            $_ALL_POST['number'] = User::getNewAccNumber($db);
            if ( empty($data['country']) ) {
                $data['country'] = '91';
            }
            $lst_country    = $region->getCountryList($data['country']);
            $lst_state      = $region->getStateList($data['state']);
            $lst_city       = $region->getCityList($data['city']);
        
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'test-upload');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'reminder_count', 'value' => '6');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'role_list', 'value' => 'role_list');
            $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
            $page["var"][] = array('variable' => 'country_code', 'value' => 'country_code');
            $page["var"][] = array('variable' => 'lst_state', 'value' => 'lst_state');
            $page["var"][] = array('variable' => 'lst_city', 'value' => 'lst_city');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
            $page["var"][] = array('variable' => 'allowipArr', 'value' => 'allowipArr');
            //$page["var"][] = array('variable' => 'account_list', 'value' => 'account_list');
            $page["var"][] = array('variable' => 'company_list', 'value' => 'company_list');
            $page["var"][] = array('variable' => 'department_list', 'value' => 'department_list');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-test-upload.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>