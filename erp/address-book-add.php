<?php
    if ( $perm->has('nc_ab_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the hardware book class.
		include_once (DIR_FS_INCLUDES .'/address-book.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST); 
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            } 
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        ); 
            if( Addressbook::validateAdd($data, $extra)){ 
                $query	= " INSERT INTO ".TABLE_ADDRESS_BOOK
                            ." SET ". TABLE_ADDRESS_BOOK .".name = '".                 $data['name'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".company = '".              $data['company'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".access_level = '".         $my['access_level'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".created_by = '".           $data['created_by'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".url = '".                  $data['url'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".address = '".              $data['address'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".mobile = '".               $data['mobile'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".phone = '".                $data['phone'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".email = '".                $data['email'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".comment = '".              $data['comment'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".contact_title = '".        $data['contact_title'] ."'"
                                .",". TABLE_ADDRESS_BOOK .".date = '".                 date('Y-m-d')."'";
                
                if( $db->query($query) && $db->affected_rows() > 0 ){
                    $messages->setOkMessage("Address entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
		
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/address-book.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/address-book.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/address-book.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'address-book-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>