<?php
	/**
	 * Short description for file
	 * 
	 *
	 * PHP versions All
	 *
	 * @category   Hotel MODULE
	 * @package    ehotel.com
	 * @author     
	 * @copyright  SMEERP Technologies
	 * @license    As described below
	 * @version    1.2.0
	 * @link       
	 * @see        -NA-
	 * @since      File available since Release 1.2.0 dt. tue, 13 Feb, 2007
	 * @deprecated -NA-
	 */
	
	/*********************************************************
	* Licence:
	* This file is sole property of the installer.
	* Any type of copy or reproduction without the consent
	* of owner is prohibited.
	* If in any case used leave this part intact without 
	* any modification.
	* All Rights Reserved
	* Copyright 2007 Owner
	*******************************************************/
	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
//$pagination = isset($_POST["pagination"]) ? $_POST["pagination"] : (isset($_GET["pagination"]) ? $_GET["pagination"]:'');

	if ( $sString != "" ) {
	
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}
        $where_added = false;
        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    if ( $table == TABLE_USER_RIGHTS) {
                        // Retrieve the ID from the Rights Table.
                        $sub_query  = "SELECT id FROM ". $table ." WHERE (";
                        foreach( $field_arr as $key => $field ) {
                            $sub_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= ')';
                        $sub_query .= " AND status = '1'";
                        
                        if ( $db->query($sub_query) & $db->nf()>0 ) {
                            // Prepare the sub-query to include the RIGHTS ID in the main query.
                            if ( $where_added ) {
                                $sub_query = "OR (";
                            }
                            else {
                                $sub_query = " WHERE (";
                                $where_added = true;
                            }
                           
                            while ( $db->next_record() ) {
                                $rights = $db->f('id');
                                
                                $sub_query .= " ( "
                                                . TABLE_USER_ROLES .".rights REGEXP '^$rights$' OR "
                                                . TABLE_USER_ROLES .".rights REGEXP ',$rights$' OR "
                                                . TABLE_USER_ROLES .".rights REGEXP ',$rights,' OR "
                                                . TABLE_USER_ROLES .".rights REGEXP '^$rights,' "
                                                . " ) OR ";
                            }
                            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                            $sub_query .= ') ';
                        }
                        else {
                            
                            if ( $where_added ) {
                                $sub_query = "OR (0) ";
                            }
                            else {
                                $sub_query = " WHERE (0) ";
                                $where_added = true;
                            }
                        }
                        
                        $condition_query    .= $sub_query;
                        $where_added        = true;
                    }
                    else {
                        foreach( $field_arr as $key => $field ) {
                            $condition_query .= $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                        }
                        $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
                    }
                }
            }
            $condition_query .= ") ";
        }
        elseif ( $search_table == TABLE_USER_RIGHTS) {
            // Retrieve the IDs from the Rights Table.
            $sub_query      = "SELECT id FROM ". $search_table ." WHERE ";
            $sub_query      .= " (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $sub_query      .= " AND status = '1'";

            if ( $db->query($sub_query) & $db->nf()>0 ) {
                // Prepare the sub-query to include the RIGHTS ID in the main query.
                $sub_query = " WHERE (";
                
                while ( $db->next_record() ) {
                    $rights = $db->f('id');
            
                    $sub_query .= "( "
                                    . TABLE_USER_ROLES .".rights REGEXP '^$rights$' OR "
                                    . TABLE_USER_ROLES .".rights REGEXP ',$rights$' OR "
                                    . TABLE_USER_ROLES .".rights REGEXP ',$rights,' OR "
                                    . TABLE_USER_ROLES .".rights REGEXP '^$rights,' "
                                    . " ) OR ";
                }
                $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                $sub_query .= ') ';
            }
            else {
                $sub_query = " WHERE (0)";
            }
            
            $condition_query    .= $sub_query;
            $where_added        = true;
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

	$condition_url .= "&rpp=$rpp"
                        ."&sString=$sString"
                        ."&sType=$sType"
                        ."&sOrderBy=$sOrderBy"
                        ."&sOrder=$sOrder"
                        ."&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sString"] 	= $sString ;
	$_SEARCH["sType"] 		= $sType ;
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
	include ( DIR_FS_NC .'/user-roles-ajx-list.php');
?>