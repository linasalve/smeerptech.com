<?php
if ( $perm->has('nc_rp_bl') ) {
		include_once ( DIR_FS_INCLUDES .'/payment-party-bills.inc.php' );
		// Read the Date data.
        //$chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
        $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
        //$chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
        $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
        $condition_query2 = $condition_url ='';
        $where_added =true;  
		$totalSum=''	;	
        $_ALL_POST 	= $_POST;
         $list=null;    
		$show=0;
        if(array_key_exists('submit_search',$_POST)){
			$show=1;
            if(empty($date_from) || empty($date_to) ){        
              $messages->setErrorMessage('Please select From Date and To Date.');
            } 
        }    
            
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    
    //As validate both dates
    if($messages->getErrorMessageCount() <= 0 && $show==1 ){
    
        $list	= $dateArr=	NULL;    
        
        // BO: From Date
        if ( !empty($date_from)){
            if ( $where_added ) {
                $condition_query2 .= " AND " ;
            }
            else {
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            $dfa = explode('/', $date_from);
            $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
            
            $condition_query2 .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_dt >= '". $dfa ."'";
            
            $condition_url              .= "&date_from=$date_from";
            //$_SEARCH["chk_date_from"]   = $chk_date_from;
            $_SEARCH["date_from"]       = $date_from;
        }
        // EO: From Date
        
        // BO: Upto Date
        if ( !empty($date_to)) {
            if ( $where_added ) {
                $condition_query2 .= " AND " ;
            }
            else {
                $condition_query2.= ' WHERE ';
                $where_added    = true;
            }
            $dta = explode('/', $date_to);
            $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
            
            $condition_query2 .= " ". TABLE_PAYMENT_PARTY_BILLS .".bill_dt <= '". $dta ."'";
            
            $condition_url          .= "&date_to=$date_to";
            //$_SEARCH["chk_date_to"] = $chk_date_to ;
            $_SEARCH["date_to"]     = $date_to ;
        }
        // EO: Upto Date
        
        $fields=" SUM(".TABLE_PAYMENT_PARTY_BILLS.".bill_amount) as totalAmount,".TABLE_VENDORS.".f_name as fname,".TABLE_VENDORS.".l_name as lname,".TABLE_VENDORS.".billing_name";
        $condition_query = " WHERE ".TABLE_VENDORS.".user_id!='' ".$condition_query2." GROUP BY ".TABLE_VENDORS.".user_id ";
		PaymentPartyBills::getList(  $db, $list, $fields, $condition_query);
       
	    $totalSum='';
		$fields=" SUM(".TABLE_PAYMENT_PARTY_BILLS.".bill_amount) as totalSum ";
		$condition_query_tot = " WHERE ".TABLE_VENDORS.".user_id!='' ".$condition_query2."";
	
		PaymentPartyBills::getList(  $db, $totalAmt, $fields, $condition_query_tot);
		if(!empty($totalAmt)){
			$totalSum=$totalAmt[0]['totalSum'];
		}
	   
    }

    $page["var"][] = array('variable' => 'list', 'value' => 'list');      
	$page["var"][] = array('variable' => 'totalSum', 'value' => 'totalSum');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
 
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-bills-wise.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>