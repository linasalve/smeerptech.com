<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
				   
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/work-report.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/user.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/vendors.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/address-book.inc.php' );
    include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
    //include_once (DIR_FS_INCLUDES .'/jobs.inc.php');
    //include_once (DIR_FS_INCLUDES .'/bill-order.inc.php');
	//include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$do_r 			= isset($_GET["do_r"])         ? $_GET["do_r"]        : ( isset($_POST["do_r"])          ? $_POST["do_r"]       :       date('d/m/Y'));
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $added 			= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined(        'RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
     $where_added    = false;
	 
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
    $condition_query ='';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
   
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    $condition_url='';
    
    if($added){
         $messages->setOkMessage("Task entry has been done.");
    }
    
  
    
    if ( $perm->has('nc_wrp') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_WORK_REPORT   =>  array( 
															//'Task No' => 'task_no',                                                               
															'Reporting title'   => 'task',                               
                                                            'Reporting' => 'comment',
                                                            //'Status'        => 'status'
                                                        ),
                                );
        
        $sOrderByArray  = array(
                                TABLE_WORK_REPORT => array( 'Date of Report'  => 'do_r',
                                                            /*  'Date of Completion'  => 'do_completion',
                                                            */  'Date of Entry'  => 'do_e' 
															
                                                            
                                                    ),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_r';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_WORK_REPORT;
        }
        
         // Read the available Status 
        $variables['status'] = WorkReport::getStatus();
        $variables['priority'] = WorkReport::getPriority();
        $variables['time_type'] = WorkReport::getTimeType();
       
       //hrs, min array
        $lst_hrs = $lst_min = NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=12;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
        
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {
                include (DIR_FS_NC.'/work-report-add.php');
                if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}else{
					$page["section"][] = array('container'=>'INDEX', 'page' => 'popup.html');
				}
                break;
            }
			case ('download_file'): {
				include (DIR_FS_NC .'/work-report-file-download.php');
				break;
			} 
			/* 
			case ('sjmadd'): {
                include (DIR_FS_NC.'/work-report-sjm-add.php');
				if($ajx==0){
					$page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}else{
					$page["section"][] = array('container'=>'INDEX', 'page' => 'popup.html');
				}
                break;
            } */
            //This is used when edit from calendar
            case ('edit'): {
                include (DIR_FS_NC .'/work-report-edit.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                
                //$page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
                //$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
             //This is used when edit from list
            case ('edit_list'): {
                include (DIR_FS_NC .'/work-report-edit-list.php');                
                           
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
           case ('acomm'): {
                include (DIR_FS_NC .'/work-report-add-comment.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
            case ('view'): {
                include (DIR_FS_NC .'/work-report-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            
            /*
			case ('view_history'): {
                include (DIR_FS_NC .'/work-report-history.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('ceo_search'): {
			
				 $chk_is_sjm = isset($_POST["chk_is_sjm"])   ? $_POST["chk_is_sjm"]  : (isset($_GET["chk_is_sjm"])   ? $_GET["chk_is_sjm"]   :'');
			
				if(empty($chk_is_sjm)){
					$condition_query .= " AND ". TABLE_WORK_REPORT .".type = '". WorkReport::CEOTASK ."'"; 
			
					$condition_url = "&chk_is_sjm=1";
					$_SEARCH["chk_is_sjm"]   = 1;   
						
					$condition_query .= " AND ".TABLE_WORK_REPORT.".status='".WorkReport::PENDING."'" ;
					
					$_SEARCH['chk_status'] = 'AND';  
					$_SEARCH['sStatus'] = WorkReport::PENDING;  
					$condition_url .= "&sStatus=".$_SEARCH['sStatus']."&chk_status=AND" ;
				}
                include(DIR_FS_NC."/work-report-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('imp_search'): {
			 
				$chk_is_imp = isset($_POST["chk_is_imp"])   ? $_POST["chk_is_imp"]  : (isset($_GET["chk_is_imp"])   ? $_GET["chk_is_imp"]   :'');
				
				if(empty($chk_is_imp)){
			 
					$condition_query .= " AND ". TABLE_WORK_REPORT .".is_imp = '1'"; 
			
					$condition_url = "&chk_is_imp=1";
					$_SEARCH["chk_is_imp"]   = 1;   
						
					$condition_query .= " AND ".TABLE_WORK_REPORT.".status='".WorkReport::PENDING."'" ;
					
					$_SEARCH['chk_status'] = 'AND';  
					$_SEARCH['sStatus'] = WorkReport::PENDING;  
					$condition_url .= "&sStatus=".$_SEARCH['sStatus']."&chk_status=AND" ;
				}
                include(DIR_FS_NC."/work-report-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            } */
			
            case ('search'): {
                include(DIR_FS_NC."/work-report-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('delete'): {
                include ( DIR_FS_NC .'/work-report-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
          
            case ('list'):
			default: 
            {
                include (DIR_FS_NC .'/work-report-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'work-report.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("perform", $perform);
    $s->assign("variables", $variables);
    $s->assign("lst_hrs", $lst_hrs);
    $s->assign("lst_min", $lst_min);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>