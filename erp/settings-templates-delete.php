<?php

    if ( $perm->has('nc_emtpl_delete') ) {

        $tpl_id	= isset($_GET["tpl_id"]) ? $_GET["tpl_id"] : ( isset($_POST["tpl_id"]) ? $_POST["tpl_id"] : '' );
        
        $extra = array( 'db' 		=> &$db,
                        'messages'  => &$messages
                    );
        EmailTemplates::delete($tpl_id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/settings-email-templates-list.php');
    }
    else {
    
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
    }
?>