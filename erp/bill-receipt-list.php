<?php

    if ( $perm->has('nc_bl_inv_list') ) { 
    
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = " WHERE ". TABLE_BILL_RCPT .".status != '". Receipt::DELETED ."'" ;
        }
        // If the Invoice is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_inv_list_al') ) {
            $access_level += 1;
        }
        $condition_query .= " AND ( ";

        // If my has created this Invoice.
        $condition_query .= " (". TABLE_BILL_RCPT .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_RCPT .".access_level < $access_level ) ";
        
        // If my is the Client Manager
        $condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
                            ." AND ". TABLE_BILL_RCPT .".access_level < $access_level ) ";
        
        // Check if the User has the Right to view Invoices created by other Users.
        if ( $perm->has('nc_bl_inv_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_bl_inv_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_BILL_RCPT. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_BILL_RCPT .".access_level < $access_level_o ) ";
        }
        $condition_query .= " ) ";
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&perform=".$perform;
        
        //Code to calculate total of Amount BOF       
        /*
         $querysubtotal = " SELECT DISTINCT ".TABLE_BILL_RCPT.".id  FROM ".TABLE_BILL_RCPT  
                        ." LEFT JOIN ". TABLE_BILL_INV
                        ." ON ". TABLE_BILL_INV.".number = ".TABLE_BILL_RCPT .".inv_no " 
                        ." LEFT JOIN ". TABLE_BILL_ORD_P
                        ." ON ". TABLE_BILL_ORD_P.".ord_no = ".TABLE_BILL_INV .".or_no " 
                        ." LEFT JOIN ". TABLE_USER." ON ". TABLE_USER .".user_id = ". TABLE_BILL_RCPT .".created_by "        
                        ." LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_RCPT .".client ". $condition_query ;
        */
        $querysubtotal = " SELECT DISTINCT ".TABLE_BILL_RCPT.".id  FROM ".TABLE_BILL_RCPT  
                        ." LEFT JOIN ". TABLE_BILL_INV
                        ." ON ". TABLE_BILL_INV.".number = ".TABLE_BILL_RCPT .".inv_no " 
                        ." LEFT JOIN ". TABLE_BILL_ORDERS
                        ." ON ". TABLE_BILL_ORDERS.".number = ".TABLE_BILL_INV .".or_no " 
                        ." LEFT JOIN ". TABLE_USER." ON ". TABLE_USER .".user_id = ". TABLE_BILL_RCPT .".created_by "        
                        ." LEFT JOIN ". TABLE_CLIENTS
                        ." ON ". TABLE_CLIENTS .".user_id = ". TABLE_BILL_RCPT .".client ". $condition_query ;

        $querytotal = " SELECT SUM(".TABLE_BILL_RCPT.".amount_inr) as totalAmountInr, SUM(".TABLE_BILL_RCPT.".amount) as totalAmount
						FROM ".TABLE_BILL_RCPT." WHERE ".TABLE_BILL_RCPT.".id IN (".$querysubtotal.")" ;

        $db->query( $querytotal );
        
        if ( $db->nf() > 0 ) {
			while ( $db->next_record() ) {
                $totalAmountInr = number_format($db->f('totalAmountInr'),2);            
                $totalAmount = number_format($db->f('totalAmount'),2);            
            }
        }
        //Code to calculate total of Amount EOF
        // To count total records.
        $list = $flist=	NULL;
        $total	=0;
        $pagination   = '';
        $extra_url  = '';
        
        if($searchStr==1){
			//echo $condition_query;
            $fields = '';
            $total	=	Receipt::getDetails( $db, $list, $fields, $condition_query);        
            //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
            $list	= NULL;
            $fields = TABLE_BILL_RCPT .'.id'
                        .','. TABLE_BILL_RCPT .'.number'
                        .','. TABLE_BILL_RCPT .'.ord_no'
                        .','. TABLE_BILL_RCPT .'.inv_profm_no'
						.','. TABLE_BILL_RCPT .'.inv_profm_id'
						.','. TABLE_BILL_RCPT .'.inv_id'
                        .','. TABLE_BILL_RCPT .'.inv_no'
						.','. TABLE_BILL_RCPT .'.voucher_no'
                        .','. TABLE_BILL_RCPT .'.access_level'
                        .','. TABLE_BILL_RCPT .'.do_r'
                        .','. TABLE_BILL_RCPT .'.do_c'
						.','. TABLE_BILL_RCPT .'.do_voucher'
                        .','. TABLE_BILL_RCPT .'.do_transaction'
                        .','. TABLE_BILL_RCPT .'.currency_abbr'
                        .','. TABLE_BILL_RCPT .'.amount'
                        .','. TABLE_BILL_RCPT .'.forex_gain'
                        .','. TABLE_BILL_RCPT .'.forex_loss'
                        .','. TABLE_BILL_RCPT .'.amount_inr'
                        .','. TABLE_BILL_RCPT .'.balance'
                        .','. TABLE_BILL_RCPT .'.status'
                        .','. TABLE_BILL_RCPT .'.reason_of_delete'
						.','. TABLE_BILL_ORDERS .'.financial_yr'	
                        .','. TABLE_BILL_ORDERS .'.order_title'
                       //.','. TABLE_BILL_INV .'.id AS inv_id'
                        .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                        .','. TABLE_CLIENTS .'.number AS c_number'
                        .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                        .','. TABLE_CLIENTS .'.l_name AS c_l_name'
						.','. TABLE_CLIENTS .'.billing_name'
                        .','. TABLE_CLIENTS .'.email AS c_email'
                        .','. TABLE_CLIENTS .'.status AS c_status';
            Receipt::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        }
        
        if(!empty($list)){
            foreach( $list as $key=>$val){               
               $val['is_last_rcpt'] = false;
               $recpt_no='';
               // check whether rcpt created or not BOF
               /*  $sql = " SELECT ".TABLE_BILL_RCPT.".number FROM ".TABLE_BILL_RCPT." WHERE ".TABLE_BILL_RCPT.".inv_no = '".$val['inv_no']."' AND status !='".Receipt::DELETED."' ORDER BY id DESC LIMIT 1";
                if ( $db->query($sql) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $recpt_no = $db->f('number');
                         
                        }
                    }
                   // check whether rcpt created or not EOF
                } 
				*/
				$sql = " SELECT ".TABLE_BILL_RCPT.".number FROM ".TABLE_BILL_RCPT." WHERE 
				".TABLE_BILL_RCPT.".ord_no = '".$val['ord_no']."' AND status !='".Receipt::DELETED."' 
				ORDER BY id DESC LIMIT 1";
                if ( $db->query($sql) ) {
                    if ( $db->nf() > 0 ) {
                        while ($db->next_record()) {
                           $recpt_no = $db->f('number');
                         
                        }
                    }
                   // check whether rcpt created or not EOF
                }
                
                if($val['number'] == $recpt_no){
                    $val['is_last_rcpt'] = true;
                }
				$invoice_id = 0;
				$sql = " SELECT ".TABLE_BILL_INV.".id FROM ".TABLE_BILL_INV." WHERE 
				".TABLE_BILL_INV.".inv_profm_id = '".$val['inv_profm_id']."' AND status NOT IN 
				('".Invoice::DELETED."','".Invoice::BLOCKED."' ) ORDER BY id DESC LIMIT 1";
                if ( $db->query($sql) ) {
                    if ( $db->nf() > 0 ) {
                        while($db->next_record()){
                           $invoice_id = $db->f('id');
                        }
                    }
                }
				$val['invoice_id'] = $invoice_id ; 
                $val['amount'] = number_format($val['amount'],2);
                $val['amount_inr'] = number_format($val['amount_inr'],2);
                $flist[$key]=$val;
                
            }
        }
       
         Receipt::getCompany($db,$company);
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_followup']= false;
        $variables['can_send_receipt'] = false;
        
        if ( $perm->has('nc_bl_inv_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('nc_bl_inv_add') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_bl_inv_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_bl_inv_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_bl_inv_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_bl_inv_status') ) {
            $variables['can_change_status']     = true;
        }
        if ( $perm->has('nc_bl_inv_flw') ) {
            $variables['can_followup'] = true;
        }
        if ( $perm->has('nc_bl_inv_send') ) {
            $variables['can_send_receipt'] = true;
        }
        
        $page["var"][] = array('variable' => 'totalAmount', 'value' => 'totalAmount');
        $page["var"][] = array('variable' => 'totalAmountInr', 'value' => 'totalAmountInr');
        $page["var"][] = array('variable' => 'total', 'value' => 'total');
        
        $page["var"][] = array('variable' => 'list', 'value' => 'flist');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
		$page["var"][] = array('variable' => 'company', 'value' => 'company');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-receipt-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>