<?php

    if ( $perm->has('nc_sl_ld_add') ) {
        include_once ( DIR_FS_CLASS .'/Region.class.php');
        include_once ( DIR_FS_CLASS .'/Phone.class.php');
        include_once ( DIR_FS_CLASS .'/UserReminder.class.php');
    	include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
		include_once ( DIR_FS_INCLUDES .'/leads-ticket.inc.php');
        $_ALL_POST	= NULL;
        $data       = NULL;
        $region     = new Region('91');
        $phone      = new Phone(TABLE_SALE_LEADS);
        //$reminder   = new UserReminder(TABLE_LEADS);
        
        if ( $perm->has('nc_sl_ld_add_al') ) {
            $al_list    = getAccessLevel($db, $my['access_level']);
            //$role_list  = Lead::getRoles($db, $my['access_level']);
        }
        else {
            $al_list    = getAccessLevel($db, ($my['access_level']-1));
            //$role_list  = Lead::getRoles($db, ($my['access_level']-1));
        }


        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

			// BO: For generating default password.
			//$data['password']	= getRandom(6, '', array_merge($letters_b, $letters_s, $digits));
			//$data['re_password']= $data['password'];
			// EO: For generating default password.

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                            'al_list'   => $al_list,
                            //'role_list' => $role_list,
                            'region'    => $region,
                            'phone'     => $phone,
                            //'reminder'  => $reminder
                        );
            /*if( isset($data['already_exist_no']) ){*/
			
					if ( Leads::validateAdd($data, $extra) ) {
			
						$query	= " INSERT INTO ". TABLE_SALE_LEADS
							." SET ".  TABLE_SALE_LEADS .".access_level			= '". $my['access_level'] ."'"
								.", ". TABLE_SALE_LEADS .".created_by			= '". $my['uid'] ."'"
								.",". TABLE_SALE_LEADS .".company_name   		= '". $data['company_name'] ."'"
								.",". TABLE_SALE_LEADS .".billing_name   		= '". $data['company_name'] ."'"
								.", ". TABLE_SALE_LEADS .".lead_owner   		= '". $my['f_name'] ."'"
								//.", ". TABLE_SALE_LEADS .".lead_assign_to   	= '". $data['lead_assign_to'] ."'"
								.", ". TABLE_SALE_LEADS .".lead_source_from		= '". $data['lead_source_from'] ."'"
								.", ". TABLE_SALE_LEADS .".lead_source_to		= '". $data['lead_source_to'] ."'"
								.", ". TABLE_SALE_LEADS .".lead_campaign_id   	= '". $data['lead_campaign_id'] ."'"
								.", ". TABLE_SALE_LEADS .".lead_industry_id   	= '". $data['lead_industry_id'] ."'"
								.", ". TABLE_SALE_LEADS .".no_emp			   	= '". $data['no_emp'] ."'"
								.", ". TABLE_SALE_LEADS .".rating			   	= '". $data['rating'] ."'"
								.", ". TABLE_SALE_LEADS .".annual_revenue   	= '". $data['annual_revenue'] ."'"
								.", ". TABLE_SALE_LEADS .".rivals_id   			= '". $data['rivals_id'] ."'"
								.", ". TABLE_SALE_LEADS .".email       			= '". $data['email'] ."'"
								.", ". TABLE_SALE_LEADS .".email_1     			= '". $data['email_1'] ."'"
								.", ". TABLE_SALE_LEADS .".email_2     			= '". $data['email_2'] ."'"
								.", ". TABLE_SALE_LEADS .".title       			= '". $data['title'] ."'"
								.", ". TABLE_SALE_LEADS .".f_name      			= '". $data['f_name'] ."'"
								.", ". TABLE_SALE_LEADS .".m_name      			= '". $data['m_name'] ."'"
								.", ". TABLE_SALE_LEADS .".l_name      			= '". $data['l_name'] ."'"
								.", ". TABLE_SALE_LEADS .".p_name      			= '". $data['p_name'] ."'"
								.", ". TABLE_SALE_LEADS .".desig       			= '". $data['desig'] ."'"
								.", ". TABLE_SALE_LEADS .".org         			= '". $data['org'] ."'"
								.", ". TABLE_SALE_LEADS .".domain      			= '". $data['domain'] ."'"
								.", ". TABLE_SALE_LEADS .".gender      			= '". $data['gender'] ."'"
								.", ". TABLE_SALE_LEADS .".do_birth    			= '". $data['do_birth'] ."'"
								.", ". TABLE_SALE_LEADS .".do_aniv     			= '". $data['do_aniv'] ."'"
								.", ". TABLE_SALE_LEADS .".remarks     			= '". $data['remarks'] ."'"
								.", ". TABLE_SALE_LEADS .".already_exist_no		= '". $data['already_exist_no'] ."'"
								.", ". TABLE_SALE_LEADS .".do_add      			= '". date('Y-m-d H:i:s', time()) ."'"
								.", ". TABLE_SALE_LEADS .".status      			= '". $data['status'] ."'" ;
					
						if ($db->query($query) && $db->affected_rows() > 0) {
							$variables['hid'] = $data['lead_id'];
							
							//Push email id into Newsletters Table BOF
							$table = TABLE_NEWSLETTERS_MEMBERS;
							include ( DIR_FS_INCLUDES .'/newsletters-members.inc.php');					
							if(!empty($data['email'])){
								$domain1 = strtolower(strstr($data['email'], '@'));
								if( $domain1!= 'smeerptech.com' && 
									!NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email']) ) {
								  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
								  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email']."'" ;
								  $db->query($sql1);
								}
							}
							if(!empty($data['email_1'])){
								$domain2 = strtolower(strstr($data['email_1'], '@'));
								if( $domain2!='smeerptech.com' && 
								  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email_1']) ) {
								  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
								  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email_1']."'" ;
								  $db->query($sql1);
								}
							}
							if(!empty($data['email_2'])){
								$domain3 = strtolower(strstr($data['email_2'], '@'));
								if($domain3!= 'smeerptech.com' && 
								  !NewslettersMembers::duplicateFieldValue($db, $table, 'email', $data['email_2']) ) {
								  $sql1 = " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS."
								  SET ".TABLE_NEWSLETTERS_MEMBERS.".email ='".$data['email_2']."'" ;
								  $db->query($sql1);
								}
							}
							//Push email id into Newsletters Table EOF
							
							
							
							//Create Followup ticket bof
							$ticket_no  =  LeadsTicket::getNewNumber($db);
							
							$data['ticket_owner_uid'] = $variables['hid'];
							$data['name']= $data['f_name']." ".$data['l_name'] ;
							$data['ticket_owner'] = $data['company_name']." ";
							if(!empty($data['name'])){
								$data['ticket_owner'] = $data['company_name']." ".$data['name'];
							}
							$data['ticket_subject'] = 'Prospect Followup';
							$data['ticket_text'] = " New Prospect - ".$data['company_name']." ".$data['name'].", added by 
								". $my['f_name']." ". $my['l_name'];
								
							$data['hrs']='';
						    $data['min']=5;
						
							$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
							$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
							$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;							 
							$hrs1 = (int) ($data['hrs'] * 6);
							$min1 = (int) ($data['min'] * 6);  
							$followup_query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
							. TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_LD_TICKETS .".leads_followup    = '1', "
							. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
							. TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
							. TABLE_LD_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
							. TABLE_LD_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
							. TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
							. TABLE_LD_TICKETS .".ticket_status     = '".LeadsTicket::PENDINGWITHCLIENTS ."', "
							. TABLE_LD_TICKETS .".ticket_child      = '0', "
							. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_LD_TICKETS .".min = '". $data['min']."', "
							. TABLE_LD_TICKETS .".ticket_response_time = '0', "
							. TABLE_LD_TICKETS .".ticket_replied        = '0', "
							. TABLE_LD_TICKETS .".from_admin_panel        = '".LeadsTicket::ADMIN_PANEL."', "
							. TABLE_LD_TICKETS .".display_name           = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_LD_TICKETS .".display_user_id        = '". $my['uid'] ."', "
							. TABLE_LD_TICKETS .".display_designation    = '". $my['desig'] ."', "
							. TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
							. TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
							
							$db->query($followup_query) ;
							//Create Followup ticket eof	
							// Insert the address.
							for ( $i=0; $i<$data['address_count']; $i++ ) {
								$address_arr = array('address_type' => $data['address_type'][$i],
													'company_name'  => $data['company_name'][$i],
													'address'       => $data['address'][$i],
													'city'          => $data['city'][$i],
													'state'         => $data['state'][$i],
													'country'       => $data['country'][$i],
													'zipcode'       => $data['zipcode'][$i],
													'is_preferred'  => $data['is_preferred'][$i],
													'is_verified'   => $data['is_verified'][$i]
													);
								
								if ( !$region->update($variables['hid'], TABLE_SALE_LEADS, $address_arr) ) {
									foreach ( $region->getError() as $errors) {
										$messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
									}
								}
						}
						
						// Insert the Contact Numbers.
						for ( $i=0; $i<$data['phone_count']; $i++ ) {
							if ( !empty($data['p_number'][$i]) ) {
								$phone_arr = array( 'p_type'        => $data['p_type'][$i],
													'cc'            => $data['cc'][$i],
													'ac'            => $data['ac'][$i],
													'p_number'      => $data['p_number'][$i],
													'p_is_preferred'=> $data['p_is_preferred'][$i],
													'p_is_verified' => $data['p_is_verified'][$i]
													);
								if ( ! $phone->update($db, $variables['hid'], TABLE_SALE_LEADS, $phone_arr) ) {
									foreach ( $phone->getError() as $errors) {
										$messages->setErrorMessage('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $errors['description']);
									}
								}
							}
						}			
					}
					$_ALL_POST	= NULL;
					$data		= NULL;
				}    
			/*}else{
				if ( Leads::validateAdd($data, $extra) ) {
				
				for ( $i=0; $i<$data['phone_count']; $i++ ) {
                        if ( !empty($data['p_number'][$i]) ) {
							$query="SELECT * FROM ". TABLE_PHONE." WHERE cc='".$data['cc'][$i]."' AND ac='".$data['ac'][$i]."' AND 	                                                    p_number='".$data['p_number'][$i]."'";
								$db->query($query);
								if($db->nf()>0){
								include("existno.php");
								exit;
						}
                    }
                }
			}*/
            
                
                    
                    
                //to flush the data.
                
           /* }*/
		   $messages->setOkMessage("The New Lead has been added.");
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/sale-lead-list.php');
        }
        else {

           // $_ALL_POST['number'] = Leads::getNewAccNumber($db);
            if ( empty($data['country']) ) {
                $data['country'] = '91';
            }
            $lst_country    = $region->getCountryList($data['country']);
        
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $hidden[] = array('name'=> 'reminder_count', 'value' => '6');

            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'lst_country', 'value' => 'lst_country');
			$page["var"][] = array('variable' => 'my', 'value' => 'my');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>