<?php
    if ( $perm->has('nc_nav_status') ) {
        $id= isset($_GET["id"])? $_GET["id"] : ( isset($_POST["id"])? $_POST["id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
/*        if ( $perm->has('nc_uc_status_al') ) {
            $access_level += 1;
        }*/
        
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        Navigation::updateStatus($id, $status, $extra);
         $variables['hid'] = $ss_id;
        // Display the  list.
        include ( DIR_FS_NC .'/navigation-main-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>
