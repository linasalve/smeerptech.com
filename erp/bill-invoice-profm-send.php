<?php
 if ( $perm->has('nc_bl_invprfm_send') ) {
    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
    $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );

    $_ALL_POST	    = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
   
    /*$condition_query = " WHERE (". TABLE_BILL_INV_PROFORMA .".id = '". $inv_id ."' "
                            ." OR ". TABLE_BILL_INV_PROFORMA .".number = '". $inv_id ."')";*/
    $condition_query = " WHERE (". TABLE_BILL_INV_PROFORMA .".id = '". $inv_id ."')";

    $fields = TABLE_BILL_INV_PROFORMA .'.number'
                .','. TABLE_BILL_INV_PROFORMA .'.or_no'
                .','. TABLE_BILL_INV_PROFORMA .'.amount'
                .','. TABLE_BILL_INV_PROFORMA .'.currency_abbr'
                .','. TABLE_BILL_INV_PROFORMA .'.do_i'
                .','. TABLE_BILL_INV_PROFORMA .'.do_d'
                .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                .','. TABLE_CLIENTS .'.number AS c_number'
                .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                .','. TABLE_CLIENTS .'.billing_name'
                .','. TABLE_CLIENTS .'.email AS c_email';
                
    if ( InvoiceProforma::getDetails($db, $_ALL_Data, $fields, $condition_query) > 0 ) {
        $_ALL_Data =  $_ALL_Data['0'];
      
        //$_ALL_POST['to']= $_ALL_Data['c_email'];
        $inv_no =  $_ALL_Data['number'];
        $_ALL_Data['amount'] =  number_format($_ALL_Data['amount'],2);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            //print_r($_ALL_POST);    
            $data		= processUserData($_ALL_POST);   
            $extra = array( 'db' 		=> &$db,
                            'messages'          => &$messages
                          );
                          
            if(InvoiceProforma::validateSendInvoice($data, $extra)){
            
                $email = NULL;
                $cc_to_sender= $cc = $bcc = Null;
                
                $data['client'] = array(
                                    "f_name" => $_ALL_Data['c_f_name'],
                                    "l_name" => $_ALL_Data['c_l_name']
                                    );
                $data['number'] = $_ALL_Data['number'];
				$data['billing_name'] = $_ALL_Data['billing_name'];
                $data['amount'] = $_ALL_Data['amount'];
                $data['currency_name'] = $_ALL_Data['currency_name'];
                $data['do_i'] = $_ALL_Data['do_i'];
                $data['do_d'] = $_ALL_Data['do_d'];
                
                $temp = NULL;
                $temp_p = NULL;
                $condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $_ALL_Data['or_no'] ."'";
                Order::getParticulars($db, $temp, 'particulars,ss_title,ss_punch_line,ord_no', $condition_query);
                if(!empty($temp)){
                    foreach ( $temp as $pKey => $parti ) {
                        $temp_p[]=array(
                                        'particulars'=>$temp[$pKey]['particulars'],
                                        'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                        'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] 
                                        );
                    }
                }                    
                $data['particulars'] = $temp_p ;     
                $file_name = DIR_FS_INVPROFM_PDF_FILES ."/". $data["number"] .".pdf";
                
                if ( getParsedEmail($db, $s, 'PROFM_INVOICE_CREATE_TO_CLIENT', $data, $email) ) {        
                    $to = '';
                    $to[]   = array('name' => $_ALL_Data['c_f_name'] .' '. $_ALL_Data['c_f_name'], 'email' => $_ALL_POST['to']);
                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                    /*echo $email["body"];
                    echo $email["subject"];
                    print_r($to);
                    print_r($from);*/
                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                }
				if(!empty($smeerp_client_email)){
					$to     = '';
					$cc_to_sender=$cc=$bcc='';
					$to[]   = array('name' => $_ALL_Data['c_f_name'] .' '. $_ALL_Data['c_f_name'], 
					'email' => $smeerp_client_email);
					$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
					SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
					$cc_to_sender,$cc,$bcc,$file_name);
				}   
                $messages->setOkMessage("Proforma Invoice has been sent successfully on given email id.");  
				
                /*
                    $_ALL_POST['email']= $_ALL_POST['to'];
                 
                    $file_name = DIR_FS_INV_FILES ."/". $data["inv_no"] .".html";
                    $handle = fopen($file_name, "r");
                    $contents = fread($handle, filesize($file_name));
                    //To check 
                    //$contents = htmlspecialchars($contents) ;
                    fclose($handle);
                    $email = NULL;
                    if ( getParsedEmail($db, $s, 'BILL_INV_NEW_CLIENT', $data, $email) ) {
                      
                        $cc_to_sender= $cc = $bcc = Null;
                        $to     = '';
                        $email["isHTML"] = true;
                        $to[]   = array('name' => $_ALL_Data['c_f_name'] .' '. $_ALL_Data['c_f_name'], 'email' => $_ALL_POST['email']);
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        
                        //SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                        SendMail($to, $from, $reply_to, $email["subject"], $contents, $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                        $messages->setOkMessage("Invoice has been sent successfully on given email id.");  
                    }
                
               */
            }
                
        }
        
        $hidden[] = array('name'=> 'inv_id' ,'value' => $inv_id);
        $hidden[] = array('name'=> 'inv_no' ,'value' => $inv_no);
        $hidden[] = array('name'=> 'perform' ,'value' => 'send');
        $hidden[] = array('name'=> 'act' , 'value' => 'save');

        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        $page["var"][] = array('variable' => '_ALL_Data', 'value' => '_ALL_Data');
        $page["var"][] = array('variable' => 'inv_no', 'value' => 'inv_no');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-profm-send.html');
        
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
}else{
 
    $messages->setErrorMessage("You donot have the Right to Send Invoice.");
}
?>