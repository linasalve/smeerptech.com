<?php

    if ( $perm->has('nc_cat_add') ) {

        $_ALL_POST	= NULL;
        $data       = NULL;
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);

            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages
                        );
            
            if ( Categories::validateAdd($data, $extra) ) {
                $query	= " INSERT INTO ". TABLE_SETTINGS_CATEGORY
                        ." SET ". TABLE_SETTINGS_CATEGORY .".sc_title     	= '". $data['categorytitle'] ."'"
                            .",". TABLE_SETTINGS_CATEGORY .".sc_parent_id   = '". $data['category'] ."'"
							.",". TABLE_SETTINGS_CATEGORY .".sc_description = '". $data['categorydetails'] ."'"
                            .",". TABLE_SETTINGS_CATEGORY .".sc_status    	= '". $data['status'] ."'"
                            .",". TABLE_SETTINGS_CATEGORY .".sc_date   		= '". date('Y-m-d') ."'";
                if ($db->query($query) && $db->affected_rows() > 0) 
					$messages->setOkMessage("The New Category has been added.");
				else
					$messages->setErrorMessage('Problem saving record. Try later');
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
		
		//get the categories
        $output = "";
		Categories::getComboMenus();

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/category.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/category.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/category.php?added=1");   
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            $hidden[] = array('name'=> 'perform','value' => 'add');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
    
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
			$page["var"][] = array('variable' => 'categlist', 'value' => 'output');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'category-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>