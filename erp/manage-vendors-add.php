<?php
    if ( $perm->has('nc_vendor_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		
        
		// Include the vendor class.
		include_once (DIR_FS_INCLUDES .'/vendors.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Vendors::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_VENDOR
                            ." SET ". TABLE_VENDOR .".f_name = '".                 $data['f_name'] ."'"
                                .",". TABLE_VENDOR .".l_name = '".              $data['l_name'] ."'"
                                .",". TABLE_VENDOR .".access_level = '".         $my['access_level'] ."'"
                                .",". TABLE_VENDOR .".created_by = '".           $data['created_by'] ."'"
                                .",". TABLE_VENDOR .".address = '".              $data['address'] ."'"
								.",". TABLE_VENDOR .".status = '".              $data['status'] ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Address entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/manage-vendors-list.php');
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'manage-vendors-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>