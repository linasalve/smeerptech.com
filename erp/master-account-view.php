<?php

    if ( $perm->has('nc_sms_ma_view') ) {
		$mah_maid = isset($_GET['ma_id']) ? $_GET['ma_id'] : ( isset($_POST['ma_id'] ) ? $_POST['ma_id'] :'');
		if(empty($condition_query))
			$condition_query = " WHERE mah_maid ='".$mah_maid."' ";
		else
			$condition_query = " AND mah_maid ='".$mah_maid."' ";

        //$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	MasterAccount::viewList( $db, $list, 'mah_maid', $condition_query);
    	$variables['hid'] = $mah_maid;
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        MasterAccount::viewList( $db, $list, '*', $condition_query, $next_record, $rpp);

        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'master-account-view.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>