<?php

    if ( $perm->has('nc_pr_details') ) {
        $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        $access_level += 1;
         

        if ( Prospects::getList($db, $list, '*', " WHERE user_id = '$user_id'") > 0 ) {
            $_ALL_POST = $list['0'];
       
            if ( $_ALL_POST['access_level'] < $access_level ) {
                include_once ( DIR_FS_CLASS .'/RegionProspects.class.php');
                include_once ( DIR_FS_CLASS .'/PhoneProspects.class.php');
                include_once ( DIR_FS_CLASS .'/ReminderProspects.class.php');

                $region         = new RegionProspects();
                $phone          = new PhoneProspects(TABLE_PROSPECTS);
                $reminder       = new ReminderProspects(TABLE_PROSPECTS);

                //$_ALL_POST['manager'] = Prospects::getManager($db, '', $_ALL_POST['manager']);
                $_ALL_POST['industry']    = explode(",", $_ALL_POST['industry']);
                //BOF read the available industries
                $industry_list = NULL;
                $required_fields ='*';
                $condition_query="WHERE status='".Industry::ACTIVE."'";
                Industry::getList($db,$industry_list,$required_fields,$condition_query);
                //EOF read the available industries
                
               
                $_ALL_POST['team_list'] = NULL;
                $required_fields = "f_name,l_name,number";
                $_ALL_POST['team']= str_replace(',',"','",$_ALL_POST['team']);
                $condition_query="WHERE user_id IN('".$_ALL_POST['team']."')";
                User::getList($db,$_ALL_POST['team_list'],$required_fields,$condition_query);               
                // Read the available Access Level and User Roles.
				
                $_ALL_POST['access_level']  = Prospects::getAccessLevel($db, $access_level, $_ALL_POST['access_level']);
                $_ALL_POST['roles']         = Prospects::getRoles($db, $access_level, $_ALL_POST['roles']);
                
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_PROSPECTS, $user_id);
                $_ALL_POST['phone'] = $phone->get($db);
                
                // Read the Addresses.
                $region->setAddressOf(TABLE_PROSPECTS, $user_id);
                $_ALL_POST['address_list'] = $region->get();
                
                // Read the Reminders.
                $reminder->setReminderOf(TABLE_PROSPECTS, $user_id);
                $_ALL_POST['reminder_list'] = $reminder->get($db);
                
                //print_r($_ALL_POST);
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                $page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
    
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Executives with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("The Executives details were not found. ");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>