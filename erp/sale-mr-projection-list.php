<?php

    if ( $perm->has('nc_sl_mr_pr_list') ) {
		
	
        // If the entry is created by the my.
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_sl_mr_pr_list_al') ) {
            $access_level += 1;
        }
        if ( $where_added ) {
            $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= "  ( ";

        // If my has created this entry.
        $condition_query .= " ( ". TABLE_SALE_MR_PROJECTION .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_SALE_MR_PROJECTION .".access_level < $access_level ) ";
       
        
         // Check if the User has the Right to view Euipment created by other Users.
        if ( $perm->has('nc_sl_mr_pr_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_sl_mr_pr_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_SALE_MR_PROJECTION. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_SALE_MR_PROJECTION .".access_level < $access_level_o ) ";
        }
        $condition_query .= ")";
        
        if(empty($condition_query)){
            $condition_query .=" WHERE " .TABLE_SALE_MR_PROJECTION .".user_id='".$my['user_id']."'";
        }else{
            $condition_query .=" AND " .TABLE_SALE_MR_PROJECTION .".user_id='".$my['user_id']."'";
        }
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        
        //echo $condition_query;
        // To count total records.
        $list	= 	NULL;
        $total	=	SaleMrProjection::getDetails( $db, $list, '', $condition_query);
    
        //$pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $condition_url .="&perform=".$perform;
        
        $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
        
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields = TABLE_SALE_MR_PROJECTION .'.*,'.TABLE_USER.'.f_name,'.TABLE_USER.'.l_name';
        SaleMrProjection::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
     
       
        if ( $perm->has('nc_sl_mr_pr_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_sl_mr_pr_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_sl_mr_pr_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_sl_mr_pr_delete') ) {
            $variables['can_delete'] = true;
        }
       
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-mr-projection-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>
