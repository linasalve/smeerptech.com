<?php

    if ( $perm->has('nc_nwlm_list') ) {        
       
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
         //By default search in On
        $_SEARCH["searched"]    = true ;
        // To count total records.
        $list	= 	NULL;
        $pagination   = '';
        $extra_url  = '';
		
        if($searchStr==1){
            $total	=	NewslettersMembers::getList( $db, $list, '', $condition_query);
            if ( isset($condition_url) && !empty($condition_url) ) {
                $extra_url  = $condition_url;
            }
            $condition_url .="&perform=".$perform;            
            $pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');           
            $extra_url  .= "&x=$x&rpp=$rpp";
            $extra_url  = '&start=url'. $extra_url .'&end=url';        
            $list	= NULL;
            NewslettersMembers::getList( $db, $list, '*', $condition_query, $next_record, $rpp);
			
        }
        
		
		$bounceRecord=0;
		$unsubscribeRecord=0;
		$condition_query1 =" WHERE ".TABLE_NEWSLETTERS_MEMBERS.".email_bounce_temp_status='1' LIMIT 0,1";
		$bounceRecord	=	NewslettersMembers::getList( $db, $arr, '', $condition_query1);
		
		$condition_query2 =" WHERE ".TABLE_NEWSLETTERS_MEMBERS.".unsubscribed_temp_status='1' LIMIT 0,1";
		$unsubscribeRecord	=	NewslettersMembers::getList( $db, $arr, '', $condition_query2);
		
		
		
		$fields1= "id,category";
		$condition_query1 = " WHERE ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY.".status='1' ";
		NewslettersMembers::getCategory($db, $category_list,$fields1, $condition_query1);
        
		
        // Set the Permissions.
        $variables['can_add']     = false;
        $variables['can_view_list']     = false;
        if ( $perm->has('nc_nwlm_list') ) {
			$variables['can_view_list']     = true;
		}
		if ( $perm->has('nc_nwlm_csv_upload') ) {
			$variables['can_add']     = true;
		}
		
       
        //$variables['searchLink']=$searchLink ;
        $page["var"][] = array('variable' => 'list', 'value' => 'list');
        $page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
		$page["var"][] = array('variable' => 'bounceRecord', 'value' => 'bounceRecord');
        $page["var"][] = array('variable' => 'unsubscribeRecord', 'value' => 'unsubscribeRecord');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletters-members-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>