<?php
if ( $perm->has('nc_pba_edit') ) { 
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        include_once (DIR_FS_INCLUDES .'/user.inc.php');	
        include_once ( DIR_FS_INCLUDES .'/clients.inc.php' );
        
      
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
                            
            $extra = array( 'db'                => &$db,
                                'access_level'      => $access_level,
                                'messages'          => &$messages
                            );
              
            if ( PaymentBillsAgainst::validateUpdate($data, $extra) ) {
                
                 $query  = " UPDATE ".TABLE_PAYMENT_BILLS_AGAINST
						 ." SET ".TABLE_PAYMENT_BILLS_AGAINST .".bill_against = '". $data['bill_against'] ."'"                                     
                            .",". TABLE_PAYMENT_BILLS_AGAINST .".vendor_id = '". $data['vendor_id'] ."'"
                            .",". TABLE_PAYMENT_BILLS_AGAINST .".status = '". 		$data['status'] ."'"            
                        ." WHERE id = '". $id ."'";
                    
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_PAYMENT_BILLS_AGAINST .'.*'  ;            
            $condition_query = " WHERE (". TABLE_PAYMENT_BILLS_AGAINST .".id = '". $id ."' )";
            
            if ( PaymentBillsAgainst::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                    $_ALL_POST = $_ALL_POST['0'];
                   
                    // Setup the date of delivery.
                    
                  
                    $db1 		= new db_local; // database handle                     
                    if(!empty($_ALL_POST['vendor_id'])){
                         Clients::getList($db1, $party_details, 'user_id,number,f_name,l_name', "WHERE user_id IN ('".$_ALL_POST['vendor_id']."')");
						$party_details1 = $party_details[0];                        
                        $_ALL_POST['vendor_details'] = $party_details1['f_name'].' '.$party_details1['l_name'].' ('. $party_details1['number'] .')'; 
                    }                         
                   $id         = $_ALL_POST['id'];
                        
            }else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
             }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $perform='list';
            $condition_query='';
            include ( DIR_FS_NC .'/payment-bills-against-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
      
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            //$page["var"][] = array('variable' => 'party', 'value' => 'party');     
            //$page["var"][] = array('variable' => 'executivelist', 'value' => 'executivelist');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-bills-against-edit.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
