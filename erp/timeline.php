<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
				   
    include_once ( DIR_FS_NC ."/header.php");
	include_once ( DIR_FS_INCLUDES .'/user.inc.php');
	$user_id    = isset($_GET["user_id"])   ? $_GET["user_id"]  : ( isset($_POST["user_id"])    ? $_POST["user_id"] : $my['uid'] );
	$date = date('Y-m-d');
	$condition_query= " WHERE user_id = '". $user_id ."' "; 
    User::getList($db, $_ALL_POST, 'f_name, l_name, number ', $condition_query);
	$_ALL_POST=$_ALL_POST[0];
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<title>Timeline of <?php echo $_ALL_POST['f_name']." ".$_ALL_POST['l_name']." (".$_ALL_POST['number'].")" ; ?></title> 
	<link rel="stylesheet" href="../lib/scripts/jq-ui/css/timeline-style.css">  
	
	<link rel="stylesheet" href="../lib/scripts/jq-ui/css/jquery.ui.all.css"> 
	<!--<link rel="stylesheet" href="../lib/scripts/jq-ui/css/jGrowl.css"> -->
	<script language="javascript" src="../lib/scripts/jq-ui/jquery-1.5.1.js"></script> 
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.bgiframe-2.1.2.js"></script> 
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.core.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.widget.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.mouse.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.button.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.draggable.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.position.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.resizable.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.dialog.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.effects.core.js"></script>
	<script language="javascript" src="../lib/scripts/jq-ui/jquery.ui.datepicker.js"></script> 
	<script type="text/javascript" src="../lib/scripts/autosuggest.js"></script> 
	<!--<script type="text/javascript" src="../lib/scripts/jq-ui/jGrowl.js"></script>  -->
	<script language="javascript" src="../lib/scripts/common-functions.js"></script>  
	<script type="text/javascript">
		 
		//Function to update the Banner status using AJAX BOF
		var http_req;
		function updateTimeline(){
		 
			http_req = false;	
			date1 = document.getElementById("date").value ;
			if (window.XMLHttpRequest) { // Mozilla, Safari,...
				http_req = new XMLHttpRequest();
			}else if (window.ActiveXObject) { // IE
				try {
					http_req = new ActiveXObject("Msxml2.XMLHTTP");
				}catch (e) {
					try {
						http_req = new ActiveXObject("Microsoft.XMLHTTP");
					}catch (e) {}
				}
			}    
			if (!http_req) {
				return false;
			} 
		   
			var uri = "timeline-loadmore.php?date="+date1;    
			
			http_req.onreadystatechange = loadTimeline;
			http_req.open('GET', uri, true);
			http_req.send(null);
		 
		 
		}
		function loadTimeline(){
			  $('div#loadmoreajaxloader').show();
			if(http_req.readyState==4){

				if (http_req.status == 200){
					var allData = http_req.responseText;
				 
					if(allData){            
					
						var dataSplit =new Array();
						dataSplit = allData.split("~");
						var dataMsg = dataSplit[0];
						document.getElementById('date').value = dataSplit[1];
						$("#updates").append(dataMsg);							 
						$('div#loadmoreajaxloader').hide(); 
					}
				}
			} 
		}
		
	</script> 
</head>
<body>


<div id='container'>
		<div id="updates">
			<input type="hidden" name="date" id="date" value="<?php echo $date; ?>"/>
			<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ; ?>"/>
			
			
			<!--<div class="stbody">
				<span class="timeline_square color3"></span>
				<div class="stimg">
					<img src="https://lh6.googleusercontent.com/-J35XS0z7KiU/U6w9exoq9oI/AAAAAAAAKtw/ooEJyCHC28A/h120/13743006356.jpg">
				</div>
				<div class="sttext"><span title="Delete" class="stdelete"></span><b>Arun</b><br>Everything is possible. Everything is possible.Everything is possible.Everything is possible.Everything is possible.Everything is possible.
					<div class="sttime">18 seconds ago</div>
					<div class="stexpand"></div>
				</div>
			</div>-->
			<?php 
			 echo "<div class=\"sttitle\">
					<span class=\"timeline_square color4\"></span> 
					<span class=\"\"> <h3>".$my['f_name']." ".$my['l_name']. "( ".$my['number']." )</h3> </span>
				</div>"; 
			echo "<div class=\"stdate\">
					<span class=\"timeline_square color1\"></span> 
					<span class=\"\"> ".date('d M Y',strtotime($date))." </span>
				</div>"; 
			echo "<div class=\"sttitle\">
					<span class=\"timeline_square color3\"></span> 
					<span class=\"\">Scores on date ".date('d M Y',strtotime($date))." </span>
				</div>";
				
			$query = "SELECT * FROM ".TABLE_SCORE_SHEET." WHERE (".TABLE_SCORE_SHEET.".comment_to LIKE '%,".$userid.",%') AND 
			date_format(".TABLE_SCORE_SHEET.".date, '%Y-%m-%d') ='".$date."'";
			$db->query($query);  
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ){ 
					$particulars = $db->f('particulars'); 
					$id = $db->f('id'); 
					$created_by_name = $db->f('created_by_name'); 
					/*echo '<div class="postitem" id="'.$date.'">Post no '.$id.': '.$particulars.'</div>'; */
					
					echo "<div class=\"stbody\">
							<span class=\"timeline_square color2\"></span>
							<div class=\"stimg\"><img src=''></div>
							<div class=\"sttext\"><span title=\"Delete\" class=\"stdelete\"></span><b>".$created_by_name."</b><br>".nl2br($particulars)."
								<div class=\"sttime\"></div>
								<div class=\"stexpand\"></div>
							</div>
						  </div>";
					
					
					
				}
			}
			include_once ( DIR_FS_INCLUDES .'/task-reminder.inc.php');
			
			echo "<div class=\"sttitle\">
					<span class=\"timeline_square color2\"></span> 
					<span class=\"\">Tasks posted on date ".date('d M Y',strtotime($date))." </span>
				  </div>";
			 $sqlTask1 = " SELECT DISTINCT(task_no), task,created_by_name FROM ".TABLE_TASK_REMINDER ."   
                         WHERE   ( "
                                . TABLE_TASK_REMINDER .".allotted_to LIKE '%,". $user_id .",%'  " 
                            . " ) AND ".TABLE_TASK_REMINDER.".status IN('".Taskreminder::PENDING."') AND
						date_format(".TABLE_TASK_REMINDER.".do_e, '%Y-%m-%d') ='".$date."'";
							
			$db->query($sqlTask1);  
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ){ 
					$task = $db->f('task'); 
					$task_no = $db->f('task_no'); 
					$created_by_name = $db->f('created_by_name'); 
					
					echo "<div class=\"stbody\">
							<span class=\"timeline_square color2\"></span>
							<div class=\"stimg\"><img src=''></div>
							<div class=\"sttext\"><span title=\"Delete\" class=\"stdelete\"></span><b>".$created_by_name."</b><br> ".nl2br($task)."
								<div class=\"sttime\">task no. ".$task_no."</div>
								<div class=\"stexpand\"></div>
							</div>
						</div>";
				}
			}
			
			
			echo "<div class=\"sttitle\">
					<span class=\"timeline_square color2\"></span> 
					<span class=\"\">Tasks deadline on date ".date('d M Y',strtotime($date))." </span>
				  </div>";			
			$sqlTask1 = " SELECT DISTINCT(task_no), task,created_by_name FROM ".TABLE_TASK_REMINDER ."   
                         WHERE   ( "
                               . TABLE_TASK_REMINDER .".allotted_to LIKE '%,". $user_id .",%'  " 
                            . " ) AND ".TABLE_TASK_REMINDER.".status IN('".Taskreminder::PENDING."') AND
						date_format(".TABLE_TASK_REMINDER.".do_r, '%Y-%m-%d') ='".$date."'";
							
			$db->query($sqlTask1);  
			if ( $db->nf()>0 ) {
				while ( $db->next_record() ){ 
					$task = $db->f('task'); 
					$task_no = $db->f('task_no'); 
					$created_by_name = $db->f('created_by_name'); 
					
					echo "<div class=\"stbody\">
							<span class=\"timeline_square color2\"></span>
							<div class=\"stimg\"><img src=''></div>
							<div class=\"sttext\"><span title=\"Delete\" class=\"stdelete\"></span><b>".$created_by_name."</b><br> ".nl2br($task)."
								<div class=\"sttime\">task no. ".$task_no."</div>
								<div class=\"stexpand\"></div>
							</div>
						</div>";
				}
			}
			 
			?>
		</div>
		<div><center><a href="javascript:updateTimeline();">loadmore</div>
		<div id="loadmoreajaxloader" style="display:none;"><center><img src="ajax-loader.gif" /></center></div>
		 
		<!-- <div id="updates">  
			<input type="hidden" name="date" id="date" value="<?php echo date('Y-m-d'); ?>"/>
			<div class="stbody"> dsfsd fdsfd sfdssdfdss sdfsdfs fsd
			</div>
			<div class="stbody">
				<span class="timeline_square color5"></span>
				<div class="stimg">
					<img src="https://lh5.googleusercontent.com/-la25iL9hGxc/UweXYfFTLiI/AAAAAAAAJDI/83w9Z-2P3AU/s566-no/2014-02-21+16.10.37.jpg">
				</div>
				<div class="sttext">
					<span title="Delete" class="stdelete"></span><b>Srinivas</b><br>9lessons Programming Blog <a rel="nofollow" target="_blank" href="http://www.9lessons.info">http://www.9lessons.info</a> . <div class="sttime">16 seconds ago</div>
					<div class="stexpand"></div>
				</div>
			</div>
			<div class="stbody">
				<span class="timeline_square color3"></span>
				<div class="stimg">
					<img src="https://lh6.googleusercontent.com/-J35XS0z7KiU/U6w9exoq9oI/AAAAAAAAKtw/ooEJyCHC28A/h120/13743006356.jpg">
				</div>
				<div class="sttext"><span title="Delete" class="stdelete"></span><b>Arun</b><br>Everything is possible. <div class="sttime">18 seconds ago</div><div class="stexpand"></div>
				</div>
			</div>
			< !--
			<div class="stbody">
				<span class="timeline_square color1"></span>
				<div class="stimg">
					<img src="https://lh5.googleusercontent.com/-EzIZSBu8K8Y/U6w9ua2nRaI/AAAAAAAAKuM/ZOvtXqPoBSo/s400/137432094211.png">
				</div>
				<div class="sttext"><span title="Delete" class="stdelete">X</span><b>Joker</b><br>If you are good at something, never do it for free
					<div class="sttime">18 seconds ago</div>
					<div class="stexpand"><iframe width="560" height="315" frameborder="0" allowfullscreen="" src="//www.youtube.com/embed/FalHdi2DkEg"></iframe></div>
				</div>
			</div> -- >
			<div class="stbody">
				<span class="timeline_square color5"></span>
				<div class="stimg"><img src="https://lh4.googleusercontent.com/-u7t4ULGdO0E/U6w9e1H4DUI/AAAAAAAAKt0/07cA9G_rzBM/s231/137432130613.png"></div>
				<div class="sttext"><span title="Delete" class="stdelete"></span><b>Superman</b><br>With great power comes great responsibility.<div class="sttime">10 days ago</div><div class="stexpand"></div></div>
			</div> 
		</div> -->
	</div>

</body>
</html>