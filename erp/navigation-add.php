<?php
  if ( $perm->has('nc_nav_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        // $menu = NULL;
		//Navigation::getParent($db,$menu);
		
		include_once (DIR_FS_INCLUDES .'/navigation.inc.php');	
        	
		 
 		$menu=array(); 
        $menu = Navigation::getComboMenus();
		 
			
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
		$_ALL_POST 	= $_POST;
		$data		= processUserData($_ALL_POST);
                       
		$extra = array( 'db' 				=> &$db,
						'access_level'      => $access_level,
						'messages'          => &$messages
		);
        
                if ( Navigation::validateSubAdd($data, $extra) ) { 
				$query	= " INSERT INTO ".TABLE_NAVIGATION
						." SET ".TABLE_NAVIGATION .".parent_id = '". $data['parent_id'] ."'"                                     
						.",". TABLE_NAVIGATION .".title = '".         $data['title'] ."'"
						.",". TABLE_NAVIGATION .".page = '".         $data['page1'] ."'"
						.",". TABLE_NAVIGATION .".image = '".         $data['image'] ."'"
						.",". TABLE_NAVIGATION .".level 	 = '".         $data['level'] ."'"
						.",". TABLE_NAVIGATION .".selected_page 	 = '".         $data['selected_page'] ."'"
						.",". TABLE_NAVIGATION .".sequence 	 = '".         $data['sequence'] ."'"
						.",". TABLE_NAVIGATION .".perm_name 	 = '".         $data['perm_name'] ."'"
						.",". TABLE_NAVIGATION .".main_perm_name 	 = '".         $data['main_perm_name'] ."'"
						.",". TABLE_NAVIGATION .".access_level = '".         $my['access_level'] ."'"
						.",". TABLE_NAVIGATION .".status = '". 		$data['status'] ."'"       
						.",". TABLE_NAVIGATION .".ajax = '". 		$data['ajax'] ."'"        
						.",". TABLE_NAVIGATION .".ip     = '". 		$_SERVER['REMOTE_ADDR'] ."'"                                
						.",". TABLE_NAVIGATION .".created_by = '".           $my['user_id'] ."'"
						.",". TABLE_NAVIGATION .".do_e   = '". 		date('Y-m-d H:i:s')."'";
                
				if ( $db->query($query) && $db->affected_rows() > 0 ) {
					$messages->setOkMessage("Record has been added successfully.");
					$variables['hid'] = $db->last_inserted_id();              
				}
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
		}
        }               
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/navigation.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/navigation.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/navigation.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'menu', 'value' => 'menu');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'navigation-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");        
    }
?>
