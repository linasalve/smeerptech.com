<?php
    if ( $perm->has('nc_vbk_su_up_mst') ) {
        $user_id= isset($_GET["user_id"])? $_GET["user_id"] : ( isset($_POST["user_id"])? $_POST["user_id"]: '' );
        $send_mail	= isset($_GET["send_mail"]) ? $_GET["send_mail"] 	: ( isset($_POST["send_mail"]) ? $_POST["send_mail"] : '' );
        $access_level = $my['access_level'];
       
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        VendorsBank::updateMailStatus($user_id, $send_mail, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/vendors-bank-su-list.php');
		
    } else {
        $messages->setErrorMessage("You donot have the Right to Change the Mail Status.");
    }
?> 
 
