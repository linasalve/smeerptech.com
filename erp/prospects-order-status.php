<?php
	if ( $perm->has('nc_ps_or_status') ) {
        $or_id	= isset($_GET["or_id"])  ? $_GET["or_id"] 	: ( isset($_POST["or_id"]) 	? $_POST["or_id"] : '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        $access_level += 1;
  
        
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                    );
        ProspectsOrder::updateStatus($or_id, $status, $extra);
        
        
        
        $perform = 'search';
        // Display the  list.
        include ( DIR_FS_NC .'/prospects-order-search.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>