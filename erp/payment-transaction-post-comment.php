<?php
    if ( $perm->has(EDIT) ) {
    
         $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*if ( $perm->has('nc_ts_edit_al') ) {
            $access_level += 1;
        }*/
	    // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null ;
        User::getMemberList($db,$lst_executive);
        // BO:  Read the Team members list ie member list in dropdown box  EOF 
        
        // Read the record which is to be Updated.
        $fields =TABLE_PAYMENT_TRANSACTION .'.*'  ;            
        $condition_query = " WHERE (".TABLE_PAYMENT_TRANSACTION .".id = '". $id ."' )";
        $where_added =1;
        if($CHECK_PERMISSION){
        
            if($where_added){
                $condition_query .= " AND  ";
            }else{
               $condition_query .=" WHERE  ";
                $where_added=true;
            }
            $condition_query .= "  ( ";
            // If my has created this .                                
            $condition_query .= " ( ".TABLE_PAYMENT_TRANSACTION .".created_by = '". $my['user_id'] ."' ) ";
            
            $condition_query .= " )";
        }
       
        if ( Paymenttransaction::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];
            $id         = $_ALL_POST['id'];
            $_ALL_POST['do_r']  = explode(' ', $_ALL_POST['do_r']);
            $temp               = explode('-', $_ALL_POST['do_r'][0]);
            $_ALL_POST['do_r']  = NULL;
            $_ALL_POST['do_r']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
            $_ALL_POST['allotted_to'] = explode(',', $_ALL_POST['allotted_to']);
            
        }
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            
            $data       = processUserData($_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( Paymenttransaction::validateCommentAdd($data, $extra) ) {
                //Send mail to selected staff BOF
                $to_idArr = $data['to_id'];
              
                if(!empty($data['to_id'])){
                    $to_idStr =implode(',',$data['to_id']) ;                  
                    $query = " SELECT * FROM ".TABLE_AUTH_USER." WHERE ".TABLE_AUTH_USER.".user_id IN ( ".$to_idStr." ) AND ".TABLE_AUTH_USER.".status ='1' ";
                    $db->query( $query );
                   
                    if($db->nf() >0 ) {          
                        while( $db->next_record() )
                        {
                                            
                          $to[] = array("name" 	=> $db->f('first_name')." ".$db->f('last_name'),
                                          "email" => $db->f('user_email')
                                           );
                        }
                    }
                    
                    $cc = NULL ;
                    $bcc = NULL ;
                    
                    // EOF get email-id ,cc & bcc 
                    $name = $my["first_name"]." ".$my["last_name"] ;
                    $from["name"] 	= $name ;
                    $from["email"] 	= $my["user_email"] ;
                    
                    $is_html 	= true;
                    $sendCopy	= false;
                    $message =  $data['pcomment'];
                    $subject = " Payment Reminder :: Comments from ".$name." \n\r";
                    SendMail($to, $from, $from, $subject, $message, $is_html, $sendCopy, $cc, $bcc);
                }
                //Send mail to selected staff EOF
                
                 $query	= " INSERT INTO ".TABLE_PAYMENT_TRANSACTION_LOG
                        ." SET ".TABLE_PAYMENT_TRANSACTION_LOG .".task_id = '".              $id ."'"
                            .",".TABLE_PAYMENT_TRANSACTION_LOG .".created_by = '".           $my['user_id'] ."'"
                            .",".TABLE_PAYMENT_TRANSACTION_LOG .".to_id = '".                implode(',',$data['to_id']) ."'"
                            //.",".TABLE_PAYMENT_REMINDER_LOG .".to_id = '".              $data['to_id'] ."'"
                            .",".TABLE_PAYMENT_TRANSACTION_LOG .".comment = '".              $data['pcomment'] ."'"
                            .",".TABLE_PAYMENT_TRANSACTION_LOG .".do_a = '".                 date('Y-m-d h:i:s') ."'";
               
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Comment has been added successfully.");
                    $variables['hid'] = $id ;
                }
                
                
                $data       = NULL;
                
            }
        }
        
        // get list of all comments of change log BOF
        $condition_query1 = $condition_url1 = '';
        
        $listLog	= 	NULL;
        //$condition_url .= '&perform=edit&id='.$id;
        $condition_url .= '&perform=list';
        $fields =TABLE_PAYMENT_TRANSACTION_LOG.".id " ;
        $condition_query1 = " WHERE ".TABLE_PAYMENT_TRANSACTION_LOG.".task_id = '".$id."' " ;
        $condition_query1 .= " ORDER BY ".TABLE_PAYMENT_TRANSACTION_LOG.".do_a DESC";
        $total	=	Paymenttransaction::getDetailsCommLog( $db, $listLog, $fields , $condition_query1);
        $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
        //$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url1, 'changePageWithExtra');
        //$extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        $listLog	= NULL;
        $fields =TABLE_PAYMENT_TRANSACTION_LOG .'.*';   
        Paymenttransaction::getDetailsCommLog( $db, $listLog, $fields, $condition_query1, $next_record, $rpp);
      
        $fList2  = array() ;
        if(!empty($listLog)){
            foreach( $listLog as $key2=>$val2){      
             
               $executive=null;
               $string = str_replace(",","','", $val2['to_id']);
               $fields1 =  TABLE_AUTH_USER .'.first_name'.','. TABLE_AUTH_USER .'.last_name';
               $condition1 = " WHERE ".TABLE_AUTH_USER .".user_id IN('".$string."') " ;
               User::getList($db,$executive,$fields1,$condition1);
              
               $executivename='';
               foreach($executive as $key1=>$val1){
                    $executivename .= $val1['first_name']." ".$val1['last_name']."\n"."," ;
               } 
               $val2["to_id"] = trim($executivename,",") ; 
               $created_name =$fields1=$condition1='';
               $created_by=null;
               $fields1 =  TABLE_AUTH_USER .'.first_name'.','. TABLE_AUTH_USER .'.last_name';
               $condition1 = " WHERE ".TABLE_AUTH_USER .".user_id IN('".$val2["created_by"]."') " ;
               User::getList($db,$created_by,$fields1,$condition1);
               
               foreach($created_by as $k=>$v){
                    $created_name  = $v['first_name']." ".$v['last_name'].",\n" ;
               } 
               $val2["created_by"] = $created_name ;
               $fList2[$key2]=$val2;
            }
        }
       
       // print_r($fList2);
        
        // get list of all comments of change log EOF
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
           
            //$condition_query='';
            include ( DIR_FS_ADMIN .'/payment-transaction-list.php');
        }else{
       
            
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'pcomm');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
               
            
            $page["var"][] = array('variable' => 'fList', 'value' => 'fList2');
            $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');      
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'payment-transaction-post-comment.html');                   
       }
        
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-edit-permission.html');
    }
?>
