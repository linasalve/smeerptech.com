<?php
    if ( $perm->has('nc_bl_inv_details') ) {
		$inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );

        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        if ( $perm->has('nc_bl_inv_details_al') ) {
            $access_level += 1;
        }

        $condition_query = " WHERE (". TABLE_BILL_INV .".id = '". $inv_id ."' "
                                ." OR ". TABLE_BILL_INV .".number = '". $inv_id ."')";

        $condition_query .= " AND ( ";

		// If my has created this Invoice.
		$condition_query .= " (". TABLE_BILL_INV .".created_by = '". $my['user_id'] ."' "
							." AND ". TABLE_BILL_INV .".access_level < $access_level ) ";
		
		// If my is the Client Manager (Remove if Client manager cannot edit the Order)
		$condition_query .= " OR (". TABLE_CLIENTS .".manager = '". $my['user_id'] ."'"
							." AND ". TABLE_BILL_INV .".access_level < $access_level ) ";

        
		// Check if the User has the Right to Edit Invoices created by other Users.
		if ( $perm->has('nc_bl_inv_edit_ot') ) {
			$access_level_o   = $my['access_level'];
			if ( $perm->has('nc_bl_inv_edit_ot_al') ) {
				$access_level_o += 1;
			}
			$condition_query .= " OR ( ". TABLE_BILL_INV. ".created_by != '". $my['user_id'] ."' "
								." AND ". TABLE_BILL_INV .".access_level < $access_level_o ) ";
		}
		$condition_query .= " )";
			
		$fields = TABLE_BILL_INV .'.*'
				.','. TABLE_BILL_ORDERS .'.followup_status_remarks'
				.','. TABLE_CLIENTS .'.user_id AS c_user_id'
				.','. TABLE_CLIENTS .'.manager AS c_manager'
				.','. TABLE_CLIENTS .'.number AS c_number'
				.','. TABLE_CLIENTS .'.f_name AS c_f_name'
				.','. TABLE_CLIENTS .'.l_name AS c_l_name'
				.','. TABLE_CLIENTS .'.email AS c_email'
				.','. TABLE_CLIENTS .'.status AS c_status';
					
        if ( Invoice::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];

            if ( $_ALL_POST['access_level'] < $access_level ) {
				// Set up the Client Details field.
				$_ALL_POST['client_details']= $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
												.' ('. $_ALL_POST['c_number'] .')'
												.' ('. $_ALL_POST['c_email'] .')';
												
				//include ( DIR_FS_INCLUDES .'/user.inc.php');
                // Read the Order Creator's Information.
                $_ALL_POST['creator']= '';
                User::getList($db, $_ALL_POST['creator'], 'user_id,username,number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                $_ALL_POST['creator'] = $_ALL_POST['creator'][0];
				
                // Read the Client Managers Information.
                $_ALL_POST['manager']= '';
                User::getList($db, $_ALL_POST['manager'], 'user_id,username,number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['c_manager'] ."'");
                $_ALL_POST['manager'] = $_ALL_POST['manager'][0];
				
				// Set up the Services.
				include_once ( DIR_FS_INCLUDES .'/services.inc.php' );
					
                $condition_query= " LEFT JOIN ".TABLE_BILL_ORD_P." ON ".TABLE_BILL_ORD_P.".s_id =".TABLE_SETTINGS_SERVICES.".ss_id 
                                    WHERE ".TABLE_BILL_ORD_P.".ord_no='".$_ALL_POST['or_no']."' ORDER BY ss_title ASC";
                $fields = TABLE_SETTINGS_SERVICES.'.ss_id,'.TABLE_SETTINGS_SERVICES.'.ss_title' ;
				Services::getList($db, $_ALL_POST['services'],  $fields, $condition_query);
			
				$condition_query = '';
				
                // Read the Particulars.
				$temp = NULL;
				//$condition_query = " WHERE inv_no = '". $_ALL_POST['number'] ."'";
                $condition_query = " WHERE ord_no = '". $_ALL_POST['or_no'] ."'";
				Invoice::getParticulars($db, $temp, '*', $condition_query);
                $_ALL_POST['particulars']=$temp;
                
				
				$condition_query = '';
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Order with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("Records not found to view.");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>