<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    //include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
    //include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
   
    
$flwArray = array(

					'21'		=>'100',
					'22'		=>'101',
					'23'		=>'102',
					'24'		=>'103',
					'25'		=>'104',
					'26'		=>'105',
					'27'		=>'106',
					'28'		=>'107',
					'29'		=>'108',
					'30'	    =>'109',
					'31'	    =>'110',
					'32'	    =>'111',
					'33'	    =>'112',
					'34'	    =>'113',
					'35'	    =>'114',
					'36'	    =>'115',
					'37'	    =>'116',
					'38'	    =>'117',
					'39'	    =>'118',
					'40'	    =>'119',
					'41'	    =>'120',
                    '1'	    =>'1',
                    '4'	    =>'1',
                    '5'	    =>'1',
                    '6'	    =>'1',
                    '11'	    =>'1',
                    '12'	    =>'1'
				);
    
    foreach($flwArray as $key=>$val)
    {
         $query  = " UPDATE ". TABLE_SALE_LEADS
                            ." SET ". TABLE_SALE_LEADS .".followup_no = '". $val ."'"
                            ." WHERE lead_id = '". $key ."'";
                            
        //$db->query($query);                             
    }
    
        $page["var"][] = array('variable' => 'flwArray', 'value' => 'flwArray');
        
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>