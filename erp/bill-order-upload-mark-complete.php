<?php
    if ( $perm->has('nc_bl_or_up_mu') ) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
                
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages,
                        'my'          => $my
                        );
       BillOrderUpload::markCompleted($id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/bill-order-upload-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this module.");
         //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
