<?php
if ( $perm->has('nc_ps_qt_delete') ) {
    $inv_id = isset($_GET["inv_id"]) ? $_GET["inv_id"] : ( isset($_POST["inv_id"]) ? $_POST["inv_id"] : '' ); 
    $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        ); 
        
	if ( (ProspectsQuotation::getList($db, $invoice, TABLE_PROSPECTS_QUOTATION.'.id,'.TABLE_PROSPECTS_QUOTATION.'.or_no, 
		'.	TABLE_PROSPECTS_QUOTATION.'.number,'.TABLE_PROSPECTS_QUOTATION.'.access_level, 
		'.TABLE_PROSPECTS_QUOTATION.'.status ', " WHERE ".TABLE_PROSPECTS_QUOTATION.".id = '$inv_id'")) > 0 ) {
			$invoice = $invoice[0];
			if ( $invoice['status'] != ProspectsQuotation::ACTIVE ) {
				if ( $invoice['status'] == ProspectsQuotation::BLOCKED ) {
					 
				}else {
					$messages->setErrorMessage("To Cancel Proposal ".$invoice['number']." it should be Blocked.");
				}
			}
			else {
				$messages->setErrorMessage("To Cancel Proposal ".$invoice['number']." it should be Blocked.");
			}
		}
		else {
			$messages->setErrorMessage("The Proposal ".$invoice['number']." was not found.");
		}
	
	if( isset($_POST['btnCancel'])){
		header("Location:".DIR_WS_NC."/prospects-quotation.php?perform=list");
	 
	}  
		
	if($messages->getErrorMessageCount() > 0){
		 // Display the list.
		$perform = 'list';
		include ( DIR_FS_NC .'/prospects-quotation-list.php');
	
	}else{
	
		if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
			$data		= processUserData($_POST);
			if(empty($data['reason_of_delete'])){
				$messages->setErrorMessage("Please Specify Reason To Delete the Proposal ".$data['number']);
			}
			if($messages->getErrorMessageCount() <= 0){                    
				//Sql to delete invoice 
				 $query = "UPDATE ". TABLE_PROSPECTS_QUOTATION
								." SET status ='".ProspectsQuotation::DELETED."', 
								reason_of_delete ='".$data['reason_of_delete']."',
								delete_by ='".$my['uid']."',
								delete_ip ='".$_SERVER['REMOTE_ADDR']."',
								do_delete='".date("Y-m-d")."'
								WHERE id = '$inv_id'";
										
				if ( $db->query($query) && $db->affected_rows()>0 ) {    
					//Update in order, invoice_created =0                       
					$query = "UPDATE ".TABLE_PROSPECTS_ORDERS." SET ".TABLE_PROSPECTS_ORDERS.".is_invoice_create='0' WHERE 
					".TABLE_PROSPECTS_ORDERS.".number = '".$invoice['or_no']."'";
					$db->query($query);
					
					//Add Followup of order delete EOF
					 header("Location:".DIR_WS_NC."/prospects-quotation.php?perform=list&deleted=1&inv_no=".$data['number']);
				}
				else {
					$messages->setErrorMessage("The Proposal was not deleted.");
				} 
			} 
		} 
		$hidden[] = array('name'=> 'inv_id' ,'value' => $inv_id);
		$hidden[] = array('name'=> 'number' ,'value' => $invoice['number']);
		$hidden[] = array('name'=> 'perform' ,'value' => $perform);
		$hidden[] = array('name'=> 'act' , 'value' => 'save');
		$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
		$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-quotation-delete.html'); 
	}    
} else {
	$messages->setErrorMessage("You donot have the Right to Delete the Quotation.");
}
?>