<?php

    if ( $perm->has('nc_uv_details') ) {
        $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
    
        $list           = NULL;
        $_ALL_POST      = NULL;
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_uv_details_al') ) {
            $access_level += 1;
        }

        if ( Vendors::getList($db, $list, '*', " WHERE user_id = '$user_id'") > 0 ) {
            $_ALL_POST = $list['0'];
       
            if ( $_ALL_POST['access_level'] < $access_level ) {
                include_once ( DIR_FS_CLASS .'/RegionVendor.class.php');
                include_once ( DIR_FS_CLASS .'/PhoneVendor.class.php');
                include_once ( DIR_FS_CLASS .'/ReminderVendor.class.php');

                $region         = new RegionVendor();
                $phone          = new PhoneVendor(TABLE_VENDORS);
                $reminder       = new ReminderVendor(TABLE_VENDORS);

                //$_ALL_POST['manager'] = Vendors::getManager($db, '', $_ALL_POST['manager']);
                $_ALL_POST['industry']    = explode(",", $_ALL_POST['industry']);
                //BOF read the available industries
                $industry_list = NULL;
                $required_fields ='*';
                $condition_query="WHERE status='".Industry::ACTIVE."'";
                Industry::getList($db,$industry_list,$required_fields,$condition_query);
                //EOF read the available industries
                
               
                $_ALL_POST['team_list'] = NULL;
                $required_fields = "f_name,l_name,number";
                $_ALL_POST['team']= str_replace(',',"','",$_ALL_POST['team']);
                $condition_query="WHERE user_id IN('".$_ALL_POST['team']."')";
                User::getList($db,$_ALL_POST['team_list'],$required_fields,$condition_query);               
                // Read the available Access Level and User Roles.
				
                $_ALL_POST['access_level']  = Vendors::getAccessLevel($db, $access_level, $_ALL_POST['access_level']);
                $_ALL_POST['roles']         = Vendors::getRoles($db, $access_level, $_ALL_POST['roles']);
                //print_r($role_list);
                // Read the Contact Numbers.
                $phone->setPhoneOf(TABLE_VENDORS, $user_id);
                $_ALL_POST['phone'] = $phone->get($db);
                
                // Read the Addresses.
                $region->setAddressOf(TABLE_VENDORS, $user_id);
                $_ALL_POST['address_list'] = $region->get();
                
                // Read the Reminders.
                $reminder->setReminderOf(TABLE_VENDORS, $user_id);
                $_ALL_POST['reminder_list'] = $reminder->get($db);
                
                //print_r($_ALL_POST);
                
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                $page["var"][] = array('variable' => 'industry_list', 'value' => 'industry_list');
    
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Vendors with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("The Vendors details were not found. ");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permission to Access this module.");
    }
?>