<?php

	$condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = true;
	
    // BO: Ticket Status data.
    $tStatusStr = '';
    $chk_t_status = isset($_POST["chk_t_status"])   ? $_POST["chk_t_status"]  : (isset($_GET["chk_t_status"])   ? $_GET["chk_t_status"]   :'');
    $tStatus    = isset($_POST["tStatus"])      ? $_POST["tStatus"]     : (isset($_GET["tStatus"])      ? $_GET["tStatus"]      :'');
    if ( ($chk_t_status == 'AND' || $chk_t_status == 'OR' )  && isset($tStatus)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_t_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if(is_array($tStatus)){
             $tStatusStr = implode("','", $tStatus) ;
             $tStatusStr1 = implode(",", $tStatus) ;
             
        }else{
            $tStatusStr = str_replace(',',"','",$tStatus);
            $tStatusStr1 = $tStatus ;
            $tStatus = explode(",",$tStatus) ;
            
        }
        
        $condition_query .= " ". TABLE_TICKET_REMINDER .".ticket_status IN ('". $tStatusStr ."') ";
        
        $condition_url          .= "&chk_t_status=$chk_t_status&tStatus=$tStatusStr1";
        $_SEARCH["chk_t_status"]  = $chk_t_status;
        $_SEARCH["tStatus"]     = $tStatus;        
    }
    // EO: Ticket Status data.
	// BO: Type data.
    $sTypeStr = '';
    $chk_type = isset($_POST["chk_type"])   ? $_POST["chk_type"]  : (isset($_GET["chk_type"])   ? $_GET["chk_type"]   :'');
    $tType    = isset($_POST["tType"])      ? $_POST["tType"]     : (isset($_GET["tType"])      ? $_GET["tType"]      :'');
    if ( ($chk_type == 'AND' || $chk_type == 'OR' )  && isset($tType)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_type;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if(is_array($tType)){
             $tTypeStr = implode("','", $tType) ;
             $tTypeStr1 = implode(",", $tType) ; 
        }else{
            $tTypeStr = str_replace(',',",",$tType);
            $tTypeStr1 = $tType ;
            $tType = explode(",",$tType) ; 
        } 
        $condition_query .= " ". TABLE_TICKET_REMINDER .".ticket_type IN (". $tTypeStr .") "; 
        $condition_url          .= "&chk_type=$chk_type&tType=$tTypeStr1";
        $_SEARCH["chk_type"]  = $chk_type;
        $_SEARCH["tType"]     = $tType;        
    }
    // EO: Type data.
	$is_classified = isset($_POST["is_classified"]) ? $_POST["is_classified"]:(isset($_GET["is_classified"])? $_GET["is_classified"]   :0);
	if($is_classified!=''){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_CLIENTS .".is_classified ='". $is_classified ."' ";        
        $condition_url          .= "&is_classified=$is_classified";
        $_SEARCH["is_classified"]  = $is_classified; 
	}
	
    if ( $sString != "" ){
		 // Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ){
			$sType = "";
			$search_table = NULL ;
		}
        //$where_added = true;
        $sub_query='';        
        if ( $search_table == "Any" ) 
        {
            
            if ( $where_added ) 
            {
                //$condition_query .= "OR ";
                $condition_query .= " AND ";
            }
            else 
            {
                $condition_query .= " WHERE ";
                $where_added = true;
            }
          
            foreach( $sTypeArray as $table => $field_arr ) 
            {
              
                if ( $table != "Any" ) 
                { 
                    
                    if ($table == TABLE_TICKET_REMINDER) 
                    {
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            
                          
                           if($field !='number' && $field !='or_no' && $field !='ticket_no' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }
                            
                            //$sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                        }
                        
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                            
                    }elseif($table == TABLE_USER){
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            if($field !='number' ){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }
                            
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }elseif($table == TABLE_CLIENTS){
                    
                        $sub_query .= " ( " ;
                        foreach( $field_arr as $key => $field ) 
                        {
                            if($field !='number'){
                                $sub_query .= $table .".". clearSearchString($field,"-") ." LIKE '%".trim($sString)."%' OR " ;
                            }else{
                                  $sub_query .= $table .".". clearSearchString($field,"-") ." = '".trim($sString)."' OR " ;
                            }
                           
                           
                            //$sub_query .= $table .".". clearSearchString($field) ." LIKE '%".trim($sString)."%' OR " ;
                        }
                        $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
                        $sub_query .= " ) " ;
                        
                        if ( $where_added ) 
                        {
                            $sub_query .= "OR ";
                        }
                        else 
                        {
                            $sub_query .= " WHERE ";
                            $where_added = true;
                        }
                    
                    }
                    
                }
            }
            $sub_query = substr( $sub_query, 0, strlen( $sub_query ) - 3 );
            $condition_query .="(".$sub_query.")" ;
            
        }else{  
                
                if ( $where_added ) 
                {
                    $sub_query .= "AND ";
                }
                else 
                {
                    $sub_query .= " WHERE ";
                    $where_added = true;
                }
                if($sType !='number' && $sType !='or_no' && $sType !='ticket_no' ){
                    $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ." LIKE '%".trim($sString) ."%' )" ;
                }else{
                     $sub_query    .= " (". $search_table .".". clearSearchString($sType,"-") ."='".trim($sString) ."' )" ;
                }
              
               $condition_query .= $sub_query  ;
        }
        
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
        $condition_url      .= "&sString=$sString&sType=$sType";
	}    
    
    //Client serach bof
	$search_client = isset($_POST["search_client"])   ? $_POST["search_client"]  : (isset($_GET["search_client"])   ? $_GET["search_client"]   :'');
	$search_client_details = isset($_POST["search_client_details"])   ? $_POST["search_client_details"]  : (isset($_GET["search_client_details"])   ? $_GET["search_client_details"]   :'');
	
	if(!empty($search_client)){
		$searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " AND ";
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		$condition_query .= " ". TABLE_TICKET_REMINDER .".ticket_owner_uid ='". $search_client ."' ";        
        $condition_url          .= "&search_client=$search_client";
		
		
		$table = TABLE_CLIENTS;
		$fields1 =  TABLE_CLIENTS .'.f_name,'.TABLE_CLIENTS.'.l_name,'.TABLE_CLIENTS.'.billing_name' ;
		$condition1 = " WHERE ".TABLE_CLIENTS .".user_id ='".$search_client."' " ;
		$arr = getRecord($table,$fields1,$condition1);
		if(!empty($arr)){
			$search_client_details = $arr['f_name']." ".$arr['l_name']." (".$arr['billing_name'].")";
		} 
        $_SEARCH["search_client_details"]  = $search_client_details;
        $_SEARCH["search_client"] = $search_client;   
	}
	//Client serach eof
    // BO: Status data.
    $sStatusStr = '';
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( ($chk_status == 'AND' || $chk_status == 'OR' )  && isset($sStatus)){
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        if(is_array($sStatus)){
             $sStatusStr = implode("','", $sStatus) ;
             $sStatusStr1 = implode(",", $sStatus) ; 
        }else{
            $sStatusStr = str_replace(',',"','",$sStatus);
            $sStatusStr1 = $sStatus ;
            $sStatus = explode(",",$sStatus) ; 
        } 
        $condition_query .= " ". TABLE_TICKET_REMINDER .".status IN ('". $sStatusStr ."') "; 
        $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatusStr1";
        $_SEARCH["chk_status"]  = $chk_status;
        $_SEARCH["sStatus"]     = $sStatus;        
    }
    // EO: Status data  
    // BO: Status data.
    $ces_statusStr = '';
    $chk_cst_status= isset($_POST["chk_cst_status"]) ? $_POST["chk_cst_status"]  : (isset($_GET["chk_cst_status"])   ? $_GET["chk_cst_status"]   :'');
    $ces_status    = isset($_POST["ces_status"]) ? $_POST["ces_status"] : (isset($_GET["ces_status"])  ? $_GET["ces_status"]  :'');
    if ( ($chk_cst_status == 'AND' || $chk_cst_status == 'OR' )  && isset($chk_cst_status)){
        if( $where_added ) {
            $condition_query .= " ". $chk_cst_status;
        }else{
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }		
        if(is_array($ces_status)){
            $ces_statusStr = implode("','", $ces_status) ;
            $ces_statusStr1 = implode(",", $ces_status) ; 
        }else{
            $ces_statusStr = str_replace(',',"','",$ces_status);
            $ces_statusStr1 = $ces_status ;
            $ces_status = explode(",",$ces_status) ; 
        }
		
        $condition_query .= " ". TABLE_TICKET_REMINDER .".ces_status IN ('". $ces_statusStr ."') "; 
        $condition_url          .= "&chk_cst_status=$chk_cst_status&ces_status=$ces_statusStr1";
        $_SEARCH["chk_cst_status"]  = $chk_cst_status;
        $_SEARCH["ces_status"]  = $ces_status;        
    }
    // EO: Status data.
    
    
    // BO: Grade data.
    $sGradeStr = '';
    $chk_grade = isset($_POST["chk_grade"]) ? $_POST["chk_grade"]:(isset($_GET["chk_grade"])? $_GET["chk_grade"]   :'');
    $sGrade    = isset($_POST["sGrade"])  ? $_POST["sGrade"]: (isset($_GET["sGrade"]) ? $_GET["sGrade"]      :'');
    if ( ($chk_grade == 'AND' || $chk_grade == 'OR')  && isset($sGrade)){
        $searchStr = 1;
        if ( $where_added ) {
            $condition_query .= " ". $chk_grade;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        
        if(is_array($sGrade)){
             $sGradeStr = implode("','", $sGrade) ;
             $sGradeStr1 = implode(",", $sGrade) ;
             
        }else{
            $sGradeStr = str_replace(',',"','",$sGrade);
            $sGradeStr1 = $sGrade ;
            $sGrade = explode(",",$sGrade) ;
            
        }        
        
        $condition_query .= " ". TABLE_CLIENTS .".grade IN ('". $sGradeStr ."') ";        
        $condition_url          .= "&chk_grade=$chk_grade&sGrade=$sGradeStr1";
        $_SEARCH["chk_grade"]  = $chk_grade;
        $_SEARCH["sGrade"]     = $sGrade;
        
    }
	
	
	
	
    // BO: Priority data.
    $sPriorityStr = '';
    $chk_priority = isset($_POST["chk_priority"])   ? $_POST["chk_priority"]  : (isset($_GET["chk_priority"])   ? $_GET["chk_priority"]   :'');
    $sPriority    = isset($_POST["sPriority"])      ? $_POST["sPriority"]     : (isset($_GET["sPriority"])      ? $_GET["sPriority"]      :'');
    if ( ($chk_priority == 'AND' || $chk_priority == 'OR') && isset($sPriority)) {
        if ( $where_added ) {
            $condition_query .= " ". $chk_priority;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
         if(is_array($sPriority)){
             $sPriorityStr = implode("','", $sPriority) ;
             $sPriorityStr1 = implode(",", $sPriority) ;
             
        }else{
            $sPriorityStr = str_replace(',',"','",$sPriority);
            $sPriorityStr1 = $sPriority ;
            $sPriority = explode(",",$sPriority) ;
            
        }
        
        $condition_query .= " ". TABLE_TICKET_REMINDER .".ticket_priority IN ('". $sPriorityStr ."') ";
        
        $condition_url          .= "&chk_priority=$chk_priority&sPriority=$sPriorityStr1";
        $_SEARCH["chk_priority"]  = $chk_priority;
        $_SEARCH["sPriority"]     = $sPriority;   
    }
    // EO: Priority data.
    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
	 $dt_field        = isset($_POST["dt_field"])      ? $_POST["dt_field"]         : (isset($_GET["dt_field"])      ? $_GET["dt_field"]      :'');
    
    // BO: From Date
    if ( ($chk_date_from == 'AND' || $chk_date_from == 'OR' ) && !empty($date_from)) {
        
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
       
        $dfa = explode('/', $date_from);       
        $mktimefrm = mktime(0,0,0,$dfa[1],$dfa[0],$dfa[2]);
        
        $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
         
        //$condition_query .= " ". TABLE_TICKET_REMINDER .".do_d >= '". $dfa ."'";
        if($dt_field=='ticket_date'){
			$condition_query .= " ". TABLE_TICKET_REMINDER .".ticket_date >= '". $mktimefrm ."'";    
		}else{
			$condition_query .= " ". TABLE_TICKET_REMINDER .".".$dt_field." >= '". $dfa ."'";  
		}
        $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
        $_SEARCH["chk_date_from"]   = $chk_date_from;
        $_SEARCH["date_from"]       = $date_from;
		$_SEARCH["dt_field"]       = $dt_field;
    }  
    // EO: From Date
    
    // BO: Upto Date
    if ( ($chk_date_to == 'AND' || $chk_date_to == 'OR') && !empty($date_to)) {
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta = explode('/', $date_to);
        $mktimeto = mktime(60,0,0,$dta[1],$dta[0],$dta[2]);
        
        $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        if($dt_field=='ticket_date'){
			$condition_query .= " ". TABLE_TICKET_REMINDER .".ticket_date <= '". $mktimeto ."'"; 
		}else{
			$condition_query .= " ". TABLE_TICKET_REMINDER .".".$dt_field." <= '". $dta ."'";  
		}
        
        $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
        $_SEARCH["chk_date_to"] = $chk_date_to ;
        $_SEARCH["date_to"]     = $date_to ;
		$_SEARCH["dt_field"]       = $dt_field;
    }
    
	 $chk_assign  = isset($_POST["chk_assign"])? $_POST["chk_assign"]   : (isset($_GET["chk_assign"])? $_GET["chk_assign"]:'');
    $assign = isset($_POST["assign"]) ? $_POST["assign"] : (isset($_GET["assign"])    ? $_GET["assign"]    :'');
	
	if($chk_assign == 'AND' && $assign!=''){
        if ( $where_added ){
            $condition_query .= $chk_assign;
        }else{
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
		
        if($assign==1){
			$condition_query .= " ". TABLE_TICKET_REMINDER .".assign_members !=''"; 
		}else{
			$condition_query .= " ". TABLE_TICKET_REMINDER .".assign_members =''"; 
		}
		
        $condition_url          .= "&chk_assign=$chk_assign&assign=$assign";
        $_SEARCH["chk_assign"] = $chk_assign ;
        $_SEARCH["assign"]     = $assign ; 
    }
    
    // EO: Upto Date

    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&dt_field=$dt_field&action=search";
			
	//print_r($condition_url);
	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    
      
    include ( DIR_FS_NC .'/ticket-reminder-list.php');
?>