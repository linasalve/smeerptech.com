<?php
    if ( $perm->has('nc_ld_or_add') ) {
    
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
       // include_once ( DIR_FS_INCLUDES .'/leads-quotation.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
        include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
        //include_once ( DIR_FS_INCLUDES .'/followup.inc.php' );
        include_once (DIR_FS_INCLUDES .'/service-tax.inc.php');
		include_once ( DIR_FS_INCLUDES .'/service-tax-price.inc.php' );
		include_once ( DIR_FS_INCLUDES .'/leads-ticket.inc.php');
		
       
        $po_id          = isset($_GET["po_id"]) ? $_GET["po_id"] : ( isset($_POST["po_id"]) ? $_POST["po_id"] : '' );
        $lead_id        = isset($_GET["lead_id"]) ? $_GET["lead_id"] : ( isset($_POST["lead_id"]) ? $_POST["lead_id"] : '' );
        $q_id           = isset($_GET["q_id"]) ? $_GET["q_id"] : ( isset($_POST["q_id"]) ? $_POST["q_id"] : '' );
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
		
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
		
        /*
		if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }*/
        //getfinancial yr bof

		
		$stype_list = LeadsOrder::getSTypes();
		$op_list = LeadsOrder::getRoundOffOp();		 
		$discountType = LeadsOrder::getDiscountType();
        // Read the taxlist 
		$subtax_list    = NULL;		
		$tax_list    = NULL;
		$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=0 ORDER BY tax_name ASC ";
		ServiceTax::getList($db, $tax_list, 'id,tax_name', $condition_queryst);
		
		$tax_vlist    = NULL;
		$condition_queryvt= " WHERE status = '". ACTIVE ."' ORDER BY tax_price ASC";
		ServiceTaxPrice::getList($db, $tax_vlist, 'id,tax_price,tax_percent', $condition_queryvt);
		
		  
		//Get company list 
        $lst_company=null;
        $fields = TABLE_SETTINGS_COMPANY .'.id'
                   .','. TABLE_SETTINGS_COMPANY .'.name';
        Company::getList($db,$lst_company,$fields);
      
        // Read the available source
       	Leads::getSource($db,$source);
         
		// Include the Work Stage class and Work Timeline class.
		include_once (DIR_FS_INCLUDES .'/work-stage.inc.php');
		include_once (DIR_FS_INCLUDES .'/work-timeline.inc.php');
		include_once (DIR_FS_INCLUDES .'/user.inc.php');
		include_once (DIR_FS_INCLUDES .'/common-functions.inc.php');
		$lst_work_stage = NULL;
		//WorkStage::getWorkStages($db, $lst_work_stage);
        // list of executives to assign project manager
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
		$condition1 = " WHERE ".TABLE_USER.".status = '".User::ACTIVE."' 
			AND ".TABLE_USER.".department='".ID_MARKETING."' AND 
			".TABLE_USER.".user_id NOT IN ('".SALES_MEMBER_USER_ID."','".CEO_USER_ID."')"; 
        User::getList($db,$lst_executive,$fields,$condition1);     
        
        //code to generate $vendorOptionList BOF        
      /*$vendorSql = "SELECT id,billing_name FROM ".TABLE_VENDORS ;
        $db->query( $vendorSql );
        if( $db->nf() > 0 )
		{
            $vendorList['0'] = "" ;
			while($db->next_record())
			{
                $vendorList[$db->f('id')] = $db->f('billing_name') ;
            }
            
        } */
        //code to generate $vendorOptionList EOF
        
        $currency = NULL;
        $required_fields ='*';
		Currency::getList($db,$currency,$required_fields);        
        // code for particulars bof 
         $rejectedFiles=null;
        // Read the Services.
        include_once ( DIR_FS_INCLUDES .'/services.inc.php');        
        if(!empty($or_id)){
			// Read the Order which is to be Updated.
            $fields = TABLE_LEADS_ORDERS .'.*'
                        .','. TABLE_SALE_LEADS .'.lead_id AS c_user_id'
                        .','. TABLE_SALE_LEADS .'.number AS c_number'
                        .','. TABLE_SALE_LEADS .'.f_name AS c_f_name'
                        .','. TABLE_SALE_LEADS .'.l_name AS c_l_name'
						.','. TABLE_SALE_LEADS .'.billing_name AS billing_name'
                        .','. TABLE_SALE_LEADS .'.company_name AS c_company_name'
                        .','. TABLE_SALE_LEADS .'.email AS c_email'
                        .','. TABLE_SALE_LEADS .'.status AS c_status';
            
            $condition_query = " WHERE (". TABLE_LEADS_ORDERS .".id = '". $or_id ."' "
                                    ." OR ". TABLE_LEADS_ORDERS .".number = '". $or_id ."')";

            if ( $perm->has('nc_ld_or_edit_all') ) {
			
			}else{
			
				$condition_query .= " AND ( ";
		
				// If my has created this Order.
				$condition_query .= " (". TABLE_LEADS_ORDERS .".created_by = '". $my['user_id'] ."' ) OR  
										(".TABLE_LEADS_ORDERS.".access_level < ".$access_level.") ";
										
				$condition_query .= " ) ";						
			}
            
			if ( LeadsOrder::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                
                $_ALL_POST = $_ALL_POST['0'];
				
				 $is_inv_cr = $is_invoice_created=0;
                $sql= " SELECT COUNT(*) as count FROM ".TABLE_LEADS_QUOTATION." WHERE 
				".TABLE_LEADS_QUOTATION.".or_no='".$_ALL_POST['number']."' AND 
				".TABLE_LEADS_QUOTATION.".status !='".LeadsQuotation::DELETED."' 
				LIMIT 0,1";
			    $db->query($sql);               
			    if ( $db->nf()>0 ) {
					while ( $db->next_record() ) {
						$count = $db->f('count') ;
						if( $count > 0){
							$is_inv_cr = 1;
						}
					}
				}
				
                $data= $_ALL_POST;
               
                //Check whether is_invoice_created created or not                
                //if ( $_ALL_POST['access_level'] < $access_level ) {
                    // Set up the Client Details field.
                    $_ALL_POST['client_details'] = $_ALL_POST['c_f_name'] .' '. $_ALL_POST['c_l_name']
                                                    .' ('. $_ALL_POST['c_company_name'] .')';
                                                    //.' ('. $_ALL_POST['c_email'] .')';                    
                    // Read the Team Members Information.lead_details
					$_ALL_POST['lead'] = $_ALL_POST['client'] ;
					$_ALL_POST['lead_details'] = $_ALL_POST['client_details'] ;
					$_ALL_POST['team'] = trim($_ALL_POST['team'],",") ;
					$_ALL_POST['old_team'] 		= $_ALL_POST['team'] ;
                    $_ALL_POST['team']          = explode(',', $_ALL_POST['team']);
                    $temp                       = "'". implode("','", $_ALL_POST['team'])."'";
                    $_ALL_POST['team_members']  = '';
                    $_ALL_POST['team_details']  = array();
                    User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', 
					" WHERE user_id IN (". $temp .")");
                    $_ALL_POST['team'] = array();
                    foreach ( $_ALL_POST['team_members'] as $key=>$members) {
                        $_ALL_POST['team'][] = $members['user_id'];
                        $_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ('. $members['number'] .')';
                    }
					 // Read the Clients Sub Members Information.
					$_ALL_POST['clients_su']= trim($_ALL_POST['clients_su']);
                    $_ALL_POST['clients_su']          = explode(',', $_ALL_POST['clients_su']);
                    $temp_su                       = "'". implode("','", $_ALL_POST['clients_su']) ."'";
                    $_ALL_POST['clients_su_members']  = '';
                    $_ALL_POST['clients_su_details']  = array();
                    Leads::getList($db, $_ALL_POST['clients_su_members'], 'lead_id as user_id,number,f_name,l_name', "
					WHERE lead_id IN (". $temp_su .")");
                    $_ALL_POST['clients_su'] = array();
					if(!empty($_ALL_POST['clients_su_members'])){
						foreach ( $_ALL_POST['clients_su_members'] as $key=>$members) {
					  
							$_ALL_POST['clients_su'][] = $members['user_id'];
							/*$_ALL_POST['clients_su_details'][] = $members['f_name'] .' '. $members['l_name'] 
							.' ('. $members['number'] .')'; */
							$_ALL_POST['clients_su_details'][] = $members['f_name'] .' '. $members['l_name'] .'';
						}
					}
                    //date of order
                    $_ALL_POST['do_o']  = explode(' ', $_ALL_POST['do_o']);
                    $temp               = explode('-', $_ALL_POST['do_o'][0]);
                    $_ALL_POST['do_o']  = NULL;
                    $_ALL_POST['do_o']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    // Setup the date of delivery.
                    $_ALL_POST['do_d']  = explode(' ', $_ALL_POST['do_d']);
                    $temp               = explode('-', $_ALL_POST['do_d'][0]);
                    $_ALL_POST['do_d']  = NULL;
                    $_ALL_POST['do_d']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    $po_id              = $_ALL_POST['po_id'];
                    
                    // Setup the start date and est date
                    if( $_ALL_POST['st_date'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['st_date']  ='';
                    }else{
                        $_ALL_POST['st_date']  = explode(' ', $_ALL_POST['st_date']);
                        $temp               = explode('-', $_ALL_POST['st_date'][0]);
                        $_ALL_POST['st_date']  = NULL;
                        $_ALL_POST['st_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    // Setup the start date and est date
                    
                    if( $_ALL_POST['es_ed_date'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['es_ed_date']  ='';
                    }else{
                        $_ALL_POST['es_ed_date']  = explode(' ', $_ALL_POST['es_ed_date']);
                        $temp               = explode('-', $_ALL_POST['es_ed_date'][0]);
                        $_ALL_POST['es_ed_date']  = NULL;
                        $_ALL_POST['es_ed_date']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    if( $_ALL_POST['do_e'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['do_e']  ='';
                    }else{
                        $_ALL_POST['do_e']  = explode(' ', $_ALL_POST['do_e']);
                        $temp               = explode('-', $_ALL_POST['do_e'][0]);
                        $_ALL_POST['do_e']  = NULL;
                        $_ALL_POST['do_e']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    if( $_ALL_POST['do_fe'] =='0000-00-00 00:00:00'){
                        $_ALL_POST['do_fe']  ='';
                    }else{
                        $_ALL_POST['do_fe']  = explode(' ', $_ALL_POST['do_fe']);
                        $temp               = explode('-', $_ALL_POST['do_fe'][0]);
                        $_ALL_POST['do_fe']  = NULL;
                        $_ALL_POST['do_fe']  = $temp[2] .'/'. $temp[1] .'/'. $temp[0];
                    }
                    
                   
                    // Read the Particulars.
                    $temp_p = NULL;
                    $condition_query_p ='';
                    $service_idArr= array();
                    $condition_query_p = " WHERE ".TABLE_LEADS_ORD_P.".ord_no = '". $_ALL_POST['number'] ."'";
                    LeadsOrder::getParticulars($db, $temp_p, '*', $condition_query_p);
                    if(!empty($temp_p)){                       
						foreach ( $temp_p as $pKey => $parti ) {                            
							$_ALL_POST['p_id'][$pKey]       = $temp_p[$pKey]['id'];	
							$_ALL_POST['sub_s_id'][$pKey]       = $temp_p[$pKey]['sub_s_id'];
                            $_ALL_POST['s_id'][$pKey]       = $temp_p[$pKey]['s_id'];							
                            $_ALL_POST['ss_title'][$pKey]       = $temp_p[$pKey]['ss_title'];							
							$_ALL_POST['s_type'][$pKey]       = $temp_p[$pKey]['s_type'];
							$_ALL_POST['s_quantity'][$pKey]       = $temp_p[$pKey]['s_quantity'];
							$_ALL_POST['s_amount'][$pKey]       = $temp_p[$pKey]['s_amount'];
							$_ALL_POST['particulars'][$pKey]= $temp_p[$pKey]['particulars'];
							$_ALL_POST['p_amount'][$pKey]   = $temp_p[$pKey]['p_amount'];
							$_ALL_POST['d_amount'][$pKey]   = $temp_p[$pKey]['d_amount'];
							$_ALL_POST['ss_punch_line'][$pKey]   = $temp_p[$pKey]['ss_punch_line'];							
							$_ALL_POST['ss_div_id'][$pKey]   = $temp_p[$pKey]['ss_div_id'];							
							$_ALL_POST['tax1_name'][$pKey]   = $temp_p[$pKey]['tax1_name'];
							$_ALL_POST['tax1_id_check'][$pKey]   = $temp_p[$pKey]['tax1_id'];
							$_ALL_POST['tax1_value'][$pKey]   = $temp_p[$pKey]['tax1_value'];
							$_ALL_POST['tax1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_pvalue'];
							$_ALL_POST['tax1_number'][$pKey]   = $temp_p[$pKey]['tax1_number'];
							$_ALL_POST['tax1_amount'][$pKey]   = $temp_p[$pKey]['tax1_amount'];					
							$_ALL_POST['tax1_sub1_name'][$pKey]   = $temp_p[$pKey]['tax1_sub1_name'];
							$_ALL_POST['tax1_sub1_id_check'][$pKey]   = $temp_p[$pKey]['tax1_sub1_id'];
							$_ALL_POST['tax1_sub1_value'][$pKey]   = $temp_p[$pKey]['tax1_sub1_value'];
							$_ALL_POST['tax1_sub1_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub1_pvalue'];
							$_ALL_POST['tax1_sub1_number'][$pKey]   = $temp_p[$pKey]['tax1_sub1_number'];
							$_ALL_POST['tax1_sub1_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub1_amount'];					
							$_ALL_POST['tax1_sub2_name'][$pKey]   = $temp_p[$pKey]['tax1_sub2_name'];
							$_ALL_POST['tax1_sub2_id_check'][$pKey]   = $temp_p[$pKey]['tax1_sub2_id'];
							$_ALL_POST['tax1_sub2_value'][$pKey]   = $temp_p[$pKey]['tax1_sub2_value'];
							$_ALL_POST['tax1_sub2_pvalue'][$pKey]   = $temp_p[$pKey]['tax1_sub2_pvalue'];
							$_ALL_POST['tax1_sub2_number'][$pKey]   = $temp_p[$pKey]['tax1_sub2_number'];
							$_ALL_POST['tax1_sub2_amount'][$pKey]   = $temp_p[$pKey]['tax1_sub2_amount'];					 
							$_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
							$_ALL_POST['tot_amount'][$pKey]   = $temp_p[$pKey]['tot_amount'];
							$_ALL_POST['discount_type'][$pKey]   = $temp_p[$pKey]['discount_type'];
                            $_ALL_POST['pp_amount'][$pKey]   = $temp_p[$pKey]['pp_amount'];
                            $_ALL_POST['is_renewable'][$pKey]   = $temp_p[$pKey]['is_renewable'];
                            $_ALL_POST['stot_amount'][$pKey]   = $temp_p[$pKey]['stot_amount'];
							$_ALL_POST['vendor'][$pKey]   = $temp_p[$pKey]['vendor'];
                            $_ALL_POST['purchase_particulars'][$pKey]   = $temp_p[$pKey]['purchase_particulars'];							
							$_ALL_POST['tax1_sid_opt'][$pKey]=array();
							$_ALL_POST['tax1_sid_opt_count'][$pKey]=0;
							
							if($_ALL_POST['tax1_id_check'][$pKey]>0){
								//tax1_sub_id Options of taxes 
								//$_ALL_POST['tax1_sid_opt'][$pKey] = array('0'=>2);
								$tax_opt = array();
								$tax_id = $_ALL_POST['tax1_id_check'][$pKey];
								$condition_queryst= " WHERE status = '". ACTIVE ."' AND parent_id=".$tax_id." 
								ORDER BY tax_name ASC ";
								ServiceTax::getList($db, $tax_opt, 'id,tax_name', $condition_queryst);
								if(!empty($tax_opt)){
									$_ALL_POST['tax1_sid_opt'][$pKey] = $tax_opt ;  
									$_ALL_POST['tax1_sid_opt_count'][$pKey] = count($tax_opt) ;  
								}
							}
							$_ALL_POST['sub_sid_list'][$pKey]= $_ALL_POST['subsid'][$pKey]=$ss_sub_id_details=array();

							if(!empty($_ALL_POST['s_id'][$pKey])){
								$condition_query_sid = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = 
								".$_ALL_POST['s_id'][$pKey]." 
								AND ".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
								$condition_query_sid .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id 
								= ".$_ALL_POST['currency_id']." 
								AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;
								
								$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".
								TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
								$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." 
								".$condition_query_sid ; 
								 $query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".ss_title ASC";

								if ( $db->query($query) ) {
									if ( $db->nf() > 0 ) {
										while ($db->next_record()) {
											$ss_subtitle =  trim($db->f("ss_title")).",";
											$ss_subprice =  $db->f("ss_price") ;
											$ss_subid =  $db->f("ss_id") ;
											$ss_sub_id_details[] = array(
												'ss_title'=>$ss_subtitle,
												'ss_price'=>$ss_subprice,
												'ss_id'=>$ss_subid	
											);
										}
									}
								} 
						   }
						   $_ALL_POST['subsidstr'][$pKey] =  $_ALL_POST['sub_s_id'][$pKey] ;
						   $_ALL_POST['sub_sid_list'][$pKey]=$ss_sub_id_details;
						   $sub_s_id_str='';
						   $sub_s_id_str =trim($_ALL_POST['subsidstr'][$pKey],",");
						   if(!empty($sub_s_id_str)){
								$_ALL_POST['subsid'][$pKey] = explode(",",$sub_s_id_str);
								$sub_s_id_str=",".$sub_s_id_str.",";
						   }						   
						}						
                    }
                    $_ALL_POST['service_id'] = $service_idArr;
                   
                    $_ALL_POST['po_filename']=$_ALL_POST['po_filename'];
                    $_ALL_POST['filename_1']=$_ALL_POST['filename_1'];
                    $_ALL_POST['filename_2']=$_ALL_POST['filename_2'];
                    $_ALL_POST['filename_3']=$_ALL_POST['filename_3']; 
					$data=$_ALL_POST;
					
				
            } 
            //get order details eof
		
		}
		
        $lst_service    = NULL;
        $condition_query= " WHERE ss_status = '". ACTIVE ."' AND ss_parent_id=0 ORDER BY ss_title ASC"; 
        Services::getList($db, $lst_service, 'ss_division,ss_id,ss_title,ss_punch_line,tax1_id, tax1_name, tax1_value,is_renewable
		', $condition_query);
        $condition_query = '';
        if(!empty($lst_service)){
            foreach($lst_service as $val =>$key ){
				$ss_id= $key['ss_id'];
				$ss_div_id= $key['ss_division'];
				$ss_title= $key['ss_title'];
				$ss_punch_line= $key['ss_punch_line'];
				$tax1_id = $key['tax1_id'];
				$tax1_name = $key['tax1_name'];
				$tax1_value = $key['tax1_value'];
				$is_renewable = $key['is_renewable'];
				$tax1_pvalue = (float) (((float )  $key['tax1_value'] ) / 100 );
				$keyNew['ss_div_id'] = $ss_div_id ; 
				$keyNew['ss_id'] = $ss_id ; 
				$keyNew['ss_title'] = $ss_title ; 
				$keyNew['ss_punch_line'] = $ss_punch_line ; 
				$keyNew['tax1_id'] = $tax1_id ; 
				$keyNew['tax1_name'] = $tax1_name ; 
				$keyNew['tax1_value'] = $tax1_value ; 
				$keyNew['tax1_pvalue'] = $tax1_pvalue ; 
				$keyNew['is_renewable'] = $is_renewable ; 
				$lst_service[$val] = $keyNew;
            }
        }
		
        // code for particulars eof
           
        /* Get quotation details eof*/
        
    
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn']) || isset($_POST['btnReturnInv'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
       
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types   ;
            $data['max_file_size'] = MAX_FILE_SIZE     ;
            $data['max_mfile_size'] = MAX_MULTIPLE_FILE_SIZE     ;
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
            
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'lst_service'       => $lst_service,
							'lst_work_stage'    => $lst_work_stage,
                            'messages'          => &$messages,
                            'data'          => &$data
                        );
            
            if ( LeadsOrder::validateAdd($data, $extra)  ) {
			    
                
                $currency1 =null;
                $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                $fields1 = "*";
                Currency::getList( $db, $currency1, $fields1, $condition_query1);              
                if(!empty($currency1)){
                   $data['currency_abbr'] = $currency1[0]['abbr'];
                   $data['currency_name'] = $currency1[0]['currency_name'];
                   $data['currency_symbol'] = $currency1[0]['currency_symbol'];
                   $data['currency_country'] = $currency1[0]['country_name'];
                }
				
                $company1 =null;
                $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                $fields_c1 = " prefix,name ";
                Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
                if(!empty($company1)){                  
                   $data['company_name'] = $company1[0]['name'];
                   $data['company_prefix'] = $company1[0]['prefix'];
                }
                /*
                if(!empty($data['po_filename'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['po_filename']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "PO-".$data['number'].".".$ext ;
                    $attachfilename = "PO-".mktime().".".$ext ;
                    $data['po_filename'] = $attachfilename;
                    
                  
                    if (move_uploaded_file ($files['po_filename']['tmp_name'], 
					DIR_FS_ORDER_FILES."/".$attachfilename)){
                        @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                          
                    }
                }
                
                if(!empty($data['filename_1'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_1']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                    //$attachfilename = "WO1-".$data['number'].".".$ext ;
                    $attachfilename = "WO1-".mktime().".".$ext ;
                     $data['filename_1'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_1']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                       
                      @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
                
                if(!empty($data['filename_2'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_2']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "WO2-".$data['number'].".".$ext ;
                    $attachfilename = "WO2-".mktime().".".$ext ;
                     $data['filename_2'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_2']['tmp_name'], DIR_FS_ORDER_FILES."/".$attachfilename)){
                         @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
                
                if(!empty($data['filename_3'])){
                    $filedata["extension"]='';
                    $filedata = pathinfo($files['filename_3']["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    //$attachfilename = "WO3-".$data['number'].".".$ext ;
                    $attachfilename = "WO3-".mktime().".".$ext ;
                     $data['filename_3'] = $attachfilename;
                    
                    if (move_uploaded_file ($files['filename_3']['tmp_name'],
					DIR_FS_ORDER_FILES."/".$attachfilename)){
                       @chmod(DIR_FS_ORDER_FILES."/".$attachfilename,0777);
                    }
                }
                */
                if($data['isrenewable']){                    
                    $do_e  = date('Y-m-d H:i:s', $data['do_e']) ;
                    $do_fe  = date('Y-m-d H:i:s', $data['do_fe']) ;
					$do_e  = '';
					$do_fe  = '';
                }
				$do_d ='';
				if(!empty($data['do_d'])){
					$do_d = date('Y-m-d H:i:s', $data['do_d']) ;
				}							
                $clients_su_str='';
                if(!empty($data['clients_su'])){
					$clients_su_str = implode(",", $data['clients_su']);
					$clients_su_str = ",".$clients_su_str."," ;
				}
				$tax_ids_str='';				
				if(!empty($data['alltax_ids'])){
					$tax_ids_str = implode(",", $data['alltax_ids']);
					$tax_ids_str = ",".$tax_ids_str."," ;
				}
				$s_id_str ='';
				if(!empty($data['s_id'])){
					$s_id_str = implode(",", $data['s_id']);
					$s_id_str = ",".$s_id_str."," ;
				}				
				$dont_show_total=0;
				if(isset($data['dont_show_total'])){
					$dont_show_total=1;
				}
				
                $query	= " INSERT INTO ".TABLE_LEADS_ORDERS
						." SET ". TABLE_LEADS_ORDERS .".number       = '". $data['number'] ."'"
						//.",". TABLE_LEADS_ORDERS .".po_id = '".    $data['po_id'] ."'"
						.",". TABLE_LEADS_ORDERS .".service_id       = '". $s_id_str ."'"
						.",". TABLE_LEADS_ORDERS .".ord_counter      = '". $data['ord_counter'] ."'"
						.",". TABLE_LEADS_ORDERS .".access_level     = '". $my['access_level'] ."'"
						.",". TABLE_LEADS_ORDERS .".client           = '". $data['lead']['user_id'] ."'"
						.",". TABLE_LEADS_ORDERS .".created_by       = '". $data['created_by'] ."'"
						.",". TABLE_LEADS_ORDERS .".company_id	     = '". $data['company_id'] ."'"
						.",". TABLE_LEADS_ORDERS .".company_name	 = '". $data['company_name'] ."'"
						.",". TABLE_LEADS_ORDERS .".company_prefix   = '". $data['company_prefix'] ."'"
						.",". TABLE_LEADS_ORDERS .".order_title	     = '". $data['order_title'] ."'"
						.",". TABLE_LEADS_ORDERS .".dont_show_total	     = '". $dont_show_total ."'"
						.",". TABLE_LEADS_ORDERS .".tax_ids     = '". $tax_ids_str ."'"
						.",". TABLE_LEADS_ORDERS .".order_domain	 = '". $data['order_domain'] ."'"
						.",". TABLE_LEADS_ORDERS .".currency_id      = '". $data['currency_id'] ."'"
						.",". TABLE_LEADS_ORDERS .".currency_abbr    = '". $data['currency_abbr'] ."'"
						 .",".TABLE_LEADS_ORDERS .".currency_name    = '". $data['currency_name'] ."'"
						.",". TABLE_LEADS_ORDERS .".currency_symbol  = '". $data['currency_symbol'] ."'"
						.",". TABLE_LEADS_ORDERS .".currency_country = '". $data['currency_country'] ."'"
						.",". TABLE_LEADS_ORDERS .".quotation_no     = '". $data['quotation_no'] ."'"
						.",". TABLE_LEADS_ORDERS .".source_to        = '". $data['source_to'] ."'"
						.",". TABLE_LEADS_ORDERS .".source_from      = '". $data['source_from'] ."'"
						.",". TABLE_LEADS_ORDERS .".po_filename      = '". $data['po_filename'] ."'"
						.",". TABLE_LEADS_ORDERS .".filecaption_1    = '". $data['filecaption_1'] ."'"
						.",". TABLE_LEADS_ORDERS .".filename_1       = '". $data['filename_1'] ."'"
						.",". TABLE_LEADS_ORDERS .".filecaption_2    = '". $data['filecaption_2'] ."'"
						.",". TABLE_LEADS_ORDERS .".filename_2       = '". $data['filename_2'] ."'"
						.",". TABLE_LEADS_ORDERS .".filecaption_3    = '". $data['filecaption_3'] ."'"
						.",". TABLE_LEADS_ORDERS .".filename_3       = '". $data['filename_3'] ."'"
						.",". TABLE_LEADS_ORDERS .".amount          = '". $data['amount'] ."'"
						.",". TABLE_LEADS_ORDERS .".stotal_amount   = '". $data['stotal_amount'] ."'"
						.",". TABLE_LEADS_ORDERS .".round_off_op 	= '". $data['round_off_op'] ."'"                        
						.",". TABLE_LEADS_ORDERS .".round_off 		= '". $data['round_off'] ."'"        
						.",". TABLE_LEADS_ORDERS .".existing_client	= '". $data['existing_client'] ."'"
						.",". TABLE_LEADS_ORDERS .".client_s_client	= '". $data['client_s_client'] ."'"
						.",". TABLE_LEADS_ORDERS .".order_type	    = '". $data['order_type'] ."'"
						.",". TABLE_LEADS_ORDERS .".order_closed_by	= '". $data['order_closed_by'] ."'"
						.",". TABLE_LEADS_ORDERS .".team             = ',". implode(",", $data['team']) .",'"
						.",". TABLE_LEADS_ORDERS .".clients_su       = '".$clients_su_str ."'" 
					   // .",". TABLE_LEADS_ORDERS .".particulars 	 = '".$data['particulars']."'"
						.",". TABLE_LEADS_ORDERS .".details          = '". $data['details']."'"
						.",". TABLE_LEADS_ORDERS .".do_o             = '". date('Y-m-d H:i:s', $data['do_o']) ."'"
						.",". TABLE_LEADS_ORDERS .".do_c             = '". date('Y-m-d H:i:s') ."'"
						.",". TABLE_LEADS_ORDERS .".do_d             = '". $do_d ."'"
						.",". TABLE_LEADS_ORDERS .".do_e             = '". $do_e."'"
						.",". TABLE_LEADS_ORDERS .".do_fe            = '". $do_fe."'"
						.",". TABLE_LEADS_ORDERS .".project_manager  = '". $data['project_manager']."'"
						.",". TABLE_LEADS_ORDERS .".ref_website  	 = '". $data['ref_website']."'"
						.",". TABLE_LEADS_ORDERS .".color_likes  	 = '". $data['color_likes']."'"
						.",". TABLE_LEADS_ORDERS .".color_dislikes   = '". $data['color_dislikes']."'"
						.",". TABLE_LEADS_ORDERS .".requirements     = '". $data['requirements']."'"
						.",". TABLE_LEADS_ORDERS .".competitor_quote = '". $data['competitor_quote'] ."'"
						.",". TABLE_LEADS_ORDERS .".competitor_details = '". $data['competitor_details'] ."'"
						.",". TABLE_LEADS_ORDERS .".prospect_budget	     = '". $data['prospect_budget'] ."'"
						.",". TABLE_LEADS_ORDERS .".prospect_can_spent_amt = '". $data['prospect_can_spent_amt'] ."'"
						.",". TABLE_LEADS_ORDERS .".target_service	     = '". $data['target_service'] ."'"
						.",". TABLE_LEADS_ORDERS .".st_date          = '". $data['st_date']."'"
						.",". TABLE_LEADS_ORDERS .".es_ed_date       = '". $data['es_ed_date']."'"
						.",". TABLE_LEADS_ORDERS .".es_hrs           = '". $data['es_hrs']."'"
						.",". TABLE_LEADS_ORDERS .".status           = '". $data['status'] ."'" 
						.",". TABLE_LEADS_ORDERS .".is_renewable      = '". $data['isrenewable'] ."'"
						.",". TABLE_LEADS_ORDERS .".is_renewal_ord   = '". $data['is_renewal_ord'] ."'"						   
						.",". TABLE_LEADS_ORDERS .".ip           = '".$_SERVER['REMOTE_ADDR']."'" ;
               
                if ( $db->query($query) ) {
                    
                    $variables['hid'] = $db->last_inserted_id();
                    $messages->setOkMessage("New Order has been created.");
					
                    
                    //After insert, update order counter in                      
					
                    // Insert the Particulars.
                    if(!empty($data['query_p'])){
                        if ( !$db->query($data['query_p']) ) {
                            $messages->setErrorMessage("The Particulars were not Saved.");
                        }
                    }
                    // Send the notification mails to the concerned persons.
                
                    
                    
                    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
                    $file_name='';
                    // Send Email to the Client.
                   
					$temp = NULL;
                    $temp_p = NULL;
                    $condition_query = "WHERE ".TABLE_LEADS_ORD_P.".ord_no = '". $data['number'] ."'";
                    LeadsOrder::getParticulars($db, $temp, '*', $condition_query);
                    $particulars = "" ;
					if(!empty($temp)){
						
						$particulars .="<table cellpadding='0' cellspacing='0' border='1' width='100%'>" ;
                        foreach ( $temp as $pKey => $parti ){
							$sr = $pKey + 1;
							$service_type = $temp[$pKey]['is_renewable']  ? 'Renewable' : 'Non-Renewable' ;
                            $temp_p[] = array(
                                            'particulars'=>$temp[$pKey]['particulars'],
                                            'ss_punch_line' =>$temp[$pKey]['ss_punch_line']  
                                            );
							  	 	
							$ss_title =  $temp[$pKey]['ss_title'] ;
							$ss_punch_line =  $temp[$pKey]['ss_punch_line'] ;
							$p_amount =  $temp[$pKey]['p_amount'] ;
							$s_type =  $temp[$pKey]['s_type'] ;
							$s_quantity =  $temp[$pKey]['s_quantity'] ;
							$samount =  $temp[$pKey]['samount'] ;
							$discount_type =  $temp[$pKey]['discount_type'] ; 
							$stot_amount =  $temp[$pKey]['stot_amount'] ; 
							$tax1_name =  $temp[$pKey]['tax1_name'] ; 
							$tax1_value =  $temp[$pKey]['tax1_value'] ; 
							$tax1_amount =  $temp[$pKey]['tax1_amount'] ; 
							$tax1_sub1_name =  $temp[$pKey]['tax1_sub1_name'] ; 
							$tax1_sub1_value =  $temp[$pKey]['tax1_sub1_value'] ; 
							$tax1_sub1_amount =  $temp[$pKey]['tax1_sub1_amount'] ; 
							$tax1_sub2_name =  $temp[$pKey]['tax1_sub2_name'] ; 
							$tax1_sub2_value =  $temp[$pKey]['tax1_sub2_value'] ; 
							$tax1_sub2_amount =  $temp[$pKey]['tax1_sub2_amount'] ; 
							$d_amount =  $temp[$pKey]['d_amount'] ; 
							$stot_amount =  $temp[$pKey]['stot_amount'] ; 
							$tot_amount =  $temp[$pKey]['tot_amount'] ; 
							$discount_type =  $temp[$pKey]['discount_type'] ; 
							$particulars .="
							<tr>
								<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'><b>".$sr."</b> Punchline : ".$temp[$pKey]['ss_punch_line'] ." - Service type  : ".$service_type  ."</td/>
							</tr>
							<tr>
								<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Services : ".$ss_title ."</td/>
							</tr>
							<tr>
								<td> 
									<table border='0'>
										<tr>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Price</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Type</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Qty</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Amt</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Discount</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>DiscType</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Sub Tot</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Taxname</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tax%</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tax Amt</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>Tot Amt</td>
										</tr>
										<tr>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$p_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$s_type."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$s_quantity."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$samount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$d_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$discount_type."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$stot_amount."</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_name."<br/>".$tax1_sub1_name."<br/>".$tax1_sub2_name."
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_value."<br/>".$tax1_sub1_value."<br/>".$tax1_sub2_value."
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tax1_amount."<br/>".$tax1_sub1_amount."<br/>".$tax1_sub2_amount."
											
											</td>
											<td style='font-family: arial,sans-serif; font-size: 13px; line-height: 18px; padding-left: 0px; padding-top: 3px; color: rgb(102, 102, 102);'>".$tot_amount."</td>
										</tr>
									</table>
								</td/>
							</tr>
							" ;
                        }
						$particulars .= "</table>";
						
                    }
					$data['link']   = DIR_WS_MP .'/leads-order.php?perform=view&or_id='. $variables['hid'];
                    $data['af_name'] =$my['f_name'];
                    $data['al_name'] = $my['l_name'] ;
                    $data['particulars']=$temp_p;
					
                   /*  
				    if( isset($data['mail_client']) &&  $data['mail_client']==1 ){
                        $email = NULL;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'LEADS_ORDER_CREATE_TO_CLIENT', $data, $email) ) {
                            $to     = '';                           
                            $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                        }
                    } 
					*/
                    //Send mail after create invoice to admin bof/
                    
					$team_members='';
					$team_str = implode(",", $data['team']) ;
					if(!empty($team_str )){
						$team_str = trim($team_str,",");
						$team_str = str_replace(",","','",$team_str);
					 
						User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN ('". $team_str ."')");
						foreach ( $_ALL_POST['team_members'] as $key=>$members) {
						
							$team_members .= $members['f_name'] .' '. $members['l_name']."," ;
							 
						}
					} 
					$lead_by_str = $data['order_closed_by'];
					$lead_by = '';
					if(!empty($lead_by_str )){
					 
						User::getList($db, $_ALL_POST['lead_by'], 'user_id,number,f_name,l_name', 
						"WHERE user_id IN ('". $lead_by_str ."')");
						foreach ( $_ALL_POST['lead_by'] as $key=>$lead_by1) {
						
							$lead_by .= $lead_by1['f_name'] .' '. $lead_by1['l_name']."" ;
							 
						}
						
					}
                    $data['client_name']= $data['lead']['f_name'] .' '. $data['lead']['l_name']; 
                    $data['added_by']= $my['f_name'].' '.$my['l_name']; 
                   // $data['sub_clients_members']=$sub_clients_members; 
                    $data['team_members']=$team_members; 
                    $data['lead_by']=$lead_by; 
                    $data['particulars']=$particulars; 
                    $data['currency']=$data['currency_name']; 
                    $data['link']   = DIR_WS_NC .'/leads-order.php?perform=view&or_id='. $variables['hid'];
                    $email = NULL;
					 
                    $cc_to_sender= $cc = $bcc = Null;
                    /* 
					if ( getParsedEmail($db, $s, 'LEADS_ORDER_CREATE_TO_ADMIN', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => $sales_name , 'email' => $sales_email);                           
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);        				 
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,
					    $bcc,$file_name);
                    } 
					*/
                    
					//insert order based comment in ticket bof
					$flw_ord_id =$variables['hid'];
					$ticket_no  =  LeadsTicket::getNewNumber($db);	
					$sql2 = " SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
					".TABLE_LD_TICKETS.".flw_ord_id = '".$flw_ord_id."' LIMIT 0,1";
					$db->query($sql2) ;
					if ( $db->nf() > 0 ) {
						while ($db->next_record()) {
						   $ticket_id= $db->f('ticket_id');
						}
					}
					if ( $ticket_id>0 ) {	
						//Add Comment BOF	
						$data['ticket_owner_uid'] = $data['lead']['user_id'];
						$data['name']= $data['lead']['f_name']." ".$data['lead']['l_name'] ;
						$data['ticket_owner'] = $data['lead']['billing_name']." ";
						if(!empty($data['name'])){
							$data['ticket_owner'] = $data['lead']['billing_name']." ".$data['name'];
						}
						
						$data['ticket_text'] = "New Lead added BY - 
						".$my['f_name']." ". $my['l_name']." <br/> 
						ON DATE  : ".date('d M Y') ;
						$data['ticket_subject']= 'Follow-up & Update';	
						$data['hrs']='';
						$data['min']=5;
						$data['posted_by'] = 'dec32cb82fe9bd2ffbec266ab14fdac6'; 
						$data['posted_by_name'] = 'Auto System' ;

						$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
						$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
						$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;	
						if(!empty($data['order_closed_by'])){
							$table = TABLE_USER;
							 $condition2 = " WHERE ".TABLE_USER.".user_id = '".$data['order_closed_by']."'
							AND ".TABLE_USER.".user_id != '".SALES_MEMBER_USER_ID."' 
							AND ".TABLE_USER.".department ='".ID_MARKETING."'" ;
							$fields1 =  TABLE_USER .".f_name, ".TABLE_USER.".l_name,".TABLE_USER.".department,
							".TABLE_USER.".marketing_contact, 
							".TABLE_USER.".email, 
							".TABLE_USER.".user_id " ;
							$detailsArr = getRecord($table,$fields1,$condition2);
							if(!empty($detailsArr)){
								//This is the marketing person identity
								$data['tck_owner_member_id'] = $detailsArr['user_id'];
								$data['tck_owner_member_name']  = $detailsArr['f_name']." ".$detailsArr['l_name'];
								$data['tck_owner_member_email'] = $detailsArr['email'];
								$data['marketing'] = 1;
								//disply random name EOF
							} 
						} 
						
						$hrs1 = (int) ($data['hrs'] * 6);
						$min1 = (int) ($data['min'] * 6);  
						
							
						$followup_query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
						. TABLE_LD_TICKETS .".ticket_no   = '". $ticket_no ."', "
						. TABLE_LD_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
						. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
						. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
						. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
						. TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
						. TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
						. TABLE_LD_TICKETS .".ticket_creator_uid= '". $data['posted_by']."', "
						. TABLE_LD_TICKETS .".ticket_creator    = '". $data['posted_by_name'] ."', "
						//. TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
						. TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
						. TABLE_LD_TICKETS .".ticket_status     = '".LeadsTicket::PENDINGWITHCLIENTS ."', "
						. TABLE_LD_TICKETS .".ticket_child      = '".$ticket_id."',"
						. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_LD_TICKETS .".min = '". $data['min']."', "
						. TABLE_LD_TICKETS .".ticket_response_time = '0', "
						. TABLE_LD_TICKETS .".ticket_replied        = '0', "
						. TABLE_LD_TICKETS .".from_admin_panel        = '".LeadsTicket::ADMIN_PANEL."', "
						. TABLE_LD_TICKETS .".display_name          = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_LD_TICKETS .".display_user_id        = '". $my['uid'] ."', "
						. TABLE_LD_TICKETS .".display_designation    = '". $my['desig'] ."', "
						. TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
						. TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
							
						$db->query($followup_query) ;
						//Add Comment EOF
						
					}else{							
						
						
						//Add Comment BOF
						
						$data['ticket_owner_uid'] = $data['client']['user_id'];
						$data['name']= $data['client']['f_name']." ".$data['client']['l_name'] ;
						$data['ticket_owner'] = $data['client']['billing_name']." ";
						if(!empty($data['name'])){
							$data['ticket_owner'] = $data['client']['billing_name']." ".$data['name'];
						}
						
						$data['ticket_text'] = "New Lead added BY - 
						".$my['f_name']." ". $my['l_name']." <br/> ON DATE  : ".date('d M Y') ;
							
						$data['hrs']='';
						$data['min']=5;
						$data['ticket_subject']= 'Follow-up & Update';
						$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
						$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
						$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;
						if(!empty($data['order_closed_by'])){
							$table = TABLE_USER;
							 $condition2 = " WHERE ".TABLE_USER.".user_id = '".$data['order_closed_by']."'
							AND ".TABLE_USER.".user_id != '".SALES_MEMBER_USER_ID."' 
							AND ".TABLE_USER.".department ='".ID_MARKETING."'" ;
							$fields1 =  TABLE_USER .".f_name, ".TABLE_USER.".l_name,".TABLE_USER.".department,
							".TABLE_USER.".marketing_contact, 
							".TABLE_USER.".email, 
							".TABLE_USER.".user_id " ;
							$detailsArr = getRecord($table,$fields1,$condition2);
							
							if(!empty($detailsArr)){
								//This is the marketing person identity
								$data['tck_owner_member_id'] = $detailsArr['user_id'];
								$data['tck_owner_member_name']  = $detailsArr['f_name']." ".$detailsArr['l_name'];
								$data['tck_owner_member_email'] = $detailsArr['email'];
								$data['marketing'] = 1;
								//disply random name EOF
							} 
						} 
						$hrs1 = (int) ($data['hrs'] * 6);
						$min1 = (int) ($data['min'] * 6);  	
						$data['posted_by'] = 'dec32cb82fe9bd2ffbec266ab14fdac6'; 
						$data['posted_by_name'] = 'Auto System' ;
							
						 $followup_query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
						. TABLE_LD_TICKETS .".ticket_no   = '". $ticket_no ."', "
						. TABLE_LD_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
						. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
						. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
						. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
						. TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
						. TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
						. TABLE_LD_TICKETS .".ticket_creator_uid= '". $data['posted_by'] ."', "
						. TABLE_LD_TICKETS .".ticket_creator    = '". $data['posted_by_name'] ."', "
						. TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
						. TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
						. TABLE_LD_TICKETS .".ticket_status     = '".LeadsTicket::PENDINGWITHCLIENTS ."', "
						. TABLE_LD_TICKETS .".ticket_child      = '0',"
						. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_LD_TICKETS .".min = '". $data['min']."', "
						. TABLE_LD_TICKETS .".ticket_response_time = '0', "
						. TABLE_LD_TICKETS .".ticket_replied        = '0', "
						. TABLE_LD_TICKETS .".from_admin_panel        = '".LeadsTicket::ADMIN_PANEL."', "
						. TABLE_LD_TICKETS .".display_name          = '". $my['f_name']." ". $my['l_name'] ."', "
						. TABLE_LD_TICKETS .".display_user_id       = '". $my['uid'] ."', "
						. TABLE_LD_TICKETS .".display_designation   = '". $my['desig'] ."', "
						. TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
						. TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
						
						$db->query($followup_query) ;
						//Add Comment EOF
					}
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $ord_no = $data['number'];
                //$_ALL_POST['number'] = getOldCounterNumber($db,'ORD',$data['company_id'],$data['financialYr']); 
                $_ALL_POST['exchange_rate'] = 1; 
                 $_ALL_POST['particulars']=array('0'=>'');
            }
        } else {           
            
            if(!isset($_ALL_POST['particulars'])){
				$_ALL_POST['particulars']=array('0'=>'');
			}
        }
 
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/leads-order.php?perform=add&added=1&hid=".$variables['hid']."&ord_no=".$ord_no);
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/leads-order.php");
        }
       
        if(isset($_POST['btnReturnInv']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/leads-quotation.php?perform=add&or_id=".$variables['hid']);
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ((isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {            
            header("Location:".DIR_WS_NC."/leads-order.php?perform=list&added=1&hid=".$variables['hid']."&ord_no=".$ord_no);
        }
        else {
            //Code default aryan manke in team list EOF
            if(!isset($_ALL_POST['team'])){
                $temp ="e68adc58f2062f58802e4cdcfec0af2d','".$my['user_id'];
                User::getList($db, $_ALL_POST['team_members'], 'user_id,number,f_name,l_name', "WHERE user_id IN ('". $temp ."')");
                $_ALL_POST['team'] = array();
                foreach ( $_ALL_POST['team_members'] as $key=>$members) {
                
                    $_ALL_POST['team'][] = $members['user_id'];
                    $_ALL_POST['team_details'][] = $members['f_name'] .' '. $members['l_name'] .' ( '. $members['number'] .')';
                }
            }
            //Code default aryan manke in team list BOF
            if(!isset($_ALL_POST['order_type'])){
                $_ALL_POST['order_type']= LeadsOrder::TARGETED ;
            }
            if(!isset($_ALL_POST['order_closed_by'])){
                //$_ALL_POST['order_closed_by']='e68adc58f2062f58802e4cdcfec0af2d' ;
                $_ALL_POST['order_closed_by']=$my['user_id'] ;
            }
			if(!isset($_ALL_POST['project_manager'])){
			  $_ALL_POST['project_manager']= 'e68adc58f2062f58802e4cdcfec0af2d' ;
			}
            $al_list = getAccessLevel($db, $access_level);
            $index = array();
            if ( isset($_ALL_POST['po_access_level']) ) {
                array_key_search('access_level', $_ALL_POST['po_access_level'], $al_list, $index);
                $_ALL_POST['po_access_level'] = $al_list[$index[0]]['title'];
            }
            
            //$hidden[] = array('name'=> 'po_id' ,'value' => $po_id);
            //$hidden[] = array('name'=> 'lead_id' ,'value' => $lead_id);
            //$hidden[] = array('name'=> 'q_id' ,'value' => $q_id);
            
            //$hidden[] = array('name'=> 'tbl_name' ,'value' => $tbl_name);
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
			
            //$page["var"][] = array('variable' => 'lead_id', 'value' => 'lead_id');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
			$page["var"][] = array('variable' => 'lst_work_stage', 'value' => 'lst_work_stage');
			$page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');
			$page["var"][] = array('variable' => 'lst_company', 'value' => 'lst_company');
			$page["var"][] = array('variable' => 'lst_fyr', 'value' => 'lst_fyr');
            $page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
            $page["var"][] = array('variable' => 'rejectedFiles', 'value' => 'rejectedFiles');
          
            $page["var"][] = array('variable' => 'source', 'value' => 'source');
			$page["var"][] = array('variable' => 'lst_service', 'value' => 'lst_service');
			
            $page["var"][] = array('variable' => 'vendorList', 'value' => 'vendorList');
            $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
			
			$page["var"][] = array('variable' => 'stype_list', 'value' => 'stype_list');  
			$page["var"][] = array('variable' => 'discountType', 'value' => 'discountType');  
			$page["var"][] = array('variable' => 'no_list', 'value' => 'no_list');  
            $page["var"][] = array('variable' => 'op_list', 'value' => 'op_list');     
            $page["var"][] = array('variable' => 'roundOff_list', 'value' => 'roundOff_list');    		
            $page["var"][] = array('variable' => 'tax_list', 'value' => 'tax_list');    
			$page["var"][] = array('variable' => 'subtax_list', 'value' => 'subtax_list');			
            $page["var"][] = array('variable' => 'tax_vlist', 'value' => 'tax_vlist');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-order-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>