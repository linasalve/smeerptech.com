<?php
    if ( $perm->has('nc_outward_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
        /*if ( $perm->has('nc_bl_or_add_al') ) {
            $access_level += 1;
        }*/
		
        
		// Include the hardware book class.
		include_once (DIR_FS_INCLUDES .'/outward.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Outward::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_OUT_WARD
                            ." SET ". TABLE_OUT_WARD .".to = '".                $data['to'] ."'"
                                .",". TABLE_OUT_WARD .".particulars = '".       $data['particulars'] ."'"
                                .",". TABLE_OUT_WARD .".access_level = '".      $my['access_level'] ."'"
                                .",". TABLE_OUT_WARD .".created_by = '".        $data['created_by'] ."'"
                                .",". TABLE_OUT_WARD .".content = '".           $data['content'] ."'"
                                .",". TABLE_OUT_WARD .".mode = '".              $data['mode'] ."'"
                                .",". TABLE_OUT_WARD .".for = '".               $data['for'] ."'"
                                .",". TABLE_OUT_WARD .".tpf = '".               $data['tpf'] ."'"
                                .",". TABLE_OUT_WARD .".ps = '".                $data['ps'] ."'"
                                .",". TABLE_OUT_WARD .".date = '".              $data['date'] ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Outward entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
         // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/outward.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/outward.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/outward.php?added=1");   
        }
        else {
        
             $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'ps_arr', 'value' => 'ps_arr');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'outward-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }                
?>
