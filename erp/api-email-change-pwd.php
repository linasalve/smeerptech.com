<?php 
    if ( $perm->has('nc_api') ) {    //nc_api_eml_chgpwd
		include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
		include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
		
		$step1 = isset ($_GET['step1']) ? $_GET['step1'] : ( isset($_POST['step1'] ) ? $_POST['step1'] : '1');
        $step2 = isset ($_GET['step2']) ? $_GET['step2'] : ( isset($_POST['step2'] ) ? $_POST['step2'] : '0');
		$_ALL_POST = isset ($_POST['perform']) ? $_POST : ( isset($_GET['perform'] ) ? $_GET : NULL);
		
		$no1 = rand(10,99);
		$no2 = rand(10,99);
		$spec1 = randomString(1,'+#-=!$%@^&<*_>');
		$str1 = randomString(3);
		$str2 = randomString(3);
		$email_password = $str1.$no1.$spec1.$str2.$no2;
		$format = 'application/json';
	    
		if ( (isset($_ALL_POST['btnCreate']) || isset($_ALL_POST['btnReturn'])) && $_ALL_POST['perform'] == 'change-pwd'){	
			$_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST); 
			$extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
						); 
			if($data['step1']=='1'){
                if ( SupportTicket::validateMemberSelect($data, $extra)) {
					$_ALL_POST = NULL;
					
                    $step2=1;
                    $step1=0;
                    $condition_query= " WHERE user_id = '". $data['client'] ."' ";
                   
                    if ( Clients::getList($db, $_ALL_POST, 'user_id, f_name,l_name,send_mail,payment_remarks,
						org,billing_name, additional_email,email,check_email,email_1,
						email_2,email_3, mobile1, mobile2,authorization_id', $condition_query) > 0 ) {
                        $_ALL_POST = $_ALL_POST[0];
                        $_ALL_POST['user_id']=$_ALL_POST['user_id'];
                        $_ALL_POST['name']=$_ALL_POST['f_name']." ".$_ALL_POST['l_name'];
                        //$_ALL_POST['email']=$_ALL_POST['email'];
                        $_ALL_POST['org']=$_ALL_POST['org'];
                        $_ALL_POST['billing_name']=$_ALL_POST['billing_name'];
                        $_ALL_POST['mobile1']=$_ALL_POST['mobile1'];
                        $_ALL_POST['mobile2']=$_ALL_POST['mobile2'];
                        $_ALL_POST['send_mail']=$_ALL_POST['send_mail'];
                        $_ALL_POST['check_email']=$_ALL_POST['check_email'];
                        $_ALL_POST['payment_remarks'] = $_ALL_POST['payment_remarks'];						
                        $all_additional_email = trim($_ALL_POST['additional_email'],",");
						$additionalEmailArr = explode(",",$all_additional_email);
                        $_ALL_POST['authorization_id'] = $_ALL_POST['authorization_id'];
						$authorization_id = trim($_ALL_POST['authorization_id'],","); 
						$authorizationName='';
						if(!empty($authorization_id)){
							$authorization_id = trim($authorization_id,",");
							$authorizationArr = explode(",",$authorization_id);
							if(!empty($authorizationArr)){
								foreach($authorizationArr as $key2=>$val2){
									$authorizationName.= $clientAuth[$val2].", ";
								}
							}
						}
						$_ALL_POST['authorizationName'] =  $authorizationName; 
                        $_ALL_POST['mail_client'] = '0';
                        //$_ALL_POST['mail_to_all_su'] = '1';
						$_ALL_POST['accountValid'] = 0;
                        if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){ 
							$isValid = in_array(Clients::ACCOUNTS,$authorizationArr);
							$_ALL_POST['accountValid'] = $isValid ;
						}
                    }
					//GetDomain Details Bof
					if(!empty($data['domain_name'])){
						$_ALL_POST['email_domain'] = $data['domain_name'] ;
						$url_string = "/customers/all/domains/".$_ALL_POST['email_domain'];  
						//$url_string = "/customers/all/domains?domainNames=".$_ALL_POST['email_domain'];   
						$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
						$headers = array();
						$headers = array("Accept: $format"); 
						$curl_session = curl_init();
						curl_setopt($curl_session, CURLOPT_URL, $url);
						$time_stamp = date('YmdHis'); 
						$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
						$signature = base64_encode(sha1($data_to_sign, true)); 
						$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
						$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
						//http://stackoverflow.com/questions/14346540/php-get-json-using-curl-sorting-returned-array
						curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
						$httpResponse1 = curl_exec($curl_session); 
						curl_close($curl_session);						
						$_ALL_POST['domain_details'] = array();
						$_ALL_POST['domain_details'] = json_decode(stripslashes($httpResponse1),true);
						if(json_last_error() == JSON_ERROR_NONE){
							
							//Get Rackspace email ids of domain 
							if($_ALL_POST['domain_details']){
							
								$serviceType = $_ALL_POST['domain_details']['serviceType']; 
								if($serviceType == 'rsemail' || $serviceType =='both'){
									//$_ALL_POST['domain_details']['serviceType'] ; 
									$accountNumber = $_ALL_POST['domain_details']['accountNumber'] ;  
									$url_string = "/customers/".$accountNumber."/domains/".$_ALL_POST['email_domain']."/rs/mailboxes?size=500&offset=0"; 
									$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
									$headers = array();
									$headers = array("Accept: $format"); 
									$curl_session = curl_init();
									curl_setopt($curl_session, CURLOPT_URL, $url1);
									$time_stamp = date('YmdHis'); 
									$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
									$signature = base64_encode(sha1($data_to_sign, true)); 
									$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
									$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
									// comment me for time being
									//curl_setopt($curl_session, CURLOPT_HEADER, true);
									curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
									curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
									$httpResponse = curl_exec($curl_session);
									curl_close($curl_session);	
									$_ALL_POST['rs_email'] = json_decode(stripslashes($httpResponse),true); 
								} 
								
								if($serviceType == 'exchange' || $serviceType =='both'){
									$accountNumber = $_ALL_POST['domain_details']['accountNumber'] ;  
									$url_string = "/customers/".$accountNumber."/domains/".$_ALL_POST['email_domain']."/ex/mailboxes?size=500&offset=0"; 
									$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
									$headers = array(); 
									$headers = array("Accept: $format"); 
									$curl_session = curl_init();
									curl_setopt($curl_session, CURLOPT_URL, $url1);
									$time_stamp = date('YmdHis'); 
									$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
									$signature = base64_encode(sha1($data_to_sign, true)); 
									$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
									$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
									// comment me for time being
									//curl_setopt($curl_session, CURLOPT_HEADER, true);
									curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
									curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
									$httpResponse = curl_exec($curl_session);
									curl_close($curl_session);	
									$_ALL_POST['ex_email'] = json_decode(stripslashes($httpResponse),true); 
								} 
							} 
						}
					}
					
					//GetDomain Details Eof
					$subUserDetailsAccts=$subUserDetailsMgmts=$subUserDetailsAuth=$subUserDetailsUsr=$subUserDetailsDnd=$subUserDetailsDir=array();
					$followupSql= ""; 
					//Directors BOF
					$followupSql4 = " AND authorization_id  LIKE '%,".Clients::DIRECTOR.",%'" ;
					$suCondition4 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql4." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsDir1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition4); 
					if(!empty($subUserDetailsDir1)){
						foreach($subUserDetailsDir1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsDir[$key_su] = $val_su; 
						}
					} 
					//Directors EOF
					//Accounts BOF
					$followupSql = " AND authorization_id  LIKE '%,".Clients::ACCOUNTS.",%'" ;
					$suCondition = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."') AND status='".Clients::ACTIVE."'  
					".$followupSql." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsAccts1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition);  
					if(!empty($subUserDetailsAccts1)){
						foreach($subUserDetailsAccts1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsAccts[$key_su] = $val_su; 
						}
					} 
					//Accounts EOF
					//MANAGEMENT BOF
					$followupSql1 = " AND authorization_id  LIKE '%,".Clients::MANAGEMENT.",%'" ;
					$suCondition1 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql1." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsMgmts1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1,mobile2,authorization_id', $suCondition1); 
					if(!empty($subUserDetailsMgmts1)){
						foreach($subUserDetailsMgmts1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']); 
							if(!empty($authorizationArr1)){ 
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsMgmts[$key_su] = $val_su; 
						}
					} 
					//MANAGEMENT EOF
					//Authorised BOF
					$followupSql2 = " AND authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%'" ;
					$suCondition2 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql2." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsAuth1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition2); 
					if(!empty($subUserDetailsAuth1)){
						foreach($subUserDetailsAuth1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsAuth[$key_su] = $val_su; 
						}
					} 
					
					//Authorised EOF
					//USER BOF
					$followupSql3 = " AND authorization_id  LIKE '%,".Clients::USER.",%'" ;
					$suCondition3 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."') 
					AND status='".Clients::ACTIVE."'  
					".$followupSql3." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsUsr1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition3); 
					if(!empty($subUserDetailsUsr1)){
						foreach($subUserDetailsUsr1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsUsr[$key_su] = $val_su; 
						}
					} 
					//USER EOF
					//DND BOF
					$followupSql4 = " AND authorization_id  LIKE '%,".Clients::DND.",%'" ;
					$suCondition4 = " WHERE ( parent_id = '".$data['client']."' OR user_id='".$data['client']."')
					AND status='".Clients::ACTIVE."'  
					".$followupSql4." ORDER BY f_name ";
					Clients::getList($db, $subUserDetailsDnd1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition4); 
					if(!empty($subUserDetailsDnd1)){
						foreach($subUserDetailsDnd1 as $key_su => $val_su){
							$authorizationNameSu='';
							$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
							$authorizationArr1 = explode(",",$val_su['authorization_id']);
							 
							if(!empty($authorizationArr1)){
								foreach($authorizationArr1 as $keys=>$vals){
									if($clientAuth[$vals]=="DND"){
										$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
									}
									$authorizationNameSu.= $clientAuth[$vals].", ";
								}
							}							
							$val_su['authorizationNameSu'] = $authorizationNameSu ;
							$subUserDetailsDnd[$key_su] = $val_su; 
						}
					} 
					//DND EOF 
					 
                } 
            }else{  
				
				//Get details of Domain  
				if(!empty($data['email_domain'])){
					$_ALL_POST['email_domain'] = $data['email_domain'] ;
					$url_string = "/customers/all/domains/".$_ALL_POST['email_domain'];  
					//$url_string = "/customers/all/domains?domainNames=".$_ALL_POST['email_domain']; 
					$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
					$headers = array();
					$headers = array("Accept: $format"); 
					$curl_session = curl_init();
					curl_setopt($curl_session, CURLOPT_URL, $url);
					$time_stamp = date('YmdHis'); 
					$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
					$signature = base64_encode(sha1($data_to_sign, true)); 
					$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
					$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
					//https://stackoverflow.com/questions/14346540/php-get-json-using-curl-sorting-returned-array
					curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
					$httpResponse1 = curl_exec($curl_session);  
					curl_close($curl_session);	
					$_ALL_POST['domain_details'] = array();
					$_ALL_POST['domain_details'] = json_decode(stripslashes($httpResponse1),true);
					if(json_last_error() == JSON_ERROR_NONE){
						if($_ALL_POST['domain_details']){
							
							$serviceType = $_ALL_POST['domain_details']['serviceType'];
							//Get Email ids of domain 
							if($serviceType == 'rsemail' || $serviceType =='both'){
								
								$accountNumber = $_ALL_POST['domain_details']['accountNumber'] ;  
								$url_string = "/customers/".$accountNumber."/domains/".$_ALL_POST['email_domain']."/rs/mailboxes?size=500&offset=0"; 
								$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
								$headers = array();
								$headers = array("Accept: $format"); 
								$curl_session = curl_init();
								curl_setopt($curl_session, CURLOPT_URL, $url1);
								$time_stamp = date('YmdHis'); 
								$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
								$signature = base64_encode(sha1($data_to_sign, true)); 
								$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
								$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
								// comment me for time being
								//curl_setopt($curl_session, CURLOPT_HEADER, true);
								curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
								curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
								$httpResponse = curl_exec($curl_session);
								curl_close($curl_session);	
								$_ALL_POST['rs_email'] = json_decode(stripslashes($httpResponse),true); 
							} 
							
							if($serviceType == 'exchange' || $serviceType =='both'){
								$accountNumber = $_ALL_POST['domain_details']['accountNumber'] ;  
								$url_string = "/customers/".$accountNumber."/domains/".$_ALL_POST['email_domain']."/ex/mailboxes?size=500&offset=0"; 
								$url1 = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
								$headers = array();
								$headers = array("Accept: $format"); 
								$curl_session = curl_init();
								curl_setopt($curl_session, CURLOPT_URL, $url1);
								$time_stamp = date('YmdHis'); 
								$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
								$signature = base64_encode(sha1($data_to_sign, true)); 
								$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
								$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
								// comment me for time being
								//curl_setopt($curl_session, CURLOPT_HEADER, true);
								curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
								curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true); 
								$httpResponse = curl_exec($curl_session);
								curl_close($curl_session);	
								$_ALL_POST['ex_email'] = json_decode(stripslashes($httpResponse),true); 
							}
							
						} 
					} 
					 
				}
				
				if($data['form_type']=='change_pwd'){ 
					if ( empty($data['user_email1']) ) {
						$messages->setErrorMessage('Please enter email.');
					} 
				}
				
				if($data['form_type']=='create_email'){ 
					if ( empty($data['user_email2']) ) {
						$messages->setErrorMessage('Please enter email.');
					} 
					if ( empty($data['email_service']) ) {
						$messages->setErrorMessage('Select Premium Email Service OR Enterprise Email Service.');
					}
					if($data['email_service']=='ex'){ //Exchange mail service
						if( ($_ALL_POST['domain_details']['exchangeMaxNumMailboxes'] >0 ) && ( $_ALL_POST['domain_details']['exchangeUsedStorage']==$_ALL_POST['domain_details']['exchangeMaxNumMailboxes'] ) ){ 
							$messages->setErrorMessage('You can not create email as all Enterprise Mailboxes utilised.'); 
						} 
					}
					if($data['email_service']=='rs'){ //Premium mail service
						if( ($_ALL_POST['domain_details']['rsMaxNumMailboxes'] >0 ) && ( $_ALL_POST['domain_details']['rsUsedStorage']==$_ALL_POST['domain_details']['rsMaxNumMailboxes'] ) ){ 
							$messages->setErrorMessage('You can not create email as all Premium Mailboxes utilised.'); 
						} 
					}
					if(empty($data['email_size'])){ 
						$messages->setErrorMessage('Choose size of mailbox.'); 
					}  
				}
				 
				if($data['form_type']=='delete_email'){
					if ( empty($data['user_email3']) ) {
						$messages->setErrorMessage('Please enter email to be deleted.');
					} 
				
				}
					 
				if($data['form_type']=='allot_quota'){ 
					if ( empty($data['quota_email']) ) {
						$messages->setErrorMessage('Please enter email quota.');
					} 
					if ( empty($data['email_service1']) ) {
						$messages->setErrorMessage('Select Premium Email Service OR Enterprise Email Service.');
					}
				}
				
				print_R($data);
				if ( $messages->getErrorMessageCount() <= 0 ) {
				    if($data['form_type']=='change_pwd'){
						$data['user_email'] = $data['user_email1'] ;
						if(!empty($data['user_email']) && !empty($data['email_domain'])){				
							$url_string="/customers/all/domains/".$data['email_domain']."/rs/mailboxes/".$data['user_email'];
							$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
							$headers = array();
							$headers = array("Accept: $format");
							$curl_session = curl_init();
							curl_setopt($curl_session, CURLOPT_URL, $url);
							$time_stamp = date('YmdHis'); 
							$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
							$signature = base64_encode(sha1($data_to_sign, true));

							$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
							$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
							
							/*
							$no1 = rand(10,99);
							$no2 = rand(10,99);
							$spec1=randomString(2,'+#-=!$%@^&<*_>');
							$str1=randomString(3);
							$str2=randomString(2);
							$email_password =$str1.$no1.$spec1.$str2.$no2;
							*/
							$fields =  array( 
								'password' => $email_password 
							);
							curl_setopt($curl_session, CURLOPT_CUSTOMREQUEST, 'PUT');
							curl_setopt($curl_session, CURLOPT_POSTFIELDS, $fields);
							curl_setopt($curl_session, CURLOPT_HEADER, true);
							curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
							curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);

							$httpResponse = curl_exec($curl_session);
							curl_close($curl_session);								
							//$httpResponse = 'df fsdf sdfsfsdfdsf HTTP/1.1 200 OK';
							if(strpos($httpResponse, 'HTTP/1.1 200 OK')!==false){
								$ticket_no  =  SupportTicket::getNewNumber($db);
								$data['ticket_subject']	='Password Changed - '.$data['user_email'].'@'.$data['email_domain'];
								$data['ticket_text']= "<div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:18px;\">Dear Sir/Madam, <br/> Please check new password as below:</div><div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:15px;\">
								Email: ".$data['user_email']."@".$data['email_domain']." 
								New Password: ".$email_password."
								</div>";
								 
								$attachfilename=$ticket_attachment_path='';
								$mail_to_all_su=0;
								if(isset($data['mail_to_all_su']) ){
									$mail_to_all_su=1;
								}
								$mail_to_additional_email=0;
								if(isset($data['mail_to_additional_email']) ){
									$mail_to_additional_email=1;
								}
								$mail_client=0;
								if(isset($data['mail_client']) ){
									$mail_client=1;
								}
								$mail_ceo=0;
								if(isset($data['mail_ceo'])){
									$mail_ceo = 1;
								}
							 
								//disply random name BOF 
								$data['display_name']=$data['display_user_id']=$data['display_designation']='';
								$randomUser = getRandomAssociate($data['executive_id']);
								$data['display_name'] = $randomUser['name'];
								$data['display_user_id'] = $randomUser['user_id'];
								$data['display_designation'] = $randomUser['desig'];					
								//disply random name EOF		
								/* 
								if(!isset($data['ticket_status']) ){                    
									$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
								}else{
									$ticket_status = $data['ticket_status'];
								} */
								$data['min']=5;
								$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
								$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
								$min1 = (int) ($data['min'] * HRS_FACTOR);  
								$sent_inv_id_str = $sent_inv_no_str ='';
								if(!empty($data['sent_inv_id'])){	
									foreach($data['sent_inv_id'] as $key1=>$val1){
										$sent_inv_id_str .= $key1."," ;
										$sent_inv_no_str .= $val1."," ;
									}		
									$sent_inv_id_str = ",".$sent_inv_id_str ;
									$sent_inv_no_str = ",".$sent_inv_no_str ;
								}
								$sent_pinv_id_str = $sent_pinv_no_str ='';
								if(!empty($data['sent_pinv_id'])){	
									foreach($data['sent_pinv_id'] as $key2=>$val2){
										$sent_pinv_id_str .= $key2."," ;
										$sent_pinv_no_str .= $val2."," ;
									}
									$sent_pinv_id_str = ",".$sent_pinv_id_str ;					
									$sent_pinv_no_str = ",".$sent_pinv_no_str ;
								}
								if(empty($data['ss_id'])){		
									$data['ss_title']='';
									$data['ss_file_1']='';
									$data['ss_file_2']='';
									$data['ss_file_3']='';
								}
							
								$query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
								. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
								. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
								. TABLE_ST_TICKETS .".invoice_profm_id    = '". $invoice_profm_id ."', "
								. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
								. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
								. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
								. TABLE_ST_TICKETS .".on_behalf        = '". $data['on_behalf'] ."', "
								. TABLE_ST_TICKETS .".template_id      = '". $data['template_id'] ."', "
								. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
								. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
								. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
								. TABLE_ST_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
								. TABLE_ST_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
								. TABLE_ST_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
								. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
								. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
								. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
								. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
								. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
								. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
								. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
								. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
								. TABLE_ST_TICKETS .".ticket_creator_uid    = '". $my['uid'] ."', "
								. TABLE_ST_TICKETS .".ticket_creator         = '". $my['f_name']." ". $my['l_name'] ."', "
								. TABLE_ST_TICKETS .".ticket_subject    	 = '". $data['ticket_subject'] ."', "
								. TABLE_ST_TICKETS .".ticket_mail_subject    = '". $data['ticket_subject'] ."', "
								. TABLE_ST_TICKETS .".ticket_text         = '". $data['ticket_text'] ."', "
								. TABLE_ST_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
								. TABLE_ST_TICKETS.".last_comment_by 	  = '".$my['uid'] ."', "
								. TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
								//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
								//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
								//. TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
								. TABLE_ST_TICKETS .".mail_ceo        = '".$mail_ceo."', "
								. TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
								. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
								. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
								. TABLE_ST_TICKETS .".ticket_child      = '0', "
								. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
								. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
								. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
								. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
								. TABLE_ST_TICKETS .".min = '". $data['min']."', "
								. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
								. TABLE_ST_TICKETS .".ticket_response_time = '0', "
								. TABLE_ST_TICKETS .".ticket_replied        = '0', "
								. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
								//. TABLE_ST_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
								. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
								. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
								. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
								. TABLE_ST_TICKETS .".is_private           = '". $data['is_private'] ."', "
								. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
								. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
								. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
								. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
								. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ; 
								//unset($GLOBALS["flag"]);
								//unset($GLOBALS["submit"]); 
								if ($db->query($query) && $db->affected_rows() > 0) { 
									$variables['hid'] = $db->last_inserted_id() ;
									$data['ticket_no']    =   $ticket_no ;
									$data['domain']   = THIS_DOMAIN;                        
									//$data['replied_by']   =   $data['ticket_owner'] ;
									$data['mticket_no']   =   $ticket_no ;
									$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
									$data['subject']    =     $data['ticket_subject'] ;
									$data['text']    =   $data['ticket_text'] ;
									$data['attachment']   =   $attachfilename ;
									$data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $variables['hid'];
									if ($data['is_private'] != 1){
										if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
											if(isset($_POST['mail_to_all_su'])){
												$followupSql= "";
												if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
													$followupSql = " AND ( authorization_id  LIKE '%,".Clients::ACCOUNTS.",%' OR 
													authorization_id  LIKE '%,".Clients::MANAGEMENT.",%' OR
													authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%' ) " ;
												}
												Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, 
												email,email_1,email_2, email_3,email_4', " 
												WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."')
												AND status='".Clients::ACTIVE."' AND check_email='0' ".$followupSql."");
											}elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
												$mail_to_su = implode("','",array_unique($_POST['mail_to_su']));  
												Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " WHERE user_id IN('".$mail_to_su."') AND status='".Clients::ACTIVE."' "); 
											}                                
											if(!empty($subUserDetails)){ 
												foreach ($subUserDetails  as $key=>$value){
													$data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
													$data['ctitle']    =   $subUserDetails[$key]['ctitle'];
													$email = NULL;
													$cc_to_sender= $cc = $bcc = Null;
													if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email'], 
														'email' => $subUserDetails[$key]['email']);
														$from   = $reply_to = array('name' => $email["from_name"], 
														'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ; 
														if(!empty($subUserDetails[$key]['email_1'])){                                       
															//$to = '';
															$to[]   = array('name' => $subUserDetails[$key]['email_1'], 
															'email' => $subUserDetails[$key]['email_1']);
															$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
															$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ; 
														} 
														if(!empty($subUserDetails[$key]['email_2'])){                                       
															//$to = '';
															$to[]   = array('name' => $subUserDetails[$key]['email_2'], 
															'email' => $subUserDetails[$key]['email_2']);
															$from  = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
															$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ; 
														}
														if(!empty($subUserDetails[$key]['email_3'])){                                       
															//$to = '';
															$to[]   = array('name' => $subUserDetails[$key]['email_3'], 
															'email' => $subUserDetails[$key]['email_3']);
															$from   = $reply_to = array('name' => $email["from_name"], 
															'email' => $email['from_email']);
															$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ; 
														}
														if(!empty($subUserDetails[$key]['email_4'])){                                       
															//$to = '';
															$to[]   = array('name' => $subUserDetails[$key]['email_4'], 
															'email' => $subUserDetails[$key]['email_4']);
															$from   = $reply_to = array('name' => $email["from_name"],
															'email' => $email['from_email']);
															$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ; 
														}
													}                      
												}
											}
										} 
										if($mail_to_additional_email==1){ 
											if(!empty($userDetails['additional_email'])){
												$arrAddEmail = explode(",",$userDetails['additional_email']); 
												foreach($arrAddEmail as $key=>$val ){
													if(!empty($val)){
														$mail_send_to_su  .= ",".$val;
														//$to = '';
														$to[]   = array('name' =>$val, 'email' => $val);  
														 
													}
												}
											}
										}else{ 
											if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
												$arrAddEmail = $data['mail_additional_email'];
												foreach($arrAddEmail as $key=>$val ){
													if(!empty($val)){
														$mail_send_to_su  .= ",".$val;
														//$to = '';
														$to[]   = array('name' =>$val, 'email' => $val);  
													 
													}
												}
											}
										}	
										if(isset($_POST['mobile1_to_su']) ){                                
										   $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
											Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."') AND user_id IN('".$mobile1_to_su."') AND status='".Clients::ACTIVE."' ");
											if(!empty($subsmsUserDetails)){                                                                   
												foreach ($subsmsUserDetails  as $key=>$value){
													if(isset($value['mobile1']) && $smsLength<=130){
														$sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
														$sms_to_number[] =$value['mobile1'];
														$mobile1 = "91".$value['mobile1'];
														$client_mobile .= ','.$mobile1;
														ProjectTask::sendSms($data['text'],$mobile1);
														$counter = $counter+1; 
													}
												}
											} 
										}
										$subsmsUserDetails=array();
										if(isset($_POST['mobile2_to_su']) ){                                
										   $mobile2_to_su = implode("','",$_POST['mobile2_to_su']);                                  
											Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile2', " 
											WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."') AND user_id IN('".$mobile2_to_su."') AND status='".Clients::ACTIVE."' ");
											if(!empty($subsmsUserDetails)){                                                                   
												foreach ($subsmsUserDetails  as $key=>$value){
													if(isset($value['mobile2']) && $smsLength<=130){
														$sms_to[]  = $value['f_name'] .' '. $value['l_name'];
														$sms_to_number[] =$value['mobile2'];
														$mobile2 = "91".$value['mobile2'];
														$client_mobile .= ','.$mobile2;
														ProjectTask::sendSms($data['text'],$mobile2);
														$counter = $counter+1;
													}
												}
											} 
										}         
									}
									//Mail to CEO BOF
									if($mail_ceo==1){
										$to[]   = array('name' =>CEO_USER_EMAIL,'email' => CEO_USER_EMAIL);
										$mail_send_to_su .=",".CEO_USER_EMAIL ;
									}
									//Mail to CEO EOF
									//Set the list subuser's email-id to whom the mails are sent bof
									$query_update = "UPDATE ". TABLE_ST_TICKETS 
												." SET ". TABLE_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', sms_to_number='".implode(",",array_unique($sms_to_number))."',
													sms_to ='".implode(",",array_unique($sms_to))."' "                                    
												." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
									$db->query($query_update) ;
									//Set the list subuser's email-id to whom the mails are sent eof                        
									//update sms counter
									if($counter >0){
										$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
										$db->query($sql); 
									} 
									//Send one copy on clients BOF
									if(!empty($to)){
										getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
										//$to     = '';
										$cc_to_sender=$cc=$bcc='';
										$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
										'email' => $smeerp_client_email); 
										$from= $reply_to= array('name' => $email["from_name"], 'email' => $email['from_email']); 
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
										$cc_to_sender,$cc,$bcc,$file_name);
									}    
									//Send one copy on clients EOF
								} 
								$messages->setOkMessage("Password change successufully. Check ticket no.".$ticket_no);
								
							}else{
								
								$messages->setErrorMessage("Sorry password can not changed. Please try again.<br>Check your email-id".$httpResponse);
							}
						}	
					}
					
					//Create email id bof
					if($data['form_type']=='create_email'){
						$accountNumber = $_ALL_POST['domain_details']['accountNumber'] ;  
						$serv_type =$data['email_service'];
						$url_string="/customers/".$accountNumber."/domains/".$data['email_domain']."/".$serv_type."/mailboxes/".$data['user_email2']; 
						//$url_string = "/customers/".$accountNumber."/domains/".$_ALL_POST['email_domain']."/ex/mailboxes?size=500&offset=0"; 
						
						
						$url = 'https://' . RACKSPACE_SERVER_HOST . '/' . RACKSPACE_VERSION . $url_string; 
						$headers = array();
						$headers = array("Accept: $format");
						$curl_session = curl_init();
						curl_setopt($curl_session, CURLOPT_URL, $url);
						$time_stamp = date('YmdHis'); 
						$data_to_sign = RACKSPACE_USER_KEY . RACKSPACE_USER_AGENT . $time_stamp. RACKSPACE_SECRET_KEY;
						$signature = base64_encode(sha1($data_to_sign, true)); 
						$headers[] = "User-Agent: " . RACKSPACE_USER_AGENT;
						$headers[] = 'X-Api-Signature: '.RACKSPACE_USER_KEY . ":$time_stamp:$signature";
						/*
						$url_string = "/customers/1198581/domains/smeerptech.com/rs/mailboxes/testing";   
						$fields= array(
						  'password' => 'Secret13!@#',
						  'size' => '1024', //ie1MB
						  'lastName' => 'A',
						  'firstName' =>  'B',
						  'initials' =>  'A',
						  'notes' =>  'This is my note about my mailbox.',
						  'title' =>  'My Title', 
						  'organizationalStatus' =>  'OrgStatus',
						  'employeeType' =>  'Full-Time',
						  'visibleInExchangeGAL'=>'true', 
						  'visibleInRackspaceEmailCompanyDirectory' => 'false' 
						); 
						curl_setopt($curl_session, CURLOPT_POST,1);
						curl_setopt($curl_session, CURLOPT_CUSTOMREQUEST, 'POST');
						curl_setopt($curl_session, CURLOPT_POSTFIELDS, $fields);
						*/
						//echo $emailsize = ($data['email_size'] * 1024 );
						 $emailsize = $data['email_size'];
						//$emailsize = 1000;
						$fields =  array( 
							  'password' => $email_password,
							  'size' => $emailsize, //ie1MB
							  'lastName' => 'A',
							  'firstName' =>  'B',
							  'initials' =>  'A'
						);
						curl_setopt($curl_session, CURLOPT_POST,1);
						curl_setopt($curl_session, CURLOPT_CUSTOMREQUEST, 'POST');
						curl_setopt($curl_session, CURLOPT_POSTFIELDS, $fields);
						curl_setopt($curl_session, CURLOPT_HEADER, true);
						curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
						
						$httpResponse = curl_exec($curl_session); 	
						 
						curl_close($curl_session);	
						if(strpos($httpResponse, 'HTTP/1.1 200 OK')!==false){
						
							//$data['email_domain']."/".$serv_type."/mailboxes/".$data['user_email2']
							//Insert into TABLE_BILL_ORD_ALLOTED
							 
							$ticket_no  =  SupportTicket::getNewNumber($db);
							$data['full_details'] = $data['user_email2']."@".$data['email_domain']." email id created <br/>
							Password: ".$email_password;
							
							$sql = "INSERT INTO "	. TABLE_BILL_ORD_ALLOTED ." SET "
							. TABLE_BILL_ORD_ALLOTED .".ticket_no         = '". $ticket_no ."', " 
							. TABLE_BILL_ORD_ALLOTED .".client  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_BILL_ORD_ALLOTED .".client_details      = '". $data['ticket_owner'] ."', "
							. TABLE_BILL_ORD_ALLOTED .".full_details        = '". $data['full_details'] ."', "
							. TABLE_BILL_ORD_ALLOTED .".created_email        = '". $data['user_email2']."@".$data['email_domain']."', "
							. TABLE_BILL_ORD_ALLOTED .".created_by    = '". $my['uid'] ."', "
							. TABLE_BILL_ORD_ALLOTED .".created_by_name         = '". $my['f_name']." ". $my['l_name'] ."', " 
							. TABLE_BILL_ORD_ALLOTED .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_BILL_ORD_ALLOTED .".do_e    = '".date('Y-m-d H:i:s')."' "  ; 
							$db->query($sql) ;
							
							
							$data['ticket_subject']	='New Email Created - '.$data['user_email2'].'@'.$data['email_domain'];
							$data['ticket_text']= "<div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:18px;\">Dear Sir/Madam, <br/> Please check new password as below:</div><div style=\"font-family:verdana;arial;font-size:12px;font-weight:normal;line-height:15px;\">
							Email: ".$data['user_email2']."@".$data['email_domain']." 
							Password: ".$email_password."
							</div>";
							 
							$attachfilename=$ticket_attachment_path='';
							$mail_to_all_su=0;
							if(isset($data['mail_to_all_su']) ){
								$mail_to_all_su=1;
							}
							$mail_to_additional_email=0;
							if(isset($data['mail_to_additional_email']) ){
								$mail_to_additional_email=1;
							}
							$mail_client=0;
							if(isset($data['mail_client']) ){
								$mail_client=1;
							}
							$mail_ceo=0;
							if(isset($data['mail_ceo'])){
								$mail_ceo = 1;
							}
						 
							//disply random name BOF 
							$data['display_name']=$data['display_user_id']=$data['display_designation']='';
							$randomUser = getRandomAssociate($data['executive_id']);
							$data['display_name'] = $randomUser['name'];
							$data['display_user_id'] = $randomUser['user_id'];
							$data['display_designation'] = $randomUser['desig'];					
							//disply random name EOF		
							/* 
							if(!isset($data['ticket_status']) ){                    
								$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
							}else{
								$ticket_status = $data['ticket_status'];
							} */
							$data['min']=5;
							$ticket_status = SupportTicket::PENDINGWITHCLIENTS;
							$hrs1 = (int) ($data['hrs'] * HRS_FACTOR);
							$min1 = (int) ($data['min'] * HRS_FACTOR);  
							$sent_inv_id_str = $sent_inv_no_str ='';
							if(!empty($data['sent_inv_id'])){	
								foreach($data['sent_inv_id'] as $key1=>$val1){
									$sent_inv_id_str .= $key1."," ;
									$sent_inv_no_str .= $val1."," ;
								}		
								$sent_inv_id_str = ",".$sent_inv_id_str ;
								$sent_inv_no_str = ",".$sent_inv_no_str ;
							}
							$sent_pinv_id_str = $sent_pinv_no_str ='';
							if(!empty($data['sent_pinv_id'])){	
								foreach($data['sent_pinv_id'] as $key2=>$val2){
									$sent_pinv_id_str .= $key2."," ;
									$sent_pinv_no_str .= $val2."," ;
								}
								$sent_pinv_id_str = ",".$sent_pinv_id_str ;					
								$sent_pinv_no_str = ",".$sent_pinv_no_str ;
							}
							if(empty($data['ss_id'])){		
								$data['ss_title']='';
								$data['ss_file_1']='';
								$data['ss_file_2']='';
								$data['ss_file_3']='';
							}
						
							 $query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
							. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
							. TABLE_ST_TICKETS .".invoice_id         = '". $invoice_id ."', "
							. TABLE_ST_TICKETS .".invoice_profm_id    = '". $invoice_profm_id ."', "
							. TABLE_ST_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
							. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
							. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
							. TABLE_ST_TICKETS .".on_behalf        = '". $data['on_behalf'] ."', "
							. TABLE_ST_TICKETS .".template_id      = '". $data['template_id'] ."', "
							. TABLE_ST_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
							. TABLE_ST_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
							. TABLE_ST_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
							. TABLE_ST_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
							. TABLE_ST_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
							. TABLE_ST_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
							. TABLE_ST_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
							. TABLE_ST_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
							. TABLE_ST_TICKETS .".sent_inv_id   		= '". $sent_inv_id_str ."', "
							. TABLE_ST_TICKETS .".sent_inv_no   		= '". $sent_inv_no_str ."', "
							. TABLE_ST_TICKETS .".sent_pinv_id   		= '". $sent_pinv_id_str ."', "
							. TABLE_ST_TICKETS .".sent_pinv_no   		= '". $sent_pinv_no_str ."', "
							. TABLE_ST_TICKETS .".followup_pinv_str   	= '". $followupPInvStr ."', "
							. TABLE_ST_TICKETS .".followup_inv_str   	= '". $followupInvStr ."', "
							. TABLE_ST_TICKETS .".ticket_creator_uid    = '". $my['uid'] ."', "
							. TABLE_ST_TICKETS .".ticket_creator         = '". $my['f_name']." ". $my['l_name'] ."', "
							. TABLE_ST_TICKETS .".ticket_subject    	 = '". $data['ticket_subject'] ."', "
							. TABLE_ST_TICKETS .".ticket_mail_subject    = '". $data['ticket_subject'] ."', "
							. TABLE_ST_TICKETS .".ticket_text         = '". $data['ticket_text'] ."', "
							. TABLE_ST_TICKETS .".last_comment        = '". $data['ticket_text'] ."', "
							. TABLE_ST_TICKETS.".last_comment_by 	  = '".$my['uid'] ."', "
							. TABLE_ST_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."', "
							//. TABLE_ST_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
							//. TABLE_ST_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
							//. TABLE_ST_TICKETS .".ticket_status     = '". SupportTicket::PENDINGWITHCLIENTS ."', "
							. TABLE_ST_TICKETS .".mail_ceo        = '".$mail_ceo."', "
							. TABLE_ST_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
							. TABLE_ST_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
							. TABLE_ST_TICKETS .".ticket_status     = '".$ticket_status ."', "
							. TABLE_ST_TICKETS .".ticket_child      = '0', "
							. TABLE_ST_TICKETS .".ticket_attachment = '". $attachfilename ."', "
							. TABLE_ST_TICKETS .".hrs1 = '". $hrs1 ."', "
							. TABLE_ST_TICKETS .".min1 = '". $min1 ."', "
							. TABLE_ST_TICKETS .".hrs = '". $data['hrs'] ."', "
							. TABLE_ST_TICKETS .".min = '". $data['min']."', "
							. TABLE_ST_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
							. TABLE_ST_TICKETS .".ticket_response_time = '0', "
							. TABLE_ST_TICKETS .".ticket_replied        = '0', "
							. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
							//. TABLE_ST_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
							. TABLE_ST_TICKETS .".display_name           = '". $data['display_name'] ."', "
							. TABLE_ST_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
							. TABLE_ST_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
							. TABLE_ST_TICKETS .".is_private           = '". $data['is_private'] ."', "
							. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
							. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
							. TABLE_ST_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
							. TABLE_ST_TICKETS .".last_comment_from = '".SupportTicket::ADMIN_COMMENT."', "
							. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ; 
							//unset($GLOBALS["flag"]);
							//unset($GLOBALS["submit"]); 
							if ($db->query($query) && $db->affected_rows() > 0) { 
								$variables['hid'] = $db->last_inserted_id() ;
								$data['ticket_no']    =   $ticket_no ;
								$data['domain']   = THIS_DOMAIN;                        
								//$data['replied_by']   =   $data['ticket_owner'] ;
								$data['mticket_no']   =   $ticket_no ;
								$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
								$data['subject']    =     $data['ticket_subject'] ;
								$data['text']    =   $data['ticket_text'] ;
								$data['attachment']   =   $attachfilename ;
								$data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $variables['hid'];
								if ($data['is_private'] != 1){
									if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
										if(isset($_POST['mail_to_all_su'])){
											$followupSql= "";
											if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
												$followupSql = " AND ( authorization_id  LIKE '%,".Clients::ACCOUNTS.",%' OR 
												authorization_id  LIKE '%,".Clients::MANAGEMENT.",%' OR
												authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%' ) " ;
											}
											Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, 
											email,email_1,email_2, email_3,email_4', " 
											WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."')
											AND status='".Clients::ACTIVE."' AND check_email='0' ".$followupSql."");
										}elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
											$mail_to_su = implode("','",array_unique($_POST['mail_to_su']));  
											Clients::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " WHERE user_id IN('".$mail_to_su."') AND status='".Clients::ACTIVE."' "); 
										}                                
										if(!empty($subUserDetails)){ 
											foreach ($subUserDetails  as $key=>$value){
												$data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
												$data['ctitle']    =   $subUserDetails[$key]['ctitle'];
												$email = NULL;
												$cc_to_sender= $cc = $bcc = Null;
												if ( getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
													//$to = '';
													$to[]   = array('name' => $subUserDetails[$key]['email'], 
													'email' => $subUserDetails[$key]['email']);
													$from   = $reply_to = array('name' => $email["from_name"], 
													'email' => $email['from_email']);
													$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ; 
													if(!empty($subUserDetails[$key]['email_1'])){                                       
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email_1'], 
														'email' => $subUserDetails[$key]['email_1']);
														$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ; 
													} 
													if(!empty($subUserDetails[$key]['email_2'])){                                       
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email_2'], 
														'email' => $subUserDetails[$key]['email_2']);
														$from  = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ; 
													}
													if(!empty($subUserDetails[$key]['email_3'])){                                       
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email_3'], 
														'email' => $subUserDetails[$key]['email_3']);
														$from   = $reply_to = array('name' => $email["from_name"], 
														'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ; 
													}
													if(!empty($subUserDetails[$key]['email_4'])){                                       
														//$to = '';
														$to[]   = array('name' => $subUserDetails[$key]['email_4'], 
														'email' => $subUserDetails[$key]['email_4']);
														$from   = $reply_to = array('name' => $email["from_name"],
														'email' => $email['from_email']);
														$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ; 
													}
												}                      
											}
										}
									} 
									if($mail_to_additional_email==1){ 
										if(!empty($userDetails['additional_email'])){
											$arrAddEmail = explode(",",$userDetails['additional_email']); 
											foreach($arrAddEmail as $key=>$val ){
												if(!empty($val)){
													$mail_send_to_su  .= ",".$val;
													//$to = '';
													$to[]   = array('name' =>$val, 'email' => $val);  
													 
												}
											}
										}
									}else{ 
										if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
											$arrAddEmail = $data['mail_additional_email'];
											foreach($arrAddEmail as $key=>$val ){
												if(!empty($val)){
													$mail_send_to_su  .= ",".$val;
													//$to = '';
													$to[]   = array('name' =>$val, 'email' => $val);  
												 
												}
											}
										}
									}	
									if(isset($_POST['mobile1_to_su']) ){                                
									   $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
										Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."') AND user_id IN('".$mobile1_to_su."') AND status='".Clients::ACTIVE."' ");
										if(!empty($subsmsUserDetails)){                                                                   
											foreach ($subsmsUserDetails  as $key=>$value){
												if(isset($value['mobile1']) && $smsLength<=130){
													$sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
													$sms_to_number[] =$value['mobile1'];
													$mobile1 = "91".$value['mobile1'];
													$client_mobile .= ','.$mobile1;
													ProjectTask::sendSms($data['text'],$mobile1);
													$counter = $counter+1; 
												}
											}
										} 
									}
									$subsmsUserDetails=array();
									if(isset($_POST['mobile2_to_su']) ){                                
									   $mobile2_to_su = implode("','",$_POST['mobile2_to_su']);                                  
										Clients::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile2', " 
										WHERE (parent_id = '".$data['ticket_owner_uid']."' OR user_id='".$data['ticket_owner_uid']."') AND user_id IN('".$mobile2_to_su."') AND status='".Clients::ACTIVE."' ");
										if(!empty($subsmsUserDetails)){                                                                   
											foreach ($subsmsUserDetails  as $key=>$value){
												if(isset($value['mobile2']) && $smsLength<=130){
													$sms_to[]  = $value['f_name'] .' '. $value['l_name'];
													$sms_to_number[] =$value['mobile2'];
													$mobile2 = "91".$value['mobile2'];
													$client_mobile .= ','.$mobile2;
													ProjectTask::sendSms($data['text'],$mobile2);
													$counter = $counter+1;
												}
											}
										} 
									}         
								}
								//Mail to CEO BOF
								if($mail_ceo==1){
									$to[]   = array('name' =>CEO_USER_EMAIL,'email' => CEO_USER_EMAIL);
									$mail_send_to_su .=",".CEO_USER_EMAIL ;
								}
								//Mail to CEO EOF
								//Set the list subuser's email-id to whom the mails are sent bof
								$query_update = "UPDATE ". TABLE_ST_TICKETS 
											." SET ". TABLE_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', sms_to_number='".implode(",",array_unique($sms_to_number))."',
												sms_to ='".implode(",",array_unique($sms_to))."' "                                    
											." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
								$db->query($query_update) ;
								//Set the list subuser's email-id to whom the mails are sent eof                        
								//update sms counter
								if($counter >0){
									$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
									$db->query($sql); 
								} 
								//Send one copy on clients BOF
								if(!empty($to)){
									getParsedEmail($db, $s, 'EMAIL_ST_MEMBER', $data, $email);
									//$to     = '';
									$cc_to_sender=$cc=$bcc='';
									$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
									'email' => $smeerp_client_email); 
									$from= $reply_to= array('name' => $email["from_name"], 'email' => $email['from_email']); 
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
									$cc_to_sender,$cc,$bcc,$file_name);
								}    
								//Send one copy on clients EOF
							} 
							$messages->setOkMessage("Password change successufully. Check ticket no.".$ticket_no);
							
						}else{
							
							$messages->setErrorMessage("Sorry password can not changed. Please try again.<br>Check your email-id".$httpResponse);
						}
					
					}
					if($data['form_type']=='delete_email'){
					
					
					}
					if($data['form_type']=='allot_quota'){
					
					
					}
					 
					
					
					
					
					
					
				} 
				
				$condition_query1 = " WHERE user_id = '". $data['ticket_owner_uid'] ."' ";
				if ( Clients::getList($db, $_ALL_POST['client_details'], 'user_id, f_name,l_name,additional_email,status,send_mail,org,billing_name, payment_remarks,
					email,email_1,email_2, email_3,email_4,mobile1, mobile2,authorization_id', $condition_query1) > 0 ) {
					$_ALL_POST['user_id']=$_ALL_POST['client_details']['0']['user_id'];
					$_ALL_POST['name']=$_ALL_POST['client_details']['0']['f_name']." ".$_ALL_POST['client_details']['0']['l_name'];
					//$_ALL_POST['email']=$_ALL_POST['email'];
					$_ALL_POST['billing_name']=$_ALL_POST['client_details']['0']['billing_name'];
					$_ALL_POST['payment_remarks']=$_ALL_POST['client_details']['0']['payment_remarks'];
					$_ALL_POST['additional_email']=$_ALL_POST['client_details']['0']['additional_email'];
					$_ALL_POST['mobile1']=$_ALL_POST['client_details']['0']['mobile1'];
					$_ALL_POST['mobile2']=$_ALL_POST['client_details']['0']['mobile2'];
					$_ALL_POST['send_mail']=$_ALL_POST['client_details']['0']['send_mail'];
					$_ALL_POST['email']=$_ALL_POST['client_details']['0']['email'];
					$_ALL_POST['email_1']=$_ALL_POST['client_details']['0']['email_1'];
					$_ALL_POST['email_2']=$_ALL_POST['client_details']['0']['email_2'];
					$_ALL_POST['email_3']=$_ALL_POST['client_details']['0']['email_3'];
					$_ALL_POST['email_4']=$_ALL_POST['client_details']['0']['email_4'];
					$all_additional_email = trim($_ALL_POST['additional_email'],",");
					$additionalEmailArr = explode(",",$all_additional_email);
					$_ALL_POST['accountValid'] = 0;
					if(!empty($invoice_id) || !empty($invoice_profm_id) || !empty($flw_ord_id)){
						$authorization_id = trim($_ALL_POST['client_details']['0']['authorization_id'],",");
						$authorizationArr = explode(",",$authorization_id);
						$isValid = in_array(Clients::ACCOUNTS,$authorizationArr);
						$_ALL_POST['accountValid'] = $isValid ;
					} 
				}
				
				if(isset($data['mail_to_all_su'])){
				   $_ALL_POST['mail_to_all_su']=$data['mail_to_all_su'];
				}
				if(isset($data['mail_to_su'])){
					$_ALL_POST['mail_to_su']=$data['mail_to_su'];
				}
				//BOF read the available domains
				// SupportTicket::getDomains($db,$domains,$data['ticket_owner_uid']);
				//EOF read the available domains
				$subUserDetails=null;  
				$subUserDetailsAccts=$subUserDetailsMgmts=$subUserDetailsAuth=$subUserDetailsUsr=$subUserDetailsDnd=$subUserDetailsDir=array();
				$followupSql= ""; 
				//Director BOF
				$followupSql = " AND authorization_id  LIKE '%,".Clients::DIRECTOR.",%'" ;
				$suCondition = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."') AND status='".Clients::ACTIVE."'  
				".$followupSql." ORDER BY f_name ";
				Clients::getList($db, $subUserDetailsDir1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
				email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition);  
				if(!empty($subUserDetailsDir1)){
					foreach($subUserDetailsDir1 as $key_su => $val_su){
						$authorizationNameSu='';
						$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
						$authorizationArr1 = explode(",",$val_su['authorization_id']);
						 
						if(!empty($authorizationArr1)){
							foreach($authorizationArr1 as $keys=>$vals){
								if($clientAuth[$vals]=="DND"){
									$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
								}
								$authorizationNameSu.= $clientAuth[$vals].", ";
							}
						}							
						$val_su['authorizationNameSu'] = $authorizationNameSu ;
						$subUserDetailsDir[$key_su] = $val_su; 
					}
				} 
				//Director EOF
				//Accounts BOF
				$followupSql = " AND authorization_id  LIKE '%,".Clients::ACCOUNTS.",%'" ;
				$suCondition = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."') AND status='".Clients::ACTIVE."'  
				".$followupSql." ORDER BY f_name ";
				Clients::getList($db, $subUserDetailsAccts1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
				email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition);  
				if(!empty($subUserDetailsAccts1)){
					foreach($subUserDetailsAccts1 as $key_su => $val_su){
						$authorizationNameSu='';
						$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
						$authorizationArr1 = explode(",",$val_su['authorization_id']);
						 
						if(!empty($authorizationArr1)){
							foreach($authorizationArr1 as $keys=>$vals){
								if($clientAuth[$vals]=="DND"){
									$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
								}
								$authorizationNameSu.= $clientAuth[$vals].", ";
							}
						}							
						$val_su['authorizationNameSu'] = $authorizationNameSu ;
						$subUserDetailsAccts[$key_su] = $val_su; 
					}
				} 
				//Accounts EOF
				//MANAGEMENT BOF
				$followupSql1 = " AND authorization_id  LIKE '%,".Clients::MANAGEMENT.",%'" ;
				$suCondition1 = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."')
				AND status='".Clients::ACTIVE."'  
				".$followupSql1." ORDER BY f_name ";
				Clients::getList($db, $subUserDetailsMgmts1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
				email,check_email,email_1,email_2, email_3,email_4, mobile1,mobile2,authorization_id', $suCondition1); 
				if(!empty($subUserDetailsMgmts1)){
					foreach($subUserDetailsMgmts1 as $key_su => $val_su){
						$authorizationNameSu='';
						$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
						$authorizationArr1 = explode(",",$val_su['authorization_id']);
						 
						if(!empty($authorizationArr1)){
							foreach($authorizationArr1 as $keys=>$vals){
								if($clientAuth[$vals]=="DND"){
									$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
								}
								$authorizationNameSu.= $clientAuth[$vals].", ";
							}
						}							
						$val_su['authorizationNameSu'] = $authorizationNameSu ;
						$subUserDetailsMgmts[$key_su] = $val_su; 
					}
				} 
				//MANAGEMENT EOF
				//Authorised BOF
				$followupSql2 = " AND authorization_id  LIKE '%,".Clients::CONFIDENTIAL.",%'" ;
				$suCondition2 = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."')
				AND status='".Clients::ACTIVE."'  
				".$followupSql2." ORDER BY f_name ";
				Clients::getList($db, $subUserDetailsAuth1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
				email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition2); 
				if(!empty($subUserDetailsAuth1)){
					foreach($subUserDetailsAuth1 as $key_su => $val_su){
						$authorizationNameSu='';
						$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
						$authorizationArr1 = explode(",",$val_su['authorization_id']);
						 
						if(!empty($authorizationArr1)){
							foreach($authorizationArr1 as $keys=>$vals){
								if($clientAuth[$vals]=="DND"){
									$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
								}
								$authorizationNameSu.= $clientAuth[$vals].", ";
							}
						}							
						$val_su['authorizationNameSu'] = $authorizationNameSu ;
						$subUserDetailsAuth[$key_su] = $val_su; 
					}
				} 
				
				//Authorised EOF
				//USER BOF
				$followupSql3 = " AND authorization_id  LIKE '%,".Clients::USER.",%'" ;
				$suCondition3 = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."') 
				AND status='".Clients::ACTIVE."'  
				".$followupSql3." ORDER BY f_name ";
				Clients::getList($db, $subUserDetailsUsr1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
				email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition3); 
				if(!empty($subUserDetailsUsr1)){
					foreach($subUserDetailsUsr1 as $key_su => $val_su){
						$authorizationNameSu='';
						$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
						$authorizationArr1 = explode(",",$val_su['authorization_id']);
						 
						if(!empty($authorizationArr1)){
							foreach($authorizationArr1 as $keys=>$vals){
								if($clientAuth[$vals]=="DND"){
									$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
								}
								$authorizationNameSu.= $clientAuth[$vals].", ";
							}
						}							
						$val_su['authorizationNameSu'] = $authorizationNameSu ;
						$subUserDetailsUsr[$key_su] = $val_su; 
					}
				} 
				//USER EOF
				//DND BOF
				$followupSql4 = " AND authorization_id  LIKE '%,".Clients::DND.",%'" ;
				$suCondition4 = " WHERE ( parent_id = '".$_ALL_POST['ticket_owner_uid']."' OR user_id='".$_ALL_POST['ticket_owner_uid']."')
				AND status='".Clients::ACTIVE."'  
				".$followupSql4." ORDER BY f_name ";
				Clients::getList($db, $subUserDetailsDnd1, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
				email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2,authorization_id', $suCondition4); 
				if(!empty($subUserDetailsDnd1)){
					foreach($subUserDetailsDnd1 as $key_su => $val_su){
						$authorizationNameSu='';
						$val_su['authorization_id'] = trim($val_su['authorization_id'],",");
						$authorizationArr1 = explode(",",$val_su['authorization_id']);
						 
						if(!empty($authorizationArr1)){
							foreach($authorizationArr1 as $keys=>$vals){
								if($clientAuth[$vals]=="DND"){
									$clientAuth[$vals] = "<span class='pre_color'><b>".$clientAuth[$vals]."</b></span>";
								}
								$authorizationNameSu.= $clientAuth[$vals].", ";
							}
						}							
						$val_su['authorizationNameSu'] = $authorizationNameSu ;
						$subUserDetailsDnd[$key_su] = $val_su; 
					}
				} 
			}
		}
		if(!isset($_ALL_POST ['form_type']) ){
			$_ALL_POST['form_type']='change_pwd';
		}
		/*
		$url="https://httpapi.com/api/domains/search.json?auth-userid=35247&api-key=qE1QP2Kd9xEbx6U0xICEi29nPvSbQ8tW&no-of-records=".$rpp."&page-no=".$x.$sub_url."&order-by=".$sOrderBy."+".$sOrder;
		$_SEARCH["searched"]    = true ;
		$curl_session = curl_init();
		curl_setopt($curl_session, CURLOPT_URL, $url); 
		curl_setopt($curl_session, CURLOPT_HEADER, false);
        curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl_session);
        curl_close($curl_session); 
		$dataArr=json_decode($response, true);		 
		$total = $dataArr['recsindb'];
		
		unset($dataArr['recsindb']);
		unset($dataArr['recsonpage']);
		
		//print_R($dataArr);
		if(!empty($dataArr)){
			foreach($dataArr as $key => $value)	{
				$dataArr1=array();
				foreach($value as $key1 => $value1)	{ 
					$key2 = str_replace('.','_',$key1);
					$dataArr1[$key2] = $value1; 
				}
				$dataArr2[]=$dataArr1; 
			}
		}
		 
		$condition_url.="&dt_field=".$dt_field."&perform=".$perform;     
		//$pagination = ajaxPagination($total, $x, $rpp, 'changeRecordList','support-ticket.php','frmSearch');
		$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');        
        
		*/
		

	
		// Set the Permissions.
        $variables['can_view_client_details']  = false;
		$variables['can_select_team']  = false;         
		if ( $perm->has('nc_uc_contact_details') ) {
			$variables['can_view_client_details'] = true;
		}
		if ( $perm->has('nc_st_select_team') ) {
			$variables['can_select_team'] = true;
		}	
	   
        
		$page["var"][] = array('variable' => 'additionalEmailArr', 'value' => 'additionalEmailArr');
		$page["var"][] = array('variable' => 'subUserDetailsAccts', 'value' => 'subUserDetailsAccts');
		$page["var"][] = array('variable' => 'subUserDetailsMgmts', 'value' => 'subUserDetailsMgmts');
		$page["var"][] = array('variable' => 'subUserDetailsAuth', 'value' => 'subUserDetailsAuth');
		$page["var"][] = array('variable' => 'subUserDetailsUsr', 'value' => 'subUserDetailsUsr');
		$page["var"][] = array('variable' => 'subUserDetailsDnd', 'value' => 'subUserDetailsDnd');
		$page["var"][] = array('variable' => 'subUserDetailsDir', 'value' => 'subUserDetailsDir');
	    $page["var"][] = array('variable' => 'step1', 'value' => 'step1');
        $page["var"][] = array('variable' => 'step2', 'value' => 'step2');
        $page["var"][] = array('variable' => 'perform', 'value' => 'change-pwd');
        $page["var"][] = array('variable' => 'dataArr', 'value' => 'dataArr2');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'api-email-change-pwd.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>