<?php
    if ( $perm->has('nc_nwlm_csv_upload') ) {
        $_ALL_POST  = NULL;
        $data       = NULL;
        $messages_ask 	= new Messager;	// create the object for the Message class. 
        $CSV = NewslettersMembers::getRestrictions();
        // The table in which the data will be imported.
       // $db_ask 		= new DB_Local_Ask; // database handle
       // $db_task 		= new DB_Local_Task; // database handle
       // $db_buzz 		= new DB_Local_Buzz; // database handle
		
    
        if( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save'){
            $_ALL_POST  = $_POST;
            $post_data  = processUserData($_ALL_POST);          
			$files       = processUserData($_FILES);
    		$ask_added      = 0;
			$ask_duplicate  = 0;
			$ask_empty      = 0; 
            $extra = array( 'file'      => &$files,
                            'CSV'       => $CSV,
                            'messages'  => &$messages
                        );
			
			/* $extra_ask = array( 
				'messages_ask'  => &$messages_ask,
				'ask_added'=>&$ask_added,
				'ask_empty'=>&$ask_empty,
				'ask_duplicate'=>&$ask_duplicate
			); */
            if ( NewslettersMembers::validateUpload($post_data, $extra) ){
                // Read the file.
                $row        = 0;
                $added      = 0;
                $duplicate  = 0;
                $invalid  = 0;
                $empty      = 0;
                $index      = array();
                $handle     = fopen($_FILES['file_csv']['tmp_name'],"rb");
                $message_duplicate = $message = '';
                while ( $data = fgetcsv($handle, $_FILES['file_csv']['size'], ",") ) {
                    $data = processUserData($data);
                    $row  += 1;
                    // If the first Row contains the Fields names then use this else remove this code
                    // and enter the indexes manually.
                    if ( $row == 1 ) {
                        $num = count($data);
                        for ($i=0; $i < $num; $i++) {
                            $case = trim($data[$i]);
                            switch ($case) {
                                case ('Email'): {
                                    $index['Email'] = $i;
                                    break;
                                }
								case ('First-Name'): {
                                    $index['First-Name'] = $i;
                                    break;
                                }
                                case ('Last-Name'): {
                                    $index['Last-Name'] = $i;
                                    break;
                                }
                               
                                case ('Mobile1'): {
                                    $index['Mobile1'] = $i;
                                    break;
                                }
                                case ('Mobile2'): {
                                    $index['Mobile2'] = $i;
                                    break;
                                }
                                case ('City'): {
                                    $index['City'] = $i;
                                    break;
                                }
                                case ('State'): {
                                    $index['State'] = $i;
                                    break;
                                }
                                case ('Country'): {
                                    $index['Country'] = $i;
                                    break;
                                }
                                case ('Address'): {
                                    $index['Address'] = $i;
                                    break;
                                }
                                case ('Phone'): {
                                    $index['Phone'] = $i;
                                    break;
                                }                                
                            }
                        }
                    }
                    else {                 
                          $index= array (
							"Email"    =>"0",
                            "First-Name" => "1",
                            "Last-Name" =>"2",                           
                            "Mobile1" =>"3",
                            "Mobile2" =>"4",
                            "City" =>"5",
                            "State" =>"6",
                            "Country" =>"7",
                            "Address" =>"8",
                            "Phone" =>"9"
                           );
						   
                        if ( isset($index['Email']) && $data[$index['Email']] != '' ) {
                             
                            if ( !isValidEmail($data[$index['Email']]) ) {
									//$messages->setErrorMessage('The Primary Email Address is not valid.');
									$message.= $data[$index['Email']]." is not a valid email. <br/>";								 
									$invalid += 1; 
							}else{ 
								//Insert Newsletter Members BOF
								$table=TABLE_NEWSLETTERS_MEMBERS;
								$cond1 = " WHERE email='".strtolower(trim($data[$index['Email']]))."' AND nws_category_id ='".$post_data['nws_category_id']."'" ;
								
								if ( !NewslettersMembers::duplicateFieldValue($db, $table,'email', $cond1) ) {
								
									$query	= " INSERT INTO ".TABLE_NEWSLETTERS_MEMBERS
									." SET ".TABLE_NEWSLETTERS_MEMBERS .".f_name = 
									   '". processUserData($data[$index['First-Name']]) ."'" 
									.",". TABLE_NEWSLETTERS_MEMBERS .".l_name = '".
									processUserData($data[$index['Last-Name']])."'"  
									.",". TABLE_NEWSLETTERS_MEMBERS .".email = '".
									processUserData(strtolower(trim($data[$index['Email']])))."'"   
									.",". TABLE_NEWSLETTERS_MEMBERS .".mobile_1 = '".
									processUserData($data[$index['Mobile1']])."'" 
									.",". TABLE_NEWSLETTERS_MEMBERS .".mobile_2 = '".
									processUserData($data[$index['Mobile2']])."'"   
									.",". TABLE_NEWSLETTERS_MEMBERS .".city = '".
									processUserData($data[$index['City']])."'"	
									.",". TABLE_NEWSLETTERS_MEMBERS .".state = '".
									processUserData($data[$index['State']])."'"	
									.",". TABLE_NEWSLETTERS_MEMBERS .".country_name = '".
									processUserData($data[$index['Country']])."'"
									.",". TABLE_NEWSLETTERS_MEMBERS .".address = '".
									processUserData($data[$index['Address']])."'"	
									.",". TABLE_NEWSLETTERS_MEMBERS .".phone = '".
									processUserData($data[$index['Phone']])."'"									
									.",". TABLE_NEWSLETTERS_MEMBERS .".nws_category_id = '".$post_data['nws_category_id'] ."'"
									.",". TABLE_NEWSLETTERS_MEMBERS .".added_by = '". $my['user_id'] ."'"
									.",". TABLE_NEWSLETTERS_MEMBERS .".added_by_name = '". $my['f_name']." ".
									$my['l_name']."'"
									.",". TABLE_NEWSLETTERS_MEMBERS .".ip    = '". $_SERVER['REMOTE_ADDR'] ."'"
									.",". TABLE_NEWSLETTERS_MEMBERS .".site_id    = '".SITE_ID."'"
									.",". TABLE_NEWSLETTERS_MEMBERS .".do_e  = '". 	date('Y-m-d H:i:s')."'";
								  
									$db->query($query);
									$added += 1;
									//Insert Newsletter Members EOF
								
								}else{
									$duplicate += 1;
									$message_duplicate.= $data[$index['Email']]." is already exist in selected category. <br/>";
								}
								
								//DO for ASK BOF
								/*
								$otherData['first_name']= processUserData($data[$index['First-Name']]);
								$otherData['last_name']= processUserData($data[$index['Last-Name']]);
								$otherData['email']= processUserData($data[$index['Email']]);
								$otherData['mobile_1']= processUserData($data[$index['Mobile1']]);
								$otherData['mobile_2']= processUserData($data[$index['Mobile2']]);
								$otherData['city']= processUserData($data[$index['City']]);
								$otherData['state']= processUserData($data[$index['State']]);
								$otherData['country']= processUserData($data[$index['Country']]);
								$otherData['address']= processUserData($data[$index['Address']]);
								$otherData['phone']= processUserData($data[$index['Phone']]);
															
								$otherData['first_name']= $data[$index['First-Name']];
								$otherData['last_name']= $data[$index['Last-Name']];
								$otherData['email']= $data[$index['Email']];
								$otherData['mobile_1']= $data[$index['Mobile1']];
								$otherData['mobile_2']= $data[$index['Mobile2']];
								$otherData['city']= $data[$index['City']];
								$otherData['state']= $data[$index['State']];
								$otherData['country']= $data[$index['Country']];
								$otherData['address']= $data[$index['Address']];
								$otherData['phone']= $data[$index['Phone']];
								*/	
								//NewslettersMembers::askDataEntry($db_ask,$otherData,$extra_ask); 
								 
								//DO for ASK EOF 
								
							}
                        }else {
                            $empty += 1;
                        }    
                        
                        
                        // The Row is not the First row.
                        /*
                        if ( isset($index['user_id']) && $data[$index['user_id']] != '' ) {
                            if ( !CSV::duplicateFieldValue($db, $table, 'user_id', $data[$index['user_id']]) ) {
                                // Prepare the query using the index created above.
                                $query = " SET ";
								
									foreach ( $index as $field=>$f_index ) {
										$query .= "$field = '". $data[$f_index] ."', ";
									}
								
                                $query = substr($query, 0, (strlen($query)-2) );
                                // To INSERT.
                                $query = "INSERT INTO $table ". $query;
                                if ( $db->query($query) && $db->affected_rows()>0 ) {
                                    $added += 1;
                                }
                            }
                            else {
                                $duplicate += 1;
                            }
                        }
                        else {
                            $empty += 1;
                        }*/
                    }                    
                }
                if ($added)
                    $messages->setOkMessage($added .' out of '. $row .' records have been added.');
                if ($duplicate)
                    $messages->setOkMessage($duplicate .' out of '. $row .' records were duplicate and neglected.'.$message_duplicate);
                if ($invalid)
                    $messages->setOkMessage($invalid .' out of '. $row .' email invalid records found. <br/>'.$message);
				if ($empty)
                    $messages->setOkMessage($empty .' out of '. $row .' records were empty and neglected.');
            }
			 
        }
       
        // Check if the Form to upload is to be displayed or the control is to be sent to the List page.
		$fields1= "id,category";
		$condition_query1 = " WHERE ".TABLE_NEWSLETTERS_MEMBERS_CATEGORY.".status='1' ";
		NewslettersMembers::getCategory($db, $category_list,$fields1, $condition_query1); 
		
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
			$condition ='';
            include ( DIR_FS_NC .'/newsletters-members-csv.php');
        }
        else {
			
			 // Set the Permissions.
			$variables['can_add']     = false;
			$variables['can_view_list']     = false;
			if ( $perm->has('nc_nwlm_list') ) {
				$variables['can_view_list']     = true;
			}
			if ( $perm->has('nc_nwlm_csv_upload') ) {
				$variables['can_add']     = true;
			}
            
            $hidden[] = array('name'=> 'perform' ,'value' => 'csv_upload');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            
            //$page["var"][] = array('variable' => 'table_list', 'value' => 'table_list');
            $page["var"][] = array('variable' => 'CSV', 'value' => 'CSV');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletters-members-csv.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>