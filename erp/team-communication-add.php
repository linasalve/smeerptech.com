<?php
    if ( $perm->has('nc_tcm_add')) {
        $subUserDetails=$additionalEmailArr= null;
      
						
		$_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates=null;
		$_ALL_POST = isset ($_POST['perform']) ? $_POST : ( isset($_GET['perform'] ) ? $_GET : NULL);
       
		
	    
      
        //$condition_query_st = " WHERE status='".SupportTicketTemplate::ACTIVE."'";
        //SupportTicketTemplate::getDetails( $db, $stTemplates, 'id,title', $condition_query_st);
         $stTemplates=array();
        $domains = array();
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["status"] = TeamCommunication::getTicketStatusList();
		
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
		// hrs, min.
        if ( (isset($_ALL_POST['btnCreate']) || isset($_ALL_POST['btnReturn'])) && $_ALL_POST['act'] == 'save') {
            //$_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
          
			if ( TeamCommunication::validateAdd($data, $extra) ) {
				
				//$ticket_no  =  "PT". date("ymdhi-s");
				$ticket_no  =  TeamCommunication::getNewNumber($db);
				$attachfilename=$ticket_attachment_path='';
				if(!empty($data['ticket_attachment'])){
					$filedata["extension"]='';
					$filedata = pathinfo($files['ticket_attachment']["name"]);
				   
					$ext = $filedata["extension"] ; 
					//$attachfilename = $username."-".mktime().".".$ext ;
				   
					$attachfilename = $ticket_no.".".$ext ;
					$data['ticket_attachment'] = $attachfilename;
					
					$ticket_attachment_path = DIR_WS_TM_FILES;
					
					if(move_uploaded_file($files['ticket_attachment']['tmp_name'], 
						DIR_FS_TM_FILES."/".$attachfilename)){
						@chmod(DIR_FS_TM_FILES."/".$attachfilename, 0777);
					}
				}
				
				
				$assign_members_str = implode(",",$data['assign_members']);
				$assign_members_str = ",".$assign_members_str.",";
				
				$assign_members_details_str = implode(",",$data['assign_members_details']);
				$assign_members_details_str = ",".$assign_members_details_str.",";
				
				$mail_to_all_su=0;
				if(isset($data['mail_to_all_su']) ){
					$mail_to_all_su=1;
				}
				$mail_to_additional_email=0;
				if(isset($data['mail_to_additional_email']) ){
					$mail_to_additional_email=1;
				}
				$mail_client=0;
				if(isset($data['mail_client']) ){
					$mail_client=1;
				}
				
				if(!isset($data['ticket_status']) ){                    
					$ticket_status = TeamCommunication::OPEN;
				}else{
					$ticket_status = $data['ticket_status'];
				}
				$hrs1 = (int) ($data['hrs'] * 6);
				$min1 = (int) ($data['min'] * 6);     
				
				$query = "INSERT INTO "	. TABLE_TM_COMMUNICATIONS ." SET "
					. TABLE_TM_COMMUNICATIONS .".ticket_no          = '". $ticket_no ."', "					
					. TABLE_TM_COMMUNICATIONS .".ticket_owner_uid   = '". $my['uid'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_owner       = '". $my['f_name']." ". $my['l_name']."', "
					. TABLE_TM_COMMUNICATIONS .".on_behalf          = '". $data['on_behalf'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_creator_uid = '". $my['uid'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_subject    = '". $data['ticket_subject'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_text       = '". $data['ticket_text'] ."', "
					. TABLE_TM_COMMUNICATIONS .".assign_members 	= '".$assign_members_str."', "
					. TABLE_TM_COMMUNICATIONS .".assign_members_name  = '".$assign_members_details_str."', "
					//. TABLE_TM_COMMUNICATIONS .".mail_to_all_su        = '".$mail_to_all_su."', "
					//. TABLE_TM_COMMUNICATIONS .".mail_to_additional_email = '".$mail_to_additional_email."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_status     = '".$ticket_status ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_child      = '0', "
					. TABLE_TM_COMMUNICATIONS .".ticket_attachment = '". $attachfilename ."', "
					. TABLE_TM_COMMUNICATIONS .".hrs1 = '". $hrs1 ."', "
					. TABLE_TM_COMMUNICATIONS .".min1 = '". $min1 ."', "
					. TABLE_TM_COMMUNICATIONS .".hrs = '". $data['hrs'] ."', "
					. TABLE_TM_COMMUNICATIONS .".min = '". $data['min']."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_response_time = '0', "
					. TABLE_TM_COMMUNICATIONS .".ticket_replied        = '0', "
					. TABLE_TM_COMMUNICATIONS .".from_admin_panel        = '".TeamCommunication::ADMIN_PANEL."', "
					. TABLE_TM_COMMUNICATIONS .".is_private           = '". $data['is_private'] ."', "
					. TABLE_TM_COMMUNICATIONS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
					. TABLE_TM_COMMUNICATIONS .".do_e    = '".date('Y-m-d H:i:s')."', "
					. TABLE_TM_COMMUNICATIONS .".do_comment   = '".date('Y-m-d H:i:s')."', "
					. TABLE_TM_COMMUNICATIONS .".last_comment_from = '".TeamCommunication::ADMIN_COMMENT."', "
					. TABLE_TM_COMMUNICATIONS .".ticket_date       = '". time() ."' " ;
				
				//unset($GLOBALS["flag"]);
				//unset($GLOBALS["submit"]);
							
				if ($db->query($query) && $db->affected_rows() > 0) {
					
					$variables['hid'] = $db->last_inserted_id() ;
					
					
					/**************************************************************************
					* Send the notification to the ticket owner and the Administrators
					* of the new Ticket being created
					**************************************************************************/
					if($ticket_status==TeamCommunication::OPEN){
						$ticket_status_name = 'OPEN';
					}else{
						$ticket_status_name = 'CLOSED';
					}
					
					$data['status_name']    = $ticket_status_name;
					$data['assign_members_name']  = trim($assign_members_details_str,",");
					$data['ticket_no']    =   $ticket_no ;
					$data['mticket_no']   =   $ticket_no ;
					$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
					$data['subject']    =     $_ALL_POST['ticket_subject'] ;
					$data['text']    =   $_ALL_POST['ticket_text'] ;
					$data['attachment']   =   $attachfilename ;
					$data['link']   = DIR_WS_MP .'/team-communication.php?perform=view&ticket_id='. $variables['hid'];
					$file_name = DIR_FS_TM_FILES ."/". $attachfilename;
					
					$sms_to_number = $sms_to =array();
					$mobile1 = $mobile2='';
					$counter=0;
					$smsLength = strlen($data['text']); 
					$assign_members_str = str_replace(",","','", trim($assign_members_str,","));
					User::getList($db, $userDetailsList, 'number, f_name, l_name, email,email_1,email_2,send_mail,
					title as ctitle,  mobile1, mobile2 ', " WHERE user_id IN  ('".$assign_members_str."')");
					if(!empty($userDetailsList)){
						$sms_to_number = $sms_to =array(); 
						foreach ($userDetailsList  as $key=>$userDetails){
							
							$mobile1 = $mobile2='';
							$data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
							$data['ctitle']    =   $userDetails['ctitle'];		
							$send_mail		= $userDetails['send_mail'];							
							$email = NULL;	
							if($send_mail){
								if ( getParsedEmail($db, $s, 'EMAIL_TC_MEMBER', $data, $email) ) {                                    
									$to = '';
									 
									$to[]   = array('name' => $userDetails['f_name'] .' '. 
									$userDetails['l_name'], 'email' => 
									$userDetails['email']);                                   
									$from   = $reply_to = array('name' => $email["from_name"],
									'email' => $email['from_email']);
									$mail_send_to_su  .= ",".$userDetails['email'];
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,
									$cc,$bcc,$file_name);
								   /*
									if(!empty($userDetails['email_1'])){                                       
										$to = '';
										$to[]   = array('name' => $userDetails['f_name'] .' '.
										$userDetails['l_name'], 'email' =>
										$userDetails['email_1']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_1'];
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);  
									}
									if(!empty($userDetails['email_2'])){                                       
										$to = '';
										$to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 'email' =>
										$userDetails['email_2']);                                   
										$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email_2'];
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"],
										$email["isHTML"],    $cc_to_sender,$cc,$bcc,$file_name); 
																  
									} */
									//Send a copy to check the mail for clients
									if(!empty($smeerp_client_email)){
										$to     = '';
										$cc_to_sender=$cc=$bcc='';
										$to[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
										'email' => $smeerp_client_email);
										$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
										SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
										$cc_to_sender,$cc,$bcc,$file_name);
									}  
								}
							}
								if(isset($data['send_sms']) && $smsLength<=130 && !empty($userDetails['mobile1'])){
									//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
									$sms_to_number[] =$userDetails['mobile1'];
									$mobile1 = "91".$userDetails['mobile1'];
									$client_mobile .= ','.$mobile1;
									ProjectTask::sendSms($data['text'],$mobile1);
									$counter = $counter+1;
										
								}
								if(isset($data['send_sms']) && $smsLength<=130 && !empty($userDetails['mobile2'])){
									//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
									$sms_to_number[] =$userDetails['mobile2'] ;
									$mobile2 = "91".$userDetails['mobile2'];
									$client_mobile .= ','.$mobile2;
									ProjectTask::sendSms($data['text'],$mobile2);
									$counter = $counter+1;                                        
								}
								if(!empty($mobile1) || !empty($mobile2)){
									 $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
								}
								   
							
						}
					}
				   //Set the list subuser's email-id to whom the mails are sent bof
					$query_update = "UPDATE ". TABLE_TM_COMMUNICATIONS 
								." SET ". TABLE_TM_COMMUNICATIONS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', sms_to_number='".implode(",",array_unique($sms_to_number))."',
									sms_to ='".implode(",",array_unique($sms_to))."' "                                    
								." WHERE ". TABLE_TM_COMMUNICATIONS .".ticket_id = '". $variables['hid'] ."' " ;
					$db->query($query_update) ;
					//Set the list subuser's email-id to whom the mails are sent eof                        
					//update sms counter
					if($counter >0){
						$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
						$db->query($sql); 
					}
					// Send Email to the admin BOF
					$email = NULL;
					$data['link']   = DIR_WS_NC .'/team-communication.php?perform=view&ticket_id='. $variables['hid'];
					$data['quicklink']   = DIR_WS_SHORTCUT .'/team-communication.php?perform=view&ticket_id='. $variables['hid'];      
					$data['main_client_name']   = $main_client_name;
					$data['billing_name']   = $billing_name;
					$cc_to_sender= $cc = $bcc = Null;
					if ( getParsedEmail($db, $s, 'EMAIL_TC_ADMIN', $data, $email) ) {
						$to = '';
						$to[]   = array('name' => $team_communication_name , 'email' => $team_communication_email);                           
						$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
						 
						SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
						$cc_to_sender,$cc,$bcc,$file_name);
					}                        
					// Send Email to the admin EOF
				 
					$messages->setOkMessage("New Support Ticket has been created.And email Notification has been sent.");
				}
				//to flush the data.
				$_ALL_POST	= NULL;
				$data		= NULL;
			}  
            
        }
        
        if(isset($_POST['btnCreate'])  && $messages->getErrorMessageCount() <= 0 ){
			 
				header("Location:".DIR_WS_NC."/team-communication.php?perform=view&added=1&ticket_id=".$variables['hid']);
			 
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/team-communication-list.php');
        }
        else {
        
          

           
            $hidden[] = array('name'=> 'perform','value' => $perform); 
            
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //$hidden[] = array('name'=> 'user_id', 'value' => $user_id);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST'); 
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
			
			$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
			$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            //$page["var"][] = array('variable' => 'invoice_id', 'value' => 'invoice_id');
            //$page["var"][] = array('variable' => 'flw_ord_id', 'value' => 'flw_ord_id');
            //$page["var"][] = array('variable' => 'pendingInvlist', 'value' => 'fpendingInvlist');
			//$page["var"][] = array('variable' => 'totalBalanceAmount', 'value' => 'totalBalanceAmount');
            $page["var"][] = array('variable' => 'domains', 'value' => 'domains');
             
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'team-communication-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>