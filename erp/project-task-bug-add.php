<?php
if ( $perm->has('nc_p_tsb_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        		        
		// Include the 
		include_once (DIR_FS_INCLUDES .'/project-task.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-module.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-priority.inc.php');
		
		include_once (DIR_FS_INCLUDES .'/project-resolution.inc.php');
		include_once (DIR_FS_INCLUDES .'/project-type.inc.php');
		include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
        
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        }   
       
		$_ALL_POST 			=  ( isset($_POST)    ? processUserData($_POST)       :'');
       
        
        //Get all req array BOF     
      
        $moduleArr	= NULL;
        $condition_querym = " WHERE parent_id='0' AND order_id='".$or_id."' AND status ='".ProjectTask::ACTIVE."'";
        ProjectModule::getList( $db, $moduleArr, 'id, title', $condition_querym);
        
        $query = "SELECT ".TABLE_PROJECT_TASK_DETAILS.".module_id,".TABLE_PROJECT_TASK_DETAILS.".sub_module_id"." FROM ".TABLE_PROJECT_TASK_DETAILS." WHERE project_id='".$or_id."' AND id='".$task_id."'";
        
        if ( $db->query($query) ) {
            if ( $db->nf() > 0 ) {
                while ($db->next_record()) {
                    $_ALL_POST['module_id'] = $db->f('module_id');
                    $_ALL_POST['sub_module_id'] = $db->f('sub_module_id');
                }
            }
        }
        
        $submoduleArr	= NULL;
        $condition_querysm = " WHERE parent_id='".$_ALL_POST['module_id']."' AND order_id='".$or_id."' AND status ='".ProjectModule::ACTIVE."' ";
        ProjectModule::getList( $db, $submoduleArr, 'id, title', $condition_querysm);
        
        $priorityArr	= NULL;
        $condition_queryp = " WHERE  status ='".ProjectTask::ACTIVE."'";
        ProjectPriority::getList( $db, $priorityArr, 'id, title', $condition_queryp);
        
        $resolutionArr	= NULL;
        $condition_queryr = " WHERE status ='".ProjectTask::ACTIVE."'";
        ProjectResolution::getList( $db, $resolutionArr, 'id, title', $condition_queryr);
        
        $typeArr	= NULL;
        $condition_queryt = " WHERE status ='".ProjectTask::ACTIVE."'";
        ProjectType::getList( $db, $typeArr, 'id, title', $condition_queryt);
        //Get all req array EOF
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files		= processUserData($_FILES);
          
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
                            'files'          => &$files,
							'messages'          => &$messages
                        );
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size']= MAX_MULTIPLE_FILE_SIZE ;       
            
            if ( ProjectTask::validateBugAdd($data, $extra) ) {
                $is_visibleto_client = 0;
                
                if(isset($data['is_visibleto_client'])){
                    $is_visibleto_client = 1;
                }
              
                $i=1;
                $attachfilename='';
                if(!empty($files["attached_file"]["name"])){
                    
                    //$val["name"] = str_replace(' ','-',$val["name"]);
                    $filedata["extension"]='';
                    $filedata = pathinfo($files["attached_file"]["name"]);
                   
                    $ext = $filedata["extension"] ; 
                    //$attachfilename = $username."-".mktime().".".$ext ;
                   
                    $attachfilename = mktime()."-".$i.".".$ext ;
                  
                   if(!empty($files["attached_file"]["name"])){
                 
                        if (move_uploaded_file($files["attached_file"]['tmp_name'], DIR_FS_ATTACHMENTS_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_ATTACHMENTS_FILES."/".$attachfilename, 0777);
                            $attached_type = $files["attached_file"]["type"] ;
                        }
                    }
                    $i++;
                }
               $hrs1 = (int) ($data['hrs'] * 3);
               $min1 = (int) ($data['min'] * 3);
               $last_updated = $datetime->unixTimeToDate(time()); 
               //Mark send notify BOF                        
                $mail_client=0;
                if(isset($_POST['mail_client']) ){
                    $mail_client=1;
                }
                $mail_staff=0;
                if(isset($_POST['mail_staff']) ){
                    $mail_staff=1;
                }
                $mark_imp=0;
                if(isset($_POST['mark_imp']) ){
                    $mark_imp=1;
                }
				$current_salary = $my['current_salary'];
				$per_hour_salary = $my['current_salary'] / STD_WORKING_HRS;
				$per_hour_salary =  number_format($per_hour_salary,2);
				$worked_min = ( $data['hrs']*60 ) + $data['min'] ;
				$worked_hour = ($worked_min/60 ) ;
				$worked_hour =  number_format($worked_hour,2);
				$worked_hour_salary = ( $per_hour_salary * $worked_hour );
                //Mark send notify EOF
				  $query = "INSERT INTO ".TABLE_PROJECT_TASK_DETAILS
							  ." SET "
							  ." project_id 	= '".$data['or_id']."',"
							  ." task_id	    = '".$data['task_id']."',"
							  ." module_id	    = '".$data['module_id']."',"
							  ." sub_module_id	= '".$data['sub_module_id']."',"
							  ." title      	= '".$data['title']."',"	
							  ." details      	= '".$data['details']."',"	
							  ." status_id     	= '".$data['status_id']."',"	
							  ." priority_id   	= '".$data['priority_id']."',"
							  ." resolution_id  = '".$data['resolution_id']."',"
							  ." type_id        ='".$data['type_id']."',"
							  ." hrs            ='".$data['hrs']."',"
							  ." min  =         '".$data['min']."',"
                              ." hrs1  =         '".$hrs1."',"
							  ." min1  =         '".$min1."',"
						    ." current_salary  = '".$current_salary."',"
							  ." per_hour_salary  = '".$per_hour_salary."',"
							  ." worked_hour_salary  = '".$worked_hour_salary."',"
							  ." last_updated 	= '".$last_updated."',"
							  ." added_by      	= '".$my['user_id']."',"
							  ." is_bug       	    = '1',"
							  ." attached_file	     = '".$attachfilename."',"
                              ." attached_type	= '".$attached_type."',"   
                              ." attached_desc       = '".$data['attached_desc']."',"
							  ." is_visibleto_client = '".$is_visibleto_client."',"
                              ." mail_client = '".$mail_client."',"
							  ." mail_staff = '".$mail_staff."',"
							  ." mark_imp = '".$mark_imp."',"
							  ." ip  	= '".$_SERVER['REMOTE_ADDR']."',"
							  ." do_e 	= '".date('Y-m-d H:i:s')."'" ;
                              
                   
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("New Task bug entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();   
                    
                    //Send notification BOF    
                    $data1=array();
                    $condition_query = " WHERE ".TABLE_PROJECT_TASK_DETAILS.".id='".$variables['hid']."'" ;
                   /* 
				    $fields = TABLE_PROJECT_TASK_DETAILS.".id, ".TABLE_PROJECT_TASK_DETAILS.".added_by,".                    TABLE_PROJECT_TASK_DETAILS.".title,".TABLE_PROJECT_TASK_DETAILS.".details,".TABLE_PROJECT_TASK_DETAILS.".last_updated,".TABLE_PROJECT_TASK_MODULE.".title as module,".TABLE_PROJECT_TASK_STATUS.".title as status," ;
                    $fields .= TABLE_PROJECT_TASK_PRIORITY.".title as priority, ".TABLE_PROJECT_TASK_TYPE.".title as type,".TABLE_PROJECT_TASK_RESOLUTION.".title as resolution" ;
					 */
					 $fields = TABLE_PROJECT_TASK_DETAILS.".id, ".TABLE_PROJECT_TASK_DETAILS.".added_by,".
                    TABLE_PROJECT_TASK_DETAILS.".title,".TABLE_PROJECT_TASK_DETAILS.".details,".TABLE_PROJECT_TASK_DETAILS.".last_updated,".TABLE_PROJECT_TASK_MODULE.".title as module" ;
                   // $fields .= TABLE_PROJECT_TASK_PRIORITY.".title as priority, ".TABLE_PROJECT_TASK_TYPE.".title as type" ;
					
                    ProjectTask::getList( $db, $data1, $fields, $condition_query);
                    if(!empty($data1) ){      
                        $data = $data1[0];          
                    }
                    $data['project_title'] =$order['order_title'];
                    $data['images_nc']=DIR_WS_IMAGES_NC;
                    // Send Email to the Client BOF                    
                    if(isset($_POST['mail_client']) ){
                         $data['link']   = DIR_WS_MP .'/project-task-bug.php?perform=details&id='. $variables['hid'].'&or_id='.$or_id."&task_id=".$task_id;
                        // include ( DIR_FS_INCLUDES .'/clients.inc.php');
                         Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                         if(!empty($clientDetails)){
                            $client = $clientDetails['0'];                       
                            $data['cf_name'] =$client['f_name'];
                            $data['cl_name'] = $client['l_name'] ;
                            $data['uf_name'] =$my['f_name'];
                            $data['ul_name'] = $my['l_name'] ;
                            $data['af_name'] =$my['f_name'];
                            $data['al_name'] = $my['l_name'] ;
                            $email = NULL;
                            $data['project_title'] =$order['order_title'];
                            if ( getParsedEmail($db, $s, 'TASKBUG_REPORT_CLIENT', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $client['f_name'].' '. $client['l_name'], 'email' => $client['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                /*echo "subject=>".$email["subject"];
                                echo "<br/>";
                                echo $email["body"];
                                echo "To<br/>";
                                print_r($to);
                                echo "<br/>";
                                echo "From<br/>";
                                print_r($from);*/
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                         }
                    }
                    // Send Email to the Client EOF                    
                        // Send Email to the Staff BOF                    
                        if(isset($_POST['mail_staff']) ){
                            $data['link']   = DIR_WS_NC .'/project-task-bug.php?perform=details&id='. $variables['hid'].'&or_id='.$or_id."&task_id=".$task_id;
                            //include ( DIR_FS_INCLUDES .'/client.inc.php');
                            Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                            if(!empty($clientDetails)){
                                $client = $clientDetails['0'];                       
                                $data['cf_name'] =$client['f_name'];
                                $data['cl_name'] = $client['l_name'] ;    
                            }
                            $data['af_name'] =$my['f_name'];
                            $data['al_name'] = $my['l_name'] ;     
                            if(!empty($team_members)){
                                foreach ( $team_members as $user ) {
                                    $data['uf_name']    = $user['f_name'];
                                    $data['ul_name']    = $user['l_name'];
                                    $email = NULL;
                                    if ( getParsedEmail($db, $s, 'TASKBUG_REPORT_STAFF', $data, $email) ) {
                                        $to     = '';
                                        $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        /*echo "subject=>".$email["subject"];
                                        echo "<br/>";
                                        echo $email["body"];
                                        echo "To<br/>";
                                        print_r($to);
                                        echo "<br/>";
                                        echo "From<br/>";
                                        print_r($from);*/
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                    }
                                }                                 
                            }
                        }
                        
                        
                }
                // Send Email to the Staff EOF
               
                //$users = getExecutivesWith($db, array(  array('right'=>'nc_p_tsb_notify', 'access_level'=>'')  ) );
                if(isset($_POST['mark_imp']) ){
                    $data['link']   = DIR_WS_NC .'/project-task-bug.php?perform=details&id='. $variables['hid'].'&or_id='.$or_id."&task_id=".$task_id;
                    //include ( DIR_FS_INCLUDES .'/client.inc.php');
                    Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                    if(!empty($clientDetails)){
                        $client = $clientDetails['0'];                       
                        $data['cf_name'] =$client['f_name'];
                        $data['cl_name'] = $client['l_name'] ;   
                    }
                    $data['af_name'] =$my['f_name'];
                    $data['al_name'] = $my['l_name'] ; 
                    if ( getParsedEmail($db, $s, 'TASKBUG_REPORT_STAFF_IMP', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);                           
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                        /*echo "subject=>".$email["subject"];
                        echo "<br/>";
                        echo $email["body"];
                        echo "To<br/>";
                        print_r($to);
                        echo "<br/>";
                        echo "From<br/>";
                        print_r($from);*/
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }                        
                    /*
                    if(!empty($users)){
                            foreach ( $users as $user ) {
                                $data['uf_name']    = $user['f_name'];
                                $data['ul_name']    = $user['l_name'];
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'TASK_REPORT_STAFF_IMP', $data, $email) ) {
                                    $to     = '';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                }
                            }
                     
                    }*/
                }else{
                
                    $data['link']   = DIR_WS_NC .'/project-task-bug.php?perform=details&id='. $variables['hid'].'&or_id='.$or_id."&task_id=".$task_id;
                    //include ( DIR_FS_INCLUDES .'/client.inc.php');
                    Clients::getList($db, $clientDetails, 'number, f_name, l_name, email', " WHERE user_id = '".$order['client']."'");
                    if(!empty($clientDetails)){
                        $client = $clientDetails['0'];                       
                        $data['cf_name'] =$client['f_name'];
                        $data['cl_name'] = $client['l_name'] ;   
                    }
                    $data['af_name'] =$my['f_name'];
                    $data['al_name'] = $my['l_name'] ;  
                    if ( getParsedEmail($db, $s, 'TASKBUG_REPORT_STAFF', $data, $email) ) {
                        $to     = '';
                        $to[]   = array('name' => 'Admin', 'email' => $email_ord_production_updates);   
                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                     
                        /*echo "subject=>".$email["subject"];
                        echo "<br/>";
                        echo $email["body"];
                        echo "To<br/>";
                        print_r($to);
                        echo "<br/>";
                        echo "From<br/>";
                        print_r($from);*/
                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                    }
                    /*
                    if(!empty($users)){
                        foreach ( $users as $user ) {
                            $data['uf_name']    = $user['f_name'];
                            $data['ul_name']    = $user['l_name'];
                            $email = NULL;
                            if ( getParsedEmail($db, $s, 'TASK_REPORT_STAFF', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                               
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                            }
                        }
                    }*/
                }
                
            }
                   
                   //to flush the data.
                   $_ALL_POST	= NULL;
                   $data		= NULL;
        }
    
        //exit;    
        if(isset($_POST['btnCreate'])  && $messages->getErrorMessageCount() <= 0){
             header("Location:".DIR_WS_NC."/project-task-bug.php?perform=add&added=1&or_id=".$or_id."&task_id=".$task_id);
        }
        if(isset($_POST['btnCancel'])){
           
              header("Location:".DIR_WS_NC."/project-task-bug.php?perform=list&or_id=".$or_id."&task_id=".$task_id);
        }
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if (  (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            
            header("Location:".DIR_WS_NC."/project-task-bug.php?added=1&or_id=".$or_id."&task_id=".$task_id);
        }
        else {
        $count =  $temp ='';
        
            /*
            if(isset($_SESSION['attachment'])){
                $count = sizeof($_SESSION['attachment']);
	            $temp = $_SESSION['attachment'];
            }*/
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $hidden[] = array('name'=> 'or_id' , 'value' => $or_id);           
            $hidden[] = array('name'=> 'task_id' , 'value' => $task_id);           
            
          
            $page["var"][] = array('variable' => 'moduleArr', 'value' => 'moduleArr');
            $page["var"][] = array('variable' => 'submoduleArr', 'value' => 'submoduleArr');
            
            $page["var"][] = array('variable' => 'priorityArr', 'value' => 'priorityArr');
            $page["var"][] = array('variable' => 'statusArr', 'value' => 'statusArr');
            $page["var"][] = array('variable' => 'resolutionArr', 'value' => 'resolutionArr');
            $page["var"][] = array('variable' => 'typeArr', 'value' => 'typeArr');
            $page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
            $page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'count', 'value' => 'count');
            $page["var"][] = array('variable' => 'or_id', 'value' => 'or_id');  
            $page["var"][] = array('variable' => 'task_id', 'value' => 'task_id');  
            $page["var"][] = array('variable' => 'temp', 'value' => 'temp');
            
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'project-task-bug-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-add-permission.html');
        
    }
?>
