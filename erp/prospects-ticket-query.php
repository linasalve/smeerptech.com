<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
	page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/user.inc.php');
    include_once ( DIR_FS_INCLUDES .'/prospects.inc.php');
    include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
    include_once ( DIR_FS_INCLUDES .'/prospects-ticket.inc.php');
    //include_once ( DIR_FS_INCLUDES .'/prospects-ticket-template.inc.php');
	include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
    include_once ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');    
    $added_all 	= isset($_GET["added_all"]) ? $_GET["added_all"]: ( isset($_POST["added_all"])  ? $_POST["added_all"]:'0');    
    $i 	        = isset($_GET["i"])         ? $_GET["i"]        : ( isset($_POST["i"])          ? $_POST["i"]   :     '0');    
    $j 	        = isset($_GET["j"])         ? $_GET["j"]        : ( isset($_POST["j"])          ? $_POST["j"]   :     '0');    
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
	
	
    $condition_query = '';
    $condition_url = '';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    //echo "added=>".$added;
    
    if($added){
        $messages->setOkMessage("Ticket has been created successfully. And email Notification has been sent.");
    }
    if($added_all){
        $messages->setOkMessage("Ticket has been created successfully for ".$i ."/". $j .". And email Notification has been sent.");
    }
    $action='Query';
    if ( $perm->has('nc_pst') ){
    
        if($perform !='list_st_clients'){
            $sTypeArray     = array('Any'  =>  array(  'Any of following' => '-1'),
                                    TABLE_PROSPECTS_TICKETS  => array(
																'Ticket no.'        => 'ticket_no',
                                                                'Subject'       => 'ticket_subject',
                                                                'Order Details'       => 'order_details',
                                                                'Ticket Creator'       => 'ticket_creator',
                                                                //'Priority'      => 'ticket_priority',
                                                                //'Department'    => 'ticket_department'
                                                                //'Status'        => 'ticket_status'
                                                            ),
                                     TABLE_PROSPECTS    =>  array(  'Prospect First Name'        => 'f_name',
                                                                  'Prospect Last Name'       => 'l_name',
                                                                  'Prospect Account No.'       => 'number',
                                                                  'Prospect Billing Name'       => 'billing_name',
                                                                  'Prospect Email'       => 'email',
                                                                  'Prospect Username.'       => 'username'
                                                                )
                                );
            
            $sOrderByArray  = array(
                                    TABLE_PROSPECTS_TICKETS    => array('Comment Date'  => 'do_comment',
                                                        'Ticket no'               => 'ticket_no',
                                                        'Subject'               => 'ticket_subject',
                                                        'Ticket Create Date'    => 'ticket_date' )
                                );
        
            // Set the sorting order of the user list.
            if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ){
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_comment';
                $_SEARCH['sOrder']  = $sOrder   = 'DESC';
                $order_by_table     = TABLE_PROSPECTS_TICKETS;
            }
    
            $variables['active']  = ProspectsTicket::ACTIVE;
            $variables['deactive']  = ProspectsTicket::DEACTIVE;
            $variables['closed']  = ProspectsTicket::CLOSED;
            
            //BOF read the available status
            $variables['status']=ProspectsTicket::getStatus();
            $variables['ticket_status']=ProspectsTicket::getTicketQStatus();
            $variables['PENDINGWITHSMEERP']=ProspectsTicket::PENDINGWITHSMEERP;
            $variables['PENDINGWITHTEAM']=ProspectsTicket::PENDINGWITHTEAM;
            
            //EOF read the available status
       }else{
                $sTypeArray     = array('Any' =>  array(  'Any of following'    => '-1'),
											TABLE_PROSPECTS   =>  array(  'Relationship Number' => 'number',
														  'Billing Name'     	=> 'billing_name',
														  'User Name'     		=> 'username',
														  'E-mail'        		=> 'email',
														  'First Name'    		=> 'f_name',
														  'Middle Name'   		=> 'm_name',
														  'Last Name'     		=> 'l_name',
														  'Pet Name'      		=> 'p_name',
														  'Designation'   		=> 'desig',
														  'Organization'  		=> 'org',
														  'Domain'        		=> 'domain'
													    )
                                    );
                
                $sOrderByArray  = array(
                                        TABLE_PROSPECTS => array('Relationship Number'   => 'number',
                                                            'User Name'     => 'username',
                                                            'E-mail'        => 'email',
                                                            'First Name'    => 'f_name',
                                                            'Last Name'     => 'l_name',
                                                            'Date of Birth' => 'do_birth',
                                                            'Date of Regis.'=> 'do_reg',
                                                            'Date of Login' => 'do_login',
                                                            'Status'        => 'status'
                                                            ),
                                    );
            
                // Set the sorting order of the user list.
                if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
                    $_SEARCH['sOrderBy']= $sOrderBy = 'do_reg';
                    $_SEARCH['sOrder']  = $sOrder   = 'DESC';
                    $order_by_table     = TABLE_PROSPECTS;
                }
            
                $variables['status'] = Prospects::getStatus(); 
       }
        //BOF read the available priority
        //ProspectsTicket::getPriority($db,$priority);
        //EOF read the available priority
        
        //use switch case here to perform action. 
        switch ($perform) {
        
            case ('add'): {
				 $action.=' Add';
                include (DIR_FS_NC .'/prospects-ticket-query-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
			case ('queryflw_add'): {
				 $action.=' Add';
                include (DIR_FS_NC .'/prospects-ticket-query-add.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('queryflw_view'): {
				$action.=' View';
                include (DIR_FS_NC .'/prospects-ticket-query-view.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            /*  
			case ('ordflw_add'):{ 
                include (DIR_FS_NC .'/prospects-ticket-query-add.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
			case ('ordflw_view'): {			
                include (DIR_FS_NC .'/prospects-ticket-query-view.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            } */
			case ('download_file'): {
				include (DIR_FS_NC .'/prospects-ticket-query-template-download.php'); 
				break;   
			}  
            case ('view'): {
				$action.=' View';
                include (DIR_FS_NC.'/prospects-ticket-query-view.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
                if($ajx==0){
					$page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
				}
                break;
            }
            case ('view_header'): {
                include (DIR_FS_NC.'/prospects-ticket-query-header.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('search'): {
				$action.=' List';
                include(DIR_FS_NC."/prospects-ticket-query-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('assign_st'): {
                include(DIR_FS_NC."/prospects-ticket-query-assign-st.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('active'):{
                $perform='list';
            	include ( DIR_FS_NC .'/prospects-ticket-query-active.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
			}
            case ('deactive'): {
                $perform='list';
                include ( DIR_FS_NC .'/prospects-ticket-query-deactive.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('list_st_clients'):{
                $searchStr=0;
                
                include (DIR_FS_NC .'/prospects-st-select-search.php');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-st-select.html');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-st-select-search.html');
                
                $page["section"][] = array('container'=>'INDEX', 'page' => 'blank.html');
                break;
            }        
			case ('list-team'):{
                $action.=' List - Pending With Team';
				if(empty($condition_query)){
			
					$_SEARCH["chk_t_status"]  = 'AND';
					$_SEARCH["tStatus"]   = array(ProspectsTicket::PENDINGWITHTEAM);            
					$condition_query = " AND ticket_status 
					IN('".ProspectsTicket::PENDINGWITHTEAM."') ";
				}
                include (DIR_FS_NC .'/prospects-ticket-query-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }     			
            case ('list'):
            default: {
				$action.=' List - Pending With SMEERP';
				if(empty($condition_query)){
			
					$_SEARCH["chk_t_status"]  = 'AND';
					$_SEARCH["tStatus"]   = array(ProspectsTicket::PENDINGWITHSMEERP);            
					$condition_query = " AND ticket_status 
					IN('".ProspectsTicket::PENDINGWITHSMEERP."') ";
				}
                include (DIR_FS_NC .'/prospects-ticket-query-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'prospects-ticket-query.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("action", $action);
    $s->assign("variables", $variables);
    //$s->assign("status", $status);
    //$s->assign("priority", $priority);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
           $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>