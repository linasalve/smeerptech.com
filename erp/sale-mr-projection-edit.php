<?php
    if ( $perm->has('nc_sl_mr_pr_edit') ) {
    
        $id     = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        $_ALL_POST      = NULL;
        $data           = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        /*
        if ( $perm->has('nc_bl_or_edit_al') ) {
            $access_level += 1;
        }*/
        $yearStart =date('Y');
        $yearEnd  =  $yearStart + 1 ;
        for($j=$yearStart; $j<=$yearEnd  ;$j++){
            $lst_year[$j] = $j;
        }  
        include_once (DIR_FS_INCLUDES .'/sale-mr-projection.inc.php');
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST  = $_POST;
            $data       = processUserData($_ALL_POST);
            
            
            $extra = array( 'db'                => &$db,
                            'access_level'      => $access_level,
                            'messages'          => &$messages
                        );
          
            if ( SaleMrProjection::validateUpdate($data, $extra) ) {
                $query  = " UPDATE ". TABLE_SALE_MR_PROJECTION
                            ." SET ". TABLE_SALE_MR_PROJECTION .".month = '".  $data['month'] ."'"
                                 .",". TABLE_SALE_MR_PROJECTION .".year = '".  $data['year'] ."'"
                                .",". TABLE_SALE_MR_PROJECTION .".amount= '".  $data['amount'] ."'"
                            ." WHERE id = '". $id ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Sale Marketing Projection entry has been updated successfully.");
                    $variables['hid'] = $id;
                }
                $data       = NULL;
            }
        }
        else {
            // Read the record which is to be Updated.
            $fields = TABLE_SALE_MR_PROJECTION .'.*'  ;            
            $condition_query = " WHERE (". TABLE_SALE_MR_PROJECTION .".id = '". $id ."' )";
           /* $condition_query .= " AND ( ";
            // If my has created this record.
            $condition_query .= " (". TABLE_SALE_MR_PROJECTION .".created_by = '". $my['user_id'] ."' "
                                    ." AND ". TABLE_SALE_MR_PROJECTION .".access_level < $access_level ) ";              
            // Check if the User has the Right to Edit Orders created by other Users.
            
            if ( $perm->has('nc_sl_mr_pr_edit_ot') ) {
                $access_level_o   = $my['access_level'];
                if ( $perm->has('nc_sl_mr_pr_edit_ot_al') ) {
                    $access_level_o += 1;
                }
                $condition_query .= " OR ( ". TABLE_SALE_MR_PROJECTION. ".created_by != '". $my['user_id'] ."' "
                                    ." AND ". TABLE_SALE_MR_PROJECTION .".access_level < $access_level_o ) ";
            }
            $condition_query .= " )";*/
          
            if ( SaleMrProjection::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
                $_ALL_POST = $_ALL_POST['0'];
                   $id         = $_ALL_POST['id'];
                
            }
            else { 
                $messages->setErrorMessage("The Record was not found or you do not have the Permission to access this Module.");
            }
        }

        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            $variables['hid'] = $id;
            $condition_query='';
            include ( DIR_FS_NC .'/sale-mr-projection-list.php');
        }
        else {
            // These parameters will be used when returning the control back to the List page.
            foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }
            
            $hidden[] = array('name'=> 'id', 'value' => $id);
            $hidden[] = array('name'=> 'perform', 'value' => 'edit');
            $hidden[] = array('name'=> 'act', 'value' => 'save');
             $page["var"][] = array('variable' => 'lst_year', 'value' => 'lst_year'); 
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-mr-projection-edit.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
