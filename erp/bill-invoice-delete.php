<?php
    if ( $perm->has('nc_bl_inv_delete') ) {
		include_once ( DIR_FS_INCLUDES .'/bill-invoice-profm.inc.php');
	    $inv_id = isset($_GET["inv_id"]) ? $_GET["inv_id"] : ( isset($_POST["inv_id"]) ? $_POST["inv_id"] : '' );
        $invpfm_id = isset($_GET["invpfm_id"]) ? $_GET["invpfm_id"] : ( isset($_POST["invpfm_id"]) ? $_POST["invpfm_id"] : '' );
        $access_level = $my['access_level'];
        if ( $perm->has('nc_bl_inv_delete_al') ) {
            $access_level += 1;
        }
    
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        
        /*
        Invoice::delete($inv_id, $extra);
        */
        
        if ( (Invoice::getList($db, $invoice, TABLE_BILL_INV.'.id,'.TABLE_BILL_INV.'.or_no, '.TABLE_BILL_INV.'.number, 
		'.TABLE_BILL_INV.'.access_level,'.TABLE_BILL_INV.'.inv_profm_id, 
		'.TABLE_BILL_INV.'.status', " WHERE ".TABLE_BILL_INV.".id = '$inv_id'")) > 0 ) {
                $invoice = $invoice[0];
                if ( $invoice['status'] != Invoice::ACTIVE ) {
                    if ( $invoice['status'] == Invoice::BLOCKED ) {
                        if ( $invoice['access_level'] < $access_level ) {
                             /* check receipt created or not if receipt created then dont delete it 
                             ie invoice will be deleted only after deleting the receipts
                            */
                            
                            if((Receipt::getList($db, $receipt, 'id', " WHERE inv_no = '".$invoice['number']."' 
							AND status !='".Receipt::DELETED."'") ) > 0){
                             
                                $messages->setErrorMessage("Please delete  Receipts of Invoice ".$invoice['number']." ");
                             
                            }else{                                
                                /*
                                $query = " UPDATE ". TABLE_BILL_INV
                                            ." SET status ='".Invoice::DELETED."' WHERE id = '$inv_id'";                                            
                                if ( $db->query($query) && $db->affected_rows()>0 ) {    
                                    $messages->setOkMessage("The Invoice has been deleted.");                                    
                                    //Get order details by using order_no           
                                }
                                else {
                                    $messages->setErrorMessage("The Invoice was not deleted.");
                                }
                               */
                            }
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Invoice ".$invoice['number']." with the current Access Level.");
                        }
                    }
                    else {
                        $messages->setErrorMessage("To Delete Invoice ".$invoice['number']." it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("To Delete Invoice ".$invoice['number']." it should be Blocked.");
                }
            }
            else {
                $messages->setErrorMessage("The Invoice ".$invoice['number']." was not found.");
            }
        
        if( isset($_POST['btnCancel'])){
            header("Location:".DIR_WS_NC."/bill-invoice.php?perform=list");
         
        }  
            
        if($messages->getErrorMessageCount() > 0){
             // Display the list.
            $perform = 'list';
            include ( DIR_FS_NC .'/bill-invoice-list.php');
        
        }else{
        
            if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
                $data		= processUserData($_POST);
                if(empty($data['reason_of_delete'])){
                    $messages->setErrorMessage("Please Specify Reason To Delete The Invoice ".$data['number']);
                }
                if($messages->getErrorMessageCount() <= 0){                    
                    //Sql to delete invoice 
                     $query = "UPDATE ". TABLE_BILL_INV
                                    ." SET status ='".Invoice::DELETED."', reason_of_delete ='".$data['reason_of_delete']."',
                                    delete_by ='".$my['uid']."',
                                    delete_ip ='".$_SERVER['REMOTE_ADDR']."',
                                    do_delete='".date("Y-m-d")."'
                                    WHERE id = '$inv_id'";
                                            
                    if ( $db->query($query) && $db->affected_rows()>0 ) {    
                        $del_invprofm_sql = '';
						if(!empty($invpfm_id) && $invpfm_id > 0){
							$query = "UPDATE ". TABLE_BILL_INV_PROFORMA
                                    ." SET status ='".InvoiceProforma::DELETED."', 
									reason_of_delete ='".$data['reason_of_delete']."',
                                    delete_by ='".$my['uid']."',
                                    delete_ip ='".$_SERVER['REMOTE_ADDR']."',
                                    do_delete='".date("Y-m-d")."'
                                    WHERE id = '$invpfm_id'";
							$db->query($query) ;
							$del_invprofm_sql =   ",".TABLE_BILL_ORDERS.".is_invoiceprofm_create='0'" ;
						}elseif(!empty($invoice['inv_profm_id'])){
							$query = "UPDATE ". TABLE_BILL_INV_PROFORMA
                                    ." SET status ='".InvoiceProforma::PENDING."' 
                                    WHERE id = '".$invoice['inv_profm_id']."'";
							$db->query($query) ;
						}							
						//Update in order, invoice_created =0                       
                        $query = "UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".is_invoice_create='0',
						".TABLE_BILL_ORDERS.".followup_status ='0' ".$del_invprofm_sql."
						WHERE ".TABLE_BILL_ORDERS.".number = '".$invoice['or_no']."'";
                        $db->query($query);
												
                        /*
						$query_rm1 = "DELETE FROM ". TABLE_INV_REMINDER." WHERE 
                        ".TABLE_INV_REMINDER.".inv_no = '".$invoice['number']."'";
                        $db->query($query_rm1);
                        $query_rm2 = "DELETE FROM ". TABLE_INV_REMINDER_CRON." WHERE 
                        ".TABLE_INV_REMINDER_CRON.".inv_no = '".$invoice['number']."'";
                        $db->query($query_rm2);
                         */
                        
                        //Add Followup of order delete EOF
                         header("Location:".DIR_WS_NC."/bill-invoice.php?perform=list&deleted=1&inv_no=".$data['number']);
                    }
                    else {
                        $messages->setErrorMessage("The Invoice was not deleted.");
                    }
                    //include ( DIR_FS_NC .'/bill-invoice-list.php');
        
                }
             
            }
            
            $hidden[] = array('name'=> 'inv_id' ,'value' => $inv_id);
			$hidden[] = array('name'=> 'invpfm_id' ,'value' => $invpfm_id);
            $hidden[] = array('name'=> 'number' ,'value' => $invoice['number']);
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-delete.html');
                    
        }  
        
       
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the Invoice.");
    }
?>