<?php
    if ( $perm->has('nc_pst_add') || $perm->has('nc_pst_flw_ord')) {
		include_once ( DIR_FS_INCLUDES .'/st-template-category.inc.php');
        $subUserDetails=$additionalEmailArr= null;
        //$user_id = isset ($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
        $step1 = isset ($_GET['step1']) ? $_GET['step1'] : ( isset($_POST['step1'] ) ? $_POST['step1'] : '0');
        $step2 = isset ($_GET['step2']) ? $_GET['step2'] : ( isset($_POST['step2'] ) ? $_POST['step2'] : '1');
        $client_inv = isset ($_GET['client_inv']) ? $_GET['client_inv'] : ( isset($_POST['client_inv'] ) ? 
		$_POST['client_inv'] : '');
		$invoice_id = isset ($_GET['invoice_id']) ? $_GET['invoice_id'] : ( isset($_POST['invoice_id'] ) ?	$_POST['invoice_id'] : '');
		$flw_ord_id = isset ($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : ( isset($_POST['flw_ord_id'] ) ?	$_POST['flw_ord_id'] : '');
						
		$_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates=null;
		$_ALL_POST = isset ($_POST['perform']) ? $_POST : ( isset($_GET['perform'] ) ? $_GET : NULL);
       
		$fpendingInvlist = array();
		$totalBalanceAmount=0;
		if( !empty($flw_ord_id)){
			//check if invoice id already in ST then redirect on view ST BOF
			/*if(!empty($flw_ord_id)){
				$sql2 = " SELECT ticket_id FROM ".TABLE_PROSPECTS_TICKETS." WHERE 
				".TABLE_PROSPECTS_TICKETS.".flw_ord_id = '".$flw_ord_id."' LIMIT 0,1";
			}
			if( $db->query($sql2)){
				$ticket_id='';
				if ( $db->nf() > 0 ){
					while ($db->next_record()){
					   $ticket_id= $db->f('ticket_id');
					}
				}
				if($ticket_id >0){
					header("Location:".DIR_WS_NC."/prospects-ticket.php?perform=ordflw_view&ticket_id=".$ticket_id."&invoice_id=".$invoice_id."&flw_ord_id=".$flw_ord_id."&client_inv=".$client_inv);
				}
			}*/
			//check if invoice id already in ST then redirect on view ST EOF
			$_ALL_POST['client'] = $client_inv;		
		} 
      
        $category_list	= 	NULL;
		$condition_query1 = " WHERE ".TABLE_ST_TEMPLATE_CATEGORY.".status = '".StTemplateCategory::ACTIVE."'" ;
		StTemplateCategory::getList( $db, $category_list, TABLE_ST_TEMPLATE_CATEGORY.".category,".TABLE_ST_TEMPLATE_CATEGORY.".id", $condition_query1);
        
		$stTemplates = null;
		if(!empty($template_category)){
			$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
			".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::ST.",%' AND 
			".TABLE_ST_TEMPLATE.".template_category = '".$template_category."' ORDER BY title";
			SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'
			.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
		}
        //BOF read the available departments
        //ProspectsTicket::getDepartments($db,$department);
        //EOF read the available departments
        $domains = array();
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["status"] = ProspectsTicket::getTicketStatusList();
		
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        //$lst_min[0] = 'Min';
        for($j=12; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
		// hrs, min.
        if ( (isset($_ALL_POST['btnCreate']) || isset($_ALL_POST['btnReturn'])) && $_ALL_POST['act'] == 'save') {
            //$_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
          
             
            if($data['step1']=='1'){
                if ( ProspectsTicket::validateMemberSelect($data, $extra) ) {
                    $step2=1;
                    $step1=0;
                    $condition_query= " WHERE user_id = '". $data['client'] ."' ";
                    $_ALL_POST = NULL;
                    if ( Prospects::getList($db, $_ALL_POST, 'user_id, f_name,l_name,send_mail, org,billing_name, additional_email,email,check_email,email_1,
						email_2,email_3, mobile1, mobile2,authorization_id', $condition_query) > 0 ) {
                        $_ALL_POST = $_ALL_POST[0];
                        $_ALL_POST['user_id']=$_ALL_POST['user_id'];
                        $_ALL_POST['name']=$_ALL_POST['f_name']." ".$_ALL_POST['l_name'];
                        //$_ALL_POST['email']=$_ALL_POST['email'];
                        $_ALL_POST['org']=$_ALL_POST['org'];
                        $_ALL_POST['billing_name']=$_ALL_POST['billing_name'];
                        $_ALL_POST['mobile1']=$_ALL_POST['mobile1'];
                        $_ALL_POST['mobile2']=$_ALL_POST['mobile2'];
                        $_ALL_POST['send_mail']=$_ALL_POST['send_mail'];
                        $_ALL_POST['check_email']=$_ALL_POST['check_email'];
                        $all_additional_email = trim($_ALL_POST['additional_email'],",");
						$additionalEmailArr = explode(",",$all_additional_email);
                        $_ALL_POST['authorization_id'] = $_ALL_POST['authorization_id'];
                        $_ALL_POST['mail_client'] = '0';
                       // $_ALL_POST['mail_to_all_su'] = '1';
						$_ALL_POST['accountValid'] = 1;
                       
                    }
					$followupSql= "";					 
					$suCondition = " WHERE parent_id = '".$data['client']."' 
					AND status='".Prospects::ACTIVE."' ".$followupSql." ORDER BY f_name" ;
                    Prospects::getList($db, $subUserDetails, 'number,user_id, f_name, l_name,title 
					as ctitle,send_mail,email,check_email,email_1,email_2,email_3,email_4,mobile1,mobile2', $suCondition);
					$domains=array();
                    //BOF read the available domains
                    //ProspectsTicket::getDomains($db,$domains,$data['client']);
                    //EOF read the available domains
                }
            }else{
                if ( ProspectsTicket::validateQueryAdd($data, $extra) ) {
                    
                     
					if(empty($data['ticket_owner_uid']) && !empty($client_inv)){
						$condition_query0 = " WHERE user_id = '". $client_inv ."' ";
						if ( Prospects::getList($db, $client_data, 'user_id, f_name,l_name,send_mail, org,billing_name, 			additional_email,email,check_email,email_1,
						email_2,email_3, mobile1, mobile2,authorization_id', $condition_query0) > 0 ) {
							$client_data1 = $client_data[0];
							$data['ticket_owner_uid'] = $client_data1['user_id'];
							$data['ticket_owner'] =$client_data1['f_name']." ".$client_data1['l_name']."(".$client_data1['billing_name'].")";
						} 
					}
					if(!empty($data['task_members'])) {
						$allotted_to_str1 = implode(",",$data['task_members']);  
						$allotted_to_str1 = trim($allotted_to_str1,',');
						$allotted_to_str = ",".$allotted_to_str1.",";   
						
						$sql= " SELECT f_name,l_name FROM ".TABLE_USER." WHERE user_id 
						IN('".str_replace(",","','",$allotted_to_str1)."')";
						$db->query($sql);
						if ( $db->nf()>0 ) {
							while ( $db->next_record() ) { 
								$allotted_to_name .= $db->f('f_name')." ".$db->f('l_name').",";
							}
							$allotted_to_name1 = trim($allotted_to_name,",") ;
							$allotted_to_name = ",".$allotted_to_name1."," ;
						}
					}
					
                    $ticket_no  =  ProspectsTicket::getNewNumber($db);
                    $attachfilename=$ticket_attachment_path='';
                    if(!empty($data['ticket_attachment'])){
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['ticket_attachment']["name"]);
                       
                        $ext = $filedata["extension"] ; 
                        //$attachfilename = $username."-".mktime().".".$ext ;
                       
                        $attachfilename = $ticket_no.".".$ext ;
                        $data['ticket_attachment'] = $attachfilename;
                        
                        $ticket_attachment_path = DIR_WS_PST_FILES;
                        
                        if(move_uploaded_file($files['ticket_attachment']['tmp_name'], 
							DIR_FS_PST_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_PST_FILES."/".$attachfilename, 0777);
                        }
                    }
                    
                    $mail_to_all_su=0;
                    if(isset($data['mail_to_all_su']) ){
                        $mail_to_all_su=1;
                    }
                    $mail_to_additional_email=0;
                    if(isset($data['mail_to_additional_email']) ){
                        $mail_to_additional_email=1;
                    }
                    $mail_client=0;
                    if(isset($data['mail_client']) ){
                        $mail_client=1;
                    }
					//disply random name BOF 
					$data['display_name']=$data['display_user_id']=$data['display_designation']='';				
					$randomUser = getCeoDetails();
					$data['display_name'] = $randomUser['name'];
					$data['display_user_id'] = $randomUser['user_id'];
					$data['display_designation'] = $randomUser['desig'];
					  
                    if(!isset($data['ticket_status']) ){                    
                        $ticket_status = ProspectsTicket::PENDINGWITHCLIENTS;
                    }else{
                        $ticket_status = $data['ticket_status'];
                    }
                    $hrs1 = (int) ($data['hrs'] * 6);
					$min1 = (int) ($data['min'] * 6);     
					if(empty($data['ss_id'])){		
						$data['ss_title']='';
						$data['ss_file_1']='';
						$data['ss_file_2']='';
						$data['ss_file_3']='';
					}
					
                    $query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
                        . TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
                        . TABLE_PROSPECTS_TICKETS .".invoice_id         = '". $invoice_id ."', "
                        . TABLE_PROSPECTS_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
						. TABLE_PROSPECTS_TICKETS .".template_id      = '". $data['template_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".on_behalf      = '". $data['on_behalf'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_type       = '". ProspectsTicket::TYP_QUERY ."', "
						. TABLE_PROSPECTS_TICKETS .".allotted_to 		= '".$allotted_to_str ."',"
						. TABLE_PROSPECTS_TICKETS .".allotted_to_name 	= '".$allotted_to_name ."',"
                        //. TABLE_PROSPECTS_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
                        //. TABLE_PROSPECTS_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
                        . TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_status     = '".$ticket_status ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_child      = '0', "
						. TABLE_PROSPECTS_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
                        . TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
                        . TABLE_PROSPECTS_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".display_name           = '". $data['display_name'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".is_private           = '". $data['is_private'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
                    
                    //unset($GLOBALS["flag"]);
                    //unset($GLOBALS["submit"]);
                                
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        
                        $variables['hid'] = $db->last_inserted_id() ;
                        
                        /**************************************************************************
                        * Send the notification to the ticket owner and the Administrators
                        * of the new Ticket being created
                        **************************************************************************/
                        $data['ticket_no']    =   $ticket_no ;
                        /*foreach( $status as $key=>$val){                           
                            if ($data['ticket_status'] == $val['status_id'])
                            {
                                $data['status']=$val['status_name'];
                            }
                        }*/
                        
                        $data['domain']   = THIS_DOMAIN;                        
                        //$data['replied_by']   =   $data['ticket_owner'] ;
                        $data['mticket_no']   =   $ticket_no ;
                        $data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
                       	$data['subject']    =     $_ALL_POST['ticket_subject'] ;
                        $data['text']    =   $_ALL_POST['ticket_text'] ;
                        $data['attachment']   =   $attachfilename ;
                        $data['link']   = DIR_WS_MP .'/prospects-ticket-query.php?perform=view&ticket_id='. $variables['hid'];
                       
                        $file_name = '';
						if(!empty($attachfilename)){
							$file_name = DIR_FS_PST_FILES ."/". $attachfilename;
						}
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
						
						if(!empty($data['ss_file_1'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_1'];
						}
						if(!empty($data['ss_file_2'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_2'];
						}
						if(!empty($data['ss_file_3'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_3'];
						}
						// Send Email to the ticket owner BOF  
                        $main_client_name = '';
                        $mail_send_to_su  = '';
                        $billing_name = '';
                        //for sms
                        $sms_to_number = $sms_to =array();
                        $mobile1 = $mobile2='';
                        $counter=0;
                        $smsLength = strlen(trim($data['text'])); 
                        Prospects::getList($db, $userDetails, 'number, f_name, l_name, email,email_1, 
						email_2, email_3,email_4,additional_email,title as ctitle, billing_name,
						mobile1, mobile2 ', " WHERE user_id = '".$data['ticket_owner_uid'] ."'");
						
                        if(!empty($userDetails)){
                            $userDetails=$userDetails['0'];
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    =   $userDetails['ctitle'];
                            $main_client_name  =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $billing_name    =   $userDetails['billing_name'];
                            $email = NULL;
                          
                            $cc_to_sender= $cc = $bcc = Null;
							
                            if ($data['is_private'] == 1){
                                /*  
							    if ( getParsedEmail($db, $s, 'EMAIL_PST_CONFIDENTIAL', $data, $email) ) {
                                    $to = '';
                                    $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 
                                    'email' => $userDetails['email']);                                   
                                    $from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email'];
                                    
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name);
                                    
                                    if($userDetails['email_1']){                                   
                                        $to = '';
                                        $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 
                                        'email' => $userDetails['email_1']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_1'];
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                        $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                    
                                    }
                                    if($userDetails['email_2']){                                   
                                        $to = '';
                                        $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 
                                        'email' => $userDetails['email_2']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_2'];
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                        $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                  
                                    }
                                    
                                    if($userDetails['email_3']){                                   
                                        $to = '';
                                        $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'],
                                        'email' => $userDetails['email_3']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_3'];
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                        $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);                                   
                                    }
                                    if($userDetails['email_4']){                                   
                                        $to = '';
                                        $to[]   = array('name' => $userDetails['f_name'] .' '. $userDetails['l_name'], 
                                        'email' => $userDetails['email_4']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_4'];
                                        SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                                        $cc_to_sender,$cc,$bcc,$file_name);                                   
                                    }
                                } */                                
                            }else{
                                if ($data['mail_client'] == 1 ){  
									if ( getParsedEmail($db, $s, 'EMAIL_PST_MEMBER', $data, $email) ) {                                    
										$to = '';
										/* 
										$to[]   = array('name' => $userDetails['f_name'] .' '. 
										$userDetails['l_name'], 'email' => 
										$userDetails['email']); */ 
										$to[]   = array('name' => $userDetails['email'], 'email' => 
										$userDetails['email']);   
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email'];
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									   
										if(!empty($userDetails['email_1'])){                                       
											//$to = '';
											$to[] = array('name' => $userDetails['email_1'], 'email' =>
											$userDetails['email_1']);                                   
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_1'];
											/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
											$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */ 
										}
										if(!empty($userDetails['email_2'])){                                       
											//$to = '';
											/* $to[]   = array('name' => $userDetails['f_name'] .' '. 
											$userDetails['l_name'], 'email' =>
											$userDetails['email_2']);*/
											
											$to[] = array('name' => $userDetails['email_2'], 'email' =>
											$userDetails['email_2']);      											
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_2'];
											/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"],
											$email["isHTML"],    $cc_to_sender,$cc,$bcc,$file_name);  */
											                              
										} 
										if(!empty($userDetails['email_3'])){                                       
											//$to = '';
											/* $to[]   = array('name' => $userDetails['f_name'] .' '. 
											$userDetails['l_name'], 
											'email' => $userDetails['email_3']);*/
											$to[]   = array('name' => $userDetails['email_3'], 
											'email' => $userDetails['email_3']);      
											$from = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_3'];
											/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
											$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
										   
										} 
										if(!empty($userDetails['email_4'])){                                       
											//$to = '';
											/* $to[]   = array('name' => 
											$userDetails['f_name'] .' '.$userDetails['l_name'], 
											'email' => $userDetails['email_4']); */ 
											$to[]   = array('name' => $userDetails['email_4'], 
											'email' => $userDetails['email_4']);  
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_4'];                                        
											/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
											$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                        
										}
									   
									}                               
                                }
							    if($mail_to_additional_email==1){
									getParsedEmail($db, $s, 'EMAIL_PST_MEMBER', $data, $email);
									if(!empty($userDetails['additional_email'])){
										$arrAddEmail = explode(",",$userDetails['additional_email']); 
										foreach($arrAddEmail as $key=>$val ){
											if(!empty($val)){
												$mail_send_to_su  .= ",".$val;
												//$to = '';
												$to[]   = array('name' =>$val, 'email' => $val);  
/* 												SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
												$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
*/											}
										}
									}
                                }else{
									getParsedEmail($db, $s, 'EMAIL_PST_MEMBER', $data, $email);
									if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
										$arrAddEmail = $data['mail_additional_email'];
										foreach($arrAddEmail as $key=>$val ){
											if(!empty($val)){
												$mail_send_to_su  .= ",".$val;
												//$to = '';
												$to[]   = array('name' =>$val, 'email' => $val);  
												/* SendMail($to, $from, $reply_to, $email["subject"], 
												$email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name); */
											}
										}
									} 
								}							  
                            } 
							//Send SMS
							if(isset($data['mobile1_client']) && $smsLength<=130){
								//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
								$sms_to_number[] =$userDetails['mobile1'];
								$mobile1 = "91".$userDetails['mobile1'];
								$client_mobile .= ','.$mobile1;
								ProjectTask::sendSms($data['text'],$mobile1);
								$counter = $counter+1;
									
							}
							if(isset($data['mobile2_client']) && $smsLength<=130){
								//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
								$sms_to_number[] =$userDetails['mobile2'] ;
								$mobile2 = "91".$userDetails['mobile2'];
								$client_mobile .= ','.$mobile2;
								ProjectTask::sendSms($data['text'],$mobile2);
								$counter = $counter+1;                                        
							}
							if(!empty($mobile1) || !empty($mobile2)){
								 $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
							}
                        }
                        // Send Email to the ticket owner EOF
                        $subsmsUserDetails=array();
                        // Send Email to all the subuser BOF
                        if ($data['is_private'] != 1){
                            if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
                                if(isset($_POST['mail_to_all_su'])){
									$followupSql= "";
									
                                    Prospects::getList($db, $subUserDetails, 'number, f_name, l_name,
									title as ctitle, email,email_1,email_2, email_3,email_4', " 
                                    WHERE parent_id = '".$data['ticket_owner_uid']."' 
                                    AND status='".Prospects::ACTIVE."' AND 
									send_mail='".Prospects::SENDMAILYES."' ".$followupSql."");
                                }elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
                                   $mail_to_su= implode("','",$_POST['mail_to_su']);                                  
                                    Prospects::getList($db, $subUserDetails, 'number, f_name, l_name,
									title as ctitle, email,email_1,email_2, email_3,email_4', " 
                                    WHERE parent_id = '".$data['ticket_owner_uid']."' 
									AND user_id IN('".$mail_to_su."')
                                    AND status='".Prospects::ACTIVE."' ");
                                    
                                }                                
                                if(!empty($subUserDetails)){
								
                                    foreach ($subUserDetails  as $key=>$value){
                                        $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                        $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        if ( getParsedEmail($db, $s, 'EMAIL_PST_MEMBER', $data, $email) ) {
                                            //$to = '';
                                            /*  $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                            '. $subUserDetails[$key]['l_name'], 
											'email' => $subUserDetails[$key]['email']); */
											$to[]   = array('name' => $subUserDetails[$key]['email'], 
											'email' => $subUserDetails[$key]['email']);
                                            $from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
                                            $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
											/* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                  
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                            } */
                                            if(!empty($subUserDetails[$key]['email_1'])){                                       
                                                //$to = '';
                                               /*  $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                                '. $subUserDetails[$key]['l_name'], 'email' => $subUserDetails[$key]['email_1']); */
                                                $to[]  = array('name' => $subUserDetails[$key]['email_1'], 
												'email' => $subUserDetails[$key]['email_1']);
												$from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                                /* 
												if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                    $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                                } */
                                            } 
                                            if(!empty($subUserDetails[$key]['email_2'])){                                       
                                                //$to = '';
                                                /* $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                                '. $subUserDetails[$key]['l_name'], 'email' => 
												$subUserDetails[$key]['email_2']); */
												
												$to[]   = array('name' => $subUserDetails[$key]['email_2'], 
												'email' => $subUserDetails[$key]['email_2']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												 $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                                /* if(SendMail($to, $from, $reply_to, 
													$email["subject"], $email["body"], 
                                                  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                    $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                                } */
                                            }
                                            if(!empty($subUserDetails[$key]['email_3'])){                                       
                                                //$to = '';
                                                /* $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                                '. $subUserDetails[$key]['l_name'], 
												'email' => $subUserDetails[$key]['email_3']); */
												
												$to[]  = array('name' => $subUserDetails[$key]['email_3'], 
												'email' => $subUserDetails[$key]['email_3']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                                /* 
											    if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                    $mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                                } */
                                            }
                                            if(!empty($subUserDetails[$key]['email_4'])){                                       
                                                //$to = '';
                                               /*  $to[]   = array('name' => $subUserDetails[$key]['f_name'] .' 
                                                '. $subUserDetails[$key]['l_name'], 
												'email' => $subUserDetails[$key]['email_4']); */
												
												$to[]   = array('name' => $subUserDetails[$key]['email_4'], 
												'email' => $subUserDetails[$key]['email_4']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												 $mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                                
												/* 
												if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                    $mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                                } 
												*/
                                            }
                                        }                      
                                    }
                                }
                            }
                            
                            if(isset($_POST['mobile1_to_su']) ){                                
                               $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
                                Prospects::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND user_id IN('".$mobile1_to_su."')
                                AND status='".Prospects::ACTIVE."' ");
                                if(!empty($subsmsUserDetails)){                                                                   
                                    foreach ($subsmsUserDetails  as $key=>$value){
                                        if(isset($value['mobile1']) && $smsLength<=130){
                                            $sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
                                            $sms_to_number[] =$value['mobile1'];
                                            $mobile1 = "91".$value['mobile1'];
                                            $client_mobile .= ','.$mobile1;
                                            ProjectTask::sendSms($data['text'],$mobile1);
                                            $counter = $counter+1;
                                                
                                        }
                                    }
                                }
                                
                            }
                            $subsmsUserDetails=array();
                            if(isset($_POST['mobile2_to_su']) ){                                
                                $mobile2_to_su = implode("','",$_POST['mobile2_to_su']);
                                Prospects::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile2', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND user_id IN('".$mobile2_to_su."')
                                AND status='".Prospects::ACTIVE."' ");
                                if(!empty($subsmsUserDetails)){                                                                   
                                    foreach ($subsmsUserDetails  as $key=>$value){
                                        if(isset($value['mobile2']) && $smsLength<=130){
                                            $sms_to[]  = $value['f_name'] .' '. $value['l_name'];
                                            $sms_to_number[] =$value['mobile2'];
                                            $mobile2 = "91".$value['mobile2'];
                                            $client_mobile .= ','.$mobile2;
                                            ProjectTask::sendSms($data['text'],$mobile2);
                                            $counter = $counter+1;
                                        }
                                    }
                                }
                            }         
                        }						
						//Send one copy on clients BOF
						if(!empty($to)){
							getParsedEmail($db, $s, 'EMAIL_PST_MEMBER', $data, $email);
							//$to     = '';
							$cc_to_sender=$cc=$bcc='';
							$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
							'email' => $smeerp_client_email);
							
							$from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name);
						}    
						//Send one copy on clients EOF
                        // Send Email to all the subuser EOF
                        
                        //Set the list subuser's email-id to whom the mails are sent bof
                        $query_update = "UPDATE ". TABLE_PROSPECTS_TICKETS 
                        ." SET ". TABLE_PROSPECTS_TICKETS .".mail_send_to_su 
						= '".trim($mail_send_to_su,",")."', 
						sms_to_number='".implode(",",array_unique($sms_to_number))."',
                        sms_to ='".implode(",",array_unique($sms_to))."' "                                    
                        ." WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
                        $db->query($query_update) ;
                        //Set the list subuser's email-id to whom the mails are sent eof                        
                        //update sms counter
                        if($counter >0){
                            $sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
                            $db->query($sql); 
                        }
                        // Send Email to the admin BOF
                        $email = NULL;
                        $data['link']   = DIR_WS_NC .'/prospects-ticket.php?perform=view&ticket_id='. $variables['hid'];
                        $data['quicklink']   = DIR_WS_SHORTCUT .'/prospects-ticket.php?perform=view&ticket_id='. $variables['hid'];      
                        $data['main_client_name']   = $main_client_name;
                        $data['billing_name']   = $billing_name;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'EMAIL_PST_ADMIN', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $sales_name , 'email' => $sales_email);                           
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							/*  print_r($to);
								print_r($from);
								echo $email["body"];
								echo $email["subject"];
							*/
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                            $cc_to_sender,$cc,$bcc,$file_name);
                        }                        
                        // Send Email to the admin EOF
                        $messages->setOkMessage("New Support Ticket has been created.And email Notification has been sent.");
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
                }else{
                    $condition_query1 = " WHERE user_id = '". $data['ticket_owner_uid'] ."' ";
                    if ( Prospects::getList($db, $_ALL_POST['client_details'], 'user_id, f_name,l_name,additional_email,status,send_mail,org,billing_name, 
                        email,email_1,email_2, email_3,email_4,mobile1, mobile2,authorization_id', $condition_query1) > 0 ) {
                        $_ALL_POST['user_id']=$_ALL_POST['client_details']['0']['user_id'];
                        $_ALL_POST['name']=$_ALL_POST['client_details']['0']['f_name']." ".$_ALL_POST['client_details']['0']['l_name'];
                        //$_ALL_POST['email']=$_ALL_POST['email'];
                        $_ALL_POST['billing_name']=$_ALL_POST['client_details']['0']['billing_name'];
                        $_ALL_POST['additional_email']=$_ALL_POST['client_details']['0']['additional_email'];
                        $_ALL_POST['mobile1']=$_ALL_POST['client_details']['0']['mobile1'];
                        $_ALL_POST['mobile2']=$_ALL_POST['client_details']['0']['mobile2'];
                        $_ALL_POST['send_mail']=$_ALL_POST['client_details']['0']['send_mail'];
                        $_ALL_POST['email']=$_ALL_POST['client_details']['0']['email'];
                        $_ALL_POST['email_1']=$_ALL_POST['client_details']['0']['email_1'];
                        $_ALL_POST['email_2']=$_ALL_POST['client_details']['0']['email_2'];
                        $_ALL_POST['email_3']=$_ALL_POST['client_details']['0']['email_3'];
                        $_ALL_POST['email_4']=$_ALL_POST['client_details']['0']['email_4'];
						$all_additional_email = trim($_ALL_POST['additional_email'],",");
						$additionalEmailArr = explode(",",$all_additional_email);
						 
						$_ALL_POST['accountValid'] = 1;
						/* if(!empty($invoice_id) || !empty($flw_ord_id)){
							$authorization_id = trim($_ALL_POST['client_details']['0']['authorization_id'],",");
							$authorizationArr = explode(",",$authorization_id);
							$isValid = in_array(Prospects::ACCOUNTS,$authorizationArr);
							$_ALL_POST['accountValid'] = $isValid ;
						} */
						
			
                    }
                    
                    if(isset($data['mail_to_all_su'])){
                        $_ALL_POST['mail_to_all_su']=$data['mail_to_all_su'];
                    }
                    if(isset($data['mail_to_su'])){
                        $_ALL_POST['mail_to_su']=$data['mail_to_su'];
                    }
                    //BOF read the available domains
                    $domains=array();
					//ProspectsTicket::getDomains($db,$domains,$data['ticket_owner_uid']);
                    //EOF read the available domains
                    $subUserDetails=null;
                    /*
					$suCondition = " WHERE parent_id = '".$_ALL_POST['ticket_owner_uid']."' AND status='".Prospects::ACTIVE."'  
					ORDER BY f_name";
                    Prospects::getList($db, $subUserDetails, 'number,user_id, f_name, l_name,title as ctitle,status,send_mail,
					email,check_email,email_1,email_2, email_3,email_4, mobile1, mobile2', $suCondition);
                    */    
                }  
            }
        }
		 
        if(!isset($_ALL_POST['ticket_text'])){
			$_ALL_POST['ticket_text'] = 'Dear....';			
		}
		/* if(!isset($_ALL_POST['ticket_subject'])){ 
			$_ALL_POST['ticket_subject'] = PROSPECT_FOLLOWUP_TITLE ;
		} */
        if(isset($_POST['btnCreate']) && $data['step1']!='1' && $messages->getErrorMessageCount() <= 0 ){
			if(!empty($invoice_id) || !empty($flw_ord_id)){
				if($perform=='queryflw_add'){
					$perform='queryflw_add' ;
				}elseif($perform=='add'){
					$perform='view' ;
				}
				 
				 header("Location:".DIR_WS_NC."/prospects-ticket-query.php?perform=".$perform."&added=1&ticket_id=".$variables['hid']."&invoice_id=".$invoice_id."&flw_ord_id=".$flw_ord_id."&client_inv=".$client_inv);  
			}else{
				//echo DIR_WS_NC."/prospects-ticket-query.php?perform=add&added=1";
				 header("Location:".DIR_WS_NC."/prospects-ticket-query.php?perform=add&added=1");
			}
        }
		
		
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            if(!empty($invoice_id) || !empty($flw_ord_id)){
				if($perform=='queryflw_add'){
					$perform='queryflw_add' ;
				}elseif($perform=='add'){
					$perform='view' ;
				}
				 
				  header("Location:".DIR_WS_NC."/prospects-ticket-query.php?perform=".$perform."&added=1&ticket_id=".$variables['hid']."&invoice_id=".$invoice_id."&flw_ord_id=".$flw_ord_id."&client_inv=".$client_inv); 
			}else{
				
				header("Location:".DIR_WS_NC."/prospects-ticket-query.php?perform=add&added=1");
			}
        }
        else {
			
			
			if(!empty($flw_ord_id)){
				$condition_query1 = " WHERE ".TABLE_PROSPECTS_TICKETS.".ticket_child='0' AND 
				".TABLE_PROSPECTS_TICKETS.".ticket_type='".ProspectsTicket::TYP_QUERY."' AND ".TABLE_PROSPECTS_TICKETS.".flw_ord_id=".$flw_ord_id."
				"." ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
				$total	=	ProspectsTicket::getList( $db, $list, '', $condition_query1);
				$condition_url .="&perform=".$perform."&flw_ord_id=".$flw_ord_id."&client_inv=".$client_inv."&step1=1&btnCreate=1&act=save";
				$pagination1 = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
				$fields1 = TABLE_PROSPECTS_TICKETS.".*";
				ProspectsTicket::getList( $db, $list1,$fields1, $condition_query1, $next_record, $rpp);
				$path_attachment_imap = DIR_WS_PST_FILES ;
				if(!empty($list1)){
					foreach($list1 as $key=>$val){ 
						$query = "SELECT "	. TABLE_PROSPECTS_TICKETS .".ticket_no, ". TABLE_PROSPECTS_TICKETS .".ticket_id, "
						. TABLE_PROSPECTS_TICKETS.".to_email_1, ". TABLE_PROSPECTS_TICKETS .".cc_email_1, ". TABLE_PROSPECTS_TICKETS.".bcc_email_1, "
						. TABLE_PROSPECTS_TICKETS.".on_behalf,".TABLE_PROSPECTS_TICKETS.".from_email,".TABLE_PROSPECTS_TICKETS .".mail_send_to_su, "
						. TABLE_PROSPECTS_TICKETS.".ticket_subject,".TABLE_PROSPECTS_TICKETS.".ticket_text,"
						. TABLE_PROSPECTS_TICKETS.".ticket_creator, "
						. TABLE_PROSPECTS_TICKETS.".ticket_creator_uid, ". TABLE_PROSPECTS_TICKETS .".ticket_attachment,"
						. TABLE_PROSPECTS_TICKETS.".ticket_attachment_1,".TABLE_PROSPECTS_TICKETS.".ticket_attachment_2,"
						. TABLE_PROSPECTS_TICKETS.".template_id, ". TABLE_PROSPECTS_TICKETS .".template_file_1, "
						. TABLE_PROSPECTS_TICKETS .".template_file_2, ". TABLE_PROSPECTS_TICKETS .".template_file_3, "
						. TABLE_PROSPECTS_TICKETS .".ss_id, ". TABLE_PROSPECTS_TICKETS .".ss_file_1, ". TABLE_PROSPECTS_TICKETS .".ss_file_2, "
						. TABLE_PROSPECTS_TICKETS .".ss_file_3, ". TABLE_PROSPECTS_TICKETS .".ticket_attachment_path, "
						. TABLE_PROSPECTS_TICKETS .".ticket_response_time, ". TABLE_PROSPECTS_TICKETS .".attachment_imap, "
						. TABLE_PROSPECTS_TICKETS .".ticket_date, ". TABLE_PROSPECTS_TICKETS .".status, ". TABLE_PROSPECTS_TICKETS .".hrs1, "
						. TABLE_PROSPECTS_TICKETS .".min1, ". TABLE_PROSPECTS_TICKETS .".hrs, ". TABLE_PROSPECTS_TICKETS .".min, "
						. TABLE_PROSPECTS_TICKETS .".ip, ". TABLE_PROSPECTS_TICKETS .".headers_imap, ". TABLE_PROSPECTS_TICKETS .".sms_to, "
						. TABLE_PROSPECTS_TICKETS .".display_name, "
						. TABLE_PROSPECTS_TICKETS .".sms_to_number, "
						. TABLE_PROSPECTS_TICKETS .".allotted_to_name, "
						. TABLE_PROSPECTS .".mobile1, ". TABLE_PROSPECTS .".mobile2 "
						." FROM ". TABLE_PROSPECTS_TICKETS ." LEFT JOIN ".TABLE_PROSPECTS." 
						ON ".TABLE_PROSPECTS.".user_id = ".TABLE_PROSPECTS_TICKETS.".ticket_creator_uid
                        WHERE ". TABLE_PROSPECTS_TICKETS .".ticket_owner_uid = '". $val['ticket_owner_uid'] ."' " 
						." AND ". TABLE_PROSPECTS_TICKETS .".ticket_child = '". $val['ticket_id'] ."' " 
						." ORDER BY ". TABLE_PROSPECTS_TICKETS .".ticket_date DESC";
			
						$db->query($query) ;
						$ticketThreads = array() ;
						while($db->next_record()){
							$response["h"] = round($db->f("ticket_response_time") / 3600) ;
							$response["m"] = round(($db->f("ticket_response_time") % 3600) / 60 ) ;
							$response["s"] = round(($db->f("ticket_response_time") % 3600) % 60 ) ;
							
							/* $db_new = new db_local();
							$sql_phone = "SELECT ". TABLE_PROSPECTS_PHONE .".ac, "
											. TABLE_PROSPECTS_PHONE .".cc, "
											. TABLE_PROSPECTS_PHONE .".p_number "
										." FROM ". TABLE_PROSPECTS_PHONE.
										" WHERE ". TABLE_PROSPECTS_PHONE .".phone_of = '".$db->f("ticket_creator_uid")."' " 
										." AND ". TABLE_PROSPECTS_PHONE .".p_is_preferred = '1' " 
										." AND ". TABLE_PROSPECTS_PHONE .".table_name='".TABLE_PROSPECTS."'";
							$db_new->query($sql_phone);        
							$db_new->next_record(); */
							$attachmentArr='';
							$attachment_imap = $db->f("attachment_imap") ; 
							if(!empty($attachment_imap)){
								$attachment_imap = trim($attachment_imap,",");
								$attachmentArr = explode(",",$attachment_imap);
							}
							
							$ticket_date_thread = date("d M Y H:i:s",$db->f("ticket_date"));
							$allotted_to_name = $db->f("allotted_to_name");
							$allotted_to_name =  trim($allotted_to_name,",");
							$ticketThreads[] = array(	"ticket_no" 			=> $db->f("ticket_no"), 
														"ticket_id" 		=> $db->f("ticket_id"), 
														"ticket_subject" 		=> $db->f("ticket_subject"), 
														"display_name" 		=> $db->f("display_name"), 
														"allotted_to_name" 	=> $allotted_to_name, 
														"on_behalf" 		=> $db->f("on_behalf"), 
														"mail_send_to_su" 		=> chunk_split($db->f("mail_send_to_su")), 
														"ticket_text" 			=> $db->f("ticket_text"), 
														"ticket_creator" 		=> $db->f("ticket_creator"), 
														"ticket_attachment" 	=> $db->f("ticket_attachment"), 
														"ticket_attachment_1" 	=> $db->f("ticket_attachment_1"), 
														"ticket_attachment_2" 	=> $db->f("ticket_attachment_2"), 
														"template_id" 			=> $db->f("template_id"), 
														"template_file_1" 		=> $db->f("template_file_1"), 
														"template_file_2" 		=> $db->f("template_file_2"), 
														"template_file_3" 		=> $db->f("template_file_3"), 
														"ss_id" 				=> $db->f("ss_id"), 
														"ss_file_1" 	=> $db->f("ss_file_1"), 
														"ss_file_2" 	=> $db->f("ss_file_2"), 
														"ss_file_3" 	=> $db->f("ss_file_3"), 
														"ticket_attachment_path"=> $db->f("ticket_attachment_path"),
														"ticket_response_time" 	=> $response["h"] ." hr(s) : "
																					. $response["m"] ." min(s) : " 
																					. $response["s"]." sec(s) ",
														"ticket_date" 	=>  $ticket_date_thread,
														"mobile1" 			=> $db->f("mobile1"),
														"mobile2" 			=> $db->f("mobile2"),
														"sms_to" 			=> chunk_split($db->f("sms_to")),
														"sms_to_number" 	=>chunk_split( $db->f("sms_to_number")),
														/*"cc" 			=> $db_new->f("cc"),
														"ac" 			=> $db_new->f("ac"),
														"p_number" 		=> $db_new->f("p_number"),*/
														"to_email_1" 	=> $db->f("to_email_1"),
														"cc_email_1" 	=> $db->f("cc_email_1"),
														"bcc_email_1" 	=> $db->f("bcc_email_1"),
														"from_email" 	=> $db->f("from_email"),
														"status" 	=> $db->f("status"),
														"ip" 	=> $db->f("ip"),
														"hrs" 	=> $db->f("hrs"),
														"min" 	=> $db->f("min"),
														"hrs1" 	=> $db->f("hrs1"),
														"min1" 	=> $db->f("min1"),
														"ip" 	=> $db->f("ip"),
														"headers_imap" 	=> $db->f("headers_imap"),
														"path_attachment_imap" 	=> $path_attachment_imap,
														"attachmentArr" 	=> $attachmentArr
													);
						} 
						$val['ticketThreads']=$ticketThreads;
						$val['allotted_to_name'] =  trim($val['allotted_to_name'],",");
						$qlist[$key]=$val;
					}
				}
				 
			}
			
			
            $variables['can_view_client_details']  = true;            
            $variables['client_sendmail'] = Prospects::SENDMAILNO;
			
            /*$condition_query= " WHERE user_id = '". $user_id ."' ";
            $clients      = NULL;
            if ( Prospects::getList($db, $clients, '*', $condition_query) > 0 ) {
                //$_ALL_POST = $_ALL_POST[0];
                $_ALL_POST['name']=$clients[0]['f_name']." ".$clients[0]['l_name'];
                $_ALL_POST['email']=$clients[0]['email'];
            }*/            
            /*
			$condition_query= " WHERE service_id LIKE '%". Prospects::ST ."%'";            
            $clients      = NULL;
            Prospects::getList($db, $clients, '*', $condition_query);
			*/
            
            // These parameters will be used when returning the control back to the List page.
            /*foreach ( parseURL($_SERVER['QUERY_STRING']) as $key=>$value ) {
                $hidden[]   = array('name'=> $key, 'value' => $value);
            }*/
			
            $hidden[] = array('name'=> 'perform','value' => $perform); 
            $hidden[] = array('name'=> 'invoice_id','value' => $invoice_id);
            $hidden[] = array('name'=> 'flw_ord_id','value' => $flw_ord_id);
            $hidden[] = array('name'=> 'client_inv','value' => $client_inv);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //$hidden[] = array('name'=> 'user_id', 'value' => $user_id);
           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'qlist', 'value' => 'qlist');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'pagination1', 'value' => 'pagination1');
            $page["var"][] = array('variable' => 'additionalEmailArr', 'value' => 'additionalEmailArr');
            $page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
			$page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
			$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
			$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            $page["var"][] = array('variable' => 'invoice_id', 'value' => 'invoice_id');
            $page["var"][] = array('variable' => 'client_inv', 'value' => 'client_inv');
            $page["var"][] = array('variable' => 'flw_ord_id', 'value' => 'flw_ord_id');
            $page["var"][] = array('variable' => 'pendingInvlist', 'value' => 'fpendingInvlist');
			$page["var"][] = array('variable' => 'totalBalanceAmount', 'value' => 'totalBalanceAmount');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            //$page["var"][] = array('variable' => 'status', 'value' => 'status');
            //$page["var"][] = array('variable' => 'priority', 'value' => 'priority');
            $page["var"][] = array('variable' => 'domains', 'value' => 'domains');
            //$page["var"][] = array('variable' => 'clients', 'value' => 'clients');
            $page["var"][] = array('variable' => 'step1', 'value' => 'step1');
            $page["var"][] = array('variable' => 'step2', 'value' => 'step2');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-ticket-query-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>