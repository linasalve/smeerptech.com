<?php

    if ( $perm->has('nc_sl_ld_mr_list') ) {
        $variables['followup_type']=FOLLOWUP::LEADM;
        //include ( DIR_FS_INCLUDES .'/followup.inc.php');
		     
		include_once ( DIR_FS_CLASS .'/RegionLead.class.php');
        include_once ( DIR_FS_CLASS .'/PhoneLead.class.php');
        //Mark Dead Lead BOF
         $lead_id	= isset($_GET["lead_id"])  ? $_GET["lead_id"] 	: ( isset($_POST["lead_id"]) 	? $_POST["lead_id"] : '' );
         $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );
         $tbl_name = isset ($_GET['tbl_name']) ? $_GET['tbl_name'] : ( isset($_POST['tbl_name'] ) ? $_POST['tbl_name'] :'');
        
		if(isset($lead_id) && !empty($lead_id) && isset($status) && !empty($status)){
             $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages,
                        'my'     => $my
                    );
            //status marked by account head
            Leads::markDead($lead_id, $status, $tbl_name, $extra);
        }
		
		$memberTypeList  = Leads::getMemberType() ;
		
        //Mark Dead Lead EOF        
        $variables['search']        = 'search_mr';
	    $variables['list']        = 'list_mr';
	    $variables['add']        = 'add_mr';
        $variables['dead']        = LEADS::DEAD;
		//include_once ( DIR_FS_CLASS .'/Phone.class.php');
		//$condition_query = " WHERE ".TABLE_SALE_LEADS .".lead_assign_to_mr !='' ";
        
        //To access dept wise leads
       
        
        if ( !isset($condition_query) || $condition_query == '' ) {
            $condition_query = ' WHERE ';
            $where_added = true;
        }
        else {
            $condition_query .= ' AND ';
        }
        
        /* 
		if ( $perm->has('nc_sl_ld_my_dpt') ) {
             $condition_query .= " ".TABLE_USER.".department='".$my['department']."'" ;
        }        
        $access_level   = $my['access_level'];
        
        // If Lead is alloted to my
        $condition_query1 = " ( "." ". TABLE_SALE_LEADS .".lead_assign_to_mr ='".$my['user_id']."' ) AND (".TABLE_SALE_LEADS .".lead_assign_to_mr !='' )";  */
        
        /* uncomment me eof
        // If the User has the Right to View Leads of the same Access Level.
        if ( $perm->has('nc_sl_ld_list_al') ) {
            $access_level += 1;
        }
        // If my is the Manager of this Client.
        //$condition_query .= " ( "." AND ". TABLE_CLIENTS .".access_level < $access_level ) "; 
        
        // Check if the User has the Right to view Leads of other Executives.
        if ( $perm->has('nc_sl_ld_list_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_sl_ld_list_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= "( ". TABLE_SALE_LEADS .".access_level < $access_level_o ) ";
        }
		uncomment me eof        
        */
        
        /* uncomment me bof
		if ( $perm->has('nc_sl_ld_list_ot') ) {            
            $access_level_o   = $my['access_level'];
            $access_level_o += 1;
            
           if ( $perm->has('nc_sl_ld_my_dpt') ) {
                $condition_query .=' AND ';
           }
            $condition_query .= " ( ". TABLE_SALE_LEADS .".access_level < $access_level_o ) ";
        }
        
        if ( $perm->has('nc_sl_ld_list_my') ) {
              if ( $perm->has('nc_sl_ld_list_ot') ) { 
                 $condition_query .= " OR ";
              }

              $condition_query .= "  (". TABLE_SALE_LEADS .".created_by = '". $my['user_id'] ."' "." ) ";
        }
        
        if($perm->has('nc_sl_ld_my_dpt') || $perm->has('nc_sl_ld_list_my') || $perm->has('nc_sl_ld_list_ot')){
            $condition_query.= " OR ".$condition_query1 ;
        }else{
            $condition_query.= $condition_query1 ;
        } 
        $condition_query .= ')';
        
		
        $condition_query .= " AND (".TABLE_SALE_LEADS.".lead_status >=".LEADS::POSITIVE." ) AND ".TABLE_SALE_LEADS .".lead_assign_to_mr !=''";
        uncomment me eof
		*/
		
		if ( $perm->has('nc_sl_ld_list_my') ) {
			$condition_query .= "  (". TABLE_SALE_LEADS .".created_by = '". $my['user_id'] ."' "." 
			OR  "." ". TABLE_SALE_LEADS .".lead_assign_to_mr ='".$my['user_id']."'  ) ";
		}else{
		
			$condition_query .= "  (". TABLE_SALE_LEADS .".lead_assign_to_mr != '') " ; 
		}
		
		
        $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
        
        // To count total records.
        $list	= 	NULL;
        $total	=	Leads::getMrList( $db, $list, '', $condition_query);
    
        $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
    
        $list	= NULL;
        $fields ='';
        $fields = TABLE_SALE_LEADS.".lead_id, ".TABLE_SALE_LEADS.".company_name,
		".TABLE_SALE_LEADS.".member_type,".TABLE_SALE_LEADS.".f_name,".TABLE_SALE_LEADS.".m_name,".TABLE_SALE_LEADS.".l_name," ;
        $fields .= TABLE_SALE_LEADS.".email, ". TABLE_SALE_LEADS.".email_1, ".TABLE_SALE_LEADS.".email_2, "
		.TABLE_SALE_LEADS.".mobile1,". TABLE_SALE_LEADS.".mobile2, ".TABLE_SALE_LEADS.".mobile3,".
		TABLE_SALE_LEADS.".status,".TABLE_SALE_LEADS.".lead_status, ".TABLE_SALE_LEADS.".created_by, " ;
        $fields .= TABLE_SALE_LEADS.".lead_assign_to_mr , ".TABLE_SALE_LEADS.".status " ;
        Leads::getMrList( $db, $list,$fields , $condition_query, $next_record, $rpp);
        
        $leadStatusArr =Leads::getLeadStatus();
        $leadStatusArr  = array_flip($leadStatusArr );
        $regionlead     = new RegionLead('91');
        $phonelead      = new PhoneLead(TABLE_SALE_LEADS);
         
        $fList=array();
        if(!empty($list)){
            foreach( $list as $key=>$val){      
                
                $exeCreted=$exeCreatedname='';
                $table = TABLE_AUTH_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_AUTH_USER .'.l_name';
                $exeCreted= getRecord($table,$fields1,$condition2);
            
                if(!empty($exeCreted)){
                   
                   $exeCreatedname = $exeCreted['f_name']." ".$exeCreted['l_name']."<br/>" ;
                }
                $val['created_by_name']    = $exeCreatedname ;     
                
				$phonelead->setPhoneOf(TABLE_SALE_LEADS, $val['lead_id']);
				$val['phone'] = $phonelead->get($db);
				
				// Read the Addresses.
				$regionlead->setAddressOf(TABLE_SALE_LEADS, $val['lead_id']);
				$val['address_list'] = $regionlead->get();

				
                $assign_to_mr=$assign_to_mrname='';
                $table = TABLE_AUTH_USER;
                $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['lead_assign_to_mr'] ."' " ;
                $fields1 =  TABLE_USER .'.f_name'.','. TABLE_AUTH_USER .'.l_name';
                $assign_to_mr= getRecord($table,$fields1,$condition2);
            
                if(!empty($assign_to_mr)){
                   
                   $assign_to_mrname = $assign_to_mr['f_name']." ".$assign_to_mr['l_name']."<br/>" ;
                }
                $val['assign_to_name']    = $assign_to_mrname ;     
                
                $leadstatus = $val['lead_status'];
                $val['lead_status_name']    = $leadStatusArr[$leadstatus] ;     
                $val['member_type_name'] = '';
				$mArr = explode(",",trim($val['member_type'],","));
				if(!empty($mArr)){
					foreach($mArr as $key1=>$val1){
						$val['member_type_name'].= $memberTypeList[$val1];
					}
				}
               $fList[$key]=$val;
            }
        }
		$regionlead = $phonelead = null;
    	/*
		$phone          = new Phone(TABLE_SALE_LEADS);
		for ( $i=0; $i<$total; $i++ ) {
			$phone->setPhoneOf(TABLE_SALE_LEADS, $list[$i]['lead_id']);
        	$list[$i]['phone'] = $phone->get($db);    	
		}*/
		
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
        $variables['can_change_status'] = false;
        $variables['can_view_tck']      = false;

        if ( $perm->has('nc_sl_ld_mr_list') ) {
            $variables['can_view_list']     = true;
        }
        if ( $perm->has('') ) {
            $variables['can_add']     = true;
        }
        if ( $perm->has('nc_sl_ld_edit') ) {
            $variables['can_edit']     = true;
        }
        if ( $perm->has('nc_sl_ld_delete') ) {
            $variables['can_delete']     = true;
        }
        if ( $perm->has('nc_sl_ld_details') ) {
            $variables['can_view_details']     = true;
        }
        if ( $perm->has('nc_sl_ld_status') ) {
            $variables['can_change_status']     = true;
        }
        if ( $perm->has('nc_ldt') && $perm->has('nc_ldt_details') ) {
			$variables['can_view_tck'] = true;
		}
       
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
		$page["var"][] = array('variable' => 'memberTypeList', 'value' => 'memberTypeList');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'sale-lead-mr-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>