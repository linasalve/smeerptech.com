<?php 
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	 
	}
     
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/navigation.inc.php' );    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
	$parent_id 			= isset($_GET["parent_id"])         ? $_GET["parent_id"]        : ( isset($_POST["parent_id"])          ? $_POST["parent_id"]       :'');	
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 			= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');
    $hid 			= isset($_GET["hid"])         ? $_GET["hid"]        : ( isset($_POST["hid"])          ? $_POST["hid"]       :'');
    $condition_url=$condition_query='';
    
    if($added){
         $messages->setOkMessage("Record has been added successfully.");
    }
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;    
        
    $variables["active"]            = Navigation::ACTIVE;
    $variables["deactive"]            = Navigation::DEACTIVE;  
    // Read the available Status for the Pre Order.
    $variables['status']        = Navigation::getStatus();
    
    if ( $perm->has('nc_nav') ) {

        $sTypeArray     = array('Any'        =>  array(  'Any of following'  => '-1'),
                                TABLE_NAVIGATION   =>   array( 'Title' => 'title',	
                                										 'Page' => 'page',
                                										 'Selected Page' => 'selected_page',
                                										 'Permission Name' => 'perm_name'
                                										 
                                                                    ),                                    
                            );
        
        $sOrderByArray  = array(
                                TABLE_NAVIGATION => array(                                 						
                                                        'Title'              => 'title', 
														'Sequence'           => 'sequence', 														
                                                        'Date'              => 'do_e'                                                        
                                                        ),
                                );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            if(!empty($hid)){
               $variables['hid']=$hid;
            }
            $_SEARCH['sOrderBy']= $sOrderBy = 'id';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_NAVIGATION;
        }
    
        
        //use switch case here to perform action. 
        switch ($perform) {
           
            case ('add'): {
            
                include (DIR_FS_NC.'/navigation-main-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'navigation-main.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }            
            case ('edit'): {
                include (DIR_FS_NC .'/navigation-main-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'navigation-main.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }           
            /*case ('view'): {
                include (DIR_FS_NC .'/service-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }*/
           /* case ('subservices'): {
                include (DIR_FS_NC .'/subservices.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }*/
            case ('change_status'): {
                 $perform ='list';
                include ( DIR_FS_NC .'/navigation-main-status.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'navigation-main.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): {
                include(DIR_FS_NC."/navigation-main-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'navigation-main.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
                        
            case ('delete'): {
                $perform ='list';
                include ( DIR_FS_NC .'/navigation-main-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'navigation-main.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            /*            
            case ('list_services'):{
                include (DIR_FS_NC .'/services-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'services.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }*/
            case ('list'):
            default: {
                include (DIR_FS_NC .'/navigation-main-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'navigation-main.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'navigation-main.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign    
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>
