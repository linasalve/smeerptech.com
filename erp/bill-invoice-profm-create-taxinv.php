<?php
 if ( $perm->has('nc_bl_invprfm_taxinv') ) {
 
	include_once ( DIR_FS_INCLUDES .'/bill-invoice.inc.php');
    include_once ( DIR_FS_INCLUDES .'/bill-order.inc.php');
	include_once ( DIR_FS_INCLUDES .'/currency.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/company.inc.php' );
	
    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
    $inv_id         = isset($_GET["inv_id"])? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );

    $_ALL_POST	    = NULL;
    $condition_query= NULL;
    $access_level   = $my['access_level'];
    
    
    $condition_query = " WHERE (". TABLE_BILL_INV_PROFORMA .".id = '". $inv_id ."') AND 
	".TABLE_BILL_INV_PROFORMA.".status!='".InvoiceProforma::COMPLETED."'";

    $fields = TABLE_BILL_INV_PROFORMA .'.*'
                .','. TABLE_CLIENTS .'.user_id AS c_user_id'
                .','. TABLE_CLIENTS .'.number AS c_number'
                .','. TABLE_CLIENTS .'.f_name AS c_f_name'
                .','. TABLE_CLIENTS .'.l_name AS c_l_name'
                .','. TABLE_CLIENTS .'.billing_name'
                .','. TABLE_CLIENTS .'.email AS c_email';
                
    if ( InvoiceProforma::getList($db, $invDetails, $fields, $condition_query) > 0 ) {
       
		$data['currency_abbr'] = '';
		$data['currency_name'] = '';
		$data['currency_symbol'] = '';
		$data['currency_country'] = '';
		$data['old_billing_address']='';
		$data['do_tax']=$data['do_st']=0;
		
		if(!empty($invDetails)){
			$data=$invDetails[0];
			$data['inv_profm_id'] = $data['id'];
			$data['inv_profm_no'] = $data['number'];
			$data['client']= array('number'=>$data['c_number'],
								'f_name'=>$data['c_f_name'],
								'l_name'=>$data['c_l_name']
							);
		    $data['do_i'] = strtotime($data['do_i']) ;
		    $data['do_d'] = strtotime($data['do_d']) ;
			 
			$detailInvNo = getInvoiceNumber($data['do_i'],$data['company_id']); 				
			if(!empty($detailInvNo)){
				$data['number'] = $detailInvNo['number'];
				$data['inv_counter'] = $detailInvNo['inv_counter'];
			}
			
			if(!empty($data['do_e']) && $data['do_e']!='0000-00-00 00:00:00'){				
				$data['is_renewable']=1;
			}else{
				$data['do_e'] ='';
				$data['is_renewable']=0;
			}
			if(!empty($data['do_fe']) && $data['do_fe']!='0000-00-00 00:00:00'){
				
			}else{
				$data['do_fe'] ='';
			}
			//insert query here bof 
			
			$query	= " INSERT INTO ".TABLE_BILL_INV
						." SET ". TABLE_BILL_INV .".number = '".  $data['number'] ."'"
						.",". TABLE_BILL_INV .".inv_counter = '". $data['inv_counter'] ."'"
						.",". TABLE_BILL_INV .".or_id = '".         $data['or_id'] ."'"
						.",". TABLE_BILL_INV .".or_no = '".         $data['or_no'] ."'"
						.",". TABLE_BILL_INV .".inv_profm_id = '".  $data['inv_profm_id'] ."'"
						.",". TABLE_BILL_INV .".inv_profm_no = '".  $data['inv_profm_no'] ."'"
						.",". TABLE_BILL_INV .".tax_ids = '".  		$data['tax_ids'] ."'"
						.",". TABLE_BILL_INV .".service_id = '".    $data['service_id'] ."'"
						.",". TABLE_BILL_INV .".access_level = '".  $data['access_level'] ."'"
						.",". TABLE_BILL_INV .".created_by = '".     $my['user_id'] ."'"
						.",". TABLE_BILL_INV .".client = '".        $data['c_user_id']."'"                                
						.",". TABLE_BILL_INV .".currency_abbr = '". processUserData($data['currency_abbr']) ."'"
						.",". TABLE_BILL_INV .".currency_id = '".   $data['currency_id'] ."'"
						.",". TABLE_BILL_INV .".currency_name = '".   processUserData($data['currency_name']) ."'"
						.",". TABLE_BILL_INV .".currency_symbol = '". processUserData($data['currency_symbol']) ."'"
                        .",". TABLE_BILL_INV .".currency_country = '". processUserData($data['currency_country']) ."'"
						.",". TABLE_BILL_INV .".exchange_rate = '". $data['exchange_rate'] ."'"
						.",". TABLE_BILL_INV .".sub_total_amount = '". $data['sub_total_amount'] ."'"
						.",". TABLE_BILL_INV .".tax1_id = '". $data['tax1_id'] ."'"
						.",". TABLE_BILL_INV .".tax1_number = '". $data['tax1_number'] ."'"
						.",". TABLE_BILL_INV .".tax1_name = '". $data['tax1_name'] ."'"
						.",". TABLE_BILL_INV .".tax1_value = '". $data['tax1_value'] ."'"
						.",". TABLE_BILL_INV .".tax1_pvalue = '". $data['tax1_pvalue'] ."'"
						.",". TABLE_BILL_INV .".tax1_total_amount = '". $data['tax1_total_amount'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub1_id = '". $data['tax1_sub1_id'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub1_number = '". $data['tax1_sub1_number'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub1_name = '". $data['tax1_sub1_name'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub1_value = '". $data['tax1_sub1_value'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub1_pvalue = '". $data['tax1_sub1_pvalue'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub1_total_amount = '". $data['tax1_sub1_total_amount'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub2_id = '". $data['tax1_sub2_id'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub2_number = '". $data['tax1_sub2_number'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub2_name = '". $data['tax1_sub2_name'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub2_value = '". $data['tax1_sub2_value'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub2_pvalue = '". $data['tax1_sub2_pvalue'] ."'"
						.",". TABLE_BILL_INV .".tax1_sub2_total_amount = '". $data['tax1_sub2_total_amount'] ."'"
						.",". TABLE_BILL_INV .".discount = '". $data['discount'] ."'"
						.",". TABLE_BILL_INV .".octroi = '". $data['octroi'] ."'"
						.",". TABLE_BILL_INV .".service_tax = '". $data['service_tax'] ."'"
						.",". TABLE_BILL_INV .".vat = '". $data['vat'] ."'"
						.",". TABLE_BILL_INV .".amount = '".        $data['amount'] ."'"
						.",". TABLE_BILL_INV .".round_off = '".     $data['round_off'] ."'"
						.",". TABLE_BILL_INV .".round_off_op = '".  $data['round_off_op'] ."'"
						.",". TABLE_BILL_INV .".amount_inr = '".    $data['amount_inr'] ."'"
						.",". TABLE_BILL_INV .".amount_words = '".  $data['amount_words'] ."'"
						.",". TABLE_BILL_INV .".delivery_at = '". processUserData( $data['delivery_at']) ."'"
						.",". TABLE_BILL_INV .".tax1_declaration = '". processUserData( $data['tax1_declaration']) ."'"
						.",". TABLE_BILL_INV .".do_c = '".          date('Y-m-d H:i:s') ."'"
						.",". TABLE_BILL_INV .".do_i = '".          date('Y-m-d H:i:s', $data['do_i']) ."'"
						.",". TABLE_BILL_INV .".do_d = '".          date('Y-m-d H:i:s', $data['do_d']) ."'"
						.",". TABLE_BILL_INV .".do_e = '".          $data['do_e'] ."'"
						.",". TABLE_BILL_INV .".do_fe = '".         $data['do_fe']."'"
						.",". TABLE_BILL_INV .".remarks = '".       $data['remarks'] ."'"
						.",". TABLE_BILL_INV .".balance = '".       $data['balance'] ."'"
						.",". TABLE_BILL_INV .".balance_inr = '".   $data['amount_inr'] ."'"
						.",". TABLE_BILL_INV .".balance_opening = '".   $data['balance_opening'] ."'"
						.",". TABLE_BILL_INV .".billing_name      = '".processUserData($data['billing_name']) ."'"						  .",". TABLE_BILL_INV .".b_addr_company_name = '".processUserData($data['b_addr_company_name']) ."'"
						.",". TABLE_BILL_INV .".b_addr              = '".processUserData($data['b_addr'])."'"
						.",". TABLE_BILL_INV .".b_addr_city         = '".processUserData($data['b_addr_city'] )."'"
						.",". TABLE_BILL_INV .".b_addr_state        = '".processUserData($data['b_addr_state']) ."'"
						.",". TABLE_BILL_INV .".b_addr_country      = '".processUserData($data['b_addr_country']) ."'"
						.",". TABLE_BILL_INV .".b_addr_zip          = '".processUserData($data['b_addr_zip']) ."'"
						.",". TABLE_BILL_INV .".billing_address     = '".processUserData($data['billing_address']) ."'"
						.",". TABLE_BILL_INV .".company_id = '".    $data['company_id'] ."'"
						.",". TABLE_BILL_INV .".company_name = '". processUserData($data['company_name']) ."'"
						.",". TABLE_BILL_INV .".company_prefix = '".processUserData($data['company_prefix']) ."'"
						.",". TABLE_BILL_INV .".company_quot_prefix = '".processUserData($data['company_quot_prefix']) ."'"
						.",". TABLE_BILL_INV .".tin_no = '".processUserData($data['tin_no']) ."'"
						.",". TABLE_BILL_INV .".service_tax_regn = '".processUserData($data['service_tax_regn']) ."'"
						.",". TABLE_BILL_INV .".do_st = '".$data['do_st'] ."'"
						.",". TABLE_BILL_INV .".pan_no = '".processUserData($data['pan_no']) ."'"
						.",". TABLE_BILL_INV .".cst_no = '".processUserData($data['cst_no']) ."'"
						.",". TABLE_BILL_INV .".vat_no = '".processUserData($data['vat_no']) ."'"
						.",". TABLE_BILL_INV .".do_tax = '".$data['do_tax'] ."'"
						.",". TABLE_BILL_INV .".is_renewable        = '". $data['is_renewable'] ."'"
						.",". TABLE_BILL_INV .".ip                  = '".$_SERVER['REMOTE_ADDR']."'"
						.",". TABLE_BILL_INV .".new_ref_ord_id = '".    $data['new_ref_ord_id'] ."'"
						.",". TABLE_BILL_INV .".new_ref_ord_no = '".    $data['new_ref_ord_no'] ."'"
						.",". TABLE_BILL_INV .".status              = '".Invoice::PENDING ."'" ;
			 		
			if ( $db->query($query) && $db->affected_rows() > 0 ) {
					
				$variables['hid'] = $db->last_inserted_id();      
				//Update in order, invoice_created =1                       
				$query1 = "UPDATE ".TABLE_BILL_ORDERS." SET ".TABLE_BILL_ORDERS.".is_invoice_create='1' WHERE 
					".TABLE_BILL_ORDERS.".number = '".$data['or_no']."'";
				$db->query($query1);
				
				//Update in order, invoice_created =1                       
				$query1 = "UPDATE ".TABLE_BILL_INV_PROFORMA." SET 
				".TABLE_BILL_INV_PROFORMA.".status='".InvoiceProforma::COMPLETED."' WHERE 
				".TABLE_BILL_INV_PROFORMA.".id = '". $data['inv_profm_id']."'";
				$db->query($query1);
				
				$messages->setOkMessage('The Invoice "'. $data['number'] .'" has been created.');
			}
			//insert query here eof 
			
			 
			
			$temp = NULL;
			$temp_p = NULL;
			$condition_query = "WHERE ".TABLE_BILL_ORD_P.".ord_no = '". $data['or_no'] ."'";
			Order::getParticulars($db, $temp, '*', $condition_query);
			$data['show_discount']=0;
			$data['show_nos']=0;
			$data['colsSpanNo1']=5;
			$data['colsSpanNo2']=6;
			
			if(!empty($temp)){
				/*
				$data['wd1']=10;
				$data['wdinvp']=217;
				$data['wdpri']=70;
				$data['wdnos']=70;
				$data['wdamt']=70;
				$data['wddisc']=50;
				$data['wdst']=70;
				//$data['wdtx']=40; as three columns are removed so manage its width in other cols
			   // $data['wdtxp']=50;
				//$data['wdtotam']=75;
				$data['wd2']=9;*/
				
				$data['wd1']=10;
				$data['wdinvp']=245;//217+28
				$data['wdpri']=98;//70+28
				$data['wdnos']=98;//70+28
				$data['wdamt']=97;//70+27
				$data['wddisc']=77; //50+27
				$data['wdst']=97;   //70+27                     
				$data['wd2']=9;
			
				foreach ( $temp as $pKey => $parti ) {
					/*
					   if($temp[$pKey]['s_type']=='1'){
							$temp[$pKey]['s_type']='Years';
					   }elseif($temp[$pKey]['s_type']=='2'){
							$temp[$pKey]['s_type']='Quantity';
					   }
				   */
					if($temp[$pKey]['d_amount'] >0 || $data['show_discount'] ==1){
						$data['show_discount']=1;
						$show_discountNo=1;
					}
					if($temp[$pKey]['s_quantity'] >0 || $data['show_nos'] ==1){
						$data['show_nos']=1;
						$show_nos=1;
					}
					$temp_p[]=array(
						'p_id'=>$temp[$pKey]['id'],
						'particulars'=>$temp[$pKey]['particulars'],
						'p_amount' =>number_format($temp[$pKey]['p_amount'],2) ,                                   
						's_type' =>$temp[$pKey]['s_type'] ,                                   
						's_quantity' =>$temp[$pKey]['s_quantity'] ,                                   
						's_amount' =>number_format($temp[$pKey]['s_amount'],2) ,                                   
						's_id' =>$temp[$pKey]['s_id'] ,                                   
						'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
						'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] ,                                   
						'tax1_name' =>$temp[$pKey]['tax1_name'] ,                                   
						'tax1_number' =>$temp[$pKey]['tax1_number'] ,                                   
						'tax1_value' =>$temp[$pKey]['tax1_value'] ,                                   
						'tax1_pvalue' =>$temp[$pKey]['tax1_pvalue'] ,                                   
						'tax1_amount' =>number_format($temp[$pKey]['tax1_amount'],2) ,   
						'tax1_sub1_name' =>$temp[$pKey]['tax1_sub1_name'] ,                                   
						'tax1_sub1_number' =>$temp[$pKey]['tax1_sub1_number'] ,                                   
						'tax1_sub1_value' =>$temp[$pKey]['tax1_sub1_value'] ,                                   
						'tax1_sub1_pvalue' =>$temp[$pKey]['tax1_sub1_pvalue'] ,                                   
						'tax1_sub1_amount' =>number_format($temp[$pKey]['tax1_sub1_amount'],2) ,
						'tax1_sub2_name' =>$temp[$pKey]['tax1_sub2_name'] ,                                   
						'tax1_sub2_number' =>$temp[$pKey]['tax1_sub2_number'] ,                                   
						'tax1_sub2_value' =>$temp[$pKey]['tax1_sub2_value'] ,                                   
						'tax1_sub2_pvalue' =>$temp[$pKey]['tax1_sub2_pvalue'] ,                                   
						'tax1_sub2_amount' =>number_format($temp[$pKey]['tax1_sub2_amount'],2) ,                                
						'd_amount' =>number_format($temp[$pKey]['d_amount'],2) ,                                   
						'stot_amount' =>number_format($temp[$pKey]['stot_amount'],2) ,                                   
						'tot_amount' =>number_format($temp[$pKey]['tot_amount'],2) ,                                   
						'discount_type' =>$temp[$pKey]['discount_type'] ,                                   
						'pp_amount' =>number_format($temp[$pKey]['pp_amount'],2)                                    
						);
				}
				if($data['show_discount'] ==0){ 
                             // disc : 50 & subtot : 70
                           /* $data['wdinvp']= $data['wdinvp']+20 + 40;
                            $data['wdpri'] = $data['wdpri'] + 5 + 6;
                            $data['wdamt'] = $data['wdamt'] + 5+ 6;                            
                            $data['wdtx']  = $data['wdtx'] + 5+ 6;
                            $data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            $data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							*/
							 // disc : 77 & subtot : 97
							$data['wdinvp']= $data['wdinvp']+50 + 70;
                            $data['wdpri'] = $data['wdpri'] +10 + 10;
                            $data['wdamt'] = $data['wdamt'] + 17+ 17;                            
                            //$data['wdtx']  = $data['wdtx'] + 5+ 6;
                            //$data['wdtxp'] = $data['wdtxp'] +10 + 6;
                            //$data['wdtotam'] =$data['wdtotam'] + 5+ 6;
							
                            $data['colsSpanNo1'] = $data['colsSpanNo1'] - 2; // as 2 tds will hide
                            $data['colsSpanNo2'] = $data['colsSpanNo2'] - 2; // as 2 tds will hide
                            
                }
				if($data['show_nos'] ==0){
					// disc : 70
				    /* 
					$data['wdinvp']= $data['wdinvp']+30;
					$data['wdpri'] = $data['wdpri'] + 5;
					$data['wdamt'] = $data['wdamt'] + 5;
					$data['wdtx']  = $data['wdtx'] + 10;
					$data['wdtxp'] = $data['wdtxp'] + 10;
					$data['wdtotam'] =$data['wdtotam'] + 10;
					*/
					// disc :98
					$data['wdinvp']= $data['wdinvp']+80;
					$data['wdpri'] = $data['wdpri'] + 8;
					$data['wdamt'] = $data['wdamt'] + 10;
					/*
					$data['wdtx']  = $data['wdtx'] + 10;
					$data['wdtxp'] = $data['wdtxp'] + 10;
					$data['wdtotam'] =$data['wdtotam'] + 10;*/
					$data['colsSpanNo1'] = $data['colsSpanNo1'] - 1;
					$data['colsSpanNo2'] = $data['colsSpanNo2'] - 1;
					
				}     
			}
			$data['particulars']=$temp_p ;
			//get invoice details bof			
			$data['amount'] = number_format($data['amount'],2);		
			$data['sub_total_amount'] = number_format($data['sub_total_amount'],2);		 
			$data['tax1_total_amount']  = number_format($data['tax1_total_amount'],2);		 
			$data['tax1_sub1_total_amount'] = number_format($data['tax1_sub1_total_amount'],2);
			$data['tax1_sub2_total_amount'] = number_format($data['tax1_sub2_total_amount'],2);
			$data['delivery_at'] =  $data['delivery_at'];
			$data['pan_no'] =  $data['pan_no'];
			if( $data['do_tax']!=0 &&  $data['do_tax']!='0000:00:00 00:00:00'){
				$data['do_tax']= strtotime($data['do_tax']);				
			}
			if($data['do_i'] >= $data['do_tax'] && $data['do_tax']!= '0000:00:00 00:00:00'){
				
			   $data['tin_no'] 			=  $data['tin_no'];
			   $data['cst_no'] 			=  $data['cst_no'];
			   $data['vat_no'] 			=  $data['vat_no'];
			}
			if( $data['do_st']!=0 &&  $data['do_st']!='0000:00:00 00:00:00'){
				$data['do_st']= strtotime($data['do_st']);				
			}
			if($data['do_i'] >= $data['do_st'] && $data['do_st']!= '0000:00:00 00:00:00'){
				$data['service_tax_regn']=  $data['service_tax_regn'];	
			}
		}
		
		$data['invoice_title']='INVOICE';
		$data['profm']='';
		if(!empty($data['tax1_name'])){
			$data['invoice_title']='TAX INVOICE';
		}
		//Get billing address BOF
		/* include_once ( DIR_FS_CLASS .'/Region.class.php');
		$region         = new Region();
		$region->setAddressOf(TABLE_CLIENTS,  $data['client']);
		$addId = $data['billing_address'] ;
		$address_list  = $region->get($addId);
		$data['b_addr_company_name'] = $address_list['company_name'];
		$data['b_addr'] = $address_list['address'];
		$data['b_addr_city'] = $address_list['city_title'];
		$data['b_addr_state'] = $address_list['state_title'];
		$data['b_addr_country'] = $address_list['c_title'];
		$data['b_addr_zip'] = $address_list['zipcode'];
		*/
		//$data['b_address'] = $address_list ;
		//Get billing address EOF 
		$data['do_i_chk'] = $data['do_i'] ;
		$data['do_rs_symbol'] = strtotime('2010-07-20'); //Rupee symbol published
		$data['do_tm_enc'] = strtotime('2010-10-23'); //date of trademark implement on ecraft,NC
		$data['do_iso'] = strtotime('2011-01-21'); //date of ISO CERTIFIED
        // Create the InvoiceProforma PDF, HTML in file.
		$extra = array( 's'         => &$s,
						'messages'  => &$messages
					  );
		$attch =$attch1= '';
		if ( !($attch1 = Invoice::createFile($data, 'HTMLPRINT', $extra)) ) {
			$messages->setErrorMessage("The Invoice print html  file was not created.");
		}
		if ( !($attch = Invoice::createFile($data, 'HTML', $extra)) ) {
			$messages->setErrorMessage("The Invoice file was not created.");
		}
		Invoice::createPdfFile($data); 
		//Invoice::createPdfFileSJM($data); 
		$file_name = DIR_FS_INV_PDF_FILES ."/". $data["number"] .".pdf";
		
		// BO: Set up Reminder for invoice expiry, if applicable.
		
		if ( !empty($data["do_e"]) && $data["do_e"] != "0000-00-00 00:00:00" ) {
			$dayArr = Invoice::expirySetDays();
			foreach($dayArr as $key =>$val){
				$temp = explode(' ',$data["do_e"]);
				$temp1 =explode('-',$temp[0]);
				$dd=$temp1[2] - $val ;
				$mm=$temp1[1];
				$yy=$temp1[0];
				$sendDt = mktime(0,0,0,$mm,$dd,$yy);                            
				$send_reminder_dt=date('Y-m-d',$sendDt);
				$query_rm1	= " INSERT INTO ".TABLE_INV_REMINDER
				." SET ". TABLE_INV_REMINDER .".client_id 	= '".$data['c_user_id']."'"
					.",". TABLE_INV_REMINDER .".inv_id		= '".         $variables['hid'] ."'"
					.",". TABLE_INV_REMINDER .".inv_no 		= '".         $data["number"] ."'"
					.",". TABLE_INV_REMINDER .".do_expiry 	= '".         $data["do_e"] ."'"
					.",". TABLE_INV_REMINDER .".date 		= '".date('Y-m-d h:i:s') ."'"
					.",". TABLE_INV_REMINDER .".days_before_expiry = '". $val ."'"
					.",". TABLE_INV_REMINDER .".send_reminder_dt = '".$send_reminder_dt ."'"    
					.",". TABLE_INV_REMINDER .".created_by = '".     $my['user_id'] ."'"
					.",". TABLE_INV_REMINDER .".ip     = '".$_SERVER['REMOTE_ADDR']."'" ;
				$db->query($query_rm1)   ;
			   
			   $query_rm2	= " INSERT INTO ".TABLE_INV_REMINDER_CRON
				." SET ". TABLE_INV_REMINDER_CRON .".client_id 	= '".$data['c_user_id']."'"
					.",". TABLE_INV_REMINDER_CRON .".inv_id 	= '".         $variables['hid'] ."'"
					.",". TABLE_INV_REMINDER_CRON .".inv_no 	= '".         $data["number"] ."'"
					.",". TABLE_INV_REMINDER_CRON .".do_expiry 	= '".         $data["do_e"] ."'"
					.",". TABLE_INV_REMINDER_CRON .".date 		= '".date('Y-m-d h:i:s') ."'"
					.",". TABLE_INV_REMINDER_CRON .".days_before_expiry = '". $val ."'"
					.",". TABLE_INV_REMINDER_CRON .".send_reminder_dt = '".$send_reminder_dt ."'"                                
					.",". TABLE_INV_REMINDER_CRON .".created_by = '".     $my['user_id'] ."'"
					  .",". TABLE_INV_REMINDER_CRON .".ip                  = '".$_SERVER['REMOTE_ADDR']."'" ;
				$db->query($query_rm2)   ;
			}                                
		}                    
		//Code for Reminder expiry EOF
		//Send mail after create invoice to admin bof
		 $data['link']   = DIR_WS_NC .'/bill-invoice-profm.php?perform=view&inv_id='. $variables['hid'];    
		 $email = NULL;
		 $cc_to_sender= $cc = $bcc = Null;
		 if ( getParsedEmail($db, $s, 'INVOICE_CREATE_TO_ADMIN', $data, $email) ) {
			$to     = '';
			 //echo " subject ".$email["subject"] ;
			//echo "<br/>";
			 //echo " body ".$email["body"];
			$to[]   = array('name' => $bill_inv_name , 'email' => $bill_inv_email);                           
			$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
			SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
		 }
		//Send mail after create invoice to admin eof
		
		
		
        
        /*
		$hidden[] = array('name'=> 'inv_id' ,'value' => $inv_id);
        $hidden[] = array('name'=> 'inv_no' ,'value' => $inv_no);
        $hidden[] = array('name'=> 'perform' ,'value' => 'send');
        $hidden[] = array('name'=> 'act' , 'value' => 'save');

        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        $page["var"][] = array('variable' => '_ALL_Data', 'value' => '_ALL_Data');
        $page["var"][] = array('variable' => 'inv_no', 'value' => 'inv_no');*/
        //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-invoice-profm-send.html');
        
    }else{
        $messages->setErrorMessage("Invoice is already generated.");
    }
}else{
 
    $messages->setErrorMessage("You donot have the Right to Access this Module.");
}
?>