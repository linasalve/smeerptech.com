<?php
    if ( $perm->has('nc_uv_status') ) {
        $user_id= isset($_GET["user_id"])? $_GET["user_id"] : ( isset($_POST["user_id"])? $_POST["user_id"]: '' );
        $status	= isset($_GET["status"]) ? $_GET["status"] 	: ( isset($_POST["status"]) ? $_POST["status"] : '' );

        $access_level = $my['access_level'];
        if ( $perm->has('nc_uv_status_al') ) {
            $access_level += 1;
        }
        
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        Vendors::updateStatus($user_id, $status, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/vendors-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?>