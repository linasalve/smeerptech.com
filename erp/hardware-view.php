<?php
    if ( $perm->has('nc_hd_details') ) {
        $id = isset ($_GET['id']) ? $_GET['id'] : ( isset($_POST['id'] ) ? $_POST['id'] :'');

        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_hd_details_al') ) {
            $access_level += 1;
        }

        // Read the visitor details
        $fields = TABLE_VISITOR_BOOK .'.*'  ;            
        $condition_query = " WHERE (". TABLE_HARDWARE_ENTRY .".id = '". $id ."' )";
        $condition_query .= " AND ( ";
        // If my has created this Order.
        $condition_query .= " (". TABLE_HARDWARE_ENTRY .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_HARDWARE_ENTRY .".access_level < $access_level ) ";              
 
       
        // Check if the User has the Right to View visitors created by other Users.
        /*if ( $perm->has('nc_bl_or_details_ot') ) {
            $access_level_o   = $my['access_level'];
            if ( $perm->has('nc_bl_or_details_ot_al') ) {
                $access_level_o += 1;
            }
            $condition_query .= " OR ( ". TABLE_VISITOR_BOOK. ".created_by != '". $my['user_id'] ."' "
                                ." AND ". TABLE_VISITOR_BOOK .".access_level < $access_level_o ) ";
        }*/
        $condition_query .= " ) ";

        $fields = TABLE_HARDWARE_ENTRY .'.*'  ;
        if ( Hardware::getDetails($db, $_ALL_POST, $fields, $condition_query) > 0 ) {
            $_ALL_POST = $_ALL_POST['0'];

            if ( $_ALL_POST['access_level'] < $access_level ) {
                include ( DIR_FS_INCLUDES .'/user.inc.php');
                // Read the Order Creator's Information.
                $_ALL_POST['creator']= '';
                User::getList($db, $_ALL_POST['creator'], 'user_id,username,number,email,f_name,l_name', "WHERE user_id = '". $_ALL_POST['created_by'] ."'");
                $_ALL_POST['creator'] = $_ALL_POST['creator'][0];
                $_ALL_POST['access_level'] = getAccessLevel($db, $_ALL_POST['access_level'], true);
                $_ALL_POST['access_level'] = $_ALL_POST['access_level'][0];
                $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
                
                $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'hardware-view.html');
            }
            else {
                $messages->setErrorMessage("You do not have the Permission to view the Hardware details with the current Access Level.");
            }
        }
        else {
            $messages->setErrorMessage("Records not found to view.");
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>