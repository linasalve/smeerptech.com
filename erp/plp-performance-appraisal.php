<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
   page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/plp-appraisal.inc.php' );
    include_once ( DIR_FS_INCLUDES .'/plp-performance-appraisal.inc.php' );
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	
    $x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       :'');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])        ? $_POST["rpp"]     : ( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 			= isset($_GET["added"])         ? $_GET["added"]        : ( isset($_POST["added"])          ? $_POST["added"]       :'0');

    $condition_query ='';
    $condition_url='';
    
    if(empty($rpp)){
        $rpp = RESULTS_PER_PAGE;
    }
 
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    }
    else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
    
    if($added){
        $messages->setOkMessage("Record has been added successfully.");
    }
    
    if ( $perm->has('nc_plp_ap') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following' => '-1'),
                                TABLE_PLP_APPRAISAL   =>  array(
                                                          'Criteria'            => 'pa_app_criteria',
                                                          'Particulars'             => 'pa_app_particulars',
                                                                                                            
                                                        ),
                                );
        
        $sOrderByArray  = array(
                                TABLE_PLP_APPRAISAL => array(                                 						
                                                        'Date of Entry'              => 'do_e',                                                              
                                                         'Criteria'            => 'pa_app_criteria',
                                                        ),
                                );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
            $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_SETTINGS_CALENDAR;
        }
        $variables['status'] = PlpAppraisal::getStatus();
        //use switch case here to perform action. 
        switch ($perform) {
        
            case ('generate'): {
            
                include (DIR_FS_NC.'/plp-performance-appraisal-generate.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-performance-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('view'): {
            
                include (DIR_FS_NC.'/plp-performance-appraisal-view.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-performance-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('add'): {
            
                include (DIR_FS_NC.'/plp-appraisal-add.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('edit'): {
                include (DIR_FS_NC .'/plp-performance-appraisal-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-performance-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('edit2'): {
                include (DIR_FS_NC .'/plp-performance-appraisal-edit-2.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-performance-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('edit3'): {
                include (DIR_FS_NC .'/plp-performance-appraisal-edit-3.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-performance-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            /*case ('view'): {
                include (DIR_FS_NC .'/address-book-view.php');
                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }*/
            
            case ('search'): {
            	
                include(DIR_FS_NC."/plp-appraisal-search.php");
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('delete'): {
                $perform='list';
                include ( DIR_FS_NC .'/plp-appraisal-delete.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            
            case ('active'):{
                $perform='list';
            	include ( DIR_FS_NC .'/plp-appraisal-active.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
			}
            case ('deactive'): {
                $perform='list';
                include ( DIR_FS_NC .'/plp-appraisal-deactive.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
           
            case ('list'):
            default: {
            	
                include (DIR_FS_NC .'/plp-performance-appraisal-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-performance-appraisal.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        //$messages->setErrorMessage("You donot have the Right to Access this Module.");
        
         $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'plp-performance-appraisal.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
       
    }
    
    // always assign
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
?>
