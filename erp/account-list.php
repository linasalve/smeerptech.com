<?php
if ( $perm->has('nc_ac_list') ) {

     // If the entry is created by the my.
     $access_level   = $my['access_level'];
     if ( $where_added ) {
            $condition_query .= " AND " ;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $condition_query .= "  ( ";

        // If my has created this entry.
        $condition_query .= " ( ". TABLE_ACCOUNT .".created_by = '". $my['user_id'] ."' "
                                ." AND ". TABLE_ACCOUNT .".access_level < $access_level ) ";
   	$condition_query .= ")";
        
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    
    if ( !isset($_SEARCH) ) {
        $_SEARCH = '';
    }
    $_SEARCH['searched'] = 1;
    
    //echo $condition_query;
    // To count total records.
    $list	= 	NULL;
    $total	=	Account::getDetails( $db, $list, '', $condition_query);

    $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
    // $pagination = showpagination($total, $x, $rpp, "x", $condition_url);
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';

    $list	= NULL;
    $fields = TABLE_ACCOUNT.'.*' ;
    Account::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    
    // Set the Permissions.
   
    if ( $perm->has('nc_ac_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_ac_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_ac_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_ac_delete') ) {
        $variables['can_delete'] = true;
    }
    if ( $perm->has('nc_ac_details') ) {
        $variables['can_view_details'] = true;
    }
    
    
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'account-list.html');
}else{
    $messages->setErrorMessage("You do not have the permission to view the list.");
}
?>
