<?php
    if ( $perm->has('nc_ldt_add') || $perm->has('nc_ldt_flw_ord') ) {
		 
	    include_once ( DIR_FS_INCLUDES .'/st-template-category.inc.php');
        $subUserDetails=$additionalEmailArr= null;
        //$user_id = isset ($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
        $step1 = isset ($_GET['step1']) ? $_GET['step1'] : ( isset($_POST['step1'] ) ? $_POST['step1'] : '1');
        $step2 = isset ($_GET['step2']) ? $_GET['step2'] : ( isset($_POST['step2'] ) ? $_POST['step2'] : '0');
        $leads_followup = isset ($_GET['leads_followup']) ? $_GET['leads_followup'] : ( isset($_POST['leads_followup'] ) ? 	$_POST['leads_followup'] : '0');
		
        $lead_ord = isset($_GET['lead_ord'])? $_GET['lead_ord']:( isset($_POST['lead_ord'])? $_POST['lead_ord'] : '');
$flw_ord_id =isset($_GET['flw_ord_id']) ? $_GET['flw_ord_id']:( isset($_POST['flw_ord_id'] )?$_POST['flw_ord_id']:'');
		$invoice_id = isset ($_GET['invoice_id']) ? $_GET['invoice_id'] : ( isset($_POST['invoice_id'] ) 
						?	$_POST['invoice_id'] : '');
		
		$template_category = isset ($_GET['template_category']) ? $_GET['template_category'] : 
		( isset($_POST['template_category'] ) ? $_POST['template_category'] :'');

		$_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates=null;
		$_ALL_POST = isset ($_POST['perform']) ? $_POST : ( isset($_GET['perform'] ) ? $_GET : NULL);
      
		$fpendingInvlist = array();
		$totalBalanceAmount=0;
		if(!empty($flw_ord_id)){
			//check if invoice id already in ST then redirect on view ST BOF
			$sql2 = " SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
			".TABLE_LD_TICKETS.".flw_ord_id = '".$flw_ord_id."' AND ".TABLE_LD_TICKETS.".ticket_child=0 LIMIT 0,1";
			if ( $db->query($sql2) ) {
				$ticket_id='';
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
					   $ticket_id= $db->f('ticket_id');
					}
				}
				if($ticket_id >0){
					header("Location:".DIR_WS_NC."/leads-ticket.php?perform=ordflw_view&ticket_id=".$ticket_id."&flw_ord_id=".$flw_ord_id."&lead_ord=".$lead_ord);
				
				}
			}
			//check if invoice id already in ST then redirect on view ST EOF		 
			$_ALL_POST['lead']=$lead_ord;
		}
		//Lead Followup BOF
		if(!empty($leads_followup) && $leads_followup==1){
			//check if invoice id already in ST then redirect on view ST BOF
			$sql2 = " SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
				".TABLE_LD_TICKETS.".leads_followup = '".$leads_followup."' AND 
				".TABLE_LD_TICKETS.".ticket_owner_uid='".$lead_ord."' AND ".TABLE_LD_TICKETS.".ticket_child ='0' LIMIT 0,1";
			
			if ( $db->query($sql2) ) {
				$ticket_id='';
				if ( $db->nf() > 0 ) {
					while ($db->next_record()) {
					   $ticket_id= $db->f('ticket_id');
					}
				}
				if($ticket_id >0){
					header("Location:".DIR_WS_NC."/leads-ticket.php?perform=ldflw_view&ticket_id=".$ticket_id."&leads_followup=".$leads_followup."&lead_ord=".$lead_ord);
				
				}
			}
			//check if invoice id already in ST then redirect on view ST EOF		 
			$_ALL_POST['lead']=$lead_ord;
		}
		//Lead Followup EOF
	    
	    
      
		$category_list	= 	NULL;
		$condition_query1 = " WHERE ".TABLE_ST_TEMPLATE_CATEGORY.".status = '".StTemplateCategory::ACTIVE."'
		ORDER BY ".TABLE_ST_TEMPLATE_CATEGORY.".category" ;
		StTemplateCategory::getList( $db, $category_list, TABLE_ST_TEMPLATE_CATEGORY.".category,".TABLE_ST_TEMPLATE_CATEGORY.".id", $condition_query1);
        
		$stTemplates = null;
		if(!empty($template_category)){
			$condition_query_st = " WHERE ".TABLE_ST_TEMPLATE.".status='".SupportTicketTemplate::ACTIVE."' AND 
			".TABLE_ST_TEMPLATE.".show_in LIKE '%,".SupportTicketTemplate::ST.",%' AND 
			".TABLE_ST_TEMPLATE.".template_category LIKE '%,".$template_category.",%' ORDER BY title";
			SupportTicketTemplate::getDetails( $db, $stTemplates, TABLE_ST_TEMPLATE.'.id,'
			.TABLE_ST_TEMPLATE.'.title', $condition_query_st);
		}
        $domains = array();
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["status"] = LeadsTicket::getTicketStatusList();
		
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        $lst_min[0] = 'Min';
        for($j=1; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
		// hrs, min.
        if ( (isset($_ALL_POST['btnCreate']) || isset($_ALL_POST['btnReturn'])) && $_ALL_POST['act'] == 'save') {
            //$_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        );
          
             
            if($data['step1']=='1'){
                if ( LeadsTicket::validateMemberSelect($data, $extra) ) {
                    $step2=1;
                    $step1=0;
                    $condition_query= " WHERE lead_id = '". $data['lead']."' ";
                    $_ALL_POST = NULL;
                    if ( Leads::getList($db, $_ALL_POST, 'lead_id, f_name,l_name,send_mail, org,billing_name, 	
						company_name,additional_email,email,email_1,
						email_2,email_3, mobile1, mobile2', $condition_query) > 0 ) {
                        $_ALL_POST = $_ALL_POST[0];
                        $_ALL_POST['lead_id']=$_ALL_POST['lead_id'];
                        $_ALL_POST['name']=$_ALL_POST['f_name']." ".$_ALL_POST['l_name'];
                        //$_ALL_POST['email']=$_ALL_POST['email'];
                        $_ALL_POST['org']=$_ALL_POST['org'];
                        $_ALL_POST['billing_name']=$_ALL_POST['billing_name'];
                        $_ALL_POST['mobile1']=$_ALL_POST['mobile1'];
                        $_ALL_POST['mobile2']=$_ALL_POST['mobile2'];
                        $_ALL_POST['send_mail']=$_ALL_POST['send_mail'];
                        $all_additional_email = trim($_ALL_POST['additional_email'],",");
						$additionalEmailArr = explode(",",$all_additional_email);
                        //$_ALL_POST['authorization_id'] = $_ALL_POST['authorization_id'];
                        $_ALL_POST['mail_client'] = '0';
                        //$_ALL_POST['mail_to_all_su'] = '1';
						$_ALL_POST['accountValid'] = 0;
                       /* if(!empty($lead_enquiry_id)){
							$authorization_id = trim($_ALL_POST['authorization_id'],",");
							$authorizationArr = explode(",",$authorization_id);
							$isValid = in_array(Leads::ACCOUNTS,$authorizationArr);
							$_ALL_POST['accountValid'] = $isValid ;
						}*/
                    }
					$followupSql= "";
					/*
					if(!empty($lead_enquiry_id)){
						$followupSql = " AND authorization_id  LIKE '%,".Leads::ACCOUNTS.",%'" ;
					}*/
					$suCondition = " WHERE parent_id = '".$data['lead']."' 
                                AND status='".Leads::ACTIVE."' ".$followupSql." ORDER BY f_name" ;
                    Leads::getList($db, $subUserDetails, 'lead_number,lead_id , f_name, l_name,title as ctitle,send_mail, email,email_1,email_2, 
					email_3,email_4,mobile1,mobile2', $suCondition);
					$domains=array();
                    //BOF read the available domains
                    // LeadsTicket::getDomains($db,$domains,$data['lead']);
                    //EOF read the available domains
                }
            }else{
                if( LeadsTicket::validateAdd($data, $extra) ) {
                    
                    
                    $ticket_no  =  LeadsTicket::getNewNumber($db);
                    $attachfilename=$ticket_attachment_path='';
                    if(!empty($data['ticket_attachment'])){
                        $filedata["extension"]='';
                        $filedata = pathinfo($files['ticket_attachment']["name"]);
                       
                        $ext = $filedata["extension"] ;                        
                        $attachfilename = $ticket_no.".".$ext ;
                        $data['ticket_attachment'] = $attachfilename;
                        
                        $ticket_attachment_path = DIR_WS_LD_FILES;
                        
                        if(move_uploaded_file($files['ticket_attachment']['tmp_name'], 
							DIR_FS_LD_FILES."/".$attachfilename)){
                            @chmod(DIR_FS_LD_FILES."/".$attachfilename, 0777);
                        }
                    }
                    
                    $mail_to_all_su=0;
                    if(isset($data['mail_to_all_su']) ){
                        $mail_to_all_su=1;
                    }
                    $mail_to_additional_email=0;
                    if(isset($data['mail_to_additional_email']) ){
                        $mail_to_additional_email=1;
                    }
                    $mail_client=0;
                    if(isset($data['mail_client']) ){
                        $mail_client=1;
                    }
					//disply random name BOF 
					$data['display_name'] = $my['f_name']." ".$my['l_name'];
					$data['display_user_id'] = $my['user_id'];
					$data['display_designation'] = $my['desig'];
					//$my['department']=ID_MARKETING;
					//$my['marketing_contact']='9843324324';
					if($my['department'] == ID_MARKETING){
						//This is the marketing person identity
						$data['tck_owner_member_id'] = $my['user_id'];
						$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
						$data['tck_owner_member_email'] = $my['email'];
						$data['marketing_email'] = $my['email'];
						$data['marketing_contact'] = $my['marketing_contact'];
						$data['marketing'] = 1;
					}else{ 
						$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
						$data['tck_owner_member_name']= SALES_MEMBER_USER_NAME;
						$data['tck_owner_member_email'] = SALES_MEMBER_USER_EMAIL;
						$data['marketing_email'] = SALES_MEMBER_USER_EMAIL;
						$data['marketing_contact'] = '';
						$data['marketing'] = 0;
					}
					//disply random name EOF
					
                    if(!isset($data['ticket_status']) ){                    
                        $ticket_status = LeadsTicket::PENDINGWITHCLIENTS;
                    }else{
                        $ticket_status = $data['ticket_status'];
                    }
                    $hrs1 = (int) ($data['hrs'] * 6);
					$min1 = (int) ($data['min'] * 6);    
					
					if(empty($data['ss_id'])){		
						$data['ss_title']='';
						$data['ss_file_1']='';
						$data['ss_file_2']='';
						$data['ss_file_3']='';
					}
					
					$query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
                        . TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "
                        . TABLE_LD_TICKETS .".invoice_id         = '". $invoice_id ."', "
                        . TABLE_LD_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
                        . TABLE_LD_TICKETS .".leads_followup    = '". $leads_followup ."', "
                        . TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
                        . TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
                        . TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
                        . TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
                        . TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
                        . TABLE_LD_TICKETS .".on_behalf      = '". $data['on_behalf'] ."', "
						. TABLE_LD_TICKETS .".template_id      = '". $data['template_id'] ."', "
						. TABLE_LD_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
                        . TABLE_LD_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
                        . TABLE_LD_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_LD_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
						. TABLE_LD_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
						. TABLE_LD_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
                        . TABLE_LD_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
                        . TABLE_LD_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', "
                        . TABLE_LD_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
                        . TABLE_LD_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
                        . TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
                        . TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
                        //. TABLE_LD_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
						//. TABLE_LD_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
                        //. TABLE_LD_TICKETS .".ticket_status     = '". LeadsTicket::PENDINGWITHCLIENTS ."', "
                        . TABLE_LD_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
                        . TABLE_LD_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
                        . TABLE_LD_TICKETS .".ticket_status     = '".$ticket_status ."', "
                        . TABLE_LD_TICKETS .".ticket_child      = '0', "
						. TABLE_LD_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_LD_TICKETS .".min = '". $data['min']."', "
                        . TABLE_LD_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
                        . TABLE_LD_TICKETS .".ticket_response_time = '0', "
                        . TABLE_LD_TICKETS .".ticket_replied        = '0', "
                        . TABLE_LD_TICKETS .".from_admin_panel        = '".LeadsTicket::ADMIN_PANEL."', "
                        . TABLE_LD_TICKETS .".domain_name           = '". $data['domain_name'] ."', "
                        . TABLE_LD_TICKETS .".display_name           = '". $data['display_name'] ."', "
                        . TABLE_LD_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
                        . TABLE_LD_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
                        . TABLE_LD_TICKETS .".is_private           = '". $data['is_private'] ."', "
                        . TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
                        . TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
                    
                    //unset($GLOBALS["flag"]);
                    //unset($GLOBALS["submit"]);
                                
                    if ($db->query($query) && $db->affected_rows() > 0) {
                        
                        $variables['hid'] = $db->last_inserted_id() ;
                        
                        /**************************************************************************
                        * Send the notification to the ticket owner and the Administrators
                        * of the new Ticket being created
                        **************************************************************************/
                        $data['ticket_no']    =   $ticket_no ;
                        
                        $data['domain']   = THIS_DOMAIN;                        
                        //$data['replied_by']   =   $data['ticket_owner'] ;
                        $data['mticket_no']   =   $ticket_no ;
                        $data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
                       	$data['subject']    =     $_ALL_POST['ticket_subject'] ;
                        $data['text']    =   $_ALL_POST['ticket_text'] ;
                        $data['attachment']   =   $attachfilename ;
                        $data['link']   = DIR_WS_MP .'/leads-ticket.php?perform=view&ticket_id='. $variables['hid'];
                       
						$file_name='';
						if(!empty($attachfilename)){
							$file_name[] = DIR_FS_LD_FILES ."/". $attachfilename;
						}
						if(!empty($data['template_file_1'])){
							//$file_name[] = DIR_FS_LD_TEMPLATES_FILES ."/". $data['template_file_1'];
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							//$file_name[] = DIR_FS_LD_TEMPLATES_FILES ."/". $data['template_file_2'];
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							//$file_name[] = DIR_FS_LD_TEMPLATES_FILES ."/". $data['template_file_3'];
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						}
                        
						if(!empty($data['ss_file_1'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_1'];
						}
						if(!empty($data['ss_file_2'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_2'];
						}
						if(!empty($data['ss_file_3'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_3'];
						}
						// Send Email to the ticket owner BOF  
                        $main_client_name = '';
                        $mail_send_to_su  = '';
                        $billing_name = '';
                        //for sms
                        $sms_to_number = $sms_to =array();
                        $mobile1 = $mobile2='';
                        $counter=0;
                        $smsLength = strlen($data['text']); 
                        Leads::getList($db, $userDetails, 'lead_number, f_name, l_name, email,email_1,email_2, email_3,email_4,additional_email,title as ctitle, billing_name,  mobile1, mobile2 ', " 
						WHERE lead_id = '".$data['ticket_owner_uid'] ."'");
                        if(!empty($userDetails)){
                            $userDetails=$userDetails['0'];
                            $data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $data['ctitle']    =   $userDetails['ctitle'];
                            $main_client_name  =   $userDetails['f_name']." ".$userDetails['l_name'];
                            $billing_name    =   $userDetails['billing_name'];
                            $email = NULL;
                          
                            $cc_to_sender= $cc = $bcc = Null;
                            if ($data['is_private'] == 1){
                                if ( getParsedEmail($db, $s, 'EMAIL_LD_CONFIDENTIAL', $data, $email) ) {
                                    //$to = '';
                                    $to[]   = array('name' =>  $userDetails['email'], 
                                    'email' => $userDetails['email']);    
									
									if(!empty($data['tck_owner_member_email'])){
										$email["from_name"] =  $data['tck_owner_member_name'];
										$email["from_email"] = $data['tck_owner_member_email'];
									}
                                    $from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);
                                    $mail_send_to_su  .= ",".$userDetails['email'];
                                    /* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],
                                    $cc_to_sender,$cc,$bcc,$file_name); */
                                    
                                    if($userDetails['email_1']){                                   
                                        //$to = '';
                                        $to[]   = array('name' => $userDetails['email_1'], 
                                        'email' => $userDetails['email_1']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_1'];
                                        /* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                        $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                    
                                    }
                                    if($userDetails['email_2']){                                   
                                        //$to = '';
                                        $to[]   = array('name' => $userDetails['email_2'], 
                                        'email' => $userDetails['email_2']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_2'];
                                        /*  SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                        $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                  
                                    }
                                    
                                    if($userDetails['email_3']){                                   
                                        //$to = '';
                                        $to[]   = array('name' => $userDetails['email_3'],
                                        'email' => $userDetails['email_3']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_3'];
                                        /*  SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                        $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                   
                                    }
                                    if($userDetails['email_4']){                                   
                                        //$to = '';
                                        $to[]   = array('name' => $userDetails['email_4'], 
                                        'email' => $userDetails['email_4']);                                   
                                        $from   = $reply_to = array('name' => $email["from_name"], 
										'email' => $email['from_email']);
                                        $mail_send_to_su  .= ",".$userDetails['email_4'];
                                        /*   
									    SendMail($to, $from, $reply_to, $email["subject"], 
										$email["body"], $email["isHTML"],
                                        $cc_to_sender,$cc,$bcc,$file_name);      */                              
                                    }
                                }                                
                            }else{
                                if ($data['mail_client'] == 1 ){  
									if ( getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email) ) {                                    
										//$to = '';
										$to[]   = array('name' => $userDetails['email'], 'email' => 
										$userDetails['email']);    
										if(!empty($data['tck_owner_member_email'])){
											$email["from_name"] =  $data['tck_owner_member_name'];
											$email["from_email"] = $data['tck_owner_member_email'];
										}
										
										$from   = $reply_to = array('name' => $email["from_name"],
										'email' => $email['from_email']);
										$mail_send_to_su  .= ",".$userDetails['email'];										
										/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
										$email["isHTML"],$cc_to_sender,
										$cc,$bcc,$file_name); */
									   
										if(!empty($userDetails['email_1'])){                                       
											//$to = '';
											$to[]   = array('name' => $userDetails['email_1'], 'email' =>
											$userDetails['email_1']);                                   
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_1'];
											/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
											$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);   */
										}
										if(!empty($userDetails['email_2'])){                                       
											//$to = '';
											$to[]   = array('name' => $userDetails['email_2'], 'email' =>
											$userDetails['email_2']);                                   
											$from   = $reply_to = array('name' => $email["from_name"], 
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_2'];
											/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"],
											$email["isHTML"],    $cc_to_sender,$cc,$bcc,$file_name);  */
											                            
										} 
										if(!empty($userDetails['email_3'])){                                       
											//$to = '';
											$to[]   = array('name' => $userDetails['email_3'], 
											'email' => $userDetails['email_3']);                                   
											$from   = $reply_to = array('name' => $email["from_name"],
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_3'];
											/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
											$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
										   
										} 
										if(!empty($userDetails['email_4'])){                                       
											//$to = '';
											$to[]   = array('name' => $userDetails['email_4'], 
											'email' => $userDetails['email_4']);                                   
											$from   = $reply_to = array('name' => $email["from_name"],
											'email' => $email['from_email']);
											$mail_send_to_su  .= ",".$userDetails['email_4'];                                        
											/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
											$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                        
										}
									   
									}                               
                                }
							    if($mail_to_additional_email==1){
									getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email);
									if(!empty($userDetails['additional_email'])){
										$arrAddEmail = explode(",",$userDetails['additional_email']); 
										foreach($arrAddEmail as $key=>$val ){
											if(!empty($val)){
												$mail_send_to_su  .= ",".$val;
												//$to = '';
												$to[]   = array('name' =>$val, 'email' => $val);  
												if(!empty($data['tck_owner_member_email'])){
													$email["from_name"] =  $data['tck_owner_member_name'];
													$email["from_email"] = $data['tck_owner_member_email'];
												}
												$from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												/* SendMail($to, $from, $reply_to, $email["subject"], 
												$email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name); */
											}
										}
									}
                                }else{
									getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email);
									if(isset($data['mail_additional_email']) && !empty($data['mail_additional_email'])){
										$arrAddEmail = $data['mail_additional_email'];
										foreach($arrAddEmail as $key=>$val ){
											if(!empty($val)){
												$mail_send_to_su  .= ",".$val;
												//$to = '';
												$to[]   = array('name' =>$val, 'email' => $val); 
												if(!empty($data['tck_owner_member_email'])){
													$email["from_name"] =  $data['tck_owner_member_name'];
													$email["from_email"] = $data['tck_owner_member_email'];
												}
												$from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);						
												/* SendMail($to, $from, $reply_to, $email["subject"], 
												$email["body"], $email["isHTML"],
												$cc_to_sender,$cc,$bcc,$file_name); */
											}
										}
									}
									
								}							  
                            }
							//Send SMS
							if(isset($data['mobile1_client']) && $smsLength<=130){
								//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
								$sms_to_number[] =$userDetails['mobile1'];
								$mobile1 = "91".$userDetails['mobile1'];
								$client_mobile .= ','.$mobile1;
								ProjectTask::sendSms($data['text'],$mobile1);
								$counter = $counter+1;
									
							}
							if(isset($data['mobile2_client']) && $smsLength<=130){
								//$sms_to_number .= $userDetails['f_name'] .' '. $userDetails['l_name']."," ;
								$sms_to_number[] =$userDetails['mobile2'] ;
								$mobile2 = "91".$userDetails['mobile2'];
								$client_mobile .= ','.$mobile2;
								ProjectTask::sendSms($data['text'],$mobile2);
								$counter = $counter+1;                                        
							}
							if(!empty($mobile1) || !empty($mobile2)){
								 $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
							}                         
                        }
                        // Send Email to the ticket owner EOF
                        $subsmsUserDetails=array();
                        // Send Email to all the subuser BOF
                        if ($data['is_private'] != 1){
                            if(isset($_POST['mail_to_all_su']) || isset($_POST['mail_to_su'])){
                                if(isset($_POST['mail_to_all_su'])){
									$followupSql= "";
									/*if(!empty($invoice_id)){
										$followupSql = " AND authorization_id  LIKE '%,".Leads::ACCOUNTS.",%'" ;
									}*/
                                    Leads::getList($db, $subUserDetails, 'number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " 
                                    WHERE parent_id = '".$data['ticket_owner_uid']."' AND service_id LIKE '%,".Leads::ST.",%' 
                                    AND status='".Leads::ACTIVE."' AND send_mail='".Leads::SENDMAILYES."' ".$followupSql."");
                                }elseif( isset($_POST['mail_to_su']) && !empty($_POST['mail_to_su']) ){
                                   $mail_to_su= implode("','",$_POST['mail_to_su']);                                  
                                    Leads::getList($db, $subUserDetails, 'lead_number, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " 
                                    WHERE parent_id = '".$data['ticket_owner_uid']."' AND lead_id IN('".$mail_to_su."')
                                    AND status='".Leads::ACTIVE."' ");
                                    
                                }                                
                                if(!empty($subUserDetails)){
								
                                    foreach ($subUserDetails  as $key=>$value){
                                        $data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
                                        $data['ctitle']    =   $subUserDetails[$key]['ctitle'];
                                        $email = NULL;
                                        $cc_to_sender= $cc = $bcc = Null;
                                        if ( getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email) ) {
                                            //$to = '';
                                            $to[]   = array('name' => $subUserDetails[$key]['email'], 
											'email' => $subUserDetails[$key]['email']);
											if(!empty($data['tck_owner_member_email'])){
												$email["from_name"] =  $data['tck_owner_member_name'];
												$email["from_email"] = $data['tck_owner_member_email'];
											}
                                            $from   = $reply_to = array('name' => $email["from_name"],
											'email' => $email['from_email']);
											$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                            /* if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                              $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                  
                                                $mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
                                            } */
                                            if(!empty($subUserDetails[$key]['email_1'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_1'], 
												'email' => $subUserDetails[$key]['email_1']);
                                                $from   = $reply_to = array('name' => $email["from_name"],
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                                /*  
												if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                    $mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
                                                } */
                                            } 
                                            if(!empty($subUserDetails[$key]['email_2'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_2'],
												'email' => $subUserDetails[$key]['email_2']);
                                                $from   = $reply_to = array('name' => $email["from_name"],
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
												
                                                /* 
												if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                    $mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
                                                } */
                                            }
                                            if(!empty($subUserDetails[$key]['email_3'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_3'], 
												'email' => $subUserDetails[$key]['email_3']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
												
                                                /* 
												if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                    $mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
                                                } */
                                            }
                                            if(!empty($subUserDetails[$key]['email_4'])){                                       
                                                //$to = '';
                                                $to[]   = array('name' => $subUserDetails[$key]['email_4'], 
												'email' => $subUserDetails[$key]['email_4']);
                                                $from   = $reply_to = array('name' => $email["from_name"], 
												'email' => $email['from_email']);
												$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                                /* 
												if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
                                                  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
                                                    $mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
                                                } */
                                            }
                                        }                      
                                    }
                                }
                            }
                            
                            if(isset($_POST['mobile1_to_su']) ){                                
                               $mobile1_to_su = implode("','",$_POST['mobile1_to_su']);                                  
                                Leads::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile1', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND lead_id IN('".$mobile1_to_su."')
                                AND status='".Leads::ACTIVE."' ");
                                if(!empty($subsmsUserDetails)){                                                                   
                                    foreach ($subsmsUserDetails  as $key=>$value){
                                        if(isset($value['mobile1']) && $smsLength<=130){
                                            $sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
                                            $sms_to_number[] =$value['mobile1'];
                                            $mobile1 = "91".$value['mobile1'];
                                            $client_mobile .= ','.$mobile1;
                                            ProjectTask::sendSms($data['text'],$mobile1);
                                            $counter = $counter+1;
                                                
                                        }
                                    }
                                }
                                
                            }
                            $subsmsUserDetails=array();
                            if(isset($_POST['mobile2_to_su']) ){                                
                               $mobile2_to_su = implode("','",$_POST['mobile2_to_su']);                                  
                                Leads::getList($db, $subsmsUserDetails, 'f_name, l_name,mobile2', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND lead_id IN('".$mobile2_to_su."')
                                AND status='".Leads::ACTIVE."' ");
                                if(!empty($subsmsUserDetails)){                                                                   
                                    foreach ($subsmsUserDetails  as $key=>$value){
                                        if(isset($value['mobile2']) && $smsLength<=130){
                                            $sms_to[]  = $value['f_name'] .' '. $value['l_name'];
                                            $sms_to_number[] =$value['mobile2'];
                                            $mobile2 = "91".$value['mobile2'];
                                            $client_mobile .= ','.$mobile2;
                                            ProjectTask::sendSms($data['text'],$mobile2);
                                            $counter = $counter+1;
                                        }
                                    }
                                }
                            }         
                        }
                        // Send Email to all the subuser EOF
                        
                        //Set the list subuser's email-id to whom the mails are sent bof
                        $query_update = "UPDATE ". TABLE_LD_TICKETS 
                                    ." SET ". TABLE_LD_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', sms_to_number='".implode(",",array_unique($sms_to_number))."',
                                        sms_to ='".implode(",",array_unique($sms_to))."' "                                    
                                    ." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
                        $db->query($query_update) ;
                        //Set the list subuser's email-id to whom the mails are sent eof                        
                        //update sms counter
                        if($counter >0){
                            $sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
                            $db->query($sql); 
                        }
                        //Send a copy to check the mail for clients
						
						if(!empty($to)){
							getParsedEmail($db, $s, 'EMAIL_LD_MEMBER', $data, $email);
							//$to     = '';
							$cc_to_sender=$cc=$bcc='';
							$bcc[]   = array('name' => $userDetails['f_name'].' '. $userDetails['l_name'], 
							'email' => $sales_email);
							if(!empty($data['tck_owner_member_email'])){
								$email["from_name"] =  $data['tck_owner_member_name'];
								$email["from_email"] = $data['tck_owner_member_email'];
							}
							$from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name);
						}      
                        // Send Email to the admin BOF
                        /* $email = NULL;
                        $data['link']   = DIR_WS_NC .'/leads-ticket.php?perform=view&ticket_id='. $variables['hid'];
                        $data['quicklink']   = DIR_WS_SHORTCUT .'/leads-ticket.php?perform=view&ticket_id='. $variables['hid'];      
                        $data['main_client_name']   = $main_client_name;
                        $data['billing_name']   = $billing_name;
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'EMAIL_LD_ADMIN', $data, $email) ) {
                            $to = '';
                            $to[]   = array('name' => $sales_name , 'email' => $sales_email);         
							
                            $from = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
							
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
                            $cc_to_sender,$cc,$bcc,$file_name);
                        } */                        
                        // Send Email to the admin EOF
                        $messages->setOkMessage("New Support Ticket has been created.And email Notification has been sent.");
                    }
                    //to flush the data.
                    $_ALL_POST	= NULL;
                    $data		= NULL;
                }else{
                    $condition_query1 = " WHERE lead_id = '". $data['ticket_owner_uid'] ."' ";
                    if ( Leads::getList($db, $_ALL_POST['client_details'], 'lead_id, f_name,l_name,additional_email,status,send_mail,org,billing_name, 
                        email,email_1,email_2, email_3,email_4,mobile1, mobile2', $condition_query1) > 0 ) {
                        $_ALL_POST['lead_id']=$_ALL_POST['client_details']['0']['lead_id'];
                        $_ALL_POST['name']=$_ALL_POST['client_details']['0']['f_name']." ".$_ALL_POST['client_details']['0']['l_name'];
                        //$_ALL_POST['email']=$_ALL_POST['email'];
                        $_ALL_POST['billing_name']=$_ALL_POST['client_details']['0']['billing_name'];
                        $_ALL_POST['additional_email']=$_ALL_POST['client_details']['0']['additional_email'];
                        $_ALL_POST['mobile1']=$_ALL_POST['client_details']['0']['mobile1'];
                        $_ALL_POST['mobile2']=$_ALL_POST['client_details']['0']['mobile2'];
                        $_ALL_POST['send_mail']=$_ALL_POST['client_details']['0']['send_mail'];
                        $_ALL_POST['email']=$_ALL_POST['client_details']['0']['email'];
                        $_ALL_POST['email_1']=$_ALL_POST['client_details']['0']['email_1'];
                        $_ALL_POST['email_2']=$_ALL_POST['client_details']['0']['email_2'];
                        $_ALL_POST['email_3']=$_ALL_POST['client_details']['0']['email_3'];
                        $_ALL_POST['email_4']=$_ALL_POST['client_details']['0']['email_4'];
						$all_additional_email = trim($_ALL_POST['additional_email'],",");
						$additionalEmailArr = explode(",",$all_additional_email);
						$_ALL_POST['accountValid'] = 0;
						/*
						if(!empty($invoice_id)){
							$authorization_id = trim($_ALL_POST['client_details']['0']['authorization_id'],",");
							$authorizationArr = explode(",",$authorization_id);
							$isValid = in_array(Leads::ACCOUNTS,$authorizationArr);
							$_ALL_POST['accountValid'] = $isValid ;
						}*/
						
			
                    }
                    
                    if(isset($data['mail_to_all_su'])){
                        $_ALL_POST['mail_to_all_su']=$data['mail_to_all_su'];
                    }
                    if(isset($data['mail_to_su'])){
                        $_ALL_POST['mail_to_su']=$data['mail_to_su'];
                    }
                    //BOF read the available domains
					$domains=array();
                    //LeadsTicket::getDomains($db,$domains,$data['ticket_owner_uid']);
                    //EOF read the available domains
                    $subUserDetails=null;
                    /*Leads::getList($db, $subUserDetails, 'number,user_id, f_name, l_name,title as ctitle, email,email_1,email_2, email_3,email_4', " 
                                WHERE parent_id = '".$data['ticket_owner_uid']."' AND service_id LIKE '%,".Leads::ST.",%' 
                                AND status='".Leads::ACTIVE."' ");
					*/
					 $followupSql= "";
					 /*
					 if(!empty($invoice_id)){
							$followupSql = " AND authorization_id  LIKE '%,".Leads::ACCOUNTS.",%'" ;
					 }*/
					$suCondition = " WHERE parent_id = '".$_ALL_POST['ticket_owner_uid']."' AND status='".Leads::ACTIVE."' ".$followupSql." ORDER BY f_name";
                    Leads::getList($db, $subUserDetails, 'lead_number,lead_id, f_name, l_name,title as ctitle,status,send_mail,email,email_1,email_2, email_3,email_4, mobile1, mobile2', $suCondition);
                        
                }  
            }
        }
		if(!isset($_ALL_POST['ticket_text'])){
			$_ALL_POST['ticket_text'] ='Dear....'; 
		}
		if(!isset($_ALL_POST['ticket_subject'])){ 
			$_ALL_POST['ticket_subject'] = 'Following up on your requirements. Kindly consider.'; 
		}
        if(isset($_POST['btnCreate']) && $data['step1']!='1' && $messages->getErrorMessageCount() <= 0 ){
			if(!empty($flw_ord_id)){
				header("Location:".DIR_WS_NC."/leads-ticket.php?perform=ordflw_view&added=1&ticket_id=".$variables['hid']."&flw_ord_id=".$flw_ord_id."&lead_ord=".$lead_ord);
			}else{
				header("Location:".DIR_WS_NC."/leads-ticket.php?perform=add&added=1");
			}
        }
	 
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnCancel'])
            || (isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 )) {
            include ( DIR_FS_NC .'/leads-ticket-list.php');
        }
        else {
        
            $variables['can_view_client_details']  =  true;
			$variables['can_send_mail'] = false;
			if ( $perm->has('nc_ldt_send_mail') ) {
                $variables['can_send_mail'] = true;
				
            } 
			
            $variables['client_sendmail'] = Leads::SENDMAILNO;
           
            $hidden[] = array('name'=> 'perform','value' => $perform);
            $hidden[] = array('name'=> 'flw_ord_id','value' => $flw_ord_id);
            //$hidden[] = array('name'=> 'invoice_id','value' => $invoice_id);
            $hidden[] = array('name'=> 'lead_ord','value' => $lead_ord);
            $hidden[] = array('name'=> 'leads_followup','value' => $leads_followup);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            //$hidden[] = array('name'=> 'user_id', 'value' => $user_id);
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'additionalEmailArr', 'value' => 'additionalEmailArr');
            $page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
            $page["var"][] = array('variable' => 'stTemplates', 'value' => 'stTemplates');
			$page["var"][] = array('variable' => 'category_list', 'value' => 'category_list');
			$page["var"][] = array('variable' => 'lst_hrs', 'value' => 'lst_hrs');
			$page["var"][] = array('variable' => 'lst_min', 'value' => 'lst_min');
            //$page["var"][] = array('variable' => 'invoice_id', 'value' => 'invoice_id');
            //$page["var"][] = array('variable' => 'pendingInvlist', 'value' => 'fpendingInvlist');
			//$page["var"][] = array('variable' => 'totalBalanceAmount', 'value' => 'totalBalanceAmount');
            //$page["var"][] = array('variable' => 'department', 'value' => 'department');
            //$page["var"][] = array('variable' => 'status', 'value' => 'status');
            //$page["var"][] = array('variable' => 'priority', 'value' => 'priority');
            $page["var"][] = array('variable' => 'domains', 'value' => 'domains');
            //$page["var"][] = array('variable' => 'clients', 'value' => 'clients');
            $page["var"][] = array('variable' => 'step1', 'value' => 'step1');
            $page["var"][] = array('variable' => 'step2', 'value' => 'step2');
            $page["var"][] = array('variable' => 'data', 'value' => 'data');
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-ticket-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>