<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");

    include_once ( DIR_FS_INCLUDES .'/support-ticket.inc.php');
	include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
    include_once ( DIR_FS_INCLUDES .'/clients.inc.php');
	include_once ( DIR_FS_INCLUDES .'/project-task.inc.php');
	include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
	
	
	$db1 		= new db_local; 
    $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : '';
    $transaction_id = isset($_GET['trans_id']) ? $_GET['trans_id'] : '';
    $transaction_no = isset($_GET['trans_no']) ? $_GET['trans_no'] : '';
    $type = isset($_GET['type']) ? $_GET['type'] : '';  // transaction type
    $msg_by = 1;
	
    if(!empty($client_id) && !empty($transaction_id)){
	
		$sql = "SELECT user_id,title,billing_name, f_name, l_name,email,email_1,email_2,email_3,email_4,parent_id, authorization_id,mobile1,mobile2 FROM ".TABLE_CLIENTS." WHERE user_id ='".$client_id."' AND status ='".Clients::ACTIVE."'" ;
		$db->query($sql) ;          
		$total	= $db->nf();	
		if( $db->nf() > 0 ){
			while($db->next_record()){
				$userDetails['user_id'] = $user_id = $db->f("user_id");
				$userDetails['ctitle'] = $ctitle = $db->f("title");
				$userDetails['f_name'] = $f_name = $db->f("f_name");
				$userDetails['l_name'] = $l_name = $db->f("l_name");
				$userDetails['email'] = $email = $db->f("email");
				$userDetails['email_1'] = $email_1 = $db->f("email_1");
				$userDetails['email_2'] = $email_2 = $db->f("email_2");
				$userDetails['email_3'] = $email_3 = $db->f("email_3");
				$userDetails['email_4'] = $email_4 = $db->f("email_4");
				$userDetails['mobile1'] = $db->f("mobile1");
				$userDetails['mobile2'] = $db->f("mobile2");
				//$message_id = $db->f("message_id");		
				
				$authorization_id = $db->f("authorization_id");
				
				$main_client_name    =   $userDetails['f_name']." ".$userDetails['l_name'];
                $billing_name    =   $userDetails['billing_name']; 
				/*
				$table = TABLE_PAYMENT_TRANSACTION;
				$fields1 =  TABLE_PAYMENT_TRANSACTION .'.pay_received_amt_inr ' ;
				$condition1 = " WHERE ".TABLE_PAYMENT_TRANSACTION .".id =".$transaction_id." " ;
				$traArr = getRecord($table,$fields1,$condition1);
				if(!empty($traArr)){
					$tran_amount = number_format($traArr['pay_received_amt_inr'],2);
					$tran_mode = number_format($traArr['pay_received_amt_inr'],2);
				} 
				*/
				$data['ticket_text']='';
				$fields1 = TABLE_PAYMENT_TRANSACTION .'.pay_received_amt_inr, 
				'.TABLE_PAYMENT_MODE .'.payment_mode, 
				'.TABLE_PAYMENT_TRANSACTION .'.pay_bank_company, 
				'.TABLE_PAYMENT_TRANSACTION .'.pay_cheque_no,
				'.TABLE_PAYMENT_TRANSACTION .'.do_pay_received,
				'.TABLE_PAYMENT_TRANSACTION .'.pay_branch,
				'.TABLE_PAYMENT_TRANSACTION .'.pay_city_id'; 
				
				$condition_query1=" WHERE ".TABLE_PAYMENT_TRANSACTION.".id= ".$transaction_id ;
				Paymenttransaction::getDetails($db, $dataDetails, $fields1, $condition_query1) ;
				if(!empty($dataDetails)){
					$dataDetails = $dataDetails[0] ;
					$tran_amount = number_format($dataDetails['pay_received_amt_inr'],2);
					$tran_mode = $dataDetails['payment_mode'];
					$pay_bank_company = $dataDetails['pay_bank_company'];
					$pay_cheque_no = $dataDetails['pay_cheque_no'];
					$do_pay_received = $dataDetails['do_pay_received'];
					$tran_bank_name='';
					
					if(!empty($pay_bank_company)){
						$table = TABLE_PAYMENT_BANK;
						$fields1 =  TABLE_PAYMENT_BANK .'.bank_name ' ;
						$condition1 = " WHERE ".TABLE_PAYMENT_BANK .".id =".$pay_bank_company." " ;
						$bkArr = getRecord($table,$fields1,$condition1);
						if(!empty($bkArr)){
							$tran_bank_name = $bkArr['bank_name'];
						}
					}
					$pay_branch = $dataDetails['pay_branch'];
					$pay_city_id = $dataDetails['pay_city_id'];
					$city_name = '';
					if(!empty($pay_city_id)){
						$table = TABLE_LIST_CITY;
						$fields1 =  TABLE_LIST_CITY .'.name ' ;
						$condition1 = " WHERE ".TABLE_LIST_CITY .".id =".$pay_city_id." " ;
						$ctArr = getRecord($table,$fields1,$condition1);
						if(!empty($bkArr)){
							$city_name = $ctArr['name'];
						}
					}
					if($do_pay_received!='0000-00-00'){
						$temp1 = explode("-",$do_pay_received);
						$do_pay_received = $temp1[2]."-".$temp1[1]."-".$temp1[0] ;
						$tran_details = "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Transaction Mode details, if any  
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chq/DD/Tran No : ".$pay_cheque_no ." 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date : ".$do_pay_received." 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bank/NBFC : ".$tran_bank_name."	
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Branch : ".$pay_branch."  
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City : ".$city_name."</span>";
					}
					if(!empty($tran_amount ) && !empty($tran_mode)){
						
						if($type==Paymenttransaction::PAYMENTIN){
							$data['ticket_subject'] = 'Thank you for the payment';
							$data['ticket_text'] = 	"<span style=\"line-height:20px;\"><b>Thank you</b> for the payment.&nbsp; </span>
						
						 <span style=\"line-height:20px;\">We herewith confirm the receipt of payment as per following details -</span>
						
						 <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amount 	= Rs. ".$tran_amount."	</span>
						 <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Transaction Mode = ".$tran_mode."</span>	
						 ".$tran_details."
						 <span style=\"line-height:20px;\"> Cheques are subject to realisation.</span>
						 <span><b>Again, Thank you</b> for choosing SMEERP.</span>
						 "; 					
						}
						if($type==Paymenttransaction::PAYMENTOUT){
							$data['ticket_subject'] = 'Thank you for your services';
							 
							$data['ticket_text'] = 	"<span style=\"line-height:20px;\"><b>Thank you</b> for your services. &nbsp; </span>
						
						 <span style=\"line-height:20px;\">Herewith you are informed that your payment is released as per following details -</span>
						
						 <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amount 	= Rs. ".$tran_amount."	</span>
						 <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Transaction Mode     	= ".$tran_mode."</span>	
						 ".$tran_details."
										
						 <span><b>Again, Thank you</b> for your services.</span>
						 "; 					
						}
						
					}
					
				}
				
				
				 //Payment Date =<br/>
				 
				$isValid=0;
				if(!empty($transaction_id)){
					$authorization_id = trim($authorization_id,",");
					$authorizationArr = explode(",",$authorization_id);
					$isValid = in_array(Clients::ACCOUNTS,$authorizationArr);
				}
				
			}
		}
		
		$sql1 = "SELECT user_id,title,f_name, l_name,email,email_1,email_2,email_3,email_4,parent_id, authorization_id,mobile1,mobile2 FROM 
		".TABLE_CLIENTS." WHERE ( parent_id = '".$client_id."' ) AND authorization_id LIKE '%,".Clients::ACCOUNTS.",%'" ;
		$db->query($sql1) ;  
		$subUserDetails = array();        
		$subuserscount	= $db->nf();	
		if( $db->nf() > 0 ){
			while($db->next_record()){
				 $sub1_ctitle = $db->f("title");
				 $sub1_user_id = $db->f("user_id");
				 $sub1_l_name = $db->f("l_name");
				 $sub1_email = $db->f("email");
				 $sub1_email_1 = $db->f("email_1");
				 $sub1_email_2 = $db->f("email_2");
				 $sub1_email_3 = $db->f("email_3");
				 $sub1_email_4 = $db->f("email_4");
				 $sub1_mobile1 = $db->f("mobile1");
				 $sub1_mobile2 = $db->f("mobile2");		 
				 $subUserDetails[] = array(
					'user_id'=>$sub1_user_id,
					'ctitle'=>$sub1_ctitle,
					'f_name'=>$sub1_f_name,
					'l_name'=>$sub1_l_name,
					'email'=>$sub1_email,
					'email_1'=>$sub1_email_1,
					'email_2'=>$sub1_email_2,
					'email_3'=>$sub1_email_3,
					'email_4'=>$sub1_email_4,
					'mobile1'=>$sub1_mobile1,
					'mobile2'=>$sub1_mobile2
				);
			
			}
		}
		
		
		if( ( $isValid || $subuserscount > 0) && !empty($data['ticket_text']) ){
		
			//fetch ticket details bof
			$data['ticket_owner_uid'] = $userDetails['user_id'];
			$data['ticket_owner'] = $userDetails['f_name']." ".$userDetails['l_name'];
			
			if($ticket_id==0){
			  //disply random name BOF 
			   $data['display_name']=$data['display_user_id']=$data['display_designation']='';
			   $randomUser = getRandomAssociate($data['executive_id']);
			   $data['display_name'] = $randomUser['name'];
			   $data['display_user_id'] = $randomUser['user_id'];
			   $data['display_designation'] = $randomUser['desig'];	
			   
			  
			   $ticket_no  =  SupportTicket::getNewNumber($db);
			   $query = "INSERT INTO "	. TABLE_ST_TICKETS ." SET "
				. TABLE_ST_TICKETS .".ticket_no         = '". $ticket_no ."', "
				. TABLE_ST_TICKETS .".transaction_id    = '". $transaction_id ."', "
				. TABLE_ST_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
				. TABLE_ST_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
				. TABLE_ST_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
				. TABLE_ST_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
				. TABLE_ST_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
				. TABLE_ST_TICKETS .".ticket_text       = '". processUserData($data['ticket_text'])."', "
				. TABLE_ST_TICKETS .".ticket_status     = '".SupportTicket::PENDINGWITHCLIENTS ."', "
				. TABLE_ST_TICKETS .".ticket_child      = '0', "
				. TABLE_ST_TICKETS .".ticket_response_time = '0', "
				. TABLE_ST_TICKETS .".ticket_replied        = '0', "
				. TABLE_ST_TICKETS .".from_admin_panel        = '".SupportTicket::ADMIN_PANEL."', "
				. TABLE_ST_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
				. TABLE_ST_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
				. TABLE_ST_TICKETS .".do_comment    = '".date('Y-m-d H:i:s')."', "
				. TABLE_ST_TICKETS .".last_comment_from    = '".SupportTicket::ADMIN_COMMENT."', "
				. TABLE_ST_TICKETS .".ticket_date       = '". time() ."' " ;
				
			} 
			
			if ($db->query($query) && $db->affected_rows() > 0) {
                        
				$variables['hid'] = $db->last_inserted_id() ;
				$data['ticket_no']    =   $ticket_no ;
				$data['ticket_owner_uid']= $userDetails['user_id'];
				$data['ticket_owner'] = $userDetails['f_name']." ".$userDetails['l_name'] ;
				$data['mticket_no']   =   $ticket_no ;
				$data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;			 
				$data['subject']    =     $data['ticket_subject'] ;
				$data['text']    =   $data['ticket_text'] ;
				$data['attachment']   =   $attachfilename ;
				$data['link']   = DIR_WS_MP .'/support-ticket.php?perform=view&ticket_id='. $variables['hid'];
				//fetch ticket details eof 
				
				//check Main Client Valid for account followup bof
				if($isValid && !empty($data['ticket_text']) && ( $msg_by==1 ||  $msg_by==2) ){
					$data['client_name']    =   $userDetails['f_name']." ".$userDetails['l_name'];
                    $data['ctitle']    =   $userDetails['ctitle'];
					if ( getParsedEmail($db1, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
						$to = '';
						$to[]   = array('name' => $userDetails['email'], 'email' => 
						$userDetails['email']);                                   
						$from   = $reply_to = array('name' => $email["from_name"], 
						'email' => $email['from_email']);
						$mail_send_to_su  .= ",".$userDetails['email'];
						/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
						$email["isHTML"],$cc_to_sender,	$cc,$bcc,$file_name); */
						//echo  $email["body"];
						if(!empty($userDetails['email_1'])){                                       
							//$to = '';
							$to[]   = array('name' => $userDetails['email_1'], 'email' =>
							$userDetails['email_1']);                                   
							$from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);
							$mail_send_to_su  .= ",".$userDetails['email_1'];
							/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
							$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                           
						}
						if(!empty($userDetails['email_2'])){                                       
							//$to = '';
							$to[]   = array('name' => $userDetails['email_2'], 'email' =>
							$userDetails['email_2']);                                   
							$from   = $reply_to = array('name' => $email["from_name"],
							'email' => $email['from_email']);
							$mail_send_to_su  .= ",".$userDetails['email_2'];
							/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"],
							$email["isHTML"],    $cc_to_sender,$cc,$bcc,$file_name);  */
														 
						} 
						if(!empty($userDetails['email_3'])){                                       
							//$to = '';
							$to[]   = array('name' => $userDetails['email_3'], 
							'email' => $userDetails['email_3']);                                   
							$from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);
							$mail_send_to_su  .= ",".$userDetails['email_3'];
							/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
							$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
						   
						} 
						if(!empty($userDetails['email_4'])){                                       
							//$to = '';
							$to[]   = array('name' => $userDetails['email_4'], 
							'email' => $userDetails['email_4']);                                   
							$from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);
							$mail_send_to_su  .= ",".$userDetails['email_4'];                                        
							/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
							$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */                                        
						}

					}
					/* if($msg_by==2){
						$smsLength = strlen($data['ticket_text']);
						if(!empty($userDetails['mobile1']) && $smsLength<=130){ 				
							$sms_to_number[] =$userDetails['mobile1'];
							$mobile1 = "91".$userDetails['mobile1'];
							$client_mobile .= ','.$mobile1;
							ProjectTask::sendSms($data['ticket_text'],$mobile1);
							$counter = $counter+1;					
						}
						if(!empty($userDetails['mobile2']) && $smsLength<=130){
							
							$sms_to_number[] = $userDetails['mobile2'] ;
							$mobile2 = "91".$userDetails['mobile2'];
							$client_mobile .= ','.$mobile2;
							ProjectTask::sendSms($data['ticket_text'],$mobile2);
							$counter = $counter+1;                                        
						}
						if(!empty($mobile1) || !empty($mobile2)){
							 $sms_to[] = $userDetails['f_name'] .' '. $userDetails['l_name'] ;
						}      
					
					} */
				}
				
				//Send mail to subusers bof
				if(!empty($subUserDetails) && !empty($data['ticket_text']) ){
				
					foreach ($subUserDetails  as $key=>$value){
						$data['client_name']    =   $subUserDetails[$key]['f_name']." ".$subUserDetails[$key]['l_name'];
						$data['ctitle']    =   $subUserDetails[$key]['ctitle'];
						$email = NULL;
						$cc_to_sender= $cc = $bcc = Null;
						if ( getParsedEmail($db1, $s, 'EMAIL_ST_MEMBER', $data, $email) ) {
							//$to = '';
							$to[]   = array('name' => $subUserDetails[$key]['email'], 
							'email' => $subUserDetails[$key]['email']);
							$from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);
							$mail_send_to_su .= ",".$subUserDetails[$key]['email'] ;
							/*if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
							$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
							}*/
							if(!empty($subUserDetails[$key]['email_1'])){
								$to[]   = array('name' => $subUserDetails[$key]['email_1'], 
								'email' => $subUserDetails[$key]['email_1']);
								$from   = $reply_to = array('name' => $email["from_name"],
								'email' => $email['from_email']);
								$mail_send_to_su .= ",".$subUserDetails[$key]['email_1'] ;
							} 
							if(!empty($subUserDetails[$key]['email_2'])){                                       
								//$to = '';
								$to[]   = array('name' => $subUserDetails[$key]['email_2'], 'email' => $subUserDetails[$key]['email_2']);
								$from   = $reply_to = array('name' => $email["from_name"],
								'email' => $email['from_email']);
								$mail_send_to_su .= ",".$subUserDetails[$key]['email_2'] ;
								/*if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
								  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
								} */
							}
							if(!empty($subUserDetails[$key]['email_3'])){                                       
								//$to = '';
								$to[]   = array('name' =>$subUserDetails[$key]['email_3'], 'email' => $subUserDetails[$key]['email_3']);
								$from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);
								$mail_send_to_su .= ",".$subUserDetails[$key]['email_3'] ;
								/* 
								if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
								  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
									
								} */
							}
							if(!empty($subUserDetails[$key]['email_4'])){                                       
								//$to = '';
								$to[]   = array('name' => $subUserDetails[$key]['email_4'], 'email' => $subUserDetails[$key]['email_4']);
								$from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);
								$mail_send_to_su .= ",".$subUserDetails[$key]['email_4'] ;
								/* 
								if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
								  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
									
								} */
							}
							
						}
						/* 
						if($msg_by==2){
							$smsLength = strlen($data['ticket_text']);						
							if(!empty($subUserDetails[$key]['mobile1']) && $smsLength<=130){
								$sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
								$sms_to_number[] =$subUserDetails[$key]['mobile1'];
								$mobile1 = "91".$subUserDetails[$key]['mobile1'];
								$client_mobile .= ','.$mobile1;
								ProjectTask::sendSms($data['ticket_text'],$mobile1);
								$counter = $counter+1;
							}
							if(!empty($subUserDetails[$key]['mobile2']) && $smsLength<=130){
								$sms_to[]  = $value['f_name'] .' '. $value['l_name'] ;
								$sms_to_number[] =$subUserDetails[$key]['mobile2'];
								$mobile2 = "91".$subUserDetails[$key]['mobile2'];
								$client_mobile .= ','.$mobile2;
								ProjectTask::sendSms($data['ticket_text'],$mobile2);
								$counter = $counter+1;
							}
						} */
					}
				
				}
				//Send mail to subusers eof
				if(!empty($to)){
					$bcc[]   = array('name' => '','email' => $smeerp_client_email);
					
					$from   = $reply_to = array('name' => $email["from_name"], 
								'email' => $email['from_email']);
					if(SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
					  $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name)){
						//echo $email["subject"];
						//echo $email["body"];
					}
				
				}
				
				
				
				$query_update = "UPDATE ". TABLE_ST_TICKETS 
							." SET ". TABLE_ST_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', sms_to_number='".implode(",",array_unique($sms_to_number))."',
								sms_to ='".implode(",",array_unique($sms_to))."' "                                    
							." WHERE ". TABLE_ST_TICKETS .".ticket_id = '". $variables['hid'] ."' " ;
				$db->query($query_update) ;
				//Set the list subuser's email-id to whom the mails are sent eof                        
				//update sms counter
				if($counter >0){
					$sql = "UPDATE ".TABLE_SITE_SETTINGS." SET sms_counter=sms_counter +".$counter ;
					$db->query($sql); 
				}
				 
				// Send Email to the admin EOF
				/* $via='';
				if(!empty($mail_send_to_su)){
					$via = 'Mail';
				}
				if(!empty($sms_to)){
					$via .= ' & SMS';
				} */
                $messageText= " We informed the Vendor by ".$via." for transaction no ".$transaction_no;  
				echo $success_message = "<table id=\"table_message_ok\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_header_ok\">
							<img src=\"".addslashes($variables['nc_images'])."/success.png\" border=\"0\" />&nbsp;
							Success
						</td>
					</tr>
					<tr>
						<td class=\"message_ok\" align=\"left\" valign=\"top\">
							<ul class=\"message_ok_2\">
								<li class=\"message_ok\">".$messageText." </li>
							</ul>
						</td>
					</tr>
					</table>";
				
			}
			
			
		
		}else{
		
			$messageText = " There are no authorised persons for Accounts for transaction no ".$transaction_no;
			
			echo $error_message = "<table id=\"table_message_error\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
					<tr>
						<td class=\"message_error_header\">
							<img src=\"".addslashes($variables['nc_images'])."/exclamation.png\" border=\"0\" />&nbsp;
							Not Success
						</td>
					</tr>
					<tr>
						<td class=\"message_error\" align=\"left\" valign=\"top\">
							<ul class=\"message_error\">
								<li class=\"message_error\">".$messageText." </li>
							</ul>
						</td>
					</tr>
					</table>";
		
		}
		
		
		
		
		
		
		
	
		
		
   	   
	}	


?>
