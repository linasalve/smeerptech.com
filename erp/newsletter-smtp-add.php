<?php
    if ( $perm->has('nc_nwl_smtp_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
       
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);             
           
            $extra = array( 'db' 				=> &$db,
                         	'messages'          => &$messages
                        );
                
            if ( NewsletterSmtp::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_NEWSLETTER_SMTP
                            ." SET ". TABLE_NEWSLETTER_SMTP .".sendmethod    	= '". $data['sendmethod'] ."', "
							. TABLE_NEWSLETTER_SMTP .".sockethost    			= '". $data['sockethost'] ."', "
							. TABLE_NEWSLETTER_SMTP .".smtpauth      			= '". $data['smtpauth'] ."', "
							. TABLE_NEWSLETTER_SMTP .".smtpauthuser  			= '". $data['smtpauthuser'] ."', "
							. TABLE_NEWSLETTER_SMTP .".smtpauthpass  			= '". $data['smtpauthpass'] ."', "
							. TABLE_NEWSLETTER_SMTP .".smtpport       			= '". $data['smtpport'] ."', "
							. TABLE_NEWSLETTER_SMTP .".from_name       			= '". $data['from_name'] ."', "
							. TABLE_NEWSLETTER_SMTP .".from_email       		= '". $data['from_email'] ."', "
							. TABLE_NEWSLETTER_SMTP .".replyto_name    			= '". $data['replyto_name'] ."', "
							. TABLE_NEWSLETTER_SMTP .".replyto_email    		= '". $data['replyto_email'] ."', "
							. TABLE_NEWSLETTER_SMTP .".client    				= '". $data['client'] ."', "
							. TABLE_NEWSLETTER_SMTP .".domain_name    			= '". $data['domain_name'] ."', "
							. TABLE_NEWSLETTER_SMTP .".domain_mailgun_key 	    = '". $data['domain_mailgun_key'] ."', "
							. TABLE_NEWSLETTER_SMTP .".status 					= '". $data['status'] ."'".","
							. TABLE_NEWSLETTER_SMTP .".created_by 				= '". $my['uid'] ."'".","
							. TABLE_NEWSLETTER_SMTP .".ip 						= '". $_SERVER['SERVER_ADDR'] ."'".","
							. TABLE_NEWSLETTER_SMTP .".do_e 					= '". date('Y-m-d')."'";
                
                
                
                
                
                  
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Newsletter entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/newsletter-smtp.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/newsletter-smtp.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/newsletter-smtp.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'newsletter-smtp-add.html');
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>