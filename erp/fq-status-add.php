<?php
  if ( $perm->has('nc_fq_s_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];
        
		// Include the payment party class
		//include_once (DIR_FS_INCLUDES .'/quotation-subject.inc.php');
		
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
                       
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
               
                if ( FqStatus::validateAdd($data, $extra) ) { 
                            $query	= " INSERT INTO ".TABLE_FQ_STATUS
                                    ." SET ".TABLE_FQ_STATUS .".status_name     = '". $data['status_name'] ."'"  
                                    .",". TABLE_FQ_STATUS .".status_color       = '". $data['status_color'] ."'"                                
                                    .",". TABLE_FQ_STATUS .".status_font_color  = '". $data['status_font_color'] ."'"                                
                                	.",". TABLE_FQ_STATUS .".status_status      = '". $data['status_status'] ."'"
                                    .",". TABLE_FQ_STATUS .".created_by      = '". $my['user_id'] ."'"
                                    .",". TABLE_FQ_STATUS .".date      = '". date('Y-m-d H:i:s') ."'"
                                    .",". TABLE_FQ_STATUS .".ip      = '". $_SERVER['REMOTE_ADDR'] ."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Record has been added successfully.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }               
        
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/fq-status.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/fq-status.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
           
            //include ( DIR_FS_NC .'/payment-transaction-list.php');
            header("Location:".DIR_WS_NC."/fq-status.php?added=1");
            
        }else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'fq-status-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
