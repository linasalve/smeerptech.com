<?php
    if ( $perm->has('nc_cb_add') ) {
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        $access_level   = $my['access_level'];           
        
		// Include the  class.
		include_once (DIR_FS_INCLUDES .'/complain-box.inc.php');
		
         // BO: Read the Team members list ie member list in dropdown box BOF
        $lst_executive = Null;
        $fields = TABLE_USER .'.user_id'
                   .','. TABLE_USER .'.f_name'
                   .','. TABLE_USER .'.l_name';
        User::getList($db,$lst_executive,$fields);
        // BO:  Read the Team members list ie member list in dropdown box  EOF 
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            
            // Set the Creator.
            if ( !isset($data['created_by']) || empty($data['created_by']) ) {
                $data['created_by'] = $my['user_id'];
            }
             
           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        );
                
            if ( Complainbox::validateAdd($data, $extra) ) { 
                $query	= " INSERT INTO ".TABLE_COMPLAIN_BOX
                            ." SET ". TABLE_COMPLAIN_BOX .".title = '".                 $data['title'] ."'"
                                .",". TABLE_COMPLAIN_BOX .".complain = '".              $data['complain'] ."'"
                                .",". TABLE_COMPLAIN_BOX .".access_level = '".         $my['access_level'] ."'"
                                .",". TABLE_COMPLAIN_BOX .".created_by = '".           $data['created_by'] ."'"
                                .",". TABLE_COMPLAIN_BOX .".complain_of = '".          $data['complain_of'] ."'"
                                .",". TABLE_COMPLAIN_BOX .".priority = '".               $data['priority'] ."'"
                                .",". TABLE_COMPLAIN_BOX .".status = '".                $data['status'] ."'"
                                .",". TABLE_COMPLAIN_BOX .".date = '".                 date('Y-m-d')."'";
                
                if ( $db->query($query) && $db->affected_rows() > 0 ) {
                    $messages->setOkMessage("Address entry has been done.");
                    $variables['hid'] = $db->last_inserted_id();              
                }
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
        
        
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
             header("Location:".DIR_WS_NC."/complain-box.php?perform=add&added=1");
        }
        if(isset($_POST['btnCancel'])){
             header("Location:".DIR_WS_NC."/complain-box.php");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
            header("Location:".DIR_WS_NC."/complain-box.php?added=1");   
        }
        else {
        
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');           
            $page["var"][] = array('variable' => 'lst_executive', 'value' => 'lst_executive');   
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'complain-box-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }        
?>
