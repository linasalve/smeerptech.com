<?php
if ( $perm->has('nc_rp_tr') ) {    

    include_once ( DIR_FS_INCLUDES .'/task-reminder.inc.php' );
    $variables["completed"]            = Taskreminder::COMPLETED;
    $variables["pending"]           = Taskreminder::PENDING;
    $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                            TABLE_TASK_REMINDER   =>  array(  'ID'            => 'id',
                                                            'Task title'   => 'task',
                                                            'Task summury' => 'comment',
                                                            'Status'        => 'status'
                                                    ),
                            );
    
    $sOrderByArray  = array(
                            TABLE_TASK_REMINDER => array( 'ID'               => 'id',
                                                          'Reminder Date '  => 'do_r',
                                                          'Date'  => 'do_e'
                                                        
                                                ),
                        );

    // Set the sorting order of the user list.
    if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
        $_SEARCH['sOrderBy']= $sOrderBy = 'do_e';
        $_SEARCH['sOrder']  = $sOrder   = 'DESC';
        $order_by_table     = TABLE_TASK_REMINDER;
    }
    
     // Read the available Status 
    $variables['status'] = Taskreminder::getStatus();
    $variables['priority'] = Taskreminder::getPriority();
    //Search code EOF
    $condition_query = isset($_POST["condition_query"]) ? $_POST["condition_query"] : (isset($_GET["condition_query"]) ? $_GET["condition_query"]:'');
    $condition_url  = '';
    $where_added    = false;
    
	if ( $sString != "" ) {
		// Set the field on which to make the search.
		if ( !($search_table = findIndex($sType, $sTypeArray )) ) {
			$sType = "";
			$search_table = NULL ;
		}

        if ( $search_table == "Any" ) {
            $condition_query .= " WHERE (";
            foreach( $sTypeArray as $table => $field_arr ) {
                if ( $table != "Any" ) {
                    
                    $table = TABLE_TASK_REMINDER ;
                    foreach( $field_arr as $key => $field ) {
                            $condition_query .= " ". $table .".". clearSearchString($field) ." LIKE '%". $sString ."%' OR " ;
                            $where_added = true;
                        }
                    $condition_query = substr( $condition_query, 0, strlen( $condition_query ) - 3 );
                }
            }
            $condition_query .= ") ";
        }
        elseif ( $search_table != "Any" && $search_table != NULL) {
            $condition_query .= " WHERE  (". $search_table .".". clearSearchString($sType) ." LIKE '%". $sString ."%' )" ;
            $where_added = true;
        }
		$_SEARCH["sString"] = $sString;
		$_SEARCH["sType"] 	= $sType;
	}

    $condition_url      .= "&sString=$sString&sType=$sType";
    $_SEARCH["sString"] = $sString ;
    $_SEARCH["sType"]   = $sType ;
    
    // BO: Status data.
    
    $chk_status = isset($_POST["chk_status"])   ? $_POST["chk_status"]  : (isset($_GET["chk_status"])   ? $_GET["chk_status"]   :'');
    $sStatus    = isset($_POST["sStatus"])      ? $_POST["sStatus"]     : (isset($_GET["sStatus"])      ? $_GET["sStatus"]      :'');
    if ( $chk_status == 'AND' || $chk_status == 'OR') {
        if ( $where_added ) {
            $condition_query .= " ". $chk_status;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
      
        
        $condition_query .= " ". TABLE_TASK_REMINDER .".status IN ('". implode("','", $sStatus) ."') ";
    }
    $condition_url          .= "&chk_status=$chk_status&sStatus=$sStatus";
    $_SEARCH["chk_status"]  = $chk_status;
    $_SEARCH["sStatus"]     = $sStatus;
    // EO: Status data.
    
	// Read the Date data.
    $chk_date_from  = isset($_POST["chk_date_from"])? $_POST["chk_date_from"]   : (isset($_GET["chk_date_from"])? $_GET["chk_date_from"]:'');
    $date_from      = isset($_POST["date_from"])    ? $_POST["date_from"]       : (isset($_GET["date_from"])    ? $_GET["date_from"]    :'');
    $chk_date_to    = isset($_POST["chk_date_to"])  ? $_POST["chk_date_to"]     : (isset($_GET["chk_date_to"])  ? $_GET["chk_date_to"]  :'');
    $date_to        = isset($_POST["date_to"])      ? $_POST["date_to"]         : (isset($_GET["date_to"])      ? $_GET["date_to"]      :'');
    
    // BO: From Date
    if ( $chk_date_from == 'AND' || $chk_date_from == 'OR') {
        if ( $where_added ) {
            $condition_query .= " ". $chk_date_from;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dfa='';
        if(!empty($date_from)){
            $dfa = explode('/', $date_from);
            $dfa = $dfa[2] .'-'. $dfa[1] .'-'. $dfa[0] .' 00:00:00';
        }
        $condition_query .= " ". TABLE_TASK_REMINDER .".do_r >= '". $dfa ."'";
    }
    $condition_url              .= "&chk_date_from=$chk_date_from&date_from=$date_from";
    $_SEARCH["chk_date_from"]   = $chk_date_from;
    $_SEARCH["date_from"]       = $date_from;
    // EO: From Date
    
    // BO: Upto Date
    if ( $chk_date_to == 'AND' || $chk_date_to == 'OR') {
        if ( $where_added ) {
            $condition_query .= $chk_date_to;
        }
        else {
            $condition_query.= ' WHERE ';
            $where_added    = true;
        }
        $dta ='';
        if(!empty($date_to)){
            $dta = explode('/', $date_to);
            $dta = $dta[2] .'-'. $dta[1] .'-'. $dta[0] .' 23:59:59';
        }
        $condition_query .= " ". TABLE_TASK_REMINDER .".do_r <= '". $dta ."'";
    }
    $condition_url          .= "&chk_date_to=$chk_date_to&date_to=$date_to";
    $_SEARCH["chk_date_to"] = $chk_date_to ;
    $_SEARCH["date_to"]     = $date_to ;
    // EO: Upto Date
    
    $condition_url .= "&sOrderBy=$sOrderBy&sOrder=$sOrder&rpp=$rpp&action=search";
			

	$_SEARCH["sOrderBy"]	= $sOrderBy ;
	$_SEARCH["sOrder"] 		= $sOrder ;
    $_SEARCH["searched"]    = true ;
    $condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
    //Search code BOF


    

    // To count total records.
    $list	= 	NULL;
    $total	=	Taskreminder::getDetails( $db, $list, '', $condition_query);

    $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';

    $list	= NULL;
    $fields = TABLE_TASK_REMINDER .'.*'    ;
                
    Taskreminder::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
    $fList=array();
    if(!empty($list)){
        foreach( $list as $key=>$val){      
           $executive=array();
           $string = str_replace(",","','", $val['allotted_to']);
           $fields1 =  TABLE_USER .'.f_name'.','. TABLE_USER .'.l_name';
           $condition1 = " WHERE ".TABLE_USER .".user_id IN('".$string."') " ;
           User::getList($db,$executive,$fields1,$condition1);
           $executivename='';
          
           foreach($executive as $key1=>$val1){
                $executivename .= $val1['f_name']." ".$val1['l_name']."<br/>" ;
           } 
           $val['allotted_to'] = $executivename ;
           $statusArr = Taskreminder::getStatus();
           $statusArr = array_flip($statusArr);
           $val['status'] = $statusArr[$val['status']];
           $priorityArr = Taskreminder::getPriority();
           $priorityArr = array_flip($priorityArr);
           $val['priority'] = $priorityArr[$val['priority']];
           $exeCreted=$exeCreatedname='';
           $condition2 = " WHERE ".TABLE_USER .".user_id= '".$val['created_by']."' " ;
           User::getList($db,$exeCreted,$fields1,$condition2);
          
           foreach($exeCreted as $key2=>$val2){
                $exeCreatedname .= $val2['f_name']." ".$val2['l_name']."<br/>" ;
           } 
           $val['created_by'] = $exeCreatedname;
           
           
           $fList[$key]=$val;
        }
    }
  
    // Set the Permissions.
    $variables['can_view_list']     = false;
    $variables['can_add']           = false;
    $variables['can_edit']          = false;
    $variables['can_delete']        = false;
    $variables['can_view_details']  = false;
   
    if ( $perm->has('nc_ts_list') ) {
        $variables['can_view_list'] = true;
    }
    if ( $perm->has('nc_ts_add') ) {
        $variables['can_add'] = true;
    }
    if ( $perm->has('nc_ts_edit') ) {
        $variables['can_edit'] = true;
    }
    if ( $perm->has('nc_ts_delete') ) {
        $variables['can_delete'] = true;
    }
    if ( $perm->has('nc_ts_details') ) {
        $variables['can_view_details'] = true;
    }
    
    
    $page["var"][] = array('variable' => 'list', 'value' => 'fList');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
    $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'reports-task-reminder.html');
}
else {
     $messages->setErrorMessage("You do not have the permission to view the list.");
}
    
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
?>