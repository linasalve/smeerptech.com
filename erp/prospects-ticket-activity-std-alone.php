<?php
    if ( $perm->has('nc_pst_add') || $perm->has('nc_pst_flw_ord')) {
		include_once ( DIR_FS_INCLUDES .'/st-template-category.inc.php');
        $subUserDetails=$additionalEmailArr= null;
        //$user_id = isset ($_GET['user_id']) ? $_GET['user_id'] : ( isset($_POST['user_id'] ) ? $_POST['user_id'] :'');
        $step1 = isset ($_GET['step1']) ? $_GET['step1'] : ( isset($_POST['step1'] ) ? $_POST['step1'] : '1');
        $step2 = isset ($_GET['step2']) ? $_GET['step2'] : ( isset($_POST['step2'] ) ? $_POST['step2'] : '0');
        $client_inv = isset ($_GET['client_inv']) ? $_GET['client_inv'] : ( isset($_POST['client_inv'] ) ? 
		$_POST['client_inv'] : '');
		$client = isset ($_GET['client']) ? $_GET['client'] : ( isset($_POST['client'] ) ?	$_POST['client'] : '');
		$invoice_id = isset ($_GET['invoice_id']) ? $_GET['invoice_id'] : ( isset($_POST['invoice_id'] ) ?	$_POST['invoice_id'] : '');
		$flw_ord_id = isset ($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : ( isset($_POST['flw_ord_id'] ) ?	$_POST['flw_ord_id'] : '');
						
		$_ALL_POST	= NULL;
        $data       = NULL;
        $stTemplates=null;
		$_ALL_POST = isset ($_POST['perform']) ? $_POST : ( isset($_GET['perform'] ) ? $_GET : NULL);
        
		$fpendingInvlist = array();
		$totalBalanceAmount=0;
		 
		/*
		As  $flw_ord_id>0, means this ticket is viewed using prospect order list.
		$ticket_id=0 means no ticket created against this order.
		Check on runtime again main ticket id against flw_ord_id, if $ticket_id==0 then create one default ticket against the flw_ord_id
		*/
		//Create default ticket for lead order $flw_ord_id
		if(!empty($client)){
			$table = TABLE_PROSPECTS;
			$condition2 = " WHERE ".TABLE_PROSPECTS .".user_id= '".$client ."' " ;
			$fields1 =  TABLE_PROSPECTS .".f_name,".TABLE_PROSPECTS.".l_name,".TABLE_PROSPECTS.".billing_name" ;
			$clientArr = getRecord($table,$fields1,$condition2);
			if(!empty($clientArr)){
				 
				$data1['ticket_owner_uid'] = $client;
				$data1['name']= $clientArr['f_name']." ".$clientArr['l_name'] ;
				$data1['ticket_owner'] = $clientArr['billing_name']." ";
			} 		
			if(!empty($data1['name'])){
				$data1['ticket_owner'] = $clientArr['billing_name']." ".$data1['name'];
			}
		}
		if($ticket_id==0  &&  $flw_ord_id>0 && !empty($client)){
			$sql1 = "SELECT ".TABLE_PROSPECTS_TICKETS.".ticket_id FROM ".TABLE_PROSPECTS_TICKETS." WHERE 
				(".TABLE_PROSPECTS_TICKETS.".ticket_child=0 AND ".TABLE_PROSPECTS_TICKETS.".flw_ord_id = '".$flw_ord_id."' AND ".TABLE_PROSPECTS_TICKETS.".ticket_type = '".ProspectsTicket::TYP_COMM."') LIMIT 0,1";
			if ( $db->query($sql1) ){
				if ( $db->nf() > 0 ){
					while ($db->next_record()) {
					   $ticket_id_new = $db->f('ticket_id');
					}
				}                   
			}
			if($ticket_id_new==0){ 
			
				$data['ticket_text'] = "This is Default ticket created. On Date - ".date('d M Y') ;
				
				$data['hrs']='';
				$data['min']=5;  
											 
				$hrs1 = (int) ($data['hrs'] * 6);
				$min1 = (int) ($data['min'] * 6);  	
				$data['posted_by'] = AUTO_USER_ID  ; 
				$data['posted_by_name'] = AUTO_USER_NAME ;  
				$ticket_no  =  ProspectsTicket::getNewNumber($db);
				
				$followup_query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
				. TABLE_PROSPECTS_TICKETS .".ticket_no   = '". $ticket_no ."', "
				. TABLE_PROSPECTS_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data1['ticket_owner_uid'] ."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data1['ticket_owner'] ."', "
				. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data_mkt['tck_owner_member_id'] ."', " 
				. TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data_mkt['tck_owner_member_name'] ."', "
				. TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data_mkt['tck_owner_member_email'] ."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $data['posted_by'] ."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $data['posted_by_name'] ."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_status     = '".ProspectsTicket::PENDINGWITHCLIENTS ."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_child      = '0',"
				. TABLE_PROSPECTS_TICKETS .".ticket_type      = '".ProspectsTicket::TYP_COMM."',"
				. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
				. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
				. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
				. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
				. TABLE_PROSPECTS_TICKETS .".ticket_replied        = '0', "
				. TABLE_PROSPECTS_TICKETS .".from_admin_panel        = '".ProspectsTicket::ADMIN_PANEL."', "
				. TABLE_PROSPECTS_TICKETS .".display_name           = '". $my['f_name']." ". $my['l_name'] ."', "
				. TABLE_PROSPECTS_TICKETS .".display_user_id        = '". $my['uid'] ."', "
				. TABLE_PROSPECTS_TICKETS .".display_designation    = '". $my['desig'] ."', "
				. TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
				. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
				. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
				. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
				. TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;
				
				$db->query($followup_query) ; 
				$ticket_id = $db->last_inserted_id() ;
			}else{			
				$ticket_id = $ticket_id_new ;
			} 
		}
      
        
        $variables["MAX_FILE_SIZE"] = MAX_MULTIPLE_FILE_SIZE ;
        $variables['max_file_size_in_kb'] = MAX_MULTIPLE_FILE_SIZE/1024;
        $variables['max_file_size_in_mb'] = MAX_MULTIPLE_FILE_SIZE/(1024*1024);
        $variables["status"] = ProspectsTicket::getTicketStatusList();
		
        //hrs, min array
        $lst_hrs = $lst_min = $lst_typ= NULL;
        $lst_hrs[0] = 'Hr';
        for($i=1; $i<=20;$i++){
            if(strlen($i)==1) $i='0'.$i ;
            $lst_hrs[$i] = $i;
        }
        //$lst_min[0] = 'Min';
        for($j=12; $j<=59 ;$j++){
            if(strlen($j)==1) $j='0'.$j ;
            $lst_min[$j] = $j;
        } 
		//List of Leads of current prospect where current user exist BOF
		$prospectOrdArr=array();
		if(!empty($client)){
			$query =" SELECT DISTINCT(".TABLE_PROSPECTS_ORDERS.".id),".TABLE_PROSPECTS_ORDERS.".order_title,".TABLE_PROSPECTS_ORDERS.".number FROM ".TABLE_PROSPECTS_ORDERS." WHERE (".TABLE_PROSPECTS_ORDERS.".client = '".$client."' OR ".TABLE_PROSPECTS_ORDERS.".clients_su LIKE '%,".$client.",%' ) AND ( ".TABLE_PROSPECTS_ORDERS.".team LIKE '%,".$my['user_id'].",%' )  ";		
			$db->query($query);			
			if ( $db->nf()>0 ) { 
				while ($db->next_record()){  
					$prsp_ord_id=$db->f('id');
					$prsp_ord_title=$db->f('order_title');
					$prsp_ord_no=$db->f('number');
					$prospectOrdArr[] = array(
						'prsp_ord_id'=>$prsp_ord_id,
						'prsp_ord_title'=>$prsp_ord_title,
						'prsp_ord_no'=>$prsp_ord_no 
					);
					 
				} 
			}
		}
		//List of Leads of current prospect where current user exist OF
		
		
        if ( (isset($_ALL_POST['btnCreate']) || isset($_ALL_POST['btnReturn'])) && $_ALL_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST);
            $files       = processUserData($_FILES);
          
            $data['files'] = $files ;
            $data['allowed_file_types'] = $allowed_file_types ;
            $data['max_file_size'] = MAX_MULTIPLE_FILE_SIZE ;
			
            $extra = array( 'db' 		=> &$db,
                            'messages'  => $messages,
                        ); 
             
                if ( ProspectsTicket::validateReportingAdd($data, $extra) ) { 
                     
                    $ticket_no  =  ProspectsTicket::getNewNumber($db);
                    $attachfilename=$ticket_attachment_path='';
                    
					$data['display_name'] = $my['f_name']." ".$my['l_name'];
					$data['display_user_id'] = $my['user_id'];
					$data['display_designation'] = $my['desig'];
					 
					//if($my['department'] == ID_MARKETING){
						//This is the marketing person identity
						$data['tck_owner_member_id'] = $my['user_id'];
						$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
						$data['tck_owner_member_email'] = $my['email'];
						$data['marketing_email'] = $my['email'];
						$data['marketing_contact'] = $my['marketing_contact'];
						$data['marketing'] = 1;
					/*}else{ 
						$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
						$data['tck_owner_member_name']= SALES_MEMBER_USER_NAME;
						$data['tck_owner_member_email'] = SALES_MEMBER_USER_EMAIL;
						$data['marketing_email'] = SALES_MEMBER_USER_EMAIL;
						$data['marketing_contact'] = '';
						$data['marketing'] = 0;
					} */
					 
                    $hrs1 = (int) ($data['hrs'] * 6);
					$min1 = (int) ($data['min'] * 6);     
					if(empty($data['ss_id'])){		
						$data['ss_title']='';
						$data['ss_file_1']='';
						$data['ss_file_2']='';
						$data['ss_file_3']='';
					}
					
                     $query = "INSERT INTO "	. TABLE_PROSPECTS_TICKETS ." SET "
                        . TABLE_PROSPECTS_TICKETS .".ticket_no         = '". $ticket_no ."', "
                        . TABLE_PROSPECTS_TICKETS .".invoice_id         = '". $invoice_id ."', "
                        . TABLE_PROSPECTS_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
					/*  . TABLE_PROSPECTS_TICKETS .".template_id      = '". $data['template_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_PROSPECTS_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ss_id      		= '". $data['ss_id'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ss_title   		= '". $data['ss_title'] ."', "
						. TABLE_PROSPECTS_TICKETS .".ss_file_1   		= '". $data['ss_file_1'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ss_file_2   		= '". $data['ss_file_2'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ss_file_3   		= '". $data['ss_file_3'] ."', " */
                        . TABLE_PROSPECTS_TICKETS .".ticket_owner_uid  = '". $data1['ticket_owner_uid'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_owner      = '". $data1['ticket_owner'] ."', "
						. TABLE_PROSPECTS_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".on_behalf      = '". $data['on_behalf'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
                        //. TABLE_PROSPECTS_TICKETS .".ticket_department = '". $data['ticket_department'] ."', "
                        //. TABLE_PROSPECTS_TICKETS .".ticket_priority   = '". $data['ticket_priority'] ."', "
                        //. TABLE_PROSPECTS_TICKETS .".ticket_status     = '". ProspectsTicket::PENDINGWITHCLIENTS ."', "
                        //. TABLE_PROSPECTS_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
                        //. TABLE_PROSPECTS_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_status     = '". $data['ticket_status'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_child      = '0', "
						. TABLE_PROSPECTS_TICKETS .".ticket_attachment = '". $attachfilename ."', "
						. TABLE_PROSPECTS_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_PROSPECTS_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_PROSPECTS_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_PROSPECTS_TICKETS .".min = '". $data['min']."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_attachment_path = '". $ticket_attachment_path ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_response_time = '0', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_replied     = '0', "
                        . TABLE_PROSPECTS_TICKETS .".from_admin_panel   = '".ProspectsTicket::ADMIN_PANEL."', "
                        . TABLE_PROSPECTS_TICKETS .".domain_name        = '". $data['domain_name'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".display_name       = '". $data['display_name'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".display_user_id  	  = '". $data['display_user_id'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".do_activity    = '". $data['do_activity'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".medium_comm    = '". $data['medium_comm'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_type    = '". ProspectsTicket::TYP_REPORTING ."', "
                     // . TABLE_PROSPECTS_TICKETS .".is_private           = '". $data['is_private'] ."', "
                        . TABLE_PROSPECTS_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_PROSPECTS_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_PROSPECTS_TICKETS .".last_comment_from = '".ProspectsTicket::ADMIN_COMMENT."', "
                        . TABLE_PROSPECTS_TICKETS .".ticket_date       = '". time() ."' " ;  
						
                    if ($db->query($query) && $db->affected_rows() > 0) {                        
                        $variables['hid'] = $db->last_inserted_id() ;
						
                        /**************************************************************************
                        * Send the notification to the ticket owner and the Administrators
                        * of the new Ticket being created
                        **************************************************************************/
                        /*
						$data['ticket_no']    =   $ticket_no ; 
                        $data['domain']   = THIS_DOMAIN;                        
                        $data['mticket_no']   =   $ticket_no ;
                        $data['replied_by']   =   $my['f_name']." ".$my['l_name'] ;
                       	$data['subject']    =     $_ALL_POST['ticket_subject'] ;
                        $data['text']    =   $_ALL_POST['ticket_text'] ;
                        $data['attachment']   =   $attachfilename ;
                        $data['link']   = DIR_WS_MP .'/prospects-ticket.php?perform=view&ticket_id='. $variables['hid']; 
                        $file_name = '';
						if(!empty($attachfilename)){
							$file_name = DIR_FS_PST_FILES ."/". $attachfilename;
						}
						if(!empty($data['template_file_1'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
						}
						if(!empty($data['template_file_2'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
						}
						if(!empty($data['template_file_3'])){
							$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
						} 
						if(!empty($data['ss_file_1'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_1'];
						}
						if(!empty($data['ss_file_2'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_2'];
						}
						if(!empty($data['ss_file_3'])){
							$file_name[] = DIR_FS_SVC_FILES ."/". $data['ss_file_3'];
						}
						*/
                        $messages->setOkMessage("Activity details added.");
                    }
                    //to flush the data.
                    //$_ALL_POST	= NULL;
                    //$data		= NULL;
                }else{
                    $condition_query1 = " WHERE user_id = '". $data['ticket_owner_uid'] ."' ";
                    if ( Prospects::getList($db, $_ALL_POST['client_details'], 'user_id, 	 f_name,l_name,additional_email,status,send_mail,org,billing_name, 
                        email,email_1,email_2, email_3,email_4,mobile1, mobile2,authorization_id', $condition_query1) > 0 ) {
                        $_ALL_POST['user_id']=$_ALL_POST['client_details']['0']['user_id'];
                        $_ALL_POST['name']=$_ALL_POST['client_details']['0']['f_name']." ".$_ALL_POST['client_details']['0']['l_name'];
                        //$_ALL_POST['email']=$_ALL_POST['email'];
                        $_ALL_POST['billing_name']=$_ALL_POST['client_details']['0']['billing_name'];
                        $_ALL_POST['additional_email']=$_ALL_POST['client_details']['0']['additional_email'];
                        $_ALL_POST['mobile1']=$_ALL_POST['client_details']['0']['mobile1'];
                        $_ALL_POST['mobile2']=$_ALL_POST['client_details']['0']['mobile2'];
                        $_ALL_POST['send_mail']=$_ALL_POST['client_details']['0']['send_mail'];
                        $_ALL_POST['email']=$_ALL_POST['client_details']['0']['email'];
                        $_ALL_POST['email_1']=$_ALL_POST['client_details']['0']['email_1'];
                        $_ALL_POST['email_2']=$_ALL_POST['client_details']['0']['email_2'];
                        $_ALL_POST['email_3']=$_ALL_POST['client_details']['0']['email_3'];
                        $_ALL_POST['email_4']=$_ALL_POST['client_details']['0']['email_4'];
						$all_additional_email = trim($_ALL_POST['additional_email'],",");
						$additionalEmailArr = explode(",",$all_additional_email); 
						$_ALL_POST['accountValid'] = 1; 
                    }  
                }  
           
        }
		
        
        if(isset($_POST['btnCreate']) && $data['step1']!='1' && $messages->getErrorMessageCount() <= 0 ){
			if(!empty($invoice_id) || !empty($flw_ord_id)){
				header("Location:".DIR_WS_NC."/prospects-ticket.php?perform=activity_std_alone&added=1&invoice_id=".$invoice_id."&flw_ord_id=".$flw_ord_id."&client=".$client);
			}else{
				header("Location:".DIR_WS_NC."/prospects-ticket.php?perform=activity_std_alone&added=1");
			}
        }
        
			//GET LIST OF TODAY'S ACTIVITY BOF
			$sql1 = "SELECT ".TABLE_PROSPECTS_TICKETS.".ticket_text,".TABLE_PROSPECTS_TICKETS.".do_activity,".TABLE_PROSPECTS_TICKETS.".ticket_type,
				 ".TABLE_PROSPECTS_TICKETS.".medium_comm,".TABLE_PROSPECTS_ORDERS.".order_title, ".TABLE_PROSPECTS_ORDERS.".number as or_no
				 FROM ".TABLE_PROSPECTS_TICKETS." LEFT JOIN ".TABLE_PROSPECTS_ORDERS."
				 ON ".TABLE_PROSPECTS_TICKETS.".flw_ord_id = ".TABLE_PROSPECTS_ORDERS.".id
				 WHERE ".TABLE_PROSPECTS_TICKETS .".ticket_type  = '". ProspectsTicket::TYP_REPORTING ."'  
				 AND date_format(".TABLE_PROSPECTS_TICKETS.".do_e, '%Y-%m-%d') ='".date('Y-m-d')."'
				 AND ". TABLE_PROSPECTS_TICKETS .".ticket_creator_uid= '". $my['uid']."' ORDER BY ".TABLE_PROSPECTS_TICKETS.".do_e";
			$db->query($sql1);
			if ( $db->nf()>0 ) { 
				while ($db->next_record()){ 
					$activity_text1 = $db->f('ticket_text') ;
					$do_activity1 = $db->f('do_activity') ;
					$medium_comm1 = $db->f('medium_comm') ;
					if($medium_comm1==ProspectsTicket::MEDIUM_VISIT){
						$medium_comm11='Visit';
					}else{
						$medium_comm11='Phone';
					}
					$order_title1 = $db->f('order_title') ;
					$or_no1 = $db->f('or_no') ;
					$activityList[] = array(
					 'activity_text'=>$activity_text1,
					 'do_activity1'=>$do_activity1,
					 'medium_comm1'=>$medium_comm11,
					 'order_title1'=>$order_title1,
					 'or_no1'=>$or_no1					
					);
				}
			}
			
			//GET LIST OF TODAY'S ACTIVITY EOF
			 
            $variables['can_view_client_details']  = true;            
            $variables['client_sendmail'] = Prospects::SENDMAILNO;  
            $hidden[] = array('name'=> 'perform','value' => $perform);
            //$hidden[] = array('name'=> 'invoice_id','value' => $invoice_id);
            //$hidden[] = array('name'=> 'flw_ord_id','value' => $flw_ord_id);
            //$hidden[] = array('name'=> 'client_inv','value' => $client_inv);
            $hidden[] = array('name'=> 'act', 'value' => 'save');
            
           
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => 'activityList', 'value' => 'activityList');
            $page["var"][] = array('variable' => 'prospectOrdArr', 'value' => 'prospectOrdArr');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            $page["var"][] = array('variable' => 'step1', 'value' => 'step1');
            $page["var"][] = array('variable' => 'step2', 'value' => 'step2');
            
        
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-ticket-activity-std-alone.html');
        
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>