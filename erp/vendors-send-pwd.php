<?php

 if ( $perm->has('nc_uv_send_pwd') ) {
        include_once (DIR_FS_INCLUDES .'/support-functions.inc.php');
        
        $client_id         = isset($_GET["client_id"])? $_GET["client_id"]: ( isset($_POST["client_id"])? $_POST["client_id"]: '' );

        $_ALL_POST	    = NULL;
        $condition_query= NULL;
        $access_level   = $my['access_level'];
       

        $condition_query = " WHERE (". TABLE_VENDORS .".user_id = '". $client_id ."')";

        
			
		$fields = TABLE_VENDORS .'.user_id '
    				.','. TABLE_VENDORS .'.number'
					.','. TABLE_VENDORS .'.f_name '
					.','. TABLE_VENDORS .'.l_name'
					.','. TABLE_VENDORS .'.email'
					.','. TABLE_VENDORS .'.username'
					.','. TABLE_VENDORS .'.status AS c_status';
					
    if ( Vendors::getList($db, $list, $fields, $condition_query) > 0 ) {
            $list = $list['0'];
                  
            
            if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
                $_ALL_POST 	= $_POST;
                $data		= processUserData($_ALL_POST);     
               
                if(empty($data['to'])){
                     
                     //$messages->setErrorMessage("Please enter email ids in To."); 
                     
                    $data['f_name']=$list['f_name'];
                    $data['l_name']=$list['l_name'];
                    $data['number']=$list['number'];
                    $data['username']=$list['username'];
                    $data['user_id']=$list['user_id'];
                    
                    $new_pass = substr(md5($data["f_name"]), 2, 6) . rand(5, 125) ;
                    $data['newpassword'] =  $new_pass;
                    $data['password'] = md5($new_pass);
                    $data['link']       = DIR_WS_MP ; 
                    
                    $query  = " UPDATE ". TABLE_VENDORS
                        ." SET password='". $data['password']."'"
                        ." WHERE ". TABLE_VENDORS .".user_id   = '". $data['user_id'] ."'";
                 
                            
                    if ( $db->query($query) ) {
                        
                        $email = NULL;
                        if ( getParsedEmail($db, $s, 'VENDOR_FORGOT_PASSWORD', $data, $email) ) {
                      
                            $to     = '';                   
                            $to[]   = array('name' => $name, 'email' => $data['email']);
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                           $messages->setOkMessage("The email containing the new password is emailed to you.");
                         
                        }
                        //to flush the data.
                        $_ALL_POST  = NULL;
                        $data       = NULL;
                    }
                    else {
                        $messages->setErrorMessage('Cannot change password.');
                    }
                     
                }else{
                    
                    
                    if(isValidEmail($data['to'])){   
                        
                        
                            $data['f_name']=$list['f_name'];
                            $data['l_name']=$list['l_name'];
                            $data['number']=$list['number'];
                            $data['username']=$list['username'];
                            $data['user_id']=$list['user_id'];
                            $data['email']=$data['to'];
                            
                            $new_pass = substr(md5($data["f_name"]), 2, 6) . rand(5, 125) ;
                            $data['newpassword'] =  $new_pass;
                            $data['password'] = md5($new_pass);
                            $data['link']       = DIR_WS_MP ; 
                            
                            $query  = " UPDATE ". TABLE_VENDORS
                                ." SET password='". $data['password']."'"
                                ." WHERE ". TABLE_VENDORS .".user_id   = '". $data['user_id'] ."'";
                         
                                    
                            if ( $db->query($query) ) {
                                
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'VENDOR_FORGOT_PASSWORD', $data, $email) ) {
                              
                                    $to     = '';                   
                                    $to[]   = array('name' => $name, 'email' => $data['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"]);
                                   $messages->setOkMessage("The email containing the new password has been sent on email".$data['email']." .");
                                 
                                }
                                //to flush the data.
                                $_ALL_POST  = NULL;
                                $data       = NULL;
                            }
                            else {
                                $messages->setErrorMessage('Cannot change password.');
                            }
                    }else{
                      $messages->setErrorMessage('Enter Valid email Id.');
                    }
                }
                
            }
            
            $hidden[] = array('name'=> 'client_id' ,'value' => $client_id);           
            $hidden[] = array('name'=> 'perform' ,'value' => 'send_pwd');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');

            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
            
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'vendors-send-pwd.html');
            
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
    
    
}else{
 
    $messages->setErrorMessage("You donot have the Right to Send Login Details.");
}
?>