<?php
    if ( $perm->has('nc_ps_qtt_edit') ) {
        $id= isset($_GET["id"])? $_GET["id"] : ( isset($_POST["id"])? $_POST["id"]: '' );
        $type	= isset($_GET["type"]) ? $_GET["type"] 	: ( isset($_POST["type"]) ? $_POST["type"] : '' );
        $access_level = $my['access_level'];
       
        $extra = array( 'db' 		    => &$db,
                        'access_level'  => $access_level,
                        'messages' 	    => $messages
                    );
        
        ProspectsQuotationTemplate::updateType($id, $type, $extra);
            
        // Display the  list.
        include ( DIR_FS_NC .'/prospects-quotation-template-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Change Status.");
    }
?> 
