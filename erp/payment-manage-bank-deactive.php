<?php
    if ($perm->has('nc_p_mb_edit')) {
	    $id = isset($_GET["id"]) ? $_GET["id"] : ( isset($_POST["id"]) ? $_POST["id"] : '' );
        
        $access_level = $my['access_level'];
                
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        Paymentbank::deactive($id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/payment-manage-bank-list.php');
    }
    else {
         $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>
