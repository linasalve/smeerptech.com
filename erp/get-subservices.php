<?php
// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php");
    include_once ( DIR_FS_INCLUDES .'/services.inc.php'); 
	
    $service_id = isset($_GET['service_id']) ? $_GET['service_id'] : '';
    $subservice_str = isset($_GET['subservice_str']) ? $_GET['subservice_str'] : '';
    $currency_id = isset($_GET['currency_id']) ? $_GET['currency_id'] : '';
    $perform = isset($_GET['perform']) ? $_GET['perform'] : '';
    
  
	
	if($perform=='load-particulars'){
	
		$subservice_str = trim($subservice_str,",");
		$subservice_str = str_replace(",","','",$subservice_str);
		     
        //To count total records.
		$ss_title = "";
		$ss_price = 0; 
        $fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".TABLE_SETTINGS_SERVICES.".ss_particulars,".TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price"; 
		$sql = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES." LEFT JOIN ".TABLE_SETTINGS_SERVICES_PRICE." ON 
		".TABLE_SETTINGS_SERVICES.".ss_id = ".TABLE_SETTINGS_SERVICES_PRICE.".service_id 
		WHERE ".TABLE_SETTINGS_SERVICES.".ss_id IN( '".$subservice_str."' ) AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = ".$currency_id." 
		AND ".TABLE_SETTINGS_SERVICES.".ss_status = '".Services::ACTIVE."'" ;
		 
        if ( $db->query($sql) ) {
			if ( $db->nf() > 0 ) {
				while ($db->next_record()) {
					//$ss_title1  = $db->f("ss_title");
					$ss_title1  = $db->f("ss_particulars");
					$ss_title .= trim($ss_title1).",";
					$ss_price =  $ss_price + $db->f("ss_price") ;
				}
			}
        }           
		$infoStr = $ss_title."#".$ss_price ;
		
	
	}elseif($perform=='load-sub-service'){
	
	 	$subServiceList = '' ;
		$serviceInfo=" #0";
		$infoStr =  " | "." ~ ".$serviceInfo;
	    if (isset($service_id) && !empty($service_id)){
       
			$sql = " SELECT ss_title, ss_punch_line,is_renewable,ss_division FROM ".TABLE_SETTINGS_SERVICES." WHERE 
				".TABLE_SETTINGS_SERVICES.".ss_id = ".$service_id."";
				
			$db->query( $sql );
			if( $db->nf() > 0 ){
				while($db->next_record()){
					$serviceInfo =  trim($db->f("ss_punch_line"))."#".$db->f("is_renewable")."#".$db->f("ss_division")."#".trim($db->f("ss_title"));
				}
			}			
			 
			$condition_query = " WHERE ".TABLE_SETTINGS_SERVICES.".ss_parent_id = ".$service_id." AND 
			".TABLE_SETTINGS_SERVICES_PRICE.".service_id = ".TABLE_SETTINGS_SERVICES.".ss_id ";
			$condition_query .= " AND ".TABLE_SETTINGS_SERVICES_PRICE.".currency_id = ".$currency_id." 
			AND ".TABLE_SETTINGS_SERVICES.".ss_status='".Services::ACTIVE."'" ;
			
			$fields = TABLE_SETTINGS_SERVICES.".ss_id,".TABLE_SETTINGS_SERVICES.".ss_title,".TABLE_SETTINGS_SERVICES_PRICE.".service_price as ss_price" ;        
			$query = "SELECT ".$fields." FROM ".TABLE_SETTINGS_SERVICES.",".TABLE_SETTINGS_SERVICES_PRICE." ".$condition_query ; 
			$query	.= " ORDER BY ".TABLE_SETTINGS_SERVICES.".sequence_subservices ASC";
			
			$db->query( $query );
			$total	= $db->nf();	
			if( $db->nf() > 0 ){
				while($db->next_record()){
					$subServiceList .=  trim($db->f("ss_title"))." - ".$db->f("ss_price") ."|".$db->f("ss_id")."^";
				}
				$subServiceList=  rtrim($subServiceList,'^');
				$infoStr= $subServiceList."~".$serviceInfo ;
			}else{		
				$infoStr =  " | "." ~ ".$serviceInfo;
			}
		}
	}
	echo $infoStr ;
	//include_once( DIR_FS_NC ."/flush.php");
?>
