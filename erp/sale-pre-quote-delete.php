<?php
    if ( $perm->has('nc_sl_pq_delete') ) {
	   $pq_id = isset($_GET["pq_id"]) ? $_GET["pq_id"] : ( isset($_POST["pq_id"]) ? $_POST["pq_id"] : '' );
        
        $access_level = $my['access_level'];
        if ( $perm->has('nc_sl_pq_delete_al') ) {
            $access_level += 1;
        }
    
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        PreQuote::delete($pq_id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/sale-pre-quote-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the Pre Quote.");
    }
?>