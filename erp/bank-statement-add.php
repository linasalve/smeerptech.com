<?php
    if ( $perm->has('nc_bk_stmt_add') ) { 
        
        $_ALL_POST      = NULL;
        $data           = NULL; 
		$mnList = array(
				1=>'Jan',2=>'Feb',3=>'Mar',	4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'			
			);
		$yrList = array();
		$y = date('Y');
		$currentY = date('Y');
		for($year=$y; $year<=$currentY;$year++){
			$yrList[] = $year;
		}
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
            $_ALL_POST 	= $_POST;
            $data		= processUserData($_ALL_POST); 
			           
            $extra = array( 'db' 				=> &$db,
                            'access_level'      => $access_level,
							'messages'          => &$messages
                        ); 
            if(BankStatement::validateAdd($data, $extra) ) { 
				$company_id = 0;
				$banknameArr=array();
				$table = TABLE_PAYMENT_BANK;
				$condition2 = " WHERE ".TABLE_PAYMENT_BANK .".id = '".$data['bank_id']."' " ;
				$fields1 =  TABLE_PAYMENT_BANK .'.company_id,'.TABLE_PAYMENT_BANK .'.bank_name' ;
				$banknameArr = getRecord($table,$fields1,$condition2);
				if(!empty($banknameArr)){
					$company_id =  $banknameArr['company_id'] ;    
				}
				 
				if ( isset($data['stmt_date']) && !empty($data['stmt_date']) ) {
					$data['stmt_date'] = explode('/', $data['stmt_date']);
					$data['stmt_date'] = mktime(0, 0, 0, $data['stmt_date'][1], $data['stmt_date'][0], $data['stmt_date'][2]);
				}
				if ( isset($data['value_date']) && !empty($data['value_date']) ) {
					$data['value_date'] = explode('/', $data['value_date']);
					$data['value_date'] = mktime(0, 0, 0, $data['value_date'][1], $data['value_date'][0], $data['value_date'][2]);
				}
				
				$month_name = $mnList[$data['month']];
				$do_stmt = $data['year']."-".$data['month']."-01";
				$query	= " INSERT INTO ".TABLE_BANK_STATEMENT
					." SET  ".TABLE_BANK_STATEMENT .".bank_id = '".$data['bank_id'] ."'"
					.",". TABLE_BANK_STATEMENT .".company_id = '". 	$company_id ."'"
					.",". TABLE_BANK_STATEMENT .".stmt_date = '".date('Y-m-d H:i:s', $data['stmt_date']) ."'"
					.",". TABLE_BANK_STATEMENT .".description = '".$data['description'] ."'"
					.",". TABLE_BANK_STATEMENT .".remarks = '".$data['remarks'] ."'"
					.",". TABLE_BANK_STATEMENT .".month = '".$data['month'] ."'"
					.",". TABLE_BANK_STATEMENT .".year = '".$data['year'] ."'"
					.",". TABLE_BANK_STATEMENT .".month_name = '".$month_name ."'"
					.",". TABLE_BANK_STATEMENT .".cheque_no = '".$data['cheque_no'] ."'"
					.",". TABLE_BANK_STATEMENT .".debit = '".$data['debit'] ."'"
					.",". TABLE_BANK_STATEMENT .".credit = '".$data['credit'] ."'"
					.",". TABLE_BANK_STATEMENT .".balance = '".$data['balance'] ."'"
					.",". TABLE_BANK_STATEMENT .".value_date = '".date('Y-m-d H:i:s', $data['value_date']) ."'"
					.",". TABLE_BANK_STATEMENT .".do_stmt = '".$do_stmt ."'"
					.",". TABLE_BANK_STATEMENT .".status = '".BankStatement::ACTIVE ."'"
					.",". TABLE_BANK_STATEMENT .".added_by = '". $my['user_id'] ."'"       
					.",". TABLE_BANK_STATEMENT .".added_by_name = '". $my['f_name']." ".$my['l_name'] ."'"       
					.",". TABLE_BANK_STATEMENT .".do_e = '". date('Y-m-d h:i:s')."'"; 
				
				if( $db->query($query) && $db->affected_rows() > 0 ){ 
					$variables['hid'] = $db->last_inserted_id();     
					$messages->setOkMessage("Record added successfully.");					
				}  
                //to flush the data.
                $_ALL_POST	= NULL;
                $data		= NULL;
            }
        }
		
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
       /*  if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
            header("Location:".DIR_WS_NC."/bank-statement.php?perform=add&added=1");
        } */
        if(isset($_POST['btnCancel'])){
            header("Location:".DIR_WS_NC."/bank-statement.php?perform=list");
        }
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if(isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ){
            header("Location:".DIR_WS_NC."/bank-statement.php?added=1");   
        }
        else {
			$banklst	= NULL;
			$fieldsi = TABLE_PAYMENT_BANK.'.*' ;
			$condition_queryib = " WHERE ".TABLE_PAYMENT_BANK.".type = '1' AND ".TABLE_PAYMENT_BANK.".status='1' ORDER BY bank_name";
			Paymentbank::getDetails($db, $banklst, $fieldsi, $condition_queryib);
			
			
		
            $hidden[] = array('name'=> 'perform' ,'value' => 'add');
            $hidden[] = array('name'=> 'act' , 'value' => 'save');     
            $page["var"][] = array('variable' => 'mnList', 'value' => 'mnList');
            $page["var"][] = array('variable' => 'yrList', 'value' => 'yrList');
            $page["var"][] = array('variable' => 'banklst', 'value' => 'banklst');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');     
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bank-statement-add.html');
        }
    }
    else {
        //$messages->setErrorMessage("You donot have the Permisson to Access this module.");
       $messages->setErrorMessage("You donot have the Permisson to Access this module.");
        
    }
?>
