<?php

    if ( $perm->has('nc_bl_rcpt_add') ) {
    
        include_once ( DIR_FS_INCLUDES .'/currency.inc.php');
        include_once ( DIR_FS_INCLUDES .'/followup.inc.php');
        include_once ( DIR_FS_INCLUDES .'/payment-mode.inc.php');
        include_once ( DIR_FS_INCLUDES .'/payment-bank.inc.php');
		include_once ( DIR_FS_INCLUDES .'/payment-transaction.inc.php');
        include_once ( DIR_FS_INCLUDES .'/financial-year.inc.php' );
        
        $inv_id 		= isset($_GET["inv_id"]) ? $_GET["inv_id"] : ( isset($_POST["inv_id"]) ? $_POST["inv_id"] : '' );
        
        $_ALL_POST      = NULL;
        $data           = NULL;
        
        $lst_currency   = Currency::getCurrency();
        $access_level   = $my['access_level'];
        
        if ( $perm->has('nc_bl_rcpt_add_al') ) {
            $access_level += 1;
        }  
		$extra = array( 'db' 				=> &$db,
                     	'access_level'      => $access_level,
                     	'messages'          => &$messages
					);
        
        $currency = NULL ;
        $required_fields = '*' ;
		Currency::getList($db,$currency,$required_fields);
         //getfinancial yr bof
        $lst_fyr = null;
        $lst_fyr = FinancialYear::yearRange();
        //getfinancial yr eof  
		//echo $time_start = microtime(true);
        //cho "<br/>";
		// Read the Invoice details.
        $invoice = NULL;
		if ( Receipt::getInvoice($inv_id, $invoice, $extra) ) {
			// Retrieve the Receipts for the invoice.
			$receipt_list = array();
			Receipt::getInvoiceReceipts($invoice['number'], $receipt_list, $extra);            
			// Get the balance.
          
            $balance['amount'] = $invoice['balance'];
            $invoice['grinvamount']          = number_format($invoice['amount'],2);
			$receipt_list_history =  $receipt_list;
			if ( isset($invoice['balance']) && $invoice['balance'] <= 0 ) {
				$messages->setErrorMessage("The Invoice has been paid fully.");
			}          
            $_ALL_POST['particulars'] = $invoice['particular_details'];
            $data['company_id'] = $invoice['company_id'];
            
		}       
      
        // Read the available payment mode 
        
        $payment_mode	= NULL;
        $fieldspm= TABLE_PAYMENT_MODE.'.*' ;
        $condition_querypm= " WHERE ".TABLE_PAYMENT_MODE.".status='1'";
        Paymentmode::getDetails($db,$payment_mode, $fieldspm, $condition_querypm);
       
        
        $inhouseBanklst	= NULL;
        $condition_queryib = " WHERE ".TABLE_PAYMENT_BANK.".type = '1' AND ".TABLE_PAYMENT_BANK.".status='1'";
        $fieldsi = TABLE_PAYMENT_BANK.'.*' ;
        Paymentbank::getDetails($db, $inhouseBanklst, $fieldsi, $condition_queryib);
        
        $globalBanklst	= NULL;
        $fieldsg = TABLE_PAYMENT_BANK.'.*' ;
        $condition_querygb = " WHERE ".TABLE_PAYMENT_BANK.".type = '0' AND ".TABLE_PAYMENT_BANK.".status='1'";
        Paymentbank::getDetails($db, $globalBanklst, $fieldsg, $condition_querygb);
        
		//******** check list BOF*************
		if ( !empty($inv_id) && $messages->getErrorMessageCount() <= 0 ) {
         
			//$payment_mode = Receipt::getPaymentMode();
            
			if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
				$_ALL_POST 	= $_POST;
				$data		= processUserData($_ALL_POST);
                
                $_ALL_POST['particulars'] = $invoice['particular_details'];
                
				// Set the Creator.
				if ( !isset($data['created_by']) || empty($data['created_by']) ) {
					$data['created_by'] = $my['user_id'];
				}
				// Set the Client.
				$data['client'] = $invoice['c_user_id'];
	            $data['balance'] = $invoice['balance'];
	            $data['balance_inr'] = $invoice['balance_inr'];
                
				$data['do_c'] = time();
                
                $extra = array( 'db' 		=> &$db,
                     	'access_level'      => $access_level,
                     	'payment_mode'      => $payment_mode,
						'messages'          => &$messages
					);

                //echo " 1 ".$sttime= microtime(true)."</br>";
                if ( Receipt::validateAdd($data, $extra) ) {
                    
                    //echo "<br/> 2 ".$edtime = microtime(true);
                    //get currency abbr from d/b bof
                        $currency1 =null;
                        $condition_query1 = " WHERE ".TABLE_SETTINGS_CURRENCY.".id= ".$data['currency_id'];
                        $fields1 = "*";
                        Currency::getList( $db, $currency1, $fields1, $condition_query1);              
                        if(!empty($currency1)){
                           $data['currency_abbr'] = $currency1[0]['abbr'];
                           $data['currency_name'] = $currency1[0]['currency_name'];
                           $data['currency_symbol'] = $currency1[0]['currency_symbol'];
                           $data['currency_country'] = $currency1[0]['country_name'];
                        }
                    //get currency abbr from d/b eof  
                     //get currency abbr from d/b eof                      
                        $company1 =null;
                        $condition_query_c1 = " WHERE ".TABLE_SETTINGS_COMPANY.".id= ".$data['company_id'];
                        $fields_c1 = " prefix,name ";
                        Company::getList( $db, $company1, $fields_c1, $condition_query_c1);              
                        if(!empty($company1)){                  
                           $data['company_name'] = $company1[0]['name'];
                           $data['company_prefix'] = $company1[0]['prefix'];
                        }
                    //Payment details bof
                        $pm_Arr	= NULL;
                        $fpm = TABLE_PAYMENT_MODE.'.payment_mode' ;
                        $conditionpm= " WHERE ".TABLE_PAYMENT_MODE.".id=".$data['p_mode'];
                        Paymentmode::getDetails($db,$pm_Arr, $fpm, $conditionpm);
                        if(!empty($pm_Arr)){
                            $pm_Arr = $pm_Arr[0];
                            $data['payment_mode_name'] = $pm_Arr['payment_mode'];
                        }
                        
                        $data['pay_bank_company_name'] = '';
                        
                        if($data['p_mode'] != '2' ){
                            //ie p_mode is not cash                            
                            if($data['do_pay_received'] =='0000-00-00'){
                                $data['do_pay_received'] ='';
                            }
                            if(!empty($data['pay_bank_company'])){
                            
                                $bankarr	= NULL;
                                $conditionfb = " WHERE ".TABLE_PAYMENT_BANK.".id = ".$data['pay_bank_company'];
                                $fpb = TABLE_PAYMENT_BANK.'.bank_name' ;
                                Paymentbank::getDetails($db, $bankarr, $fpb, $conditionfb);
                                if(!empty($bankarr)){
                                    $bankarr = $bankarr[0];
                                    $data['pay_bank_company_name'] = $bankarr['bank_name'];
                                }                                
                            }
                        }
                    //Payment details eof      
                    
                    //Get billing address BOF
                    include_once ( DIR_FS_CLASS .'/Region.class.php');
                    $region         = new Region();
                    $region->setAddressOf(TABLE_CLIENTS,  $data['client']['user_id']);
                    $addId = $data['billing_address'] ;
                    $address_list  = $region->get($addId);
                    $data['b_addr_company_name'] = $address_list['company_name'];
                    $data['b_addr'] = $address_list['address'];
                    $data['b_addr_city'] = $address_list['city_title'];
                    $data['b_addr_state'] = $address_list['state_title'];
                    $data['b_addr_country'] = $address_list['c_title'];
                    $data['b_addr_zip'] = $address_list['zipcode'];
                    //$data['b_address'] = $address_list ;
                    //Get billing address EOF

                    $data['invoice_opening_balance'] = $data['balance'];
                    $data['invoice_closing_balance'] = $data['balance'] - $data['amount'];
                    
                    $data['invoice_opening_balance_inr'] = $data['balance_inr'];
                    $data['invoice_closing_balance_inr'] = $data['balance_inr'] - $data['amount_inr'];
                    
                   
					$query	= " INSERT INTO ".TABLE_BILL_RCPT
			." SET ". TABLE_BILL_RCPT .".number = '".       $data['number'] ."'"
				.",". TABLE_BILL_RCPT .".rcpt_counter = '". $data['rcpt_counter'] ."'"
				.",". TABLE_BILL_RCPT .".inv_no = '".       $data['inv_no'] ."'"
				.",". TABLE_BILL_RCPT .".inv_id = '".       $data['inv_id'] ."'"
				.",". TABLE_BILL_RCPT .".inv_profm_id = '".       $data['inv_profm_id'] ."'"
				.",". TABLE_BILL_RCPT .".inv_profm_no = '".       $data['inv_profm_no'] ."'"
				.",". TABLE_BILL_RCPT .".ord_no = '".       $data['or_no'] ."'"
				.",". TABLE_BILL_RCPT .".voucher_no = '".       $data['voucher_no'] ."'"
				.",". TABLE_BILL_RCPT .".voucher_total_amount = '".       $data['voucher_total_amount'] ."'"
				.",". TABLE_BILL_RCPT .".voucher_id = '".       $data['voucher'] ."'"
				.",". TABLE_BILL_RCPT .".access_level = '". $data['access_level'] ."'"
				.",". TABLE_BILL_RCPT .".created_by = '".   $data['created_by'] ."'"
				.",". TABLE_BILL_RCPT .".client = '".       $data['client']['user_id'] ."'"
				.",". TABLE_BILL_RCPT .".currency_abbr = '". processUserData($data['currency_abbr']) ."'"
				.",". TABLE_BILL_RCPT .".currency_id = '".   $data['currency_id'] ."'"
				.",". TABLE_BILL_RCPT .".currency_name = '".  processUserData( $data['currency_name']) ."'"
				.",". TABLE_BILL_RCPT .".currency_symbol = '". processUserData($data['currency_symbol']) ."'"
				.",". TABLE_BILL_RCPT .".currency_country = '". processUserData($data['currency_country'])."'"
				.",". TABLE_BILL_RCPT .".exchange_rate  = '".$data['exchange_rate'] ."'"
				.",". TABLE_BILL_RCPT .".amount         = '". 		$data['amount'] ."'"
				.",". TABLE_BILL_RCPT .".amount_inr     = '".   $data['amount_inr'] ."'"
				.",". TABLE_BILL_RCPT .".amount_words   = '". $data['amount_words'] ."'"
				.",". TABLE_BILL_RCPT .".p_mode         = '".		$data['p_mode'] ."'"
				.",". TABLE_BILL_RCPT .".payment_mode_name = '".$data['payment_mode_name'] ."'"
				.",". TABLE_BILL_RCPT .".p_details = '".	$data['p_details'] ."'"
				.",". TABLE_BILL_RCPT .".pay_cheque_no = '".   $data['pay_cheque_no'] ."'"
				.",". TABLE_BILL_RCPT .".do_pay_received = '". $data['do_pay_received'] ."'"                 
				.",". TABLE_BILL_RCPT .".pay_bank_company = '". $data['pay_bank_company'] ."'"
				.",". TABLE_BILL_RCPT .".pay_bank_company_name = '".
				processUserData($data['pay_bank_company_name']) ."'"
				.",". TABLE_BILL_RCPT .".pay_branch = '".      $data['pay_branch'] ."'"                           
				.",". TABLE_BILL_RCPT .".do_c = '".         date('Y-m-d H:i:s', $data['do_c']) ."'"
				.",". TABLE_BILL_RCPT .".do_r = '".         date('Y-m-d H:i:s', $data['do_r']) ."'"
				.",". TABLE_BILL_RCPT .".do_voucher = '". date('Y-m-d H:i:s', $data['do_voucher']) ."'"
				.",". TABLE_BILL_RCPT .".do_transaction = '".date('Y-m-d H:i:s', $data['do_transaction']) ."'"
				.",". TABLE_BILL_RCPT .".remarks = '".      $data['remarks'] ."'"
				.",". TABLE_BILL_RCPT .".balance = '".      $data['balance'] ."'"
				.",". TABLE_BILL_RCPT .".invoice_opening_balance = '".$data['invoice_opening_balance'] ."'"
				.",". TABLE_BILL_RCPT .".invoice_closing_balance = '".$data['invoice_closing_balance'] ."'"
				.",". TABLE_BILL_RCPT .".invoice_opening_balance_inr = '".$data['invoice_opening_balance_inr'] ."'"
				.",". TABLE_BILL_RCPT .".invoice_closing_balance_inr = '".$data['invoice_closing_balance_inr'] ."'"
				.",". TABLE_BILL_RCPT .".billing_name = '". processUserData($data['billing_name'])."'"
				.",". TABLE_BILL_RCPT .".billing_address    ='".processUserData($data['billing_address']) ."'"
				.",". TABLE_BILL_RCPT .".b_addr_company_name = '".processUserData($data['b_addr_company_name']) ."'"
				.",". TABLE_BILL_RCPT .".b_addr              = '".processUserData($data['b_addr'])."'"
				.",". TABLE_BILL_RCPT .".b_addr_city         = '".processUserData($data['b_addr_city']) ."'"
				.",". TABLE_BILL_RCPT .".b_addr_state        = '". processUserData($data['b_addr_state']) ."'"
				.",". TABLE_BILL_RCPT .".b_addr_country      = '".processUserData($data['b_addr_country']) ."'"
				.",". TABLE_BILL_RCPT .".b_addr_zip          = '".processUserData($data['b_addr_zip'] )."'"
				.",". TABLE_BILL_RCPT .".company_id          ='".  $data['company_id'] ."'"
				.",". TABLE_BILL_RCPT .".company_name          ='". processUserData($data['company_name']) ."'"
				.",". TABLE_BILL_RCPT .".company_prefix          ='". processUserData( $data['company_prefix'] )."'"
				.",". TABLE_BILL_RCPT .".status              = '". $data['status'] ."'" ;

					if ( $db->query($query) && $db->affected_rows() > 0 ) {
                         //echo "<br/> 3 ".$edtime = microtime(true);
						$messages->setOkMessage("New Receipt has been created.");
						$variables['hid'] = $db->last_inserted_id();
                        $data['do_r_chk'] = $data['do_r'];			 
						$data['do_rs_symbol'] = strtotime('2010-07-20');
						
						//After insert, update invoice counter in 
                        // updateOldCounterOf($db,CONTR_RPT,$data['company_id'],$data['financialYr']);
                        
                        //To disply in html and pdf                         
                        $data['amount_words']=processSqlData($data['amount_words']);
                        $data['pay_branch']=processSqlData($data['pay_branch']);
                        $data['p_details']=processSqlData($data['p_details']);
                        $data['pay_cheque_no']=processSqlData($data['pay_cheque_no']);
                        // Calculate forex gain  and loss BOF
                        
                        $invExRate = $invoice['exchange_rate']; 
                        $rcpExRate = $data['exchange_rate']; 
                        $forexGainVal =  $forexLossVal = 0;
                        $invExAmt = ($invExRate * $data['amount']) ;
                        $rcpExAmt = ($rcpExRate * $data['amount']) ;
                        $forexVal = ( $rcpExAmt - $invExAmt );
                        
                        if($forexVal > 0){
                           $forexGainVal =$forexVal;
                           $query = " UPDATE ". TABLE_BILL_RCPT
									." SET forex_gain = '".	$forexGainVal."'"
                                    ." WHERE id =".$variables['hid'];
                            $db->query($query);
                                    
                        }else{
                            $forexLossVal =$forexVal;
                            $query = " UPDATE ". TABLE_BILL_RCPT
									." SET forex_loss = '".	$forexLossVal."'"
                                    ." WHERE id =".$variables['hid'];
                            $db->query($query);
                        }                        
                        // Calculate forex gain  and loss EOF
                     
                    
                        if ( !empty($invoice['number']) ) {
                       
                            // Retrieve the Receipts for the invoice.
                            $receipt_list = array();
                            Receipt::getInvoiceReceipts($invoice['number'], $receipt_list, $extra);
                            // Get the balance.
                            $rptList=array();
                            if(!empty($receipt_list)){
                                foreach($receipt_list as $keyRpt=>$valRpt){
                                    $valRpt2['id'] = $valRpt['id'];
                                    $valRpt2['number'] = $valRpt['number'];
                                    $valRpt2['amount'] = number_format($valRpt['amount'],2);
                                    $valRpt2['amount_inr'] = number_format($valRpt['amount_inr'],2);
                                    $valRpt2['do_r'] = $valRpt['do_r'];
                                    $rptList[$keyRpt] = $valRpt2;
                                }
                            }
                           
                           $statusSql='';
                           $balance['balance']  = $data['invoice_closing_balance'] ;
                           $balance['balance_inr'] =    $data['invoice_closing_balance_inr'];
                            if($balance['balance']<=0){
                                $statusSql = ", status = '".	Invoice::COMPLETED ."'";
                            }     
                           
                           $query = " UPDATE ". TABLE_BILL_INV
									." SET balance = '".	$balance['balance'] ."',"
                                    ." balance_inr = '".	$balance['balance_inr'] ."'"
                                    .$statusSql
                                    ." WHERE id =".$inv_id;
                            $db->query($query);
                        }
						
						if ( !empty($data['voucher_no']) ) {
							$sql_vh=" UPDATE ".TABLE_PAYMENT_TRANSACTION 
								." SET pay_received_amt_rcpt = (pay_received_amt_rcpt - '".	$data['amount'] ."'),"
								." pay_received_amt_rcpt_inr =( pay_received_amt_rcpt_inr - '".	$data['amount_inr'] ."'),"                                    ." receipt_no = CONCAT( receipt_no, ',".$data['number']."' ),"   
								." invoice_no = CONCAT( invoice_no, ',".$invoice['number']."' )"    							
								." WHERE id =".$data['voucher'];
							$db->query($sql_vh);

							$sql="SELECT pay_received_amt_rcpt FROM ".TABLE_PAYMENT_TRANSACTION." WHERE id =".$data['voucher'];
							$db->query( $sql );
							$pay_received_amt_rcpt=0;
							if( $db->nf() > 0 ){
								while($db->next_record()){
									$pay_received_amt_rcpt = $db->f('pay_received_amt_rcpt') ;
								}
							}
							
							if($pay_received_amt_rcpt <=0 ){
								$sql_vh1=" UPDATE ".TABLE_PAYMENT_TRANSACTION." SET linking_status
    								='".Paymenttransaction::COMPLETEDLINK."'  WHERE id =".$data['voucher'];
								$db->query( $sql_vh1 );	
							}elseif($pay_received_amt_rcpt>0){
								$sql_vh1=" UPDATE ".TABLE_PAYMENT_TRANSACTION." SET linking_status 
								='".Paymenttransaction::PARTIALLINK."' WHERE id =".$data['voucher'];
								$db->query( $sql_vh1 );	
							}
						}
                        //echo "<br/> 7 ".$edtime = microtime(true);
                        
                        //2008-12-dec-17 EOF
                        $data['particulars'] = $invoice['particular_details'];                        
                        $data['inv_amount'] = number_format($invoice['amount'],2);
                        $data['voucher_total_amount'] = number_format($data['voucher_total_amount'],2);
                        $data['do_i']=$invoice['do_i'];
                        // code to show history of all receipts againce invoice BOF
                        $data['historyrcp'] = $rptList;
                        
                        //commented now $data['invbalance'] = $balance['amount'];
                        $data['invbalance'] = number_format($balance['balance'],2);
                        // code to show history of all receipts againce invoice EOF  
                        // Format the amount
                        $data['amount'] = number_format($data['amount'],2);
                                     
                        
						// Create the Invoice PDF, HTML in file.
						$extra = array( 's'         => &$s,
										'messages'  => &$messages
									  );
						$attch = '';
                       
						if ( !($attch = Receipt::createFile($data, 'HTML', $extra)) ) {
							$messages->setErrorMessage("The Receipt file was not created.");
						}
                       
						Receipt::createRptPdfFile($data);
                        $file_name = DIR_FS_RPT_PDF_FILES ."/". $data["number"] .".pdf";
						
                        // Send the notification mails to the concerned persons.
                        
						include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
						/*
                        $users = getExecutivesWith($db, array(  array('right'=>'nc_bl_rcpt_add', 'access_level'=>$data['access_level']),
																array('right'=>'nc_bl_rcpt_add_al', 'access_level'=>($data['access_level']-1) )
															)
												);
	                    */
						// Organize the data to be sent in email.
						$data['link']   = DIR_WS_MP .'/bill-receipt.php?perform=view&rcpt_id='. $variables['hid'];
	
						// Read the Client Manager information.
						include ( DIR_FS_INCLUDES .'/clients.inc.php');
						
						$data['manager'] = Clients::getManager($db, '', $data['client']['manager'], 'number, f_name, l_name, email');
					
                        // Send Email to the Client.
                        if( isset( $data['mail_client']) &&  $data['mail_client']==1 ){
                            $email = NULL;
                            $cc_to_sender= $cc = $bcc = Null;
                            if ( getParsedEmail($db, $s, 'RECEIPT_CREATE_TO_CLIENT', $data, $email) ) {
                                $to     = '';
                                /*  echo " subject ".$email["subject"] ;
                                    echo "<br/>";
                                    echo " body ".$email["body"];
                                */
                                $to[]   = array('name' => $data['client']['f_name'] .' '. $data['client']['l_name'], 'email' => $data['client']['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                            }
                        }
                        
                        $data['link']   = DIR_WS_NC .'/bill-receipt.php?perform=view&rcpt_id='. $variables['hid'];
                        $cc_to_sender= $cc = $bcc = Null;
                        if ( getParsedEmail($db, $s, 'RECEIPT_CREATE_TO_ADMIN', $data, $email) ) {
                            $to     = '';
                            /*
                            echo " subject ".$email["subject"] ;
                            echo "<br/>";
                            echo " body ".$email["body"];
                            */
                            $to[]   = array('name' => $bill_inv_name , 'email' => $bill_inv_email);                           
                            $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                          
                            SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
                         }
                        
                        
                        
						 // Send mail to the Concerned Executives with same access level and higher access levels
                        /*
                        if(isset($data['mail_exec_ac'])){
                            foreach ( $users as $user ) {
                                $data['myFname']    = $user['f_name'];
                                $data['myLname']    = $user['l_name'];
                                $email = NULL;
                                if ( getParsedEmail($db, $s, 'BILL_RCPT_NEW_MANAGERS', $data, $email) ) {
                                    $to     = '';
                                    $to[]   = array('name' => $user['f_name'] .' '. $user['l_name'], 'email' => $user['email']);
                                    $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                    SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                                }
                            }
						}*/
						// Send Email to the Client Manager.
						/*
                        $email = NULL;
                        // Send Email to the Client Manager.
                        if(isset($data['mail_client_mgr'])){ 
                            if ( getParsedEmail($db, $s, 'BILL_RCPT_NEW_CLIENT_MANAGER', $data, $email) ) {
                                $to     = '';
                                $to[]   = array('name' => $data['manager']['f_name'] .' '. $data['manager']['l_name'], 'email' => $data['manager']['email']);
                                $from   = $reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);
                                SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"], '', '', '', $attch);
                            }
					    }*/
						//to flush the data.
						$_ALL_POST  = NULL;
                        //As receipt-number formate changed on the basis of ( fanancial yr + sr no ) 
                        //$_ALL_POST['number'] = getCounterNumber($db,CONTR_RPT,$data['company_id']); 
                        $_ALL_POST['exchange_rate'] = 1; 
                        $rcp_no=$data['number'];
						$data       = NULL;
					}
				}
                
			}
			else {
                
				// Set up the default values to be displayed.
				//$_ALL_POST['number'] = "PT". date("Y") ."-RCT-". date("mdhi-s") ;
                //As receipt-number formate changed on the basis of ( fanancial yr + company + sr no ) 
                $_ALL_POST['number'] ='';
                //$_ALL_POST['number'] = getCounterNumber($db,CONTR_RPT,$data['company_id']); 
                $_ALL_POST['exchange_rate'] = 1; 
				
				//$_ALL_POST['billing_name']		= $invoice['billing_name'];
				$_ALL_POST['billing_address']	    = $invoice['billing_address'];
				$_ALL_POST['do_r']				    = date('d/m/Y', time());
				$_ALL_POST['invoice_amount']		= $invoice['amount'];
               
				$_ALL_POST['amount']		        = $balance['amount'];
				$_ALL_POST['amount_inr']	        = $balance['amount'];
				$_ALL_POST['amount_words']	        = '';
            }
	   
            if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
                 header("Location:".DIR_WS_NC."/bill-receipt.php?added=1&hid=".$variables['hid']."&rcp_no=".$rcp_no);
                 
            }
            if(isset($_POST['btnCancel'])){
                 header("Location:".DIR_WS_NC."/bill-receipt.php");
            }
 
            
            
			// Check if the Form to add is to be displayed or the control is to be sent to the List page.
			if ( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
				//include_once ( DIR_FS_NC .'/bill-receipt-list.php');
                header("Location:".DIR_WS_NC."/bill-receipt.php?perform=list&added=1&hid=".$variables['hid']."&rcp_no=".$rcp_no);
			}
			else {
				$_ALL_POST['invoice']	= $invoice;
                
                $_ALL_POST['currency_id']= $invoice['currency_id'];
               
				//$_ALL_POST['particular_details']	= $invoice['particular_details'];                
				// Read the Client's all Addresses BOF
                    
				    include_once ( DIR_FS_CLASS .'/Region.class.php');
				    $region         = new Region();
				    $region->setAddressOf(TABLE_CLIENTS, $invoice['c_user_id']);
				    //$_ALL_POST['address_list']  = $region->get();                   
				    $addId = $invoice['billing_address'] ;
					$_ALL_POST['address_list'][]  = $region->get($addId);
					 
                // Read the Client all Addresses EOF
                
                /* Get only that address which is selected in invoice bof*/
                /*
                include_once ( DIR_FS_CLASS .'/Region.class.php');
				$region         = new Region();
				$region->setAddressOf(TABLE_CLIENTS, $invoice['c_user_id']);
                $addId = $invoice['billing_address'] ;
				$address_list  = $region->get($addId);
                $_ALL_POST['address_list'][0] = $address_list ;
                */
                /* Get only that address which is selected in invoice eof*/
               
				$al_list = getAccessLevel($db, $access_level);
				$index = array();
				if ( isset($_ALL_POST['invoice']['access_level']) ) {
					array_key_search('access_level', $invoice['access_level'], $al_list, $index);
					$_ALL_POST['invoice']['access_level'] = $al_list[$index[0]]['title'];
				}

				$hidden[] = array('name'=> 'inv_id' ,'value' => $invoice['id']);
				$hidden[] = array('name'=> 'inv_no' ,'value' => $invoice['number']); 	
				$hidden[] = array('name'=> 'inv_profm_id' ,'value' => $invoice['inv_profm_id']);
				$hidden[] = array('name'=> 'inv_profm_no' ,'value' => $invoice['inv_profm_no']);
				$hidden[] = array('name'=> 'or_no' ,'value' => $invoice['or_no']);
				$hidden[] = array('name'=> 'company_id' ,'value' => $data['company_id']);
				$hidden[] = array('name'=> 'perform' ,'value' => 'add');
				$hidden[] = array('name'=> 'act' , 'value' => 'save');
                    
				$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
				$page["var"][] = array('variable' => 'payment_mode', 'value' => 'payment_mode');
				$page["var"][] = array('variable' => 'receipt_list', 'value' => 'receipt_list');
				$page["var"][] = array('variable' => 'lst_currency', 'value' => 'lst_currency');
				$page["var"][] = array('variable' => 'inhouseBanklst', 'value' => 'inhouseBanklst');
				$page["var"][] = array('variable' => 'globalBanklst', 'value' => 'globalBanklst');
				$page["var"][] = array('variable' => 'payment_mode', 'value' => 'payment_mode');
				$page["var"][] = array('variable' => 'lst_fyr', 'value' => 'lst_fyr');
				$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
				$page["var"][] = array('variable' => 'al_list', 'value' => 'al_list');
                $page["var"][] = array('variable' => 'currency', 'value' => 'currency');
				
				$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'bill-receipt-add.html');
			}
		}
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>