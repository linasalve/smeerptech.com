<?php

    if ( $perm->has('nc_inward_list') ) {
	
        
        
        if ( !isset($_SEARCH) ) {
            $_SEARCH = '';
        }
		if ( !isset($condition_query) || $condition_query == '' ) {
           
			$condition_query = " WHERE  (". TABLE_IN_WARD .".status = '". Inward::PENDING ."'".")";
			$_SEARCH["chk_status"]  = 'AND';
            $_SEARCH["sStatus"]     = Inward::PENDING ;
        } 
		
		$condition_query .= " ORDER BY ". $order_by_table .".". $sOrderBy ." ". $sOrder;
		$_SEARCH['searched'] = 1;
        // To count total records.
        $list	= 	NULL;
        $total	=	Inward::getDetails( $db, $list, '', $condition_query);
    
        
        $extra_url  = '';
        if ( isset($condition_url) && !empty($condition_url) ) {
            $extra_url  = $condition_url;
        }
        $extra_url  .= "&x=$x&rpp=$rpp";
        $extra_url  = '&start=url'. $extra_url .'&end=url';
        
        $condition_url .="&rpp=".$rpp."&perform=".$perform;
    
    	$pagination = showPaginationAjaxWithExtra($total, $x, $rpp, $condition_url, 'changePageWithExtra');
    
        $list	= NULL;
        $fields = TABLE_IN_WARD .'.*'     ;
                    
        Inward::getDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);
         $fList=array();
        if(!empty($list)){
            $typeArr = Inward::getTypes();
            $typeArr = array_flip($typeArr);
            $ctypeArr = Inward::getContentTypes();
            
            foreach( $list as $key=>$val){
            
                $content_type = $val['content_type'];
                $type = $val['type'];
                $typename =$typeArr[$type];
                if($content_type!=0){
                    $content_type = $ctypeArr[$content_type];
                }else{
                    $content_type='';
                }
                $val['type'] = $typename ;                
                $val['content_type'] = $content_type ;    
                if($val['is_client']){
                     $val['is_client']='Yes';
                }else{
                    $val['is_client']='No';
                }
                $fList[$key]=$val;                 
            }
        }
        
        // Set the Permissions.
        $variables['can_view_list']     = false;
        $variables['can_add']           = false;
        $variables['can_edit']          = false;
        $variables['can_status_edit']          = false;
        $variables['can_delete']        = false;
        $variables['can_view_details']  = false;
       
        if ( $perm->has('nc_inward_list') ) {
            $variables['can_view_list'] = true;
        }
        if ( $perm->has('nc_inward_add') ) {
            $variables['can_add'] = true;
        }
        if ( $perm->has('nc_inward_edit') ) {
            $variables['can_edit'] = true;
        }
        if ( $perm->has('nc_inward_status') ) {
            $variables['can_status_edit'] = true;
        }
        if ( $perm->has('nc_inward_delete') ) {
            $variables['can_delete'] = true;
        }
        if ( $perm->has('nc_inward_details') ) {
            $variables['can_view_details'] = true;
        }
        
        
        $page["var"][] = array('variable' => 'list', 'value' => 'fList');
        $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
        $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
        $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
        // PAGE = CONTENT_MAIN
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'inward-list.html');
    }
    else {
        $messages->setErrorMessage("You do not have the permission to view the list.");
    }
?>
