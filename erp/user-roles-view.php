<?php
	if ( $perm->has('nc_urol_details') ) {
        $role_id = isset ($_GET['role_id']) ? $_GET['role_id'] : ( isset($_POST['role_id'] ) ? $_POST['role_id'] :'');
        
        if ( $role_id !='' ) {
            $data	=	NULL;
            if ( !UserRoles::getView($db, $data, $role_id) ) {
                $messages->setErrorMessage("The selected Role not found.");
            }
        }
        else {
            $messages->setErrorMessage("The selected Role not found.");
        }
        
        $page["var"][] = array('variable' => 'data', 'value' => 'data');
    
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'user-roles-view.html');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
?>