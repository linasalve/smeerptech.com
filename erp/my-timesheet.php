<?php
    
       
    include_once ( DIR_FS_INCLUDES .'/my-profile.inc.php');
    $user_id = $my['user_id'];
    
    $_ALL_POST      = NULL;
    $data           = NULL;
    $access_level   = $my['access_level'];
    $access_level   += 1;
    $date	= isset($_GET["date"]) 	? $_GET["date"]	: ( isset($_POST["date"]) 	? $_POST["date"] : '' );
    $_SEARCH["date"] 	= $date;
    $subcondition_query='';
    if(!empty($date)){
    
        $dateArr= explode("/",$date);
        $checkDate = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
        $subcondition_query = " AND ".TABLE_USER_TIMESHEET .".date = '".$checkDate."'";
        $_SEARCH['searched']=true;
        $extra_url ="&searched=1&date=$date";
    }
    // To count total records.
    $list	= 	NULL;
    
    // If my is the Client Manager    
    $condition_query = " WHERE (". TABLE_USER_TIMESHEET .".user_id = '". $my['user_id'] ."')" ;
    $condition_query = $condition_query.$subcondition_query;
    $total	=	Myprofile::getTimesheetDetails( $db, $list, TABLE_USER_TIMESHEET.'.id', $condition_query);    
    $pagination = showPaginationAjax($total, $x, $rpp, 'changePage');
    $extra_url  = '';
    if ( isset($condition_url) && !empty($condition_url) ) {
        $extra_url  = $condition_url;
    }
    $extra_url  .= "&x=$x&rpp=$rpp";
    $extra_url  = '&start=url'. $extra_url .'&end=url';    
    
    $list	= NULL;
    $fields = TABLE_USER_TIMESHEET .'.id'
                .','. TABLE_USER_TIMESHEET .'.date'
                .','. TABLE_USER_TIMESHEET .'.hrs'
                .','. TABLE_USER_TIMESHEET .'.min'
                .','. TABLE_USER_TIMESHEET .'.note'
                .','. TABLE_BILL_ORDERS .'.order_title AS order_title ';
   
    Myprofile::getTimesheetDetails( $db, $list, $fields, $condition_query, $next_record, $rpp);   
    // Set the Permissions.to access the functionality
    
    $variables['can_view_list']     = true;
    $variables['can_add']           = true;
    $variables['can_edit']          = false;
    $variables['can_delete']        = false;    
   
    $page["var"][] = array('variable' => 'list', 'value' => 'list');
    $page["var"][] = array('variable' => 'pagination', 'value' => 'pagination');
    $page["var"][] = array('variable' => '_SEARCH', 'value' => '_SEARCH');
    $page["var"][] = array('variable' => 'extra_url', 'value' => 'extra_url');
    // PAGE = CONTENT_MAIN
    $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'my-timesheet.html');        
    include_once( DIR_FS_NC ."/flush.php");
    //echo(sprintf('%s - %s = %s', time(), $start['time'], (time()-$start['time'])));
    //echo "<br/>";
    //echo(sprintf('%s - %s = %s', microtime(), $start['microtime'], (microtime()-$start['microtime'])));
    
?>