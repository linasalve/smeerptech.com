<?php
    if ( $perm->has('nc_ld_or_delete') ) {
        
	    $or_id = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        
        $access_level = $my['access_level'];
        /*
		if ( $perm->has('nc_bl_or_delete_al') ) {
            $access_level += 1;
        }*/
    
        $extra = array( 'db'           => &$db,
                        'access_level' => $access_level,
                        'messages'     => &$messages
                        );
        
       
        if ( (LeadsOrder::getList($db, $order, TABLE_LEADS_ORDERS.".id,".TABLE_LEADS_ORDERS.".number,
		".TABLE_LEADS_ORDERS.".access_level, ".TABLE_LEADS_ORDERS.".status", " WHERE ".TABLE_LEADS_ORDERS.".id = '$or_id'")) > 0 ) {
                $order = $order[0];
                if ( $order['status'] != LeadsOrder::COMPLETED ) {
                    if ( $order['status'] == LeadsOrder::BLOCKED ) {
                        if ( $order['access_level'] < $access_level ) {
                            if((LeadsQuotation::getList($db, $invoice, TABLE_LEADS_QUOTATION.'.id', " 
							WHERE ".TABLE_LEADS_QUOTATION.".or_no = '".$order['number']."' AND 
							".TABLE_LEADS_QUOTATION.".status !='".LeadsQuotation::DELETED."'") ) > 0){
                             $messages->setErrorMessage("Please delete Quotations of enquiry ".$order['number']."  ");
                            
							}else{    
                                
                                /* $query = "UPDATE ". TABLE_LEADS_ORDERS
                                        ." SET status ='".LeadsOrder::DELETED."' WHERE id = '$or_id'";
                                            
                                if ( $db->query($query) && $db->affected_rows()>0 ) {                                
                                    $messages->setOkMessage("The Lead has been deleted.");
                                }
                                else {
                                    $messages->setErrorMessage("The Lead was not deleted.");
                                } */
                                                               
                            }                            
                        }
                        else {
                            $messages->setErrorMessage("Cannot Delete Lead Order with the current Access Level.");
                        }
                    }else{
                        $messages->setErrorMessage("To Delete Lead it should be Blocked.");
                    }
                }
                else {
                    $messages->setErrorMessage("Lead Order cannot be Deleted: It has been Approved.");
                }
            }
            else {
                $messages->setErrorMessage("The Lead Order was not found.");
            }
        
         
        if($messages->getErrorMessageCount() > 0){
             $perform='list';
            include ( DIR_FS_NC .'/leads-order-list.php');
        
        }else{
        
             if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save') {
                 $data		= processUserData($_POST);
                if(empty($data['reason_of_delete'])){
                    $messages->setErrorMessage("Please specify reason to delete the lead ".$data['number']);
                }
                if($messages->getErrorMessageCount() <= 0){                    
                    //Sql to delete order
                    $query = "UPDATE ". TABLE_LEADS_ORDERS
                            ." SET status ='".LeadsOrder::DELETED."', 
							reason_of_delete ='".$data['reason_of_delete']."',
                            delete_by ='".$my['uid']."',
                            delete_ip ='".$_SERVER['REMOTE_ADDR']."',
                            do_delete='".date("Y-m-d")."'
                            WHERE id = '$or_id'";                                            
                    if ( $db->query($query) && $db->affected_rows()>0 ) {                    
                        
                        header("Location:".DIR_WS_NC."/leads-order.php?perform=list&deleted=1&or_no=".
						$data['number']);
                        //Add Followup of order delete EOF
                        $messages->setOkMessage("The lead has been deleted.");                         
                    }
                    else {
                        $messages->setErrorMessage("The lead was not deleted.");
                    }        
                }                
            }
            $hidden[] = array('name'=> 'or_id' ,'value' => $or_id);
            $hidden[] = array('name'=> 'perform' ,'value' => $perform);
            $hidden[] = array('name'=> 'number' ,'value' => $order['number']);
            $hidden[] = array('name'=> 'act' , 'value' => 'save');
            $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
            $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-order-delete.html');      
        }        
          
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Delete the Order.");
    }
?>