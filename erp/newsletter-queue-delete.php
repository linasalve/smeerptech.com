<?php
    if ( $perm->has('nc_nwl_qdelete') ) {
	    $temp_news_id = isset($_GET["temp_news_id"]) ? $_GET["temp_news_id"] : ( isset($_POST["temp_news_id"]) ? $_POST["temp_news_id"] : '' );
        
        
                
        $extra = array( 'db'           => &$db,
                        'messages'     => &$messages
                        );
        Newsletter::deleteQueue($temp_news_id, $extra);
        
        // Display the list.
        include ( DIR_FS_NC .'/newsletter-queue-list.php');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this module.");
         //$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'no-delete-permission.html');
    }
?>
