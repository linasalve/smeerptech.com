<?php
 if ( $perm->has('nc_ld_qt_send') ) {
    include ( DIR_FS_INCLUDES .'/support-functions.inc.php');
	include_once ( DIR_FS_INCLUDES .'/leads-ticket.inc.php');
	include_once ( DIR_FS_INCLUDES .'/sale-lead.inc.php' );
	
    $inv_id         = isset($_GET["inv_id"]) ? $_GET["inv_id"]: ( isset($_POST["inv_id"])? $_POST["inv_id"]: '' );
	$flw_ord_id = isset($_GET['flw_ord_id']) ? $_GET['flw_ord_id'] : (isset($_POST['flw_ord_id']) ?	$_POST['flw_ord_id'] : '');
    $_ALL_POST	    = NULL;
    $condition_query = NULL;
    $access_level   = $my['access_level'];
 
	/*
	$condition_query = " WHERE (". TABLE_LEADS_QUOTATION .".id = '". $inv_id ."' "
                            ." OR ". TABLE_LEADS_QUOTATION .".number = '". $inv_id ."')";
	*/
    $condition_query = " WHERE (". TABLE_LEADS_QUOTATION .".id = '". $inv_id ."')";

    $fields = TABLE_LEADS_QUOTATION .'.number'
			.','. TABLE_LEADS_QUOTATION .'.or_no'
			.','. TABLE_LEADS_QUOTATION .'.amount'
			.','. TABLE_LEADS_QUOTATION .'.order_closed_by'
			.','. TABLE_LEADS_QUOTATION .'.currency_abbr'
			.','. TABLE_LEADS_QUOTATION .'.currency_name'
			.','. TABLE_LEADS_QUOTATION .'.do_i'
			.','. TABLE_LEADS_QUOTATION .'.do_d'
			.','. TABLE_LEADS_QUOTATION .'.attach_file1'
			.','. TABLE_LEADS_QUOTATION .'.attach_file2'
			.','. TABLE_LEADS_QUOTATION .'.attach_file3'
			.','. TABLE_LEADS_QUOTATION .'.attach_file4'
			.','. TABLE_LEADS_QUOTATION .'.attach_file5'
			.','. TABLE_SALE_LEADS .'.lead_id AS c_user_id'
			.','. TABLE_SALE_LEADS .'.number AS c_number'
			.','. TABLE_SALE_LEADS .'.f_name AS c_f_name'
			.','. TABLE_SALE_LEADS .'.l_name AS c_l_name'
			.','. TABLE_SALE_LEADS .'.billing_name'
			.','. TABLE_SALE_LEADS .'.email AS c_email'
			.','. TABLE_SALE_LEADS .'.email_1 AS c_email_1'
			.','. TABLE_SALE_LEADS .'.email_2 AS c_email_2'
			.','. TABLE_SALE_LEADS .'.email_3 AS c_email_3';
			
    if ( LeadsQuotation::getDetails($db, $_ALL_Data, $fields, $condition_query) > 0 ) {
        $_ALL_Data =  $_ALL_Data['0'];
		
        //$_ALL_POST['to']= $_ALL_Data['c_email'];
        $inv_no =  $_ALL_Data['number'];
        $_ALL_Data['amount'] =  number_format($_ALL_Data['amount'],2);
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn'])) && $_POST['act'] == 'save'){
            $_ALL_POST 	= $_POST;
			$_ALL_POST['email'] = $_ALL_Data['c_email'];
			$_ALL_POST['email_1']=$_ALL_Data['c_email_1'];
			$_ALL_POST['email_2']=$_ALL_Data['c_email_2'];
			$_ALL_POST['email_3']=$_ALL_Data['c_email_3'];
            //print_r($_ALL_POST);    
            $data		= processUserData($_ALL_POST);   
            $extra = array( 'db' 		=> &$db,
                            'messages'          => &$messages
                          );
                          
            if(LeadsQuotation::validateSendInvoice($data, $extra)){
            
                $email = NULL;
                $cc_to_sender= $cc = $bcc = Null;
                
                $data['client'] = array(
                                    "f_name" => $_ALL_Data['c_f_name'],
                                    "l_name" => $_ALL_Data['c_l_name']
                                    );
                $data['number'] = $_ALL_Data['number'];
                $data['amount'] = $_ALL_Data['amount'];
                $data['currency_name'] = $_ALL_Data['currency_name'];                 
				$data['do_i'] = date("d M Y",strtotime($_ALL_Data['do_i']));
                $data['do_d'] = date("d M Y",strtotime($_ALL_Data['do_d']));
				/* 
				$data['mkex_fname']   = '';
				$data['mkex_lname']   = '';
				$data['mkex_designation'] ='';
				$data['mkex_mobile1']   ='';					
				$data['mkex_email']   ='';			
				if(!empty($_ALL_Data['order_closed_by'])){
					$clientfields='';
					$condition = TABLE_USER." WHERE  user_id='".$_ALL_Data['order_closed_by']."'";
					$clientfields .= ' '.TABLE_USER.'.marketing_contact, '. TABLE_USER .'.desig,'
					. TABLE_USER .'.email,'
					. TABLE_USER .'.department,
					'.TABLE_USER.'.f_name, '.TABLE_USER.'.l_name';
					User::getList($db, $closed_by, $clientfields, $condition) ;
					
					$data['ord_user_id']   	= $closed_by['0']['user_id'];
					$data['department']   	= $closed_by['0']['department'];
					$data['mkex_fname']   	= $closed_by['0']['f_name'];
					$data['mkex_lname']   	= $closed_by['0']['l_name'];
					$data['display_designation']   = $closed_by['0']['desig'];
					$data['mkex_email']   = $closed_by['0']['email'];
					$data['display_mobile1'] =$data['marketing_contact']  = $closed_by['0']['marketing_contact'];
					$data['display_user_id']   = $_ALL_Data['order_closed_by'];
					$data['display_name'] = $data['mkex_fname']." ".$data['mkex_lname'];
					$data['marketing'] = 0;
					 
					if($data['department'] == ID_MARKETING){
						//This is the marketing person identity
						$data['tck_owner_member_id'] =$data['ord_user_id'] ;
						$data['tck_owner_member_name']  = $data['mkex_fname']." ".$data['mkex_lname'];
						$data['tck_owner_member_email'] = $data['mkex_email'];
						$data['marketing_email'] = $data['mkex_email'];
						$data['marketing_contact'] = $data['marketing_contact'];
						$data['marketing'] = 1;
					}else{ 
						$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
						$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
						$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;
						$data['marketing_email'] = SALES_MEMBER_USER_EMAIL;
						$data['marketing_contact'] = '';
						$data['marketing'] = 0;
					}
					
				} */
				
				
				$mail_client = $mail_to_all_su = 0;
				if(isset($data['mail_client'])){
					$mail_client = 1 ;
				}				
				if(isset($data['mail_to_all_su'])){
					$mail_to_all_su = 1 ;
				}	
				
                $temp = NULL;
                $temp_p = NULL;
                $condition_query = "WHERE ".TABLE_LEADS_ORD_P.".ord_no = '". $_ALL_Data['or_no'] ."'";
                LeadsOrder::getParticulars($db, $temp, 'particulars,ss_title,ss_punch_line,ord_no', $condition_query);
                if(!empty($temp)){
                    foreach ( $temp as $pKey => $parti ) {
                        $temp_p[] = array(
                                        'particulars'=>$temp[$pKey]['particulars'],
                                        'ss_title' =>$temp[$pKey]['ss_title'] ,                                   
                                        'ss_punch_line' =>$temp[$pKey]['ss_punch_line'] 
                                        );
                    }
                }
				
                $data['particulars'] = $temp_p ;     
                $file_name[] = DIR_FS_QUOTATIONS_PDF ."/". $data["number"] .".pdf";
				if(!empty($_ALL_Data['attach_file1'])){
					$file_name[] = DIR_FS_QUOTATIONS ."/". $_ALL_Data['attach_file1'];
				}
				if(!empty($_ALL_Data['attach_file2'])){
					$file_name[] = DIR_FS_QUOTATIONS ."/". $_ALL_Data['attach_file2'];
				}
				if(!empty($_ALL_Data['attach_file3'])){
					$file_name[] = DIR_FS_QUOTATIONS ."/". $_ALL_Data['attach_file3'];
				}
				if(!empty($_ALL_Data['attach_file4'])){
					$file_name[] = DIR_FS_QUOTATIONS ."/". $_ALL_Data['attach_file4'];
				}
				if(!empty($_ALL_Data['attach_file5'])){
					$file_name[] = DIR_FS_QUOTATIONS ."/". $_ALL_Data['attach_file5'];
				}
                $inv_no = $data["number"] ;
				$mail_send_to_su=$name='';
				//Send Company Service PDF BOF
				include_once ( DIR_FS_INCLUDES .'/support-ticket-template.inc.php');
				$id=39;
				$fields = TABLE_ST_TEMPLATE .'.*'  ;            
				$condition_query_st1 = " WHERE (". TABLE_ST_TEMPLATE .".id = '". $id ."' )";            
				SupportTicketTemplate::getDetails($db, $details, $fields, $condition_query_st1);
				if(!empty($details)){
					$details = $details[0];
					$data['template_id'] = $details['id'];
					$data['template_file_1'] = $details['file_1'];
					$data['template_file_2'] = $details['file_2'];
					$data['template_file_3'] = $details['file_3'];
					
					if(!empty($data['template_file_1'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_1'];
					}
					if(!empty($data['template_file_2'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_2'];
					}
					if(!empty($data['template_file_3'])){
						$file_name[] = DIR_FS_ST_TEMPLATES_FILES ."/". $data['template_file_3'];
					}
				}
				
                if ( getParsedEmail($db, $s, 'QUOTATION_CREATE_TO_CLIENT', $data, $email) ) {        
                    if(!empty($data['tck_owner_member_email'])){
						$email["from_name"] =  $data['tck_owner_member_name'];
						$email["from_email"] = $data['tck_owner_member_email'];
					}
					
					if(!empty($data['to'])){
						$mail_send_to_su .= ",".$data['to'];
						$to = '';
						$to[]   = array('name' => $data['to'], 'email' => $data['to']);
						$from = $reply_to = array('name' => $email["from_name"],'email' => $email['from_email']);                   	
						/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
					}
					//Send mail to sales admin as a client copy
					
					
					if($mail_client){
					
					    if(!empty($_ALL_POST['email'])){						
							$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
						    $mail_send_to_su .= ",".$_ALL_POST['email'];
							//$to = '';
							$to[]   = array('name' => $_ALL_POST['email'], 'email' => $_ALL_POST['email'] );
							$from   = $reply_to = array('name' => $email["from_name"], 
							'email' => $email['from_email']);                    		
							/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name); */
						}
						if(!empty($_ALL_POST['email_1'])){						
						
							$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
						    $mail_send_to_su .= ",".$_ALL_POST['email_1'];
							//$to = '';
							$to[]   = array('name' => $_ALL_POST['email_1'], 'email' => $_ALL_POST['email_1'] );
							$from =$reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    		
							/*  SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name); */
						}						
						if(!empty($_ALL_POST['email_2'])){						
						
							$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
						    $mail_send_to_su .= ",".$_ALL_POST['email_2'];
							//$to = '';
							$to[]   = array('name' => $_ALL_POST['email_2'], 'email' => $_ALL_POST['email_2'] );
							$from=$reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    		
							/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name); */
						}
						if(!empty($_ALL_POST['email_3'])){
							$name = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name']	;
						    $mail_send_to_su .= ",".$_ALL_POST['email_3'];
							//$to = '';
							$to[]= array('name' => $_ALL_POST['email_3'], 'email' => $_ALL_POST['email_3'] );
							$from=$reply_to = array('name' => $email["from_name"], 'email' => $email['from_email']);                    		
							/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], $email["isHTML"],
							$cc_to_sender,$cc,$bcc,$file_name); */
						}
					}
					
					if( $mail_to_all_su || !empty($data['mail_to_su']) ){
					
						if($mail_to_all_su){
							Leads::getList($db, $subUDetails, 'lead_number,lead_id as user_id, f_name, l_name,
							title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', 
							" WHERE parent_id = '".$_ALL_Data['c_user_id']."' AND status='".Leads::ACTIVE."' ORDER BY f_name");
						
						}elseif(!empty($data['mail_to_su'])){
							$mail_to_su= implode("','",$data['mail_to_su']);							
							Leads::getList($db, $subUDetails, 'lead_number,lead_id as user_id, f_name, l_name,
							title as ctitle, email,email_1,email_2, email_3,email_4,mobile1,mobile2', 
							" WHERE parent_id = '".$_ALL_Data['c_user_id']."' AND status='".Leads::ACTIVE."' 
							AND lead_id IN('".$mail_to_su."')
							ORDER BY f_name");
						}						
						if(!empty($subUDetails)){

							foreach ($subUDetails  as $key=>$value){
							
								if(!empty($subUDetails[$key]['email'])){
									$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
									$mail_send_to_su .= ",".$subUDetails[$key]['email'];
									
									//$to = '';
									$to[]   = array('name' => $name, 'email' =>$subUDetails[$key]['email'] );
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);                    				
									/* SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
								}
								if(!empty($subUDetails[$key]['email_1'])){
									$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
									$mail_send_to_su .= ",".$subUDetails[$key]['email_1'];
									
									//$to = '';
									$to[]   = array('name' => $name, 'email' =>$subUDetails[$key]['email_1'] );
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);                    			
									/* 
									SendMail($to, $from, $reply_to, $email["subject"], $email["body"], 
									$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
								}
								if(!empty($subUDetails[$key]['email_2'])){
									$name = $subUDetails[$key]['f_name']." ".$subUDetails[$key]['l_name'];
									$mail_send_to_su .= ",".$subUDetails[$key]['email_2'];
									//$to = '';
									$to[]   = array('name' => $name, 'email' =>$subUDetails[$key]['email_2'] );
									$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);                    
									/* 
									SendMail($to, $from, $reply_to, $email["subject"], 
									$email["body"],$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name); */
								}
							}
						}
						
						
					}
                }
				
				if(!empty($to)){
					$send_sales=1;
					$bcc[]   = array('name' => $name, 'email' => $sales_email);
					$bcc[]   = array('name' => $name, 'email' => $smeerp_client_email);
					 
					$from   = $reply_to = array('name' => $email["from_name"], 
									'email' => $email['from_email']);  
					//echo $email["body"] ;
					SendMail($to, $from, $reply_to, $email["subject"], 
					$email["body"],$email["isHTML"],$cc_to_sender,$cc,$bcc,$file_name);
				}
				
				//POST DATA in LEADS PROPOSAL FOLLOWUP BOF
				
				$data['ticket_owner_uid'] = $_ALL_Data['c_user_id'];
				$data['ticket_owner'] = $_ALL_Data['c_f_name']." ".$_ALL_Data['c_l_name'];
				if(!empty($flw_ord_id)){					
					$sql2 = " SELECT ticket_id FROM ".TABLE_LD_TICKETS." WHERE 
					".TABLE_LD_TICKETS.".flw_ord_id = '".$flw_ord_id."' AND ".TABLE_LD_TICKETS.".ticket_child ='0' LIMIT 0,1";
					if ( $db->query($sql2) ) {
						$ticket_id=0;
						if ( $db->nf() > 0 ) {
							while ($db->next_record()) {
							   $ticket_id= $db->f('ticket_id');
							}
						}
					}				 
				}
				$ticket_no  =  LeadsTicket::getNewNumber($db);
				
				//disply random name BOF 				 
				/* if($data['department'] == ID_MARKETING){
					//This is the marketing person identity
					$data['tck_owner_member_id'] = $my['user_id'];
					$data['tck_owner_member_name']  = $my['f_name']." ".$my['l_name'];
					$data['tck_owner_member_email'] = $my['email'];
				}else{ 
					$data['tck_owner_member_id'] = SALES_MEMBER_USER_ID;
					$data['tck_owner_member_name']  =SALES_MEMBER_USER_NAME;
					$data['tck_owner_member_email'] =SALES_MEMBER_USER_EMAIL;
				} */
				$data['ticket_subject'] = $email["subject"];
				$data['ticket_text'] = $email["body"];
				$data['hrs'] =0;
				$data['min'] =10;
				$hrs1 = (int) ($data['hrs'] * 6);
				$min1 = (int) ($data['min'] * 6);  
				
				//disply random name EOF				
				if($ticket_id>0){
				
					$query= "SELECT ". TABLE_LD_TICKETS .".ticket_date, ".TABLE_LD_TICKETS.".status "
							." FROM ". TABLE_LD_TICKETS 
							." WHERE ". TABLE_LD_TICKETS .".ticket_owner_uid = '". $data['ticket_owner_uid'] ."' " 
							 ." AND  (". TABLE_LD_TICKETS .".ticket_id = '". $ticket_id ."' ) " 
							/* ." AND  (". TABLE_LD_TICKETS .".ticket_child = '". $ticket_id ."' " 
										." OR "
										. TABLE_LD_TICKETS .".ticket_id = '". $ticket_id ."' " 
									.") " */
							." ORDER BY ". TABLE_LD_TICKETS .".ticket_date DESC LIMIT 0,1";				
						
					$db->query($query) ;
					if($db->nf() > 0){
						$db->next_record() ;
						$last_time = $db->f("ticket_date") ;
						$ticket_response_time = time() - $last_time ;
						$status = $db->f("status") ;
					}else{
						$ticket_response_time = 0 ;
					}
					$ticket_date 	= time() ;
					$ticket_replied = '0' ;
					
					$query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
					. TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "
					. TABLE_LD_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
					. TABLE_LD_TICKETS .".sent_inv_id         = '".$inv_id ."', "
                    . TABLE_LD_TICKETS .".sent_inv_no         = '".$inv_no ."', "
					. TABLE_LD_TICKETS .".template_id       = '". $data['template_id'] ."', "
					. TABLE_LD_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
					. TABLE_LD_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
					. TABLE_LD_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
					. TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
					. TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
					. TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', " 
                    . TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
                    . TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
					. TABLE_LD_TICKETS .".ticket_creator_uid = '". $my['uid'] ."', "
					. TABLE_LD_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
					. TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
					. TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
					. TABLE_LD_TICKETS .".ticket_status     = '".LeadsTicket::PENDINGWITHCLIENTS."', "
					. TABLE_LD_TICKETS .".ticket_child      = '".$ticket_id."', "
					. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
					. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
					. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
					. TABLE_LD_TICKETS .".min = '". $data['min']."', "
					. TABLE_LD_TICKETS .".ticket_response_time = '".$ticket_response_time."', "
					. TABLE_LD_TICKETS .".ticket_replied    = '".$ticket_replied."', "
					. TABLE_LD_TICKETS .".from_admin_panel  = '".LeadsTicket::ADMIN_PANEL."', "
					. TABLE_LD_TICKETS .".mail_client    = '".$mail_client."', "
					. TABLE_LD_TICKETS .".mail_to_all_su    = '".$mail_to_all_su."', "
					. TABLE_LD_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
					. TABLE_LD_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', "
					. TABLE_LD_TICKETS .".display_name           = '". $data['display_name'] ."', "
					. TABLE_LD_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
					. TABLE_LD_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
					. TABLE_LD_TICKETS .".ip                = '".$_SERVER['REMOTE_ADDR']."', "
					. TABLE_LD_TICKETS .".do_e              = '".date('Y-m-d H:i:s')."', "
					. TABLE_LD_TICKETS .".do_comment        = '".date('Y-m-d H:i:s')."', "
					. TABLE_LD_TICKETS .".ticket_date        = '". $ticket_date ."' " ;
					
					$db->query($query) ;
					$variables['hid'] =  $db->last_inserted_id() ;

					$query_update = "UPDATE ". TABLE_LD_TICKETS 
							." SET ". TABLE_LD_TICKETS .".ticket_status = '".LeadsTicket::PENDINGWITHCLIENTS."'"  
							.",".TABLE_LD_TICKETS.".do_comment = '".date('Y-m-d H:i:s')."'"
							.",".TABLE_LD_TICKETS.".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."'"
							.",".TABLE_LD_TICKETS.".last_comment = '".$data['ticket_text']."'"
							.",".TABLE_LD_TICKETS.".last_comment_by = '".$my['uid'] ."'"
							.",".TABLE_LD_TICKETS.".last_comment_by_name = '".$my['f_name']." ". $my['l_name'] ."'"
							.",".TABLE_LD_TICKETS.".status='".LeadsTicket::ACTIVE."'"
							." WHERE ". TABLE_LD_TICKETS .".ticket_id = '". $ticket_id ."' " ;
				    $db->query($query_update) ;
				
				}else{
					//Create New Ticket 
					$query = "INSERT INTO "	. TABLE_LD_TICKETS ." SET "
                        . TABLE_LD_TICKETS .".ticket_no         = '". $ticket_no ."', "
                        . TABLE_LD_TICKETS .".flw_ord_id         = '". $flw_ord_id ."', "
                        . TABLE_LD_TICKETS .".sent_inv_id         = '". $inv_id ."', "
                        . TABLE_LD_TICKETS .".sent_inv_no         = '". $inv_no ."', "
						. TABLE_LD_TICKETS .".template_id       = '". $data['template_id'] ."', "
						. TABLE_LD_TICKETS .".template_file_1   = '". $data['template_file_1'] ."', "
						. TABLE_LD_TICKETS .".template_file_2   = '". $data['template_file_2'] ."', "
						. TABLE_LD_TICKETS .".template_file_3   = '". $data['template_file_3'] ."', "
                        . TABLE_LD_TICKETS .".ticket_owner_uid  = '". $data['ticket_owner_uid'] ."', "
                        . TABLE_LD_TICKETS .".ticket_owner      = '". $data['ticket_owner'] ."', "
                        . TABLE_LD_TICKETS .".tck_owner_member_id   = '". $data['tck_owner_member_id'] ."', "
                        . TABLE_LD_TICKETS .".tck_owner_member_name = '". $data['tck_owner_member_name'] ."', "
                        . TABLE_LD_TICKETS .".tck_owner_member_email= '". $data['tck_owner_member_email'] ."', "
                        . TABLE_LD_TICKETS .".ticket_creator_uid= '". $my['uid'] ."', "
                        . TABLE_LD_TICKETS .".ticket_creator    = '". $my['f_name']." ". $my['l_name'] ."', "
                        . TABLE_LD_TICKETS .".ticket_subject    = '". $data['ticket_subject'] ."', "
                        . TABLE_LD_TICKETS .".ticket_text       = '". $data['ticket_text'] ."', "
                        . TABLE_LD_TICKETS .".mail_to_all_su        = '".$mail_to_all_su."', "
                        . TABLE_LD_TICKETS .".mail_to_additional_email = '".$mail_to_additional_email."', "
                        . TABLE_LD_TICKETS .".mail_send_to_su = '".trim($mail_send_to_su,",")."', "
                        . TABLE_LD_TICKETS .".ticket_status     = '". LeadsTicket::PENDINGWITHCLIENTS ."', "
						. TABLE_LD_TICKETS.".status='".LeadsTicket::ACTIVE."',"
                        . TABLE_LD_TICKETS .".ticket_child      = '0', "
						. TABLE_LD_TICKETS .".hrs1 = '". $hrs1 ."', "
						. TABLE_LD_TICKETS .".min1 = '". $min1 ."', "
						. TABLE_LD_TICKETS .".hrs = '". $data['hrs'] ."', "
						. TABLE_LD_TICKETS .".min = '". $data['min']."', "                        
                        . TABLE_LD_TICKETS .".ticket_response_time = '0', "
                        . TABLE_LD_TICKETS .".ticket_replied        = '0', "
                        . TABLE_LD_TICKETS .".from_admin_panel        = '".LeadsTicket::ADMIN_PANEL."', "
                        . TABLE_LD_TICKETS .".display_name           = '". $data['display_name'] ."', "
                        . TABLE_LD_TICKETS .".display_user_id        = '". $data['display_user_id'] ."', "
                        . TABLE_LD_TICKETS .".display_designation    = '". $data['display_designation'] ."', "
                        . TABLE_LD_TICKETS .".ip    = '".$_SERVER['REMOTE_ADDR']."', "
						. TABLE_LD_TICKETS .".do_e    = '".date('Y-m-d H:i:s')."', "
						. TABLE_LD_TICKETS .".do_comment   = '".date('Y-m-d H:i:s')."', "
						. TABLE_LD_TICKETS .".last_comment_from = '".LeadsTicket::ADMIN_COMMENT."', "
                        . TABLE_LD_TICKETS .".ticket_date       = '". time() ."' " ;
					$db->query($query) ;
					$variables['hid'] =  $db->last_inserted_id() ;
				}
				//POST DATA in LEADS PROPOSAL FOLLOWUP EOF
				
				 
                $messages->setOkMessage("Proposal has been sent successfully on 
				".trim($mail_send_to_su,",")." email id.");  
            }   
        }else{
			$_ALL_POST['email'] = $_ALL_Data['c_email'];
			$_ALL_POST['email_1']=$_ALL_Data['c_email_1'];
			$_ALL_POST['email_2']=$_ALL_Data['c_email_2'];
			$_ALL_POST['email_3']=$_ALL_Data['c_email_3'];
		}
	
		$variables['can_view_client_details']=1;
		
		Leads::getList($db, $subUserDetails, 'lead_number,lead_id as user_id, f_name, l_name,title as ctitle, 	
		email,email_1,email_2, email_3,email_4,mobile1,mobile2', " 
		WHERE parent_id = '".$_ALL_Data['c_user_id']."' AND status='".Leads::ACTIVE."' 
		ORDER BY f_name");
		
		 
		
        
        $hidden[] = array('name'=> 'inv_id' ,'value' => $inv_id);
        $hidden[] = array('name'=> 'inv_no' ,'value' => $inv_no);
        $hidden[] = array('name'=> 'flw_ord_id' ,'value' => $flw_ord_id);
        $hidden[] = array('name'=> 'perform' ,'value' => 'send');
        $hidden[] = array('name'=> 'act' , 'value' => 'save');

        $page["var"][] = array('variable' => 'hidden', 'value' => 'hidden');
        $page["var"][] = array('variable' => 'subUserDetails', 'value' => 'subUserDetails');
        $page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
        $page["var"][] = array('variable' => 'variables', 'value' => 'variables');
        $page["var"][] = array('variable' => '_ALL_Data', 'value' => '_ALL_Data');
        $page["var"][] = array('variable' => 'inv_no', 'value' => 'inv_no');
        $page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'leads-quotation-send.html');
        
    }else{
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
    }
}else{
 
    $messages->setErrorMessage("You donot have the Right to Send Quotation.");
}
?>