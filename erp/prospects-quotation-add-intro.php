<?php
    if ( $perm->has('nc_ps_qt_add_intro') ) {
       //include_once ( DIR_FS_INCLUDES .'/currency.inc.php');      
       include_once ( DIR_FS_INCLUDES .'/prospects.inc.php' );
       include_once ( DIR_FS_INCLUDES .'/financial-year.inc.php' );
       include_once ( DIR_FS_INCLUDES .'/service-tax.inc.php' );
       include_once ( DIR_FS_INCLUDES .'/prospects-quotation.inc.php');
	   
        $or_id          = isset($_GET["or_id"]) ? $_GET["or_id"] : ( isset($_POST["or_id"]) ? $_POST["or_id"] : '' );
        $_ALL_POST      = NULL;
        $table = TABLE_PROSPECTS_QUOT_INTRO;
		$condition2 = " WHERE ".TABLE_PROSPECTS_QUOT_INTRO .".id= '1' " ;
		$fields1 = "*" ;
		$_ALL_POST2 = getRecord($table,$fields1,$condition2);
		$_ALL_POST2['current_page']		=	'intro_page1_id';
		$_ALL_POST2['current_page_type']	=	'intro';
		$_ALL_POST = $_ALL_POST2;
		
        $data           = NULL; 
        $extra = array( 'db' 				=> &$db,
                        'access_level'      => $access_level,
                         'messages'          => &$messages
                        ); 
        
        if ( (isset($_POST['btnCreate']) || isset($_POST['btnReturn']) ) &&	$_POST['act'] == 'save') {
            $_ALL_POST1 	= $_POST;
            $data		= processUserData($_ALL_POST1); 
			$_ALL_POST['current_page']	= $_ALL_POST1['current_page'];
			$_ALL_POST['current_page_type']=$_ALL_POST1['current_page_type'];
			if($_ALL_POST1['current_page']!=''){
				$current_page = $_ALL_POST1['current_page'];
			} 
			$pageArr = explode('_id',$current_page);
			/* if(empty($data[$pageArr[0]])){  
				
				$messages->setErrorMessage("Please enter the data");  
			}else{
				$query = "UPDATE ".TABLE_PROSPECTS_QUOT_INTRO." SET ".TABLE_PROSPECTS_QUOT_INTRO.".".$pageArr[0]." = '". $data[$pageArr[0]]."'
					WHERE ".TABLE_PROSPECTS_QUOT_INTRO.".id='1'	";
				if ( $db->query($query) ) {
					$_ALL_POST[$pageArr[0]] = $_ALL_POST1[$pageArr[0]];
                    $messages->setOkMessage("Record updated successfully."); 
                }
			} */
				$query = "UPDATE ".TABLE_PROSPECTS_QUOT_INTRO." SET ".TABLE_PROSPECTS_QUOT_INTRO.".".$pageArr[0]." = '". $data[$pageArr[0]]."'
					WHERE ".TABLE_PROSPECTS_QUOT_INTRO.".id='1'	";
				if ( $db->query($query) ) {
					$_ALL_POST[$pageArr[0]] = $_ALL_POST1[$pageArr[0]];
                    $messages->setOkMessage("Record updated successfully."); 
                }
        } 
         
	
        if(isset($_POST['btnCreate']) && $messages->getErrorMessageCount() <= 0 ){
        
           //header("Location:".DIR_WS_NC."/prospects-quotation.php?perform=add-intro&added=1");           
        } 
        
        // Check if the Form to add is to be displayed or the control is to be sent to the List page.
        if ( isset($_POST['btnReturn']) && $messages->getErrorMessageCount() <= 0 ) {
             header("Location:".DIR_WS_NC."/prospects-quotation.php?perform=add-intro&added=1");           
        } else { 
			
			
		
			$file_path = DIR_WS_PST_QUOT_PDF;
			$hidden[] = array('name'=> 'perform' ,'value' => 'add-intro');
			$hidden[] = array('name'=> 'act' , 'value' => 'save');

			$page["var"][] = array('variable' => 'hidden', 'value' => 'hidden'); 
			$page["var"][] = array('variable' => 'messages', 'value' => 'messages');
			$page["var"][] = array('variable' => '_ALL_POST', 'value' => '_ALL_POST');
			$page["var"][] = array('variable' => 'file_path', 'value' => 'file_path');
			$page["section"][] = array('container'=>'CONTENT_MAIN', 'page' => 'prospects-quotation-add-intro.html');  
        }
    }
    else {
        $messages->setErrorMessage("You donot have the Permisson to Access this module.");
    }
?>