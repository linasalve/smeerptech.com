<?php
	// include all files required for menu.
	if ( !defined('THIS_DOMAIN') ) {
		require("../lib/config.php");
	}
    
    page_open(array("sess" => "NC_Session",
                    "auth" => "NC_Auth",
                    "perm" => "NC_Perm"
                   ));
    
    include_once ( DIR_FS_NC ."/header.php" );
    include_once ( DIR_FS_INCLUDES .'/leads-order.inc.php');
    include_once ( DIR_FS_INCLUDES .'/leads-quotation.inc.php');
 
    
	
    $sString    = isset($_GET["sString"])   ? $_GET["sString"]  : ( isset($_POST["sString"])    ? $_POST["sString"] : '' );
    $sType      = isset($_GET["sType"])     ? $_GET["sType"]    : ( isset($_POST["sType"])      ? $_POST["sType"]   : '' );
	$sOrder		= isset($_GET["sOrder"]) 	? $_GET["sOrder"]	: ( isset($_POST["sOrder"]) 	? $_POST["sOrder"]  : '' );
	$sOrderBy	= isset($_GET["sOrderBy"])	? $_GET["sOrderBy"]	: ( isset($_POST["sOrderBy"]) 	? $_POST["sOrderBy"]: '' );
	$perform	= isset($_GET["perform"]) 	? $_GET["perform"]	: ( isset($_POST["perform"]) 	? $_POST["perform"] : '' );
	$x 			= isset($_GET["x"])         ? $_GET["x"]        : ( isset($_POST["x"])          ? $_POST["x"]       : '');
    $rpp 		= isset($_GET["rpp"])       ? $_GET["rpp"]      : ( isset($_POST["rpp"])  ? $_POST["rpp"]     : 
	( defined('RESULTS_PER_PAGE') ? RESULTS_PER_PAGE : 20));// Result per page
    $added 		= isset($_GET["added"])     ? $_GET["added"]    : ( isset($_POST["added"])      ? $_POST["added"]   : '0');
    $deleted 	= isset($_GET["deleted"])   ? $_GET["deleted"]  : ( isset($_POST["deleted"])    ? $_POST["deleted"] : '0');
    $updated 	= isset($_GET["updated"])   ? $_GET["updated"]  : ( isset($_POST["updated"])    ? $_POST["updated"] : '0');
    $or_no 		= isset($_GET["or_no"])     ? $_GET["or_no"]    : ( isset($_POST["or_no"])      ? $_POST["or_no"]   : '');
    $hid 		= isset($_GET["hid"])         ? $_GET["hid"]        : ( isset($_POST["hid"])          ? $_POST["hid"]       :'');
	
	$action="Lead Orders" ;
    $condition_url ='';
    $condition_query = '';
    if ( empty($x) ) {
        $x              = 1;
        $next_record    = 0 ;
    } else {
        $next_record    = ($x-1) * $rpp;
    }
    $variables["x"]     = $x;
    $variables["rpp"]   = $rpp;
     //By default search in On
    $_SEARCH["searched"]    = true ;
     // Read the available Status 
    $variables['types'] = LeadsOrder::getTypes();
    
    if($added){
    
        $messages->setOkMessage("New Order has been created.");
    }
    if($updated){
    
        $messages->setOkMessage("Order has been updated.");
    }
    if($deleted){    
        $messages->setOkMessage("Order $or_no has been deleted.");
    }
    if ( $perm->has('nc_ld_or') ) {

        $sTypeArray     = array('Any'           =>  array(  'Any of following'  => '-1'),
                                TABLE_LEADS_ORDERS   =>  array(  'Order No.'     => 'number',
                                                                'Order Title'   => 'order_title',
                                                               //'Particulars'   => 'particulars',
                                                                'Details'       => 'details'
                                                        ),
                                 // * column names with -, inserted to distinguish between the coloumn 
                                //* of different tables with same name.
                                TABLE_USER      =>  array(  'Creator Number'    => 'number-user',
                                                            'Creator Username'  => 'username-user',
                                                            'Creator Email'     => 'email-user',
                                                            'Creator First Name'=> 'f_name-user',
                                                            'Creator Last Name' => 'l_name-user',
                                                ),
								TABLE_SALE_LEADS 	=>  array( //'Prospect Number'    => 'number-clients',
                                                            'Prospect Company'  => 'company_name-'.TABLE_SALE_LEADS,
                                                           // 'Prospect Username'  => 'username-'.TABLE_SALE_LEADS,
                                                            'Prospect Email'     => 'email-'.TABLE_SALE_LEADS,
                                                            'Prospect First Name'=> 'f_name-'.TABLE_SALE_LEADS,
                                                            'Prospect Last Name' => 'l_name-'.TABLE_SALE_LEADS,
                                                ),
								
                            );
        
        $sOrderByArray  = array(
                                TABLE_LEADS_ORDERS => array( 'Date of Order'     => 'do_o',
                                                            'Date of Delivery'  => 'do_d',
                                                            'Date of Creation'  => 'do_c',
                                                            'Status'            => 'status',
                                                            'Delay'            => 'delay'
														),
                            );
    
        // Set the sorting order of the user list.
        if ( !($order_by_table = findIndex($sOrderBy, $sOrderByArray)) ) {
          
            if(!empty($hid)){
                $_SEARCH['sOrderBy']= $sOrderBy = 'id';
                $variables['hid']=$hid;
            }else{
                $_SEARCH['sOrderBy']= $sOrderBy = 'do_o';
            }
            $_SEARCH['sOrder']  = $sOrder   = 'DESC';
            $order_by_table     = TABLE_LEADS_ORDERS;
        }

        // Read the available Status for the Pre Order.
        $variables['status'] = LeadsOrder::getStatus();
        $variables['flwstatus'] = LeadsOrder::getFollowupStatus();
        
        
        //use switch case here to perform action. 
        switch ($perform) {
           
        
            case ('add'): {            
				$action.=' Add';
                include (DIR_FS_NC.'/leads-order-add.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            } 
			case ('approval'): {
				$action.=' Add';
                include (DIR_FS_NC.'/leads-order-approval.php');                 
                break;
            }
			case ('edit_pre_order'):
            case ('edit'): {	
				$action.=' Edit';
                include (DIR_FS_NC .'/leads-order-edit.php');
                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
			case ('followup'): {
                include (DIR_FS_NC .'/leads-order-followup.php');
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('view_pre_order'):
            case ('view'): {
                include (DIR_FS_NC .'/leads-order-view.php');                
                $page["section"][] = array('container'=>'POPUP', 'page' => 'popup.html');
                break;
            }
            case ('change_status'): {
                $searchStr=1;
                include ( DIR_FS_NC .'/leads-order-status.php');                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('search'): {
				$searchStr=1;
                include(DIR_FS_NC."/leads-order-search.php");                
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('delete_pre_order'):
            case ('delete'): {
                include ( DIR_FS_NC .'/leads-order-delete.php');
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
            case ('view_attachment'): {
             
                include (DIR_FS_NC .'/leads-order-view-attachment.php');
                break;
            }     
            /*EOF added on 28-april-2009*/
            case ('list_pre_order'):
            case ('list'):
            default: {
                $searchStr = 1;
                include (DIR_FS_NC .'/leads-order-list.php');
                // CONTENT = CONTENT
                $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-order.html');
                $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
                break;
            }
        }
        $page["var"][] = array('variable' => 'sTypeArray', 'value' => 'sTypeArray');
        $page["var"][] = array('variable' => 'sOrderByArray', 'value' => 'sOrderByArray');
    }
    else {
        $messages->setErrorMessage("You donot have the Right to Access this Module.");
        
        $page["section"][] = array('container'=>'CONTENT', 'page' => 'leads-order.html');
        $page["section"][] = array('container'=>'INDEX', 'page' => 'index.html');
    }
    
    // always assign
    $s->assign("action", $action);
    $s->assign("variables", $variables);
    $s->assign("error_messages", $messages->getErrorMessages());
    $s->assign("success_messages", $messages->getOkMessages());

    if ( isset($page["var"]) && is_array($page["var"]) ) {
        foreach ( $page["var"] as $key=>$fetch ) {
            $s->assign( $fetch["variable"], ${$fetch["value"]});
        }
    }
    if ( isset($page["section"]) && is_array($page["section"]) ) {
        $sections = count($page["section"]);
        for ( $i=0; $i<($sections-1); $i++) {
            $s->assign( $page["section"][$i]["container"], $s->fetch($page["section"][$i]["page"]));
        }
    }
    $s->display($page["section"][$sections-1]["page"]);
	include_once( DIR_FS_NC ."/flush.php");
?>